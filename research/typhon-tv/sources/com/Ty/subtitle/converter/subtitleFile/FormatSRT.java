package com.Ty.subtitle.converter.subtitleFile;

import com.Ty.subtitle.converter.subtitleFile.model.Caption;
import com.Ty.subtitle.converter.subtitleFile.model.Time;
import com.Ty.subtitle.converter.subtitleFile.model.TimedTextObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class FormatSRT implements TimedTextFileFormat {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f6516 = Pattern.compile(".*(\\d+):(\\d+):(\\d+),(\\d+).*-->.*(\\d+):(\\d+):(\\d+),(\\d+).*");

    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r4 = r3.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00fb, code lost:
        if (r4.startsWith(org.apache.commons.lang3.StringUtils.LF) == false) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00fd, code lost:
        r4 = r4.substring(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0102, code lost:
        r4 = r4.trim().replaceAll("\\{\\\\(.|\n)*?\\}", "");
        r2 = new com.Ty.subtitle.converter.subtitleFile.model.Caption();
        r2.f6521 = new com.Ty.subtitle.converter.subtitleFile.model.Time(r13);
        r2.f6520 = new com.Ty.subtitle.converter.subtitleFile.model.Time(r8);
        r2.f6517 = r4;
        r5.put(java.lang.Integer.valueOf(r13), r2);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.TreeMap<java.lang.Integer, com.Ty.subtitle.converter.subtitleFile.model.Caption> m7478(java.io.InputStreamReader r19) throws java.io.IOException {
        /*
            r18 = this;
            java.util.TreeMap r5 = new java.util.TreeMap
            r5.<init>()
            java.io.BufferedReader r1 = new java.io.BufferedReader
            r0 = r19
            r1.<init>(r0)
            r9 = 0
            r12 = 0
        L_0x000e:
            java.lang.String r10 = r1.readLine()     // Catch:{ Exception -> 0x00e2 }
            if (r10 == 0) goto L_0x0133
            boolean r15 = r10.isEmpty()     // Catch:{ Exception -> 0x00d9 }
            if (r15 != 0) goto L_0x000e
            java.lang.String r14 = r1.readLine()     // Catch:{ Exception -> 0x00d9 }
            if (r14 == 0) goto L_0x000e
            java.util.regex.Pattern r15 = f6516     // Catch:{ Exception -> 0x00d9 }
            java.util.regex.Matcher r11 = r15.matcher(r14)     // Catch:{ Exception -> 0x00d9 }
            boolean r15 = r11.matches()     // Catch:{ Exception -> 0x00d9 }
            if (r15 == 0) goto L_0x000e
            if (r9 != 0) goto L_0x0043
            r15 = 1
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            r16 = 3600000(0x36ee80, float:5.044674E-39)
            int r12 = r15 * r16
            if (r12 <= 0) goto L_0x00ed
            r15 = 1
        L_0x003f:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r15)     // Catch:{ Exception -> 0x00d9 }
        L_0x0043:
            r13 = 0
            r15 = 1
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            r16 = 3600000(0x36ee80, float:5.044674E-39)
            int r15 = r15 * r16
            int r15 = r15 - r12
            int r13 = r13 + r15
            r15 = 2
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            r16 = 60000(0xea60, float:8.4078E-41)
            int r15 = r15 * r16
            int r13 = r13 + r15
            r15 = 3
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = r15 * 1000
            int r13 = r13 + r15
            r15 = 4
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            int r13 = r13 + r15
            r8 = 0
            r15 = 5
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            r16 = 3600000(0x36ee80, float:5.044674E-39)
            int r15 = r15 * r16
            int r15 = r15 - r12
            int r8 = r8 + r15
            r15 = 6
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            r16 = 60000(0xea60, float:8.4078E-41)
            int r15 = r15 * r16
            int r8 = r8 + r15
            r15 = 7
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = r15 * 1000
            int r8 = r8 + r15
            r15 = 8
            java.lang.String r15 = r11.group(r15)     // Catch:{ Exception -> 0x00d9 }
            int r15 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x00d9 }
            int r8 = r8 + r15
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r15 = ""
            r3.<init>(r15)     // Catch:{ Exception -> 0x00d9 }
        L_0x00b8:
            java.lang.String r6 = r1.readLine()     // Catch:{ Exception -> 0x00d9 }
            if (r6 == 0) goto L_0x00f0
            java.lang.String r15 = r6.trim()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)     // Catch:{ Exception -> 0x00d9 }
            if (r15 != 0) goto L_0x00f0
            java.lang.String r6 = r6.trim()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r15 = "\n"
            r3.append(r15)     // Catch:{ Exception -> 0x00d9 }
            r3.append(r6)     // Catch:{ Exception -> 0x00d9 }
            goto L_0x00b8
        L_0x00d9:
            r7 = move-exception
            r15 = 0
            boolean[] r15 = new boolean[r15]     // Catch:{ Exception -> 0x00e2 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r15)     // Catch:{ Exception -> 0x00e2 }
            goto L_0x000e
        L_0x00e2:
            r7 = move-exception
            r15 = 0
            boolean[] r15 = new boolean[r15]     // Catch:{ all -> 0x012e }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r15)     // Catch:{ all -> 0x012e }
            r1.close()
        L_0x00ec:
            return r5
        L_0x00ed:
            r15 = 0
            goto L_0x003f
        L_0x00f0:
            java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r15 = "\n"
            boolean r15 = r4.startsWith(r15)     // Catch:{ Exception -> 0x00d9 }
            if (r15 == 0) goto L_0x0102
            r15 = 1
            java.lang.String r4 = r4.substring(r15)     // Catch:{ Exception -> 0x00d9 }
        L_0x0102:
            java.lang.String r15 = r4.trim()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r16 = "\\{\\\\(.|\n)*?\\}"
            java.lang.String r17 = ""
            java.lang.String r4 = r15.replaceAll(r16, r17)     // Catch:{ Exception -> 0x00d9 }
            com.Ty.subtitle.converter.subtitleFile.model.Caption r2 = new com.Ty.subtitle.converter.subtitleFile.model.Caption     // Catch:{ Exception -> 0x00d9 }
            r2.<init>()     // Catch:{ Exception -> 0x00d9 }
            com.Ty.subtitle.converter.subtitleFile.model.Time r15 = new com.Ty.subtitle.converter.subtitleFile.model.Time     // Catch:{ Exception -> 0x00d9 }
            r15.<init>(r13)     // Catch:{ Exception -> 0x00d9 }
            r2.f6521 = r15     // Catch:{ Exception -> 0x00d9 }
            com.Ty.subtitle.converter.subtitleFile.model.Time r15 = new com.Ty.subtitle.converter.subtitleFile.model.Time     // Catch:{ Exception -> 0x00d9 }
            r15.<init>(r8)     // Catch:{ Exception -> 0x00d9 }
            r2.f6520 = r15     // Catch:{ Exception -> 0x00d9 }
            r2.f6517 = r4     // Catch:{ Exception -> 0x00d9 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x00d9 }
            r5.put(r15, r2)     // Catch:{ Exception -> 0x00d9 }
            goto L_0x000e
        L_0x012e:
            r15 = move-exception
            r1.close()
            throw r15
        L_0x0133:
            r1.close()
            goto L_0x00ec
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Ty.subtitle.converter.subtitleFile.FormatSRT.m7478(java.io.InputStreamReader):java.util.TreeMap");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String[] m7479(Caption caption) {
        String[] split = caption.f6517.split("<br />");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].replaceAll("\\<.*?\\>", "");
        }
        return split;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TimedTextObject m7481(String str, InputStream inputStream, String str2) throws IOException {
        InputStreamReader inputStreamReader;
        Caption caption;
        String str3;
        TimedTextObject timedTextObject = new TimedTextObject();
        try {
            inputStreamReader = new InputStreamReader(inputStream, str2);
        } catch (UnsupportedEncodingException e) {
            inputStreamReader = new InputStreamReader(inputStream);
        }
        timedTextObject.f6542 = str;
        TreeMap<Integer, Caption> r16 = m7478(inputStreamReader);
        if (!r16.isEmpty()) {
            timedTextObject.f6539 = r16;
        } else {
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            Caption caption2 = new Caption();
            int i = 1;
            String replace = bufferedReader.readLine().replace("﻿", "");
            int i2 = 0;
            Caption caption3 = caption2;
            while (replace != null) {
                try {
                    String trim = replace.trim();
                    i2++;
                    if (!trim.isEmpty()) {
                        boolean z = false;
                        try {
                            if (Integer.parseInt(trim) != i) {
                                throw new Exception();
                            }
                            i++;
                            z = true;
                            if (z) {
                                i2++;
                                try {
                                    trim = bufferedReader.readLine().trim();
                                    String substring = trim.substring(0, 12);
                                    String substring2 = trim.substring(trim.length() - 12, trim.length());
                                    caption3.f6521 = new Time("hh:mm:ss,ms", substring);
                                    caption3.f6520 = new Time("hh:mm:ss,ms", substring2);
                                } catch (Exception e2) {
                                    timedTextObject.f6540 += "incorrect time format at line " + i2;
                                    z = false;
                                }
                            }
                            if (z) {
                                i2++;
                                str3 = bufferedReader.readLine().trim();
                                String str4 = "";
                                while (!str3.isEmpty()) {
                                    str4 = str4 + str3 + "<br />";
                                    str3 = bufferedReader.readLine().trim();
                                    i2++;
                                }
                                caption3.f6517 = str4;
                                int i3 = caption3.f6521.f6533;
                                while (timedTextObject.f6539.containsKey(Integer.valueOf(i3))) {
                                    i3++;
                                }
                                if (i3 != caption3.f6521.f6533) {
                                    timedTextObject.f6540 += "caption with same start time found...\n\n";
                                }
                                timedTextObject.f6539.put(Integer.valueOf(i3), caption3);
                            }
                            while (!str3.isEmpty()) {
                                str3 = bufferedReader.readLine().trim();
                                i2++;
                            }
                            caption = new Caption();
                        } catch (Exception e3) {
                            timedTextObject.f6540 += i + " expected at line " + i2;
                            timedTextObject.f6540 += "\n skipping to next line\n\n";
                        }
                    } else {
                        caption = caption3;
                    }
                    try {
                        replace = bufferedReader.readLine();
                        caption3 = caption;
                    } catch (NullPointerException e4) {
                        try {
                            timedTextObject.f6540 += "unexpected end of file, maybe last caption is not complete.\n\n";
                            bufferedReader.close();
                            timedTextObject.f6537 = true;
                            return timedTextObject;
                        } catch (Throwable th) {
                            th = th;
                            bufferedReader.close();
                            throw th;
                        }
                    }
                } catch (NullPointerException e5) {
                    Caption caption4 = caption3;
                } catch (Throwable th2) {
                    th = th2;
                    Caption caption5 = caption3;
                    bufferedReader.close();
                    throw th;
                }
            }
            bufferedReader.close();
        }
        timedTextObject.f6537 = true;
        return timedTextObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String[] m7480(TimedTextObject timedTextObject) {
        if (!timedTextObject.f6537) {
            return null;
        }
        int i = 0;
        ArrayList arrayList = new ArrayList(timedTextObject.f6539.size() * 5);
        int i2 = 1;
        for (Caption next : timedTextObject.f6539.values()) {
            int i3 = i + 1;
            int i4 = i2 + 1;
            arrayList.add(i, Integer.toString(i2));
            if (timedTextObject.f6538 != 0) {
                next.f6521.f6533 += timedTextObject.f6538;
                next.f6520.f6533 += timedTextObject.f6538;
            }
            int i5 = i3 + 1;
            arrayList.add(i3, next.f6521.m7492("hh:mm:ss,ms") + " --> " + next.f6520.m7492("hh:mm:ss,ms"));
            if (timedTextObject.f6538 != 0) {
                next.f6521.f6533 -= timedTextObject.f6538;
                next.f6520.f6533 -= timedTextObject.f6538;
            }
            String[] r11 = m7479(next);
            int i6 = 0;
            while (i6 < r11.length) {
                arrayList.add(i5, "" + r11[i6]);
                i6++;
                i5++;
            }
            arrayList.add(i5, "");
            i2 = i4;
            i = i5 + 1;
        }
        String[] strArr = new String[arrayList.size()];
        for (int i7 = 0; i7 < strArr.length; i7++) {
            strArr[i7] = (String) arrayList.get(i7);
        }
        return strArr;
    }
}
