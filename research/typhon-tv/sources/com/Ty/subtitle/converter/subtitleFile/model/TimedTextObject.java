package com.Ty.subtitle.converter.subtitleFile.model;

import java.util.Hashtable;
import java.util.TreeMap;

public class TimedTextObject {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f6534 = "";

    /* renamed from: ʼ  reason: contains not printable characters */
    public Hashtable<String, Style> f6535 = new Hashtable<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    public Hashtable<String, Region> f6536 = new Hashtable<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f6537 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int f6538 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    public TreeMap<Integer, Caption> f6539 = new TreeMap<>();

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f6540 = "List of non fatal errors produced during parsing:\n\n";

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean f6541 = true;

    /* renamed from: 连任  reason: contains not printable characters */
    public String f6542 = "";

    /* renamed from: 靐  reason: contains not printable characters */
    public String f6543 = "";

    /* renamed from: 麤  reason: contains not printable characters */
    public String f6544 = "";

    /* renamed from: 齉  reason: contains not printable characters */
    public String f6545 = "";

    /* renamed from: 龘  reason: contains not printable characters */
    public String f6546 = "";

    /* renamed from: 龘  reason: contains not printable characters */
    public static Caption m7493() {
        Caption caption = new Caption();
        caption.f6521 = new Time(1);
        caption.f6520 = new Time(86400000);
        caption.f6517 = "Subtitle is broken. Please choose another one.";
        return caption;
    }

    public String toString() {
        return "TimedTextObject{title='" + this.f6546 + '\'' + ", description='" + this.f6543 + '\'' + ", copyright='" + this.f6545 + '\'' + ", author='" + this.f6544 + '\'' + ", fileName='" + this.f6542 + '\'' + ", language='" + this.f6534 + '\'' + ", styling=" + this.f6535 + ", layout=" + this.f6536 + ", captions=" + this.f6539 + ", warnings='" + this.f6540 + '\'' + ", useASSInsteadOfSSA=" + this.f6541 + ", offset=" + this.f6538 + ", built=" + this.f6537 + '}';
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m7494() {
        Hashtable<String, Style> hashtable = new Hashtable<>();
        for (Caption next : this.f6539.values()) {
            if (next.f6522 != null) {
                String str = next.f6522.f6532;
                if (!hashtable.containsKey(str)) {
                    hashtable.put(str, next.f6522);
                }
            }
        }
        this.f6535 = hashtable;
    }
}
