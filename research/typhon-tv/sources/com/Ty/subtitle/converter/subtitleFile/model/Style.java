package com.Ty.subtitle.converter.subtitleFile.model;

public class Style {

    /* renamed from: ٴ  reason: contains not printable characters */
    private static int f6523;

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f6524 = "";

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f6525;

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f6526;

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean f6527;

    /* renamed from: 连任  reason: contains not printable characters */
    public String f6528;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f6529;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f6530;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f6531;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f6532;

    public Style(String str) {
        this.f6532 = str;
    }

    public Style(String str, Style style) {
        this.f6532 = str;
        this.f6529 = style.f6529;
        this.f6531 = style.f6531;
        this.f6530 = style.f6530;
        this.f6528 = style.f6528;
        this.f6524 = style.f6524;
        this.f6525 = style.f6525;
        this.f6527 = style.f6527;
        this.f6526 = style.f6526;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7490() {
        StringBuilder append = new StringBuilder().append("default");
        int i = f6523;
        f6523 = i + 1;
        return append.append(i).toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7491(String str, String str2) {
        if (str.equalsIgnoreCase("name")) {
            if (str2.equals("transparent")) {
                return "00000000";
            }
            if (str2.equals("black")) {
                return "000000ff";
            }
            if (str2.equals("silver")) {
                return "c0c0c0ff";
            }
            if (str2.equals("gray")) {
                return "808080ff";
            }
            if (str2.equals("white")) {
                return "ffffffff";
            }
            if (str2.equals("maroon")) {
                return "800000ff";
            }
            if (str2.equals("red")) {
                return "ff0000ff";
            }
            if (str2.equals("purple")) {
                return "800080ff";
            }
            if (str2.equals("fuchsia")) {
                return "ff00ffff";
            }
            if (str2.equals("magenta")) {
                return "ff00ffff ";
            }
            if (str2.equals("green")) {
                return "008000ff";
            }
            if (str2.equals("lime")) {
                return "00ff00ff";
            }
            if (str2.equals("olive")) {
                return "808000ff";
            }
            if (str2.equals("yellow")) {
                return "ffff00ff";
            }
            if (str2.equals("navy")) {
                return "000080ff";
            }
            if (str2.equals("blue")) {
                return "0000ffff";
            }
            if (str2.equals("teal")) {
                return "008080ff";
            }
            if (str2.equals("aqua")) {
                return "00ffffff";
            }
            if (str2.equals("cyan")) {
                return "00ffffff ";
            }
            return null;
        } else if (str.equalsIgnoreCase("&HBBGGRR")) {
            return str2.substring(6) + str2.substring(4, 5) + str2.substring(2, 3) + "ff";
        } else {
            if (str.equalsIgnoreCase("&HAABBGGRR")) {
                return str2.substring(8) + str2.substring(6, 7) + str2.substring(4, 5) + str2.substring(2, 3);
            }
            if (str.equalsIgnoreCase("decimalCodedBBGGRR")) {
                String hexString = Integer.toHexString(Integer.parseInt(str2));
                while (hexString.length() < 6) {
                    hexString = "0" + hexString;
                }
                return hexString.substring(4) + hexString.substring(2, 4) + hexString.substring(0, 2) + "ff";
            } else if (!str.equalsIgnoreCase("decimalCodedAABBGGRR")) {
                return null;
            } else {
                String hexString2 = Long.toHexString(Long.parseLong(str2));
                while (hexString2.length() < 8) {
                    hexString2 = "0" + hexString2;
                }
                return hexString2.substring(6) + hexString2.substring(4, 6) + hexString2.substring(2, 4) + hexString2.substring(0, 2);
            }
        }
    }

    public String toString() {
        return "Style{id='" + this.f6532 + '\'' + ", font='" + this.f6529 + '\'' + ", fontSize='" + this.f6531 + '\'' + ", color='" + this.f6530 + '\'' + ", backgroundColor='" + this.f6528 + '\'' + ", textAlign='" + this.f6524 + '\'' + ", italic=" + this.f6525 + ", bold=" + this.f6526 + ", underline=" + this.f6527 + '}';
    }
}
