package com.Ty.subtitle.converter.subtitleFile;

import com.Ty.subtitle.converter.subtitleFile.exception.FatalParsingException;
import com.Ty.subtitle.converter.subtitleFile.model.TimedTextObject;
import java.io.IOException;
import java.io.InputStream;

public interface TimedTextFileFormat {
    /* renamed from: 靐  reason: contains not printable characters */
    Object m7488(TimedTextObject timedTextObject);

    /* renamed from: 龘  reason: contains not printable characters */
    TimedTextObject m7489(String str, InputStream inputStream, String str2) throws IOException, FatalParsingException;
}
