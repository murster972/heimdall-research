package com.Ty.subtitle.converter.subtitleFile.model;

import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class Time {

    /* renamed from: 龘  reason: contains not printable characters */
    public int f6533;

    public Time(int i) {
        this.f6533 = i;
    }

    public Time(String str, String str2) {
        if (str.equalsIgnoreCase("hh:mm:ss,ms")) {
            int parseInt = Integer.parseInt(str2.substring(0, 2));
            int parseInt2 = Integer.parseInt(str2.substring(3, 5));
            int parseInt3 = Integer.parseInt(str2.substring(6, 8));
            this.f6533 = (parseInt3 * 1000) + Integer.parseInt(str2.substring(9, 12)) + (60000 * parseInt2) + (3600000 * parseInt);
        } else if (str.equalsIgnoreCase("h:mm:ss.cs")) {
            String[] split = str2.split(":");
            int parseInt4 = Integer.parseInt(split[0]);
            int parseInt5 = Integer.parseInt(split[1]);
            this.f6533 = (Integer.parseInt(split[2].substring(3, 5)) * 10) + (Integer.parseInt(split[2].substring(0, 2)) * 1000) + (60000 * parseInt5) + (3600000 * parseInt4);
        } else if (str.equalsIgnoreCase("h:m:s:f/fps")) {
            String[] split2 = str2.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
            float parseFloat = Float.parseFloat(split2[1]);
            String[] split3 = split2[0].split(":");
            int parseInt6 = Integer.parseInt(split3[0]);
            int parseInt7 = Integer.parseInt(split3[1]);
            this.f6533 = ((int) (((float) (Integer.parseInt(split3[3]) * 1000)) / parseFloat)) + (Integer.parseInt(split3[2]) * 1000) + (60000 * parseInt7) + (3600000 * parseInt6);
        }
    }

    public String toString() {
        return m7492("h:mm:ss.cs");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7492(String str) {
        StringBuilder sb = new StringBuilder();
        if (str.equalsIgnoreCase("hh:mm:ss,ms")) {
            String valueOf = String.valueOf(this.f6533 / 3600000);
            if (valueOf.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf);
            sb.append(':');
            String valueOf2 = String.valueOf((this.f6533 / 60000) % 60);
            if (valueOf2.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf2);
            sb.append(':');
            String valueOf3 = String.valueOf((this.f6533 / 1000) % 60);
            if (valueOf3.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf3);
            sb.append(',');
            String valueOf4 = String.valueOf(this.f6533 % 1000);
            if (valueOf4.length() == 1) {
                sb.append("00");
            } else if (valueOf4.length() == 2) {
                sb.append('0');
            }
            sb.append(valueOf4);
        } else if (str.equalsIgnoreCase("h:mm:ss.cs")) {
            String valueOf5 = String.valueOf(this.f6533 / 3600000);
            if (valueOf5.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf5);
            sb.append(':');
            String valueOf6 = String.valueOf((this.f6533 / 60000) % 60);
            if (valueOf6.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf6);
            sb.append(':');
            String valueOf7 = String.valueOf((this.f6533 / 1000) % 60);
            if (valueOf7.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf7);
            sb.append('.');
            String valueOf8 = String.valueOf((this.f6533 / 10) % 100);
            if (valueOf8.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf8);
        } else if (str.startsWith("hhmmssff/")) {
            float parseFloat = Float.parseFloat(str.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)[1]);
            String valueOf9 = String.valueOf(this.f6533 / 3600000);
            if (valueOf9.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf9);
            String valueOf10 = String.valueOf((this.f6533 / 60000) % 60);
            if (valueOf10.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf10);
            String valueOf11 = String.valueOf((this.f6533 / 1000) % 60);
            if (valueOf11.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf11);
            String valueOf12 = String.valueOf(((this.f6533 % 1000) * ((int) parseFloat)) / 1000);
            if (valueOf12.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf12);
        } else if (str.startsWith("h:m:s:f/")) {
            float parseFloat2 = Float.parseFloat(str.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)[1]);
            sb.append(String.valueOf(this.f6533 / 3600000));
            sb.append(':');
            sb.append(String.valueOf((this.f6533 / 60000) % 60));
            sb.append(':');
            sb.append(String.valueOf((this.f6533 / 1000) % 60));
            sb.append(':');
            sb.append(String.valueOf(((this.f6533 % 1000) * ((int) parseFloat2)) / 1000));
        } else if (str.startsWith("hh:mm:ss:ff/")) {
            float parseFloat3 = Float.parseFloat(str.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)[1]);
            String valueOf13 = String.valueOf(this.f6533 / 3600000);
            if (valueOf13.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf13);
            sb.append(':');
            String valueOf14 = String.valueOf((this.f6533 / 60000) % 60);
            if (valueOf14.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf14);
            sb.append(':');
            String valueOf15 = String.valueOf((this.f6533 / 1000) % 60);
            if (valueOf15.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf15);
            sb.append(':');
            String valueOf16 = String.valueOf(((this.f6533 % 1000) * ((int) parseFloat3)) / 1000);
            if (valueOf16.length() == 1) {
                sb.append('0');
            }
            sb.append(valueOf16);
        }
        return sb.toString();
    }
}
