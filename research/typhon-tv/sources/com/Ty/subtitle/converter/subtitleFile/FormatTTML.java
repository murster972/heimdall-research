package com.Ty.subtitle.converter.subtitleFile;

import com.Ty.subtitle.converter.subtitleFile.exception.FatalParsingException;
import com.Ty.subtitle.converter.subtitleFile.model.Caption;
import com.Ty.subtitle.converter.subtitleFile.model.Style;
import com.Ty.subtitle.converter.subtitleFile.model.Time;
import com.Ty.subtitle.converter.subtitleFile.model.TimedTextObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class FormatTTML implements TimedTextFileFormat {
    /* renamed from: 龘  reason: contains not printable characters */
    private int m7483(String str, TimedTextObject timedTextObject, Document document) {
        Node item;
        if (str.contains(":")) {
            String[] split = str.split(":");
            if (split.length == 3) {
                return (3600000 * Integer.parseInt(split[0])) + (60000 * Integer.parseInt(split[1])) + ((int) (1000.0f * Float.parseFloat(split[2])));
            } else if (split.length != 4) {
                return 0;
            } else {
                int i = 25;
                Node item2 = document.getElementsByTagName("ttp:frameRate").item(0);
                if (item2 != null) {
                    try {
                        i = Integer.parseInt(item2.getNodeValue());
                    } catch (NumberFormatException e) {
                    }
                }
                return (3600000 * Integer.parseInt(split[0])) + (60000 * Integer.parseInt(split[1])) + (Integer.parseInt(split[2]) * 1000) + ((int) ((1000.0f * Float.parseFloat(split[3])) / ((float) i)));
            }
        } else {
            String substring = str.substring(str.length() - 1);
            try {
                double parseDouble = Double.parseDouble(str.substring(0, str.length() - 1).replace(',', '.').trim());
                if (substring.equalsIgnoreCase("h")) {
                    return (int) (3600000.0d * parseDouble);
                }
                if (substring.equalsIgnoreCase("m")) {
                    return (int) (60000.0d * parseDouble);
                }
                if (substring.equalsIgnoreCase("s")) {
                    return (int) (1000.0d * parseDouble);
                }
                if (substring.equalsIgnoreCase("ms")) {
                    return (int) parseDouble;
                }
                if (substring.equalsIgnoreCase("f")) {
                    Node item3 = document.getElementsByTagName("ttp:frameRate").item(0);
                    if (item3 != null) {
                        return (int) ((1000.0d * parseDouble) / ((double) Integer.parseInt(item3.getNodeValue())));
                    }
                    return 0;
                } else if (!substring.equalsIgnoreCase("t") || (item = document.getElementsByTagName("ttp:tickRate").item(0)) == null) {
                    return 0;
                } else {
                    return (int) ((1000.0d * parseDouble) / ((double) Integer.parseInt(item.getNodeValue())));
                }
            } catch (NumberFormatException e2) {
                return 0;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m7484(String str, TimedTextObject timedTextObject) {
        String str2 = "";
        if (str.startsWith("#")) {
            if (str.length() == 7) {
                return str.substring(1) + "ff";
            }
            if (str.length() == 9) {
                return str.substring(1);
            }
            timedTextObject.f6540 += "Unrecoginzed format: " + str + "\n\n";
            return "ffffffff";
        } else if (str.startsWith("rgb")) {
            boolean z = false;
            if (str.startsWith("rgba")) {
                z = true;
            }
            try {
                String[] split = str.split("\\(")[1].split(",");
                int i = 255;
                int parseInt = Integer.parseInt(split[0]);
                int parseInt2 = Integer.parseInt(split[1]);
                int parseInt3 = Integer.parseInt(split[2].substring(0, 2));
                if (z) {
                    i = Integer.parseInt(split[3].substring(0, 2));
                }
                split[0] = Integer.toHexString(parseInt);
                split[1] = Integer.toHexString(parseInt2);
                split[2] = Integer.toHexString(parseInt3);
                if (z) {
                    split[2] = Integer.toHexString(i);
                }
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (split[i2].length() < 2) {
                        split[i2] = "0" + split[i2];
                    }
                    str2 = str2 + split[i2];
                }
                return !z ? str2 + "ff" : str2;
            } catch (Exception e) {
                timedTextObject.f6540 += "Unrecoginzed color: " + str + "\n\n";
                return "ffffffff";
            }
        } else {
            String r7 = Style.m7491("name", str);
            if (r7 != null && !r7.isEmpty()) {
                return r7;
            }
            timedTextObject.f6540 += "Unrecoginzed color: " + str + "\n\n";
            return "ffffffff";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TimedTextObject m7486(String str, InputStream inputStream, String str2) throws IOException, FatalParsingException {
        InputStreamReader inputStreamReader;
        try {
            inputStreamReader = new InputStreamReader(inputStream, str2);
        } catch (Exception e) {
            inputStreamReader = new InputStreamReader(inputStream);
        }
        InputSource inputSource = new InputSource(inputStreamReader);
        inputSource.setEncoding(str2);
        TimedTextObject timedTextObject = new TimedTextObject();
        timedTextObject.f6542 = str;
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputSource);
            parse.getDocumentElement().normalize();
            Node item = parse.getElementsByTagName("ttm:title").item(0);
            if (item != null) {
                timedTextObject.f6546 = item.getTextContent();
            }
            Node item2 = parse.getElementsByTagName("ttm:copyright").item(0);
            if (item2 != null) {
                timedTextObject.f6545 = item2.getTextContent();
            }
            Node item3 = parse.getElementsByTagName("ttm:desc").item(0);
            if (item3 != null) {
                timedTextObject.f6543 = item3.getTextContent();
            }
            NodeList elementsByTagName = parse.getElementsByTagName(TtmlNode.TAG_STYLE);
            NodeList elementsByTagName2 = parse.getElementsByTagName(TtmlNode.TAG_P);
            timedTextObject.f6540 += "Styling attributes are only recognized inside a style definition, to be referenced later in the captions.\n\n";
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                Style style = new Style(Style.m7490());
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                Node namedItem = attributes.getNamedItem("id");
                if (namedItem != null) {
                    style.f6532 = namedItem.getNodeValue();
                }
                Node namedItem2 = attributes.getNamedItem("xml:id");
                if (namedItem2 != null) {
                    style.f6532 = namedItem2.getNodeValue();
                }
                Node namedItem3 = attributes.getNamedItem(TtmlNode.TAG_STYLE);
                if (namedItem3 != null && timedTextObject.f6535.containsKey(namedItem3.getNodeValue())) {
                    style = new Style(style.f6532, timedTextObject.f6535.get(namedItem3.getNodeValue()));
                }
                Node namedItem4 = attributes.getNamedItem("tts:backgroundColor");
                if (namedItem4 != null) {
                    style.f6528 = m7484(namedItem4.getNodeValue(), timedTextObject);
                }
                Node namedItem5 = attributes.getNamedItem("tts:color");
                if (namedItem5 != null) {
                    style.f6530 = m7484(namedItem5.getNodeValue(), timedTextObject);
                }
                Node namedItem6 = attributes.getNamedItem("tts:fontFamily");
                if (namedItem6 != null) {
                    style.f6529 = namedItem6.getNodeValue();
                }
                Node namedItem7 = attributes.getNamedItem("tts:fontSize");
                if (namedItem7 != null) {
                    style.f6531 = namedItem7.getNodeValue();
                }
                Node namedItem8 = attributes.getNamedItem("tts:fontStyle");
                if (namedItem8 != null) {
                    if (namedItem8.getNodeValue().equalsIgnoreCase(TtmlNode.ITALIC) || namedItem8.getNodeValue().equalsIgnoreCase("oblique")) {
                        style.f6525 = true;
                    } else if (namedItem8.getNodeValue().equalsIgnoreCase("normal")) {
                        style.f6525 = false;
                    }
                }
                Node namedItem9 = attributes.getNamedItem("tts:fontWeight");
                if (namedItem9 != null) {
                    if (namedItem9.getNodeValue().equalsIgnoreCase(TtmlNode.BOLD)) {
                        style.f6526 = true;
                    } else if (namedItem9.getNodeValue().equalsIgnoreCase("normal")) {
                        style.f6526 = false;
                    }
                }
                Node namedItem10 = attributes.getNamedItem("tts:opacity");
                if (namedItem10 != null) {
                    try {
                        float parseFloat = Float.parseFloat(namedItem10.getNodeValue());
                        if (parseFloat > 1.0f) {
                            parseFloat = 1.0f;
                        } else if (parseFloat < 0.0f) {
                            parseFloat = 0.0f;
                        }
                        String hexString = Integer.toHexString((int) (255.0f * parseFloat));
                        if (hexString.length() < 2) {
                            hexString = "0" + hexString;
                        }
                        style.f6530 = style.f6530.substring(0, 6) + hexString;
                        style.f6528 = style.f6528.substring(0, 6) + hexString;
                    } catch (NumberFormatException e2) {
                    }
                }
                Node namedItem11 = attributes.getNamedItem("tts:textAlign");
                if (namedItem11 != null) {
                    if (namedItem11.getNodeValue().equalsIgnoreCase(TtmlNode.LEFT) || namedItem11.getNodeValue().equalsIgnoreCase(TtmlNode.START)) {
                        style.f6524 = "bottom-left";
                    } else if (namedItem11.getNodeValue().equalsIgnoreCase(TtmlNode.RIGHT) || namedItem11.getNodeValue().equalsIgnoreCase(TtmlNode.END)) {
                        style.f6524 = "bottom-right";
                    }
                }
                Node namedItem12 = attributes.getNamedItem("tts:textDecoration");
                if (namedItem12 != null) {
                    if (namedItem12.getNodeValue().equalsIgnoreCase(TtmlNode.UNDERLINE)) {
                        style.f6527 = true;
                    } else if (namedItem12.getNodeValue().equalsIgnoreCase("noUnderline")) {
                        style.f6527 = false;
                    }
                }
                timedTextObject.f6535.put(style.f6532, style);
            }
            for (int i2 = 0; i2 < elementsByTagName2.getLength(); i2++) {
                Caption caption = new Caption();
                caption.f6517 = "";
                boolean z = true;
                Node item4 = elementsByTagName2.item(i2);
                NamedNodeMap attributes2 = item4.getAttributes();
                Node namedItem13 = attributes2.getNamedItem("begin");
                caption.f6521 = new Time("", "");
                caption.f6520 = new Time("", "");
                if (namedItem13 != null) {
                    caption.f6521.f6533 = m7483(namedItem13.getNodeValue(), timedTextObject, parse);
                }
                Node namedItem14 = attributes2.getNamedItem(TtmlNode.END);
                if (namedItem14 != null) {
                    caption.f6520.f6533 = m7483(namedItem14.getNodeValue(), timedTextObject, parse);
                } else {
                    Node namedItem15 = attributes2.getNamedItem("dur");
                    if (namedItem15 != null) {
                        caption.f6520.f6533 = caption.f6521.f6533 + m7483(namedItem15.getNodeValue(), timedTextObject, parse);
                    } else {
                        z = false;
                    }
                }
                Node namedItem16 = attributes2.getNamedItem(TtmlNode.TAG_STYLE);
                if (namedItem16 != null) {
                    Style style2 = timedTextObject.f6535.get(namedItem16.getNodeValue());
                    if (style2 != null) {
                        caption.f6522 = style2;
                    } else {
                        timedTextObject.f6540 += "unrecoginzed style referenced: " + namedItem16.getNodeValue() + "\n\n";
                    }
                }
                NodeList childNodes = item4.getChildNodes();
                for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                    if (childNodes.item(i3).getNodeName().equals("#text")) {
                        caption.f6517 += childNodes.item(i3).getTextContent().trim();
                    } else if (childNodes.item(i3).getNodeName().equals(TtmlNode.TAG_BR)) {
                        caption.f6517 += "<br />";
                    }
                }
                if (caption.f6517.replaceAll("<br />", "").trim().isEmpty()) {
                    z = false;
                }
                if (z) {
                    int i4 = caption.f6521.f6533;
                    while (timedTextObject.f6539.containsKey(Integer.valueOf(i4))) {
                        i4++;
                    }
                    timedTextObject.f6539.put(Integer.valueOf(i4), caption);
                }
            }
            try {
                inputStreamReader.close();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            try {
                inputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            timedTextObject.f6537 = true;
            return timedTextObject;
        } catch (Exception e5) {
            e5.printStackTrace();
            throw new FatalParsingException("Error during parsing: " + e5.getMessage());
        } catch (Throwable th) {
            try {
                inputStreamReader.close();
            } catch (Exception e6) {
                e6.printStackTrace();
            }
            try {
                inputStream.close();
            } catch (Exception e7) {
                e7.printStackTrace();
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String[] m7485(TimedTextObject timedTextObject) {
        int i;
        int i2;
        if (!timedTextObject.f6537) {
            return null;
        }
        ArrayList arrayList = new ArrayList(timedTextObject.f6535.size() + 30 + timedTextObject.f6539.size());
        int i3 = 0 + 1;
        arrayList.add(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        int i4 = i3 + 1;
        arrayList.add(i3, "<tt xml:lang=\"" + timedTextObject.f6534 + "\" xmlns=\"http://www.w3.org/ns/ttml\" xmlns:tts=\"http://www.w3.org/ns/ttml#styling\">");
        int i5 = i4 + 1;
        arrayList.add(i4, "\t<head>");
        int i6 = i5 + 1;
        arrayList.add(i5, "\t\t<metadata xmlns:ttm=\"http://www.w3.org/ns/ttml#metadata\">");
        int i7 = i6 + 1;
        arrayList.add(i6, "\t\t\t<ttm:title>" + ((timedTextObject.f6546 == null || timedTextObject.f6546.isEmpty()) ? timedTextObject.f6542 : timedTextObject.f6546) + "</ttm:title>");
        if (timedTextObject.f6545 == null || timedTextObject.f6545.isEmpty()) {
            i = i7;
        } else {
            i = i7 + 1;
            arrayList.add(i7, "\t\t\t<ttm:copyright>" + timedTextObject.f6545 + "</ttm:copyright>");
        }
        String str = "Copyright (c) 2012 J. David Requejo\n";
        if (timedTextObject.f6544 != null && !timedTextObject.f6544.isEmpty()) {
            str = str + "\n Original file by: " + timedTextObject.f6544 + StringUtils.LF;
        }
        if (timedTextObject.f6543 != null && !timedTextObject.f6543.isEmpty()) {
            str = str + timedTextObject.f6543 + StringUtils.LF;
        }
        int i8 = i + 1;
        arrayList.add(i, "\t\t\t<ttm:desc>" + str + "\t\t\t</ttm:desc>");
        int i9 = i8 + 1;
        arrayList.add(i8, "\t\t</metadata>");
        int i10 = i9 + 1;
        arrayList.add(i9, "\t\t<styling>");
        Iterator<Style> it2 = timedTextObject.f6535.values().iterator();
        while (true) {
            i2 = i10;
            if (!it2.hasNext()) {
                break;
            }
            Style next = it2.next();
            String str2 = "\t\t\t<style xml:id=\"" + next.f6532 + "\"";
            if (next.f6530 != null) {
                str2 = str2 + " tts:color=\"#" + next.f6530 + "\"";
            }
            if (next.f6528 != null) {
                str2 = str2 + " tts:backgroundColor=\"#" + next.f6528 + "\"";
            }
            if (next.f6529 != null) {
                str2 = str2 + " tts:fontFamily=\"" + next.f6529 + "\"";
            }
            if (next.f6531 != null) {
                str2 = str2 + " tts:fontSize=\"" + next.f6531 + "\"";
            }
            if (next.f6525) {
                str2 = str2 + " tts:fontStyle=\"italic\"";
            }
            if (next.f6526) {
                str2 = str2 + " tts:fontWeight=\"bold\"";
            }
            String str3 = str2 + " tts:textAlign=\"";
            String str4 = next.f6524.contains(TtmlNode.LEFT) ? str3 + "left\"" : next.f6524.contains(TtmlNode.RIGHT) ? str3 + "rigth\"" : str3 + "center\"";
            if (next.f6527) {
                str4 = str4 + " tts:textDecoration=\"underline\"";
            }
            i10 = i2 + 1;
            arrayList.add(i2, str4 + " />");
        }
        int i11 = i2 + 1;
        arrayList.add(i2, "\t\t</styling>");
        int i12 = i11 + 1;
        arrayList.add(i11, "\t</head>");
        int i13 = i12 + 1;
        arrayList.add(i12, "\t<body>");
        int i14 = i13 + 1;
        arrayList.add(i13, "\t\t<div>");
        if (timedTextObject.f6539.isEmpty()) {
            timedTextObject.f6539.put(1, TimedTextObject.m7493());
        }
        for (Caption next2 : timedTextObject.f6539.values()) {
            String str5 = ("\t\t\t<p begin=\"" + next2.f6521.m7492("hh:mm:ss,ms").replace(',', '.') + "\"") + " end=\"" + next2.f6520.m7492("hh:mm:ss,ms").replace(',', '.') + "\"";
            if (next2.f6522 != null) {
                str5 = str5 + " style=\"" + next2.f6522.f6532 + "\"";
            }
            arrayList.add(i14, str5 + " >" + next2.f6517.replaceAll("(?s)<i>(.*?)</i>", "$1").replaceAll("(?s)<b>(.*?)</b>", "$1").replaceAll("(?s)<font[^>]*?>(.*?)</font>", "$1").replaceAll("(?s)&(?!amp;|quot;|apos;|lt;|gt;)", "&amp;").replace("\"", "&quot;").replace("'", "&apos;").replace("<", "&lt;").replace(">", "&gt;") + "</p>\n");
            i14++;
        }
        int i15 = i14 + 1;
        arrayList.add(i14, "\t\t</div>");
        int i16 = i15 + 1;
        arrayList.add(i15, "\t</body>");
        int i17 = i16 + 1;
        arrayList.add(i16, "</tt>");
        int i18 = i17 + 1;
        arrayList.add(i17, "");
        String[] strArr = new String[arrayList.size()];
        for (int i19 = 0; i19 < strArr.length; i19++) {
            strArr[i19] = (String) arrayList.get(i19);
        }
        return strArr;
    }
}
