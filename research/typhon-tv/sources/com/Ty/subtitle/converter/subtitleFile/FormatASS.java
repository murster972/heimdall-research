package com.Ty.subtitle.converter.subtitleFile;

import com.Ty.subtitle.converter.subtitleFile.model.Caption;
import com.Ty.subtitle.converter.subtitleFile.model.Style;
import com.Ty.subtitle.converter.subtitleFile.model.Time;
import com.Ty.subtitle.converter.subtitleFile.model.TimedTextObject;
import com.typhoon.tv.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class FormatASS implements TimedTextFileFormat {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f6515 = Pattern.compile("(\\d+):(\\d+):(\\d+)\\.(\\d+)");

    /* renamed from: 靐  reason: contains not printable characters */
    private String m7459(boolean z, Style style) {
        String str = style.f6526 ? "-1," : "0,";
        String str2 = style.f6525 ? str + "-1," : str + "0,";
        if (!z) {
            return str2;
        }
        return (style.f6527 ? str2 + "-1," : str2 + "0,") + "0,100,100,0,0,";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m7460(String str) {
        Matcher matcher = f6515.matcher(str);
        if (!matcher.matches()) {
            return -1;
        }
        return 0 + (Integer.parseInt(matcher.group(1)) * 3600000) + (Integer.parseInt(matcher.group(2)) * 60000) + (Integer.parseInt(matcher.group(3)) * 1000) + (Integer.parseInt(matcher.group(4)) * 10);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m7461(boolean z, String str) {
        if (z) {
            int i = 2;
            if ("bottom-left".equals(str)) {
                i = 1;
            } else if ("bottom-center".equals(str)) {
                i = 2;
            } else if ("bottom-right".equals(str)) {
                i = 3;
            } else if ("mid-left".equals(str)) {
                i = 4;
            } else if ("mid-center".equals(str)) {
                i = 5;
            } else if ("mid-right".equals(str)) {
                i = 6;
            } else if ("top-left".equals(str)) {
                i = 7;
            } else if ("top-center".equals(str)) {
                i = 8;
            } else if ("top-right".equals(str)) {
                i = 9;
            }
            int i2 = i;
            return i;
        }
        int i3 = 10;
        if ("bottom-left".equals(str)) {
            i3 = 9;
        } else if ("bottom-center".equals(str)) {
            i3 = 10;
        } else if ("bottom-right".equals(str)) {
            i3 = 11;
        } else if ("mid-left".equals(str)) {
            i3 = 1;
        } else if ("mid-center".equals(str)) {
            i3 = 2;
        } else if ("mid-right".equals(str)) {
            i3 = 3;
        } else if ("top-left".equals(str)) {
            i3 = 5;
        } else if ("top-center".equals(str)) {
            i3 = 6;
        } else if ("top-right".equals(str)) {
            i3 = 7;
        }
        int i4 = i3;
        return i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Caption m7462(String[] strArr, String[] strArr2, float f, TimedTextObject timedTextObject) {
        Caption caption = new Caption();
        for (int i = 0; i < strArr2.length; i++) {
            String trim = strArr2[i].trim();
            if (trim.equalsIgnoreCase("Style")) {
                Style style = timedTextObject.f6535.get(strArr[i].trim());
                if (style != null) {
                    caption.f6522 = style;
                } else {
                    timedTextObject.f6540 += "undefined style: " + strArr[i].trim() + "\n\n";
                }
            } else if (trim.equalsIgnoreCase("Start")) {
                caption.f6521 = new Time("h:mm:ss.cs", strArr[i].trim());
            } else if (trim.equalsIgnoreCase("End")) {
                caption.f6520 = new Time("h:mm:ss.cs", strArr[i].trim());
            } else if (trim.equalsIgnoreCase("Text")) {
                String str = strArr[i];
                caption.f6518 = str;
                caption.f6517 = str.replaceAll("\\{.*?\\}", "").replace(StringUtils.LF, "<br />").replace("\\N", "<br />");
            }
        }
        if (f != 100.0f) {
            Time time = caption.f6521;
            time.f6533 = (int) (((float) time.f6533) / (f / 100.0f));
            Time time2 = caption.f6520;
            time2.f6533 = (int) (((float) time2.f6533) / (f / 100.0f));
        }
        return caption;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Style m7463(String[] strArr, String[] strArr2, int i, boolean z, String str) {
        Style style = new Style(Style.m7490());
        if (strArr.length == strArr2.length) {
            for (int i2 = 0; i2 < strArr2.length; i2++) {
                String trim = strArr2[i2].trim();
                if (trim.equalsIgnoreCase("Name")) {
                    style.f6532 = strArr[i2].trim();
                } else if (trim.equalsIgnoreCase("Fontname")) {
                    style.f6529 = strArr[i2].trim();
                } else if (trim.equalsIgnoreCase("Fontsize")) {
                    style.f6531 = strArr[i2].trim();
                } else if (trim.equalsIgnoreCase("PrimaryColour")) {
                    String trim2 = strArr[i2].trim();
                    if (z) {
                        if (trim2.startsWith("&H")) {
                            style.f6530 = Style.m7491("&HAABBGGRR", trim2);
                        } else {
                            style.f6530 = Style.m7491("decimalCodedAABBGGRR", trim2);
                        }
                    } else if (trim2.startsWith("&H")) {
                        style.f6530 = Style.m7491("&HBBGGRR", trim2);
                    } else {
                        style.f6530 = Style.m7491("decimalCodedBBGGRR", trim2);
                    }
                } else if (trim.equalsIgnoreCase("BackColour")) {
                    String trim3 = strArr[i2].trim();
                    if (z) {
                        if (trim3.startsWith("&H")) {
                            style.f6528 = Style.m7491("&HAABBGGRR", trim3);
                        } else {
                            style.f6528 = Style.m7491("decimalCodedAABBGGRR", trim3);
                        }
                    } else if (trim3.startsWith("&H")) {
                        style.f6528 = Style.m7491("&HBBGGRR", trim3);
                    } else {
                        style.f6528 = Style.m7491("decimalCodedBBGGRR", trim3);
                    }
                } else if (trim.equalsIgnoreCase("Bold")) {
                    style.f6526 = Boolean.parseBoolean(strArr[i2].trim());
                } else if (trim.equalsIgnoreCase("Italic")) {
                    style.f6525 = Boolean.parseBoolean(strArr[i2].trim());
                } else if (trim.equalsIgnoreCase("Underline")) {
                    style.f6527 = Boolean.parseBoolean(strArr[i2].trim());
                } else if (trim.equalsIgnoreCase("Alignment")) {
                    int parseInt = Integer.parseInt(strArr[i2].trim());
                    if (!z) {
                        switch (parseInt) {
                            case 1:
                                style.f6524 = "mid-left";
                                break;
                            case 2:
                                style.f6524 = "mid-center";
                                break;
                            case 3:
                                style.f6524 = "mid-right";
                                break;
                            case 5:
                                style.f6524 = "top-left";
                                break;
                            case 6:
                                style.f6524 = "top-center";
                                break;
                            case 7:
                                style.f6524 = "top-right";
                                break;
                            case 9:
                                style.f6524 = "bottom-left";
                                break;
                            case 10:
                                style.f6524 = "bottom-center";
                                break;
                            case 11:
                                style.f6524 = "bottom-right";
                                break;
                            default:
                                str = str + "undefined alignment for style at line " + i + "\n\n";
                                break;
                        }
                    } else {
                        switch (parseInt) {
                            case 1:
                                style.f6524 = "bottom-left";
                                break;
                            case 2:
                                style.f6524 = "bottom-center";
                                break;
                            case 3:
                                style.f6524 = "bottom-right";
                                break;
                            case 4:
                                style.f6524 = "mid-left";
                                break;
                            case 5:
                                style.f6524 = "mid-center";
                                break;
                            case 6:
                                style.f6524 = "mid-right";
                                break;
                            case 7:
                                style.f6524 = "top-left";
                                break;
                            case 8:
                                style.f6524 = "top-center";
                                break;
                            case 9:
                                style.f6524 = "top-right";
                                break;
                            default:
                                str = str + "undefined alignment for style at line " + i + "\n\n";
                                break;
                        }
                    }
                }
            }
        }
        return style;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m7464(boolean z, Style style) {
        if (z) {
            return Integer.parseInt("00" + style.f6530.substring(4, 6) + style.f6530.substring(2, 4) + style.f6530.substring(0, 2), 16) + ",16777215,0," + Long.parseLong("80" + style.f6528.substring(4, 6) + style.f6528.substring(2, 4) + style.f6528.substring(0, 2), 16) + ",";
        }
        return Long.parseLong(style.f6530.substring(4, 6) + style.f6530.substring(2, 4) + style.f6530.substring(0, 2), 16) + ",16777215,0," + Long.parseLong(style.f6528.substring(4, 6) + style.f6528.substring(2, 4) + style.f6528.substring(0, 2), 16) + ",";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private TreeMap<Integer, Caption> m7465(InputStreamReader inputStreamReader) throws IOException {
        TreeMap<Integer, Caption> treeMap = new TreeMap<>();
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        int i = 0;
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                if (i == 0) {
                    try {
                        if (!readLine.equals("[Script Info]")) {
                            break;
                        }
                        while (!readLine.startsWith("Dialogue")) {
                            readLine = bufferedReader.readLine();
                            i++;
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                if (!readLine.startsWith("Dialogue")) {
                    i++;
                } else {
                    String[] split = readLine.split(",", 10);
                    if (split.length < 10) {
                        i++;
                    } else {
                        int r10 = m7460(split[1]);
                        int r5 = m7460(split[2]);
                        if (!(r10 == -1 || r5 == -1)) {
                            String replaceAll = split[9].replace("\\N", StringUtils.LF).replace("\\n", "").replaceAll("\\{\\\\(.|\n)*?\\}", "");
                            Caption caption = new Caption();
                            caption.f6521 = new Time(r10);
                            caption.f6520 = new Time(r5);
                            caption.f6517 = replaceAll;
                            treeMap.put(Integer.valueOf(caption.f6521.f6533), caption);
                            i++;
                        }
                    }
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            } finally {
                bufferedReader.close();
            }
        }
        return treeMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TimedTextObject m7467(String str, InputStream inputStream, String str2) throws IOException {
        InputStreamReader inputStreamReader;
        TimedTextObject timedTextObject = new TimedTextObject();
        timedTextObject.f6542 = str;
        try {
            inputStreamReader = new InputStreamReader(inputStream, str2);
        } catch (UnsupportedEncodingException e) {
            inputStreamReader = new InputStreamReader(inputStream);
        }
        TreeMap<Integer, Caption> r17 = m7465(inputStreamReader);
        if (!r17.isEmpty()) {
            timedTextObject.f6539 = r17;
        } else {
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            float f = 100.0f;
            boolean z = false;
            try {
                String readLine = bufferedReader.readLine();
                int i = 0 + 1;
                while (readLine != null) {
                    String trim = readLine.trim();
                    if (!trim.startsWith("[")) {
                        readLine = bufferedReader.readLine();
                        i++;
                    } else if (trim.equalsIgnoreCase("[Script info]")) {
                        i++;
                        readLine = bufferedReader.readLine().trim();
                        while (!readLine.startsWith("[")) {
                            if (readLine.startsWith("Title:")) {
                                timedTextObject.f6546 = readLine.split(":")[1].trim();
                            } else if (readLine.startsWith("Original Script:")) {
                                timedTextObject.f6544 = readLine.split(":")[1].trim();
                            } else if (readLine.startsWith("Script Type:")) {
                                if (readLine.split(":")[1].trim().equalsIgnoreCase("v4.00+")) {
                                    z = true;
                                } else if (!readLine.split(":")[1].trim().equalsIgnoreCase("v4.00")) {
                                    timedTextObject.f6540 += "Script version is older than 4.00, it may produce parsing errors.";
                                }
                            } else if (readLine.startsWith("Timer:")) {
                                f = Float.parseFloat(readLine.split(":")[1].trim().replace(',', '.'));
                            }
                            i++;
                            readLine = bufferedReader.readLine().trim();
                        }
                    } else if (trim.equalsIgnoreCase("[v4 Styles]") || trim.equalsIgnoreCase("[v4 Styles+]") || trim.equalsIgnoreCase("[v4+ Styles]")) {
                        if (trim.contains("+") && !z) {
                            z = true;
                            timedTextObject.f6540 += "ScriptType should be set to v4:00+ in the [Script Info] section.\n\n";
                        }
                        int i2 = i + 1;
                        String trim2 = bufferedReader.readLine().trim();
                        if (!trim2.startsWith("Format:")) {
                            timedTextObject.f6540 += "Format: (format definition) expected at line " + trim2 + " for the styles section\n\n";
                            while (!trim2.startsWith("Format:")) {
                                i2++;
                                trim2 = bufferedReader.readLine().trim();
                            }
                        }
                        String[] split = trim2.split(":")[1].trim().split(",");
                        int i3 = i2 + 1;
                        String trim3 = bufferedReader.readLine().trim();
                        while (!readLine.startsWith("[")) {
                            if (readLine.startsWith("Style:")) {
                                Style r18 = m7463(readLine.split(":")[1].trim().split(","), split, i, z, timedTextObject.f6540);
                                timedTextObject.f6535.put(r18.f6532, r18);
                            }
                            i3 = i + 1;
                            trim3 = bufferedReader.readLine().trim();
                        }
                    } else if (trim.trim().equalsIgnoreCase("[Events]")) {
                        int i4 = i + 1;
                        String trim4 = bufferedReader.readLine().trim();
                        timedTextObject.f6540 += "Only dialogue events are considered, all other events are ignored.\n\n";
                        if (!trim4.startsWith("Format:")) {
                            timedTextObject.f6540 += "Format: (format definition) expected at line " + trim4 + " for the events section\n\n";
                            while (!trim4.startsWith("Format:")) {
                                i4++;
                                trim4 = bufferedReader.readLine().trim();
                            }
                        }
                        String[] split2 = trim4.split(":")[1].trim().split(",");
                        int i5 = i4 + 1;
                        String trim5 = bufferedReader.readLine().trim();
                        while (!readLine.startsWith("[")) {
                            if (readLine.startsWith("Dialogue:")) {
                                Caption r10 = m7462(readLine.split(":", 2)[1].trim().split(",", split2.length), split2, f, timedTextObject);
                                int i6 = r10.f6521.f6533;
                                while (timedTextObject.f6539.containsKey(Integer.valueOf(i6))) {
                                    i6++;
                                }
                                timedTextObject.f6539.put(Integer.valueOf(i6), r10);
                            }
                            i5 = i + 1;
                            trim5 = bufferedReader.readLine().trim();
                        }
                    } else if (trim.trim().equalsIgnoreCase("[Fonts]") || trim.trim().equalsIgnoreCase("[Graphics]")) {
                        timedTextObject.f6540 += "The section " + trim.trim() + " is not supported for conversion, all information there will be lost.\n\n";
                        readLine = bufferedReader.readLine().trim();
                    } else {
                        timedTextObject.f6540 += "Unrecognized section: " + trim.trim() + " all information there is ignored.";
                        readLine = bufferedReader.readLine().trim();
                    }
                }
                timedTextObject.m7494();
            } catch (NullPointerException e2) {
                timedTextObject.f6540 += "unexpected end of file, maybe last caption is not complete.\n\n";
            } finally {
                bufferedReader.close();
            }
        }
        timedTextObject.f6537 = true;
        return timedTextObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String[] m7466(TimedTextObject timedTextObject) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        if (!timedTextObject.f6537) {
            return null;
        }
        ArrayList arrayList = new ArrayList(timedTextObject.f6535.size() + 30 + timedTextObject.f6539.size());
        int i6 = 0 + 1;
        arrayList.add(0, "[Script Info]");
        int i7 = i6 + 1;
        arrayList.add(i6, (timedTextObject.f6546 == null || timedTextObject.f6546.isEmpty()) ? "Title: " + timedTextObject.f6542 : "Title: " + timedTextObject.f6546);
        int i8 = i7 + 1;
        arrayList.add(i7, (timedTextObject.f6544 == null || timedTextObject.f6544.isEmpty()) ? "Original Script: " + "Unknown" : "Original Script: " + timedTextObject.f6544);
        if (timedTextObject.f6545 == null || timedTextObject.f6545.isEmpty()) {
            i = i8;
        } else {
            i = i8 + 1;
            arrayList.add(i8, "; " + timedTextObject.f6545);
        }
        if (timedTextObject.f6543 != null && !timedTextObject.f6543.isEmpty()) {
            arrayList.add(i, "; " + timedTextObject.f6543);
            i++;
        }
        int i9 = i + 1;
        arrayList.add(i, "; Copyright (c) 2012 J. David Requejo");
        if (timedTextObject.f6541) {
            i2 = i9 + 1;
            arrayList.add(i9, "Script Type: V4.00+");
        } else {
            i2 = i9 + 1;
            arrayList.add(i9, "Script Type: V4.00");
        }
        int i10 = i2 + 1;
        arrayList.add(i2, "Collisions: Normal");
        int i11 = i10 + 1;
        arrayList.add(i10, "Timer: 100,0000");
        if (timedTextObject.f6541) {
            arrayList.add(i11, "WrapStyle: 1");
            i11++;
        }
        int i12 = i11 + 1;
        arrayList.add(i11, "");
        if (timedTextObject.f6541) {
            i3 = i12 + 1;
            arrayList.add(i12, "[V4+ Styles]");
        } else {
            i3 = i12 + 1;
            arrayList.add(i12, "[V4 Styles]");
        }
        if (timedTextObject.f6541) {
            arrayList.add(i3, "Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding");
            i4 = i3 + 1;
        } else {
            arrayList.add(i3, "Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, TertiaryColour, BackColour, Bold, Italic, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, AlphaLevel, Encoding");
            i4 = i3 + 1;
        }
        for (Style next : timedTextObject.f6535.values()) {
            String str = ((((((("Style: " + next.f6532 + ",") + next.f6529 + ",") + next.f6531 + ",") + m7464(timedTextObject.f6541, next)) + m7459(timedTextObject.f6541, next)) + "1,2,2,") + m7461(timedTextObject.f6541, next.f6524)) + ",0,0,0,";
            if (!timedTextObject.f6541) {
                str = str + "0,";
            }
            arrayList.add(i4, str + "0");
            i4++;
        }
        int i13 = i4 + 1;
        arrayList.add(i4, "");
        int i14 = i13 + 1;
        arrayList.add(i13, "[Events]");
        if (timedTextObject.f6541) {
            arrayList.add(i14, "Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
            i5 = i14 + 1;
        } else {
            arrayList.add(i14, "Format: Marked, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
            i5 = i14 + 1;
        }
        for (Caption next2 : timedTextObject.f6539.values()) {
            if (timedTextObject.f6538 != 0) {
                next2.f6521.f6533 += timedTextObject.f6538;
                next2.f6520.f6533 += timedTextObject.f6538;
            }
            String str2 = ("Dialogue: 0," + next2.f6521.m7492("h:mm:ss.cs") + ",") + next2.f6520.m7492("h:mm:ss.cs") + ",";
            if (timedTextObject.f6538 != 0) {
                next2.f6521.f6533 -= timedTextObject.f6538;
                next2.f6520.f6533 -= timedTextObject.f6538;
            }
            arrayList.add(i5, ((next2.f6522 != null ? str2 + next2.f6522.f6532 : str2 + "Default") + ",,0000,0000,0000,,") + next2.f6517.replaceAll("<br />", "\\\\N").replaceAll("\\<.*?\\>", ""));
            i5++;
        }
        int i15 = i5 + 1;
        arrayList.add(i5, "");
        String[] strArr = new String[arrayList.size()];
        for (int i16 = 0; i16 < strArr.length; i16++) {
            strArr[i16] = (String) arrayList.get(i16);
        }
        return strArr;
    }
}
