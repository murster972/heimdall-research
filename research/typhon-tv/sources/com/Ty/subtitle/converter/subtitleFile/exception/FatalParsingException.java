package com.Ty.subtitle.converter.subtitleFile.exception;

public class FatalParsingException extends Exception {
    private static final long serialVersionUID = 6798827566637277804L;
    private final String parsingError;

    public FatalParsingException(String str) {
        super(str);
        this.parsingError = str;
    }

    public String getLocalizedMessage() {
        return this.parsingError;
    }
}
