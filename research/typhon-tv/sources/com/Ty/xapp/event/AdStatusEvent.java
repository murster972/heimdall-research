package com.Ty.xapp.event;

public class AdStatusEvent {
    private boolean showHostAppAd;

    public AdStatusEvent(boolean z) {
        this.showHostAppAd = z;
    }

    public boolean isShowHostAppAd() {
        return this.showHostAppAd;
    }

    public void setShowHostAppAd(boolean z) {
        this.showHostAppAd = z;
    }
}
