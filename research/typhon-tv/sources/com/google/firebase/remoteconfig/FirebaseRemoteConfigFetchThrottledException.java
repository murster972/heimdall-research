package com.google.firebase.remoteconfig;

public class FirebaseRemoteConfigFetchThrottledException extends FirebaseRemoteConfigFetchException {
    private final long zzgfv;

    public FirebaseRemoteConfigFetchThrottledException(long j) {
        this.zzgfv = j;
    }

    public long getThrottleEndTimeMillis() {
        return this.zzgfv;
    }
}
