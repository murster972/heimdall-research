package com.google.firebase.remoteconfig;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.internal.zzbhm;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zza implements ResultCallback<zzbhm> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ FirebaseRemoteConfig f11252;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ TaskCompletionSource f11253;

    zza(FirebaseRemoteConfig firebaseRemoteConfig, TaskCompletionSource taskCompletionSource) {
        this.f11252 = firebaseRemoteConfig;
        this.f11253 = taskCompletionSource;
    }

    public final /* synthetic */ void onResult(Result result) {
        this.f11252.m6125((TaskCompletionSource<Void>) this.f11253, (zzbhm) result);
    }
}
