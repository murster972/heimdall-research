package com.google.firebase.remoteconfig;

public class FirebaseRemoteConfigSettings {

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f5651;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean f5652 = false;

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6132(boolean z) {
            this.f5652 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public FirebaseRemoteConfigSettings m6133() {
            return new FirebaseRemoteConfigSettings(this);
        }
    }

    private FirebaseRemoteConfigSettings(Builder builder) {
        this.f5651 = builder.f5652;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6130() {
        return this.f5651;
    }
}
