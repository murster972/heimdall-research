package com.google.firebase.remoteconfig;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.internal.zzbhg;
import com.google.android.gms.internal.zzbhl;
import com.google.android.gms.internal.zzbhm;
import com.google.android.gms.internal.zzbhx;
import com.google.android.gms.internal.zzdvs;
import com.google.android.gms.internal.zzexb;
import com.google.android.gms.internal.zzexc;
import com.google.android.gms.internal.zzexd;
import com.google.android.gms.internal.zzexe;
import com.google.android.gms.internal.zzexf;
import com.google.android.gms.internal.zzexg;
import com.google.android.gms.internal.zzexh;
import com.google.android.gms.internal.zzexi;
import com.google.android.gms.internal.zzexj;
import com.google.android.gms.internal.zzexk;
import com.google.android.gms.internal.zzexl;
import com.google.android.gms.internal.zzexm;
import com.google.android.gms.internal.zzexn;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class FirebaseRemoteConfig {

    /* renamed from: 靐  reason: contains not printable characters */
    private static FirebaseRemoteConfig f5643;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final byte[] f5644 = new byte[0];

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzexh f5645;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Context f5646;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ReadWriteLock f5647;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzexe f5648;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzexe f5649;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzexe f5650;

    private FirebaseRemoteConfig(Context context) {
        this(context, (zzexe) null, (zzexe) null, (zzexe) null, (zzexh) null);
    }

    private FirebaseRemoteConfig(Context context, zzexe zzexe, zzexe zzexe2, zzexe zzexe3, zzexh zzexh) {
        this.f5647 = new ReentrantReadWriteLock(true);
        this.f5646 = context;
        if (zzexh != null) {
            this.f5645 = zzexh;
        } else {
            this.f5645 = new zzexh();
        }
        this.f5645.m12331(m6113(this.f5646));
        if (zzexe != null) {
            this.f5650 = zzexe;
        }
        if (zzexe2 != null) {
            this.f5649 = zzexe2;
        }
        if (zzexe3 != null) {
            this.f5648 = zzexe3;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x006a A[SYNTHETIC, Splitter:B:31:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007d A[SYNTHETIC, Splitter:B:38:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.gms.internal.zzexm m6111(android.content.Context r5) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0004
        L_0x0003:
            return r0
        L_0x0004:
            java.lang.String r1 = "persisted_config"
            java.io.FileInputStream r2 = r5.openFileInput(r1)     // Catch:{ FileNotFoundException -> 0x0037, IOException -> 0x005d, all -> 0x0079 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            m6114((java.io.InputStream) r2, (java.io.OutputStream) r1)     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            byte[] r1 = r1.toByteArray()     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            r3 = 0
            int r4 = r1.length     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            com.google.android.gms.internal.zzfjj r3 = com.google.android.gms.internal.zzfjj.m12783(r1, r3, r4)     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            com.google.android.gms.internal.zzexm r1 = new com.google.android.gms.internal.zzexm     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            r1.m12876((com.google.android.gms.internal.zzfjj) r3)     // Catch:{ FileNotFoundException -> 0x0091, IOException -> 0x008f }
            if (r2 == 0) goto L_0x002a
            r2.close()     // Catch:{ IOException -> 0x002c }
        L_0x002a:
            r0 = r1
            goto L_0x0003
        L_0x002c:
            r0 = move-exception
            java.lang.String r2 = "FirebaseRemoteConfig"
            java.lang.String r3 = "Failed to close persisted config file."
            android.util.Log.e(r2, r3, r0)
            goto L_0x002a
        L_0x0037:
            r1 = move-exception
            r2 = r0
        L_0x0039:
            java.lang.String r3 = "FirebaseRemoteConfig"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ all -> 0x008c }
            if (r3 == 0) goto L_0x004c
            java.lang.String r3 = "FirebaseRemoteConfig"
            java.lang.String r4 = "Persisted config file was not found."
            android.util.Log.d(r3, r4, r1)     // Catch:{ all -> 0x008c }
        L_0x004c:
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x0003
        L_0x0052:
            r1 = move-exception
            java.lang.String r2 = "FirebaseRemoteConfig"
            java.lang.String r3 = "Failed to close persisted config file."
            android.util.Log.e(r2, r3, r1)
            goto L_0x0003
        L_0x005d:
            r1 = move-exception
            r2 = r0
        L_0x005f:
            java.lang.String r3 = "FirebaseRemoteConfig"
            java.lang.String r4 = "Cannot initialize from persisted config."
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x008c }
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x006e }
            goto L_0x0003
        L_0x006e:
            r1 = move-exception
            java.lang.String r2 = "FirebaseRemoteConfig"
            java.lang.String r3 = "Failed to close persisted config file."
            android.util.Log.e(r2, r3, r1)
            goto L_0x0003
        L_0x0079:
            r1 = move-exception
            r2 = r0
        L_0x007b:
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ IOException -> 0x0081 }
        L_0x0080:
            throw r1
        L_0x0081:
            r0 = move-exception
            java.lang.String r2 = "FirebaseRemoteConfig"
            java.lang.String r3 = "Failed to close persisted config file."
            android.util.Log.e(r2, r3, r0)
            goto L_0x0080
        L_0x008c:
            r0 = move-exception
            r1 = r0
            goto L_0x007b
        L_0x008f:
            r1 = move-exception
            goto L_0x005f
        L_0x0091:
            r1 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.remoteconfig.FirebaseRemoteConfig.m6111(android.content.Context):com.google.android.gms.internal.zzexm");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m6112() {
        this.f5647.readLock().lock();
        try {
            m6118((Runnable) new zzexd(this.f5646, this.f5650, this.f5649, this.f5648, this.f5645));
        } finally {
            this.f5647.readLock().unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final long m6113(Context context) {
        try {
            return this.f5646.getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
        } catch (PackageManager.NameNotFoundException e) {
            String packageName = context.getPackageName();
            Log.e("FirebaseRemoteConfig", new StringBuilder(String.valueOf(packageName).length() + 25).append("Package [").append(packageName).append("] was not found!").toString());
            return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m6114(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzexe m6115(zzexi zzexi) {
        if (zzexi == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (zzexl zzexl : zzexi.f10317) {
            String str = zzexl.f10326;
            HashMap hashMap2 = new HashMap();
            for (zzexj zzexj : zzexl.f10325) {
                hashMap2.put(zzexj.f10320, zzexj.f10319);
            }
            hashMap.put(str, hashMap2);
        }
        byte[][] bArr = zzexi.f10316;
        ArrayList arrayList = new ArrayList();
        for (byte[] add : bArr) {
            arrayList.add(add);
        }
        return new zzexe(hashMap, zzexi.f10315, arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FirebaseRemoteConfig m6116() {
        zzexh zzexh;
        if (f5643 != null) {
            return f5643;
        }
        FirebaseApp r0 = FirebaseApp.m13765();
        if (r0 == null) {
            throw new IllegalStateException("FirebaseApp has not been initialized.");
        }
        Context r1 = r0.m13775();
        if (f5643 == null) {
            zzexm r02 = m6111(r1);
            if (r02 == null) {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "No persisted config was found. Initializing from scratch.");
                }
                f5643 = new FirebaseRemoteConfig(r1);
            } else {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "Initializing from persisted config.");
                }
                zzexe r2 = m6115(r02.f10331);
                zzexe r3 = m6115(r02.f10328);
                zzexe r4 = m6115(r02.f10330);
                zzexk zzexk = r02.f10329;
                if (zzexk == null) {
                    zzexh = null;
                } else {
                    zzexh = new zzexh();
                    zzexh.m12330(zzexk.f10323);
                    zzexh.m12334(zzexk.f10321);
                    zzexh.m12325(zzexk.f10322);
                }
                if (zzexh != null) {
                    zzexh.m12333(m6117(r02.f10327));
                }
                f5643 = new FirebaseRemoteConfig(r1, r2, r3, r4, zzexh);
            }
        }
        return f5643;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<String, zzexb> m6117(zzexn[] zzexnArr) {
        HashMap hashMap = new HashMap();
        if (zzexnArr != null) {
            for (zzexn zzexn : zzexnArr) {
                hashMap.put(zzexn.f10334, new zzexb(zzexn.f10335, zzexn.f10333));
            }
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m6118(Runnable runnable) {
        AsyncTask.SERIAL_EXECUTOR.execute(runnable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m6119(Map<String, Object> map, String str, boolean z) {
        if (str != null) {
            boolean z2 = map == null || map.isEmpty();
            HashMap hashMap = new HashMap();
            if (!z2) {
                for (String next : map.keySet()) {
                    Object obj = map.get(next);
                    if (obj instanceof String) {
                        hashMap.put(next, ((String) obj).getBytes(zzexg.f10309));
                    } else if (obj instanceof Long) {
                        hashMap.put(next, ((Long) obj).toString().getBytes(zzexg.f10309));
                    } else if (obj instanceof Integer) {
                        hashMap.put(next, ((Integer) obj).toString().getBytes(zzexg.f10309));
                    } else if (obj instanceof Double) {
                        hashMap.put(next, ((Double) obj).toString().getBytes(zzexg.f10309));
                    } else if (obj instanceof Float) {
                        hashMap.put(next, ((Float) obj).toString().getBytes(zzexg.f10309));
                    } else if (obj instanceof byte[]) {
                        hashMap.put(next, (byte[]) obj);
                    } else if (obj instanceof Boolean) {
                        hashMap.put(next, ((Boolean) obj).toString().getBytes(zzexg.f10309));
                    } else {
                        throw new IllegalArgumentException("The type of a default value needs to beone of String, Long, Double, Boolean, or byte[].");
                    }
                }
            }
            this.f5647.writeLock().lock();
            if (z2) {
                try {
                    if (this.f5648 != null && this.f5648.m12319(str)) {
                        this.f5648.m12318((Map<String, byte[]>) null, str);
                        this.f5648.m12317(System.currentTimeMillis());
                    } else {
                        return;
                    }
                } finally {
                    this.f5647.writeLock().unlock();
                }
            } else {
                if (this.f5648 == null) {
                    this.f5648 = new zzexe(new HashMap(), System.currentTimeMillis(), (List<byte[]>) null);
                }
                this.f5648.m12318((Map<String, byte[]>) hashMap, str);
                this.f5648.m12317(System.currentTimeMillis());
            }
            if (z) {
                this.f5645.m12332(str);
            }
            m6112();
            this.f5647.writeLock().unlock();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6120() {
        this.f5647.writeLock().lock();
        try {
            if (this.f5650 == null) {
                return false;
            }
            if (this.f5649 == null || this.f5649.m12314() < this.f5650.m12314()) {
                long r0 = this.f5650.m12314();
                this.f5649 = this.f5650;
                this.f5649.m12317(System.currentTimeMillis());
                this.f5650 = new zzexe((Map<String, Map<String, byte[]>>) null, r0, (List<byte[]>) null);
                long r02 = this.f5645.m12327();
                this.f5645.m12325(zzdvs.m12255(r02, this.f5649.m12312()));
                m6118((Runnable) new zzexc(this.f5646, this.f5649.m12312(), r02));
                m6112();
                this.f5647.writeLock().unlock();
                return true;
            }
            this.f5647.writeLock().unlock();
            return false;
        } finally {
            this.f5647.writeLock().unlock();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public FirebaseRemoteConfigInfo m6121() {
        zzexf zzexf = new zzexf();
        this.f5647.readLock().lock();
        try {
            zzexf.m12323(this.f5650 == null ? -1 : this.f5650.m12314());
            zzexf.m12322(this.f5645.m12329());
            zzexf.m12324(new FirebaseRemoteConfigSettings.Builder().m6132(this.f5645.m12326()).m6133());
            return zzexf;
        } finally {
            this.f5647.readLock().unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public Task<Void> m6122(long j) {
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.f5647.readLock().lock();
        try {
            zzbhl zzbhl = new zzbhl();
            zzbhl.m10248(j);
            if (this.f5645.m12326()) {
                zzbhl.m10249("_rcn_developer", "true");
            }
            zzbhl.m10247(10300);
            if (!(this.f5649 == null || this.f5649.m12314() == -1)) {
                long convert = TimeUnit.SECONDS.convert(System.currentTimeMillis() - this.f5649.m12314(), TimeUnit.MILLISECONDS);
                zzbhl.m10245(convert < 2147483647L ? (int) convert : Integer.MAX_VALUE);
            }
            if (!(this.f5650 == null || this.f5650.m12314() == -1)) {
                long convert2 = TimeUnit.SECONDS.convert(System.currentTimeMillis() - this.f5650.m12314(), TimeUnit.MILLISECONDS);
                if (convert2 < 2147483647L) {
                    i = (int) convert2;
                }
                zzbhl.m10244(i);
            }
            zzbhg.f8758.m10233(new zzbhx(this.f5646).m4178(), zzbhl.m10246()).m8535(new zza(this, taskCompletionSource));
            this.f5647.readLock().unlock();
            return taskCompletionSource.m13720();
        } catch (Throwable th) {
            this.f5647.readLock().unlock();
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6123(String str) {
        return m6124(str, "configns:firebase");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6124(String str, String str2) {
        if (str2 == null) {
            return "";
        }
        this.f5647.readLock().lock();
        try {
            if (this.f5649 != null && this.f5649.m12320(str, str2)) {
                return new String(this.f5649.m12313(str, str2), zzexg.f10309);
            }
            if (this.f5648 == null || !this.f5648.m12320(str, str2)) {
                this.f5647.readLock().unlock();
                return "";
            }
            String str3 = new String(this.f5648.m12313(str, str2), zzexg.f10309);
            this.f5647.readLock().unlock();
            return str3;
        } finally {
            this.f5647.readLock().unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6125(TaskCompletionSource<Void> taskCompletionSource, zzbhm zzbhm) {
        if (zzbhm == null || zzbhm.s_() == null) {
            this.f5645.m12330(1);
            taskCompletionSource.m13721((Exception) new FirebaseRemoteConfigFetchException());
            m6112();
            return;
        }
        int r0 = zzbhm.s_().m8547();
        this.f5647.writeLock().lock();
        switch (r0) {
            case -6508:
            case -6506:
                this.f5645.m12330(-1);
                if (this.f5650 != null && !this.f5650.m12315()) {
                    Map<String, Set<String>> r2 = zzbhm.m10251();
                    HashMap hashMap = new HashMap();
                    for (String next : r2.keySet()) {
                        HashMap hashMap2 = new HashMap();
                        for (String str : r2.get(next)) {
                            hashMap2.put(str, zzbhm.m10253(str, (byte[]) null, next));
                        }
                        hashMap.put(next, hashMap2);
                    }
                    this.f5650 = new zzexe(hashMap, this.f5650.m12314(), zzbhm.m10252());
                }
                taskCompletionSource.m13722(null);
                m6112();
                break;
            case -6505:
                Map<String, Set<String>> r22 = zzbhm.m10251();
                HashMap hashMap3 = new HashMap();
                for (String next2 : r22.keySet()) {
                    HashMap hashMap4 = new HashMap();
                    for (String str2 : r22.get(next2)) {
                        hashMap4.put(str2, zzbhm.m10253(str2, (byte[]) null, next2));
                    }
                    hashMap3.put(next2, hashMap4);
                }
                this.f5650 = new zzexe(hashMap3, System.currentTimeMillis(), zzbhm.m10252());
                this.f5645.m12330(-1);
                taskCompletionSource.m13722(null);
                m6112();
                break;
            case 6500:
            case 6501:
            case 6503:
            case 6504:
                this.f5645.m12330(1);
                taskCompletionSource.m13721((Exception) new FirebaseRemoteConfigFetchException());
                m6112();
                break;
            case 6502:
            case 6507:
                this.f5645.m12330(2);
                taskCompletionSource.m13721((Exception) new FirebaseRemoteConfigFetchThrottledException(zzbhm.m10250()));
                m6112();
                break;
            default:
                try {
                    if (zzbhm.s_().m8549()) {
                        Log.w("FirebaseRemoteConfig", new StringBuilder(45).append("Unknown (successful) status code: ").append(r0).toString());
                    }
                    this.f5645.m12330(1);
                    taskCompletionSource.m13721((Exception) new FirebaseRemoteConfigFetchException());
                    m6112();
                    break;
                } catch (Throwable th) {
                    this.f5647.writeLock().unlock();
                    throw th;
                }
        }
        this.f5647.writeLock().unlock();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6126(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.f5647.writeLock().lock();
        try {
            boolean r1 = this.f5645.m12326();
            boolean r0 = firebaseRemoteConfigSettings == null ? false : firebaseRemoteConfigSettings.m6130();
            this.f5645.m12334(r0);
            if (r1 != r0) {
                m6112();
            }
        } finally {
            this.f5647.writeLock().unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6127(Map<String, Object> map) {
        m6128(map, "configns:firebase");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6128(Map<String, Object> map, String str) {
        m6119(map, str, true);
    }
}
