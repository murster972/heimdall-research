package com.google.firebase;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzca;
import com.google.android.gms.common.util.zzu;
import com.mopub.mobileads.FlurryAgentWrapper;
import java.util.Arrays;

public final class FirebaseOptions {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f11143;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f11144;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f11145;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f11146;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f11147;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f11148;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f11149;

    private FirebaseOptions(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        zzbq.m9126(!zzu.m9276(str), (Object) "ApplicationId must be set.");
        this.f11146 = str;
        this.f11149 = str2;
        this.f11148 = str3;
        this.f11147 = str4;
        this.f11145 = str5;
        this.f11143 = str6;
        this.f11144 = str7;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FirebaseOptions m13779(Context context) {
        zzca zzca = new zzca(context);
        String r1 = zzca.m9141("google_app_id");
        if (TextUtils.isEmpty(r1)) {
            return null;
        }
        return new FirebaseOptions(r1, zzca.m9141("google_api_key"), zzca.m9141("firebase_database_url"), zzca.m9141("ga_trackingId"), zzca.m9141("gcm_defaultSenderId"), zzca.m9141("google_storage_bucket"), zzca.m9141("project_id"));
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof FirebaseOptions)) {
            return false;
        }
        FirebaseOptions firebaseOptions = (FirebaseOptions) obj;
        return zzbg.m9113(this.f11146, firebaseOptions.f11146) && zzbg.m9113(this.f11149, firebaseOptions.f11149) && zzbg.m9113(this.f11148, firebaseOptions.f11148) && zzbg.m9113(this.f11147, firebaseOptions.f11147) && zzbg.m9113(this.f11145, firebaseOptions.f11145) && zzbg.m9113(this.f11143, firebaseOptions.f11143) && zzbg.m9113(this.f11144, firebaseOptions.f11144);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f11146, this.f11149, this.f11148, this.f11147, this.f11145, this.f11143, this.f11144});
    }

    public final String toString() {
        return zzbg.m9112(this).m9114("applicationId", this.f11146).m9114(FlurryAgentWrapper.PARAM_API_KEY, this.f11149).m9114("databaseUrl", this.f11148).m9114("gcmSenderId", this.f11145).m9114("storageBucket", this.f11143).m9114("projectId", this.f11144).toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m13780() {
        return this.f11145;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13781() {
        return this.f11146;
    }
}
