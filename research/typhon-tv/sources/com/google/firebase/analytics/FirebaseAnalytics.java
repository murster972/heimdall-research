package com.google.firebase.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Keep;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzcim;
import com.google.android.gms.tasks.Task;

@Keep
public final class FirebaseAnalytics {
    private final zzcim zziwf;

    public static class Event {
    }

    public static class Param {
    }

    public static class UserProperty {
    }

    public FirebaseAnalytics(zzcim zzcim) {
        zzbq.m9120(zzcim);
        this.zziwf = zzcim;
    }

    @Keep
    public static FirebaseAnalytics getInstance(Context context) {
        return zzcim.m11006(context).m11020();
    }

    public final Task<String> getAppInstanceId() {
        return this.zziwf.m11022().m11151();
    }

    public final void logEvent(String str, Bundle bundle) {
        this.zziwf.m11019().logEvent(str, bundle);
    }

    public final void resetAnalyticsData() {
        this.zziwf.m11022().m11153();
    }

    public final void setAnalyticsCollectionEnabled(boolean z) {
        this.zziwf.m11019().setMeasurementEnabled(z);
    }

    @Keep
    public final void setCurrentScreen(Activity activity, String str, String str2) {
        this.zziwf.m11027().m11217(activity, str, str2);
    }

    public final void setMinimumSessionDuration(long j) {
        this.zziwf.m11019().setMinimumSessionDuration(j);
    }

    public final void setSessionTimeoutDuration(long j) {
        this.zziwf.m11019().setSessionTimeoutDuration(j);
    }

    public final void setUserId(String str) {
        this.zziwf.m11019().setUserPropertyInternal("app", "_id", str);
    }

    public final void setUserProperty(String str, String str2) {
        this.zziwf.m11019().setUserProperty(str, str2);
    }
}
