package com.google.firebase.internal;

import android.content.Context;
import com.google.firebase.FirebaseApp;
import java.util.concurrent.atomic.AtomicReference;

public final class zzb {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final AtomicReference<zzb> f11237 = new AtomicReference<>();

    private zzb(Context context) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzb m13886(Context context) {
        f11237.compareAndSet((Object) null, new zzb(context));
        return f11237.get();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m13887(FirebaseApp firebaseApp) {
    }
}
