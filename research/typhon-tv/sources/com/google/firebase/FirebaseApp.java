package com.google.firebase;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.api.internal.zzk;
import com.google.android.gms.common.api.internal.zzl;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzs;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class FirebaseApp {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Set<String> f11125 = Collections.emptySet();
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final Object f11126 = new Object();

    /* renamed from: 连任  reason: contains not printable characters */
    private static final List<String> f11127 = Arrays.asList(new String[0]);

    /* renamed from: 靐  reason: contains not printable characters */
    private static final List<String> f11128 = Arrays.asList(new String[]{"com.google.firebase.auth.FirebaseAuth", "com.google.firebase.iid.FirebaseInstanceId"});

    /* renamed from: 麤  reason: contains not printable characters */
    private static final List<String> f11129 = Arrays.asList(new String[]{"com.google.android.gms.measurement.AppMeasurement"});

    /* renamed from: 齉  reason: contains not printable characters */
    private static final List<String> f11130 = Collections.singletonList("com.google.firebase.crash.FirebaseCrash");

    /* renamed from: 龘  reason: contains not printable characters */
    static final Map<String, FirebaseApp> f11131 = new ArrayMap();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Context f11132;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final List<Object> f11133 = new CopyOnWriteArrayList();

    /* renamed from: ʿ  reason: contains not printable characters */
    private final List<zza> f11134 = new CopyOnWriteArrayList();

    /* renamed from: ˈ  reason: contains not printable characters */
    private final AtomicBoolean f11135 = new AtomicBoolean();

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f11136;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final FirebaseOptions f11137;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final AtomicBoolean f11138 = new AtomicBoolean(false);

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final List<Object> f11139 = new CopyOnWriteArrayList();

    /* renamed from: ﾞ  reason: contains not printable characters */
    private zzb f11140;

    public interface zza {
        /* renamed from: 龘  reason: contains not printable characters */
        void m13776(boolean z);
    }

    public interface zzb {
    }

    @TargetApi(24)
    static class zzc extends BroadcastReceiver {

        /* renamed from: 龘  reason: contains not printable characters */
        private static AtomicReference<zzc> f11141 = new AtomicReference<>();

        /* renamed from: 靐  reason: contains not printable characters */
        private final Context f11142;

        private zzc(Context context) {
            this.f11142 = context;
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public static void m13777(Context context) {
            if (f11141.get() == null) {
                zzc zzc = new zzc(context);
                if (f11141.compareAndSet((Object) null, zzc)) {
                    context.registerReceiver(zzc, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        public final void onReceive(Context context, Intent intent) {
            synchronized (FirebaseApp.f11126) {
                for (FirebaseApp r0 : FirebaseApp.f11131.values()) {
                    r0.m13763();
                }
            }
            this.f11142.unregisterReceiver(this);
        }
    }

    private FirebaseApp(Context context, String str, FirebaseOptions firebaseOptions) {
        this.f11132 = (Context) zzbq.m9120(context);
        this.f11136 = zzbq.m9122(str);
        this.f11137 = (FirebaseOptions) zzbq.m9120(firebaseOptions);
        this.f11140 = new com.google.firebase.internal.zza();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final void m13762() {
        zzbq.m9126(!this.f11135.get(), (Object) "FirebaseApp was deleted");
    }

    /* access modifiers changed from: private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m13763() {
        m13770(FirebaseApp.class, this, (Iterable<String>) f11128);
        if (m13772()) {
            m13770(FirebaseApp.class, this, (Iterable<String>) f11130);
            m13770(Context.class, this.f11132, (Iterable<String>) f11129);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m13764(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (zza r0 : this.f11134) {
            r0.m13776(z);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static FirebaseApp m13765() {
        FirebaseApp firebaseApp;
        synchronized (f11126) {
            firebaseApp = f11131.get("[DEFAULT]");
            if (firebaseApp == null) {
                String r2 = zzs.m9274();
                throw new IllegalStateException(new StringBuilder(String.valueOf(r2).length() + 116).append("Default FirebaseApp is not initialized in this process ").append(r2).append(". Make sure to call FirebaseApp.initializeApp(Context) first.").toString());
            }
        }
        return firebaseApp;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FirebaseApp m13766(Context context) {
        FirebaseApp r0;
        synchronized (f11126) {
            if (f11131.containsKey("[DEFAULT]")) {
                r0 = m13765();
            } else {
                FirebaseOptions r02 = FirebaseOptions.m13779(context);
                r0 = r02 == null ? null : m13767(context, r02);
            }
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FirebaseApp m13767(Context context, FirebaseOptions firebaseOptions) {
        return m13768(context, firebaseOptions, "[DEFAULT]");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FirebaseApp m13768(Context context, FirebaseOptions firebaseOptions, String str) {
        FirebaseApp firebaseApp;
        com.google.firebase.internal.zzb.m13886(context);
        if (context.getApplicationContext() instanceof Application) {
            zzk.m8936((Application) context.getApplicationContext());
            zzk.m8935().m8937((zzl) new zza());
        }
        String trim = str.trim();
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (f11126) {
            zzbq.m9126(!f11131.containsKey(trim), (Object) new StringBuilder(String.valueOf(trim).length() + 33).append("FirebaseApp name ").append(trim).append(" already exists!").toString());
            zzbq.m9121(context, (Object) "Application context cannot be null.");
            firebaseApp = new FirebaseApp(context, trim, firebaseOptions);
            f11131.put(trim, firebaseApp);
        }
        com.google.firebase.internal.zzb.m13887(firebaseApp);
        firebaseApp.m13770(FirebaseApp.class, firebaseApp, (Iterable<String>) f11128);
        if (firebaseApp.m13772()) {
            firebaseApp.m13770(FirebaseApp.class, firebaseApp, (Iterable<String>) f11130);
            firebaseApp.m13770(Context.class, firebaseApp.m13775(), (Iterable<String>) f11129);
        }
        return firebaseApp;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final <T> void m13770(Class<T> cls, T t, Iterable<String> iterable) {
        boolean isDeviceProtectedStorage = ContextCompat.isDeviceProtectedStorage(this.f11132);
        if (isDeviceProtectedStorage) {
            zzc.m13777(this.f11132);
        }
        for (String next : iterable) {
            if (isDeviceProtectedStorage) {
                try {
                    if (!f11127.contains(next)) {
                    }
                } catch (ClassNotFoundException e) {
                    if (f11125.contains(next)) {
                        throw new IllegalStateException(String.valueOf(next).concat(" is missing, but is required. Check if it has been removed by Proguard."));
                    }
                    Log.d("FirebaseApp", String.valueOf(next).concat(" is not linked. Skipping initialization."));
                } catch (NoSuchMethodException e2) {
                    throw new IllegalStateException(String.valueOf(next).concat("#getInstance has been removed by Proguard. Add keep rule to prevent it."));
                } catch (InvocationTargetException e3) {
                    Log.wtf("FirebaseApp", "Firebase API initialization failure.", e3);
                } catch (IllegalAccessException e4) {
                    String valueOf = String.valueOf(next);
                    Log.wtf("FirebaseApp", valueOf.length() != 0 ? "Failed to initialize ".concat(valueOf) : new String("Failed to initialize "), e4);
                }
            }
            Method method = Class.forName(next).getMethod("getInstance", new Class[]{cls});
            int modifiers = method.getModifiers();
            if (Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers)) {
                method.invoke((Object) null, new Object[]{t});
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m13771(boolean z) {
        synchronized (f11126) {
            ArrayList arrayList = new ArrayList(f11131.values());
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                FirebaseApp firebaseApp = (FirebaseApp) obj;
                if (firebaseApp.f11138.get()) {
                    firebaseApp.m13764(z);
                }
            }
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FirebaseApp)) {
            return false;
        }
        return this.f11136.equals(((FirebaseApp) obj).m13773());
    }

    public int hashCode() {
        return this.f11136.hashCode();
    }

    public String toString() {
        return zzbg.m9112(this).m9114("name", this.f11136).m9114("options", this.f11137).toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m13772() {
        return "[DEFAULT]".equals(m13773());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m13773() {
        m13762();
        return this.f11136;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public FirebaseOptions m13774() {
        m13762();
        return this.f11137;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Context m13775() {
        m13762();
        return this.f11132;
    }
}
