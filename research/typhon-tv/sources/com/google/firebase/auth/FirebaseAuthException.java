package com.google.firebase.auth;

import com.google.android.gms.common.internal.zzbq;
import com.google.firebase.FirebaseException;

public class FirebaseAuthException extends FirebaseException {
    private final String zzmdm;

    public FirebaseAuthException(String str, String str2) {
        super(str2);
        this.zzmdm = zzbq.m9122(str);
    }

    public String getErrorCode() {
        return this.zzmdm;
    }
}
