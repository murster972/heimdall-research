package com.google.firebase.iid;

import android.content.Intent;
import android.util.Log;

final class zze implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzd f11179;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Intent f11180;

    zze(zzd zzd, Intent intent) {
        this.f11179 = zzd;
        this.f11180 = intent;
    }

    public final void run() {
        String action = this.f11180.getAction();
        Log.w("EnhancedIntentService", new StringBuilder(String.valueOf(action).length() + 61).append("Service took too long to process intent: ").append(action).append(" App may get closed.").toString());
        this.f11179.m13819();
    }
}
