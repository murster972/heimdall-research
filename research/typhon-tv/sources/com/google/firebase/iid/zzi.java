package com.google.firebase.iid;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public final class zzi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzi f11190;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11191 = 1;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context f11192;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzk f11193 = new zzk(this);
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final ScheduledExecutorService f11194;

    private zzi(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.f11194 = scheduledExecutorService;
        this.f11192 = context.getApplicationContext();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final synchronized int m13825() {
        int i;
        i = this.f11191;
        this.f11191 = i + 1;
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final synchronized <T> Task<T> m13827(zzr<T> zzr) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(zzr);
            Log.d("MessengerIpcClient", new StringBuilder(String.valueOf(valueOf).length() + 9).append("Queueing ").append(valueOf).toString());
        }
        if (!this.f11193.m13838((zzr) zzr)) {
            this.f11193 = new zzk(this);
            this.f11193.m13838((zzr) zzr);
        }
        return zzr.f11208.m13720();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized zzi m13828(Context context) {
        zzi zzi;
        synchronized (zzi.class) {
            if (f11190 == null) {
                f11190 = new zzi(context, Executors.newSingleThreadScheduledExecutor());
            }
            zzi = f11190;
        }
        return zzi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Task<Bundle> m13829(int i, Bundle bundle) {
        return m13827(new zzt(m13825(), 1, bundle));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Task<Void> m13830(int i, Bundle bundle) {
        return m13827(new zzq(m13825(), 2, bundle));
    }
}
