package com.google.firebase.iid;

import android.util.Log;

final class zzg implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzf f11182;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzd f11183;

    zzg(zzf zzf, zzd zzd) {
        this.f11182 = zzf;
        this.f11183 = zzd;
    }

    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.f11182.f11181.m13816(this.f11183.f11178);
        this.f11183.m13819();
    }
}
