package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.zzq;

public final class FirebaseInstanceIdReceiver extends WakefulBroadcastReceiver {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzh f11159;

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzh f11160;

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f11161 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized zzh m13802(Context context, String str) {
        zzh zzh;
        synchronized (FirebaseInstanceIdReceiver.class) {
            if ("com.google.firebase.MESSAGING_EVENT".equals(str)) {
                if (f11160 == null) {
                    f11160 = new zzh(context, str);
                }
                zzh = f11160;
            } else {
                if (f11159 == null) {
                    f11159 = new zzh(context, str);
                }
                zzh = f11159;
            }
        }
        return zzh;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m13803(Context context, Intent intent, String str) {
        String str2 = null;
        boolean z = false;
        int i = -1;
        intent.setComponent((ComponentName) null);
        intent.setPackage(context.getPackageName());
        if (Build.VERSION.SDK_INT <= 18) {
            intent.removeCategory(context.getPackageName());
        }
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        if ("google.com/iid".equals(intent.getStringExtra("from")) || "com.google.firebase.INSTANCE_ID_EVENT".equals(str)) {
            str2 = "com.google.firebase.INSTANCE_ID_EVENT";
        } else if ("com.google.android.c2dm.intent.RECEIVE".equals(str) || "com.google.firebase.MESSAGING_EVENT".equals(str)) {
            str2 = "com.google.firebase.MESSAGING_EVENT";
        } else {
            Log.d("FirebaseInstanceId", "Unexpected intent");
        }
        if (str2 != null) {
            if (zzq.m9267() && context.getApplicationInfo().targetSdkVersion >= 26) {
                z = true;
            }
            if (z) {
                if (isOrderedBroadcast()) {
                    setResultCode(-1);
                }
                m13802(context, str2).m13823(intent, goAsync());
            } else {
                i = zzx.m13866().m13868(context, str2, intent);
            }
        }
        if (isOrderedBroadcast()) {
            setResultCode(i);
        }
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
            if (parcelableExtra instanceof Intent) {
                m13803(context, (Intent) parcelableExtra, intent.getAction());
            } else {
                m13803(context, intent, intent.getAction());
            }
        }
    }
}
