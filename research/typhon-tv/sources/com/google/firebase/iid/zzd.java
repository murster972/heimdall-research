package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class zzd {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BroadcastReceiver.PendingResult f11175;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ScheduledFuture<?> f11176;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f11177 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    final Intent f11178;

    zzd(Intent intent, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.f11178 = intent;
        this.f11175 = pendingResult;
        this.f11176 = scheduledExecutorService.schedule(new zze(this, intent), 9500, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13819() {
        if (!this.f11177) {
            this.f11175.finish();
            this.f11176.cancel(false);
            this.f11177 = true;
        }
    }
}
