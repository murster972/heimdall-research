package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import java.io.IOException;

final class zzaa implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final PowerManager.WakeLock f11162 = ((PowerManager) m13812().getSystemService("power")).newWakeLock(1, "fiid-sync");

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzu f11163;

    /* renamed from: 齉  reason: contains not printable characters */
    private final FirebaseInstanceId f11164;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f11165;

    zzaa(FirebaseInstanceId firebaseInstanceId, zzu zzu, long j) {
        this.f11164 = firebaseInstanceId;
        this.f11163 = zzu;
        this.f11165 = j;
        this.f11162.setReferenceCounted(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
        if (m13810(r0) != false) goto L_0x0025;
     */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m13808() {
        /*
            r3 = this;
        L_0x0000:
            com.google.firebase.iid.FirebaseInstanceId r1 = r3.f11164
            monitor-enter(r1)
            com.google.firebase.iid.zzy r0 = com.google.firebase.iid.FirebaseInstanceId.m13782()     // Catch:{ all -> 0x0022 }
            java.lang.String r0 = r0.m13880()     // Catch:{ all -> 0x0022 }
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = "FirebaseInstanceId"
            java.lang.String r2 = "topic sync succeeded"
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x0022 }
            r0 = 1
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
        L_0x0018:
            return r0
        L_0x0019:
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            boolean r1 = r3.m13810(r0)
            if (r1 != 0) goto L_0x0025
            r0 = 0
            goto L_0x0018
        L_0x0022:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            throw r0
        L_0x0025:
            com.google.firebase.iid.zzy r1 = com.google.firebase.iid.FirebaseInstanceId.m13782()
            r1.m13876(r0)
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.zzaa.m13808():boolean");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m13809() {
        zzz r2 = this.f11164.m13792();
        if (r2 != null && !r2.m13885(this.f11163.m13852())) {
            return true;
        }
        try {
            String r3 = this.f11164.m13789();
            if (r3 == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if (r2 != null && (r2 == null || r3.equals(r2.f11236))) {
                return true;
            }
            Context r22 = m13812();
            Intent intent = new Intent("com.google.firebase.iid.TOKEN_REFRESH");
            Intent intent2 = new Intent("com.google.firebase.INSTANCE_ID_EVENT");
            intent2.setClass(r22, FirebaseInstanceIdReceiver.class);
            intent2.putExtra("wrapped_intent", intent);
            r22.sendBroadcast(intent2);
            return true;
        } catch (IOException | SecurityException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.e("FirebaseInstanceId", valueOf.length() != 0 ? "Token retrieval failed: ".concat(valueOf) : new String("Token retrieval failed: "));
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007b  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m13810(java.lang.String r7) {
        /*
            r6 = this;
            r0 = 1
            r1 = 0
            java.lang.String r2 = "!"
            java.lang.String[] r2 = r7.split(r2)
            int r3 = r2.length
            r4 = 2
            if (r3 != r4) goto L_0x001c
            r3 = r2[r1]
            r4 = r2[r0]
            r2 = -1
            int r5 = r3.hashCode()     // Catch:{ IOException -> 0x0048 }
            switch(r5) {
                case 83: goto L_0x001d;
                case 84: goto L_0x0019;
                case 85: goto L_0x0028;
                default: goto L_0x0019;
            }     // Catch:{ IOException -> 0x0048 }
        L_0x0019:
            switch(r2) {
                case 0: goto L_0x0033;
                case 1: goto L_0x0066;
                default: goto L_0x001c;
            }     // Catch:{ IOException -> 0x0048 }
        L_0x001c:
            return r0
        L_0x001d:
            java.lang.String r5 = "S"
            boolean r3 = r3.equals(r5)     // Catch:{ IOException -> 0x0048 }
            if (r3 == 0) goto L_0x0019
            r2 = r1
            goto L_0x0019
        L_0x0028:
            java.lang.String r5 = "U"
            boolean r3 = r3.equals(r5)     // Catch:{ IOException -> 0x0048 }
            if (r3 == 0) goto L_0x0019
            r2 = r0
            goto L_0x0019
        L_0x0033:
            com.google.firebase.iid.FirebaseInstanceId r2 = r6.f11164     // Catch:{ IOException -> 0x0048 }
            r2.m13794(r4)     // Catch:{ IOException -> 0x0048 }
            boolean r2 = com.google.firebase.iid.FirebaseInstanceId.m13783()     // Catch:{ IOException -> 0x0048 }
            if (r2 == 0) goto L_0x001c
            java.lang.String r2 = "FirebaseInstanceId"
            java.lang.String r3 = "subscribe operation succeeded"
            android.util.Log.d(r2, r3)     // Catch:{ IOException -> 0x0048 }
            goto L_0x001c
        L_0x0048:
            r0 = move-exception
            java.lang.String r2 = "FirebaseInstanceId"
            java.lang.String r3 = "Topic sync failed: "
            java.lang.String r0 = r0.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r4 = r0.length()
            if (r4 == 0) goto L_0x007b
            java.lang.String r0 = r3.concat(r0)
        L_0x0061:
            android.util.Log.e(r2, r0)
            r0 = r1
            goto L_0x001c
        L_0x0066:
            com.google.firebase.iid.FirebaseInstanceId r2 = r6.f11164     // Catch:{ IOException -> 0x0048 }
            r2.m13797(r4)     // Catch:{ IOException -> 0x0048 }
            boolean r2 = com.google.firebase.iid.FirebaseInstanceId.m13783()     // Catch:{ IOException -> 0x0048 }
            if (r2 == 0) goto L_0x001c
            java.lang.String r2 = "FirebaseInstanceId"
            java.lang.String r3 = "unsubscribe operation succeeded"
            android.util.Log.d(r2, r3)     // Catch:{ IOException -> 0x0048 }
            goto L_0x001c
        L_0x007b:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r3)
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.zzaa.m13810(java.lang.String):boolean");
    }

    public final void run() {
        boolean z = true;
        this.f11162.acquire();
        try {
            this.f11164.m13801(true);
            if (this.f11163.m13855() == 0) {
                z = false;
            }
            if (!z) {
                this.f11164.m13801(false);
            } else if (!m13811()) {
                new zzab(this).m13813();
                this.f11162.release();
            } else {
                if (!m13809() || !m13808()) {
                    this.f11164.m13799(this.f11165);
                } else {
                    this.f11164.m13801(false);
                }
                this.f11162.release();
            }
        } finally {
            this.f11162.release();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13811() {
        ConnectivityManager connectivityManager = (ConnectivityManager) m13812().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Context m13812() {
        return this.f11164.m13793().m13775();
    }
}
