package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

final class zzab extends BroadcastReceiver {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzaa f11166;

    public zzab(zzaa zzaa) {
        this.f11166 = zzaa;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.f11166 != null && this.f11166.m13811()) {
            if (FirebaseInstanceId.m13783()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.m13788((Runnable) this.f11166, 0);
            this.f11166.m13812().unregisterReceiver(this);
            this.f11166 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13813() {
        if (FirebaseInstanceId.m13783()) {
            Log.d("FirebaseInstanceId", "Connectivity change received registered");
        }
        this.f11166.m13812().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }
}
