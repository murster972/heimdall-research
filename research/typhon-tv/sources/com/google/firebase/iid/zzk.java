package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.stats.zza;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

final class zzk implements ServiceConnection {

    /* renamed from: ʻ  reason: contains not printable characters */
    final /* synthetic */ zzi f11195;

    /* renamed from: 连任  reason: contains not printable characters */
    final SparseArray<zzr<?>> f11196;

    /* renamed from: 靐  reason: contains not printable characters */
    final Messenger f11197;

    /* renamed from: 麤  reason: contains not printable characters */
    final Queue<zzr<?>> f11198;

    /* renamed from: 齉  reason: contains not printable characters */
    zzp f11199;

    /* renamed from: 龘  reason: contains not printable characters */
    int f11200;

    private zzk(zzi zzi) {
        this.f11195 = zzi;
        this.f11200 = 0;
        this.f11197 = new Messenger(new Handler(Looper.getMainLooper(), new zzl(this)));
        this.f11198 = new ArrayDeque();
        this.f11196 = new SparseArray<>();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m13831() {
        this.f11195.f11194.execute(new zzn(this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m13832(zzs zzs) {
        for (zzr r0 : this.f11198) {
            r0.m13843(zzs);
        }
        this.f11198.clear();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f11196.size()) {
                this.f11196.valueAt(i2).m13843(zzs);
                i = i2 + 1;
            } else {
                this.f11196.clear();
                return;
            }
        }
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        if (iBinder == null) {
            m13836(0, "Null service connection");
        } else {
            try {
                this.f11199 = new zzp(iBinder);
                this.f11200 = 2;
                m13831();
            } catch (RemoteException e) {
                m13836(0, e.getMessage());
            }
        }
        return;
    }

    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        m13836(2, "Service disconnected");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m13833() {
        if (this.f11200 == 1) {
            m13836(1, "Timed out while binding");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13834() {
        if (this.f11200 == 2 && this.f11198.isEmpty() && this.f11196.size() == 0) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
            }
            this.f11200 = 3;
            zza.m9235();
            this.f11195.f11192.unbindService(this);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13835(int i) {
        zzr zzr = this.f11196.get(i);
        if (zzr != null) {
            Log.w("MessengerIpcClient", new StringBuilder(31).append("Timing out request: ").append(i).toString());
            this.f11196.remove(i);
            zzr.m13843(new zzs(3, "Timed out waiting for response"));
            m13834();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13836(int i, String str) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(str);
            Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
        }
        switch (this.f11200) {
            case 0:
                throw new IllegalStateException();
            case 1:
            case 2:
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Unbinding service");
                }
                this.f11200 = 4;
                zza.m9235();
                this.f11195.f11192.unbindService(this);
                m13832(new zzs(i, str));
                break;
            case 3:
                this.f11200 = 4;
                break;
            case 4:
                break;
            default:
                throw new IllegalStateException(new StringBuilder(26).append("Unknown state: ").append(this.f11200).toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13837(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            Log.d("MessengerIpcClient", new StringBuilder(41).append("Received response to request: ").append(i).toString());
        }
        synchronized (this) {
            zzr zzr = this.f11196.get(i);
            if (zzr == null) {
                Log.w("MessengerIpcClient", new StringBuilder(50).append("Received response for unknown request: ").append(i).toString());
            } else {
                this.f11196.remove(i);
                m13834();
                Bundle data = message.getData();
                if (data.getBoolean("unsupported", false)) {
                    zzr.m13843(new zzs(4, "Not supported by GmsCore"));
                } else {
                    zzr.m13842(data);
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized boolean m13838(zzr zzr) {
        boolean z = false;
        boolean z2 = true;
        synchronized (this) {
            switch (this.f11200) {
                case 0:
                    this.f11198.add(zzr);
                    if (this.f11200 == 0) {
                        z = true;
                    }
                    zzbq.m9125(z);
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Starting bind to GmsCore");
                    }
                    this.f11200 = 1;
                    Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                    intent.setPackage("com.google.android.gms");
                    if (zza.m9235().m9236(this.f11195.f11192, intent, this, 1)) {
                        this.f11195.f11194.schedule(new zzm(this), 30, TimeUnit.SECONDS);
                        break;
                    } else {
                        m13836(0, "Unable to bind to service");
                        break;
                    }
                case 1:
                    this.f11198.add(zzr);
                    break;
                case 2:
                    this.f11198.add(zzr);
                    m13831();
                    break;
                case 3:
                case 4:
                    z2 = false;
                    break;
                default:
                    throw new IllegalStateException(new StringBuilder(26).append("Unknown state: ").append(this.f11200).toString());
            }
        }
        return z2;
    }
}
