package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.zzq;
import com.google.firebase.FirebaseApp;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

final class zzu {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11212 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f11213;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f11214;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f11215;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f11216;

    public zzu(Context context) {
        this.f11216 = context;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final synchronized void m13848() {
        PackageInfo r0 = m13849(this.f11216.getPackageName());
        if (r0 != null) {
            this.f11213 = Integer.toString(r0.versionCode);
            this.f11215 = r0.versionName;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final PackageInfo m13849(String str) {
        try {
            return this.f11216.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 23).append("Failed to find package ").append(valueOf).toString());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m13850(FirebaseApp firebaseApp) {
        String r0 = firebaseApp.m13774().m13780();
        if (r0 != null) {
            return r0;
        }
        String r02 = firebaseApp.m13774().m13781();
        if (!r02.startsWith("1:")) {
            return r02;
        }
        String[] split = r02.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m13851(KeyPair keyPair) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(keyPair.getPublic().getEncoded());
            digest[0] = (byte) ((digest[0] & 15) + 112);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException e) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required algorithms");
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized String m13852() {
        if (this.f11213 == null) {
            m13848();
        }
        return this.f11213;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final synchronized int m13853() {
        PackageInfo r0;
        if (this.f11214 == 0 && (r0 = m13849("com.google.android.gms")) != null) {
            this.f11214 = r0.versionCode;
        }
        return this.f11214;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final synchronized String m13854() {
        if (this.f11215 == null) {
            m13848();
        }
        return this.f11215;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized int m13855() {
        int i = 0;
        synchronized (this) {
            if (this.f11212 != 0) {
                i = this.f11212;
            } else {
                PackageManager packageManager = this.f11216.getPackageManager();
                if (packageManager.checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1) {
                    Log.e("FirebaseInstanceId", "Google Play services missing or without correct permission.");
                } else {
                    if (!zzq.m9267()) {
                        Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                        intent.setPackage("com.google.android.gms");
                        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
                        if (queryIntentServices != null && queryIntentServices.size() > 0) {
                            this.f11212 = 1;
                            i = this.f11212;
                        }
                    }
                    Intent intent2 = new Intent("com.google.iid.TOKEN_REQUEST");
                    intent2.setPackage("com.google.android.gms");
                    List<ResolveInfo> queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent2, 0);
                    if (queryBroadcastReceivers == null || queryBroadcastReceivers.size() <= 0) {
                        Log.w("FirebaseInstanceId", "Failed to resolve IID implementation package, falling back");
                        if (zzq.m9267()) {
                            this.f11212 = 2;
                        } else {
                            this.f11212 = 1;
                        }
                        i = this.f11212;
                    } else {
                        this.f11212 = 2;
                        i = this.f11212;
                    }
                }
            }
        }
        return i;
    }
}
