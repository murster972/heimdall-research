package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import com.google.android.gms.iid.MessengerCompat;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class zzv {

    /* renamed from: 靐  reason: contains not printable characters */
    private static PendingIntent f11217;

    /* renamed from: 龘  reason: contains not printable characters */
    private static int f11218 = 0;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Messenger f11219;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Messenger f11220;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MessengerCompat f11221;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzu f11222;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f11223;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SimpleArrayMap<String, TaskCompletionSource<Bundle>> f11224 = new SimpleArrayMap<>();

    public zzv(Context context, zzu zzu) {
        this.f11223 = context;
        this.f11222 = zzu;
        this.f11219 = new Messenger(new zzw(this, Looper.getMainLooper()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final Bundle m13856(Bundle bundle) throws IOException {
        Bundle r0 = m13857(bundle);
        if (r0 == null || !r0.containsKey("google.messenger")) {
            return r0;
        }
        Bundle r02 = m13857(bundle);
        if (r02 == null || !r02.containsKey("google.messenger")) {
            return r02;
        }
        return null;
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    /* renamed from: 齉  reason: contains not printable characters */
    private final android.os.Bundle m13857(android.os.Bundle r10) throws java.io.IOException {
        /*
            r9 = this;
            r8 = 3
            r7 = 2
            java.lang.String r1 = m13858()
            com.google.android.gms.tasks.TaskCompletionSource r0 = new com.google.android.gms.tasks.TaskCompletionSource
            r0.<init>()
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r2 = r9.f11224
            monitor-enter(r2)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r3 = r9.f11224     // Catch:{ all -> 0x0025 }
            r3.put(r1, r0)     // Catch:{ all -> 0x0025 }
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            com.google.firebase.iid.zzu r2 = r9.f11222
            int r2 = r2.m13855()
            if (r2 != 0) goto L_0x0028
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "MISSING_INSTANCEID_SERVICE"
            r0.<init>(r1)
            throw r0
        L_0x0025:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            throw r0
        L_0x0028:
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "com.google.android.gms"
            r2.setPackage(r3)
            com.google.firebase.iid.zzu r3 = r9.f11222
            int r3 = r3.m13855()
            if (r3 != r7) goto L_0x00e0
            java.lang.String r3 = "com.google.iid.TOKEN_REQUEST"
            r2.setAction(r3)
        L_0x0041:
            r2.putExtras(r10)
            android.content.Context r3 = r9.f11223
            m13859((android.content.Context) r3, (android.content.Intent) r2)
            java.lang.String r3 = "kid"
            java.lang.String r4 = java.lang.String.valueOf(r1)
            int r4 = r4.length()
            int r4 = r4 + 5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "|ID|"
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = "|"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.putExtra(r3, r4)
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r3 = android.util.Log.isLoggable(r3, r8)
            if (r3 == 0) goto L_0x00a9
            java.lang.String r3 = "FirebaseInstanceId"
            android.os.Bundle r4 = r2.getExtras()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.String r5 = java.lang.String.valueOf(r4)
            int r5 = r5.length()
            int r5 = r5 + 8
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            java.lang.String r5 = "Sending "
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
        L_0x00a9:
            java.lang.String r3 = "google.messenger"
            android.os.Messenger r4 = r9.f11219
            r2.putExtra(r3, r4)
            android.os.Messenger r3 = r9.f11220
            if (r3 != 0) goto L_0x00b9
            com.google.android.gms.iid.MessengerCompat r3 = r9.f11221
            if (r3 == 0) goto L_0x0101
        L_0x00b9:
            android.os.Message r3 = android.os.Message.obtain()
            r3.obj = r2
            android.os.Messenger r4 = r9.f11220     // Catch:{ RemoteException -> 0x00ee }
            if (r4 == 0) goto L_0x00e8
            android.os.Messenger r4 = r9.f11220     // Catch:{ RemoteException -> 0x00ee }
            r4.send(r3)     // Catch:{ RemoteException -> 0x00ee }
        L_0x00c8:
            com.google.android.gms.tasks.Task r0 = r0.m13720()     // Catch:{ InterruptedException -> 0x0118, TimeoutException -> 0x014b, ExecutionException -> 0x0136 }
            r2 = 30000(0x7530, double:1.4822E-319)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0118, TimeoutException -> 0x014b, ExecutionException -> 0x0136 }
            java.lang.Object r0 = com.google.android.gms.tasks.Tasks.m13728(r0, r2, r4)     // Catch:{ InterruptedException -> 0x0118, TimeoutException -> 0x014b, ExecutionException -> 0x0136 }
            android.os.Bundle r0 = (android.os.Bundle) r0     // Catch:{ InterruptedException -> 0x0118, TimeoutException -> 0x014b, ExecutionException -> 0x0136 }
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r2 = r9.f11224
            monitor-enter(r2)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r3 = r9.f11224     // Catch:{ all -> 0x0115 }
            r3.remove(r1)     // Catch:{ all -> 0x0115 }
            monitor-exit(r2)     // Catch:{ all -> 0x0115 }
            return r0
        L_0x00e0:
            java.lang.String r3 = "com.google.android.c2dm.intent.REGISTER"
            r2.setAction(r3)
            goto L_0x0041
        L_0x00e8:
            com.google.android.gms.iid.MessengerCompat r4 = r9.f11221     // Catch:{ RemoteException -> 0x00ee }
            r4.m9421(r3)     // Catch:{ RemoteException -> 0x00ee }
            goto L_0x00c8
        L_0x00ee:
            r3 = move-exception
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r3 = android.util.Log.isLoggable(r3, r8)
            if (r3 == 0) goto L_0x0101
            java.lang.String r3 = "FirebaseInstanceId"
            java.lang.String r4 = "Messenger failed, fallback to startService"
            android.util.Log.d(r3, r4)
        L_0x0101:
            com.google.firebase.iid.zzu r3 = r9.f11222
            int r3 = r3.m13855()
            if (r3 != r7) goto L_0x010f
            android.content.Context r3 = r9.f11223
            r3.sendBroadcast(r2)
            goto L_0x00c8
        L_0x010f:
            android.content.Context r3 = r9.f11223
            r3.startService(r2)
            goto L_0x00c8
        L_0x0115:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0115 }
            throw r0
        L_0x0118:
            r0 = move-exception
        L_0x0119:
            java.lang.String r0 = "FirebaseInstanceId"
            java.lang.String r2 = "No response"
            android.util.Log.w(r0, r2)     // Catch:{ all -> 0x012b }
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x012b }
            java.lang.String r2 = "TIMEOUT"
            r0.<init>(r2)     // Catch:{ all -> 0x012b }
            throw r0     // Catch:{ all -> 0x012b }
        L_0x012b:
            r0 = move-exception
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r2 = r9.f11224
            monitor-enter(r2)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r3 = r9.f11224     // Catch:{ all -> 0x0148 }
            r3.remove(r1)     // Catch:{ all -> 0x0148 }
            monitor-exit(r2)     // Catch:{ all -> 0x0148 }
            throw r0
        L_0x0136:
            r0 = move-exception
            java.lang.Throwable r0 = r0.getCause()     // Catch:{ all -> 0x012b }
            boolean r2 = r0 instanceof java.io.IOException     // Catch:{ all -> 0x012b }
            if (r2 == 0) goto L_0x0142
            java.io.IOException r0 = (java.io.IOException) r0     // Catch:{ all -> 0x012b }
            throw r0     // Catch:{ all -> 0x012b }
        L_0x0142:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x012b }
            r2.<init>(r0)     // Catch:{ all -> 0x012b }
            throw r2     // Catch:{ all -> 0x012b }
        L_0x0148:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0148 }
            throw r0
        L_0x014b:
            r0 = move-exception
            goto L_0x0119
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.zzv.m13857(android.os.Bundle):android.os.Bundle");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized String m13858() {
        String num;
        synchronized (zzv.class) {
            int i = f11218;
            f11218 = i + 1;
            num = Integer.toString(i);
        }
        return num;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized void m13859(Context context, Intent intent) {
        synchronized (zzv.class) {
            if (f11217 == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                f11217 = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", f11217);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13860(Message message) {
        String str;
        if (message == null || !(message.obj instanceof Intent)) {
            Log.w("FirebaseInstanceId", "Dropping invalid message");
            return;
        }
        Intent intent = (Intent) message.obj;
        intent.setExtrasClassLoader(MessengerCompat.class.getClassLoader());
        if (intent.hasExtra("google.messenger")) {
            Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
            if (parcelableExtra instanceof MessengerCompat) {
                this.f11221 = (MessengerCompat) parcelableExtra;
            }
            if (parcelableExtra instanceof Messenger) {
                this.f11220 = (Messenger) parcelableExtra;
            }
        }
        Intent intent2 = (Intent) message.obj;
        String action = intent2.getAction();
        if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
            String stringExtra = intent2.getStringExtra("registration_id");
            if (stringExtra == null) {
                stringExtra = intent2.getStringExtra("unregistered");
            }
            if (stringExtra == null) {
                String stringExtra2 = intent2.getStringExtra("error");
                if (stringExtra2 == null) {
                    String valueOf = String.valueOf(intent2.getExtras());
                    Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 49).append("Unexpected response, no error or registration id ").append(valueOf).toString());
                    return;
                }
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf2 = String.valueOf(stringExtra2);
                    Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Received InstanceID error ".concat(valueOf2) : new String("Received InstanceID error "));
                }
                if (stringExtra2.startsWith("|")) {
                    String[] split = stringExtra2.split("\\|");
                    if (!"ID".equals(split[1])) {
                        String valueOf3 = String.valueOf(stringExtra2);
                        Log.w("FirebaseInstanceId", valueOf3.length() != 0 ? "Unexpected structured response ".concat(valueOf3) : new String("Unexpected structured response "));
                    }
                    if (split.length > 2) {
                        str = split[2];
                        String str2 = split[3];
                        stringExtra2 = str2.startsWith(":") ? str2.substring(1) : str2;
                    } else {
                        stringExtra2 = "UNKNOWN";
                        str = null;
                    }
                    intent2.putExtra("error", stringExtra2);
                } else {
                    str = null;
                }
                m13862(str, stringExtra2);
                return;
            }
            Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
            if (matcher.matches()) {
                String group = matcher.group(1);
                String group2 = matcher.group(2);
                Bundle extras = intent2.getExtras();
                extras.putString("registration_id", group2);
                synchronized (this.f11224) {
                    TaskCompletionSource remove = this.f11224.remove(group);
                    if (remove == null) {
                        String valueOf4 = String.valueOf(group);
                        Log.w("FirebaseInstanceId", valueOf4.length() != 0 ? "Missing callback for ".concat(valueOf4) : new String("Missing callback for "));
                        return;
                    }
                    remove.m13722(extras);
                }
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf5 = String.valueOf(stringExtra);
                Log.d("FirebaseInstanceId", valueOf5.length() != 0 ? "Unexpected response string: ".concat(valueOf5) : new String("Unexpected response string: "));
            }
        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf6 = String.valueOf(action);
            Log.d("FirebaseInstanceId", valueOf6.length() != 0 ? "Unexpected response action: ".concat(valueOf6) : new String("Unexpected response action: "));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m13862(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r2 = r5.f11224
            monitor-enter(r2)
            if (r6 != 0) goto L_0x002a
            r0 = 0
            r1 = r0
        L_0x0007:
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r0 = r5.f11224     // Catch:{ all -> 0x004d }
            int r0 = r0.size()     // Catch:{ all -> 0x004d }
            if (r1 >= r0) goto L_0x0023
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r0 = r5.f11224     // Catch:{ all -> 0x004d }
            java.lang.Object r0 = r0.valueAt(r1)     // Catch:{ all -> 0x004d }
            com.google.android.gms.tasks.TaskCompletionSource r0 = (com.google.android.gms.tasks.TaskCompletionSource) r0     // Catch:{ all -> 0x004d }
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x004d }
            r3.<init>(r7)     // Catch:{ all -> 0x004d }
            r0.m13721((java.lang.Exception) r3)     // Catch:{ all -> 0x004d }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0007
        L_0x0023:
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r0 = r5.f11224     // Catch:{ all -> 0x004d }
            r0.clear()     // Catch:{ all -> 0x004d }
        L_0x0028:
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
        L_0x0029:
            return
        L_0x002a:
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.TaskCompletionSource<android.os.Bundle>> r0 = r5.f11224     // Catch:{ all -> 0x004d }
            java.lang.Object r0 = r0.remove(r6)     // Catch:{ all -> 0x004d }
            com.google.android.gms.tasks.TaskCompletionSource r0 = (com.google.android.gms.tasks.TaskCompletionSource) r0     // Catch:{ all -> 0x004d }
            if (r0 != 0) goto L_0x0056
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r3 = "Missing callback for "
            java.lang.String r0 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x004d }
            int r4 = r0.length()     // Catch:{ all -> 0x004d }
            if (r4 == 0) goto L_0x0050
            java.lang.String r0 = r3.concat(r0)     // Catch:{ all -> 0x004d }
        L_0x0048:
            android.util.Log.w(r1, r0)     // Catch:{ all -> 0x004d }
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
            goto L_0x0029
        L_0x004d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
            throw r0
        L_0x0050:
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x004d }
            r0.<init>(r3)     // Catch:{ all -> 0x004d }
            goto L_0x0048
        L_0x0056:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x004d }
            r1.<init>(r7)     // Catch:{ all -> 0x004d }
            r0.m13721((java.lang.Exception) r1)     // Catch:{ all -> 0x004d }
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.zzv.m13862(java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m13863(Bundle bundle) throws IOException {
        if (this.f11222.m13853() < 12000000) {
            return m13856(bundle);
        }
        try {
            return (Bundle) Tasks.m13727(zzi.m13828(this.f11223).m13829(1, bundle));
        } catch (InterruptedException | ExecutionException e) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e);
                Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 22).append("Error making request: ").append(valueOf).toString());
            }
            if (!(e.getCause() instanceof zzs) || ((zzs) e.getCause()).getErrorCode() != 4) {
                return null;
            }
            return m13856(bundle);
        }
    }
}
