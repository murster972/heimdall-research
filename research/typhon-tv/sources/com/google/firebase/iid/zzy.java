package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.zzv;
import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

final class zzy {

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f11231;

    /* renamed from: 龘  reason: contains not printable characters */
    private SharedPreferences f11232;

    public zzy(Context context) {
        this(context, "com.google.android.gms.appid");
    }

    private zzy(Context context, String str) {
        this.f11231 = context;
        this.f11232 = context.getSharedPreferences(str, 0);
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf("-no-backup");
        File file = new File(zzv.m9277(this.f11231), valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !m13871()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    m13875();
                    FirebaseInstanceId.m13786().m13790();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf3 = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf3.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf3) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final void m13869(String str) {
        SharedPreferences.Editor edit = this.f11232.edit();
        for (String next : this.f11232.getAll().keySet()) {
            if (next.startsWith(str)) {
                edit.remove(next);
            }
        }
        edit.commit();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m13870(String str, String str2, String str3) {
        return new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf("|T|").length() + String.valueOf(str2).length() + String.valueOf(str3).length()).append(str).append("|T|").append(str2).append("|").append(str3).toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final synchronized boolean m13871() {
        return this.f11232.getAll().isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m13872(String str, String str2) {
        return new StringBuilder(String.valueOf(str).length() + String.valueOf("|S|").length() + String.valueOf(str2).length()).append(str).append("|S|").append(str2).toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final synchronized KeyPair m13873(String str) {
        KeyPair keyPair;
        String string = this.f11232.getString(m13872(str, "|P|"), (String) null);
        String string2 = this.f11232.getString(m13872(str, "|K|"), (String) null);
        if (string == null || string2 == null) {
            keyPair = null;
        } else {
            try {
                byte[] decode = Base64.decode(string, 8);
                byte[] decode2 = Base64.decode(string2, 8);
                KeyFactory instance = KeyFactory.getInstance("RSA");
                keyPair = new KeyPair(instance.generatePublic(new X509EncodedKeySpec(decode)), instance.generatePrivate(new PKCS8EncodedKeySpec(decode2)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 19).append("Invalid key stored ").append(valueOf).toString());
                FirebaseInstanceId.m13786().m13790();
                keyPair = null;
            }
        }
        return keyPair;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final synchronized void m13874(String str) {
        m13869(String.valueOf(str).concat("|T|"));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m13875() {
        this.f11232.edit().clear().commit();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized boolean m13876(String str) {
        boolean z;
        String string = this.f11232.getString("topic_operaion_queue", "");
        String valueOf = String.valueOf(",");
        String valueOf2 = String.valueOf(str);
        if (string.startsWith(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf))) {
            String valueOf3 = String.valueOf(",");
            String valueOf4 = String.valueOf(str);
            this.f11232.edit().putString("topic_operaion_queue", string.substring((valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3)).length())).apply();
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final synchronized void m13877(String str) {
        m13869(String.valueOf(str).concat("|"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final synchronized KeyPair m13878(String str) {
        KeyPair r0;
        r0 = zza.m13807();
        long currentTimeMillis = System.currentTimeMillis();
        SharedPreferences.Editor edit = this.f11232.edit();
        edit.putString(m13872(str, "|P|"), Base64.encodeToString(r0.getPublic().getEncoded(), 11));
        edit.putString(m13872(str, "|K|"), Base64.encodeToString(r0.getPrivate().getEncoded(), 11));
        edit.putString(m13872(str, "cre"), Long.toString(currentTimeMillis));
        edit.commit();
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized zzz m13879(String str, String str2, String str3) {
        return zzz.m13883(this.f11232.getString(m13870(str, str2, str3), (String) null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized String m13880() {
        String str = null;
        synchronized (this) {
            String string = this.f11232.getString("topic_operaion_queue", (String) null);
            if (string != null) {
                String[] split = string.split(",");
                if (split.length > 1 && !TextUtils.isEmpty(split[1])) {
                    str = split[1];
                }
            }
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13881(String str) {
        String string = this.f11232.getString("topic_operaion_queue", "");
        this.f11232.edit().putString("topic_operaion_queue", new StringBuilder(String.valueOf(string).length() + String.valueOf(",").length() + String.valueOf(str).length()).append(string).append(",").append(str).toString()).apply();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13882(String str, String str2, String str3, String str4, String str5) {
        String r0 = zzz.m13884(str4, str5, System.currentTimeMillis());
        if (r0 != null) {
            SharedPreferences.Editor edit = this.f11232.edit();
            edit.putString(m13870(str, str2, str3), r0);
            edit.commit();
        }
    }
}
