package com.google.firebase.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.iid.MessengerCompat;

final class zzp {

    /* renamed from: 靐  reason: contains not printable characters */
    private final MessengerCompat f11206;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Messenger f11207;

    zzp(IBinder iBinder) throws RemoteException {
        String interfaceDescriptor = iBinder.getInterfaceDescriptor();
        if ("android.os.IMessenger".equals(interfaceDescriptor)) {
            this.f11207 = new Messenger(iBinder);
            this.f11206 = null;
        } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
            this.f11206 = new MessengerCompat(iBinder);
            this.f11207 = null;
        } else {
            String valueOf = String.valueOf(interfaceDescriptor);
            Log.w("MessengerIpcClient", valueOf.length() != 0 ? "Invalid interface descriptor: ".concat(valueOf) : new String("Invalid interface descriptor: "));
            throw new RemoteException();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13839(Message message) throws RemoteException {
        if (this.f11207 != null) {
            this.f11207.send(message);
        } else if (this.f11206 != null) {
            this.f11206.m9421(message);
        } else {
            throw new IllegalStateException("Both messengers are null");
        }
    }
}
