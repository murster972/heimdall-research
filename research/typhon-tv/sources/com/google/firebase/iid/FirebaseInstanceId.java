package com.google.firebase.iid;

import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Keep;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import java.io.IOException;
import java.security.KeyPair;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.oltu.oauth2.common.OAuth;

public class FirebaseInstanceId {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Map<String, FirebaseInstanceId> f11150 = new ArrayMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private static ScheduledThreadPoolExecutor f11151;

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzy f11152;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final long f11153 = TimeUnit.HOURS.toSeconds(8);

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzu f11154;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzv f11155;

    /* renamed from: ʽ  reason: contains not printable characters */
    private KeyPair f11156;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f11157 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private final FirebaseApp f11158;

    private FirebaseInstanceId(FirebaseApp firebaseApp) {
        this.f11158 = firebaseApp;
        if (zzu.m13850(firebaseApp) == null) {
            throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
        }
        this.f11154 = new zzu(firebaseApp.m13775());
        this.f11155 = new zzv(firebaseApp.m13775(), this.f11154);
        zzz r0 = m13792();
        if (r0 == null || r0.m13885(this.f11154.m13852()) || f11152.m13880() != null) {
            m13785();
        }
    }

    @Keep
    public static synchronized FirebaseInstanceId getInstance(FirebaseApp firebaseApp) {
        FirebaseInstanceId firebaseInstanceId;
        synchronized (FirebaseInstanceId.class) {
            firebaseInstanceId = f11150.get(firebaseApp.m13774().m13781());
            if (firebaseInstanceId == null) {
                if (f11152 == null) {
                    f11152 = new zzy(firebaseApp.m13775());
                }
                firebaseInstanceId = new FirebaseInstanceId(firebaseApp);
                f11150.put(firebaseApp.m13774().m13781(), firebaseInstanceId);
            }
        }
        return firebaseInstanceId;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static zzy m13782() {
        return f11152;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static boolean m13783() {
        return Log.isLoggable("FirebaseInstanceId", 3) || (Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private final void m13784() {
        f11152.m13877("");
        this.f11156 = null;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final synchronized void m13785() {
        if (!this.f11157) {
            m13799(0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FirebaseInstanceId m13786() {
        return getInstance(FirebaseApp.m13765());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final String m13787(String str, String str2, Bundle bundle) throws IOException {
        bundle.putString(OAuth.OAUTH_SCOPE, str2);
        bundle.putString("sender", str);
        bundle.putString("subtype", str);
        bundle.putString("appid", m13796());
        bundle.putString("gmp_app_id", this.f11158.m13774().m13781());
        bundle.putString("gmsv", Integer.toString(this.f11154.m13853()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.f11154.m13852());
        bundle.putString("app_ver_name", this.f11154.m13854());
        bundle.putString("cliv", "fiid-11910000");
        Bundle r1 = this.f11155.m13863(bundle);
        if (r1 == null) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
        String string = r1.getString("registration_id");
        if (string == null && (string = r1.getString("unregistered")) == null) {
            String string2 = r1.getString("error");
            if (string2 != null) {
                throw new IOException(string2);
            }
            String valueOf = String.valueOf(r1);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 20).append("Unexpected response ").append(valueOf).toString(), new Throwable());
            throw new IOException("SERVICE_NOT_AVAILABLE");
        } else if (!"RST".equals(string) && !string.startsWith("RST|")) {
            return string;
        } else {
            m13790();
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m13788(Runnable runnable, long j) {
        synchronized (FirebaseInstanceId.class) {
            if (f11151 == null) {
                f11151 = new ScheduledThreadPoolExecutor(1);
            }
            f11151.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m13789() throws IOException {
        return m13798(zzu.m13850(this.f11158), "*");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m13790() {
        f11152.m13875();
        m13784();
        m13785();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m13791() {
        f11152.m13874("");
        m13785();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final zzz m13792() {
        return f11152.m13879("", zzu.m13850(this.f11158), "*");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final FirebaseApp m13793() {
        return this.f11158;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13794(String str) throws IOException {
        zzz r1 = m13792();
        if (r1 == null || r1.m13885(this.f11154.m13852())) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString("gcm.topic", valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        String str2 = r1.f11236;
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        m13787(str2, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m13795() {
        zzz r0 = m13792();
        if (r0 == null || r0.m13885(this.f11154.m13852())) {
            m13785();
        }
        if (r0 != null) {
            return r0.f11236;
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m13796() {
        if (this.f11156 == null) {
            this.f11156 = f11152.m13873("");
        }
        if (this.f11156 == null) {
            this.f11156 = f11152.m13878("");
        }
        return zzu.m13851(this.f11156);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13797(String str) throws IOException {
        zzz r1 = m13792();
        if (r1 == null || r1.m13885(this.f11154.m13852())) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString("gcm.topic", valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        bundle.putString("delete", PubnativeRequest.LEGACY_ZONE_ID);
        String str2 = r1.f11236;
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        m13787(str2, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m13798(String str, String str2) throws IOException {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        zzz r0 = f11152.m13879("", str, str2);
        if (r0 != null && !r0.m13885(this.f11154.m13852())) {
            return r0.f11236;
        }
        String r4 = m13787(str, str2, new Bundle());
        if (r4 == null) {
            return r4;
        }
        f11152.m13882("", str, str2, r4, this.f11154.m13852());
        return r4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13799(long j) {
        m13788((Runnable) new zzaa(this, this.f11154, Math.min(Math.max(30, j << 1), f11153)), j);
        this.f11157 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13800(String str) {
        f11152.m13881(str);
        m13785();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13801(boolean z) {
        this.f11157 = z;
    }
}
