package com.google.firebase.iid;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class zzw extends Handler {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzv f11225;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzw(zzv zzv, Looper looper) {
        super(looper);
        this.f11225 = zzv;
    }

    public final void handleMessage(Message message) {
        this.f11225.m13860(message);
    }
}
