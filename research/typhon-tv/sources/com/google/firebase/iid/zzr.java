package com.google.firebase.iid;

import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zzr<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final TaskCompletionSource<T> f11208 = new TaskCompletionSource<>();

    /* renamed from: 麤  reason: contains not printable characters */
    final Bundle f11209;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f11210;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f11211;

    zzr(int i, int i2, Bundle bundle) {
        this.f11211 = i;
        this.f11210 = i2;
        this.f11209 = bundle;
    }

    public String toString() {
        int i = this.f11210;
        int i2 = this.f11211;
        return new StringBuilder(55).append("Request { what=").append(i).append(" id=").append(i2).append(" oneWay=").append(m13845()).append("}").toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m13842(Bundle bundle);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13843(zzs zzs) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(zzs);
            Log.d("MessengerIpcClient", new StringBuilder(String.valueOf(valueOf).length() + 14 + String.valueOf(valueOf2).length()).append("Failing ").append(valueOf).append(" with ").append(valueOf2).toString());
        }
        this.f11208.m13721((Exception) zzs);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13844(T t) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(t);
            Log.d("MessengerIpcClient", new StringBuilder(String.valueOf(valueOf).length() + 16 + String.valueOf(valueOf2).length()).append("Finishing ").append(valueOf).append(" with ").append(valueOf2).toString());
        }
        this.f11208.m13722(t);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m13845();
}
