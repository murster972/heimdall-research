package com.google.firebase.iid;

import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

final class zzz {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final long f11233 = TimeUnit.DAYS.toMillis(7);

    /* renamed from: 麤  reason: contains not printable characters */
    private long f11234;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f11235;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f11236;

    private zzz(String str, String str2, long j) {
        this.f11236 = str;
        this.f11235 = str2;
        this.f11234 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzz m13883(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!str.startsWith("{")) {
            return new zzz(str, (String) null, 0);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new zzz(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 23).append("Failed to parse token: ").append(valueOf).toString());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m13884(String str, String str2, long j) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("token", (Object) str);
            jSONObject.put("appVersion", (Object) str2);
            jSONObject.put("timestamp", j);
            return jSONObject.toString();
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 24).append("Failed to encode token: ").append(valueOf).toString());
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13885(String str) {
        return System.currentTimeMillis() > this.f11234 + f11233 || !str.equals(this.f11235);
    }
}
