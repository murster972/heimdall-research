package com.google.firebase.iid;

import android.os.Bundle;

final class zzq extends zzr<Void> {
    zzq(int i, int i2, Bundle bundle) {
        super(i, 2, bundle);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13840(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            m13844(null);
        } else {
            m13843(new zzs(4, "Invalid response to one way request"));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13841() {
        return true;
    }
}
