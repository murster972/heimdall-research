package com.google.firebase.iid;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class zzb extends Service {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11167 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private Binder f11168;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f11169;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Object f11170 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    final ExecutorService f11171 = Executors.newSingleThreadExecutor();

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13814(Intent intent) {
        if (intent != null) {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
        synchronized (this.f11170) {
            this.f11167--;
            if (this.f11167 == 0) {
                stopSelfResult(this.f11169);
            }
        }
    }

    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.f11168 == null) {
            this.f11168 = new zzf(this);
        }
        return this.f11168;
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.f11170) {
            this.f11169 = i2;
            this.f11167++;
        }
        Intent r1 = m13818(intent);
        if (r1 == null) {
            m13814(intent);
            return 2;
        } else if (m13817(r1)) {
            m13814(intent);
            return 2;
        } else {
            this.f11171.execute(new zzc(this, r1, intent));
            return 3;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m13816(Intent intent);

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m13817(Intent intent) {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m13818(Intent intent) {
        return intent;
    }
}
