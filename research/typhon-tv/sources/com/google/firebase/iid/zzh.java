package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public final class zzh implements ServiceConnection {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f11184;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzf f11185;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Intent f11186;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Queue<zzd> f11187;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ScheduledExecutorService f11188;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f11189;

    public zzh(Context context, String str) {
        this(context, str, new ScheduledThreadPoolExecutor(0));
    }

    private zzh(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
        this.f11187 = new ArrayDeque();
        this.f11184 = false;
        this.f11189 = context.getApplicationContext();
        this.f11186 = new Intent(str).setPackage(this.f11189.getPackageName());
        this.f11188 = scheduledExecutorService;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0068, code lost:
        if (android.util.Log.isLoggable("EnhancedIntentService", 3) == false) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006f, code lost:
        if (r4.f11184 != false) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0071, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0072, code lost:
        android.util.Log.d("EnhancedIntentService", new java.lang.StringBuilder(39).append("binder is dead. start connection? ").append(r0).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008d, code lost:
        if (r4.f11184 != false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008f, code lost:
        r4.f11184 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a0, code lost:
        if (com.google.android.gms.common.stats.zza.m9235().m9236(r4.f11189, r4.f11186, r4, 65) == false) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a4, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        android.util.Log.e("EnhancedIntentService", "binding to the service failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c4, code lost:
        android.util.Log.e("EnhancedIntentService", "Exception while binding the service", r0);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final synchronized void m13822() {
        /*
            r4 = this;
            r1 = 1
            monitor-enter(r4)
            java.lang.String r0 = "EnhancedIntentService"
            r2 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0015
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "flush queue called"
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x005d }
        L_0x0015:
            java.util.Queue<com.google.firebase.iid.zzd> r0 = r4.f11187     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x00a2
            java.lang.String r0 = "EnhancedIntentService"
            r2 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "found intent to be delivered"
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x005d }
        L_0x0030:
            com.google.firebase.iid.zzf r0 = r4.f11185     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0060
            com.google.firebase.iid.zzf r0 = r4.f11185     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isBinderAlive()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0060
            java.lang.String r0 = "EnhancedIntentService"
            r2 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "binder is alive, sending the intent."
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x005d }
        L_0x004f:
            java.util.Queue<com.google.firebase.iid.zzd> r0 = r4.f11187     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x005d }
            com.google.firebase.iid.zzd r0 = (com.google.firebase.iid.zzd) r0     // Catch:{ all -> 0x005d }
            com.google.firebase.iid.zzf r2 = r4.f11185     // Catch:{ all -> 0x005d }
            r2.m13821((com.google.firebase.iid.zzd) r0)     // Catch:{ all -> 0x005d }
            goto L_0x0015
        L_0x005d:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0060:
            java.lang.String r0 = "EnhancedIntentService"
            r2 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x008b
            java.lang.String r2 = "EnhancedIntentService"
            boolean r0 = r4.f11184     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x00a4
            r0 = r1
        L_0x0072:
            r1 = 39
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            r3.<init>(r1)     // Catch:{ all -> 0x005d }
            java.lang.String r1 = "binder is dead. start connection? "
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x005d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005d }
            android.util.Log.d(r2, r0)     // Catch:{ all -> 0x005d }
        L_0x008b:
            boolean r0 = r4.f11184     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x00a2
            r0 = 1
            r4.f11184 = r0     // Catch:{ all -> 0x005d }
            com.google.android.gms.common.stats.zza r0 = com.google.android.gms.common.stats.zza.m9235()     // Catch:{ SecurityException -> 0x00c3 }
            android.content.Context r1 = r4.f11189     // Catch:{ SecurityException -> 0x00c3 }
            android.content.Intent r2 = r4.f11186     // Catch:{ SecurityException -> 0x00c3 }
            r3 = 65
            boolean r0 = r0.m9236(r1, r2, r4, r3)     // Catch:{ SecurityException -> 0x00c3 }
            if (r0 == 0) goto L_0x00a6
        L_0x00a2:
            monitor-exit(r4)
            return
        L_0x00a4:
            r0 = 0
            goto L_0x0072
        L_0x00a6:
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r1 = "binding to the service failed"
            android.util.Log.e(r0, r1)     // Catch:{ SecurityException -> 0x00c3 }
        L_0x00af:
            java.util.Queue<com.google.firebase.iid.zzd> r0 = r4.f11187     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x00a2
            java.util.Queue<com.google.firebase.iid.zzd> r0 = r4.f11187     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x005d }
            com.google.firebase.iid.zzd r0 = (com.google.firebase.iid.zzd) r0     // Catch:{ all -> 0x005d }
            r0.m13819()     // Catch:{ all -> 0x005d }
            goto L_0x00af
        L_0x00c3:
            r0 = move-exception
            java.lang.String r1 = "EnhancedIntentService"
            java.lang.String r2 = "Exception while binding the service"
            android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x005d }
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.zzh.m13822():void");
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this) {
            this.f11184 = false;
            this.f11185 = (zzf) iBinder;
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                String valueOf = String.valueOf(componentName);
                Log.d("EnhancedIntentService", new StringBuilder(String.valueOf(valueOf).length() + 20).append("onServiceConnected: ").append(valueOf).toString());
            }
            m13822();
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            String valueOf = String.valueOf(componentName);
            Log.d("EnhancedIntentService", new StringBuilder(String.valueOf(valueOf).length() + 23).append("onServiceDisconnected: ").append(valueOf).toString());
        }
        m13822();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13823(Intent intent, BroadcastReceiver.PendingResult pendingResult) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "new intent queued in the bind-strategy delivery");
        }
        this.f11187.add(new zzd(intent, pendingResult, this.f11188));
        m13822();
    }
}
