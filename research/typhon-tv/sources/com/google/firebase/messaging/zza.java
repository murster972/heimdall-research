package com.google.firebase.messaging;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.R;
import com.google.android.gms.common.util.zzq;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.MissingFormatArgumentException;
import java.util.concurrent.atomic.AtomicInteger;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONArray;
import org.json.JSONException;

final class zza {

    /* renamed from: 龘  reason: contains not printable characters */
    private static zza f11241;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final AtomicInteger f11242 = new AtomicInteger((int) SystemClock.elapsedRealtime());

    /* renamed from: 连任  reason: contains not printable characters */
    private Method f11243;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f11244;

    /* renamed from: 麤  reason: contains not printable characters */
    private Method f11245;

    /* renamed from: 齉  reason: contains not printable characters */
    private Bundle f11246;

    private zza(Context context) {
        this.f11244 = context.getApplicationContext();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final PendingIntent m13898(Bundle bundle) {
        Intent intent;
        String r1 = m13909(bundle, "gcm.n.click_action");
        if (!TextUtils.isEmpty(r1)) {
            Intent intent2 = new Intent(r1);
            intent2.setPackage(this.f11244.getPackageName());
            intent2.setFlags(268435456);
            intent = intent2;
        } else {
            Uri r12 = m13899(bundle);
            if (r12 != null) {
                Intent intent3 = new Intent("android.intent.action.VIEW");
                intent3.setPackage(this.f11244.getPackageName());
                intent3.setData(r12);
                intent = intent3;
            } else {
                Intent launchIntentForPackage = this.f11244.getPackageManager().getLaunchIntentForPackage(this.f11244.getPackageName());
                if (launchIntentForPackage == null) {
                    Log.w("FirebaseMessaging", "No activity found to launch app");
                }
                intent = launchIntentForPackage;
            }
        }
        if (intent == null) {
            return null;
        }
        intent.addFlags(67108864);
        Bundle bundle2 = new Bundle(bundle);
        FirebaseMessagingService.m13889(bundle2);
        intent.putExtras(bundle2);
        for (String str : bundle2.keySet()) {
            if (str.startsWith("gcm.n.") || str.startsWith("gcm.notification.")) {
                intent.removeExtra(str);
            }
        }
        return PendingIntent.getActivity(this.f11244, this.f11242.incrementAndGet(), intent, 1073741824);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static Uri m13899(Bundle bundle) {
        String r0 = m13909(bundle, "gcm.n.link_android");
        if (TextUtils.isEmpty(r0)) {
            r0 = m13909(bundle, "gcm.n.link");
        }
        if (!TextUtils.isEmpty(r0)) {
            return Uri.parse(r0);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final Integer m13900(String str) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException e) {
                Log.w("FirebaseMessaging", new StringBuilder(String.valueOf(str).length() + 54).append("Color ").append(str).append(" not valid. Notification will use default color.").toString());
            }
        }
        int i = m13907().getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i == 0) {
            return null;
        }
        try {
            return Integer.valueOf(ContextCompat.getColor(this.f11244, i));
        } catch (Resources.NotFoundException e2) {
            Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static String m13901(Bundle bundle, String str) {
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf("_loc_key");
        return m13909(bundle, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static String m13902(Bundle bundle) {
        String r0 = m13909(bundle, "gcm.n.sound2");
        return TextUtils.isEmpty(r0) ? m13909(bundle, "gcm.n.sound") : r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final String m13903(Bundle bundle, String str) {
        String r0 = m13909(bundle, str);
        if (!TextUtils.isEmpty(r0)) {
            return r0;
        }
        String r2 = m13901(bundle, str);
        if (TextUtils.isEmpty(r2)) {
            return null;
        }
        Resources resources = this.f11244.getResources();
        int identifier = resources.getIdentifier(r2, "string", this.f11244.getPackageName());
        if (identifier == 0) {
            String valueOf = String.valueOf(str);
            String valueOf2 = String.valueOf("_loc_key");
            String substring = (valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).substring(6);
            Log.w("FirebaseMessaging", new StringBuilder(String.valueOf(substring).length() + 49 + String.valueOf(r2).length()).append(substring).append(" resource not found: ").append(r2).append(" Default value will be used.").toString());
            return null;
        }
        Object[] r4 = m13905(bundle, str);
        if (r4 == null) {
            return resources.getString(identifier);
        }
        try {
            return resources.getString(identifier, r4);
        } catch (MissingFormatArgumentException e) {
            String arrays = Arrays.toString(r4);
            Log.w("FirebaseMessaging", new StringBuilder(String.valueOf(r2).length() + 58 + String.valueOf(arrays).length()).append("Missing format argument for ").append(r2).append(": ").append(arrays).append(" Default value will be used.").toString(), e);
            return null;
        }
    }

    @TargetApi(26)
    /* renamed from: 齉  reason: contains not printable characters */
    private final String m13904(String str) {
        if (!zzq.m9267()) {
            return null;
        }
        NotificationManager notificationManager = (NotificationManager) this.f11244.getSystemService(NotificationManager.class);
        try {
            if (this.f11243 == null) {
                this.f11243 = notificationManager.getClass().getMethod("getNotificationChannel", new Class[]{String.class});
            }
            if (!TextUtils.isEmpty(str)) {
                if (this.f11243.invoke(notificationManager, new Object[]{str}) != null) {
                    return str;
                }
                Log.w("FirebaseMessaging", new StringBuilder(String.valueOf(str).length() + 122).append("Notification Channel requested (").append(str).append(") has not been created by the app. Manifest configuration, or default, value will be used.").toString());
            }
            String string = m13907().getString("com.google.firebase.messaging.default_notification_channel_id");
            if (!TextUtils.isEmpty(string)) {
                if (this.f11243.invoke(notificationManager, new Object[]{string}) != null) {
                    return string;
                }
                Log.w("FirebaseMessaging", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
            } else {
                Log.w("FirebaseMessaging", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
            }
            if (this.f11243.invoke(notificationManager, new Object[]{"fcm_fallback_notification_channel"}) == null) {
                Class<?> cls = Class.forName("android.app.NotificationChannel");
                Object newInstance = cls.getConstructor(new Class[]{String.class, CharSequence.class, Integer.TYPE}).newInstance(new Object[]{"fcm_fallback_notification_channel", this.f11244.getString(R.string.fcm_fallback_notification_channel_label), 3});
                notificationManager.getClass().getMethod("createNotificationChannel", new Class[]{cls}).invoke(notificationManager, new Object[]{newInstance});
            }
            return "fcm_fallback_notification_channel";
        } catch (InstantiationException e) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e);
            return null;
        } catch (InvocationTargetException e2) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e2);
            return null;
        } catch (NoSuchMethodException e3) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e3);
            return null;
        } catch (IllegalAccessException e4) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e4);
            return null;
        } catch (ClassNotFoundException e5) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e5);
            return null;
        } catch (SecurityException e6) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e6);
            return null;
        } catch (IllegalArgumentException e7) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e7);
            return null;
        } catch (LinkageError e8) {
            Log.e("FirebaseMessaging", "Error while setting the notification channel", e8);
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static Object[] m13905(Bundle bundle, String str) {
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf("_loc_args");
        String r3 = m13909(bundle, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        if (TextUtils.isEmpty(r3)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(r3);
            Object[] objArr = new String[jSONArray.length()];
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = jSONArray.opt(i);
            }
            return objArr;
        } catch (JSONException e) {
            String valueOf3 = String.valueOf(str);
            String valueOf4 = String.valueOf("_loc_args");
            String substring = (valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3)).substring(6);
            Log.w("FirebaseMessaging", new StringBuilder(String.valueOf(substring).length() + 41 + String.valueOf(r3).length()).append("Malformed ").append(substring).append(": ").append(r3).append("  Default value will be used.").toString());
            return null;
        }
    }

    @TargetApi(26)
    /* renamed from: 龘  reason: contains not printable characters */
    private final Notification m13906(CharSequence charSequence, String str, int i, Integer num, Uri uri, PendingIntent pendingIntent, PendingIntent pendingIntent2, String str2) {
        Notification.Builder smallIcon = new Notification.Builder(this.f11244).setAutoCancel(true).setSmallIcon(i);
        if (!TextUtils.isEmpty(charSequence)) {
            smallIcon.setContentTitle(charSequence);
        }
        if (!TextUtils.isEmpty(str)) {
            smallIcon.setContentText(str);
            smallIcon.setStyle(new Notification.BigTextStyle().bigText(str));
        }
        if (num != null) {
            smallIcon.setColor(num.intValue());
        }
        if (uri != null) {
            smallIcon.setSound(uri);
        }
        if (pendingIntent != null) {
            smallIcon.setContentIntent(pendingIntent);
        }
        if (pendingIntent2 != null) {
            smallIcon.setDeleteIntent(pendingIntent2);
        }
        if (str2 != null) {
            if (this.f11245 == null) {
                this.f11245 = m13910("setChannelId");
            }
            if (this.f11245 == null) {
                this.f11245 = m13910("setChannel");
            }
            if (this.f11245 == null) {
                Log.e("FirebaseMessaging", "Error while setting the notification channel");
            } else {
                try {
                    this.f11245.invoke(smallIcon, new Object[]{str2});
                } catch (IllegalAccessException e) {
                    Log.e("FirebaseMessaging", "Error while setting the notification channel", e);
                } catch (InvocationTargetException e2) {
                    Log.e("FirebaseMessaging", "Error while setting the notification channel", e2);
                } catch (SecurityException e3) {
                    Log.e("FirebaseMessaging", "Error while setting the notification channel", e3);
                } catch (IllegalArgumentException e4) {
                    Log.e("FirebaseMessaging", "Error while setting the notification channel", e4);
                }
            }
        }
        return smallIcon.build();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Bundle m13907() {
        if (this.f11246 != null) {
            return this.f11246;
        }
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = this.f11244.getPackageManager().getApplicationInfo(this.f11244.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (applicationInfo == null || applicationInfo.metaData == null) {
            return Bundle.EMPTY;
        }
        this.f11246 = applicationInfo.metaData;
        return this.f11246;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static synchronized zza m13908(Context context) {
        zza zza;
        synchronized (zza.class) {
            if (f11241 == null) {
                f11241 = new zza(context);
            }
            zza = f11241;
        }
        return zza;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m13909(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    @TargetApi(26)
    /* renamed from: 龘  reason: contains not printable characters */
    private static Method m13910(String str) {
        try {
            return Notification.Builder.class.getMethod(str, new Class[]{String.class});
        } catch (NoSuchMethodException | SecurityException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m13911(Intent intent, Bundle bundle) {
        for (String str : bundle.keySet()) {
            if (str.startsWith("google.c.a.") || str.equals("from")) {
                intent.putExtra(str, bundle.getString(str));
            }
        }
    }

    @TargetApi(26)
    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m13912(int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            Drawable drawable = this.f11244.getResources().getDrawable(i, (Resources.Theme) null);
            if (drawable.getBounds().height() != 0 && drawable.getBounds().width() != 0) {
                return true;
            }
            Log.e("FirebaseMessaging", new StringBuilder(72).append("Icon with id: ").append(i).append(" uses an invalid gradient. Using fallback icon.").toString());
            return false;
        } catch (Resources.NotFoundException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m13913(Bundle bundle) {
        return PubnativeRequest.LEGACY_ZONE_ID.equals(m13909(bundle, "gcm.n.e")) || m13909(bundle, "gcm.n.icon") != null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x024e  */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m13914(android.os.Bundle r14) {
        /*
            r13 = this;
            r7 = 0
            r12 = 1073741824(0x40000000, float:2.0)
            r9 = 1
            r10 = 0
            java.lang.String r0 = "1"
            java.lang.String r1 = "gcm.n.noui"
            java.lang.String r1 = m13909((android.os.Bundle) r14, (java.lang.String) r1)
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0017
            r0 = r9
        L_0x0016:
            return r0
        L_0x0017:
            android.content.Context r0 = r13.f11244
            java.lang.String r1 = "keyguard"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.app.KeyguardManager r0 = (android.app.KeyguardManager) r0
            boolean r0 = r0.inKeyguardRestrictedInputMode()
            if (r0 != 0) goto L_0x0069
            boolean r0 = com.google.android.gms.common.util.zzq.m9265()
            if (r0 != 0) goto L_0x0033
            r0 = 10
            android.os.SystemClock.sleep(r0)
        L_0x0033:
            int r1 = android.os.Process.myPid()
            android.content.Context r0 = r13.f11244
            java.lang.String r2 = "activity"
            java.lang.Object r0 = r0.getSystemService(r2)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            java.util.List r0 = r0.getRunningAppProcesses()
            if (r0 == 0) goto L_0x0069
            java.util.Iterator r2 = r0.iterator()
        L_0x004c:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r2.next()
            android.app.ActivityManager$RunningAppProcessInfo r0 = (android.app.ActivityManager.RunningAppProcessInfo) r0
            int r3 = r0.pid
            if (r3 != r1) goto L_0x004c
            int r0 = r0.importance
            r1 = 100
            if (r0 != r1) goto L_0x0067
            r0 = r9
        L_0x0063:
            if (r0 == 0) goto L_0x006b
            r0 = r10
            goto L_0x0016
        L_0x0067:
            r0 = r10
            goto L_0x0063
        L_0x0069:
            r0 = r10
            goto L_0x0063
        L_0x006b:
            java.lang.String r0 = "gcm.n.title"
            java.lang.String r1 = r13.m13903(r14, r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 == 0) goto L_0x0088
            android.content.Context r0 = r13.f11244
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()
            android.content.Context r1 = r13.f11244
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            java.lang.CharSequence r1 = r0.loadLabel(r1)
        L_0x0088:
            java.lang.String r0 = "gcm.n.body"
            java.lang.String r2 = r13.m13903(r14, r0)
            java.lang.String r0 = "gcm.n.icon"
            java.lang.String r0 = m13909((android.os.Bundle) r14, (java.lang.String) r0)
            boolean r3 = android.text.TextUtils.isEmpty(r0)
            if (r3 != 0) goto L_0x01b9
            android.content.Context r3 = r13.f11244
            android.content.res.Resources r4 = r3.getResources()
            java.lang.String r3 = "drawable"
            android.content.Context r5 = r13.f11244
            java.lang.String r5 = r5.getPackageName()
            int r3 = r4.getIdentifier(r0, r3, r5)
            if (r3 == 0) goto L_0x0179
            boolean r5 = r13.m13912((int) r3)
            if (r5 == 0) goto L_0x0179
        L_0x00b7:
            java.lang.String r0 = "gcm.n.color"
            java.lang.String r0 = m13909((android.os.Bundle) r14, (java.lang.String) r0)
            java.lang.Integer r4 = r13.m13900((java.lang.String) r0)
            java.lang.String r0 = m13902(r14)
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 == 0) goto L_0x01e2
            r5 = r7
        L_0x00cd:
            android.app.PendingIntent r6 = r13.m13898(r14)
            boolean r0 = com.google.firebase.messaging.FirebaseMessagingService.m13888((android.os.Bundle) r14)
            if (r0 == 0) goto L_0x010b
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r7 = "com.google.firebase.messaging.NOTIFICATION_OPEN"
            r0.<init>(r7)
            m13911((android.content.Intent) r0, (android.os.Bundle) r14)
            java.lang.String r7 = "pending_intent"
            r0.putExtra(r7, r6)
            android.content.Context r6 = r13.f11244
            java.util.concurrent.atomic.AtomicInteger r7 = r13.f11242
            int r7 = r7.incrementAndGet()
            android.app.PendingIntent r6 = com.google.firebase.iid.zzx.m13865(r6, r7, r0, r12)
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r7 = "com.google.firebase.messaging.NOTIFICATION_DISMISS"
            r0.<init>(r7)
            m13911((android.content.Intent) r0, (android.os.Bundle) r14)
            android.content.Context r7 = r13.f11244
            java.util.concurrent.atomic.AtomicInteger r8 = r13.f11242
            int r8 = r8.incrementAndGet()
            android.app.PendingIntent r7 = com.google.firebase.iid.zzx.m13865(r7, r8, r0, r12)
        L_0x010b:
            boolean r0 = com.google.android.gms.common.util.zzq.m9267()
            if (r0 == 0) goto L_0x024e
            android.content.Context r0 = r13.f11244
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()
            int r0 = r0.targetSdkVersion
            r8 = 25
            if (r0 <= r8) goto L_0x024e
            java.lang.String r0 = "gcm.n.android_channel_id"
            java.lang.String r0 = m13909((android.os.Bundle) r14, (java.lang.String) r0)
            java.lang.String r8 = r13.m13904((java.lang.String) r0)
            r0 = r13
            android.app.Notification r0 = r0.m13906(r1, r2, r3, r4, r5, r6, r7, r8)
            r1 = r0
        L_0x012e:
            java.lang.String r0 = "gcm.n.tag"
            java.lang.String r2 = m13909((android.os.Bundle) r14, (java.lang.String) r0)
            java.lang.String r0 = "FirebaseMessaging"
            r3 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r3)
            if (r0 == 0) goto L_0x0148
            java.lang.String r0 = "FirebaseMessaging"
            java.lang.String r3 = "Showing notification"
            android.util.Log.d(r0, r3)
        L_0x0148:
            android.content.Context r0 = r13.f11244
            java.lang.String r3 = "notification"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 == 0) goto L_0x0173
            long r2 = android.os.SystemClock.uptimeMillis()
            r4 = 37
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "FCM-Notification:"
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
        L_0x0173:
            r0.notify(r2, r10, r1)
            r0 = r9
            goto L_0x0016
        L_0x0179:
            java.lang.String r3 = "mipmap"
            android.content.Context r5 = r13.f11244
            java.lang.String r5 = r5.getPackageName()
            int r3 = r4.getIdentifier(r0, r3, r5)
            if (r3 == 0) goto L_0x018e
            boolean r4 = r13.m13912((int) r3)
            if (r4 != 0) goto L_0x00b7
        L_0x018e:
            java.lang.String r3 = "FirebaseMessaging"
            java.lang.String r4 = java.lang.String.valueOf(r0)
            int r4 = r4.length()
            int r4 = r4 + 61
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "Icon resource "
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = " not found. Notification will use default icon."
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            android.util.Log.w(r3, r0)
        L_0x01b9:
            android.os.Bundle r0 = r13.m13907()
            java.lang.String r3 = "com.google.firebase.messaging.default_notification_icon"
            int r0 = r0.getInt(r3, r10)
            if (r0 == 0) goto L_0x01cc
            boolean r3 = r13.m13912((int) r0)
            if (r3 != 0) goto L_0x01d4
        L_0x01cc:
            android.content.Context r0 = r13.f11244
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()
            int r0 = r0.icon
        L_0x01d4:
            if (r0 == 0) goto L_0x01dc
            boolean r3 = r13.m13912((int) r0)
            if (r3 != 0) goto L_0x01df
        L_0x01dc:
            r0 = 17301651(0x1080093, float:2.4979667E-38)
        L_0x01df:
            r3 = r0
            goto L_0x00b7
        L_0x01e2:
            java.lang.String r5 = "default"
            boolean r5 = r5.equals(r0)
            if (r5 != 0) goto L_0x0247
            android.content.Context r5 = r13.f11244
            android.content.res.Resources r5 = r5.getResources()
            java.lang.String r6 = "raw"
            android.content.Context r8 = r13.f11244
            java.lang.String r8 = r8.getPackageName()
            int r5 = r5.getIdentifier(r0, r6, r8)
            if (r5 == 0) goto L_0x0247
            java.lang.String r5 = "android.resource://"
            android.content.Context r6 = r13.f11244
            java.lang.String r6 = r6.getPackageName()
            java.lang.String r8 = java.lang.String.valueOf(r5)
            int r8 = r8.length()
            int r8 = r8 + 5
            java.lang.String r11 = java.lang.String.valueOf(r6)
            int r11 = r11.length()
            int r8 = r8 + r11
            java.lang.String r11 = java.lang.String.valueOf(r0)
            int r11 = r11.length()
            int r8 = r8 + r11
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>(r8)
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "/raw/"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            android.net.Uri r5 = android.net.Uri.parse(r0)
            goto L_0x00cd
        L_0x0247:
            r0 = 2
            android.net.Uri r5 = android.media.RingtoneManager.getDefaultUri(r0)
            goto L_0x00cd
        L_0x024e:
            android.support.v4.app.NotificationCompat$Builder r0 = new android.support.v4.app.NotificationCompat$Builder
            android.content.Context r8 = r13.f11244
            r0.<init>(r8)
            android.support.v4.app.NotificationCompat$Builder r0 = r0.setAutoCancel(r9)
            android.support.v4.app.NotificationCompat$Builder r0 = r0.setSmallIcon(r3)
            boolean r3 = android.text.TextUtils.isEmpty(r1)
            if (r3 != 0) goto L_0x0266
            r0.setContentTitle(r1)
        L_0x0266:
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 != 0) goto L_0x027b
            r0.setContentText(r2)
            android.support.v4.app.NotificationCompat$BigTextStyle r1 = new android.support.v4.app.NotificationCompat$BigTextStyle
            r1.<init>()
            android.support.v4.app.NotificationCompat$BigTextStyle r1 = r1.bigText(r2)
            r0.setStyle(r1)
        L_0x027b:
            if (r4 == 0) goto L_0x0284
            int r1 = r4.intValue()
            r0.setColor(r1)
        L_0x0284:
            if (r5 == 0) goto L_0x0289
            r0.setSound(r5)
        L_0x0289:
            if (r6 == 0) goto L_0x028e
            r0.setContentIntent(r6)
        L_0x028e:
            if (r7 == 0) goto L_0x0293
            r0.setDeleteIntent(r7)
        L_0x0293:
            android.app.Notification r0 = r0.build()
            r1 = r0
            goto L_0x012e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.messaging.zza.m13914(android.os.Bundle):boolean");
    }
}
