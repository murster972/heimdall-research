package com.google.firebase.messaging;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Map;

public final class RemoteMessage extends zzbfm {
    public static final Parcelable.Creator<RemoteMessage> CREATOR = new zzf();

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, String> f11239;

    /* renamed from: 龘  reason: contains not printable characters */
    Bundle f11240;

    RemoteMessage(Bundle bundle) {
        this.f11240 = bundle;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10187(parcel, 2, this.f11240, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<String, String> m13897() {
        if (this.f11239 == null) {
            this.f11239 = new ArrayMap();
            for (String str : this.f11240.keySet()) {
                Object obj = this.f11240.get(str);
                if (obj instanceof String) {
                    String str2 = (String) obj;
                    if (!str.startsWith("google.") && !str.startsWith("gcm.") && !str.equals("from") && !str.equals("message_type") && !str.equals("collapse_key")) {
                        this.f11239.put(str, str2);
                    }
                }
            }
        }
        return this.f11239;
    }
}
