package com.google.firebase.messaging;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.firebase.iid.zzb;
import com.google.firebase.iid.zzx;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import net.pubnative.library.request.PubnativeRequest;

public class FirebaseMessagingService extends zzb {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Queue<String> f11238 = new ArrayDeque(10);

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m13888(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        return PubnativeRequest.LEGACY_ZONE_ID.equals(bundle.getString("google.c.a.e"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m13889(Bundle bundle) {
        Iterator it2 = bundle.keySet().iterator();
        while (it2.hasNext()) {
            String str = (String) it2.next();
            if (str != null && str.startsWith("google.c.")) {
                it2.remove();
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f0, code lost:
        if (r0.equals("gcm") != false) goto L_0x006f;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m13890(android.content.Intent r11) {
        /*
            r10 = this;
            r5 = 3
            r4 = 2
            r2 = -1
            r3 = 1
            r1 = 0
            java.lang.String r0 = r11.getAction()
            if (r0 != 0) goto L_0x000e
            java.lang.String r0 = ""
        L_0x000e:
            int r6 = r0.hashCode()
            switch(r6) {
                case 75300319: goto L_0x0040;
                case 366519424: goto L_0x0035;
                default: goto L_0x0015;
            }
        L_0x0015:
            r0 = r2
        L_0x0016:
            switch(r0) {
                case 0: goto L_0x004b;
                case 1: goto L_0x0192;
                default: goto L_0x0019;
            }
        L_0x0019:
            java.lang.String r1 = "FirebaseMessaging"
            java.lang.String r2 = "Unknown intent action: "
            java.lang.String r0 = r11.getAction()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r3 = r0.length()
            if (r3 == 0) goto L_0x01a1
            java.lang.String r0 = r2.concat(r0)
        L_0x0031:
            android.util.Log.d(r1, r0)
        L_0x0034:
            return
        L_0x0035:
            java.lang.String r6 = "com.google.android.c2dm.intent.RECEIVE"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x0015
            r0 = r1
            goto L_0x0016
        L_0x0040:
            java.lang.String r6 = "com.google.firebase.messaging.NOTIFICATION_DISMISS"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x0015
            r0 = r3
            goto L_0x0016
        L_0x004b:
            java.lang.String r0 = "google.message_id"
            java.lang.String r6 = r11.getStringExtra(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x00a2
            r0 = r1
        L_0x0059:
            if (r0 != 0) goto L_0x0089
            java.lang.String r0 = "message_type"
            java.lang.String r0 = r11.getStringExtra(r0)
            if (r0 != 0) goto L_0x0067
            java.lang.String r0 = "gcm"
        L_0x0067:
            int r7 = r0.hashCode()
            switch(r7) {
                case -2062414158: goto L_0x00f4;
                case 102161: goto L_0x00e9;
                case 814694033: goto L_0x010c;
                case 814800675: goto L_0x0100;
                default: goto L_0x006e;
            }
        L_0x006e:
            r1 = r2
        L_0x006f:
            switch(r1) {
                case 0: goto L_0x0118;
                case 1: goto L_0x0159;
                case 2: goto L_0x015e;
                case 3: goto L_0x016a;
                default: goto L_0x0072;
            }
        L_0x0072:
            java.lang.String r1 = "FirebaseMessaging"
            java.lang.String r2 = "Received message with unknown type: "
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r3 = r0.length()
            if (r3 == 0) goto L_0x018b
            java.lang.String r0 = r2.concat(r0)
        L_0x0086:
            android.util.Log.w(r1, r0)
        L_0x0089:
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x0034
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            java.lang.String r1 = "google.message_id"
            r0.putString(r1, r6)
            com.google.firebase.iid.zzi r1 = com.google.firebase.iid.zzi.m13828((android.content.Context) r10)
            r1.m13830(r4, r0)
            goto L_0x0034
        L_0x00a2:
            java.util.Queue<java.lang.String> r0 = f11238
            boolean r0 = r0.contains(r6)
            if (r0 == 0) goto L_0x00d2
            java.lang.String r0 = "FirebaseMessaging"
            boolean r0 = android.util.Log.isLoggable(r0, r5)
            if (r0 == 0) goto L_0x00ca
            java.lang.String r7 = "FirebaseMessaging"
            java.lang.String r8 = "Received duplicate message: "
            java.lang.String r0 = java.lang.String.valueOf(r6)
            int r9 = r0.length()
            if (r9 == 0) goto L_0x00cc
            java.lang.String r0 = r8.concat(r0)
        L_0x00c7:
            android.util.Log.d(r7, r0)
        L_0x00ca:
            r0 = r3
            goto L_0x0059
        L_0x00cc:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r8)
            goto L_0x00c7
        L_0x00d2:
            java.util.Queue<java.lang.String> r0 = f11238
            int r0 = r0.size()
            r7 = 10
            if (r0 < r7) goto L_0x00e1
            java.util.Queue<java.lang.String> r0 = f11238
            r0.remove()
        L_0x00e1:
            java.util.Queue<java.lang.String> r0 = f11238
            r0.add(r6)
            r0 = r1
            goto L_0x0059
        L_0x00e9:
            java.lang.String r3 = "gcm"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x006e
            goto L_0x006f
        L_0x00f4:
            java.lang.String r1 = "deleted_messages"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x006e
            r1 = r3
            goto L_0x006f
        L_0x0100:
            java.lang.String r1 = "send_event"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x006e
            r1 = r4
            goto L_0x006f
        L_0x010c:
            java.lang.String r1 = "send_error"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x006e
            r1 = r5
            goto L_0x006f
        L_0x0118:
            android.os.Bundle r0 = r11.getExtras()
            boolean r0 = m13888((android.os.Bundle) r0)
            if (r0 == 0) goto L_0x0125
            com.google.firebase.messaging.zzd.m13936(r10, r11)
        L_0x0125:
            android.os.Bundle r0 = r11.getExtras()
            if (r0 != 0) goto L_0x0130
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
        L_0x0130:
            java.lang.String r1 = "android.support.content.wakelockid"
            r0.remove(r1)
            boolean r1 = com.google.firebase.messaging.zza.m13913((android.os.Bundle) r0)
            if (r1 == 0) goto L_0x014f
            com.google.firebase.messaging.zza r1 = com.google.firebase.messaging.zza.m13908((android.content.Context) r10)
            boolean r1 = r1.m13914((android.os.Bundle) r0)
            if (r1 != 0) goto L_0x0089
            boolean r1 = m13888((android.os.Bundle) r0)
            if (r1 == 0) goto L_0x014f
            com.google.firebase.messaging.zzd.m13933(r10, r11)
        L_0x014f:
            com.google.firebase.messaging.RemoteMessage r1 = new com.google.firebase.messaging.RemoteMessage
            r1.<init>(r0)
            r10.m13894((com.google.firebase.messaging.RemoteMessage) r1)
            goto L_0x0089
        L_0x0159:
            r10.m13893()
            goto L_0x0089
        L_0x015e:
            java.lang.String r0 = "google.message_id"
            java.lang.String r0 = r11.getStringExtra(r0)
            r10.m13895((java.lang.String) r0)
            goto L_0x0089
        L_0x016a:
            java.lang.String r0 = "google.message_id"
            java.lang.String r0 = r11.getStringExtra(r0)
            if (r0 != 0) goto L_0x017a
            java.lang.String r0 = "message_id"
            java.lang.String r0 = r11.getStringExtra(r0)
        L_0x017a:
            com.google.firebase.messaging.SendException r1 = new com.google.firebase.messaging.SendException
            java.lang.String r2 = "error"
            java.lang.String r2 = r11.getStringExtra(r2)
            r1.<init>(r2)
            r10.m13896(r0, r1)
            goto L_0x0089
        L_0x018b:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r2)
            goto L_0x0086
        L_0x0192:
            android.os.Bundle r0 = r11.getExtras()
            boolean r0 = m13888((android.os.Bundle) r0)
            if (r0 == 0) goto L_0x0034
            com.google.firebase.messaging.zzd.m13934(r10, r11)
            goto L_0x0034
        L_0x01a1:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r2)
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.messaging.FirebaseMessagingService.m13890(android.content.Intent):void");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m13891(Intent intent) {
        if (!"com.google.firebase.messaging.NOTIFICATION_OPEN".equals(intent.getAction())) {
            return false;
        }
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("pending_intent");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException e) {
                Log.e("FirebaseMessaging", "Notification pending intent canceled");
            }
        }
        if (m13888(intent.getExtras())) {
            zzd.m13932(this, intent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Intent m13892(Intent intent) {
        return zzx.m13866().m13867();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m13893() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m13894(RemoteMessage remoteMessage) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m13895(String str) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m13896(String str, Exception exc) {
    }
}
