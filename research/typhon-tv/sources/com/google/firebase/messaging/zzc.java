package com.google.firebase.messaging;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.internal.zzfjr;
import com.google.android.gms.internal.zzfkt;
import com.google.android.gms.internal.zzfku;
import com.google.android.gms.measurement.AppMeasurement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public final class zzc {
    /* renamed from: 靐  reason: contains not printable characters */
    private static int m13919(AppMeasurement appMeasurement, String str) {
        try {
            Method declaredMethod = AppMeasurement.class.getDeclaredMethod("getMaxUserProperties", new Class[]{String.class});
            declaredMethod.setAccessible(true);
            return ((Integer) declaredMethod.invoke(appMeasurement, new Object[]{str})).intValue();
        } catch (Exception e) {
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return 20;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m13920(Object obj) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        return (String) Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty").getField("mValue").get(obj);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m13921(Context context) {
        if (m13924(context) != null) {
            try {
                Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
                return true;
            } catch (ClassNotFoundException e) {
                if (!Log.isLoggable("FirebaseAbtUtil", 2)) {
                    return false;
                }
                Log.v("FirebaseAbtUtil", "Firebase Analytics library is missing support for abt. Please update to a more recent version.");
                return false;
            }
        } else if (!Log.isLoggable("FirebaseAbtUtil", 2)) {
            return false;
        } else {
            Log.v("FirebaseAbtUtil", "Firebase Analytics not available");
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Bundle m13922(String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString(str, str2);
        return bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzfku m13923(byte[] bArr) {
        try {
            return zzfku.m12923(bArr);
        } catch (zzfjr e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static AppMeasurement m13924(Context context) {
        try {
            return AppMeasurement.getInstance(context);
        } catch (NoClassDefFoundError e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object m13925(zzfku zzfku, String str, zzb zzb) {
        Object obj;
        String str2 = null;
        try {
            Class<?> cls = Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
            Bundle r4 = m13922(zzfku.f10631, zzfku.f10628);
            obj = cls.getConstructor(new Class[0]).newInstance(new Object[0]);
            try {
                cls.getField("mOrigin").set(obj, str);
                cls.getField("mCreationTimestamp").set(obj, Long.valueOf(zzfku.f10630));
                cls.getField("mName").set(obj, zzfku.f10631);
                cls.getField("mValue").set(obj, zzfku.f10628);
                if (!TextUtils.isEmpty(zzfku.f10629)) {
                    str2 = zzfku.f10629;
                }
                cls.getField("mTriggerEventName").set(obj, str2);
                cls.getField("mTimedOutEventName").set(obj, !TextUtils.isEmpty(zzfku.f10625) ? zzfku.f10625 : zzb.m13915());
                cls.getField("mTimedOutEventParams").set(obj, r4);
                cls.getField("mTriggerTimeout").set(obj, Long.valueOf(zzfku.f10627));
                cls.getField("mTriggeredEventName").set(obj, !TextUtils.isEmpty(zzfku.f10621) ? zzfku.f10621 : zzb.m13918());
                cls.getField("mTriggeredEventParams").set(obj, r4);
                cls.getField("mTimeToLive").set(obj, Long.valueOf(zzfku.f10619));
                cls.getField("mExpiredEventName").set(obj, !TextUtils.isEmpty(zzfku.f10626) ? zzfku.f10626 : zzb.m13917());
                cls.getField("mExpiredEventParams").set(obj, r4);
            } catch (Exception e) {
                e = e;
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
                return obj;
            }
        } catch (Exception e2) {
            e = e2;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        }
        return obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m13926(zzfku zzfku, zzb zzb) {
        return (zzfku == null || TextUtils.isEmpty(zzfku.f10624)) ? zzb.m13916() : zzfku.f10624;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m13927(Object obj) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        return (String) Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty").getField("mName").get(obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<Object> m13928(AppMeasurement appMeasurement, String str) {
        List<Object> list;
        ArrayList arrayList = new ArrayList();
        try {
            Method declaredMethod = AppMeasurement.class.getDeclaredMethod("getConditionalUserProperties", new Class[]{String.class, String.class});
            declaredMethod.setAccessible(true);
            list = (List) declaredMethod.invoke(appMeasurement, new Object[]{str, ""});
        } catch (Exception e) {
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            list = arrayList;
        }
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str).length() + 55).append("Number of currently set _Es for origin: ").append(str).append(" is ").append(list.size()).toString());
        }
        return list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m13929(Context context, String str, String str2, String str3, String str4) {
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            String valueOf = String.valueOf(str);
            Log.v("FirebaseAbtUtil", valueOf.length() != 0 ? "_CE(experimentId) called by ".concat(valueOf) : new String("_CE(experimentId) called by "));
        }
        if (m13921(context)) {
            AppMeasurement r0 = m13924(context);
            try {
                Method declaredMethod = AppMeasurement.class.getDeclaredMethod("clearConditionalUserProperty", new Class[]{String.class, String.class, Bundle.class});
                declaredMethod.setAccessible(true);
                if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                    Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str2).length() + 17 + String.valueOf(str3).length()).append("Clearing _E: [").append(str2).append(", ").append(str3).append("]").toString());
                }
                declaredMethod.invoke(r0, new Object[]{str2, str4, m13922(str2, str3)});
            } catch (Exception e) {
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m13930(Context context, String str, byte[] bArr, zzb zzb, int i) {
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            String valueOf = String.valueOf(str);
            Log.v("FirebaseAbtUtil", valueOf.length() != 0 ? "_SE called by ".concat(valueOf) : new String("_SE called by "));
        }
        if (m13921(context)) {
            AppMeasurement r2 = m13924(context);
            zzfku r5 = m13923(bArr);
            if (r5 != null) {
                try {
                    Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
                    boolean z = false;
                    for (Object next : m13928(r2, str)) {
                        String r8 = m13927(next);
                        String r9 = m13920(next);
                        long longValue = ((Long) Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty").getField("mCreationTimestamp").get(next)).longValue();
                        if (!r5.f10631.equals(r8) || !r5.f10628.equals(r9)) {
                            boolean z2 = false;
                            zzfkt[] zzfktArr = r5.f10622;
                            int length = zzfktArr.length;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length) {
                                    break;
                                } else if (zzfktArr[i2].f10618.equals(r8)) {
                                    if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r8).length() + 33 + String.valueOf(r9).length()).append("_E is found in the _OE list. [").append(r8).append(", ").append(r9).append("]").toString());
                                    }
                                    z2 = true;
                                } else {
                                    i2++;
                                }
                            }
                            if (!z2) {
                                if (r5.f10630 > longValue) {
                                    if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r8).length() + 115 + String.valueOf(r9).length()).append("Clearing _E as it was not in the _OE list, andits start time is older than the start time of the _E to be set. [").append(r8).append(", ").append(r9).append("]").toString());
                                    }
                                    m13929(context, str, r8, r9, m13926(r5, zzb));
                                } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                                    Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r8).length() + 109 + String.valueOf(r9).length()).append("_E was not found in the _OE list, but not clearing it as it has a new start time than the _E to be set.  [").append(r8).append(", ").append(r9).append("]").toString());
                                }
                            }
                        } else {
                            if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                                Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r8).length() + 23 + String.valueOf(r9).length()).append("_E is already set. [").append(r8).append(", ").append(r9).append("]").toString());
                            }
                            z = true;
                        }
                    }
                    if (!z) {
                        m13931(r2, context, str, r5, zzb, 1);
                    } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                        String str2 = r5.f10631;
                        String str3 = r5.f10628;
                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str2).length() + 44 + String.valueOf(str3).length()).append("_E is already set. Not setting it again [").append(str2).append(", ").append(str3).append("]").toString());
                    }
                } catch (Exception e) {
                    Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
                }
            } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                Log.v("FirebaseAbtUtil", "_SE failed; either _P was not set, or we couldn't deserialize the _P.");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m13931(AppMeasurement appMeasurement, Context context, String str, zzfku zzfku, zzb zzb, int i) {
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            String str2 = zzfku.f10631;
            String str3 = zzfku.f10628;
            Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str2).length() + 7 + String.valueOf(str3).length()).append("_SEI: ").append(str2).append(StringUtils.SPACE).append(str3).toString());
        }
        try {
            Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
            List<Object> r2 = m13928(appMeasurement, str);
            if (m13928(appMeasurement, str).size() >= m13919(appMeasurement, str)) {
                if ((zzfku.f10623 != 0 ? zzfku.f10623 : 1) == 1) {
                    Object obj = r2.get(0);
                    String r1 = m13927(obj);
                    String r0 = m13920(obj);
                    if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r1).length() + 38).append("Clearing _E due to overflow policy: [").append(r1).append("]").toString());
                    }
                    m13929(context, str, r1, r0, m13926(zzfku, zzb));
                } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                    String str4 = zzfku.f10631;
                    String str5 = zzfku.f10628;
                    Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str4).length() + 44 + String.valueOf(str5).length()).append("_E won't be set due to overflow policy. [").append(str4).append(", ").append(str5).append("]").toString());
                    return;
                } else {
                    return;
                }
            }
            for (Object next : r2) {
                String r22 = m13927(next);
                String r12 = m13920(next);
                if (r22.equals(zzfku.f10631) && !r12.equals(zzfku.f10628) && Log.isLoggable("FirebaseAbtUtil", 2)) {
                    Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r22).length() + 77 + String.valueOf(r12).length()).append("Clearing _E, as only one _V of the same _E can be set atany given time: [").append(r22).append(", ").append(r12).append("].").toString());
                    m13929(context, str, r22, r12, m13926(zzfku, zzb));
                }
            }
            Object r02 = m13925(zzfku, str, zzb);
            if (r02 != null) {
                try {
                    Method declaredMethod = AppMeasurement.class.getDeclaredMethod("setConditionalUserProperty", new Class[]{Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty")});
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(appMeasurement, new Object[]{r02});
                } catch (Exception e) {
                    Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
                }
            } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                String str6 = zzfku.f10631;
                String str7 = zzfku.f10628;
                Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str6).length() + 42 + String.valueOf(str7).length()).append("Could not create _CUP for: [").append(str6).append(", ").append(str7).append("]. Skipping.").toString());
            }
        } catch (Exception e2) {
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e2);
        }
    }
}
