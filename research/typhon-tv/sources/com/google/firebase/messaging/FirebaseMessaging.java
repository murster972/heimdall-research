package com.google.firebase.messaging;

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.regex.Pattern;

public class FirebaseMessaging {

    /* renamed from: 靐  reason: contains not printable characters */
    private static FirebaseMessaging f5640;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f5641 = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");

    /* renamed from: 齉  reason: contains not printable characters */
    private final FirebaseInstanceId f5642;

    private FirebaseMessaging(FirebaseInstanceId firebaseInstanceId) {
        this.f5642 = firebaseInstanceId;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized FirebaseMessaging m6109() {
        FirebaseMessaging firebaseMessaging;
        synchronized (FirebaseMessaging.class) {
            if (f5640 == null) {
                f5640 = new FirebaseMessaging(FirebaseInstanceId.m13786());
            }
            firebaseMessaging = f5640;
        }
        return firebaseMessaging;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6110(String str) {
        if (str != null && str.startsWith("/topics/")) {
            Log.w("FirebaseMessaging", "Format /topics/topic-name is deprecated. Only 'topic-name' should be used in subscribeToTopic.");
            str = str.substring(8);
        }
        if (str == null || !f5641.matcher(str).matches()) {
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 55 + String.valueOf("[a-zA-Z0-9-_.~%]{1,900}").length()).append("Invalid topic name: ").append(str).append(" does not match the allowed format ").append("[a-zA-Z0-9-_.~%]{1,900}").toString());
        }
        FirebaseInstanceId firebaseInstanceId = this.f5642;
        String valueOf = String.valueOf("S!");
        String valueOf2 = String.valueOf(str);
        firebaseInstanceId.m13800(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
    }
}
