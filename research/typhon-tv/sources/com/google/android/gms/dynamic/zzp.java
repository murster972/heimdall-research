package com.google.android.gms.dynamic;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.internal.zzbq;

public abstract class zzp<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private T f7963;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f7964;

    protected zzp(String str) {
        this.f7964 = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final T m9308(Context context) throws zzq {
        if (this.f7963 == null) {
            zzbq.m9120(context);
            Context remoteContext = com.google.android.gms.common.zzp.getRemoteContext(context);
            if (remoteContext == null) {
                throw new zzq("Could not get remote context.");
            }
            try {
                this.f7963 = m9309((IBinder) remoteContext.getClassLoader().loadClass(this.f7964).newInstance());
            } catch (ClassNotFoundException e) {
                throw new zzq("Could not load creator class.", e);
            } catch (InstantiationException e2) {
                throw new zzq("Could not instantiate creator.", e2);
            } catch (IllegalAccessException e3) {
                throw new zzq("Could not access creator.", e3);
            }
        }
        return this.f7963;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m9309(IBinder iBinder);
}
