package com.google.android.gms.auth.api.signin.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public interface zzu extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m7744(zzs zzs, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
