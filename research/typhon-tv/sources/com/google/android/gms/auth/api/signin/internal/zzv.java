package com.google.android.gms.auth.api.signin.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzv extends zzeu implements zzu {
    zzv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7745(zzs zzs, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzs);
        zzew.m12306(v_, (Parcelable) googleSignInOptions);
        m12298(103, v_);
    }
}
