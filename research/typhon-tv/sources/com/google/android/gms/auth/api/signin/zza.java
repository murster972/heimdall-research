package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

final /* synthetic */ class zza implements Comparator {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Comparator f6933 = new zza();

    private zza() {
    }

    public final int compare(Object obj, Object obj2) {
        return ((Scope) obj).m8544().compareTo(((Scope) obj2).m8544());
    }
}
