package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public final class zzc implements GoogleSignInApi {
    /* renamed from: 龘  reason: contains not printable characters */
    public final PendingResult<Status> m7718(GoogleApiClient googleApiClient) {
        return zze.m7725(googleApiClient, googleApiClient.getContext(), false);
    }
}
