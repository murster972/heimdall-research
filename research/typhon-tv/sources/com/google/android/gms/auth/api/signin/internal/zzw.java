package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.os.Binder;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;

public final class zzw extends zzr {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f6928;

    public zzw(Context context) {
        this.f6928 = context;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m7746() {
        if (!GooglePlayServicesUtil.zzf(this.f6928, Binder.getCallingUid())) {
            throw new SecurityException(new StringBuilder(52).append("Calling UID ").append(Binder.getCallingUid()).append(" is not Google Play services.").toString());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m7747() {
        m7746();
        zzo.m7733(this.f6928).m7734();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7748() {
        m7746();
        zzz r1 = zzz.m7754(this.f6928);
        GoogleSignInAccount r2 = r1.m7758();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.f6879;
        if (r2 != null) {
            googleSignInOptions = r1.m7755();
        }
        GoogleApiClient build = new GoogleApiClient.Builder(this.f6928).addApi(Auth.f6846, googleSignInOptions).build();
        try {
            if (build.blockingConnect().m8478()) {
                if (r2 != null) {
                    Auth.f6839.m7689(build);
                } else {
                    build.clearDefaultAccountAndReconnect();
                }
            }
        } finally {
            build.disconnect();
        }
    }
}
