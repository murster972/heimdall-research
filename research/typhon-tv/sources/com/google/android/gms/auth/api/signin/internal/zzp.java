package com.google.android.gms.auth.api.signin.internal;

public final class zzp {

    /* renamed from: 龘  reason: contains not printable characters */
    private static int f6926 = 31;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f6927 = 1;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m7736() {
        return this.f6927;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzp m7737(Object obj) {
        this.f6927 = (obj == null ? 0 : obj.hashCode()) + (this.f6927 * f6926);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzp m7738(boolean z) {
        this.f6927 = (z ? 1 : 0) + (this.f6927 * f6926);
        return this;
    }
}
