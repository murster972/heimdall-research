package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzr;

public final class zzd extends zzab<zzu> {

    /* renamed from: 麤  reason: contains not printable characters */
    private final GoogleSignInOptions f6916;

    public zzd(Context context, Looper looper, zzr zzr, GoogleSignInOptions googleSignInOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 91, zzr, connectionCallbacks, onConnectionFailedListener);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.Builder().m7705() : googleSignInOptions;
        if (!zzr.m4211().isEmpty()) {
            GoogleSignInOptions.Builder builder = new GoogleSignInOptions.Builder(googleSignInOptions);
            for (Scope r0 : zzr.m4211()) {
                builder.m7707(r0, new Scope[0]);
            }
            googleSignInOptions = builder.m7705();
        }
        this.f6916 = googleSignInOptions;
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final GoogleSignInOptions m7719() {
        return this.f6916;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m7720() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Intent m7721() {
        return zze.m7724(m9168(), this.f6916);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m7722() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m7723(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        return queryLocalInterface instanceof zzu ? (zzu) queryLocalInterface : new zzv(iBinder);
    }
}
