package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzj implements Parcelable.Creator<PasswordSpecification> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        int i = 0;
        int i2 = 0;
        ArrayList<Integer> arrayList = null;
        ArrayList<String> arrayList2 = null;
        String str = null;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 2:
                    arrayList2 = zzbfn.m10159(parcel, readInt);
                    break;
                case 3:
                    arrayList = zzbfn.m10158(parcel, readInt);
                    break;
                case 4:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new PasswordSpecification(str, arrayList2, arrayList, i2, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new PasswordSpecification[i];
    }
}
