package com.google.android.gms.auth.api.signin;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzb implements Parcelable.Creator<GoogleSignInAccount> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r16 = zzbfn.m10169(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Uri uri = null;
        String str5 = null;
        long j = 0;
        String str6 = null;
        ArrayList<Scope> arrayList = null;
        String str7 = null;
        String str8 = null;
        while (parcel.dataPosition() < r16) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    uri = (Uri) zzbfn.m10171(parcel, readInt, Uri.CREATOR);
                    break;
                case 7:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 9:
                    str6 = zzbfn.m10162(parcel, readInt);
                    break;
                case 10:
                    arrayList = zzbfn.m10167(parcel, readInt, Scope.CREATOR);
                    break;
                case 11:
                    str7 = zzbfn.m10162(parcel, readInt);
                    break;
                case 12:
                    str8 = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r16);
        return new GoogleSignInAccount(i, str, str2, str3, str4, uri, str5, j, str6, arrayList, str7, str8);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInAccount[i];
    }
}
