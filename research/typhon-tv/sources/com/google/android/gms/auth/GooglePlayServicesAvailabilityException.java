package com.google.android.gms.auth;

public class GooglePlayServicesAvailabilityException extends UserRecoverableAuthException {
    private final int zzecj;

    public int getConnectionStatusCode() {
        return this.zzecj;
    }
}
