package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzcu;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public final class zzb extends AsyncTaskLoader<Void> implements zzcu {

    /* renamed from: 靐  reason: contains not printable characters */
    private Set<GoogleApiClient> f6914;

    /* renamed from: 龘  reason: contains not printable characters */
    private Semaphore f6915 = new Semaphore(0);

    public zzb(Context context, Set<GoogleApiClient> set) {
        super(context);
        this.f6914 = set;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final Void loadInBackground() {
        int i = 0;
        Iterator<GoogleApiClient> it2 = this.f6914.iterator();
        while (true) {
            int i2 = i;
            if (it2.hasNext()) {
                i = it2.next().zza((zzcu) this) ? i2 + 1 : i2;
            } else {
                try {
                    this.f6915.tryAcquire(i2, 5, TimeUnit.SECONDS);
                    return null;
                } catch (InterruptedException e) {
                    Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
                    Thread.currentThread().interrupt();
                    return null;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onStartLoading() {
        this.f6915.drainPermits();
        forceLoad();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7717() {
        this.f6915.release();
    }
}
