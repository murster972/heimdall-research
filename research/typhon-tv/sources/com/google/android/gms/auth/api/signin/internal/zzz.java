package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

public final class zzz {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzz f6929;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Lock f6930 = new ReentrantLock();

    /* renamed from: 麤  reason: contains not printable characters */
    private final SharedPreferences f6931;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Lock f6932 = new ReentrantLock();

    private zzz(Context context) {
        this.f6931 = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final GoogleSignInOptions m7749(String str) {
        String r1;
        if (TextUtils.isEmpty(str) || (r1 = m7752(m7750("googleSignInOptions", str))) == null) {
            return null;
        }
        try {
            return GoogleSignInOptions.m7699(r1);
        } catch (JSONException e) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m7750(String str, String str2) {
        return new StringBuilder(String.valueOf(str).length() + String.valueOf(":").length() + String.valueOf(str2).length()).append(str).append(":").append(str2).toString();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m7751(String str) {
        this.f6932.lock();
        try {
            this.f6931.edit().remove(str).apply();
        } finally {
            this.f6932.unlock();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final String m7752(String str) {
        this.f6932.lock();
        try {
            return this.f6931.getString(str, (String) null);
        } finally {
            this.f6932.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final GoogleSignInAccount m7753(String str) {
        String r1;
        if (TextUtils.isEmpty(str) || (r1 = m7752(m7750("googleSignInAccount", str))) == null) {
            return null;
        }
        try {
            return GoogleSignInAccount.m7675(r1);
        } catch (JSONException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzz m7754(Context context) {
        zzbq.m9120(context);
        f6930.lock();
        try {
            if (f6929 == null) {
                f6929 = new zzz(context.getApplicationContext());
            }
            return f6929;
        } finally {
            f6930.unlock();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final GoogleSignInOptions m7755() {
        return m7749(m7752("defaultGoogleSignInAccount"));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m7756() {
        this.f6932.lock();
        try {
            this.f6931.edit().clear().apply();
        } finally {
            this.f6932.unlock();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m7757() {
        String r0 = m7752("defaultGoogleSignInAccount");
        m7751("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(r0)) {
            m7751(m7750("googleSignInAccount", r0));
            m7751(m7750("googleSignInOptions", r0));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final GoogleSignInAccount m7758() {
        return m7753(m7752("defaultGoogleSignInAccount"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7759(GoogleSignInAccount googleSignInAccount, GoogleSignInOptions googleSignInOptions) {
        zzbq.m9120(googleSignInAccount);
        zzbq.m9120(googleSignInOptions);
        String r0 = googleSignInAccount.m7682();
        m7760(m7750("googleSignInAccount", r0), googleSignInAccount.m7680());
        m7760(m7750("googleSignInOptions", r0), googleSignInOptions.m7702());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7760(String str, String str2) {
        this.f6932.lock();
        try {
            this.f6931.edit().putString(str, str2).apply();
        } finally {
            this.f6932.unlock();
        }
    }
}
