package com.google.android.gms.auth.api.signin.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

public interface zzs extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    void m7741(Status status) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m7742(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m7743(Status status) throws RemoteException;
}
