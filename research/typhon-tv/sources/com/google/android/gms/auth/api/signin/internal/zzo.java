package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.zzbq;

public final class zzo {

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzo f6922 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzz f6923;

    /* renamed from: 麤  reason: contains not printable characters */
    private GoogleSignInOptions f6924 = this.f6923.m7755();

    /* renamed from: 齉  reason: contains not printable characters */
    private GoogleSignInAccount f6925 = this.f6923.m7758();

    private zzo(Context context) {
        this.f6923 = zzz.m7754(context);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static synchronized zzo m7732(Context context) {
        zzo zzo;
        synchronized (zzo.class) {
            if (f6922 == null) {
                f6922 = new zzo(context);
            }
            zzo = f6922;
        }
        return zzo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized zzo m7733(Context context) {
        zzo r0;
        synchronized (zzo.class) {
            r0 = m7732(context.getApplicationContext());
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m7734() {
        this.f6923.m7756();
        this.f6925 = null;
        this.f6924 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m7735(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        zzz zzz = this.f6923;
        zzbq.m9120(googleSignInAccount);
        zzbq.m9120(googleSignInOptions);
        zzz.m7760("defaultGoogleSignInAccount", googleSignInAccount.m7682());
        zzz.m7759(googleSignInAccount, googleSignInOptions);
        this.f6925 = googleSignInAccount;
        this.f6924 = googleSignInOptions;
    }
}
