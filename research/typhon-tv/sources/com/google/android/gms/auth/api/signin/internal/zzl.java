package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.zzm;

abstract class zzl<R extends Result> extends zzm<R, zzd> {
    public zzl(GoogleApiClient googleApiClient) {
        super(Auth.f6846, googleApiClient);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m7730(Object obj) {
        super.m4198((Result) obj);
    }
}
