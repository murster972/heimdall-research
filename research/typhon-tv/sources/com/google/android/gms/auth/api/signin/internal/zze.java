package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzbm;
import com.google.android.gms.internal.zzbgg;

public final class zze {

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzbgg f6917 = new zzbgg("GoogleSignInCommon", new String[0]);

    /* renamed from: 龘  reason: contains not printable characters */
    public static Intent m7724(Context context, GoogleSignInOptions googleSignInOptions) {
        f6917.m10220("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PendingResult<Status> m7725(GoogleApiClient googleApiClient, Context context, boolean z) {
        f6917.m10220("Revoking access", new Object[0]);
        m7726(context);
        return z ? PendingResults.m8537(Status.f7458, googleApiClient) : googleApiClient.zze(new zzj(googleApiClient));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m7726(Context context) {
        zzo.m7733(context).m7734();
        for (GoogleApiClient zzags : GoogleApiClient.zzagr()) {
            zzags.zzags();
        }
        zzbm.m8760();
    }
}
