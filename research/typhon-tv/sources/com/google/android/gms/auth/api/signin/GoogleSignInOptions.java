package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.internal.zzn;
import com.google.android.gms.auth.api.signin.internal.zzp;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInOptions extends zzbfm implements Api.ApiOptions.Optional, ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR = new zze();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final GoogleSignInOptions f6879 = new Builder().m7706().m7704().m7705();

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final GoogleSignInOptions f6880 = new Builder().m7707(f6884, new Scope[0]).m7705();

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Comparator<Scope> f6881 = new zzd();

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Scope f6882 = new Scope("https://www.googleapis.com/auth/games");

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Scope f6883 = new Scope(NotificationCompat.CATEGORY_EMAIL);

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Scope f6884 = new Scope("https://www.googleapis.com/auth/games_lite");

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Scope f6885 = new Scope("openid");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Scope f6886 = new Scope("profile");

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f6887;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean f6888;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f6889;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean f6890;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Map<Integer, zzn> f6891;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final ArrayList<Scope> f6892;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public Account f6893;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean f6894;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public String f6895;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public ArrayList<zzn> f6896;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Account f6897;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f6898;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Map<Integer, zzn> f6899 = new HashMap();

        /* renamed from: 连任  reason: contains not printable characters */
        private String f6900;

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f6901;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f6902;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f6903;

        /* renamed from: 龘  reason: contains not printable characters */
        private Set<Scope> f6904 = new HashSet();

        public Builder() {
        }

        public Builder(GoogleSignInOptions googleSignInOptions) {
            zzbq.m9120(googleSignInOptions);
            this.f6904 = new HashSet(googleSignInOptions.f6892);
            this.f6901 = googleSignInOptions.f6890;
            this.f6903 = googleSignInOptions.f6888;
            this.f6902 = googleSignInOptions.f6894;
            this.f6900 = googleSignInOptions.f6889;
            this.f6897 = googleSignInOptions.f6893;
            this.f6898 = googleSignInOptions.f6895;
            this.f6899 = GoogleSignInOptions.m7694((List<zzn>) googleSignInOptions.f6896);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final Builder m7704() {
            this.f6904.add(GoogleSignInOptions.f6886);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final GoogleSignInOptions m7705() {
            if (this.f6904.contains(GoogleSignInOptions.f6882) && this.f6904.contains(GoogleSignInOptions.f6884)) {
                this.f6904.remove(GoogleSignInOptions.f6884);
            }
            if (this.f6902 && (this.f6897 == null || !this.f6904.isEmpty())) {
                m7706();
            }
            return new GoogleSignInOptions(3, new ArrayList(this.f6904), this.f6897, this.f6902, this.f6901, this.f6903, this.f6900, this.f6898, this.f6899, (zzd) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7706() {
            this.f6904.add(GoogleSignInOptions.f6885);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7707(Scope scope, Scope... scopeArr) {
            this.f6904.add(scope);
            this.f6904.addAll(Arrays.asList(scopeArr));
            return this;
        }
    }

    GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, ArrayList<zzn> arrayList2) {
        this(i, arrayList, account, z, z2, z3, str, str2, m7694((List<zzn>) arrayList2));
    }

    private GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map<Integer, zzn> map) {
        this.f6887 = i;
        this.f6892 = arrayList;
        this.f6893 = account;
        this.f6894 = z;
        this.f6890 = z2;
        this.f6888 = z3;
        this.f6889 = str;
        this.f6895 = str2;
        this.f6896 = new ArrayList<>(map.values());
        this.f6891 = map;
    }

    /* synthetic */ GoogleSignInOptions(int i, ArrayList arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map map, zzd zzd) {
        this(3, (ArrayList<Scope>) arrayList, account, z, z2, z3, str, str2, (Map<Integer, zzn>) map);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static Map<Integer, zzn> m7694(List<zzn> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (zzn next : list) {
            hashMap.put(Integer.valueOf(next.m7731()), next);
        }
        return hashMap;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final JSONObject m7697() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            Collections.sort(this.f6892, f6881);
            ArrayList arrayList = this.f6892;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                jSONArray.put((Object) ((Scope) obj).m8544());
            }
            jSONObject.put("scopes", (Object) jSONArray);
            if (this.f6893 != null) {
                jSONObject.put("accountName", (Object) this.f6893.name);
            }
            jSONObject.put("idTokenRequested", this.f6894);
            jSONObject.put("forceCodeForRefreshToken", this.f6888);
            jSONObject.put("serverAuthRequested", this.f6890);
            if (!TextUtils.isEmpty(this.f6889)) {
                jSONObject.put("serverClientId", (Object) this.f6889);
            }
            if (!TextUtils.isEmpty(this.f6895)) {
                jSONObject.put("hostedDomain", (Object) this.f6895);
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GoogleSignInOptions m7699(String str) throws JSONException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("scopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        String optString = jSONObject.optString("accountName", (String) null);
        return new GoogleSignInOptions(3, (ArrayList<Scope>) new ArrayList(hashSet), !TextUtils.isEmpty(optString) ? new Account(optString, "com.google") : null, jSONObject.getBoolean("idTokenRequested"), jSONObject.getBoolean("serverAuthRequested"), jSONObject.getBoolean("forceCodeForRefreshToken"), jSONObject.optString("serverClientId", (String) null), jSONObject.optString("hostedDomain", (String) null), (Map<Integer, zzn>) new HashMap());
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions) obj;
            if (this.f6896.size() > 0 || googleSignInOptions.f6896.size() > 0 || this.f6892.size() != googleSignInOptions.m7703().size() || !this.f6892.containsAll(googleSignInOptions.m7703())) {
                return false;
            }
            if (this.f6893 == null) {
                if (googleSignInOptions.f6893 != null) {
                    return false;
                }
            } else if (!this.f6893.equals(googleSignInOptions.f6893)) {
                return false;
            }
            if (TextUtils.isEmpty(this.f6889)) {
                if (!TextUtils.isEmpty(googleSignInOptions.f6889)) {
                    return false;
                }
            } else if (!this.f6889.equals(googleSignInOptions.f6889)) {
                return false;
            }
            return this.f6888 == googleSignInOptions.f6888 && this.f6894 == googleSignInOptions.f6894 && this.f6890 == googleSignInOptions.f6890;
        } catch (ClassCastException e) {
            return false;
        }
    }

    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = this.f6892;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            arrayList.add(((Scope) obj).m8544());
        }
        Collections.sort(arrayList);
        return new zzp().m7737((Object) arrayList).m7737((Object) this.f6893).m7737((Object) this.f6889).m7738(this.f6888).m7738(this.f6894).m7738(this.f6890).m7736();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f6887);
        zzbfp.m10180(parcel, 2, m7703(), false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f6893, i, false);
        zzbfp.m10195(parcel, 4, this.f6894);
        zzbfp.m10195(parcel, 5, this.f6890);
        zzbfp.m10195(parcel, 6, this.f6888);
        zzbfp.m10193(parcel, 7, this.f6889, false);
        zzbfp.m10193(parcel, 8, this.f6895, false);
        zzbfp.m10180(parcel, 9, this.f6896, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m7702() {
        return m7697().toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ArrayList<Scope> m7703() {
        return new ArrayList<>(this.f6892);
    }
}
