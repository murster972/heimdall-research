package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class SignInConfiguration extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<SignInConfiguration> CREATOR = new zzx();

    /* renamed from: 靐  reason: contains not printable characters */
    private GoogleSignInOptions f6905;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f6906;

    public SignInConfiguration(String str, GoogleSignInOptions googleSignInOptions) {
        this.f6906 = zzbq.m9122(str);
        this.f6905 = googleSignInOptions;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            SignInConfiguration signInConfiguration = (SignInConfiguration) obj;
            if (!this.f6906.equals(signInConfiguration.f6906)) {
                return false;
            }
            if (this.f6905 == null) {
                if (signInConfiguration.f6905 != null) {
                    return false;
                }
            } else if (!this.f6905.equals(signInConfiguration.f6905)) {
                return false;
            }
            return true;
        } catch (ClassCastException e) {
            return false;
        }
    }

    public final int hashCode() {
        return new zzp().m7737((Object) this.f6906).m7737((Object) this.f6905).m7736();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f6906, false);
        zzbfp.m10189(parcel, 5, (Parcelable) this.f6905, i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final GoogleSignInOptions m7708() {
        return this.f6905;
    }
}
