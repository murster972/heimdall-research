package com.google.android.gms.auth.api.signin.internal;

import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

public class zza extends zzt {
    /* renamed from: 靐  reason: contains not printable characters */
    public void m7713(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7714(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7715(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
