package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.internal.zzn;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zze implements Parcelable.Creator<GoogleSignInOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        ArrayList<zzn> arrayList = null;
        String str = null;
        String str2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        Account account = null;
        ArrayList<Scope> arrayList2 = null;
        int i = 0;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    arrayList2 = zzbfn.m10167(parcel, readInt, Scope.CREATOR);
                    break;
                case 3:
                    account = (Account) zzbfn.m10171(parcel, readInt, Account.CREATOR);
                    break;
                case 4:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 7:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 9:
                    arrayList = zzbfn.m10167(parcel, readInt, zzn.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new GoogleSignInOptions(i, arrayList2, account, z3, z2, z, str2, str, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInOptions[i];
    }
}
