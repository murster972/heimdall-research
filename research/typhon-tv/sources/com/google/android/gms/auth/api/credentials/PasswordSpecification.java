package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

public final class PasswordSpecification extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<PasswordSpecification> CREATOR = new zzj();

    /* renamed from: 靐  reason: contains not printable characters */
    private static PasswordSpecification f6851 = new zza().m7669(12, 16).m7670("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").m7671("abcdefghijklmnopqrstuvwxyz", 1).m7671("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1).m7671("1234567890", 1).m7672();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final PasswordSpecification f6852 = new zza().m7669(12, 16).m7670("abcdefghijkmnopqrstxyzABCDEFGHJKLMNPQRSTXY3456789").m7671("abcdefghijkmnopqrstxyz", 1).m7671("ABCDEFGHJKLMNPQRSTXY", 1).m7671("3456789", 1).m7672();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f6853;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f6854;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int[] f6855;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Random f6856;

    /* renamed from: 连任  reason: contains not printable characters */
    private List<Integer> f6857;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<String> f6858;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f6859;

    public static class zza {

        /* renamed from: 连任  reason: contains not printable characters */
        private int f6860 = 16;

        /* renamed from: 靐  reason: contains not printable characters */
        private final List<String> f6861 = new ArrayList();

        /* renamed from: 麤  reason: contains not printable characters */
        private int f6862 = 12;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<Integer> f6863 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        private final TreeSet<Character> f6864 = new TreeSet<>();

        /* renamed from: 龘  reason: contains not printable characters */
        private static TreeSet<Character> m7668(String str, String str2) {
            if (TextUtils.isEmpty(str)) {
                throw new zzb(String.valueOf(str2).concat(" cannot be null or empty"));
            }
            TreeSet<Character> treeSet = new TreeSet<>();
            for (char c : str.toCharArray()) {
                if (PasswordSpecification.m7665(c, 32, 126)) {
                    throw new zzb(String.valueOf(str2).concat(" must only contain ASCII printable characters"));
                }
                treeSet.add(Character.valueOf(c));
            }
            return treeSet;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m7669(int i, int i2) {
            this.f6862 = 12;
            this.f6860 = 16;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m7670(String str) {
            this.f6864.addAll(m7668(str, "allowedChars"));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m7671(String str, int i) {
            this.f6861.add(PasswordSpecification.m7664(m7668(str, "requiredChars")));
            this.f6863.add(1);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final PasswordSpecification m7672() {
            if (this.f6864.isEmpty()) {
                throw new zzb("no allowed characters specified");
            }
            int i = 0;
            for (Integer intValue : this.f6863) {
                i = intValue.intValue() + i;
            }
            if (i > this.f6860) {
                throw new zzb("required character count cannot be greater than the max password size");
            }
            boolean[] zArr = new boolean[95];
            for (String charArray : this.f6861) {
                char[] charArray2 = charArray.toCharArray();
                int length = charArray2.length;
                int i2 = 0;
                while (true) {
                    if (i2 < length) {
                        char c = charArray2[i2];
                        if (zArr[c - ' ']) {
                            throw new zzb(new StringBuilder(58).append("character ").append(c).append(" occurs in more than one required character set").toString());
                        }
                        zArr[c - ' '] = true;
                        i2++;
                    }
                }
            }
            return new PasswordSpecification(PasswordSpecification.m7664(this.f6864), this.f6861, this.f6863, this.f6862, this.f6860);
        }
    }

    public static class zzb extends Error {
        public zzb(String str) {
            super(str);
        }
    }

    PasswordSpecification(String str, List<String> list, List<Integer> list2, int i, int i2) {
        this.f6859 = str;
        this.f6858 = Collections.unmodifiableList(list);
        this.f6857 = Collections.unmodifiableList(list2);
        this.f6853 = i;
        this.f6854 = i2;
        int[] iArr = new int[95];
        Arrays.fill(iArr, -1);
        int i3 = 0;
        for (String charArray : this.f6858) {
            char[] charArray2 = charArray.toCharArray();
            int length = charArray2.length;
            for (int i4 = 0; i4 < length; i4++) {
                iArr[charArray2[i4] - ' '] = i3;
            }
            i3++;
        }
        this.f6855 = iArr;
        this.f6856 = new SecureRandom();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m7664(Collection<Character> collection) {
        char[] cArr = new char[collection.size()];
        int i = 0;
        Iterator<Character> it2 = collection.iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return new String(cArr);
            }
            i = i2 + 1;
            cArr[i2] = it2.next().charValue();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m7665(int i, int i2, int i3) {
        return i < 32 || i > 126;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 1, this.f6859, false);
        zzbfp.m10178(parcel, 2, this.f6858, false);
        zzbfp.m10194(parcel, 3, this.f6857, false);
        zzbfp.m10185(parcel, 4, this.f6853);
        zzbfp.m10185(parcel, 5, this.f6854);
        zzbfp.m10182(parcel, r0);
    }
}
