package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInAccount extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInAccount> CREATOR = new zzb();

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzd f6865 = zzh.m9250();

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f6866;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Uri f6867;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f6868;

    /* renamed from: ʾ  reason: contains not printable characters */
    private String f6869;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Set<Scope> f6870 = new HashSet();

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f6871;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f6872;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f6873;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private List<Scope> f6874;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f6875;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f6876;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f6877;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f6878;

    GoogleSignInAccount(int i, String str, String str2, String str3, String str4, Uri uri, String str5, long j, String str6, List<Scope> list, String str7, String str8) {
        this.f6876 = i;
        this.f6878 = str;
        this.f6877 = str2;
        this.f6875 = str3;
        this.f6866 = str4;
        this.f6867 = uri;
        this.f6868 = str5;
        this.f6872 = j;
        this.f6873 = str6;
        this.f6874 = list;
        this.f6871 = str7;
        this.f6869 = str8;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private final JSONObject m7673() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (m7688() != null) {
                jSONObject.put("id", (Object) m7688());
            }
            if (m7685() != null) {
                jSONObject.put("tokenId", (Object) m7685());
            }
            if (m7687() != null) {
                jSONObject.put(NotificationCompat.CATEGORY_EMAIL, (Object) m7687());
            }
            if (m7684() != null) {
                jSONObject.put("displayName", (Object) m7684());
            }
            if (m7677() != null) {
                jSONObject.put("givenName", (Object) m7677());
            }
            if (m7678() != null) {
                jSONObject.put("familyName", (Object) m7678());
            }
            if (m7679() != null) {
                jSONObject.put("photoUrl", (Object) m7679().toString());
            }
            if (m7681() != null) {
                jSONObject.put("serverAuthCode", (Object) m7681());
            }
            jSONObject.put("expirationTime", this.f6872);
            jSONObject.put("obfuscatedIdentifier", (Object) this.f6873);
            JSONArray jSONArray = new JSONArray();
            Scope[] scopeArr = (Scope[]) this.f6874.toArray(new Scope[this.f6874.size()]);
            Arrays.sort(scopeArr, zza.f6933);
            for (Scope r5 : scopeArr) {
                jSONArray.put((Object) r5.m8544());
            }
            jSONObject.put("grantedScopes", (Object) jSONArray);
            return jSONObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GoogleSignInAccount m7675(String str) throws JSONException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        String optString = jSONObject.optString("photoUrl", (String) null);
        Uri parse = !TextUtils.isEmpty(optString) ? Uri.parse(optString) : null;
        long parseLong = Long.parseLong(jSONObject.getString("expirationTime"));
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("grantedScopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        GoogleSignInAccount r0 = m7676(jSONObject.optString("id"), jSONObject.optString("tokenId", (String) null), jSONObject.optString(NotificationCompat.CATEGORY_EMAIL, (String) null), jSONObject.optString("displayName", (String) null), jSONObject.optString("givenName", (String) null), jSONObject.optString("familyName", (String) null), parse, Long.valueOf(parseLong), jSONObject.getString("obfuscatedIdentifier"), hashSet);
        r0.f6868 = jSONObject.optString("serverAuthCode", (String) null);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static GoogleSignInAccount m7676(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Long l, String str7, Set<Scope> set) {
        if (l == null) {
            l = Long.valueOf(f6865.m9243() / 1000);
        }
        return new GoogleSignInAccount(3, str, str2, str3, str4, uri, (String) null, l.longValue(), zzbq.m9122(str7), new ArrayList((Collection) zzbq.m9120(set)), str5, str6);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof GoogleSignInAccount)) {
            return false;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) obj;
        return googleSignInAccount.f6873.equals(this.f6873) && googleSignInAccount.m7683().equals(m7683());
    }

    public int hashCode() {
        return ((this.f6873.hashCode() + 527) * 31) + m7683().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f6876);
        zzbfp.m10193(parcel, 2, m7688(), false);
        zzbfp.m10193(parcel, 3, m7685(), false);
        zzbfp.m10193(parcel, 4, m7687(), false);
        zzbfp.m10193(parcel, 5, m7684(), false);
        zzbfp.m10189(parcel, 6, (Parcelable) m7679(), i, false);
        zzbfp.m10193(parcel, 7, m7681(), false);
        zzbfp.m10186(parcel, 8, this.f6872);
        zzbfp.m10193(parcel, 9, this.f6873, false);
        zzbfp.m10180(parcel, 10, this.f6874, false);
        zzbfp.m10193(parcel, 11, m7677(), false);
        zzbfp.m10193(parcel, 12, m7678(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7677() {
        return this.f6871;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7678() {
        return this.f6869;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Uri m7679() {
        return this.f6867;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m7680() {
        JSONObject r0 = m7673();
        r0.remove("serverAuthCode");
        return r0.toString();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m7681() {
        return this.f6868;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final String m7682() {
        return this.f6873;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final Set<Scope> m7683() {
        HashSet hashSet = new HashSet(this.f6874);
        hashSet.addAll(this.f6870);
        return hashSet;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m7684() {
        return this.f6866;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7685() {
        return this.f6877;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Account m7686() {
        if (this.f6875 == null) {
            return null;
        }
        return new Account(this.f6875, "com.google");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m7687() {
        return this.f6875;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7688() {
        return this.f6878;
    }
}
