package com.google.android.gms.auth.api.signin.internal;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

@KeepName
public class SignInHubActivity extends FragmentActivity {

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f6907 = false;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public Intent f6908;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public int f6909;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f6910 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f6911;

    /* renamed from: 齉  reason: contains not printable characters */
    private SignInConfiguration f6912;

    class zza implements LoaderManager.LoaderCallbacks<Void> {
        private zza() {
        }

        public final Loader<Void> onCreateLoader(int i, Bundle bundle) {
            return new zzb(SignInHubActivity.this, GoogleApiClient.zzagr());
        }

        public final /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            SignInHubActivity.this.setResult(SignInHubActivity.this.f6909, SignInHubActivity.this.f6908);
            SignInHubActivity.this.finish();
        }

        public final void onLoaderReset(Loader<Void> loader) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m7711() {
        getSupportLoaderManager().initLoader(0, (Bundle) null, new zza());
        f6907 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m7712(int i) {
        Status status = new Status(i);
        Intent intent = new Intent();
        intent.putExtra("googleSignInStatus", status);
        setResult(0, intent);
        finish();
        f6907 = false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!this.f6910) {
            setResult(0);
            switch (i) {
                case 40962:
                    if (intent != null) {
                        SignInAccount signInAccount = (SignInAccount) intent.getParcelableExtra("signInAccount");
                        if (signInAccount != null && signInAccount.m4108() != null) {
                            GoogleSignInAccount r0 = signInAccount.m4108();
                            zzo.m7733(this).m7735(this.f6912.m7708(), r0);
                            intent.removeExtra("signInAccount");
                            intent.putExtra("googleSignInAccount", r0);
                            this.f6911 = true;
                            this.f6909 = i2;
                            this.f6908 = intent;
                            m7711();
                            return;
                        } else if (intent.hasExtra("errorCode")) {
                            m7712(intent.getIntExtra("errorCode", 8));
                            return;
                        }
                    }
                    m7712(8);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        String action = intent.getAction();
        if ("com.google.android.gms.auth.NO_IMPL".equals(action)) {
            m7712(12500);
        } else if (f6907) {
            setResult(0);
            m7712(12502);
        } else {
            f6907 = true;
            if (action.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN") || action.equals("com.google.android.gms.auth.APPAUTH_SIGN_IN")) {
                this.f6912 = (SignInConfiguration) intent.getBundleExtra("config").getParcelable("config");
                if (this.f6912 == null) {
                    Log.e("AuthSignInClient", "Activity started with invalid configuration.");
                    setResult(0);
                    finish();
                } else if (bundle == null) {
                    Intent intent2 = new Intent(action);
                    if (action.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN")) {
                        intent2.setPackage("com.google.android.gms");
                    } else {
                        intent2.setPackage(getPackageName());
                    }
                    intent2.putExtra("config", this.f6912);
                    try {
                        startActivityForResult(intent2, 40962);
                    } catch (ActivityNotFoundException e) {
                        this.f6910 = true;
                        Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
                        m7712(17);
                    }
                } else {
                    this.f6911 = bundle.getBoolean("signingInGoogleApiClients");
                    if (this.f6911) {
                        this.f6909 = bundle.getInt("signInResultCode");
                        this.f6908 = (Intent) bundle.getParcelable("signInResultData");
                        m7711();
                    }
                }
            } else {
                String valueOf = String.valueOf(intent.getAction());
                Log.e("AuthSignInClient", valueOf.length() != 0 ? "Unknown action: ".concat(valueOf) : new String("Unknown action: "));
                finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("signingInGoogleApiClients", this.f6911);
        if (this.f6911) {
            bundle.putInt("signInResultCode", this.f6909);
            bundle.putParcelable("signInResultData", this.f6908);
        }
    }
}
