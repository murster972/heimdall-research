package com.google.android.gms.auth.api.signin;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public class SignInAccount extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<SignInAccount> CREATOR = new zzf();

    /* renamed from: 靐  reason: contains not printable characters */
    private GoogleSignInAccount f3626;
    @Deprecated

    /* renamed from: 齉  reason: contains not printable characters */
    private String f3627;
    @Deprecated

    /* renamed from: 龘  reason: contains not printable characters */
    private String f3628;

    SignInAccount(String str, GoogleSignInAccount googleSignInAccount, String str2) {
        this.f3626 = googleSignInAccount;
        this.f3628 = zzbq.m9123(str, (Object) "8.3 and 8.4 SDKs require non-null email");
        this.f3627 = zzbq.m9123(str2, (Object) "8.3 and 8.4 SDKs require non-null userId");
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 4, this.f3628, false);
        zzbfp.m10189(parcel, 7, (Parcelable) this.f3626, i, false);
        zzbfp.m10193(parcel, 8, this.f3627, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final GoogleSignInAccount m4108() {
        return this.f3626;
    }
}
