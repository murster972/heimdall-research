package com.google.android.gms.auth.api;

import android.os.Bundle;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzc;
import com.google.android.gms.auth.api.signin.internal.zzd;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.zzaur;
import com.google.android.gms.internal.zzaus;
import com.google.android.gms.internal.zzaut;
import com.google.android.gms.internal.zzavp;
import com.google.android.gms.internal.zzavy;
import com.google.android.gms.internal.zzawx;

public final class Auth {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final ProxyApi f6837 = new zzawx();

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final CredentialsApi f6838 = new zzavp();

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final GoogleSignInApi f6839 = new zzc();

    /* renamed from: ʾ  reason: contains not printable characters */
    private static Api<Object> f6840 = new Api<>("Auth.ACCOUNT_STATUS_API", f6845, f6843);

    /* renamed from: ʿ  reason: contains not printable characters */
    private static zzaur f6841 = new zzaus();

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final Api.zza<zzd, GoogleSignInOptions> f6842 = new zzc();

    /* renamed from: ˑ  reason: contains not printable characters */
    private static Api.zzf<zzaut> f6843 = new Api.zzf<>();

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final Api.zza<zzavy, AuthCredentialsOptions> f6844 = new zza();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final Api.zza<zzaut, Object> f6845 = new zzb();

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Api<GoogleSignInOptions> f6846 = new Api<>("Auth.GOOGLE_SIGN_IN_API", f6842, f6847);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Api.zzf<zzd> f6847 = new Api.zzf<>();

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Api<AuthCredentialsOptions> f6848 = new Api<>("Auth.CREDENTIALS_API", f6844, f6850);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Api<zzf> f6849 = zzd.f6936;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Api.zzf<zzavy> f6850 = new Api.zzf<>();

    @Deprecated
    public static class AuthCredentialsOptions implements Api.ApiOptions.Optional {

        /* renamed from: 龘  reason: contains not printable characters */
        private static AuthCredentialsOptions f3620 = new Builder().m4107();

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f3621 = null;

        /* renamed from: 麤  reason: contains not printable characters */
        private final boolean f3622;

        /* renamed from: 齉  reason: contains not printable characters */
        private final PasswordSpecification f3623;

        @Deprecated
        public static class Builder {

            /* renamed from: 靐  reason: contains not printable characters */
            protected Boolean f3624 = false;

            /* renamed from: 龘  reason: contains not printable characters */
            protected PasswordSpecification f3625 = PasswordSpecification.f6852;

            /* renamed from: 龘  reason: contains not printable characters */
            public AuthCredentialsOptions m4107() {
                return new AuthCredentialsOptions(this);
            }
        }

        public AuthCredentialsOptions(Builder builder) {
            this.f3623 = builder.f3625;
            this.f3622 = builder.f3624.booleanValue();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Bundle m4106() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", (String) null);
            bundle.putParcelable("password_specification", this.f3623);
            bundle.putBoolean("force_save_dialog", this.f3622);
            return bundle;
        }
    }
}
