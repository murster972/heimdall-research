package com.google.android.gms.auth.api;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.zzawi;

public final class zzd {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Api.zzf<zzawi> f6934 = new Api.zzf<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Api.zza<zzawi, zzf> f6935 = new zze();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Api<zzf> f6936 = new Api<>("Auth.PROXY_API", f6935, f6934);
}
