package com.google.android.gms.auth.api.signin.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzn extends zzbfm {
    public static final Parcelable.Creator<zzn> CREATOR = new zzm();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f6919;

    /* renamed from: 齉  reason: contains not printable characters */
    private Bundle f6920;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f6921;

    zzn(int i, int i2, Bundle bundle) {
        this.f6921 = i;
        this.f6919 = i2;
        this.f6920 = bundle;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f6921);
        zzbfp.m10185(parcel, 2, this.f6919);
        zzbfp.m10187(parcel, 3, this.f6920, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m7731() {
        return this.f6919;
    }
}
