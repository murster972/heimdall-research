package com.google.android.gms.location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.internal.zzcfk;

public abstract class LocationServices$zza<R extends Result> extends zzm<R, zzcfk> {
    public LocationServices$zza(GoogleApiClient googleApiClient) {
        super(LocationServices.API, googleApiClient);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m13676(Object obj) {
        super.m4198((Result) obj);
    }
}
