package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.Arrays;

public final class LocationRequest extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<LocationRequest> CREATOR = new zzw();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f11042;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f11043;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f11044;

    /* renamed from: 连任  reason: contains not printable characters */
    private long f11045;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f11046;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f11047;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f11048;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f11049;

    public LocationRequest() {
        this.f11049 = 102;
        this.f11046 = 3600000;
        this.f11048 = 600000;
        this.f11047 = false;
        this.f11045 = Long.MAX_VALUE;
        this.f11042 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        this.f11043 = 0.0f;
        this.f11044 = 0;
    }

    LocationRequest(int i, long j, long j2, boolean z, long j3, int i2, float f, long j4) {
        this.f11049 = i;
        this.f11046 = j;
        this.f11048 = j2;
        this.f11047 = z;
        this.f11045 = j3;
        this.f11042 = i2;
        this.f11043 = f;
        this.f11044 = j4;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m13670(long j) {
        if (j < 0) {
            throw new IllegalArgumentException(new StringBuilder(38).append("invalid interval: ").append(j).toString());
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocationRequest)) {
            return false;
        }
        LocationRequest locationRequest = (LocationRequest) obj;
        return this.f11049 == locationRequest.f11049 && this.f11046 == locationRequest.f11046 && this.f11048 == locationRequest.f11048 && this.f11047 == locationRequest.f11047 && this.f11045 == locationRequest.f11045 && this.f11042 == locationRequest.f11042 && this.f11043 == locationRequest.f11043 && m13672() == locationRequest.m13672();
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f11049), Long.valueOf(this.f11046), Float.valueOf(this.f11043), Long.valueOf(this.f11044)});
    }

    public final String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        StringBuilder append = sb.append("Request[");
        switch (this.f11049) {
            case 100:
                str = "PRIORITY_HIGH_ACCURACY";
                break;
            case 102:
                str = "PRIORITY_BALANCED_POWER_ACCURACY";
                break;
            case 104:
                str = "PRIORITY_LOW_POWER";
                break;
            case 105:
                str = "PRIORITY_NO_POWER";
                break;
            default:
                str = "???";
                break;
        }
        append.append(str);
        if (this.f11049 != 105) {
            sb.append(" requested=");
            sb.append(this.f11046).append("ms");
        }
        sb.append(" fastest=");
        sb.append(this.f11048).append("ms");
        if (this.f11044 > this.f11046) {
            sb.append(" maxWait=");
            sb.append(this.f11044).append("ms");
        }
        if (this.f11043 > 0.0f) {
            sb.append(" smallestDisplacement=");
            sb.append(this.f11043).append("m");
        }
        if (this.f11045 != Long.MAX_VALUE) {
            sb.append(" expireIn=");
            sb.append(this.f11045 - SystemClock.elapsedRealtime()).append("ms");
        }
        if (this.f11042 != Integer.MAX_VALUE) {
            sb.append(" num=").append(this.f11042);
        }
        sb.append(']');
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f11049);
        zzbfp.m10186(parcel, 2, this.f11046);
        zzbfp.m10186(parcel, 3, this.f11048);
        zzbfp.m10195(parcel, 4, this.f11047);
        zzbfp.m10186(parcel, 5, this.f11045);
        zzbfp.m10185(parcel, 6, this.f11042);
        zzbfp.m10184(parcel, 7, this.f11043);
        zzbfp.m10186(parcel, 8, this.f11044);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final LocationRequest m13671(long j) {
        m13670(j);
        this.f11047 = true;
        this.f11048 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m13672() {
        long j = this.f11044;
        return j < this.f11046 ? this.f11046 : j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final LocationRequest m13673(int i) {
        switch (i) {
            case 100:
            case 102:
            case 104:
            case 105:
                this.f11049 = i;
                return this;
            default:
                throw new IllegalArgumentException(new StringBuilder(28).append("invalid quality: ").append(i).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final LocationRequest m13674(long j) {
        m13670(j);
        this.f11046 = j;
        if (!this.f11047) {
            this.f11048 = (long) (((double) this.f11046) / 6.0d);
        }
        return this;
    }
}
