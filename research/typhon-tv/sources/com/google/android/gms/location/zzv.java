package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzv implements Parcelable.Creator<LocationAvailability> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        int i = 1000;
        long j = 0;
        zzae[] zzaeArr = null;
        int i2 = 1;
        int i3 = 1;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    zzaeArr = (zzae[]) zzbfn.m10165(parcel, readInt, zzae.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new LocationAvailability(i, i3, i2, j, zzaeArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationAvailability[i];
    }
}
