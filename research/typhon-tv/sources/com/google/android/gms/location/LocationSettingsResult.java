package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class LocationSettingsResult extends zzbfm implements Result {
    public static final Parcelable.Creator<LocationSettingsResult> CREATOR = new zzac();

    /* renamed from: 靐  reason: contains not printable characters */
    private final LocationSettingsStates f11060;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Status f11061;

    public LocationSettingsResult(Status status) {
        this(status, (LocationSettingsStates) null);
    }

    public LocationSettingsResult(Status status, LocationSettingsStates locationSettingsStates) {
        this.f11061 = status;
        this.f11060 = locationSettingsStates;
    }

    public final Status s_() {
        return this.f11061;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 1, (Parcelable) s_(), i, false);
        zzbfp.m10189(parcel, 2, (Parcelable) m13679(), i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final LocationSettingsStates m13679() {
        return this.f11060;
    }
}
