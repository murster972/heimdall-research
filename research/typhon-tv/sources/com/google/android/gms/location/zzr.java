package com.google.android.gms.location;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzr extends zzeu implements zzp {
    zzr(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13692(LocationAvailability locationAvailability) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) locationAvailability);
        m12299(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13693(LocationResult locationResult) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) locationResult);
        m12299(1, v_);
    }
}
