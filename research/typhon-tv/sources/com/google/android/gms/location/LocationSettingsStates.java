package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class LocationSettingsStates extends zzbfm {
    public static final Parcelable.Creator<LocationSettingsStates> CREATOR = new zzad();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f11062;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f11063;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f11064;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f11065;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f11066;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f11067;

    public LocationSettingsStates(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.f11067 = z;
        this.f11064 = z2;
        this.f11066 = z3;
        this.f11065 = z4;
        this.f11063 = z5;
        this.f11062 = z6;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 1, m13685());
        zzbfp.m10195(parcel, 2, m13684());
        zzbfp.m10195(parcel, 3, m13681());
        zzbfp.m10195(parcel, 4, m13682());
        zzbfp.m10195(parcel, 5, m13683());
        zzbfp.m10195(parcel, 6, m13680());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m13680() {
        return this.f11062;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m13681() {
        return this.f11066;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13682() {
        return this.f11065;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m13683() {
        return this.f11063;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m13684() {
        return this.f11064;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13685() {
        return this.f11067;
    }
}
