package com.google.android.gms.location;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class LocationResult extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<LocationResult> CREATOR = new zzx();

    /* renamed from: 龘  reason: contains not printable characters */
    static final List<Location> f11050 = Collections.emptyList();

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<Location> f11051;

    LocationResult(List<Location> list) {
        this.f11051 = list;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof LocationResult)) {
            return false;
        }
        LocationResult locationResult = (LocationResult) obj;
        if (locationResult.f11051.size() != this.f11051.size()) {
            return false;
        }
        Iterator<Location> it2 = this.f11051.iterator();
        for (Location time : locationResult.f11051) {
            if (it2.next().getTime() != time.getTime()) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = 17;
        Iterator<Location> it2 = this.f11051.iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return i2;
            }
            long time = it2.next().getTime();
            i = ((int) (time ^ (time >>> 32))) + (i2 * 31);
        }
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f11051);
        return new StringBuilder(String.valueOf(valueOf).length() + 27).append("LocationResult[locations: ").append(valueOf).append("]").toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10180(parcel, 1, m13675(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<Location> m13675() {
        return this.f11051;
    }
}
