package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import com.mopub.nativeads.MoPubNativeAdPositioning;

public final class zzw implements Parcelable.Creator<LocationRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        int i = 102;
        long j = 3600000;
        long j2 = 600000;
        boolean z = false;
        long j3 = Long.MAX_VALUE;
        int i2 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        float f = 0.0f;
        long j4 = 0;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    j3 = zzbfn.m10163(parcel, readInt);
                    break;
                case 6:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 7:
                    f = zzbfn.m10151(parcel, readInt);
                    break;
                case 8:
                    j4 = zzbfn.m10163(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new LocationRequest(i, j, j2, z, j3, i2, f, j4);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationRequest[i];
    }
}
