package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;
import java.util.Comparator;

public class DetectedActivity extends zzbfm {
    public static final Parcelable.Creator<DetectedActivity> CREATOR = new zzd();
    public static final int IN_VEHICLE = 0;
    public static final int ON_BICYCLE = 1;
    public static final int ON_FOOT = 2;
    public static final int RUNNING = 8;
    public static final int STILL = 3;
    public static final int TILTING = 5;
    public static final int UNKNOWN = 4;
    public static final int WALKING = 7;

    /* renamed from: 靐  reason: contains not printable characters */
    private static int[] f11037 = {9, 10};

    /* renamed from: 齉  reason: contains not printable characters */
    private static int[] f11038 = {0, 1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 16, 17};

    /* renamed from: 龘  reason: contains not printable characters */
    private static Comparator<DetectedActivity> f11039 = new zzc();

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11040;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f11041;

    public DetectedActivity(int i, int i2) {
        this.f11041 = i;
        this.f11040 = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DetectedActivity detectedActivity = (DetectedActivity) obj;
        return this.f11041 == detectedActivity.f11041 && this.f11040 == detectedActivity.f11040;
    }

    public int getConfidence() {
        return this.f11040;
    }

    public int getType() {
        int i = this.f11041;
        if (i > 17) {
            return 4;
        }
        return i;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f11041), Integer.valueOf(this.f11040)});
    }

    public String toString() {
        String str;
        int type = getType();
        switch (type) {
            case 0:
                str = "IN_VEHICLE";
                break;
            case 1:
                str = "ON_BICYCLE";
                break;
            case 2:
                str = "ON_FOOT";
                break;
            case 3:
                str = "STILL";
                break;
            case 4:
                str = "UNKNOWN";
                break;
            case 5:
                str = "TILTING";
                break;
            case 7:
                str = "WALKING";
                break;
            case 8:
                str = "RUNNING";
                break;
            case 16:
                str = "IN_ROAD_VEHICLE";
                break;
            case 17:
                str = "IN_RAIL_VEHICLE";
                break;
            default:
                str = Integer.toString(type);
                break;
        }
        return new StringBuilder(String.valueOf(str).length() + 48).append("DetectedActivity [type=").append(str).append(", confidence=").append(this.f11040).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f11041);
        zzbfp.m10185(parcel, 2, this.f11040);
        zzbfp.m10182(parcel, r0);
    }
}
