package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class LocationSettingsRequest extends zzbfm {
    public static final Parcelable.Creator<LocationSettingsRequest> CREATOR = new zzab();

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f11052;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzz f11053;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f11054;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<LocationRequest> f11055;

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f11056 = false;

        /* renamed from: 麤  reason: contains not printable characters */
        private zzz f11057 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f11058 = false;

        /* renamed from: 龘  reason: contains not printable characters */
        private final ArrayList<LocationRequest> f11059 = new ArrayList<>();

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m13677(LocationRequest locationRequest) {
            if (locationRequest != null) {
                this.f11059.add(locationRequest);
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final LocationSettingsRequest m13678() {
            return new LocationSettingsRequest(this.f11059, this.f11056, this.f11058, (zzz) null);
        }
    }

    LocationSettingsRequest(List<LocationRequest> list, boolean z, boolean z2, zzz zzz) {
        this.f11055 = list;
        this.f11052 = z;
        this.f11054 = z2;
        this.f11053 = zzz;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10180(parcel, 1, Collections.unmodifiableList(this.f11055), false);
        zzbfp.m10195(parcel, 2, this.f11052);
        zzbfp.m10195(parcel, 3, this.f11054);
        zzbfp.m10189(parcel, 5, (Parcelable) this.f11053, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
