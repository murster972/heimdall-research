package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import com.mopub.nativeads.MoPubNativeAdPositioning;

public final class zzf implements Parcelable.Creator<zze> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r0 = zzbfn.m10169(parcel);
        boolean z = true;
        long j = 50;
        float f = 0.0f;
        long j2 = Long.MAX_VALUE;
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        while (parcel.dataPosition() < r0) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 2:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    f = zzbfn.m10151(parcel, readInt);
                    break;
                case 4:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 5:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r0);
        return new zze(z, j, f, j2, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zze[i];
    }
}
