package com.google.android.gms.location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.internal.zzcfk;

public abstract class ActivityRecognition$zza<R extends Result> extends zzm<R, zzcfk> {
    public ActivityRecognition$zza(GoogleApiClient googleApiClient) {
        super(ActivityRecognition.API, googleApiClient);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m13664(Object obj) {
        super.m4198((Result) obj);
    }
}
