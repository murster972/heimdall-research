package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzad implements Parcelable.Creator<LocationSettingsStates> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    z6 = zzbfn.m10168(parcel, readInt);
                    break;
                case 2:
                    z5 = zzbfn.m10168(parcel, readInt);
                    break;
                case 3:
                    z4 = zzbfn.m10168(parcel, readInt);
                    break;
                case 4:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new LocationSettingsStates(z6, z5, z4, z3, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationSettingsStates[i];
    }
}
