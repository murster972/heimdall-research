package com.google.android.gms.location;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzu extends zzeu implements zzs {
    zzu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13696(Location location) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) location);
        m12299(1, v_);
    }
}
