package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzaf implements Parcelable.Creator<zzae> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r9 = zzbfn.m10169(parcel);
        long j = -1;
        long j2 = -1;
        int i = 1;
        int i2 = 1;
        while (parcel.dataPosition() < r9) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r9);
        return new zzae(i2, i, j2, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzae[i];
    }
}
