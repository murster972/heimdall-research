package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzz extends zzbfm {
    public static final Parcelable.Creator<zzz> CREATOR = new zzaa();

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f11077;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f11078;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f11079;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f11080;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f11081;

    zzz(String str, String str2, String str3, int i, boolean z) {
        this.f11077 = str;
        this.f11081 = str2;
        this.f11078 = str3;
        this.f11080 = i;
        this.f11079 = z;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 1, this.f11081, false);
        zzbfp.m10193(parcel, 2, this.f11078, false);
        zzbfp.m10185(parcel, 3, this.f11080);
        zzbfp.m10195(parcel, 4, this.f11079);
        zzbfp.m10193(parcel, 5, this.f11077, false);
        zzbfp.m10182(parcel, r0);
    }
}
