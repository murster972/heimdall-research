package com.google.android.gms.location;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzp extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m13689(LocationAvailability locationAvailability) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13690(LocationResult locationResult) throws RemoteException;
}
