package com.google.android.gms.location;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;
import java.util.List;

public final class zzb implements Parcelable.Creator<ActivityRecognitionResult> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r10 = zzbfn.m10169(parcel);
        int i = 0;
        Bundle bundle = null;
        long j = 0;
        long j2 = 0;
        ArrayList<DetectedActivity> arrayList = null;
        while (parcel.dataPosition() < r10) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    arrayList = zzbfn.m10167(parcel, readInt, DetectedActivity.CREATOR);
                    break;
                case 2:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r10);
        return new ActivityRecognitionResult((List<DetectedActivity>) arrayList, j2, j, i, bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ActivityRecognitionResult[i];
    }
}
