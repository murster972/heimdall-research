package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzab implements Parcelable.Creator<LocationSettingsRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r6 = zzbfn.m10169(parcel);
        zzz zzz = null;
        boolean z = false;
        boolean z2 = false;
        ArrayList<LocationRequest> arrayList = null;
        while (parcel.dataPosition() < r6) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    arrayList = zzbfn.m10167(parcel, readInt, LocationRequest.CREATOR);
                    break;
                case 2:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 3:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    zzz = (zzz) zzbfn.m10171(parcel, readInt, zzz.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r6);
        return new LocationSettingsRequest(arrayList, z2, z, zzz);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationSettingsRequest[i];
    }
}
