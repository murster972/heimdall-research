package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzaa implements Parcelable.Creator<zzz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r0 = zzbfn.m10169(parcel);
        String str = "";
        String str2 = "";
        String str3 = "";
        int i = 0;
        boolean z = true;
        while (parcel.dataPosition() < r0) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 2:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r0);
        return new zzz(str, str2, str3, i, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzz[i];
    }
}
