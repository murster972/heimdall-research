package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;

public final class zzae extends zzbfm {
    public static final Parcelable.Creator<zzae> CREATOR = new zzaf();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f11068;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f11069;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f11070;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f11071;

    zzae(int i, int i2, long j, long j2) {
        this.f11071 = i;
        this.f11068 = i2;
        this.f11070 = j;
        this.f11069 = j2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        zzae zzae = (zzae) obj;
        return this.f11071 == zzae.f11071 && this.f11068 == zzae.f11068 && this.f11070 == zzae.f11070 && this.f11069 == zzae.f11069;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f11068), Integer.valueOf(this.f11071), Long.valueOf(this.f11069), Long.valueOf(this.f11070)});
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("NetworkLocationStatus:");
        sb.append(" Wifi status: ").append(this.f11071).append(" Cell status: ").append(this.f11068).append(" elapsed time NS: ").append(this.f11069).append(" system time ms: ").append(this.f11070);
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f11071);
        zzbfp.m10185(parcel, 2, this.f11068);
        zzbfp.m10186(parcel, 3, this.f11070);
        zzbfp.m10186(parcel, 4, this.f11069);
        zzbfp.m10182(parcel, r0);
    }
}
