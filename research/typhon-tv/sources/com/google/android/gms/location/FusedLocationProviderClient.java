package com.google.android.gms.location;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.zzcz;
import com.google.android.gms.common.api.internal.zzg;
import com.google.android.gms.tasks.Task;

public class FusedLocationProviderClient extends GoogleApi<Object> {
    public FusedLocationProviderClient(Activity activity) {
        super(activity, LocationServices.API, null, (zzcz) new zzg());
    }

    public FusedLocationProviderClient(Context context) {
        super(context, LocationServices.API, null, (zzcz) new zzg());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Task<Location> m13667() {
        return m4184(new zzg(this));
    }
}
