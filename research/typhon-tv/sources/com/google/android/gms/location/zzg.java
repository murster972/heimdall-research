package com.google.android.gms.location;

import android.location.Location;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdd;
import com.google.android.gms.internal.zzcfk;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzg extends zzdd<zzcfk, Location> {
    zzg(FusedLocationProviderClient fusedLocationProviderClient) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m13687(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        taskCompletionSource.m13722(((zzcfk) zzb).m10403());
    }
}
