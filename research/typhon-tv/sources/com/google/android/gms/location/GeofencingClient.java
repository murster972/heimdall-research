package com.google.android.gms.location;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.zzcz;
import com.google.android.gms.common.api.internal.zzg;

public class GeofencingClient extends GoogleApi<Object> {
    public GeofencingClient(Activity activity) {
        super(activity, LocationServices.API, null, (zzcz) new zzg());
    }

    public GeofencingClient(Context context) {
        super(context, LocationServices.API, null, (zzcz) new zzg());
    }
}
