package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.Arrays;

public final class zze extends zzbfm {
    public static final Parcelable.Creator<zze> CREATOR = new zzf();

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11072;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f11073;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f11074;

    /* renamed from: 齉  reason: contains not printable characters */
    private float f11075;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f11076;

    public zze() {
        this(true, 50, 0.0f, Long.MAX_VALUE, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    zze(boolean z, long j, float f, long j2, int i) {
        this.f11076 = z;
        this.f11073 = j;
        this.f11075 = f;
        this.f11074 = j2;
        this.f11072 = i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zze)) {
            return false;
        }
        zze zze = (zze) obj;
        return this.f11076 == zze.f11076 && this.f11073 == zze.f11073 && Float.compare(this.f11075, zze.f11075) == 0 && this.f11074 == zze.f11074 && this.f11072 == zze.f11072;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Boolean.valueOf(this.f11076), Long.valueOf(this.f11073), Float.valueOf(this.f11075), Long.valueOf(this.f11074), Integer.valueOf(this.f11072)});
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOrientationRequest[mShouldUseMag=").append(this.f11076);
        sb.append(" mMinimumSamplingPeriodMs=").append(this.f11073);
        sb.append(" mSmallestAngleChangeRadians=").append(this.f11075);
        if (this.f11074 != Long.MAX_VALUE) {
            sb.append(" expireIn=");
            sb.append(this.f11074 - SystemClock.elapsedRealtime()).append("ms");
        }
        if (this.f11072 != Integer.MAX_VALUE) {
            sb.append(" num=").append(this.f11072);
        }
        sb.append(']');
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 1, this.f11076);
        zzbfp.m10186(parcel, 2, this.f11073);
        zzbfp.m10184(parcel, 3, this.f11075);
        zzbfp.m10186(parcel, 4, this.f11074);
        zzbfp.m10185(parcel, 5, this.f11072);
        zzbfp.m10182(parcel, r0);
    }
}
