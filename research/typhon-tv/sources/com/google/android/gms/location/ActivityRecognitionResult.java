package com.google.android.gms.location;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.google.android.gms.internal.zzbfr;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ActivityRecognitionResult extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<ActivityRecognitionResult> CREATOR = new zzb();

    /* renamed from: 连任  reason: contains not printable characters */
    private Bundle f11032;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f11033;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f11034;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f11035;

    /* renamed from: 龘  reason: contains not printable characters */
    private List<DetectedActivity> f11036;

    public ActivityRecognitionResult(DetectedActivity detectedActivity, long j, long j2) {
        this(detectedActivity, j, j2, 0, (Bundle) null);
    }

    private ActivityRecognitionResult(DetectedActivity detectedActivity, long j, long j2, int i, Bundle bundle) {
        this((List<DetectedActivity>) Collections.singletonList(detectedActivity), j, j2, 0, (Bundle) null);
    }

    public ActivityRecognitionResult(List<DetectedActivity> list, long j, long j2) {
        this(list, j, j2, 0, (Bundle) null);
    }

    public ActivityRecognitionResult(List<DetectedActivity> list, long j, long j2, int i, Bundle bundle) {
        boolean z = true;
        zzbq.m9117(list != null && list.size() > 0, "Must have at least 1 detected activity");
        zzbq.m9117((j <= 0 || j2 <= 0) ? false : z, "Must set times");
        this.f11036 = list;
        this.f11033 = j;
        this.f11035 = j2;
        this.f11034 = i;
        this.f11032 = bundle;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.location.ActivityRecognitionResult extractResult(android.content.Intent r3) {
        /*
            r1 = 0
            boolean r0 = hasResult(r3)
            if (r0 == 0) goto L_0x002a
            android.os.Bundle r0 = r3.getExtras()
            java.lang.String r2 = "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"
            java.lang.Object r0 = r0.get(r2)
            boolean r2 = r0 instanceof byte[]
            if (r2 == 0) goto L_0x0023
            byte[] r0 = (byte[]) r0
            android.os.Parcelable$Creator<com.google.android.gms.location.ActivityRecognitionResult> r2 = CREATOR
            com.google.android.gms.internal.zzbfq r0 = com.google.android.gms.internal.zzbfr.m10202(r0, r2)
            com.google.android.gms.location.ActivityRecognitionResult r0 = (com.google.android.gms.location.ActivityRecognitionResult) r0
        L_0x0020:
            if (r0 == 0) goto L_0x002c
        L_0x0022:
            return r0
        L_0x0023:
            boolean r2 = r0 instanceof com.google.android.gms.location.ActivityRecognitionResult
            if (r2 == 0) goto L_0x002a
            com.google.android.gms.location.ActivityRecognitionResult r0 = (com.google.android.gms.location.ActivityRecognitionResult) r0
            goto L_0x0020
        L_0x002a:
            r0 = r1
            goto L_0x0020
        L_0x002c:
            java.util.List r0 = m13665(r3)
            if (r0 == 0) goto L_0x0038
            boolean r2 = r0.isEmpty()
            if (r2 == 0) goto L_0x003a
        L_0x0038:
            r0 = r1
            goto L_0x0022
        L_0x003a:
            int r1 = r0.size()
            int r1 = r1 + -1
            java.lang.Object r0 = r0.get(r1)
            com.google.android.gms.location.ActivityRecognitionResult r0 = (com.google.android.gms.location.ActivityRecognitionResult) r0
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.location.ActivityRecognitionResult.extractResult(android.content.Intent):com.google.android.gms.location.ActivityRecognitionResult");
    }

    public static boolean hasResult(Intent intent) {
        if (intent == null) {
            return false;
        }
        if (intent == null ? false : intent.hasExtra("com.google.android.location.internal.EXTRA_ACTIVITY_RESULT")) {
            return true;
        }
        List<ActivityRecognitionResult> r2 = m13665(intent);
        return r2 != null && !r2.isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<ActivityRecognitionResult> m13665(Intent intent) {
        if (!(intent == null ? false : intent.hasExtra("com.google.android.location.internal.EXTRA_ACTIVITY_RESULT_LIST"))) {
            return null;
        }
        return zzbfr.m10203(intent, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT_LIST", CREATOR);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m13666(Bundle bundle, Bundle bundle2) {
        if (bundle == null && bundle2 == null) {
            return true;
        }
        if ((bundle == null && bundle2 != null) || (bundle != null && bundle2 == null)) {
            return false;
        }
        if (bundle.size() != bundle2.size()) {
            return false;
        }
        for (String str : bundle.keySet()) {
            if (!bundle2.containsKey(str)) {
                return false;
            }
            if (bundle.get(str) == null) {
                if (bundle2.get(str) != null) {
                    return false;
                }
            } else if (bundle.get(str) instanceof Bundle) {
                if (!m13666(bundle.getBundle(str), bundle2.getBundle(str))) {
                    return false;
                }
            } else if (!bundle.get(str).equals(bundle2.get(str))) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ActivityRecognitionResult activityRecognitionResult = (ActivityRecognitionResult) obj;
        return this.f11033 == activityRecognitionResult.f11033 && this.f11035 == activityRecognitionResult.f11035 && this.f11034 == activityRecognitionResult.f11034 && zzbg.m9113(this.f11036, activityRecognitionResult.f11036) && m13666(this.f11032, activityRecognitionResult.f11032);
    }

    public int getActivityConfidence(int i) {
        for (DetectedActivity next : this.f11036) {
            if (next.getType() == i) {
                return next.getConfidence();
            }
        }
        return 0;
    }

    public long getElapsedRealtimeMillis() {
        return this.f11035;
    }

    public DetectedActivity getMostProbableActivity() {
        return this.f11036.get(0);
    }

    public List<DetectedActivity> getProbableActivities() {
        return this.f11036;
    }

    public long getTime() {
        return this.f11033;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.f11033), Long.valueOf(this.f11035), Integer.valueOf(this.f11034), this.f11036, this.f11032});
    }

    public String toString() {
        String valueOf = String.valueOf(this.f11036);
        long j = this.f11033;
        return new StringBuilder(String.valueOf(valueOf).length() + 124).append("ActivityRecognitionResult [probableActivities=").append(valueOf).append(", timeMillis=").append(j).append(", elapsedRealtimeMillis=").append(this.f11035).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10180(parcel, 1, this.f11036, false);
        zzbfp.m10186(parcel, 2, this.f11033);
        zzbfp.m10186(parcel, 3, this.f11035);
        zzbfp.m10185(parcel, 4, this.f11034);
        zzbfp.m10187(parcel, 5, this.f11032, false);
        zzbfp.m10182(parcel, r0);
    }
}
