package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import java.util.Set;

@TargetApi(11)
public class zzahw extends zzahv {
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9610(View view) {
        view.setLayerType(0, (Paint) null);
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public WebChromeClient m9611(zzanh zzanh) {
        return new zzaoi(zzanh);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m9612(View view) {
        view.setLayerType(1, (Paint) null);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public zzani m9613(zzanh zzanh, boolean z) {
        return new zzaoq(zzanh, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Set<String> m9614(Uri uri) {
        return uri.getQueryParameterNames();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9615(DownloadManager.Request request) {
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(1);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m9616(Context context, WebSettings webSettings) {
        super.m4662(context, webSettings);
        return ((Boolean) zzajk.m4728(context, new zzahx(this, context, webSettings))).booleanValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9617(Window window) {
        window.setFlags(16777216, 16777216);
        return true;
    }
}
