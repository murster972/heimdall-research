package com.google.android.gms.internal;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

public interface zzakv<A> extends Future<A> {
    /* renamed from: 龘  reason: contains not printable characters */
    void m9670(Runnable runnable, Executor executor);
}
