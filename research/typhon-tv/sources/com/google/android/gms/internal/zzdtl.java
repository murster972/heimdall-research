package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtn;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdtl extends zzffu<zzdtl, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdtl f10143;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdtl> f10144;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdtn f10145;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10146;

    public static final class zza extends zzffu.zza<zzdtl, zza> implements zzfhg {
        private zza() {
            super(zzdtl.f10143);
        }

        /* synthetic */ zza(zzdtm zzdtm) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12124(int i) {
            m12541();
            ((zzdtl) this.f10398).m12115(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12125(zzdtn zzdtn) {
            m12541();
            ((zzdtl) this.f10398).m12118(zzdtn);
            return this;
        }
    }

    static {
        zzdtl zzdtl = new zzdtl();
        f10143 = zzdtl;
        zzdtl.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdtl.f10394.m12718();
    }

    private zzdtl() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zza m12113() {
        zzdtl zzdtl = f10143;
        zzffu.zza zza2 = (zzffu.zza) zzdtl.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdtl);
        return (zza) zza2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdtl m12114(zzfes zzfes) throws zzfge {
        return (zzdtl) zzffu.m12526(f10143, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12115(int i) {
        this.f10146 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12118(zzdtn zzdtn) {
        if (zzdtn == null) {
            throw new NullPointerException();
        }
        this.f10145 = zzdtn;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdtn m12119() {
        return this.f10145 == null ? zzdtn.m12126() : this.f10145;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12120() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10146 != 0) {
            i2 = zzffg.m5245(1, this.f10146) + 0;
        }
        if (this.f10145 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10145 == null ? zzdtn.m12126() : this.f10145);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12121() {
        return this.f10146;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m12122(int i, Object obj, Object obj2) {
        zzdtn.zza zza2;
        boolean z = true;
        switch (zzdtm.f10147[i - 1]) {
            case 1:
                return new zzdtl();
            case 2:
                return f10143;
            case 3:
                return null;
            case 4:
                return new zza((zzdtm) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdtl zzdtl = (zzdtl) obj2;
                boolean z2 = this.f10146 != 0;
                int i2 = this.f10146;
                if (zzdtl.f10146 == 0) {
                    z = false;
                }
                this.f10146 = zzh.m12569(z2, i2, z, zzdtl.f10146);
                this.f10145 = (zzdtn) zzh.m12572(this.f10145, zzdtl.f10145);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f10146 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f10145 != null) {
                                        zzdtn zzdtn = this.f10145;
                                        zzffu.zza zza3 = (zzffu.zza) zzdtn.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdtn);
                                        zza2 = (zzdtn.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10145 = (zzdtn) zzffb.m12416(zzdtn.m12126(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10145);
                                        this.f10145 = (zzdtn) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10144 == null) {
                    synchronized (zzdtl.class) {
                        if (f10144 == null) {
                            f10144 = new zzffu.zzb(f10143);
                        }
                    }
                }
                return f10144;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10143;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12123(zzffg zzffg) throws IOException {
        if (this.f10146 != 0) {
            zzffg.m5281(1, this.f10146);
        }
        if (this.f10145 != null) {
            zzffg.m5289(2, (zzfhe) this.f10145 == null ? zzdtn.m12126() : this.f10145);
        }
        this.f10394.m12719(zzffg);
    }
}
