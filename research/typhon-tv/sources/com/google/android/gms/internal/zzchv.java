package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.android.gms.common.internal.zzbq;

class zzchv extends BroadcastReceiver {

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f9294 = zzchv.class.getName();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzcim f9295;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f9296;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f9297;

    zzchv(zzcim zzcim) {
        zzbq.m9120(zzcim);
        this.f9295 = zzcim;
    }

    public void onReceive(Context context, Intent intent) {
        this.f9295.m11051();
        String action = intent.getAction();
        this.f9295.m11016().m10848().m10850("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean r0 = this.f9295.m11026().m10872();
            if (this.f9296 != r0) {
                this.f9296 = r0;
                this.f9295.m11018().m10986((Runnable) new zzchw(this, r0));
                return;
            }
            return;
        }
        this.f9295.m11016().m10834().m10850("NetworkBroadcastReceiver received unknown action", action);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10882() {
        this.f9295.m11051();
        this.f9295.m11018().m11109();
        this.f9295.m11018().m11109();
        if (this.f9297) {
            this.f9295.m11016().m10848().m10849("Unregistering connectivity change receiver");
            this.f9297 = false;
            this.f9296 = false;
            try {
                this.f9295.m11021().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.f9295.m11016().m10832().m10850("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10883() {
        this.f9295.m11051();
        this.f9295.m11018().m11109();
        if (!this.f9297) {
            this.f9295.m11021().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.f9296 = this.f9295.m11026().m10872();
            this.f9295.m11016().m10848().m10850("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.f9296));
            this.f9297 = true;
        }
    }
}
