package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionProvider;

public final class zzayv extends SessionProvider {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzazm f8409;

    /* renamed from: 龘  reason: contains not printable characters */
    private final CastOptions f8410;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzayv(Context context, CastOptions castOptions, zzazm zzazm) {
        super(context, castOptions.m7990().isEmpty() ? CastMediaControlIntent.m7828(castOptions.m7993()) : CastMediaControlIntent.m7830(castOptions.m7993(), castOptions.m7990()));
        this.f8410 = castOptions;
        this.f8409 = zzazm;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m9767() {
        return this.f8410.m7989();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Session m9768(String str) {
        return new CastSession(m8096(), m8093(), str, this.f8410, Cast.f6961, new zzayw(), new zzazy(m8096(), this.f8410, this.f8409));
    }
}
