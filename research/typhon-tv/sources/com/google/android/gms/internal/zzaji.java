package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzaji {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f4258 = Long.MIN_VALUE;

    /* renamed from: 齉  reason: contains not printable characters */
    private Object f4259 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private long f4260;

    public zzaji(long j) {
        this.f4260 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4724() {
        boolean z;
        synchronized (this.f4259) {
            long r2 = zzbs.zzeo().m9241();
            if (this.f4258 + this.f4260 > r2) {
                z = false;
            } else {
                this.f4258 = r2;
                z = true;
            }
        }
        return z;
    }
}
