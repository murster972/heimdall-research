package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzfjs {

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    protected volatile int f10549 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final <T extends zzfjs> T m12869(T t, byte[] bArr) throws zzfjr {
        return m12870(t, bArr, 0, bArr.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T extends zzfjs> T m12870(T t, byte[] bArr, int i, int i2) throws zzfjr {
        try {
            zzfjj r0 = zzfjj.m12783(bArr, 0, i2);
            t.m12876(r0);
            r0.m12801(0);
            return t;
        } catch (zzfjr e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final byte[] m12871(zzfjs zzfjs) {
        byte[] bArr = new byte[zzfjs.m12872()];
        try {
            zzfjk r1 = zzfjk.m12822(bArr, 0, bArr.length);
            zzfjs.m12877(r1);
            r1.m12829();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public String toString() {
        return zzfjt.m12879(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int m12872() {
        int r0 = m12875();
        this.f10549 = r0;
        return r0;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m12873() {
        if (this.f10549 < 0) {
            m12872();
        }
        return this.f10549;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public zzfjs clone() throws CloneNotSupportedException {
        return (zzfjs) super.clone();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m12875() {
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzfjs m12876(zzfjj zzfjj) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public void m12877(zzfjk zzfjk) throws IOException {
    }
}
