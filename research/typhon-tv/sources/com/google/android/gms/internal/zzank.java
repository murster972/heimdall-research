package com.google.android.gms.internal;

import android.view.View;

final class zzank implements View.OnAttachStateChangeListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzani f8343;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzafb f8344;

    zzank(zzani zzani, zzafb zzafb) {
        this.f8343 = zzani;
        this.f8344 = zzafb;
    }

    public final void onViewAttachedToWindow(View view) {
        this.f8343.m5024(view, this.f8344, 10);
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
