package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import org.json.JSONObject;

@zzzv
public final class zzafp {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final long f4127;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final long f4128;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final JSONObject f4129;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzis f4130;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean f4131;

    /* renamed from: 连任  reason: contains not printable characters */
    public final int f4132;

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzaax f4133;

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzjn f4134;

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzui f4135;

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaat f4136;

    public zzafp(zzaat zzaat, zzaax zzaax, zzui zzui, zzjn zzjn, int i, long j, long j2, JSONObject jSONObject, zzis zzis, Boolean bool) {
        this.f4136 = zzaat;
        this.f4133 = zzaax;
        this.f4135 = zzui;
        this.f4134 = zzjn;
        this.f4132 = i;
        this.f4127 = j;
        this.f4128 = j2;
        this.f4129 = jSONObject;
        this.f4130 = zzis;
        if (bool != null) {
            this.f4131 = bool.booleanValue();
            return;
        }
        zzbs.zzei();
        if (zzahn.m4620(zzaat.f3763)) {
            this.f4131 = true;
        } else {
            this.f4131 = false;
        }
    }

    public zzafp(zzaat zzaat, zzaax zzaax, zzui zzui, zzjn zzjn, int i, long j, long j2, JSONObject jSONObject, zzix zzix) {
        this.f4136 = zzaat;
        this.f4133 = zzaax;
        this.f4135 = null;
        this.f4134 = null;
        this.f4132 = i;
        this.f4127 = j;
        this.f4128 = j2;
        this.f4129 = null;
        this.f4130 = new zzis(zzix, ((Boolean) zzkb.m5481().m5595(zznh.f5040)).booleanValue());
        this.f4131 = false;
    }
}
