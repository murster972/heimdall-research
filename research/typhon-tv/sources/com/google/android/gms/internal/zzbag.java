package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.ImagePicker;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbag extends UIController {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzazn f8558;

    /* renamed from: 连任  reason: contains not printable characters */
    private final ImagePicker f8559;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ImageHints f8560;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final View f8561;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Bitmap f8562;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final ImageView f8563;

    public zzbag(ImageView imageView, Context context, ImageHints imageHints, int i, View view) {
        ImagePicker imagePicker = null;
        this.f8563 = imageView;
        this.f8560 = imageHints;
        this.f8562 = i != 0 ? BitmapFactory.decodeResource(context.getResources(), i) : null;
        this.f8561 = view;
        CastMediaOptions r0 = CastContext.m7977(context).m7984().m7986();
        this.f8559 = r0 != null ? r0.m8136() : imagePicker;
        this.f8558 = new zzazn(context.getApplicationContext());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m9881() {
        if (this.f8561 != null) {
            this.f8561.setVisibility(0);
            this.f8563.setVisibility(4);
        }
        if (this.f8562 != null) {
            this.f8563.setImageBitmap(this.f8562);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        r1 = r4.f8559.m4121(r0.m7846(), r4.f8560);
     */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m9882() {
        /*
            r4 = this;
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r4.m8226()
            if (r0 == 0) goto L_0x000c
            boolean r1 = r0.m4143()
            if (r1 != 0) goto L_0x0010
        L_0x000c:
            r4.m9881()
        L_0x000f:
            return
        L_0x0010:
            com.google.android.gms.cast.MediaInfo r0 = r0.m4137()
            if (r0 != 0) goto L_0x001d
            r0 = 0
        L_0x0017:
            if (r0 != 0) goto L_0x0040
            r4.m9881()
            goto L_0x000f
        L_0x001d:
            com.google.android.gms.cast.framework.media.ImagePicker r1 = r4.f8559
            if (r1 == 0) goto L_0x003a
            com.google.android.gms.cast.framework.media.ImagePicker r1 = r4.f8559
            com.google.android.gms.cast.MediaMetadata r2 = r0.m7846()
            com.google.android.gms.cast.framework.media.ImageHints r3 = r4.f8560
            com.google.android.gms.common.images.WebImage r1 = r1.m4121((com.google.android.gms.cast.MediaMetadata) r2, (com.google.android.gms.cast.framework.media.ImageHints) r3)
            if (r1 == 0) goto L_0x003a
            android.net.Uri r2 = r1.m9037()
            if (r2 == 0) goto L_0x003a
            android.net.Uri r0 = r1.m9037()
            goto L_0x0017
        L_0x003a:
            r1 = 0
            android.net.Uri r0 = com.google.android.gms.cast.framework.media.MediaUtils.m8161(r0, r1)
            goto L_0x0017
        L_0x0040:
            com.google.android.gms.internal.zzazn r1 = r4.f8558
            r1.m9837((android.net.Uri) r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbag.m9882():void");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9885() {
        this.f8558.m9834();
        m9881();
        super.m8223();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9886() {
        m9882();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9887(CastSession castSession) {
        super.m8227(castSession);
        this.f8558.m9836((zzazo) new zzbah(this));
        m9881();
        m9882();
    }
}
