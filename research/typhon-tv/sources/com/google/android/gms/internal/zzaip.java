package com.google.android.gms.internal;

import android.content.Context;
import java.io.File;
import java.util.regex.Pattern;

@zzzv
public final class zzaip extends zzai {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f4241;

    private zzaip(Context context, zzaq zzaq) {
        super(zzaq);
        this.f4241 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzv m4706(Context context) {
        zzv zzv = new zzv(new zzal(new File(context.getCacheDir(), "admob_volley")), new zzaip(context, new zzar()));
        zzv.m13454();
        return zzv;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzp m4707(zzr<?> zzr) throws zzad {
        if (zzr.m13353() && zzr.m13363() == 0) {
            if (Pattern.matches((String) zzkb.m5481().m5595(zznh.f5023), zzr.m13359())) {
                zzkb.m5487();
                if (zzajr.m4756(this.f4241)) {
                    zzp r1 = new zzrx(this.f4241).m5817(zzr);
                    if (r1 != null) {
                        String valueOf = String.valueOf(zzr.m13359());
                        zzagf.m4527(valueOf.length() != 0 ? "Got gmscore asset response: ".concat(valueOf) : new String("Got gmscore asset response: "));
                        return r1;
                    }
                    String valueOf2 = String.valueOf(zzr.m13359());
                    zzagf.m4527(valueOf2.length() != 0 ? "Failed to get gmscore asset response: ".concat(valueOf2) : new String("Failed to get gmscore asset response: "));
                }
            }
        }
        return super.m4668(zzr);
    }
}
