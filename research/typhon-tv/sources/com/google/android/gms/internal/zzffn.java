package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffs;

abstract class zzffn<T extends zzffs<T>> {
    zzffn() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzffq<T> m12501(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m12502(Class<?> cls);
}
