package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class zzahs extends BroadcastReceiver {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzahn f8216;

    private zzahs(zzahn zzahn) {
        this.f8216 = zzahn;
    }

    /* synthetic */ zzahs(zzahn zzahn, zzaho zzaho) {
        this(zzahn);
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            boolean unused = this.f8216.f4219 = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            boolean unused2 = this.f8216.f4219 = false;
        }
    }
}
