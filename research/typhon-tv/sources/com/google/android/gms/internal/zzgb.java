package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import org.json.JSONObject;

@zzzv
public final class zzgb implements zzgo {

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzt<zzanh> f4639 = new zzge(this);

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzanh f4640;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzt<zzanh> f4641 = new zzgd(this);

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzt<zzanh> f4642 = new zzgc(this);
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzft f4643;

    public zzgb(zzft zzft, zzanh zzanh) {
        this.f4643 = zzft;
        this.f4640 = zzanh;
        zzanh zzanh2 = this.f4640;
        zzanh2.m5014("/updateActiveView", this.f4642);
        zzanh2.m5014("/untrackActiveViewUnit", this.f4641);
        zzanh2.m5014("/visibilityChanged", this.f4639);
        String valueOf = String.valueOf(this.f4643.f4636.m5300());
        zzagf.m4792(valueOf.length() != 0 ? "Custom JS tracking ad unit: ".concat(valueOf) : new String("Custom JS tracking ad unit: "));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5336() {
        zzanh zzanh = this.f4640;
        zzanh.m5001("/visibilityChanged", this.f4639);
        zzanh.m5001("/untrackActiveViewUnit", this.f4641);
        zzanh.m5001("/updateActiveView", this.f4642);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5337(JSONObject jSONObject, boolean z) {
        if (!z) {
            this.f4640.zzb("AFMA_updateActiveView", jSONObject);
        } else {
            this.f4643.m5324((zzgo) this);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5338() {
        return true;
    }
}
