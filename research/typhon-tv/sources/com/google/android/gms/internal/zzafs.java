package com.google.android.gms.internal;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.common.AdType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzafs {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f4151;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4152 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f4153;

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<String> f4154 = new ArrayList();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<String, zzui> f4155 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<String> f4156 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f4157;

    public zzafs(String str, long j) {
        this.f4151 = str;
        this.f4157 = j;
        m4456(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4456(String str) {
        JSONObject optJSONObject;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.optInt(NotificationCompat.CATEGORY_STATUS, -1) != 1) {
                    this.f4152 = false;
                    zzagf.m4791("App settings could not be fetched successfully.");
                    return;
                }
                this.f4152 = true;
                this.f4153 = jSONObject.optString("app_id");
                JSONArray optJSONArray = jSONObject.optJSONArray("ad_unit_id_settings");
                if (optJSONArray != null) {
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                        String optString = jSONObject2.optString("format");
                        String optString2 = jSONObject2.optString("ad_unit_id");
                        if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2)) {
                            if (AdType.INTERSTITIAL.equalsIgnoreCase(optString)) {
                                this.f4156.add(optString2);
                            } else if ("rewarded".equalsIgnoreCase(optString) && (optJSONObject = jSONObject2.optJSONObject("mediation_config")) != null) {
                                this.f4155.put(optString2, new zzui(optJSONObject));
                            }
                        }
                    }
                }
                m4457(jSONObject);
            } catch (JSONException e) {
                zzagf.m4796("Exception occurred while processing app setting json", e);
                zzbs.zzem().m4505((Throwable) e, "AppSettings.parseAppSettingsJson");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4457(JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("persistable_banner_ad_unit_ids");
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                this.f4154.add(optJSONArray.optString(i));
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Map<String, zzui> m4458() {
        return this.f4155;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4459() {
        return this.f4152;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m4460() {
        return this.f4153;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m4461() {
        return this.f4151;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m4462() {
        return this.f4157;
    }
}
