package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;
import java.util.List;

public final class zzdtv extends zzffu<zzdtv, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdtv f10164;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdtv> f10165;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfgd<zzdtf> f10166 = m12525();

    /* renamed from: 连任  reason: contains not printable characters */
    private String f10167 = "";

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10168;

    public static final class zza extends zzffu.zza<zzdtv, zza> implements zzfhg {
        private zza() {
            super(zzdtv.f10164);
        }

        /* synthetic */ zza(zzdtw zzdtw) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12165(zzdtf zzdtf) {
            m12541();
            ((zzdtv) this.f10398).m12157(zzdtf);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12166(String str) {
            m12541();
            ((zzdtv) this.f10398).m12160(str);
            return this;
        }
    }

    static {
        zzdtv zzdtv = new zzdtv();
        f10164 = zzdtv;
        zzdtv.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdtv.f10394.m12718();
    }

    private zzdtv() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zza m12155() {
        zzdtv zzdtv = f10164;
        zzffu.zza zza2 = (zzffu.zza) zzdtv.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdtv);
        return (zza) zza2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12157(zzdtf zzdtf) {
        if (zzdtf == null) {
            throw new NullPointerException();
        }
        if (!this.f10166.m12584()) {
            zzfgd<zzdtf> zzfgd = this.f10166;
            int size = zzfgd.size();
            this.f10166 = zzfgd.m12583(size == 0 ? 10 : size << 1);
        }
        this.f10166.add(zzdtf);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12160(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f10167 = str;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12161() {
        int i = 0;
        int i2 = this.f10395;
        if (i2 != -1) {
            return i2;
        }
        int r0 = !this.f10167.isEmpty() ? zzffg.m5248(1, this.f10167) + 0 : 0;
        while (true) {
            int i3 = r0;
            if (i < this.f10166.size()) {
                r0 = zzffg.m5262(2, (zzfhe) this.f10166.get(i)) + i3;
                i++;
            } else {
                int r02 = this.f10394.m12716() + i3;
                this.f10395 = r02;
                return r02;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 172 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m12162(int r6, java.lang.Object r7, java.lang.Object r8) {
        /*
            r5 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdtw.f10169
            int r4 = r6 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x0022;
                case 5: goto L_0x0028;
                case 6: goto L_0x0061;
                case 7: goto L_0x00d2;
                case 8: goto L_0x00d6;
                case 9: goto L_0x00f2;
                case 10: goto L_0x00f8;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdtv r5 = new com.google.android.gms.internal.zzdtv
            r5.<init>()
        L_0x0017:
            return r5
        L_0x0018:
            com.google.android.gms.internal.zzdtv r5 = f10164
            goto L_0x0017
        L_0x001b:
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdtf> r1 = r5.f10166
            r1.m12582()
            r5 = r0
            goto L_0x0017
        L_0x0022:
            com.google.android.gms.internal.zzdtv$zza r5 = new com.google.android.gms.internal.zzdtv$zza
            r5.<init>(r0)
            goto L_0x0017
        L_0x0028:
            com.google.android.gms.internal.zzffu$zzh r7 = (com.google.android.gms.internal.zzffu.zzh) r7
            com.google.android.gms.internal.zzdtv r8 = (com.google.android.gms.internal.zzdtv) r8
            java.lang.String r0 = r5.f10167
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x005d
            r0 = r1
        L_0x0035:
            java.lang.String r3 = r5.f10167
            java.lang.String r4 = r8.f10167
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x005f
        L_0x003f:
            java.lang.String r2 = r8.f10167
            java.lang.String r0 = r7.m12574((boolean) r0, (java.lang.String) r3, (boolean) r1, (java.lang.String) r2)
            r5.f10167 = r0
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdtf> r0 = r5.f10166
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdtf> r1 = r8.f10166
            com.google.android.gms.internal.zzfgd r0 = r7.m12571(r0, r1)
            r5.f10166 = r0
            com.google.android.gms.internal.zzffu$zzf r0 = com.google.android.gms.internal.zzffu.zzf.f10404
            if (r7 != r0) goto L_0x0017
            int r0 = r5.f10168
            int r1 = r8.f10168
            r0 = r0 | r1
            r5.f10168 = r0
            goto L_0x0017
        L_0x005d:
            r0 = r2
            goto L_0x0035
        L_0x005f:
            r1 = r2
            goto L_0x003f
        L_0x0061:
            com.google.android.gms.internal.zzffb r7 = (com.google.android.gms.internal.zzffb) r7
            com.google.android.gms.internal.zzffm r8 = (com.google.android.gms.internal.zzffm) r8
            if (r8 != 0) goto L_0x006e
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x006d:
            r2 = r1
        L_0x006e:
            if (r2 != 0) goto L_0x00d2
            int r0 = r7.m12415()     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            switch(r0) {
                case 0: goto L_0x006d;
                case 10: goto L_0x007f;
                case 18: goto L_0x0093;
                default: goto L_0x0077;
            }     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
        L_0x0077:
            boolean r0 = r5.m12537((int) r0, (com.google.android.gms.internal.zzffb) r7)     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            if (r0 != 0) goto L_0x006e
            r2 = r1
            goto L_0x006e
        L_0x007f:
            java.lang.String r0 = r7.m12400()     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            r5.f10167 = r0     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            goto L_0x006e
        L_0x0086:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0091 }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r5)     // Catch:{ all -> 0x0091 }
            r1.<init>(r0)     // Catch:{ all -> 0x0091 }
            throw r1     // Catch:{ all -> 0x0091 }
        L_0x0091:
            r0 = move-exception
            throw r0
        L_0x0093:
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdtf> r0 = r5.f10166     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            boolean r0 = r0.m12584()     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            if (r0 != 0) goto L_0x00ab
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdtf> r3 = r5.f10166     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            int r0 = r3.size()     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            if (r0 != 0) goto L_0x00cf
            r0 = 10
        L_0x00a5:
            com.google.android.gms.internal.zzfgd r0 = r3.m12583(r0)     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            r5.f10166 = r0     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
        L_0x00ab:
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdtf> r3 = r5.f10166     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            com.google.android.gms.internal.zzdtf r0 = com.google.android.gms.internal.zzdtf.m12040()     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            com.google.android.gms.internal.zzffu r0 = r7.m12416(r0, r8)     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            com.google.android.gms.internal.zzdtf r0 = (com.google.android.gms.internal.zzdtf) r0     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            r3.add(r0)     // Catch:{ zzfge -> 0x0086, IOException -> 0x00bb }
            goto L_0x006e
        L_0x00bb:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0091 }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x0091 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0091 }
            r2.<init>(r0)     // Catch:{ all -> 0x0091 }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r5)     // Catch:{ all -> 0x0091 }
            r1.<init>(r0)     // Catch:{ all -> 0x0091 }
            throw r1     // Catch:{ all -> 0x0091 }
        L_0x00cf:
            int r0 = r0 << 1
            goto L_0x00a5
        L_0x00d2:
            com.google.android.gms.internal.zzdtv r5 = f10164
            goto L_0x0017
        L_0x00d6:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtv> r0 = f10165
            if (r0 != 0) goto L_0x00eb
            java.lang.Class<com.google.android.gms.internal.zzdtv> r1 = com.google.android.gms.internal.zzdtv.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtv> r0 = f10165     // Catch:{ all -> 0x00ef }
            if (r0 != 0) goto L_0x00ea
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00ef }
            com.google.android.gms.internal.zzdtv r2 = f10164     // Catch:{ all -> 0x00ef }
            r0.<init>(r2)     // Catch:{ all -> 0x00ef }
            f10165 = r0     // Catch:{ all -> 0x00ef }
        L_0x00ea:
            monitor-exit(r1)     // Catch:{ all -> 0x00ef }
        L_0x00eb:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtv> r5 = f10165
            goto L_0x0017
        L_0x00ef:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ef }
            throw r0
        L_0x00f2:
            java.lang.Byte r5 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00f8:
            r5 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdtv.m12162(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzdtf> m12163() {
        return this.f10166;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12164(zzffg zzffg) throws IOException {
        if (!this.f10167.isEmpty()) {
            zzffg.m5290(1, this.f10167);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f10166.size()) {
                zzffg.m5289(2, (zzfhe) this.f10166.get(i2));
                i = i2 + 1;
            } else {
                this.f10394.m12719(zzffg);
                return;
            }
        }
    }
}
