package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.DownloadManager;

@TargetApi(9)
public class zzahv extends zzaht {
    public zzahv() {
        super();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9607() {
        return 7;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9608() {
        return 6;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m9609(DownloadManager.Request request) {
        request.setShowRunningNotification(true);
        return true;
    }
}
