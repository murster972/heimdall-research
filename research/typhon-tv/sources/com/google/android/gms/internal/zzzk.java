package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final /* synthetic */ class zzzk implements zzt {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzanh f11015;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzalf f11016;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzyl f11017;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzzf f11018;

    zzzk(zzzf zzzf, zzanh zzanh, zzyl zzyl, zzalf zzalf) {
        this.f11018 = zzzf;
        this.f11015 = zzanh;
        this.f11017 = zzyl;
        this.f11016 = zzalf;
    }

    public final void zza(Object obj, Map map) {
        this.f11018.m6091(this.f11015, this.f11017, this.f11016, (zzanh) obj, map);
    }
}
