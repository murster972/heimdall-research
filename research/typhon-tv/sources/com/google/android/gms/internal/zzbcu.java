package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;

public abstract class zzbcu extends zzev implements zzbct {
    public zzbcu() {
        attachInterface(this, "com.google.android.gms.cast.internal.ICastDeviceControllerListener");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m10074(parcel.readInt());
                break;
            case 2:
                m10075((ApplicationMetadata) zzew.m12304(parcel, ApplicationMetadata.CREATOR), parcel.readString(), parcel.readString(), zzew.m12308(parcel));
                break;
            case 3:
                m10071(parcel.readInt());
                break;
            case 4:
                m10078(parcel.readString(), parcel.readDouble(), zzew.m12308(parcel));
                break;
            case 5:
                m10081(parcel.readString(), parcel.readString());
                break;
            case 6:
                m10082(parcel.readString(), parcel.createByteArray());
                break;
            case 7:
                m10072(parcel.readInt());
                break;
            case 8:
                m10073(parcel.readInt());
                break;
            case 9:
                m10070(parcel.readInt());
                break;
            case 10:
                m10080(parcel.readString(), parcel.readLong(), parcel.readInt());
                break;
            case 11:
                m10079(parcel.readString(), parcel.readLong());
                break;
            case 12:
                m10076((zzbbt) zzew.m12304(parcel, zzbbt.CREATOR));
                break;
            case 13:
                m10077((zzbcn) zzew.m12304(parcel, zzbcn.CREATOR));
                break;
            default:
                return false;
        }
        return true;
    }
}
