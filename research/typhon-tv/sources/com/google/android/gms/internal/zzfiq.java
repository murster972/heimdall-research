package com.google.android.gms.internal;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;

final class zzfiq {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final boolean f10492 = m12740(Integer.TYPE);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final zzd f10493 = (f10510 == null ? null : m12727() ? f10509 ? new zzb(f10510) : f10492 ? new zza(f10510) : null : new zzc(f10510));

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final boolean f10494 = m12726();

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final long f10495 = ((long) m12742((Class<?>) int[].class));

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final long f10496 = ((long) m12731(int[].class));

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final long f10497 = ((long) m12731(double[].class));

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final long f10498 = ((long) m12731(boolean[].class));

    /* renamed from: ˉ  reason: contains not printable characters */
    private static final long f10499 = ((long) m12742((Class<?>) Object[].class));

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final long f10500 = ((long) m12742((Class<?>) float[].class));

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final long f10501 = ((long) m12731(float[].class));

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final long f10502 = ((long) m12742((Class<?>) double[].class));

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final long f10503 = ((long) m12731(Object[].class));

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final boolean f10504 = m12729();

    /* renamed from: י  reason: contains not printable characters */
    private static final long f10505;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public static final boolean f10506;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final long f10507 = ((long) m12742((Class<?>) byte[].class));

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final long f10508 = ((long) m12742((Class<?>) boolean[].class));

    /* renamed from: 连任  reason: contains not printable characters */
    private static final boolean f10509 = m12740(Long.TYPE);

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Unsafe f10510 = m12735();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final boolean f10511 = (m12744("org.robolectric.Robolectric") != null);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Class<?> f10512 = m12744("libcore.io.Memory");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Logger f10513 = Logger.getLogger(zzfiq.class.getName());

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final long f10514 = ((long) m12742((Class<?>) long[].class));

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final long f10515 = ((long) m12731(long[].class));

    static final class zza extends zzd {
        zza(Unsafe unsafe) {
            super(unsafe);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final byte m12750(Object obj, long j) {
            return zzfiq.f10506 ? zzfiq.m12734(obj, j) : zzfiq.m12728(obj, j);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12751(Object obj, long j, byte b) {
            if (zzfiq.f10506) {
                zzfiq.m12738(obj, j, b);
            } else {
                zzfiq.m12736(obj, j, b);
            }
        }
    }

    static final class zzb extends zzd {
        zzb(Unsafe unsafe) {
            super(unsafe);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final byte m12752(Object obj, long j) {
            return zzfiq.f10506 ? zzfiq.m12734(obj, j) : zzfiq.m12728(obj, j);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12753(Object obj, long j, byte b) {
            if (zzfiq.f10506) {
                zzfiq.m12738(obj, j, b);
            } else {
                zzfiq.m12736(obj, j, b);
            }
        }
    }

    static final class zzc extends zzd {
        zzc(Unsafe unsafe) {
            super(unsafe);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final byte m12754(Object obj, long j) {
            return this.f10516.getByte(obj, j);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12755(Object obj, long j, byte b) {
            this.f10516.putByte(obj, j, b);
        }
    }

    static abstract class zzd {

        /* renamed from: 龘  reason: contains not printable characters */
        Unsafe f10516;

        zzd(Unsafe unsafe) {
            this.f10516 = unsafe;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final int m12756(Object obj, long j) {
            return this.f10516.getInt(obj, j);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract byte m12757(Object obj, long j);

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m12758(Object obj, long j, byte b);
    }

    static {
        Field r0;
        boolean z = true;
        if (!m12727() || (r0 = m12745((Class<?>) Buffer.class, "effectiveDirectAddress")) == null) {
            r0 = m12745((Class<?>) Buffer.class, "address");
        }
        f10505 = (r0 == null || f10493 == null) ? -1 : f10493.f10516.objectFieldOffset(r0);
        if (ByteOrder.nativeOrder() != ByteOrder.BIG_ENDIAN) {
            z = false;
        }
        f10506 = z;
    }

    private zzfiq() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m12726() {
        if (f10510 == null) {
            return false;
        }
        try {
            Class<?> cls = f10510.getClass();
            cls.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls.getMethod("getLong", new Class[]{Object.class, Long.TYPE});
            if (m12727()) {
                return true;
            }
            cls.getMethod("getByte", new Class[]{Long.TYPE});
            cls.getMethod("putByte", new Class[]{Long.TYPE, Byte.TYPE});
            cls.getMethod("getInt", new Class[]{Long.TYPE});
            cls.getMethod("putInt", new Class[]{Long.TYPE, Integer.TYPE});
            cls.getMethod("getLong", new Class[]{Long.TYPE});
            cls.getMethod("putLong", new Class[]{Long.TYPE, Long.TYPE});
            cls.getMethod("copyMemory", new Class[]{Long.TYPE, Long.TYPE, Long.TYPE});
            cls.getMethod("copyMemory", new Class[]{Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger = f10513;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", new StringBuilder(String.valueOf(valueOf).length() + 71).append("platform method missing - proto runtime falling back to safer methods: ").append(valueOf).toString());
            return false;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean m12727() {
        return f10512 != null && !f10511;
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public static byte m12728(Object obj, long j) {
        return (byte) (m12743(obj, -4 & j) >>> ((int) ((3 & j) << 3)));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean m12729() {
        if (f10510 == null) {
            return false;
        }
        try {
            Class<?> cls = f10510.getClass();
            cls.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls.getMethod("arrayBaseOffset", new Class[]{Class.class});
            cls.getMethod("arrayIndexScale", new Class[]{Class.class});
            cls.getMethod("getInt", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putInt", new Class[]{Object.class, Long.TYPE, Integer.TYPE});
            cls.getMethod("getLong", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putLong", new Class[]{Object.class, Long.TYPE, Long.TYPE});
            cls.getMethod("getObject", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putObject", new Class[]{Object.class, Long.TYPE, Object.class});
            if (m12727()) {
                return true;
            }
            cls.getMethod("getByte", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putByte", new Class[]{Object.class, Long.TYPE, Byte.TYPE});
            cls.getMethod("getBoolean", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putBoolean", new Class[]{Object.class, Long.TYPE, Boolean.TYPE});
            cls.getMethod("getFloat", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putFloat", new Class[]{Object.class, Long.TYPE, Float.TYPE});
            cls.getMethod("getDouble", new Class[]{Object.class, Long.TYPE});
            cls.getMethod("putDouble", new Class[]{Object.class, Long.TYPE, Double.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger = f10513;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", new StringBuilder(String.valueOf(valueOf).length() + 71).append("platform method missing - proto runtime falling back to safer methods: ").append(valueOf).toString());
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m12731(Class<?> cls) {
        if (f10504) {
            return f10493.f10516.arrayIndexScale(cls);
        }
        return -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m12733() {
        return f10494;
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static byte m12734(Object obj, long j) {
        return (byte) (m12743(obj, -4 & j) >>> ((int) (((-1 ^ j) & 3) << 3)));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static Unsafe m12735() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zzfir());
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static void m12736(Object obj, long j, byte b) {
        int i = (((int) j) & 3) << 3;
        m12747(obj, j & -4, (m12743(obj, j & -4) & ((255 << i) ^ -1)) | ((b & 255) << i));
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static void m12738(Object obj, long j, byte b) {
        int i = ((((int) j) ^ -1) & 3) << 3;
        m12747(obj, j & -4, (m12743(obj, j & -4) & ((255 << i) ^ -1)) | ((b & 255) << i));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean m12740(Class<?> cls) {
        if (!m12727()) {
            return false;
        }
        try {
            Class<?> cls2 = f10512;
            cls2.getMethod("peekLong", new Class[]{cls, Boolean.TYPE});
            cls2.getMethod("pokeLong", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls2.getMethod("pokeInt", new Class[]{cls, Integer.TYPE, Boolean.TYPE});
            cls2.getMethod("peekInt", new Class[]{cls, Boolean.TYPE});
            cls2.getMethod("pokeByte", new Class[]{cls, Byte.TYPE});
            cls2.getMethod("peekByte", new Class[]{cls});
            cls2.getMethod("pokeByteArray", new Class[]{cls, byte[].class, Integer.TYPE, Integer.TYPE});
            cls2.getMethod("peekByteArray", new Class[]{cls, byte[].class, Integer.TYPE, Integer.TYPE});
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static byte m12741(byte[] bArr, long j) {
        return f10493.m12757(bArr, f10507 + j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m12742(Class<?> cls) {
        if (f10504) {
            return f10493.f10516.arrayBaseOffset(cls);
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m12743(Object obj, long j) {
        return f10493.m12756(obj, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> Class<T> m12744(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Field m12745(Class<?> cls, String str) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12747(Object obj, long j, int i) {
        f10493.f10516.putInt(obj, j, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m12748(byte[] bArr, long j, byte b) {
        f10493.m12758(bArr, f10507 + j, b);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m12749() {
        return f10504;
    }
}
