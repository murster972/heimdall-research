package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzuv implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzuo f10916;

    zzuv(zzuu zzuu, zzuo zzuo) {
        this.f10916 = zzuo;
    }

    public final void run() {
        try {
            this.f10916.f5448.m13467();
        } catch (RemoteException e) {
            zzagf.m4796("Could not destroy mediation adapter.", e);
        }
    }
}
