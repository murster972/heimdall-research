package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzahb extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f8193;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8194;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzahb(Context context, boolean z) {
        super((zzagi) null);
        this.f8194 = context;
        this.f8193 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9600() {
        SharedPreferences.Editor edit = this.f8194.getSharedPreferences("admob", 0).edit();
        edit.putBoolean("content_url_opted_out", this.f8193);
        edit.apply();
    }
}
