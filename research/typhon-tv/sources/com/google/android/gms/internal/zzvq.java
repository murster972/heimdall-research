package com.google.android.gms.internal;

import android.location.Location;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.Set;

@zzzv
public final class zzvq implements MediationAdRequest {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f5481;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f5482;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Location f5483;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f5484;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f5485;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Set<String> f5486;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Date f5487;

    public zzvq(Date date, int i, Set<String> set, Location location, boolean z, int i2, boolean z2) {
        this.f5487 = date;
        this.f5484 = i;
        this.f5486 = set;
        this.f5483 = location;
        this.f5485 = z;
        this.f5481 = i2;
        this.f5482 = z2;
    }

    public final Date getBirthday() {
        return this.f5487;
    }

    public final int getGender() {
        return this.f5484;
    }

    public final Set<String> getKeywords() {
        return this.f5486;
    }

    public final Location getLocation() {
        return this.f5483;
    }

    public final boolean isDesignedForFamilies() {
        return this.f5482;
    }

    public final boolean isTesting() {
        return this.f5485;
    }

    public final int taggedForChildDirectedTreatment() {
        return this.f5481;
    }
}
