package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagr extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8174;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8175;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagr(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8175 = context;
        this.f8174 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9590() {
        SharedPreferences sharedPreferences = this.f8175.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putBoolean("use_https", sharedPreferences.getBoolean("use_https", true));
        if (this.f8174 != null) {
            this.f8174.m9605(bundle);
        }
    }
}
