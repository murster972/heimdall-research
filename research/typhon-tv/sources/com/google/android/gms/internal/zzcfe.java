package com.google.android.gms.internal;

import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.zzq;

final class zzcfe extends zzq {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzci<LocationCallback> f9076;

    zzcfe(zzci<LocationCallback> zzci) {
        this.f9076 = zzci;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m10390() {
        this.f9076.m8854();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10391(LocationAvailability locationAvailability) {
        this.f9076.m8855(new zzcfg(this, locationAvailability));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10392(LocationResult locationResult) {
        this.f9076.m8855(new zzcff(this, locationResult));
    }
}
