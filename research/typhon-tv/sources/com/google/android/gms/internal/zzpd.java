package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class zzpd implements zzt<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzoy f10829;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzzb f10830;

    zzpd(zzoy zzoy, zzzb zzzb) {
        this.f10829 = zzoy;
        this.f10830 = zzzb;
    }

    public final void zza(Object obj, Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        try {
            for (String next : map.keySet()) {
                jSONObject.put(next, (Object) map.get(next));
            }
            jSONObject.put("id", (Object) this.f10829.f10819);
            this.f10830.m6077("sendMessageToNativeJs", jSONObject);
        } catch (JSONException e) {
            zzagf.m4793("Unable to dispatch sendMessageToNativeJs event", e);
        }
    }
}
