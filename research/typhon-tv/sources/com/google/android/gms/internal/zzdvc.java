package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Provider;
import java.security.Signature;

public final class zzdvc implements zzduv<Signature> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m12225(String str, Provider provider) throws GeneralSecurityException {
        return provider == null ? Signature.getInstance(str) : Signature.getInstance(str, provider);
    }
}
