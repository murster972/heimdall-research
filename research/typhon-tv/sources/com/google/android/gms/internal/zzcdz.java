package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.zzm;
import com.google.android.gms.location.zzn;

public final class zzcdz extends zzbfm {
    public static final Parcelable.Creator<zzcdz> CREATOR = new zzcea();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzcdx f9045;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzceu f9046;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzm f9047;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9048;

    zzcdz(int i, zzcdx zzcdx, IBinder iBinder, IBinder iBinder2) {
        zzceu zzceu = null;
        this.f9048 = i;
        this.f9045 = zzcdx;
        this.f9047 = iBinder == null ? null : zzn.m13688(iBinder);
        if (!(iBinder2 == null || iBinder2 == null)) {
            IInterface queryLocalInterface = iBinder2.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            zzceu = queryLocalInterface instanceof zzceu ? (zzceu) queryLocalInterface : new zzcew(iBinder2);
        }
        this.f9046 = zzceu;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        IBinder iBinder = null;
        int r2 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9048);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f9045, i, false);
        zzbfp.m10188(parcel, 3, this.f9047 == null ? null : this.f9047.asBinder(), false);
        if (this.f9046 != null) {
            iBinder = this.f9046.asBinder();
        }
        zzbfp.m10188(parcel, 4, iBinder, false);
        zzbfp.m10182(parcel, r2);
    }
}
