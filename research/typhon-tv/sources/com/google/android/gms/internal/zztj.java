package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzak;
import com.google.android.gms.ads.internal.zzbs;

final class zztj {

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean f10888;

    /* renamed from: ʼ  reason: contains not printable characters */
    private /* synthetic */ zzti f10889;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean f10890;

    /* renamed from: 靐  reason: contains not printable characters */
    zzjj f10891;

    /* renamed from: 麤  reason: contains not printable characters */
    long f10892;

    /* renamed from: 齉  reason: contains not printable characters */
    zzse f10893;

    /* renamed from: 龘  reason: contains not printable characters */
    zzak f10894;

    zztj(zzti zzti, zzsd zzsd) {
        this.f10889 = zzti;
        this.f10894 = zzsd.m5818(zzti.f5369);
        this.f10893 = new zzse();
        zzse zzse = this.f10893;
        zzak zzak = this.f10894;
        zzak.zza(new zzsf(zzse));
        zzak.zza(new zzsn(zzse));
        zzak.zza(new zzsp(zzse));
        zzak.zza(new zzsr(zzse));
        zzak.zza(new zzst(zzse));
    }

    zztj(zzti zzti, zzsd zzsd, zzjj zzjj) {
        this(zzti, zzsd);
        this.f10891 = zzjj;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13431() {
        if (this.f10890) {
            return false;
        }
        this.f10888 = this.f10894.zzb(zztg.m5825(this.f10891 != null ? this.f10891 : this.f10889.f5367));
        this.f10890 = true;
        this.f10892 = zzbs.zzeo().m9243();
        return true;
    }
}
