package com.google.android.gms.internal;

import java.util.concurrent.Future;

final class zzace implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Future f8070;

    zzace(zzacb zzacb, Future future) {
        this.f8070 = future;
    }

    public final void run() {
        if (!this.f8070.isDone()) {
            this.f8070.cancel(true);
        }
    }
}
