package com.google.android.gms.internal;

import java.io.IOException;

public final class zzexn extends zzfjm<zzexn> {

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile zzexn[] f10332;

    /* renamed from: 靐  reason: contains not printable characters */
    public long f10333 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f10334 = "";

    /* renamed from: 龘  reason: contains not printable characters */
    public int f10335 = 0;

    public zzexn() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzexn[] m12352() {
        if (f10332 == null) {
            synchronized (zzfjq.f10546) {
                if (f10332 == null) {
                    f10332 = new zzexn[0];
                }
            }
        }
        return f10332;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzexn)) {
            return false;
        }
        zzexn zzexn = (zzexn) obj;
        if (this.f10335 != zzexn.f10335) {
            return false;
        }
        if (this.f10333 != zzexn.f10333) {
            return false;
        }
        if (this.f10334 == null) {
            if (zzexn.f10334 != null) {
                return false;
            }
        } else if (!this.f10334.equals(zzexn.f10334)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzexn.f10533 == null || zzexn.f10533.m12849() : this.f10533.equals(zzexn.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f10334 == null ? 0 : this.f10334.hashCode()) + ((((((getClass().getName().hashCode() + 527) * 31) + this.f10335) * 31) + ((int) (this.f10333 ^ (this.f10333 >>> 32)))) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12353() {
        int r0 = super.m12841();
        if (this.f10335 != 0) {
            r0 += zzfjk.m12806(1, this.f10335);
        }
        if (this.f10333 != 0) {
            r0 += zzfjk.m12805(2) + 8;
        }
        return (this.f10334 == null || this.f10334.equals("")) ? r0 : r0 + zzfjk.m12808(3, this.f10334);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12354(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f10335 = zzfjj.m12785();
                    continue;
                case 17:
                    this.f10333 = zzfjj.m12789();
                    continue;
                case 26:
                    this.f10334 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12355(zzfjk zzfjk) throws IOException {
        if (this.f10335 != 0) {
            zzfjk.m12832(1, this.f10335);
        }
        if (this.f10333 != 0) {
            zzfjk.m12827(2, this.f10333);
        }
        if (this.f10334 != null && !this.f10334.equals("")) {
            zzfjk.m12835(3, this.f10334);
        }
        super.m12842(zzfjk);
    }
}
