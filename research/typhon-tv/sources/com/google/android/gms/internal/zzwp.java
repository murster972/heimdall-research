package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.content.Intent;
import com.google.android.gms.ads.internal.zzbs;

final class zzwp implements DialogInterface.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzwo f10951;

    zzwp(zzwo zzwo) {
        this.f10951 = zzwo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent r0 = this.f10951.m5993();
        zzbs.zzei();
        zzahn.m4608(this.f10951.f5509, r0);
    }
}
