package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.internal.gmsg.HttpClient;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.gmsg.zzy;
import com.google.android.gms.ads.internal.js.zzaa;
import com.google.android.gms.ads.internal.js.zzc;
import com.google.android.gms.ads.internal.js.zzn;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzabo extends zzagb {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzy f3880 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static zzt<Object> f3881 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private static HttpClient f3882 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f3883 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static zzn f3884 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean f3885 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static long f3886 = TimeUnit.SECONDS.toMillis(10);
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzzx f3887;

    /* renamed from: ʾ  reason: contains not printable characters */
    private zzix f3888;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public zzaa f3889;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzaau f3890;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Object f3891 = new Object();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Context f3892;

    public zzabo(Context context, zzaau zzaau, zzzx zzzx, zzix zzix) {
        super(true);
        this.f3887 = zzzx;
        this.f3892 = context;
        this.f3890 = zzaau;
        this.f3888 = zzix;
        synchronized (f3883) {
            if (!f3885) {
                f3880 = new zzy();
                f3882 = new HttpClient(context.getApplicationContext(), zzaau.f3800);
                f3881 = new zzabw();
                f3884 = new zzn(this.f3892.getApplicationContext(), this.f3890.f3800, (String) zzkb.m5481().m5595(zznh.f5159), new zzabv(), new zzabu());
                f3885 = true;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    protected static void m4268(zzc zzc) {
        zzc.zzb("/loadAd", f3880);
        zzc.zzb("/fetchHttpRequest", f3882);
        zzc.zzb("/invalidRequest", f3881);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaax m4270(zzaat zzaat) {
        zzbs.zzei();
        String r0 = zzahn.m4597();
        JSONObject r1 = m4272(zzaat, r0);
        if (r1 == null) {
            return new zzaax(0);
        }
        long r2 = zzbs.zzeo().m9241();
        Future<JSONObject> zzas = f3880.zzas(r0);
        zzajr.f4285.post(new zzabq(this, r1, r0));
        try {
            JSONObject jSONObject = zzas.get(f3886 - (zzbs.zzeo().m9241() - r2), TimeUnit.MILLISECONDS);
            if (jSONObject == null) {
                return new zzaax(-1);
            }
            zzaax r02 = zzacg.m4285(this.f3892, zzaat, jSONObject.toString());
            return (r02.f3859 == -3 || !TextUtils.isEmpty(r02.f3858)) ? r02 : new zzaax(3);
        } catch (InterruptedException | CancellationException e) {
            return new zzaax(-1);
        } catch (TimeoutException e2) {
            return new zzaax(2);
        } catch (ExecutionException e3) {
            return new zzaax(0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final JSONObject m4272(zzaat zzaat, String str) {
        zzaco zzaco;
        AdvertisingIdClient.Info info;
        Bundle bundle = zzaat.f3763.f4767.getBundle("sdk_less_server_data");
        if (bundle == null) {
            return null;
        }
        try {
            zzaco = zzbs.zzes().m4318(this.f3892).get();
        } catch (Exception e) {
            zzagf.m4796("Error grabbing device info: ", e);
            zzaco = null;
        }
        Context context = this.f3892;
        zzabz zzabz = new zzabz();
        zzabz.f3898 = zzaat;
        zzabz.f3899 = zzaco;
        JSONObject r3 = zzacg.m4289(context, zzabz);
        if (r3 == null) {
            return null;
        }
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(this.f3892);
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e2) {
            zzagf.m4796("Cannot get advertising id info", e2);
            info = null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("request_id", str);
        hashMap.put("request_param", r3);
        hashMap.put("data", bundle);
        if (info != null) {
            hashMap.put("adid", info.getId());
            hashMap.put(PubnativeRequest.Parameters.LAT, Integer.valueOf(info.isLimitAdTrackingEnabled() ? 1 : 0));
        }
        try {
            return zzbs.zzei().m4634((Map<String, ?>) hashMap);
        } catch (JSONException e3) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static void m4273(zzc zzc) {
        zzc.zza("/loadAd", f3880);
        zzc.zza("/fetchHttpRequest", f3882);
        zzc.zza("/invalidRequest", f3881);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4274() {
        synchronized (this.f3891) {
            zzajr.f4285.post(new zzabt(this));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4275() {
        zzagf.m4792("SdkLessAdLoaderBackgroundTask started.");
        String r6 = zzbs.zzfd().m4423(this.f3892);
        zzaat zzaat = new zzaat(this.f3890, -1, zzbs.zzfd().m4421(this.f3892), zzbs.zzfd().m4422(this.f3892), r6);
        zzbs.zzfd().m4420(this.f3892, r6);
        zzaax r4 = m4270(zzaat);
        zzaat zzaat2 = zzaat;
        zzajr.f4285.post(new zzabp(this, new zzafp(zzaat2, r4, (zzui) null, (zzjn) null, r4.f3859, zzbs.zzeo().m9241(), r4.f3826, (JSONObject) null, this.f3888)));
    }
}
