package com.google.android.gms.internal;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

@zzzv
public final class zzia {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f4723;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f4724;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f4725 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f4726;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f4727;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f4728;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzhq f4729;

    public zzia(int i, int i2, int i3) {
        this.f4726 = i;
        if (i2 > 64 || i2 < 0) {
            this.f4723 = 64;
        } else {
            this.f4723 = i2;
        }
        if (i3 <= 0) {
            this.f4724 = 1;
        } else {
            this.f4724 = i3;
        }
        this.f4729 = new zzhz(this.f4723);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e0 A[SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m5408(java.lang.String r12, java.util.HashSet<java.lang.String> r13) {
        /*
            r11 = this;
            r10 = 32
            r3 = 1
            r1 = 0
            java.lang.String r0 = "\n"
            java.lang.String[] r6 = r12.split(r0)
            int r0 = r6.length
            if (r0 != 0) goto L_0x000f
        L_0x000e:
            return r3
        L_0x000f:
            r0 = r1
        L_0x0010:
            int r2 = r6.length
            if (r0 >= r2) goto L_0x000e
            r5 = r6[r0]
            java.lang.String r2 = "'"
            int r2 = r5.indexOf(r2)
            r4 = -1
            if (r2 == r4) goto L_0x00e6
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>(r5)
            r2 = r3
            r4 = r1
        L_0x0026:
            int r8 = r2 + 2
            int r9 = r7.length()
            if (r8 > r9) goto L_0x006f
            char r8 = r7.charAt(r2)
            r9 = 39
            if (r8 != r9) goto L_0x0068
            int r4 = r2 + -1
            char r4 = r7.charAt(r4)
            if (r4 == r10) goto L_0x006b
            int r4 = r2 + 1
            char r4 = r7.charAt(r4)
            r8 = 115(0x73, float:1.61E-43)
            if (r4 == r8) goto L_0x0052
            int r4 = r2 + 1
            char r4 = r7.charAt(r4)
            r8 = 83
            if (r4 != r8) goto L_0x006b
        L_0x0052:
            int r4 = r2 + 2
            int r8 = r7.length()
            if (r4 == r8) goto L_0x0062
            int r4 = r2 + 2
            char r4 = r7.charAt(r4)
            if (r4 != r10) goto L_0x006b
        L_0x0062:
            r7.insert(r2, r10)
            int r2 = r2 + 2
        L_0x0067:
            r4 = r3
        L_0x0068:
            int r2 = r2 + 1
            goto L_0x0026
        L_0x006b:
            r7.setCharAt(r2, r10)
            goto L_0x0067
        L_0x006f:
            if (r4 == 0) goto L_0x00a4
            java.lang.String r2 = r7.toString()
        L_0x0075:
            if (r2 == 0) goto L_0x00e6
            r11.f4727 = r2
        L_0x0079:
            java.lang.String[] r7 = com.google.android.gms.internal.zzhu.m5400(r2, r3)
            int r2 = r7.length
            int r4 = r11.f4724
            if (r2 < r4) goto L_0x00e0
            r2 = r1
        L_0x0083:
            int r4 = r7.length
            if (r2 >= r4) goto L_0x00d5
            java.lang.String r4 = ""
            r5 = r1
        L_0x008a:
            int r8 = r11.f4724
            if (r5 >= r8) goto L_0x00e4
            int r8 = r2 + r5
            int r9 = r7.length
            if (r8 < r9) goto L_0x00a6
            r5 = r1
        L_0x0094:
            if (r5 == 0) goto L_0x00d5
            r13.add(r4)
            int r4 = r13.size()
            int r5 = r11.f4726
            if (r4 < r5) goto L_0x00d2
            r3 = r1
            goto L_0x000e
        L_0x00a4:
            r2 = 0
            goto L_0x0075
        L_0x00a6:
            if (r5 <= 0) goto L_0x00b3
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.String r8 = " "
            java.lang.String r4 = r4.concat(r8)
        L_0x00b3:
            java.lang.String r8 = java.lang.String.valueOf(r4)
            int r4 = r2 + r5
            r4 = r7[r4]
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r9 = r4.length()
            if (r9 == 0) goto L_0x00cc
            java.lang.String r4 = r8.concat(r4)
        L_0x00c9:
            int r5 = r5 + 1
            goto L_0x008a
        L_0x00cc:
            java.lang.String r4 = new java.lang.String
            r4.<init>(r8)
            goto L_0x00c9
        L_0x00d2:
            int r2 = r2 + 1
            goto L_0x0083
        L_0x00d5:
            int r2 = r13.size()
            int r4 = r11.f4726
            if (r2 < r4) goto L_0x00e0
            r3 = r1
            goto L_0x000e
        L_0x00e0:
            int r0 = r0 + 1
            goto L_0x0010
        L_0x00e4:
            r5 = r3
            goto L_0x0094
        L_0x00e6:
            r2 = r5
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzia.m5408(java.lang.String, java.util.HashSet):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5409(ArrayList<String> arrayList, ArrayList<zzhp> arrayList2) {
        Collections.sort(arrayList2, new zzib(this));
        HashSet hashSet = new HashSet();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList2.size() || !m5408(Normalizer.normalize(arrayList.get(arrayList2.get(i2).m5390()), Normalizer.Form.NFKC).toLowerCase(Locale.US), (HashSet<String>) hashSet)) {
                zzht zzht = new zzht();
                this.f4728 = "";
                Iterator it2 = hashSet.iterator();
            } else {
                i = i2 + 1;
            }
        }
        zzht zzht2 = new zzht();
        this.f4728 = "";
        Iterator it22 = hashSet.iterator();
        while (it22.hasNext()) {
            try {
                zzht2.m12998(this.f4729.m5396((String) it22.next()));
            } catch (IOException e) {
                zzagf.m4793("Error while writing hash to byteStream", e);
            }
        }
        return zzht2.toString();
    }
}
