package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class zzfet implements Iterator {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f10346 = this.f10347.size();

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzfes f10347;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f10348 = 0;

    zzfet(zzfes zzfes) {
        this.f10347 = zzfes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte m12381() {
        try {
            zzfes zzfes = this.f10347;
            int i = this.f10348;
            this.f10348 = i + 1;
            return zzfes.zzkn(i);
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    public final boolean hasNext() {
        return this.f10348 < this.f10346;
    }

    public final /* synthetic */ Object next() {
        return Byte.valueOf(m12381());
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
