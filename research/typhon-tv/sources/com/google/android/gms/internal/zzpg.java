package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@zzzv
public final class zzpg extends zzpv implements View.OnClickListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private static String[] f5288 = {NativeAppInstallAd.ASSET_MEDIA_VIDEO, NativeContentAd.ASSET_MEDIA_VIDEO};
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f5289;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Map<String, WeakReference<View>> f5290 = Collections.synchronizedMap(new HashMap());

    /* renamed from: ʽ  reason: contains not printable characters */
    private View f5291;

    /* renamed from: ʾ  reason: contains not printable characters */
    private WeakReference<zzgp> f5292 = new WeakReference<>((Object) null);

    /* renamed from: ˈ  reason: contains not printable characters */
    private Point f5293 = new Point();

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzos f5294;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f5295 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Point f5296 = new Point();
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public View f5297;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final FrameLayout f5298;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Object f5299 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    FrameLayout f5300;

    @TargetApi(21)
    public zzpg(FrameLayout frameLayout, FrameLayout frameLayout2) {
        this.f5298 = frameLayout;
        this.f5300 = frameLayout2;
        zzbs.zzfc();
        zzaln.m4826((View) this.f5298, (ViewTreeObserver.OnGlobalLayoutListener) this);
        zzbs.zzfc();
        zzaln.m4827((View) this.f5298, (ViewTreeObserver.OnScrollChangedListener) this);
        this.f5298.setOnTouchListener(this);
        this.f5298.setOnClickListener(this);
        if (frameLayout2 != null && zzq.m9265()) {
            frameLayout2.setElevation(Float.MAX_VALUE);
        }
        zznh.m5599(this.f5298.getContext());
        this.f5289 = ((Boolean) zzkb.m5481().m5595(zznh.f4995)).booleanValue();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m5769() {
        synchronized (this.f5299) {
            if (!this.f5289 && this.f5295) {
                int measuredWidth = this.f5298.getMeasuredWidth();
                int measuredHeight = this.f5298.getMeasuredHeight();
                if (!(measuredWidth == 0 || measuredHeight == 0 || this.f5300 == null)) {
                    this.f5300.setLayoutParams(new FrameLayout.LayoutParams(measuredWidth, measuredHeight));
                    this.f5295 = false;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m5771(int i) {
        zzkb.m5487();
        return zzajr.m4749(this.f5294.m13187(), i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5773(View view) {
        if (this.f5294 != null) {
            zzos r0 = this.f5294 instanceof zzor ? ((zzor) this.f5294).m5723() : this.f5294;
            if (r0 != null) {
                r0.m13189(view);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0034  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5774(com.google.android.gms.internal.zzow r7) {
        /*
            r6 = this;
            java.lang.Object r2 = r6.f5299
            monitor-enter(r2)
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r0 = r6.f5290     // Catch:{ all -> 0x0042 }
            r7.m5763((java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r0)     // Catch:{ all -> 0x0042 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r0 = r6.f5290     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0032
            java.lang.String[] r3 = f5288     // Catch:{ all -> 0x0042 }
            int r4 = r3.length     // Catch:{ all -> 0x0042 }
            r0 = 0
            r1 = r0
        L_0x0011:
            if (r1 >= r4) goto L_0x0032
            r0 = r3[r1]     // Catch:{ all -> 0x0042 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r5 = r6.f5290     // Catch:{ all -> 0x0042 }
            java.lang.Object r0 = r5.get(r0)     // Catch:{ all -> 0x0042 }
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x002e
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0042 }
            android.view.View r0 = (android.view.View) r0     // Catch:{ all -> 0x0042 }
        L_0x0025:
            boolean r1 = r0 instanceof android.widget.FrameLayout     // Catch:{ all -> 0x0042 }
            if (r1 != 0) goto L_0x0034
            r7.m5743()     // Catch:{ all -> 0x0042 }
            monitor-exit(r2)     // Catch:{ all -> 0x0042 }
        L_0x002d:
            return
        L_0x002e:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0011
        L_0x0032:
            r0 = 0
            goto L_0x0025
        L_0x0034:
            com.google.android.gms.internal.zzpi r1 = new com.google.android.gms.internal.zzpi     // Catch:{ all -> 0x0042 }
            r1.<init>(r6, r0)     // Catch:{ all -> 0x0042 }
            boolean r3 = r7 instanceof com.google.android.gms.internal.zzor     // Catch:{ all -> 0x0042 }
            if (r3 == 0) goto L_0x0045
            r7.m5752((android.view.View) r0, (com.google.android.gms.internal.zzoq) r1)     // Catch:{ all -> 0x0042 }
        L_0x0040:
            monitor-exit(r2)     // Catch:{ all -> 0x0042 }
            goto L_0x002d
        L_0x0042:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0042 }
            throw r0
        L_0x0045:
            r7.m5758((android.view.View) r0, (com.google.android.gms.internal.zzoq) r1)     // Catch:{ all -> 0x0042 }
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpg.m5774(com.google.android.gms.internal.zzow):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r8) {
        /*
            r7 = this;
            java.lang.Object r6 = r7.f5299
            monitor-enter(r6)
            com.google.android.gms.internal.zzos r0 = r7.f5294     // Catch:{ all -> 0x007b }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r6)     // Catch:{ all -> 0x007b }
        L_0x0008:
            return
        L_0x0009:
            android.os.Bundle r3 = new android.os.Bundle     // Catch:{ all -> 0x007b }
            r3.<init>()     // Catch:{ all -> 0x007b }
            java.lang.String r0 = "x"
            android.graphics.Point r1 = r7.f5296     // Catch:{ all -> 0x007b }
            int r1 = r1.x     // Catch:{ all -> 0x007b }
            int r1 = r7.m5771((int) r1)     // Catch:{ all -> 0x007b }
            float r1 = (float) r1     // Catch:{ all -> 0x007b }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x007b }
            java.lang.String r0 = "y"
            android.graphics.Point r1 = r7.f5296     // Catch:{ all -> 0x007b }
            int r1 = r1.y     // Catch:{ all -> 0x007b }
            int r1 = r7.m5771((int) r1)     // Catch:{ all -> 0x007b }
            float r1 = (float) r1     // Catch:{ all -> 0x007b }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x007b }
            java.lang.String r0 = "start_x"
            android.graphics.Point r1 = r7.f5293     // Catch:{ all -> 0x007b }
            int r1 = r1.x     // Catch:{ all -> 0x007b }
            int r1 = r7.m5771((int) r1)     // Catch:{ all -> 0x007b }
            float r1 = (float) r1     // Catch:{ all -> 0x007b }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x007b }
            java.lang.String r0 = "start_y"
            android.graphics.Point r1 = r7.f5293     // Catch:{ all -> 0x007b }
            int r1 = r1.y     // Catch:{ all -> 0x007b }
            int r1 = r7.m5771((int) r1)     // Catch:{ all -> 0x007b }
            float r1 = (float) r1     // Catch:{ all -> 0x007b }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x007b }
            android.view.View r0 = r7.f5291     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x008c
            android.view.View r0 = r7.f5291     // Catch:{ all -> 0x007b }
            boolean r0 = r0.equals(r8)     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x008c
            com.google.android.gms.internal.zzos r0 = r7.f5294     // Catch:{ all -> 0x007b }
            boolean r0 = r0 instanceof com.google.android.gms.internal.zzor     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x007e
            com.google.android.gms.internal.zzos r0 = r7.f5294     // Catch:{ all -> 0x007b }
            com.google.android.gms.internal.zzor r0 = (com.google.android.gms.internal.zzor) r0     // Catch:{ all -> 0x007b }
            com.google.android.gms.internal.zzos r0 = r0.m5723()     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x0079
            com.google.android.gms.internal.zzos r0 = r7.f5294     // Catch:{ all -> 0x007b }
            com.google.android.gms.internal.zzor r0 = (com.google.android.gms.internal.zzor) r0     // Catch:{ all -> 0x007b }
            com.google.android.gms.internal.zzos r0 = r0.m5723()     // Catch:{ all -> 0x007b }
            java.lang.String r2 = "1007"
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r7.f5290     // Catch:{ all -> 0x007b }
            android.widget.FrameLayout r5 = r7.f5298     // Catch:{ all -> 0x007b }
            r1 = r8
            r0.m13198(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x007b }
        L_0x0079:
            monitor-exit(r6)     // Catch:{ all -> 0x007b }
            goto L_0x0008
        L_0x007b:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x007b }
            throw r0
        L_0x007e:
            com.google.android.gms.internal.zzos r0 = r7.f5294     // Catch:{ all -> 0x007b }
            java.lang.String r2 = "1007"
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r7.f5290     // Catch:{ all -> 0x007b }
            android.widget.FrameLayout r5 = r7.f5298     // Catch:{ all -> 0x007b }
            r1 = r8
            r0.m13198(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x007b }
            goto L_0x0079
        L_0x008c:
            com.google.android.gms.internal.zzos r0 = r7.f5294     // Catch:{ all -> 0x007b }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r1 = r7.f5290     // Catch:{ all -> 0x007b }
            android.widget.FrameLayout r2 = r7.f5298     // Catch:{ all -> 0x007b }
            r0.m13200(r8, r1, r3, r2)     // Catch:{ all -> 0x007b }
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpg.onClick(android.view.View):void");
    }

    public final void onGlobalLayout() {
        synchronized (this.f5299) {
            m5769();
            if (this.f5294 != null) {
                this.f5294.m13193(this.f5298, this.f5290);
            }
        }
    }

    public final void onScrollChanged() {
        synchronized (this.f5299) {
            if (this.f5294 != null) {
                this.f5294.m13193(this.f5298, this.f5290);
            }
            m5769();
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.f5299) {
            if (this.f5294 != null) {
                int[] iArr = new int[2];
                this.f5298.getLocationOnScreen(iArr);
                Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
                this.f5296 = point;
                if (motionEvent.getAction() == 0) {
                    this.f5293 = point;
                }
                MotionEvent obtain = MotionEvent.obtain(motionEvent);
                obtain.setLocation((float) point.x, (float) point.y);
                this.f5294.m13195(obtain);
                obtain.recycle();
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m5777(String str) {
        View view = null;
        synchronized (this.f5299) {
            if (this.f5290 == null) {
                return null;
            }
            WeakReference weakReference = this.f5290.get(str);
            if (weakReference != null) {
                view = (View) weakReference.get();
            }
            IObjectWrapper r0 = zzn.m9306(view);
            return r0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5778() {
        synchronized (this.f5299) {
            if (this.f5300 != null) {
                this.f5300.removeAllViews();
            }
            this.f5300 = null;
            this.f5290 = null;
            this.f5291 = null;
            this.f5294 = null;
            this.f5296 = null;
            this.f5293 = null;
            this.f5292 = null;
            this.f5297 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5779(com.google.android.gms.dynamic.IObjectWrapper r11) {
        /*
            r10 = this;
            r3 = 1
            r5 = 0
            r4 = 0
            java.lang.Object r7 = r10.f5299
            monitor-enter(r7)
            r1 = 0
            r10.m5773((android.view.View) r1)     // Catch:{ all -> 0x00b0 }
            java.lang.Object r1 = com.google.android.gms.dynamic.zzn.m9307((com.google.android.gms.dynamic.IObjectWrapper) r11)     // Catch:{ all -> 0x00b0 }
            boolean r2 = r1 instanceof com.google.android.gms.internal.zzow     // Catch:{ all -> 0x00b0 }
            if (r2 != 0) goto L_0x001a
            java.lang.String r1 = "Not an instance of native engine. This is most likely a transient error"
            com.google.android.gms.internal.zzagf.m4791(r1)     // Catch:{ all -> 0x00b0 }
            monitor-exit(r7)     // Catch:{ all -> 0x00b0 }
        L_0x0019:
            return
        L_0x001a:
            boolean r2 = r10.f5289     // Catch:{ all -> 0x00b0 }
            if (r2 != 0) goto L_0x0033
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0033
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout$LayoutParams r6 = new android.widget.FrameLayout$LayoutParams     // Catch:{ all -> 0x00b0 }
            r8 = 0
            r9 = 0
            r6.<init>(r8, r9)     // Catch:{ all -> 0x00b0 }
            r2.setLayoutParams(r6)     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r2 = r10.f5298     // Catch:{ all -> 0x00b0 }
            r2.requestLayout()     // Catch:{ all -> 0x00b0 }
        L_0x0033:
            r2 = 1
            r10.f5295 = r2     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzow r1 = (com.google.android.gms.internal.zzow) r1     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0057
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r2 = com.google.android.gms.internal.zznh.f4987     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zznf r6 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x00b0 }
            java.lang.Object r2 = r6.m5595(r2)     // Catch:{ all -> 0x00b0 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x00b0 }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0057
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r6 = r10.f5298     // Catch:{ all -> 0x00b0 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r8 = r10.f5290     // Catch:{ all -> 0x00b0 }
            r2.m13190(r6, r8)     // Catch:{ all -> 0x00b0 }
        L_0x0057:
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            boolean r2 = r2 instanceof com.google.android.gms.internal.zzow     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0092
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzow r2 = (com.google.android.gms.internal.zzow) r2     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0092
            android.content.Context r6 = r2.m5745()     // Catch:{ all -> 0x00b0 }
            if (r6 == 0) goto L_0x0092
            com.google.android.gms.internal.zzaff r6 = com.google.android.gms.ads.internal.zzbs.zzfd()     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r8 = r10.f5298     // Catch:{ all -> 0x00b0 }
            android.content.Context r8 = r8.getContext()     // Catch:{ all -> 0x00b0 }
            boolean r6 = r6.m4432(r8)     // Catch:{ all -> 0x00b0 }
            if (r6 == 0) goto L_0x0092
            com.google.android.gms.internal.zzafe r6 = r2.m5746()     // Catch:{ all -> 0x00b0 }
            if (r6 == 0) goto L_0x0083
            r2 = 0
            r6.m4408((boolean) r2)     // Catch:{ all -> 0x00b0 }
        L_0x0083:
            java.lang.ref.WeakReference<com.google.android.gms.internal.zzgp> r2 = r10.f5292     // Catch:{ all -> 0x00b0 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzgp r2 = (com.google.android.gms.internal.zzgp) r2     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0092
            if (r6 == 0) goto L_0x0092
            r2.m5355((com.google.android.gms.internal.zzgt) r6)     // Catch:{ all -> 0x00b0 }
        L_0x0092:
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            boolean r2 = r2 instanceof com.google.android.gms.internal.zzor     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x00b3
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzor r2 = (com.google.android.gms.internal.zzor) r2     // Catch:{ all -> 0x00b0 }
            boolean r2 = r2.m5724()     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x00b3
            com.google.android.gms.internal.zzos r2 = r10.f5294     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzor r2 = (com.google.android.gms.internal.zzor) r2     // Catch:{ all -> 0x00b0 }
            r2.m5729(r1)     // Catch:{ all -> 0x00b0 }
        L_0x00a9:
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            if (r2 != 0) goto L_0x00c2
            monitor-exit(r7)     // Catch:{ all -> 0x00b0 }
            goto L_0x0019
        L_0x00b0:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00b0 }
            throw r1
        L_0x00b3:
            r10.f5294 = r1     // Catch:{ all -> 0x00b0 }
            boolean r2 = r1 instanceof com.google.android.gms.internal.zzor     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x00a9
            r0 = r1
            com.google.android.gms.internal.zzor r0 = (com.google.android.gms.internal.zzor) r0     // Catch:{ all -> 0x00b0 }
            r2 = r0
            r6 = 0
            r2.m5729(r6)     // Catch:{ all -> 0x00b0 }
            goto L_0x00a9
        L_0x00c2:
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r2 = com.google.android.gms.internal.zznh.f4987     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zznf r6 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x00b0 }
            java.lang.Object r2 = r6.m5595(r2)     // Catch:{ all -> 0x00b0 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x00b0 }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x00da
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            r6 = 0
            r2.setClickable(r6)     // Catch:{ all -> 0x00b0 }
        L_0x00da:
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            r2.removeAllViews()     // Catch:{ all -> 0x00b0 }
            boolean r6 = r1.m5764()     // Catch:{ all -> 0x00b0 }
            if (r6 == 0) goto L_0x0103
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r10.f5290     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0103
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r10.f5290     // Catch:{ all -> 0x00b0 }
            java.lang.String r8 = "1098"
            java.lang.Object r2 = r2.get(r8)     // Catch:{ all -> 0x00b0 }
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0196
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x00b0 }
            android.view.View r2 = (android.view.View) r2     // Catch:{ all -> 0x00b0 }
        L_0x00fc:
            boolean r8 = r2 instanceof android.view.ViewGroup     // Catch:{ all -> 0x00b0 }
            if (r8 == 0) goto L_0x0103
            android.view.ViewGroup r2 = (android.view.ViewGroup) r2     // Catch:{ all -> 0x00b0 }
            r5 = r2
        L_0x0103:
            if (r6 == 0) goto L_0x0199
            if (r5 == 0) goto L_0x0199
            r2 = r3
        L_0x0108:
            android.view.View r3 = r1.m5755((android.view.View.OnClickListener) r10, (boolean) r2)     // Catch:{ all -> 0x00b0 }
            r10.f5291 = r3     // Catch:{ all -> 0x00b0 }
            android.view.View r3 = r10.f5291     // Catch:{ all -> 0x00b0 }
            if (r3 == 0) goto L_0x012c
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r10.f5290     // Catch:{ all -> 0x00b0 }
            if (r3 == 0) goto L_0x0125
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r10.f5290     // Catch:{ all -> 0x00b0 }
            java.lang.String r4 = "1007"
            java.lang.ref.WeakReference r6 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x00b0 }
            android.view.View r8 = r10.f5291     // Catch:{ all -> 0x00b0 }
            r6.<init>(r8)     // Catch:{ all -> 0x00b0 }
            r3.put(r4, r6)     // Catch:{ all -> 0x00b0 }
        L_0x0125:
            if (r2 == 0) goto L_0x019c
            r5.removeAllViews()     // Catch:{ all -> 0x00b0 }
            android.view.View r2 = r10.f5291     // Catch:{ all -> 0x00b0 }
        L_0x012c:
            android.widget.FrameLayout r2 = r10.f5298     // Catch:{ all -> 0x00b0 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r10.f5290     // Catch:{ all -> 0x00b0 }
            r4 = 0
            r5 = r10
            r6 = r10
            r1.m5762((android.view.View) r2, (java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r3, (java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r4, (android.view.View.OnTouchListener) r5, (android.view.View.OnClickListener) r6)     // Catch:{ all -> 0x00b0 }
            android.os.Handler r2 = com.google.android.gms.internal.zzahn.f4212     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzph r3 = new com.google.android.gms.internal.zzph     // Catch:{ all -> 0x00b0 }
            r3.<init>(r10, r1)     // Catch:{ all -> 0x00b0 }
            r2.post(r3)     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r1 = r10.f5298     // Catch:{ all -> 0x00b0 }
            r10.m5773((android.view.View) r1)     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzos r1 = r10.f5294     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r2 = r10.f5298     // Catch:{ all -> 0x00b0 }
            r1.m13196((android.view.View) r2)     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzos r1 = r10.f5294     // Catch:{ all -> 0x00b0 }
            boolean r1 = r1 instanceof com.google.android.gms.internal.zzow     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x0193
            com.google.android.gms.internal.zzos r1 = r10.f5294     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzow r1 = (com.google.android.gms.internal.zzow) r1     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x0193
            android.content.Context r2 = r1.m5745()     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0193
            com.google.android.gms.internal.zzaff r2 = com.google.android.gms.ads.internal.zzbs.zzfd()     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r3 = r10.f5298     // Catch:{ all -> 0x00b0 }
            android.content.Context r3 = r3.getContext()     // Catch:{ all -> 0x00b0 }
            boolean r2 = r2.m4432(r3)     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0193
            java.lang.ref.WeakReference<com.google.android.gms.internal.zzgp> r2 = r10.f5292     // Catch:{ all -> 0x00b0 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.internal.zzgp r2 = (com.google.android.gms.internal.zzgp) r2     // Catch:{ all -> 0x00b0 }
            if (r2 != 0) goto L_0x018c
            com.google.android.gms.internal.zzgp r2 = new com.google.android.gms.internal.zzgp     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r3 = r10.f5298     // Catch:{ all -> 0x00b0 }
            android.content.Context r3 = r3.getContext()     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r4 = r10.f5298     // Catch:{ all -> 0x00b0 }
            r2.<init>(r3, r4)     // Catch:{ all -> 0x00b0 }
            java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x00b0 }
            r3.<init>(r2)     // Catch:{ all -> 0x00b0 }
            r10.f5292 = r3     // Catch:{ all -> 0x00b0 }
        L_0x018c:
            com.google.android.gms.internal.zzafe r1 = r1.m5746()     // Catch:{ all -> 0x00b0 }
            r2.m5357((com.google.android.gms.internal.zzgt) r1)     // Catch:{ all -> 0x00b0 }
        L_0x0193:
            monitor-exit(r7)     // Catch:{ all -> 0x00b0 }
            goto L_0x0019
        L_0x0196:
            r2 = r5
            goto L_0x00fc
        L_0x0199:
            r2 = r4
            goto L_0x0108
        L_0x019c:
            android.content.Context r2 = r1.m5745()     // Catch:{ all -> 0x00b0 }
            com.google.android.gms.ads.formats.AdChoicesView r3 = new com.google.android.gms.ads.formats.AdChoicesView     // Catch:{ all -> 0x00b0 }
            r3.<init>(r2)     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams     // Catch:{ all -> 0x00b0 }
            r4 = -1
            r5 = -1
            r2.<init>(r4, r5)     // Catch:{ all -> 0x00b0 }
            r3.setLayoutParams(r2)     // Catch:{ all -> 0x00b0 }
            android.view.View r2 = r10.f5291     // Catch:{ all -> 0x00b0 }
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x012c
            android.widget.FrameLayout r2 = r10.f5300     // Catch:{ all -> 0x00b0 }
            goto L_0x012c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpg.m5779(com.google.android.gms.dynamic.IObjectWrapper):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5780(IObjectWrapper iObjectWrapper, int i) {
        zzgp zzgp;
        if (!(!zzbs.zzfd().m4432(this.f5298.getContext()) || this.f5292 == null || (zzgp = (zzgp) this.f5292.get()) == null)) {
            zzgp.m5356();
        }
        m5769();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5781(java.lang.String r5, com.google.android.gms.dynamic.IObjectWrapper r6) {
        /*
            r4 = this;
            java.lang.Object r0 = com.google.android.gms.dynamic.zzn.m9307((com.google.android.gms.dynamic.IObjectWrapper) r6)
            android.view.View r0 = (android.view.View) r0
            java.lang.Object r1 = r4.f5299
            monitor-enter(r1)
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r4.f5290     // Catch:{ all -> 0x0018 }
            if (r2 != 0) goto L_0x000f
            monitor-exit(r1)     // Catch:{ all -> 0x0018 }
        L_0x000e:
            return
        L_0x000f:
            if (r0 != 0) goto L_0x001b
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r0 = r4.f5290     // Catch:{ all -> 0x0018 }
            r0.remove(r5)     // Catch:{ all -> 0x0018 }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x0018 }
            goto L_0x000e
        L_0x0018:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0018 }
            throw r0
        L_0x001b:
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r4.f5290     // Catch:{ all -> 0x0018 }
            java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0018 }
            r3.<init>(r0)     // Catch:{ all -> 0x0018 }
            r2.put(r5, r3)     // Catch:{ all -> 0x0018 }
            java.lang.String r2 = "1098"
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x0018 }
            if (r2 == 0) goto L_0x0030
            monitor-exit(r1)     // Catch:{ all -> 0x0018 }
            goto L_0x000e
        L_0x0030:
            r0.setOnTouchListener(r4)     // Catch:{ all -> 0x0018 }
            r2 = 1
            r0.setClickable(r2)     // Catch:{ all -> 0x0018 }
            r0.setOnClickListener(r4)     // Catch:{ all -> 0x0018 }
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpg.m5781(java.lang.String, com.google.android.gms.dynamic.IObjectWrapper):void");
    }
}
