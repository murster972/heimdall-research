package com.google.android.gms.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.zzbq;

public final class zzcia {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzchx f9328;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f9329;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f9330;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f9331;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f9332;

    public zzcia(zzchx zzchx, String str, long j) {
        this.f9328 = zzchx;
        zzbq.m9122(str);
        this.f9332 = str;
        this.f9329 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10902() {
        if (!this.f9331) {
            this.f9331 = true;
            this.f9330 = this.f9328.m10885().getLong(this.f9332, this.f9329);
        }
        return this.f9330;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10903(long j) {
        SharedPreferences.Editor edit = this.f9328.m10885().edit();
        edit.putLong(this.f9332, j);
        edit.apply();
        this.f9330 = j;
    }
}
