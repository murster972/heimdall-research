package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzaim {

    /* renamed from: 连任  reason: contains not printable characters */
    private String f4236 = "";

    /* renamed from: 靐  reason: contains not printable characters */
    private String f4237 = "";

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f4238 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f4239 = "";

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4240 = new Object();

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m4694(Context context, String str, String str2) {
        zzbs.zzei();
        zzahn.m4609(context, m4697(context, (String) zzkb.m5481().m5595(zznh.f5031), str, str2));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean m4695(Context context, String str, String str2) {
        String r0 = m4696(context, m4697(context, (String) zzkb.m5481().m5595(zznh.f5032), str, str2).toString(), str2);
        if (TextUtils.isEmpty(r0)) {
            zzagf.m4792("Not linked for in app preview.");
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(r0.trim());
            String optString = jSONObject.optString("gct");
            this.f4236 = jSONObject.optString(NotificationCompat.CATEGORY_STATUS);
            synchronized (this.f4240) {
                this.f4239 = optString;
            }
            return true;
        } catch (JSONException e) {
            zzagf.m4796("Fail to get in app preview response json.", e);
            return false;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static String m4696(Context context, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, zzbs.zzei().m4632(context, str2));
        zzakv<String> r2 = new zzaiv(context).m4714(str, (Map<String, String>) hashMap);
        try {
            return (String) r2.get((long) ((Integer) zzkb.m5481().m5595(zznh.f5035)).intValue(), TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            TimeoutException timeoutException = e;
            String valueOf = String.valueOf(str);
            zzagf.m4793(valueOf.length() != 0 ? "Timeout while retriving a response from: ".concat(valueOf) : new String("Timeout while retriving a response from: "), timeoutException);
            r2.cancel(true);
        } catch (InterruptedException e2) {
            InterruptedException interruptedException = e2;
            String valueOf2 = String.valueOf(str);
            zzagf.m4793(valueOf2.length() != 0 ? "Interrupted while retriving a response from: ".concat(valueOf2) : new String("Interrupted while retriving a response from: "), interruptedException);
            r2.cancel(true);
        } catch (Exception e3) {
            Exception exc = e3;
            String valueOf3 = String.valueOf(str);
            zzagf.m4793(valueOf3.length() != 0 ? "Error retriving a response from: ".concat(valueOf3) : new String("Error retriving a response from: "), exc);
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final Uri m4697(Context context, String str, String str2, String str3) {
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter("linkedDeviceId", m4699(context));
        buildUpon.appendQueryParameter("adSlotPath", str2);
        buildUpon.appendQueryParameter("afmaVersion", str3);
        return buildUpon.build();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m4698(Context context, String str, String str2) {
        String r0 = m4696(context, m4697(context, (String) zzkb.m5481().m5595(zznh.f5033), str, str2).toString(), str2);
        if (TextUtils.isEmpty(r0)) {
            zzagf.m4792("Not linked for debug signals.");
            return false;
        }
        try {
            boolean equals = PubnativeRequest.LEGACY_ZONE_ID.equals(new JSONObject(r0.trim()).optString("debug_mode"));
            synchronized (this.f4240) {
                this.f4238 = equals;
            }
            return equals;
        } catch (JSONException e) {
            zzagf.m4796("Fail to get debug mode response json.", e);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final String m4699(Context context) {
        String str;
        synchronized (this.f4240) {
            if (TextUtils.isEmpty(this.f4237)) {
                zzbs.zzei();
                this.f4237 = zzahn.m4579(context, "debug_signals_id.txt");
                if (TextUtils.isEmpty(this.f4237)) {
                    zzbs.zzei();
                    this.f4237 = zzahn.m4597();
                    zzbs.zzei();
                    zzahn.m4588(context, "debug_signals_id.txt", this.f4237);
                }
            }
            str = this.f4237;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4700(Context context, String str, boolean z, boolean z2) {
        if (!(context instanceof Activity)) {
            zzagf.m4794("Can not create dialog without Activity Context");
        } else {
            zzahn.f4212.post(new zzain(this, context, str, z, z2));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4701(Context context, String str, String str2, String str3) {
        Uri.Builder buildUpon = m4697(context, (String) zzkb.m5481().m5595(zznh.f5034), str3, str).buildUpon();
        buildUpon.appendQueryParameter("debugData", str2);
        zzbs.zzei();
        zzahn.m4582(context, str, buildUpon.build().toString());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4702() {
        boolean z;
        synchronized (this.f4240) {
            z = this.f4238;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m4703() {
        String str;
        synchronized (this.f4240) {
            str = this.f4239;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4704(Context context, String str, String str2) {
        if (!m4695(context, str, str2)) {
            m4700(context, "In-app preview failed to load because of a system error. Please try again later.", true, true);
        } else if ("2".equals(this.f4236)) {
            zzagf.m4792("Creative is not pushed for this device.");
            m4700(context, "There was no creative pushed from DFP to the device.", false, false);
        } else if (PubnativeRequest.LEGACY_ZONE_ID.equals(this.f4236)) {
            zzagf.m4792("The app is not linked for creative preview.");
            m4694(context, str, str2);
        } else if ("0".equals(this.f4236)) {
            zzagf.m4792("Device is linked for in app preview.");
            m4700(context, "The device is successfully linked for creative preview.", false, true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4705(Context context, String str, String str2, String str3) {
        boolean r0 = m4702();
        if (m4698(context, str, str2)) {
            if (!r0 && !TextUtils.isEmpty(str3)) {
                m4701(context, str2, str3, str);
            }
            zzagf.m4792("Device is linked for debug signals.");
            m4700(context, "The device is successfully linked for troubleshooting.", false, true);
            return;
        }
        m4694(context, str, str2);
    }
}
