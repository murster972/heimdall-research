package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;

@zzzv
public final class zzamh extends zzamc {
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzamb m4896(Context context, zzamp zzamp, int i, boolean z, zznu zznu, zzamo zzamo) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (!(applicationInfo == null || applicationInfo.targetSdkVersion >= 11)) {
            return null;
        }
        return new zzalr(context, z, zzamp.m5218().m5227(), zzamo, new zzamq(context, zzamp.m4923(), zzamp.m4921(), zznu, zzamp.m4929()));
    }
}
