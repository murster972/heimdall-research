package com.google.android.gms.internal;

import android.content.Context;

@zzzv
public final class zztq {

    /* renamed from: 靐  reason: contains not printable characters */
    private zztx f5390;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f5391 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    public final zztx m5869(Context context, zzakd zzakd) {
        zztx zztx;
        synchronized (this.f5391) {
            if (this.f5390 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext != null) {
                    context = applicationContext;
                }
                this.f5390 = new zztx(context, zzakd, (String) zzkb.m5481().m5595(zznh.f5159));
            }
            zztx = this.f5390;
        }
        return zztx;
    }
}
