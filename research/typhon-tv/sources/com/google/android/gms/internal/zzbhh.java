package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzr;

final class zzbhh extends Api.zza<zzbhy, Object> {
    zzbhh() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Api.zze m10232(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return new zzbhy(context, looper, zzr, connectionCallbacks, onConnectionFailedListener);
    }
}
