package com.google.android.gms.internal;

import android.content.Context;

@zzzv
public final class zzagd {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4526(Context context) {
        if (zzajv.m4784(context) && !zzajv.m4773()) {
            zzagf.m4794("Updating ad debug logging enablement.");
            zzakj.m4801((zzakv) new zzage(context).m4523(), "AdDebugLogUpdater.updateEnablement");
        }
    }
}
