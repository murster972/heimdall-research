package com.google.android.gms.internal;

import android.support.v4.app.NotificationCompat;
import java.util.HashMap;
import java.util.Map;

final class zzanc implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8330;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzana f8331;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f8332;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f8333;

    zzanc(zzana zzana, String str, String str2, int i) {
        this.f8331 = zzana;
        this.f8333 = str;
        this.f8330 = str2;
        this.f8332 = i;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, "precacheComplete");
        hashMap.put("src", this.f8333);
        hashMap.put("cachedSrc", this.f8330);
        hashMap.put("totalBytes", Integer.toString(this.f8332));
        this.f8331.m4961("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
