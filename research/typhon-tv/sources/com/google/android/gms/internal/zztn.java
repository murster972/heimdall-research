package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;

@zzzv
final class zztn {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final long f5385 = ((Long) zzkb.m5481().m5595(zznh.f4935)).longValue();

    /* renamed from: 靐  reason: contains not printable characters */
    private static final float f5386 = ((Float) zzkb.m5481().m5595(zznh.f4936)).floatValue();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final float f5387 = ((Float) zzkb.m5481().m5595(zznh.f4937)).floatValue();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final long f5388 = ((Long) zzkb.m5481().m5595(zznh.f4934)).longValue();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zztk f5389 = zztk.m5852();

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m5866(long j, int i) {
        return (int) ((j >>> ((i % 16) * 4)) & 15);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m5867() {
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int r2 = f5389.m5855();
        int r3 = f5389.m5856();
        int r4 = f5389.m5853() + f5389.m5854();
        if (r3 <= ((r2 >= 16 || f5385 == 0) ? f5387 != 0.0f ? ((int) (f5387 * ((float) r2))) + 1 : Integer.MAX_VALUE : m5866(f5385, r2))) {
            if (r2 < 16 && f5388 != 0) {
                i = m5866(f5388, r2);
            } else if (f5386 != 0.0f) {
                i = (int) (f5386 * ((float) r2));
            }
            if (r4 <= i) {
                return true;
            }
        }
        return false;
    }
}
