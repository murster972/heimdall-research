package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.formats.NativeAdOptions;

@zzzv
public final class zzpe extends zzbfm {
    public static final Parcelable.Creator<zzpe> CREATOR = new zzpf();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzmr f5282;

    /* renamed from: 连任  reason: contains not printable characters */
    public final int f5283;

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f5284;

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean f5285;

    /* renamed from: 齉  reason: contains not printable characters */
    public final int f5286;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f5287;

    public zzpe(int i, boolean z, int i2, boolean z2, int i3, zzmr zzmr) {
        this.f5287 = i;
        this.f5284 = z;
        this.f5286 = i2;
        this.f5285 = z2;
        this.f5283 = i3;
        this.f5282 = zzmr;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public zzpe(NativeAdOptions nativeAdOptions) {
        this(3, nativeAdOptions.shouldReturnUrlsForImageAssets(), nativeAdOptions.getImageOrientation(), nativeAdOptions.shouldRequestMultipleImages(), nativeAdOptions.getAdChoicesPlacement(), nativeAdOptions.getVideoOptions() != null ? new zzmr(nativeAdOptions.getVideoOptions()) : null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f5287);
        zzbfp.m10195(parcel, 2, this.f5284);
        zzbfp.m10185(parcel, 3, this.f5286);
        zzbfp.m10195(parcel, 4, this.f5285);
        zzbfp.m10185(parcel, 5, this.f5283);
        zzbfp.m10189(parcel, 6, (Parcelable) this.f5282, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
