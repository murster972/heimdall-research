package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.zzbs;
import java.util.Map;

@zzzv
public final class zzxa extends zzxb implements zzt<zzanh> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f5542;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f5543 = -1;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f5544 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f5545 = -1;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f5546 = -1;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f5547;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f5548 = -1;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f5549 = -1;

    /* renamed from: 连任  reason: contains not printable characters */
    private DisplayMetrics f5550;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f5551;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzmt f5552;

    /* renamed from: 齉  reason: contains not printable characters */
    private final WindowManager f5553;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzanh f5554;

    public zzxa(zzanh zzanh, Context context, zzmt zzmt) {
        super(zzanh);
        this.f5554 = zzanh;
        this.f5551 = context;
        this.f5552 = zzmt;
        this.f5553 = (WindowManager) context.getSystemService("window");
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        this.f5550 = new DisplayMetrics();
        Display defaultDisplay = this.f5553.getDefaultDisplay();
        defaultDisplay.getMetrics(this.f5550);
        this.f5542 = this.f5550.density;
        this.f5547 = defaultDisplay.getRotation();
        zzkb.m5487();
        this.f5543 = zzajr.m4750(this.f5550, this.f5550.widthPixels);
        zzkb.m5487();
        this.f5544 = zzajr.m4750(this.f5550, this.f5550.heightPixels);
        Activity r0 = this.f5554.m5003();
        if (r0 == null || r0.getWindow() == null) {
            this.f5548 = this.f5543;
            this.f5549 = this.f5544;
        } else {
            zzbs.zzei();
            int[] r02 = zzahn.m4622(r0);
            zzkb.m5487();
            this.f5548 = zzajr.m4750(this.f5550, r02[0]);
            zzkb.m5487();
            this.f5549 = zzajr.m4750(this.f5550, r02[1]);
        }
        if (this.f5554.m4983().m5227()) {
            this.f5546 = this.f5543;
            this.f5545 = this.f5544;
        } else {
            this.f5554.measure(0, 0);
        }
        m6011(this.f5543, this.f5544, this.f5548, this.f5549, this.f5542, this.f5547);
        this.f5554.zza("onDeviceFeaturesReceived", new zzwx(new zzwz().m13627(this.f5552.m5573()).m13630(this.f5552.m5570()).m13629(this.f5552.m5571()).m13628(this.f5552.m5572()).m13626(true)).m6005());
        int[] iArr = new int[2];
        this.f5554.getLocationOnScreen(iArr);
        zzkb.m5487();
        int r1 = zzajr.m4749(this.f5551, iArr[0]);
        zzkb.m5487();
        m6006(r1, zzajr.m4749(this.f5551, iArr[1]));
        if (zzagf.m4798(2)) {
            zzagf.m4794("Dispatching Ready Event.");
        }
        m6008(this.f5554.m4985().f4297);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6006(int i, int i2) {
        int i3 = this.f5551 instanceof Activity ? zzbs.zzei().m4631((Activity) this.f5551)[0] : 0;
        if (this.f5554.m4983() == null || !this.f5554.m4983().m5227()) {
            zzkb.m5487();
            this.f5546 = zzajr.m4749(this.f5551, this.f5554.getWidth());
            zzkb.m5487();
            this.f5545 = zzajr.m4749(this.f5551, this.f5554.getHeight());
        }
        m6007(i, i2 - i3, this.f5546, this.f5545);
        this.f5554.m4980().m5044(i, i2);
    }
}
