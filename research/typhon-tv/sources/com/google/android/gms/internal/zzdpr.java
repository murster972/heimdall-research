package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtf;

public final class zzdpr {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdtf m11651(String str, String str2, String str3, int i, boolean z) {
        zzdtf.zza r1 = zzdtf.m12039().m12063(str2);
        String valueOf = String.valueOf(str3);
        return (zzdtf) r1.m12060(valueOf.length() != 0 ? "type.googleapis.com/google.crypto.tink.".concat(valueOf) : new String("type.googleapis.com/google.crypto.tink.")).m12062(0).m12064(true).m12061(str).m12542();
    }
}
