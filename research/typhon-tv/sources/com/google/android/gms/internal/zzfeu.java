package com.google.android.gms.internal;

import java.util.Arrays;

final class zzfeu implements zzfew {
    private zzfeu() {
    }

    /* synthetic */ zzfeu(zzfet zzfet) {
        this();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12382(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i + i2);
    }
}
