package com.google.android.gms.internal;

import android.app.Activity;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;

final class zzka extends zzjr.zza<zzxe> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzjr f10789;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f10790;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzka(zzjr zzjr, Activity activity) {
        super();
        this.f10789 = zzjr;
        this.f10790 = activity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13051() throws RemoteException {
        zzxe r0 = this.f10789.f4804.m6013(this.f10790);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10790, "ad_overlay");
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13052(zzla zzla) throws RemoteException {
        return zzla.createAdOverlay(zzn.m9306(this.f10790));
    }
}
