package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.Choreographer;

public abstract class zzdmm {

    /* renamed from: 靐  reason: contains not printable characters */
    private Choreographer.FrameCallback f9899;

    /* renamed from: 龘  reason: contains not printable characters */
    private Runnable f9900;

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final Runnable m11636() {
        if (this.f9900 == null) {
            this.f9900 = new zzdmo(this);
        }
        return this.f9900;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(16)
    /* renamed from: 龘  reason: contains not printable characters */
    public final Choreographer.FrameCallback m11637() {
        if (this.f9899 == null) {
            this.f9899 = new zzdmn(this);
        }
        return this.f9899;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m11638(long j);
}
