package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;

public final class zzaiu {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<Double> f8237 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<Double> f8238 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<String> f8239 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzair m9639() {
        return new zzair(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaiu m9640(String str, double d, double d2) {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= this.f8239.size()) {
                break;
            }
            double doubleValue = this.f8238.get(i).doubleValue();
            double doubleValue2 = this.f8237.get(i).doubleValue();
            if (d < doubleValue || (doubleValue == d && d2 < doubleValue2)) {
                break;
            }
            i2 = i + 1;
        }
        this.f8239.add(i, str);
        this.f8238.add(i, Double.valueOf(d));
        this.f8237.add(i, Double.valueOf(d2));
        return this;
    }
}
