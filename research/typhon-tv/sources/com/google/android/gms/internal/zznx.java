package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zznx extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    String m13167() throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m13168() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13169() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13170() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13171(IObjectWrapper iObjectWrapper) throws RemoteException;
}
