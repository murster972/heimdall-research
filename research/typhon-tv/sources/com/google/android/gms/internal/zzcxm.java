package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzan;

public final class zzcxm extends zzeu implements zzcxl {
    zzcxm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11565(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(7, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11566(zzan zzan, int i, boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzan);
        v_.writeInt(i);
        zzew.m12307(v_, z);
        m12298(9, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11567(zzcxo zzcxo, zzcxj zzcxj) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcxo);
        zzew.m12305(v_, (IInterface) zzcxj);
        m12298(12, v_);
    }
}
