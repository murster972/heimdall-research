package com.google.android.gms.internal;

import com.google.android.gms.measurement.AppMeasurement$zzb;

final class zzckf extends AppMeasurement$zzb {

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f9554;

    public zzckf(zzckf zzckf) {
        this.f11088 = zzckf.f11088;
        this.f11090 = zzckf.f11090;
        this.f11089 = zzckf.f11089;
        this.f9554 = zzckf.f9554;
    }

    public zzckf(String str, String str2, long j) {
        this.f11088 = str;
        this.f11090 = str2;
        this.f11089 = j;
        this.f9554 = false;
    }
}
