package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzafi {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4437(String str, Context context) {
        String r1;
        if (!zzbs.zzfd().m4436(context) || TextUtils.isEmpty(str) || (r1 = zzbs.zzfd().m4423(context)) == null) {
            return str;
        }
        if (((Boolean) zzkb.m5481().m5595(zznh.f4904)).booleanValue()) {
            String str2 = (String) zzkb.m5481().m5595(zznh.f4905);
            if (!str.contains(str2)) {
                return str;
            }
            if (zzbs.zzei().m4629(str)) {
                zzbs.zzfd().m4429(context, r1);
                return str.replace(str2, r1);
            } else if (!zzbs.zzei().m4625(str)) {
                return str;
            } else {
                zzbs.zzfd().m4425(context, r1);
                return str.replace(str2, r1);
            }
        } else if (str.contains("fbs_aeid")) {
            return str;
        } else {
            if (zzbs.zzei().m4629(str)) {
                zzbs.zzfd().m4429(context, r1);
                zzbs.zzei();
                return zzahn.m4592(str, "fbs_aeid", r1).toString();
            } else if (!zzbs.zzei().m4625(str)) {
                return str;
            } else {
                zzbs.zzfd().m4425(context, r1);
                zzbs.zzei();
                return zzahn.m4592(str, "fbs_aeid", r1).toString();
            }
        }
    }
}
