package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzkv extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m13071(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, zzux zzux, int i, int i2) throws RemoteException;
}
