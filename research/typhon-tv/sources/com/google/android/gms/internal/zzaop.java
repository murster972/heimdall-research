package com.google.android.gms.internal;

import android.webkit.ConsoleMessage;

final /* synthetic */ class zzaop {

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ int[] f8382 = new int[ConsoleMessage.MessageLevel.values().length];

    static {
        try {
            f8382[ConsoleMessage.MessageLevel.ERROR.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f8382[ConsoleMessage.MessageLevel.WARNING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f8382[ConsoleMessage.MessageLevel.LOG.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f8382[ConsoleMessage.MessageLevel.TIP.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f8382[ConsoleMessage.MessageLevel.DEBUG.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
