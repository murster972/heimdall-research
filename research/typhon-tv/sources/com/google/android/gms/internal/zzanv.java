package com.google.android.gms.internal;

public final class zzanv extends Exception {
    public zzanv(zzanr zzanr, String str, Throwable th) {
        super(str, th);
    }
}
