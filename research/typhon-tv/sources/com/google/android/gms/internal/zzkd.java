package com.google.android.gms.internal;

import com.google.android.gms.ads.AdListener;

@zzzv
public class zzkd extends AdListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private AdListener f4822;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4823 = new Object();

    public void onAdClosed() {
        synchronized (this.f4823) {
            if (this.f4822 != null) {
                this.f4822.onAdClosed();
            }
        }
    }

    public void onAdFailedToLoad(int i) {
        synchronized (this.f4823) {
            if (this.f4822 != null) {
                this.f4822.onAdFailedToLoad(i);
            }
        }
    }

    public void onAdLeftApplication() {
        synchronized (this.f4823) {
            if (this.f4822 != null) {
                this.f4822.onAdLeftApplication();
            }
        }
    }

    public void onAdLoaded() {
        synchronized (this.f4823) {
            if (this.f4822 != null) {
                this.f4822.onAdLoaded();
            }
        }
    }

    public void onAdOpened() {
        synchronized (this.f4823) {
            if (this.f4822 != null) {
                this.f4822.onAdOpened();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5490(AdListener adListener) {
        synchronized (this.f4823) {
            this.f4822 = adListener;
        }
    }
}
