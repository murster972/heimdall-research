package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;

public final class zzcdv extends zzbfm {
    public static final Parcelable.Creator<zzcdv> CREATOR = new zzcdw();

    /* renamed from: 靐  reason: contains not printable characters */
    private String f9038;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9039;

    public zzcdv(int i, String str) {
        this.f9039 = i;
        this.f9038 = str;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof zzcdv)) {
            return false;
        }
        zzcdv zzcdv = (zzcdv) obj;
        return zzcdv.f9039 == this.f9039 && zzbg.m9113(zzcdv.f9038, this.f9038);
    }

    public final int hashCode() {
        return this.f9039;
    }

    public final String toString() {
        return String.format("%d:%s", new Object[]{Integer.valueOf(this.f9039), this.f9038});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9039);
        zzbfp.m10193(parcel, 2, this.f9038, false);
        zzbfp.m10182(parcel, r0);
    }
}
