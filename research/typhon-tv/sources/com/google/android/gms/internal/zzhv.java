package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import org.apache.commons.lang3.StringUtils;

@zzzv
public final class zzhv extends zzhq {

    /* renamed from: 靐  reason: contains not printable characters */
    private MessageDigest f4719;

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m5401(String str) {
        byte[] bArr;
        byte[] bArr2;
        String[] split = str.split(StringUtils.SPACE);
        if (split.length == 1) {
            int r0 = zzhu.m5399(split[0]);
            ByteBuffer allocate = ByteBuffer.allocate(4);
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putInt(r0);
            bArr = allocate.array();
        } else if (split.length < 5) {
            byte[] bArr3 = new byte[(split.length << 1)];
            for (int i = 0; i < split.length; i++) {
                int r5 = zzhu.m5399(split[i]);
                int i2 = (r5 >> 16) ^ (65535 & r5);
                byte[] bArr4 = {(byte) i2, (byte) (i2 >> 8)};
                bArr3[i << 1] = bArr4[0];
                bArr3[(i << 1) + 1] = bArr4[1];
            }
            bArr = bArr3;
        } else {
            bArr = new byte[split.length];
            for (int i3 = 0; i3 < split.length; i3++) {
                int r2 = zzhu.m5399(split[i3]);
                bArr[i3] = (byte) ((r2 >> 24) ^ (((r2 & 255) ^ ((r2 >> 8) & 255)) ^ ((r2 >> 16) & 255)));
            }
        }
        this.f4719 = m5395();
        synchronized (this.f4714) {
            if (this.f4719 == null) {
                bArr2 = new byte[0];
            } else {
                this.f4719.reset();
                this.f4719.update(bArr);
                byte[] digest = this.f4719.digest();
                bArr2 = new byte[(digest.length > 4 ? 4 : digest.length)];
                System.arraycopy(digest, 0, bArr2, 0, bArr2.length);
            }
        }
        return bArr2;
    }
}
