package com.google.android.gms.internal;

import android.content.Context;

@zzzv
public final class zzafe implements zzgt {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f4076;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f4077;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f4078;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f4079;

    public zzafe(Context context, String str) {
        this.f4079 = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.f4078 = str;
        this.f4077 = false;
        this.f4076 = new Object();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4406(zzgs zzgs) {
        m4408(zzgs.f10667);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4407(String str) {
        this.f4078 = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4408(boolean r5) {
        /*
            r4 = this;
            com.google.android.gms.internal.zzaff r0 = com.google.android.gms.ads.internal.zzbs.zzfd()
            android.content.Context r1 = r4.f4079
            boolean r0 = r0.m4436(r1)
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            java.lang.Object r1 = r4.f4076
            monitor-enter(r1)
            boolean r0 = r4.f4077     // Catch:{ all -> 0x0016 }
            if (r0 != r5) goto L_0x0019
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            goto L_0x000c
        L_0x0016:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            throw r0
        L_0x0019:
            r4.f4077 = r5     // Catch:{ all -> 0x0016 }
            java.lang.String r0 = r4.f4078     // Catch:{ all -> 0x0016 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0016 }
            if (r0 == 0) goto L_0x0025
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            goto L_0x000c
        L_0x0025:
            boolean r0 = r4.f4077     // Catch:{ all -> 0x0016 }
            if (r0 == 0) goto L_0x0036
            com.google.android.gms.internal.zzaff r0 = com.google.android.gms.ads.internal.zzbs.zzfd()     // Catch:{ all -> 0x0016 }
            android.content.Context r2 = r4.f4079     // Catch:{ all -> 0x0016 }
            java.lang.String r3 = r4.f4078     // Catch:{ all -> 0x0016 }
            r0.m4433((android.content.Context) r2, (java.lang.String) r3)     // Catch:{ all -> 0x0016 }
        L_0x0034:
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            goto L_0x000c
        L_0x0036:
            com.google.android.gms.internal.zzaff r0 = com.google.android.gms.ads.internal.zzbs.zzfd()     // Catch:{ all -> 0x0016 }
            android.content.Context r2 = r4.f4079     // Catch:{ all -> 0x0016 }
            java.lang.String r3 = r4.f4078     // Catch:{ all -> 0x0016 }
            r0.m4427(r2, r3)     // Catch:{ all -> 0x0016 }
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzafe.m4408(boolean):void");
    }
}
