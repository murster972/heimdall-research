package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class zzf implements zzt {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzd f10336;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, List<zzr<?>>> f10337 = new HashMap();

    zzf(zzd zzd) {
        this.f10336 = zzd;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized boolean m12356(zzr<?> zzr) {
        boolean z = false;
        synchronized (this) {
            String r2 = zzr.m13359();
            if (this.f10337.containsKey(r2)) {
                List list = this.f10337.get(r2);
                if (list == null) {
                    list = new ArrayList();
                }
                zzr.m13361("waiting-for-response");
                list.add(zzr);
                this.f10337.put(r2, list);
                if (zzae.f8117) {
                    zzae.m9513("Request for cacheKey=%s is in flight, putting on hold.", r2);
                }
                z = true;
            } else {
                this.f10337.put(r2, (Object) null);
                zzr.m13370((zzt) this);
                if (zzae.f8117) {
                    zzae.m9513("new request, sending to network %s", r2);
                }
            }
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m12358(zzr<?> zzr) {
        String r2 = zzr.m13359();
        List remove = this.f10337.remove(r2);
        if (remove != null && !remove.isEmpty()) {
            if (zzae.f8117) {
                zzae.m9516("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(remove.size()), r2);
            }
            zzr zzr2 = (zzr) remove.remove(0);
            this.f10337.put(r2, remove);
            zzr2.m13370((zzt) this);
            try {
                this.f10336.f9853.put(zzr2);
            } catch (InterruptedException e) {
                zzae.m9515("Couldn't add request to queue. %s", e.toString());
                Thread.currentThread().interrupt();
                this.f10336.m11583();
            }
        }
        return;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12359(zzr<?> zzr, zzw<?> zzw) {
        List<zzr> remove;
        if (zzw.f10931 == null || zzw.f10931.m10309()) {
            m12358(zzr);
            return;
        }
        String r1 = zzr.m13359();
        synchronized (this) {
            remove = this.f10337.remove(r1);
        }
        if (remove != null) {
            if (zzae.f8117) {
                zzae.m9516("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), r1);
            }
            for (zzr r0 : remove) {
                this.f10336.f9850.m13647((zzr<?>) r0, zzw);
            }
        }
    }
}
