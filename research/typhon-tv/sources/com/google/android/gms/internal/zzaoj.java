package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.webkit.JsResult;

final class zzaoj implements DialogInterface.OnCancelListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ JsResult f8375;

    zzaoj(JsResult jsResult) {
        this.f8375 = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f8375.cancel();
    }
}
