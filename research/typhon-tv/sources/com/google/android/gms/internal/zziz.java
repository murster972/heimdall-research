package com.google.android.gms.internal;

import android.os.RemoteException;

public final class zziz {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f10739;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzix f10740;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f10741;

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f10742;

    private zziz(zzix zzix, byte[] bArr) {
        this.f10740 = zzix;
        this.f10742 = bArr;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zziz m13015(int i) {
        this.f10741 = i;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zziz m13016(int i) {
        this.f10739 = i;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m13017() {
        try {
            if (this.f10740.f4749) {
                this.f10740.f4750.m12932(this.f10742);
                this.f10740.f4750.m12929(this.f10739);
                this.f10740.f4750.m12927(this.f10741);
                this.f10740.f4750.m12933((int[]) null);
                this.f10740.f4750.m12928();
            }
        } catch (RemoteException e) {
            zzakb.m4797("Clearcut log failed", e);
        }
        return;
    }
}
