package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.common.images.WebImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbdf {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f8720 = {"Z", "+hh", "+hhmm", "+hh:mm"};

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String f8721;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f8722 = new zzbcy("MetadataUtils");

    static {
        String valueOf = String.valueOf("yyyyMMdd'T'HHmmss");
        String valueOf2 = String.valueOf(f8720[0]);
        f8721 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m10134(String str) {
        if (TextUtils.isEmpty(str)) {
            f8722.m10090("Input string is empty or null", new Object[0]);
            return null;
        }
        try {
            return str.substring(0, 8);
        } catch (IndexOutOfBoundsException e) {
            f8722.m10085("Error extracting the date: %s", e.getMessage());
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m10135(String str) {
        boolean z = false;
        if (TextUtils.isEmpty(str)) {
            f8722.m10090("string is empty or null", new Object[0]);
            return null;
        }
        int indexOf = str.indexOf(84);
        int i = indexOf + 1;
        if (indexOf != 8) {
            f8722.m10090("T delimeter is not found", new Object[0]);
            return null;
        }
        try {
            String substring = str.substring(i);
            if (substring.length() == 6) {
                return substring;
            }
            switch (substring.charAt(6)) {
                case '+':
                case '-':
                    int length = substring.length();
                    if (length == f8720[1].length() + 6 || length == f8720[2].length() + 6 || length == f8720[3].length() + 6) {
                        z = true;
                    }
                    if (z) {
                        return substring.replaceAll("([\\+\\-]\\d\\d):(\\d\\d)", "$1$2");
                    }
                    return null;
                case 'Z':
                    if (substring.length() != f8720[0].length() + 6) {
                        return null;
                    }
                    String valueOf = String.valueOf(substring.substring(0, substring.length() - 1));
                    String valueOf2 = String.valueOf("+0000");
                    return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                default:
                    return null;
            }
        } catch (IndexOutOfBoundsException e) {
            f8722.m10090("Error extracting the time substring: %s", e.getMessage());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Calendar m10136(String str) {
        if (TextUtils.isEmpty(str)) {
            f8722.m10090("Input string is empty or null", new Object[0]);
            return null;
        }
        String r1 = m10134(str);
        if (TextUtils.isEmpty(r1)) {
            f8722.m10090("Invalid date format", new Object[0]);
            return null;
        }
        String r3 = m10135(str);
        String str2 = "yyyyMMdd";
        if (!TextUtils.isEmpty(r3)) {
            String valueOf = String.valueOf(r1);
            r1 = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(r3).length()).append(valueOf).append("T").append(r3).toString();
            str2 = r3.length() == 6 ? "yyyyMMdd'T'HHmmss" : f8721;
        }
        Calendar instance = GregorianCalendar.getInstance();
        try {
            instance.setTime(new SimpleDateFormat(str2).parse(r1));
            return instance;
        } catch (ParseException e) {
            f8722.m10090("Error parsing string: %s", e.getMessage());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10137(List<WebImage> list, JSONObject jSONObject) {
        try {
            list.clear();
            JSONArray jSONArray = jSONObject.getJSONArray("images");
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    list.add(new WebImage(jSONArray.getJSONObject(i)));
                } catch (IllegalArgumentException e) {
                }
            }
        } catch (JSONException e2) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10138(JSONObject jSONObject, List<WebImage> list) {
        if (list != null && !list.isEmpty()) {
            JSONArray jSONArray = new JSONArray();
            for (WebImage r0 : list) {
                jSONArray.put((Object) r0.m9035());
            }
            try {
                jSONObject.put("images", (Object) jSONArray);
            } catch (JSONException e) {
            }
        }
    }
}
