package com.google.android.gms.internal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzaig {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final float f4226;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f4227;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f4228;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f4229;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f4230;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public String f4231;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f4232;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public String f4233;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public String f4234;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context f4235;

    public zzaig(Context context) {
        this.f4230 = 0;
        this.f4235 = context;
        this.f4226 = context.getResources().getDisplayMetrics().density;
    }

    public zzaig(Context context, String str) {
        this(context);
        this.f4232 = str;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0074, code lost:
        if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0076;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4679() {
        /*
            r6 = this;
            android.content.Context r0 = r6.f4235
            boolean r0 = r0 instanceof android.app.Activity
            if (r0 != 0) goto L_0x000d
            java.lang.String r0 = "Can not create dialog without Activity Context"
            com.google.android.gms.internal.zzagf.m4794(r0)
        L_0x000c:
            return
        L_0x000d:
            java.lang.String r0 = r6.f4232
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r1 = "\\+"
            java.lang.String r2 = "%20"
            java.lang.String r0 = r0.replaceAll(r1, r2)
            android.net.Uri$Builder r1 = new android.net.Uri$Builder
            r1.<init>()
            android.net.Uri$Builder r0 = r1.encodedQuery(r0)
            android.net.Uri r0 = r0.build()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.google.android.gms.ads.internal.zzbs.zzei()
            java.util.Map r2 = com.google.android.gms.internal.zzahn.m4603((android.net.Uri) r0)
            java.util.Set r0 = r2.keySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x0040:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0068
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r4 = r1.append(r0)
            java.lang.String r5 = " = "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = "\n\n"
            r0.append(r4)
            goto L_0x0040
        L_0x0068:
            java.lang.String r0 = r1.toString()
            java.lang.String r0 = r0.trim()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x00a2
        L_0x0076:
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder
            android.content.Context r2 = r6.f4235
            r1.<init>(r2)
            r1.setMessage(r0)
            java.lang.String r2 = "Ad Information"
            r1.setTitle(r2)
            java.lang.String r2 = "Share"
            com.google.android.gms.internal.zzaii r3 = new com.google.android.gms.internal.zzaii
            r3.<init>(r6, r0)
            r1.setPositiveButton(r2, r3)
            java.lang.String r0 = "Close"
            com.google.android.gms.internal.zzaij r2 = new com.google.android.gms.internal.zzaij
            r2.<init>(r6)
            r1.setNegativeButton(r0, r2)
            android.app.AlertDialog r0 = r1.create()
            goto L_0x000c
        L_0x00a2:
            java.lang.String r0 = "No debug information"
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaig.m4679():void");
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4682() {
        zzagf.m4792("Debug mode [Troubleshooting] selected.");
        zzahh.m4555((Runnable) new zzail(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4683() {
        zzagf.m4792("Debug mode [Creative Preview] selected.");
        zzahh.m4555((Runnable) new zzaik(this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m4685(List<String> list, String str, boolean z) {
        if (!z) {
            return -1;
        }
        list.add(str);
        return list.size() - 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4686(int i, float f, float f2) {
        if (i == 0) {
            this.f4230 = 0;
            this.f4227 = f;
            this.f4228 = f2;
            this.f4229 = f2;
        } else if (this.f4230 == -1) {
        } else {
            if (i == 2) {
                if (f2 > this.f4228) {
                    this.f4228 = f2;
                } else if (f2 < this.f4229) {
                    this.f4229 = f2;
                }
                if (this.f4228 - this.f4229 > 30.0f * this.f4226) {
                    this.f4230 = -1;
                    return;
                }
                if (this.f4230 == 0 || this.f4230 == 2) {
                    if (f - this.f4227 >= 50.0f * this.f4226) {
                        this.f4227 = f;
                        this.f4230++;
                    }
                } else if ((this.f4230 == 1 || this.f4230 == 3) && f - this.f4227 <= -50.0f * this.f4226) {
                    this.f4227 = f;
                    this.f4230++;
                }
                if (this.f4230 == 1 || this.f4230 == 3) {
                    if (f > this.f4227) {
                        this.f4227 = f;
                    }
                } else if (this.f4230 == 2 && f < this.f4227) {
                    this.f4227 = f;
                }
            } else if (i == 1 && this.f4230 == 4) {
                m4691();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4688(String str) {
        this.f4233 = str;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4689(String str) {
        this.f4231 = str;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4690(String str) {
        this.f4232 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4691() {
        if (!((Boolean) zzkb.m5481().m5595(zznh.f5030)).booleanValue()) {
            if (!((Boolean) zzkb.m5481().m5595(zznh.f5029)).booleanValue()) {
                m4679();
                return;
            }
        }
        if (!(this.f4235 instanceof Activity)) {
            zzagf.m4794("Can not create dialog without Activity Context");
            return;
        }
        String str = !TextUtils.isEmpty(zzbs.zzer().m4703()) ? "Creative Preview (Enabled)" : "Creative Preview";
        String str2 = zzbs.zzer().m4702() ? "Troubleshooting (Enabled)" : "Troubleshooting";
        ArrayList arrayList = new ArrayList();
        int r4 = m4685((List<String>) arrayList, "Ad Information", true);
        int r1 = m4685((List<String>) arrayList, str, ((Boolean) zzkb.m5481().m5595(zznh.f5029)).booleanValue());
        int r2 = m4685((List<String>) arrayList, str2, ((Boolean) zzkb.m5481().m5595(zznh.f5030)).booleanValue());
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f4235, zzbs.zzek().m4643());
        builder.setTitle("Select a Debug Mode").setItems((CharSequence[]) arrayList.toArray(new String[0]), new zzaih(this, r4, r1, r2));
        AlertDialog create = builder.create();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4692(MotionEvent motionEvent) {
        int historySize = motionEvent.getHistorySize();
        for (int i = 0; i < historySize; i++) {
            m4686(motionEvent.getActionMasked(), motionEvent.getHistoricalX(0, i), motionEvent.getHistoricalY(0, i));
        }
        m4686(motionEvent.getActionMasked(), motionEvent.getX(), motionEvent.getY());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4693(String str) {
        this.f4234 = str;
    }
}
