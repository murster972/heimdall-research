package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import java.util.Map;

@zzzv
@TargetApi(11)
public final class zzaoq extends zzaos {
    public zzaoq(zzanh zzanh, boolean z) {
        super(zzanh, z);
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        return m5217(webView, str, (Map<String, String>) null);
    }
}
