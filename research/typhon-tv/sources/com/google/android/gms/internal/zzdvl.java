package com.google.android.gms.internal;

import java.io.PrintWriter;

public final class zzdvl {

    /* renamed from: 靐  reason: contains not printable characters */
    private static int f10234;

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzdvm f10235;

    static final class zza extends zzdvm {
        zza() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12241(Throwable th, PrintWriter printWriter) {
            th.printStackTrace(printWriter);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0019  */
    static {
        /*
            r2 = 1
            r0 = 0
            java.lang.Integer r0 = m12239()     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x001d
            int r1 = r0.intValue()     // Catch:{ Throwable -> 0x002f }
            r3 = 19
            if (r1 < r3) goto L_0x001d
            com.google.android.gms.internal.zzdvq r1 = new com.google.android.gms.internal.zzdvq     // Catch:{ Throwable -> 0x002f }
            r1.<init>()     // Catch:{ Throwable -> 0x002f }
        L_0x0015:
            f10235 = r1
            if (r0 != 0) goto L_0x0073
            r0 = r2
        L_0x001a:
            f10234 = r0
            return
        L_0x001d:
            java.lang.String r1 = "com.google.devtools.build.android.desugar.runtime.twr_disable_mimic"
            boolean r1 = java.lang.Boolean.getBoolean(r1)     // Catch:{ Throwable -> 0x002f }
            if (r1 != 0) goto L_0x006b
            r1 = r2
        L_0x0027:
            if (r1 == 0) goto L_0x006d
            com.google.android.gms.internal.zzdvp r1 = new com.google.android.gms.internal.zzdvp     // Catch:{ Throwable -> 0x002f }
            r1.<init>()     // Catch:{ Throwable -> 0x002f }
            goto L_0x0015
        L_0x002f:
            r1 = move-exception
            java.io.PrintStream r3 = java.lang.System.err
            java.lang.Class<com.google.android.gms.internal.zzdvl$zza> r4 = com.google.android.gms.internal.zzdvl.zza.class
            java.lang.String r4 = r4.getName()
            java.lang.String r5 = java.lang.String.valueOf(r4)
            int r5 = r5.length()
            int r5 = r5 + 132
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            java.lang.String r5 = "An error has occured when initializing the try-with-resources desuguring strategy. The default strategy "
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.String r5 = "will be used. The error is: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.println(r4)
            java.io.PrintStream r3 = java.lang.System.err
            r1.printStackTrace(r3)
            com.google.android.gms.internal.zzdvl$zza r1 = new com.google.android.gms.internal.zzdvl$zza
            r1.<init>()
            goto L_0x0015
        L_0x006b:
            r1 = 0
            goto L_0x0027
        L_0x006d:
            com.google.android.gms.internal.zzdvl$zza r1 = new com.google.android.gms.internal.zzdvl$zza     // Catch:{ Throwable -> 0x002f }
            r1.<init>()     // Catch:{ Throwable -> 0x002f }
            goto L_0x0015
        L_0x0073:
            int r0 = r0.intValue()
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdvl.<clinit>():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Integer m12239() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get((Object) null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12240(Throwable th, PrintWriter printWriter) {
        f10235.m12242(th, printWriter);
    }
}
