package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzdy extends zzet {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f10248 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile String f10249 = null;

    public zzdy(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 29);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12270() throws IllegalAccessException, InvocationTargetException {
        this.f10284.f8472 = "E";
        if (f10249 == null) {
            synchronized (f10248) {
                if (f10249 == null) {
                    f10249 = (String) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()});
                }
            }
        }
        synchronized (this.f10284) {
            this.f10284.f8472 = zzbu.m10295(f10249.getBytes(), true);
        }
    }
}
