package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdtf extends zzffu<zzdtf, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final zzdtf f10111;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static volatile zzfhk<zzdtf> f10112;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10113;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f10114;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f10115 = "";

    /* renamed from: 连任  reason: contains not printable characters */
    private String f10116 = "";

    /* renamed from: 麤  reason: contains not printable characters */
    private String f10117 = "";

    public static final class zza extends zzffu.zza<zzdtf, zza> implements zzfhg {
        private zza() {
            super(zzdtf.f10111);
        }

        /* synthetic */ zza(zzdtg zzdtg) {
            this();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final zza m12060(String str) {
            m12541();
            ((zzdtf) this.f10398).m12043(str);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final zza m12061(String str) {
            m12541();
            ((zzdtf) this.f10398).m12045(str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12062(int i) {
            m12541();
            ((zzdtf) this.f10398).m12046(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12063(String str) {
            m12541();
            ((zzdtf) this.f10398).m12050(str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12064(boolean z) {
            m12541();
            ((zzdtf) this.f10398).m12051(true);
            return this;
        }
    }

    static {
        zzdtf zzdtf = new zzdtf();
        f10111 = zzdtf;
        zzdtf.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdtf.f10394.m12718();
    }

    private zzdtf() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static zza m12039() {
        zzdtf zzdtf = f10111;
        zzffu.zza zza2 = (zzffu.zza) zzdtf.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdtf);
        return (zza) zza2;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static zzdtf m12040() {
        return f10111;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12043(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f10116 = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12045(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f10115 = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12046(int i) {
        this.f10113 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12050(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f10117 = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12051(boolean z) {
        this.f10114 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m12052() {
        return this.f10115;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m12053() {
        return this.f10114;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m12054() {
        return this.f10116;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12055() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.f10117.isEmpty()) {
            i2 = zzffg.m5248(1, this.f10117) + 0;
        }
        if (!this.f10116.isEmpty()) {
            i2 += zzffg.m5248(2, this.f10116);
        }
        if (this.f10113 != 0) {
            i2 += zzffg.m5245(3, this.f10113);
        }
        if (this.f10114) {
            i2 += zzffg.m5249(4, this.f10114);
        }
        if (!this.f10115.isEmpty()) {
            i2 += zzffg.m5248(5, this.f10115);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12056() {
        return this.f10113;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 201 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m12057(int r7, java.lang.Object r8, java.lang.Object r9) {
        /*
            r6 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdtg.f10118
            int r4 = r7 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x00b8;
                case 7: goto L_0x011a;
                case 8: goto L_0x011e;
                case 9: goto L_0x013a;
                case 10: goto L_0x0140;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdtf r6 = new com.google.android.gms.internal.zzdtf
            r6.<init>()
        L_0x0017:
            return r6
        L_0x0018:
            com.google.android.gms.internal.zzdtf r6 = f10111
            goto L_0x0017
        L_0x001b:
            r6 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdtf$zza r6 = new com.google.android.gms.internal.zzdtf$zza
            r6.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r8 = (com.google.android.gms.internal.zzffu.zzh) r8
            com.google.android.gms.internal.zzdtf r9 = (com.google.android.gms.internal.zzdtf) r9
            java.lang.String r0 = r6.f10117
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00a4
            r0 = r1
        L_0x0030:
            java.lang.String r4 = r6.f10117
            java.lang.String r3 = r9.f10117
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x00a6
            r3 = r1
        L_0x003b:
            java.lang.String r5 = r9.f10117
            java.lang.String r0 = r8.m12574((boolean) r0, (java.lang.String) r4, (boolean) r3, (java.lang.String) r5)
            r6.f10117 = r0
            java.lang.String r0 = r6.f10116
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00a8
            r0 = r1
        L_0x004c:
            java.lang.String r4 = r6.f10116
            java.lang.String r3 = r9.f10116
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x00aa
            r3 = r1
        L_0x0057:
            java.lang.String r5 = r9.f10116
            java.lang.String r0 = r8.m12574((boolean) r0, (java.lang.String) r4, (boolean) r3, (java.lang.String) r5)
            r6.f10116 = r0
            int r0 = r6.f10113
            if (r0 == 0) goto L_0x00ac
            r0 = r1
        L_0x0064:
            int r4 = r6.f10113
            int r3 = r9.f10113
            if (r3 == 0) goto L_0x00ae
            r3 = r1
        L_0x006b:
            int r5 = r9.f10113
            int r0 = r8.m12569((boolean) r0, (int) r4, (boolean) r3, (int) r5)
            r6.f10113 = r0
            boolean r0 = r6.f10114
            if (r0 == 0) goto L_0x00b0
            r0 = r1
        L_0x0078:
            boolean r4 = r6.f10114
            boolean r3 = r9.f10114
            if (r3 == 0) goto L_0x00b2
            r3 = r1
        L_0x007f:
            boolean r5 = r9.f10114
            boolean r0 = r8.m12575((boolean) r0, (boolean) r4, (boolean) r3, (boolean) r5)
            r6.f10114 = r0
            java.lang.String r0 = r6.f10115
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00b4
            r0 = r1
        L_0x0090:
            java.lang.String r3 = r6.f10115
            java.lang.String r4 = r9.f10115
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x00b6
        L_0x009a:
            java.lang.String r2 = r9.f10115
            java.lang.String r0 = r8.m12574((boolean) r0, (java.lang.String) r3, (boolean) r1, (java.lang.String) r2)
            r6.f10115 = r0
            goto L_0x0017
        L_0x00a4:
            r0 = r2
            goto L_0x0030
        L_0x00a6:
            r3 = r2
            goto L_0x003b
        L_0x00a8:
            r0 = r2
            goto L_0x004c
        L_0x00aa:
            r3 = r2
            goto L_0x0057
        L_0x00ac:
            r0 = r2
            goto L_0x0064
        L_0x00ae:
            r3 = r2
            goto L_0x006b
        L_0x00b0:
            r0 = r2
            goto L_0x0078
        L_0x00b2:
            r3 = r2
            goto L_0x007f
        L_0x00b4:
            r0 = r2
            goto L_0x0090
        L_0x00b6:
            r1 = r2
            goto L_0x009a
        L_0x00b8:
            com.google.android.gms.internal.zzffb r8 = (com.google.android.gms.internal.zzffb) r8
            com.google.android.gms.internal.zzffm r9 = (com.google.android.gms.internal.zzffm) r9
            if (r9 != 0) goto L_0x00c5
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x00c4:
            r2 = r1
        L_0x00c5:
            if (r2 != 0) goto L_0x011a
            int r0 = r8.m12415()     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            switch(r0) {
                case 0: goto L_0x00c4;
                case 10: goto L_0x00d6;
                case 18: goto L_0x00ea;
                case 24: goto L_0x0105;
                case 32: goto L_0x010c;
                case 42: goto L_0x0113;
                default: goto L_0x00ce;
            }     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
        L_0x00ce:
            boolean r0 = r6.m12537((int) r0, (com.google.android.gms.internal.zzffb) r8)     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            if (r0 != 0) goto L_0x00c5
            r2 = r1
            goto L_0x00c5
        L_0x00d6:
            java.lang.String r0 = r8.m12400()     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            r6.f10117 = r0     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            goto L_0x00c5
        L_0x00dd:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00e8 }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r6)     // Catch:{ all -> 0x00e8 }
            r1.<init>(r0)     // Catch:{ all -> 0x00e8 }
            throw r1     // Catch:{ all -> 0x00e8 }
        L_0x00e8:
            r0 = move-exception
            throw r0
        L_0x00ea:
            java.lang.String r0 = r8.m12400()     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            r6.f10116 = r0     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            goto L_0x00c5
        L_0x00f1:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00e8 }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x00e8 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00e8 }
            r2.<init>(r0)     // Catch:{ all -> 0x00e8 }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r6)     // Catch:{ all -> 0x00e8 }
            r1.<init>(r0)     // Catch:{ all -> 0x00e8 }
            throw r1     // Catch:{ all -> 0x00e8 }
        L_0x0105:
            int r0 = r8.m12402()     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            r6.f10113 = r0     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            goto L_0x00c5
        L_0x010c:
            boolean r0 = r8.m12408()     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            r6.f10114 = r0     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            goto L_0x00c5
        L_0x0113:
            java.lang.String r0 = r8.m12400()     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            r6.f10115 = r0     // Catch:{ zzfge -> 0x00dd, IOException -> 0x00f1 }
            goto L_0x00c5
        L_0x011a:
            com.google.android.gms.internal.zzdtf r6 = f10111
            goto L_0x0017
        L_0x011e:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtf> r0 = f10112
            if (r0 != 0) goto L_0x0133
            java.lang.Class<com.google.android.gms.internal.zzdtf> r1 = com.google.android.gms.internal.zzdtf.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtf> r0 = f10112     // Catch:{ all -> 0x0137 }
            if (r0 != 0) goto L_0x0132
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x0137 }
            com.google.android.gms.internal.zzdtf r2 = f10111     // Catch:{ all -> 0x0137 }
            r0.<init>(r2)     // Catch:{ all -> 0x0137 }
            f10112 = r0     // Catch:{ all -> 0x0137 }
        L_0x0132:
            monitor-exit(r1)     // Catch:{ all -> 0x0137 }
        L_0x0133:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtf> r6 = f10112
            goto L_0x0017
        L_0x0137:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0137 }
            throw r0
        L_0x013a:
            java.lang.Byte r6 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x0140:
            r6 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdtf.m12057(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m12058() {
        return this.f10117;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12059(zzffg zzffg) throws IOException {
        if (!this.f10117.isEmpty()) {
            zzffg.m5290(1, this.f10117);
        }
        if (!this.f10116.isEmpty()) {
            zzffg.m5290(2, this.f10116);
        }
        if (this.f10113 != 0) {
            zzffg.m5281(3, this.f10113);
        }
        if (this.f10114) {
            zzffg.m5291(4, this.f10114);
        }
        if (!this.f10115.isEmpty()) {
            zzffg.m5290(5, this.f10115);
        }
        this.f10394.m12719(zzffg);
    }
}
