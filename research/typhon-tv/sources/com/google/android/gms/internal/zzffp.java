package com.google.android.gms.internal;

final class zzffp {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final zzffn<?> f10383 = m12506();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzffn<?> f10384 = new zzffo();

    /* renamed from: 靐  reason: contains not printable characters */
    static zzffn<?> m12505() {
        if (f10383 != null) {
            return f10383;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzffn<?> m12506() {
        try {
            return (zzffn) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzffn<?> m12507() {
        return f10384;
    }
}
