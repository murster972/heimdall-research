package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzek extends zzet {
    public zzek(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 61);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12285() throws IllegalAccessException, InvocationTargetException {
        long longValue = ((Long) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623(), Boolean.valueOf(this.f10287.m11616())})).longValue();
        synchronized (this.f10284) {
            this.f10284.f8436 = Long.valueOf(longValue);
        }
    }
}
