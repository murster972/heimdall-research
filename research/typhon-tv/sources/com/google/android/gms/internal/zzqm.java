package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzqm extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m13318() throws RemoteException;

    /* renamed from: ˈ  reason: contains not printable characters */
    String m13319() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    IObjectWrapper m13320() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    IObjectWrapper m13321() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    zzpq m13322(String str) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m13323() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    zzll m13324() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13325(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13326(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    List<String> m13327() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m13328(IObjectWrapper iObjectWrapper) throws RemoteException;
}
