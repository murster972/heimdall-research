package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsm extends zzffu<zzdsm, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdsm f10052;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsm> f10053;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f10054 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10055;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10056;

    public static final class zza extends zzffu.zza<zzdsm, zza> implements zzfhg {
        private zza() {
            super(zzdsm.f10052);
        }

        /* synthetic */ zza(zzdsn zzdsn) {
            this();
        }
    }

    static {
        zzdsm zzdsm = new zzdsm();
        f10052 = zzdsm;
        zzdsm.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsm.f10394.m12718();
    }

    private zzdsm() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zzdsm m11965() {
        return f10052;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdsq m11966() {
        zzdsq zzfr = zzdsq.zzfr(this.f10055);
        return zzfr == null ? zzdsq.UNRECOGNIZED : zzfr;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11967() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10056 != zzdso.UNKNOWN_CURVE.zzhq()) {
            i2 = zzffg.m5236(1, this.f10056) + 0;
        }
        if (this.f10055 != zzdsq.UNKNOWN_HASH.zzhq()) {
            i2 += zzffg.m5236(2, this.f10055);
        }
        if (!this.f10054.isEmpty()) {
            i2 += zzffg.m5261(11, this.f10054);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m11968() {
        return this.f10054;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdso m11969() {
        zzdso zzfq = zzdso.zzfq(this.f10056);
        return zzfq == null ? zzdso.UNRECOGNIZED : zzfq;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 181 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m11970(int r7, java.lang.Object r8, java.lang.Object r9) {
        /*
            r6 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdsn.f10057
            int r4 = r7 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x0073;
                case 7: goto L_0x00c7;
                case 8: goto L_0x00cb;
                case 9: goto L_0x00e7;
                case 10: goto L_0x00ed;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdsm r6 = new com.google.android.gms.internal.zzdsm
            r6.<init>()
        L_0x0017:
            return r6
        L_0x0018:
            com.google.android.gms.internal.zzdsm r6 = f10052
            goto L_0x0017
        L_0x001b:
            r6 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdsm$zza r6 = new com.google.android.gms.internal.zzdsm$zza
            r6.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r8 = (com.google.android.gms.internal.zzffu.zzh) r8
            com.google.android.gms.internal.zzdsm r9 = (com.google.android.gms.internal.zzdsm) r9
            int r0 = r6.f10056
            if (r0 == 0) goto L_0x0067
            r0 = r1
        L_0x002c:
            int r4 = r6.f10056
            int r3 = r9.f10056
            if (r3 == 0) goto L_0x0069
            r3 = r1
        L_0x0033:
            int r5 = r9.f10056
            int r0 = r8.m12569((boolean) r0, (int) r4, (boolean) r3, (int) r5)
            r6.f10056 = r0
            int r0 = r6.f10055
            if (r0 == 0) goto L_0x006b
            r0 = r1
        L_0x0040:
            int r4 = r6.f10055
            int r3 = r9.f10055
            if (r3 == 0) goto L_0x006d
            r3 = r1
        L_0x0047:
            int r5 = r9.f10055
            int r0 = r8.m12569((boolean) r0, (int) r4, (boolean) r3, (int) r5)
            r6.f10055 = r0
            com.google.android.gms.internal.zzfes r0 = r6.f10054
            com.google.android.gms.internal.zzfes r3 = com.google.android.gms.internal.zzfes.zzpfg
            if (r0 == r3) goto L_0x006f
            r0 = r1
        L_0x0056:
            com.google.android.gms.internal.zzfes r3 = r6.f10054
            com.google.android.gms.internal.zzfes r4 = r9.f10054
            com.google.android.gms.internal.zzfes r5 = com.google.android.gms.internal.zzfes.zzpfg
            if (r4 == r5) goto L_0x0071
        L_0x005e:
            com.google.android.gms.internal.zzfes r2 = r9.f10054
            com.google.android.gms.internal.zzfes r0 = r8.m12570((boolean) r0, (com.google.android.gms.internal.zzfes) r3, (boolean) r1, (com.google.android.gms.internal.zzfes) r2)
            r6.f10054 = r0
            goto L_0x0017
        L_0x0067:
            r0 = r2
            goto L_0x002c
        L_0x0069:
            r3 = r2
            goto L_0x0033
        L_0x006b:
            r0 = r2
            goto L_0x0040
        L_0x006d:
            r3 = r2
            goto L_0x0047
        L_0x006f:
            r0 = r2
            goto L_0x0056
        L_0x0071:
            r1 = r2
            goto L_0x005e
        L_0x0073:
            com.google.android.gms.internal.zzffb r8 = (com.google.android.gms.internal.zzffb) r8
            com.google.android.gms.internal.zzffm r9 = (com.google.android.gms.internal.zzffm) r9
            if (r9 != 0) goto L_0x0080
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x007f:
            r2 = r1
        L_0x0080:
            if (r2 != 0) goto L_0x00c7
            int r0 = r8.m12415()     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            switch(r0) {
                case 0: goto L_0x007f;
                case 8: goto L_0x0091;
                case 16: goto L_0x00a5;
                case 90: goto L_0x00c0;
                default: goto L_0x0089;
            }     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
        L_0x0089:
            boolean r0 = r6.m12537((int) r0, (com.google.android.gms.internal.zzffb) r8)     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            if (r0 != 0) goto L_0x0080
            r2 = r1
            goto L_0x0080
        L_0x0091:
            int r0 = r8.m12405()     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            r6.f10056 = r0     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            goto L_0x0080
        L_0x0098:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00a3 }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r6)     // Catch:{ all -> 0x00a3 }
            r1.<init>(r0)     // Catch:{ all -> 0x00a3 }
            throw r1     // Catch:{ all -> 0x00a3 }
        L_0x00a3:
            r0 = move-exception
            throw r0
        L_0x00a5:
            int r0 = r8.m12405()     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            r6.f10055 = r0     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            goto L_0x0080
        L_0x00ac:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00a3 }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x00a3 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a3 }
            r2.<init>(r0)     // Catch:{ all -> 0x00a3 }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r6)     // Catch:{ all -> 0x00a3 }
            r1.<init>(r0)     // Catch:{ all -> 0x00a3 }
            throw r1     // Catch:{ all -> 0x00a3 }
        L_0x00c0:
            com.google.android.gms.internal.zzfes r0 = r8.m12401()     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            r6.f10054 = r0     // Catch:{ zzfge -> 0x0098, IOException -> 0x00ac }
            goto L_0x0080
        L_0x00c7:
            com.google.android.gms.internal.zzdsm r6 = f10052
            goto L_0x0017
        L_0x00cb:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsm> r0 = f10053
            if (r0 != 0) goto L_0x00e0
            java.lang.Class<com.google.android.gms.internal.zzdsm> r1 = com.google.android.gms.internal.zzdsm.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsm> r0 = f10053     // Catch:{ all -> 0x00e4 }
            if (r0 != 0) goto L_0x00df
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00e4 }
            com.google.android.gms.internal.zzdsm r2 = f10052     // Catch:{ all -> 0x00e4 }
            r0.<init>(r2)     // Catch:{ all -> 0x00e4 }
            f10053 = r0     // Catch:{ all -> 0x00e4 }
        L_0x00df:
            monitor-exit(r1)     // Catch:{ all -> 0x00e4 }
        L_0x00e0:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsm> r6 = f10053
            goto L_0x0017
        L_0x00e4:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00e4 }
            throw r0
        L_0x00e7:
            java.lang.Byte r6 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00ed:
            r6 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdsm.m11970(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11971(zzffg zzffg) throws IOException {
        if (this.f10056 != zzdso.UNKNOWN_CURVE.zzhq()) {
            zzffg.m5274(1, this.f10056);
        }
        if (this.f10055 != zzdsq.UNKNOWN_HASH.zzhq()) {
            zzffg.m5274(2, this.f10055);
        }
        if (!this.f10054.isEmpty()) {
            zzffg.m5288(11, this.f10054);
        }
        this.f10394.m12719(zzffg);
    }
}
