package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzel extends zzet {

    /* renamed from: 麤  reason: contains not printable characters */
    private final StackTraceElement[] f10266;

    public zzel(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2, StackTraceElement[] stackTraceElementArr) {
        super(zzdm, str, str2, zzaz, i, 45);
        this.f10266 = stackTraceElementArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12286() throws IllegalAccessException, InvocationTargetException {
        if (this.f10266 != null) {
            zzdk zzdk = new zzdk((String) this.f10286.invoke((Object) null, new Object[]{this.f10266}));
            synchronized (this.f10284) {
                this.f10284.f8462 = zzdk.f9868;
                if (zzdk.f9866.booleanValue()) {
                    this.f10284.f8426 = Integer.valueOf(zzdk.f9867.booleanValue() ? 0 : 1);
                }
            }
        }
    }
}
