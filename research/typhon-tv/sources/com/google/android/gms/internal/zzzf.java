package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.gmsg.zzx;
import com.google.android.gms.ads.internal.gmsg.zzz;
import com.google.android.gms.ads.internal.js.JavascriptEngineFactory;
import com.google.android.gms.ads.internal.js.zzn;
import com.google.android.gms.ads.internal.zzba;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzw;
import com.mopub.common.AdType;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

@zzzv
public final class zzzf extends zzyk<zzzf> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static zzn f5616 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean f5617 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Object f5618 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final long f5619 = TimeUnit.SECONDS.toMillis(60);

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzz f5620;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzox f5621;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Object f5622 = new Object();

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f5623;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzcv f5624;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Context f5625;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzakd f5626;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final zzba f5627;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzakv<zzanh> f5628;

    /* renamed from: 龘  reason: contains not printable characters */
    private JavascriptEngineFactory f5629;

    public zzzf(Context context, zzba zzba, String str, zzcv zzcv, zzakd zzakd) {
        zzagf.m4794("Webview loading for native ads.");
        this.f5625 = context;
        this.f5627 = zzba;
        this.f5624 = zzcv;
        this.f5626 = zzakd;
        this.f5623 = str;
        this.f5629 = new JavascriptEngineFactory();
        zzbs.zzej();
        zzakv<zzanh> r0 = zzanr.m5059(this.f5625, this.f5626, (String) zzkb.m5481().m5595(zznh.f4985), this.f5624, this.f5627.zzbq());
        this.f5620 = new zzz(this.f5625);
        this.f5621 = new zzox(zzba, str);
        this.f5628 = zzakl.m4804(r0, new zzzg(this), zzala.f4304);
        zzakj.m4801(this.f5628, "WebViewNativeAdsUtil.constructor");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzakv<JSONObject> m6082(JSONObject jSONObject) {
        return zzakl.m4804(this.f5628, new zzzi(this, jSONObject), zzala.f4307);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final /* synthetic */ zzakv m6083(JSONObject jSONObject, zzanh zzanh) throws Exception {
        jSONObject.put("ads_id", (Object) this.f5623);
        zzanh.zzb("google.afma.nativeAds.handleClickGmsg", jSONObject);
        return zzakl.m4802(new JSONObject());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m6084(String str, zzt zzt) {
        zzakl.m4809(this.f5628, new zzzm(this, str, zzt), zzala.f4307);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzakv<JSONObject> m6085(JSONObject jSONObject) {
        return zzakl.m4804(this.f5628, new zzzj(this, jSONObject), zzala.f4307);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final /* synthetic */ zzakv m6086(JSONObject jSONObject, zzanh zzanh) throws Exception {
        jSONObject.put("ads_id", (Object) this.f5623);
        zzalf zzalf = new zzalf();
        zzyl zzyl = new zzyl();
        zzzk zzzk = new zzzk(this, zzanh, zzyl, zzalf);
        zzyl.f10973 = zzzk;
        zzanh.m5014("/nativeAdPreProcess", zzzk);
        zzanh.zzb("google.afma.nativeAds.preProcessJsonGmsg", jSONObject);
        return zzalf;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzakv m6087(zzanh zzanh) throws Exception {
        zzagf.m4794("Javascript has loaded for native ads.");
        zzanh.m4980().m5052(this.f5627, this.f5627, this.f5627, this.f5627, false, (zzx) null, new zzw(this.f5625, (zzafb) null, (zzaaz) null), (zzxc) null, (zzafb) null);
        zzanh.m4980().m5053("/logScionEvent", (zzt<? super zzanh>) this.f5620);
        zzanh.m4980().m5053("/logScionEvent", (zzt<? super zzanh>) this.f5621);
        return zzakl.m4802(zzanh);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<JSONObject> m6088(JSONObject jSONObject) {
        return zzakl.m4804(this.f5628, new zzzh(this, jSONObject), zzala.f4307);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzakv m6089(JSONObject jSONObject, zzanh zzanh) throws Exception {
        jSONObject.put("ads_id", (Object) this.f5623);
        zzanh.zzb("google.afma.nativeAds.handleImpressionPing", jSONObject);
        return zzakl.m4802(new JSONObject());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6090() {
        zzakl.m4809(this.f5628, new zzzo(this), zzala.f4307);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m6091(zzanh zzanh, zzyl zzyl, zzalf zzalf, zzanh zzanh2, Map map) {
        JSONObject jSONObject;
        boolean z;
        try {
            String str = (String) map.get("success");
            String str2 = (String) map.get("failure");
            if (!TextUtils.isEmpty(str2)) {
                z = false;
                jSONObject = new JSONObject(str2);
            } else {
                jSONObject = new JSONObject(str);
                z = true;
            }
            if (this.f5623.equals(jSONObject.optString("ads_id", ""))) {
                zzanh.m5001("/nativeAdPreProcess", zzyl.f10973);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("success", z);
                jSONObject2.put(AdType.STATIC_NATIVE, (Object) jSONObject);
                zzalf.m4822(jSONObject2);
            }
        } catch (Throwable th) {
            zzagf.m4793("Error while preprocessing json.", th);
            zzalf.m4824(th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6092(String str, zzt zzt) {
        zzakl.m4809(this.f5628, new zzzl(this, str, zzt), zzala.f4307);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6093(String str, JSONObject jSONObject) {
        zzakl.m4809(this.f5628, new zzzn(this, str, jSONObject), zzala.f4307);
    }
}
