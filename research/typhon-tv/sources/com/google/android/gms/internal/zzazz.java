package com.google.android.gms.internal;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

final class zzazz implements zzazo {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzazy f8526;

    zzazz(zzazy zzazy) {
        this.f8526 = zzazy;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9864(Bitmap bitmap) {
        Bitmap createBitmap;
        if (bitmap == null) {
            createBitmap = null;
        } else {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int i = (int) (((((float) width) * 9.0f) / 16.0f) + 0.5f);
            float f = (float) ((i - height) / 2);
            RectF rectF = new RectF(0.0f, f, (float) width, ((float) height) + f);
            Bitmap.Config config = bitmap.getConfig();
            if (config == null) {
                config = Bitmap.Config.ARGB_8888;
            }
            createBitmap = Bitmap.createBitmap(width, i, config);
            new Canvas(createBitmap).drawBitmap(bitmap, (Rect) null, rectF, (Paint) null);
        }
        this.f8526.m9853(createBitmap, 0);
    }
}
