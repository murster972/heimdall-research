package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzlc extends zzeu implements zzla {
    zzlc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IClientApi");
    }

    public final zzkn createAdLoaderBuilder(IObjectWrapper iObjectWrapper, String str, zzux zzux, int i) throws RemoteException {
        zzkn zzkp;
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) zzux);
        v_.writeInt(i);
        Parcel r1 = m12300(3, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzkp = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            zzkp = queryLocalInterface instanceof zzkn ? (zzkn) queryLocalInterface : new zzkp(readStrongBinder);
        }
        r1.recycle();
        return zzkp;
    }

    public final zzxe createAdOverlay(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        Parcel r0 = m12300(8, v_);
        zzxe zzv = zzxf.zzv(r0.readStrongBinder());
        r0.recycle();
        return zzv;
    }

    public final zzks createBannerAdManager(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, zzux zzux, int i) throws RemoteException {
        zzks zzku;
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12306(v_, (Parcelable) zzjn);
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) zzux);
        v_.writeInt(i);
        Parcel r1 = m12300(1, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzku = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            zzku = queryLocalInterface instanceof zzks ? (zzks) queryLocalInterface : new zzku(readStrongBinder);
        }
        r1.recycle();
        return zzku;
    }

    public final zzxo createInAppPurchaseManager(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        Parcel r0 = m12300(7, v_);
        zzxo r1 = zzxp.m13636(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    public final zzks createInterstitialAdManager(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, zzux zzux, int i) throws RemoteException {
        zzks zzku;
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12306(v_, (Parcelable) zzjn);
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) zzux);
        v_.writeInt(i);
        Parcel r1 = m12300(2, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzku = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            zzku = queryLocalInterface instanceof zzks ? (zzks) queryLocalInterface : new zzku(readStrongBinder);
        }
        r1.recycle();
        return zzku;
    }

    public final zzpu createNativeAdViewDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) iObjectWrapper2);
        Parcel r0 = m12300(5, v_);
        zzpu r1 = zzpv.m13235(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    public final zzpz createNativeAdViewHolderDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) iObjectWrapper2);
        zzew.m12305(v_, (IInterface) iObjectWrapper3);
        Parcel r0 = m12300(11, v_);
        zzpz r1 = zzqa.m13245(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    public final zzadk createRewardedVideoAd(IObjectWrapper iObjectWrapper, zzux zzux, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) zzux);
        v_.writeInt(i);
        Parcel r0 = m12300(6, v_);
        zzadk r1 = zzadl.m9482(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    public final zzks createSearchAdManager(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, int i) throws RemoteException {
        zzks zzku;
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12306(v_, (Parcelable) zzjn);
        v_.writeString(str);
        v_.writeInt(i);
        Parcel r1 = m12300(10, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzku = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            zzku = queryLocalInterface instanceof zzks ? (zzks) queryLocalInterface : new zzku(readStrongBinder);
        }
        r1.recycle();
        return zzku;
    }

    public final zzlg getMobileAdsSettingsManager(IObjectWrapper iObjectWrapper) throws RemoteException {
        zzlg zzli;
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        Parcel r1 = m12300(4, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzli = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            zzli = queryLocalInterface instanceof zzlg ? (zzlg) queryLocalInterface : new zzli(readStrongBinder);
        }
        r1.recycle();
        return zzli;
    }

    public final zzlg getMobileAdsSettingsManagerWithClientJarVersion(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        zzlg zzli;
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeInt(i);
        Parcel r1 = m12300(9, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzli = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            zzli = queryLocalInterface instanceof zzlg ? (zzlg) queryLocalInterface : new zzli(readStrongBinder);
        }
        r1.recycle();
        return zzli;
    }
}
