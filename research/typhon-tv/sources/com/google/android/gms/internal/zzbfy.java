package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;

final class zzbfy extends zzbfs {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzn<Status> f8741;

    public zzbfy(zzn<Status> zzn) {
        this.f8741 = zzn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10209(int i) throws RemoteException {
        this.f8741.m8947(new Status(i));
    }
}
