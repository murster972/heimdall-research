package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzuy extends zzev implements zzux {
    public zzuy() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzux m13449(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
        return queryLocalInterface instanceof zzux ? (zzux) queryLocalInterface : new zzuz(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                zzva r1 = m13448(parcel.readString());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r1);
                return true;
            case 2:
                boolean r12 = m13447(parcel.readString());
                parcel2.writeNoException();
                zzew.m12307(parcel2, r12);
                return true;
            default:
                return false;
        }
    }
}
