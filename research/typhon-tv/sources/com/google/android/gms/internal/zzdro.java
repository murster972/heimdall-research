package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrq;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdro extends zzffu<zzdro, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdro f9992;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdro> f9993;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f9994;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdrq f9995;

    public static final class zza extends zzffu.zza<zzdro, zza> implements zzfhg {
        private zza() {
            super(zzdro.f9992);
        }

        /* synthetic */ zza(zzdrp zzdrp) {
            this();
        }
    }

    static {
        zzdro zzdro = new zzdro();
        f9992 = zzdro;
        zzdro.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdro.f10394.m12718();
    }

    private zzdro() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdro m11846(zzfes zzfes) throws zzfge {
        return (zzdro) zzffu.m12526(f9992, zzfes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m11847() {
        return this.f9994;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11848() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9995 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f9995 == null ? zzdrq.m11852() : this.f9995) + 0;
        }
        if (this.f9994 != 0) {
            i2 += zzffg.m5245(2, this.f9994);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdrq m11849() {
        return this.f9995 == null ? zzdrq.m11852() : this.f9995;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11850(int i, Object obj, Object obj2) {
        zzdrq.zza zza2;
        boolean z = true;
        switch (zzdrp.f9996[i - 1]) {
            case 1:
                return new zzdro();
            case 2:
                return f9992;
            case 3:
                return null;
            case 4:
                return new zza((zzdrp) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdro zzdro = (zzdro) obj2;
                this.f9995 = (zzdrq) zzh.m12572(this.f9995, zzdro.f9995);
                boolean z2 = this.f9994 != 0;
                int i2 = this.f9994;
                if (zzdro.f9994 == 0) {
                    z = false;
                }
                this.f9994 = zzh.m12569(z2, i2, z, zzdro.f9994);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 10:
                                    if (this.f9995 != null) {
                                        zzdrq zzdrq = this.f9995;
                                        zzffu.zza zza3 = (zzffu.zza) zzdrq.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdrq);
                                        zza2 = (zzdrq.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f9995 = (zzdrq) zzffb.m12416(zzdrq.m11852(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f9995);
                                        this.f9995 = (zzdrq) zza2.m12543();
                                        break;
                                    }
                                case 16:
                                    this.f9994 = zzffb.m12402();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f9993 == null) {
                    synchronized (zzdro.class) {
                        if (f9993 == null) {
                            f9993 = new zzffu.zzb(f9992);
                        }
                    }
                }
                return f9993;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f9992;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11851(zzffg zzffg) throws IOException {
        if (this.f9995 != null) {
            zzffg.m5289(1, (zzfhe) this.f9995 == null ? zzdrq.m11852() : this.f9995);
        }
        if (this.f9994 != 0) {
            zzffg.m5281(2, this.f9994);
        }
        this.f10394.m12719(zzffg);
    }
}
