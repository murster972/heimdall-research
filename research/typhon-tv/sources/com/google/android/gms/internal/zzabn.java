package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzabn implements Parcelable.Creator<zzabm> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r10 = zzbfn.m10169(parcel);
        boolean z = false;
        String str = null;
        String str2 = null;
        PackageInfo packageInfo = null;
        ArrayList<String> arrayList = null;
        String str3 = null;
        ApplicationInfo applicationInfo = null;
        zzakd zzakd = null;
        Bundle bundle = null;
        while (parcel.dataPosition() < r10) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 2:
                    zzakd = (zzakd) zzbfn.m10171(parcel, readInt, zzakd.CREATOR);
                    break;
                case 3:
                    applicationInfo = (ApplicationInfo) zzbfn.m10171(parcel, readInt, ApplicationInfo.CREATOR);
                    break;
                case 4:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 6:
                    packageInfo = (PackageInfo) zzbfn.m10171(parcel, readInt, PackageInfo.CREATOR);
                    break;
                case 7:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r10);
        return new zzabm(bundle, zzakd, applicationInfo, str3, arrayList, packageInfo, str2, z, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzabm[i];
    }
}
