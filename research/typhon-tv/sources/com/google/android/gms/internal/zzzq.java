package com.google.android.gms.internal;

import java.lang.Thread;

final class zzzq implements Thread.UncaughtExceptionHandler {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzzp f11025;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Thread.UncaughtExceptionHandler f11026;

    zzzq(zzzp zzzp, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f11025 = zzzp;
        this.f11026 = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            this.f11025.m6097(thread, th);
            if (this.f11026 != null) {
                this.f11026.uncaughtException(thread, th);
            }
        } catch (Throwable th2) {
            if (this.f11026 != null) {
                this.f11026.uncaughtException(thread, th);
            }
            throw th2;
        }
    }
}
