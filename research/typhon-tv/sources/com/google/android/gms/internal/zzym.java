package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.internal.zzba;
import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzym implements Callable<zzafo> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static long f5584 = 10;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzzb f5585;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Object f5586 = new Object();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzafp f5587;

    /* renamed from: ʾ  reason: contains not printable characters */
    private JSONObject f5588;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f5589;

    /* renamed from: ˈ  reason: contains not printable characters */
    private List<String> f5590;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zznu f5591;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f5592;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f5593;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzcv f5594;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f5595;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzba f5596;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzaiv f5597;

    public zzym(Context context, zzba zzba, zzaiv zzaiv, zzcv zzcv, zzafp zzafp, zznu zznu) {
        this.f5595 = context;
        this.f5596 = zzba;
        this.f5597 = zzaiv;
        this.f5587 = zzafp;
        this.f5594 = zzcv;
        this.f5591 = zznu;
        this.f5585 = zzba.zzdq();
        this.f5592 = false;
        this.f5593 = -2;
        this.f5590 = null;
        this.f5589 = null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static <V> zzakv<List<V>> m6041(List<zzakv<V>> list) {
        zzalf zzalf = new zzalf();
        int size = list.size();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for (zzakv<V> r0 : list) {
            r0.m9670(new zzyr(atomicInteger, size, zzalf, list), zzahh.f4211);
        }
        return zzalf;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Integer m6042(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt(InternalZipTyphoonApp.READ_MODE), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException e) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean m6043() {
        boolean z;
        synchronized (this.f5586) {
            z = this.f5592;
        }
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static <V> List<V> m6044(List<zzakv<V>> list) throws ExecutionException, InterruptedException {
        ArrayList arrayList = new ArrayList();
        for (zzakv<V> zzakv : list) {
            Object obj = zzakv.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String[] m6045(JSONObject jSONObject, String str) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        String[] strArr = new String[optJSONArray.length()];
        for (int i = 0; i < optJSONArray.length(); i++) {
            strArr[i] = optJSONArray.getString(i);
        }
        return strArr;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0048, code lost:
        if (r2.length() != 0) goto L_0x004e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0093 A[Catch:{ InterruptedException | CancellationException -> 0x015f, ExecutionException -> 0x0172, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0164  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzafo call() {
        /*
            r13 = this;
            r11 = 0
            r12 = 0
            com.google.android.gms.ads.internal.zzba r2 = r13.f5596     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r10 = r2.getUuid()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            boolean r2 = r13.m6043()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 != 0) goto L_0x00b2
            com.google.android.gms.internal.zzalf r2 = new com.google.android.gms.internal.zzalf     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzyl r2 = new com.google.android.gms.internal.zzyl     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzafp r2 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaax r2 = r2.f4133     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r2 = r2.f3858     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3.<init>((java.lang.String) r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzafp r4 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaax r4 = r4.f4133     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r4 = r4.f3858     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>((java.lang.String) r4)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            int r4 = r2.length()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r4 == 0) goto L_0x004a
            java.lang.String r4 = "ads"
            org.json.JSONArray r2 = r2.optJSONArray(r4)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x00b0
            r4 = 0
            org.json.JSONObject r2 = r2.optJSONObject(r4)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x0042:
            if (r2 == 0) goto L_0x004a
            int r2 = r2.length()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 != 0) goto L_0x004e
        L_0x004a:
            r2 = 3
            r13.m6052((int) r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x004e:
            com.google.android.gms.internal.zzzb r2 = r13.f5585     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzakv r2 = r2.m6074(r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            long r4 = f5584     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.Object r2 = r2.get(r4, r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            org.json.JSONObject r2 = (org.json.JSONObject) r2     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r3 = "success"
            r4 = 0
            boolean r3 = r2.optBoolean(r3, r4)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r3 == 0) goto L_0x00b2
            java.lang.String r3 = "json"
            org.json.JSONObject r2 = r2.getJSONObject(r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r3 = "ads"
            org.json.JSONArray r2 = r2.optJSONArray(r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3 = 0
            org.json.JSONObject r7 = r2.getJSONObject(r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x007b:
            boolean r2 = r13.m6043()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 != 0) goto L_0x0083
            if (r7 != 0) goto L_0x00b4
        L_0x0083:
            r3 = r11
        L_0x0084:
            boolean r2 = r13.m6043()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 != 0) goto L_0x008e
            if (r3 == 0) goto L_0x008e
            if (r7 != 0) goto L_0x0174
        L_0x008e:
            r3 = r11
        L_0x008f:
            boolean r2 = r3 instanceof com.google.android.gms.internal.zzon     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x00ab
            r0 = r3
            com.google.android.gms.internal.zzon r0 = (com.google.android.gms.internal.zzon) r0     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2 = r0
            com.google.android.gms.internal.zzyl r4 = new com.google.android.gms.internal.zzyl     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r4.<init>()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzyo r5 = new com.google.android.gms.internal.zzyo     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r5.<init>(r13, r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r4.f10973 = r5     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzzb r2 = r13.f5585     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r4 = "/nativeAdCustomClick"
            r2.m6076((java.lang.String) r4, r5)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x00ab:
            com.google.android.gms.internal.zzafo r2 = r13.m6048((com.google.android.gms.internal.zzou) r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x00af:
            return r2
        L_0x00b0:
            r2 = r11
            goto L_0x0042
        L_0x00b2:
            r7 = r11
            goto L_0x007b
        L_0x00b4:
            java.lang.String r2 = "template_id"
            java.lang.String r5 = r7.getString(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzafp r2 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaat r2 = r2.f4136     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzpe r2 = r2.f3750     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x00f1
            com.google.android.gms.internal.zzafp r2 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaat r2 = r2.f4136     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzpe r2 = r2.f3750     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            boolean r2 = r2.f5284     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r4 = r2
        L_0x00cc:
            com.google.android.gms.internal.zzafp r2 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaat r2 = r2.f4136     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzpe r2 = r2.f3750     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x00f3
            com.google.android.gms.internal.zzafp r2 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaat r2 = r2.f4136     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzpe r2 = r2.f3750     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            boolean r2 = r2.f5285     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3 = r2
        L_0x00dd:
            java.lang.String r2 = "2"
            boolean r2 = r2.equals(r5)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x00f5
            com.google.android.gms.internal.zzzc r2 = new com.google.android.gms.internal.zzzc     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzafp r5 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            boolean r5 = r5.f4131     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>(r4, r3, r5)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3 = r2
            goto L_0x0084
        L_0x00f1:
            r4 = r12
            goto L_0x00cc
        L_0x00f3:
            r3 = r12
            goto L_0x00dd
        L_0x00f5:
            java.lang.String r2 = "1"
            boolean r2 = r2.equals(r5)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x010a
            com.google.android.gms.internal.zzzd r2 = new com.google.android.gms.internal.zzzd     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzafp r5 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            boolean r5 = r5.f4131     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>(r4, r3, r5)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3 = r2
            goto L_0x0084
        L_0x010a:
            java.lang.String r2 = "3"
            boolean r2 = r2.equals(r5)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x016d
            java.lang.String r2 = "custom_template_id"
            java.lang.String r2 = r7.getString(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzalf r3 = new com.google.android.gms.internal.zzalf     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3.<init>()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            android.os.Handler r5 = com.google.android.gms.internal.zzahn.f4212     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzyn r6 = new com.google.android.gms.internal.zzyn     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r6.<init>(r13, r3, r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r5.post(r6)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            long r8 = f5584     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.Object r2 = r3.get(r8, r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 == 0) goto L_0x013b
            com.google.android.gms.internal.zzze r2 = new com.google.android.gms.internal.zzze     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>(r4)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3 = r2
            goto L_0x0084
        L_0x013b:
            java.lang.String r3 = "No handler for custom template: "
            java.lang.String r2 = "custom_template_id"
            java.lang.String r2 = r7.getString(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            int r4 = r2.length()     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r4 == 0) goto L_0x0159
            java.lang.String r2 = r3.concat(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x0153:
            com.google.android.gms.internal.zzagf.m4795(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
        L_0x0156:
            r3 = r11
            goto L_0x0084
        L_0x0159:
            java.lang.String r2 = new java.lang.String     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>(r3)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            goto L_0x0153
        L_0x015f:
            r2 = move-exception
        L_0x0160:
            boolean r2 = r13.f5592
            if (r2 != 0) goto L_0x0167
            r13.m6052((int) r12)
        L_0x0167:
            com.google.android.gms.internal.zzafo r2 = r13.m6048((com.google.android.gms.internal.zzou) r11)
            goto L_0x00af
        L_0x016d:
            r2 = 0
            r13.m6052((int) r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            goto L_0x0156
        L_0x0172:
            r2 = move-exception
            goto L_0x0160
        L_0x0174:
            java.lang.String r2 = "tracking_urls_and_actions"
            org.json.JSONObject r4 = r7.getJSONObject(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r2 = "impression_tracking_urls"
            java.lang.String[] r2 = m6045(r4, r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            if (r2 != 0) goto L_0x01b6
            r2 = r11
        L_0x0185:
            r13.f5590 = r2     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r2 = "active_view"
            org.json.JSONObject r2 = r4.optJSONObject(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r13.f5588 = r2     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            java.lang.String r2 = "debug_signals"
            java.lang.String r2 = r7.optString(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r13.f5589 = r2     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzou r8 = r3.m13643(r13, r7)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzow r2 = new com.google.android.gms.internal.zzow     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            android.content.Context r3 = r13.f5595     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.ads.internal.zzba r4 = r13.f5596     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzzb r5 = r13.f5585     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzcv r6 = r13.f5594     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzafp r9 = r13.f5587     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzaat r9 = r9.f4136     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            com.google.android.gms.internal.zzakd r9 = r9.f3748     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r8.m13206(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            r3 = r8
            goto L_0x008f
        L_0x01b6:
            java.util.List r2 = java.util.Arrays.asList(r2)     // Catch:{ CancellationException -> 0x015f, ExecutionException -> 0x0172, InterruptedException -> 0x01d3, JSONException -> 0x01bb, TimeoutException -> 0x01c3, Exception -> 0x01cb }
            goto L_0x0185
        L_0x01bb:
            r2 = move-exception
            java.lang.String r3 = "Malformed native JSON response."
            com.google.android.gms.internal.zzagf.m4796(r3, r2)
            goto L_0x0160
        L_0x01c3:
            r2 = move-exception
            java.lang.String r3 = "Timeout when loading native ad."
            com.google.android.gms.internal.zzagf.m4796(r3, r2)
            goto L_0x0160
        L_0x01cb:
            r2 = move-exception
            java.lang.String r3 = "Error occured while doing native ads initialization."
            com.google.android.gms.internal.zzagf.m4796(r3, r2)
            goto L_0x0160
        L_0x01d3:
            r2 = move-exception
            goto L_0x0160
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzym.call():com.google.android.gms.internal.zzafo");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzafo m6048(zzou zzou) {
        int i;
        synchronized (this.f5586) {
            i = this.f5593;
            if (zzou == null && this.f5593 == -2) {
                i = 0;
            }
        }
        return new zzafo(this.f5587.f4136.f3763, (zzanh) null, this.f5587.f4133.f3860, i, this.f5587.f4133.f3857, this.f5590, this.f5587.f4133.f3849, this.f5587.f4133.f3848, this.f5587.f4136.f3740, false, (zzuh) null, (zzva) null, (String) null, (zzui) null, (zzuk) null, 0, this.f5587.f4134, this.f5587.f4133.f3820, this.f5587.f4127, this.f5587.f4128, this.f5587.f4133.f3828, this.f5588, i != -2 ? null : zzou, (zzaeq) null, (List<String>) null, (List<String>) null, this.f5587.f4133.f3862, this.f5587.f4133.f3865, (String) null, this.f5587.f4133.f3821, this.f5589, this.f5587.f4130, this.f5587.f4133.f3835, this.f5587.f4131);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzakv<zzoi> m6049(JSONObject jSONObject, boolean z, boolean z2) throws JSONException {
        String string = z ? jSONObject.getString("url") : jSONObject.optString("url");
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        if (!TextUtils.isEmpty(string)) {
            return z2 ? zzakl.m4802(new zzoi((Drawable) null, Uri.parse(string), optDouble)) : this.f5597.m4713(string, new zzyq(this, z, optDouble, optBoolean, string));
        }
        m6060(0, z);
        return zzakl.m4802(null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzanh m6050(zzakv<zzanh> zzakv) {
        try {
            return (zzanh) zzakv.get((long) ((Integer) zzkb.m5481().m5595(zznh.f4990)).intValue(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            zzagf.m4796("InterruptedException occurred while waiting for video to load", e);
            Thread.currentThread().interrupt();
        } catch (CancellationException | ExecutionException | TimeoutException e2) {
            zzagf.m4796("Exception occurred while waiting for video to load", e2);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m6052(int i) {
        synchronized (this.f5586) {
            this.f5592 = true;
            this.f5593 = i;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6053(zzqm zzqm, String str) {
        try {
            zzqw zzs = this.f5596.zzs(zzqm.m13319());
            if (zzs != null) {
                zzs.m13347(zzqm, str);
            }
        } catch (RemoteException e) {
            zzagf.m4796(new StringBuilder(String.valueOf(str).length() + 40).append("Failed to call onCustomClick for asset ").append(str).append(".").toString(), e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<zzog> m6055(JSONObject jSONObject) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject("attribution");
        if (optJSONObject == null) {
            return zzakl.m4802(null);
        }
        String optString = optJSONObject.optString(MimeTypes.BASE_TYPE_TEXT);
        int optInt = optJSONObject.optInt("text_size", -1);
        Integer r13 = m6042(optJSONObject, "text_color");
        Integer r14 = m6042(optJSONObject, "bg_color");
        int optInt2 = optJSONObject.optInt("animation_ms", 1000);
        int optInt3 = optJSONObject.optInt("presentation_ms", 4000);
        int i = (this.f5587.f4136.f3750 == null || this.f5587.f4136.f3750.f5287 < 2) ? 1 : this.f5587.f4136.f3750.f5283;
        boolean optBoolean = optJSONObject.optBoolean("allow_pub_rendering");
        List arrayList = new ArrayList();
        if (optJSONObject.optJSONArray("images") != null) {
            arrayList = m6058(optJSONObject, "images", false, false, true);
        } else {
            arrayList.add(m6057(optJSONObject, "image", false, false));
        }
        return zzakl.m4805(m6041(arrayList), new zzyp(this, optString, r14, r13, optInt, optInt3, optInt2, i, optBoolean), (Executor) zzahh.f4211);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<zzanh> m6056(JSONObject jSONObject, String str) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return zzakl.m4802(null);
        }
        if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
            zzagf.m4791("Required field 'vast_xml' is missing");
            return zzakl.m4802(null);
        }
        zzyt zzyt = new zzyt(this.f5595, this.f5594, this.f5587, this.f5591, this.f5596);
        zzalf zzalf = new zzalf();
        zzbs.zzei();
        zzahn.m4612((Runnable) new zzyu(zzyt, optJSONObject, zzalf));
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<zzoi> m6057(JSONObject jSONObject, String str, boolean z, boolean z2) throws JSONException {
        JSONObject jSONObject2 = z ? jSONObject.getJSONObject(str) : jSONObject.optJSONObject(str);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return m6049(jSONObject2, z, z2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzakv<zzoi>> m6058(JSONObject jSONObject, String str, boolean z, boolean z2, boolean z3) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() == 0) {
            m6060(0, false);
            return arrayList;
        }
        int length = z3 ? optJSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            if (jSONObject2 == null) {
                jSONObject2 = new JSONObject();
            }
            arrayList.add(m6049(jSONObject2, false, z2));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Future<zzoi> m6059(JSONObject jSONObject, String str, boolean z) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        boolean optBoolean = jSONObject2.optBoolean("require", true);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return m6049(jSONObject2, optBoolean, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6060(int i, boolean z) {
        if (z) {
            m6052(i);
        }
    }
}
