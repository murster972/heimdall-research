package com.google.android.gms.internal;

import android.os.RemoteException;
import android.support.v7.media.MediaRouter;
import com.google.android.gms.common.internal.zzbq;

public final class zzazl extends MediaRouter.Callback {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f8499 = new zzbcy("MediaRouterCallback");

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzazb f8500;

    public zzazl(zzazb zzazb) {
        this.f8500 = (zzazb) zzbq.m9120(zzazb);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9818(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.f8500.m9796(routeInfo.m1222(), routeInfo.m1210());
        } catch (RemoteException e) {
            f8499.m10091(e, "Unable to call %s on %s.", "onRouteRemoved", zzazb.class.getSimpleName());
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9819(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.f8500.m9795(routeInfo.m1222(), routeInfo.m1210());
        } catch (RemoteException e) {
            f8499.m10091(e, "Unable to call %s on %s.", "onRouteSelected", zzazb.class.getSimpleName());
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9820(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.f8500.m9794(routeInfo.m1222(), routeInfo.m1210());
        } catch (RemoteException e) {
            f8499.m10091(e, "Unable to call %s on %s.", "onRouteChanged", zzazb.class.getSimpleName());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9821(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.f8500.m9797(routeInfo.m1222(), routeInfo.m1210());
        } catch (RemoteException e) {
            f8499.m10091(e, "Unable to call %s on %s.", "onRouteAdded", zzazb.class.getSimpleName());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9822(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo, int i) {
        try {
            this.f8500.m9798(routeInfo.m1222(), routeInfo.m1210(), i);
        } catch (RemoteException e) {
            f8499.m10091(e, "Unable to call %s on %s.", "onRouteUnselected", zzazb.class.getSimpleName());
        }
    }
}
