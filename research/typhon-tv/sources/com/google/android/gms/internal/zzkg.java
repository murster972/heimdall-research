package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.RemoteException;

public final class zzkg extends zzeu implements zzke {
    zzkg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdClickListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13054() throws RemoteException {
        m12298(1, v_());
    }
}
