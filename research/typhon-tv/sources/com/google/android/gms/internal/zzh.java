package com.google.android.gms.internal;

public final class zzh implements zzaa {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f10677;

    /* renamed from: 麤  reason: contains not printable characters */
    private final float f10678;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f10679;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f10680;

    public zzh() {
        this(2500, 1, 1.0f);
    }

    private zzh(int i, int i2, float f) {
        this.f10680 = 2500;
        this.f10679 = 1;
        this.f10678 = 1.0f;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m12978() {
        return this.f10677;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12979() {
        return this.f10680;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12980(zzad zzad) throws zzad {
        this.f10677++;
        this.f10680 = (int) (((float) this.f10680) + (((float) this.f10680) * this.f10678));
        if (!(this.f10677 <= this.f10679)) {
            throw zzad;
        }
    }
}
