package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfkc extends zzfjm<zzfkc> {

    /* renamed from: 靐  reason: contains not printable characters */
    private byte[] f10598 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f10599 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Integer f10600 = null;

    public zzfkc() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12904() {
        int r0 = super.m12841();
        if (this.f10600 != null) {
            r0 += zzfjk.m12806(1, this.f10600.intValue());
        }
        if (this.f10598 != null) {
            r0 += zzfjk.m12809(2, this.f10598);
        }
        return this.f10599 != null ? r0 + zzfjk.m12809(3, this.f10599) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12905(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f10600 = Integer.valueOf(zzfjj.m12798());
                    continue;
                case 18:
                    this.f10598 = zzfjj.m12784();
                    continue;
                case 26:
                    this.f10599 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12906(zzfjk zzfjk) throws IOException {
        if (this.f10600 != null) {
            zzfjk.m12832(1, this.f10600.intValue());
        }
        if (this.f10598 != null) {
            zzfjk.m12837(2, this.f10598);
        }
        if (this.f10599 != null) {
            zzfjk.m12837(3, this.f10599);
        }
        super.m12842(zzfjk);
    }
}
