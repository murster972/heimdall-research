package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.overlay.zzn;

final class zzanp implements zzn {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzn f8346;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzanh f8347;

    public zzanp(zzanh zzanh, zzn zzn) {
        this.f8347 = zzanh;
        this.f8346 = zzn;
    }

    public final void onPause() {
    }

    public final void onResume() {
    }

    public final void zzcg() {
        this.f8346.zzcg();
        this.f8347.m4976();
    }

    public final void zzch() {
        this.f8346.zzch();
        this.f8347.m4977();
    }
}
