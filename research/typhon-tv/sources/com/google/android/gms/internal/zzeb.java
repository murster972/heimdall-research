package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzeb extends zzet {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f10255 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile Long f10256 = null;

    public zzeb(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 44);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12274() throws IllegalAccessException, InvocationTargetException {
        if (f10256 == null) {
            synchronized (f10255) {
                if (f10256 == null) {
                    f10256 = (Long) this.f10286.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.f10284) {
            this.f10284.f8460 = f10256;
        }
    }
}
