package com.google.android.gms.internal;

import android.location.Location;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

final class zzcef extends zzcem {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Location f9054;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcef(zzceb zzceb, GoogleApiClient googleApiClient, Location location) {
        super(googleApiClient);
        this.f9054 = location;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10342(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10408(this.f9054);
        m4198(Status.f7464);
    }
}
