package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzdl extends zzbt<Integer, Long> {

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f9869;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f9870;

    public zzdl() {
    }

    public zzdl(String str) {
        m10294(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<Integer, Long> m11595() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.f9870);
        hashMap.put(1, this.f9869);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11596(String str) {
        HashMap r1 = m10292(str);
        if (r1 != null) {
            this.f9870 = (Long) r1.get(0);
            this.f9869 = (Long) r1.get(1);
        }
    }
}
