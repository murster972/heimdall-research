package com.google.android.gms.internal;

final class zzxv implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzxu f10963;

    zzxv(zzxu zzxu) {
        this.f10963 = zzxu;
    }

    public final void run() {
        if (this.f10963.f5559.get()) {
            zzagf.m4795("Timed out waiting for WebView to finish loading.");
            this.f10963.m6018();
        }
    }
}
