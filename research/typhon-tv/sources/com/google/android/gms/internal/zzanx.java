package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.Pinkamena;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.util.zzq;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
final class zzanx extends WebView implements ViewTreeObserver.OnGlobalLayoutListener, DownloadListener, zzanh {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzv f4477;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private int f4478 = -1;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzani f4479;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private int f4480 = -1;

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzd f4481;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private int f4482 = -1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4483;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private Map<String, zzana> f4484;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f4485;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private int f4486 = -1;

    /* renamed from: ˆ  reason: contains not printable characters */
    private zzaoa f4487;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private final zzis f4488;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f4489;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f4490;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f4491 = true;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f4492 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f4493 = "";

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f4494;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzapa f4495;

    /* renamed from: י  reason: contains not printable characters */
    private zzoq f4496;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f4497;

    /* renamed from: ــ  reason: contains not printable characters */
    private final WindowManager f4498;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f4499;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f4500;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private boolean f4501;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public int f4502;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private zzajq f4503;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private zzns f4504;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private zzns f4505;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private zzns f4506;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private zznt f4507;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzbl f4508;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f4509 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzakd f4510;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzcv f4511;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaoz f4512;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private WeakReference<View.OnClickListener> f4513;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Boolean f4514;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f4515;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private zzd f4516;

    /* JADX WARNING: type inference failed for: r2v15, types: [com.google.android.gms.internal.zzaof, com.google.android.gms.internal.zzaoe] */
    private zzanx(zzaoz zzaoz, zzapa zzapa, String str, boolean z, boolean z2, zzcv zzcv, zzakd zzakd, zznu zznu, zzbl zzbl, zzv zzv, zzis zzis) {
        super(zzaoz);
        this.f4512 = zzaoz;
        this.f4495 = zzapa;
        this.f4499 = str;
        this.f4483 = z;
        this.f4515 = -1;
        this.f4511 = zzcv;
        this.f4510 = zzakd;
        this.f4508 = zzbl;
        this.f4477 = zzv;
        this.f4498 = (WindowManager) getContext().getSystemService("window");
        this.f4488 = zzis;
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        try {
            settings.setJavaScriptEnabled(true);
        } catch (NullPointerException e) {
            zzagf.m4793("Unable to enable Javascript.", e);
        }
        settings.setSavePassword(false);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(2);
        }
        settings.setUserAgentString(zzbs.zzei().m4632((Context) zzaoz, zzakd.f4297));
        zzbs.zzek().m4662(getContext(), settings);
        setDownloadListener(this);
        m5122();
        if (zzq.m9271()) {
            addJavascriptInterface(new zzaod(this, new zzaoe(this)), "googleAdsJsInterface");
        }
        removeJavascriptInterface("accessibility");
        removeJavascriptInterface("accessibilityTraversal");
        this.f4503 = new zzajq(this.f4512.m5220(), this, this, (ViewTreeObserver.OnScrollChangedListener) null);
        m5125();
        this.f4507 = new zznt(new zznu(true, "make_wv", this.f4499));
        this.f4507.m5619().m5627(zznu);
        this.f4505 = zznn.m5613(this.f4507.m5619());
        this.f4507.m5620("native:view_create", this.f4505);
        this.f4506 = null;
        this.f4504 = null;
        zzbs.zzek().m4645((Context) zzaoz);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m5119(boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", z ? PubnativeRequest.LEGACY_ZONE_ID : "0");
        zza("onAdVisibilityChanged", (Map<String, ?>) hashMap);
    }

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private final Boolean m5120() {
        Boolean bool;
        synchronized (this.f4509) {
            bool = this.f4514;
        }
        return bool;
    }

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private final boolean m5121() {
        int i;
        int i2;
        if (!this.f4479.m5040() && !this.f4479.m5042()) {
            return false;
        }
        zzbs.zzei();
        DisplayMetrics r6 = zzahn.m4594(this.f4498);
        zzkb.m5487();
        int r1 = zzajr.m4750(r6, r6.widthPixels);
        zzkb.m5487();
        int r2 = zzajr.m4750(r6, r6.heightPixels);
        Activity r3 = this.f4512.m5220();
        if (r3 == null || r3.getWindow() == null) {
            i = r2;
            i2 = r1;
        } else {
            zzbs.zzei();
            int[] r4 = zzahn.m4622(r3);
            zzkb.m5487();
            i2 = zzajr.m4750(r6, r4[0]);
            zzkb.m5487();
            i = zzajr.m4750(r6, r4[1]);
        }
        if (this.f4482 == r1 && this.f4478 == r2 && this.f4480 == i2 && this.f4486 == i) {
            return false;
        }
        boolean z = (this.f4482 == r1 && this.f4478 == r2) ? false : true;
        this.f4482 = r1;
        this.f4478 = r2;
        this.f4480 = i2;
        this.f4486 = i;
        new zzxb(this).m6011(r1, r2, i2, i, r6.density, this.f4498.getDefaultDisplay().getRotation());
        return z;
    }

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private final void m5122() {
        synchronized (this.f4509) {
            if (this.f4483 || this.f4495.m5227()) {
                zzagf.m4792("Enabling hardware acceleration on an overlay.");
                m5124();
            } else if (Build.VERSION.SDK_INT < 18) {
                zzagf.m4792("Disabling hardware acceleration on an AdView.");
                synchronized (this.f4509) {
                    if (!this.f4485) {
                        zzbs.zzek().m4651((View) this);
                    }
                    this.f4485 = true;
                }
            } else {
                zzagf.m4792("Enabling hardware acceleration on an AdView.");
                m5124();
            }
        }
    }

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private final void m5123() {
        synchronized (this.f4509) {
            if (!this.f4501) {
                this.f4501 = true;
                zzbs.zzem().m4479();
            }
        }
    }

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private final void m5124() {
        synchronized (this.f4509) {
            if (this.f4485) {
                zzbs.zzek().m4646((View) this);
            }
            this.f4485 = false;
        }
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private final void m5125() {
        zznu r0;
        if (this.f4507 != null && (r0 = this.f4507.m5619()) != null && zzbs.zzem().m4483() != null) {
            zzbs.zzem().m4483().m5611(r0);
        }
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private final void m5126() {
        synchronized (this.f4509) {
            this.f4484 = null;
        }
    }

    /* renamed from: ــ  reason: contains not printable characters */
    private final void m5127() {
        zznn.m5614(this.f4507.m5619(), this.f4505, "aeh2");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m5129(String str) {
        if (zzq.m9268()) {
            if (m5120() == null) {
                synchronized (this.f4509) {
                    this.f4514 = zzbs.zzem().m4477();
                    if (this.f4514 == null) {
                        try {
                            evaluateJavascript("(function(){})()", (ValueCallback<String>) null);
                            m5135((Boolean) true);
                        } catch (IllegalStateException e) {
                            m5135((Boolean) false);
                        }
                    }
                }
            }
            if (m5120().booleanValue()) {
                synchronized (this.f4509) {
                    if (!m5159()) {
                        evaluateJavascript(str, (ValueCallback<String>) null);
                    } else {
                        zzagf.m4791("The webview is destroyed. Ignoring action.");
                    }
                }
                return;
            }
            String valueOf = String.valueOf(str);
            m5131(valueOf.length() != 0 ? "javascript:".concat(valueOf) : new String("javascript:"));
            return;
        }
        String valueOf2 = String.valueOf(str);
        m5131(valueOf2.length() != 0 ? "javascript:".concat(valueOf2) : new String("javascript:"));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m5131(String str) {
        synchronized (this.f4509) {
            if (!m5159()) {
                Pinkamena.DianePie();
            } else {
                zzagf.m4791("The webview is destroyed. Ignoring action.");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzanx m5133(Context context, zzapa zzapa, String str, boolean z, boolean z2, zzcv zzcv, zzakd zzakd, zznu zznu, zzbl zzbl, zzv zzv, zzis zzis) {
        return new zzanx(new zzaoz(context), zzapa, str, z, z2, zzcv, zzakd, zznu, zzbl, zzv, zzis);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5135(Boolean bool) {
        synchronized (this.f4509) {
            this.f4514 = bool;
        }
        zzbs.zzem().m4503(bool);
    }

    public final void destroy() {
        synchronized (this.f4509) {
            m5125();
            this.f4503.m4739();
            if (this.f4481 != null) {
                this.f4481.close();
                this.f4481.onDestroy();
                this.f4481 = null;
            }
            this.f4479.m5034();
            if (!this.f4489) {
                zzbs.zzfb();
                zzamz.m4954((zzamp) this);
                m5126();
                this.f4489 = true;
                zzagf.m4527("Initiating WebView self destruct sequence in 3...");
                this.f4479.m5030();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    @android.annotation.TargetApi(19)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void evaluateJavascript(java.lang.String r3, android.webkit.ValueCallback<java.lang.String> r4) {
        /*
            r2 = this;
            java.lang.Object r1 = r2.f4509
            monitor-enter(r1)
            boolean r0 = r2.m5159()     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = "The webview is destroyed. Ignoring action."
            com.google.android.gms.internal.zzagf.m4791(r0)     // Catch:{ all -> 0x001c }
            if (r4 == 0) goto L_0x0015
            r0 = 0
            r4.onReceiveValue(r0)     // Catch:{ all -> 0x001c }
        L_0x0015:
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
        L_0x0016:
            return
        L_0x0017:
            super.evaluateJavascript(r3, r4)     // Catch:{ all -> 0x001c }
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
            goto L_0x0016
        L_0x001c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzanx.evaluateJavascript(java.lang.String, android.webkit.ValueCallback):void");
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            if (this.f4509 != null) {
                synchronized (this.f4509) {
                    if (!this.f4489) {
                        this.f4479.m5034();
                        zzbs.zzfb();
                        zzamz.m4954((zzamp) this);
                        m5126();
                        m5123();
                    }
                }
            }
            super.finalize();
        } catch (Throwable th) {
            super.finalize();
            throw th;
        }
    }

    public final void loadData(String str, String str2, String str3) {
        synchronized (this.f4509) {
            if (!m5159()) {
                super.loadData(str, str2, str3);
            } else {
                zzagf.m4791("The webview is destroyed. Ignoring action.");
            }
        }
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        synchronized (this.f4509) {
            if (!m5159()) {
                super.loadDataWithBaseURL(str, str2, str3, str4, str5);
            } else {
                zzagf.m4791("The webview is destroyed. Ignoring action.");
            }
        }
    }

    public final void loadUrl(String str) {
        synchronized (this.f4509) {
            if (!m5159()) {
                try {
                    super.loadUrl(str);
                } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError e) {
                    zzbs.zzem().m4505(e, "AdWebViewImpl.loadUrl");
                    zzagf.m4796("Could not call loadUrl. ", e);
                }
            } else {
                zzagf.m4791("The webview is destroyed. Ignoring action.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        boolean z = true;
        synchronized (this.f4509) {
            super.onAttachedToWindow();
            if (!m5159()) {
                this.f4503.m4741();
            }
            boolean z2 = this.f4490;
            if (this.f4479 == null || !this.f4479.m5042()) {
                z = z2;
            } else {
                if (!this.f4494) {
                    ViewTreeObserver.OnGlobalLayoutListener r1 = this.f4479.m5041();
                    if (r1 != null) {
                        zzbs.zzfc();
                        if (this == null) {
                            throw null;
                        }
                        zzaln.m4826((View) this, r1);
                    }
                    ViewTreeObserver.OnScrollChangedListener r12 = this.f4479.m5038();
                    if (r12 != null) {
                        zzbs.zzfc();
                        if (this == null) {
                            throw null;
                        }
                        zzaln.m4827((View) this, r12);
                    }
                    this.f4494 = true;
                }
                m5121();
            }
            m5119(z);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        synchronized (this.f4509) {
            if (!m5159()) {
                this.f4503.m4740();
            }
            super.onDetachedFromWindow();
            if (this.f4494 && this.f4479 != null && this.f4479.m5042() && getViewTreeObserver() != null && getViewTreeObserver().isAlive()) {
                ViewTreeObserver.OnGlobalLayoutListener r0 = this.f4479.m5041();
                if (r0 != null) {
                    zzbs.zzek().m4660(getViewTreeObserver(), r0);
                }
                ViewTreeObserver.OnScrollChangedListener r02 = this.f4479.m5038();
                if (r02 != null) {
                    getViewTreeObserver().removeOnScrollChangedListener(r02);
                }
                this.f4494 = false;
            }
        }
        m5119(false);
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            zzbs.zzei();
            zzahn.m4608(getContext(), intent);
        } catch (ActivityNotFoundException e) {
            zzagf.m4792(new StringBuilder(String.valueOf(str).length() + 51 + String.valueOf(str4).length()).append("Couldn't find an Activity to view url/mimetype: ").append(str).append(" / ").append(str4).toString());
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public final void onDraw(Canvas canvas) {
        if (!m5159()) {
            if (Build.VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
                super.onDraw(canvas);
                if (this.f4479 != null && this.f4479.m5032() != null) {
                    this.f4479.m5032().m9710();
                }
            }
        }
    }

    public final boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if (((Boolean) zzkb.m5481().m5595(zznh.f4908)).booleanValue()) {
            float axisValue = motionEvent.getAxisValue(9);
            float axisValue2 = motionEvent.getAxisValue(10);
            if (motionEvent.getActionMasked() == 8 && ((axisValue > 0.0f && !canScrollVertically(-1)) || ((axisValue < 0.0f && !canScrollVertically(1)) || ((axisValue2 > 0.0f && !canScrollHorizontally(-1)) || (axisValue2 < 0.0f && !canScrollHorizontally(1)))))) {
                return false;
            }
        }
        return super.onGenericMotionEvent(motionEvent);
    }

    public final void onGlobalLayout() {
        boolean r0 = m5121();
        zzd r1 = m5147();
        if (r1 != null && r0) {
            r1.zzmy();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return;
     */
    @android.annotation.SuppressLint({"DrawAllocation"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            r8 = 1073741824(0x40000000, float:2.0)
            r7 = 8
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = 0
            java.lang.Object r6 = r9.f4509
            monitor-enter(r6)
            boolean r0 = r9.m5159()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x0017
            r0 = 0
            r1 = 0
            r9.setMeasuredDimension(r0, r1)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
        L_0x0016:
            return
        L_0x0017:
            boolean r0 = r9.isInEditMode()     // Catch:{ all -> 0x002e }
            if (r0 != 0) goto L_0x0029
            boolean r0 = r9.f4483     // Catch:{ all -> 0x002e }
            if (r0 != 0) goto L_0x0029
            com.google.android.gms.internal.zzapa r0 = r9.f4495     // Catch:{ all -> 0x002e }
            boolean r0 = r0.m5226()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x0031
        L_0x0029:
            super.onMeasure(r10, r11)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x002e:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            throw r0
        L_0x0031:
            com.google.android.gms.internal.zzapa r0 = r9.f4495     // Catch:{ all -> 0x002e }
            boolean r0 = r0.m5225()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x007b
            com.google.android.gms.internal.zzaoa r0 = r9.m5165()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x01b8
            float r0 = r0.m5205()     // Catch:{ all -> 0x002e }
        L_0x0043:
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x004c
            super.onMeasure(r10, r11)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x004c:
            int r5 = android.view.View.MeasureSpec.getSize(r10)     // Catch:{ all -> 0x002e }
            int r4 = android.view.View.MeasureSpec.getSize(r11)     // Catch:{ all -> 0x002e }
            float r1 = (float) r4     // Catch:{ all -> 0x002e }
            float r1 = r1 * r0
            int r3 = (int) r1     // Catch:{ all -> 0x002e }
            float r1 = (float) r5     // Catch:{ all -> 0x002e }
            float r1 = r1 / r0
            int r1 = (int) r1     // Catch:{ all -> 0x002e }
            if (r4 != 0) goto L_0x0070
            if (r1 == 0) goto L_0x0070
            float r2 = (float) r1     // Catch:{ all -> 0x002e }
            float r0 = r0 * r2
            int r2 = (int) r0     // Catch:{ all -> 0x002e }
            r0 = r1
            r4 = r1
        L_0x0063:
            int r1 = java.lang.Math.min(r2, r5)     // Catch:{ all -> 0x002e }
            int r0 = java.lang.Math.min(r0, r4)     // Catch:{ all -> 0x002e }
            r9.setMeasuredDimension(r1, r0)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x0070:
            if (r5 != 0) goto L_0x01b4
            if (r3 == 0) goto L_0x01b4
            float r1 = (float) r3     // Catch:{ all -> 0x002e }
            float r0 = r1 / r0
            int r0 = (int) r0     // Catch:{ all -> 0x002e }
            r2 = r3
            r5 = r3
            goto L_0x0063
        L_0x007b:
            com.google.android.gms.internal.zzapa r0 = r9.f4495     // Catch:{ all -> 0x002e }
            boolean r0 = r0.m5228()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x00d7
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f4998     // Catch:{ all -> 0x002e }
            com.google.android.gms.internal.zznf r1 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x002e }
            java.lang.Object r0 = r1.m5595(r0)     // Catch:{ all -> 0x002e }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x002e }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x002e }
            if (r0 != 0) goto L_0x009b
            boolean r0 = com.google.android.gms.common.util.zzq.m9271()     // Catch:{ all -> 0x002e }
            if (r0 != 0) goto L_0x00a1
        L_0x009b:
            super.onMeasure(r10, r11)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x00a1:
            java.lang.String r0 = "/contentHeight"
            com.google.android.gms.internal.zzany r1 = new com.google.android.gms.internal.zzany     // Catch:{ all -> 0x002e }
            r1.<init>(r9)     // Catch:{ all -> 0x002e }
            r9.m5185((java.lang.String) r0, (com.google.android.gms.ads.internal.gmsg.zzt<? super com.google.android.gms.internal.zzanh>) r1)     // Catch:{ all -> 0x002e }
            java.lang.String r0 = "(function() {  var height = -1;  if (document.body) {    height = document.body.offsetHeight;  } else if (document.documentElement) {    height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  try {    window.googleAdsJsInterface.notify(url);  } catch (e) {    var frame = document.getElementById('afma-notify-fluid');    if (!frame) {      frame = document.createElement('IFRAME');      frame.id = 'afma-notify-fluid';      frame.style.display = 'none';      var body = document.body || document.documentElement;      body.appendChild(frame);    }    frame.src = url;  }})();"
            r9.m5129((java.lang.String) r0)     // Catch:{ all -> 0x002e }
            com.google.android.gms.internal.zzaoz r0 = r9.f4512     // Catch:{ all -> 0x002e }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ all -> 0x002e }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ all -> 0x002e }
            float r0 = r0.density     // Catch:{ all -> 0x002e }
            int r1 = android.view.View.MeasureSpec.getSize(r10)     // Catch:{ all -> 0x002e }
            int r2 = r9.f4502     // Catch:{ all -> 0x002e }
            switch(r2) {
                case -1: goto L_0x00d2;
                default: goto L_0x00c7;
            }     // Catch:{ all -> 0x002e }
        L_0x00c7:
            int r2 = r9.f4502     // Catch:{ all -> 0x002e }
            float r2 = (float) r2     // Catch:{ all -> 0x002e }
            float r0 = r0 * r2
            int r0 = (int) r0     // Catch:{ all -> 0x002e }
        L_0x00cc:
            r9.setMeasuredDimension(r1, r0)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x00d2:
            int r0 = android.view.View.MeasureSpec.getSize(r11)     // Catch:{ all -> 0x002e }
            goto L_0x00cc
        L_0x00d7:
            com.google.android.gms.internal.zzapa r0 = r9.f4495     // Catch:{ all -> 0x002e }
            boolean r0 = r0.m5227()     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x00f7
            android.util.DisplayMetrics r0 = new android.util.DisplayMetrics     // Catch:{ all -> 0x002e }
            r0.<init>()     // Catch:{ all -> 0x002e }
            android.view.WindowManager r1 = r9.f4498     // Catch:{ all -> 0x002e }
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ all -> 0x002e }
            r1.getMetrics(r0)     // Catch:{ all -> 0x002e }
            int r1 = r0.widthPixels     // Catch:{ all -> 0x002e }
            int r0 = r0.heightPixels     // Catch:{ all -> 0x002e }
            r9.setMeasuredDimension(r1, r0)     // Catch:{ all -> 0x002e }
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x00f7:
            int r2 = android.view.View.MeasureSpec.getMode(r10)     // Catch:{ all -> 0x002e }
            int r3 = android.view.View.MeasureSpec.getSize(r10)     // Catch:{ all -> 0x002e }
            int r4 = android.view.View.MeasureSpec.getMode(r11)     // Catch:{ all -> 0x002e }
            int r1 = android.view.View.MeasureSpec.getSize(r11)     // Catch:{ all -> 0x002e }
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r2 == r5) goto L_0x010e
            if (r2 != r8) goto L_0x01b1
        L_0x010e:
            r2 = r3
        L_0x010f:
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L_0x0116
            if (r4 != r8) goto L_0x0117
        L_0x0116:
            r0 = r1
        L_0x0117:
            com.google.android.gms.internal.zzapa r4 = r9.f4495     // Catch:{ all -> 0x002e }
            int r4 = r4.f4537     // Catch:{ all -> 0x002e }
            if (r4 > r2) goto L_0x0123
            com.google.android.gms.internal.zzapa r2 = r9.f4495     // Catch:{ all -> 0x002e }
            int r2 = r2.f4539     // Catch:{ all -> 0x002e }
            if (r2 <= r0) goto L_0x0199
        L_0x0123:
            com.google.android.gms.internal.zzaoz r0 = r9.f4512     // Catch:{ all -> 0x002e }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ all -> 0x002e }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ all -> 0x002e }
            float r0 = r0.density     // Catch:{ all -> 0x002e }
            com.google.android.gms.internal.zzapa r2 = r9.f4495     // Catch:{ all -> 0x002e }
            int r2 = r2.f4537     // Catch:{ all -> 0x002e }
            float r2 = (float) r2     // Catch:{ all -> 0x002e }
            float r2 = r2 / r0
            int r2 = (int) r2     // Catch:{ all -> 0x002e }
            com.google.android.gms.internal.zzapa r4 = r9.f4495     // Catch:{ all -> 0x002e }
            int r4 = r4.f4539     // Catch:{ all -> 0x002e }
            float r4 = (float) r4     // Catch:{ all -> 0x002e }
            float r4 = r4 / r0
            int r4 = (int) r4     // Catch:{ all -> 0x002e }
            float r3 = (float) r3     // Catch:{ all -> 0x002e }
            float r3 = r3 / r0
            int r3 = (int) r3     // Catch:{ all -> 0x002e }
            float r1 = (float) r1     // Catch:{ all -> 0x002e }
            float r0 = r1 / r0
            int r0 = (int) r0     // Catch:{ all -> 0x002e }
            r1 = 103(0x67, float:1.44E-43)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x002e }
            r5.<init>(r1)     // Catch:{ all -> 0x002e }
            java.lang.String r1 = "Not enough space to show ad. Needs "
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x002e }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "x"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x002e }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x002e }
            java.lang.String r2 = " dp, but only has "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x002e }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "x"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x002e }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x002e }
            java.lang.String r1 = " dp."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x002e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x002e }
            com.google.android.gms.internal.zzagf.m4791(r0)     // Catch:{ all -> 0x002e }
            int r0 = r9.getVisibility()     // Catch:{ all -> 0x002e }
            if (r0 == r7) goto L_0x0191
            r0 = 8
            r9.setVisibility(r0)     // Catch:{ all -> 0x002e }
        L_0x0191:
            r0 = 0
            r1 = 0
            r9.setMeasuredDimension(r0, r1)     // Catch:{ all -> 0x002e }
        L_0x0196:
            monitor-exit(r6)     // Catch:{ all -> 0x002e }
            goto L_0x0016
        L_0x0199:
            int r0 = r9.getVisibility()     // Catch:{ all -> 0x002e }
            if (r0 == r7) goto L_0x01a5
            r0 = 8
            r9.setVisibility(r0)     // Catch:{ all -> 0x002e }
        L_0x01a5:
            com.google.android.gms.internal.zzapa r0 = r9.f4495     // Catch:{ all -> 0x002e }
            int r0 = r0.f4537     // Catch:{ all -> 0x002e }
            com.google.android.gms.internal.zzapa r1 = r9.f4495     // Catch:{ all -> 0x002e }
            int r1 = r1.f4539     // Catch:{ all -> 0x002e }
            r9.setMeasuredDimension(r0, r1)     // Catch:{ all -> 0x002e }
            goto L_0x0196
        L_0x01b1:
            r2 = r0
            goto L_0x010f
        L_0x01b4:
            r0 = r1
            r2 = r3
            goto L_0x0063
        L_0x01b8:
            r0 = r1
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzanx.onMeasure(int, int):void");
    }

    public final void onPause() {
        if (!m5159()) {
            try {
                super.onPause();
            } catch (Exception e) {
                zzagf.m4793("Could not pause webview.", e);
            }
        }
    }

    public final void onResume() {
        if (!m5159()) {
            try {
                super.onResume();
            } catch (Exception e) {
                zzagf.m4793("Could not resume webview.", e);
            }
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f4479.m5042()) {
            synchronized (this.f4509) {
                if (this.f4496 != null) {
                    this.f4496.m13183(motionEvent);
                }
            }
        } else if (this.f4511 != null) {
            this.f4511.m11540(motionEvent);
        }
        if (m5159()) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.f4513 = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        super.setWebViewClient(webViewClient);
        if (webViewClient instanceof zzani) {
            this.f4479 = (zzani) webViewClient;
        }
    }

    public final void stopLoading() {
        if (!m5159()) {
            try {
                super.stopLoading();
            } catch (Exception e) {
                zzagf.m4793("Could not stop loading webview.", e);
            }
        }
    }

    public final void zza(String str, Map<String, ?> map) {
        try {
            zza(str, zzbs.zzei().m4634(map));
        } catch (JSONException e) {
            zzagf.m4791("Could not convert parameters to JSON.");
        }
    }

    public final void zza(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("(window.AFMA_ReceiveMessage || function() {})('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String valueOf = String.valueOf(sb.toString());
        zzagf.m4792(valueOf.length() != 0 ? "Dispatching AFMA event: ".concat(valueOf) : new String("Dispatching AFMA event: "));
        m5129(sb.toString());
    }

    public final void zzb(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        m5129(new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(jSONObject2).length()).append(str).append("(").append(jSONObject2).append(");").toString());
    }

    public final void zzcq() {
        synchronized (this.f4509) {
            this.f4492 = true;
            if (this.f4508 != null) {
                this.f4508.zzcq();
            }
        }
    }

    public final void zzcr() {
        synchronized (this.f4509) {
            this.f4492 = false;
            if (this.f4508 != null) {
                this.f4508.zzcr();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5136() {
        zzd r0 = m5147();
        if (r0 != null) {
            r0.zzna();
        }
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final zzoq m5137() {
        zzoq zzoq;
        synchronized (this.f4509) {
            zzoq = this.f4496;
        }
        return zzoq;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5138() {
        String str;
        synchronized (this.f4509) {
            str = this.f4493;
        }
        return str;
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final void m5139() {
        zzagf.m4527("Cannot add text view to inner AdWebView");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zznt m5140() {
        return this.f4507;
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final void m5141() {
        setBackgroundColor(0);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m5142() {
        m5127();
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.f4510.f4297);
        zza("onhide", (Map<String, ?>) hashMap);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m5143() {
        if (this.f4504 == null) {
            zznn.m5614(this.f4507.m5619(), this.f4505, "aes2");
            this.f4504 = zznn.m5613(this.f4507.m5619());
            this.f4507.m5620("native:view_show", this.f4504);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.f4510.f4297);
        zza("onshow", (Map<String, ?>) hashMap);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final String m5144() {
        String str;
        synchronized (this.f4509) {
            str = this.f4499;
        }
        return str;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final WebView m5145() {
        return this;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final zzani m5146() {
        return this.f4479;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final zzd m5147() {
        zzd zzd;
        synchronized (this.f4509) {
            zzd = this.f4481;
        }
        return zzd;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzd m5148() {
        zzd zzd;
        synchronized (this.f4509) {
            zzd = this.f4516;
        }
        return zzd;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final zzapa m5149() {
        zzapa zzapa;
        synchronized (this.f4509) {
            zzapa = this.f4495;
        }
        return zzapa;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final boolean m5150() {
        boolean z;
        synchronized (this.f4509) {
            z = this.f4500;
        }
        return z;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzakd m5151() {
        return this.f4510;
    }

    /* renamed from: י  reason: contains not printable characters */
    public final zzcv m5152() {
        return this.f4511;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final boolean m5153() {
        boolean z;
        synchronized (this.f4509) {
            z = this.f4483;
        }
        return z;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m5154() {
        return getMeasuredHeight();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int m5155() {
        return getMeasuredWidth();
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final void m5156() {
        if (this.f4506 == null) {
            this.f4506 = zznn.m5613(this.f4507.m5619());
            this.f4507.m5620("native:view_load", this.f4506);
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final int m5157() {
        int i;
        synchronized (this.f4509) {
            i = this.f4515;
        }
        return i;
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final View.OnClickListener m5158() {
        return (View.OnClickListener) this.f4513.get();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final boolean m5159() {
        boolean z;
        synchronized (this.f4509) {
            z = this.f4489;
        }
        return z;
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final void m5160() {
        synchronized (this.f4509) {
            zzagf.m4527("Destroying WebView!");
            m5123();
            zzahn.f4212.post(new zzanz(this));
        }
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final boolean m5161() {
        boolean z;
        synchronized (this.f4509) {
            z = this.f4491;
        }
        return z;
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final boolean m5162() {
        boolean z;
        synchronized (this.f4509) {
            z = this.f4492;
        }
        return z;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzv m5163() {
        return this.f4477;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m5164(boolean z) {
        synchronized (this.f4509) {
            this.f4497 = (z ? 1 : -1) + this.f4497;
            if (this.f4497 <= 0 && this.f4481 != null) {
                this.f4481.zznb();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzaoa m5165() {
        zzaoa zzaoa;
        synchronized (this.f4509) {
            zzaoa = this.f4487;
        }
        return zzaoa;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5166(int i) {
        synchronized (this.f4509) {
            this.f4515 = i;
            if (this.f4481 != null) {
                this.f4481.setRequestedOrientation(this.f4515);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5167(zzd zzd) {
        synchronized (this.f4509) {
            this.f4516 = zzd;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5168(String str) {
        synchronized (this.f4509) {
            if (str == null) {
                str = "";
            }
            this.f4493 = str;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5169(String str, zzt<? super zzanh> zzt) {
        if (this.f4479 != null) {
            this.f4479.m5039(str, zzt);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5170(boolean z) {
        synchronized (this.f4509) {
            boolean z2 = z != this.f4483;
            this.f4483 = z;
            m5122();
            if (z2) {
                new zzxb(this).m6009(z ? "expanded" : "default");
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Activity m5171() {
        return this.f4512.m5220();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5172(boolean z) {
        synchronized (this.f4509) {
            this.f4491 = z;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzns m5173() {
        return this.f4505;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5174(boolean z) {
        synchronized (this.f4509) {
            if (this.f4481 != null) {
                this.f4481.zza(this.f4479.m5040(), z);
            } else {
                this.f4500 = z;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzamg m5175() {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5176(int i) {
        if (i == 0) {
            zznn.m5614(this.f4507.m5619(), this.f4505, "aebb2");
        }
        m5127();
        if (this.f4507.m5619() != null) {
            this.f4507.m5619().m5629("close_type", String.valueOf(i));
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i));
        hashMap.put("version", this.f4510.f4297);
        zza("onhide", (Map<String, ?>) hashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5177(Context context) {
        this.f4512.setBaseContext(context);
        this.f4503.m4743(this.f4512.m5220());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5178(zzc zzc) {
        this.f4479.m5047(zzc);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5179(zzd zzd) {
        synchronized (this.f4509) {
            this.f4481 = zzd;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5180(zzaoa zzaoa) {
        synchronized (this.f4509) {
            if (this.f4487 != null) {
                zzagf.m4795("Attempt to create multiple AdWebViewVideoControllers.");
            } else {
                this.f4487 = zzaoa;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5181(zzapa zzapa) {
        synchronized (this.f4509) {
            this.f4495 = zzapa;
            requestLayout();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5182(zzgs zzgs) {
        synchronized (this.f4509) {
            this.f4490 = zzgs.f10667;
        }
        m5119(zzgs.f10667);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5183(zzoq zzoq) {
        synchronized (this.f4509) {
            this.f4496 = zzoq;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5184(String str) {
        synchronized (this.f4509) {
            try {
                super.loadUrl(str);
            } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError | UnsatisfiedLinkError e) {
                zzbs.zzem().m4505(e, "AdWebViewImpl.loadUrlUnsafe");
                zzagf.m4796("Could not call loadUrl. ", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5185(String str, zzt<? super zzanh> zzt) {
        if (this.f4479 != null) {
            this.f4479.m5053(str, zzt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5186(boolean z) {
        this.f4479.m5055(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5187(boolean z, int i) {
        this.f4479.m5056(z, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5188(boolean z, int i, String str) {
        this.f4479.m5057(z, i, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5189(boolean z, int i, String str, String str2) {
        this.f4479.m5058(z, i, str, str2);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public final boolean m5190() {
        boolean z;
        synchronized (this.f4509) {
            z = this.f4497 > 0;
        }
        return z;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m5191() {
        HashMap hashMap = new HashMap(3);
        zzbs.zzei();
        hashMap.put("app_muted", String.valueOf(zzahn.m4586()));
        zzbs.zzei();
        hashMap.put("app_volume", String.valueOf(zzahn.m4587()));
        zzbs.zzei();
        hashMap.put("device_volume", String.valueOf(zzahn.m4571(getContext())));
        zza("volume", (Map<String, ?>) hashMap);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final Context m5192() {
        return this.f4512.m5219();
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final void m5193() {
        this.f4503.m4742();
    }
}
