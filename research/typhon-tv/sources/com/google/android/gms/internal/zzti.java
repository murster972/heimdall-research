package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.util.Iterator;
import java.util.LinkedList;

@zzzv
final class zzti {

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f5366;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public zzjj f5367;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f5368;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f5369;

    /* renamed from: 龘  reason: contains not printable characters */
    private final LinkedList<zztj> f5370 = new LinkedList<>();

    zzti(zzjj zzjj, String str, int i) {
        zzbq.m9120(zzjj);
        zzbq.m9120(str);
        this.f5367 = zzjj;
        this.f5369 = str;
        this.f5368 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final int m5841() {
        int i = 0;
        Iterator it2 = this.f5370.iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return i2;
            }
            i = ((zztj) it2.next()).m13431() ? i2 + 1 : i2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m5842() {
        this.f5366 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m5843() {
        return this.f5366;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final int m5844() {
        int i = 0;
        Iterator it2 = this.f5370.iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return i2;
            }
            i = ((zztj) it2.next()).f10890 ? i2 + 1 : i2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final int m5845() {
        return this.f5368;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final int m5846() {
        return this.f5370.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final String m5847() {
        return this.f5369;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzjj m5848() {
        return this.f5367;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zztj m5849(zzjj zzjj) {
        if (zzjj != null) {
            this.f5367 = zzjj;
        }
        return this.f5370.remove();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5850(zzsd zzsd, zzjj zzjj) {
        this.f5370.add(new zztj(this, zzsd, zzjj));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5851(zzsd zzsd) {
        zztj zztj = new zztj(this, zzsd);
        this.f5370.add(zztj);
        return zztj.m13431();
    }
}
