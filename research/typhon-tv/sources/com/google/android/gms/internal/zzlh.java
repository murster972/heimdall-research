package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzlh extends zzev implements zzlg {
    public zzlh() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                initialize();
                parcel2.writeNoException();
                return true;
            case 2:
                setAppVolume(parcel.readFloat());
                parcel2.writeNoException();
                return true;
            case 3:
                zzu(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 4:
                setAppMuted(zzew.m12308(parcel));
                parcel2.writeNoException();
                return true;
            case 5:
                zzb(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                zza(parcel.readString(), IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 7:
                float zzdn = zzdn();
                parcel2.writeNoException();
                parcel2.writeFloat(zzdn);
                return true;
            case 8:
                boolean zzdo = zzdo();
                parcel2.writeNoException();
                zzew.m12307(parcel2, zzdo);
                return true;
            default:
                return false;
        }
    }
}
