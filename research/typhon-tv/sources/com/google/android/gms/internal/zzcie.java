package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;

final class zzcie implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ BroadcastReceiver.PendingResult f9344;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzchm f9345;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ long f9346;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ Context f9347;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ Bundle f9348;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcim f9349;

    zzcie(zzcid zzcid, zzcim zzcim, long j, Bundle bundle, Context context, zzchm zzchm, BroadcastReceiver.PendingResult pendingResult) {
        this.f9349 = zzcim;
        this.f9346 = j;
        this.f9348 = bundle;
        this.f9347 = context;
        this.f9345 = zzchm;
        this.f9344 = pendingResult;
    }

    public final void run() {
        zzclp r0 = this.f9349.m11024().m10598(this.f9349.m11034().m10724(), "_fot");
        long longValue = (r0 == null || !(r0.f9654 instanceof Long)) ? 0 : ((Long) r0.f9654).longValue();
        long j = this.f9346;
        long j2 = (longValue <= 0 || (j < longValue && j > 0)) ? j : longValue - 1;
        if (j2 > 0) {
            this.f9348.putLong("click_timestamp", j2);
        }
        AppMeasurement.getInstance(this.f9347).logEventInternal("auto", "_cmp", this.f9348);
        this.f9345.m10848().m10849("Install campaign recorded");
        if (this.f9344 != null) {
            this.f9344.finish();
        }
    }
}
