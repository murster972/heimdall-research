package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public final class zzcv {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final String[] f9806 = {"/aclk", "/pcs/click"};

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzcr f9807;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f9808 = "/pagead/ads";

    /* renamed from: 麤  reason: contains not printable characters */
    private String[] f9809 = {".doubleclick.net", ".googleadservices.com", ".googlesyndication.com"};

    /* renamed from: 齉  reason: contains not printable characters */
    private String f9810 = "ad.doubleclick.net";

    /* renamed from: 龘  reason: contains not printable characters */
    private String f9811 = "googleads.g.doubleclick.net";

    public zzcv(zzcr zzcr) {
        this.f9807 = zzcr;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m11534(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }
        try {
            return uri.getHost().equals(this.f9810);
        } catch (NullPointerException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Uri m11535(Uri uri, Context context, String str, boolean z, View view, Activity activity) throws zzcw {
        try {
            boolean r2 = m11534(uri);
            if (r2) {
                if (uri.toString().contains("dc_ms=")) {
                    throw new zzcw("Parameter already exists: dc_ms");
                }
            } else if (uri.getQueryParameter("ms") != null) {
                throw new zzcw("Query parameter already exists: ms");
            }
            String zza = z ? this.f9807.zza(context, str, view, activity) : this.f9807.zza(context);
            if (r2) {
                String uri2 = uri.toString();
                int indexOf = uri2.indexOf(";adurl");
                if (indexOf != -1) {
                    return Uri.parse(uri2.substring(0, indexOf + 1) + "dc_ms" + "=" + zza + ";" + uri2.substring(indexOf + 1));
                }
                String encodedPath = uri.getEncodedPath();
                int indexOf2 = uri2.indexOf(encodedPath);
                return Uri.parse(uri2.substring(0, encodedPath.length() + indexOf2) + ";" + "dc_ms" + "=" + zza + ";" + uri2.substring(encodedPath.length() + indexOf2));
            }
            String uri3 = uri.toString();
            int indexOf3 = uri3.indexOf("&adurl");
            if (indexOf3 == -1) {
                indexOf3 = uri3.indexOf("?adurl");
            }
            return indexOf3 != -1 ? Uri.parse(uri3.substring(0, indexOf3 + 1) + "ms" + "=" + zza + "&" + uri3.substring(indexOf3 + 1)) : uri.buildUpon().appendQueryParameter("ms", zza).build();
        } catch (UnsupportedOperationException e) {
            throw new zzcw("Provided Uri is not in a valid state");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m11536(Uri uri) {
        if (!m11541(uri)) {
            return false;
        }
        for (String endsWith : f9806) {
            if (uri.getPath().endsWith(endsWith)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Uri m11537(Uri uri, Context context) throws zzcw {
        return m11535(uri, context, (String) null, false, (View) null, (Activity) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Uri m11538(Uri uri, Context context, View view, Activity activity) throws zzcw {
        try {
            return m11535(uri, context, uri.getQueryParameter("ai"), true, view, activity);
        } catch (UnsupportedOperationException e) {
            throw new zzcw("Provided Uri is not in a valid state");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcr m11539() {
        return this.f9807;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11540(MotionEvent motionEvent) {
        this.f9807.zza(motionEvent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11541(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }
        try {
            String host = uri.getHost();
            for (String endsWith : this.f9809) {
                if (host.endsWith(endsWith)) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
