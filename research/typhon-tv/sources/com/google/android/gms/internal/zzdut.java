package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class zzdut implements zzdpp {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzdqa f10210;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f10211;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzdvf f10212;

    public zzdut(zzdvf zzdvf, zzdqa zzdqa, int i) {
        this.f10212 = zzdvf;
        this.f10210 = zzdqa;
        this.f10211 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12215(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] r0 = this.f10212.m12229(bArr);
        byte[] copyOf = Arrays.copyOf(ByteBuffer.allocate(8).putLong(8 * ((long) bArr2.length)).array(), 8);
        return zzdua.m12173(r0, this.f10210.m11665(zzdua.m12173(bArr2, r0, copyOf)));
    }
}
