package com.google.android.gms.internal;

@zzzv
public final class zzhp {

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f4708;

    /* renamed from: 靐  reason: contains not printable characters */
    private final float f4709;

    /* renamed from: 麤  reason: contains not printable characters */
    private final float f4710;

    /* renamed from: 齉  reason: contains not printable characters */
    private final float f4711;

    /* renamed from: 龘  reason: contains not printable characters */
    private final float f4712;

    public zzhp(float f, float f2, float f3, float f4, int i) {
        this.f4712 = f;
        this.f4709 = f2;
        this.f4711 = f + f3;
        this.f4710 = f2 + f4;
        this.f4708 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final int m5390() {
        return this.f4708;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final float m5391() {
        return this.f4709;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final float m5392() {
        return this.f4710;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final float m5393() {
        return this.f4711;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final float m5394() {
        return this.f4712;
    }
}
