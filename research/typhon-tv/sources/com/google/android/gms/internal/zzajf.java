package com.google.android.gms.internal;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.common.internal.zzbq;

@zzzv
public final class zzajf {

    /* renamed from: 靐  reason: contains not printable characters */
    private Handler f4250 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Object f4251 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private int f4252 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private HandlerThread f4253 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public final Handler m4717() {
        return this.f4250;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Looper m4718() {
        Looper looper;
        synchronized (this.f4251) {
            if (this.f4252 != 0) {
                zzbq.m9121(this.f4253, (Object) "Invalid state: mHandlerThread should already been initialized.");
            } else if (this.f4253 == null) {
                zzagf.m4527("Starting the looper thread.");
                this.f4253 = new HandlerThread("LooperProvider");
                this.f4253.start();
                this.f4250 = new Handler(this.f4253.getLooper());
                zzagf.m4527("Looper thread started.");
            } else {
                zzagf.m4527("Resuming the looper thread");
                this.f4251.notifyAll();
            }
            this.f4252++;
            looper = this.f4253.getLooper();
        }
        return looper;
    }
}
