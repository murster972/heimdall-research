package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.FrameLayout;
import java.util.HashMap;

@zzzv
public class zzjr {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzri f4802;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final zzadt f4803;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzxd f4804;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzrj f4805;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzmb f4806;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f4807 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzjh f4808;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzji f4809;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzla f4810;

    abstract class zza<T> {
        zza() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public final T m13031() {
            zzla r1 = zzjr.this.m5469();
            if (r1 == null) {
                zzakb.m4791("ClientApi class cannot be loaded.");
                return null;
            }
            try {
                return m13034(r1);
            } catch (RemoteException e) {
                zzakb.m4796("Cannot invoke local loader using ClientApi class", e);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public final T m13032() {
            try {
                return m13033();
            } catch (RemoteException e) {
                zzakb.m4796("Cannot invoke remote loader", e);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract T m13033() throws RemoteException;

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract T m13034(zzla zzla) throws RemoteException;
    }

    public zzjr(zzji zzji, zzjh zzjh, zzmb zzmb, zzri zzri, zzadt zzadt, zzxd zzxd, zzrj zzrj) {
        this.f4809 = zzji;
        this.f4808 = zzjh;
        this.f4806 = zzmb;
        this.f4802 = zzri;
        this.f4803 = zzadt;
        this.f4804 = zzxd;
        this.f4805 = zzrj;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final zzla m5469() {
        zzla zzla;
        synchronized (this.f4807) {
            if (this.f4810 == null) {
                this.f4810 = m5472();
            }
            zzla = this.f4810;
        }
        return zzla;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzla m5472() {
        try {
            Object newInstance = zzjr.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").newInstance();
            if (newInstance instanceof IBinder) {
                return zzlb.asInterface((IBinder) newInstance);
            }
            zzakb.m4791("ClientApi class is not an instance of IBinder");
            return null;
        } catch (Exception e) {
            zzakb.m4796("Failed to instantiate ClientApi class.", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> T m5474(Context context, boolean z, zza<T> zza2) {
        boolean z2 = true;
        boolean z3 = z;
        if (!z3) {
            zzkb.m5487();
            if (!zzajr.m4756(context)) {
                zzakb.m4792("Google Play Services is not available");
                z3 = true;
            }
        }
        zzkb.m5487();
        int r2 = zzajr.m4748(context);
        zzkb.m5487();
        if (r2 <= zzajr.m4754(context)) {
            z2 = z3;
        }
        if (z2) {
            T r0 = zza2.m13031();
            return r0 == null ? zza2.m13032() : r0;
        }
        T r02 = zza2.m13032();
        return r02 == null ? zza2.m13031() : r02;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m5475(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("action", "no_ads_fallback");
        bundle.putString("flow", str);
        zzkb.m5487().m4767(context, (String) null, "gmob-apps", bundle, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzkn m5477(Context context, String str, zzux zzux) {
        return (zzkn) m5474(context, false, new zzjv(this, context, str, zzux));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzpu m5478(Context context, FrameLayout frameLayout, FrameLayout frameLayout2) {
        return (zzpu) m5474(context, false, new zzjx(this, frameLayout, frameLayout2, context));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzpz m5479(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        return (zzpz) m5474(view.getContext(), false, new zzjy(this, view, hashMap, hashMap2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzxe m5480(Activity activity) {
        boolean z = false;
        Intent intent = activity.getIntent();
        if (!intent.hasExtra("com.google.android.gms.ads.internal.overlay.useClientJar")) {
            zzakb.m4795("useClientJar flag not found in activity intent extras.");
        } else {
            z = intent.getBooleanExtra("com.google.android.gms.ads.internal.overlay.useClientJar", false);
        }
        return (zzxe) m5474((Context) activity, z, new zzka(this, activity));
    }
}
