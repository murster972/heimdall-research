package com.google.android.gms.internal;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

@zzzv
public final class zzpj extends zzqa implements View.OnClickListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: 龘  reason: contains not printable characters */
    static final String[] f5301 = {NativeAppInstallAd.ASSET_MEDIA_VIDEO, NativeContentAd.ASSET_MEDIA_VIDEO};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<String, WeakReference<View>> f5302 = new HashMap();

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzos f5303;

    /* renamed from: ʽ  reason: contains not printable characters */
    private View f5304;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Point f5305 = new Point();

    /* renamed from: ٴ  reason: contains not printable characters */
    private Point f5306 = new Point();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private WeakReference<zzgp> f5307 = new WeakReference<>((Object) null);

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<String, WeakReference<View>> f5308 = new HashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f5309 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<String, WeakReference<View>> f5310 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<View> f5311;

    public zzpj(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        zzbs.zzfc();
        zzaln.m4826(view, (ViewTreeObserver.OnGlobalLayoutListener) this);
        zzbs.zzfc();
        zzaln.m4827(view, (ViewTreeObserver.OnScrollChangedListener) this);
        view.setOnTouchListener(this);
        view.setOnClickListener(this);
        this.f5311 = new WeakReference<>(view);
        m5787((Map<String, View>) hashMap);
        this.f5302.putAll(this.f5310);
        m5782(hashMap2);
        this.f5302.putAll(this.f5308);
        zznh.m5599(view.getContext());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m5782(Map<String, View> map) {
        for (Map.Entry next : map.entrySet()) {
            View view = (View) next.getValue();
            if (view != null) {
                this.f5308.put((String) next.getKey(), new WeakReference(view));
                view.setOnTouchListener(this);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m5783(int i) {
        int r0;
        synchronized (this.f5309) {
            zzkb.m5487();
            r0 = zzajr.m4749(this.f5303.m13187(), i);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5784(View view) {
        synchronized (this.f5309) {
            if (this.f5303 != null) {
                zzos r0 = this.f5303 instanceof zzor ? ((zzor) this.f5303).m5723() : this.f5303;
                if (r0 != null) {
                    r0.m13189(view);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5785(com.google.android.gms.internal.zzow r7) {
        /*
            r6 = this;
            java.lang.Object r2 = r6.f5309
            monitor-enter(r2)
            java.lang.String[] r3 = f5301     // Catch:{ all -> 0x0039 }
            int r4 = r3.length     // Catch:{ all -> 0x0039 }
            r0 = 0
            r1 = r0
        L_0x0008:
            if (r1 >= r4) goto L_0x0029
            r0 = r3[r1]     // Catch:{ all -> 0x0039 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r5 = r6.f5302     // Catch:{ all -> 0x0039 }
            java.lang.Object r0 = r5.get(r0)     // Catch:{ all -> 0x0039 }
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x0025
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0039 }
            android.view.View r0 = (android.view.View) r0     // Catch:{ all -> 0x0039 }
        L_0x001c:
            boolean r1 = r0 instanceof android.widget.FrameLayout     // Catch:{ all -> 0x0039 }
            if (r1 != 0) goto L_0x002b
            r7.m5743()     // Catch:{ all -> 0x0039 }
            monitor-exit(r2)     // Catch:{ all -> 0x0039 }
        L_0x0024:
            return
        L_0x0025:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0008
        L_0x0029:
            r0 = 0
            goto L_0x001c
        L_0x002b:
            com.google.android.gms.internal.zzpl r1 = new com.google.android.gms.internal.zzpl     // Catch:{ all -> 0x0039 }
            r1.<init>(r6, r0)     // Catch:{ all -> 0x0039 }
            boolean r3 = r7 instanceof com.google.android.gms.internal.zzor     // Catch:{ all -> 0x0039 }
            if (r3 == 0) goto L_0x003c
            r7.m5752((android.view.View) r0, (com.google.android.gms.internal.zzoq) r1)     // Catch:{ all -> 0x0039 }
        L_0x0037:
            monitor-exit(r2)     // Catch:{ all -> 0x0039 }
            goto L_0x0024
        L_0x0039:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0039 }
            throw r0
        L_0x003c:
            r7.m5758((android.view.View) r0, (com.google.android.gms.internal.zzoq) r1)     // Catch:{ all -> 0x0039 }
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpj.m5785(com.google.android.gms.internal.zzow):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5787(Map<String, View> map) {
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            View view = (View) next.getValue();
            if (view != null) {
                this.f5310.put(str, new WeakReference(view));
                if (!NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str)) {
                    view.setOnTouchListener(this);
                    view.setClickable(true);
                    view.setOnClickListener(this);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5789(String[] strArr) {
        for (String str : strArr) {
            if (this.f5310.get(str) != null) {
                return true;
            }
        }
        for (String str2 : strArr) {
            if (this.f5308.get(str2) != null) {
                return false;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r8) {
        /*
            r7 = this;
            java.lang.Object r6 = r7.f5309
            monitor-enter(r6)
            com.google.android.gms.internal.zzos r0 = r7.f5303     // Catch:{ all -> 0x0015 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r6)     // Catch:{ all -> 0x0015 }
        L_0x0008:
            return
        L_0x0009:
            java.lang.ref.WeakReference<android.view.View> r0 = r7.f5311     // Catch:{ all -> 0x0015 }
            java.lang.Object r5 = r0.get()     // Catch:{ all -> 0x0015 }
            android.view.View r5 = (android.view.View) r5     // Catch:{ all -> 0x0015 }
            if (r5 != 0) goto L_0x0018
            monitor-exit(r6)     // Catch:{ all -> 0x0015 }
            goto L_0x0008
        L_0x0015:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0015 }
            throw r0
        L_0x0018:
            android.os.Bundle r3 = new android.os.Bundle     // Catch:{ all -> 0x0015 }
            r3.<init>()     // Catch:{ all -> 0x0015 }
            java.lang.String r0 = "x"
            android.graphics.Point r1 = r7.f5305     // Catch:{ all -> 0x0015 }
            int r1 = r1.x     // Catch:{ all -> 0x0015 }
            int r1 = r7.m5783((int) r1)     // Catch:{ all -> 0x0015 }
            float r1 = (float) r1     // Catch:{ all -> 0x0015 }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x0015 }
            java.lang.String r0 = "y"
            android.graphics.Point r1 = r7.f5305     // Catch:{ all -> 0x0015 }
            int r1 = r1.y     // Catch:{ all -> 0x0015 }
            int r1 = r7.m5783((int) r1)     // Catch:{ all -> 0x0015 }
            float r1 = (float) r1     // Catch:{ all -> 0x0015 }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x0015 }
            java.lang.String r0 = "start_x"
            android.graphics.Point r1 = r7.f5306     // Catch:{ all -> 0x0015 }
            int r1 = r1.x     // Catch:{ all -> 0x0015 }
            int r1 = r7.m5783((int) r1)     // Catch:{ all -> 0x0015 }
            float r1 = (float) r1     // Catch:{ all -> 0x0015 }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x0015 }
            java.lang.String r0 = "start_y"
            android.graphics.Point r1 = r7.f5306     // Catch:{ all -> 0x0015 }
            int r1 = r1.y     // Catch:{ all -> 0x0015 }
            int r1 = r7.m5783((int) r1)     // Catch:{ all -> 0x0015 }
            float r1 = (float) r1     // Catch:{ all -> 0x0015 }
            r3.putFloat(r0, r1)     // Catch:{ all -> 0x0015 }
            android.view.View r0 = r7.f5304     // Catch:{ all -> 0x0015 }
            if (r0 == 0) goto L_0x0094
            android.view.View r0 = r7.f5304     // Catch:{ all -> 0x0015 }
            boolean r0 = r0.equals(r8)     // Catch:{ all -> 0x0015 }
            if (r0 == 0) goto L_0x0094
            com.google.android.gms.internal.zzos r0 = r7.f5303     // Catch:{ all -> 0x0015 }
            boolean r0 = r0 instanceof com.google.android.gms.internal.zzor     // Catch:{ all -> 0x0015 }
            if (r0 == 0) goto L_0x0088
            com.google.android.gms.internal.zzos r0 = r7.f5303     // Catch:{ all -> 0x0015 }
            com.google.android.gms.internal.zzor r0 = (com.google.android.gms.internal.zzor) r0     // Catch:{ all -> 0x0015 }
            com.google.android.gms.internal.zzos r0 = r0.m5723()     // Catch:{ all -> 0x0015 }
            if (r0 == 0) goto L_0x0086
            com.google.android.gms.internal.zzos r0 = r7.f5303     // Catch:{ all -> 0x0015 }
            com.google.android.gms.internal.zzor r0 = (com.google.android.gms.internal.zzor) r0     // Catch:{ all -> 0x0015 }
            com.google.android.gms.internal.zzos r0 = r0.m5723()     // Catch:{ all -> 0x0015 }
            java.lang.String r2 = "1007"
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r7.f5302     // Catch:{ all -> 0x0015 }
            r1 = r8
            r0.m13198(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0015 }
        L_0x0086:
            monitor-exit(r6)     // Catch:{ all -> 0x0015 }
            goto L_0x0008
        L_0x0088:
            com.google.android.gms.internal.zzos r0 = r7.f5303     // Catch:{ all -> 0x0015 }
            java.lang.String r2 = "1007"
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r7.f5302     // Catch:{ all -> 0x0015 }
            r1 = r8
            r0.m13198(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0015 }
            goto L_0x0086
        L_0x0094:
            com.google.android.gms.internal.zzos r0 = r7.f5303     // Catch:{ all -> 0x0015 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r1 = r7.f5302     // Catch:{ all -> 0x0015 }
            r0.m13200(r8, r1, r3, r5)     // Catch:{ all -> 0x0015 }
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpj.onClick(android.view.View):void");
    }

    public final void onGlobalLayout() {
        View view;
        synchronized (this.f5309) {
            if (!(this.f5303 == null || (view = (View) this.f5311.get()) == null)) {
                this.f5303.m13193(view, this.f5302);
            }
        }
    }

    public final void onScrollChanged() {
        View view;
        synchronized (this.f5309) {
            if (!(this.f5303 == null || (view = (View) this.f5311.get()) == null)) {
                this.f5303.m13193(view, this.f5302);
            }
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.f5309) {
            if (this.f5303 != null) {
                View view2 = (View) this.f5311.get();
                if (view2 != null) {
                    int[] iArr = new int[2];
                    view2.getLocationOnScreen(iArr);
                    Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
                    this.f5305 = point;
                    if (motionEvent.getAction() == 0) {
                        this.f5306 = point;
                    }
                    MotionEvent obtain = MotionEvent.obtain(motionEvent);
                    obtain.setLocation((float) point.x, (float) point.y);
                    this.f5303.m13195(obtain);
                    obtain.recycle();
                }
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5790() {
        synchronized (this.f5309) {
            this.f5304 = null;
            this.f5303 = null;
            this.f5305 = null;
            this.f5306 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5791(IObjectWrapper iObjectWrapper) {
        synchronized (this.f5309) {
            m5784((View) null);
            Object r1 = zzn.m9307(iObjectWrapper);
            if (!(r1 instanceof zzow)) {
                zzagf.m4791("Not an instance of native engine. This is most likely a transient error");
                return;
            }
            zzow zzow = (zzow) r1;
            if (!zzow.m5751()) {
                zzagf.m4795("Your account must be enabled to use this feature. Talk to your account manager to request this feature for your account.");
                return;
            }
            View view = (View) this.f5311.get();
            if (!(this.f5303 == null || view == null)) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f4987)).booleanValue()) {
                    this.f5303.m13190(view, this.f5302);
                }
            }
            synchronized (this.f5309) {
                if (this.f5303 instanceof zzow) {
                    zzow zzow2 = (zzow) this.f5303;
                    View view2 = (View) this.f5311.get();
                    if (!(zzow2 == null || zzow2.m5745() == null || view2 == null || !zzbs.zzfd().m4432(view2.getContext()))) {
                        zzafe r4 = zzow2.m5746();
                        if (r4 != null) {
                            r4.m4408(false);
                        }
                        zzgp zzgp = (zzgp) this.f5307.get();
                        if (!(zzgp == null || r4 == null)) {
                            zzgp.m5355((zzgt) r4);
                        }
                    }
                }
            }
            if (!(this.f5303 instanceof zzor) || !((zzor) this.f5303).m5724()) {
                this.f5303 = zzow;
                if (zzow instanceof zzor) {
                    ((zzor) zzow).m5729((zzos) null);
                }
            } else {
                ((zzor) this.f5303).m5729(zzow);
            }
            WeakReference weakReference = this.f5302.get(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW);
            if (weakReference == null) {
                zzagf.m4791("Ad choices asset view is not provided.");
            } else {
                View view3 = (View) weakReference.get();
                ViewGroup viewGroup = view3 instanceof ViewGroup ? (ViewGroup) view3 : null;
                if (viewGroup != null) {
                    this.f5304 = zzow.m5755((View.OnClickListener) this, true);
                    if (this.f5304 != null) {
                        this.f5302.put(NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE, new WeakReference(this.f5304));
                        this.f5310.put(NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE, new WeakReference(this.f5304));
                        viewGroup.removeAllViews();
                        View view4 = this.f5304;
                    }
                }
            }
            zzow.m5762(view, this.f5310, this.f5308, (View.OnTouchListener) this, (View.OnClickListener) this);
            zzahn.f4212.post(new zzpk(this, zzow));
            m5784(view);
            this.f5303.m13196(view);
            synchronized (this.f5309) {
                if (this.f5303 instanceof zzow) {
                    zzow zzow3 = (zzow) this.f5303;
                    View view5 = (View) this.f5311.get();
                    if (!(zzow3 == null || zzow3.m5745() == null || view5 == null || !zzbs.zzfd().m4432(view5.getContext()))) {
                        zzgp zzgp2 = (zzgp) this.f5307.get();
                        if (zzgp2 == null) {
                            zzgp2 = new zzgp(view5.getContext(), view5);
                            this.f5307 = new WeakReference<>(zzgp2);
                        }
                        zzgp2.m5357((zzgt) zzow3.m5746());
                    }
                }
            }
        }
    }
}
