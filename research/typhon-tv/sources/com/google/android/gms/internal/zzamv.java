package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

@zzzv
public final class zzamv implements zzt<zzamp> {

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f4431;

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m4949(Context context, Map<String, String> map, String str, int i) {
        String str2 = map.get(str);
        if (str2 == null) {
            return i;
        }
        try {
            zzkb.m5487();
            return zzajr.m4757(context, Integer.parseInt(str2));
        } catch (NumberFormatException e) {
            zzagf.m4791(new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(str2).length()).append("Could not parse ").append(str).append(" in a video GMSG: ").append(str2).toString());
            return i;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4950(zzamd zzamd, Map<String, String> map) {
        String str = map.get("minBufferMs");
        String str2 = map.get("maxBufferMs");
        String str3 = map.get("bufferForPlaybackMs");
        String str4 = map.get("bufferForPlaybackAfterRebufferMs");
        if (str != null) {
            try {
                Integer.parseInt(str);
            } catch (NumberFormatException e) {
                zzagf.m4791(String.format("Could not parse buffer parameters in loadControl video GMSG: (%s, %s)", new Object[]{str, str2}));
                return;
            }
        }
        if (str2 != null) {
            Integer.parseInt(str2);
        }
        if (str3 != null) {
            Integer.parseInt(str3);
        }
        if (str4 != null) {
            Integer.parseInt(str4);
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        int i;
        int i2;
        zzamp zzamp = (zzamp) obj;
        String str = (String) map.get("action");
        if (str == null) {
            zzagf.m4791("Action missing from video GMSG.");
            return;
        }
        if (zzagf.m4798(3)) {
            JSONObject jSONObject = new JSONObject(map);
            jSONObject.remove("google.afma.Notify_dt");
            String jSONObject2 = jSONObject.toString();
            zzagf.m4792(new StringBuilder(String.valueOf(str).length() + 13 + String.valueOf(jSONObject2).length()).append("Video GMSG: ").append(str).append(StringUtils.SPACE).append(jSONObject2).toString());
        }
        if ("background".equals(str)) {
            String str2 = (String) map.get(TtmlNode.ATTR_TTS_COLOR);
            if (TextUtils.isEmpty(str2)) {
                zzagf.m4791("Color parameter missing from color video GMSG.");
                return;
            }
            try {
                zzamp.setBackgroundColor(Color.parseColor(str2));
            } catch (IllegalArgumentException e) {
                zzagf.m4791("Invalid color parameter in video GMSG.");
            }
        } else if ("decoderProps".equals(str)) {
            String str3 = (String) map.get("mimeTypes");
            if (str3 == null) {
                zzagf.m4791("No MIME types specified for decoder properties inspection.");
                zzamd.m4864(zzamp, "missingMimeTypes");
            } else if (Build.VERSION.SDK_INT < 16) {
                zzagf.m4791("Video decoder properties available on API versions >= 16.");
                zzamd.m4864(zzamp, "deficientApiVersion");
            } else {
                HashMap hashMap = new HashMap();
                for (String str4 : str3.split(",")) {
                    hashMap.put(str4, zzajp.m4735(str4.trim()));
                }
                zzamd.m4865(zzamp, (Map<String, List<Map<String, Object>>>) hashMap);
            }
        } else {
            zzamg r0 = zzamp.m4930();
            if (r0 == null) {
                zzagf.m4791("Could not get underlay container for a video GMSG.");
                return;
            }
            boolean equals = "new".equals(str);
            boolean equals2 = "position".equals(str);
            if (equals || equals2) {
                Context context = zzamp.getContext();
                int r1 = m4949(context, map, "x", 0);
                int r2 = m4949(context, map, "y", 0);
                int r5 = m4949(context, map, "w", -1);
                int r4 = m4949(context, map, "h", -1);
                if (((Boolean) zzkb.m5481().m5595(zznh.f4992)).booleanValue()) {
                    i = Math.min(r5, zzamp.m4925() - r1);
                    r4 = Math.min(r4, zzamp.m4924() - r2);
                } else {
                    i = r5;
                }
                try {
                    i2 = Integer.parseInt((String) map.get("player"));
                } catch (NumberFormatException e2) {
                    i2 = 0;
                }
                boolean parseBoolean = Boolean.parseBoolean((String) map.get("spherical"));
                if (!equals || r0.m4893() != null) {
                    r0.m4894(r1, r2, i, r4);
                    return;
                }
                r0.m4895(r1, r2, i, r4, i2, parseBoolean, new zzamo((String) map.get("flags")));
                zzamd r02 = r0.m4893();
                if (r02 != null) {
                    m4950(r02, map);
                    return;
                }
                return;
            }
            zzamd r9 = r0.m4893();
            if (r9 == null) {
                zzamd.m4863(zzamp);
            } else if ("click".equals(str)) {
                Context context2 = zzamp.getContext();
                int r22 = m4949(context2, map, "x", 0);
                int r3 = m4949(context2, map, "y", 0);
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, (float) r22, (float) r3, 0);
                r9.m4887(obtain);
                obtain.recycle();
            } else if ("currentTime".equals(str)) {
                String str5 = (String) map.get("time");
                if (str5 == null) {
                    zzagf.m4791("Time parameter missing from currentTime video GMSG.");
                    return;
                }
                try {
                    r9.m4884((int) (Float.parseFloat(str5) * 1000.0f));
                } catch (NumberFormatException e3) {
                    String valueOf = String.valueOf(str5);
                    zzagf.m4791(valueOf.length() != 0 ? "Could not parse time parameter from currentTime video GMSG: ".concat(valueOf) : new String("Could not parse time parameter from currentTime video GMSG: "));
                }
            } else if ("hide".equals(str)) {
                r9.setVisibility(8);
            } else if ("load".equals(str)) {
                r9.m4870();
            } else if ("loadControl".equals(str)) {
                m4950(r9, map);
            } else if ("muted".equals(str)) {
                if (Boolean.parseBoolean((String) map.get("muted"))) {
                    r9.m4876();
                } else {
                    r9.m4873();
                }
            } else if ("pause".equals(str)) {
                r9.m4874();
            } else if ("play".equals(str)) {
                r9.m4875();
            } else if ("show".equals(str)) {
                r9.setVisibility(0);
            } else if ("src".equals(str)) {
                r9.m4888((String) map.get("src"));
            } else if ("touchMove".equals(str)) {
                Context context3 = zzamp.getContext();
                r9.m4883((float) m4949(context3, map, "dx", 0), (float) m4949(context3, map, "dy", 0));
                if (!this.f4431) {
                    zzamp.m4920();
                    this.f4431 = true;
                }
            } else if ("volume".equals(str)) {
                String str6 = (String) map.get("volume");
                if (str6 == null) {
                    zzagf.m4791("Level parameter missing from volume video GMSG.");
                    return;
                }
                try {
                    r9.m4882(Float.parseFloat(str6));
                } catch (NumberFormatException e4) {
                    String valueOf2 = String.valueOf(str6);
                    zzagf.m4791(valueOf2.length() != 0 ? "Could not parse volume parameter from volume video GMSG: ".concat(valueOf2) : new String("Could not parse volume parameter from volume video GMSG: "));
                }
            } else if ("watermark".equals(str)) {
                r9.m4871();
            } else {
                String valueOf3 = String.valueOf(str);
                zzagf.m4791(valueOf3.length() != 0 ? "Unknown video action: ".concat(valueOf3) : new String("Unknown video action: "));
            }
        }
    }
}
