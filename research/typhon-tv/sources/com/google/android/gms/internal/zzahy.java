package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.net.http.SslError;
import android.webkit.WebChromeClient;

@TargetApi(14)
public class zzahy extends zzahw {
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m9618() {
        return 1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final WebChromeClient m9619(zzanh zzanh) {
        return new zzaor(zzanh);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m9620(SslError sslError) {
        return sslError.getUrl();
    }
}
