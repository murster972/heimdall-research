package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzbv extends zzbt<Integer, Object> {

    /* renamed from: 连任  reason: contains not printable characters */
    public String f8805;

    /* renamed from: 靐  reason: contains not printable characters */
    public long f8806;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f8807;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f8808;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f8809;

    public zzbv() {
        this.f8809 = "E";
        this.f8806 = -1;
        this.f8808 = "E";
        this.f8807 = "E";
        this.f8805 = "E";
    }

    public zzbv(String str) {
        this();
        m10294(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<Integer, Object> m10297() {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.put(0, this.f8809);
        hashMap.put(4, this.f8805);
        hashMap.put(3, this.f8807);
        hashMap.put(2, this.f8808);
        hashMap.put(1, Long.valueOf(this.f8806));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10298(String str) {
        HashMap r2 = m10292(str);
        if (r2 != null) {
            this.f8809 = r2.get(0) == null ? "E" : (String) r2.get(0);
            this.f8806 = r2.get(1) == null ? -1 : ((Long) r2.get(1)).longValue();
            this.f8808 = r2.get(2) == null ? "E" : (String) r2.get(2);
            this.f8807 = r2.get(3) == null ? "E" : (String) r2.get(3);
            this.f8805 = r2.get(4) == null ? "E" : (String) r2.get(4);
        }
    }
}
