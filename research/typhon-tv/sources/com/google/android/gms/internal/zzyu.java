package com.google.android.gms.internal;

import com.Pinkamena;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

final class zzyu implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    final /* synthetic */ zzalf f10996;

    /* renamed from: 齉  reason: contains not printable characters */
    final /* synthetic */ zzyt f10997;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ JSONObject f10998;

    zzyu(zzyt zzyt, JSONObject jSONObject, zzalf zzalf) {
        this.f10997 = zzyt;
        this.f10998 = jSONObject;
        this.f10996 = zzalf;
    }

    public final void run() {
        try {
            zzanh r1 = this.f10997.m6070();
            r1.m5011(zzapa.m5221());
            this.f10997.f5598.zze(r1);
            WeakReference weakReference = new WeakReference(r1);
            r1.m4980().m5046(this.f10997.m6064((WeakReference<zzanh>) weakReference), this.f10997.m6062(weakReference));
            this.f10997.m6066(r1);
            r1.m4980().m5049((zzann) new zzyv(this, r1));
            r1.m4980().m5048((zzanm) new zzyw(this));
            String str = (String) zzkb.m5481().m5595(zznh.f4986);
            Pinkamena.DianePie();
        } catch (Exception e) {
            zzagf.m4796("Exception occurred while getting video view", e);
            this.f10996.m4822(null);
        }
    }
}
