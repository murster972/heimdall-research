package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzak;
import com.google.android.gms.ads.internal.zzv;

@zzzv
public final class zzsd {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzux f5352;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzv f5353;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzakd f5354;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f5355;

    zzsd(Context context, zzux zzux, zzakd zzakd, zzv zzv) {
        this.f5355 = context;
        this.f5352 = zzux;
        this.f5354 = zzakd;
        this.f5353 = zzv;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzak m5818(String str) {
        return new zzak(this.f5355.getApplicationContext(), new zzjn(), str, this.f5352, this.f5354, this.f5353);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzsd m5819() {
        return new zzsd(this.f5355.getApplicationContext(), this.f5352, this.f5354, this.f5353);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context m5820() {
        return this.f5355.getApplicationContext();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzak m5821(String str) {
        return new zzak(this.f5355, new zzjn(), str, this.f5352, this.f5354, this.f5353);
    }
}
