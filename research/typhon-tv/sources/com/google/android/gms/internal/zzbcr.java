package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.zzab;

public interface zzbcr extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    void m10052(String str) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m10053(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10054() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10055(double d, double d2, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10056(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10057(String str, LaunchOptions launchOptions) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10058(String str, String str2, long j) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10059(String str, String str2, zzab zzab) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10060(boolean z, double d, boolean z2) throws RemoteException;
}
