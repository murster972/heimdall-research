package com.google.android.gms.internal;

import java.util.concurrent.Executor;

final /* synthetic */ class zzakx implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Runnable f8288;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Executor f8289;

    zzakx(Executor executor, Runnable runnable) {
        this.f8289 = executor;
        this.f8288 = runnable;
    }

    public final void run() {
        this.f8289.execute(this.f8288);
    }
}
