package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzfn extends zzeu implements zzfl {
    zzfn(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.clearcut.IClearcut");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12935(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(7, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12936() throws RemoteException {
        m12298(3, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12937(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(6, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12938(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeString(str);
        m12298(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12939(IObjectWrapper iObjectWrapper, String str, String str2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeString(str);
        v_.writeString((String) null);
        m12298(8, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12940(byte[] bArr) throws RemoteException {
        Parcel v_ = v_();
        v_.writeByteArray(bArr);
        m12298(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12941(int[] iArr) throws RemoteException {
        Parcel v_ = v_();
        v_.writeIntArray((int[]) null);
        m12298(4, v_);
    }
}
