package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzvy implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10929;

    zzvy(zzvx zzvx) {
        this.f10929 = zzvx;
    }

    public final void run() {
        try {
            this.f10929.f5502.m13513();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClicked.", e);
        }
    }
}
