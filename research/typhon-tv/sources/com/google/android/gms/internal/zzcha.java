package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;

public final class zzcha extends zzbfm {
    public static final Parcelable.Creator<zzcha> CREATOR = new zzchb();

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzcgx f9200;

    /* renamed from: 麤  reason: contains not printable characters */
    public final long f9201;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f9202;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f9203;

    zzcha(zzcha zzcha, long j) {
        zzbq.m9120(zzcha);
        this.f9203 = zzcha.f9203;
        this.f9200 = zzcha.f9200;
        this.f9202 = zzcha.f9202;
        this.f9201 = j;
    }

    public zzcha(String str, zzcgx zzcgx, String str2, long j) {
        this.f9203 = str;
        this.f9200 = zzcgx;
        this.f9202 = str2;
        this.f9201 = j;
    }

    public final String toString() {
        String str = this.f9202;
        String str2 = this.f9203;
        String valueOf = String.valueOf(this.f9200);
        return new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("origin=").append(str).append(",name=").append(str2).append(",params=").append(valueOf).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f9203, false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f9200, i, false);
        zzbfp.m10193(parcel, 4, this.f9202, false);
        zzbfp.m10186(parcel, 5, this.f9201);
        zzbfp.m10182(parcel, r0);
    }
}
