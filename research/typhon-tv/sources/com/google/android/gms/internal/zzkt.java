package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;

public abstract class zzkt extends zzev implements zzks {
    public zzkt() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IAdManager");
    }

    public static zzks zzf(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        return queryLocalInterface instanceof zzks ? (zzks) queryLocalInterface : new zzku(iBinder);
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v22, types: [com.google.android.gms.internal.zzld] */
    /* JADX WARNING: type inference failed for: r0v27, types: [com.google.android.gms.internal.zzke] */
    /* JADX WARNING: type inference failed for: r0v43, types: [com.google.android.gms.internal.zzkx] */
    /* JADX WARNING: type inference failed for: r0v48, types: [com.google.android.gms.internal.zzkh] */
    /* JADX WARNING: type inference failed for: r0v56 */
    /* JADX WARNING: type inference failed for: r0v57 */
    /* JADX WARNING: type inference failed for: r0v58 */
    /* JADX WARNING: type inference failed for: r0v59 */
    /* JADX WARNING: type inference failed for: r0v60 */
    /* JADX WARNING: type inference failed for: r0v61 */
    /* JADX WARNING: type inference failed for: r0v62 */
    /* JADX WARNING: type inference failed for: r0v63 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r5, android.os.Parcel r6, android.os.Parcel r7, int r8) throws android.os.RemoteException {
        /*
            r4 = this;
            r1 = 1
            r0 = 0
            boolean r2 = r4.zza(r5, r6, r7, r8)
            if (r2 == 0) goto L_0x000a
            r0 = r1
        L_0x0009:
            return r0
        L_0x000a:
            switch(r5) {
                case 1: goto L_0x000f;
                case 2: goto L_0x001b;
                case 3: goto L_0x0022;
                case 4: goto L_0x002d;
                case 5: goto L_0x0040;
                case 6: goto L_0x0047;
                case 7: goto L_0x004e;
                case 8: goto L_0x006f;
                case 9: goto L_0x0090;
                case 10: goto L_0x0097;
                case 11: goto L_0x009f;
                case 12: goto L_0x00a7;
                case 13: goto L_0x00b3;
                case 14: goto L_0x00c3;
                case 15: goto L_0x00d3;
                case 16: goto L_0x000d;
                case 17: goto L_0x000d;
                case 18: goto L_0x00e7;
                case 19: goto L_0x00f3;
                case 20: goto L_0x0103;
                case 21: goto L_0x0125;
                case 22: goto L_0x0147;
                case 23: goto L_0x0153;
                case 24: goto L_0x015f;
                case 25: goto L_0x016f;
                case 26: goto L_0x017b;
                case 27: goto L_0x000d;
                case 28: goto L_0x000d;
                case 29: goto L_0x0187;
                case 30: goto L_0x0197;
                case 31: goto L_0x01a7;
                case 32: goto L_0x01b3;
                case 33: goto L_0x01bf;
                case 34: goto L_0x01cb;
                case 35: goto L_0x01d7;
                default: goto L_0x000d;
            }
        L_0x000d:
            r0 = 0
            goto L_0x0009
        L_0x000f:
            com.google.android.gms.dynamic.IObjectWrapper r0 = r4.zzbr()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12305((android.os.Parcel) r7, (android.os.IInterface) r0)
        L_0x0019:
            r0 = r1
            goto L_0x0009
        L_0x001b:
            r4.destroy()
            r7.writeNoException()
            goto L_0x0019
        L_0x0022:
            boolean r0 = r4.isReady()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12307((android.os.Parcel) r7, (boolean) r0)
            goto L_0x0019
        L_0x002d:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzjj> r0 = com.google.android.gms.internal.zzjj.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.internal.zzjj r0 = (com.google.android.gms.internal.zzjj) r0
            boolean r0 = r4.zzb(r0)
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12307((android.os.Parcel) r7, (boolean) r0)
            goto L_0x0019
        L_0x0040:
            r4.pause()
            r7.writeNoException()
            goto L_0x0019
        L_0x0047:
            r4.resume()
            r7.writeNoException()
            goto L_0x0019
        L_0x004e:
            android.os.IBinder r2 = r6.readStrongBinder()
            if (r2 != 0) goto L_0x005b
        L_0x0054:
            r4.zza((com.google.android.gms.internal.zzkh) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x005b:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.IAdListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r3 = r0 instanceof com.google.android.gms.internal.zzkh
            if (r3 == 0) goto L_0x0069
            com.google.android.gms.internal.zzkh r0 = (com.google.android.gms.internal.zzkh) r0
            goto L_0x0054
        L_0x0069:
            com.google.android.gms.internal.zzkj r0 = new com.google.android.gms.internal.zzkj
            r0.<init>(r2)
            goto L_0x0054
        L_0x006f:
            android.os.IBinder r2 = r6.readStrongBinder()
            if (r2 != 0) goto L_0x007c
        L_0x0075:
            r4.zza((com.google.android.gms.internal.zzkx) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x007c:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.IAppEventListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r3 = r0 instanceof com.google.android.gms.internal.zzkx
            if (r3 == 0) goto L_0x008a
            com.google.android.gms.internal.zzkx r0 = (com.google.android.gms.internal.zzkx) r0
            goto L_0x0075
        L_0x008a:
            com.google.android.gms.internal.zzkz r0 = new com.google.android.gms.internal.zzkz
            r0.<init>(r2)
            goto L_0x0075
        L_0x0090:
            com.Pinkamena.DianePie()
            r7.writeNoException()
            goto L_0x0019
        L_0x0097:
            r4.stopLoading()
            r7.writeNoException()
            goto L_0x0019
        L_0x009f:
            r4.zzbu()
            r7.writeNoException()
            goto L_0x0019
        L_0x00a7:
            com.google.android.gms.internal.zzjn r0 = r4.zzbs()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12302(r7, r0)
            goto L_0x0019
        L_0x00b3:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzjn> r0 = com.google.android.gms.internal.zzjn.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.internal.zzjn r0 = (com.google.android.gms.internal.zzjn) r0
            r4.zza((com.google.android.gms.internal.zzjn) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x00c3:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzxl r0 = com.google.android.gms.internal.zzxm.m13635(r0)
            r4.zza((com.google.android.gms.internal.zzxl) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x00d3:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzxr r0 = com.google.android.gms.internal.zzxs.m13637(r0)
            java.lang.String r2 = r6.readString()
            r4.zza(r0, r2)
            r7.writeNoException()
            goto L_0x0019
        L_0x00e7:
            java.lang.String r0 = r4.getMediationAdapterClassName()
            r7.writeNoException()
            r7.writeString(r0)
            goto L_0x0019
        L_0x00f3:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzoa r0 = com.google.android.gms.internal.zzob.m13178(r0)
            r4.zza((com.google.android.gms.internal.zzoa) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0103:
            android.os.IBinder r2 = r6.readStrongBinder()
            if (r2 != 0) goto L_0x0111
        L_0x0109:
            r4.zza((com.google.android.gms.internal.zzke) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0111:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.IAdClickListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r3 = r0 instanceof com.google.android.gms.internal.zzke
            if (r3 == 0) goto L_0x011f
            com.google.android.gms.internal.zzke r0 = (com.google.android.gms.internal.zzke) r0
            goto L_0x0109
        L_0x011f:
            com.google.android.gms.internal.zzkg r0 = new com.google.android.gms.internal.zzkg
            r0.<init>(r2)
            goto L_0x0109
        L_0x0125:
            android.os.IBinder r2 = r6.readStrongBinder()
            if (r2 != 0) goto L_0x0133
        L_0x012b:
            r4.zza((com.google.android.gms.internal.zzld) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0133:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.ICorrelationIdProvider"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r3 = r0 instanceof com.google.android.gms.internal.zzld
            if (r3 == 0) goto L_0x0141
            com.google.android.gms.internal.zzld r0 = (com.google.android.gms.internal.zzld) r0
            goto L_0x012b
        L_0x0141:
            com.google.android.gms.internal.zzlf r0 = new com.google.android.gms.internal.zzlf
            r0.<init>(r2)
            goto L_0x012b
        L_0x0147:
            boolean r0 = com.google.android.gms.internal.zzew.m12308(r6)
            r4.setManualImpressionsEnabled(r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0153:
            boolean r0 = r4.isLoading()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12307((android.os.Parcel) r7, (boolean) r0)
            goto L_0x0019
        L_0x015f:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzadp r0 = com.google.android.gms.internal.zzadq.m9505(r0)
            r4.zza((com.google.android.gms.internal.zzadp) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x016f:
            java.lang.String r0 = r6.readString()
            r4.setUserId(r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x017b:
            com.google.android.gms.internal.zzll r0 = r4.getVideoController()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12305((android.os.Parcel) r7, (android.os.IInterface) r0)
            goto L_0x0019
        L_0x0187:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzmr> r0 = com.google.android.gms.internal.zzmr.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.internal.zzmr r0 = (com.google.android.gms.internal.zzmr) r0
            r4.zza((com.google.android.gms.internal.zzmr) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0197:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzlr> r0 = com.google.android.gms.internal.zzlr.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.internal.zzlr r0 = (com.google.android.gms.internal.zzlr) r0
            r4.zza((com.google.android.gms.internal.zzlr) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x01a7:
            java.lang.String r0 = r4.getAdUnitId()
            r7.writeNoException()
            r7.writeString(r0)
            goto L_0x0019
        L_0x01b3:
            com.google.android.gms.internal.zzkx r0 = r4.zzcd()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12305((android.os.Parcel) r7, (android.os.IInterface) r0)
            goto L_0x0019
        L_0x01bf:
            com.google.android.gms.internal.zzkh r0 = r4.zzce()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12305((android.os.Parcel) r7, (android.os.IInterface) r0)
            goto L_0x0019
        L_0x01cb:
            boolean r0 = com.google.android.gms.internal.zzew.m12308(r6)
            r4.setImmersiveMode(r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x01d7:
            java.lang.String r0 = r4.zzcp()
            r7.writeNoException()
            r7.writeString(r0)
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkt.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
