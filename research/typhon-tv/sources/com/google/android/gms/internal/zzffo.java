package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;

final class zzffo extends zzffn<Object> {
    zzffo() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzffq<Object> m12503(Object obj) {
        return ((zzffu.zzd) obj).f10402;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12504(Class<?> cls) {
        return zzffu.zzd.class.isAssignableFrom(cls);
    }
}
