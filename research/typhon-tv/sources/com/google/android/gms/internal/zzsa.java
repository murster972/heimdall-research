package com.google.android.gms.internal;

import android.os.RemoteException;

final /* synthetic */ class zzsa implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzrq f10865;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzrr f10866;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzalf f10867;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzrz f10868;

    zzsa(zzrz zzrz, zzrq zzrq, zzalf zzalf, zzrr zzrr) {
        this.f10868 = zzrz;
        this.f10865 = zzrq;
        this.f10867 = zzalf;
        this.f10866 = zzrr;
    }

    public final void run() {
        zzrz zzrz = this.f10868;
        zzrq zzrq = this.f10865;
        zzalf zzalf = this.f10867;
        try {
            zzalf.m4822(zzrq.m5806().m13382(this.f10866));
        } catch (RemoteException e) {
            zzagf.m4793("Unable to obtain a cache service instance.", e);
            zzalf.m4824(e);
            zzrz.f10861.m5814();
        }
    }
}
