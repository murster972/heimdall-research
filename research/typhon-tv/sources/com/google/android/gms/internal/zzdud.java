package com.google.android.gms.internal;

import java.nio.ByteBuffer;

final class zzdud extends zzdue {
    private zzdud(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final zzduf m12186(byte[] bArr) {
        return new zzduf(this, bArr, 1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12187(int[] iArr) {
        iArr[12] = iArr[12] + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12188() {
        return 12;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int[] m12189(byte[] bArr, int i) {
        int[] iArr = new int[16];
        zzdue.m12190(iArr);
        zzdue.m12191(iArr, this.f10181.m12228());
        iArr[12] = i;
        System.arraycopy(m12177(ByteBuffer.wrap(bArr)), 0, iArr, 13, 3);
        return iArr;
    }
}
