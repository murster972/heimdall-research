package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzuq {
    /* renamed from: 龘  reason: contains not printable characters */
    private static String m5894(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        return str.replaceAll(str2, str3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m5895(JSONObject jSONObject, String str) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(optJSONArray.length());
        for (int i = 0; i < optJSONArray.length(); i++) {
            arrayList.add(optJSONArray.getString(i));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m5896(Context context, String str, zzafo zzafo, String str2, boolean z, List<String> list) {
        if (list != null && !list.isEmpty()) {
            String str3 = z ? PubnativeRequest.LEGACY_ZONE_ID : "0";
            for (String r0 : list) {
                String r02 = m5894(m5894(m5894(m5894(m5894(m5894(m5894(r0, "@gw_adlocid@", str2), "@gw_adnetrefresh@", str3), "@gw_qdata@", zzafo.f4101.f5423), "@gw_sdkver@", str), "@gw_sessid@", zzkb.m5486()), "@gw_seqnum@", zzafo.f4105), "@gw_adnetstatus@", zzafo.f4103);
                if (zzafo.f4096 != null) {
                    r02 = m5894(m5894(r02, "@gw_adnetid@", zzafo.f4096.f5415), "@gw_allocid@", zzafo.f4096.f5416);
                }
                String r03 = zzafi.m4437(r02, context);
                zzbs.zzei();
                zzahn.m4582(context, str, r03);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m5897(Context context, String str, List<String> list, String str2, zzaeq zzaeq) {
        if (list != null && !list.isEmpty()) {
            long r2 = zzbs.zzeo().m9243();
            for (String r0 : list) {
                String r02 = m5894(m5894(r0, "@gw_rwd_userid@", str2), "@gw_tmstmp@", Long.toString(r2));
                if (zzaeq != null) {
                    r02 = m5894(m5894(r02, "@gw_rwd_itm@", zzaeq.f4056), "@gw_rwd_amt@", Integer.toString(zzaeq.f4055));
                }
                zzbs.zzei();
                zzahn.m4582(context, str, r02);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m5898(String str, int[] iArr) {
        if (TextUtils.isEmpty(str) || iArr.length != 2) {
            return false;
        }
        String[] split = str.split("x");
        if (split.length != 2) {
            return false;
        }
        try {
            iArr[0] = Integer.parseInt(split[0]);
            iArr[1] = Integer.parseInt(split[1]);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
