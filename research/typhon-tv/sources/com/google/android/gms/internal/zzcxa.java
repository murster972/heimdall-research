package com.google.android.gms.internal;

import android.support.v4.app.NotificationCompat;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;

public final class zzcxa {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Scope f9816 = new Scope("profile");

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Scope f9817 = new Scope(NotificationCompat.CATEGORY_EMAIL);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static Api<Object> f9818 = new Api<>("SignIn.INTERNAL_API", f9819, f9821);

    /* renamed from: 连任  reason: contains not printable characters */
    private static Api.zza<zzcxn, Object> f9819 = new zzcxc();

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Api<zzcxe> f9820 = new Api<>("SignIn.API", f9823, f9822);

    /* renamed from: 麤  reason: contains not printable characters */
    private static Api.zzf<zzcxn> f9821 = new Api.zzf<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static Api.zzf<zzcxn> f9822 = new Api.zzf<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Api.zza<zzcxn, zzcxe> f9823 = new zzcxb();
}
