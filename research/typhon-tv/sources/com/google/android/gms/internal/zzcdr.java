package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

final class zzcdr extends zzcds {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ PendingIntent f9034;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcdr(zzcdp zzcdp, GoogleApiClient googleApiClient, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.f9034 = pendingIntent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10330(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10406(this.f9034);
        m4198(Status.f7464);
    }
}
