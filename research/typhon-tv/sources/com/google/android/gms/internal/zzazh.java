package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.R;
import com.google.android.gms.cast.framework.IntroductoryOverlay;

public final class zzazh extends RelativeLayout implements IntroductoryOverlay {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzazk f8487;

    /* renamed from: 连任  reason: contains not printable characters */
    private IntroductoryOverlay.OnOverlayDismissedListener f8488;

    /* renamed from: 靐  reason: contains not printable characters */
    private Activity f8489;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f8490;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f8491;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f8492;

    public zzazh(IntroductoryOverlay.Builder builder) {
        this(builder, (AttributeSet) null, R.attr.castIntroOverlayStyle);
    }

    private zzazh(IntroductoryOverlay.Builder builder, AttributeSet attributeSet, int i) {
        super(builder.m8044(), (AttributeSet) null, i);
        this.f8489 = builder.m8044();
        this.f8492 = builder.m8038();
        this.f8488 = builder.m8046();
        TypedArray obtainStyledAttributes = this.f8489.getTheme().obtainStyledAttributes((AttributeSet) null, R.styleable.CastIntroOverlay, i, R.style.CastIntroOverlay);
        if (builder.m8047() != null) {
            Rect rect = new Rect();
            builder.m8047().getGlobalVisibleRect(rect);
            this.f8487 = new zzazk((zzazi) null);
            this.f8487.f8498 = rect.centerX();
            this.f8487.f8495 = rect.centerY();
            zzazk zzazk = this.f8487;
            PorterDuffXfermode porterDuffXfermode = new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY);
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setAlpha(0);
            paint.setXfermode(porterDuffXfermode);
            paint.setAntiAlias(true);
            zzazk.f8497 = paint;
            this.f8487.f8496 = builder.m8041();
            if (this.f8487.f8496 == 0.0f) {
                this.f8487.f8496 = obtainStyledAttributes.getDimension(R.styleable.CastIntroOverlay_castFocusRadius, 0.0f);
            }
        } else {
            this.f8487 = null;
        }
        LayoutInflater.from(this.f8489).inflate(R.layout.cast_intro_overlay, this);
        this.f8491 = builder.m8043();
        if (this.f8491 == 0) {
            this.f8491 = obtainStyledAttributes.getColor(R.styleable.CastIntroOverlay_castBackgroundColor, Color.argb(0, 0, 0, 0));
        }
        TextView textView = (TextView) findViewById(R.id.textTitle);
        if (!TextUtils.isEmpty(builder.m8039())) {
            textView.setText(builder.m8039());
            int resourceId = obtainStyledAttributes.getResourceId(R.styleable.CastIntroOverlay_castTitleTextAppearance, 0);
            if (resourceId != 0) {
                textView.setTextAppearance(this.f8489, resourceId);
            }
        }
        String r0 = builder.m8040();
        String string = TextUtils.isEmpty(r0) ? obtainStyledAttributes.getString(R.styleable.CastIntroOverlay_castButtonText) : r0;
        int color = obtainStyledAttributes.getColor(R.styleable.CastIntroOverlay_castButtonBackgroundColor, Color.argb(0, 0, 0, 0));
        Button button = (Button) findViewById(R.id.button);
        button.setText(string);
        button.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        int resourceId2 = obtainStyledAttributes.getResourceId(R.styleable.CastIntroOverlay_castButtonTextAppearance, 0);
        if (resourceId2 != 0) {
            button.setTextAppearance(this.f8489, resourceId2);
        }
        button.setOnClickListener(new zzazi(this));
        obtainStyledAttributes.recycle();
        setFitsSystemWindows(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9814() {
        IntroductoryOverlay.zza.m8052(this.f8489);
        if (this.f8488 != null) {
            this.f8488.m8050();
            this.f8488 = null;
        }
        m9816();
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(createBitmap);
        canvas2.drawColor(this.f8491);
        if (this.f8487 != null) {
            canvas2.drawCircle((float) this.f8487.f8498, (float) this.f8487.f8495, this.f8487.f8496, this.f8487.f8497);
        }
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, (Paint) null);
        createBitmap.recycle();
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        if (this.f8489 != null) {
            this.f8489 = null;
        }
        super.onDetachedFromWindow();
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9816() {
        if (this.f8489 != null) {
            ((ViewGroup) this.f8489.getWindow().getDecorView()).removeView(this);
            this.f8489 = null;
        }
        this.f8488 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9817() {
        if (this.f8489 == null || zzazd.m9809((Context) this.f8489)) {
            return;
        }
        if (this.f8492 && IntroductoryOverlay.zza.m8051(this.f8489)) {
            this.f8489 = null;
            this.f8488 = null;
        } else if (!this.f8490) {
            this.f8490 = true;
            ((ViewGroup) this.f8489.getWindow().getDecorView()).addView(this);
        }
    }
}
