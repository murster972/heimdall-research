package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.VideoOptions;

@zzzv
public final class zzmr extends zzbfm {
    public static final Parcelable.Creator<zzmr> CREATOR = new zzms();

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f4877;

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean f4878;

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean f4879;

    public zzmr(VideoOptions videoOptions) {
        this(videoOptions.getStartMuted(), videoOptions.getCustomControlsRequested(), videoOptions.getClickToExpandRequested());
    }

    public zzmr(boolean z, boolean z2, boolean z3) {
        this.f4879 = z;
        this.f4877 = z2;
        this.f4878 = z3;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 2, this.f4879);
        zzbfp.m10195(parcel, 3, this.f4877);
        zzbfp.m10195(parcel, 4, this.f4878);
        zzbfp.m10182(parcel, r0);
    }
}
