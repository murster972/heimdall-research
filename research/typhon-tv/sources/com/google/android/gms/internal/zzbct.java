package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;

public interface zzbct extends IInterface {
    /* renamed from: 连任  reason: contains not printable characters */
    void m10070(int i) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m10071(int i) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m10072(int i) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m10073(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10074(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10075(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10076(zzbbt zzbbt) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10077(zzbcn zzbcn) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10078(String str, double d, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10079(String str, long j) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10080(String str, long j, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10081(String str, String str2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10082(String str, byte[] bArr) throws RemoteException;
}
