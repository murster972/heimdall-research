package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$zzb;

public final class zzclf extends zzcjl {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f9630 = m11105().m9241();

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzcgs f9631 = new zzclh(this, this.f9487);

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzcgs f9632 = new zzclg(this, this.f9487);

    /* renamed from: 龘  reason: contains not printable characters */
    private Handler f9633;

    zzclf(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final void m11299() {
        synchronized (this) {
            if (this.f9633 == null) {
                this.f9633 = new Handler(Looper.getMainLooper());
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final void m11300() {
        m11109();
        m11327(false);
        m11108().m10456(m11105().m9241());
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11301(long j) {
        m11109();
        m11299();
        this.f9632.m10624();
        this.f9631.m10624();
        m11096().m10848().m10850("Activity paused, time", Long.valueOf(j));
        if (this.f9630 != 0) {
            m11098().f9305.m10903(m11098().f9305.m10902() + (j - this.f9630));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11303(long j) {
        m11109();
        m11299();
        this.f9632.m10624();
        this.f9631.m10624();
        m11096().m10848().m10850("Activity resumed, time", Long.valueOf(j));
        this.f9630 = j;
        if (m11105().m9243() - m11098().f9316.m10902() > m11098().f9304.m10902()) {
            m11098().f9307.m10899(true);
            m11098().f9305.m10903(0);
        }
        if (m11098().f9307.m10900()) {
            this.f9632.m10626(Math.max(0, m11098().f9315.m10902() - m11098().f9305.m10902()));
        } else {
            this.f9631.m10626(Math.max(0, 3600000 - m11098().f9305.m10902()));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m11306() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m11307() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m11308() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m11309() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m11310() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m11311() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m11312() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m11313() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m11314() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m11315() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m11316() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m11317() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m11318() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m11319() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m11320() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m11321() {
        return super.m11105();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m11322() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11323() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m11324() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11325() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11326() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11327(boolean z) {
        m11109();
        m11115();
        long r0 = m11105().m9241();
        m11098().f9304.m10903(m11105().m9243());
        long j = r0 - this.f9630;
        if (z || j >= 1000) {
            m11098().f9305.m10903(j);
            m11096().m10848().m10850("Recording user engagement, ms", Long.valueOf(j));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j);
            zzckc.m11187((AppMeasurement$zzb) m11104().m11204(), bundle);
            m11091().m11174("auto", "_e", bundle);
            this.f9630 = r0;
            this.f9631.m10624();
            this.f9631.m10626(Math.max(0, 3600000 - m11098().f9305.m10902()));
            return true;
        }
        m11096().m10848().m10850("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j));
        return false;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m11328() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m11329() {
        return super.m11112();
    }
}
