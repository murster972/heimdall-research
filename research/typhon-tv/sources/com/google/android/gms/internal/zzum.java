package com.google.android.gms.internal;

final class zzum implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzul f10909;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzuk f10910;

    zzum(zzul zzul, zzuk zzuk) {
        this.f10909 = zzul;
        this.f10910 = zzuk;
    }

    public final void run() {
        synchronized (zzul.龘(this.f10909)) {
            if (zzul.靐(this.f10909) == -2) {
                zzul.龘(this.f10909, zzul.齉(this.f10909));
                if (zzul.麤(this.f10909) == null) {
                    this.f10909.龘(4);
                } else if (!zzul.连任(this.f10909) || zzul.龘(this.f10909, 1)) {
                    this.f10910.m5891((zzup) this.f10909);
                    zzul.龘(this.f10909, this.f10910);
                } else {
                    String r0 = zzul.ʻ(this.f10909);
                    zzagf.m4791(new StringBuilder(String.valueOf(r0).length() + 56).append("Ignoring adapter ").append(r0).append(" as delayed impression is not supported").toString());
                    this.f10909.龘(2);
                }
            }
        }
    }
}
