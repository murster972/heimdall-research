package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzaey extends zzbfm {
    public static final Parcelable.Creator<zzaey> CREATOR = new zzaez();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f4069;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean f4070;

    /* renamed from: 连任  reason: contains not printable characters */
    public final List<String> f4071;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f4072;

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean f4073;

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean f4074;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f4075;

    public zzaey(String str, String str2, boolean z, boolean z2, List<String> list, boolean z3, boolean z4) {
        this.f4075 = str;
        this.f4072 = str2;
        this.f4074 = z;
        this.f4073 = z2;
        this.f4071 = list;
        this.f4069 = z3;
        this.f4070 = z4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzaey m4403(JSONObject jSONObject) throws JSONException {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("click_string", "");
        String optString2 = jSONObject.optString("report_url", "");
        boolean optBoolean = jSONObject.optBoolean("rendered_ad_enabled", false);
        boolean optBoolean2 = jSONObject.optBoolean("non_malicious_reporting_enabled", false);
        JSONArray optJSONArray = jSONObject.optJSONArray("allowed_headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); i++) {
            String optString3 = optJSONArray.optString(i);
            if (!TextUtils.isEmpty(optString3)) {
                arrayList.add(optString3.toLowerCase(Locale.ENGLISH));
            }
        }
        return new zzaey(optString, optString2, optBoolean, optBoolean2, arrayList, jSONObject.optBoolean("protection_enabled", false), jSONObject.optBoolean("malicious_reporting_enabled", false));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f4075, false);
        zzbfp.m10193(parcel, 3, this.f4072, false);
        zzbfp.m10195(parcel, 4, this.f4074);
        zzbfp.m10195(parcel, 5, this.f4073);
        zzbfp.m10178(parcel, 6, this.f4071, false);
        zzbfp.m10195(parcel, 7, this.f4069);
        zzbfp.m10195(parcel, 8, this.f4070);
        zzbfp.m10182(parcel, r0);
    }
}
