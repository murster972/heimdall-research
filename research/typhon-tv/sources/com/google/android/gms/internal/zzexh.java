package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;

public final class zzexh {

    /* renamed from: 连任  reason: contains not printable characters */
    private long f10310;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f10311;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f10312;

    /* renamed from: 齉  reason: contains not printable characters */
    private Map<String, zzexb> f10313;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f10314;

    public zzexh() {
        this(-1);
    }

    private zzexh(int i, long j, Map<String, zzexb> map, boolean z) {
        this(0, -1, (Map<String, zzexb>) null, false, -1);
    }

    private zzexh(int i, long j, Map<String, zzexb> map, boolean z, long j2) {
        this.f10314 = 0;
        this.f10311 = j;
        this.f10313 = new HashMap();
        this.f10312 = false;
        this.f10310 = -1;
    }

    private zzexh(long j) {
        this(0, -1, (Map<String, zzexb>) null, false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12325(long j) {
        this.f10310 = j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12326() {
        return this.f10312;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final long m12327() {
        return this.f10310;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Map<String, zzexb> m12328() {
        return this.f10313;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12329() {
        return this.f10314;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12330(int i) {
        this.f10314 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12331(long j) {
        this.f10311 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12332(String str) {
        if (this.f10313.get(str) != null) {
            this.f10313.remove(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12333(Map<String, zzexb> map) {
        if (map == null) {
            map = new HashMap<>();
        }
        this.f10313 = map;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12334(boolean z) {
        this.f10312 = z;
    }
}
