package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public final class zzim implements Parcelable.Creator<zzil> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        boolean z = false;
        long j = 0;
        Bundle bundle = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j2 = 0;
        String str4 = null;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 8:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new zzil(str4, j2, str3, str2, str, bundle, z, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzil[i];
    }
}
