package com.google.android.gms.internal;

import android.content.Context;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class zzexd implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private zzexh f10296;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzexe f10297;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzexe f10298;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzexe f10299;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f10300;

    public zzexd(Context context, zzexe zzexe, zzexe zzexe2, zzexe zzexe3, zzexh zzexh) {
        this.f10300 = context;
        this.f10297 = zzexe;
        this.f10299 = zzexe2;
        this.f10298 = zzexe3;
        this.f10296 = zzexh;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzexi m12311(zzexe zzexe) {
        zzexi zzexi = new zzexi();
        if (zzexe.m12316() != null) {
            Map<String, Map<String, byte[]>> r4 = zzexe.m12316();
            ArrayList arrayList = new ArrayList();
            for (String next : r4.keySet()) {
                ArrayList arrayList2 = new ArrayList();
                Map map = r4.get(next);
                for (String str : map.keySet()) {
                    zzexj zzexj = new zzexj();
                    zzexj.f10320 = str;
                    zzexj.f10319 = (byte[]) map.get(str);
                    arrayList2.add(zzexj);
                }
                zzexl zzexl = new zzexl();
                zzexl.f10326 = next;
                zzexl.f10325 = (zzexj[]) arrayList2.toArray(new zzexj[arrayList2.size()]);
                arrayList.add(zzexl);
            }
            zzexi.f10317 = (zzexl[]) arrayList.toArray(new zzexl[arrayList.size()]);
        }
        if (zzexe.m12312() != null) {
            List<byte[]> r0 = zzexe.m12312();
            zzexi.f10316 = (byte[][]) r0.toArray(new byte[r0.size()][]);
        }
        zzexi.f10315 = zzexe.m12314();
        return zzexi;
    }

    public final void run() {
        zzexm zzexm = new zzexm();
        if (this.f10297 != null) {
            zzexm.f10331 = m12311(this.f10297);
        }
        if (this.f10299 != null) {
            zzexm.f10328 = m12311(this.f10299);
        }
        if (this.f10298 != null) {
            zzexm.f10330 = m12311(this.f10298);
        }
        if (this.f10296 != null) {
            zzexk zzexk = new zzexk();
            zzexk.f10323 = this.f10296.m12329();
            zzexk.f10321 = this.f10296.m12326();
            zzexk.f10322 = this.f10296.m12327();
            zzexm.f10329 = zzexk;
        }
        if (!(this.f10296 == null || this.f10296.m12328() == null)) {
            ArrayList arrayList = new ArrayList();
            Map<String, zzexb> r4 = this.f10296.m12328();
            for (String next : r4.keySet()) {
                if (r4.get(next) != null) {
                    zzexn zzexn = new zzexn();
                    zzexn.f10334 = next;
                    zzexn.f10333 = r4.get(next).m12309();
                    zzexn.f10335 = r4.get(next).m12310();
                    arrayList.add(zzexn);
                }
            }
            zzexm.f10327 = (zzexn[]) arrayList.toArray(new zzexn[arrayList.size()]);
        }
        byte[] r0 = zzfjs.m12871((zzfjs) zzexm);
        try {
            FileOutputStream openFileOutput = this.f10300.openFileOutput("persisted_config", 0);
            openFileOutput.write(r0);
            openFileOutput.close();
        } catch (IOException e) {
            Log.e("AsyncPersisterTask", "Could not persist config.", e);
        }
    }
}
