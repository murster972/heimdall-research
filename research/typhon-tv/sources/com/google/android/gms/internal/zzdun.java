package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.EllipticCurve;
import javax.crypto.KeyAgreement;

public final class zzdun {

    /* renamed from: 龘  reason: contains not printable characters */
    private ECPublicKey f10203;

    public zzdun(ECPublicKey eCPublicKey) {
        this.f10203 = eCPublicKey;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzduo m12207(String str, byte[] bArr, byte[] bArr2, int i, zzdus zzdus) throws GeneralSecurityException {
        byte[] bArr3;
        ECParameterSpec params = this.f10203.getParams();
        KeyPairGenerator r0 = zzduu.f10219.m12217("EC");
        r0.initialize(params);
        KeyPair generateKeyPair = r0.generateKeyPair();
        ECPublicKey eCPublicKey = (ECPublicKey) generateKeyPair.getPublic();
        zzdup.m12214(this.f10203.getW(), this.f10203.getParams().getCurve());
        KeyAgreement r2 = zzduu.f10220.m12217("ECDH");
        r2.init((ECPrivateKey) generateKeyPair.getPrivate());
        r2.doPhase(this.f10203, true);
        byte[] generateSecret = r2.generateSecret();
        EllipticCurve curve = eCPublicKey.getParams().getCurve();
        ECPoint w = eCPublicKey.getW();
        zzdup.m12214(w, curve);
        int r5 = zzdup.m12211(curve);
        switch (zzduq.f10207[zzdus.ordinal()]) {
            case 1:
                bArr3 = new byte[((r5 * 2) + 1)];
                byte[] byteArray = w.getAffineX().toByteArray();
                byte[] byteArray2 = w.getAffineY().toByteArray();
                System.arraycopy(byteArray2, 0, bArr3, ((r5 * 2) + 1) - byteArray2.length, byteArray2.length);
                System.arraycopy(byteArray, 0, bArr3, (r5 + 1) - byteArray.length, byteArray.length);
                bArr3[0] = 4;
                break;
            case 2:
                byte[] bArr4 = new byte[(r5 + 1)];
                byte[] byteArray3 = w.getAffineX().toByteArray();
                System.arraycopy(byteArray3, 0, bArr4, (r5 + 1) - byteArray3.length, byteArray3.length);
                bArr4[0] = (byte) (w.getAffineY().testBit(0) ? 3 : 2);
                bArr3 = bArr4;
                break;
            default:
                String valueOf = String.valueOf(zzdus);
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(valueOf).length() + 15).append("invalid format:").append(valueOf).toString());
        }
        return new zzduo(bArr3, zzdvd.m12226(str, zzdua.m12173(bArr3, generateSecret), bArr, bArr2, i));
    }
}
