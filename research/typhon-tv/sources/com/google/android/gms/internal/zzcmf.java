package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcmf extends zzfjm<zzcmf> {

    /* renamed from: 靐  reason: contains not printable characters */
    public long[] f9760 = zzfjv.f10560;

    /* renamed from: 龘  reason: contains not printable characters */
    public long[] f9761 = zzfjv.f10560;

    public zzcmf() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcmf)) {
            return false;
        }
        zzcmf zzcmf = (zzcmf) obj;
        if (!zzfjq.m12862(this.f9761, zzcmf.f9761)) {
            return false;
        }
        if (!zzfjq.m12862(this.f9760, zzcmf.f9760)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcmf.f10533 == null || zzcmf.f10533.m12849() : this.f10533.equals(zzcmf.f10533);
    }

    public final int hashCode() {
        return ((this.f10533 == null || this.f10533.m12849()) ? 0 : this.f10533.hashCode()) + ((((((getClass().getName().hashCode() + 527) * 31) + zzfjq.m12858(this.f9761)) * 31) + zzfjq.m12858(this.f9760)) * 31);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11504() {
        int i;
        int r3 = super.m12841();
        if (this.f9761 == null || this.f9761.length <= 0) {
            i = r3;
        } else {
            int i2 = 0;
            for (long r4 : this.f9761) {
                i2 += zzfjk.m12817(r4);
            }
            i = r3 + i2 + (this.f9761.length * 1);
        }
        if (this.f9760 == null || this.f9760.length <= 0) {
            return i;
        }
        int i3 = 0;
        for (long r42 : this.f9760) {
            i3 += zzfjk.m12817(r42);
        }
        return i + i3 + (this.f9760.length * 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11505(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    int r2 = zzfjv.m12883(zzfjj, 8);
                    int length = this.f9761 == null ? 0 : this.f9761.length;
                    long[] jArr = new long[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9761, 0, jArr, 0, length);
                    }
                    while (length < jArr.length - 1) {
                        jArr[length] = zzfjj.m12786();
                        zzfjj.m12800();
                        length++;
                    }
                    jArr[length] = zzfjj.m12786();
                    this.f9761 = jArr;
                    continue;
                case 10:
                    int r3 = zzfjj.m12799(zzfjj.m12785());
                    int r22 = zzfjj.m12787();
                    int i = 0;
                    while (zzfjj.m12790() > 0) {
                        zzfjj.m12786();
                        i++;
                    }
                    zzfjj.m12792(r22);
                    int length2 = this.f9761 == null ? 0 : this.f9761.length;
                    long[] jArr2 = new long[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f9761, 0, jArr2, 0, length2);
                    }
                    while (length2 < jArr2.length) {
                        jArr2[length2] = zzfjj.m12786();
                        length2++;
                    }
                    this.f9761 = jArr2;
                    zzfjj.m12796(r3);
                    continue;
                case 16:
                    int r23 = zzfjv.m12883(zzfjj, 16);
                    int length3 = this.f9760 == null ? 0 : this.f9760.length;
                    long[] jArr3 = new long[(r23 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.f9760, 0, jArr3, 0, length3);
                    }
                    while (length3 < jArr3.length - 1) {
                        jArr3[length3] = zzfjj.m12786();
                        zzfjj.m12800();
                        length3++;
                    }
                    jArr3[length3] = zzfjj.m12786();
                    this.f9760 = jArr3;
                    continue;
                case 18:
                    int r32 = zzfjj.m12799(zzfjj.m12785());
                    int r24 = zzfjj.m12787();
                    int i2 = 0;
                    while (zzfjj.m12790() > 0) {
                        zzfjj.m12786();
                        i2++;
                    }
                    zzfjj.m12792(r24);
                    int length4 = this.f9760 == null ? 0 : this.f9760.length;
                    long[] jArr4 = new long[(i2 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.f9760, 0, jArr4, 0, length4);
                    }
                    while (length4 < jArr4.length) {
                        jArr4[length4] = zzfjj.m12786();
                        length4++;
                    }
                    this.f9760 = jArr4;
                    zzfjj.m12796(r32);
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11506(zzfjk zzfjk) throws IOException {
        if (this.f9761 != null && this.f9761.length > 0) {
            for (long r4 : this.f9761) {
                zzfjk.m12833(1, r4);
            }
        }
        if (this.f9760 != null && this.f9760.length > 0) {
            for (long r2 : this.f9760) {
                zzfjk.m12833(2, r2);
            }
        }
        super.m12842(zzfjk);
    }
}
