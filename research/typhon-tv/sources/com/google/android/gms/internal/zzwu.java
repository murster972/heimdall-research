package com.google.android.gms.internal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.R;
import com.google.android.gms.ads.internal.zzbs;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.Map;

@zzzv
public final class zzwu extends zzxb {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context f5535;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, String> f5536;

    public zzwu(zzanh zzanh, Map<String, String> map) {
        super(zzanh, "storePicture");
        this.f5536 = map;
        this.f5535 = zzanh.m5003();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6004() {
        if (this.f5535 == null) {
            m6012("Activity context is not available");
            return;
        }
        zzbs.zzei();
        if (!zzahn.m4559(this.f5535).m5572()) {
            m6012("Feature is not supported by the device.");
            return;
        }
        String str = this.f5536.get("iurl");
        if (TextUtils.isEmpty(str)) {
            m6012("Image url cannot be empty.");
        } else if (!URLUtil.isValidUrl(str)) {
            String valueOf = String.valueOf(str);
            m6012(valueOf.length() != 0 ? "Invalid image url: ".concat(valueOf) : new String("Invalid image url: "));
        } else {
            String lastPathSegment = Uri.parse(str).getLastPathSegment();
            zzbs.zzei();
            if (!zzahn.m4590(lastPathSegment)) {
                String valueOf2 = String.valueOf(lastPathSegment);
                m6012(valueOf2.length() != 0 ? "Image type not recognized: ".concat(valueOf2) : new String("Image type not recognized: "));
                return;
            }
            Resources r3 = zzbs.zzem().m4476();
            zzbs.zzei();
            AlertDialog.Builder r4 = zzahn.m4573(this.f5535);
            r4.setTitle(r3 != null ? r3.getString(R.string.s1) : "Save image");
            r4.setMessage(r3 != null ? r3.getString(R.string.s2) : "Allow Ad to store image in Picture gallery?");
            r4.setPositiveButton(r3 != null ? r3.getString(R.string.s3) : AbstractSpiCall.HEADER_ACCEPT, new zzwv(this, str, lastPathSegment));
            r4.setNegativeButton(r3 != null ? r3.getString(R.string.s4) : "Decline", new zzww(this));
            AlertDialog create = r4.create();
        }
    }
}
