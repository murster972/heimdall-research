package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzfie implements Iterator<Map.Entry<K, V>> {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f10480;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzfhy f10481;

    /* renamed from: 齉  reason: contains not printable characters */
    private Iterator<Map.Entry<K, V>> f10482;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f10483;

    private zzfie(zzfhy zzfhy) {
        this.f10481 = zzfhy;
        this.f10483 = -1;
    }

    /* synthetic */ zzfie(zzfhy zzfhy, zzfhz zzfhz) {
        this(zzfhy);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Iterator<Map.Entry<K, V>> m12698() {
        if (this.f10482 == null) {
            this.f10482 = this.f10481.f10473.entrySet().iterator();
        }
        return this.f10482;
    }

    public final boolean hasNext() {
        return this.f10483 + 1 < this.f10481.f10471.size() || (!this.f10481.f10473.isEmpty() && m12698().hasNext());
    }

    public final /* synthetic */ Object next() {
        this.f10480 = true;
        int i = this.f10483 + 1;
        this.f10483 = i;
        return i < this.f10481.f10471.size() ? (Map.Entry) this.f10481.f10471.get(this.f10483) : (Map.Entry) m12698().next();
    }

    public final void remove() {
        if (!this.f10480) {
            throw new IllegalStateException("remove() was called before next()");
        }
        this.f10480 = false;
        this.f10481.m12680();
        if (this.f10483 < this.f10481.f10471.size()) {
            zzfhy zzfhy = this.f10481;
            int i = this.f10483;
            this.f10483 = i - 1;
            Object unused = zzfhy.m12682(i);
            return;
        }
        m12698().remove();
    }
}
