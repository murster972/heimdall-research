package com.google.android.gms.internal;

import java.io.IOException;

public final class zzav extends zzfjm<zzav> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Long f8391 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Long f8392 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f8393 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Long f8394 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f8395 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f8396 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f8397 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f8398 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f8399 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f8400 = null;

    public zzav() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9743() {
        int r0 = super.m12841();
        if (this.f8400 != null) {
            r0 += zzfjk.m12808(1, this.f8400);
        }
        if (this.f8397 != null) {
            r0 += zzfjk.m12814(2, this.f8397.longValue());
        }
        if (this.f8399 != null) {
            r0 += zzfjk.m12808(3, this.f8399);
        }
        if (this.f8398 != null) {
            r0 += zzfjk.m12808(4, this.f8398);
        }
        if (this.f8396 != null) {
            r0 += zzfjk.m12808(5, this.f8396);
        }
        if (this.f8391 != null) {
            r0 += zzfjk.m12814(6, this.f8391.longValue());
        }
        if (this.f8392 != null) {
            r0 += zzfjk.m12814(7, this.f8392.longValue());
        }
        if (this.f8393 != null) {
            r0 += zzfjk.m12808(8, this.f8393);
        }
        if (this.f8394 != null) {
            r0 += zzfjk.m12814(9, this.f8394.longValue());
        }
        return this.f8395 != null ? r0 + zzfjk.m12808(10, this.f8395) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m9744(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f8400 = zzfjj.m12791();
                    continue;
                case 16:
                    this.f8397 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 26:
                    this.f8399 = zzfjj.m12791();
                    continue;
                case 34:
                    this.f8398 = zzfjj.m12791();
                    continue;
                case 42:
                    this.f8396 = zzfjj.m12791();
                    continue;
                case 48:
                    this.f8391 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 56:
                    this.f8392 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 66:
                    this.f8393 = zzfjj.m12791();
                    continue;
                case 72:
                    this.f8394 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 82:
                    this.f8395 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9745(zzfjk zzfjk) throws IOException {
        if (this.f8400 != null) {
            zzfjk.m12835(1, this.f8400);
        }
        if (this.f8397 != null) {
            zzfjk.m12824(2, this.f8397.longValue());
        }
        if (this.f8399 != null) {
            zzfjk.m12835(3, this.f8399);
        }
        if (this.f8398 != null) {
            zzfjk.m12835(4, this.f8398);
        }
        if (this.f8396 != null) {
            zzfjk.m12835(5, this.f8396);
        }
        if (this.f8391 != null) {
            zzfjk.m12824(6, this.f8391.longValue());
        }
        if (this.f8392 != null) {
            zzfjk.m12824(7, this.f8392.longValue());
        }
        if (this.f8393 != null) {
            zzfjk.m12835(8, this.f8393);
        }
        if (this.f8394 != null) {
            zzfjk.m12824(9, this.f8394.longValue());
        }
        if (this.f8395 != null) {
            zzfjk.m12835(10, this.f8395);
        }
        super.m12842(zzfjk);
    }
}
