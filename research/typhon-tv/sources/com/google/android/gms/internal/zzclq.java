package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$Event;
import com.google.android.gms.measurement.AppMeasurement$UserProperty;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.security.auth.x500.X500Principal;
import net.pubnative.library.request.PubnativeRequest;

public final class zzclq extends zzcjl {

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] f9659 = {"firebase_"};

    /* renamed from: 靐  reason: contains not printable characters */
    private SecureRandom f9660;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9661;

    /* renamed from: 齉  reason: contains not printable characters */
    private final AtomicLong f9662 = new AtomicLong(0);

    zzclq(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static MessageDigest m11365(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 2) {
                return null;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance(str);
                if (instance != null) {
                    return instance;
                }
                i = i2 + 1;
            } catch (NoSuchAlgorithmException e) {
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    static boolean m11366(String str) {
        zzbq.m9122(str);
        char c = 65535;
        switch (str.hashCode()) {
            case 94660:
                if (str.equals("_in")) {
                    c = 0;
                    break;
                }
                break;
            case 95025:
                if (str.equals("_ug")) {
                    c = 2;
                    break;
                }
                break;
            case 95027:
                if (str.equals("_ui")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
                return true;
            default:
                return false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static int m11367(String str) {
        return "_ldl".equals(str) ? 2048 : 36;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static boolean m11368(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    static boolean m11369(String str) {
        return str != null && str.matches("(\\+|-)?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0033 A[Catch:{ IOException | ClassNotFoundException -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038 A[Catch:{ IOException | ClassNotFoundException -> 0x003c }] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object m11370(java.lang.Object r5) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0004
        L_0x0003:
            return r0
        L_0x0004:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x002e }
            r1.<init>()     // Catch:{ all -> 0x002e }
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ all -> 0x002e }
            r3.<init>(r1)     // Catch:{ all -> 0x002e }
            r3.writeObject(r5)     // Catch:{ all -> 0x0040 }
            r3.flush()     // Catch:{ all -> 0x0040 }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ all -> 0x0040 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0040 }
            byte[] r1 = r1.toByteArray()     // Catch:{ all -> 0x0040 }
            r4.<init>(r1)     // Catch:{ all -> 0x0040 }
            r2.<init>(r4)     // Catch:{ all -> 0x0040 }
            java.lang.Object r1 = r2.readObject()     // Catch:{ all -> 0x0043 }
            r3.close()     // Catch:{ IOException -> 0x003c, ClassNotFoundException -> 0x003e }
            r2.close()     // Catch:{ IOException -> 0x003c, ClassNotFoundException -> 0x003e }
            r0 = r1
            goto L_0x0003
        L_0x002e:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0031:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x003c, ClassNotFoundException -> 0x003e }
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x003c, ClassNotFoundException -> 0x003e }
        L_0x003b:
            throw r1     // Catch:{ IOException -> 0x003c, ClassNotFoundException -> 0x003e }
        L_0x003c:
            r1 = move-exception
            goto L_0x0003
        L_0x003e:
            r1 = move-exception
            goto L_0x0003
        L_0x0040:
            r1 = move-exception
            r2 = r0
            goto L_0x0031
        L_0x0043:
            r1 = move-exception
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzclq.m11370(java.lang.Object):java.lang.Object");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean m11371(String str, String str2) {
        if (str2 == null) {
            m11096().m10832().m10850("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            m11096().m10832().m10850("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                m11096().m10832().m10851("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    m11096().m10832().m10851("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static long m11372(byte[] bArr) {
        int i = 0;
        zzbq.m9120(bArr);
        zzbq.m9125(bArr.length > 0);
        long j = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j += (((long) bArr[length]) & 255) << i;
            i += 8;
            length--;
        }
        return j;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m11373(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo r0 = zzbhf.m10231(context).m10222(str, 64);
            if (!(r0 == null || r0.signatures == null || r0.signatures.length <= 0)) {
                return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(r0.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
            }
        } catch (CertificateException e) {
            m11096().m10832().m10850("Error obtaining certificate", e);
        } catch (PackageManager.NameNotFoundException e2) {
            m11096().m10832().m10850("Package name not found", e2);
        }
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m11374(String str, String str2) {
        if (str2 == null) {
            m11096().m10832().m10850("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            m11096().m10832().m10850("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        m11096().m10832().m10851("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            m11096().m10832().m10851("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0028 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int m11375(java.lang.String r8, java.lang.Object r9, boolean r10) {
        /*
            r7 = this;
            r1 = 1
            r6 = 0
            if (r10 == 0) goto L_0x003b
            java.lang.String r2 = "param"
            boolean r0 = r9 instanceof android.os.Parcelable[]
            if (r0 == 0) goto L_0x002b
            r0 = r9
            android.os.Parcelable[] r0 = (android.os.Parcelable[]) r0
            int r0 = r0.length
        L_0x000f:
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r0 <= r3) goto L_0x0039
            com.google.android.gms.internal.zzchm r1 = r7.m11096()
            com.google.android.gms.internal.zzcho r1 = r1.m10834()
            java.lang.String r3 = "Parameter array is too long; discarded. Value kind, name, array length"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.m10852(r3, r2, r8, r0)
            r0 = r6
        L_0x0026:
            if (r0 != 0) goto L_0x003b
            r0 = 17
        L_0x002a:
            return r0
        L_0x002b:
            boolean r0 = r9 instanceof java.util.ArrayList
            if (r0 == 0) goto L_0x0037
            r0 = r9
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            int r0 = r0.size()
            goto L_0x000f
        L_0x0037:
            r0 = r1
            goto L_0x0026
        L_0x0039:
            r0 = r1
            goto L_0x0026
        L_0x003b:
            boolean r0 = m11368(r8)
            if (r0 == 0) goto L_0x0052
            java.lang.String r1 = "param"
            r3 = 256(0x100, float:3.59E-43)
            r0 = r7
            r2 = r8
            r4 = r9
            r5 = r10
            boolean r0 = r0.m11388((java.lang.String) r1, (java.lang.String) r2, (int) r3, (java.lang.Object) r4, (boolean) r5)
        L_0x004e:
            if (r0 == 0) goto L_0x0060
            r0 = r6
            goto L_0x002a
        L_0x0052:
            java.lang.String r1 = "param"
            r3 = 100
            r0 = r7
            r2 = r8
            r4 = r9
            r5 = r10
            boolean r0 = r0.m11388((java.lang.String) r1, (java.lang.String) r2, (int) r3, (java.lang.Object) r4, (boolean) r5)
            goto L_0x004e
        L_0x0060:
            r0 = 4
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzclq.m11375(java.lang.String, java.lang.Object, boolean):int");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object m11376(int i, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return m11378(String.valueOf(obj), i, z);
            }
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m11377(zzcmb zzcmb, String str) {
        for (zzcmc zzcmc : zzcmb.f9716) {
            if (zzcmc.f9722.equals(str)) {
                if (zzcmc.f9719 != null) {
                    return zzcmc.f9719;
                }
                if (zzcmc.f9721 != null) {
                    return zzcmc.f9721;
                }
                if (zzcmc.f9720 != null) {
                    return zzcmc.f9720;
                }
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m11378(String str, int i, boolean z) {
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m11379(String str, String[] strArr, String[] strArr2) {
        zzbq.m9120(strArr);
        zzbq.m9120(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            if (m11387(str, strArr[i])) {
                return strArr2[i];
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m11380(Bundle bundle, Object obj) {
        zzbq.m9120(bundle);
        if (obj == null) {
            return;
        }
        if ((obj instanceof String) || (obj instanceof CharSequence)) {
            bundle.putLong("_el", (long) String.valueOf(obj).length());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0008, code lost:
        r1 = r1.getServiceInfo(new android.content.ComponentName(r4, r5), 4);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m11381(android.content.Context r4, java.lang.String r5) {
        /*
            r0 = 0
            android.content.pm.PackageManager r1 = r4.getPackageManager()     // Catch:{ NameNotFoundException -> 0x001a }
            if (r1 != 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            android.content.ComponentName r2 = new android.content.ComponentName     // Catch:{ NameNotFoundException -> 0x001a }
            r2.<init>(r4, r5)     // Catch:{ NameNotFoundException -> 0x001a }
            r3 = 4
            android.content.pm.ServiceInfo r1 = r1.getServiceInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x001a }
            if (r1 == 0) goto L_0x0007
            boolean r1 = r1.enabled     // Catch:{ NameNotFoundException -> 0x001a }
            if (r1 == 0) goto L_0x0007
            r0 = 1
            goto L_0x0007
        L_0x001a:
            r1 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzclq.m11381(android.content.Context, java.lang.String):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m11382(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m11383(Bundle bundle, int i) {
        if (bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m11384(zzcha zzcha, zzcgi zzcgi) {
        zzbq.m9120(zzcha);
        zzbq.m9120(zzcgi);
        return !TextUtils.isEmpty(zzcgi.f9140);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m11385(String str) {
        zzbq.m9122(str);
        return str.charAt(0) != '_' || str.equals("_ep");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m11386(String str, int i, String str2) {
        if (str2 == null) {
            m11096().m10832().m10850("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            m11096().m10832().m10852("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m11387(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m11388(String str, String str2, int i, Object obj, boolean z) {
        if (obj == null || (obj instanceof Long) || (obj instanceof Float) || (obj instanceof Integer) || (obj instanceof Byte) || (obj instanceof Short) || (obj instanceof Boolean) || (obj instanceof Double)) {
            return true;
        }
        if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
            String valueOf = String.valueOf(obj);
            if (valueOf.codePointCount(0, valueOf.length()) <= i) {
                return true;
            }
            m11096().m10834().m10852("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
            return false;
        } else if ((obj instanceof Bundle) && z) {
            return true;
        } else {
            if ((obj instanceof Parcelable[]) && z) {
                for (Parcelable parcelable : (Parcelable[]) obj) {
                    if (!(parcelable instanceof Bundle)) {
                        m11096().m10834().m10851("All Parcelable[] elements must be of type Bundle. Value type, name", parcelable.getClass(), str2);
                        return false;
                    }
                }
                return true;
            } else if (!(obj instanceof ArrayList) || !z) {
                return false;
            } else {
                ArrayList arrayList = (ArrayList) obj;
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj2 = arrayList.get(i2);
                    i2++;
                    if (!(obj2 instanceof Bundle)) {
                        m11096().m10834().m10851("All ArrayList elements must be of type Bundle. Value type, name", obj2.getClass(), str2);
                        return false;
                    }
                }
                return true;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m11389(String str, String[] strArr, String str2) {
        boolean z;
        boolean z2;
        if (str2 == null) {
            m11096().m10832().m10850("Name is required and can't be null. Type", str);
            return false;
        }
        zzbq.m9120(str2);
        int i = 0;
        while (true) {
            if (i >= f9659.length) {
                z = false;
                break;
            } else if (str2.startsWith(f9659[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            m11096().m10832().m10851("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        }
        if (strArr != null) {
            zzbq.m9120(strArr);
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    z2 = false;
                    break;
                } else if (m11387(str2, strArr[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                m11096().m10832().m10851("Name is reserved. Type, name", str, str2);
                return false;
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m11390(long[] jArr, int i) {
        return i < (jArr.length << 6) && (jArr[i / 64] & (1 << (i % 64))) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static byte[] m11391(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long[] m11392(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        long[] jArr = new long[length];
        int i = 0;
        while (i < length) {
            jArr[i] = 0;
            int i2 = 0;
            while (i2 < 64 && (i << 6) + i2 < bitSet.length()) {
                if (bitSet.get((i << 6) + i2)) {
                    jArr[i] = jArr[i] | (1 << i2);
                }
                i2++;
            }
            i++;
        }
        return jArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bundle[] m11393(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            return (Bundle[]) Arrays.copyOf((Parcelable[]) obj, ((Parcelable[]) obj).length, Bundle[].class);
        } else {
            if (!(obj instanceof ArrayList)) {
                return null;
            }
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzcmc[] m11394(zzcmc[] zzcmcArr, String str, Object obj) {
        int length = zzcmcArr.length;
        int i = 0;
        while (i < length) {
            zzcmc zzcmc = zzcmcArr[i];
            if (Objects.equals(zzcmc.f9722, str)) {
                zzcmc.f9721 = null;
                zzcmc.f9719 = null;
                zzcmc.f9720 = null;
                if (obj instanceof Long) {
                    zzcmc.f9721 = (Long) obj;
                    return zzcmcArr;
                } else if (obj instanceof String) {
                    zzcmc.f9719 = (String) obj;
                    return zzcmcArr;
                } else if (!(obj instanceof Double)) {
                    return zzcmcArr;
                } else {
                    zzcmc.f9720 = (Double) obj;
                    return zzcmcArr;
                }
            } else {
                i++;
            }
        }
        zzcmc[] zzcmcArr2 = new zzcmc[(zzcmcArr.length + 1)];
        System.arraycopy(zzcmcArr, 0, zzcmcArr2, 0, zzcmcArr.length);
        zzcmc zzcmc2 = new zzcmc();
        zzcmc2.f9722 = str;
        if (obj instanceof Long) {
            zzcmc2.f9721 = (Long) obj;
        } else if (obj instanceof String) {
            zzcmc2.f9719 = (String) obj;
        } else if (obj instanceof Double) {
            zzcmc2.f9720 = (Double) obj;
        }
        zzcmcArr2[zzcmcArr.length] = zzcmc2;
        return zzcmcArr2;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int m11395(String str) {
        if (!m11371("event param", str)) {
            return 3;
        }
        if (!m11389("event param", (String[]) null, str)) {
            return 14;
        }
        return m11386("event param", 40, str) ? 0 : 3;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final int m11396(String str) {
        if (!m11374("event param", str)) {
            return 3;
        }
        if (!m11389("event param", (String[]) null, str)) {
            return 14;
        }
        return m11386("event param", 40, str) ? 0 : 3;
    }

    /* access modifiers changed from: protected */
    public final void t_() {
        m11109();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                m11096().m10834().m10849("Utils falling back to Random for random id");
            }
        }
        this.f9662.set(nextLong);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m11397() {
        return super.m11091();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m11398(String str) {
        if (TextUtils.isEmpty(str)) {
            m11096().m10832().m10849("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            return false;
        }
        zzbq.m9120(str);
        if (str.matches("^1:\\d+:android:[a-f0-9]+$")) {
            return true;
        }
        m11096().m10832().m10850("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", str);
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m11399() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m11400() {
        return super.m11093();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m11401(String str) {
        m11109();
        if (zzbhf.m10231(m11097()).m10224(str) == 0) {
            return true;
        }
        m11096().m10845().m10850("Permission not granted", str);
        return false;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m11402() {
        return super.m11094();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean m11403(String str) {
        return PubnativeRequest.LEGACY_ZONE_ID.equals(m11099().m10945(str, "measurement.upload.blacklist_public"));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m11404() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m11405() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m11406() {
        return super.m11097();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean m11407(String str) {
        return PubnativeRequest.LEGACY_ZONE_ID.equals(m11099().m10945(str, "measurement.upload.blacklist_internal"));
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m11408() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m11409() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m11410() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m11411() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m11412() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m11413() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m11414() {
        return true;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m11415() {
        return super.m11104();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m11416(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return m11102().m10542().equals(str);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m11417() {
        return super.m11105();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final long m11418() {
        long andIncrement;
        if (this.f9662.get() == 0) {
            synchronized (this.f9662) {
                long nextLong = new Random(System.nanoTime() ^ m11105().m9243()).nextLong();
                int i = this.f9661 + 1;
                this.f9661 = i;
                andIncrement = nextLong + ((long) i);
            }
        } else {
            synchronized (this.f9662) {
                this.f9662.compareAndSet(-1, 1);
                andIncrement = this.f9662.getAndIncrement();
            }
        }
        return andIncrement;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final SecureRandom m11419() {
        m11109();
        if (this.f9660 == null) {
            this.f9660 = new SecureRandom();
        }
        return this.f9660;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m11420(String str) {
        if (!m11374("user property", str)) {
            return 6;
        }
        if (!m11389("user property", AppMeasurement$UserProperty.f11087, str)) {
            return 15;
        }
        return m11386("user property", 24, str) ? 0 : 6;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m11421() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m11422(String str) {
        if (!m11371(NotificationCompat.CATEGORY_EVENT, str)) {
            return 2;
        }
        if (!m11389(NotificationCompat.CATEGORY_EVENT, AppMeasurement$Event.f11083, str)) {
            return 13;
        }
        return m11386(NotificationCompat.CATEGORY_EVENT, 40, str) ? 0 : 2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m11423(String str, Object obj) {
        return "_ldl".equals(str) ? m11388("user property referrer", str, m11367(str), obj, false) : m11388("user property", str, m11367(str), obj, false) ? 0 : 7;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final long m11424(Context context, String str) {
        m11109();
        zzbq.m9120(context);
        zzbq.m9122(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest r5 = m11365("MD5");
        if (r5 == null) {
            m11096().m10832().m10849("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!m11373(context, str)) {
                    PackageInfo r4 = zzbhf.m10231(context).m10222(m11097().getPackageName(), 64);
                    if (r4.signatures != null && r4.signatures.length > 0) {
                        return m11372(r5.digest(r4.signatures[0].toByteArray()));
                    }
                    m11096().m10834().m10849("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e) {
                m11096().m10832().m10850("Package name not found", e);
            }
        }
        return 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11425() {
        super.m11107();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final byte[] m11426(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            m11096().m10832().m10850("Failed to ungzip content", e);
            throw e;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11427(String str) {
        if (!m11371("user property", str)) {
            return 6;
        }
        if (!m11389("user property", AppMeasurement$UserProperty.f11087, str)) {
            return 15;
        }
        return m11386("user property", 24, str) ? 0 : 6;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m11428() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m11429(String str) {
        if (!m11374(NotificationCompat.CATEGORY_EVENT, str)) {
            return 2;
        }
        if (!m11389(NotificationCompat.CATEGORY_EVENT, AppMeasurement$Event.f11083, str)) {
            return 13;
        }
        return m11386(NotificationCompat.CATEGORY_EVENT, 40, str) ? 0 : 2;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Object m11430(String str, Object obj) {
        return "_ldl".equals(str) ? m11376(m11367(str), obj, true) : m11376(m11367(str), obj, false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11431() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m11432(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        Bundle bundle = null;
        if (uri != null) {
            try {
                if (uri.isHierarchical()) {
                    str4 = uri.getQueryParameter("utm_campaign");
                    str3 = uri.getQueryParameter("utm_source");
                    str2 = uri.getQueryParameter("utm_medium");
                    str = uri.getQueryParameter("gclid");
                } else {
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                }
                if (!TextUtils.isEmpty(str4) || !TextUtils.isEmpty(str3) || !TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str)) {
                    bundle = new Bundle();
                    if (!TextUtils.isEmpty(str4)) {
                        bundle.putString("campaign", str4);
                    }
                    if (!TextUtils.isEmpty(str3)) {
                        bundle.putString("source", str3);
                    }
                    if (!TextUtils.isEmpty(str2)) {
                        bundle.putString("medium", str2);
                    }
                    if (!TextUtils.isEmpty(str)) {
                        bundle.putString("gclid", str);
                    }
                    String queryParameter = uri.getQueryParameter("utm_term");
                    if (!TextUtils.isEmpty(queryParameter)) {
                        bundle.putString("term", queryParameter);
                    }
                    String queryParameter2 = uri.getQueryParameter("utm_content");
                    if (!TextUtils.isEmpty(queryParameter2)) {
                        bundle.putString("content", queryParameter2);
                    }
                    String queryParameter3 = uri.getQueryParameter("aclid");
                    if (!TextUtils.isEmpty(queryParameter3)) {
                        bundle.putString("aclid", queryParameter3);
                    }
                    String queryParameter4 = uri.getQueryParameter("cp1");
                    if (!TextUtils.isEmpty(queryParameter4)) {
                        bundle.putString("cp1", queryParameter4);
                    }
                    String queryParameter5 = uri.getQueryParameter("anid");
                    if (!TextUtils.isEmpty(queryParameter5)) {
                        bundle.putString("anid", queryParameter5);
                    }
                }
            } catch (UnsupportedOperationException e) {
                m11096().m10834().m10850("Install referrer url isn't a hierarchical URI", e);
            }
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m11433(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object r3 = m11437(str, bundle.get(str));
                if (r3 == null) {
                    m11096().m10834().m10850("Param value can't be null", m11111().m10794(str));
                } else {
                    m11440(bundle2, str, r3);
                }
            }
        }
        return bundle2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m11434(String str, Bundle bundle, List<String> list, boolean z, boolean z2) {
        if (bundle == null) {
            return null;
        }
        Bundle bundle2 = new Bundle(bundle);
        int i = 0;
        for (String str2 : bundle.keySet()) {
            int i2 = 0;
            if (list == null || !list.contains(str2)) {
                if (z) {
                    i2 = m11395(str2);
                }
                if (i2 == 0) {
                    i2 = m11396(str2);
                }
            }
            if (i2 != 0) {
                if (m11383(bundle2, i2)) {
                    bundle2.putString("_ev", m11378(str2, 40, true));
                    if (i2 == 3) {
                        m11380(bundle2, (Object) str2);
                    }
                }
                bundle2.remove(str2);
            } else {
                int r3 = m11375(str2, bundle.get(str2), z2);
                if (r3 != 0 && !"_ev".equals(str2)) {
                    if (m11383(bundle2, r3)) {
                        bundle2.putString("_ev", m11378(str2, 40, true));
                        m11380(bundle2, bundle.get(str2));
                    }
                    bundle2.remove(str2);
                } else if (!m11385(str2) || (i = i + 1) <= 25) {
                    i = i;
                } else {
                    m11096().m10832().m10851(new StringBuilder(48).append("Event can't contain more then 25 params").toString(), m11111().m10805(str), m11111().m10799(bundle));
                    m11383(bundle2, 5);
                    bundle2.remove(str2);
                }
            }
        }
        return bundle2;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends Parcelable> T m11435(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            T t = (Parcelable) creator.createFromParcel(obtain);
            obtain.recycle();
            return t;
        } catch (zzbfo e) {
            m11096().m10832().m10849("Failed to load parcelable from buffer");
            obtain.recycle();
            return null;
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcha m11436(String str, Bundle bundle, String str2, long j, boolean z, boolean z2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (m11429(str) != 0) {
            m11096().m10832().m10850("Invalid conditional property event name", m11111().m10797(str));
            throw new IllegalArgumentException();
        }
        Bundle bundle2 = bundle != null ? new Bundle(bundle) : new Bundle();
        bundle2.putString("_o", str2);
        return new zzcha(str, new zzcgx(m11433(m11434(str, bundle2, (List<String>) Collections.singletonList("_o"), false, false))), str2, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11437(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return m11376(256, obj, true);
        }
        if (!m11368(str)) {
            i = 100;
        }
        return m11376(i, obj, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11438() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11439(int i, String str, String str2, int i2) {
        m11443((String) null, i, str, str2, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11440(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (str != null) {
                m11096().m10835().m10851("Not putting event parameter. Invalid value type. name, type", m11111().m10794(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11441(zzcmc zzcmc, Object obj) {
        zzbq.m9120(obj);
        zzcmc.f9719 = null;
        zzcmc.f9721 = null;
        zzcmc.f9720 = null;
        if (obj instanceof String) {
            zzcmc.f9719 = (String) obj;
        } else if (obj instanceof Long) {
            zzcmc.f9721 = (Long) obj;
        } else if (obj instanceof Double) {
            zzcmc.f9720 = (Double) obj;
        } else {
            m11096().m10832().m10850("Ignoring invalid (type) event param value", obj);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11442(zzcmg zzcmg, Object obj) {
        zzbq.m9120(obj);
        zzcmg.f9767 = null;
        zzcmg.f9766 = null;
        zzcmg.f9764 = null;
        if (obj instanceof String) {
            zzcmg.f9767 = (String) obj;
        } else if (obj instanceof Long) {
            zzcmg.f9766 = (Long) obj;
        } else if (obj instanceof Double) {
            zzcmg.f9764 = (Double) obj;
        } else {
            m11096().m10832().m10850("Ignoring invalid (type) user attribute value", obj);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11443(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        m11383(bundle, i);
        if (!TextUtils.isEmpty(str2)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        this.f9487.m11022().m11174("auto", "_err", bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11444(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(m11105().m9243() - j) > j2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m11445(zzcmd zzcmd) {
        try {
            byte[] bArr = new byte[zzcmd.m12872()];
            zzfjk r1 = zzfjk.m12822(bArr, 0, bArr.length);
            zzcmd.m12877(r1);
            r1.m12829();
            return bArr;
        } catch (IOException e) {
            m11096().m10832().m10850("Data loss. Failed to serialize batch", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m11446(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            m11096().m10832().m10850("Failed to gzip content", e);
            throw e;
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m11447() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m11448() {
        return super.m11112();
    }
}
