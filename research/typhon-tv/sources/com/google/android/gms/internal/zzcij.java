package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.lang.Thread;

final class zzcij implements Thread.UncaughtExceptionHandler {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcih f9369;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f9370;

    public zzcij(zzcih zzcih, String str) {
        this.f9369 = zzcih;
        zzbq.m9120(str);
        this.f9370 = str;
    }

    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.f9369.m11096().m10832().m10850(this.f9370, th);
    }
}
