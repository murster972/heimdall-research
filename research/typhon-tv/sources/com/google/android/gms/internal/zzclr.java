package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclr extends zzfjm<zzclr> {

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile zzclr[] f9663;

    /* renamed from: 靐  reason: contains not printable characters */
    public zzclv[] f9664 = zzclv.m11465();

    /* renamed from: 齉  reason: contains not printable characters */
    public zzcls[] f9665 = zzcls.m11453();

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9666 = null;

    public zzclr() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzclr[] m11449() {
        if (f9663 == null) {
            synchronized (zzfjq.f10546) {
                if (f9663 == null) {
                    f9663 = new zzclr[0];
                }
            }
        }
        return f9663;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclr)) {
            return false;
        }
        zzclr zzclr = (zzclr) obj;
        if (this.f9666 == null) {
            if (zzclr.f9666 != null) {
                return false;
            }
        } else if (!this.f9666.equals(zzclr.f9666)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9664, (Object[]) zzclr.f9664)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9665, (Object[]) zzclr.f9665)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclr.f10533 == null || zzclr.f10533.m12849() : this.f10533.equals(zzclr.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((this.f9666 == null ? 0 : this.f9666.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + zzfjq.m12859((Object[]) this.f9664)) * 31) + zzfjq.m12859((Object[]) this.f9665)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11450() {
        int r0 = super.m12841();
        if (this.f9666 != null) {
            r0 += zzfjk.m12806(1, this.f9666.intValue());
        }
        if (this.f9664 != null && this.f9664.length > 0) {
            int i = r0;
            for (zzclv zzclv : this.f9664) {
                if (zzclv != null) {
                    i += zzfjk.m12807(2, (zzfjs) zzclv);
                }
            }
            r0 = i;
        }
        if (this.f9665 != null && this.f9665.length > 0) {
            for (zzcls zzcls : this.f9665) {
                if (zzcls != null) {
                    r0 += zzfjk.m12807(3, (zzfjs) zzcls);
                }
            }
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11451(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9666 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 18:
                    int r2 = zzfjv.m12883(zzfjj, 18);
                    int length = this.f9664 == null ? 0 : this.f9664.length;
                    zzclv[] zzclvArr = new zzclv[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9664, 0, zzclvArr, 0, length);
                    }
                    while (length < zzclvArr.length - 1) {
                        zzclvArr[length] = new zzclv();
                        zzfjj.m12802((zzfjs) zzclvArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzclvArr[length] = new zzclv();
                    zzfjj.m12802((zzfjs) zzclvArr[length]);
                    this.f9664 = zzclvArr;
                    continue;
                case 26:
                    int r22 = zzfjv.m12883(zzfjj, 26);
                    int length2 = this.f9665 == null ? 0 : this.f9665.length;
                    zzcls[] zzclsArr = new zzcls[(r22 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f9665, 0, zzclsArr, 0, length2);
                    }
                    while (length2 < zzclsArr.length - 1) {
                        zzclsArr[length2] = new zzcls();
                        zzfjj.m12802((zzfjs) zzclsArr[length2]);
                        zzfjj.m12800();
                        length2++;
                    }
                    zzclsArr[length2] = new zzcls();
                    zzfjj.m12802((zzfjs) zzclsArr[length2]);
                    this.f9665 = zzclsArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11452(zzfjk zzfjk) throws IOException {
        if (this.f9666 != null) {
            zzfjk.m12832(1, this.f9666.intValue());
        }
        if (this.f9664 != null && this.f9664.length > 0) {
            for (zzclv zzclv : this.f9664) {
                if (zzclv != null) {
                    zzfjk.m12834(2, (zzfjs) zzclv);
                }
            }
        }
        if (this.f9665 != null && this.f9665.length > 0) {
            for (zzcls zzcls : this.f9665) {
                if (zzcls != null) {
                    zzfjk.m12834(3, (zzfjs) zzcls);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
