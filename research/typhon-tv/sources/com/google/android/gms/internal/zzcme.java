package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcme extends zzfjm<zzcme> {

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private static volatile zzcme[] f9724;

    /* renamed from: ʻ  reason: contains not printable characters */
    public Long f9725 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Long f9726 = null;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private Integer f9727 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Long f9728 = null;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private Integer f9729 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public Integer f9730 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f9731 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    public Boolean f9732 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f9733 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    public String f9734 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    public Long f9735 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    public Long f9736 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    public String f9737 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    public Long f9738 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    public String f9739 = null;

    /* renamed from: י  reason: contains not printable characters */
    public Integer f9740 = null;

    /* renamed from: ـ  reason: contains not printable characters */
    public String f9741 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f9742 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f9743 = null;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public Long f9744 = null;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public String f9745 = null;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public String f9746 = null;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public Boolean f9747 = null;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public zzcma[] f9748 = zzcma.m11485();

    /* renamed from: ᵢ  reason: contains not printable characters */
    public String f9749 = null;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public Integer f9750 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public Long f9751 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public zzcmb[] f9752 = zzcmb.m11489();

    /* renamed from: 麤  reason: contains not printable characters */
    public Long f9753 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzcmg[] f9754 = zzcmg.m11507();

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9755 = null;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public String f9756 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public String f9757 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public String f9758 = null;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public Long f9759 = null;

    public zzcme() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzcme[] m11500() {
        if (f9724 == null) {
            synchronized (zzfjq.f10546) {
                if (f9724 == null) {
                    f9724 = new zzcme[0];
                }
            }
        }
        return f9724;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcme)) {
            return false;
        }
        zzcme zzcme = (zzcme) obj;
        if (this.f9755 == null) {
            if (zzcme.f9755 != null) {
                return false;
            }
        } else if (!this.f9755.equals(zzcme.f9755)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9752, (Object[]) zzcme.f9752)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9754, (Object[]) zzcme.f9754)) {
            return false;
        }
        if (this.f9753 == null) {
            if (zzcme.f9753 != null) {
                return false;
            }
        } else if (!this.f9753.equals(zzcme.f9753)) {
            return false;
        }
        if (this.f9751 == null) {
            if (zzcme.f9751 != null) {
                return false;
            }
        } else if (!this.f9751.equals(zzcme.f9751)) {
            return false;
        }
        if (this.f9725 == null) {
            if (zzcme.f9725 != null) {
                return false;
            }
        } else if (!this.f9725.equals(zzcme.f9725)) {
            return false;
        }
        if (this.f9726 == null) {
            if (zzcme.f9726 != null) {
                return false;
            }
        } else if (!this.f9726.equals(zzcme.f9726)) {
            return false;
        }
        if (this.f9728 == null) {
            if (zzcme.f9728 != null) {
                return false;
            }
        } else if (!this.f9728.equals(zzcme.f9728)) {
            return false;
        }
        if (this.f9739 == null) {
            if (zzcme.f9739 != null) {
                return false;
            }
        } else if (!this.f9739.equals(zzcme.f9739)) {
            return false;
        }
        if (this.f9742 == null) {
            if (zzcme.f9742 != null) {
                return false;
            }
        } else if (!this.f9742.equals(zzcme.f9742)) {
            return false;
        }
        if (this.f9743 == null) {
            if (zzcme.f9743 != null) {
                return false;
            }
        } else if (!this.f9743.equals(zzcme.f9743)) {
            return false;
        }
        if (this.f9733 == null) {
            if (zzcme.f9733 != null) {
                return false;
            }
        } else if (!this.f9733.equals(zzcme.f9733)) {
            return false;
        }
        if (this.f9730 == null) {
            if (zzcme.f9730 != null) {
                return false;
            }
        } else if (!this.f9730.equals(zzcme.f9730)) {
            return false;
        }
        if (this.f9731 == null) {
            if (zzcme.f9731 != null) {
                return false;
            }
        } else if (!this.f9731.equals(zzcme.f9731)) {
            return false;
        }
        if (this.f9757 == null) {
            if (zzcme.f9757 != null) {
                return false;
            }
        } else if (!this.f9757.equals(zzcme.f9757)) {
            return false;
        }
        if (this.f9758 == null) {
            if (zzcme.f9758 != null) {
                return false;
            }
        } else if (!this.f9758.equals(zzcme.f9758)) {
            return false;
        }
        if (this.f9735 == null) {
            if (zzcme.f9735 != null) {
                return false;
            }
        } else if (!this.f9735.equals(zzcme.f9735)) {
            return false;
        }
        if (this.f9736 == null) {
            if (zzcme.f9736 != null) {
                return false;
            }
        } else if (!this.f9736.equals(zzcme.f9736)) {
            return false;
        }
        if (this.f9737 == null) {
            if (zzcme.f9737 != null) {
                return false;
            }
        } else if (!this.f9737.equals(zzcme.f9737)) {
            return false;
        }
        if (this.f9732 == null) {
            if (zzcme.f9732 != null) {
                return false;
            }
        } else if (!this.f9732.equals(zzcme.f9732)) {
            return false;
        }
        if (this.f9734 == null) {
            if (zzcme.f9734 != null) {
                return false;
            }
        } else if (!this.f9734.equals(zzcme.f9734)) {
            return false;
        }
        if (this.f9738 == null) {
            if (zzcme.f9738 != null) {
                return false;
            }
        } else if (!this.f9738.equals(zzcme.f9738)) {
            return false;
        }
        if (this.f9740 == null) {
            if (zzcme.f9740 != null) {
                return false;
            }
        } else if (!this.f9740.equals(zzcme.f9740)) {
            return false;
        }
        if (this.f9741 == null) {
            if (zzcme.f9741 != null) {
                return false;
            }
        } else if (!this.f9741.equals(zzcme.f9741)) {
            return false;
        }
        if (this.f9745 == null) {
            if (zzcme.f9745 != null) {
                return false;
            }
        } else if (!this.f9745.equals(zzcme.f9745)) {
            return false;
        }
        if (this.f9747 == null) {
            if (zzcme.f9747 != null) {
                return false;
            }
        } else if (!this.f9747.equals(zzcme.f9747)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9748, (Object[]) zzcme.f9748)) {
            return false;
        }
        if (this.f9749 == null) {
            if (zzcme.f9749 != null) {
                return false;
            }
        } else if (!this.f9749.equals(zzcme.f9749)) {
            return false;
        }
        if (this.f9750 == null) {
            if (zzcme.f9750 != null) {
                return false;
            }
        } else if (!this.f9750.equals(zzcme.f9750)) {
            return false;
        }
        if (this.f9729 == null) {
            if (zzcme.f9729 != null) {
                return false;
            }
        } else if (!this.f9729.equals(zzcme.f9729)) {
            return false;
        }
        if (this.f9727 == null) {
            if (zzcme.f9727 != null) {
                return false;
            }
        } else if (!this.f9727.equals(zzcme.f9727)) {
            return false;
        }
        if (this.f9756 == null) {
            if (zzcme.f9756 != null) {
                return false;
            }
        } else if (!this.f9756.equals(zzcme.f9756)) {
            return false;
        }
        if (this.f9759 == null) {
            if (zzcme.f9759 != null) {
                return false;
            }
        } else if (!this.f9759.equals(zzcme.f9759)) {
            return false;
        }
        if (this.f9744 == null) {
            if (zzcme.f9744 != null) {
                return false;
            }
        } else if (!this.f9744.equals(zzcme.f9744)) {
            return false;
        }
        if (this.f9746 == null) {
            if (zzcme.f9746 != null) {
                return false;
            }
        } else if (!this.f9746.equals(zzcme.f9746)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcme.f10533 == null || zzcme.f10533.m12849() : this.f10533.equals(zzcme.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9746 == null ? 0 : this.f9746.hashCode()) + (((this.f9744 == null ? 0 : this.f9744.hashCode()) + (((this.f9759 == null ? 0 : this.f9759.hashCode()) + (((this.f9756 == null ? 0 : this.f9756.hashCode()) + (((this.f9727 == null ? 0 : this.f9727.hashCode()) + (((this.f9729 == null ? 0 : this.f9729.hashCode()) + (((this.f9750 == null ? 0 : this.f9750.hashCode()) + (((this.f9749 == null ? 0 : this.f9749.hashCode()) + (((((this.f9747 == null ? 0 : this.f9747.hashCode()) + (((this.f9745 == null ? 0 : this.f9745.hashCode()) + (((this.f9741 == null ? 0 : this.f9741.hashCode()) + (((this.f9740 == null ? 0 : this.f9740.hashCode()) + (((this.f9738 == null ? 0 : this.f9738.hashCode()) + (((this.f9734 == null ? 0 : this.f9734.hashCode()) + (((this.f9732 == null ? 0 : this.f9732.hashCode()) + (((this.f9737 == null ? 0 : this.f9737.hashCode()) + (((this.f9736 == null ? 0 : this.f9736.hashCode()) + (((this.f9735 == null ? 0 : this.f9735.hashCode()) + (((this.f9758 == null ? 0 : this.f9758.hashCode()) + (((this.f9757 == null ? 0 : this.f9757.hashCode()) + (((this.f9731 == null ? 0 : this.f9731.hashCode()) + (((this.f9730 == null ? 0 : this.f9730.hashCode()) + (((this.f9733 == null ? 0 : this.f9733.hashCode()) + (((this.f9743 == null ? 0 : this.f9743.hashCode()) + (((this.f9742 == null ? 0 : this.f9742.hashCode()) + (((this.f9739 == null ? 0 : this.f9739.hashCode()) + (((this.f9728 == null ? 0 : this.f9728.hashCode()) + (((this.f9726 == null ? 0 : this.f9726.hashCode()) + (((this.f9725 == null ? 0 : this.f9725.hashCode()) + (((this.f9751 == null ? 0 : this.f9751.hashCode()) + (((this.f9753 == null ? 0 : this.f9753.hashCode()) + (((((((this.f9755 == null ? 0 : this.f9755.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + zzfjq.m12859((Object[]) this.f9752)) * 31) + zzfjq.m12859((Object[]) this.f9754)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + zzfjq.m12859((Object[]) this.f9748)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11501() {
        int r0 = super.m12841();
        if (this.f9755 != null) {
            r0 += zzfjk.m12806(1, this.f9755.intValue());
        }
        if (this.f9752 != null && this.f9752.length > 0) {
            int i = r0;
            for (zzcmb zzcmb : this.f9752) {
                if (zzcmb != null) {
                    i += zzfjk.m12807(2, (zzfjs) zzcmb);
                }
            }
            r0 = i;
        }
        if (this.f9754 != null && this.f9754.length > 0) {
            int i2 = r0;
            for (zzcmg zzcmg : this.f9754) {
                if (zzcmg != null) {
                    i2 += zzfjk.m12807(3, (zzfjs) zzcmg);
                }
            }
            r0 = i2;
        }
        if (this.f9753 != null) {
            r0 += zzfjk.m12814(4, this.f9753.longValue());
        }
        if (this.f9751 != null) {
            r0 += zzfjk.m12814(5, this.f9751.longValue());
        }
        if (this.f9725 != null) {
            r0 += zzfjk.m12814(6, this.f9725.longValue());
        }
        if (this.f9728 != null) {
            r0 += zzfjk.m12814(7, this.f9728.longValue());
        }
        if (this.f9739 != null) {
            r0 += zzfjk.m12808(8, this.f9739);
        }
        if (this.f9742 != null) {
            r0 += zzfjk.m12808(9, this.f9742);
        }
        if (this.f9743 != null) {
            r0 += zzfjk.m12808(10, this.f9743);
        }
        if (this.f9733 != null) {
            r0 += zzfjk.m12808(11, this.f9733);
        }
        if (this.f9730 != null) {
            r0 += zzfjk.m12806(12, this.f9730.intValue());
        }
        if (this.f9731 != null) {
            r0 += zzfjk.m12808(13, this.f9731);
        }
        if (this.f9757 != null) {
            r0 += zzfjk.m12808(14, this.f9757);
        }
        if (this.f9758 != null) {
            r0 += zzfjk.m12808(16, this.f9758);
        }
        if (this.f9735 != null) {
            r0 += zzfjk.m12814(17, this.f9735.longValue());
        }
        if (this.f9736 != null) {
            r0 += zzfjk.m12814(18, this.f9736.longValue());
        }
        if (this.f9737 != null) {
            r0 += zzfjk.m12808(19, this.f9737);
        }
        if (this.f9732 != null) {
            this.f9732.booleanValue();
            r0 += zzfjk.m12805(20) + 1;
        }
        if (this.f9734 != null) {
            r0 += zzfjk.m12808(21, this.f9734);
        }
        if (this.f9738 != null) {
            r0 += zzfjk.m12814(22, this.f9738.longValue());
        }
        if (this.f9740 != null) {
            r0 += zzfjk.m12806(23, this.f9740.intValue());
        }
        if (this.f9741 != null) {
            r0 += zzfjk.m12808(24, this.f9741);
        }
        if (this.f9745 != null) {
            r0 += zzfjk.m12808(25, this.f9745);
        }
        if (this.f9726 != null) {
            r0 += zzfjk.m12814(26, this.f9726.longValue());
        }
        if (this.f9747 != null) {
            this.f9747.booleanValue();
            r0 += zzfjk.m12805(28) + 1;
        }
        if (this.f9748 != null && this.f9748.length > 0) {
            for (zzcma zzcma : this.f9748) {
                if (zzcma != null) {
                    r0 += zzfjk.m12807(29, (zzfjs) zzcma);
                }
            }
        }
        if (this.f9749 != null) {
            r0 += zzfjk.m12808(30, this.f9749);
        }
        if (this.f9750 != null) {
            r0 += zzfjk.m12806(31, this.f9750.intValue());
        }
        if (this.f9729 != null) {
            r0 += zzfjk.m12806(32, this.f9729.intValue());
        }
        if (this.f9727 != null) {
            r0 += zzfjk.m12806(33, this.f9727.intValue());
        }
        if (this.f9756 != null) {
            r0 += zzfjk.m12808(34, this.f9756);
        }
        if (this.f9759 != null) {
            r0 += zzfjk.m12814(35, this.f9759.longValue());
        }
        if (this.f9744 != null) {
            r0 += zzfjk.m12814(36, this.f9744.longValue());
        }
        return this.f9746 != null ? r0 + zzfjk.m12808(37, this.f9746) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11502(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9755 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 18:
                    int r2 = zzfjv.m12883(zzfjj, 18);
                    int length = this.f9752 == null ? 0 : this.f9752.length;
                    zzcmb[] zzcmbArr = new zzcmb[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9752, 0, zzcmbArr, 0, length);
                    }
                    while (length < zzcmbArr.length - 1) {
                        zzcmbArr[length] = new zzcmb();
                        zzfjj.m12802((zzfjs) zzcmbArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzcmbArr[length] = new zzcmb();
                    zzfjj.m12802((zzfjs) zzcmbArr[length]);
                    this.f9752 = zzcmbArr;
                    continue;
                case 26:
                    int r22 = zzfjv.m12883(zzfjj, 26);
                    int length2 = this.f9754 == null ? 0 : this.f9754.length;
                    zzcmg[] zzcmgArr = new zzcmg[(r22 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f9754, 0, zzcmgArr, 0, length2);
                    }
                    while (length2 < zzcmgArr.length - 1) {
                        zzcmgArr[length2] = new zzcmg();
                        zzfjj.m12802((zzfjs) zzcmgArr[length2]);
                        zzfjj.m12800();
                        length2++;
                    }
                    zzcmgArr[length2] = new zzcmg();
                    zzfjj.m12802((zzfjs) zzcmgArr[length2]);
                    this.f9754 = zzcmgArr;
                    continue;
                case 32:
                    this.f9753 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 40:
                    this.f9751 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 48:
                    this.f9725 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 56:
                    this.f9728 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 66:
                    this.f9739 = zzfjj.m12791();
                    continue;
                case 74:
                    this.f9742 = zzfjj.m12791();
                    continue;
                case 82:
                    this.f9743 = zzfjj.m12791();
                    continue;
                case 90:
                    this.f9733 = zzfjj.m12791();
                    continue;
                case 96:
                    this.f9730 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 106:
                    this.f9731 = zzfjj.m12791();
                    continue;
                case 114:
                    this.f9757 = zzfjj.m12791();
                    continue;
                case 130:
                    this.f9758 = zzfjj.m12791();
                    continue;
                case 136:
                    this.f9735 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 144:
                    this.f9736 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 154:
                    this.f9737 = zzfjj.m12791();
                    continue;
                case 160:
                    this.f9732 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 170:
                    this.f9734 = zzfjj.m12791();
                    continue;
                case 176:
                    this.f9738 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 184:
                    this.f9740 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 194:
                    this.f9741 = zzfjj.m12791();
                    continue;
                case 202:
                    this.f9745 = zzfjj.m12791();
                    continue;
                case 208:
                    this.f9726 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 224:
                    this.f9747 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 234:
                    int r23 = zzfjv.m12883(zzfjj, 234);
                    int length3 = this.f9748 == null ? 0 : this.f9748.length;
                    zzcma[] zzcmaArr = new zzcma[(r23 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.f9748, 0, zzcmaArr, 0, length3);
                    }
                    while (length3 < zzcmaArr.length - 1) {
                        zzcmaArr[length3] = new zzcma();
                        zzfjj.m12802((zzfjs) zzcmaArr[length3]);
                        zzfjj.m12800();
                        length3++;
                    }
                    zzcmaArr[length3] = new zzcma();
                    zzfjj.m12802((zzfjs) zzcmaArr[length3]);
                    this.f9748 = zzcmaArr;
                    continue;
                case 242:
                    this.f9749 = zzfjj.m12791();
                    continue;
                case 248:
                    this.f9750 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 256:
                    this.f9729 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 264:
                    this.f9727 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 274:
                    this.f9756 = zzfjj.m12791();
                    continue;
                case 280:
                    this.f9759 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 288:
                    this.f9744 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 298:
                    this.f9746 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11503(zzfjk zzfjk) throws IOException {
        if (this.f9755 != null) {
            zzfjk.m12832(1, this.f9755.intValue());
        }
        if (this.f9752 != null && this.f9752.length > 0) {
            for (zzcmb zzcmb : this.f9752) {
                if (zzcmb != null) {
                    zzfjk.m12834(2, (zzfjs) zzcmb);
                }
            }
        }
        if (this.f9754 != null && this.f9754.length > 0) {
            for (zzcmg zzcmg : this.f9754) {
                if (zzcmg != null) {
                    zzfjk.m12834(3, (zzfjs) zzcmg);
                }
            }
        }
        if (this.f9753 != null) {
            zzfjk.m12824(4, this.f9753.longValue());
        }
        if (this.f9751 != null) {
            zzfjk.m12824(5, this.f9751.longValue());
        }
        if (this.f9725 != null) {
            zzfjk.m12824(6, this.f9725.longValue());
        }
        if (this.f9728 != null) {
            zzfjk.m12824(7, this.f9728.longValue());
        }
        if (this.f9739 != null) {
            zzfjk.m12835(8, this.f9739);
        }
        if (this.f9742 != null) {
            zzfjk.m12835(9, this.f9742);
        }
        if (this.f9743 != null) {
            zzfjk.m12835(10, this.f9743);
        }
        if (this.f9733 != null) {
            zzfjk.m12835(11, this.f9733);
        }
        if (this.f9730 != null) {
            zzfjk.m12832(12, this.f9730.intValue());
        }
        if (this.f9731 != null) {
            zzfjk.m12835(13, this.f9731);
        }
        if (this.f9757 != null) {
            zzfjk.m12835(14, this.f9757);
        }
        if (this.f9758 != null) {
            zzfjk.m12835(16, this.f9758);
        }
        if (this.f9735 != null) {
            zzfjk.m12824(17, this.f9735.longValue());
        }
        if (this.f9736 != null) {
            zzfjk.m12824(18, this.f9736.longValue());
        }
        if (this.f9737 != null) {
            zzfjk.m12835(19, this.f9737);
        }
        if (this.f9732 != null) {
            zzfjk.m12836(20, this.f9732.booleanValue());
        }
        if (this.f9734 != null) {
            zzfjk.m12835(21, this.f9734);
        }
        if (this.f9738 != null) {
            zzfjk.m12824(22, this.f9738.longValue());
        }
        if (this.f9740 != null) {
            zzfjk.m12832(23, this.f9740.intValue());
        }
        if (this.f9741 != null) {
            zzfjk.m12835(24, this.f9741);
        }
        if (this.f9745 != null) {
            zzfjk.m12835(25, this.f9745);
        }
        if (this.f9726 != null) {
            zzfjk.m12824(26, this.f9726.longValue());
        }
        if (this.f9747 != null) {
            zzfjk.m12836(28, this.f9747.booleanValue());
        }
        if (this.f9748 != null && this.f9748.length > 0) {
            for (zzcma zzcma : this.f9748) {
                if (zzcma != null) {
                    zzfjk.m12834(29, (zzfjs) zzcma);
                }
            }
        }
        if (this.f9749 != null) {
            zzfjk.m12835(30, this.f9749);
        }
        if (this.f9750 != null) {
            zzfjk.m12832(31, this.f9750.intValue());
        }
        if (this.f9729 != null) {
            zzfjk.m12832(32, this.f9729.intValue());
        }
        if (this.f9727 != null) {
            zzfjk.m12832(33, this.f9727.intValue());
        }
        if (this.f9756 != null) {
            zzfjk.m12835(34, this.f9756);
        }
        if (this.f9759 != null) {
            zzfjk.m12824(35, this.f9759.longValue());
        }
        if (this.f9744 != null) {
            zzfjk.m12824(36, this.f9744.longValue());
        }
        if (this.f9746 != null) {
            zzfjk.m12835(37, this.f9746);
        }
        super.m12842(zzfjk);
    }
}
