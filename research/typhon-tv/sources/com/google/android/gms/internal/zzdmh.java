package com.google.android.gms.internal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v4.util.SimpleArrayMap;

public class zzdmh extends AnimatorListenerAdapter {

    /* renamed from: 龘  reason: contains not printable characters */
    private SimpleArrayMap<Animator, Boolean> f9891 = new SimpleArrayMap<>();

    public void onAnimationCancel(Animator animator) {
        this.f9891.put(animator, true);
    }

    public void onAnimationStart(Animator animator) {
        this.f9891.put(animator, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11627(Animator animator) {
        return this.f9891.containsKey(animator) && this.f9891.get(animator).booleanValue();
    }
}
