package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzadm extends zzeu implements zzadk {
    zzadm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m9483() throws RemoteException {
        Parcel r0 = m12300(12, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m9484() throws RemoteException {
        m12298(8, v_());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9485(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(10, v_);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9486() throws RemoteException {
        Parcel r0 = m12300(5, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9487() throws RemoteException {
        m12298(7, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9488() throws RemoteException {
        m12298(6, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9489(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(11, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9490() throws RemoteException {
        m12298(2, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9491(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(9, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9492(zzadp zzadp) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzadp);
        m12298(3, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9493(zzadv zzadv) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzadv);
        m12298(1, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9494(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12298(13, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9495(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(34, v_);
    }
}
