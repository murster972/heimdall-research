package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzoc extends zzeu implements zzoa {
    zzoc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.customrenderedad.client.IOnCustomRenderedAdLoadedListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13179(zznx zznx) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zznx);
        m12298(1, v_);
    }
}
