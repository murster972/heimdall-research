package com.google.android.gms.internal;

import com.google.android.gms.ads.AdListener;

@zzzv
public final class zzjg extends zzki {

    /* renamed from: 龘  reason: contains not printable characters */
    private final AdListener f4752;

    public zzjg(AdListener adListener) {
        this.f4752 = adListener;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5437() {
        this.f4752.onAdImpression();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final AdListener m5438() {
        return this.f4752;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m5439() {
        this.f4752.onAdClicked();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5440() {
        this.f4752.onAdLeftApplication();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5441() {
        this.f4752.onAdOpened();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5442() {
        this.f4752.onAdLoaded();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5443() {
        this.f4752.onAdClosed();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5444(int i) {
        this.f4752.onAdFailedToLoad(i);
    }
}
