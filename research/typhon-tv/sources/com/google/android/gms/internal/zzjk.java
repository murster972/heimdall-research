package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzjk {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f4771;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4772;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f4773;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Bundle f4774;

    /* renamed from: ʿ  reason: contains not printable characters */
    private List<String> f4775;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Bundle f4776;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f4777;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzmn f4778;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Location f4779;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f4780;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4781;

    /* renamed from: 靐  reason: contains not printable characters */
    private Bundle f4782;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<String> f4783;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f4784;

    /* renamed from: 龘  reason: contains not printable characters */
    private long f4785;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String f4786;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f4787;

    public zzjk() {
        this.f4785 = -1;
        this.f4782 = new Bundle();
        this.f4784 = -1;
        this.f4783 = new ArrayList();
        this.f4781 = false;
        this.f4771 = -1;
        this.f4772 = false;
        this.f4773 = null;
        this.f4778 = null;
        this.f4779 = null;
        this.f4780 = null;
        this.f4776 = new Bundle();
        this.f4774 = new Bundle();
        this.f4775 = new ArrayList();
        this.f4786 = null;
        this.f4787 = null;
        this.f4777 = false;
    }

    public zzjk(zzjj zzjj) {
        this.f4785 = zzjj.f4765;
        this.f4782 = zzjj.f4767;
        this.f4784 = zzjj.f4766;
        this.f4783 = zzjj.f4764;
        this.f4781 = zzjj.f4753;
        this.f4771 = zzjj.f4754;
        this.f4772 = zzjj.f4755;
        this.f4773 = zzjj.f4761;
        this.f4778 = zzjj.f4762;
        this.f4779 = zzjj.f4763;
        this.f4780 = zzjj.f4758;
        this.f4776 = zzjj.f4756;
        this.f4774 = zzjj.f4757;
        this.f4775 = zzjj.f4769;
        this.f4786 = zzjj.f4770;
        this.f4787 = zzjj.f4759;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzjj m5450() {
        return new zzjj(7, this.f4785, this.f4782, this.f4784, this.f4783, this.f4781, this.f4771, this.f4772, this.f4773, this.f4778, this.f4779, this.f4780, this.f4776, this.f4774, this.f4775, this.f4786, this.f4787, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzjk m5451(Location location) {
        this.f4779 = null;
        return this;
    }
}
