package com.google.android.gms.internal;

import java.util.concurrent.Future;

final /* synthetic */ class zzsb implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Future f10869;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzalf f10870;

    zzsb(zzalf zzalf, Future future) {
        this.f10870 = zzalf;
        this.f10869 = future;
    }

    public final void run() {
        zzalf zzalf = this.f10870;
        Future future = this.f10869;
        if (zzalf.isCancelled()) {
            future.cancel(true);
        }
    }
}
