package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzags extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8176;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8177;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzags(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8177 = context;
        this.f8176 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9591() {
        SharedPreferences sharedPreferences = this.f8177.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putString("native_advanced_settings", sharedPreferences.getString("native_advanced_settings", "{}"));
        if (this.f8176 != null) {
            this.f8176.m9605(bundle);
        }
    }
}
