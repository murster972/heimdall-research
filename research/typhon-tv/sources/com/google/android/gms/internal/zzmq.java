package com.google.android.gms.internal;

import com.google.android.gms.ads.VideoController;

public final class zzmq extends zzlp {

    /* renamed from: 龘  reason: contains not printable characters */
    private final VideoController.VideoLifecycleCallbacks f10803;

    public zzmq(VideoController.VideoLifecycleCallbacks videoLifecycleCallbacks) {
        this.f10803 = videoLifecycleCallbacks;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13141() {
        this.f10803.onVideoPlay();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13142() {
        this.f10803.onVideoEnd();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13143() {
        this.f10803.onVideoPause();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13144() {
        this.f10803.onVideoStart();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13145(boolean z) {
        this.f10803.onVideoMute(z);
    }
}
