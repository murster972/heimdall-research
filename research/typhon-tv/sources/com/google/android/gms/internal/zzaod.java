package com.google.android.gms.internal;

import android.annotation.TargetApi;
import com.google.android.gms.internal.zzaog;
import com.google.android.gms.internal.zzaow;
import com.google.android.gms.internal.zzaoy;

@zzzv
@TargetApi(17)
public final class zzaod<WebViewT extends zzaog & zzaow & zzaoy> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final WebViewT f4531;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaof f4532;

    public zzaod(WebViewT webviewt, zzaof zzaof) {
        this.f4532 = zzaof;
        this.f4531 = webviewt;
    }
}
