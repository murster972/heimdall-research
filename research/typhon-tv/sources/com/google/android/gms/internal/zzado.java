package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzado extends zzeu implements zzadn {
    zzado(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdCreator");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m9497(IObjectWrapper iObjectWrapper, zzux zzux, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) zzux);
        v_.writeInt(11910000);
        Parcel r0 = m12300(1, v_);
        IBinder readStrongBinder = r0.readStrongBinder();
        r0.recycle();
        return readStrongBinder;
    }
}
