package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbhn extends zzbfm {
    public static final Parcelable.Creator<zzbhn> CREATOR = new zzbho();

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8773;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8774;

    public zzbhn(String str, String str2) {
        this.f8774 = str;
        this.f8773 = str2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f8774, false);
        zzbfp.m10193(parcel, 3, this.f8773, false);
        zzbfp.m10182(parcel, r0);
    }
}
