package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.js.zzaa;
import org.json.JSONObject;

final class zzabq implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    final /* synthetic */ String f8060;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzabo f8061;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ JSONObject f8062;

    zzabq(zzabo zzabo, JSONObject jSONObject, String str) {
        this.f8061 = zzabo;
        this.f8062 = jSONObject;
        this.f8060 = str;
    }

    public final void run() {
        zzaa unused = this.f8061.f3889 = zzabo.f3884.zzb((zzcv) null);
        this.f8061.f3889.zza(new zzabr(this), new zzabs(this));
    }
}
