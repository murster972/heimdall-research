package com.google.android.gms.internal;

import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.internal.featurehighlight.zzh;

final class zzaze implements zzh {

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzazd f8484;

    zzaze(zzazd zzazd) {
        this.f8484 = zzazd;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9812() {
        if (this.f8484.f8477) {
            IntroductoryOverlay.zza.m8052(this.f8484.f8480);
            this.f8484.f8479.m8121((Runnable) new zzazg(this));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9813() {
        if (this.f8484.f8477) {
            IntroductoryOverlay.zza.m8052(this.f8484.f8480);
            this.f8484.f8479.m8124((Runnable) new zzazf(this));
        }
    }
}
