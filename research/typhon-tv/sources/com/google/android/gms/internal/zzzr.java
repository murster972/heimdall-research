package com.google.android.gms.internal;

import java.lang.Thread;

final class zzzr implements Thread.UncaughtExceptionHandler {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzzp f11027;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Thread.UncaughtExceptionHandler f11028;

    zzzr(zzzp zzzp, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f11027 = zzzp;
        this.f11028 = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            this.f11027.m6097(thread, th);
            if (this.f11028 != null) {
                this.f11028.uncaughtException(thread, th);
            }
        } catch (Throwable th2) {
            if (this.f11028 != null) {
                this.f11028.uncaughtException(thread, th);
            }
            throw th2;
        }
    }
}
