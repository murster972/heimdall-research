package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zze;
import java.util.Set;

@zzzv
public final class zzwr extends zzxb {

    /* renamed from: 龘  reason: contains not printable characters */
    private static Set<String> f5513 = zze.m9249("top-left", "top-right", "top-center", TtmlNode.CENTER, "bottom-left", "bottom-right", "bottom-center");

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f5514 = -1;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f5515 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f5516 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    private zzapa f5517;

    /* renamed from: ʿ  reason: contains not printable characters */
    private ImageView f5518;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Activity f5519;

    /* renamed from: ˊ  reason: contains not printable characters */
    private PopupWindow f5520;

    /* renamed from: ˋ  reason: contains not printable characters */
    private RelativeLayout f5521;

    /* renamed from: ˎ  reason: contains not printable characters */
    private ViewGroup f5522;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f5523 = -1;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Object f5524 = new Object();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final zzanh f5525;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f5526 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f5527 = "top-right";

    /* renamed from: 麤  reason: contains not printable characters */
    private int f5528 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f5529 = true;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private LinearLayout f5530;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private zzxc f5531;

    public zzwr(zzanh zzanh, zzxc zzxc) {
        super(zzanh, "resize");
        this.f5525 = zzanh;
        this.f5519 = zzanh.m5003();
        this.f5531 = zzxc;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m5995(int i, int i2) {
        m6010(i, i2 - zzbs.zzei().m4631(this.f5519)[0], this.f5523, this.f5514);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final int[] m5996() {
        boolean z;
        int i;
        int i2;
        int[] r0 = zzbs.zzei().m4628(this.f5519);
        int[] r5 = zzbs.zzei().m4631(this.f5519);
        int i3 = r0[0];
        int i4 = r0[1];
        if (this.f5523 < 50 || this.f5523 > i3) {
            zzagf.m4791("Width is too small or too large.");
            z = false;
        } else if (this.f5514 < 50 || this.f5514 > i4) {
            zzagf.m4791("Height is too small or too large.");
            z = false;
        } else if (this.f5514 == i4 && this.f5523 == i3) {
            zzagf.m4791("Cannot resize to a full-screen ad.");
            z = false;
        } else {
            if (this.f5529) {
                String str = this.f5527;
                char c = 65535;
                switch (str.hashCode()) {
                    case -1364013995:
                        if (str.equals(TtmlNode.CENTER)) {
                            c = 2;
                            break;
                        }
                        break;
                    case -1012429441:
                        if (str.equals("top-left")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -655373719:
                        if (str.equals("bottom-left")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 1163912186:
                        if (str.equals("bottom-right")) {
                            c = 5;
                            break;
                        }
                        break;
                    case 1288627767:
                        if (str.equals("bottom-center")) {
                            c = 4;
                            break;
                        }
                        break;
                    case 1755462605:
                        if (str.equals("top-center")) {
                            c = 1;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        i = this.f5515 + this.f5528;
                        i2 = this.f5526 + this.f5516;
                        break;
                    case 1:
                        i = ((this.f5528 + this.f5515) + (this.f5523 / 2)) - 25;
                        i2 = this.f5526 + this.f5516;
                        break;
                    case 2:
                        i = ((this.f5528 + this.f5515) + (this.f5523 / 2)) - 25;
                        i2 = ((this.f5526 + this.f5516) + (this.f5514 / 2)) - 25;
                        break;
                    case 3:
                        i = this.f5515 + this.f5528;
                        i2 = ((this.f5526 + this.f5516) + this.f5514) - 50;
                        break;
                    case 4:
                        i = ((this.f5528 + this.f5515) + (this.f5523 / 2)) - 25;
                        i2 = ((this.f5526 + this.f5516) + this.f5514) - 50;
                        break;
                    case 5:
                        i = ((this.f5528 + this.f5515) + this.f5523) - 50;
                        i2 = ((this.f5526 + this.f5516) + this.f5514) - 50;
                        break;
                    default:
                        i = ((this.f5528 + this.f5515) + this.f5523) - 50;
                        i2 = this.f5526 + this.f5516;
                        break;
                }
                if (i < 0 || i + 50 > i3 || i2 < r5[0] || i2 + 50 > r5[1]) {
                    z = false;
                }
            }
            z = true;
        }
        if (!z) {
            return null;
        }
        if (this.f5529) {
            return new int[]{this.f5528 + this.f5515, this.f5526 + this.f5516};
        }
        int[] r02 = zzbs.zzei().m4628(this.f5519);
        int[] r52 = zzbs.zzei().m4631(this.f5519);
        int i5 = r02[0];
        int i6 = this.f5528 + this.f5515;
        int i7 = this.f5526 + this.f5516;
        if (i6 < 0) {
            i6 = 0;
        } else if (this.f5523 + i6 > i5) {
            i6 = i5 - this.f5523;
        }
        if (i7 < r52[0]) {
            i7 = r52[0];
        } else if (this.f5514 + i7 > r52[1]) {
            i7 = r52[1] - this.f5514;
        }
        return new int[]{i6, i7};
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5997(int i, int i2) {
        this.f5528 = i;
        this.f5526 = i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5998(int i, int i2, boolean z) {
        synchronized (this.f5524) {
            this.f5528 = i;
            this.f5526 = i2;
            if (this.f5520 != null && z) {
                int[] r0 = m5996();
                if (r0 != null) {
                    PopupWindow popupWindow = this.f5520;
                    zzkb.m5487();
                    int r3 = zzajr.m4757((Context) this.f5519, r0[0]);
                    zzkb.m5487();
                    popupWindow.update(r3, zzajr.m4757((Context) this.f5519, r0[1]), this.f5520.getWidth(), this.f5520.getHeight());
                    m5995(r0[0], r0[1]);
                } else {
                    m6000(true);
                }
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5999(java.util.Map<java.lang.String, java.lang.String> r15) {
        /*
            r14 = this;
            r5 = -1
            r13 = 0
            r6 = 1
            r4 = 0
            java.lang.Object r7 = r14.f5524
            monitor-enter(r7)
            android.app.Activity r1 = r14.f5519     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0013
            java.lang.String r1 = "Not an activity context. Cannot resize."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
        L_0x0012:
            return
        L_0x0013:
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzapa r1 = r1.m4983()     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0026
            java.lang.String r1 = "Webview is not yet available, size is not set."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x0023:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            throw r1
        L_0x0026:
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzapa r1 = r1.m4983()     // Catch:{ all -> 0x0023 }
            boolean r1 = r1.m5227()     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x003a
            java.lang.String r1 = "Is interstitial. Cannot resize an interstitial."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x003a:
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            boolean r1 = r1.m4987()     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x004a
            java.lang.String r1 = "Cannot resize an expanded banner."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x004a:
            java.lang.String r1 = "width"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1     // Catch:{ all -> 0x0023 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x006b
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "width"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            int r1 = com.google.android.gms.internal.zzahn.m4576((java.lang.String) r1)     // Catch:{ all -> 0x0023 }
            r14.f5523 = r1     // Catch:{ all -> 0x0023 }
        L_0x006b:
            java.lang.String r1 = "height"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1     // Catch:{ all -> 0x0023 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x008c
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "height"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            int r1 = com.google.android.gms.internal.zzahn.m4576((java.lang.String) r1)     // Catch:{ all -> 0x0023 }
            r14.f5514 = r1     // Catch:{ all -> 0x0023 }
        L_0x008c:
            java.lang.String r1 = "offsetX"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1     // Catch:{ all -> 0x0023 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x00ad
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "offsetX"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            int r1 = com.google.android.gms.internal.zzahn.m4576((java.lang.String) r1)     // Catch:{ all -> 0x0023 }
            r14.f5515 = r1     // Catch:{ all -> 0x0023 }
        L_0x00ad:
            java.lang.String r1 = "offsetY"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1     // Catch:{ all -> 0x0023 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x00ce
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "offsetY"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            int r1 = com.google.android.gms.internal.zzahn.m4576((java.lang.String) r1)     // Catch:{ all -> 0x0023 }
            r14.f5516 = r1     // Catch:{ all -> 0x0023 }
        L_0x00ce:
            java.lang.String r1 = "allowOffscreen"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1     // Catch:{ all -> 0x0023 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x00ec
            java.lang.String r1 = "allowOffscreen"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            boolean r1 = java.lang.Boolean.parseBoolean(r1)     // Catch:{ all -> 0x0023 }
            r14.f5529 = r1     // Catch:{ all -> 0x0023 }
        L_0x00ec:
            java.lang.String r1 = "customClosePosition"
            java.lang.Object r1 = r15.get(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0023 }
            if (r2 != 0) goto L_0x00fd
            r14.f5527 = r1     // Catch:{ all -> 0x0023 }
        L_0x00fd:
            int r1 = r14.f5523     // Catch:{ all -> 0x0023 }
            if (r1 < 0) goto L_0x0111
            int r1 = r14.f5514     // Catch:{ all -> 0x0023 }
            if (r1 < 0) goto L_0x0111
            r1 = r6
        L_0x0106:
            if (r1 != 0) goto L_0x0113
            java.lang.String r1 = "Invalid width and height options. Cannot resize."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x0111:
            r1 = r4
            goto L_0x0106
        L_0x0113:
            android.app.Activity r1 = r14.f5519     // Catch:{ all -> 0x0023 }
            android.view.Window r8 = r1.getWindow()     // Catch:{ all -> 0x0023 }
            if (r8 == 0) goto L_0x0121
            android.view.View r1 = r8.getDecorView()     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x012a
        L_0x0121:
            java.lang.String r1 = "Activity context is not ready, cannot get window or decor view."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x012a:
            int[] r9 = r14.m5996()     // Catch:{ all -> 0x0023 }
            if (r9 != 0) goto L_0x0139
            java.lang.String r1 = "Resize location out of screen or close button is not visible."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x0139:
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ all -> 0x0023 }
            android.app.Activity r1 = r14.f5519     // Catch:{ all -> 0x0023 }
            int r2 = r14.f5523     // Catch:{ all -> 0x0023 }
            int r10 = com.google.android.gms.internal.zzajr.m4757((android.content.Context) r1, (int) r2)     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ all -> 0x0023 }
            android.app.Activity r1 = r14.f5519     // Catch:{ all -> 0x0023 }
            int r2 = r14.f5514     // Catch:{ all -> 0x0023 }
            int r11 = com.google.android.gms.internal.zzajr.m4757((android.content.Context) r1, (int) r2)     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0154
            throw r13     // Catch:{ all -> 0x0023 }
        L_0x0154:
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x0023 }
            android.view.ViewParent r2 = r1.getParent()     // Catch:{ all -> 0x0023 }
            if (r2 == 0) goto L_0x01e6
            boolean r1 = r2 instanceof android.view.ViewGroup     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x01e6
            r0 = r2
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0     // Catch:{ all -> 0x0023 }
            r1 = r0
            com.google.android.gms.internal.zzanh r3 = r14.f5525     // Catch:{ all -> 0x0023 }
            if (r3 != 0) goto L_0x0169
            throw r13     // Catch:{ all -> 0x0023 }
        L_0x0169:
            android.view.View r3 = (android.view.View) r3     // Catch:{ all -> 0x0023 }
            r1.removeView(r3)     // Catch:{ all -> 0x0023 }
            android.widget.PopupWindow r1 = r14.f5520     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x01e0
            android.view.ViewGroup r2 = (android.view.ViewGroup) r2     // Catch:{ all -> 0x0023 }
            r14.f5522 = r2     // Catch:{ all -> 0x0023 }
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x017e
            throw r13     // Catch:{ all -> 0x0023 }
        L_0x017e:
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x0023 }
            android.graphics.Bitmap r1 = com.google.android.gms.internal.zzahn.m4591((android.view.View) r1)     // Catch:{ all -> 0x0023 }
            android.widget.ImageView r2 = new android.widget.ImageView     // Catch:{ all -> 0x0023 }
            android.app.Activity r3 = r14.f5519     // Catch:{ all -> 0x0023 }
            r2.<init>(r3)     // Catch:{ all -> 0x0023 }
            r14.f5518 = r2     // Catch:{ all -> 0x0023 }
            android.widget.ImageView r2 = r14.f5518     // Catch:{ all -> 0x0023 }
            r2.setImageBitmap(r1)     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzapa r1 = r1.m4983()     // Catch:{ all -> 0x0023 }
            r14.f5517 = r1     // Catch:{ all -> 0x0023 }
            android.view.ViewGroup r1 = r14.f5522     // Catch:{ all -> 0x0023 }
            android.widget.ImageView r2 = r14.f5518     // Catch:{ all -> 0x0023 }
        L_0x019e:
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout     // Catch:{ all -> 0x0023 }
            android.app.Activity r2 = r14.f5519     // Catch:{ all -> 0x0023 }
            r1.<init>(r2)     // Catch:{ all -> 0x0023 }
            r14.f5521 = r1     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout r1 = r14.f5521     // Catch:{ all -> 0x0023 }
            r2 = 0
            r1.setBackgroundColor(r2)     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout r1 = r14.f5521     // Catch:{ all -> 0x0023 }
            android.view.ViewGroup$LayoutParams r2 = new android.view.ViewGroup$LayoutParams     // Catch:{ all -> 0x0023 }
            r2.<init>(r10, r11)     // Catch:{ all -> 0x0023 }
            r1.setLayoutParams(r2)     // Catch:{ all -> 0x0023 }
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout r1 = r14.f5521     // Catch:{ all -> 0x0023 }
            r2 = 0
            android.widget.PopupWindow r1 = com.google.android.gms.internal.zzahn.m4595((android.view.View) r1, (int) r10, (int) r11, (boolean) r2)     // Catch:{ all -> 0x0023 }
            r14.f5520 = r1     // Catch:{ all -> 0x0023 }
            android.widget.PopupWindow r1 = r14.f5520     // Catch:{ all -> 0x0023 }
            r2 = 1
            r1.setOutsideTouchable(r2)     // Catch:{ all -> 0x0023 }
            android.widget.PopupWindow r1 = r14.f5520     // Catch:{ all -> 0x0023 }
            r2 = 1
            r1.setTouchable(r2)     // Catch:{ all -> 0x0023 }
            android.widget.PopupWindow r2 = r14.f5520     // Catch:{ all -> 0x0023 }
            boolean r1 = r14.f5529     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x01ef
            r1 = r6
        L_0x01d6:
            r2.setClippingEnabled(r1)     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout r2 = r14.f5521     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x01f1
            throw r13     // Catch:{ all -> 0x0023 }
        L_0x01e0:
            android.widget.PopupWindow r1 = r14.f5520     // Catch:{ all -> 0x0023 }
            r1.dismiss()     // Catch:{ all -> 0x0023 }
            goto L_0x019e
        L_0x01e6:
            java.lang.String r1 = "Webview is detached, probably in the middle of a resize or expand."
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x01ef:
            r1 = r4
            goto L_0x01d6
        L_0x01f1:
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x0023 }
            r3 = -1
            r12 = -1
            r2.addView(r1, r3, r12)     // Catch:{ all -> 0x0023 }
            android.widget.LinearLayout r1 = new android.widget.LinearLayout     // Catch:{ all -> 0x0023 }
            android.app.Activity r2 = r14.f5519     // Catch:{ all -> 0x0023 }
            r1.<init>(r2)     // Catch:{ all -> 0x0023 }
            r14.f5530 = r1     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ all -> 0x0023 }
            android.app.Activity r1 = r14.f5519     // Catch:{ all -> 0x0023 }
            r3 = 50
            int r1 = com.google.android.gms.internal.zzajr.m4757((android.content.Context) r1, (int) r3)     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ all -> 0x0023 }
            android.app.Activity r3 = r14.f5519     // Catch:{ all -> 0x0023 }
            r12 = 50
            int r3 = com.google.android.gms.internal.zzajr.m4757((android.content.Context) r3, (int) r12)     // Catch:{ all -> 0x0023 }
            r2.<init>(r1, r3)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = r14.f5527     // Catch:{ all -> 0x0023 }
            int r3 = r1.hashCode()     // Catch:{ all -> 0x0023 }
            switch(r3) {
                case -1364013995: goto L_0x02b0;
                case -1012429441: goto L_0x0299;
                case -655373719: goto L_0x02bc;
                case 1163912186: goto L_0x02d4;
                case 1288627767: goto L_0x02c8;
                case 1755462605: goto L_0x02a4;
                default: goto L_0x0225;
            }     // Catch:{ all -> 0x0023 }
        L_0x0225:
            r1 = r5
        L_0x0226:
            switch(r1) {
                case 0: goto L_0x02e0;
                case 1: goto L_0x02ec;
                case 2: goto L_0x02f8;
                case 3: goto L_0x02ff;
                case 4: goto L_0x030b;
                case 5: goto L_0x0317;
                default: goto L_0x0229;
            }     // Catch:{ all -> 0x0023 }
        L_0x0229:
            r1 = 10
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            r1 = 11
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
        L_0x0233:
            android.widget.LinearLayout r1 = r14.f5530     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzws r3 = new com.google.android.gms.internal.zzws     // Catch:{ all -> 0x0023 }
            r3.<init>(r14)     // Catch:{ all -> 0x0023 }
            r1.setOnClickListener(r3)     // Catch:{ all -> 0x0023 }
            android.widget.LinearLayout r1 = r14.f5530     // Catch:{ all -> 0x0023 }
            java.lang.String r3 = "Close button"
            r1.setContentDescription(r3)     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout r1 = r14.f5521     // Catch:{ all -> 0x0023 }
            android.widget.LinearLayout r3 = r14.f5530     // Catch:{ all -> 0x0023 }
            android.widget.PopupWindow r1 = r14.f5520     // Catch:{ RuntimeException -> 0x0323 }
            android.view.View r2 = r8.getDecorView()     // Catch:{ RuntimeException -> 0x0323 }
            r3 = 0
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ RuntimeException -> 0x0323 }
            android.app.Activity r4 = r14.f5519     // Catch:{ RuntimeException -> 0x0323 }
            r5 = 0
            r5 = r9[r5]     // Catch:{ RuntimeException -> 0x0323 }
            int r4 = com.google.android.gms.internal.zzajr.m4757((android.content.Context) r4, (int) r5)     // Catch:{ RuntimeException -> 0x0323 }
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ RuntimeException -> 0x0323 }
            android.app.Activity r5 = r14.f5519     // Catch:{ RuntimeException -> 0x0323 }
            r6 = 1
            r6 = r9[r6]     // Catch:{ RuntimeException -> 0x0323 }
            int r5 = com.google.android.gms.internal.zzajr.m4757((android.content.Context) r5, (int) r6)     // Catch:{ RuntimeException -> 0x0323 }
            r1.showAtLocation(r2, r3, r4, r5)     // Catch:{ RuntimeException -> 0x0323 }
            r1 = 0
            r1 = r9[r1]     // Catch:{ all -> 0x0023 }
            r2 = 1
            r2 = r9[r2]     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzxc r3 = r14.f5531     // Catch:{ all -> 0x0023 }
            if (r3 == 0) goto L_0x027e
            com.google.android.gms.internal.zzxc r3 = r14.f5531     // Catch:{ all -> 0x0023 }
            int r4 = r14.f5523     // Catch:{ all -> 0x0023 }
            int r5 = r14.f5514     // Catch:{ all -> 0x0023 }
            r3.zza(r1, r2, r4, r5)     // Catch:{ all -> 0x0023 }
        L_0x027e:
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzapa r2 = com.google.android.gms.internal.zzapa.m5223(r10, r11)     // Catch:{ all -> 0x0023 }
            r1.m5011((com.google.android.gms.internal.zzapa) r2)     // Catch:{ all -> 0x0023 }
            r1 = 0
            r1 = r9[r1]     // Catch:{ all -> 0x0023 }
            r2 = 1
            r2 = r9[r2]     // Catch:{ all -> 0x0023 }
            r14.m5995(r1, r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "resized"
            r14.m6009(r1)     // Catch:{ all -> 0x0023 }
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        L_0x0299:
            java.lang.String r3 = "top-left"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0225
            r1 = r4
            goto L_0x0226
        L_0x02a4:
            java.lang.String r3 = "top-center"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0225
            r1 = r6
            goto L_0x0226
        L_0x02b0:
            java.lang.String r3 = "center"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0225
            r1 = 2
            goto L_0x0226
        L_0x02bc:
            java.lang.String r3 = "bottom-left"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0225
            r1 = 3
            goto L_0x0226
        L_0x02c8:
            java.lang.String r3 = "bottom-center"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0225
            r1 = 4
            goto L_0x0226
        L_0x02d4:
            java.lang.String r3 = "bottom-right"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0225
            r1 = 5
            goto L_0x0226
        L_0x02e0:
            r1 = 10
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            r1 = 9
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0233
        L_0x02ec:
            r1 = 10
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            r1 = 14
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0233
        L_0x02f8:
            r1 = 13
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0233
        L_0x02ff:
            r1 = 12
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            r1 = 9
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0233
        L_0x030b:
            r1 = 12
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            r1 = 14
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0233
        L_0x0317:
            r1 = 12
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            r1 = 11
            r2.addRule(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0233
        L_0x0323:
            r1 = move-exception
            java.lang.String r2 = "Cannot show popup window: "
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x0023 }
            int r3 = r1.length()     // Catch:{ all -> 0x0023 }
            if (r3 == 0) goto L_0x0343
            java.lang.String r1 = r2.concat(r1)     // Catch:{ all -> 0x0023 }
        L_0x0339:
            r14.m6012(r1)     // Catch:{ all -> 0x0023 }
            android.widget.RelativeLayout r2 = r14.f5521     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0349
            throw r13     // Catch:{ all -> 0x0023 }
        L_0x0343:
            java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x0023 }
            r1.<init>(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x0339
        L_0x0349:
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x0023 }
            r2.removeView(r1)     // Catch:{ all -> 0x0023 }
            android.view.ViewGroup r1 = r14.f5522     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x036c
            android.view.ViewGroup r1 = r14.f5522     // Catch:{ all -> 0x0023 }
            android.widget.ImageView r2 = r14.f5518     // Catch:{ all -> 0x0023 }
            r1.removeView(r2)     // Catch:{ all -> 0x0023 }
            android.view.ViewGroup r2 = r14.f5522     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0360
            throw r13     // Catch:{ all -> 0x0023 }
        L_0x0360:
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x0023 }
            r2.addView(r1)     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzanh r1 = r14.f5525     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzapa r2 = r14.f5517     // Catch:{ all -> 0x0023 }
            r1.m5011((com.google.android.gms.internal.zzapa) r2)     // Catch:{ all -> 0x0023 }
        L_0x036c:
            monitor-exit(r7)     // Catch:{ all -> 0x0023 }
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzwr.m5999(java.util.Map):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6000(boolean z) {
        synchronized (this.f5524) {
            if (this.f5520 != null) {
                this.f5520.dismiss();
                RelativeLayout relativeLayout = this.f5521;
                zzanh zzanh = this.f5525;
                if (zzanh == null) {
                    throw null;
                }
                relativeLayout.removeView((View) zzanh);
                if (this.f5522 != null) {
                    this.f5522.removeView(this.f5518);
                    ViewGroup viewGroup = this.f5522;
                    zzanh zzanh2 = this.f5525;
                    if (zzanh2 == null) {
                        throw null;
                    }
                    viewGroup.addView((View) zzanh2);
                    this.f5525.m5011(this.f5517);
                }
                if (z) {
                    m6009("default");
                    if (this.f5531 != null) {
                        this.f5531.zzcu();
                    }
                }
                this.f5520 = null;
                this.f5521 = null;
                this.f5522 = null;
                this.f5530 = null;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m6001() {
        boolean z;
        synchronized (this.f5524) {
            z = this.f5520 != null;
        }
        return z;
    }
}
