package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzbq;

public final class zzcjm {

    /* renamed from: 龘  reason: contains not printable characters */
    final Context f9489;

    public zzcjm(Context context) {
        zzbq.m9120(context);
        Context applicationContext = context.getApplicationContext();
        zzbq.m9120(applicationContext);
        this.f9489 = applicationContext;
    }
}
