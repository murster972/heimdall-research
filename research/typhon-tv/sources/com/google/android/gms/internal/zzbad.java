package com.google.android.gms.internal;

import android.content.Context;
import android.view.View;
import com.google.android.gms.R;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import java.util.Iterator;
import java.util.List;

public final class zzbad extends UIController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8555;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f8556;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8557;

    public zzbad(View view, Context context) {
        this.f8557 = view;
        this.f8555 = context.getString(R.string.cast_closed_captions);
        this.f8556 = context.getString(R.string.cast_closed_captions_unavailable);
        this.f8557.setEnabled(false);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9876() {
        boolean z;
        int i;
        RemoteMediaClient r4 = m8226();
        if (r4 != null && r4.m4143()) {
            MediaInfo r0 = r4.m4137();
            if (r0 != null) {
                List<MediaTrack> r02 = r0.m7838();
                if (r02 != null && !r02.isEmpty()) {
                    Iterator<MediaTrack> it2 = r02.iterator();
                    int i2 = 0;
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        MediaTrack next = it2.next();
                        if (next.m7929() == 2) {
                            i = i2 + 1;
                            if (i > 1) {
                                z = true;
                                break;
                            }
                        } else if (next.m7929() == 1) {
                            z = true;
                            break;
                        } else {
                            i = i2;
                        }
                        i2 = i;
                    }
                } else {
                    z = false;
                    if (z && !r4.m4144()) {
                        this.f8557.setEnabled(true);
                        this.f8557.setContentDescription(this.f8555);
                        return;
                    }
                }
            }
            z = false;
            this.f8557.setEnabled(true);
            this.f8557.setContentDescription(this.f8555);
            return;
        }
        this.f8557.setEnabled(false);
        this.f8557.setContentDescription(this.f8556);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9877() {
        this.f8557.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9878() {
        this.f8557.setEnabled(false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9879() {
        m9876();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9880(CastSession castSession) {
        super.m8227(castSession);
        this.f8557.setEnabled(true);
        m9876();
    }
}
