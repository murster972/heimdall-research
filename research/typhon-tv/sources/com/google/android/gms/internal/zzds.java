package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzds extends zzbt<Integer, Long> {

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f10020;

    /* renamed from: 齉  reason: contains not printable characters */
    public Long f10021;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f10022;

    public zzds() {
    }

    public zzds(String str) {
        m10294(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<Integer, Long> m11901() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.f10022);
        hashMap.put(1, this.f10020);
        hashMap.put(2, this.f10021);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11902(String str) {
        HashMap r1 = m10292(str);
        if (r1 != null) {
            this.f10022 = (Long) r1.get(0);
            this.f10020 = (Long) r1.get(1);
            this.f10021 = (Long) r1.get(2);
        }
    }
}
