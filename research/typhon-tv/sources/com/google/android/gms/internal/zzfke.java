package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfke extends zzfjm<zzfke> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzfke[] f10604;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzfkb f10605 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Integer f10606 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int[] f10607 = zzfjv.f10563;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f10608 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public String[] f10609 = zzfjv.f10552;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f10610 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer f10611 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzfjz f10612 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f10613 = null;

    public zzfke() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzfke m12914(com.google.android.gms.internal.zzfjj r8) throws java.io.IOException {
        /*
            r7 = this;
            r1 = 0
        L_0x0001:
            int r0 = r8.m12800()
            switch(r0) {
                case 0: goto L_0x000e;
                case 8: goto L_0x000f;
                case 18: goto L_0x001a;
                case 26: goto L_0x0021;
                case 34: goto L_0x0032;
                case 40: goto L_0x0043;
                case 48: goto L_0x004e;
                case 50: goto L_0x0081;
                case 58: goto L_0x00c3;
                case 64: goto L_0x00cb;
                case 74: goto L_0x0103;
                default: goto L_0x0008;
            }
        L_0x0008:
            boolean r0 = super.m12843(r8, r0)
            if (r0 != 0) goto L_0x0001
        L_0x000e:
            return r7
        L_0x000f:
            int r0 = r8.m12798()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r7.f10613 = r0
            goto L_0x0001
        L_0x001a:
            java.lang.String r0 = r8.m12791()
            r7.f10610 = r0
            goto L_0x0001
        L_0x0021:
            com.google.android.gms.internal.zzfjz r0 = r7.f10612
            if (r0 != 0) goto L_0x002c
            com.google.android.gms.internal.zzfjz r0 = new com.google.android.gms.internal.zzfjz
            r0.<init>()
            r7.f10612 = r0
        L_0x002c:
            com.google.android.gms.internal.zzfjz r0 = r7.f10612
            r8.m12802((com.google.android.gms.internal.zzfjs) r0)
            goto L_0x0001
        L_0x0032:
            com.google.android.gms.internal.zzfkb r0 = r7.f10605
            if (r0 != 0) goto L_0x003d
            com.google.android.gms.internal.zzfkb r0 = new com.google.android.gms.internal.zzfkb
            r0.<init>()
            r7.f10605 = r0
        L_0x003d:
            com.google.android.gms.internal.zzfkb r0 = r7.f10605
            r8.m12802((com.google.android.gms.internal.zzfjs) r0)
            goto L_0x0001
        L_0x0043:
            int r0 = r8.m12798()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r7.f10606 = r0
            goto L_0x0001
        L_0x004e:
            r0 = 48
            int r2 = com.google.android.gms.internal.zzfjv.m12883(r8, r0)
            int[] r0 = r7.f10607
            if (r0 != 0) goto L_0x0074
            r0 = r1
        L_0x0059:
            int r2 = r2 + r0
            int[] r2 = new int[r2]
            if (r0 == 0) goto L_0x0063
            int[] r3 = r7.f10607
            java.lang.System.arraycopy(r3, r1, r2, r1, r0)
        L_0x0063:
            int r3 = r2.length
            int r3 = r3 + -1
            if (r0 >= r3) goto L_0x0078
            int r3 = r8.m12798()
            r2[r0] = r3
            r8.m12800()
            int r0 = r0 + 1
            goto L_0x0063
        L_0x0074:
            int[] r0 = r7.f10607
            int r0 = r0.length
            goto L_0x0059
        L_0x0078:
            int r3 = r8.m12798()
            r2[r0] = r3
            r7.f10607 = r2
            goto L_0x0001
        L_0x0081:
            int r0 = r8.m12785()
            int r3 = r8.m12799(r0)
            int r2 = r8.m12787()
            r0 = r1
        L_0x008e:
            int r4 = r8.m12790()
            if (r4 <= 0) goto L_0x009a
            r8.m12798()
            int r0 = r0 + 1
            goto L_0x008e
        L_0x009a:
            r8.m12792(r2)
            int[] r2 = r7.f10607
            if (r2 != 0) goto L_0x00b8
            r2 = r1
        L_0x00a2:
            int r0 = r0 + r2
            int[] r0 = new int[r0]
            if (r2 == 0) goto L_0x00ac
            int[] r4 = r7.f10607
            java.lang.System.arraycopy(r4, r1, r0, r1, r2)
        L_0x00ac:
            int r4 = r0.length
            if (r2 >= r4) goto L_0x00bc
            int r4 = r8.m12798()
            r0[r2] = r4
            int r2 = r2 + 1
            goto L_0x00ac
        L_0x00b8:
            int[] r2 = r7.f10607
            int r2 = r2.length
            goto L_0x00a2
        L_0x00bc:
            r7.f10607 = r0
            r8.m12796(r3)
            goto L_0x0001
        L_0x00c3:
            java.lang.String r0 = r8.m12791()
            r7.f10608 = r0
            goto L_0x0001
        L_0x00cb:
            int r2 = r8.m12787()
            int r3 = r8.m12798()     // Catch:{ IllegalArgumentException -> 0x00f2 }
            switch(r3) {
                case 0: goto L_0x00fb;
                case 1: goto L_0x00fb;
                case 2: goto L_0x00fb;
                case 3: goto L_0x00fb;
                default: goto L_0x00d6;
            }     // Catch:{ IllegalArgumentException -> 0x00f2 }
        L_0x00d6:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00f2 }
            r5 = 46
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00f2 }
            r6.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x00f2 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ IllegalArgumentException -> 0x00f2 }
            java.lang.String r5 = " is not a valid enum AdResourceType"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IllegalArgumentException -> 0x00f2 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x00f2 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x00f2 }
            throw r4     // Catch:{ IllegalArgumentException -> 0x00f2 }
        L_0x00f2:
            r3 = move-exception
            r8.m12792(r2)
            r7.m12843(r8, r0)
            goto L_0x0001
        L_0x00fb:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x00f2 }
            r7.f10611 = r3     // Catch:{ IllegalArgumentException -> 0x00f2 }
            goto L_0x0001
        L_0x0103:
            r0 = 74
            int r2 = com.google.android.gms.internal.zzfjv.m12883(r8, r0)
            java.lang.String[] r0 = r7.f10609
            if (r0 != 0) goto L_0x0129
            r0 = r1
        L_0x010e:
            int r2 = r2 + r0
            java.lang.String[] r2 = new java.lang.String[r2]
            if (r0 == 0) goto L_0x0118
            java.lang.String[] r3 = r7.f10609
            java.lang.System.arraycopy(r3, r1, r2, r1, r0)
        L_0x0118:
            int r3 = r2.length
            int r3 = r3 + -1
            if (r0 >= r3) goto L_0x012d
            java.lang.String r3 = r8.m12791()
            r2[r0] = r3
            r8.m12800()
            int r0 = r0 + 1
            goto L_0x0118
        L_0x0129:
            java.lang.String[] r0 = r7.f10609
            int r0 = r0.length
            goto L_0x010e
        L_0x012d:
            java.lang.String r3 = r8.m12791()
            r2[r0] = r3
            r7.f10609 = r2
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfke.m12914(com.google.android.gms.internal.zzfjj):com.google.android.gms.internal.zzfke");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzfke[] m12912() {
        if (f10604 == null) {
            synchronized (zzfjq.f10546) {
                if (f10604 == null) {
                    f10604 = new zzfke[0];
                }
            }
        }
        return f10604;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12913() {
        int r0 = super.m12841() + zzfjk.m12806(1, this.f10613.intValue());
        if (this.f10610 != null) {
            r0 += zzfjk.m12808(2, this.f10610);
        }
        if (this.f10612 != null) {
            r0 += zzfjk.m12807(3, (zzfjs) this.f10612);
        }
        if (this.f10605 != null) {
            r0 += zzfjk.m12807(4, (zzfjs) this.f10605);
        }
        if (this.f10606 != null) {
            r0 += zzfjk.m12806(5, this.f10606.intValue());
        }
        if (this.f10607 != null && this.f10607.length > 0) {
            int i = 0;
            for (int r4 : this.f10607) {
                i += zzfjk.m12816(r4);
            }
            r0 = r0 + i + (this.f10607.length * 1);
        }
        if (this.f10608 != null) {
            r0 += zzfjk.m12808(7, this.f10608);
        }
        if (this.f10611 != null) {
            r0 += zzfjk.m12806(8, this.f10611.intValue());
        }
        if (this.f10609 == null || this.f10609.length <= 0) {
            return r0;
        }
        int i2 = 0;
        int i3 = 0;
        for (String str : this.f10609) {
            if (str != null) {
                i3++;
                i2 += zzfjk.m12820(str);
            }
        }
        return r0 + i2 + (i3 * 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12915(zzfjk zzfjk) throws IOException {
        zzfjk.m12832(1, this.f10613.intValue());
        if (this.f10610 != null) {
            zzfjk.m12835(2, this.f10610);
        }
        if (this.f10612 != null) {
            zzfjk.m12834(3, (zzfjs) this.f10612);
        }
        if (this.f10605 != null) {
            zzfjk.m12834(4, (zzfjs) this.f10605);
        }
        if (this.f10606 != null) {
            zzfjk.m12832(5, this.f10606.intValue());
        }
        if (this.f10607 != null && this.f10607.length > 0) {
            for (int r3 : this.f10607) {
                zzfjk.m12832(6, r3);
            }
        }
        if (this.f10608 != null) {
            zzfjk.m12835(7, this.f10608);
        }
        if (this.f10611 != null) {
            zzfjk.m12832(8, this.f10611.intValue());
        }
        if (this.f10609 != null && this.f10609.length > 0) {
            for (String str : this.f10609) {
                if (str != null) {
                    zzfjk.m12835(9, str);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
