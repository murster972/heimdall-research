package com.google.android.gms.internal;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzvt extends zzvk {

    /* renamed from: 龘  reason: contains not printable characters */
    private final NativeAppInstallAdMapper f5488;

    public zzvt(NativeAppInstallAdMapper nativeAppInstallAdMapper) {
        this.f5488 = nativeAppInstallAdMapper;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final double m5918() {
        return this.f5488.getStarRating();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5919() {
        return this.f5488.getStore();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m5920() {
        return this.f5488.getPrice();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzll m5921() {
        if (this.f5488.getVideoController() != null) {
            return this.f5488.getVideoController().zzbj();
        }
        return null;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final IObjectWrapper m5922() {
        View adChoicesContent = this.f5488.getAdChoicesContent();
        if (adChoicesContent == null) {
            return null;
        }
        return zzn.m9306(adChoicesContent);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final Bundle m5923() {
        return this.f5488.getExtras();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final IObjectWrapper m5924() {
        return null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m5925() {
        this.f5488.recordImpression();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m5926() {
        return this.f5488.getOverrideImpressionRecording();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m5927() {
        return this.f5488.getOverrideClickHandling();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m5928() {
        return this.f5488.getCallToAction();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m5929() {
        List<NativeAd.Image> images = this.f5488.getImages();
        if (images == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (NativeAd.Image next : images) {
            arrayList.add(new zzoi(next.getDrawable(), next.getUri(), next.getScale()));
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5930(IObjectWrapper iObjectWrapper) {
        this.f5488.trackView((View) zzn.m9307(iObjectWrapper));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzpq m5931() {
        NativeAd.Image icon = this.f5488.getIcon();
        if (icon != null) {
            return new zzoi(icon.getDrawable(), icon.getUri(), icon.getScale());
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m5932() {
        return this.f5488.getBody();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5933(IObjectWrapper iObjectWrapper) {
        this.f5488.untrackView((View) zzn.m9307(iObjectWrapper));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5934() {
        return this.f5488.getHeadline();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5935(IObjectWrapper iObjectWrapper) {
        this.f5488.handleClick((View) zzn.m9307(iObjectWrapper));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final zzpm m5936() {
        return null;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final IObjectWrapper m5937() {
        View zzul = this.f5488.zzul();
        if (zzul == null) {
            return null;
        }
        return zzn.m9306(zzul);
    }
}
