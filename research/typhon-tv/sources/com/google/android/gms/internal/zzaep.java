package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzaep implements MediationRewardedVideoAdListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaem f4054;

    public zzaep(zzaem zzaem) {
        this.f4054 = zzaem;
    }

    public final void onAdClicked(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onAdClicked must be called on the main UI thread.");
        zzakb.m4792("Adapter called onAdClicked.");
        try {
            this.f4054.m9539(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClicked.", e);
        }
    }

    public final void onAdClosed(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onAdClosed must be called on the main UI thread.");
        zzakb.m4792("Adapter called onAdClosed.");
        try {
            this.f4054.m9541(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClosed.", e);
        }
    }

    public final void onAdFailedToLoad(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, int i) {
        zzbq.m9115("onAdFailedToLoad must be called on the main UI thread.");
        zzakb.m4792("Adapter called onAdFailedToLoad.");
        try {
            this.f4054.m9543(zzn.m9306(mediationRewardedVideoAdAdapter), i);
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdFailedToLoad.", e);
        }
    }

    public final void onAdLeftApplication(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onAdLeftApplication must be called on the main UI thread.");
        zzakb.m4792("Adapter called onAdLeftApplication.");
        try {
            this.f4054.m9540(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLeftApplication.", e);
        }
    }

    public final void onAdLoaded(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onAdLoaded must be called on the main UI thread.");
        zzakb.m4792("Adapter called onAdLoaded.");
        try {
            this.f4054.m9542(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLoaded.", e);
        }
    }

    public final void onAdOpened(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onAdOpened must be called on the main UI thread.");
        zzakb.m4792("Adapter called onAdOpened.");
        try {
            this.f4054.m9545(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdOpened.", e);
        }
    }

    public final void onInitializationFailed(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, int i) {
        zzbq.m9115("onInitializationFailed must be called on the main UI thread.");
        zzakb.m4792("Adapter called onInitializationFailed.");
        try {
            this.f4054.m9547(zzn.m9306(mediationRewardedVideoAdAdapter), i);
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onInitializationFailed.", e);
        }
    }

    public final void onInitializationSucceeded(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onInitializationSucceeded must be called on the main UI thread.");
        zzakb.m4792("Adapter called onInitializationSucceeded.");
        try {
            this.f4054.m9546(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onInitializationSucceeded.", e);
        }
    }

    public final void onRewarded(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, RewardItem rewardItem) {
        zzbq.m9115("onRewarded must be called on the main UI thread.");
        zzakb.m4792("Adapter called onRewarded.");
        if (rewardItem != null) {
            try {
                this.f4054.m9548(zzn.m9306(mediationRewardedVideoAdAdapter), new zzaeq(rewardItem));
            } catch (RemoteException e) {
                zzakb.m4796("Could not call onRewarded.", e);
            }
        } else {
            this.f4054.m9548(zzn.m9306(mediationRewardedVideoAdAdapter), new zzaeq("", 1));
        }
    }

    public final void onVideoStarted(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        zzbq.m9115("onVideoStarted must be called on the main UI thread.");
        zzakb.m4792("Adapter called onVideoStarted.");
        try {
            this.f4054.m9544(zzn.m9306(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onVideoStarted.", e);
        }
    }
}
