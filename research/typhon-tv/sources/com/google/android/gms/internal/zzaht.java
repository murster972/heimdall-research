package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Process;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import com.google.android.gms.ads.internal.zzbs;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

@zzzv
@TargetApi(8)
public class zzaht {
    private zzaht() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m4640() {
        int myUid = Process.myUid();
        return myUid == 0 || myUid == 1000;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4641(zzanh zzanh) {
        if (zzanh == null) {
            return false;
        }
        zzanh.onResume();
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4642(zzanh zzanh) {
        if (zzanh == null) {
            return false;
        }
        zzanh.onPause();
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m4643() {
        return 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m4644() {
        return 1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m4645(Context context) {
        zzbs.zzem().m4478();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m4646(View view) {
        return false;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ViewGroup.LayoutParams m4647() {
        return new ViewGroup.LayoutParams(-2, -2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m4648() {
        return 5;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public CookieManager m4649(Context context) {
        if (m4640()) {
            return null;
        }
        try {
            CookieSyncManager.createInstance(context);
            return CookieManager.getInstance();
        } catch (Throwable th) {
            zzagf.m4793("Failed to obtain CookieManager.", th);
            zzbs.zzem().m4505(th, "ApiLevelUtil.getCookieManager");
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public WebChromeClient m4650(zzanh zzanh) {
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m4651(View view) {
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m4652() {
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Drawable m4653(Context context, Bitmap bitmap, boolean z, float f) {
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public zzani m4654(zzanh zzanh, boolean z) {
        return new zzani(zzanh, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m4655(Context context) {
        return "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m4656(SslError sslError) {
        return "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<String> m4657(Uri uri) {
        if (uri.isOpaque()) {
            return Collections.emptySet();
        }
        String encodedQuery = uri.getEncodedQuery();
        if (encodedQuery == null) {
            return Collections.emptySet();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int i = 0;
        do {
            int indexOf = encodedQuery.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = encodedQuery.length();
            }
            int indexOf2 = encodedQuery.indexOf(61, i);
            if (indexOf2 > indexOf || indexOf2 == -1) {
                indexOf2 = indexOf;
            }
            linkedHashSet.add(Uri.decode(encodedQuery.substring(i, indexOf2)));
            i = indexOf + 1;
        } while (i < encodedQuery.length());
        return Collections.unmodifiableSet(linkedHashSet);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4658(Activity activity, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        Window window = activity.getWindow();
        if (window != null && window.getDecorView() != null && window.getDecorView().getViewTreeObserver() != null) {
            m4660(window.getDecorView().getViewTreeObserver(), onGlobalLayoutListener);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4659(View view, Drawable drawable) {
        view.setBackgroundDrawable(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4660(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        viewTreeObserver.removeGlobalOnLayoutListener(onGlobalLayoutListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m4661(DownloadManager.Request request) {
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m4662(Context context, WebSettings webSettings) {
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m4663(View view) {
        return (view.getWindowToken() == null && view.getWindowVisibility() == 8) ? false : true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m4664(Window window) {
        return false;
    }
}
