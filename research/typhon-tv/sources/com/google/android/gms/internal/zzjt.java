package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;

final class zzjt extends zzjr.zza<zzks> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzjn f10760;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzjr f10761;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f10762;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f10763;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjt(zzjr zzjr, Context context, zzjn zzjn, String str) {
        super();
        this.f10761 = zzjr;
        this.f10763 = context;
        this.f10760 = zzjn;
        this.f10762 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13037() throws RemoteException {
        zzks r0 = this.f10761.f4809.m5447(this.f10763, this.f10760, this.f10762, (zzux) null, 3);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10763, "search");
        return new zzmg();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13038(zzla zzla) throws RemoteException {
        return zzla.createSearchAdManager(zzn.m9306(this.f10763), this.f10760, this.f10762, 11910000);
    }
}
