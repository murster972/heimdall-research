package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzan;

public interface zzcxl extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m11562(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m11563(zzan zzan, int i, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m11564(zzcxo zzcxo, zzcxj zzcxj) throws RemoteException;
}
