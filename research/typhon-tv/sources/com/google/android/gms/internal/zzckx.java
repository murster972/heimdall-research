package com.google.android.gms.internal;

final class zzckx implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcku f9615;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzche f9616;

    zzckx(zzcku zzcku, zzche zzche) {
        this.f9615 = zzcku;
        this.f9616 = zzche;
    }

    public final void run() {
        synchronized (this.f9615) {
            boolean unused = this.f9615.f9608 = false;
            if (!this.f9615.f9610.m11251()) {
                this.f9615.f9610.m11096().m10845().m10849("Connected to remote service");
                this.f9615.f9610.m11263(this.f9616);
            }
        }
    }
}
