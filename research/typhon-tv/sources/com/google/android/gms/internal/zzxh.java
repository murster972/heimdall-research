package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzxh extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m13632(IObjectWrapper iObjectWrapper) throws RemoteException;
}
