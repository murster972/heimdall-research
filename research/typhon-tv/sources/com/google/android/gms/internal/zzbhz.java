package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbhz extends zzbfm {
    public static final Parcelable.Creator<zzbhz> CREATOR = new zzbia();

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f8785;

    public zzbhz(byte[] bArr) {
        this.f8785 = bArr;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10196(parcel, 2, this.f8785, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m10278() {
        return this.f8785;
    }
}
