package com.google.android.gms.internal;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzuu implements zzug {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f5465;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final long f5466;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final long f5467;

    /* renamed from: ʾ  reason: contains not printable characters */
    private zzul f5468;

    /* renamed from: ʿ  reason: contains not printable characters */
    private List<zzuo> f5469 = new ArrayList();

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f5470 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zznu f5471;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f5472;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final String f5473;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzui f5474;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzux f5475;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Object f5476 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f5477;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaat f5478;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final boolean f5479;

    public zzuu(Context context, zzaat zzaat, zzux zzux, zzui zzui, boolean z, boolean z2, String str, long j, long j2, zznu zznu, boolean z3) {
        this.f5477 = context;
        this.f5478 = zzaat;
        this.f5475 = zzux;
        this.f5474 = zzui;
        this.f5465 = z;
        this.f5472 = z2;
        this.f5473 = str;
        this.f5466 = j;
        this.f5467 = j2;
        this.f5471 = zznu;
        this.f5479 = z3;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzuo> m5910() {
        return this.f5469;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ef, code lost:
        r2 = r24.f5468.龘(r24.f5466, r24.f5467);
        r24.f5469.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0108, code lost:
        if (r2.f5449 != 0) goto L_0x015c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x010a, code lost:
        com.google.android.gms.internal.zzagf.m4792("Adapter succeeded.");
        r24.f5471.m5629("mediation_network_succeed", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011e, code lost:
        if (r18.isEmpty() != false) goto L_0x0133;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0120, code lost:
        r24.f5471.m5629("mediation_networks_fail", android.text.TextUtils.join(",", r18));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0133, code lost:
        r24.f5471.m5631(r22, "mls");
        r24.f5471.m5631(r19, "ttm");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x015c, code lost:
        r18.add(r4);
        r24.f5471.m5631(r22, "mlf");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0175, code lost:
        if (r2.f5448 == null) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0177, code lost:
        com.google.android.gms.internal.zzahn.f4212.post(new com.google.android.gms.internal.zzuv(r24, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return r2;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzuo m5911(java.util.List<com.google.android.gms.internal.zzuh> r25) {
        /*
            r24 = this;
            java.lang.String r2 = "Starting mediation."
            com.google.android.gms.internal.zzagf.m4792(r2)
            java.util.ArrayList r18 = new java.util.ArrayList
            r18.<init>()
            r0 = r24
            com.google.android.gms.internal.zznu r2 = r0.f5471
            com.google.android.gms.internal.zzns r19 = r2.m5625()
            r0 = r24
            com.google.android.gms.internal.zzaat r2 = r0.f5478
            com.google.android.gms.internal.zzjn r2 = r2.f3762
            r3 = 2
            int[] r3 = new int[r3]
            com.google.android.gms.internal.zzjn[] r4 = r2.f4790
            if (r4 == 0) goto L_0x01a6
            com.google.android.gms.ads.internal.zzbs.zzez()
            r0 = r24
            java.lang.String r4 = r0.f5473
            boolean r4 = com.google.android.gms.internal.zzuq.m5898((java.lang.String) r4, (int[]) r3)
            if (r4 == 0) goto L_0x01a6
            r4 = 0
            r4 = r3[r4]
            r5 = 1
            r5 = r3[r5]
            com.google.android.gms.internal.zzjn[] r6 = r2.f4790
            int r7 = r6.length
            r3 = 0
        L_0x0037:
            if (r3 >= r7) goto L_0x01a6
            r9 = r6[r3]
            int r8 = r9.f4794
            if (r4 != r8) goto L_0x0098
            int r8 = r9.f4795
            if (r5 != r8) goto L_0x0098
        L_0x0043:
            java.util.Iterator r20 = r25.iterator()
        L_0x0047:
            boolean r2 = r20.hasNext()
            if (r2 == 0) goto L_0x0185
            java.lang.Object r7 = r20.next()
            com.google.android.gms.internal.zzuh r7 = (com.google.android.gms.internal.zzuh) r7
            java.lang.String r3 = "Trying mediation network: "
            java.lang.String r2 = r7.f5415
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r4 = r2.length()
            if (r4 == 0) goto L_0x009b
            java.lang.String r2 = r3.concat(r2)
        L_0x0066:
            com.google.android.gms.internal.zzagf.m4794(r2)
            java.util.List<java.lang.String> r2 = r7.f5417
            java.util.Iterator r21 = r2.iterator()
        L_0x006f:
            boolean r2 = r21.hasNext()
            if (r2 == 0) goto L_0x0047
            java.lang.Object r4 = r21.next()
            java.lang.String r4 = (java.lang.String) r4
            r0 = r24
            com.google.android.gms.internal.zznu r2 = r0.f5471
            com.google.android.gms.internal.zzns r22 = r2.m5625()
            r0 = r24
            java.lang.Object r0 = r0.f5476
            r23 = r0
            monitor-enter(r23)
            r0 = r24
            boolean r2 = r0.f5470     // Catch:{ all -> 0x0159 }
            if (r2 == 0) goto L_0x00a1
            com.google.android.gms.internal.zzuo r2 = new com.google.android.gms.internal.zzuo     // Catch:{ all -> 0x0159 }
            r3 = -1
            r2.<init>(r3)     // Catch:{ all -> 0x0159 }
            monitor-exit(r23)     // Catch:{ all -> 0x0159 }
        L_0x0097:
            return r2
        L_0x0098:
            int r3 = r3 + 1
            goto L_0x0037
        L_0x009b:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r3)
            goto L_0x0066
        L_0x00a1:
            com.google.android.gms.internal.zzul r2 = new com.google.android.gms.internal.zzul     // Catch:{ all -> 0x0159 }
            r0 = r24
            android.content.Context r3 = r0.f5477     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzux r5 = r0.f5475     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzui r6 = r0.f5474     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzaat r8 = r0.f5478     // Catch:{ all -> 0x0159 }
            com.google.android.gms.internal.zzjj r8 = r8.f3763     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzaat r10 = r0.f5478     // Catch:{ all -> 0x0159 }
            com.google.android.gms.internal.zzakd r10 = r10.f3748     // Catch:{ all -> 0x0159 }
            r0 = r24
            boolean r11 = r0.f5465     // Catch:{ all -> 0x0159 }
            r0 = r24
            boolean r12 = r0.f5472     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzaat r13 = r0.f5478     // Catch:{ all -> 0x0159 }
            com.google.android.gms.internal.zzpe r13 = r13.f3750     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzaat r14 = r0.f5478     // Catch:{ all -> 0x0159 }
            java.util.List<java.lang.String> r14 = r14.f3724     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzaat r15 = r0.f5478     // Catch:{ all -> 0x0159 }
            java.util.List<java.lang.String> r15 = r15.f3752     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzaat r0 = r0.f5478     // Catch:{ all -> 0x0159 }
            r16 = r0
            r0 = r16
            java.util.List<java.lang.String> r0 = r0.f3753     // Catch:{ all -> 0x0159 }
            r16 = r0
            r0 = r24
            boolean r0 = r0.f5479     // Catch:{ all -> 0x0159 }
            r17 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x0159 }
            r0 = r24
            r0.f5468 = r2     // Catch:{ all -> 0x0159 }
            monitor-exit(r23)     // Catch:{ all -> 0x0159 }
            r0 = r24
            com.google.android.gms.internal.zzul r2 = r0.f5468
            r0 = r24
            long r10 = r0.f5466
            r0 = r24
            long r12 = r0.f5467
            com.google.android.gms.internal.zzuo r2 = r2.龘(r10, r12)
            r0 = r24
            java.util.List<com.google.android.gms.internal.zzuo> r3 = r0.f5469
            r3.add(r2)
            int r3 = r2.f5449
            if (r3 != 0) goto L_0x015c
            java.lang.String r3 = "Adapter succeeded."
            com.google.android.gms.internal.zzagf.m4792(r3)
            r0 = r24
            com.google.android.gms.internal.zznu r3 = r0.f5471
            java.lang.String r5 = "mediation_network_succeed"
            r3.m5629((java.lang.String) r5, (java.lang.String) r4)
            boolean r3 = r18.isEmpty()
            if (r3 != 0) goto L_0x0133
            r0 = r24
            com.google.android.gms.internal.zznu r3 = r0.f5471
            java.lang.String r4 = "mediation_networks_fail"
            java.lang.String r5 = ","
            r0 = r18
            java.lang.String r5 = android.text.TextUtils.join(r5, r0)
            r3.m5629((java.lang.String) r4, (java.lang.String) r5)
        L_0x0133:
            r0 = r24
            com.google.android.gms.internal.zznu r3 = r0.f5471
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]
            r5 = 0
            java.lang.String r6 = "mls"
            r4[r5] = r6
            r0 = r22
            r3.m5631((com.google.android.gms.internal.zzns) r0, (java.lang.String[]) r4)
            r0 = r24
            com.google.android.gms.internal.zznu r3 = r0.f5471
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]
            r5 = 0
            java.lang.String r6 = "ttm"
            r4[r5] = r6
            r0 = r19
            r3.m5631((com.google.android.gms.internal.zzns) r0, (java.lang.String[]) r4)
            goto L_0x0097
        L_0x0159:
            r2 = move-exception
            monitor-exit(r23)     // Catch:{ all -> 0x0159 }
            throw r2
        L_0x015c:
            r0 = r18
            r0.add(r4)
            r0 = r24
            com.google.android.gms.internal.zznu r3 = r0.f5471
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]
            r5 = 0
            java.lang.String r6 = "mlf"
            r4[r5] = r6
            r0 = r22
            r3.m5631((com.google.android.gms.internal.zzns) r0, (java.lang.String[]) r4)
            com.google.android.gms.internal.zzva r3 = r2.f5448
            if (r3 == 0) goto L_0x006f
            android.os.Handler r3 = com.google.android.gms.internal.zzahn.f4212
            com.google.android.gms.internal.zzuv r4 = new com.google.android.gms.internal.zzuv
            r0 = r24
            r4.<init>(r0, r2)
            r3.post(r4)
            goto L_0x006f
        L_0x0185:
            boolean r2 = r18.isEmpty()
            if (r2 != 0) goto L_0x019e
            r0 = r24
            com.google.android.gms.internal.zznu r2 = r0.f5471
            java.lang.String r3 = "mediation_networks_fail"
            java.lang.String r4 = ","
            r0 = r18
            java.lang.String r4 = android.text.TextUtils.join(r4, r0)
            r2.m5629((java.lang.String) r3, (java.lang.String) r4)
        L_0x019e:
            com.google.android.gms.internal.zzuo r2 = new com.google.android.gms.internal.zzuo
            r3 = 1
            r2.<init>(r3)
            goto L_0x0097
        L_0x01a6:
            r9 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzuu.m5911(java.util.List):com.google.android.gms.internal.zzuo");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5912() {
        synchronized (this.f5476) {
            this.f5470 = true;
            if (this.f5468 != null) {
                this.f5468.龘();
            }
        }
    }
}
