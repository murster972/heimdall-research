package com.google.android.gms.internal;

import java.util.Map;
import org.apache.oltu.oauth2.common.OAuth;

public final class zzajd extends zzr<zzp> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, String> f8252;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzajv f8253;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzalf<zzp> f8254;

    public zzajd(String str, zzalf<zzp> zzalf) {
        this(str, (Map<String, String>) null, zzalf);
    }

    private zzajd(String str, Map<String, String> map, zzalf<zzp> zzalf) {
        super(0, str, new zzaje(zzalf));
        this.f8252 = null;
        this.f8254 = zzalf;
        this.f8253 = new zzajv();
        this.f8253.m4786(str, OAuth.HttpMethod.GET, (Map<String, ?>) null, (byte[]) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzw<zzp> m9654(zzp zzp) {
        return zzw.m13620(zzp, zzao.m9715(zzp));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m9655(Object obj) {
        zzp zzp = (zzp) obj;
        this.f8253.m4789((Map<String, ?>) zzp.f5280, zzp.f5281);
        zzajv zzajv = this.f8253;
        byte[] bArr = zzp.f5278;
        if (zzajv.m4775() && bArr != null) {
            zzajv.m4790(bArr);
        }
        this.f8254.m4822(zzp);
    }
}
