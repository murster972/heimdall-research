package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzzv
public final class zzamz implements Iterable<zzamx> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<zzamx> f4435 = new LinkedList();

    /* renamed from: 靐  reason: contains not printable characters */
    static zzamx m4953(zzamp zzamp) {
        Iterator<zzamx> it2 = zzbs.zzfb().iterator();
        while (it2.hasNext()) {
            zzamx next = it2.next();
            if (next.f4434 == zzamp) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4954(zzamp zzamp) {
        zzamx r0 = m4953(zzamp);
        if (r0 == null) {
            return false;
        }
        r0.f4432.m4962();
        return true;
    }

    public final Iterator<zzamx> iterator() {
        return this.f4435.iterator();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4955(zzamx zzamx) {
        this.f4435.remove(zzamx);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m4956() {
        return this.f4435.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4957(zzamx zzamx) {
        this.f4435.add(zzamx);
    }
}
