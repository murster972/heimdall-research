package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

public abstract class zzfes implements Serializable, Iterable<Byte> {
    public static final zzfes zzpfg = new zzfez(zzffz.f10420);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfew f10345;
    private int zzmal = 0;

    static {
        boolean z = true;
        try {
            Class.forName("android.content.Context");
        } catch (ClassNotFoundException e) {
            z = false;
        }
        f10345 = z ? new zzffa((zzfet) null) : new zzfeu((zzfet) null);
    }

    zzfes() {
    }

    public static zzfes zzaz(byte[] bArr) {
        return zze(bArr, 0, bArr.length);
    }

    public static zzfes zze(byte[] bArr, int i, int i2) {
        return new zzfez(f10345.m12385(bArr, i, i2));
    }

    public static zzfes zzf(Iterable<zzfes> iterable) {
        int size = ((Collection) iterable).size();
        return size == 0 ? zzpfg : m12371(iterable.iterator(), size);
    }

    public static zzfes zztr(String str) {
        return new zzfez(str.getBytes(zzffz.f10423));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static int m12370(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException(new StringBuilder(32).append("Beginning index: ").append(i).append(" < 0").toString());
        } else if (i2 < i) {
            throw new IndexOutOfBoundsException(new StringBuilder(66).append("Beginning index larger than ending index: ").append(i).append(", ").append(i2).toString());
        } else {
            throw new IndexOutOfBoundsException(new StringBuilder(37).append("End index: ").append(i2).append(" >= ").append(i3).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzfes m12371(Iterator<zzfes> it2, int i) {
        if (i <= 0) {
            throw new IllegalArgumentException(String.format("length (%s) must be >= 1", new Object[]{Integer.valueOf(i)}));
        } else if (i == 1) {
            return it2.next();
        } else {
            int i2 = i >>> 1;
            zzfes r1 = m12371(it2, i2);
            zzfes r0 = m12371(it2, i - i2);
            if (MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - r1.size() >= r0.size()) {
                return zzfhq.m12653(r1, r0);
            }
            int size = r1.size();
            throw new IllegalArgumentException(new StringBuilder(53).append("ByteString would be too long: ").append(size).append("+").append(r0.size()).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzfes m12372(byte[] bArr) {
        return new zzfez(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzfex m12373(int i) {
        return new zzfex(i, (zzfet) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m12374(int i, int i2) {
        if (((i2 - (i + 1)) | i) >= 0) {
            return;
        }
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException(new StringBuilder(22).append("Index < 0: ").append(i).toString());
        }
        throw new ArrayIndexOutOfBoundsException(new StringBuilder(40).append("Index > length: ").append(i).append(", ").append(i2).toString());
    }

    public abstract boolean equals(Object obj);

    public final int hashCode() {
        int i = this.zzmal;
        if (i == 0) {
            int size = size();
            i = m12378(size, 0, size);
            if (i == 0) {
                i = 1;
            }
            this.zzmal = i;
        }
        return i;
    }

    public final boolean isEmpty() {
        return size() == 0;
    }

    public /* synthetic */ Iterator iterator() {
        return new zzfet(this);
    }

    public abstract int size();

    public final byte[] toByteArray() {
        int size = size();
        if (size == 0) {
            return zzffz.f10420;
        }
        byte[] bArr = new byte[size];
        m12380(bArr, 0, 0, size);
        return bArr;
    }

    public final String toString() {
        return String.format("<ByteString@%s size=%d>", new Object[]{Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(size())});
    }

    public final void zza(byte[] bArr, int i, int i2, int i3) {
        m12370(i, i + i3, size());
        m12370(i2, i2 + i3, bArr.length);
        if (i3 > 0) {
            m12380(bArr, i, i2, i3);
        }
    }

    public abstract zzffb zzcvm();

    public abstract byte zzkn(int i);

    public abstract zzfes zzx(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m12375();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12376() {
        return this.zzmal;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m12377();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m12378(int i, int i2, int i3);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m12379(zzfer zzfer) throws IOException;

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m12380(byte[] bArr, int i, int i2, int i3);
}
