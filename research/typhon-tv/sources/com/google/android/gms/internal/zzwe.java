package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzwe implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10940;

    zzwe(zzvx zzvx) {
        this.f10940 = zzvx;
    }

    public final void run() {
        try {
            this.f10940.f5502.m13511();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdOpened.", e);
        }
    }
}
