package com.google.android.gms.internal;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.gms.common.api.internal.zzbz;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mopub.common.TyphoonApp;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class zzcim {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile zzcim f9378;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzcih f9379;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private int f9380;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzclf f9381;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private long f9382;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzcig f9383;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private int f9384;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final zzcgo f9385;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private boolean f9386;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final zzchi f9387;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private long f9388;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final zzcjn f9389;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private boolean f9390;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzchk f9391;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final zzchh f9392;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private final long f9393;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final zzckc f9394;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final zzckg f9395;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final zzcgu f9396;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final zzchv f9397;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final AppMeasurement f9398;

    /* renamed from: י  reason: contains not printable characters */
    private final zzcll f9399;

    /* renamed from: ـ  reason: contains not printable characters */
    private final zzcgk f9400;

    /* renamed from: ــ  reason: contains not printable characters */
    private boolean f9401;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final FirebaseAnalytics f9402;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final zzclq f9403;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private List<Long> f9404;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final zzcgd f9405;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private List<Runnable> f9406;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f9407 = false;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f9408;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private Boolean f9409;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private long f9410;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzchm f9411;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f9412;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzchx f9413;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzcgn f9414;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private FileLock f9415;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final zzchq f9416;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zzd f9417;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private FileChannel f9418;

    class zza implements zzcgq {

        /* renamed from: 靐  reason: contains not printable characters */
        List<Long> f9420;

        /* renamed from: 麤  reason: contains not printable characters */
        private long f9421;

        /* renamed from: 齉  reason: contains not printable characters */
        List<zzcmb> f9422;

        /* renamed from: 龘  reason: contains not printable characters */
        zzcme f9423;

        private zza() {
        }

        /* synthetic */ zza(zzcim zzcim, zzcin zzcin) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static long m11066(zzcmb zzcmb) {
            return ((zzcmb.f9715.longValue() / 1000) / 60) / 60;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m11067(zzcme zzcme) {
            zzbq.m9120(zzcme);
            this.f9423 = zzcme;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m11068(long j, zzcmb zzcmb) {
            zzbq.m9120(zzcmb);
            if (this.f9422 == null) {
                this.f9422 = new ArrayList();
            }
            if (this.f9420 == null) {
                this.f9420 = new ArrayList();
            }
            if (this.f9422.size() > 0 && m11066(this.f9422.get(0)) != m11066(zzcmb)) {
                return false;
            }
            long r4 = this.f9421 + ((long) zzcmb.m12872());
            if (r4 >= ((long) Math.max(0, zzchc.f9228.m10671().intValue()))) {
                return false;
            }
            this.f9421 = r4;
            this.f9422.add(zzcmb);
            this.f9420.add(Long.valueOf(j));
            return this.f9422.size() < Math.max(1, zzchc.f9216.m10671().intValue());
        }
    }

    private zzcim(zzcjm zzcjm) {
        zzbq.m9120(zzcjm);
        this.f9412 = zzcjm.f9489;
        this.f9382 = -1;
        this.f9417 = zzh.m9250();
        this.f9393 = this.f9417.m9243();
        this.f9414 = new zzcgn(this);
        zzchx zzchx = new zzchx(this);
        zzchx.m11114();
        this.f9413 = zzchx;
        zzchm zzchm = new zzchm(this);
        zzchm.m11114();
        this.f9411 = zzchm;
        zzclq zzclq = new zzclq(this);
        zzclq.m11114();
        this.f9403 = zzclq;
        zzchk zzchk = new zzchk(this);
        zzchk.m11114();
        this.f9391 = zzchk;
        zzcgu zzcgu = new zzcgu(this);
        zzcgu.m11114();
        this.f9396 = zzcgu;
        zzchh zzchh = new zzchh(this);
        zzchh.m11114();
        this.f9392 = zzchh;
        zzcgo zzcgo = new zzcgo(this);
        zzcgo.m11114();
        this.f9385 = zzcgo;
        zzchi zzchi = new zzchi(this);
        zzchi.m11114();
        this.f9387 = zzchi;
        zzcgk zzcgk = new zzcgk(this);
        zzcgk.m11114();
        this.f9400 = zzcgk;
        this.f9405 = new zzcgd(this);
        zzchq zzchq = new zzchq(this);
        zzchq.m11114();
        this.f9416 = zzchq;
        zzckc zzckc = new zzckc(this);
        zzckc.m11114();
        this.f9394 = zzckc;
        zzckg zzckg = new zzckg(this);
        zzckg.m11114();
        this.f9395 = zzckg;
        zzcjn zzcjn = new zzcjn(this);
        zzcjn.m11114();
        this.f9389 = zzcjn;
        zzcll zzcll = new zzcll(this);
        zzcll.m11114();
        this.f9399 = zzcll;
        this.f9397 = new zzchv(this);
        this.f9398 = new AppMeasurement(this);
        this.f9402 = new FirebaseAnalytics(this);
        zzclf zzclf = new zzclf(this);
        zzclf.m11114();
        this.f9381 = zzclf;
        zzcig zzcig = new zzcig(this);
        zzcig.m11114();
        this.f9383 = zzcig;
        zzcih zzcih = new zzcih(this);
        zzcih.m11114();
        this.f9379 = zzcih;
        if (this.f9412.getApplicationContext() instanceof Application) {
            zzcjn r1 = m11022();
            if (r1.m11097().getApplicationContext() instanceof Application) {
                Application application = (Application) r1.m11097().getApplicationContext();
                if (r1.f9494 == null) {
                    r1.f9494 = new zzckb(r1, (zzcjo) null);
                }
                application.unregisterActivityLifecycleCallbacks(r1.f9494);
                application.registerActivityLifecycleCallbacks(r1.f9494);
                r1.m11096().m10848().m10849("Registered activity lifecycle callback");
            }
        } else {
            m11016().m10834().m10849("Application context is not an Application");
        }
        this.f9379.m10986((Runnable) new zzcin(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final void m10991() {
        zzcho r1;
        String concat;
        m11018().m11109();
        this.f9403.m11116();
        this.f9413.m11116();
        this.f9392.m11116();
        m11016().m10836().m10850("App measurement is starting up, version", 11910L);
        m11016().m10836().m10849("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String r0 = this.f9392.m10724();
        if (m11063().m11416(r0)) {
            r1 = m11016().m10836();
            concat = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
        } else {
            r1 = m11016().m10836();
            String valueOf = String.valueOf(r0);
            concat = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
        }
        r1.m10849(concat);
        m11016().m10845().m10849("Debug-level message logging enabled");
        if (this.f9380 != this.f9384) {
            m11016().m10832().m10851("Not all components initialized", Integer.valueOf(this.f9380), Integer.valueOf(this.f9384));
        }
        this.f9407 = true;
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private final zzcll m10992() {
        m11002((zzcjl) this.f9399);
        return this.f9399;
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private final zzchv m10993() {
        if (this.f9397 != null) {
            return this.f9397;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private final long m10994() {
        long r2 = this.f9417.m9243();
        zzchx r4 = m11040();
        r4.m11115();
        r4.m11109();
        long r0 = r4.f9302.m10902();
        if (r0 == 0) {
            r0 = 1 + ((long) r4.m11112().m11419().nextInt(86400000));
            r4.f9302.m10903(r0);
        }
        return ((((r0 + r2) / 1000) / 60) / 60) / 24;
    }

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private final boolean m10995() {
        m11018().m11109();
        try {
            this.f9418 = new RandomAccessFile(new File(this.f9412.getFilesDir(), "google_app_measurement.db"), InternalZipTyphoonApp.WRITE_MODE).getChannel();
            this.f9415 = this.f9418.tryLock();
            if (this.f9415 != null) {
                m11016().m10848().m10849("Storage concurrent access okay");
                return true;
            }
            m11016().m10832().m10849("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e) {
            m11016().m10832().m10850("Failed to acquire storage lock", e);
        } catch (IOException e2) {
            m11016().m10832().m10850("Failed to access storage lock file", e2);
        }
    }

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private final void m10996() {
        long max;
        long j;
        m11018().m11109();
        m11051();
        if (m10998()) {
            if (this.f9388 > 0) {
                long abs = 3600000 - Math.abs(this.f9417.m9241() - this.f9388);
                if (abs > 0) {
                    m11016().m10848().m10850("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    m10993().m10882();
                    m10992().m11354();
                    return;
                }
                this.f9388 = 0;
            }
            if (!m11045() || !m10999()) {
                m11016().m10848().m10849("Nothing to upload or uploading impossible");
                m10993().m10882();
                m10992().m11354();
                return;
            }
            long r4 = this.f9417.m9243();
            long max2 = Math.max(0, zzchc.f9235.m10671().longValue());
            boolean z = m11024().m10577() || m11024().m10589();
            if (z) {
                String r2 = this.f9414.m10542();
                max = (TextUtils.isEmpty(r2) || ".none.".equals(r2)) ? Math.max(0, zzchc.f9224.m10671().longValue()) : Math.max(0, zzchc.f9225.m10671().longValue());
            } else {
                max = Math.max(0, zzchc.f9222.m10671().longValue());
            }
            long r10 = m11040().f9320.m10902();
            long r12 = m11040().f9319.m10902();
            long max3 = Math.max(m11024().m10616(), m11024().m10583());
            if (max3 == 0) {
                j = 0;
            } else {
                long abs2 = r4 - Math.abs(max3 - r4);
                long abs3 = r4 - Math.abs(r12 - r4);
                long max4 = Math.max(r4 - Math.abs(r10 - r4), abs3);
                j = abs2 + max2;
                if (z && max4 > 0) {
                    j = Math.min(abs2, max4) + max;
                }
                if (!m11063().m11444(max4, max)) {
                    j = max4 + max;
                }
                if (abs3 != 0 && abs3 >= abs2) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= Math.min(20, Math.max(0, zzchc.f9244.m10671().intValue()))) {
                            j = 0;
                            break;
                        }
                        j += (1 << i2) * Math.max(0, zzchc.f9241.m10671().longValue());
                        if (j > abs3) {
                            break;
                        }
                        i = i2 + 1;
                    }
                }
            }
            if (j == 0) {
                m11016().m10848().m10849("Next upload time is 0");
                m10993().m10882();
                m10992().m11354();
            } else if (!m11026().m10872()) {
                m11016().m10848().m10849("No network");
                m10993().m10883();
                m10992().m11354();
            } else {
                long r6 = m11040().f9317.m10902();
                long max5 = Math.max(0, zzchc.f9214.m10671().longValue());
                long max6 = !m11063().m11444(r6, max5) ? Math.max(j, max5 + r6) : j;
                m10993().m10882();
                long r22 = max6 - this.f9417.m9243();
                if (r22 <= 0) {
                    r22 = Math.max(0, zzchc.f9230.m10671().longValue());
                    m11040().f9320.m10903(this.f9417.m9243());
                }
                m11016().m10848().m10850("Upload scheduled in approximately ms", Long.valueOf(r22));
                m10992().m11360(r22);
            }
        }
    }

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private final void m10997() {
        m11018().m11109();
        if (this.f9386 || this.f9401 || this.f9390) {
            m11016().m10848().m10852("Not stopping services. fetch, network, upload", Boolean.valueOf(this.f9386), Boolean.valueOf(this.f9401), Boolean.valueOf(this.f9390));
            return;
        }
        m11016().m10848().m10849("Stopping uploading service(s)");
        if (this.f9406 != null) {
            for (Runnable run : this.f9406) {
                run.run();
            }
            this.f9406.clear();
        }
    }

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private final boolean m10998() {
        m11018().m11109();
        m11051();
        return this.f9408;
    }

    /* renamed from: ــ  reason: contains not printable characters */
    private final boolean m10999() {
        m11018().m11109();
        m11051();
        return m11024().m10585() || !TextUtils.isEmpty(m11024().m10588());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzcgi m11000(String str) {
        zzcgh r2 = m11024().m10592(str);
        if (r2 == null || TextUtils.isEmpty(r2.m10480())) {
            m11016().m10845().m10850("No app data available; dropping", str);
            return null;
        }
        try {
            String str2 = zzbhf.m10231(this.f9412).m10222(str, 0).versionName;
            if (r2.m10480() != null && !r2.m10480().equals(str2)) {
                m11016().m10834().m10850("App version does not match; dropping. appId", zzchm.m10812(str));
                return null;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return new zzcgi(str, r2.m10499(), r2.m10480(), r2.m10484(), r2.m10486(), r2.m10473(), r2.m10468(), (String) null, r2.m10471(), false, r2.m10460(), r2.m10490(), 0, 0, r2.m10491());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m11001(zzcha zzcha, zzcgi zzcgi) {
        zzcgw r13;
        zzcgv zzcgv;
        zzcme zzcme;
        boolean z;
        zzcgh r3;
        zzbq.m9120(zzcgi);
        zzbq.m9122(zzcgi.f9143);
        long nanoTime = System.nanoTime();
        m11018().m11109();
        m11051();
        String str = zzcgi.f9143;
        m11063();
        if (zzclq.m11384(zzcha, zzcgi)) {
            if (!zzcgi.f9132) {
                m11003(zzcgi);
            } else if (m11031().m10937(str, zzcha.f9203)) {
                m11016().m10834().m10851("Dropping blacklisted event. appId", zzchm.m10812(str), m11064().m10805(zzcha.f9203));
                boolean z2 = m11063().m11407(str) || m11063().m11403(str);
                if (!z2 && !"_err".equals(zzcha.f9203)) {
                    m11063().m11443(str, 11, "_ev", zzcha.f9203, 0);
                }
                if (z2 && (r3 = m11024().m10592(str)) != null) {
                    if (Math.abs(this.f9417.m9243() - Math.max(r3.m10476(), r3.m10511())) > zzchc.f9234.m10671().longValue()) {
                        m11016().m10845().m10849("Fetching config for blacklisted app");
                        m11007(r3);
                    }
                }
            } else {
                if (m11016().m10844(2)) {
                    m11016().m10848().m10850("Logging event", m11064().m10801(zzcha));
                }
                m11024().m10582();
                try {
                    m11003(zzcgi);
                    if (("_iap".equals(zzcha.f9203) || "ecommerce_purchase".equals(zzcha.f9203)) && !m11013(str, zzcha)) {
                        m11024().m10584();
                        m11024().m10586();
                        return;
                    }
                    boolean r10 = zzclq.m11385(zzcha.f9203);
                    boolean equals = "_err".equals(zzcha.f9203);
                    zzcgp r4 = m11024().m10600(m10994(), str, true, r10, false, equals, false);
                    long intValue = r4.f9167 - ((long) zzchc.f9210.m10671().intValue());
                    if (intValue > 0) {
                        if (intValue % 1000 == 1) {
                            m11016().m10832().m10851("Data loss. Too many events logged. appId, count", zzchm.m10812(str), Long.valueOf(r4.f9167));
                        }
                        m11024().m10584();
                        m11024().m10586();
                        return;
                    }
                    if (r10) {
                        long intValue2 = r4.f9170 - ((long) zzchc.f9242.m10671().intValue());
                        if (intValue2 > 0) {
                            if (intValue2 % 1000 == 1) {
                                m11016().m10832().m10851("Data loss. Too many public events logged. appId, count", zzchm.m10812(str), Long.valueOf(r4.f9170));
                            }
                            m11063().m11443(str, 16, "_ev", zzcha.f9203, 0);
                            m11024().m10584();
                            m11024().m10586();
                            return;
                        }
                    }
                    if (equals) {
                        long max = r4.f9168 - ((long) Math.max(0, Math.min(1000000, this.f9414.m10544(zzcgi.f9143, zzchc.f9212))));
                        if (max > 0) {
                            if (max == 1) {
                                m11016().m10832().m10851("Too many error events logged. appId, count", zzchm.m10812(str), Long.valueOf(r4.f9168));
                            }
                            m11024().m10584();
                            m11024().m10586();
                            return;
                        }
                    }
                    Bundle r20 = zzcha.f9200.m10660();
                    m11063().m11440(r20, "_o", (Object) zzcha.f9202);
                    if (m11063().m11416(str)) {
                        m11063().m11440(r20, "_dbg", (Object) 1L);
                        m11063().m11440(r20, "_r", (Object) 1L);
                    }
                    long r42 = m11024().m10597(str);
                    if (r42 > 0) {
                        m11016().m10834().m10851("Data lost. Too many events stored on disk, deleted. appId", zzchm.m10812(str), Long.valueOf(r42));
                    }
                    zzcgv zzcgv2 = new zzcgv(this, zzcha.f9202, str, zzcha.f9203, zzcha.f9201, 0, r20);
                    zzcgw r2 = m11024().m10601(str, zzcgv2.f9183);
                    if (r2 != null) {
                        zzcgv r11 = zzcgv2.m10654(this, r2.f9191);
                        r13 = r2.m10657(r11.f9185);
                        zzcgv = r11;
                    } else if (m11024().m10575(str) < 500 || !r10) {
                        r13 = new zzcgw(str, zzcgv2.f9183, 0, 0, zzcgv2.f9185, 0, (Long) null, (Long) null, (Boolean) null);
                        zzcgv = zzcgv2;
                    } else {
                        m11016().m10832().m10852("Too many event names used, ignoring event. appId, name, supported count", zzchm.m10812(str), m11064().m10805(zzcgv2.f9183), 500);
                        m11063().m11443(str, 8, (String) null, (String) null, 0);
                        m11024().m10586();
                        return;
                    }
                    m11024().m10608(r13);
                    m11018().m11109();
                    m11051();
                    zzbq.m9120(zzcgv);
                    zzbq.m9120(zzcgi);
                    zzbq.m9122(zzcgv.f9186);
                    zzbq.m9116(zzcgv.f9186.equals(zzcgi.f9143));
                    zzcme = new zzcme();
                    zzcme.f9755 = 1;
                    zzcme.f9739 = AbstractSpiCall.ANDROID_CLIENT_TYPE;
                    zzcme.f9757 = zzcgi.f9143;
                    zzcme.f9731 = zzcgi.f9141;
                    zzcme.f9758 = zzcgi.f9142;
                    zzcme.f9750 = zzcgi.f9137 == -2147483648L ? null : Integer.valueOf((int) zzcgi.f9137);
                    zzcme.f9735 = Long.valueOf(zzcgi.f9139);
                    zzcme.f9745 = zzcgi.f9140;
                    zzcme.f9738 = zzcgi.f9130 == 0 ? null : Long.valueOf(zzcgi.f9130);
                    Pair<String, Boolean> r32 = m11040().m10897(zzcgi.f9143);
                    if (r32 == null || TextUtils.isEmpty((CharSequence) r32.first)) {
                        if (!m11030().m10650(this.f9412)) {
                            String string = Settings.Secure.getString(this.f9412.getContentResolver(), "android_id");
                            if (string == null) {
                                m11016().m10834().m10850("null secure ID. appId", zzchm.m10812(zzcme.f9757));
                                string = "null";
                            } else if (string.isEmpty()) {
                                m11016().m10834().m10850("empty secure ID. appId", zzchm.m10812(zzcme.f9757));
                            }
                            zzcme.f9756 = string;
                        }
                    } else if (zzcgi.f9144) {
                        zzcme.f9737 = (String) r32.first;
                        zzcme.f9732 = (Boolean) r32.second;
                    }
                    m11030().m11115();
                    zzcme.f9743 = Build.MODEL;
                    m11030().m11115();
                    zzcme.f9742 = Build.VERSION.RELEASE;
                    zzcme.f9730 = Integer.valueOf((int) m11030().m10641());
                    zzcme.f9733 = m11030().m10644();
                    zzcme.f9736 = null;
                    zzcme.f9753 = null;
                    zzcme.f9751 = null;
                    zzcme.f9725 = null;
                    zzcme.f9744 = Long.valueOf(zzcgi.f9135);
                    if (m11038() && zzcgn.m10524()) {
                        m11034();
                        zzcme.f9746 = null;
                    }
                    zzcgh r22 = m11024().m10592(zzcgi.f9143);
                    if (r22 == null) {
                        r22 = new zzcgh(this, zzcgi.f9143);
                        r22.m10507(m11034().m10723());
                        r22.m10501(zzcgi.f9138);
                        r22.m10497(zzcgi.f9140);
                        r22.m10504(m11040().m10892(zzcgi.f9143));
                        r22.m10461(0);
                        r22.m10506(0);
                        r22.m10496(0);
                        r22.m10494(zzcgi.f9142);
                        r22.m10503(zzcgi.f9137);
                        r22.m10462(zzcgi.f9141);
                        r22.m10500(zzcgi.f9139);
                        r22.m10493(zzcgi.f9130);
                        r22.m10508(zzcgi.f9132);
                        r22.m10510(zzcgi.f9135);
                        m11024().m10607(r22);
                    }
                    zzcme.f9734 = r22.m10502();
                    zzcme.f9749 = r22.m10460();
                    List<zzclp> r5 = m11024().m10603(zzcgi.f9143);
                    zzcme.f9754 = new zzcmg[r5.size()];
                    for (int i = 0; i < r5.size(); i++) {
                        zzcmg zzcmg = new zzcmg();
                        zzcme.f9754[i] = zzcmg;
                        zzcmg.f9765 = r5.get(i).f9657;
                        zzcmg.f9768 = Long.valueOf(r5.get(i).f9656);
                        m11063().m11442(zzcmg, r5.get(i).f9654);
                    }
                    long r14 = m11024().m10599(zzcme);
                    zzcgo r132 = m11024();
                    if (zzcgv.f9182 != null) {
                        Iterator<String> it2 = zzcgv.f9182.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if ("_r".equals(it2.next())) {
                                    z = true;
                                    break;
                                }
                            } else {
                                boolean r23 = m11031().m10943(zzcgv.f9186, zzcgv.f9183);
                                zzcgp r33 = m11024().m10600(m10994(), zzcgv.f9186, false, false, false, false, false);
                                if (r23) {
                                    if (r33.f9166 < ((long) this.f9414.m10550(zzcgv.f9186))) {
                                        z = true;
                                    }
                                }
                            }
                        }
                    }
                    z = false;
                    if (r132.m10612(zzcgv, r14, z)) {
                        this.f9388 = 0;
                    }
                    m11024().m10584();
                    if (m11016().m10844(2)) {
                        m11016().m10848().m10850("Event recorded", m11064().m10800(zzcgv));
                    }
                    m11024().m10586();
                    m10996();
                    m11016().m10848().m10850("Background event processing time, ms", Long.valueOf(((System.nanoTime() - nanoTime) + 500000) / C.MICROS_PER_SECOND));
                } catch (IOException e) {
                    m11016().m10832().m10851("Data loss. Failed to insert raw event metadata. appId", zzchm.m10812(zzcme.f9757), e);
                } catch (Throwable th) {
                    m11024().m10586();
                    throw th;
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m11002(zzcjl zzcjl) {
        if (zzcjl == null) {
            throw new IllegalStateException("Component not created");
        } else if (!zzcjl.m11113()) {
            throw new IllegalStateException("Component not initialized");
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m11003(zzcgi zzcgi) {
        boolean z = true;
        m11018().m11109();
        m11051();
        zzbq.m9120(zzcgi);
        zzbq.m9122(zzcgi.f9143);
        zzcgh r2 = m11024().m10592(zzcgi.f9143);
        String r3 = m11040().m10892(zzcgi.f9143);
        boolean z2 = false;
        if (r2 == null) {
            r2 = new zzcgh(this, zzcgi.f9143);
            r2.m10507(m11034().m10723());
            r2.m10504(r3);
            z2 = true;
        } else if (!r3.equals(r2.m10492())) {
            r2.m10504(r3);
            r2.m10507(m11034().m10723());
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzcgi.f9140) && !zzcgi.f9140.equals(r2.m10499())) {
            r2.m10497(zzcgi.f9140);
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzcgi.f9138) && !zzcgi.f9138.equals(r2.m10460())) {
            r2.m10501(zzcgi.f9138);
            z2 = true;
        }
        if (!(zzcgi.f9139 == 0 || zzcgi.f9139 == r2.m10473())) {
            r2.m10500(zzcgi.f9139);
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzcgi.f9142) && !zzcgi.f9142.equals(r2.m10480())) {
            r2.m10494(zzcgi.f9142);
            z2 = true;
        }
        if (zzcgi.f9137 != r2.m10484()) {
            r2.m10503(zzcgi.f9137);
            z2 = true;
        }
        if (zzcgi.f9141 != null && !zzcgi.f9141.equals(r2.m10486())) {
            r2.m10462(zzcgi.f9141);
            z2 = true;
        }
        if (zzcgi.f9130 != r2.m10468()) {
            r2.m10493(zzcgi.f9130);
            z2 = true;
        }
        if (zzcgi.f9132 != r2.m10471()) {
            r2.m10508(zzcgi.f9132);
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzcgi.f9131) && !zzcgi.f9131.equals(r2.m10488())) {
            r2.m10465(zzcgi.f9131);
            z2 = true;
        }
        if (zzcgi.f9135 != r2.m10490()) {
            r2.m10510(zzcgi.f9135);
            z2 = true;
        }
        if (zzcgi.f9144 != r2.m10491()) {
            r2.m10498(zzcgi.f9144);
        } else {
            z = z2;
        }
        if (z) {
            m11024().m10607(r2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m11004(FileChannel fileChannel) {
        m11018().m11109();
        if (fileChannel == null || !fileChannel.isOpen()) {
            m11016().m10832().m10849("Bad chanel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read == 4) {
                allocate.flip();
                return allocate.getInt();
            } else if (read == -1) {
                return 0;
            } else {
                m11016().m10834().m10850("Unexpected data length. Bytes read", Integer.valueOf(read));
                return 0;
            }
        } catch (IOException e) {
            m11016().m10832().m10850("Failed to read from channel", e);
            return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcgi m11005(Context context, String str, String str2, boolean z, boolean z2) {
        String str3;
        String str4 = "Unknown";
        String str5 = "Unknown";
        int i = Integer.MIN_VALUE;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            m11016().m10832().m10849("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str);
        } catch (IllegalArgumentException e) {
            m11016().m10832().m10850("Error retrieving installer package name. appId", zzchm.m10812(str));
        }
        if (str4 == null) {
            str4 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str4 = "";
        }
        try {
            PackageInfo r4 = zzbhf.m10231(context).m10222(str, 0);
            if (r4 != null) {
                CharSequence r2 = zzbhf.m10231(context).m10223(str);
                str3 = !TextUtils.isEmpty(r2) ? r2.toString() : "Unknown";
                try {
                    str5 = r4.versionName;
                    i = r4.versionCode;
                } catch (PackageManager.NameNotFoundException e2) {
                    m11016().m10832().m10851("Error retrieving newly installed package info. appId, appName", zzchm.m10812(str), str3);
                    return null;
                }
            }
            return new zzcgi(str, str2, str5, (long) i, str4, 11910, m11063().m11424(context, str), (String) null, z, false, "", 0, 0, 0, z2);
        } catch (PackageManager.NameNotFoundException e3) {
            str3 = "Unknown";
            m11016().m10832().m10851("Error retrieving newly installed package info. appId, appName", zzchm.m10812(str), str3);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzcim m11006(Context context) {
        zzbq.m9120(context);
        zzbq.m9120(context.getApplicationContext());
        if (f9378 == null) {
            synchronized (zzcim.class) {
                if (f9378 == null) {
                    f9378 = new zzcim(new zzcjm(context));
                }
            }
        }
        return f9378;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11007(zzcgh zzcgh) {
        ArrayMap arrayMap;
        m11018().m11109();
        if (TextUtils.isEmpty(zzcgh.m10499())) {
            m11061(zzcgh.m10495(), 204, (Throwable) null, (byte[]) null, (Map<String, List<String>>) null);
            return;
        }
        String r1 = zzcgh.m10499();
        String r2 = zzcgh.m10502();
        Uri.Builder builder = new Uri.Builder();
        Uri.Builder encodedAuthority = builder.scheme(zzchc.f9206.m10671()).encodedAuthority(zzchc.f9208.m10671());
        String valueOf = String.valueOf(r1);
        encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", r2).appendQueryParameter("platform", AbstractSpiCall.ANDROID_CLIENT_TYPE).appendQueryParameter("gmp_version", "11910");
        String uri = builder.build().toString();
        try {
            URL url = new URL(uri);
            m11016().m10848().m10850("Fetching remote configuration", zzcgh.m10495());
            zzcly r0 = m11031().m10944(zzcgh.m10495());
            String r12 = m11031().m10935(zzcgh.m10495());
            if (r0 == null || TextUtils.isEmpty(r12)) {
                arrayMap = null;
            } else {
                ArrayMap arrayMap2 = new ArrayMap();
                arrayMap2.put("If-Modified-Since", r12);
                arrayMap = arrayMap2;
            }
            this.f9386 = true;
            zzchq r13 = m11026();
            String r22 = zzcgh.m10495();
            zzciq zzciq = new zzciq(this);
            r13.m11109();
            r13.m11115();
            zzbq.m9120(url);
            zzbq.m9120(zzciq);
            r13.m11101().m10981((Runnable) new zzchu(r13, r22, url, (byte[]) null, arrayMap, zzciq));
        } catch (MalformedURLException e) {
            m11016().m10832().m10851("Failed to parse config URL. Not fetching. appId", zzchm.m10812(zzcgh.m10495()), uri);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m11009(zzcjk zzcjk) {
        if (zzcjk == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m11010(int i, FileChannel fileChannel) {
        m11018().m11109();
        if (fileChannel == null || !fileChannel.isOpen()) {
            m11016().m10832().m10849("Bad chanel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() == 4) {
                return true;
            }
            m11016().m10832().m10850("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            return true;
        } catch (IOException e) {
            m11016().m10832().m10850("Failed to write to channel", e);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m11011(zzcmb zzcmb, String str, Object obj) {
        if (TextUtils.isEmpty(str) || obj == null) {
            return false;
        }
        for (zzcmc zzcmc : zzcmb.f9716) {
            if (str.equals(zzcmc.f9722)) {
                return ((obj instanceof Long) && obj.equals(zzcmc.f9721)) || ((obj instanceof String) && obj.equals(zzcmc.f9719)) || ((obj instanceof Double) && obj.equals(zzcmc.f9720));
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:141:0x03ad, code lost:
        if (com.google.android.gms.internal.zzclq.m11366(r0.f9422.get(r13).f9713) != false) goto L_0x03af;
     */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0374 A[Catch:{ IOException -> 0x02d5, all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0378 A[Catch:{ IOException -> 0x02d5, all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0096 A[Catch:{ IOException -> 0x02d5, all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0099 A[Catch:{ IOException -> 0x02d5, all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x0adb A[Catch:{ IOException -> 0x02d5, all -> 0x01cb }] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m11012(java.lang.String r31, long r32) {
        /*
            r30 = this;
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()
            r2.m10582()
            com.google.android.gms.internal.zzcim$zza r21 = new com.google.android.gms.internal.zzcim$zza     // Catch:{ all -> 0x01cb }
            r2 = 0
            r0 = r21
            r1 = r30
            r0.<init>(r1, r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r14 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r4 = 0
            r0 = r30
            long r0 = r0.f9382     // Catch:{ all -> 0x01cb }
            r16 = r0
            com.google.android.gms.common.internal.zzbq.m9120(r21)     // Catch:{ all -> 0x01cb }
            r14.m11109()     // Catch:{ all -> 0x01cb }
            r14.m11115()     // Catch:{ all -> 0x01cb }
            r3 = 0
            android.database.sqlite.SQLiteDatabase r2 = r14.m10587()     // Catch:{ SQLiteException -> 0x0af0 }
            r5 = 0
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            if (r5 == 0) goto L_0x01d4
            r6 = -1
            int r5 = (r16 > r6 ? 1 : (r16 == r6 ? 0 : -1))
            if (r5 == 0) goto L_0x0166
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = 0
            java.lang.String r7 = java.lang.String.valueOf(r16)     // Catch:{ SQLiteException -> 0x0af0 }
            r5[r6] = r7     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = 1
            java.lang.String r7 = java.lang.String.valueOf(r32)     // Catch:{ SQLiteException -> 0x0af0 }
            r5[r6] = r7     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = r5
        L_0x0049:
            r8 = -1
            int r5 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r5 == 0) goto L_0x0173
            java.lang.String r5 = "rowid <= ? and "
        L_0x0052:
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            int r7 = r7.length()     // Catch:{ SQLiteException -> 0x0af0 }
            int r7 = r7 + 148
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0af0 }
            r8.<init>(r7)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.String r7 = "select app_id, metadata_fingerprint from raw_events where "
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.String r7 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLiteException -> 0x0af0 }
            android.database.Cursor r3 = r2.rawQuery(r5, r6)     // Catch:{ SQLiteException -> 0x0af0 }
            boolean r5 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0af0 }
            if (r5 != 0) goto L_0x0178
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ all -> 0x01cb }
        L_0x0086:
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0096
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0378
        L_0x0096:
            r2 = 1
        L_0x0097:
            if (r2 != 0) goto L_0x0adb
            r17 = 0
            r0 = r21
            com.google.android.gms.internal.zzcme r0 = r0.f9423     // Catch:{ all -> 0x01cb }
            r22 = r0
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            int r2 = r2.size()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb[] r2 = new com.google.android.gms.internal.zzcmb[r2]     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9752 = r2     // Catch:{ all -> 0x01cb }
            r12 = 0
            r2 = 0
            r13 = r2
        L_0x00b2:
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            int r2 = r2.size()     // Catch:{ all -> 0x01cb }
            if (r13 >= r2) goto L_0x0633
            com.google.android.gms.internal.zzcig r3 = r30.m11031()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r2.f9757     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            boolean r2 = r3.m10937(r4, r2)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x037e
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r3 = r2.m10834()     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "Dropping blacklisted raw event. appId"
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9757     // Catch:{ all -> 0x01cb }
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzchk r6 = r30.m11064()     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r6.m10805((java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            r3.m10851(r4, r5, r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzclq r2 = r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            boolean r2 = r2.m11407(r3)     // Catch:{ all -> 0x01cb }
            if (r2 != 0) goto L_0x0124
            com.google.android.gms.internal.zzclq r2 = r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            boolean r2 = r2.m11403(r3)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x037b
        L_0x0124:
            r2 = 1
        L_0x0125:
            if (r2 != 0) goto L_0x0b0b
            java.lang.String r3 = "_err"
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x01cb }
            if (r2 != 0) goto L_0x0b0b
            com.google.android.gms.internal.zzclq r2 = r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            r4 = 11
            java.lang.String r5 = "_ev"
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r6 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r6 = r6.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r6 = (com.google.android.gms.internal.zzcmb) r6     // Catch:{ all -> 0x01cb }
            java.lang.String r6 = r6.f9713     // Catch:{ all -> 0x01cb }
            r7 = 0
            r2.m11443((java.lang.String) r3, (int) r4, (java.lang.String) r5, (java.lang.String) r6, (int) r7)     // Catch:{ all -> 0x01cb }
            r2 = r12
            r4 = r17
        L_0x015e:
            int r3 = r13 + 1
            r13 = r3
            r12 = r2
            r17 = r4
            goto L_0x00b2
        L_0x0166:
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = 0
            java.lang.String r7 = java.lang.String.valueOf(r32)     // Catch:{ SQLiteException -> 0x0af0 }
            r5[r6] = r7     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = r5
            goto L_0x0049
        L_0x0173:
            java.lang.String r5 = ""
            goto L_0x0052
        L_0x0178:
            r5 = 0
            java.lang.String r4 = r3.getString(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            r5 = 1
            java.lang.String r5 = r3.getString(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            r3.close()     // Catch:{ SQLiteException -> 0x0af0 }
            r13 = r5
            r11 = r3
            r12 = r4
        L_0x0188:
            java.lang.String r3 = "raw_events_metadata"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r5 = 0
            java.lang.String r6 = "metadata"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            java.lang.String r5 = "app_id = ? and metadata_fingerprint = ?"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 0
            r6[r7] = r12     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 1
            r6[r7] = r13     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            java.lang.String r10 = "2"
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            boolean r3 = r11.moveToFirst()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            if (r3 != 0) goto L_0x0242
            com.google.android.gms.internal.zzchm r2 = r14.m11096()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            java.lang.String r3 = "Raw event metadata record is missing. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r2.m10850(r3, r4)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            if (r11 == 0) goto L_0x0086
            r11.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x01cb:
            r2 = move-exception
            com.google.android.gms.internal.zzcgo r3 = r30.m11024()
            r3.m10586()
            throw r2
        L_0x01d4:
            r6 = -1
            int r5 = (r16 > r6 ? 1 : (r16 == r6 ? 0 : -1))
            if (r5 == 0) goto L_0x0228
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = 0
            r7 = 0
            r5[r6] = r7     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = 1
            java.lang.String r7 = java.lang.String.valueOf(r16)     // Catch:{ SQLiteException -> 0x0af0 }
            r5[r6] = r7     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = r5
        L_0x01e9:
            r8 = -1
            int r5 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r5 == 0) goto L_0x0231
            java.lang.String r5 = " and rowid <= ?"
        L_0x01f2:
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            int r7 = r7.length()     // Catch:{ SQLiteException -> 0x0af0 }
            int r7 = r7 + 84
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0af0 }
            r8.<init>(r7)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.String r7 = "select metadata_fingerprint from raw_events where app_id = ?"
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.String r7 = " order by rowid limit 1;"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ SQLiteException -> 0x0af0 }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLiteException -> 0x0af0 }
            android.database.Cursor r3 = r2.rawQuery(r5, r6)     // Catch:{ SQLiteException -> 0x0af0 }
            boolean r5 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0af0 }
            if (r5 != 0) goto L_0x0235
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x0228:
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = 0
            r7 = 0
            r5[r6] = r7     // Catch:{ SQLiteException -> 0x0af0 }
            r6 = r5
            goto L_0x01e9
        L_0x0231:
            java.lang.String r5 = ""
            goto L_0x01f2
        L_0x0235:
            r5 = 0
            java.lang.String r5 = r3.getString(r5)     // Catch:{ SQLiteException -> 0x0af0 }
            r3.close()     // Catch:{ SQLiteException -> 0x0af0 }
            r13 = r5
            r11 = r3
            r12 = r4
            goto L_0x0188
        L_0x0242:
            r3 = 0
            byte[] r3 = r11.getBlob(r3)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r4 = 0
            int r5 = r3.length     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            com.google.android.gms.internal.zzfjj r3 = com.google.android.gms.internal.zzfjj.m12783(r3, r4, r5)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            com.google.android.gms.internal.zzcme r4 = new com.google.android.gms.internal.zzcme     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r4.<init>()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r4.m12876((com.google.android.gms.internal.zzfjj) r3)     // Catch:{ IOException -> 0x02d5 }
            boolean r3 = r11.moveToNext()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            if (r3 == 0) goto L_0x026d
            com.google.android.gms.internal.zzchm r3 = r14.m11096()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            com.google.android.gms.internal.zzcho r3 = r3.m10834()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            java.lang.String r5 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3.m10850(r5, r6)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
        L_0x026d:
            r11.close()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r0 = r21
            r0.m10617(r4)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r4 = -1
            int r3 = (r16 > r4 ? 1 : (r16 == r4 ? 0 : -1))
            if (r3 == 0) goto L_0x02ef
            java.lang.String r5 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r3 = 3
            java.lang.String[] r6 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3 = 0
            r6[r3] = r12     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3 = 1
            r6[r3] = r13     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3 = 2
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r6[r3] = r4     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
        L_0x028e:
            java.lang.String r3 = "raw_events"
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 0
            java.lang.String r8 = "rowid"
            r4[r7] = r8     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 1
            java.lang.String r8 = "name"
            r4[r7] = r8     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 2
            java.lang.String r8 = "timestamp"
            r4[r7] = r8     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 3
            java.lang.String r8 = "data"
            r4[r7] = r8     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            r10 = 0
            android.database.Cursor r3 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            boolean r2 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0af3 }
            if (r2 != 0) goto L_0x0318
            com.google.android.gms.internal.zzchm r2 = r14.m11096()     // Catch:{ SQLiteException -> 0x0af3 }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ SQLiteException -> 0x0af3 }
            java.lang.String r4 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x0af3 }
            r2.m10850(r4, r5)     // Catch:{ SQLiteException -> 0x0af3 }
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x02d5:
            r2 = move-exception
            com.google.android.gms.internal.zzchm r3 = r14.m11096()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            com.google.android.gms.internal.zzcho r3 = r3.m10832()     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            java.lang.String r4 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3.m10851(r4, r5, r2)     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            if (r11 == 0) goto L_0x0086
            r11.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x02ef:
            java.lang.String r5 = "app_id = ? and metadata_fingerprint = ?"
            r3 = 2
            java.lang.String[] r6 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3 = 0
            r6[r3] = r12     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            r3 = 1
            r6[r3] = r13     // Catch:{ SQLiteException -> 0x02fc, all -> 0x0aec }
            goto L_0x028e
        L_0x02fc:
            r2 = move-exception
            r3 = r11
            r4 = r12
        L_0x02ff:
            com.google.android.gms.internal.zzchm r5 = r14.m11096()     // Catch:{ all -> 0x0371 }
            com.google.android.gms.internal.zzcho r5 = r5.m10832()     // Catch:{ all -> 0x0371 }
            java.lang.String r6 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r4)     // Catch:{ all -> 0x0371 }
            r5.m10851(r6, r4, r2)     // Catch:{ all -> 0x0371 }
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x0318:
            r2 = 0
            long r4 = r3.getLong(r2)     // Catch:{ SQLiteException -> 0x0af3 }
            r2 = 3
            byte[] r2 = r3.getBlob(r2)     // Catch:{ SQLiteException -> 0x0af3 }
            r6 = 0
            int r7 = r2.length     // Catch:{ SQLiteException -> 0x0af3 }
            com.google.android.gms.internal.zzfjj r2 = com.google.android.gms.internal.zzfjj.m12783(r2, r6, r7)     // Catch:{ SQLiteException -> 0x0af3 }
            com.google.android.gms.internal.zzcmb r6 = new com.google.android.gms.internal.zzcmb     // Catch:{ SQLiteException -> 0x0af3 }
            r6.<init>()     // Catch:{ SQLiteException -> 0x0af3 }
            r6.m12876((com.google.android.gms.internal.zzfjj) r2)     // Catch:{ IOException -> 0x0351 }
            r2 = 1
            java.lang.String r2 = r3.getString(r2)     // Catch:{ SQLiteException -> 0x0af3 }
            r6.f9713 = r2     // Catch:{ SQLiteException -> 0x0af3 }
            r2 = 2
            long r8 = r3.getLong(r2)     // Catch:{ SQLiteException -> 0x0af3 }
            java.lang.Long r2 = java.lang.Long.valueOf(r8)     // Catch:{ SQLiteException -> 0x0af3 }
            r6.f9715 = r2     // Catch:{ SQLiteException -> 0x0af3 }
            r0 = r21
            boolean r2 = r0.m10618(r4, r6)     // Catch:{ SQLiteException -> 0x0af3 }
            if (r2 != 0) goto L_0x0364
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x0351:
            r2 = move-exception
            com.google.android.gms.internal.zzchm r4 = r14.m11096()     // Catch:{ SQLiteException -> 0x0af3 }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ SQLiteException -> 0x0af3 }
            java.lang.String r5 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x0af3 }
            r4.m10851(r5, r6, r2)     // Catch:{ SQLiteException -> 0x0af3 }
        L_0x0364:
            boolean r2 = r3.moveToNext()     // Catch:{ SQLiteException -> 0x0af3 }
            if (r2 != 0) goto L_0x0318
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ all -> 0x01cb }
            goto L_0x0086
        L_0x0371:
            r2 = move-exception
        L_0x0372:
            if (r3 == 0) goto L_0x0377
            r3.close()     // Catch:{ all -> 0x01cb }
        L_0x0377:
            throw r2     // Catch:{ all -> 0x01cb }
        L_0x0378:
            r2 = 0
            goto L_0x0097
        L_0x037b:
            r2 = 0
            goto L_0x0125
        L_0x037e:
            com.google.android.gms.internal.zzcig r3 = r30.m11031()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r2.f9757     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            boolean r14 = r3.m10943(r4, r2)     // Catch:{ all -> 0x01cb }
            if (r14 != 0) goto L_0x03af
            r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            boolean r2 = com.google.android.gms.internal.zzclq.m11366(r2)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0630
        L_0x03af:
            r3 = 0
            r4 = 0
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = r2.f9716     // Catch:{ all -> 0x01cb }
            if (r2 != 0) goto L_0x03ce
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            r5 = 0
            com.google.android.gms.internal.zzcmc[] r5 = new com.google.android.gms.internal.zzcmc[r5]     // Catch:{ all -> 0x01cb }
            r2.f9716 = r5     // Catch:{ all -> 0x01cb }
        L_0x03ce:
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r6 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r7 = r6.length     // Catch:{ all -> 0x01cb }
            r2 = 0
            r5 = r2
        L_0x03dd:
            if (r5 >= r7) goto L_0x040f
            r2 = r6[r5]     // Catch:{ all -> 0x01cb }
            java.lang.String r8 = "_c"
            java.lang.String r9 = r2.f9722     // Catch:{ all -> 0x01cb }
            boolean r8 = r8.equals(r9)     // Catch:{ all -> 0x01cb }
            if (r8 == 0) goto L_0x03fa
            r8 = 1
            java.lang.Long r3 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x01cb }
            r2.f9721 = r3     // Catch:{ all -> 0x01cb }
            r3 = 1
            r2 = r4
        L_0x03f6:
            int r5 = r5 + 1
            r4 = r2
            goto L_0x03dd
        L_0x03fa:
            java.lang.String r8 = "_r"
            java.lang.String r9 = r2.f9722     // Catch:{ all -> 0x01cb }
            boolean r8 = r8.equals(r9)     // Catch:{ all -> 0x01cb }
            if (r8 == 0) goto L_0x0b08
            r8 = 1
            java.lang.Long r4 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x01cb }
            r2.f9721 = r4     // Catch:{ all -> 0x01cb }
            r2 = 1
            goto L_0x03f6
        L_0x040f:
            if (r3 != 0) goto L_0x0479
            if (r14 == 0) goto L_0x0479
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r3 = r2.m10848()     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "Marking event as conversion"
            com.google.android.gms.internal.zzchk r6 = r30.m11064()     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r6.m10805((java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            r3.m10850(r5, r2)     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r3 = r2.f9716     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r2 = r2.length     // Catch:{ all -> 0x01cb }
            int r2 = r2 + 1
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r3, r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = (com.google.android.gms.internal.zzcmc[]) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc r3 = new com.google.android.gms.internal.zzcmc     // Catch:{ all -> 0x01cb }
            r3.<init>()     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "_c"
            r3.f9722 = r5     // Catch:{ all -> 0x01cb }
            r6 = 1
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x01cb }
            r3.f9721 = r5     // Catch:{ all -> 0x01cb }
            int r5 = r2.length     // Catch:{ all -> 0x01cb }
            int r5 = r5 + -1
            r2[r5] = r3     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r3 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r3 = r3.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r3 = (com.google.android.gms.internal.zzcmb) r3     // Catch:{ all -> 0x01cb }
            r3.f9716 = r2     // Catch:{ all -> 0x01cb }
        L_0x0479:
            if (r4 != 0) goto L_0x04e1
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r3 = r2.m10848()     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "Marking event as real-time"
            com.google.android.gms.internal.zzchk r5 = r30.m11064()     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r5.m10805((java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            r3.m10850(r4, r2)     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r3 = r2.f9716     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r2 = r2.length     // Catch:{ all -> 0x01cb }
            int r2 = r2 + 1
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r3, r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = (com.google.android.gms.internal.zzcmc[]) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc r3 = new com.google.android.gms.internal.zzcmc     // Catch:{ all -> 0x01cb }
            r3.<init>()     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "_r"
            r3.f9722 = r4     // Catch:{ all -> 0x01cb }
            r4 = 1
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x01cb }
            r3.f9721 = r4     // Catch:{ all -> 0x01cb }
            int r4 = r2.length     // Catch:{ all -> 0x01cb }
            int r4 = r4 + -1
            r2[r4] = r3     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r3 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r3 = r3.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r3 = (com.google.android.gms.internal.zzcmb) r3     // Catch:{ all -> 0x01cb }
            r3.f9716 = r2     // Catch:{ all -> 0x01cb }
        L_0x04e1:
            r2 = 1
            com.google.android.gms.internal.zzcgo r3 = r30.m11024()     // Catch:{ all -> 0x01cb }
            long r4 = r30.m10994()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r6 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r6 = r6.f9757     // Catch:{ all -> 0x01cb }
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 1
            com.google.android.gms.internal.zzcgp r3 = r3.m10600(r4, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x01cb }
            long r4 = r3.f9166     // Catch:{ all -> 0x01cb }
            r0 = r30
            com.google.android.gms.internal.zzcgn r3 = r0.f9414     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r6 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r6 = r6.f9757     // Catch:{ all -> 0x01cb }
            int r3 = r3.m10550(r6)     // Catch:{ all -> 0x01cb }
            long r6 = (long) r3     // Catch:{ all -> 0x01cb }
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x0b04
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            r3 = 0
        L_0x0519:
            com.google.android.gms.internal.zzcmc[] r4 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r4 = r4.length     // Catch:{ all -> 0x01cb }
            if (r3 >= r4) goto L_0x054b
            java.lang.String r4 = "_r"
            com.google.android.gms.internal.zzcmc[] r5 = r2.f9716     // Catch:{ all -> 0x01cb }
            r5 = r5[r3]     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = r5.f9722     // Catch:{ all -> 0x01cb }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x05c9
            com.google.android.gms.internal.zzcmc[] r4 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r4 = r4.length     // Catch:{ all -> 0x01cb }
            int r4 = r4 + -1
            com.google.android.gms.internal.zzcmc[] r4 = new com.google.android.gms.internal.zzcmc[r4]     // Catch:{ all -> 0x01cb }
            if (r3 <= 0) goto L_0x053d
            com.google.android.gms.internal.zzcmc[] r5 = r2.f9716     // Catch:{ all -> 0x01cb }
            r6 = 0
            r7 = 0
            java.lang.System.arraycopy(r5, r6, r4, r7, r3)     // Catch:{ all -> 0x01cb }
        L_0x053d:
            int r5 = r4.length     // Catch:{ all -> 0x01cb }
            if (r3 >= r5) goto L_0x0549
            com.google.android.gms.internal.zzcmc[] r5 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r6 = r3 + 1
            int r7 = r4.length     // Catch:{ all -> 0x01cb }
            int r7 = r7 - r3
            java.lang.System.arraycopy(r5, r6, r4, r3, r7)     // Catch:{ all -> 0x01cb }
        L_0x0549:
            r2.f9716 = r4     // Catch:{ all -> 0x01cb }
        L_0x054b:
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9713     // Catch:{ all -> 0x01cb }
            boolean r2 = com.google.android.gms.internal.zzclq.m11385((java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0630
            if (r14 == 0) goto L_0x0630
            com.google.android.gms.internal.zzcgo r3 = r30.m11024()     // Catch:{ all -> 0x01cb }
            long r4 = r30.m10994()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r6 = r2.f9757     // Catch:{ all -> 0x01cb }
            r7 = 0
            r8 = 0
            r9 = 1
            r10 = 0
            r11 = 0
            com.google.android.gms.internal.zzcgp r2 = r3.m10600(r4, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x01cb }
            long r2 = r2.f9169     // Catch:{ all -> 0x01cb }
            r0 = r30
            com.google.android.gms.internal.zzcgn r4 = r0.f9414     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r5 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = r5.f9757     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzchd<java.lang.Integer> r6 = com.google.android.gms.internal.zzchc.f9243     // Catch:{ all -> 0x01cb }
            int r4 = r4.m10544(r5, r6)     // Catch:{ all -> 0x01cb }
            long r4 = (long) r4     // Catch:{ all -> 0x01cb }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0630
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "Too many conversions. Not logging as conversion. appId"
            r0 = r21
            com.google.android.gms.internal.zzcme r4 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.f9757     // Catch:{ all -> 0x01cb }
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r4)     // Catch:{ all -> 0x01cb }
            r2.m10850(r3, r4)     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            r5 = 0
            r4 = 0
            com.google.android.gms.internal.zzcmc[] r7 = r2.f9716     // Catch:{ all -> 0x01cb }
            int r8 = r7.length     // Catch:{ all -> 0x01cb }
            r3 = 0
            r6 = r3
        L_0x05b6:
            if (r6 >= r8) goto L_0x05db
            r3 = r7[r6]     // Catch:{ all -> 0x01cb }
            java.lang.String r9 = "_c"
            java.lang.String r10 = r3.f9722     // Catch:{ all -> 0x01cb }
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x01cb }
            if (r9 == 0) goto L_0x05cd
        L_0x05c5:
            int r6 = r6 + 1
            r4 = r3
            goto L_0x05b6
        L_0x05c9:
            int r3 = r3 + 1
            goto L_0x0519
        L_0x05cd:
            java.lang.String r9 = "_err"
            java.lang.String r3 = r3.f9722     // Catch:{ all -> 0x01cb }
            boolean r3 = r9.equals(r3)     // Catch:{ all -> 0x01cb }
            if (r3 == 0) goto L_0x0b01
            r5 = 1
            r3 = r4
            goto L_0x05c5
        L_0x05db:
            if (r5 == 0) goto L_0x0606
            if (r4 == 0) goto L_0x0606
            com.google.android.gms.internal.zzcmc[] r3 = r2.f9716     // Catch:{ all -> 0x01cb }
            r5 = 1
            com.google.android.gms.internal.zzcmc[] r5 = new com.google.android.gms.internal.zzcmc[r5]     // Catch:{ all -> 0x01cb }
            r6 = 0
            r5[r6] = r4     // Catch:{ all -> 0x01cb }
            java.lang.Object[] r3 = com.google.android.gms.common.util.zza.m9238(r3, r5)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r3 = (com.google.android.gms.internal.zzcmc[]) r3     // Catch:{ all -> 0x01cb }
            r2.f9716 = r3     // Catch:{ all -> 0x01cb }
            r4 = r17
        L_0x05f1:
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r5 = r0.f9752     // Catch:{ all -> 0x01cb }
            int r3 = r12 + 1
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb r2 = (com.google.android.gms.internal.zzcmb) r2     // Catch:{ all -> 0x01cb }
            r5[r12] = r2     // Catch:{ all -> 0x01cb }
            r2 = r3
            goto L_0x015e
        L_0x0606:
            if (r4 == 0) goto L_0x0618
            java.lang.String r2 = "_err"
            r4.f9722 = r2     // Catch:{ all -> 0x01cb }
            r2 = 10
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x01cb }
            r4.f9721 = r2     // Catch:{ all -> 0x01cb }
            r4 = r17
            goto L_0x05f1
        L_0x0618:
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "Did not find conversion parameter. appId"
            r0 = r21
            com.google.android.gms.internal.zzcme r4 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.f9757     // Catch:{ all -> 0x01cb }
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r4)     // Catch:{ all -> 0x01cb }
            r2.m10850(r3, r4)     // Catch:{ all -> 0x01cb }
        L_0x0630:
            r4 = r17
            goto L_0x05f1
        L_0x0633:
            r0 = r21
            java.util.List<com.google.android.gms.internal.zzcmb> r2 = r0.f9422     // Catch:{ all -> 0x01cb }
            int r2 = r2.size()     // Catch:{ all -> 0x01cb }
            if (r12 >= r2) goto L_0x064b
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r2 = r0.f9752     // Catch:{ all -> 0x01cb }
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r2, r12)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb[] r2 = (com.google.android.gms.internal.zzcmb[]) r2     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9752 = r2     // Catch:{ all -> 0x01cb }
        L_0x064b:
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9757     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmg[] r3 = r3.f9754     // Catch:{ all -> 0x01cb }
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r4 = r0.f9752     // Catch:{ all -> 0x01cb }
            r0 = r30
            com.google.android.gms.internal.zzcma[] r2 = r0.m11014((java.lang.String) r2, (com.google.android.gms.internal.zzcmg[]) r3, (com.google.android.gms.internal.zzcmb[]) r4)     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9748 = r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzchd<java.lang.Boolean> r2 = com.google.android.gms.internal.zzchc.f9237     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.m10671()     // Catch:{ all -> 0x01cb }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x01cb }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x094a
            r0 = r30
            com.google.android.gms.internal.zzcgn r2 = r0.f9414     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "1"
            com.google.android.gms.internal.zzcig r2 = r2.m11099()     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "measurement.event_sampling_enabled"
            java.lang.String r2 = r2.m10945((java.lang.String) r3, (java.lang.String) r5)     // Catch:{ all -> 0x01cb }
            boolean r2 = r4.equals(r2)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x094a
            java.util.HashMap r23 = new java.util.HashMap     // Catch:{ all -> 0x01cb }
            r23.<init>()     // Catch:{ all -> 0x01cb }
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r2 = r0.f9752     // Catch:{ all -> 0x01cb }
            int r2 = r2.length     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb[] r0 = new com.google.android.gms.internal.zzcmb[r2]     // Catch:{ all -> 0x01cb }
            r24 = r0
            r18 = 0
            com.google.android.gms.internal.zzclq r2 = r30.m11063()     // Catch:{ all -> 0x01cb }
            java.security.SecureRandom r25 = r2.m11419()     // Catch:{ all -> 0x01cb }
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r0 = r0.f9752     // Catch:{ all -> 0x01cb }
            r26 = r0
            r0 = r26
            int r0 = r0.length     // Catch:{ all -> 0x01cb }
            r27 = r0
            r2 = 0
            r20 = r2
        L_0x06b7:
            r0 = r20
            r1 = r27
            if (r0 >= r1) goto L_0x0911
            r28 = r26[r20]     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.String r2 = r0.f9713     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "_ep"
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x074b
            r30.m11063()     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = "_en"
            r0 = r28
            java.lang.Object r2 = com.google.android.gms.internal.zzclq.m11377((com.google.android.gms.internal.zzcmb) r0, (java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x01cb }
            r0 = r23
            java.lang.Object r3 = r0.get(r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = (com.google.android.gms.internal.zzcgw) r3     // Catch:{ all -> 0x01cb }
            if (r3 != 0) goto L_0x06f7
            com.google.android.gms.internal.zzcgo r3 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r4 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.f9757     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = r3.m10601((java.lang.String) r4, (java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            r0 = r23
            r0.put(r2, r3)     // Catch:{ all -> 0x01cb }
        L_0x06f7:
            java.lang.Long r2 = r3.f9188     // Catch:{ all -> 0x01cb }
            if (r2 != 0) goto L_0x090d
            java.lang.Long r2 = r3.f9189     // Catch:{ all -> 0x01cb }
            long r4 = r2.longValue()     // Catch:{ all -> 0x01cb }
            r6 = 1
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x071b
            r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r28
            com.google.android.gms.internal.zzcmc[] r2 = r0.f9716     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "_sr"
            java.lang.Long r5 = r3.f9189     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = com.google.android.gms.internal.zzclq.m11394((com.google.android.gms.internal.zzcmc[]) r2, (java.lang.String) r4, (java.lang.Object) r5)     // Catch:{ all -> 0x01cb }
            r0 = r28
            r0.f9716 = r2     // Catch:{ all -> 0x01cb }
        L_0x071b:
            java.lang.Boolean r2 = r3.f9190     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x073f
            java.lang.Boolean r2 = r3.f9190     // Catch:{ all -> 0x01cb }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x073f
            r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r28
            com.google.android.gms.internal.zzcmc[] r2 = r0.f9716     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "_efs"
            r4 = 1
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = com.google.android.gms.internal.zzclq.m11394((com.google.android.gms.internal.zzcmc[]) r2, (java.lang.String) r3, (java.lang.Object) r4)     // Catch:{ all -> 0x01cb }
            r0 = r28
            r0.f9716 = r2     // Catch:{ all -> 0x01cb }
        L_0x073f:
            int r2 = r18 + 1
            r24[r18] = r28     // Catch:{ all -> 0x01cb }
        L_0x0743:
            int r3 = r20 + 1
            r20 = r3
            r18 = r2
            goto L_0x06b7
        L_0x074b:
            r2 = 1
            java.lang.String r3 = "_dbg"
            r4 = 1
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x01cb }
            r0 = r28
            boolean r3 = m11011((com.google.android.gms.internal.zzcmb) r0, (java.lang.String) r3, (java.lang.Object) r4)     // Catch:{ all -> 0x01cb }
            if (r3 != 0) goto L_0x0afd
            com.google.android.gms.internal.zzcig r2 = r30.m11031()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            int r2 = r2.m10938(r3, r4)     // Catch:{ all -> 0x01cb }
            r19 = r2
        L_0x0771:
            if (r19 > 0) goto L_0x078e
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "Sample rate must be positive. event, rate"
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r19)     // Catch:{ all -> 0x01cb }
            r2.m10851(r3, r4, r5)     // Catch:{ all -> 0x01cb }
            int r2 = r18 + 1
            r24[r18] = r28     // Catch:{ all -> 0x01cb }
            goto L_0x0743
        L_0x078e:
            r0 = r28
            java.lang.String r2 = r0.f9713     // Catch:{ all -> 0x01cb }
            r0 = r23
            java.lang.Object r2 = r0.get(r2)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r2 = (com.google.android.gms.internal.zzcgw) r2     // Catch:{ all -> 0x01cb }
            if (r2 != 0) goto L_0x0afa
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = r2.m10601((java.lang.String) r3, (java.lang.String) r4)     // Catch:{ all -> 0x01cb }
            if (r3 != 0) goto L_0x07e9
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "Event being bundled has no eventAggregate. appId, eventName"
            r0 = r21
            com.google.android.gms.internal.zzcme r4 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.f9757     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.String r5 = r0.f9713     // Catch:{ all -> 0x01cb }
            r2.m10851(r3, r4, r5)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = new com.google.android.gms.internal.zzcgw     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r2.f9757     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.String r5 = r0.f9713     // Catch:{ all -> 0x01cb }
            r6 = 1
            r8 = 1
            r0 = r28
            java.lang.Long r2 = r0.f9715     // Catch:{ all -> 0x01cb }
            long r10 = r2.longValue()     // Catch:{ all -> 0x01cb }
            r12 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r3.<init>(r4, r5, r6, r8, r10, r12, r14, r15, r16)     // Catch:{ all -> 0x01cb }
        L_0x07e9:
            r30.m11063()     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = "_eid"
            r0 = r28
            java.lang.Object r2 = com.google.android.gms.internal.zzclq.m11377((com.google.android.gms.internal.zzcmb) r0, (java.lang.String) r2)     // Catch:{ all -> 0x01cb }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x082b
            r4 = 1
        L_0x07fa:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x01cb }
            r5 = 1
            r0 = r19
            if (r0 != r5) goto L_0x082d
            int r2 = r18 + 1
            r24[r18] = r28     // Catch:{ all -> 0x01cb }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x0743
            java.lang.Long r4 = r3.f9188     // Catch:{ all -> 0x01cb }
            if (r4 != 0) goto L_0x0819
            java.lang.Long r4 = r3.f9189     // Catch:{ all -> 0x01cb }
            if (r4 != 0) goto L_0x0819
            java.lang.Boolean r4 = r3.f9190     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x0743
        L_0x0819:
            r4 = 0
            r5 = 0
            r6 = 0
            com.google.android.gms.internal.zzcgw r3 = r3.m10658(r4, r5, r6)     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            r0 = r23
            r0.put(r4, r3)     // Catch:{ all -> 0x01cb }
            goto L_0x0743
        L_0x082b:
            r4 = 0
            goto L_0x07fa
        L_0x082d:
            r0 = r25
            r1 = r19
            int r5 = r0.nextInt(r1)     // Catch:{ all -> 0x01cb }
            if (r5 != 0) goto L_0x087e
            r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r28
            com.google.android.gms.internal.zzcmc[] r2 = r0.f9716     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "_sr"
            r0 = r19
            long r6 = (long) r0     // Catch:{ all -> 0x01cb }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = com.google.android.gms.internal.zzclq.m11394((com.google.android.gms.internal.zzcmc[]) r2, (java.lang.String) r5, (java.lang.Object) r6)     // Catch:{ all -> 0x01cb }
            r0 = r28
            r0.f9716 = r2     // Catch:{ all -> 0x01cb }
            int r2 = r18 + 1
            r24[r18] = r28     // Catch:{ all -> 0x01cb }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x0867
            r4 = 0
            r0 = r19
            long r6 = (long) r0     // Catch:{ all -> 0x01cb }
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x01cb }
            r6 = 0
            com.google.android.gms.internal.zzcgw r3 = r3.m10658(r4, r5, r6)     // Catch:{ all -> 0x01cb }
        L_0x0867:
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.Long r5 = r0.f9715     // Catch:{ all -> 0x01cb }
            long r6 = r5.longValue()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = r3.m10655(r6)     // Catch:{ all -> 0x01cb }
            r0 = r23
            r0.put(r4, r3)     // Catch:{ all -> 0x01cb }
            goto L_0x0743
        L_0x087e:
            long r6 = r3.f9187     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.Long r5 = r0.f9715     // Catch:{ all -> 0x01cb }
            long r8 = r5.longValue()     // Catch:{ all -> 0x01cb }
            long r6 = r8 - r6
            long r6 = java.lang.Math.abs(r6)     // Catch:{ all -> 0x01cb }
            r8 = 86400000(0x5265c00, double:4.2687272E-316)
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 < 0) goto L_0x08f8
            r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r28
            com.google.android.gms.internal.zzcmc[] r2 = r0.f9716     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "_efs"
            r6 = 1
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = com.google.android.gms.internal.zzclq.m11394((com.google.android.gms.internal.zzcmc[]) r2, (java.lang.String) r5, (java.lang.Object) r6)     // Catch:{ all -> 0x01cb }
            r0 = r28
            r0.f9716 = r2     // Catch:{ all -> 0x01cb }
            r30.m11063()     // Catch:{ all -> 0x01cb }
            r0 = r28
            com.google.android.gms.internal.zzcmc[] r2 = r0.f9716     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "_sr"
            r0 = r19
            long r6 = (long) r0     // Catch:{ all -> 0x01cb }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmc[] r2 = com.google.android.gms.internal.zzclq.m11394((com.google.android.gms.internal.zzcmc[]) r2, (java.lang.String) r5, (java.lang.Object) r6)     // Catch:{ all -> 0x01cb }
            r0 = r28
            r0.f9716 = r2     // Catch:{ all -> 0x01cb }
            int r2 = r18 + 1
            r24[r18] = r28     // Catch:{ all -> 0x01cb }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x08e1
            r4 = 0
            r0 = r19
            long r6 = (long) r0     // Catch:{ all -> 0x01cb }
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x01cb }
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = r3.m10658(r4, r5, r6)     // Catch:{ all -> 0x01cb }
        L_0x08e1:
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            r0 = r28
            java.lang.Long r5 = r0.f9715     // Catch:{ all -> 0x01cb }
            long r6 = r5.longValue()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r3 = r3.m10655(r6)     // Catch:{ all -> 0x01cb }
            r0 = r23
            r0.put(r4, r3)     // Catch:{ all -> 0x01cb }
            goto L_0x0743
        L_0x08f8:
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x090d
            r0 = r28
            java.lang.String r4 = r0.f9713     // Catch:{ all -> 0x01cb }
            r5 = 0
            r6 = 0
            com.google.android.gms.internal.zzcgw r2 = r3.m10658(r2, r5, r6)     // Catch:{ all -> 0x01cb }
            r0 = r23
            r0.put(r4, r2)     // Catch:{ all -> 0x01cb }
        L_0x090d:
            r2 = r18
            goto L_0x0743
        L_0x0911:
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r2 = r0.f9752     // Catch:{ all -> 0x01cb }
            int r2 = r2.length     // Catch:{ all -> 0x01cb }
            r0 = r18
            if (r0 >= r2) goto L_0x0928
            r0 = r24
            r1 = r18
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r0, r1)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcmb[] r2 = (com.google.android.gms.internal.zzcmb[]) r2     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9752 = r2     // Catch:{ all -> 0x01cb }
        L_0x0928:
            java.util.Set r2 = r23.entrySet()     // Catch:{ all -> 0x01cb }
            java.util.Iterator r3 = r2.iterator()     // Catch:{ all -> 0x01cb }
        L_0x0930:
            boolean r2 = r3.hasNext()     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x094a
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x01cb }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r4 = r30.m11024()     // Catch:{ all -> 0x01cb }
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgw r2 = (com.google.android.gms.internal.zzcgw) r2     // Catch:{ all -> 0x01cb }
            r4.m10608((com.google.android.gms.internal.zzcgw) r2)     // Catch:{ all -> 0x01cb }
            goto L_0x0930
        L_0x094a:
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9751 = r2     // Catch:{ all -> 0x01cb }
            r2 = -9223372036854775808
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9725 = r2     // Catch:{ all -> 0x01cb }
            r2 = 0
        L_0x0962:
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r3 = r0.f9752     // Catch:{ all -> 0x01cb }
            int r3 = r3.length     // Catch:{ all -> 0x01cb }
            if (r2 >= r3) goto L_0x09a2
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r3 = r0.f9752     // Catch:{ all -> 0x01cb }
            r3 = r3[r2]     // Catch:{ all -> 0x01cb }
            java.lang.Long r4 = r3.f9715     // Catch:{ all -> 0x01cb }
            long r4 = r4.longValue()     // Catch:{ all -> 0x01cb }
            r0 = r22
            java.lang.Long r6 = r0.f9751     // Catch:{ all -> 0x01cb }
            long r6 = r6.longValue()     // Catch:{ all -> 0x01cb }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x0987
            java.lang.Long r4 = r3.f9715     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9751 = r4     // Catch:{ all -> 0x01cb }
        L_0x0987:
            java.lang.Long r4 = r3.f9715     // Catch:{ all -> 0x01cb }
            long r4 = r4.longValue()     // Catch:{ all -> 0x01cb }
            r0 = r22
            java.lang.Long r6 = r0.f9725     // Catch:{ all -> 0x01cb }
            long r6 = r6.longValue()     // Catch:{ all -> 0x01cb }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x099f
            java.lang.Long r3 = r3.f9715     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9725 = r3     // Catch:{ all -> 0x01cb }
        L_0x099f:
            int r2 = r2 + 1
            goto L_0x0962
        L_0x09a2:
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r6 = r2.f9757     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgh r7 = r2.m10592(r6)     // Catch:{ all -> 0x01cb }
            if (r7 != 0) goto L_0x0a38
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "Bundling raw events w/o app info. appId"
            r0 = r21
            com.google.android.gms.internal.zzcme r4 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.f9757     // Catch:{ all -> 0x01cb }
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r4)     // Catch:{ all -> 0x01cb }
            r2.m10850(r3, r4)     // Catch:{ all -> 0x01cb }
        L_0x09ca:
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r2 = r0.f9752     // Catch:{ all -> 0x01cb }
            int r2 = r2.length     // Catch:{ all -> 0x01cb }
            if (r2 <= 0) goto L_0x0a06
            com.google.android.gms.internal.zzcig r2 = r30.m11031()     // Catch:{ all -> 0x01cb }
            r0 = r21
            com.google.android.gms.internal.zzcme r3 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = r3.f9757     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcly r2 = r2.m10944((java.lang.String) r3)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x09e5
            java.lang.Long r3 = r2.f9701     // Catch:{ all -> 0x01cb }
            if (r3 != 0) goto L_0x0abe
        L_0x09e5:
            r0 = r21
            com.google.android.gms.internal.zzcme r2 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r2.f9745     // Catch:{ all -> 0x01cb }
            boolean r2 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0aa4
            r2 = -1
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9759 = r2     // Catch:{ all -> 0x01cb }
        L_0x09fb:
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r0 = r22
            r1 = r17
            r2.m10614((com.google.android.gms.internal.zzcme) r0, (boolean) r1)     // Catch:{ all -> 0x01cb }
        L_0x0a06:
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r0 = r21
            java.util.List<java.lang.Long> r3 = r0.f9420     // Catch:{ all -> 0x01cb }
            r2.m10610((java.util.List<java.lang.Long>) r3)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r3 = r30.m11024()     // Catch:{ all -> 0x01cb }
            android.database.sqlite.SQLiteDatabase r2 = r3.m10587()     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0ac6 }
            r7 = 0
            r5[r7] = r6     // Catch:{ SQLiteException -> 0x0ac6 }
            r7 = 1
            r5[r7] = r6     // Catch:{ SQLiteException -> 0x0ac6 }
            r2.execSQL(r4, r5)     // Catch:{ SQLiteException -> 0x0ac6 }
        L_0x0a28:
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r2.m10584()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()
            r2.m10586()
            r2 = 1
        L_0x0a37:
            return r2
        L_0x0a38:
            r0 = r22
            com.google.android.gms.internal.zzcmb[] r2 = r0.f9752     // Catch:{ all -> 0x01cb }
            int r2 = r2.length     // Catch:{ all -> 0x01cb }
            if (r2 <= 0) goto L_0x09ca
            long r2 = r7.m10466()     // Catch:{ all -> 0x01cb }
            r4 = 0
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x0aa0
            java.lang.Long r4 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x01cb }
        L_0x0a4d:
            r0 = r22
            r0.f9728 = r4     // Catch:{ all -> 0x01cb }
            long r4 = r7.m10463()     // Catch:{ all -> 0x01cb }
            r8 = 0
            int r8 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r8 != 0) goto L_0x0af7
        L_0x0a5b:
            r4 = 0
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x0aa2
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x01cb }
        L_0x0a65:
            r0 = r22
            r0.f9726 = r2     // Catch:{ all -> 0x01cb }
            r7.m10477()     // Catch:{ all -> 0x01cb }
            long r2 = r7.m10509()     // Catch:{ all -> 0x01cb }
            int r2 = (int) r2     // Catch:{ all -> 0x01cb }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9740 = r2     // Catch:{ all -> 0x01cb }
            r0 = r22
            java.lang.Long r2 = r0.f9751     // Catch:{ all -> 0x01cb }
            long r2 = r2.longValue()     // Catch:{ all -> 0x01cb }
            r7.m10506((long) r2)     // Catch:{ all -> 0x01cb }
            r0 = r22
            java.lang.Long r2 = r0.f9725     // Catch:{ all -> 0x01cb }
            long r2 = r2.longValue()     // Catch:{ all -> 0x01cb }
            r7.m10496((long) r2)     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = r7.m10489()     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9741 = r2     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r2.m10607((com.google.android.gms.internal.zzcgh) r7)     // Catch:{ all -> 0x01cb }
            goto L_0x09ca
        L_0x0aa0:
            r4 = 0
            goto L_0x0a4d
        L_0x0aa2:
            r2 = 0
            goto L_0x0a65
        L_0x0aa4:
            com.google.android.gms.internal.zzchm r2 = r30.m11016()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x01cb }
            java.lang.String r3 = "Did not find measurement config or missing version info. appId"
            r0 = r21
            com.google.android.gms.internal.zzcme r4 = r0.f9423     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.f9757     // Catch:{ all -> 0x01cb }
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r4)     // Catch:{ all -> 0x01cb }
            r2.m10850(r3, r4)     // Catch:{ all -> 0x01cb }
            goto L_0x09fb
        L_0x0abe:
            java.lang.Long r2 = r2.f9701     // Catch:{ all -> 0x01cb }
            r0 = r22
            r0.f9759 = r2     // Catch:{ all -> 0x01cb }
            goto L_0x09fb
        L_0x0ac6:
            r2 = move-exception
            com.google.android.gms.internal.zzchm r3 = r3.m11096()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcho r3 = r3.m10832()     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "Failed to remove unused event metadata. appId"
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r6)     // Catch:{ all -> 0x01cb }
            r3.m10851(r4, r5, r2)     // Catch:{ all -> 0x01cb }
            goto L_0x0a28
        L_0x0adb:
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()     // Catch:{ all -> 0x01cb }
            r2.m10584()     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzcgo r2 = r30.m11024()
            r2.m10586()
            r2 = 0
            goto L_0x0a37
        L_0x0aec:
            r2 = move-exception
            r3 = r11
            goto L_0x0372
        L_0x0af0:
            r2 = move-exception
            goto L_0x02ff
        L_0x0af3:
            r2 = move-exception
            r4 = r12
            goto L_0x02ff
        L_0x0af7:
            r2 = r4
            goto L_0x0a5b
        L_0x0afa:
            r3 = r2
            goto L_0x07e9
        L_0x0afd:
            r19 = r2
            goto L_0x0771
        L_0x0b01:
            r3 = r4
            goto L_0x05c5
        L_0x0b04:
            r17 = r2
            goto L_0x054b
        L_0x0b08:
            r2 = r4
            goto L_0x03f6
        L_0x0b0b:
            r2 = r12
            r4 = r17
            goto L_0x015e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcim.m11012(java.lang.String, long):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m11013(String str, zzcha zzcha) {
        long longValue;
        zzclp zzclp;
        String r2 = zzcha.f9200.m10662("currency");
        if ("ecommerce_purchase".equals(zzcha.f9203)) {
            double doubleValue = zzcha.f9200.m10663("value").doubleValue() * 1000000.0d;
            if (doubleValue == 0.0d) {
                doubleValue = ((double) zzcha.f9200.m10661("value").longValue()) * 1000000.0d;
            }
            if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                m11016().m10834().m10851("Data lost. Currency value is too big. appId", zzchm.m10812(str), Double.valueOf(doubleValue));
                return false;
            }
            longValue = Math.round(doubleValue);
        } else {
            longValue = zzcha.f9200.m10661("value").longValue();
        }
        if (!TextUtils.isEmpty(r2)) {
            String upperCase = r2.toUpperCase(Locale.US);
            if (upperCase.matches("[A-Z]{3}")) {
                String valueOf = String.valueOf("_ltv_");
                String valueOf2 = String.valueOf(upperCase);
                String concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                zzclp r0 = m11024().m10598(str, concat);
                if (r0 == null || !(r0.f9654 instanceof Long)) {
                    zzcgo r1 = m11024();
                    int r02 = this.f9414.m10544(str, zzchc.f9231) - 1;
                    zzbq.m9122(str);
                    r1.m11109();
                    r1.m11115();
                    try {
                        r1.m10587().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str, str, String.valueOf(r02)});
                    } catch (SQLiteException e) {
                        r1.m11096().m10832().m10851("Error pruning currencies. appId", zzchm.m10812(str), e);
                    }
                    zzclp = new zzclp(str, zzcha.f9202, concat, this.f9417.m9243(), Long.valueOf(longValue));
                } else {
                    zzclp = new zzclp(str, zzcha.f9202, concat, this.f9417.m9243(), Long.valueOf(longValue + ((Long) r0.f9654).longValue()));
                }
                if (!m11024().m10613(zzclp)) {
                    m11016().m10832().m10852("Too many unique user properties are set. Ignoring user property. appId", zzchm.m10812(str), m11064().m10797(zzclp.f9657), zzclp.f9654);
                    m11063().m11443(str, 9, (String) null, (String) null, 0);
                }
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcma[] m11014(String str, zzcmg[] zzcmgArr, zzcmb[] zzcmbArr) {
        zzbq.m9122(str);
        return m11036().m10521(str, zzcmbArr, zzcmgArr);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    static void m11015() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzchm m11016() {
        m11002((zzcjl) this.f9411);
        return this.f9411;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final zzchm m11017() {
        if (this.f9411 == null || !this.f9411.m11113()) {
            return null;
        }
        return this.f9411;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzcih m11018() {
        m11002((zzcjl) this.f9379);
        return this.f9379;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final AppMeasurement m11019() {
        return this.f9398;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final FirebaseAnalytics m11020() {
        return this.f9402;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final Context m11021() {
        return this.f9412;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final zzcjn m11022() {
        m11002((zzcjl) this.f9389);
        return this.f9389;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final zzd m11023() {
        return this.f9417;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final zzcgo m11024() {
        m11002((zzcjl) this.f9385);
        return this.f9385;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzchi m11025() {
        m11002((zzcjl) this.f9387);
        return this.f9387;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final zzchq m11026() {
        m11002((zzcjl) this.f9416);
        return this.f9416;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final zzckc m11027() {
        m11002((zzcjl) this.f9394);
        return this.f9394;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzclf m11028() {
        m11002((zzcjl) this.f9381);
        return this.f9381;
    }

    /* renamed from: י  reason: contains not printable characters */
    public final zzckg m11029() {
        m11002((zzcjl) this.f9395);
        return this.f9395;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final zzcgu m11030() {
        m11002((zzcjl) this.f9396);
        return this.f9396;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final zzcig m11031() {
        m11002((zzcjl) this.f9383);
        return this.f9383;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final zzcih m11032() {
        return this.f9379;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final void m11033() {
        this.f9384++;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final zzchh m11034() {
        m11002((zzcjl) this.f9392);
        return this.f9392;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final void m11035() {
        m11018().m11109();
        m11051();
        if (!this.f9408) {
            m11016().m10836().m10849("This instance being marked as an uploader");
            m11018().m11109();
            m11051();
            if (m10998() && m10995()) {
                int r0 = m11004(this.f9418);
                int r1 = m11034().m10726();
                m11018().m11109();
                if (r0 > r1) {
                    m11016().m10832().m10851("Panic: can't downgrade version. Previous, current version", Integer.valueOf(r0), Integer.valueOf(r1));
                } else if (r0 < r1) {
                    if (m11010(r1, this.f9418)) {
                        m11016().m10848().m10851("Storage version upgraded. Previous, current version", Integer.valueOf(r0), Integer.valueOf(r1));
                    } else {
                        m11016().m10832().m10851("Storage version upgrade failed. Previous, current version", Integer.valueOf(r0), Integer.valueOf(r1));
                    }
                }
            }
            this.f9408 = true;
            m10996();
        }
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final zzcgk m11036() {
        m11002((zzcjl) this.f9400);
        return this.f9400;
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final zzcgd m11037() {
        m11009((zzcjk) this.f9405);
        return this.f9405;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final boolean m11038() {
        boolean z = false;
        m11018().m11109();
        m11051();
        if (this.f9414.m10539()) {
            return false;
        }
        Boolean r1 = this.f9414.m10545("firebase_analytics_collection_enabled");
        if (r1 != null) {
            z = r1.booleanValue();
        } else if (!zzbz.m4201()) {
            z = true;
        }
        return m11040().m10896(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public final long m11039() {
        Long valueOf = Long.valueOf(m11040().f9303.m10902());
        return valueOf.longValue() == 0 ? this.f9393 : Math.min(this.f9393, valueOf.longValue());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzchx m11040() {
        m11009((zzcjk) this.f9413);
        return this.f9413;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11041(zzcgi zzcgi) {
        m11018().m11109();
        m11051();
        zzbq.m9122(zzcgi.f9143);
        m11003(zzcgi);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11042(zzcgl zzcgl) {
        zzcgi r0 = m11000(zzcgl.f9156);
        if (r0 != null) {
            m11043(zzcgl, r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11043(zzcgl zzcgl, zzcgi zzcgi) {
        zzbq.m9120(zzcgl);
        zzbq.m9122(zzcgl.f9156);
        zzbq.m9120(zzcgl.f9155);
        zzbq.m9122(zzcgl.f9155.f9653);
        m11018().m11109();
        m11051();
        if (!TextUtils.isEmpty(zzcgi.f9140)) {
            if (!zzcgi.f9132) {
                m11003(zzcgi);
                return;
            }
            m11024().m10582();
            try {
                m11003(zzcgi);
                zzcgl r3 = m11024().m10595(zzcgl.f9156, zzcgl.f9155.f9653);
                if (r3 != null) {
                    m11016().m10845().m10851("Removing conditional user property", zzcgl.f9156, m11064().m10797(zzcgl.f9155.f9653));
                    m11024().m10590(zzcgl.f9156, zzcgl.f9155.f9653);
                    if (r3.f9152) {
                        m11024().m10594(zzcgl.f9156, zzcgl.f9155.f9653);
                    }
                    if (zzcgl.f9151 != null) {
                        Bundle bundle = null;
                        if (zzcgl.f9151.f9200 != null) {
                            bundle = zzcgl.f9151.f9200.m10660();
                        }
                        m11001(m11063().m11436(zzcgl.f9151.f9203, bundle, r3.f9153, zzcgl.f9151.f9201, true, false), zzcgi);
                    }
                } else {
                    m11016().m10834().m10851("Conditional user property doesn't exist", zzchm.m10812(zzcgl.f9156), m11064().m10797(zzcgl.f9155.f9653));
                }
                m11024().m10584();
            } finally {
                m11024().m10586();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11044(zzcln zzcln, zzcgi zzcgi) {
        m11018().m11109();
        m11051();
        if (!TextUtils.isEmpty(zzcgi.f9140)) {
            if (!zzcgi.f9132) {
                m11003(zzcgi);
                return;
            }
            m11016().m10845().m10850("Removing user property", m11064().m10797(zzcln.f9653));
            m11024().m10582();
            try {
                m11003(zzcgi);
                m11024().m10594(zzcgi.f9143, zzcln.f9653);
                m11024().m10584();
                m11016().m10845().m10850("User property removed", m11064().m10797(zzcln.f9653));
            } finally {
                m11024().m10586();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m11045() {
        boolean z = false;
        m11051();
        m11018().m11109();
        if (this.f9409 == null || this.f9410 == 0 || (this.f9409 != null && !this.f9409.booleanValue() && Math.abs(this.f9417.m9241() - this.f9410) > 1000)) {
            this.f9410 = this.f9417.m9241();
            if (m11063().m11401("android.permission.INTERNET") && m11063().m11401("android.permission.ACCESS_NETWORK_STATE") && (zzbhf.m10231(this.f9412).m10227() || (zzcid.m10910(this.f9412) && zzcla.m11286(this.f9412, false)))) {
                z = true;
            }
            this.f9409 = Boolean.valueOf(z);
            if (this.f9409.booleanValue()) {
                this.f9409 = Boolean.valueOf(m11063().m11398(m11034().m10725()));
            }
        }
        return this.f9409.booleanValue();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final byte[] m11046(zzcha zzcha, String str) {
        long j;
        m11051();
        m11018().m11109();
        m11015();
        zzbq.m9120(zzcha);
        zzbq.m9122(str);
        zzcmd zzcmd = new zzcmd();
        m11024().m10582();
        try {
            zzcgh r22 = m11024().m10592(str);
            if (r22 == null) {
                m11016().m10845().m10850("Log and bundle not available. package_name", str);
                return new byte[0];
            } else if (!r22.m10471()) {
                m11016().m10845().m10850("Log and bundle disabled. package_name", str);
                byte[] bArr = new byte[0];
                m11024().m10586();
                return bArr;
            } else {
                if (("_iap".equals(zzcha.f9203) || "ecommerce_purchase".equals(zzcha.f9203)) && !m11013(str, zzcha)) {
                    m11016().m10834().m10850("Failed to handle purchase event at single event bundle creation. appId", zzchm.m10812(str));
                }
                zzcme zzcme = new zzcme();
                zzcmd.f9723 = new zzcme[]{zzcme};
                zzcme.f9755 = 1;
                zzcme.f9739 = AbstractSpiCall.ANDROID_CLIENT_TYPE;
                zzcme.f9757 = r22.m10495();
                zzcme.f9731 = r22.m10486();
                zzcme.f9758 = r22.m10480();
                long r4 = r22.m10484();
                zzcme.f9750 = r4 == -2147483648L ? null : Integer.valueOf((int) r4);
                zzcme.f9735 = Long.valueOf(r22.m10473());
                zzcme.f9745 = r22.m10499();
                zzcme.f9738 = Long.valueOf(r22.m10468());
                if (m11038() && zzcgn.m10524() && this.f9414.m10549(zzcme.f9757)) {
                    m11034();
                    zzcme.f9746 = null;
                }
                Pair<String, Boolean> r5 = m11040().m10897(r22.m10495());
                if (r22.m10491() && r5 != null && !TextUtils.isEmpty((CharSequence) r5.first)) {
                    zzcme.f9737 = (String) r5.first;
                    zzcme.f9732 = (Boolean) r5.second;
                }
                m11030().m11115();
                zzcme.f9743 = Build.MODEL;
                m11030().m11115();
                zzcme.f9742 = Build.VERSION.RELEASE;
                zzcme.f9730 = Integer.valueOf((int) m11030().m10641());
                zzcme.f9733 = m11030().m10644();
                zzcme.f9734 = r22.m10502();
                zzcme.f9749 = r22.m10460();
                List<zzclp> r6 = m11024().m10603(r22.m10495());
                zzcme.f9754 = new zzcmg[r6.size()];
                for (int i = 0; i < r6.size(); i++) {
                    zzcmg zzcmg = new zzcmg();
                    zzcme.f9754[i] = zzcmg;
                    zzcmg.f9765 = r6.get(i).f9657;
                    zzcmg.f9768 = Long.valueOf(r6.get(i).f9656);
                    m11063().m11442(zzcmg, r6.get(i).f9654);
                }
                Bundle r42 = zzcha.f9200.m10660();
                if ("_iap".equals(zzcha.f9203)) {
                    r42.putLong("_c", 1);
                    m11016().m10845().m10849("Marking in-app purchase as real-time");
                    r42.putLong("_r", 1);
                }
                r42.putString("_o", zzcha.f9202);
                if (m11063().m11416(zzcme.f9757)) {
                    m11063().m11440(r42, "_dbg", (Object) 1L);
                    m11063().m11440(r42, "_r", (Object) 1L);
                }
                zzcgw r52 = m11024().m10601(str, zzcha.f9203);
                if (r52 == null) {
                    m11024().m10608(new zzcgw(str, zzcha.f9203, 1, 0, zzcha.f9201, 0, (Long) null, (Long) null, (Boolean) null));
                    j = 0;
                } else {
                    j = r52.f9191;
                    m11024().m10608(r52.m10657(zzcha.f9201).m10656());
                }
                zzcgv zzcgv = new zzcgv(this, zzcha.f9202, str, zzcha.f9203, zzcha.f9201, j, r42);
                zzcmb zzcmb = new zzcmb();
                zzcme.f9752 = new zzcmb[]{zzcmb};
                zzcmb.f9715 = Long.valueOf(zzcgv.f9185);
                zzcmb.f9713 = zzcgv.f9183;
                zzcmb.f9714 = Long.valueOf(zzcgv.f9184);
                zzcmb.f9716 = new zzcmc[zzcgv.f9182.m10664()];
                Iterator<String> it2 = zzcgv.f9182.iterator();
                int i2 = 0;
                while (it2.hasNext()) {
                    String next = it2.next();
                    zzcmc zzcmc = new zzcmc();
                    zzcmb.f9716[i2] = zzcmc;
                    zzcmc.f9722 = next;
                    m11063().m11441(zzcmc, zzcgv.f9182.m10665(next));
                    i2++;
                }
                zzcme.f9748 = m11014(r22.m10495(), zzcme.f9754, zzcme.f9752);
                zzcme.f9751 = zzcmb.f9715;
                zzcme.f9725 = zzcmb.f9715;
                long r43 = r22.m10466();
                zzcme.f9728 = r43 != 0 ? Long.valueOf(r43) : null;
                long r62 = r22.m10463();
                if (r62 != 0) {
                    r43 = r62;
                }
                zzcme.f9726 = r43 != 0 ? Long.valueOf(r43) : null;
                r22.m10477();
                zzcme.f9740 = Integer.valueOf((int) r22.m10509());
                zzcme.f9736 = 11910L;
                zzcme.f9753 = Long.valueOf(this.f9417.m9243());
                zzcme.f9747 = Boolean.TRUE;
                r22.m10506(zzcme.f9751.longValue());
                r22.m10496(zzcme.f9725.longValue());
                m11024().m10607(r22);
                m11024().m10584();
                m11024().m10586();
                try {
                    byte[] bArr2 = new byte[zzcmd.m12872()];
                    zzfjk r53 = zzfjk.m12822(bArr2, 0, bArr2.length);
                    zzcmd.m12877(r53);
                    r53.m12829();
                    return m11063().m11446(bArr2);
                } catch (IOException e) {
                    m11016().m10832().m10851("Data loss. Failed to bundle and serialize. appId", zzchm.m10812(str), e);
                    return null;
                }
            }
        } finally {
            m11024().m10586();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzcgn m11047() {
        return this.f9414;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m11048() {
        m11018().m11109();
        m11024().m10615();
        if (m11040().f9320.m10902() == 0) {
            m11040().f9320.m10903(this.f9417.m9243());
        }
        if (Long.valueOf(m11040().f9303.m10902()).longValue() == 0) {
            m11016().m10848().m10850("Persisting first open", Long.valueOf(this.f9393));
            m11040().f9303.m10903(this.f9393);
        }
        if (m11045()) {
            if (!TextUtils.isEmpty(m11034().m10725())) {
                String r0 = m11040().m10887();
                if (r0 == null) {
                    m11040().m10895(m11034().m10725());
                } else if (!r0.equals(m11034().m10725())) {
                    m11016().m10836().m10849("Rechecking which service to use due to a GMP App Id change");
                    m11040().m10890();
                    this.f9395.m11274();
                    this.f9395.m11255();
                    m11040().m10895(m11034().m10725());
                    m11040().f9303.m10903(this.f9393);
                    m11040().f9313.m10909((String) null);
                }
            }
            m11022().m11173(m11040().f9313.m10908());
            if (!TextUtils.isEmpty(m11034().m10725())) {
                zzcjn r02 = m11022();
                r02.m11109();
                r02.m11115();
                if (r02.f9487.m11045()) {
                    r02.m11103().m11254();
                    String r1 = r02.m11098().m10891();
                    if (!TextUtils.isEmpty(r1)) {
                        r02.m11093().m11115();
                        if (!r1.equals(Build.VERSION.RELEASE)) {
                            Bundle bundle = new Bundle();
                            bundle.putString("_po", r1);
                            r02.m11174("auto", "_ou", bundle);
                        }
                    }
                }
                m11029().m11267((AtomicReference<String>) new AtomicReference());
            }
        } else if (m11038()) {
            if (!m11063().m11401("android.permission.INTERNET")) {
                m11016().m10832().m10849("App is missing INTERNET permission");
            }
            if (!m11063().m11401("android.permission.ACCESS_NETWORK_STATE")) {
                m11016().m10832().m10849("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!zzbhf.m10231(this.f9412).m10227()) {
                if (!zzcid.m10910(this.f9412)) {
                    m11016().m10832().m10849("AppMeasurementReceiver not registered/enabled");
                }
                if (!zzcla.m11286(this.f9412, false)) {
                    m11016().m10832().m10849("AppMeasurementService not registered/enabled");
                }
            }
            m11016().m10832().m10849("Uploading is not possible. App measurement disabled");
        }
        m10996();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m11049(zzcgi zzcgi) {
        int i;
        zzcgh r0;
        ApplicationInfo applicationInfo;
        zzcgo r1;
        String r2;
        m11018().m11109();
        m11051();
        zzbq.m9120(zzcgi);
        zzbq.m9122(zzcgi.f9143);
        if (!TextUtils.isEmpty(zzcgi.f9140)) {
            zzcgh r02 = m11024().m10592(zzcgi.f9143);
            if (r02 != null && TextUtils.isEmpty(r02.m10499()) && !TextUtils.isEmpty(zzcgi.f9140)) {
                r02.m10464(0);
                m11024().m10607(r02);
                m11031().m10940(zzcgi.f9143);
            }
            if (!zzcgi.f9132) {
                m11003(zzcgi);
                return;
            }
            long j = zzcgi.f9133;
            if (j == 0) {
                j = this.f9417.m9243();
            }
            int i2 = zzcgi.f9134;
            if (i2 == 0 || i2 == 1) {
                i = i2;
            } else {
                m11016().m10834().m10851("Incorrect app type, assuming installed app. appId, appType", zzchm.m10812(zzcgi.f9143), Integer.valueOf(i2));
                i = 0;
            }
            m11024().m10582();
            try {
                r0 = m11024().m10592(zzcgi.f9143);
                if (!(r0 == null || r0.m10499() == null || r0.m10499().equals(zzcgi.f9140))) {
                    m11016().m10834().m10850("New GMP App Id passed in. Removing cached database data. appId", zzchm.m10812(r0.m10495()));
                    r1 = m11024();
                    r2 = r0.m10495();
                    r1.m11115();
                    r1.m11109();
                    zzbq.m9122(r2);
                    SQLiteDatabase r03 = r1.m10587();
                    String[] strArr = {r2};
                    int delete = r03.delete("audience_filter_values", "app_id=?", strArr) + r03.delete(TyphoonApp.VIDEO_TRACKING_EVENTS_KEY, "app_id=?", strArr) + 0 + r03.delete("user_attributes", "app_id=?", strArr) + r03.delete("conditional_properties", "app_id=?", strArr) + r03.delete("apps", "app_id=?", strArr) + r03.delete("raw_events", "app_id=?", strArr) + r03.delete("raw_events_metadata", "app_id=?", strArr) + r03.delete("event_filters", "app_id=?", strArr) + r03.delete("property_filters", "app_id=?", strArr);
                    if (delete > 0) {
                        r1.m11096().m10848().m10851("Deleted application data. app, records", r2, Integer.valueOf(delete));
                    }
                    r0 = null;
                }
            } catch (SQLiteException e) {
                r1.m11096().m10832().m10851("Error deleting application data. appId, error", zzchm.m10812(r2), e);
            } catch (Throwable th) {
                m11024().m10586();
                throw th;
            }
            if (r0 != null) {
                if (r0.m10480() != null && !r0.m10480().equals(zzcgi.f9142)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_pv", r0.m10480());
                    m11056(new zzcha("_au", new zzcgx(bundle), "auto", j), zzcgi);
                }
            }
            m11003(zzcgi);
            zzcgw zzcgw = null;
            if (i == 0) {
                zzcgw = m11024().m10601(zzcgi.f9143, "_f");
            } else if (i == 1) {
                zzcgw = m11024().m10601(zzcgi.f9143, "_v");
            }
            if (zzcgw == null) {
                long j2 = (1 + (j / 3600000)) * 3600000;
                if (i == 0) {
                    m11059(new zzcln("_fot", j, Long.valueOf(j2), "auto"), zzcgi);
                    m11018().m11109();
                    m11051();
                    Bundle bundle2 = new Bundle();
                    bundle2.putLong("_c", 1);
                    bundle2.putLong("_r", 1);
                    bundle2.putLong("_uwa", 0);
                    bundle2.putLong("_pfo", 0);
                    bundle2.putLong("_sys", 0);
                    bundle2.putLong("_sysu", 0);
                    if (this.f9412.getPackageManager() == null) {
                        m11016().m10832().m10850("PackageManager is null, first open report might be inaccurate. appId", zzchm.m10812(zzcgi.f9143));
                    } else {
                        PackageInfo packageInfo = null;
                        try {
                            packageInfo = zzbhf.m10231(this.f9412).m10222(zzcgi.f9143, 0);
                        } catch (PackageManager.NameNotFoundException e2) {
                            m11016().m10832().m10851("Package info is null, first open report might be inaccurate. appId", zzchm.m10812(zzcgi.f9143), e2);
                        }
                        if (packageInfo != null) {
                            if (packageInfo.firstInstallTime != 0) {
                                boolean z = false;
                                if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                    bundle2.putLong("_uwa", 1);
                                } else {
                                    z = true;
                                }
                                m11059(new zzcln("_fi", j, Long.valueOf(z ? 1 : 0), "auto"), zzcgi);
                            }
                        }
                        try {
                            applicationInfo = zzbhf.m10231(this.f9412).m10226(zzcgi.f9143, 0);
                        } catch (PackageManager.NameNotFoundException e3) {
                            m11016().m10832().m10851("Application info is null, first open report might be inaccurate. appId", zzchm.m10812(zzcgi.f9143), e3);
                            applicationInfo = null;
                        }
                        if (applicationInfo != null) {
                            if ((applicationInfo.flags & 1) != 0) {
                                bundle2.putLong("_sys", 1);
                            }
                            if ((applicationInfo.flags & 128) != 0) {
                                bundle2.putLong("_sysu", 1);
                            }
                        }
                    }
                    zzcgo r04 = m11024();
                    String str = zzcgi.f9143;
                    zzbq.m9122(str);
                    r04.m11109();
                    r04.m11115();
                    long r05 = r04.m10579(str, "first_open_count");
                    if (r05 >= 0) {
                        bundle2.putLong("_pfo", r05);
                    }
                    m11056(new zzcha("_f", new zzcgx(bundle2), "auto", j), zzcgi);
                } else if (i == 1) {
                    m11059(new zzcln("_fvt", j, Long.valueOf(j2), "auto"), zzcgi);
                    m11018().m11109();
                    m11051();
                    Bundle bundle3 = new Bundle();
                    bundle3.putLong("_c", 1);
                    bundle3.putLong("_r", 1);
                    m11056(new zzcha("_v", new zzcgx(bundle3), "auto", j), zzcgi);
                }
                Bundle bundle4 = new Bundle();
                bundle4.putLong("_et", 1);
                m11056(new zzcha("_e", new zzcgx(bundle4), "auto", j), zzcgi);
            } else if (zzcgi.f9136) {
                m11056(new zzcha("_cd", new zzcgx(new Bundle()), "auto", j), zzcgi);
            }
            m11024().m10584();
            m11024().m10586();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m11050(String str) {
        try {
            return (String) m11018().m10984(new zzcio(this, str)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            m11016().m10832().m10851("Failed to get app instance id. appId", zzchm.m10812(str), e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11051() {
        if (!this.f9407) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11052(int i, Throwable th, byte[] bArr) {
        zzcgo r4;
        m11018().m11109();
        m11051();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.f9401 = false;
                m10997();
                throw th2;
            }
        }
        List<Long> list = this.f9404;
        this.f9404 = null;
        if ((i == 200 || i == 204) && th == null) {
            try {
                m11040().f9320.m10903(this.f9417.m9243());
                m11040().f9319.m10903(0);
                m10996();
                m11016().m10848().m10851("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                m11024().m10582();
                try {
                    for (Long longValue : list) {
                        r4 = m11024();
                        long longValue2 = longValue.longValue();
                        r4.m11109();
                        r4.m11115();
                        if (r4.m10587().delete("queue", "rowid=?", new String[]{String.valueOf(longValue2)}) != 1) {
                            throw new SQLiteException("Deleted fewer rows from queue than expected");
                        }
                    }
                    m11024().m10584();
                    m11024().m10586();
                    if (!m11026().m10872() || !m10999()) {
                        this.f9382 = -1;
                        m10996();
                    } else {
                        m11065();
                    }
                    this.f9388 = 0;
                } catch (SQLiteException e) {
                    r4.m11096().m10832().m10850("Failed to delete a bundle in a queue table", e);
                    throw e;
                } catch (Throwable th3) {
                    m11024().m10586();
                    throw th3;
                }
            } catch (SQLiteException e2) {
                m11016().m10832().m10850("Database error while trying to delete uploaded bundles", e2);
                this.f9388 = this.f9417.m9241();
                m11016().m10848().m10850("Disable upload, time", Long.valueOf(this.f9388));
            }
        } else {
            m11016().m10848().m10851("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            m11040().f9319.m10903(this.f9417.m9243());
            if (i == 503 || i == 429) {
                m11040().f9317.m10903(this.f9417.m9243());
            }
            m10996();
        }
        this.f9401 = false;
        m10997();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11053(zzcgi zzcgi) {
        m11024().m10592(zzcgi.f9143);
        zzcgo r1 = m11024();
        String str = zzcgi.f9143;
        zzbq.m9122(str);
        r1.m11109();
        r1.m11115();
        try {
            SQLiteDatabase r0 = r1.m10587();
            String[] strArr = {str};
            int delete = r0.delete("audience_filter_values", "app_id=?", strArr) + r0.delete("apps", "app_id=?", strArr) + 0 + r0.delete(TyphoonApp.VIDEO_TRACKING_EVENTS_KEY, "app_id=?", strArr) + r0.delete("user_attributes", "app_id=?", strArr) + r0.delete("conditional_properties", "app_id=?", strArr) + r0.delete("raw_events", "app_id=?", strArr) + r0.delete("raw_events_metadata", "app_id=?", strArr) + r0.delete("queue", "app_id=?", strArr);
            if (delete > 0) {
                r1.m11096().m10848().m10851("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            r1.m11096().m10832().m10851("Error resetting analytics data. appId, error", zzchm.m10812(str), e);
        }
        m11049(m11005(this.f9412, zzcgi.f9143, zzcgi.f9140, zzcgi.f9132, zzcgi.f9144));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11054(zzcgl zzcgl) {
        zzcgi r0 = m11000(zzcgl.f9156);
        if (r0 != null) {
            m11055(zzcgl, r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11055(zzcgl zzcgl, zzcgi zzcgi) {
        boolean z = true;
        zzbq.m9120(zzcgl);
        zzbq.m9122(zzcgl.f9156);
        zzbq.m9120(zzcgl.f9153);
        zzbq.m9120(zzcgl.f9155);
        zzbq.m9122(zzcgl.f9155.f9653);
        m11018().m11109();
        m11051();
        if (!TextUtils.isEmpty(zzcgi.f9140)) {
            if (!zzcgi.f9132) {
                m11003(zzcgi);
                return;
            }
            zzcgl zzcgl2 = new zzcgl(zzcgl);
            zzcgl2.f9152 = false;
            m11024().m10582();
            try {
                zzcgl r5 = m11024().m10595(zzcgl2.f9156, zzcgl2.f9155.f9653);
                if (r5 != null && !r5.f9153.equals(zzcgl2.f9153)) {
                    m11016().m10834().m10852("Updating a conditional user property with different origin. name, origin, origin (from DB)", m11064().m10797(zzcgl2.f9155.f9653), zzcgl2.f9153, r5.f9153);
                }
                if (r5 != null && r5.f9152) {
                    zzcgl2.f9153 = r5.f9153;
                    zzcgl2.f9154 = r5.f9154;
                    zzcgl2.f9147 = r5.f9147;
                    zzcgl2.f9145 = r5.f9145;
                    zzcgl2.f9149 = r5.f9149;
                    zzcgl2.f9152 = r5.f9152;
                    zzcgl2.f9155 = new zzcln(zzcgl2.f9155.f9653, r5.f9155.f9650, zzcgl2.f9155.m11364(), r5.f9155.f9652);
                    z = false;
                } else if (TextUtils.isEmpty(zzcgl2.f9145)) {
                    zzcgl2.f9155 = new zzcln(zzcgl2.f9155.f9653, zzcgl2.f9154, zzcgl2.f9155.m11364(), zzcgl2.f9155.f9652);
                    zzcgl2.f9152 = true;
                } else {
                    z = false;
                }
                if (zzcgl2.f9152) {
                    zzcln zzcln = zzcgl2.f9155;
                    zzclp zzclp = new zzclp(zzcgl2.f9156, zzcgl2.f9153, zzcln.f9653, zzcln.f9650, zzcln.m11364());
                    if (m11024().m10613(zzclp)) {
                        m11016().m10845().m10852("User property updated immediately", zzcgl2.f9156, m11064().m10797(zzclp.f9657), zzclp.f9654);
                    } else {
                        m11016().m10832().m10852("(2)Too many active user properties, ignoring", zzchm.m10812(zzcgl2.f9156), m11064().m10797(zzclp.f9657), zzclp.f9654);
                    }
                    if (z && zzcgl2.f9149 != null) {
                        m11001(new zzcha(zzcgl2.f9149, zzcgl2.f9154), zzcgi);
                    }
                }
                if (m11024().m10611(zzcgl2)) {
                    m11016().m10845().m10852("Conditional property added", zzcgl2.f9156, m11064().m10797(zzcgl2.f9155.f9653), zzcgl2.f9155.m11364());
                } else {
                    m11016().m10832().m10852("Too many conditional properties, ignoring", zzchm.m10812(zzcgl2.f9156), m11064().m10797(zzcgl2.f9155.f9653), zzcgl2.f9155.m11364());
                }
                m11024().m10584();
            } finally {
                m11024().m10586();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11056(zzcha zzcha, zzcgi zzcgi) {
        List<zzcgl> r2;
        List<zzcgl> r22;
        List<zzcgl> r23;
        zzbq.m9120(zzcgi);
        zzbq.m9122(zzcgi.f9143);
        m11018().m11109();
        m11051();
        String str = zzcgi.f9143;
        long j = zzcha.f9201;
        m11063();
        if (zzclq.m11384(zzcha, zzcgi)) {
            if (!zzcgi.f9132) {
                m11003(zzcgi);
                return;
            }
            m11024().m10582();
            try {
                zzcgo r24 = m11024();
                zzbq.m9122(str);
                r24.m11109();
                r24.m11115();
                if (j < 0) {
                    r24.m11096().m10834().m10851("Invalid time querying timed out conditional properties", zzchm.m10812(str), Long.valueOf(j));
                    r2 = Collections.emptyList();
                } else {
                    r2 = r24.m10606("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j)});
                }
                for (zzcgl next : r2) {
                    if (next != null) {
                        m11016().m10845().m10852("User property timed out", next.f9156, m11064().m10797(next.f9155.f9653), next.f9155.m11364());
                        if (next.f9146 != null) {
                            m11001(new zzcha(next.f9146, j), zzcgi);
                        }
                        m11024().m10590(str, next.f9155.f9653);
                    }
                }
                zzcgo r25 = m11024();
                zzbq.m9122(str);
                r25.m11109();
                r25.m11115();
                if (j < 0) {
                    r25.m11096().m10834().m10851("Invalid time querying expired conditional properties", zzchm.m10812(str), Long.valueOf(j));
                    r22 = Collections.emptyList();
                } else {
                    r22 = r25.m10606("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j)});
                }
                ArrayList arrayList = new ArrayList(r22.size());
                for (zzcgl next2 : r22) {
                    if (next2 != null) {
                        m11016().m10845().m10852("User property expired", next2.f9156, m11064().m10797(next2.f9155.f9653), next2.f9155.m11364());
                        m11024().m10594(str, next2.f9155.f9653);
                        if (next2.f9151 != null) {
                            arrayList.add(next2.f9151);
                        }
                        m11024().m10590(str, next2.f9155.f9653);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i = 0;
                while (i < size) {
                    Object obj = arrayList2.get(i);
                    i++;
                    m11001(new zzcha((zzcha) obj, j), zzcgi);
                }
                zzcgo r26 = m11024();
                String str2 = zzcha.f9203;
                zzbq.m9122(str);
                zzbq.m9122(str2);
                r26.m11109();
                r26.m11115();
                if (j < 0) {
                    r26.m11096().m10834().m10852("Invalid time querying triggered conditional properties", zzchm.m10812(str), r26.m11111().m10805(str2), Long.valueOf(j));
                    r23 = Collections.emptyList();
                } else {
                    r23 = r26.m10606("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j)});
                }
                ArrayList arrayList3 = new ArrayList(r23.size());
                for (zzcgl next3 : r23) {
                    if (next3 != null) {
                        zzcln zzcln = next3.f9155;
                        zzclp zzclp = new zzclp(next3.f9156, next3.f9153, zzcln.f9653, j, zzcln.m11364());
                        if (m11024().m10613(zzclp)) {
                            m11016().m10845().m10852("User property triggered", next3.f9156, m11064().m10797(zzclp.f9657), zzclp.f9654);
                        } else {
                            m11016().m10832().m10852("Too many active user properties, ignoring", zzchm.m10812(next3.f9156), m11064().m10797(zzclp.f9657), zzclp.f9654);
                        }
                        if (next3.f9149 != null) {
                            arrayList3.add(next3.f9149);
                        }
                        next3.f9155 = new zzcln(zzclp);
                        next3.f9152 = true;
                        m11024().m10611(next3);
                    }
                }
                m11001(zzcha, zzcgi);
                ArrayList arrayList4 = arrayList3;
                int size2 = arrayList4.size();
                int i2 = 0;
                while (i2 < size2) {
                    Object obj2 = arrayList4.get(i2);
                    i2++;
                    m11001(new zzcha((zzcha) obj2, j), zzcgi);
                }
                m11024().m10584();
            } finally {
                m11024().m10586();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11057(zzcha zzcha, String str) {
        zzcgh r2 = m11024().m10592(str);
        if (r2 == null || TextUtils.isEmpty(r2.m10480())) {
            m11016().m10845().m10850("No app data available; dropping event", str);
            return;
        }
        try {
            String str2 = zzbhf.m10231(this.f9412).m10222(str, 0).versionName;
            if (r2.m10480() != null && !r2.m10480().equals(str2)) {
                m11016().m10834().m10850("App version does not match; dropping event. appId", zzchm.m10812(str));
                return;
            }
        } catch (PackageManager.NameNotFoundException e) {
            if (!"_ui".equals(zzcha.f9203)) {
                m11016().m10834().m10850("Could not find package. appId", zzchm.m10812(str));
            }
        }
        zzcha zzcha2 = zzcha;
        m11056(zzcha2, new zzcgi(str, r2.m10499(), r2.m10480(), r2.m10484(), r2.m10486(), r2.m10473(), r2.m10468(), (String) null, r2.m10471(), false, r2.m10460(), r2.m10490(), 0, 0, r2.m10491()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11058(zzcjl zzcjl) {
        this.f9380++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11059(zzcln zzcln, zzcgi zzcgi) {
        int i = 0;
        m11018().m11109();
        m11051();
        if (!TextUtils.isEmpty(zzcgi.f9140)) {
            if (!zzcgi.f9132) {
                m11003(zzcgi);
                return;
            }
            int r2 = m11063().m11420(zzcln.f9653);
            if (r2 != 0) {
                m11063();
                String r4 = zzclq.m11378(zzcln.f9653, 24, true);
                if (zzcln.f9653 != null) {
                    i = zzcln.f9653.length();
                }
                m11063().m11443(zzcgi.f9143, r2, "_ev", r4, i);
                return;
            }
            int r22 = m11063().m11423(zzcln.f9653, zzcln.m11364());
            if (r22 != 0) {
                m11063();
                String r42 = zzclq.m11378(zzcln.f9653, 24, true);
                Object r0 = zzcln.m11364();
                if (r0 != null && ((r0 instanceof String) || (r0 instanceof CharSequence))) {
                    i = String.valueOf(r0).length();
                }
                m11063().m11443(zzcgi.f9143, r22, "_ev", r42, i);
                return;
            }
            Object r6 = m11063().m11430(zzcln.f9653, zzcln.m11364());
            if (r6 != null) {
                zzclp zzclp = new zzclp(zzcgi.f9143, zzcln.f9652, zzcln.f9653, zzcln.f9650, r6);
                m11016().m10845().m10851("Setting user property", m11064().m10797(zzclp.f9657), r6);
                m11024().m10582();
                try {
                    m11003(zzcgi);
                    boolean r1 = m11024().m10613(zzclp);
                    m11024().m10584();
                    if (r1) {
                        m11016().m10845().m10851("User property set", m11064().m10797(zzclp.f9657), zzclp.f9654);
                    } else {
                        m11016().m10832().m10851("Too many unique user properties are set. Ignoring user property", m11064().m10797(zzclp.f9657), zzclp.f9654);
                        m11063().m11443(zzcgi.f9143, 9, (String) null, (String) null, 0);
                    }
                } finally {
                    m11024().m10586();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11060(Runnable runnable) {
        m11018().m11109();
        if (this.f9406 == null) {
            this.f9406 = new ArrayList();
        }
        this.f9406.add(runnable);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11061(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        boolean z = true;
        m11018().m11109();
        m11051();
        zzbq.m9122(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.f9386 = false;
                m10997();
                throw th2;
            }
        }
        m11016().m10848().m10850("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        m11024().m10582();
        zzcgh r4 = m11024().m10592(str);
        boolean z2 = (i == 200 || i == 204 || i == 304) && th == null;
        if (r4 == null) {
            m11016().m10834().m10850("App does not exist in onConfigFetched. appId", zzchm.m10812(str));
        } else if (z2 || i == 404) {
            List list = map != null ? map.get("Last-Modified") : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i == 404 || i == 304) {
                if (m11031().m10944(str) == null && !m11031().m10947(str, (byte[]) null, (String) null)) {
                    m11024().m10586();
                    this.f9386 = false;
                    m10997();
                    return;
                }
            } else if (!m11031().m10947(str, bArr, str2)) {
                m11024().m10586();
                this.f9386 = false;
                m10997();
                return;
            }
            r4.m10464(this.f9417.m9243());
            m11024().m10607(r4);
            if (i == 404) {
                m11016().m10835().m10850("Config not found. Using empty config. appId", str);
            } else {
                m11016().m10848().m10851("Successfully fetched config. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
            }
            if (!m11026().m10872() || !m10999()) {
                m10996();
            } else {
                m11065();
            }
        } else {
            r4.m10467(this.f9417.m9243());
            m11024().m10607(r4);
            m11016().m10848().m10851("Fetching config failed. code, error", Integer.valueOf(i), th);
            m11031().m10942(str);
            m11040().f9319.m10903(this.f9417.m9243());
            if (!(i == 503 || i == 429)) {
                z = false;
            }
            if (z) {
                m11040().f9317.m10903(this.f9417.m9243());
            }
            m10996();
        }
        m11024().m10584();
        m11024().m10586();
        this.f9386 = false;
        m10997();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11062(boolean z) {
        m10996();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final zzclq m11063() {
        m11009((zzcjk) this.f9403);
        return this.f9403;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final zzchk m11064() {
        m11009((zzcjk) this.f9391);
        return this.f9391;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x02d2  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0189 A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0192 A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01e0 A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01ef A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0215 A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x021d A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x023c A[Catch:{ MalformedURLException -> 0x0290, all -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0286  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0288 A[SYNTHETIC, Splitter:B:86:0x0288] */
    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m11065() {
        /*
            r14 = this;
            com.google.android.gms.internal.zzcih r2 = r14.m11018()
            r2.m11109()
            r14.m11051()
            r2 = 1
            r14.f9390 = r2
            com.google.android.gms.internal.zzckg r2 = r14.m11029()     // Catch:{ all -> 0x02a4 }
            java.lang.Boolean r2 = r2.m11271()     // Catch:{ all -> 0x02a4 }
            if (r2 != 0) goto L_0x002c
            com.google.android.gms.internal.zzchm r2 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x02a4 }
            java.lang.String r3 = "Upload data called on the client side before use of service was decided"
            r2.m10849(r3)     // Catch:{ all -> 0x02a4 }
            r2 = 0
            r14.f9390 = r2
            r14.m10997()
        L_0x002b:
            return
        L_0x002c:
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x02a4 }
            if (r2 == 0) goto L_0x0047
            com.google.android.gms.internal.zzchm r2 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x02a4 }
            java.lang.String r3 = "Upload called in the client side when service should be used"
            r2.m10849(r3)     // Catch:{ all -> 0x02a4 }
            r2 = 0
            r14.f9390 = r2
            r14.m10997()
            goto L_0x002b
        L_0x0047:
            long r2 = r14.f9388     // Catch:{ all -> 0x02a4 }
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0059
            r14.m10996()     // Catch:{ all -> 0x02a4 }
            r2 = 0
            r14.f9390 = r2
            r14.m10997()
            goto L_0x002b
        L_0x0059:
            com.google.android.gms.internal.zzcih r2 = r14.m11018()     // Catch:{ all -> 0x02a4 }
            r2.m11109()     // Catch:{ all -> 0x02a4 }
            java.util.List<java.lang.Long> r2 = r14.f9404     // Catch:{ all -> 0x02a4 }
            if (r2 == 0) goto L_0x007c
            r2 = 1
        L_0x0065:
            if (r2 == 0) goto L_0x007e
            com.google.android.gms.internal.zzchm r2 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcho r2 = r2.m10848()     // Catch:{ all -> 0x02a4 }
            java.lang.String r3 = "Uploading requested multiple times"
            r2.m10849(r3)     // Catch:{ all -> 0x02a4 }
            r2 = 0
            r14.f9390 = r2
            r14.m10997()
            goto L_0x002b
        L_0x007c:
            r2 = 0
            goto L_0x0065
        L_0x007e:
            com.google.android.gms.internal.zzchq r2 = r14.m11026()     // Catch:{ all -> 0x02a4 }
            boolean r2 = r2.m10872()     // Catch:{ all -> 0x02a4 }
            if (r2 != 0) goto L_0x00a0
            com.google.android.gms.internal.zzchm r2 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcho r2 = r2.m10848()     // Catch:{ all -> 0x02a4 }
            java.lang.String r3 = "Network not connected, ignoring upload request"
            r2.m10849(r3)     // Catch:{ all -> 0x02a4 }
            r14.m10996()     // Catch:{ all -> 0x02a4 }
            r2 = 0
            r14.f9390 = r2
            r14.m10997()
            goto L_0x002b
        L_0x00a0:
            com.google.android.gms.common.util.zzd r2 = r14.f9417     // Catch:{ all -> 0x02a4 }
            long r10 = r2.m9243()     // Catch:{ all -> 0x02a4 }
            long r2 = com.google.android.gms.internal.zzcgn.m10523()     // Catch:{ all -> 0x02a4 }
            long r2 = r10 - r2
            r4 = 0
            r14.m11012((java.lang.String) r4, (long) r2)     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzchx r2 = r14.m11040()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcia r2 = r2.f9320     // Catch:{ all -> 0x02a4 }
            long r2 = r2.m10902()     // Catch:{ all -> 0x02a4 }
            r4 = 0
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x00d8
            com.google.android.gms.internal.zzchm r4 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcho r4 = r4.m10845()     // Catch:{ all -> 0x02a4 }
            java.lang.String r5 = "Uploading events. Elapsed time since last upload attempt (ms)"
            long r2 = r10 - r2
            long r2 = java.lang.Math.abs(r2)     // Catch:{ all -> 0x02a4 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x02a4 }
            r4.m10850(r5, r2)     // Catch:{ all -> 0x02a4 }
        L_0x00d8:
            com.google.android.gms.internal.zzcgo r2 = r14.m11024()     // Catch:{ all -> 0x02a4 }
            java.lang.String r4 = r2.m10588()     // Catch:{ all -> 0x02a4 }
            boolean r2 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x02a4 }
            if (r2 != 0) goto L_0x02ac
            long r2 = r14.f9382     // Catch:{ all -> 0x02a4 }
            r6 = -1
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 != 0) goto L_0x00f8
            com.google.android.gms.internal.zzcgo r2 = r14.m11024()     // Catch:{ all -> 0x02a4 }
            long r2 = r2.m10580()     // Catch:{ all -> 0x02a4 }
            r14.f9382 = r2     // Catch:{ all -> 0x02a4 }
        L_0x00f8:
            com.google.android.gms.internal.zzcgn r2 = r14.f9414     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzchd<java.lang.Integer> r3 = com.google.android.gms.internal.zzchc.f9223     // Catch:{ all -> 0x02a4 }
            int r2 = r2.m10544(r4, r3)     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcgn r3 = r14.f9414     // Catch:{ all -> 0x02a4 }
            r5 = 0
            com.google.android.gms.internal.zzchd<java.lang.Integer> r6 = com.google.android.gms.internal.zzchc.f9227     // Catch:{ all -> 0x02a4 }
            int r3 = r3.m10544(r4, r6)     // Catch:{ all -> 0x02a4 }
            int r3 = java.lang.Math.max(r5, r3)     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcgo r5 = r14.m11024()     // Catch:{ all -> 0x02a4 }
            java.util.List r3 = r5.m10604((java.lang.String) r4, (int) r2, (int) r3)     // Catch:{ all -> 0x02a4 }
            boolean r2 = r3.isEmpty()     // Catch:{ all -> 0x02a4 }
            if (r2 != 0) goto L_0x027e
            r5 = 0
            java.util.Iterator r6 = r3.iterator()     // Catch:{ all -> 0x02a4 }
        L_0x0120:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x02a4 }
            if (r2 == 0) goto L_0x02d8
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x02a4 }
            android.util.Pair r2 = (android.util.Pair) r2     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r2.first     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme r2 = (com.google.android.gms.internal.zzcme) r2     // Catch:{ all -> 0x02a4 }
            java.lang.String r7 = r2.f9737     // Catch:{ all -> 0x02a4 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x02a4 }
            if (r7 != 0) goto L_0x0120
            java.lang.String r2 = r2.f9737     // Catch:{ all -> 0x02a4 }
            r6 = r2
        L_0x013b:
            if (r6 == 0) goto L_0x02d5
            r2 = 0
            r5 = r2
        L_0x013f:
            int r2 = r3.size()     // Catch:{ all -> 0x02a4 }
            if (r5 >= r2) goto L_0x02d5
            java.lang.Object r2 = r3.get(r5)     // Catch:{ all -> 0x02a4 }
            android.util.Pair r2 = (android.util.Pair) r2     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r2.first     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme r2 = (com.google.android.gms.internal.zzcme) r2     // Catch:{ all -> 0x02a4 }
            java.lang.String r7 = r2.f9737     // Catch:{ all -> 0x02a4 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x02a4 }
            if (r7 != 0) goto L_0x01db
            java.lang.String r2 = r2.f9737     // Catch:{ all -> 0x02a4 }
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x02a4 }
            if (r2 != 0) goto L_0x01db
            r2 = 0
            java.util.List r2 = r3.subList(r2, r5)     // Catch:{ all -> 0x02a4 }
            r6 = r2
        L_0x0165:
            com.google.android.gms.internal.zzcmd r7 = new com.google.android.gms.internal.zzcmd     // Catch:{ all -> 0x02a4 }
            r7.<init>()     // Catch:{ all -> 0x02a4 }
            int r2 = r6.size()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme[] r2 = new com.google.android.gms.internal.zzcme[r2]     // Catch:{ all -> 0x02a4 }
            r7.f9723 = r2     // Catch:{ all -> 0x02a4 }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x02a4 }
            int r2 = r6.size()     // Catch:{ all -> 0x02a4 }
            r8.<init>(r2)     // Catch:{ all -> 0x02a4 }
            boolean r2 = com.google.android.gms.internal.zzcgn.m10524()     // Catch:{ all -> 0x02a4 }
            if (r2 == 0) goto L_0x01e0
            com.google.android.gms.internal.zzcgn r2 = r14.f9414     // Catch:{ all -> 0x02a4 }
            boolean r2 = r2.m10549(r4)     // Catch:{ all -> 0x02a4 }
            if (r2 == 0) goto L_0x01e0
            r2 = 1
            r3 = r2
        L_0x018b:
            r2 = 0
            r5 = r2
        L_0x018d:
            com.google.android.gms.internal.zzcme[] r2 = r7.f9723     // Catch:{ all -> 0x02a4 }
            int r2 = r2.length     // Catch:{ all -> 0x02a4 }
            if (r5 >= r2) goto L_0x01e3
            com.google.android.gms.internal.zzcme[] r9 = r7.f9723     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r6.get(r5)     // Catch:{ all -> 0x02a4 }
            android.util.Pair r2 = (android.util.Pair) r2     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r2.first     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme r2 = (com.google.android.gms.internal.zzcme) r2     // Catch:{ all -> 0x02a4 }
            r9[r5] = r2     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r6.get(r5)     // Catch:{ all -> 0x02a4 }
            android.util.Pair r2 = (android.util.Pair) r2     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r2.second     // Catch:{ all -> 0x02a4 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x02a4 }
            r8.add(r2)     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme[] r2 = r7.f9723     // Catch:{ all -> 0x02a4 }
            r2 = r2[r5]     // Catch:{ all -> 0x02a4 }
            r12 = 11910(0x2e86, double:5.8843E-320)
            java.lang.Long r9 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x02a4 }
            r2.f9736 = r9     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme[] r2 = r7.f9723     // Catch:{ all -> 0x02a4 }
            r2 = r2[r5]     // Catch:{ all -> 0x02a4 }
            java.lang.Long r9 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x02a4 }
            r2.f9753 = r9     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcme[] r2 = r7.f9723     // Catch:{ all -> 0x02a4 }
            r2 = r2[r5]     // Catch:{ all -> 0x02a4 }
            r9 = 0
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ all -> 0x02a4 }
            r2.f9747 = r9     // Catch:{ all -> 0x02a4 }
            if (r3 != 0) goto L_0x01d7
            com.google.android.gms.internal.zzcme[] r2 = r7.f9723     // Catch:{ all -> 0x02a4 }
            r2 = r2[r5]     // Catch:{ all -> 0x02a4 }
            r9 = 0
            r2.f9746 = r9     // Catch:{ all -> 0x02a4 }
        L_0x01d7:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x018d
        L_0x01db:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x013f
        L_0x01e0:
            r2 = 0
            r3 = r2
            goto L_0x018b
        L_0x01e3:
            r2 = 0
            com.google.android.gms.internal.zzchm r3 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            r5 = 2
            boolean r3 = r3.m10844((int) r5)     // Catch:{ all -> 0x02a4 }
            if (r3 == 0) goto L_0x02d2
            com.google.android.gms.internal.zzchk r2 = r14.m11064()     // Catch:{ all -> 0x02a4 }
            java.lang.String r2 = r2.m10804((com.google.android.gms.internal.zzcmd) r7)     // Catch:{ all -> 0x02a4 }
            r3 = r2
        L_0x01f8:
            com.google.android.gms.internal.zzclq r2 = r14.m11063()     // Catch:{ all -> 0x02a4 }
            byte[] r6 = r2.m11445((com.google.android.gms.internal.zzcmd) r7)     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzchd<java.lang.String> r2 = com.google.android.gms.internal.zzchc.f9221     // Catch:{ all -> 0x02a4 }
            java.lang.Object r2 = r2.m10671()     // Catch:{ all -> 0x02a4 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x02a4 }
            r9 = r0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0290 }
            r5.<init>(r9)     // Catch:{ MalformedURLException -> 0x0290 }
            boolean r2 = r8.isEmpty()     // Catch:{ MalformedURLException -> 0x0290 }
            if (r2 != 0) goto L_0x0286
            r2 = 1
        L_0x0216:
            com.google.android.gms.common.internal.zzbq.m9116((boolean) r2)     // Catch:{ MalformedURLException -> 0x0290 }
            java.util.List<java.lang.Long> r2 = r14.f9404     // Catch:{ MalformedURLException -> 0x0290 }
            if (r2 == 0) goto L_0x0288
            com.google.android.gms.internal.zzchm r2 = r14.m11016()     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ MalformedURLException -> 0x0290 }
            java.lang.String r8 = "Set uploading progress before finishing the previous upload"
            r2.m10849(r8)     // Catch:{ MalformedURLException -> 0x0290 }
        L_0x022b:
            com.google.android.gms.internal.zzchx r2 = r14.m11040()     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzcia r2 = r2.f9319     // Catch:{ MalformedURLException -> 0x0290 }
            r2.m10903(r10)     // Catch:{ MalformedURLException -> 0x0290 }
            java.lang.String r2 = "?"
            com.google.android.gms.internal.zzcme[] r8 = r7.f9723     // Catch:{ MalformedURLException -> 0x0290 }
            int r8 = r8.length     // Catch:{ MalformedURLException -> 0x0290 }
            if (r8 <= 0) goto L_0x0243
            com.google.android.gms.internal.zzcme[] r2 = r7.f9723     // Catch:{ MalformedURLException -> 0x0290 }
            r7 = 0
            r2 = r2[r7]     // Catch:{ MalformedURLException -> 0x0290 }
            java.lang.String r2 = r2.f9757     // Catch:{ MalformedURLException -> 0x0290 }
        L_0x0243:
            com.google.android.gms.internal.zzchm r7 = r14.m11016()     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzcho r7 = r7.m10848()     // Catch:{ MalformedURLException -> 0x0290 }
            java.lang.String r8 = "Uploading data. app, uncompressed size, data"
            int r10 = r6.length     // Catch:{ MalformedURLException -> 0x0290 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ MalformedURLException -> 0x0290 }
            r7.m10852(r8, r2, r10, r3)     // Catch:{ MalformedURLException -> 0x0290 }
            r2 = 1
            r14.f9401 = r2     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzchq r3 = r14.m11026()     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzcip r8 = new com.google.android.gms.internal.zzcip     // Catch:{ MalformedURLException -> 0x0290 }
            r8.<init>(r14)     // Catch:{ MalformedURLException -> 0x0290 }
            r3.m11109()     // Catch:{ MalformedURLException -> 0x0290 }
            r3.m11115()     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.common.internal.zzbq.m9120(r5)     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.common.internal.zzbq.m9120(r6)     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.common.internal.zzbq.m9120(r8)     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzcih r10 = r3.m11101()     // Catch:{ MalformedURLException -> 0x0290 }
            com.google.android.gms.internal.zzchu r2 = new com.google.android.gms.internal.zzchu     // Catch:{ MalformedURLException -> 0x0290 }
            r7 = 0
            r2.<init>(r3, r4, r5, r6, r7, r8)     // Catch:{ MalformedURLException -> 0x0290 }
            r10.m10981((java.lang.Runnable) r2)     // Catch:{ MalformedURLException -> 0x0290 }
        L_0x027e:
            r2 = 0
            r14.f9390 = r2
            r14.m10997()
            goto L_0x002b
        L_0x0286:
            r2 = 0
            goto L_0x0216
        L_0x0288:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ MalformedURLException -> 0x0290 }
            r2.<init>(r8)     // Catch:{ MalformedURLException -> 0x0290 }
            r14.f9404 = r2     // Catch:{ MalformedURLException -> 0x0290 }
            goto L_0x022b
        L_0x0290:
            r2 = move-exception
            com.google.android.gms.internal.zzchm r2 = r14.m11016()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x02a4 }
            java.lang.String r3 = "Failed to parse upload URL. Not uploading. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r4)     // Catch:{ all -> 0x02a4 }
            r2.m10851(r3, r4, r9)     // Catch:{ all -> 0x02a4 }
            goto L_0x027e
        L_0x02a4:
            r2 = move-exception
            r3 = 0
            r14.f9390 = r3
            r14.m10997()
            throw r2
        L_0x02ac:
            r2 = -1
            r14.f9382 = r2     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcgo r2 = r14.m11024()     // Catch:{ all -> 0x02a4 }
            long r4 = com.google.android.gms.internal.zzcgn.m10523()     // Catch:{ all -> 0x02a4 }
            long r4 = r10 - r4
            java.lang.String r2 = r2.m10602((long) r4)     // Catch:{ all -> 0x02a4 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x02a4 }
            if (r3 != 0) goto L_0x027e
            com.google.android.gms.internal.zzcgo r3 = r14.m11024()     // Catch:{ all -> 0x02a4 }
            com.google.android.gms.internal.zzcgh r2 = r3.m10592(r2)     // Catch:{ all -> 0x02a4 }
            if (r2 == 0) goto L_0x027e
            r14.m11007((com.google.android.gms.internal.zzcgh) r2)     // Catch:{ all -> 0x02a4 }
            goto L_0x027e
        L_0x02d2:
            r3 = r2
            goto L_0x01f8
        L_0x02d5:
            r6 = r3
            goto L_0x0165
        L_0x02d8:
            r6 = r5
            goto L_0x013b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcim.m11065():void");
    }
}
