package com.google.android.gms.internal;

final class zzk implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzw f10786;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Runnable f10787;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzr f10788;

    public zzk(zzi zzi, zzr zzr, zzw zzw, Runnable runnable) {
        this.f10788 = zzr;
        this.f10786 = zzw;
        this.f10787 = runnable;
    }

    public final void run() {
        if (this.f10786.f10933 == null) {
            this.f10788.m13372(this.f10786.f10934);
        } else {
            this.f10788.m13369(this.f10786.f10933);
        }
        if (this.f10786.f10932) {
            this.f10788.m13361("intermediate-response");
        } else {
            this.f10788.m13364("done");
        }
        if (this.f10787 != null) {
            this.f10787.run();
        }
    }
}
