package com.google.android.gms.internal;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zzfjk {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ByteBuffer f10532;

    private zzfjk(ByteBuffer byteBuffer) {
        this.f10532 = byteBuffer;
        this.f10532.order(ByteOrder.LITTLE_ENDIAN);
    }

    private zzfjk(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m12804(int i) throws IOException {
        byte b = (byte) i;
        if (!this.f10532.hasRemaining()) {
            throw new zzfjl(this.f10532.position(), this.f10532.limit());
        }
        this.f10532.put(b);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12805(int i) {
        return m12813(i << 3);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12806(int i, int i2) {
        return m12805(i) + m12816(i2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12807(int i, zzfjs zzfjs) {
        int r0 = m12805(i);
        int r1 = zzfjs.m12872();
        return r0 + r1 + m12813(r1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12808(int i, String str) {
        return m12805(i) + m12820(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12809(int i, byte[] bArr) {
        return m12805(i) + m12810(bArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12810(byte[] bArr) {
        return m12813(bArr.length) + bArr.length;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m12811(long j) throws IOException {
        while ((-128 & j) != 0) {
            m12804((((int) j) & 127) | 128);
            j >>>= 7;
        }
        m12804((int) j);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m12812(CharSequence charSequence, ByteBuffer byteBuffer) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            char charAt = charSequence.charAt(i);
            if (charAt < 128) {
                byteBuffer.put((byte) charAt);
            } else if (charAt < 2048) {
                byteBuffer.put((byte) ((charAt >>> 6) | 960));
                byteBuffer.put((byte) ((charAt & '?') | 128));
            } else if (charAt < 55296 || 57343 < charAt) {
                byteBuffer.put((byte) ((charAt >>> 12) | 480));
                byteBuffer.put((byte) (((charAt >>> 6) & 63) | 128));
                byteBuffer.put((byte) ((charAt & '?') | 128));
            } else {
                if (i + 1 != charSequence.length()) {
                    i++;
                    char charAt2 = charSequence.charAt(i);
                    if (Character.isSurrogatePair(charAt, charAt2)) {
                        int codePoint = Character.toCodePoint(charAt, charAt2);
                        byteBuffer.put((byte) ((codePoint >>> 18) | 240));
                        byteBuffer.put((byte) (((codePoint >>> 12) & 63) | 128));
                        byteBuffer.put((byte) (((codePoint >>> 6) & 63) | 128));
                        byteBuffer.put((byte) ((codePoint & 63) | 128));
                    }
                }
                throw new IllegalArgumentException(new StringBuilder(39).append("Unpaired surrogate at index ").append(i - 1).toString());
            }
            i++;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m12813(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m12814(int i, long j) {
        return m12805(i) + m12817(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m12815(long j) throws IOException {
        if (this.f10532.remaining() < 8) {
            throw new zzfjl(this.f10532.position(), this.f10532.limit());
        }
        this.f10532.putLong(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12816(int i) {
        if (i >= 0) {
            return m12813(i);
        }
        return 10;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12817(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m12818(CharSequence charSequence) {
        int i;
        int i2 = 0;
        int length = charSequence.length();
        int i3 = 0;
        while (i3 < length && charSequence.charAt(i3) < 128) {
            i3++;
        }
        int i4 = length;
        while (true) {
            if (i3 >= length) {
                i = i4;
                break;
            }
            char charAt = charSequence.charAt(i3);
            if (charAt < 2048) {
                i4 += (127 - charAt) >>> 31;
                i3++;
            } else {
                int length2 = charSequence.length();
                while (i3 < length2) {
                    char charAt2 = charSequence.charAt(i3);
                    if (charAt2 < 2048) {
                        i2 += (127 - charAt2) >>> 31;
                    } else {
                        i2 += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i3) < 65536) {
                                throw new IllegalArgumentException(new StringBuilder(39).append("Unpaired surrogate at index ").append(i3).toString());
                            }
                            i3++;
                        }
                    }
                    i3++;
                }
                i = i4 + i2;
            }
        }
        if (i >= length) {
            return i;
        }
        throw new IllegalArgumentException(new StringBuilder(54).append("UTF-8 length does not fit in int: ").append(((long) i) + 4294967296L).toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m12819(CharSequence charSequence, byte[] bArr, int i, int i2) {
        int i3;
        int length = charSequence.length();
        int i4 = 0;
        int i5 = i + i2;
        while (i4 < length && i4 + i < i5) {
            char charAt = charSequence.charAt(i4);
            if (charAt >= 128) {
                break;
            }
            bArr[i + i4] = (byte) charAt;
            i4++;
        }
        if (i4 == length) {
            return i + length;
        }
        int i6 = i + i4;
        while (i4 < length) {
            char charAt2 = charSequence.charAt(i4);
            if (charAt2 < 128 && i6 < i5) {
                i3 = i6 + 1;
                bArr[i6] = (byte) charAt2;
            } else if (charAt2 < 2048 && i6 <= i5 - 2) {
                int i7 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 6) | 960);
                i3 = i7 + 1;
                bArr[i7] = (byte) ((charAt2 & '?') | 128);
            } else if ((charAt2 < 55296 || 57343 < charAt2) && i6 <= i5 - 3) {
                int i8 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 12) | 480);
                int i9 = i8 + 1;
                bArr[i8] = (byte) (((charAt2 >>> 6) & 63) | 128);
                i3 = i9 + 1;
                bArr[i9] = (byte) ((charAt2 & '?') | 128);
            } else if (i6 <= i5 - 4) {
                if (i4 + 1 != charSequence.length()) {
                    i4++;
                    char charAt3 = charSequence.charAt(i4);
                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                        int i10 = i6 + 1;
                        bArr[i6] = (byte) ((codePoint >>> 18) | 240);
                        int i11 = i10 + 1;
                        bArr[i10] = (byte) (((codePoint >>> 12) & 63) | 128);
                        int i12 = i11 + 1;
                        bArr[i11] = (byte) (((codePoint >>> 6) & 63) | 128);
                        i3 = i12 + 1;
                        bArr[i12] = (byte) ((codePoint & 63) | 128);
                    }
                }
                throw new IllegalArgumentException(new StringBuilder(39).append("Unpaired surrogate at index ").append(i4 - 1).toString());
            } else {
                throw new ArrayIndexOutOfBoundsException(new StringBuilder(37).append("Failed writing ").append(charAt2).append(" at index ").append(i6).toString());
            }
            i4++;
            i6 = i3;
        }
        return i6;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12820(String str) {
        int r0 = m12818((CharSequence) str);
        return r0 + m12813(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfjk m12821(byte[] bArr) {
        return m12822(bArr, 0, bArr.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfjk m12822(byte[] bArr, int i, int i2) {
        return new zzfjk(bArr, 0, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12823(CharSequence charSequence, ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly()) {
            throw new ReadOnlyBufferException();
        } else if (byteBuffer.hasArray()) {
            try {
                byteBuffer.position(m12819(charSequence, byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining()) - byteBuffer.arrayOffset());
            } catch (ArrayIndexOutOfBoundsException e) {
                BufferOverflowException bufferOverflowException = new BufferOverflowException();
                bufferOverflowException.initCause(e);
                throw bufferOverflowException;
            }
        } else {
            m12812(charSequence, byteBuffer);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12824(int i, long j) throws IOException {
        m12826(i, 0);
        m12811(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12825(int i) throws IOException {
        while ((i & -128) != 0) {
            m12804((i & 127) | 128);
            i >>>= 7;
        }
        m12804(i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12826(int i, int i2) throws IOException {
        m12825((i << 3) | i2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12827(int i, long j) throws IOException {
        m12826(i, 1);
        m12815(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12828(byte[] bArr) throws IOException {
        int length = bArr.length;
        if (this.f10532.remaining() >= length) {
            this.f10532.put(bArr, 0, length);
            return;
        }
        throw new zzfjl(this.f10532.position(), this.f10532.limit());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12829() {
        if (this.f10532.remaining() != 0) {
            throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", new Object[]{Integer.valueOf(this.f10532.remaining())}));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12830(int i, double d) throws IOException {
        m12826(i, 1);
        m12815(Double.doubleToLongBits(d));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12831(int i, float f) throws IOException {
        m12826(i, 5);
        int floatToIntBits = Float.floatToIntBits(f);
        if (this.f10532.remaining() < 4) {
            throw new zzfjl(this.f10532.position(), this.f10532.limit());
        }
        this.f10532.putInt(floatToIntBits);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12832(int i, int i2) throws IOException {
        m12826(i, 0);
        if (i2 >= 0) {
            m12825(i2);
        } else {
            m12811((long) i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12833(int i, long j) throws IOException {
        m12826(i, 0);
        m12811(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12834(int i, zzfjs zzfjs) throws IOException {
        m12826(i, 2);
        m12838(zzfjs);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12835(int i, String str) throws IOException {
        m12826(i, 2);
        try {
            int r0 = m12813(str.length());
            if (r0 == m12813(str.length() * 3)) {
                int position = this.f10532.position();
                if (this.f10532.remaining() < r0) {
                    throw new zzfjl(r0 + position, this.f10532.limit());
                }
                this.f10532.position(position + r0);
                m12823((CharSequence) str, this.f10532);
                int position2 = this.f10532.position();
                this.f10532.position(position);
                m12825((position2 - position) - r0);
                this.f10532.position(position2);
                return;
            }
            m12825(m12818((CharSequence) str));
            m12823((CharSequence) str, this.f10532);
        } catch (BufferOverflowException e) {
            zzfjl zzfjl = new zzfjl(this.f10532.position(), this.f10532.limit());
            zzfjl.initCause(e);
            throw zzfjl;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12836(int i, boolean z) throws IOException {
        int i2 = 0;
        m12826(i, 0);
        if (z) {
            i2 = 1;
        }
        byte b = (byte) i2;
        if (!this.f10532.hasRemaining()) {
            throw new zzfjl(this.f10532.position(), this.f10532.limit());
        }
        this.f10532.put(b);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12837(int i, byte[] bArr) throws IOException {
        m12826(i, 2);
        m12825(bArr.length);
        m12828(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12838(zzfjs zzfjs) throws IOException {
        m12825(zzfjs.m12873());
        zzfjs.m12877(this);
    }
}
