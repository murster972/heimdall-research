package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzbz;
import com.google.android.gms.common.util.zzd;
import com.google.firebase.iid.FirebaseInstanceId;
import java.math.BigInteger;
import java.util.Locale;

public final class zzchh extends zzcjl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f9248;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f9249;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f9250;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f9251;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f9252;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f9253;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f9254;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f9255;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f9256;

    zzchh(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final String m10706() {
        m11109();
        try {
            return FirebaseInstanceId.m13786().m13796();
        } catch (IllegalStateException e) {
            m11096().m10834().m10849("Failed to retrieve Firebase Instance Id");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void t_() {
        boolean z;
        int i = 1;
        String str = EnvironmentCompat.MEDIA_UNKNOWN;
        String str2 = "Unknown";
        int i2 = Integer.MIN_VALUE;
        String str3 = "Unknown";
        String packageName = m11097().getPackageName();
        PackageManager packageManager = m11097().getPackageManager();
        if (packageManager == null) {
            m11096().m10832().m10850("PackageManager is null, app identity information might be inaccurate. appId", zzchm.m10812(packageName));
        } else {
            try {
                str = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException e) {
                m11096().m10832().m10850("Error retrieving app installer package name. appId", zzchm.m10812(packageName));
            }
            if (str == null) {
                str = "manual_install";
            } else if ("com.android.vending".equals(str)) {
                str = "";
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(m11097().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    if (!TextUtils.isEmpty(applicationLabel)) {
                        str3 = applicationLabel.toString();
                    }
                    str2 = packageInfo.versionName;
                    i2 = packageInfo.versionCode;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                m11096().m10832().m10851("Error retrieving package info. appId, appName", zzchm.m10812(packageName), str3);
            }
        }
        this.f9256 = packageName;
        this.f9254 = str;
        this.f9253 = str2;
        this.f9255 = i2;
        this.f9252 = str3;
        this.f9248 = 0;
        Status r1 = zzbz.m4202(m11097());
        boolean z2 = r1 != null && r1.m8549();
        if (!z2) {
            if (r1 == null) {
                m11096().m10832().m10849("GoogleService failed to initialize (no status)");
            } else {
                m11096().m10832().m10851("GoogleService failed to initialize, status", Integer.valueOf(r1.m8547()), r1.m8548());
            }
        }
        if (z2) {
            Boolean r0 = m11102().m10545("firebase_analytics_collection_enabled");
            if (m11102().m10539()) {
                m11096().m10836().m10849("Collection disabled with firebase_analytics_collection_deactivated=1");
                z = false;
            } else if (r0 != null && !r0.booleanValue()) {
                m11096().m10836().m10849("Collection disabled with firebase_analytics_collection_enabled=0");
                z = false;
            } else if (r0 != null || !zzbz.m4201()) {
                m11096().m10848().m10849("Collection enabled");
                z = true;
            } else {
                m11096().m10836().m10849("Collection disabled with google_app_measurement_enable=0");
                z = false;
            }
        } else {
            z = false;
        }
        this.f9251 = "";
        this.f9249 = 0;
        try {
            String r12 = zzbz.m4204();
            if (TextUtils.isEmpty(r12)) {
                r12 = "";
            }
            this.f9251 = r12;
            if (z) {
                m11096().m10848().m10851("App package, google app id", this.f9256, this.f9251);
            }
        } catch (IllegalStateException e3) {
            m11096().m10832().m10851("getGoogleAppId or isMeasurementEnabled failed with exception. appId", zzchm.m10812(packageName), e3);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            if (!zzbhd.m10221(m11097())) {
                i = 0;
            }
            this.f9250 = i;
            return;
        }
        this.f9250 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10707() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10708() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10709() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10710() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10711() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10712() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10713() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10714() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10715() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10716() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10717() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10718() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10719() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10720() {
        return true;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10721() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10722() {
        return super.m11105();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public final String m10723() {
        byte[] bArr = new byte[16];
        m11112().m11419().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new Object[]{new BigInteger(1, bArr)});
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String m10724() {
        m11115();
        return this.f9256;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public final String m10725() {
        m11115();
        return this.f9251;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵢ  reason: contains not printable characters */
    public final int m10726() {
        m11115();
        return this.f9255;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public final int m10727() {
        m11115();
        return this.f9250;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10728() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10729() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10730() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10731() {
        super.m11109();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcgi m10732(String str) {
        m11109();
        String r4 = m10724();
        String r5 = m10725();
        m11115();
        String str2 = this.f9253;
        long r7 = (long) m10726();
        m11115();
        String str3 = this.f9254;
        m11115();
        m11109();
        if (this.f9248 == 0) {
            this.f9248 = this.f9487.m11063().m11424(m11097(), m11097().getPackageName());
        }
        long j = this.f9248;
        boolean r15 = this.f9487.m11038();
        boolean z = !m11098().f9321;
        String r17 = m10706();
        m11115();
        long r20 = this.f9487.m11039();
        int r22 = m10727();
        Boolean r2 = m11102().m10545("google_analytics_adid_collection_enabled");
        return new zzcgi(r4, r5, str2, r7, str3, 11910, j, str, r15, z, r17, 0, r20, r22, Boolean.valueOf(r2 == null || r2.booleanValue()).booleanValue());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10733() {
        super.m11110();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10734() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10735() {
        return super.m11112();
    }
}
