package com.google.android.gms.internal;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

final class zzbw {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f8810 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f8811 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Object f8812 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static MessageDigest f8813 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    static CountDownLatch f8814 = new CountDownLatch(1);

    /* renamed from: 靐  reason: contains not printable characters */
    private static MessageDigest m10299() {
        m10304();
        boolean z = false;
        try {
            z = f8814.await(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }
        if (z && f8813 != null) {
            return f8813;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzaz m10300(long j) {
        zzaz zzaz = new zzaz();
        zzaz.f8455 = Long.valueOf(PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM);
        return zzaz;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m10301(zzaz zzaz, String str) throws GeneralSecurityException, UnsupportedEncodingException {
        byte[] r0;
        byte[] r1 = zzfjs.m12871((zzfjs) zzaz);
        if (((Boolean) zzkb.m5481().m5595(zznh.f4972)).booleanValue()) {
            r0 = m10306(r1, str);
        } else if (zzbz.f8815 == null) {
            throw new GeneralSecurityException();
        } else {
            byte[] r02 = zzbz.f8815.m11652(r1, str != null ? str.getBytes() : new byte[0]);
            zzbf zzbf = new zzbf();
            zzbf.f8736 = new byte[][]{r02};
            zzbf.f8735 = 2;
            r0 = zzfjs.m12871((zzfjs) zzbf);
        }
        return zzbu.m10295(r0, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Vector<byte[]> m10303(byte[] bArr, int i) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        int length = ((bArr.length + 255) - 1) / 255;
        Vector<byte[]> vector = new Vector<>();
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 * 255;
            try {
                vector.add(Arrays.copyOfRange(bArr, i3, bArr.length - i3 > 255 ? i3 + 255 : bArr.length));
                i2++;
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }
        return vector;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m10304() {
        synchronized (f8810) {
            if (!f8811) {
                f8811 = true;
                new Thread(new zzby()).start();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m10305(byte[] bArr) throws NoSuchAlgorithmException {
        byte[] digest;
        synchronized (f8812) {
            MessageDigest r0 = m10299();
            if (r0 == null) {
                throw new NoSuchAlgorithmException("Cannot compute hash");
            }
            r0.reset();
            r0.update(bArr);
            digest = f8813.digest();
        }
        return digest;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m10306(byte[] bArr, String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Vector<byte[]> r0 = m10303(bArr, 255);
        if (r0 == null || r0.size() == 0) {
            return m10307(zzfjs.m12871((zzfjs) m10300((long) PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM)), str, true);
        }
        zzbf zzbf = new zzbf();
        zzbf.f8736 = new byte[r0.size()][];
        Iterator<byte[]> it2 = r0.iterator();
        int i = 0;
        while (it2.hasNext()) {
            zzbf.f8736[i] = m10307(it2.next(), str, false);
            i++;
        }
        zzbf.f8733 = m10305(bArr);
        return zzfjs.m12871((zzfjs) zzbf);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m10307(byte[] bArr, String str, boolean z) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] array;
        int i = z ? 239 : 255;
        if (bArr.length > i) {
            bArr = zzfjs.m12871((zzfjs) m10300((long) PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM));
        }
        if (bArr.length < i) {
            byte[] bArr2 = new byte[(i - bArr.length)];
            new SecureRandom().nextBytes(bArr2);
            array = ByteBuffer.allocate(i + 1).put((byte) bArr.length).put(bArr).put(bArr2).array();
        } else {
            array = ByteBuffer.allocate(i + 1).put((byte) bArr.length).put(bArr).array();
        }
        if (z) {
            array = ByteBuffer.allocate(256).put(m10305(array)).put(array).array();
        }
        byte[] bArr3 = new byte[256];
        new zzca().m10310(array, bArr3);
        if (str != null && str.length() > 0) {
            if (str.length() > 32) {
                str = str.substring(0, 32);
            }
            new zzfdb(str.getBytes("UTF-8")).m12360(bArr3);
        }
        return bArr3;
    }
}
