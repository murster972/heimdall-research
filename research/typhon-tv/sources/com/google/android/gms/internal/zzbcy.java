package com.google.android.gms.internal;

import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzbq;
import java.util.Locale;

public final class zzbcy {

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f8683 = false;

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f8684;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f8685;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f8686;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f8687;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f8688;

    public zzbcy(String str) {
        this(str, false);
    }

    private zzbcy(String str, boolean z) {
        zzbq.m9123(str, (Object) "The log tag cannot be null or empty.");
        this.f8686 = str;
        this.f8688 = str.length() <= 23;
        this.f8687 = false;
        this.f8685 = false;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private String m10083(String str, Object... objArr) {
        if (objArr.length != 0) {
            str = String.format(Locale.ROOT, str, objArr);
        }
        if (TextUtils.isEmpty(this.f8684)) {
            return str;
        }
        String valueOf = String.valueOf(this.f8684);
        String valueOf2 = String.valueOf(str);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m10084() {
        return this.f8687 || (this.f8688 && Log.isLoggable(this.f8686, 3));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10085(String str, Object... objArr) {
        Log.i(this.f8686, m10083(str, objArr));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10086(Throwable th, String str, Object... objArr) {
        Log.e(this.f8686, m10083(str, objArr), th);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10087(String str, Object... objArr) {
        Log.e(this.f8686, m10083(str, objArr));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10088(String str, Object... objArr) {
        Log.w(this.f8686, m10083(str, objArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10089(String str) {
        String format;
        if (TextUtils.isEmpty(str)) {
            format = null;
        } else {
            format = String.format("[%s] ", new Object[]{str});
        }
        this.f8684 = format;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10090(String str, Object... objArr) {
        if (m10084()) {
            Log.d(this.f8686, m10083(str, objArr));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10091(Throwable th, String str, Object... objArr) {
        if (m10084()) {
            Log.d(this.f8686, m10083(str, objArr), th);
        }
    }
}
