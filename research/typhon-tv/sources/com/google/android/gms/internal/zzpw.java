package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzpw extends zzeu implements zzpu {
    zzpw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m13236(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r0 = m12300(2, v_);
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13237() throws RemoteException {
        m12298(4, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13238(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(3, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13239(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeInt(i);
        m12298(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13240(String str, IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(1, v_);
    }
}
