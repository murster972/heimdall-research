package com.google.android.gms.internal;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.doubleclick.CustomRenderedAd;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zznw implements CustomRenderedAd {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zznx f5197;

    public zznw(zznx zznx) {
        this.f5197 = zznx;
    }

    public final String getBaseUrl() {
        try {
            return this.f5197.m13170();
        } catch (RemoteException e) {
            zzakb.m4796("Could not delegate getBaseURL to CustomRenderedAd", e);
            return null;
        }
    }

    public final String getContent() {
        try {
            return this.f5197.m13167();
        } catch (RemoteException e) {
            zzakb.m4796("Could not delegate getContent to CustomRenderedAd", e);
            return null;
        }
    }

    public final void onAdRendered(View view) {
        try {
            this.f5197.m13171(view != null ? zzn.m9306(view) : null);
        } catch (RemoteException e) {
            zzakb.m4796("Could not delegate onAdRendered to CustomRenderedAd", e);
        }
    }

    public final void recordClick() {
        try {
            this.f5197.m13169();
        } catch (RemoteException e) {
            zzakb.m4796("Could not delegate recordClick to CustomRenderedAd", e);
        }
    }

    public final void recordImpression() {
        try {
            this.f5197.m13168();
        } catch (RemoteException e) {
            zzakb.m4796("Could not delegate recordImpression to CustomRenderedAd", e);
        }
    }
}
