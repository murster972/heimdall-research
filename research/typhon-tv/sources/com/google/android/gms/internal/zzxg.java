package com.google.android.gms.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzxg extends zzeu implements zzxe {
    zzxg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
    }

    public final void onActivityResult(int i, int i2, Intent intent) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        v_.writeInt(i2);
        zzew.m12306(v_, (Parcelable) intent);
        m12298(12, v_);
    }

    public final void onBackPressed() throws RemoteException {
        m12298(10, v_());
    }

    public final void onCreate(Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(1, v_);
    }

    public final void onDestroy() throws RemoteException {
        m12298(8, v_());
    }

    public final void onPause() throws RemoteException {
        m12298(5, v_());
    }

    public final void onRestart() throws RemoteException {
        m12298(2, v_());
    }

    public final void onResume() throws RemoteException {
        m12298(4, v_());
    }

    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) bundle);
        Parcel r0 = m12300(6, v_);
        if (r0.readInt() != 0) {
            bundle.readFromParcel(r0);
        }
        r0.recycle();
    }

    public final void onStart() throws RemoteException {
        m12298(3, v_());
    }

    public final void onStop() throws RemoteException {
        m12298(7, v_());
    }

    public final void zzbf() throws RemoteException {
        m12298(9, v_());
    }

    public final void zzk(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(13, v_);
    }

    public final boolean zzmu() throws RemoteException {
        Parcel r0 = m12300(11, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }
}
