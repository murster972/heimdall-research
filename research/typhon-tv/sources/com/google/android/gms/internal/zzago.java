package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzago extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8167;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ long f8168;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8169;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzago(Context context, String str, long j) {
        super((zzagi) null);
        this.f8169 = context;
        this.f8167 = str;
        this.f8168 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9587() {
        SharedPreferences.Editor edit = this.f8169.getSharedPreferences("admob", 0).edit();
        edit.putString("app_settings_json", this.f8167);
        edit.putLong("app_settings_last_update_ms", this.f8168);
        edit.apply();
    }
}
