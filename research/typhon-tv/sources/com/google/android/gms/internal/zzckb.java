package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

@TargetApi(14)
final class zzckb implements Application.ActivityLifecycleCallbacks {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcjn f9538;

    private zzckb(zzcjn zzcjn) {
        this.f9538 = zzcjn;
    }

    /* synthetic */ zzckb(zzcjn zzcjn, zzcjo zzcjo) {
        this(zzcjn);
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        Bundle bundle2;
        Uri data;
        try {
            this.f9538.m11096().m10848().m10849("onActivityCreated");
            Intent intent = activity.getIntent();
            if (!(intent == null || (data = intent.getData()) == null || !data.isHierarchical())) {
                if (bundle == null) {
                    Bundle r2 = this.f9538.m11112().m11432(data);
                    this.f9538.m11112();
                    String str = zzclq.m11382(intent) ? "gs" : "auto";
                    if (r2 != null) {
                        this.f9538.m11174(str, "_cmp", r2);
                    }
                }
                String queryParameter = data.getQueryParameter("referrer");
                if (!TextUtils.isEmpty(queryParameter)) {
                    if (!(queryParameter.contains("gclid") && (queryParameter.contains("utm_campaign") || queryParameter.contains("utm_source") || queryParameter.contains("utm_medium") || queryParameter.contains("utm_term") || queryParameter.contains("utm_content")))) {
                        this.f9538.m11096().m10845().m10849("Activity created with data 'referrer' param without gclid and at least one utm field");
                        return;
                    }
                    this.f9538.m11096().m10845().m10850("Activity created with referrer", queryParameter);
                    if (!TextUtils.isEmpty(queryParameter)) {
                        this.f9538.m11177("auto", "_ldl", (Object) queryParameter);
                    }
                } else {
                    return;
                }
            }
        } catch (Throwable th) {
            this.f9538.m11096().m10832().m10850("Throwable caught in onActivityCreated", th);
        }
        zzckc r0 = this.f9538.m11104();
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.firebase.analytics.screen_service")) != null) {
            zzckf r02 = r0.m11214(activity);
            r02.f11089 = bundle2.getLong("id");
            r02.f11088 = bundle2.getString("name");
            r02.f11090 = bundle2.getString("referrer_name");
        }
    }

    public final void onActivityDestroyed(Activity activity) {
        this.f9538.m11104().m11211(activity);
    }

    public final void onActivityPaused(Activity activity) {
        this.f9538.m11104().m11213(activity);
        zzclf r0 = this.f9538.m11100();
        r0.m11101().m10986((Runnable) new zzclj(r0, r0.m11105().m9241()));
    }

    public final void onActivityResumed(Activity activity) {
        this.f9538.m11104().m11208(activity);
        zzclf r0 = this.f9538.m11100();
        r0.m11101().m10986((Runnable) new zzcli(r0, r0.m11105().m9241()));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.f9538.m11104().m11216(activity, bundle);
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }
}
