package com.google.android.gms.internal;

import java.util.Map;

final class zzfid implements Comparable<zzfid>, Map.Entry<K, V> {

    /* renamed from: 靐  reason: contains not printable characters */
    private V f10477;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzfhy f10478;

    /* renamed from: 龘  reason: contains not printable characters */
    private final K f10479;

    zzfid(zzfhy zzfhy, K k, V v) {
        this.f10478 = zzfhy;
        this.f10479 = k;
        this.f10477 = v;
    }

    zzfid(zzfhy zzfhy, Map.Entry<K, V> entry) {
        this(zzfhy, (Comparable) entry.getKey(), entry.getValue());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m12697(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return ((Comparable) getKey()).compareTo((Comparable) ((zzfid) obj).getKey());
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return m12697(this.f10479, entry.getKey()) && m12697(this.f10477, entry.getValue());
    }

    public final /* synthetic */ Object getKey() {
        return this.f10479;
    }

    public final V getValue() {
        return this.f10477;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = this.f10479 == null ? 0 : this.f10479.hashCode();
        if (this.f10477 != null) {
            i = this.f10477.hashCode();
        }
        return hashCode ^ i;
    }

    public final V setValue(V v) {
        this.f10478.m12680();
        V v2 = this.f10477;
        this.f10477 = v;
        return v2;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f10479);
        String valueOf2 = String.valueOf(this.f10477);
        return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append("=").append(valueOf2).toString();
    }
}
