package com.google.android.gms.internal;

import android.content.SharedPreferences;
import org.json.JSONObject;

final class zzna extends zzmx<Long> {
    zzna(int i, String str, Long l) {
        super(i, str, l, (zzmy) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13154(SharedPreferences sharedPreferences) {
        return Long.valueOf(sharedPreferences.getLong(m5586(), ((Long) m5582()).longValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13155(JSONObject jSONObject) {
        return Long.valueOf(jSONObject.optLong(m5586(), ((Long) m5582()).longValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m13156(SharedPreferences.Editor editor, Object obj) {
        editor.putLong(m5586(), ((Long) obj).longValue());
    }
}
