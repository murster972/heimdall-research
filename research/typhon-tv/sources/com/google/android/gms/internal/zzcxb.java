package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzr;

final class zzcxb extends Api.zza<zzcxn, zzcxe> {
    zzcxb() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Api.zze m11546(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        zzcxe zzcxe = (zzcxe) obj;
        return new zzcxn(context, looper, true, zzr, zzcxe == null ? zzcxe.f9824 : zzcxe, connectionCallbacks, onConnectionFailedListener);
    }
}
