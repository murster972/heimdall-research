package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.widget.EditText;

final class zzaoo implements DialogInterface.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ EditText f8380;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ JsPromptResult f8381;

    zzaoo(JsPromptResult jsPromptResult, EditText editText) {
        this.f8381 = jsPromptResult;
        this.f8380 = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f8381.confirm(this.f8380.getText().toString());
    }
}
