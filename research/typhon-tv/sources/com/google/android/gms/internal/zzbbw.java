package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.util.zzd;

public abstract class zzbbw extends zzbce {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Runnable f8612;

    /* renamed from: 连任  reason: contains not printable characters */
    private long f8613;

    /* renamed from: 靐  reason: contains not printable characters */
    protected boolean f8614;

    /* renamed from: 麤  reason: contains not printable characters */
    private Handler f8615 = new Handler(Looper.getMainLooper());

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzd f8616;

    public zzbbw(String str, zzd zzd, String str2, String str3, long j) {
        super(str, str2, str3);
        this.f8616 = zzd;
        this.f8612 = new zzbby(this);
        this.f8613 = 1000;
        m9960(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9959() {
        m9960(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9960(boolean z) {
        if (this.f8614 != z) {
            this.f8614 = z;
            if (z) {
                this.f8615.postDelayed(this.f8612, this.f8613);
            } else {
                this.f8615.removeCallbacks(this.f8612);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m9961(long j);
}
