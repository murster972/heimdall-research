package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$zzb;
import java.util.Map;

public final class zzcgd extends zzcjk {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, Integer> f9092 = new ArrayMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private long f9093;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, Long> f9094 = new ArrayMap();

    public zzcgd(zzcim zzcim) {
        super(zzcim);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10427(long j) {
        for (String put : this.f9094.keySet()) {
            this.f9094.put(put, Long.valueOf(j));
        }
        if (!this.f9094.isEmpty()) {
            this.f9093 = j;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10429(String str, long j) {
        m11109();
        zzbq.m9122(str);
        Integer num = this.f9092.get(str);
        if (num != null) {
            zzckf r1 = m11104().m11204();
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.f9092.remove(str);
                Long l = this.f9094.get(str);
                if (l == null) {
                    m11096().m10832().m10849("First ad unit exposure time was never set");
                } else {
                    this.f9094.remove(str);
                    m10434(str, j - l.longValue(), (AppMeasurement$zzb) r1);
                }
                if (!this.f9092.isEmpty()) {
                    return;
                }
                if (this.f9093 == 0) {
                    m11096().m10832().m10849("First ad exposure time was never set");
                    return;
                }
                m10430(j - this.f9093, (AppMeasurement$zzb) r1);
                this.f9093 = 0;
                return;
            }
            this.f9092.put(str, Integer.valueOf(intValue));
            return;
        }
        m11096().m10832().m10850("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10430(long j, AppMeasurement$zzb appMeasurement$zzb) {
        if (appMeasurement$zzb == null) {
            m11096().m10848().m10849("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            m11096().m10848().m10850("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            zzckc.m11187(appMeasurement$zzb, bundle);
            m11091().m11174("am", "_xa", bundle);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10433(String str, long j) {
        m11109();
        zzbq.m9122(str);
        if (this.f9092.isEmpty()) {
            this.f9093 = j;
        }
        Integer num = this.f9092.get(str);
        if (num != null) {
            this.f9092.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (this.f9092.size() >= 100) {
            m11096().m10834().m10849("Too many ads visible");
        } else {
            this.f9092.put(str, 1);
            this.f9094.put(str, Long.valueOf(j));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10434(String str, long j, AppMeasurement$zzb appMeasurement$zzb) {
        if (appMeasurement$zzb == null) {
            m11096().m10848().m10849("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            m11096().m10848().m10850("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            zzckc.m11187(appMeasurement$zzb, bundle);
            m11091().m11174("am", "_xu", bundle);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10435() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10436() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10437() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10438() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10439() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10440() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10441() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10442() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10443() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10444() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10445() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10446() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10447() {
        return super.m11103();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10448() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10449() {
        return super.m11105();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10450() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10451() {
        super.m11107();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10452(String str) {
        if (str == null || str.length() == 0) {
            m11096().m10832().m10849("Ad unit id must be a non-empty string");
            return;
        }
        m11101().m10986((Runnable) new zzcgf(this, str, m11105().m9241()));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10453() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10454() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10455() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10456(long j) {
        zzckf r2 = m11104().m11204();
        for (String next : this.f9094.keySet()) {
            m10434(next, j - this.f9094.get(next).longValue(), (AppMeasurement$zzb) r2);
        }
        if (!this.f9094.isEmpty()) {
            m10430(j - this.f9093, (AppMeasurement$zzb) r2);
        }
        m10427(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10457(String str) {
        if (str == null || str.length() == 0) {
            m11096().m10832().m10849("Ad unit id must be a non-empty string");
            return;
        }
        m11101().m10986((Runnable) new zzcge(this, str, m11105().m9241()));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10458() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10459() {
        return super.m11112();
    }
}
