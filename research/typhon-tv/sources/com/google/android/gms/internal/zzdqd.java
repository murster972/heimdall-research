package com.google.android.gms.internal;

import java.util.Arrays;

public final class zzdqd<P> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final byte[] f9933;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzdtt f9934;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzdtb f9935;

    /* renamed from: 龘  reason: contains not printable characters */
    private final P f9936;

    public zzdqd(P p, byte[] bArr, zzdtb zzdtb, zzdtt zzdtt) {
        this.f9936 = p;
        this.f9933 = Arrays.copyOf(bArr, bArr.length);
        this.f9935 = zzdtb;
        this.f9934 = zzdtt;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final byte[] m11669() {
        if (this.f9933 == null) {
            return null;
        }
        return Arrays.copyOf(this.f9933, this.f9933.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final P m11670() {
        return this.f9936;
    }
}
