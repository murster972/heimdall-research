package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzexe {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f10301;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<byte[]> f10302;

    /* renamed from: 龘  reason: contains not printable characters */
    private Map<String, Map<String, byte[]>> f10303;

    public zzexe(Map<String, Map<String, byte[]>> map, long j, List<byte[]> list) {
        this.f10303 = map;
        this.f10301 = j;
        this.f10302 = list;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<byte[]> m12312() {
        return this.f10302;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final byte[] m12313(String str, String str2) {
        if (str == null || !m12319(str2)) {
            return null;
        }
        return (byte[]) this.f10303.get(str2).get(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final long m12314() {
        return this.f10301;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m12315() {
        return this.f10303 != null && !this.f10303.isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<String, Map<String, byte[]>> m12316() {
        return this.f10303;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12317(long j) {
        this.f10301 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12318(Map<String, byte[]> map, String str) {
        if (this.f10303 == null) {
            this.f10303 = new HashMap();
        }
        this.f10303.put(str, map);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12319(String str) {
        if (str == null) {
            return false;
        }
        return m12315() && this.f10303.get(str) != null && !this.f10303.get(str).isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12320(String str, String str2) {
        return m12315() && m12319(str2) && m12313(str, str2) != null;
    }
}
