package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class zzdpz {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final CopyOnWriteArrayList<zzdpy> f9916 = new CopyOnWriteArrayList<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdpy m11662(String str) throws GeneralSecurityException {
        Iterator<zzdpy> it2 = f9916.iterator();
        while (it2.hasNext()) {
            zzdpy next = it2.next();
            if (next.m11661(str)) {
                return next;
            }
        }
        String valueOf = String.valueOf(str);
        throw new GeneralSecurityException(valueOf.length() != 0 ? "No KMS client does support: ".concat(valueOf) : new String("No KMS client does support: "));
    }
}
