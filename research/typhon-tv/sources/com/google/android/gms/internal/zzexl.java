package com.google.android.gms.internal;

import java.io.IOException;

public final class zzexl extends zzfjm<zzexl> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile zzexl[] f10324;

    /* renamed from: 靐  reason: contains not printable characters */
    public zzexj[] f10325 = zzexj.m12338();

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10326 = "";

    public zzexl() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzexl[] m12345() {
        if (f10324 == null) {
            synchronized (zzfjq.f10546) {
                if (f10324 == null) {
                    f10324 = new zzexl[0];
                }
            }
        }
        return f10324;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzexl)) {
            return false;
        }
        zzexl zzexl = (zzexl) obj;
        if (this.f10326 == null) {
            if (zzexl.f10326 != null) {
                return false;
            }
        } else if (!this.f10326.equals(zzexl.f10326)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f10325, (Object[]) zzexl.f10325)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzexl.f10533 == null || zzexl.f10533.m12849() : this.f10533.equals(zzexl.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.f10326 == null ? 0 : this.f10326.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + zzfjq.m12859((Object[]) this.f10325)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12346() {
        int r0 = super.m12841();
        if (this.f10326 != null && !this.f10326.equals("")) {
            r0 += zzfjk.m12808(1, this.f10326);
        }
        if (this.f10325 == null || this.f10325.length <= 0) {
            return r0;
        }
        int i = r0;
        for (zzexj zzexj : this.f10325) {
            if (zzexj != null) {
                i += zzfjk.m12807(2, (zzfjs) zzexj);
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12347(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10326 = zzfjj.m12791();
                    continue;
                case 18:
                    int r2 = zzfjv.m12883(zzfjj, 18);
                    int length = this.f10325 == null ? 0 : this.f10325.length;
                    zzexj[] zzexjArr = new zzexj[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10325, 0, zzexjArr, 0, length);
                    }
                    while (length < zzexjArr.length - 1) {
                        zzexjArr[length] = new zzexj();
                        zzfjj.m12802((zzfjs) zzexjArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzexjArr[length] = new zzexj();
                    zzfjj.m12802((zzfjs) zzexjArr[length]);
                    this.f10325 = zzexjArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12348(zzfjk zzfjk) throws IOException {
        if (this.f10326 != null && !this.f10326.equals("")) {
            zzfjk.m12835(1, this.f10326);
        }
        if (this.f10325 != null && this.f10325.length > 0) {
            for (zzexj zzexj : this.f10325) {
                if (zzexj != null) {
                    zzfjk.m12834(2, (zzfjs) zzexj);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
