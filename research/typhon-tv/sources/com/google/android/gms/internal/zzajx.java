package com.google.android.gms.internal;

import android.util.JsonWriter;
import java.util.Map;

final /* synthetic */ class zzajx implements zzaka {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map f8264;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f8265;

    zzajx(int i, Map map) {
        this.f8265 = i;
        this.f8264 = map;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9660(JsonWriter jsonWriter) {
        zzajv.m4777(this.f8265, this.f8264, jsonWriter);
    }
}
