package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagq extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8172;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8173;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagq(Context context, String str) {
        super((zzagi) null);
        this.f8173 = context;
        this.f8172 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9589() {
        SharedPreferences.Editor edit = this.f8173.getSharedPreferences("admob", 0).edit();
        edit.putString("native_advanced_settings", this.f8172);
        edit.apply();
    }
}
