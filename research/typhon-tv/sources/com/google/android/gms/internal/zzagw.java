package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagw extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f8183;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8184;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagw(Context context, int i) {
        super((zzagi) null);
        this.f8184 = context;
        this.f8183 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9595() {
        SharedPreferences.Editor edit = this.f8184.getSharedPreferences("admob", 0).edit();
        edit.putInt("request_in_session_count", this.f8183);
        edit.apply();
    }
}
