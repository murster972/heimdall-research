package com.google.android.gms.internal;

import android.content.Context;
import android.widget.ImageView;
import com.google.android.gms.R;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbao extends UIController {

    /* renamed from: 连任  reason: contains not printable characters */
    private Cast.Listener f8571;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8572 = this.f8573.getString(R.string.cast_mute);

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f8573;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f8574 = this.f8573.getString(R.string.cast_unmute);

    /* renamed from: 龘  reason: contains not printable characters */
    private final ImageView f8575;

    public zzbao(ImageView imageView, Context context) {
        this.f8575 = imageView;
        this.f8573 = context.getApplicationContext();
        this.f8575.setEnabled(false);
        this.f8571 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9900(boolean z) {
        this.f8575.setSelected(z);
        this.f8575.setContentDescription(z ? this.f8572 : this.f8574);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m9901() {
        CastSession r0 = CastContext.m7977(this.f8573).m7981().m8075();
        if (r0 == null || !r0.m8053()) {
            this.f8575.setEnabled(false);
            return;
        }
        RemoteMediaClient r1 = m8226();
        if (r1 == null || !r1.m4143()) {
            this.f8575.setEnabled(false);
        } else {
            this.f8575.setEnabled(true);
        }
        if (r0.m8019()) {
            m9900(true);
        } else {
            m9900(false);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9902() {
        this.f8575.setEnabled(false);
        CastSession r0 = CastContext.m7977(this.f8573).m7981().m8075();
        if (!(r0 == null || this.f8571 == null)) {
            r0.m8016(this.f8571);
        }
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9903() {
        this.f8575.setEnabled(false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9904() {
        m9901();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9905(CastSession castSession) {
        if (this.f8571 == null) {
            this.f8571 = new zzbap(this);
        }
        super.m8227(castSession);
        castSession.m8025(this.f8571);
        m9901();
    }
}
