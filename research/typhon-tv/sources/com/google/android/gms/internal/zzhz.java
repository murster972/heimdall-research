package com.google.android.gms.internal;

import java.nio.charset.Charset;
import java.security.MessageDigest;

@zzzv
public final class zzhz extends zzhq {

    /* renamed from: 靐  reason: contains not printable characters */
    private MessageDigest f4720;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f4721;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f4722;

    public zzhz(int i) {
        int i2 = i / 8;
        this.f4722 = i % 8 > 0 ? i2 + 1 : i2;
        this.f4721 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m5407(String str) {
        byte[] bArr;
        synchronized (this.f4714) {
            this.f4720 = m5395();
            if (this.f4720 == null) {
                bArr = new byte[0];
            } else {
                this.f4720.reset();
                this.f4720.update(str.getBytes(Charset.forName("UTF-8")));
                byte[] digest = this.f4720.digest();
                bArr = new byte[(digest.length > this.f4722 ? this.f4722 : digest.length)];
                System.arraycopy(digest, 0, bArr, 0, bArr.length);
                if (this.f4721 % 8 > 0) {
                    long j = 0;
                    for (int i = 0; i < bArr.length; i++) {
                        if (i > 0) {
                            j <<= 8;
                        }
                        j += (long) (bArr[i] & 255);
                    }
                    long j2 = j >>> (8 - (this.f4721 % 8));
                    for (int i2 = this.f4722 - 1; i2 >= 0; i2--) {
                        bArr[i2] = (byte) ((int) (255 & j2));
                        j2 >>>= 8;
                    }
                }
            }
        }
        return bArr;
    }
}
