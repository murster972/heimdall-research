package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.Pinkamena;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.internal.zzc;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzamd extends FrameLayout implements zzama {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzamb f4340;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4341;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f4342;

    /* renamed from: ʾ  reason: contains not printable characters */
    private String f4343;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Bitmap f4344;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f4345;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f4346;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f4347;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f4348;

    /* renamed from: 连任  reason: contains not printable characters */
    private final long f4349;

    /* renamed from: 靐  reason: contains not printable characters */
    private final FrameLayout f4350;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzamr f4351;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zznu f4352;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzamp f4353;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private ImageView f4354;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f4355;

    public zzamd(Context context, zzamp zzamp, int i, boolean z, zznu zznu, zzamo zzamo) {
        super(context);
        this.f4353 = zzamp;
        this.f4352 = zznu;
        this.f4350 = new FrameLayout(context);
        FrameLayout frameLayout = this.f4350;
        new FrameLayout.LayoutParams(-1, -1);
        Pinkamena.DianePie();
        zzc.m9140(zzamp.m4926());
        this.f4340 = zzamp.m4926().zzaom.m4860(context, zzamp, i, z, zznu, zzamo);
        if (this.f4340 != null) {
            this.f4350.addView(this.f4340, new FrameLayout.LayoutParams(-1, -1, 17));
            if (((Boolean) zzkb.m5481().m5595(zznh.f5131)).booleanValue()) {
                m4871();
            }
        }
        this.f4354 = new ImageView(context);
        this.f4349 = ((Long) zzkb.m5481().m5595(zznh.f5137)).longValue();
        this.f4347 = ((Boolean) zzkb.m5481().m5595(zznh.f5074)).booleanValue();
        if (this.f4352 != null) {
            this.f4352.m5629("spinner_used", this.f4347 ? PubnativeRequest.LEGACY_ZONE_ID : "0");
        }
        this.f4351 = new zzamr(this);
        if (this.f4340 != null) {
            this.f4340.m4859((zzama) this);
        }
        if (this.f4340 == null) {
            m4889("AdVideoUnderlay Error", "Allocating player failed.");
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private final void m4861() {
        if (this.f4353.m4928() != null && this.f4342 && !this.f4346) {
            this.f4353.m4928().getWindow().clearFlags(128);
            this.f4342 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4863(zzamp zzamp) {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, "no_video_view");
        zzamp.zza("onVideoEvent", hashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4864(zzamp zzamp, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, "decoderProps");
        hashMap.put("error", str);
        zzamp.zza("onVideoEvent", hashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4865(zzamp zzamp, Map<String, List<Map<String, Object>>> map) {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, "decoderProps");
        hashMap.put("mimeTypes", map);
        zzamp.zza("onVideoEvent", hashMap);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4866(String str, String... strArr) {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, str);
        int length = strArr.length;
        int i = 0;
        String str2 = null;
        while (i < length) {
            String str3 = strArr[i];
            if (str2 != null) {
                hashMap.put(str2, str3);
                str3 = null;
            }
            i++;
            str2 = str3;
        }
        this.f4353.zza("onVideoEvent", hashMap);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final boolean m4867() {
        return this.f4354.getParent() != null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m4868() {
        if (this.f4355 && this.f4344 != null && !m4867()) {
            this.f4354.setImageBitmap(this.f4344);
            this.f4354.invalidate();
            this.f4350.addView(this.f4354, new FrameLayout.LayoutParams(-1, -1));
            this.f4350.bringChildToFront(this.f4354);
        }
        this.f4351.m4941();
        this.f4345 = this.f4348;
        zzahn.f4212.post(new zzamf(this));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m4869() {
        if (this.f4341 && m4867()) {
            this.f4350.removeView(this.f4354);
        }
        if (this.f4344 != null) {
            long r0 = zzbs.zzeo().m9241();
            if (this.f4340.getBitmap(this.f4344) != null) {
                this.f4355 = true;
            }
            long r02 = zzbs.zzeo().m9241() - r0;
            if (zzagf.m4528()) {
                zzagf.m4527(new StringBuilder(46).append("Spinner frame grab took ").append(r02).append("ms").toString());
            }
            if (r02 > this.f4349) {
                zzagf.m4791("Spinner frame grab crossed jank threshold! Suspending spinner.");
                this.f4347 = false;
                this.f4344 = null;
                if (this.f4352 != null) {
                    this.f4352.m5629("spinner_jank", Long.toString(r02));
                }
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m4870() {
        if (this.f4340 != null) {
            if (!TextUtils.isEmpty(this.f4343)) {
                this.f4340.setVideoPath(this.f4343);
            } else {
                m4866("no_src", new String[0]);
            }
        }
    }

    @TargetApi(14)
    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m4871() {
        if (this.f4340 != null) {
            TextView textView = new TextView(this.f4340.getContext());
            String valueOf = String.valueOf(this.f4340.m4856());
            textView.setText(valueOf.length() != 0 ? "AdMob - ".concat(valueOf) : new String("AdMob - "));
            textView.setTextColor(SupportMenu.CATEGORY_MASK);
            textView.setBackgroundColor(InputDeviceCompat.SOURCE_ANY);
            this.f4350.addView(textView, new FrameLayout.LayoutParams(-2, -2, 17));
            this.f4350.bringChildToFront(textView);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m4872() {
        this.f4351.m4941();
        if (this.f4340 != null) {
            this.f4340.m4853();
        }
        m4861();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m4873() {
        if (this.f4340 != null) {
            zzamb zzamb = this.f4340;
            zzamb.f4338.m4947(false);
            zzamb.m4852();
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m4874() {
        if (this.f4340 != null) {
            this.f4340.m4854();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m4875() {
        if (this.f4340 != null) {
            this.f4340.m4855();
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m4876() {
        if (this.f4340 != null) {
            zzamb zzamb = this.f4340;
            zzamb.f4338.m4947(true);
            zzamb.m4852();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4877() {
        m4866("ended", new String[0]);
        m4861();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4878() {
        if (this.f4340 != null && this.f4345 == 0) {
            m4866("canplaythrough", VastIconXmlManager.DURATION, String.valueOf(((float) this.f4340.getDuration()) / 1000.0f), "videoWidth", String.valueOf(this.f4340.getVideoWidth()), "videoHeight", String.valueOf(this.f4340.getVideoHeight()));
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4879() {
        m4866("pause", new String[0]);
        m4861();
        this.f4341 = false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4880() {
        if (this.f4353.m4928() != null && !this.f4342) {
            this.f4346 = (this.f4353.m4928().getWindow().getAttributes().flags & 128) != 0;
            if (!this.f4346) {
                this.f4353.m4928().getWindow().addFlags(128);
                this.f4342 = true;
            }
        }
        this.f4341 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4881() {
        this.f4351.m4940();
        zzahn.f4212.post(new zzame(this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4882(float f) {
        if (this.f4340 != null) {
            zzamb zzamb = this.f4340;
            zzamb.f4338.m4946(f);
            zzamb.m4852();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4883(float f, float f2) {
        if (this.f4340 != null) {
            this.f4340.m4857(f, f2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4884(int i) {
        if (this.f4340 != null) {
            this.f4340.m4858(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4885(int i, int i2) {
        if (this.f4347) {
            int max = Math.max(i / ((Integer) zzkb.m5481().m5595(zznh.f5133)).intValue(), 1);
            int max2 = Math.max(i2 / ((Integer) zzkb.m5481().m5595(zznh.f5133)).intValue(), 1);
            if (this.f4344 == null || this.f4344.getWidth() != max || this.f4344.getHeight() != max2) {
                this.f4344 = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                this.f4355 = false;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4886(int i, int i2, int i3, int i4) {
        if (i3 != 0 && i4 != 0) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i3, i4);
            layoutParams.setMargins(i, i2, 0, 0);
            this.f4350.setLayoutParams(layoutParams);
            requestLayout();
        }
    }

    @TargetApi(14)
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4887(MotionEvent motionEvent) {
        if (this.f4340 != null) {
            this.f4340.dispatchTouchEvent(motionEvent);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4888(String str) {
        this.f4343 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4889(String str, String str2) {
        m4866("error", "what", str, "extra", str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m4890() {
        if (this.f4340 != null) {
            long currentPosition = (long) this.f4340.getCurrentPosition();
            if (this.f4348 != currentPosition && currentPosition > 0) {
                m4866("timeupdate", "time", String.valueOf(((float) currentPosition) / 1000.0f));
                this.f4348 = currentPosition;
            }
        }
    }
}
