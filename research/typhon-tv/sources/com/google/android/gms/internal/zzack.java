package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final class zzack implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzach f8073;

    zzack(zzach zzach) {
        this.f8073 = zzach;
    }

    public final void zza(Object obj, Map<String, String> map) {
        synchronized (this.f8073.f3928) {
            if (!this.f8073.f3927.isDone()) {
                zzacn zzacn = new zzacn(-2, map);
                if (this.f8073.f3930.equals(zzacn.m4308())) {
                    this.f8073.f3927.m4822(zzacn);
                }
            }
        }
    }
}
