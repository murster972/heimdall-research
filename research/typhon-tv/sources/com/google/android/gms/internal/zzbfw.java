package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public final class zzbfw implements zzbfv {
    /* renamed from: 龘  reason: contains not printable characters */
    public final PendingResult<Status> m10207(GoogleApiClient googleApiClient) {
        return googleApiClient.zze(new zzbfx(this, googleApiClient));
    }
}
