package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzaza extends zzev implements zzayz {
    public zzaza() {
        attachInterface(this, "com.google.android.gms.cast.framework.internal.IMediaRouter");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zzazb zzazc;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                Bundle bundle = (Bundle) zzew.m12304(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    zzazc = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.cast.framework.internal.IMediaRouterCallback");
                    zzazc = queryLocalInterface instanceof zzazb ? (zzazb) queryLocalInterface : new zzazc(readStrongBinder);
                }
                m9788(bundle, zzazc);
                parcel2.writeNoException();
                break;
            case 2:
                m9787((Bundle) zzew.m12304(parcel, Bundle.CREATOR), parcel.readInt());
                parcel2.writeNoException();
                break;
            case 3:
                m9786((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 4:
                boolean r0 = m9783((Bundle) zzew.m12304(parcel, Bundle.CREATOR), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12307(parcel2, r0);
                break;
            case 5:
                m9789(parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                m9785();
                parcel2.writeNoException();
                break;
            case 7:
                boolean r02 = m9782();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r02);
                break;
            case 8:
                Bundle r03 = m9781(parcel.readString());
                parcel2.writeNoException();
                zzew.m12302(parcel2, r03);
                break;
            case 9:
                String r04 = m9784();
                parcel2.writeNoException();
                parcel2.writeString(r04);
                break;
            case 10:
                parcel2.writeNoException();
                parcel2.writeInt(11910208);
                break;
            default:
                return false;
        }
        return true;
    }
}
