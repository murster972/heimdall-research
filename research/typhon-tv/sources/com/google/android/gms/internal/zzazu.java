package com.google.android.gms.internal;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface zzazu extends IInterface {

    public static abstract class zza extends zzev implements zzazu {
        /* renamed from: 龘  reason: contains not printable characters */
        public static zzazu m9844(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.internal.IFetchBitmapTask");
            return queryLocalInterface instanceof zzazu ? (zzazu) queryLocalInterface : new zzazv(iBinder);
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    Bitmap m9843(Uri uri) throws RemoteException;
}
