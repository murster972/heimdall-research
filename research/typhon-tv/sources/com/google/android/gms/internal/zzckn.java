package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzckn implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzckg f9573;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9574;

    zzckn(zzckg zzckg, zzcgi zzcgi) {
        this.f9573 = zzckg;
        this.f9574 = zzcgi;
    }

    public final void run() {
        zzche r0 = this.f9573.f9558;
        if (r0 == null) {
            this.f9573.m11096().m10832().m10849("Failed to send measurementEnabled to service");
            return;
        }
        try {
            r0.m10674(this.f9574);
            this.f9573.m11223();
        } catch (RemoteException e) {
            this.f9573.m11096().m10832().m10850("Failed to send measurementEnabled to the service", e);
        }
    }
}
