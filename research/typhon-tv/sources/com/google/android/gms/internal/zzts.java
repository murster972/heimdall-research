package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

public interface zzts<T> {
    /* renamed from: 龘  reason: contains not printable characters */
    JSONObject m13433(T t) throws JSONException;
}
