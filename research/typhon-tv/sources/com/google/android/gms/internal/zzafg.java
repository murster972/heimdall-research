package com.google.android.gms.internal;

import android.content.Context;
import java.util.concurrent.Callable;

final /* synthetic */ class zzafg implements Callable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f8140;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaff f8141;

    zzafg(zzaff zzaff, Context context) {
        this.f8141 = zzaff;
        this.f8140 = context;
    }

    public final Object call() {
        return this.f8141.m4424(this.f8140);
    }
}
