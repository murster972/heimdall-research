package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager;
import android.widget.RelativeLayout;
import com.Pinkamena;
import com.google.android.gms.R;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.internal.featurehighlight.zza;
import com.google.android.gms.cast.framework.internal.featurehighlight.zzi;

public final class zzazd extends RelativeLayout implements IntroductoryOverlay {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f8476;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f8477;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f8478;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public zza f8479;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public Activity f8480;

    /* renamed from: 麤  reason: contains not printable characters */
    private View f8481;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public IntroductoryOverlay.OnOverlayDismissedListener f8482;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f8483;

    @TargetApi(15)
    public zzazd(IntroductoryOverlay.Builder builder) {
        super(builder.m8044());
        this.f8480 = builder.m8044();
        this.f8483 = builder.m8038();
        this.f8482 = builder.m8046();
        this.f8481 = builder.m8047();
        this.f8476 = builder.m8039();
        this.f8478 = builder.m8043();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9806() {
        removeAllViews();
        this.f8480 = null;
        this.f8482 = null;
        this.f8481 = null;
        this.f8479 = null;
        this.f8476 = null;
        this.f8478 = 0;
        this.f8477 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m9809(Context context) {
        AccessibilityManager accessibilityManager = (AccessibilityManager) context.getSystemService("accessibility");
        return accessibilityManager != null && accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        m9806();
        super.onDetachedFromWindow();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9811() {
        if (this.f8480 != null && this.f8481 != null && !this.f8477 && !m9809((Context) this.f8480)) {
            if (!this.f8483 || !IntroductoryOverlay.zza.m8051(this.f8480)) {
                this.f8479 = new zza(this.f8480);
                if (this.f8478 != 0) {
                    this.f8479.m8126(this.f8478);
                }
                zza zza = this.f8479;
                Pinkamena.DianePie();
                zzi zzi = (zzi) this.f8480.getLayoutInflater().inflate(R.layout.cast_help_text, this.f8479, false);
                zzi.setText(this.f8476, (CharSequence) null);
                this.f8479.m8128(zzi);
                this.f8479.m8127(this.f8481, (View) null, true, new zzaze(this));
                this.f8477 = true;
                ((ViewGroup) this.f8480.getWindow().getDecorView()).addView(this);
                this.f8479.m8129((Runnable) null);
                return;
            }
            m9806();
        }
    }
}
