package com.google.android.gms.internal;

import android.os.Build;
import android.os.Looper;

final class zzdml extends ThreadLocal<zzdmk> {
    zzdml() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object initialValue() {
        if (Build.VERSION.SDK_INT >= 16) {
            return new zzdmq();
        }
        Looper myLooper = Looper.myLooper();
        if (myLooper != null) {
            return new zzdmp(myLooper);
        }
        throw new IllegalStateException("The current thread must have a looper!");
    }
}
