package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzake implements Parcelable.Creator<zzakd> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r6 = zzbfn.m10169(parcel);
        String str = null;
        boolean z = false;
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < r6) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r6);
        return new zzakd(str, i2, i, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzakd[i];
    }
}
