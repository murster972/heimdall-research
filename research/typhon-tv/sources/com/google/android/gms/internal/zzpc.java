package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final class zzpc implements zzt<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzoy f10827;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzzb f10828;

    zzpc(zzoy zzoy, zzzb zzzb) {
        this.f10827 = zzoy;
        this.f10828 = zzzb;
    }

    public final void zza(Object obj, Map<String, String> map) {
        zzanh zzanh = (zzanh) this.f10827.f10820.get();
        if (zzanh == null) {
            this.f10828.m6072("/hideOverlay", this);
        } else if (zzanh == null) {
            throw null;
        } else {
            ((View) zzanh).setVisibility(8);
        }
    }
}
