package com.google.android.gms.internal;

import java.nio.charset.Charset;
import java.util.Arrays;

public final class zzfjq {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Object f10546 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private static Charset f10547 = Charset.forName("ISO-8859-1");

    /* renamed from: 龘  reason: contains not printable characters */
    protected static final Charset f10548 = Charset.forName("UTF-8");

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12858(long[] jArr) {
        if (jArr == null || jArr.length == 0) {
            return 0;
        }
        return Arrays.hashCode(jArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12859(Object[] objArr) {
        int length = objArr == null ? 0 : objArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            Object obj = objArr[i];
            i++;
            i2 = obj != null ? obj.hashCode() + (i2 * 31) : i2;
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12860(byte[][] bArr) {
        int length = bArr == null ? 0 : bArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            byte[] bArr2 = bArr[i];
            i++;
            i2 = bArr2 != null ? Arrays.hashCode(bArr2) + (i2 * 31) : i2;
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12861(zzfjm zzfjm, zzfjm zzfjm2) {
        if (zzfjm.f10533 != null) {
            zzfjm2.f10533 = (zzfjo) zzfjm.f10533.clone();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m12862(long[] jArr, long[] jArr2) {
        return (jArr == null || jArr.length == 0) ? jArr2 == null || jArr2.length == 0 : Arrays.equals(jArr, jArr2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m12863(Object[] objArr, Object[] objArr2) {
        int length = objArr == null ? 0 : objArr.length;
        int length2 = objArr2 == null ? 0 : objArr2.length;
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i2 >= length || objArr[i2] != null) {
                int i3 = i;
                while (i3 < length2 && objArr2[i3] == null) {
                    i3++;
                }
                boolean z = i2 >= length;
                boolean z2 = i3 >= length2;
                if (z && z2) {
                    return true;
                }
                if (z != z2 || !objArr[i2].equals(objArr2[i3])) {
                    return false;
                }
                i = i3 + 1;
                i2++;
            } else {
                i2++;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m12864(byte[][] bArr, byte[][] bArr2) {
        int length = bArr == null ? 0 : bArr.length;
        int length2 = bArr2 == null ? 0 : bArr2.length;
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i2 >= length || bArr[i2] != null) {
                int i3 = i;
                while (i3 < length2 && bArr2[i3] == null) {
                    i3++;
                }
                boolean z = i2 >= length;
                boolean z2 = i3 >= length2;
                if (z && z2) {
                    return true;
                }
                if (z != z2 || !Arrays.equals(bArr[i2], bArr2[i3])) {
                    return false;
                }
                i = i3 + 1;
                i2++;
            } else {
                i2++;
            }
        }
    }
}
