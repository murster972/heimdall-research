package com.google.android.gms.internal;

import android.content.Context;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@zzzv
public final class zzacq {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public WeakHashMap<Context, zzacs> f4007 = new WeakHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public final Future<zzaco> m4318(Context context) {
        return zzahh.m4556((ExecutorService) zzahh.f4211, new zzacr(this, context));
    }
}
