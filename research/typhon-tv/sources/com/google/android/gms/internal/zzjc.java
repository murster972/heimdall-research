package com.google.android.gms.internal;

import java.io.IOException;

public final class zzjc extends zzfjm<zzjc> {

    /* renamed from: 靐  reason: contains not printable characters */
    public long[] f10747 = zzfjv.f10560;

    /* renamed from: 麤  reason: contains not printable characters */
    public zzja f10748 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzjb f10749 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10750 = null;

    public zzjc() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m13025() {
        int i;
        int i2 = 0;
        int r0 = super.m12841();
        if (this.f10750 != null) {
            r0 += zzfjk.m12808(10, this.f10750);
        }
        if (this.f10747 != null && this.f10747.length > 0) {
            int i3 = 0;
            while (true) {
                i = i2;
                if (i3 >= this.f10747.length) {
                    break;
                }
                i2 = zzfjk.m12817(this.f10747[i3]) + i;
                i3++;
            }
            r0 = r0 + i + (this.f10747.length * 1);
        }
        if (this.f10749 != null) {
            r0 += zzfjk.m12807(15, (zzfjs) this.f10749);
        }
        return this.f10748 != null ? r0 + zzfjk.m12807(18, (zzfjs) this.f10748) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m13026(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 82:
                    this.f10750 = zzfjj.m12791();
                    continue;
                case 112:
                    int r2 = zzfjv.m12883(zzfjj, 112);
                    int length = this.f10747 == null ? 0 : this.f10747.length;
                    long[] jArr = new long[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10747, 0, jArr, 0, length);
                    }
                    while (length < jArr.length - 1) {
                        jArr[length] = zzfjj.m12786();
                        zzfjj.m12800();
                        length++;
                    }
                    jArr[length] = zzfjj.m12786();
                    this.f10747 = jArr;
                    continue;
                case 114:
                    int r3 = zzfjj.m12799(zzfjj.m12785());
                    int r22 = zzfjj.m12787();
                    int i = 0;
                    while (zzfjj.m12790() > 0) {
                        zzfjj.m12786();
                        i++;
                    }
                    zzfjj.m12792(r22);
                    int length2 = this.f10747 == null ? 0 : this.f10747.length;
                    long[] jArr2 = new long[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f10747, 0, jArr2, 0, length2);
                    }
                    while (length2 < jArr2.length) {
                        jArr2[length2] = zzfjj.m12786();
                        length2++;
                    }
                    this.f10747 = jArr2;
                    zzfjj.m12796(r3);
                    continue;
                case 122:
                    if (this.f10749 == null) {
                        this.f10749 = new zzjb();
                    }
                    zzfjj.m12802((zzfjs) this.f10749);
                    continue;
                case 146:
                    if (this.f10748 == null) {
                        this.f10748 = new zzja();
                    }
                    zzfjj.m12802((zzfjs) this.f10748);
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13027(zzfjk zzfjk) throws IOException {
        if (this.f10750 != null) {
            zzfjk.m12835(10, this.f10750);
        }
        if (this.f10747 != null && this.f10747.length > 0) {
            for (long r2 : this.f10747) {
                zzfjk.m12833(14, r2);
            }
        }
        if (this.f10749 != null) {
            zzfjk.m12834(15, (zzfjs) this.f10749);
        }
        if (this.f10748 != null) {
            zzfjk.m12834(18, (zzfjs) this.f10748);
        }
        super.m12842(zzfjk);
    }
}
