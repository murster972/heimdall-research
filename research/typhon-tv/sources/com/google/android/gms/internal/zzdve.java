package com.google.android.gms.internal;

public final class zzdve {

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f10226;

    private zzdve(byte[] bArr, int i, int i2) {
        this.f10226 = new byte[i2];
        System.arraycopy(bArr, 0, this.f10226, 0, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdve m12227(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return new zzdve(bArr, 0, bArr.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12228() {
        byte[] bArr = new byte[this.f10226.length];
        System.arraycopy(this.f10226, 0, bArr, 0, this.f10226.length);
        return bArr;
    }
}
