package com.google.android.gms.internal;

import android.support.customtabs.CustomTabsServiceConnection;
import java.lang.ref.WeakReference;

public final class zzfts extends CustomTabsServiceConnection {

    /* renamed from: 龘  reason: contains not printable characters */
    private WeakReference<zzftt> f10633;

    public zzfts(zzftt zzftt) {
        this.f10633 = new WeakReference<>(zzftt);
    }
}
