package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzfhj<T> implements zzfhv<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzfin<?, ?> f10442;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzffn<?> f10443;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f10444;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzfhe f10445;

    private zzfhj(Class<T> cls, zzfin<?, ?> zzfin, zzffn<?> zzffn, zzfhe zzfhe) {
        this.f10442 = zzfin;
        this.f10444 = zzffn.m12502((Class<?>) cls);
        this.f10443 = zzffn;
        this.f10445 = zzfhe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> zzfhj<T> m12638(Class<T> cls, zzfin<?, ?> zzfin, zzffn<?> zzffn, zzfhe zzfhe) {
        return new zzfhj<>(cls, zzfin, zzffn, zzfhe);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12639(T t) {
        zzfin<?, ?> zzfin = this.f10442;
        int r0 = zzfin.m12708(zzfin.m12709(t)) + 0;
        return this.f10444 ? r0 + this.f10443.m12501((Object) t).m12516() : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12640(T t, zzfji zzfji) {
        Iterator<Map.Entry<?, Object>> r2 = this.f10443.m12501((Object) t).m12515();
        while (r2.hasNext()) {
            Map.Entry next = r2.next();
            zzffs zzffs = (zzffs) next.getKey();
            if (zzffs.m12520() != zzfjd.MESSAGE || zzffs.m12519() || zzffs.m12517()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzfgi) {
                zzfji.m12779(zzffs.m12521(), ((zzfgi) next).m12595().m12598());
            } else {
                zzfji.m12779(zzffs.m12521(), next.getValue());
            }
        }
        zzfin<?, ?> zzfin = this.f10442;
        zzfin.m12710(zzfin.m12709(t), zzfji);
    }
}
