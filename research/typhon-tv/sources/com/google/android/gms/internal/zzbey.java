package com.google.android.gms.internal;

public class zzbey<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzbfe f8726 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private static String f8727 = "com.google.android.providers.gsf.permission.READ_GSERVICES";

    /* renamed from: 齉  reason: contains not printable characters */
    private static int f8728 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f8729 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private T f8730;

    /* renamed from: ʼ  reason: contains not printable characters */
    private T f8731 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f8732;

    protected zzbey(String str, T t) {
        this.f8732 = str;
        this.f8730 = t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbey<Integer> m10142(String str, Integer num) {
        return new zzbfb(str, num);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbey<Long> m10143(String str, Long l) {
        return new zzbfa(str, l);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbey<String> m10144(String str, String str2) {
        return new zzbfd(str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbey<Boolean> m10145(String str, boolean z) {
        return new zzbez(str, Boolean.valueOf(z));
    }
}
