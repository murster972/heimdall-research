package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzkj extends zzeu implements zzkh {
    zzkj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdListener");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m13062() throws RemoteException {
        m12298(7, v_());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m13063() throws RemoteException {
        m12298(6, v_());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13064() throws RemoteException {
        m12298(3, v_());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13065() throws RemoteException {
        m12298(5, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13066() throws RemoteException {
        m12298(4, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13067() throws RemoteException {
        m12298(1, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13068(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(2, v_);
    }
}
