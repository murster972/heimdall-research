package com.google.android.gms.internal;

import android.util.Log;
import com.google.android.gms.common.internal.zzal;
import java.util.Locale;

public final class zzbgg {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8742;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f8743;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzal f8744;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8745;

    private zzbgg(String str, String str2) {
        this.f8742 = str2;
        this.f8745 = str;
        this.f8744 = new zzal(str);
        this.f8743 = m10217();
    }

    public zzbgg(String str, String... strArr) {
        this(str, m10218(strArr));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final String m10216(String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(Locale.US, str, objArr);
        }
        return this.f8742.concat(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m10217() {
        int i = 2;
        while (7 >= i && !Log.isLoggable(this.f8745, i)) {
            i++;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m10218(String... strArr) {
        if (strArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (String str : strArr) {
            if (sb.length() > 1) {
                sb.append(",");
            }
            sb.append(str);
        }
        sb.append(']').append(' ');
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m10219(int i) {
        return this.f8743 <= i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10220(String str, Object... objArr) {
        if (m10219(3)) {
            Log.d(this.f8745, m10216(str, objArr));
        }
    }
}
