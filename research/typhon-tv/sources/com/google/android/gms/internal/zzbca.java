package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzr;

final class zzbca extends Api.zza<zzbcd, Object> {
    zzbca() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Api.zze m9965(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return new zzbcd(context, looper, zzr, connectionCallbacks, onConnectionFailedListener);
    }
}
