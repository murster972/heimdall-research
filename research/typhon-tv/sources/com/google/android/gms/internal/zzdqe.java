package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import com.google.android.gms.internal.zzdth;
import java.security.GeneralSecurityException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class zzdqe {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentMap<String, zzdpw> f9937 = new ConcurrentHashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final ConcurrentMap<String, zzdpq> f9938 = new ConcurrentHashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final ConcurrentMap<String, Boolean> f9939 = new ConcurrentHashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Logger f9940 = Logger.getLogger(zzdqe.class.getName());

    /* renamed from: 靐  reason: contains not printable characters */
    private static <P> zzdpw<P> m11671(String str) throws GeneralSecurityException {
        zzdpw<P> zzdpw = (zzdpw) f9937.get(str);
        if (zzdpw != null) {
            return zzdpw;
        }
        throw new GeneralSecurityException(new StringBuilder(String.valueOf(str).length() + 78).append("No key manager found for key type: ").append(str).append(".  Check the configuration of the registry.").toString());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <P> zzfhe m11672(zzdtd zzdtd) throws GeneralSecurityException {
        zzdpw r1 = m11671(zzdtd.m12037());
        if (((Boolean) f9939.get(zzdtd.m12037())).booleanValue()) {
            return r1.m11653(zzdtd.m12034());
        }
        String valueOf = String.valueOf(zzdtd.m12037());
        throw new GeneralSecurityException(valueOf.length() != 0 ? "newKey-operation not permitted for key type ".concat(valueOf) : new String("newKey-operation not permitted for key type "));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <P> P m11673(String str, zzfhe zzfhe) throws GeneralSecurityException {
        return m11671(str).m11657(zzfhe);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdpq m11674(String str) throws GeneralSecurityException {
        if (str == null) {
            throw new IllegalArgumentException("catalogueName must be non-null.");
        }
        zzdpq zzdpq = (zzdpq) f9938.get(str.toLowerCase());
        if (zzdpq != null) {
            return zzdpq;
        }
        String format = String.format("no catalogue found for %s. ", new Object[]{str});
        if (str.toLowerCase().startsWith("tinkaead")) {
            format = String.valueOf(format).concat("Maybe call AeadConfig.init().");
        } else if (str.toLowerCase().startsWith("tinkhybrid")) {
            format = String.valueOf(format).concat("Maybe call HybridConfig.init().");
        } else if (str.toLowerCase().startsWith("tinkmac")) {
            format = String.valueOf(format).concat("Maybe call MacConfig.init().");
        } else if (str.toLowerCase().startsWith("tinksignature")) {
            format = String.valueOf(format).concat("Maybe call SignatureConfig.init().");
        } else if (str.toLowerCase().startsWith("tink")) {
            format = String.valueOf(format).concat("Maybe call TinkConfig.init().");
        }
        throw new GeneralSecurityException(format);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <P> zzdqc<P> m11675(zzdpx zzdpx, zzdpw<P> zzdpw) throws GeneralSecurityException {
        zzdth r0 = zzdpx.m11659();
        if (r0.m12069() == 0) {
            throw new GeneralSecurityException("empty keyset");
        }
        int r5 = r0.m12070();
        boolean z = true;
        boolean z2 = false;
        for (zzdth.zzb next : r0.m12067()) {
            if (!next.m12082()) {
                throw new GeneralSecurityException(String.format("key %d has no key data", new Object[]{Integer.valueOf(next.m12076())}));
            } else if (next.m12075() == zzdtt.UNKNOWN_PREFIX) {
                throw new GeneralSecurityException(String.format("key %d has unknown prefix", new Object[]{Integer.valueOf(next.m12076())}));
            } else if (next.m12079() == zzdtb.UNKNOWN_STATUS) {
                throw new GeneralSecurityException(String.format("key %d has unknown status", new Object[]{Integer.valueOf(next.m12076())}));
            } else {
                if (next.m12079() == zzdtb.ENABLED && next.m12076() == r5) {
                    if (z2) {
                        throw new GeneralSecurityException("keyset contains multiple primary keys");
                    }
                    z2 = true;
                }
                z = next.m12077().m12017() != zzdsy.zzb.ASYMMETRIC_PUBLIC ? false : z;
            }
        }
        if (z2 || z) {
            zzdqc<P> zzdqc = new zzdqc<>();
            for (zzdth.zzb next2 : zzdpx.m11659().m12067()) {
                if (next2.m12079() == zzdtb.ENABLED) {
                    zzdqd<P> r3 = zzdqc.m11667(m11678(next2.m12077().m12019(), next2.m12077().m12015()), next2);
                    if (next2.m12076() == zzdpx.m11659().m12070()) {
                        zzdqc.m11668(r3);
                    }
                }
            }
            return zzdqc;
        }
        throw new GeneralSecurityException("keyset doesn't contain a valid primary key");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <P> zzdsy m11676(zzdtd zzdtd) throws GeneralSecurityException {
        zzdpw r1 = m11671(zzdtd.m12037());
        if (((Boolean) f9939.get(zzdtd.m12037())).booleanValue()) {
            return r1.m11655(zzdtd.m12034());
        }
        String valueOf = String.valueOf(zzdtd.m12037());
        throw new GeneralSecurityException(valueOf.length() != 0 ? "newKey-operation not permitted for key type ".concat(valueOf) : new String("newKey-operation not permitted for key type "));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <P> zzfhe m11677(String str, zzfhe zzfhe) throws GeneralSecurityException {
        zzdpw r1 = m11671(str);
        if (((Boolean) f9939.get(str)).booleanValue()) {
            return r1.m11654(zzfhe);
        }
        String valueOf = String.valueOf(str);
        throw new GeneralSecurityException(valueOf.length() != 0 ? "newKey-operation not permitted for key type ".concat(valueOf) : new String("newKey-operation not permitted for key type "));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <P> P m11678(String str, zzfes zzfes) throws GeneralSecurityException {
        return m11671(str).m11656(zzfes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <P> P m11679(String str, byte[] bArr) throws GeneralSecurityException {
        return m11678(str, zzfes.zzaz(bArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m11680(String str, zzdpq zzdpq) throws GeneralSecurityException {
        synchronized (zzdqe.class) {
            if (!f9938.containsKey(str.toLowerCase()) || zzdpq.getClass().equals(((zzdpq) f9938.get(str.toLowerCase())).getClass())) {
                f9938.put(str.toLowerCase(), zzdpq);
            } else {
                Logger logger = f9940;
                Level level = Level.WARNING;
                String valueOf = String.valueOf(str);
                logger.logp(level, "com.google.crypto.tink.Registry", "addCatalogue", valueOf.length() != 0 ? "Attempted overwrite of a catalogueName catalogue for name ".concat(valueOf) : new String("Attempted overwrite of a catalogueName catalogue for name "));
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(str).length() + 47).append("catalogue for name ").append(str).append(" has been already registered").toString());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <P> void m11681(String str, zzdpw<P> zzdpw) throws GeneralSecurityException {
        m11682(str, zzdpw, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized <P> void m11682(String str, zzdpw<P> zzdpw, boolean z) throws GeneralSecurityException {
        synchronized (zzdqe.class) {
            if (zzdpw == null) {
                throw new IllegalArgumentException("key manager must be non-null.");
            }
            if (f9937.containsKey(str)) {
                zzdpw r2 = m11671(str);
                boolean booleanValue = ((Boolean) f9939.get(str)).booleanValue();
                if (!zzdpw.getClass().equals(r2.getClass()) || (!booleanValue && z)) {
                    Logger logger = f9940;
                    Level level = Level.WARNING;
                    String valueOf = String.valueOf(str);
                    logger.logp(level, "com.google.crypto.tink.Registry", "registerKeyManager", valueOf.length() != 0 ? "Attempted overwrite of a registered key manager for key type ".concat(valueOf) : new String("Attempted overwrite of a registered key manager for key type "));
                    throw new GeneralSecurityException(String.format("typeUrl (%s) is already registered with %s, cannot be re-registered with %s", new Object[]{str, r2.getClass().getName(), zzdpw.getClass().getName()}));
                }
            }
            f9937.put(str, zzdpw);
            f9939.put(str, Boolean.valueOf(z));
        }
    }
}
