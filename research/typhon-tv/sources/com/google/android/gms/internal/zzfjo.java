package com.google.android.gms.internal;

public final class zzfjo implements Cloneable {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfjp f10538 = new zzfjp();

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10539;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f10540;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzfjp[] f10541;

    /* renamed from: 齉  reason: contains not printable characters */
    private int[] f10542;

    zzfjo() {
        this(10);
    }

    private zzfjo(int i) {
        this.f10540 = false;
        int r0 = m12847(i);
        this.f10542 = new int[r0];
        this.f10541 = new zzfjp[r0];
        this.f10539 = 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final int m12846(int i) {
        int i2 = 0;
        int i3 = this.f10539 - 1;
        while (i2 <= i3) {
            int i4 = (i2 + i3) >>> 1;
            int i5 = this.f10542[i4];
            if (i5 < i) {
                i2 = i4 + 1;
            } else if (i5 <= i) {
                return i4;
            } else {
                i3 = i4 - 1;
            }
        }
        return i2 ^ -1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m12847(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            } else if (i2 <= (1 << i3) - 12) {
                i2 = (1 << i3) - 12;
                break;
            } else {
                i3++;
            }
        }
        return i2 / 4;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.f10539;
        zzfjo zzfjo = new zzfjo(i);
        System.arraycopy(this.f10542, 0, zzfjo.f10542, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            if (this.f10541[i2] != null) {
                zzfjo.f10541[i2] = (zzfjp) this.f10541[i2].clone();
            }
        }
        zzfjo.f10539 = i;
        return zzfjo;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfjo)) {
            return false;
        }
        zzfjo zzfjo = (zzfjo) obj;
        if (this.f10539 != zzfjo.f10539) {
            return false;
        }
        int[] iArr = this.f10542;
        int[] iArr2 = zzfjo.f10542;
        int i = this.f10539;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            zzfjp[] zzfjpArr = this.f10541;
            zzfjp[] zzfjpArr2 = zzfjo.f10541;
            int i3 = this.f10539;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!zzfjpArr[i4].equals(zzfjpArr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.f10539; i2++) {
            i = (((i * 31) + this.f10542[i2]) * 31) + this.f10541[i2].hashCode();
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfjp m12848(int i) {
        return this.f10541[i];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12849() {
        return this.f10539 == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12850() {
        return this.f10539;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzfjp m12851(int i) {
        int r0 = m12846(i);
        if (r0 < 0 || this.f10541[r0] == f10538) {
            return null;
        }
        return this.f10541[r0];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12852(int i, zzfjp zzfjp) {
        int r0 = m12846(i);
        if (r0 >= 0) {
            this.f10541[r0] = zzfjp;
            return;
        }
        int i2 = r0 ^ -1;
        if (i2 >= this.f10539 || this.f10541[i2] != f10538) {
            if (this.f10539 >= this.f10542.length) {
                int r1 = m12847(this.f10539 + 1);
                int[] iArr = new int[r1];
                zzfjp[] zzfjpArr = new zzfjp[r1];
                System.arraycopy(this.f10542, 0, iArr, 0, this.f10542.length);
                System.arraycopy(this.f10541, 0, zzfjpArr, 0, this.f10541.length);
                this.f10542 = iArr;
                this.f10541 = zzfjpArr;
            }
            if (this.f10539 - i2 != 0) {
                System.arraycopy(this.f10542, i2, this.f10542, i2 + 1, this.f10539 - i2);
                System.arraycopy(this.f10541, i2, this.f10541, i2 + 1, this.f10539 - i2);
            }
            this.f10542[i2] = i;
            this.f10541[i2] = zzfjp;
            this.f10539++;
            return;
        }
        this.f10542[i2] = i;
        this.f10541[i2] = zzfjp;
    }
}
