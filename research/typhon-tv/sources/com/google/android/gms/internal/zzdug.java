package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

public abstract class zzdug implements zzdpp {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzdub f10188;

    private zzdug(zzdub zzdub) {
        this.f10188 = zzdub;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12198(int i) {
        return (((i + 16) - 1) / 16) << 4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdug m12200(byte[] bArr) {
        return new zzdui(zzdub.m12174(bArr));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract byte[] m12201(byte[] bArr, ByteBuffer byteBuffer);

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12202(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        ByteBuffer allocate = ByteBuffer.allocate(this.f10188.m12181() + bArr.length + 16);
        if (allocate.remaining() < bArr.length + this.f10188.m12181() + 16) {
            throw new IllegalArgumentException("Given ByteBuffer output is too small");
        }
        int position = allocate.position();
        this.f10188.m12182(allocate, bArr);
        allocate.position(position);
        byte[] bArr3 = new byte[this.f10188.m12181()];
        allocate.get(bArr3);
        allocate.limit(allocate.limit() - 16);
        byte[] r1 = zzdvh.m12234(new zzduf(this.f10188, bArr3, 0).m12196(32), m12201(bArr2, allocate));
        allocate.limit(allocate.limit() + 16);
        allocate.put(r1);
        return allocate.array();
    }
}
