package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzwx {

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f5537;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f5538;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f5539;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f5540;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f5541;

    private zzwx(zzwz zzwz) {
        this.f5541 = zzwz.f10962;
        this.f5538 = zzwz.f10959;
        this.f5540 = zzwz.f10961;
        this.f5539 = zzwz.f10960;
        this.f5537 = zzwz.f10958;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m6005() {
        try {
            return new JSONObject().put("sms", this.f5541).put("tel", this.f5538).put("calendar", this.f5540).put("storePicture", this.f5539).put("inlineVideo", this.f5537);
        } catch (JSONException e) {
            zzagf.m4793("Error occured while obtaining the MRAID capabilities.", e);
            return null;
        }
    }
}
