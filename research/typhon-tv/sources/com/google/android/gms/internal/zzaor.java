package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.View;
import android.webkit.WebChromeClient;

@zzzv
@TargetApi(14)
public final class zzaor extends zzaoi {
    public zzaor(zzanh zzanh) {
        super(zzanh);
    }

    public final void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
        m5216(view, i, customViewCallback);
    }
}
