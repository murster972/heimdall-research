package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.concurrent.TimeUnit;

@zzzv
public final class zzamq {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzair f4406 = new zzaiu().m9640("min_1", Double.MIN_VALUE, 1.0d).m9640("1_5", 1.0d, 5.0d).m9640("5_10", 5.0d, 10.0d).m9640("10_20", 10.0d, 20.0d).m9640("20_30", 20.0d, 30.0d).m9640("30_max", 30.0d, Double.MAX_VALUE).m9639();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final long[] f4407;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String[] f4408;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4409;

    /* renamed from: ʿ  reason: contains not printable characters */
    private zzamb f4410;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f4411 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    private long f4412 = -1;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f4413 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f4414 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f4415 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zznu f4416;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f4417;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzns f4418;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzakd f4419;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f4420;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f4421;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f4422;

    public zzamq(Context context, zzakd zzakd, String str, zznu zznu, zzns zzns) {
        this.f4420 = context;
        this.f4419 = zzakd;
        this.f4417 = str;
        this.f4416 = zznu;
        this.f4418 = zzns;
        String str2 = (String) zzkb.m5481().m5595(zznh.f5100);
        if (str2 == null) {
            this.f4408 = new String[0];
            this.f4407 = new long[0];
            return;
        }
        String[] split = TextUtils.split(str2, ",");
        this.f4408 = new String[split.length];
        this.f4407 = new long[split.length];
        for (int i = 0; i < split.length; i++) {
            try {
                this.f4407[i] = Long.parseLong(split[i]);
            } catch (NumberFormatException e) {
                zzagf.m4796("Unable to parse frame hash target time number.", e);
                this.f4407[i] = -1;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4933() {
        if (((Boolean) zzkb.m5481().m5595(zznh.f5164)).booleanValue() && !this.f4421) {
            Bundle bundle = new Bundle();
            bundle.putString(VastExtensionXmlManager.TYPE, "native-player-metrics");
            bundle.putString("request", this.f4417);
            bundle.putString("player", this.f4410.m4856());
            for (zzait next : this.f4406.m4709()) {
                String valueOf = String.valueOf("fps_c_");
                String valueOf2 = String.valueOf(next.f8236);
                bundle.putString(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf), Integer.toString(next.f8235));
                String valueOf3 = String.valueOf("fps_p_");
                String valueOf4 = String.valueOf(next.f8236);
                bundle.putString(valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), Double.toString(next.f8233));
            }
            for (int i = 0; i < this.f4407.length; i++) {
                String str = this.f4408[i];
                if (str != null) {
                    String valueOf5 = String.valueOf(Long.valueOf(this.f4407[i]));
                    bundle.putString(new StringBuilder(String.valueOf("fh_").length() + String.valueOf(valueOf5).length()).append("fh_").append(valueOf5).toString(), str);
                }
            }
            zzbs.zzei().m4635(this.f4420, this.f4419.f4297, "gmob-apps", bundle, true);
            this.f4421 = true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4934(zzamb zzamb) {
        long j;
        if (this.f4415 && !this.f4411) {
            zznn.m5614(this.f4416, this.f4418, "vff2");
            this.f4411 = true;
        }
        long r0 = zzbs.zzeo().m9242();
        if (this.f4409 && this.f4422 && this.f4412 != -1) {
            this.f4406.m4710(((double) TimeUnit.SECONDS.toNanos(1)) / ((double) (r0 - this.f4412)));
        }
        this.f4422 = this.f4409;
        this.f4412 = r0;
        long longValue = ((Long) zzkb.m5481().m5595(zznh.f5126)).longValue();
        long currentPosition = (long) zzamb.getCurrentPosition();
        int i = 0;
        while (i < this.f4408.length) {
            if (this.f4408[i] != null || longValue <= Math.abs(currentPosition - this.f4407[i])) {
                i++;
            } else {
                String[] strArr = this.f4408;
                Bitmap bitmap = zzamb.getBitmap(8, 8);
                long j2 = 0;
                long j3 = 63;
                int i2 = 0;
                while (i2 < 8) {
                    int i3 = 0;
                    while (true) {
                        j = j3;
                        if (i3 >= 8) {
                            break;
                        }
                        int pixel = bitmap.getPixel(i3, i2);
                        j2 |= (Color.green(pixel) + (Color.blue(pixel) + Color.red(pixel)) > 128 ? 1 : 0) << ((int) j);
                        i3++;
                        j3 = j - 1;
                    }
                    i2++;
                    j3 = j;
                }
                strArr[i] = String.format("%016X", new Object[]{Long.valueOf(j2)});
                return;
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4935() {
        this.f4409 = false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4936() {
        this.f4409 = true;
        if (this.f4414 && !this.f4415) {
            zznn.m5614(this.f4416, this.f4418, "vfp2");
            this.f4415 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4937() {
        if (this.f4413 && !this.f4414) {
            zznn.m5614(this.f4416, this.f4418, "vfr2");
            this.f4414 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4938(zzamb zzamb) {
        zznn.m5614(this.f4416, this.f4418, "vpc2");
        this.f4413 = true;
        if (this.f4416 != null) {
            this.f4416.m5629("vpn", zzamb.m4856());
        }
        this.f4410 = zzamb;
    }
}
