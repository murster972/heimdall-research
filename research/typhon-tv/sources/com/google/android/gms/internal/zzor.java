package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

@zzzv
public final class zzor extends zzow {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f5258;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Object f5259;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzos f5260;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzvj f5261;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzot f5262;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzvm f5263;

    private zzor(Context context, zzot zzot, zzcv zzcv, zzou zzou) {
        super(context, zzot, (zzzb) null, zzcv, (JSONObject) null, zzou, (zzakd) null, (String) null);
        this.f5258 = false;
        this.f5259 = new Object();
        this.f5262 = zzot;
    }

    public zzor(Context context, zzot zzot, zzcv zzcv, zzvj zzvj, zzou zzou) {
        this(context, zzot, zzcv, zzou);
        this.f5261 = zzvj;
    }

    public zzor(Context context, zzot zzot, zzcv zzcv, zzvm zzvm, zzou zzou) {
        this(context, zzot, zzcv, zzou);
        this.f5263 = zzvm;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5718() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m5719() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzanh m5720() {
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5721(View view, Map<String, WeakReference<View>> map) {
        synchronized (this.f5259) {
            try {
                if (this.f5261 != null) {
                    this.f5261.m13546(zzn.m9306(view));
                } else if (this.f5263 != null) {
                    this.f5263.m13585(zzn.m9306(view));
                }
            } catch (RemoteException e) {
                zzagf.m4796("Failed to call untrackView", e);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5722() {
        boolean r0;
        synchronized (this.f5259) {
            r0 = this.f5260 != null ? this.f5260.m13191() : this.f5262.zzcx();
        }
        return r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzos m5723() {
        zzos zzos;
        synchronized (this.f5259) {
            zzos = this.f5260;
        }
        return zzos;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5724() {
        boolean z;
        synchronized (this.f5259) {
            z = this.f5258;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m5725(View.OnClickListener onClickListener, boolean z) {
        IObjectWrapper iObjectWrapper;
        synchronized (this.f5259) {
            if (this.f5260 != null) {
                View r0 = this.f5260.m13194(onClickListener, z);
                return r0;
            }
            try {
                if (this.f5261 != null) {
                    iObjectWrapper = this.f5261.m13535();
                } else {
                    if (this.f5263 != null) {
                        iObjectWrapper = this.f5263.m13579();
                    }
                    iObjectWrapper = null;
                }
            } catch (RemoteException e) {
                zzagf.m4796("Failed to call getAdChoicesContent", e);
            }
            if (iObjectWrapper == null) {
                return null;
            }
            View view = (View) zzn.m9307(iObjectWrapper);
            return view;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5726(View view, Map<String, WeakReference<View>> map) {
        zzbq.m9115("recordImpression must be called on the main UI thread.");
        synchronized (this.f5259) {
            this.f5275 = true;
            if (this.f5260 != null) {
                this.f5260.m13199(view, map);
                this.f5262.recordImpression();
            } else {
                try {
                    if (this.f5261 != null && !this.f5261.m13539()) {
                        this.f5261.m13538();
                        this.f5262.recordImpression();
                    } else if (this.f5263 != null && !this.f5263.m13573()) {
                        this.f5263.m13572();
                        this.f5262.recordImpression();
                    }
                } catch (RemoteException e) {
                    zzagf.m4796("Failed to call recordImpression", e);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5727(View view, Map<String, WeakReference<View>> map, Bundle bundle, View view2) {
        zzbq.m9115("performClick must be called on the main UI thread.");
        synchronized (this.f5259) {
            if (this.f5260 != null) {
                this.f5260.m13200(view, map, bundle, view2);
                this.f5262.onAdClicked();
            } else {
                try {
                    if (this.f5261 != null && !this.f5261.m13540()) {
                        this.f5261.m13548(zzn.m9306(view));
                        this.f5262.onAdClicked();
                    }
                    if (this.f5263 != null && !this.f5263.m13577()) {
                        this.f5263.m13587(zzn.m9306(view));
                        this.f5262.onAdClicked();
                    }
                } catch (RemoteException e) {
                    zzagf.m4796("Failed to call performClick", e);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5728(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, View.OnTouchListener onTouchListener, View.OnClickListener onClickListener) {
        synchronized (this.f5259) {
            this.f5258 = true;
            try {
                if (this.f5261 != null) {
                    this.f5261.m13543(zzn.m9306(view));
                } else if (this.f5263 != null) {
                    this.f5263.m13582(zzn.m9306(view));
                }
            } catch (RemoteException e) {
                zzagf.m4796("Failed to call prepareAd", e);
            }
            this.f5258 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5729(zzos zzos) {
        synchronized (this.f5259) {
            this.f5260 = zzos;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5730() {
        boolean r0;
        synchronized (this.f5259) {
            r0 = this.f5260 != null ? this.f5260.m13201() : this.f5262.zzcw();
        }
        return r0;
    }
}
