package com.google.android.gms.internal;

import java.io.IOException;

public interface zzfhe extends zzfhg {
    /* renamed from: ʾ  reason: contains not printable characters */
    zzfhf m12622();

    /* renamed from: ٴ  reason: contains not printable characters */
    zzfes m12623();

    /* renamed from: ᐧ  reason: contains not printable characters */
    byte[] m12624();

    /* renamed from: 麤  reason: contains not printable characters */
    int m12625();

    /* renamed from: 龘  reason: contains not printable characters */
    void m12626(zzffg zzffg) throws IOException;
}
