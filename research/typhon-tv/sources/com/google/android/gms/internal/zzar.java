package com.google.android.gms.internal;

import com.mopub.common.TyphoonApp;
import com.mopub.volley.toolbox.HttpClientStack;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLSocketFactory;
import org.apache.oltu.oauth2.common.OAuth;

public final class zzar extends zzah {

    /* renamed from: 靐  reason: contains not printable characters */
    private final SSLSocketFactory f8387;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzas f8388;

    public zzar() {
        this((zzas) null);
    }

    private zzar(zzas zzas) {
        this((zzas) null, (SSLSocketFactory) null);
    }

    private zzar(zzas zzas, SSLSocketFactory sSLSocketFactory) {
        this.f8388 = null;
        this.f8387 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static InputStream m9732(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException e) {
            return httpURLConnection.getErrorStream();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<zzl> m9733(Map<String, List<String>> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            if (next.getKey() != null) {
                for (String zzl : (List) next.getValue()) {
                    arrayList.add(new zzl((String) next.getKey(), zzl));
                }
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m9734(HttpURLConnection httpURLConnection, zzr<?> zzr) throws IOException, zza {
        byte[] r1 = zzr.m13373();
        if (r1 != null) {
            httpURLConnection.setDoOutput(true);
            String valueOf = String.valueOf("UTF-8");
            httpURLConnection.addRequestProperty(OAuth.HeaderType.CONTENT_TYPE, valueOf.length() != 0 ? "application/x-www-form-urlencoded; charset=".concat(valueOf) : new String("application/x-www-form-urlencoded; charset="));
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(r1);
            dataOutputStream.close();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzap m9735(zzr<?> zzr, Map<String, String> map) throws IOException, zza {
        String str;
        String r1 = zzr.m13359();
        HashMap hashMap = new HashMap();
        hashMap.putAll(zzr.m13360());
        hashMap.putAll(map);
        if (this.f8388 != null) {
            str = this.f8388.m9736(r1);
            if (str == null) {
                String valueOf = String.valueOf(r1);
                throw new IOException(valueOf.length() != 0 ? "URL blocked by rewriter: ".concat(valueOf) : new String("URL blocked by rewriter: "));
            }
        } else {
            str = r1;
        }
        URL url = new URL(str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        int r2 = zzr.m13354();
        httpURLConnection.setConnectTimeout(r2);
        httpURLConnection.setReadTimeout(r2);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDoInput(true);
        TyphoonApp.HTTPS.equals(url.getProtocol());
        for (String str2 : hashMap.keySet()) {
            httpURLConnection.addRequestProperty(str2, (String) hashMap.get(str2));
        }
        switch (zzr.m13363()) {
            case -1:
                break;
            case 0:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.GET);
                break;
            case 1:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.POST);
                m9734(httpURLConnection, zzr);
                break;
            case 2:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.PUT);
                m9734(httpURLConnection, zzr);
                break;
            case 3:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.DELETE);
                break;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                break;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                break;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                break;
            case 7:
                httpURLConnection.setRequestMethod(HttpClientStack.HttpPatch.METHOD_NAME);
                m9734(httpURLConnection, zzr);
                break;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == -1) {
            throw new IOException("Could not retrieve response code from HttpUrlConnection.");
        }
        return !(zzr.m13363() != 4 && ((100 > responseCode || responseCode >= 200) && responseCode != 204 && responseCode != 304)) ? new zzap(responseCode, m9733((Map<String, List<String>>) httpURLConnection.getHeaderFields())) : new zzap(responseCode, m9733((Map<String, List<String>>) httpURLConnection.getHeaderFields()), httpURLConnection.getContentLength(), m9732(httpURLConnection));
    }
}
