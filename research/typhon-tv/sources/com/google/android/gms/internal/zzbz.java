package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

final class zzbz {

    /* renamed from: 龘  reason: contains not printable characters */
    static zzdpv f8815;

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m10308() {
        if (f8815 != null) {
            return true;
        }
        String str = (String) zzkb.m5481().m5595(zznh.f4971);
        if (str.length() == 0) {
            return false;
        }
        try {
            try {
                zzdpx r3 = zzdqb.m5231(zzbu.m10296(str, true));
                for (zzdtf next : zzdqs.f9946.m12163()) {
                    if (next.m12054().isEmpty()) {
                        throw new GeneralSecurityException("Missing type_url.");
                    } else if (next.m12058().isEmpty()) {
                        throw new GeneralSecurityException("Missing primitive_name.");
                    } else if (next.m12052().isEmpty()) {
                        throw new GeneralSecurityException("Missing catalogue_name.");
                    } else {
                        zzdqe.m11682(next.m12054(), zzdqe.m11674(next.m12052()).m11650(next.m12054(), next.m12058(), next.m12056()), next.m12053());
                    }
                }
                f8815 = zzdqt.m11744(r3);
                return f8815 != null;
            } catch (GeneralSecurityException e) {
                return false;
            }
        } catch (IllegalArgumentException e2) {
            return false;
        }
    }
}
