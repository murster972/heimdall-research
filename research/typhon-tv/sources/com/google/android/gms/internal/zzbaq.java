package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.R;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbaq extends UIController {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Drawable f8577;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f8578;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Drawable f8579;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f8580;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f8581;

    /* renamed from: 靐  reason: contains not printable characters */
    private final View f8582;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Drawable f8583;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f8584;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ImageView f8585;

    public zzbaq(ImageView imageView, Context context, Drawable drawable, Drawable drawable2, Drawable drawable3, View view, boolean z) {
        this.f8585 = imageView;
        this.f8583 = drawable;
        this.f8577 = drawable2;
        this.f8579 = drawable3 == null ? drawable2 : drawable3;
        this.f8581 = context.getString(R.string.cast_play);
        this.f8578 = context.getString(R.string.cast_pause);
        this.f8580 = context.getString(R.string.cast_stop);
        this.f8582 = view;
        this.f8584 = z;
        this.f8585.setEnabled(false);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9907() {
        RemoteMediaClient r0 = m8226();
        if (r0 == null || !r0.m4143()) {
            this.f8585.setEnabled(false);
        } else if (r0.m4138()) {
            m9908(this.f8583, this.f8581);
        } else if (r0.m4141()) {
            if (r0.m4147()) {
                m9908(this.f8579, this.f8580);
            } else {
                m9908(this.f8577, this.f8578);
            }
        } else if (r0.m4139()) {
            m9909(false);
        } else if (r0.m4169()) {
            m9909(true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9908(Drawable drawable, String str) {
        this.f8585.setImageDrawable(drawable);
        this.f8585.setContentDescription(str);
        this.f8585.setVisibility(0);
        this.f8585.setEnabled(true);
        if (this.f8582 != null) {
            this.f8582.setVisibility(8);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9909(boolean z) {
        boolean z2 = false;
        if (this.f8582 != null) {
            this.f8582.setVisibility(0);
        }
        this.f8585.setVisibility(this.f8584 ? 4 : 0);
        ImageView imageView = this.f8585;
        if (!z) {
            z2 = true;
        }
        imageView.setEnabled(z2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9910() {
        this.f8585.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9911() {
        m9909(true);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9912() {
        m9907();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9913(CastSession castSession) {
        super.m8227(castSession);
        m9907();
    }
}
