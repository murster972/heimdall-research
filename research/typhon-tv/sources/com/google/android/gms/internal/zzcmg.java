package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcmg extends zzfjm<zzcmg> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzcmg[] f9762;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Float f9763 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public Double f9764 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9765 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Long f9766 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f9767 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f9768 = null;

    public zzcmg() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzcmg[] m11507() {
        if (f9762 == null) {
            synchronized (zzfjq.f10546) {
                if (f9762 == null) {
                    f9762 = new zzcmg[0];
                }
            }
        }
        return f9762;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcmg)) {
            return false;
        }
        zzcmg zzcmg = (zzcmg) obj;
        if (this.f9768 == null) {
            if (zzcmg.f9768 != null) {
                return false;
            }
        } else if (!this.f9768.equals(zzcmg.f9768)) {
            return false;
        }
        if (this.f9765 == null) {
            if (zzcmg.f9765 != null) {
                return false;
            }
        } else if (!this.f9765.equals(zzcmg.f9765)) {
            return false;
        }
        if (this.f9767 == null) {
            if (zzcmg.f9767 != null) {
                return false;
            }
        } else if (!this.f9767.equals(zzcmg.f9767)) {
            return false;
        }
        if (this.f9766 == null) {
            if (zzcmg.f9766 != null) {
                return false;
            }
        } else if (!this.f9766.equals(zzcmg.f9766)) {
            return false;
        }
        if (this.f9763 == null) {
            if (zzcmg.f9763 != null) {
                return false;
            }
        } else if (!this.f9763.equals(zzcmg.f9763)) {
            return false;
        }
        if (this.f9764 == null) {
            if (zzcmg.f9764 != null) {
                return false;
            }
        } else if (!this.f9764.equals(zzcmg.f9764)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcmg.f10533 == null || zzcmg.f10533.m12849() : this.f10533.equals(zzcmg.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9764 == null ? 0 : this.f9764.hashCode()) + (((this.f9763 == null ? 0 : this.f9763.hashCode()) + (((this.f9766 == null ? 0 : this.f9766.hashCode()) + (((this.f9767 == null ? 0 : this.f9767.hashCode()) + (((this.f9765 == null ? 0 : this.f9765.hashCode()) + (((this.f9768 == null ? 0 : this.f9768.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11508() {
        int r0 = super.m12841();
        if (this.f9768 != null) {
            r0 += zzfjk.m12814(1, this.f9768.longValue());
        }
        if (this.f9765 != null) {
            r0 += zzfjk.m12808(2, this.f9765);
        }
        if (this.f9767 != null) {
            r0 += zzfjk.m12808(3, this.f9767);
        }
        if (this.f9766 != null) {
            r0 += zzfjk.m12814(4, this.f9766.longValue());
        }
        if (this.f9763 != null) {
            this.f9763.floatValue();
            r0 += zzfjk.m12805(5) + 4;
        }
        if (this.f9764 == null) {
            return r0;
        }
        this.f9764.doubleValue();
        return r0 + zzfjk.m12805(6) + 8;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11509(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9768 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 18:
                    this.f9765 = zzfjj.m12791();
                    continue;
                case 26:
                    this.f9767 = zzfjj.m12791();
                    continue;
                case 32:
                    this.f9766 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 45:
                    this.f9763 = Float.valueOf(Float.intBitsToFloat(zzfjj.m12788()));
                    continue;
                case 49:
                    this.f9764 = Double.valueOf(Double.longBitsToDouble(zzfjj.m12789()));
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11510(zzfjk zzfjk) throws IOException {
        if (this.f9768 != null) {
            zzfjk.m12824(1, this.f9768.longValue());
        }
        if (this.f9765 != null) {
            zzfjk.m12835(2, this.f9765);
        }
        if (this.f9767 != null) {
            zzfjk.m12835(3, this.f9767);
        }
        if (this.f9766 != null) {
            zzfjk.m12824(4, this.f9766.longValue());
        }
        if (this.f9763 != null) {
            zzfjk.m12831(5, this.f9763.floatValue());
        }
        if (this.f9764 != null) {
            zzfjk.m12830(6, this.f9764.doubleValue());
        }
        super.m12842(zzfjk);
    }
}
