package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzd;
import com.google.android.gms.ads.internal.js.zzc;

final class zzty implements zzaiq<zzc> {
    zzty() {
    }

    public final /* synthetic */ void zzf(Object obj) {
        zzc zzc = (zzc) obj;
        zzc.zza("/log", zzd.zzbxd);
        zzc.zza("/result", zzd.zzbxl);
    }
}
