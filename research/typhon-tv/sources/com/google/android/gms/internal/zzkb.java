package com.google.android.gms.internal;

@zzzv
public final class zzkb {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzkb f4811;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f4812 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zznd f4813 = new zznd();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzne f4814 = new zzne();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zznf f4815 = new zznf();

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f4816 = zzajr.m4755();

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzjr f4817 = new zzjr(new zzji(), new zzjh(), new zzmb(), new zzri(), new zzadt(), new zzxd(), new zzrj());

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzajr f4818 = new zzajr();

    static {
        zzkb zzkb = new zzkb();
        synchronized (f4812) {
            f4811 = zzkb;
        }
    }

    protected zzkb() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zznf m5481() {
        return m5482().f4815;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static zzkb m5482() {
        zzkb zzkb;
        synchronized (f4812) {
            zzkb = f4811;
        }
        return zzkb;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zznd m5483() {
        return m5482().f4813;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzjr m5484() {
        return m5482().f4817;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static zzne m5485() {
        return m5482().f4814;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m5486() {
        return m5482().f4816;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzajr m5487() {
        return m5482().f4818;
    }
}
