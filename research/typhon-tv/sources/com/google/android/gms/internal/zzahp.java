package com.google.android.gms.internal;

import android.content.Context;

final class zzahp implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahn f8212;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8213;

    zzahp(zzahn zzahn, Context context) {
        this.f8212 = zzahn;
        this.f8213 = context;
    }

    public final void run() {
        synchronized (this.f8212.f4217) {
            String unused = this.f8212.f4218 = zzahn.m4585(this.f8213);
            this.f8212.f4217.notifyAll();
        }
    }
}
