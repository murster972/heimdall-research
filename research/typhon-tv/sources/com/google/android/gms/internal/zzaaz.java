package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzaaz extends zzbfm {
    public static final Parcelable.Creator<zzaaz> CREATOR = new zzaba();

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<String> f3866;

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean f3867;

    public zzaaz() {
        this(false, Collections.emptyList());
    }

    public zzaaz(boolean z, List<String> list) {
        this.f3867 = z;
        this.f3866 = list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzaaz m4261(JSONObject jSONObject) {
        if (jSONObject == null) {
            return new zzaaz();
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("reporting_urls");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                try {
                    arrayList.add(optJSONArray.getString(i));
                } catch (JSONException e) {
                    zzagf.m4796("Error grabbing url from json.", e);
                }
            }
        }
        return new zzaaz(jSONObject.optBoolean("enable_protection"), arrayList);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 2, this.f3867);
        zzbfp.m10178(parcel, 3, this.f3866, false);
        zzbfp.m10182(parcel, r0);
    }
}
