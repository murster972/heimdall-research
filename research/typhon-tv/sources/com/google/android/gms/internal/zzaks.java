package com.google.android.gms.internal;

import java.util.concurrent.ExecutionException;

final /* synthetic */ class zzaks implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzakv f8284;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzalf f8285;

    zzaks(zzalf zzalf, zzakv zzakv) {
        this.f8285 = zzalf;
        this.f8284 = zzakv;
    }

    public final void run() {
        zzalf zzalf = this.f8285;
        try {
            zzalf.m4822(this.f8284.get());
        } catch (ExecutionException e) {
            zzalf.m4824(e.getCause());
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            zzalf.m4824(e2);
        } catch (Exception e3) {
            zzalf.m4824(e3);
        }
    }
}
