package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.Provider;

public final class zzduy implements zzduv<KeyFactory> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m12221(String str, Provider provider) throws GeneralSecurityException {
        return provider == null ? KeyFactory.getInstance(str) : KeyFactory.getInstance(str, provider);
    }
}
