package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdtn extends zzffu<zzdtn, zza> implements zzfhg {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzfhk<zzdtn> f10148;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final zzdtn f10149;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f10150 = "";

    public static final class zza extends zzffu.zza<zzdtn, zza> implements zzfhg {
        private zza() {
            super(zzdtn.f10149);
        }

        /* synthetic */ zza(zzdto zzdto) {
            this();
        }
    }

    static {
        zzdtn zzdtn = new zzdtn();
        f10149 = zzdtn;
        zzdtn.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdtn.f10394.m12718();
    }

    private zzdtn() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzdtn m12126() {
        return f10149;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdtn m12128(zzfes zzfes) throws zzfge {
        return (zzdtn) zzffu.m12526(f10149, zzfes);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12129() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.f10150.isEmpty()) {
            i2 = zzffg.m5248(1, this.f10150) + 0;
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 161 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m12130(int r6, java.lang.Object r7, java.lang.Object r8) {
        /*
            r5 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdto.f10151
            int r4 = r6 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x0047;
                case 7: goto L_0x008d;
                case 8: goto L_0x0090;
                case 9: goto L_0x00ac;
                case 10: goto L_0x00b2;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdtn r5 = new com.google.android.gms.internal.zzdtn
            r5.<init>()
        L_0x0017:
            return r5
        L_0x0018:
            com.google.android.gms.internal.zzdtn r5 = f10149
            goto L_0x0017
        L_0x001b:
            r5 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdtn$zza r5 = new com.google.android.gms.internal.zzdtn$zza
            r5.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r7 = (com.google.android.gms.internal.zzffu.zzh) r7
            com.google.android.gms.internal.zzdtn r8 = (com.google.android.gms.internal.zzdtn) r8
            java.lang.String r0 = r5.f10150
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0043
            r0 = r1
        L_0x0030:
            java.lang.String r3 = r5.f10150
            java.lang.String r4 = r8.f10150
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x0045
        L_0x003a:
            java.lang.String r2 = r8.f10150
            java.lang.String r0 = r7.m12574((boolean) r0, (java.lang.String) r3, (boolean) r1, (java.lang.String) r2)
            r5.f10150 = r0
            goto L_0x0017
        L_0x0043:
            r0 = r2
            goto L_0x0030
        L_0x0045:
            r1 = r2
            goto L_0x003a
        L_0x0047:
            com.google.android.gms.internal.zzffb r7 = (com.google.android.gms.internal.zzffb) r7
            com.google.android.gms.internal.zzffm r8 = (com.google.android.gms.internal.zzffm) r8
            if (r8 != 0) goto L_0x0054
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0053:
            r2 = r1
        L_0x0054:
            if (r2 != 0) goto L_0x008d
            int r0 = r7.m12415()     // Catch:{ zzfge -> 0x006c, IOException -> 0x0079 }
            switch(r0) {
                case 0: goto L_0x0053;
                case 10: goto L_0x0065;
                default: goto L_0x005d;
            }     // Catch:{ zzfge -> 0x006c, IOException -> 0x0079 }
        L_0x005d:
            boolean r0 = r5.m12537((int) r0, (com.google.android.gms.internal.zzffb) r7)     // Catch:{ zzfge -> 0x006c, IOException -> 0x0079 }
            if (r0 != 0) goto L_0x0054
            r2 = r1
            goto L_0x0054
        L_0x0065:
            java.lang.String r0 = r7.m12400()     // Catch:{ zzfge -> 0x006c, IOException -> 0x0079 }
            r5.f10150 = r0     // Catch:{ zzfge -> 0x006c, IOException -> 0x0079 }
            goto L_0x0054
        L_0x006c:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0077 }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r5)     // Catch:{ all -> 0x0077 }
            r1.<init>(r0)     // Catch:{ all -> 0x0077 }
            throw r1     // Catch:{ all -> 0x0077 }
        L_0x0077:
            r0 = move-exception
            throw r0
        L_0x0079:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0077 }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x0077 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0077 }
            r2.<init>(r0)     // Catch:{ all -> 0x0077 }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r5)     // Catch:{ all -> 0x0077 }
            r1.<init>(r0)     // Catch:{ all -> 0x0077 }
            throw r1     // Catch:{ all -> 0x0077 }
        L_0x008d:
            com.google.android.gms.internal.zzdtn r5 = f10149
            goto L_0x0017
        L_0x0090:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtn> r0 = f10148
            if (r0 != 0) goto L_0x00a5
            java.lang.Class<com.google.android.gms.internal.zzdtn> r1 = com.google.android.gms.internal.zzdtn.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtn> r0 = f10148     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x00a4
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00a9 }
            com.google.android.gms.internal.zzdtn r2 = f10149     // Catch:{ all -> 0x00a9 }
            r0.<init>(r2)     // Catch:{ all -> 0x00a9 }
            f10148 = r0     // Catch:{ all -> 0x00a9 }
        L_0x00a4:
            monitor-exit(r1)     // Catch:{ all -> 0x00a9 }
        L_0x00a5:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdtn> r5 = f10148
            goto L_0x0017
        L_0x00a9:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00a9 }
            throw r0
        L_0x00ac:
            java.lang.Byte r5 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00b2:
            r5 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdtn.m12130(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m12131() {
        return this.f10150;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12132(zzffg zzffg) throws IOException {
        if (!this.f10150.isEmpty()) {
            zzffg.m5290(1, this.f10150);
        }
        this.f10394.m12719(zzffg);
    }
}
