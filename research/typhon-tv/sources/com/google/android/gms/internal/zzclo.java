package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzclo implements Parcelable.Creator<zzcln> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        int i = 0;
        long j = 0;
        Double d = null;
        String str = null;
        String str2 = null;
        Float f = null;
        Long l = null;
        String str3 = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    l = zzbfn.m10150(parcel, readInt);
                    break;
                case 5:
                    f = zzbfn.m10152(parcel, readInt);
                    break;
                case 6:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    d = zzbfn.m10161(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzcln(i, str3, j, l, f, str2, str, d);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcln[i];
    }
}
