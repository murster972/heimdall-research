package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

public final class zzqg extends zzeu implements zzqe {
    zzqg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAppInstallAd");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final double m13268() throws RemoteException {
        Parcel r0 = m12300(8, v_());
        double readDouble = r0.readDouble();
        r0.recycle();
        return readDouble;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m13269() throws RemoteException {
        Parcel r0 = m12300(9, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m13270() throws RemoteException {
        Parcel r0 = m12300(10, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Bundle m13271() throws RemoteException {
        Parcel r1 = m12300(11, v_());
        Bundle bundle = (Bundle) zzew.m12304(r1, Bundle.CREATOR);
        r1.recycle();
        return bundle;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m13272() throws RemoteException {
        Parcel r0 = m12300(19, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzpm m13273() throws RemoteException {
        zzpm zzpo;
        Parcel r1 = m12300(17, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzpo = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
            zzpo = queryLocalInterface instanceof zzpm ? (zzpm) queryLocalInterface : new zzpo(readStrongBinder);
        }
        r1.recycle();
        return zzpo;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m13274() throws RemoteException {
        m12298(12, v_());
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzll m13275() throws RemoteException {
        Parcel r0 = m12300(13, v_());
        zzll r1 = zzlm.m13094(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final IObjectWrapper m13276() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m13277() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m13278() throws RemoteException {
        Parcel r0 = m12300(4, v_());
        ArrayList r1 = zzew.m12301(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13279(Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) bundle);
        Parcel r0 = m12300(15, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzpq m13280() throws RemoteException {
        zzpq zzps;
        Parcel r1 = m12300(6, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzps = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
            zzps = queryLocalInterface instanceof zzpq ? (zzpq) queryLocalInterface : new zzps(readStrongBinder);
        }
        r1.recycle();
        return zzps;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m13281() throws RemoteException {
        Parcel r0 = m12300(5, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13282(Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(16, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13283() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13284(Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(14, v_);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final IObjectWrapper m13285() throws RemoteException {
        Parcel r0 = m12300(18, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
