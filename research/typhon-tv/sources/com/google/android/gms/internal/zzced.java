package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzcm;
import com.google.android.gms.location.LocationCallback;

final class zzced extends zzcem {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationCallback f9052;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzced(zzceb zzceb, GoogleApiClient googleApiClient, LocationCallback locationCallback) {
        super(googleApiClient);
        this.f9052 = locationCallback;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10340(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10404(zzcm.m8859(this.f9052, LocationCallback.class.getSimpleName()), new zzcen(this));
    }
}
