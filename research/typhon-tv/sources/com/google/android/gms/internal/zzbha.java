package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

public final class zzbha implements Executor {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Handler f8746;

    public zzbha(Looper looper) {
        this.f8746 = new Handler(looper);
    }

    public final void execute(Runnable runnable) {
        this.f8746.post(runnable);
    }
}
