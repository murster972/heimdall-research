package com.google.android.gms.internal;

import android.support.v4.view.animation.PathInterpolatorCompat;
import android.view.animation.Interpolator;

final class zzdms {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Interpolator f9905 = PathInterpolatorCompat.create(0.4f, 0.0f, 1.0f, 1.0f);
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Interpolator f9906 = PathInterpolatorCompat.create(0.4f, 0.0f, 0.2f, 1.0f);
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Interpolator f9907 = PathInterpolatorCompat.create(0.0f, 0.0f, 0.2f, 1.0f);
}
