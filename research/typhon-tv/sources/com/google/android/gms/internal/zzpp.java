package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeAd;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzpp extends NativeAd.AdChoicesInfo {

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<NativeAd.Image> f5312 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    private String f5313;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzpm f5314;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: com.google.android.gms.internal.zzps} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: com.google.android.gms.internal.zzps} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: com.google.android.gms.internal.zzps} */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzpp(com.google.android.gms.internal.zzpm r6) {
        /*
            r5 = this;
            r5.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.f5312 = r1
            r5.f5314 = r6
            com.google.android.gms.internal.zzpm r1 = r5.f5314     // Catch:{ RemoteException -> 0x0052 }
            java.lang.String r1 = r1.m13219()     // Catch:{ RemoteException -> 0x0052 }
            r5.f5313 = r1     // Catch:{ RemoteException -> 0x0052 }
        L_0x0014:
            java.util.List r1 = r6.m13218()     // Catch:{ RemoteException -> 0x004a }
            java.util.Iterator r3 = r1.iterator()     // Catch:{ RemoteException -> 0x004a }
        L_0x001c:
            boolean r1 = r3.hasNext()     // Catch:{ RemoteException -> 0x004a }
            if (r1 == 0) goto L_0x0051
            java.lang.Object r1 = r3.next()     // Catch:{ RemoteException -> 0x004a }
            boolean r2 = r1 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x004a }
            if (r2 == 0) goto L_0x0066
            android.os.IBinder r1 = (android.os.IBinder) r1     // Catch:{ RemoteException -> 0x004a }
            if (r1 == 0) goto L_0x0066
            java.lang.String r2 = "com.google.android.gms.ads.internal.formats.client.INativeAdImage"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)     // Catch:{ RemoteException -> 0x004a }
            boolean r4 = r2 instanceof com.google.android.gms.internal.zzpq     // Catch:{ RemoteException -> 0x004a }
            if (r4 == 0) goto L_0x005f
            r0 = r2
            com.google.android.gms.internal.zzpq r0 = (com.google.android.gms.internal.zzpq) r0     // Catch:{ RemoteException -> 0x004a }
            r1 = r0
        L_0x003d:
            if (r1 == 0) goto L_0x001c
            java.util.List<com.google.android.gms.ads.formats.NativeAd$Image> r2 = r5.f5312     // Catch:{ RemoteException -> 0x004a }
            com.google.android.gms.internal.zzpt r4 = new com.google.android.gms.internal.zzpt     // Catch:{ RemoteException -> 0x004a }
            r4.<init>(r1)     // Catch:{ RemoteException -> 0x004a }
            r2.add(r4)     // Catch:{ RemoteException -> 0x004a }
            goto L_0x001c
        L_0x004a:
            r1 = move-exception
            java.lang.String r2 = "Error while obtaining image."
            com.google.android.gms.internal.zzakb.m4793(r2, r1)
        L_0x0051:
            return
        L_0x0052:
            r1 = move-exception
            java.lang.String r2 = "Error while obtaining attribution text."
            com.google.android.gms.internal.zzakb.m4793(r2, r1)
            java.lang.String r1 = ""
            r5.f5313 = r1
            goto L_0x0014
        L_0x005f:
            com.google.android.gms.internal.zzps r2 = new com.google.android.gms.internal.zzps     // Catch:{ RemoteException -> 0x004a }
            r2.<init>(r1)     // Catch:{ RemoteException -> 0x004a }
            r1 = r2
            goto L_0x003d
        L_0x0066:
            r1 = 0
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpp.<init>(com.google.android.gms.internal.zzpm):void");
    }

    public final List<NativeAd.Image> getImages() {
        return this.f5312;
    }

    public final CharSequence getText() {
        return this.f5313;
    }
}
