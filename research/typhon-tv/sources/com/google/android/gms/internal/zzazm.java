package com.google.android.gms.internal;

import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class zzazm extends zzaza {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<MediaRouteSelector, Set<MediaRouter.Callback>> f8501 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private final MediaRouter f8502;

    public zzazm(MediaRouter mediaRouter) {
        this.f8502 = mediaRouter;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Bundle m9823(String str) {
        for (MediaRouter.RouteInfo next : this.f8502.m1114()) {
            if (next.m1222().equals(str)) {
                return next.m1210();
            }
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9824() {
        return this.f8502.m1113().m1222().equals(this.f8502.m1111().m1222());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9825(Bundle bundle, int i) {
        return this.f8502.m1121(MediaRouteSelector.m1095(bundle), i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m9826() {
        return this.f8502.m1113().m1222();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9827() {
        this.f8502.m1120(this.f8502.m1111());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9828(Bundle bundle) {
        for (MediaRouter.Callback r0 : this.f8501.get(MediaRouteSelector.m1095(bundle))) {
            this.f8502.m1119(r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9829(Bundle bundle, int i) {
        MediaRouteSelector r1 = MediaRouteSelector.m1095(bundle);
        for (MediaRouter.Callback r0 : this.f8501.get(r1)) {
            this.f8502.m1118(r1, r0, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9830(Bundle bundle, zzazb zzazb) {
        MediaRouteSelector r0 = MediaRouteSelector.m1095(bundle);
        if (!this.f8501.containsKey(r0)) {
            this.f8501.put(r0, new HashSet());
        }
        this.f8501.get(r0).add(new zzazl(zzazb));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9831(MediaSessionCompat mediaSessionCompat) {
        this.f8502.m1116(mediaSessionCompat);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9832(String str) {
        for (MediaRouter.RouteInfo next : this.f8502.m1114()) {
            if (next.m1222().equals(str)) {
                this.f8502.m1120(next);
                return;
            }
        }
    }
}
