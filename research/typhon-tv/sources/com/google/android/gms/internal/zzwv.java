package com.google.android.gms.internal;

import android.app.DownloadManager;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Environment;
import com.google.android.gms.ads.internal.zzbs;

final class zzwv implements DialogInterface.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f10954;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzwu f10955;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f10956;

    zzwv(zzwu zzwu, String str, String str2) {
        this.f10955 = zzwu;
        this.f10956 = str;
        this.f10954 = str2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        DownloadManager downloadManager = (DownloadManager) this.f10955.f5535.getSystemService("download");
        try {
            String str = this.f10956;
            String str2 = this.f10954;
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, str2);
            zzbs.zzek().m4661(request);
            downloadManager.enqueue(request);
        } catch (IllegalStateException e) {
            this.f10955.m6012("Could not store picture.");
        }
    }
}
