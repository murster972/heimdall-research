package com.google.android.gms.internal;

import java.io.IOException;

public final class zzax extends zzfjm<zzax> {

    /* renamed from: 连任  reason: contains not printable characters */
    private String f8403;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f8404;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f8405;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f8406;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f8407;

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9756() {
        int r0 = super.m12841();
        if (this.f8407 != null) {
            r0 += zzfjk.m12808(1, this.f8407);
        }
        if (this.f8404 != null) {
            r0 += zzfjk.m12808(2, this.f8404);
        }
        if (this.f8406 != null) {
            r0 += zzfjk.m12808(3, this.f8406);
        }
        if (this.f8405 != null) {
            r0 += zzfjk.m12808(4, this.f8405);
        }
        return this.f8403 != null ? r0 + zzfjk.m12808(5, this.f8403) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m9757(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f8407 = zzfjj.m12791();
                    continue;
                case 18:
                    this.f8404 = zzfjj.m12791();
                    continue;
                case 26:
                    this.f8406 = zzfjj.m12791();
                    continue;
                case 34:
                    this.f8405 = zzfjj.m12791();
                    continue;
                case 42:
                    this.f8403 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9758(zzfjk zzfjk) throws IOException {
        if (this.f8407 != null) {
            zzfjk.m12835(1, this.f8407);
        }
        if (this.f8404 != null) {
            zzfjk.m12835(2, this.f8404);
        }
        if (this.f8406 != null) {
            zzfjk.m12835(3, this.f8406);
        }
        if (this.f8405 != null) {
            zzfjk.m12835(4, this.f8405);
        }
        if (this.f8403 != null) {
            zzfjk.m12835(5, this.f8403);
        }
        super.m12842(zzfjk);
    }
}
