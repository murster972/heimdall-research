package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;
import com.google.android.gms.ads.internal.zzbs;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.CountDownLatch;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

@zzzv
@TargetApi(14)
public final class zzamn extends Thread implements SurfaceTexture.OnFrameAvailableListener, zzamm {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final float[] f4371 = {-1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final float[] f4372;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final float[] f4373;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final float[] f4374;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f4375;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f4376;

    /* renamed from: ˆ  reason: contains not printable characters */
    private FloatBuffer f4377 = ByteBuffer.allocateDirect(f4371.length << 2).order(ByteOrder.nativeOrder()).asFloatBuffer();

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f4378;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final CountDownLatch f4379;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f4380;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f4381;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f4382;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final Object f4383;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final float[] f4384;

    /* renamed from: י  reason: contains not printable characters */
    private EGL10 f4385;

    /* renamed from: ـ  reason: contains not printable characters */
    private EGLDisplay f4386;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f4387;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f4388;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private EGLContext f4389;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private EGLSurface f4390;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private volatile boolean f4391;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private volatile boolean f4392;

    /* renamed from: 连任  reason: contains not printable characters */
    private final float[] f4393;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzamk f4394;

    /* renamed from: 麤  reason: contains not printable characters */
    private final float[] f4395;

    /* renamed from: 齉  reason: contains not printable characters */
    private final float[] f4396;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private SurfaceTexture f4397;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private SurfaceTexture f4398;

    public zzamn(Context context) {
        super("SphericalVideoProcessor");
        this.f4377.put(f4371).position(0);
        this.f4396 = new float[9];
        this.f4395 = new float[9];
        this.f4393 = new float[9];
        this.f4372 = new float[9];
        this.f4373 = new float[9];
        this.f4374 = new float[9];
        this.f4384 = new float[9];
        this.f4387 = Float.NaN;
        this.f4394 = new zzamk(context);
        this.f4394.m4902((zzamm) this);
        this.f4379 = new CountDownLatch(1);
        this.f4383 = new Object();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean m4904() {
        boolean z = false;
        if (!(this.f4390 == null || this.f4390 == EGL10.EGL_NO_SURFACE)) {
            z = this.f4385.eglMakeCurrent(this.f4386, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT) | false | this.f4385.eglDestroySurface(this.f4386, this.f4390);
            this.f4390 = null;
        }
        if (this.f4389 != null) {
            z |= this.f4385.eglDestroyContext(this.f4386, this.f4389);
            this.f4389 = null;
        }
        if (this.f4386 == null) {
            return z;
        }
        boolean eglTerminate = z | this.f4385.eglTerminate(this.f4386);
        this.f4386 = null;
        return eglTerminate;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m4905(float[] fArr, float f) {
        fArr[0] = (float) Math.cos((double) f);
        fArr[1] = (float) (-Math.sin((double) f));
        fArr[2] = 0.0f;
        fArr[3] = (float) Math.sin((double) f);
        fArr[4] = (float) Math.cos((double) f);
        fArr[5] = 0.0f;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 1.0f;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m4906() {
        while (this.f4382 > 0) {
            this.f4397.updateTexImage();
            this.f4382--;
        }
        if (this.f4394.m4903(this.f4396)) {
            if (Float.isNaN(this.f4387)) {
                float[] fArr = this.f4396;
                float[] fArr2 = {0.0f, 1.0f, 0.0f};
                float[] fArr3 = {(fArr[0] * fArr2[0]) + (fArr[1] * fArr2[1]) + (fArr[2] * fArr2[2]), (fArr[3] * fArr2[0]) + (fArr[4] * fArr2[1]) + (fArr[5] * fArr2[2]), (fArr[8] * fArr2[2]) + (fArr[6] * fArr2[0]) + (fArr[7] * fArr2[1])};
                this.f4387 = -(((float) Math.atan2((double) fArr3[1], (double) fArr3[0])) - 1.5707964f);
            }
            m4905(this.f4374, this.f4387 + this.f4388);
        } else {
            m4909(this.f4396, -1.5707964f);
            m4905(this.f4374, this.f4388);
        }
        m4909(this.f4395, 1.5707964f);
        m4910(this.f4393, this.f4374, this.f4395);
        m4910(this.f4372, this.f4396, this.f4393);
        m4909(this.f4373, this.f4378);
        m4910(this.f4384, this.f4373, this.f4372);
        GLES20.glUniformMatrix3fv(this.f4381, 1, false, this.f4384, 0);
        GLES20.glDrawArrays(5, 0, 4);
        m4908("drawArrays");
        GLES20.glFinish();
        this.f4385.eglSwapBuffers(this.f4386, this.f4390);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m4907(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        m4908("createShader");
        if (glCreateShader != 0) {
            GLES20.glShaderSource(glCreateShader, str);
            m4908("shaderSource");
            GLES20.glCompileShader(glCreateShader);
            m4908("compileShader");
            int[] iArr = new int[1];
            GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
            m4908("getShaderiv");
            if (iArr[0] == 0) {
                Log.e("SphericalVideoRenderer", new StringBuilder(37).append("Could not compile shader ").append(i).append(":").toString());
                Log.e("SphericalVideoRenderer", GLES20.glGetShaderInfoLog(glCreateShader));
                GLES20.glDeleteShader(glCreateShader);
                m4908("deleteShader");
                return 0;
            }
        }
        return glCreateShader;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4908(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            Log.e("SphericalVideoRenderer", new StringBuilder(String.valueOf(str).length() + 21).append(str).append(": glError ").append(glGetError).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4909(float[] fArr, float f) {
        fArr[0] = 1.0f;
        fArr[1] = 0.0f;
        fArr[2] = 0.0f;
        fArr[3] = 0.0f;
        fArr[4] = (float) Math.cos((double) f);
        fArr[5] = (float) (-Math.sin((double) f));
        fArr[6] = 0.0f;
        fArr[7] = (float) Math.sin((double) f);
        fArr[8] = (float) Math.cos((double) f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4910(float[] fArr, float[] fArr2, float[] fArr3) {
        fArr[0] = (fArr2[0] * fArr3[0]) + (fArr2[1] * fArr3[3]) + (fArr2[2] * fArr3[6]);
        fArr[1] = (fArr2[0] * fArr3[1]) + (fArr2[1] * fArr3[4]) + (fArr2[2] * fArr3[7]);
        fArr[2] = (fArr2[0] * fArr3[2]) + (fArr2[1] * fArr3[5]) + (fArr2[2] * fArr3[8]);
        fArr[3] = (fArr2[3] * fArr3[0]) + (fArr2[4] * fArr3[3]) + (fArr2[5] * fArr3[6]);
        fArr[4] = (fArr2[3] * fArr3[1]) + (fArr2[4] * fArr3[4]) + (fArr2[5] * fArr3[7]);
        fArr[5] = (fArr2[3] * fArr3[2]) + (fArr2[4] * fArr3[5]) + (fArr2[5] * fArr3[8]);
        fArr[6] = (fArr2[6] * fArr3[0]) + (fArr2[7] * fArr3[3]) + (fArr2[8] * fArr3[6]);
        fArr[7] = (fArr2[6] * fArr3[1]) + (fArr2[7] * fArr3[4]) + (fArr2[8] * fArr3[7]);
        fArr[8] = (fArr2[6] * fArr3[2]) + (fArr2[7] * fArr3[5]) + (fArr2[8] * fArr3[8]);
    }

    public final void onFrameAvailable(SurfaceTexture surfaceTexture) {
        this.f4382++;
        synchronized (this.f4383) {
            this.f4383.notifyAll();
        }
    }

    public final void run() {
        boolean z;
        int glCreateProgram;
        boolean z2 = true;
        if (this.f4398 == null) {
            zzagf.m4795("SphericalVideoProcessor started with no output texture.");
            this.f4379.countDown();
            return;
        }
        this.f4385 = (EGL10) EGLContext.getEGL();
        this.f4386 = this.f4385.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (this.f4386 == EGL10.EGL_NO_DISPLAY) {
            z = false;
        } else {
            if (!this.f4385.eglInitialize(this.f4386, new int[2])) {
                z = false;
            } else {
                int[] iArr = new int[1];
                EGLConfig[] eGLConfigArr = new EGLConfig[1];
                EGLConfig eGLConfig = (!this.f4385.eglChooseConfig(this.f4386, new int[]{12352, 4, 12324, 8, 12323, 8, 12322, 8, 12325, 16, 12344}, eGLConfigArr, 1, iArr) || iArr[0] <= 0) ? null : eGLConfigArr[0];
                if (eGLConfig == null) {
                    z = false;
                } else {
                    this.f4389 = this.f4385.eglCreateContext(this.f4386, eGLConfig, EGL10.EGL_NO_CONTEXT, new int[]{12440, 2, 12344});
                    if (this.f4389 == null || this.f4389 == EGL10.EGL_NO_CONTEXT) {
                        z = false;
                    } else {
                        this.f4390 = this.f4385.eglCreateWindowSurface(this.f4386, eGLConfig, this.f4398, (int[]) null);
                        z = (this.f4390 == null || this.f4390 == EGL10.EGL_NO_SURFACE) ? false : this.f4385.eglMakeCurrent(this.f4386, this.f4390, this.f4390, this.f4389);
                    }
                }
            }
        }
        zzmx zzmx = zznh.f4940;
        int r2 = m4907(35633, !((String) zzkb.m5481().m5595(zzmx)).equals(zzmx.m5582()) ? (String) zzkb.m5481().m5595(zzmx) : "attribute highp vec3 aPosition;varying vec3 pos;void main() {  gl_Position = vec4(aPosition, 1.0);  pos = aPosition;}");
        if (r2 == 0) {
            glCreateProgram = 0;
        } else {
            zzmx zzmx2 = zznh.f4941;
            int r3 = m4907(35632, !((String) zzkb.m5481().m5595(zzmx2)).equals(zzmx2.m5582()) ? (String) zzkb.m5481().m5595(zzmx2) : "#extension GL_OES_EGL_image_external : require\n#define INV_PI 0.3183\nprecision highp float;varying vec3 pos;uniform samplerExternalOES uSplr;uniform mat3 uVMat;uniform float uFOVx;uniform float uFOVy;void main() {  vec3 ray = vec3(pos.x * tan(uFOVx), pos.y * tan(uFOVy), -1);  ray = (uVMat * ray).xyz;  ray = normalize(ray);  vec2 texCrd = vec2(    0.5 + atan(ray.x, - ray.z) * INV_PI * 0.5, acos(ray.y) * INV_PI);  gl_FragColor = vec4(texture2D(uSplr, texCrd).xyz, 1.0);}");
            if (r3 == 0) {
                glCreateProgram = 0;
            } else {
                glCreateProgram = GLES20.glCreateProgram();
                m4908("createProgram");
                if (glCreateProgram != 0) {
                    GLES20.glAttachShader(glCreateProgram, r2);
                    m4908("attachShader");
                    GLES20.glAttachShader(glCreateProgram, r3);
                    m4908("attachShader");
                    GLES20.glLinkProgram(glCreateProgram);
                    m4908("linkProgram");
                    int[] iArr2 = new int[1];
                    GLES20.glGetProgramiv(glCreateProgram, 35714, iArr2, 0);
                    m4908("getProgramiv");
                    if (iArr2[0] != 1) {
                        Log.e("SphericalVideoRenderer", "Could not link program: ");
                        Log.e("SphericalVideoRenderer", GLES20.glGetProgramInfoLog(glCreateProgram));
                        GLES20.glDeleteProgram(glCreateProgram);
                        m4908("deleteProgram");
                        glCreateProgram = 0;
                    } else {
                        GLES20.glValidateProgram(glCreateProgram);
                        m4908("validateProgram");
                    }
                }
            }
        }
        this.f4380 = glCreateProgram;
        GLES20.glUseProgram(this.f4380);
        m4908("useProgram");
        int glGetAttribLocation = GLES20.glGetAttribLocation(this.f4380, "aPosition");
        GLES20.glVertexAttribPointer(glGetAttribLocation, 3, 5126, false, 12, this.f4377);
        m4908("vertexAttribPointer");
        GLES20.glEnableVertexAttribArray(glGetAttribLocation);
        m4908("enableVertexAttribArray");
        int[] iArr3 = new int[1];
        GLES20.glGenTextures(1, iArr3, 0);
        m4908("genTextures");
        int i = iArr3[0];
        GLES20.glBindTexture(36197, i);
        m4908("bindTextures");
        GLES20.glTexParameteri(36197, 10240, 9729);
        m4908("texParameteri");
        GLES20.glTexParameteri(36197, 10241, 9729);
        m4908("texParameteri");
        GLES20.glTexParameteri(36197, 10242, 33071);
        m4908("texParameteri");
        GLES20.glTexParameteri(36197, 10243, 33071);
        m4908("texParameteri");
        this.f4381 = GLES20.glGetUniformLocation(this.f4380, "uVMat");
        GLES20.glUniformMatrix3fv(this.f4381, 1, false, new float[]{1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f}, 0);
        if (this.f4380 == 0) {
            z2 = false;
        }
        if (!z || !z2) {
            String valueOf = String.valueOf(GLUtils.getEGLErrorString(this.f4385.eglGetError()));
            String concat = valueOf.length() != 0 ? "EGL initialization failed: ".concat(valueOf) : new String("EGL initialization failed: ");
            zzagf.m4795(concat);
            zzbs.zzem().m4505(new Throwable(concat), "SphericalVideoProcessor.run.1");
            m4904();
            this.f4379.countDown();
            return;
        }
        this.f4397 = new SurfaceTexture(i);
        this.f4397.setOnFrameAvailableListener(this);
        this.f4379.countDown();
        this.f4394.m4901();
        try {
            this.f4391 = true;
            while (!this.f4392) {
                m4906();
                if (this.f4391) {
                    GLES20.glViewport(0, 0, this.f4376, this.f4375);
                    m4908("viewport");
                    int glGetUniformLocation = GLES20.glGetUniformLocation(this.f4380, "uFOVx");
                    int glGetUniformLocation2 = GLES20.glGetUniformLocation(this.f4380, "uFOVy");
                    if (this.f4376 > this.f4375) {
                        GLES20.glUniform1f(glGetUniformLocation, 0.87266463f);
                        GLES20.glUniform1f(glGetUniformLocation2, (((float) this.f4375) * 0.87266463f) / ((float) this.f4376));
                    } else {
                        GLES20.glUniform1f(glGetUniformLocation, (((float) this.f4376) * 0.87266463f) / ((float) this.f4375));
                        GLES20.glUniform1f(glGetUniformLocation2, 0.87266463f);
                    }
                    this.f4391 = false;
                }
                try {
                    synchronized (this.f4383) {
                        if (!this.f4392 && !this.f4391 && this.f4382 == 0) {
                            this.f4383.wait();
                        }
                    }
                } catch (InterruptedException e) {
                }
            }
        } catch (IllegalStateException e2) {
            zzagf.m4791("SphericalVideoProcessor halted unexpectedly.");
        } catch (Throwable th) {
            zzagf.m4793("SphericalVideoProcessor died.", th);
            zzbs.zzem().m4505(th, "SphericalVideoProcessor.run.2");
        } finally {
            this.f4394.m4900();
            this.f4397.setOnFrameAvailableListener((SurfaceTexture.OnFrameAvailableListener) null);
            this.f4397 = null;
            m4904();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4911() {
        synchronized (this.f4383) {
            this.f4392 = true;
            this.f4398 = null;
            this.f4383.notifyAll();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final SurfaceTexture m4912() {
        if (this.f4398 == null) {
            return null;
        }
        try {
            this.f4379.await();
        } catch (InterruptedException e) {
        }
        return this.f4397;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4913() {
        synchronized (this.f4383) {
            this.f4383.notifyAll();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4914(float f, float f2) {
        float f3;
        float f4;
        if (this.f4376 > this.f4375) {
            f3 = (1.7453293f * f) / ((float) this.f4376);
            f4 = (1.7453293f * f2) / ((float) this.f4376);
        } else {
            f3 = (1.7453293f * f) / ((float) this.f4375);
            f4 = (1.7453293f * f2) / ((float) this.f4375);
        }
        this.f4388 -= f3;
        this.f4378 -= f4;
        if (this.f4378 < -1.5707964f) {
            this.f4378 = -1.5707964f;
        }
        if (this.f4378 > 1.5707964f) {
            this.f4378 = 1.5707964f;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4915(int i, int i2) {
        synchronized (this.f4383) {
            this.f4376 = i;
            this.f4375 = i2;
            this.f4391 = true;
            this.f4383.notifyAll();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4916(SurfaceTexture surfaceTexture, int i, int i2) {
        this.f4376 = i;
        this.f4375 = i2;
        this.f4398 = surfaceTexture;
    }
}
