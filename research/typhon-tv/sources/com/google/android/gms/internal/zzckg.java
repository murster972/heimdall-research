package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.stats.zza;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.zzf;
import com.google.android.gms.measurement.AppMeasurement$zzb;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public final class zzckg extends zzcjl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<Runnable> f9555 = new ArrayList();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzcgs f9556;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzclk f9557;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public zzche f9558;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzcgs f9559;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile Boolean f9560;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcku f9561;

    protected zzckg(zzcim zzcim) {
        super(zzcim);
        this.f9557 = new zzclk(zzcim.m11023());
        this.f9561 = new zzcku(this);
        this.f9559 = new zzckh(this, zzcim);
        this.f9556 = new zzckm(this, zzcim);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final void m11222() {
        m11109();
        m11096().m10848().m10850("Processing queued up service tasks", Integer.valueOf(this.f9555.size()));
        for (Runnable run : this.f9555) {
            try {
                run.run();
            } catch (Throwable th) {
                m11096().m10832().m10850("Task exception while flushing queue", th);
            }
        }
        this.f9555.clear();
        this.f9556.m10624();
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final void m11223() {
        m11109();
        this.f9557.m11333();
        this.f9559.m10626(zzchc.f9209.m10671().longValue());
    }

    /* access modifiers changed from: private */
    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final void m11224() {
        m11109();
        if (m11251()) {
            m11096().m10848().m10849("Inactivity, disconnecting from the service");
            m11274();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcgi m11229(boolean z) {
        return m11092().m10732(z ? m11096().m10831() : null);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11232(ComponentName componentName) {
        m11109();
        if (this.f9558 != null) {
            this.f9558 = null;
            m11096().m10848().m10850("Disconnected from device MeasurementService", componentName);
            m11109();
            m11255();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11234(Runnable runnable) throws IllegalStateException {
        m11109();
        if (m11251()) {
            runnable.run();
        } else if (((long) this.f9555.size()) >= 1000) {
            m11096().m10832().m10849("Discarding data. Max runnable queue size reached");
        } else {
            this.f9555.add(runnable);
            this.f9556.m10626(60000);
            m11255();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m11235() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m11236() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m11237() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m11238() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m11239() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m11240() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m11241() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m11242() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m11243() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m11244() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m11245() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m11246() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m11247() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m11248() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m11249() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m11250() {
        return super.m11105();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final boolean m11251() {
        m11109();
        m11115();
        return this.f9558 != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final void m11252() {
        m11109();
        m11115();
        m11234((Runnable) new zzckn(this, m11229(true)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public final void m11253() {
        m11109();
        m11115();
        zzcgi r0 = m11229(false);
        m11094().m10754();
        m11234((Runnable) new zzcki(this, r0));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵢ  reason: contains not printable characters */
    public final void m11254() {
        m11109();
        m11115();
        m11234((Runnable) new zzckk(this, m11229(true)));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public final void m11255() {
        boolean z;
        boolean z2;
        boolean z3 = true;
        m11109();
        m11115();
        if (!m11251()) {
            if (this.f9560 == null) {
                m11109();
                m11115();
                Boolean r0 = m11098().m10889();
                if (r0 == null || !r0.booleanValue()) {
                    if (m11092().m10727() != 1) {
                        m11096().m10848().m10849("Checking service availability");
                        int r02 = zzf.m4219().m4227(m11112().m11097());
                        switch (r02) {
                            case 0:
                                m11096().m10848().m10849("Service available");
                                z = true;
                                z2 = true;
                                break;
                            case 1:
                                m11096().m10848().m10849("Service missing");
                                z = true;
                                z2 = false;
                                break;
                            case 2:
                                m11096().m10845().m10849("Service container out of date");
                                zzclq r03 = m11112();
                                zzf.m4219();
                                if (zzf.m4218(r03.m11097()) >= 11400) {
                                    Boolean r04 = m11098().m10889();
                                    z2 = r04 == null || r04.booleanValue();
                                    z = false;
                                    break;
                                } else {
                                    z = true;
                                    z2 = false;
                                    break;
                                }
                                break;
                            case 3:
                                m11096().m10834().m10849("Service disabled");
                                z = false;
                                z2 = false;
                                break;
                            case 9:
                                m11096().m10834().m10849("Service invalid");
                                z = false;
                                z2 = false;
                                break;
                            case 18:
                                m11096().m10834().m10849("Service updating");
                                z = true;
                                z2 = true;
                                break;
                            default:
                                m11096().m10834().m10850("Unexpected service status", Integer.valueOf(r02));
                                z = false;
                                z2 = false;
                                break;
                        }
                    } else {
                        z = true;
                        z2 = true;
                    }
                    if (z) {
                        m11098().m10898(z2);
                    }
                } else {
                    z2 = true;
                }
                this.f9560 = Boolean.valueOf(z2);
            }
            if (this.f9560.booleanValue()) {
                this.f9561.m11278();
                return;
            }
            List<ResolveInfo> queryIntentServices = m11097().getPackageManager().queryIntentServices(new Intent().setClassName(m11097(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
            if (queryIntentServices == null || queryIntentServices.size() <= 0) {
                z3 = false;
            }
            if (z3) {
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                intent.setComponent(new ComponentName(m11097(), "com.google.android.gms.measurement.AppMeasurementService"));
                this.f9561.m11280(intent);
                return;
            }
            m11096().m10832().m10849("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m11256() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11257() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m11258() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11259() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11260() {
        super.m11110();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11261(zzcgl zzcgl) {
        zzbq.m9120(zzcgl);
        m11109();
        m11115();
        m11234((Runnable) new zzckp(this, true, m11094().m10761(zzcgl), new zzcgl(zzcgl), m11229(true), zzcgl));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11262(zzcha zzcha, String str) {
        zzbq.m9120(zzcha);
        m11109();
        m11115();
        m11234((Runnable) new zzcko(this, true, m11094().m10762(zzcha), zzcha, m11229(true), str));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11263(zzche zzche) {
        m11109();
        zzbq.m9120(zzche);
        this.f9558 = zzche;
        m11223();
        m11222();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11264(zzche zzche, zzbfm zzbfm, zzcgi zzcgi) {
        m11109();
        m11115();
        int i = 100;
        for (int i2 = 0; i2 < 1001 && i == 100; i2++) {
            ArrayList arrayList = new ArrayList();
            List<zzbfm> r1 = m11094().m10759(100);
            if (r1 != null) {
                arrayList.addAll(r1);
                i = r1.size();
            } else {
                i = 0;
            }
            if (zzbfm != null && i < 100) {
                arrayList.add(zzbfm);
            }
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList2.get(i3);
                i3++;
                zzbfm zzbfm2 = (zzbfm) obj;
                if (zzbfm2 instanceof zzcha) {
                    try {
                        zzche.m10686((zzcha) zzbfm2, zzcgi);
                    } catch (RemoteException e) {
                        m11096().m10832().m10850("Failed to send event to the service", e);
                    }
                } else if (zzbfm2 instanceof zzcln) {
                    try {
                        zzche.m10688((zzcln) zzbfm2, zzcgi);
                    } catch (RemoteException e2) {
                        m11096().m10832().m10850("Failed to send attribute to the service", e2);
                    }
                } else if (zzbfm2 instanceof zzcgl) {
                    try {
                        zzche.m10685((zzcgl) zzbfm2, zzcgi);
                    } catch (RemoteException e3) {
                        m11096().m10832().m10850("Failed to send conditional property to the service", e3);
                    }
                } else {
                    m11096().m10832().m10849("Discarding data. Unrecognized parcel type.");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11265(zzcln zzcln) {
        m11109();
        m11115();
        m11234((Runnable) new zzcks(this, m11094().m10763(zzcln), zzcln, m11229(true)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11266(AppMeasurement$zzb appMeasurement$zzb) {
        m11109();
        m11115();
        m11234((Runnable) new zzckl(this, appMeasurement$zzb));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11267(AtomicReference<String> atomicReference) {
        m11109();
        m11115();
        m11234((Runnable) new zzckj(this, atomicReference, m11229(false)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11268(AtomicReference<List<zzcgl>> atomicReference, String str, String str2, String str3) {
        m11109();
        m11115();
        m11234((Runnable) new zzckq(this, atomicReference, str, str2, str3, m11229(false)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11269(AtomicReference<List<zzcln>> atomicReference, String str, String str2, String str3, boolean z) {
        m11109();
        m11115();
        m11234((Runnable) new zzckr(this, atomicReference, str, str2, str3, z, m11229(false)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11270(AtomicReference<List<zzcln>> atomicReference, boolean z) {
        m11109();
        m11115();
        m11234((Runnable) new zzckt(this, atomicReference, m11229(false), z));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹳ  reason: contains not printable characters */
    public final Boolean m11271() {
        return this.f9560;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m11272() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m11273() {
        return super.m11112();
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final void m11274() {
        m11109();
        m11115();
        try {
            zza.m9235();
            m11097().unbindService(this.f9561);
        } catch (IllegalArgumentException | IllegalStateException e) {
        }
        this.f9558 = null;
    }
}
