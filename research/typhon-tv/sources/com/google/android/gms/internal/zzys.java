package com.google.android.gms.internal;

import com.google.android.gms.internal.zzou;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

public interface zzys<T extends zzou> {
    /* renamed from: 龘  reason: contains not printable characters */
    T m13643(zzym zzym, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException;
}
