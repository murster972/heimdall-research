package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.api.internal.zzcm;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

final class zzcec extends zzcem {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ LocationListener f9050;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationRequest f9051;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcec(zzceb zzceb, GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener) {
        super(googleApiClient);
        this.f9051 = locationRequest;
        this.f9050 = locationListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10339(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10413(this.f9051, (zzci<LocationListener>) zzcm.m8858(this.f9050, zzcgc.m10425(), LocationListener.class.getSimpleName()), (zzceu) new zzcen(this));
    }
}
