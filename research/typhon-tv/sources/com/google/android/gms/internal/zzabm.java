package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

@zzzv
public final class zzabm extends zzbfm {
    public static final Parcelable.Creator<zzabm> CREATOR = new zzabn();

    /* renamed from: ʻ  reason: contains not printable characters */
    private PackageInfo f3871;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f3872;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f3873;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f3874;

    /* renamed from: 连任  reason: contains not printable characters */
    private List<String> f3875;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzakd f3876;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f3877;

    /* renamed from: 齉  reason: contains not printable characters */
    private ApplicationInfo f3878;

    /* renamed from: 龘  reason: contains not printable characters */
    private Bundle f3879;

    public zzabm(Bundle bundle, zzakd zzakd, ApplicationInfo applicationInfo, String str, List<String> list, PackageInfo packageInfo, String str2, boolean z, String str3) {
        this.f3879 = bundle;
        this.f3876 = zzakd;
        this.f3877 = str;
        this.f3878 = applicationInfo;
        this.f3875 = list;
        this.f3871 = packageInfo;
        this.f3872 = str2;
        this.f3873 = z;
        this.f3874 = str3;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10187(parcel, 1, this.f3879, false);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f3876, i, false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f3878, i, false);
        zzbfp.m10193(parcel, 4, this.f3877, false);
        zzbfp.m10178(parcel, 5, this.f3875, false);
        zzbfp.m10189(parcel, 6, (Parcelable) this.f3871, i, false);
        zzbfp.m10193(parcel, 7, this.f3872, false);
        zzbfp.m10195(parcel, 8, this.f3873);
        zzbfp.m10193(parcel, 9, this.f3874, false);
        zzbfp.m10182(parcel, r0);
    }
}
