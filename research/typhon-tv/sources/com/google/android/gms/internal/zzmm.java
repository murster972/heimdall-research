package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzmm implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzml f10802;

    zzmm(zzml zzml) {
        this.f10802 = zzml;
    }

    public final void run() {
        if (this.f10802.f10801 != null) {
            try {
                this.f10802.f10801.m9503(1);
            } catch (RemoteException e) {
                zzakb.m4796("Could not notify onRewardedVideoAdFailedToLoad event.", e);
            }
        }
    }
}
