package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzrs implements Parcelable.Creator<zzrr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        String[] strArr = null;
        String[] strArr2 = null;
        String str = null;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 2:
                    strArr2 = zzbfn.m10157(parcel, readInt);
                    break;
                case 3:
                    strArr = zzbfn.m10157(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new zzrr(str, strArr2, strArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzrr[i];
    }
}
