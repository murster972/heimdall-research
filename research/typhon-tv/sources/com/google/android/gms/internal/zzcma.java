package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcma extends zzfjm<zzcma> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile zzcma[] f9706;

    /* renamed from: 靐  reason: contains not printable characters */
    public zzcmf f9707 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Boolean f9708 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzcmf f9709 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9710 = null;

    public zzcma() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzcma[] m11485() {
        if (f9706 == null) {
            synchronized (zzfjq.f10546) {
                if (f9706 == null) {
                    f9706 = new zzcma[0];
                }
            }
        }
        return f9706;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcma)) {
            return false;
        }
        zzcma zzcma = (zzcma) obj;
        if (this.f9710 == null) {
            if (zzcma.f9710 != null) {
                return false;
            }
        } else if (!this.f9710.equals(zzcma.f9710)) {
            return false;
        }
        if (this.f9707 == null) {
            if (zzcma.f9707 != null) {
                return false;
            }
        } else if (!this.f9707.equals(zzcma.f9707)) {
            return false;
        }
        if (this.f9709 == null) {
            if (zzcma.f9709 != null) {
                return false;
            }
        } else if (!this.f9709.equals(zzcma.f9709)) {
            return false;
        }
        if (this.f9708 == null) {
            if (zzcma.f9708 != null) {
                return false;
            }
        } else if (!this.f9708.equals(zzcma.f9708)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcma.f10533 == null || zzcma.f10533.m12849() : this.f10533.equals(zzcma.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f9710 == null ? 0 : this.f9710.hashCode()) + ((getClass().getName().hashCode() + 527) * 31);
        zzcmf zzcmf = this.f9707;
        int i2 = hashCode * 31;
        int hashCode2 = zzcmf == null ? 0 : zzcmf.hashCode();
        zzcmf zzcmf2 = this.f9709;
        int hashCode3 = ((this.f9708 == null ? 0 : this.f9708.hashCode()) + (((zzcmf2 == null ? 0 : zzcmf2.hashCode()) + ((hashCode2 + i2) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode3 + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11486() {
        int r0 = super.m12841();
        if (this.f9710 != null) {
            r0 += zzfjk.m12806(1, this.f9710.intValue());
        }
        if (this.f9707 != null) {
            r0 += zzfjk.m12807(2, (zzfjs) this.f9707);
        }
        if (this.f9709 != null) {
            r0 += zzfjk.m12807(3, (zzfjs) this.f9709);
        }
        if (this.f9708 == null) {
            return r0;
        }
        this.f9708.booleanValue();
        return r0 + zzfjk.m12805(4) + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11487(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9710 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 18:
                    if (this.f9707 == null) {
                        this.f9707 = new zzcmf();
                    }
                    zzfjj.m12802((zzfjs) this.f9707);
                    continue;
                case 26:
                    if (this.f9709 == null) {
                        this.f9709 = new zzcmf();
                    }
                    zzfjj.m12802((zzfjs) this.f9709);
                    continue;
                case 32:
                    this.f9708 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11488(zzfjk zzfjk) throws IOException {
        if (this.f9710 != null) {
            zzfjk.m12832(1, this.f9710.intValue());
        }
        if (this.f9707 != null) {
            zzfjk.m12834(2, (zzfjs) this.f9707);
        }
        if (this.f9709 != null) {
            zzfjk.m12834(3, (zzfjs) this.f9709);
        }
        if (this.f9708 != null) {
            zzfjk.m12836(4, this.f9708.booleanValue());
        }
        super.m12842(zzfjk);
    }
}
