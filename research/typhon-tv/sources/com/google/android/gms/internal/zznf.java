package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.ConditionVariable;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.zzp;

@zzzv
public final class zznf {

    /* renamed from: 连任  reason: contains not printable characters */
    private Context f4887;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ConditionVariable f4888 = new ConditionVariable();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public SharedPreferences f4889 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile boolean f4890 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4891 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T> T m5595(zzmx<T> zzmx) {
        if (!this.f4888.block(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS)) {
            throw new IllegalStateException("Flags.initialize() was not called!");
        }
        if (!this.f4890 || this.f4889 == null) {
            synchronized (this.f4891) {
                if (!this.f4890 || this.f4889 == null) {
                    T r0 = zzmx.m5582();
                    return r0;
                }
            }
        }
        return zzajk.m4728(this.f4887, new zzng(this, zzmx));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5596(Context context) {
        if (!this.f4890) {
            synchronized (this.f4891) {
                if (!this.f4890) {
                    this.f4887 = context.getApplicationContext() == null ? context : context.getApplicationContext();
                    try {
                        Context remoteContext = zzp.getRemoteContext(context);
                        if (remoteContext != null || context == null) {
                            context = remoteContext;
                        } else {
                            Context applicationContext = context.getApplicationContext();
                            if (applicationContext != null) {
                                context = applicationContext;
                            }
                        }
                        if (context != null) {
                            zzkb.m5485();
                            this.f4889 = context.getSharedPreferences("google_ads_flags", 0);
                            this.f4890 = true;
                            this.f4888.open();
                        }
                    } finally {
                        this.f4888.open();
                    }
                }
            }
        }
    }
}
