package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzls implements Parcelable.Creator<zzlr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        int i = 0;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzlr(i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzlr[i];
    }
}
