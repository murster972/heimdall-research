package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import java.util.concurrent.Callable;

final class zzanu implements Callable<zzanh> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzcv f8354;

    /* renamed from: ʼ  reason: contains not printable characters */
    private /* synthetic */ zzakd f8355;

    /* renamed from: ʽ  reason: contains not printable characters */
    private /* synthetic */ zznu f8356;

    /* renamed from: ˑ  reason: contains not printable characters */
    private /* synthetic */ zzbl f8357;

    /* renamed from: ٴ  reason: contains not printable characters */
    private /* synthetic */ zzv f8358;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private /* synthetic */ zzis f8359;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ boolean f8360;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzapa f8361;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ boolean f8362;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f8363;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8364;

    zzanu(zzanr zzanr, Context context, zzapa zzapa, String str, boolean z, boolean z2, zzcv zzcv, zzakd zzakd, zznu zznu, zzbl zzbl, zzv zzv, zzis zzis) {
        this.f8364 = context;
        this.f8361 = zzapa;
        this.f8363 = str;
        this.f8362 = z;
        this.f8360 = z2;
        this.f8354 = zzcv;
        this.f8355 = zzakd;
        this.f8356 = zznu;
        this.f8357 = zzbl;
        this.f8358 = zzv;
        this.f8359 = zzis;
    }

    public final /* synthetic */ Object call() throws Exception {
        zzanw zzanw = new zzanw(zzanx.m5133(this.f8364, this.f8361, this.f8363, this.f8362, this.f8360, this.f8354, this.f8355, this.f8356, this.f8357, this.f8358, this.f8359));
        zzanw.setWebViewClient(zzbs.zzek().m4654((zzanh) zzanw, this.f8360));
        zzanw.setWebChromeClient(zzbs.zzek().m4650((zzanh) zzanw));
        return zzanw;
    }
}
