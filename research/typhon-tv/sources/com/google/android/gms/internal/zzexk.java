package com.google.android.gms.internal;

import java.io.IOException;

public final class zzexk extends zzfjm<zzexk> {

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f10321 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    public long f10322 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public int f10323 = 0;

    public zzexk() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzexk)) {
            return false;
        }
        zzexk zzexk = (zzexk) obj;
        if (this.f10323 != zzexk.f10323) {
            return false;
        }
        if (this.f10321 != zzexk.f10321) {
            return false;
        }
        if (this.f10322 != zzexk.f10322) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzexk.f10533 == null || zzexk.f10533.m12849() : this.f10533.equals(zzexk.f10533);
    }

    public final int hashCode() {
        return ((this.f10533 == null || this.f10533.m12849()) ? 0 : this.f10533.hashCode()) + (((((this.f10321 ? 1231 : 1237) + ((((getClass().getName().hashCode() + 527) * 31) + this.f10323) * 31)) * 31) + ((int) (this.f10322 ^ (this.f10322 >>> 32)))) * 31);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12342() {
        int r0 = super.m12841();
        if (this.f10323 != 0) {
            r0 += zzfjk.m12806(1, this.f10323);
        }
        if (this.f10321) {
            r0 += zzfjk.m12805(2) + 1;
        }
        return this.f10322 != 0 ? r0 + zzfjk.m12805(3) + 8 : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12343(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f10323 = zzfjj.m12785();
                    continue;
                case 16:
                    this.f10321 = zzfjj.m12797();
                    continue;
                case 25:
                    this.f10322 = zzfjj.m12789();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12344(zzfjk zzfjk) throws IOException {
        if (this.f10323 != 0) {
            zzfjk.m12832(1, this.f10323);
        }
        if (this.f10321) {
            zzfjk.m12836(2, this.f10321);
        }
        if (this.f10322 != 0) {
            zzfjk.m12827(3, this.f10322);
        }
        super.m12842(zzfjk);
    }
}
