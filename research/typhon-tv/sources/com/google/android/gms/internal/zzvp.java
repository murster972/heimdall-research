package com.google.android.gms.internal;

import android.os.RemoteException;

public final class zzvp extends zzlm {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile zzlo f10927;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f10928 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final float m13607() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final float m13608() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzlo m13609() throws RemoteException {
        zzlo zzlo;
        synchronized (this.f10928) {
            zzlo = this.f10927;
        }
        return zzlo;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m13610() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m13611() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final float m13612() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13613() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m13614() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m13615() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13616() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13617(zzlo zzlo) throws RemoteException {
        synchronized (this.f10928) {
            this.f10927 = zzlo;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13618(boolean z) throws RemoteException {
        throw new RemoteException();
    }
}
