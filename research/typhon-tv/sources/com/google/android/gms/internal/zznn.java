package com.google.android.gms.internal;

@zzzv
public final class zznn {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzns m5613(zznu zznu) {
        if (zznu == null) {
            return null;
        }
        return zznu.m5625();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m5614(zznu zznu, zzns zzns, String... strArr) {
        if (zznu == null || zzns == null) {
            return false;
        }
        return zznu.m5631(zzns, strArr);
    }
}
