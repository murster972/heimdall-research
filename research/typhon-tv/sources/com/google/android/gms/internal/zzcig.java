package com.google.android.gms.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$Event;
import com.google.android.gms.measurement.AppMeasurement$Param;
import com.google.android.gms.measurement.AppMeasurement$UserProperty;
import java.io.IOException;
import java.util.Map;

public final class zzcig extends zzcjl {

    /* renamed from: 靐  reason: contains not printable characters */
    private static int f9350 = 2;

    /* renamed from: 龘  reason: contains not printable characters */
    private static int f9351 = 65535;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<String, zzcly> f9352 = new ArrayMap();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Map<String, Map<String, Integer>> f9353 = new ArrayMap();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Map<String, String> f9354 = new ArrayMap();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<String, Map<String, Boolean>> f9355 = new ArrayMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<String, Map<String, Boolean>> f9356 = new ArrayMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Map<String, Map<String, String>> f9357 = new ArrayMap();

    zzcig(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m10914(String str) {
        m11115();
        m11109();
        zzbq.m9122(str);
        if (this.f9352.get(str) == null) {
            byte[] r0 = m11095().m10596(str);
            if (r0 == null) {
                this.f9357.put(str, (Object) null);
                this.f9356.put(str, (Object) null);
                this.f9355.put(str, (Object) null);
                this.f9352.put(str, (Object) null);
                this.f9354.put(str, (Object) null);
                this.f9353.put(str, (Object) null);
                return;
            }
            zzcly r02 = m10915(str, r0);
            this.f9357.put(str, m10916(r02));
            m10917(str, r02);
            this.f9352.put(str, r02);
            this.f9354.put(str, (Object) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcly m10915(String str, byte[] bArr) {
        if (bArr == null) {
            return new zzcly();
        }
        zzfjj r1 = zzfjj.m12783(bArr, 0, bArr.length);
        zzcly zzcly = new zzcly();
        try {
            zzcly.m12876(r1);
            m11096().m10848().m10851("Parsed config. version, gmp_app_id", zzcly.f9701, zzcly.f9698);
            return zzcly;
        } catch (IOException e) {
            m11096().m10834().m10851("Unable to merge remote config. appId", zzchm.m10812(str), e);
            return new zzcly();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<String, String> m10916(zzcly zzcly) {
        ArrayMap arrayMap = new ArrayMap();
        if (!(zzcly == null || zzcly.f9700 == null)) {
            for (zzclz zzclz : zzcly.f9700) {
                if (zzclz != null) {
                    arrayMap.put(zzclz.f9704, zzclz.f9703);
                }
            }
        }
        return arrayMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10917(String str, zzcly zzcly) {
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        if (!(zzcly == null || zzcly.f9699 == null)) {
            for (zzclx zzclx : zzcly.f9699) {
                if (TextUtils.isEmpty(zzclx.f9695)) {
                    m11096().m10834().m10849("EventConfig contained null event name");
                } else {
                    String r7 = AppMeasurement$Event.m13698(zzclx.f9695);
                    if (!TextUtils.isEmpty(r7)) {
                        zzclx.f9695 = r7;
                    }
                    arrayMap.put(zzclx.f9695, zzclx.f9692);
                    arrayMap2.put(zzclx.f9695, zzclx.f9694);
                    if (zzclx.f9693 != null) {
                        if (zzclx.f9693.intValue() < f9350 || zzclx.f9693.intValue() > f9351) {
                            m11096().m10834().m10851("Invalid sampling rate. Event name, sample rate", zzclx.f9695, zzclx.f9693);
                        } else {
                            arrayMap3.put(zzclx.f9695, zzclx.f9693);
                        }
                    }
                }
            }
        }
        this.f9356.put(str, arrayMap);
        this.f9355.put(str, arrayMap2);
        this.f9353.put(str, arrayMap3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10918() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10919() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10920() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10921() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10922() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10923() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10924() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10925() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10926() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10927() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10928() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10929() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10930() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10931() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10932() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10933() {
        return super.m11105();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10934() {
        return super.m11106();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10935(String str) {
        m11109();
        return this.f9354.get(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10936() {
        super.m11107();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m10937(String str, String str2) {
        m11109();
        m10914(str);
        if (m11112().m11407(str) && zzclq.m11368(str2)) {
            return true;
        }
        if (m11112().m11403(str) && zzclq.m11385(str2)) {
            return true;
        }
        Map map = this.f9356.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final int m10938(String str, String str2) {
        m11109();
        m10914(str);
        Map map = this.f9353.get(str);
        if (map == null) {
            return 1;
        }
        Integer num = (Integer) map.get(str2);
        if (num == null) {
            return 1;
        }
        return num.intValue();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10939() {
        return super.m11108();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10940(String str) {
        m11109();
        this.f9352.remove(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10941() {
        super.m11109();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10942(String str) {
        m11109();
        this.f9354.put(str, (Object) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m10943(String str, String str2) {
        m11109();
        m10914(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = this.f9355.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcly m10944(String str) {
        m11115();
        m11109();
        zzbq.m9122(str);
        m10914(str);
        return this.f9352.get(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10945(String str, String str2) {
        m11109();
        m10914(str);
        Map map = this.f9357.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10946() {
        super.m11110();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10947(String str, byte[] bArr, String str2) {
        m11115();
        m11109();
        zzbq.m9122(str);
        zzcly r5 = m10915(str, bArr);
        if (r5 == null) {
            return false;
        }
        m10917(str, r5);
        this.f9352.put(str, r5);
        this.f9354.put(str, str2);
        this.f9357.put(str, m10916(r5));
        zzcgk r6 = m11106();
        zzclr[] zzclrArr = r5.f9697;
        zzbq.m9120(zzclrArr);
        for (zzclr zzclr : zzclrArr) {
            for (zzcls zzcls : zzclr.f9665) {
                String r12 = AppMeasurement$Event.m13698(zzcls.f9669);
                if (r12 != null) {
                    zzcls.f9669 = r12;
                }
                for (zzclt zzclt : zzcls.f9671) {
                    String r15 = AppMeasurement$Param.m13701(zzclt.f9675);
                    if (r15 != null) {
                        zzclt.f9675 = r15;
                    }
                }
            }
            for (zzclv zzclv : zzclr.f9664) {
                String r11 = AppMeasurement$UserProperty.m13702(zzclv.f9684);
                if (r11 != null) {
                    zzclv.f9684 = r11;
                }
            }
        }
        r6.m11095().m10609(str, zzclrArr);
        try {
            r5.f9697 = null;
            byte[] bArr2 = new byte[r5.m12872()];
            r5.m12877(zzfjk.m12822(bArr2, 0, bArr2.length));
            bArr = bArr2;
        } catch (IOException e) {
            m11096().m10834().m10851("Unable to serialize reduced-size config. Storing full config instead. appId", zzchm.m10812(str), e);
        }
        zzcgo r3 = m11095();
        zzbq.m9122(str);
        r3.m11109();
        r3.m11115();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) r3.m10587().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                r3.m11096().m10832().m10850("Failed to update remote config (got 0). appId", zzchm.m10812(str));
            }
        } catch (SQLiteException e2) {
            r3.m11096().m10832().m10851("Error storing remote config. appId", zzchm.m10812(str), e2);
        }
        return true;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10948() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10949() {
        return super.m11112();
    }
}
