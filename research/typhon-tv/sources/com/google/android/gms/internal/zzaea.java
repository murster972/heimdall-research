package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;

final class zzaea implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzjj f8121;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzady f8122;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzaeg f8123;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzva f8124;

    zzaea(zzady zzady, zzva zzva, zzjj zzjj, zzaeg zzaeg) {
        this.f8122 = zzady;
        this.f8124 = zzva;
        this.f8121 = zzjj;
        this.f8123 = zzaeg;
    }

    public final void run() {
        try {
            this.f8124.m13471(zzn.m9306(zzady.龘(this.f8122)), this.f8121, (String) null, (zzaem) this.f8123, zzady.靐(this.f8122));
        } catch (RemoteException e) {
            RemoteException remoteException = e;
            String valueOf = String.valueOf(zzady.齉(this.f8122));
            zzagf.m4796(valueOf.length() != 0 ? "Fail to initialize adapter ".concat(valueOf) : new String("Fail to initialize adapter "), remoteException);
            this.f8122.龘(zzady.齉(this.f8122), 0);
        }
    }
}
