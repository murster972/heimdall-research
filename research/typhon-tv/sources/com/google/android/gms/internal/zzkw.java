package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzkw extends zzeu implements zzkv {
    zzkw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdManagerCreator");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m13072(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, zzux zzux, int i, int i2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12306(v_, (Parcelable) zzjn);
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) zzux);
        v_.writeInt(11910000);
        v_.writeInt(i2);
        Parcel r0 = m12300(2, v_);
        IBinder readStrongBinder = r0.readStrongBinder();
        r0.recycle();
        return readStrongBinder;
    }
}
