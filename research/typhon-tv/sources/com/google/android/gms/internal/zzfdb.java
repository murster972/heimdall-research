package com.google.android.gms.internal;

public final class zzfdb {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f10338;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f10339;

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f10340 = new byte[256];

    public zzfdb(byte[] bArr) {
        for (int i = 0; i < 256; i++) {
            this.f10340[i] = (byte) i;
        }
        byte b = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b = (b + this.f10340[i2] + bArr[i2 % bArr.length]) & 255;
            byte b2 = this.f10340[i2];
            this.f10340[i2] = this.f10340[b];
            this.f10340[b] = b2;
        }
        this.f10338 = 0;
        this.f10339 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12360(byte[] bArr) {
        int i = this.f10338;
        int i2 = this.f10339;
        for (int i3 = 0; i3 < bArr.length; i3++) {
            i = (i + 1) & 255;
            i2 = (i2 + this.f10340[i]) & 255;
            byte b = this.f10340[i];
            this.f10340[i] = this.f10340[i2];
            this.f10340[i2] = b;
            bArr[i3] = (byte) (bArr[i3] ^ this.f10340[(this.f10340[i] + this.f10340[i2]) & 255]);
        }
        this.f10338 = i;
        this.f10339 = i2;
    }
}
