package com.google.android.gms.internal;

import android.text.TextUtils;
import java.io.IOException;

public class zzbce {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzbdc f8626;

    /* renamed from: 齉  reason: contains not printable characters */
    protected final zzbcy f8627;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8628;

    protected zzbce(String str, String str2, String str3) {
        zzbcm.m10042(str);
        this.f8628 = str;
        this.f8627 = new zzbcy(str2);
        m9974(str3);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m9968() {
        return this.f8628;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m9969(String str) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final long m9970() {
        return this.f8626.m10123();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9971() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9972(long j, int i) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9973(zzbdc zzbdc) {
        this.f8626 = zzbdc;
        if (this.f8626 == null) {
            m9971();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9974(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f8627.m10089(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9975(String str, long j, String str2) throws IOException {
        Object[] objArr = {str, null};
        this.f8626.m10124(this.f8628, str, j, (String) null);
    }
}
