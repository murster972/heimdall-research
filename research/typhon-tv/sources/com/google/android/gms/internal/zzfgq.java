package com.google.android.gms.internal;

final class zzfgq implements zzfhw {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final zzfhd f10433 = new zzfgr();

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzfgu f10434;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzfhd f10435;

    public zzfgq() {
        this(zzfgu.DYNAMIC);
    }

    private zzfgq(zzfgu zzfgu) {
        this(new zzfgt(zzfft.m12522(), m12603()), zzfgu);
    }

    private zzfgq(zzfhd zzfhd, zzfgu zzfgu) {
        this.f10435 = (zzfhd) zzffz.m12580(zzfhd, "messageInfoFactory");
        this.f10434 = (zzfgu) zzffz.m12580(zzfgu, "mode");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static <T> zzfhv<T> m12602(Class<T> cls, zzfhc zzfhc) {
        if (zzffu.class.isAssignableFrom(cls)) {
            if (m12605(zzfhc)) {
                return zzfhi.m12634(cls, zzfhc, zzfgm.m12600(), zzfhx.m12675(), zzffp.m12507(), zzfhb.m12613());
            }
            return zzfhi.m12634(cls, zzfhc, zzfgm.m12600(), zzfhx.m12675(), (zzffn<?>) null, zzfhb.m12613());
        } else if (m12605(zzfhc)) {
            return zzfhi.m12634(cls, zzfhc, zzfgm.m12601(), zzfhx.m12676(), zzffp.m12505(), zzfhb.m12615());
        } else {
            return zzfhi.m12634(cls, zzfhc, zzfgm.m12601(), zzfhx.m12673(), (zzffn<?>) null, zzfhb.m12615());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzfhd m12603() {
        try {
            return (zzfhd) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception e) {
            return f10433;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> zzfhv<T> m12604(Class<T> cls, zzfhc zzfhc) {
        if (zzffu.class.isAssignableFrom(cls)) {
            if (m12605(zzfhc)) {
                return zzfhi.m12635(cls, zzfhc, zzfgm.m12600(), zzfhx.m12675(), zzffp.m12507(), zzfhb.m12613());
            }
            return zzfhi.m12635(cls, zzfhc, zzfgm.m12600(), zzfhx.m12675(), (zzffn<?>) null, zzfhb.m12613());
        } else if (m12605(zzfhc)) {
            return zzfhi.m12635(cls, zzfhc, zzfgm.m12601(), zzfhx.m12676(), zzffp.m12505(), zzfhb.m12615());
        } else {
            return zzfhi.m12635(cls, zzfhc, zzfgm.m12601(), zzfhx.m12673(), (zzffn<?>) null, zzfhb.m12615());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m12605(zzfhc zzfhc) {
        return zzfhc.m12619() == zzfhm.f10448;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T> zzfhv<T> m12606(Class<T> cls) {
        zzfhx.m12678((Class<?>) cls);
        zzfhc r0 = this.f10435.m12620(cls);
        if (r0.m12616()) {
            return zzffu.class.isAssignableFrom(cls) ? zzfhj.m12638(cls, zzfhx.m12675(), zzffp.m12507(), r0.m12618()) : zzfhj.m12638(cls, zzfhx.m12676(), zzffp.m12505(), r0.m12618());
        }
        switch (zzfgs.f10436[this.f10434.ordinal()]) {
            case 1:
                return m12604(cls, r0);
            case 2:
                return m12602(cls, r0);
            default:
                return r0.m12617() ? m12604(cls, r0) : m12602(cls, r0);
        }
    }
}
