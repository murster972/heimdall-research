package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public abstract class zzah implements zzaq {
    @Deprecated
    /* renamed from: 靐  reason: contains not printable characters */
    public final HttpResponse m4552(zzr<?> zzr, Map<String, String> map) throws IOException, zza {
        zzap r1 = m4553(zzr, map);
        BasicHttpResponse basicHttpResponse = new BasicHttpResponse(new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), r1.m9731(), ""));
        ArrayList arrayList = new ArrayList();
        for (zzl next : r1.m9728()) {
            arrayList.add(new BasicHeader(next.m13077(), next.m13076()));
        }
        basicHttpResponse.setHeaders((Header[]) arrayList.toArray(new Header[arrayList.size()]));
        InputStream r0 = r1.m9729();
        if (r0 != null) {
            BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
            basicHttpEntity.setContent(r0);
            basicHttpEntity.setContentLength((long) r1.m9730());
            basicHttpResponse.setEntity(basicHttpEntity);
        }
        return basicHttpResponse;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzap m4553(zzr<?> zzr, Map<String, String> map) throws IOException, zza;
}
