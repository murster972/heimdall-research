package com.google.android.gms.internal;

import android.os.Looper;
import com.google.android.gms.common.internal.zzbq;

public final class zzcgc {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Looper m10425() {
        zzbq.m9126(Looper.myLooper() != null, (Object) "Can't create handler inside thread that has not called Looper.prepare()");
        return Looper.myLooper();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Looper m10426(Looper looper) {
        return looper != null ? looper : m10425();
    }
}
