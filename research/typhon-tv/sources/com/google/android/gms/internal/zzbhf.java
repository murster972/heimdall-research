package com.google.android.gms.internal;

import android.content.Context;

public final class zzbhf {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzbhf f8756 = new zzbhf();

    /* renamed from: 龘  reason: contains not printable characters */
    private zzbhe f8757 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private final synchronized zzbhe m10230(Context context) {
        if (this.f8757 == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.f8757 = new zzbhe(context);
        }
        return this.f8757;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbhe m10231(Context context) {
        return f8756.m10230(context);
    }
}
