package com.google.android.gms.internal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import org.apache.commons.lang3.time.TimeZones;
import org.apache.oltu.oauth2.common.OAuth;

public final class zzao {
    /* renamed from: 靐  reason: contains not printable characters */
    static List<zzl> m9713(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(new zzl((String) next.getKey(), (String) next.getValue()));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m9714(String str) {
        try {
            return m9718().parse(str).getTime();
        } catch (ParseException e) {
            zzae.m9517(e, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzc m9715(zzp zzp) {
        boolean z;
        long j;
        long j2;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = zzp.f5280;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        boolean z2 = false;
        String str = map.get("Date");
        if (str != null) {
            j3 = m9714(str);
        }
        String str2 = map.get("Cache-Control");
        if (str2 != null) {
            String[] split = str2.split(",");
            int i = 0;
            z = false;
            while (i < split.length) {
                String trim = split[i].trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j4 = Long.parseLong(trim.substring(8));
                    } catch (Exception e) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    try {
                        j5 = Long.parseLong(trim.substring(23));
                    } catch (Exception e2) {
                    }
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    z = true;
                }
                i++;
                j5 = j5;
            }
            z2 = true;
        } else {
            z = false;
        }
        String str3 = map.get("Expires");
        long r12 = str3 != null ? m9714(str3) : 0;
        String str4 = map.get("Last-Modified");
        long r14 = str4 != null ? m9714(str4) : 0;
        String str5 = map.get("ETag");
        if (z2) {
            j2 = currentTimeMillis + (1000 * j4);
            j = z ? j2 : (1000 * j5) + j2;
        } else if (j3 <= 0 || r12 < j3) {
            j = 0;
            j2 = 0;
        } else {
            long j6 = currentTimeMillis + (r12 - j3);
            j = j6;
            j2 = j6;
        }
        zzc zzc = new zzc();
        zzc.f8823 = zzp.f5278;
        zzc.f8820 = str5;
        zzc.f8816 = j2;
        zzc.f8819 = j;
        zzc.f8822 = j3;
        zzc.f8821 = r14;
        zzc.f8817 = map;
        zzc.f8818 = zzp.f5279;
        return zzc;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m9716(long j) {
        return m9718().format(new Date(j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9717(Map<String, String> map) {
        String str = map.get(OAuth.HeaderType.CONTENT_TYPE);
        if (str != null) {
            String[] split = str.split(";");
            for (int i = 1; i < split.length; i++) {
                String[] split2 = split[i].trim().split("=");
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return "ISO-8859-1";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static SimpleDateFormat m9718() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(TimeZones.GMT_ID));
        return simpleDateFormat;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Map<String, String> m9719(List<zzl> list) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (zzl next : list) {
            treeMap.put(next.m13077(), next.m13076());
        }
        return treeMap;
    }
}
