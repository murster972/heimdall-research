package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzsf extends zzki {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzse f10873;

    zzsf(zzse zzse) {
        this.f10873 = zzse;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m13387() throws RemoteException {
        this.f10873.f5356.add(new zzsl(this));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m13388() throws RemoteException {
        this.f10873.f5356.add(new zzsm(this));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13389() throws RemoteException {
        this.f10873.f5356.add(new zzsi(this));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13390() throws RemoteException {
        this.f10873.f5356.add(new zzsk(this));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13391() throws RemoteException {
        this.f10873.f5356.add(new zzsj(this));
        zzagf.m4527("Pooled interstitial loaded.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13392() throws RemoteException {
        this.f10873.f5356.add(new zzsg(this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13393(int i) throws RemoteException {
        this.f10873.f5356.add(new zzsh(this, i));
        zzagf.m4527("Pooled interstitial failed to load.");
    }
}
