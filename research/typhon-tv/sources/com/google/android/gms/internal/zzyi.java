package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzba;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzzv
public final class zzyi extends zzagb {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Future<zzafo> f5577;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Object f5578;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzaax f5579;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzym f5580;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzafp f5581;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzyb f5582;

    public zzyi(Context context, zzba zzba, zzafp zzafp, zzcv zzcv, zzyb zzyb, zznu zznu) {
        this(zzafp, zzyb, new zzym(context, zzba, new zzaiv(context), zzcv, zzafp, zznu));
    }

    private zzyi(zzafp zzafp, zzyb zzyb, zzym zzym) {
        this.f5578 = new Object();
        this.f5581 = zzafp;
        this.f5579 = zzafp.f4133;
        this.f5582 = zzyb;
        this.f5580 = zzym;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m6037() {
        synchronized (this.f5578) {
            if (this.f5577 != null) {
                this.f5577.cancel(true);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6038() {
        zzafo zzafo;
        int i = -2;
        try {
            synchronized (this.f5578) {
                this.f5577 = zzahh.m4556((ExecutorService) zzahh.f4211, this.f5580);
            }
            zzafo = this.f5577.get(60000, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            zzagf.m4791("Timed out waiting for native ad.");
            i = 2;
            this.f5577.cancel(true);
            zzafo = null;
        } catch (ExecutionException e2) {
            i = 0;
            zzafo = null;
        } catch (InterruptedException e3) {
            i = 0;
            zzafo = null;
        } catch (CancellationException e4) {
            i = 0;
            zzafo = null;
        }
        if (zzafo == null) {
            zzafo = new zzafo(this.f5581.f4136.f3763, (zzanh) null, (List<String>) null, i, (List<String>) null, (List<String>) null, this.f5579.f3849, this.f5579.f3848, this.f5581.f4136.f3740, false, (zzuh) null, (zzva) null, (String) null, (zzui) null, (zzuk) null, this.f5579.f3824, this.f5581.f4134, this.f5579.f3820, this.f5581.f4127, this.f5579.f3826, this.f5579.f3828, this.f5581.f4129, (zzou) null, (zzaeq) null, (List<String>) null, (List<String>) null, this.f5581.f4133.f3862, this.f5581.f4133.f3865, (String) null, (List<String>) null, this.f5579.f3827, this.f5581.f4130, this.f5581.f4133.f3835, false);
        }
        zzahn.f4212.post(new zzyj(this, zzafo));
    }
}
