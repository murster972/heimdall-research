package com.google.android.gms.internal;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;

final class zzcjy implements Callable<String> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcjn f9533;

    zzcjy(zzcjn zzcjn) {
        this.f9533 = zzcjn;
    }

    public final /* synthetic */ Object call() throws Exception {
        String r0 = this.f9533.m11098().m10888();
        if (r0 == null) {
            zzcjn r2 = this.f9533.m11091();
            if (r2.m11101().m10976()) {
                r2.m11096().m10832().m10849("Cannot retrieve app instance id from analytics worker thread");
                r0 = null;
            } else {
                r2.m11101();
                if (zzcih.m10950()) {
                    r2.m11096().m10832().m10849("Cannot retrieve app instance id from main thread");
                    r0 = null;
                } else {
                    long r4 = r2.m11105().m9241();
                    r0 = r2.m11162(120000);
                    long r42 = r2.m11105().m9241() - r4;
                    if (r0 == null && r42 < 120000) {
                        r0 = r2.m11162(120000 - r42);
                    }
                }
            }
            if (r0 == null) {
                throw new TimeoutException();
            }
            this.f9533.m11098().m10894(r0);
        }
        return r0;
    }
}
