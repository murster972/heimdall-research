package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;

@zzzv
public final class zzri extends zzp<zzpx> {
    public zzri() {
        super("com.google.android.gms.ads.NativeAdViewDelegateCreatorImpl");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzpu m5798(Context context, FrameLayout frameLayout, FrameLayout frameLayout2) {
        try {
            IBinder r2 = ((zzpx) m9308(context)).m13241(zzn.m9306(context), zzn.m9306(frameLayout), zzn.m9306(frameLayout2), 11910000);
            if (r2 == null) {
                return null;
            }
            IInterface queryLocalInterface = r2.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
            return queryLocalInterface instanceof zzpu ? (zzpu) queryLocalInterface : new zzpw(r2);
        } catch (RemoteException | zzq e) {
            zzakb.m4796("Could not create remote NativeAdViewDelegate.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m5799(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegateCreator");
        return queryLocalInterface instanceof zzpx ? (zzpx) queryLocalInterface : new zzpy(iBinder);
    }
}
