package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class zzfjp implements Cloneable {

    /* renamed from: 靐  reason: contains not printable characters */
    private Object f10543;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<zzfju> f10544 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    private zzfjn<?, ?> f10545;

    zzfjp() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final byte[] m12853() throws IOException {
        byte[] bArr = new byte[m12855()];
        m12856(zzfjk.m12821(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public zzfjp clone() {
        int i = 0;
        zzfjp zzfjp = new zzfjp();
        try {
            zzfjp.f10545 = this.f10545;
            if (this.f10544 == null) {
                zzfjp.f10544 = null;
            } else {
                zzfjp.f10544.addAll(this.f10544);
            }
            if (this.f10543 != null) {
                if (this.f10543 instanceof zzfjs) {
                    zzfjp.f10543 = (zzfjs) ((zzfjs) this.f10543).clone();
                } else if (this.f10543 instanceof byte[]) {
                    zzfjp.f10543 = ((byte[]) this.f10543).clone();
                } else if (this.f10543 instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.f10543;
                    byte[][] bArr2 = new byte[bArr.length][];
                    zzfjp.f10543 = bArr2;
                    for (int i2 = 0; i2 < bArr.length; i2++) {
                        bArr2[i2] = (byte[]) bArr[i2].clone();
                    }
                } else if (this.f10543 instanceof boolean[]) {
                    zzfjp.f10543 = ((boolean[]) this.f10543).clone();
                } else if (this.f10543 instanceof int[]) {
                    zzfjp.f10543 = ((int[]) this.f10543).clone();
                } else if (this.f10543 instanceof long[]) {
                    zzfjp.f10543 = ((long[]) this.f10543).clone();
                } else if (this.f10543 instanceof float[]) {
                    zzfjp.f10543 = ((float[]) this.f10543).clone();
                } else if (this.f10543 instanceof double[]) {
                    zzfjp.f10543 = ((double[]) this.f10543).clone();
                } else if (this.f10543 instanceof zzfjs[]) {
                    zzfjs[] zzfjsArr = (zzfjs[]) this.f10543;
                    zzfjs[] zzfjsArr2 = new zzfjs[zzfjsArr.length];
                    zzfjp.f10543 = zzfjsArr2;
                    while (true) {
                        int i3 = i;
                        if (i3 >= zzfjsArr.length) {
                            break;
                        }
                        zzfjsArr2[i3] = (zzfjs) zzfjsArr[i3].clone();
                        i = i3 + 1;
                    }
                }
            }
            return zzfjp;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfjp)) {
            return false;
        }
        zzfjp zzfjp = (zzfjp) obj;
        if (this.f10543 == null || zzfjp.f10543 == null) {
            if (this.f10544 != null && zzfjp.f10544 != null) {
                return this.f10544.equals(zzfjp.f10544);
            }
            try {
                return Arrays.equals(m12853(), zzfjp.m12853());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.f10545 == zzfjp.f10545) {
            return !this.f10545.f10537.isArray() ? this.f10543.equals(zzfjp.f10543) : this.f10543 instanceof byte[] ? Arrays.equals((byte[]) this.f10543, (byte[]) zzfjp.f10543) : this.f10543 instanceof int[] ? Arrays.equals((int[]) this.f10543, (int[]) zzfjp.f10543) : this.f10543 instanceof long[] ? Arrays.equals((long[]) this.f10543, (long[]) zzfjp.f10543) : this.f10543 instanceof float[] ? Arrays.equals((float[]) this.f10543, (float[]) zzfjp.f10543) : this.f10543 instanceof double[] ? Arrays.equals((double[]) this.f10543, (double[]) zzfjp.f10543) : this.f10543 instanceof boolean[] ? Arrays.equals((boolean[]) this.f10543, (boolean[]) zzfjp.f10543) : Arrays.deepEquals((Object[]) this.f10543, (Object[]) zzfjp.f10543);
        } else {
            return false;
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(m12853()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12855() {
        int i = 0;
        if (this.f10543 != null) {
            zzfjn<?, ?> zzfjn = this.f10545;
            Object obj = this.f10543;
            if (!zzfjn.f10536) {
                return zzfjn.m12844(obj);
            }
            int length = Array.getLength(obj);
            int i2 = 0;
            for (int i3 = 0; i3 < length; i3++) {
                if (Array.get(obj, i3) != null) {
                    i2 += zzfjn.m12844(Array.get(obj, i3));
                }
            }
            return i2;
        }
        for (zzfju next : this.f10544) {
            i = next.f10550.length + zzfjk.m12813(next.f10551) + 0 + i;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12856(zzfjk zzfjk) throws IOException {
        if (this.f10543 != null) {
            zzfjn<?, ?> zzfjn = this.f10545;
            Object obj = this.f10543;
            if (zzfjn.f10536) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 != null) {
                        zzfjn.m12845(obj2, zzfjk);
                    }
                }
                return;
            }
            zzfjn.m12845(obj, zzfjk);
            return;
        }
        for (zzfju next : this.f10544) {
            zzfjk.m12825(next.f10551);
            zzfjk.m12828(next.f10550);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12857(zzfju zzfju) {
        this.f10544.add(zzfju);
    }
}
