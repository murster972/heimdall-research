package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzru implements Parcelable.Creator<zzrt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        long j = 0;
        boolean z = false;
        String[] strArr = null;
        String[] strArr2 = null;
        byte[] bArr = null;
        int i = 0;
        String str = null;
        boolean z2 = false;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    bArr = zzbfn.m10154(parcel, readInt);
                    break;
                case 5:
                    strArr2 = zzbfn.m10157(parcel, readInt);
                    break;
                case 6:
                    strArr = zzbfn.m10157(parcel, readInt);
                    break;
                case 7:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 8:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new zzrt(z2, str, i, bArr, strArr2, strArr, z, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzrt[i];
    }
}
