package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public interface zzbfv {
    /* renamed from: 龘  reason: contains not printable characters */
    PendingResult<Status> m10206(GoogleApiClient googleApiClient);
}
