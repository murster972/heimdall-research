package com.google.android.gms.internal;

import java.io.IOException;

public final class zzba extends zzfjm<zzba> {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static volatile zzba[] f8527;

    /* renamed from: ʻ  reason: contains not printable characters */
    public Long f8528 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Integer f8529;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Long f8530 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    public Long f8531 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    public Long f8532 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    public Long f8533 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    public Long f8534 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    public Long f8535 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    public Long f8536 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    public Long f8537 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    public Long f8538 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    public Long f8539 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    public Long f8540 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Integer f8541;

    /* renamed from: 连任  reason: contains not printable characters */
    public Long f8542 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f8543 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Long f8544 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Long f8545 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f8546 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Long f8547 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Long f8548 = null;

    public zzba() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final zzba m9871(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f8546 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 16:
                    this.f8543 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 24:
                    this.f8545 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 32:
                    this.f8544 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 40:
                    this.f8542 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 48:
                    this.f8528 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 56:
                    int r1 = zzfjj.m12787();
                    try {
                        this.f8529 = Integer.valueOf(zzay.m9759(zzfjj.m12785()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        zzfjj.m12792(r1);
                        m12843(zzfjj, r0);
                        break;
                    }
                case 64:
                    this.f8530 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 72:
                    this.f8539 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 80:
                    this.f8540 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 88:
                    int r12 = zzfjj.m12787();
                    try {
                        this.f8541 = Integer.valueOf(zzay.m9759(zzfjj.m12785()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        zzfjj.m12792(r12);
                        m12843(zzfjj, r0);
                        break;
                    }
                case 96:
                    this.f8534 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 104:
                    this.f8531 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 112:
                    this.f8532 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 120:
                    this.f8547 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 128:
                    this.f8548 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 136:
                    this.f8536 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 144:
                    this.f8537 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 152:
                    this.f8538 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 160:
                    this.f8533 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 168:
                    this.f8535 = Long.valueOf(zzfjj.m12786());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzba[] m9869() {
        if (f8527 == null) {
            synchronized (zzfjq.f10546) {
                if (f8527 == null) {
                    f8527 = new zzba[0];
                }
            }
        }
        return f8527;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9870() {
        int r0 = super.m12841();
        if (this.f8546 != null) {
            r0 += zzfjk.m12814(1, this.f8546.longValue());
        }
        if (this.f8543 != null) {
            r0 += zzfjk.m12814(2, this.f8543.longValue());
        }
        if (this.f8545 != null) {
            r0 += zzfjk.m12814(3, this.f8545.longValue());
        }
        if (this.f8544 != null) {
            r0 += zzfjk.m12814(4, this.f8544.longValue());
        }
        if (this.f8542 != null) {
            r0 += zzfjk.m12814(5, this.f8542.longValue());
        }
        if (this.f8528 != null) {
            r0 += zzfjk.m12814(6, this.f8528.longValue());
        }
        if (this.f8529 != null) {
            r0 += zzfjk.m12806(7, this.f8529.intValue());
        }
        if (this.f8530 != null) {
            r0 += zzfjk.m12814(8, this.f8530.longValue());
        }
        if (this.f8539 != null) {
            r0 += zzfjk.m12814(9, this.f8539.longValue());
        }
        if (this.f8540 != null) {
            r0 += zzfjk.m12814(10, this.f8540.longValue());
        }
        if (this.f8541 != null) {
            r0 += zzfjk.m12806(11, this.f8541.intValue());
        }
        if (this.f8534 != null) {
            r0 += zzfjk.m12814(12, this.f8534.longValue());
        }
        if (this.f8531 != null) {
            r0 += zzfjk.m12814(13, this.f8531.longValue());
        }
        if (this.f8532 != null) {
            r0 += zzfjk.m12814(14, this.f8532.longValue());
        }
        if (this.f8547 != null) {
            r0 += zzfjk.m12814(15, this.f8547.longValue());
        }
        if (this.f8548 != null) {
            r0 += zzfjk.m12814(16, this.f8548.longValue());
        }
        if (this.f8536 != null) {
            r0 += zzfjk.m12814(17, this.f8536.longValue());
        }
        if (this.f8537 != null) {
            r0 += zzfjk.m12814(18, this.f8537.longValue());
        }
        if (this.f8538 != null) {
            r0 += zzfjk.m12814(19, this.f8538.longValue());
        }
        if (this.f8533 != null) {
            r0 += zzfjk.m12814(20, this.f8533.longValue());
        }
        return this.f8535 != null ? r0 + zzfjk.m12814(21, this.f8535.longValue()) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9872(zzfjk zzfjk) throws IOException {
        if (this.f8546 != null) {
            zzfjk.m12824(1, this.f8546.longValue());
        }
        if (this.f8543 != null) {
            zzfjk.m12824(2, this.f8543.longValue());
        }
        if (this.f8545 != null) {
            zzfjk.m12824(3, this.f8545.longValue());
        }
        if (this.f8544 != null) {
            zzfjk.m12824(4, this.f8544.longValue());
        }
        if (this.f8542 != null) {
            zzfjk.m12824(5, this.f8542.longValue());
        }
        if (this.f8528 != null) {
            zzfjk.m12824(6, this.f8528.longValue());
        }
        if (this.f8529 != null) {
            zzfjk.m12832(7, this.f8529.intValue());
        }
        if (this.f8530 != null) {
            zzfjk.m12824(8, this.f8530.longValue());
        }
        if (this.f8539 != null) {
            zzfjk.m12824(9, this.f8539.longValue());
        }
        if (this.f8540 != null) {
            zzfjk.m12824(10, this.f8540.longValue());
        }
        if (this.f8541 != null) {
            zzfjk.m12832(11, this.f8541.intValue());
        }
        if (this.f8534 != null) {
            zzfjk.m12824(12, this.f8534.longValue());
        }
        if (this.f8531 != null) {
            zzfjk.m12824(13, this.f8531.longValue());
        }
        if (this.f8532 != null) {
            zzfjk.m12824(14, this.f8532.longValue());
        }
        if (this.f8547 != null) {
            zzfjk.m12824(15, this.f8547.longValue());
        }
        if (this.f8548 != null) {
            zzfjk.m12824(16, this.f8548.longValue());
        }
        if (this.f8536 != null) {
            zzfjk.m12824(17, this.f8536.longValue());
        }
        if (this.f8537 != null) {
            zzfjk.m12824(18, this.f8537.longValue());
        }
        if (this.f8538 != null) {
            zzfjk.m12824(19, this.f8538.longValue());
        }
        if (this.f8533 != null) {
            zzfjk.m12824(20, this.f8533.longValue());
        }
        if (this.f8535 != null) {
            zzfjk.m12824(21, this.f8535.longValue());
        }
        super.m12842(zzfjk);
    }
}
