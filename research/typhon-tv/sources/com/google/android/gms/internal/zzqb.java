package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzqb extends zzeu implements zzpz {
    zzqb(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegate");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13246() throws RemoteException {
        m12298(2, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13247(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(1, v_);
    }
}
