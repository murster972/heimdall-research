package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzadd extends zzadl {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f4016;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzade f4017;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzakd f4018;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f4019;

    public zzadd(Context context, zzv zzv, zzux zzux, zzakd zzakd) {
        this(context, zzakd, new zzade(context, zzv, zzjn.m5456(), zzux, zzakd));
    }

    private zzadd(Context context, zzakd zzakd, zzade zzade) {
        this.f4016 = new Object();
        this.f4019 = context;
        this.f4018 = zzakd;
        this.f4017 = zzade;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m4327() {
        String mediationAdapterClassName;
        synchronized (this.f4016) {
            mediationAdapterClassName = this.f4017.getMediationAdapterClassName();
        }
        return mediationAdapterClassName;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4328() {
        m4333((IObjectWrapper) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4329(IObjectWrapper iObjectWrapper) {
        synchronized (this.f4016) {
            Context context = iObjectWrapper == null ? null : (Context) zzn.m9307(iObjectWrapper);
            if (context != null) {
                try {
                    this.f4017.m4353(context);
                } catch (Exception e) {
                    zzagf.m4796("Unable to extract updated context.", e);
                }
            }
            this.f4017.resume();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4330() {
        boolean r0;
        synchronized (this.f4016) {
            r0 = this.f4017.m4347();
        }
        return r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4331() {
        m4329((IObjectWrapper) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4332() {
        m4335((IObjectWrapper) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4333(IObjectWrapper iObjectWrapper) {
        synchronized (this.f4016) {
            this.f4017.destroy();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4334() {
        synchronized (this.f4016) {
            this.f4017.m4343();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4335(IObjectWrapper iObjectWrapper) {
        synchronized (this.f4016) {
            this.f4017.pause();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4336(zzadp zzadp) {
        synchronized (this.f4016) {
            this.f4017.zza(zzadp);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4337(zzadv zzadv) {
        synchronized (this.f4016) {
            this.f4017.m4354(zzadv);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4338(String str) {
        synchronized (this.f4016) {
            this.f4017.setUserId(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4339(boolean z) {
        synchronized (this.f4016) {
            this.f4017.setImmersiveMode(z);
        }
    }
}
