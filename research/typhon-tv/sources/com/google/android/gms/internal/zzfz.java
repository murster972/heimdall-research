package com.google.android.gms.internal;

import android.view.View;
import java.lang.ref.WeakReference;

public final class zzfz implements zzhd {

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakReference<zzafo> f10640;

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<View> f10641;

    public zzfz(View view, zzafo zzafo) {
        this.f10641 = new WeakReference<>(view);
        this.f10640 = new WeakReference<>(zzafo);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12960() {
        return this.f10641.get() == null || this.f10640.get() == null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzhd m12961() {
        return new zzfy((View) this.f10641.get(), (zzafo) this.f10640.get());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m12962() {
        return (View) this.f10641.get();
    }
}
