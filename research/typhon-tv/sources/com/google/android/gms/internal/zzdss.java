package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsw;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdss extends zzffu<zzdss, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdss f10064;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdss> f10065;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f10066 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdsw f10067;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10068;

    public static final class zza extends zzffu.zza<zzdss, zza> implements zzfhg {
        private zza() {
            super(zzdss.f10064);
        }

        /* synthetic */ zza(zzdst zzdst) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11988(int i) {
            m12541();
            ((zzdss) this.f10398).m11977(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11989(zzdsw zzdsw) {
            m12541();
            ((zzdss) this.f10398).m11981(zzdsw);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11990(zzfes zzfes) {
            m12541();
            ((zzdss) this.f10398).m11975(zzfes);
            return this;
        }
    }

    static {
        zzdss zzdss = new zzdss();
        f10064 = zzdss;
        zzdss.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdss.f10394.m12718();
    }

    private zzdss() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzdss m11972() {
        return f10064;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zza m11974() {
        zzdss zzdss = f10064;
        zzffu.zza zza2 = (zzffu.zza) zzdss.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdss);
        return (zza) zza2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11975(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10066 = zzfes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdss m11976(zzfes zzfes) throws zzfge {
        return (zzdss) zzffu.m12526(f10064, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11977(int i) {
        this.f10068 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11981(zzdsw zzdsw) {
        if (zzdsw == null) {
            throw new NullPointerException();
        }
        this.f10067 = zzdsw;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdsw m11982() {
        return this.f10067 == null ? zzdsw.m12000() : this.f10067;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11983() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10068 != 0) {
            i2 = zzffg.m5245(1, this.f10068) + 0;
        }
        if (this.f10067 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10067 == null ? zzdsw.m12000() : this.f10067);
        }
        if (!this.f10066.isEmpty()) {
            i2 += zzffg.m5261(3, this.f10066);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m11984() {
        return this.f10066;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11985() {
        return this.f10068;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11986(int i, Object obj, Object obj2) {
        zzdsw.zza zza2;
        boolean z = true;
        switch (zzdst.f10069[i - 1]) {
            case 1:
                return new zzdss();
            case 2:
                return f10064;
            case 3:
                return null;
            case 4:
                return new zza((zzdst) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdss zzdss = (zzdss) obj2;
                this.f10068 = zzh.m12569(this.f10068 != 0, this.f10068, zzdss.f10068 != 0, zzdss.f10068);
                this.f10067 = (zzdsw) zzh.m12572(this.f10067, zzdss.f10067);
                boolean z2 = this.f10066 != zzfes.zzpfg;
                zzfes zzfes = this.f10066;
                if (zzdss.f10066 == zzfes.zzpfg) {
                    z = false;
                }
                this.f10066 = zzh.m12570(z2, zzfes, z, zzdss.f10066);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f10068 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f10067 != null) {
                                        zzdsw zzdsw = this.f10067;
                                        zzffu.zza zza3 = (zzffu.zza) zzdsw.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdsw);
                                        zza2 = (zzdsw.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10067 = (zzdsw) zzffb.m12416(zzdsw.m12000(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10067);
                                        this.f10067 = (zzdsw) zza2.m12543();
                                        break;
                                    }
                                case 26:
                                    this.f10066 = zzffb.m12401();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10065 == null) {
                    synchronized (zzdss.class) {
                        if (f10065 == null) {
                            f10065 = new zzffu.zzb(f10064);
                        }
                    }
                }
                return f10065;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10064;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11987(zzffg zzffg) throws IOException {
        if (this.f10068 != 0) {
            zzffg.m5281(1, this.f10068);
        }
        if (this.f10067 != null) {
            zzffg.m5289(2, (zzfhe) this.f10067 == null ? zzdsw.m12000() : this.f10067);
        }
        if (!this.f10066.isEmpty()) {
            zzffg.m5288(3, this.f10066);
        }
        this.f10394.m12719(zzffg);
    }
}
