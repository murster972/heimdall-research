package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzadi extends zzev implements zzadh {
    public zzadi() {
        attachInterface(this, "com.google.android.gms.ads.internal.reward.client.IRewardItem");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                String r1 = m9466();
                parcel2.writeNoException();
                parcel2.writeString(r1);
                return true;
            case 2:
                int r12 = m9465();
                parcel2.writeNoException();
                parcel2.writeInt(r12);
                return true;
            default:
                return false;
        }
    }
}
