package com.google.android.gms.internal;

@zzzv
public final class zzakc implements zzaju {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f4292;

    public zzakc() {
        this((String) null);
    }

    public zzakc(String str) {
        this.f4292 = str;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4799(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r1 = "Pinging URL: "
            java.lang.String r0 = java.lang.String.valueOf(r5)     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            int r2 = r0.length()     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            if (r2 == 0) goto L_0x0070
            java.lang.String r0 = r1.concat(r0)     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
        L_0x0011:
            com.google.android.gms.internal.zzakb.m4792(r0)     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            java.net.URL r0 = new java.net.URL     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            r0.<init>(r5)     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            com.google.android.gms.internal.zzkb.m5487()     // Catch:{ all -> 0x00b1 }
            r1 = 1
            java.lang.String r2 = r4.f4292     // Catch:{ all -> 0x00b1 }
            com.google.android.gms.internal.zzajr.m4765((boolean) r1, (java.net.HttpURLConnection) r0, (java.lang.String) r2)     // Catch:{ all -> 0x00b1 }
            com.google.android.gms.internal.zzajv r1 = new com.google.android.gms.internal.zzajv     // Catch:{ all -> 0x00b1 }
            r1.<init>()     // Catch:{ all -> 0x00b1 }
            r2 = 0
            r1.m4788((java.net.HttpURLConnection) r0, (byte[]) r2)     // Catch:{ all -> 0x00b1 }
            int r2 = r0.getResponseCode()     // Catch:{ all -> 0x00b1 }
            r1.m4787((java.net.HttpURLConnection) r0, (int) r2)     // Catch:{ all -> 0x00b1 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r2 < r1) goto L_0x0040
            r1 = 300(0x12c, float:4.2E-43)
            if (r2 < r1) goto L_0x006c
        L_0x0040:
            java.lang.String r1 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x00b1 }
            int r1 = r1.length()     // Catch:{ all -> 0x00b1 }
            int r1 = r1 + 65
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b1 }
            r3.<init>(r1)     // Catch:{ all -> 0x00b1 }
            java.lang.String r1 = "Received non-success response code "
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x00b1 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00b1 }
            java.lang.String r2 = " from pinging URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00b1 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x00b1 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00b1 }
            com.google.android.gms.internal.zzakb.m4791(r1)     // Catch:{ all -> 0x00b1 }
        L_0x006c:
            r0.disconnect()     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
        L_0x006f:
            return
        L_0x0070:
            java.lang.String r0 = new java.lang.String     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            r0.<init>(r1)     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            goto L_0x0011
        L_0x0076:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r5)
            int r1 = r1.length()
            int r1 = r1 + 32
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r2 = r2.length()
            int r1 = r1 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r1)
            java.lang.String r1 = "Error while parsing ping URL: "
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = ". "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.google.android.gms.internal.zzakb.m4791(r0)
            goto L_0x006f
        L_0x00b1:
            r1 = move-exception
            r0.disconnect()     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
            throw r1     // Catch:{ IndexOutOfBoundsException -> 0x0076, IOException -> 0x00b6, RuntimeException -> 0x00f2 }
        L_0x00b6:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r5)
            int r1 = r1.length()
            int r1 = r1 + 27
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r2 = r2.length()
            int r1 = r1 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r1)
            java.lang.String r1 = "Error while pinging URL: "
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = ". "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.google.android.gms.internal.zzakb.m4791(r0)
            goto L_0x006f
        L_0x00f2:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r5)
            int r1 = r1.length()
            int r1 = r1 + 27
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r2 = r2.length()
            int r1 = r1 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r1)
            java.lang.String r1 = "Error while pinging URL: "
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = ". "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.google.android.gms.internal.zzakb.m4791(r0)
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzakc.m4799(java.lang.String):void");
    }
}
