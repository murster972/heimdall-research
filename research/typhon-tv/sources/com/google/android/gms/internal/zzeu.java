package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public class zzeu implements IInterface {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f10288;

    /* renamed from: 龘  reason: contains not printable characters */
    private final IBinder f10289;

    protected zzeu(IBinder iBinder, String str) {
        this.f10289 = iBinder;
        this.f10288 = str;
    }

    public IBinder asBinder() {
        return this.f10289;
    }

    /* access modifiers changed from: protected */
    public final Parcel v_() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f10288);
        return obtain;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12298(int i, Parcel parcel) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        try {
            this.f10289.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12299(int i, Parcel parcel) throws RemoteException {
        try {
            this.f10289.transact(i, parcel, (Parcel) null, 1);
        } finally {
            parcel.recycle();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Parcel m12300(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.f10289.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }
}
