package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzve extends zzev implements zzvd {
    public zzve() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zzvg zzvi;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m13513();
                break;
            case 2:
                m13510();
                break;
            case 3:
                m13514(parcel.readInt());
                break;
            case 4:
                m13512();
                break;
            case 5:
                m13511();
                break;
            case 6:
                m13509();
                break;
            case 7:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    zzvi = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationResponseMetadata");
                    zzvi = queryLocalInterface instanceof zzvg ? (zzvg) queryLocalInterface : new zzvi(readStrongBinder);
                }
                m13516(zzvi);
                break;
            case 8:
                m13507();
                break;
            case 9:
                m13517(parcel.readString(), parcel.readString());
                break;
            case 10:
                m13515(zzqn.m13329(parcel.readStrongBinder()), parcel.readString());
                break;
            case 11:
                m13508();
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
