package com.google.android.gms.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;

@zzzv
public final class zzrq extends zzd<zzrv> {
    zzrq(Context context, Looper looper, zzf zzf, zzg zzg) {
        super(context, looper, 166, zzf, zzg, (String) null);
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.ads.service.HTTP";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzrv m5806() throws DeadObjectException {
        return (zzrv) super.m9171();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5807() {
        return "com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m5808(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService");
        return queryLocalInterface instanceof zzrv ? (zzrv) queryLocalInterface : new zzrw(iBinder);
    }
}
