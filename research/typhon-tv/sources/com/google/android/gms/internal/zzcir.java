package com.google.android.gms.internal;

import android.os.Binder;
import android.text.TextUtils;
import com.google.android.exoplayer2.C;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.common.zzp;
import com.google.android.gms.common.zzq;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public final class zzcir extends zzchf {

    /* renamed from: 靐  reason: contains not printable characters */
    private Boolean f9429;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f9430;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcim f9431;

    public zzcir(zzcim zzcim) {
        this(zzcim, (String) null);
    }

    private zzcir(zzcim zzcim, String str) {
        zzbq.m9120(zzcim);
        this.f9431 = zzcim;
        this.f9430 = null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m11071(zzcgi zzcgi, boolean z) {
        zzbq.m9120(zzcgi);
        m11073(zzcgi.f9143, false);
        this.f9431.m11063().m11398(zzcgi.f9140);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11073(String str, boolean z) {
        boolean z2 = false;
        if (TextUtils.isEmpty(str)) {
            this.f9431.m11016().m10832().m10849("Measurement Service called without app package");
            throw new SecurityException("Measurement Service called without app package");
        }
        if (z) {
            try {
                if (this.f9429 == null) {
                    if ("com.google.android.gms".equals(this.f9430) || zzx.m9280(this.f9431.m11021(), Binder.getCallingUid()) || zzq.m9300(this.f9431.m11021()).m9303(Binder.getCallingUid())) {
                        z2 = true;
                    }
                    this.f9429 = Boolean.valueOf(z2);
                }
                if (this.f9429.booleanValue()) {
                    return;
                }
            } catch (SecurityException e) {
                this.f9431.m11016().m10832().m10850("Measurement Service called with invalid calling package. appId", zzchm.m10812(str));
                throw e;
            }
        }
        if (this.f9430 == null && zzp.zzb(this.f9431.m11021(), Binder.getCallingUid(), str)) {
            this.f9430 = str;
        }
        if (!str.equals(this.f9430)) {
            throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11074(zzcgi zzcgi) {
        m11071(zzcgi, false);
        this.f9431.m11018().m10986((Runnable) new zzcis(this, zzcgi));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m11075(zzcgi zzcgi) {
        m11073(zzcgi.f9143, false);
        this.f9431.m11018().m10986((Runnable) new zzcjb(this, zzcgi));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m11076(zzcgi zzcgi) {
        m11071(zzcgi, false);
        return this.f9431.m11050(zzcgi.f9143);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcln> m11077(zzcgi zzcgi, boolean z) {
        m11071(zzcgi, false);
        try {
            List<zzclp> list = (List) this.f9431.m11018().m10984(new zzcjh(this, zzcgi)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (zzclp zzclp : list) {
                if (z || !zzclq.m11368(zzclp.f9657)) {
                    arrayList.add(new zzcln(zzclp));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f9431.m11016().m10832().m10851("Failed to get user attributes. appId", zzchm.m10812(zzcgi.f9143), e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcgl> m11078(String str, String str2, zzcgi zzcgi) {
        m11071(zzcgi, false);
        try {
            return (List) this.f9431.m11018().m10984(new zzciz(this, zzcgi, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f9431.m11016().m10832().m10850("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcgl> m11079(String str, String str2, String str3) {
        m11073(str, true);
        try {
            return (List) this.f9431.m11018().m10984(new zzcja(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f9431.m11016().m10832().m10850("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcln> m11080(String str, String str2, String str3, boolean z) {
        m11073(str, true);
        try {
            List<zzclp> list = (List) this.f9431.m11018().m10984(new zzciy(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (zzclp zzclp : list) {
                if (z || !zzclq.m11368(zzclp.f9657)) {
                    arrayList.add(new zzcln(zzclp));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f9431.m11016().m10832().m10851("Failed to get user attributes. appId", zzchm.m10812(str), e);
            return Collections.emptyList();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcln> m11081(String str, String str2, boolean z, zzcgi zzcgi) {
        m11071(zzcgi, false);
        try {
            List<zzclp> list = (List) this.f9431.m11018().m10984(new zzcix(this, zzcgi, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (zzclp zzclp : list) {
                if (z || !zzclq.m11368(zzclp.f9657)) {
                    arrayList.add(new zzcln(zzclp));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f9431.m11016().m10832().m10851("Failed to get user attributes. appId", zzchm.m10812(zzcgi.f9143), e);
            return Collections.emptyList();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11082(long j, String str, String str2, String str3) {
        this.f9431.m11018().m10986((Runnable) new zzcjj(this, str2, str3, str, j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11083(zzcgi zzcgi) {
        m11071(zzcgi, false);
        zzcji zzcji = new zzcji(this, zzcgi);
        if (this.f9431.m11018().m10976()) {
            zzcji.run();
        } else {
            this.f9431.m11018().m10986((Runnable) zzcji);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11084(zzcgl zzcgl) {
        zzbq.m9120(zzcgl);
        zzbq.m9120(zzcgl.f9155);
        m11073(zzcgl.f9156, true);
        zzcgl zzcgl2 = new zzcgl(zzcgl);
        if (zzcgl.f9155.m11364() == null) {
            this.f9431.m11018().m10986((Runnable) new zzciv(this, zzcgl2));
        } else {
            this.f9431.m11018().m10986((Runnable) new zzciw(this, zzcgl2));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11085(zzcgl zzcgl, zzcgi zzcgi) {
        zzbq.m9120(zzcgl);
        zzbq.m9120(zzcgl.f9155);
        m11071(zzcgi, false);
        zzcgl zzcgl2 = new zzcgl(zzcgl);
        zzcgl2.f9156 = zzcgi.f9143;
        if (zzcgl.f9155.m11364() == null) {
            this.f9431.m11018().m10986((Runnable) new zzcit(this, zzcgl2, zzcgi));
        } else {
            this.f9431.m11018().m10986((Runnable) new zzciu(this, zzcgl2, zzcgi));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11086(zzcha zzcha, zzcgi zzcgi) {
        zzbq.m9120(zzcha);
        m11071(zzcgi, false);
        this.f9431.m11018().m10986((Runnable) new zzcjc(this, zzcha, zzcgi));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11087(zzcha zzcha, String str, String str2) {
        zzbq.m9120(zzcha);
        zzbq.m9122(str);
        m11073(str, true);
        this.f9431.m11018().m10986((Runnable) new zzcjd(this, zzcha, str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11088(zzcln zzcln, zzcgi zzcgi) {
        zzbq.m9120(zzcln);
        m11071(zzcgi, false);
        if (zzcln.m11364() == null) {
            this.f9431.m11018().m10986((Runnable) new zzcjf(this, zzcln, zzcgi));
        } else {
            this.f9431.m11018().m10986((Runnable) new zzcjg(this, zzcln, zzcgi));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m11089(zzcha zzcha, String str) {
        zzbq.m9122(str);
        zzbq.m9120(zzcha);
        m11073(str, true);
        this.f9431.m11016().m10845().m10850("Log and bundle. event", this.f9431.m11064().m10805(zzcha.f9203));
        long r2 = this.f9431.m11023().m9242() / C.MICROS_PER_SECOND;
        try {
            byte[] bArr = (byte[]) this.f9431.m11018().m10979(new zzcje(this, zzcha, str)).get();
            if (bArr == null) {
                this.f9431.m11016().m10832().m10850("Log and bundle returned null. appId", zzchm.m10812(str));
                bArr = new byte[0];
            }
            this.f9431.m11016().m10845().m10852("Log and bundle processed. event, size, time_ms", this.f9431.m11064().m10805(zzcha.f9203), Integer.valueOf(bArr.length), Long.valueOf((this.f9431.m11023().m9242() / C.MICROS_PER_SECOND) - r2));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.f9431.m11016().m10832().m10852("Failed to log and bundle. appId, event, error", zzchm.m10812(str), this.f9431.m11064().m10805(zzcha.f9203), e);
            return null;
        }
    }
}
