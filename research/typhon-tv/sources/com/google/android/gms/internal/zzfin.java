package com.google.android.gms.internal;

abstract class zzfin<T, B> {
    zzfin() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract int m12708(T t);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m12709(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m12710(T t, zzfji zzfji);
}
