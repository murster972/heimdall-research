package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzcbu extends zzeu implements zzcbt {
    zzcbu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gass.internal.IGassService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcbr m10326(zzcbp zzcbp) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcbp);
        Parcel r1 = m12300(1, v_);
        zzcbr zzcbr = (zzcbr) zzew.m12304(r1, zzcbr.CREATOR);
        r1.recycle();
        return zzcbr;
    }
}
