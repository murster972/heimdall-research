package com.google.android.gms.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzahm implements ThreadFactory {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8207;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicInteger f8208 = new AtomicInteger(1);

    zzahm(String str) {
        this.f8207 = str;
    }

    public final Thread newThread(Runnable runnable) {
        String str = this.f8207;
        return new Thread(runnable, new StringBuilder(String.valueOf(str).length() + 23).append("AdWorker(").append(str).append(") #").append(this.f8208.getAndIncrement()).toString());
    }
}
