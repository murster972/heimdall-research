package com.google.android.gms.internal;

final class zzcjt implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcjn f9512;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ long f9513;

    zzcjt(zzcjn zzcjn, long j) {
        this.f9512 = zzcjn;
        this.f9513 = j;
    }

    public final void run() {
        this.f9512.m11098().f9315.m10903(this.f9513);
        this.f9512.m11096().m10845().m10850("Minimum session duration set", Long.valueOf(this.f9513));
    }
}
