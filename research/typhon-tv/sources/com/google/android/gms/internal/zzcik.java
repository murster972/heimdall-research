package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

final class zzcik<V> extends FutureTask<V> implements Comparable<zzcik> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f9371 = zzcih.f9358.getAndIncrement();

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzcih f9372;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f9373;

    /* renamed from: 龘  reason: contains not printable characters */
    final boolean f9374;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcik(zzcih zzcih, Runnable runnable, boolean z, String str) {
        super(runnable, (Object) null);
        this.f9372 = zzcih;
        zzbq.m9120(str);
        this.f9373 = str;
        this.f9374 = false;
        if (this.f9371 == Long.MAX_VALUE) {
            zzcih.m11096().m10832().m10849("Tasks index overflow");
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcik(zzcih zzcih, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.f9372 = zzcih;
        zzbq.m9120(str);
        this.f9373 = str;
        this.f9374 = z;
        if (this.f9371 == Long.MAX_VALUE) {
            zzcih.m11096().m10832().m10849("Tasks index overflow");
        }
    }

    public final /* synthetic */ int compareTo(Object obj) {
        zzcik zzcik = (zzcik) obj;
        if (this.f9374 != zzcik.f9374) {
            return this.f9374 ? -1 : 1;
        }
        if (this.f9371 < zzcik.f9371) {
            return -1;
        }
        if (this.f9371 > zzcik.f9371) {
            return 1;
        }
        this.f9372.m11096().m10833().m10850("Two tasks share the same index. index", Long.valueOf(this.f9371));
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void setException(Throwable th) {
        this.f9372.m11096().m10832().m10850(this.f9373, th);
        if (th instanceof zzcii) {
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
        }
        super.setException(th);
    }
}
