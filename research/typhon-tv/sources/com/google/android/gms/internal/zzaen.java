package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzaen extends zzev implements zzaem {
    public zzaen() {
        attachInterface(this, "com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzaem m9549(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
        return queryLocalInterface instanceof zzaem ? (zzaem) queryLocalInterface : new zzaeo(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m9546(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            case 2:
                m9547(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 3:
                m9542(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            case 4:
                m9545(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            case 5:
                m9544(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            case 6:
                m9541(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            case 7:
                m9548(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), (zzaeq) zzew.m12304(parcel, zzaeq.CREATOR));
                break;
            case 8:
                m9539(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            case 9:
                m9543(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 10:
                m9540(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
