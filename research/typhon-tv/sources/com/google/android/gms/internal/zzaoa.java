package com.google.android.gms.internal;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zze;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzaoa extends zzlm {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f4517;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public zzlo f4518;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f4519;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4520;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f4521;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f4522 = true;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f4523 = true;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f4524;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f4525;

    /* renamed from: 连任  reason: contains not printable characters */
    private final float f4526;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f4527 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f4528;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f4529;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzamp f4530;

    public zzaoa(zzamp zzamp, float f, boolean z, boolean z2) {
        this.f4530 = zzamp;
        this.f4526 = f;
        this.f4529 = z;
        this.f4528 = z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5198(String str, Map<String, String> map) {
        HashMap hashMap = map == null ? new HashMap() : new HashMap(map);
        hashMap.put("action", str);
        zzbs.zzei();
        zzahn.m4612((Runnable) new zzaob(this, hashMap));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final float m5200() {
        return this.f4526;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final float m5201() {
        float f;
        synchronized (this.f4527) {
            f = this.f4524;
        }
        return f;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzlo m5202() throws RemoteException {
        zzlo zzlo;
        synchronized (this.f4527) {
            zzlo = this.f4518;
        }
        return zzlo;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m5203() {
        boolean z;
        synchronized (this.f4527) {
            z = this.f4529 && this.f4520;
        }
        return z;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m5204() {
        boolean z;
        boolean r0 = m5203();
        synchronized (this.f4527) {
            if (!r0) {
                if (this.f4521 && this.f4528) {
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final float m5205() {
        float f;
        synchronized (this.f4527) {
            f = this.f4525;
        }
        return f;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5206() {
        m5198("pause", (Map<String, String>) null);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m5207() {
        int i;
        synchronized (this.f4527) {
            i = this.f4517;
        }
        return i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5208() {
        boolean z;
        synchronized (this.f4527) {
            z = this.f4523;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5209() {
        m5198("play", (Map<String, String>) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5210(float f, int i, boolean z, float f2) {
        boolean z2;
        int i2;
        synchronized (this.f4527) {
            this.f4524 = f;
            z2 = this.f4523;
            this.f4523 = z;
            i2 = this.f4517;
            this.f4517 = i;
            float f3 = this.f4525;
            this.f4525 = f2;
            if (Math.abs(this.f4525 - f3) > 1.0E-4f) {
                zzamp zzamp = this.f4530;
                if (zzamp == null) {
                    throw null;
                }
                ((View) zzamp).invalidate();
            }
        }
        zzbs.zzei();
        zzahn.m4612((Runnable) new zzaoc(this, i2, i, z2, z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5211(zzlo zzlo) {
        synchronized (this.f4527) {
            this.f4518 = zzlo;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5212(zzmr zzmr) {
        synchronized (this.f4527) {
            this.f4522 = zzmr.f4879;
            this.f4520 = zzmr.f4877;
            this.f4521 = zzmr.f4878;
        }
        m5198("initialState", (Map<String, String>) zze.m9245("muteStart", zzmr.f4879 ? PubnativeRequest.LEGACY_ZONE_ID : "0", "customControlsRequested", zzmr.f4877 ? PubnativeRequest.LEGACY_ZONE_ID : "0", "clickToExpandRequested", zzmr.f4878 ? PubnativeRequest.LEGACY_ZONE_ID : "0"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5213(boolean z) {
        m5198(z ? "mute" : "unmute", (Map<String, String>) null);
    }
}
