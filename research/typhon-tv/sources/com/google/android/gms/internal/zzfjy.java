package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfjy extends zzfjm<zzfjy> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile zzfjy[] f10581;

    /* renamed from: 靐  reason: contains not printable characters */
    public byte[] f10582 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public byte[] f10583 = null;

    public zzfjy() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzfjy[] m12891() {
        if (f10581 == null) {
            synchronized (zzfjq.f10546) {
                if (f10581 == null) {
                    f10581 = new zzfjy[0];
                }
            }
        }
        return f10581;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12892() {
        int r0 = super.m12841() + zzfjk.m12809(1, this.f10583);
        return this.f10582 != null ? r0 + zzfjk.m12809(2, this.f10582) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12893(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10583 = zzfjj.m12784();
                    continue;
                case 18:
                    this.f10582 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12894(zzfjk zzfjk) throws IOException {
        zzfjk.m12837(1, this.f10583);
        if (this.f10582 != null) {
            zzfjk.m12837(2, this.f10582);
        }
        super.m12842(zzfjk);
    }
}
