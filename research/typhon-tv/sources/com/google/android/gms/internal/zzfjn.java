package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfjm;
import java.io.IOException;

public final class zzfjn<M extends zzfjm<M>, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f10534;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10535;

    /* renamed from: 齉  reason: contains not printable characters */
    protected final boolean f10536;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Class<T> f10537;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfjn)) {
            return false;
        }
        zzfjn zzfjn = (zzfjn) obj;
        return this.f10535 == zzfjn.f10535 && this.f10537 == zzfjn.f10537 && this.f10534 == zzfjn.f10534 && this.f10536 == zzfjn.f10536;
    }

    public final int hashCode() {
        return (this.f10536 ? 1 : 0) + ((((((this.f10535 + 1147) * 31) + this.f10537.hashCode()) * 31) + this.f10534) * 31);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12844(Object obj) {
        int i = this.f10534 >>> 3;
        switch (this.f10535) {
            case 10:
                return (zzfjk.m12805(i) << 1) + ((zzfjs) obj).m12872();
            case 11:
                return zzfjk.m12807(i, (zzfjs) obj);
            default:
                throw new IllegalArgumentException(new StringBuilder(24).append("Unknown type ").append(this.f10535).toString());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12845(Object obj, zzfjk zzfjk) {
        try {
            zzfjk.m12825(this.f10534);
            switch (this.f10535) {
                case 10:
                    ((zzfjs) obj).m12877(zzfjk);
                    zzfjk.m12826(this.f10534 >>> 3, 4);
                    return;
                case 11:
                    zzfjk.m12838((zzfjs) obj);
                    return;
                default:
                    throw new IllegalArgumentException(new StringBuilder(24).append("Unknown type ").append(this.f10535).toString());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        throw new IllegalStateException(e);
    }
}
