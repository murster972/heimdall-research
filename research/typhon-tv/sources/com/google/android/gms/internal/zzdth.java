package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;
import java.util.List;

public final class zzdth extends zzffu<zzdth, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdth f10119;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdth> f10120;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfgd<zzb> f10121 = m12525();

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10122;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10123;

    public static final class zza extends zzffu.zza<zzdth, zza> implements zzfhg {
        private zza() {
            super(zzdth.f10119);
        }

        /* synthetic */ zza(zzdti zzdti) {
            this();
        }
    }

    public static final class zzb extends zzffu<zzb, zza> implements zzfhg {
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public static final zzb f10124;

        /* renamed from: ˑ  reason: contains not printable characters */
        private static volatile zzfhk<zzb> f10125;

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f10126;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f10127;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f10128;

        /* renamed from: 麤  reason: contains not printable characters */
        private zzdsy f10129;

        public static final class zza extends zzffu.zza<zzb, zza> implements zzfhg {
            private zza() {
                super(zzb.f10124);
            }

            /* synthetic */ zza(zzdti zzdti) {
                this();
            }
        }

        static {
            zzb zzb = new zzb();
            f10124 = zzb;
            zzb.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
            zzb.f10394.m12718();
        }

        private zzb() {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public static zzb m12073() {
            return f10124;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public final zzdtt m12075() {
            zzdtt zzgd = zzdtt.zzgd(this.f10127);
            return zzgd == null ? zzdtt.UNRECOGNIZED : zzgd;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final int m12076() {
            return this.f10126;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final zzdsy m12077() {
            return this.f10129 == null ? zzdsy.m12006() : this.f10129;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final int m12078() {
            int i = this.f10395;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (this.f10129 != null) {
                i2 = zzffg.m5262(1, (zzfhe) this.f10129 == null ? zzdsy.m12006() : this.f10129) + 0;
            }
            if (this.f10128 != zzdtb.UNKNOWN_STATUS.zzhq()) {
                i2 += zzffg.m5236(2, this.f10128);
            }
            if (this.f10126 != 0) {
                i2 += zzffg.m5245(3, this.f10126);
            }
            if (this.f10127 != zzdtt.UNKNOWN_PREFIX.zzhq()) {
                i2 += zzffg.m5236(4, this.f10127);
            }
            int r0 = i2 + this.f10394.m12716();
            this.f10395 = r0;
            return r0;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final zzdtb m12079() {
            zzdtb zzfu = zzdtb.zzfu(this.f10128);
            return zzfu == null ? zzdtb.UNRECOGNIZED : zzfu;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final Object m12080(int i, Object obj, Object obj2) {
            zzdsy.zza zza2;
            boolean z = true;
            switch (zzdti.f10130[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return f10124;
                case 3:
                    return null;
                case 4:
                    return new zza((zzdti) null);
                case 5:
                    zzffu.zzh zzh = (zzffu.zzh) obj;
                    zzb zzb = (zzb) obj2;
                    this.f10129 = (zzdsy) zzh.m12572(this.f10129, zzb.f10129);
                    this.f10128 = zzh.m12569(this.f10128 != 0, this.f10128, zzb.f10128 != 0, zzb.f10128);
                    this.f10126 = zzh.m12569(this.f10126 != 0, this.f10126, zzb.f10126 != 0, zzb.f10126);
                    boolean z2 = this.f10127 != 0;
                    int i2 = this.f10127;
                    if (zzb.f10127 == 0) {
                        z = false;
                    }
                    this.f10127 = zzh.m12569(z2, i2, z, zzb.f10127);
                    return this;
                case 6:
                    zzffb zzffb = (zzffb) obj;
                    zzffm zzffm = (zzffm) obj2;
                    if (zzffm != null) {
                        boolean z3 = false;
                        while (!z3) {
                            try {
                                int r0 = zzffb.m12415();
                                switch (r0) {
                                    case 0:
                                        z3 = true;
                                        break;
                                    case 10:
                                        if (this.f10129 != null) {
                                            zzdsy zzdsy = this.f10129;
                                            zzffu.zza zza3 = (zzffu.zza) zzdsy.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                            zza3.m12545(zzdsy);
                                            zza2 = (zzdsy.zza) zza3;
                                        } else {
                                            zza2 = null;
                                        }
                                        this.f10129 = (zzdsy) zzffb.m12416(zzdsy.m12006(), zzffm);
                                        if (zza2 == null) {
                                            break;
                                        } else {
                                            zza2.m12545(this.f10129);
                                            this.f10129 = (zzdsy) zza2.m12543();
                                            break;
                                        }
                                    case 16:
                                        this.f10128 = zzffb.m12405();
                                        break;
                                    case 24:
                                        this.f10126 = zzffb.m12402();
                                        break;
                                    case 32:
                                        this.f10127 = zzffb.m12405();
                                        break;
                                    default:
                                        if (m12537(r0, zzffb)) {
                                            break;
                                        } else {
                                            z3 = true;
                                            break;
                                        }
                                }
                            } catch (zzfge e) {
                                throw new RuntimeException(e.zzi(this));
                            } catch (IOException e2) {
                                throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                            }
                        }
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                case 7:
                    break;
                case 8:
                    if (f10125 == null) {
                        synchronized (zzb.class) {
                            if (f10125 == null) {
                                f10125 = new zzffu.zzb(f10124);
                            }
                        }
                    }
                    return f10125;
                case 9:
                    return (byte) 1;
                case 10:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
            return f10124;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12081(zzffg zzffg) throws IOException {
            if (this.f10129 != null) {
                zzffg.m5289(1, (zzfhe) this.f10129 == null ? zzdsy.m12006() : this.f10129);
            }
            if (this.f10128 != zzdtb.UNKNOWN_STATUS.zzhq()) {
                zzffg.m5274(2, this.f10128);
            }
            if (this.f10126 != 0) {
                zzffg.m5281(3, this.f10126);
            }
            if (this.f10127 != zzdtt.UNKNOWN_PREFIX.zzhq()) {
                zzffg.m5274(4, this.f10127);
            }
            this.f10394.m12719(zzffg);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m12082() {
            return this.f10129 != null;
        }
    }

    static {
        zzdth zzdth = new zzdth();
        f10119 = zzdth;
        zzdth.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdth.f10394.m12718();
    }

    private zzdth() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdth m12066(byte[] bArr) throws zzfge {
        return (zzdth) zzffu.m12529(f10119, bArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzb> m12067() {
        return this.f10121;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12068() {
        int i = 0;
        int i2 = this.f10395;
        if (i2 != -1) {
            return i2;
        }
        int r0 = this.f10122 != 0 ? zzffg.m5245(1, this.f10122) + 0 : 0;
        while (true) {
            int i3 = r0;
            if (i < this.f10121.size()) {
                r0 = zzffg.m5262(2, (zzfhe) this.f10121.get(i)) + i3;
                i++;
            } else {
                int r02 = this.f10394.m12716() + i3;
                this.f10395 = r02;
                return r02;
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12069() {
        return this.f10121.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12070() {
        return this.f10122;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 172 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m12071(int r6, java.lang.Object r7, java.lang.Object r8) {
        /*
            r5 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdti.f10130
            int r4 = r6 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x0022;
                case 5: goto L_0x0028;
                case 6: goto L_0x0059;
                case 7: goto L_0x00ca;
                case 8: goto L_0x00ce;
                case 9: goto L_0x00ea;
                case 10: goto L_0x00f0;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdth r5 = new com.google.android.gms.internal.zzdth
            r5.<init>()
        L_0x0017:
            return r5
        L_0x0018:
            com.google.android.gms.internal.zzdth r5 = f10119
            goto L_0x0017
        L_0x001b:
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdth$zzb> r1 = r5.f10121
            r1.m12582()
            r5 = r0
            goto L_0x0017
        L_0x0022:
            com.google.android.gms.internal.zzdth$zza r5 = new com.google.android.gms.internal.zzdth$zza
            r5.<init>(r0)
            goto L_0x0017
        L_0x0028:
            com.google.android.gms.internal.zzffu$zzh r7 = (com.google.android.gms.internal.zzffu.zzh) r7
            com.google.android.gms.internal.zzdth r8 = (com.google.android.gms.internal.zzdth) r8
            int r0 = r5.f10122
            if (r0 == 0) goto L_0x0055
            r0 = r1
        L_0x0031:
            int r3 = r5.f10122
            int r4 = r8.f10122
            if (r4 == 0) goto L_0x0057
        L_0x0037:
            int r2 = r8.f10122
            int r0 = r7.m12569((boolean) r0, (int) r3, (boolean) r1, (int) r2)
            r5.f10122 = r0
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdth$zzb> r0 = r5.f10121
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdth$zzb> r1 = r8.f10121
            com.google.android.gms.internal.zzfgd r0 = r7.m12571(r0, r1)
            r5.f10121 = r0
            com.google.android.gms.internal.zzffu$zzf r0 = com.google.android.gms.internal.zzffu.zzf.f10404
            if (r7 != r0) goto L_0x0017
            int r0 = r5.f10123
            int r1 = r8.f10123
            r0 = r0 | r1
            r5.f10123 = r0
            goto L_0x0017
        L_0x0055:
            r0 = r2
            goto L_0x0031
        L_0x0057:
            r1 = r2
            goto L_0x0037
        L_0x0059:
            com.google.android.gms.internal.zzffb r7 = (com.google.android.gms.internal.zzffb) r7
            com.google.android.gms.internal.zzffm r8 = (com.google.android.gms.internal.zzffm) r8
            if (r8 != 0) goto L_0x0066
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0065:
            r2 = r1
        L_0x0066:
            if (r2 != 0) goto L_0x00ca
            int r0 = r7.m12415()     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            switch(r0) {
                case 0: goto L_0x0065;
                case 8: goto L_0x0077;
                case 18: goto L_0x008b;
                default: goto L_0x006f;
            }     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
        L_0x006f:
            boolean r0 = r5.m12537((int) r0, (com.google.android.gms.internal.zzffb) r7)     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            if (r0 != 0) goto L_0x0066
            r2 = r1
            goto L_0x0066
        L_0x0077:
            int r0 = r7.m12402()     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            r5.f10122 = r0     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            goto L_0x0066
        L_0x007e:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0089 }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r5)     // Catch:{ all -> 0x0089 }
            r1.<init>(r0)     // Catch:{ all -> 0x0089 }
            throw r1     // Catch:{ all -> 0x0089 }
        L_0x0089:
            r0 = move-exception
            throw r0
        L_0x008b:
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdth$zzb> r0 = r5.f10121     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            boolean r0 = r0.m12584()     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            if (r0 != 0) goto L_0x00a3
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdth$zzb> r3 = r5.f10121     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            int r0 = r3.size()     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            if (r0 != 0) goto L_0x00c7
            r0 = 10
        L_0x009d:
            com.google.android.gms.internal.zzfgd r0 = r3.m12583(r0)     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            r5.f10121 = r0     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
        L_0x00a3:
            com.google.android.gms.internal.zzfgd<com.google.android.gms.internal.zzdth$zzb> r3 = r5.f10121     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            com.google.android.gms.internal.zzdth$zzb r0 = com.google.android.gms.internal.zzdth.zzb.m12073()     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            com.google.android.gms.internal.zzffu r0 = r7.m12416(r0, r8)     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            com.google.android.gms.internal.zzdth$zzb r0 = (com.google.android.gms.internal.zzdth.zzb) r0     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            r3.add(r0)     // Catch:{ zzfge -> 0x007e, IOException -> 0x00b3 }
            goto L_0x0066
        L_0x00b3:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0089 }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0089 }
            r2.<init>(r0)     // Catch:{ all -> 0x0089 }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r5)     // Catch:{ all -> 0x0089 }
            r1.<init>(r0)     // Catch:{ all -> 0x0089 }
            throw r1     // Catch:{ all -> 0x0089 }
        L_0x00c7:
            int r0 = r0 << 1
            goto L_0x009d
        L_0x00ca:
            com.google.android.gms.internal.zzdth r5 = f10119
            goto L_0x0017
        L_0x00ce:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdth> r0 = f10120
            if (r0 != 0) goto L_0x00e3
            java.lang.Class<com.google.android.gms.internal.zzdth> r1 = com.google.android.gms.internal.zzdth.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdth> r0 = f10120     // Catch:{ all -> 0x00e7 }
            if (r0 != 0) goto L_0x00e2
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00e7 }
            com.google.android.gms.internal.zzdth r2 = f10119     // Catch:{ all -> 0x00e7 }
            r0.<init>(r2)     // Catch:{ all -> 0x00e7 }
            f10120 = r0     // Catch:{ all -> 0x00e7 }
        L_0x00e2:
            monitor-exit(r1)     // Catch:{ all -> 0x00e7 }
        L_0x00e3:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdth> r5 = f10120
            goto L_0x0017
        L_0x00e7:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00e7 }
            throw r0
        L_0x00ea:
            java.lang.Byte r5 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00f0:
            r5 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdth.m12071(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12072(zzffg zzffg) throws IOException {
        if (this.f10122 != 0) {
            zzffg.m5281(1, this.f10122);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f10121.size()) {
                zzffg.m5289(2, (zzfhe) this.f10121.get(i2));
                i = i2 + 1;
            } else {
                this.f10394.m12719(zzffg);
                return;
            }
        }
    }
}
