package com.google.android.gms.internal;

import java.util.Map;

final class zzfgi<K> implements Map.Entry<K, Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private Map.Entry<K, zzfgg> f10424;

    private zzfgi(Map.Entry<K, zzfgg> entry) {
        this.f10424 = entry;
    }

    public final K getKey() {
        return this.f10424.getKey();
    }

    public final Object getValue() {
        if (this.f10424.getValue() == null) {
            return null;
        }
        return zzfgg.m12594();
    }

    public final Object setValue(Object obj) {
        if (obj instanceof zzfhe) {
            return this.f10424.getValue().m12599((zzfhe) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzfgg m12595() {
        return this.f10424.getValue();
    }
}
