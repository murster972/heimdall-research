package com.google.android.gms.internal;

import android.database.ContentObserver;
import android.os.Handler;

final class zzfw extends ContentObserver {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzft f10636;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzfw(zzft zzft, Handler handler) {
        super(handler);
        this.f10636 = zzft;
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        this.f10636.m5329();
    }
}
