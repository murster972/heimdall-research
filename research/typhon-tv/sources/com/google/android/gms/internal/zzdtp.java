package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtr;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdtp extends zzffu<zzdtp, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdtp f10152;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdtp> f10153;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdtr f10154;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10155;

    public static final class zza extends zzffu.zza<zzdtp, zza> implements zzfhg {
        private zza() {
            super(zzdtp.f10152);
        }

        /* synthetic */ zza(zzdtq zzdtq) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12145(int i) {
            m12541();
            ((zzdtp) this.f10398).m12136(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12146(zzdtr zzdtr) {
            m12541();
            ((zzdtp) this.f10398).m12139(zzdtr);
            return this;
        }
    }

    static {
        zzdtp zzdtp = new zzdtp();
        f10152 = zzdtp;
        zzdtp.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdtp.f10394.m12718();
    }

    private zzdtp() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zza m12134() {
        zzdtp zzdtp = f10152;
        zzffu.zza zza2 = (zzffu.zza) zzdtp.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdtp);
        return (zza) zza2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdtp m12135(zzfes zzfes) throws zzfge {
        return (zzdtp) zzffu.m12526(f10152, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12136(int i) {
        this.f10155 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12139(zzdtr zzdtr) {
        if (zzdtr == null) {
            throw new NullPointerException();
        }
        this.f10154 = zzdtr;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdtr m12140() {
        return this.f10154 == null ? zzdtr.m12148() : this.f10154;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12141() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10155 != 0) {
            i2 = zzffg.m5245(1, this.f10155) + 0;
        }
        if (this.f10154 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10154 == null ? zzdtr.m12148() : this.f10154);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12142() {
        return this.f10155;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m12143(int i, Object obj, Object obj2) {
        zzdtr.zza zza2;
        boolean z = true;
        switch (zzdtq.f10156[i - 1]) {
            case 1:
                return new zzdtp();
            case 2:
                return f10152;
            case 3:
                return null;
            case 4:
                return new zza((zzdtq) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdtp zzdtp = (zzdtp) obj2;
                boolean z2 = this.f10155 != 0;
                int i2 = this.f10155;
                if (zzdtp.f10155 == 0) {
                    z = false;
                }
                this.f10155 = zzh.m12569(z2, i2, z, zzdtp.f10155);
                this.f10154 = (zzdtr) zzh.m12572(this.f10154, zzdtp.f10154);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f10155 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f10154 != null) {
                                        zzdtr zzdtr = this.f10154;
                                        zzffu.zza zza3 = (zzffu.zza) zzdtr.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdtr);
                                        zza2 = (zzdtr.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10154 = (zzdtr) zzffb.m12416(zzdtr.m12148(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10154);
                                        this.f10154 = (zzdtr) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10153 == null) {
                    synchronized (zzdtp.class) {
                        if (f10153 == null) {
                            f10153 = new zzffu.zzb(f10152);
                        }
                    }
                }
                return f10153;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10152;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12144(zzffg zzffg) throws IOException {
        if (this.f10155 != 0) {
            zzffg.m5281(1, this.f10155);
        }
        if (this.f10154 != null) {
            zzffg.m5289(2, (zzfhe) this.f10154 == null ? zzdtr.m12148() : this.f10154);
        }
        this.f10394.m12719(zzffg);
    }
}
