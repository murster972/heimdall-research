package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;
import java.util.Map;

@zzzv
public final class zzrr extends zzbfm {
    public static final Parcelable.Creator<zzrr> CREATOR = new zzrs();

    /* renamed from: 靐  reason: contains not printable characters */
    private String[] f5337;

    /* renamed from: 齉  reason: contains not printable characters */
    private String[] f5338;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f5339;

    zzrr(String str, String[] strArr, String[] strArr2) {
        this.f5339 = str;
        this.f5337 = strArr;
        this.f5338 = strArr2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzrr m5809(zzr zzr) throws zza {
        Map<String, String> r1 = zzr.m13360();
        int size = r1.size();
        String[] strArr = new String[size];
        String[] strArr2 = new String[size];
        int i = 0;
        Iterator<Map.Entry<String, String>> it2 = r1.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return new zzrr(zzr.m13359(), strArr, strArr2);
            }
            Map.Entry next = it2.next();
            strArr[i2] = (String) next.getKey();
            strArr2[i2] = (String) next.getValue();
            i = i2 + 1;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 1, this.f5339, false);
        zzbfp.m10200(parcel, 2, this.f5337, false);
        zzbfp.m10200(parcel, 3, this.f5338, false);
        zzbfp.m10182(parcel, r0);
    }
}
