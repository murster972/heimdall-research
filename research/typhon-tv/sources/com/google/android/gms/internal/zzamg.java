package com.google.android.gms.internal;

import android.content.Context;
import android.view.ViewGroup;
import com.google.android.gms.common.internal.zzbq;

@zzzv
public final class zzamg {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzamp f4356;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzamd f4357;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ViewGroup f4358;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f4359;

    private zzamg(Context context, ViewGroup viewGroup, zzamp zzamp, zzamd zzamd) {
        this.f4359 = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.f4358 = viewGroup;
        this.f4356 = zzamp;
        this.f4357 = null;
    }

    public zzamg(Context context, ViewGroup viewGroup, zzanh zzanh) {
        this(context, viewGroup, zzanh, (zzamd) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4891() {
        zzbq.m9115("onPause must be called from the UI thread.");
        if (this.f4357 != null) {
            this.f4357.m4874();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4892() {
        zzbq.m9115("onDestroy must be called from the UI thread.");
        if (this.f4357 != null) {
            this.f4357.m4872();
            this.f4358.removeView(this.f4357);
            this.f4357 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzamd m4893() {
        zzbq.m9115("getAdVideoUnderlay must be called from the UI thread.");
        return this.f4357;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4894(int i, int i2, int i3, int i4) {
        zzbq.m9115("The underlay may only be modified from the UI thread.");
        if (this.f4357 != null) {
            this.f4357.m4886(i, i2, i3, i4);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4895(int i, int i2, int i3, int i4, int i5, boolean z, zzamo zzamo) {
        if (this.f4357 == null) {
            zznn.m5614(this.f4356.m4922().m5619(), this.f4356.m4929(), "vpr2");
            this.f4357 = new zzamd(this.f4359, this.f4356, i5, z, this.f4356.m4922().m5619(), zzamo);
            this.f4358.addView(this.f4357, 0, new ViewGroup.LayoutParams(-1, -1));
            this.f4357.m4886(i, i2, i3, i4);
            this.f4356.m4932(false);
        }
    }
}
