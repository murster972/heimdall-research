package com.google.android.gms.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;

@zzzv
public final class zzaas extends zzd<zzabb> {

    /* renamed from: 麤  reason: contains not printable characters */
    private int f3715;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzaas(Context context, Looper looper, zzf zzf, zzg zzg, int i) {
        super(context.getApplicationContext() != null ? context.getApplicationContext() : context, looper, 8, zzf, zzg, (String) null);
        this.f3715 = i;
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.ads.service.START";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzabb m4257() throws DeadObjectException {
        return (zzabb) super.m9171();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m4258() {
        return "com.google.android.gms.ads.internal.request.IAdRequestService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m4259(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.request.IAdRequestService");
        return queryLocalInterface instanceof zzabb ? (zzabb) queryLocalInterface : new zzabd(iBinder);
    }
}
