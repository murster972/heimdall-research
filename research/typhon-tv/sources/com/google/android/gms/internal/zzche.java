package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface zzche extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    void m10674(zzcgi zzcgi) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m10675(zzcgi zzcgi) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    String m10676(zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    List<zzcln> m10677(zzcgi zzcgi, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    List<zzcgl> m10678(String str, String str2, zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    List<zzcgl> m10679(String str, String str2, String str3) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    List<zzcln> m10680(String str, String str2, String str3, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    List<zzcln> m10681(String str, String str2, boolean z, zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10682(long j, String str, String str2, String str3) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10683(zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10684(zzcgl zzcgl) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10685(zzcgl zzcgl, zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10686(zzcha zzcha, zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10687(zzcha zzcha, String str, String str2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10688(zzcln zzcln, zzcgi zzcgi) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    byte[] m10689(zzcha zzcha, String str) throws RemoteException;
}
