package com.google.android.gms.internal;

import java.io.OutputStream;

final class zzabk implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ byte[] f8056;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ OutputStream f8057;

    zzabk(zzabj zzabj, OutputStream outputStream, byte[] bArr) {
        this.f8057 = outputStream;
        this.f8056 = bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
            r2 = 0
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0017, all -> 0x0035 }
            java.io.OutputStream r0 = r4.f8057     // Catch:{ IOException -> 0x0017, all -> 0x0035 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0017, all -> 0x0035 }
            byte[] r0 = r4.f8056     // Catch:{ IOException -> 0x0045 }
            int r0 = r0.length     // Catch:{ IOException -> 0x0045 }
            r1.writeInt(r0)     // Catch:{ IOException -> 0x0045 }
            byte[] r0 = r4.f8056     // Catch:{ IOException -> 0x0045 }
            r1.write(r0)     // Catch:{ IOException -> 0x0045 }
            com.google.android.gms.common.util.zzn.m9261(r1)
        L_0x0016:
            return
        L_0x0017:
            r0 = move-exception
            r1 = r2
        L_0x0019:
            java.lang.String r2 = "Error transporting the ad response"
            com.google.android.gms.internal.zzagf.m4793(r2, r0)     // Catch:{ all -> 0x0043 }
            com.google.android.gms.internal.zzaft r2 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ all -> 0x0043 }
            java.lang.String r3 = "LargeParcelTeleporter.pipeData.1"
            r2.m4505((java.lang.Throwable) r0, (java.lang.String) r3)     // Catch:{ all -> 0x0043 }
            if (r1 != 0) goto L_0x0031
            java.io.OutputStream r0 = r4.f8057
            com.google.android.gms.common.util.zzn.m9261(r0)
            goto L_0x0016
        L_0x0031:
            com.google.android.gms.common.util.zzn.m9261(r1)
            goto L_0x0016
        L_0x0035:
            r0 = move-exception
            r1 = r2
        L_0x0037:
            if (r1 != 0) goto L_0x003f
            java.io.OutputStream r1 = r4.f8057
            com.google.android.gms.common.util.zzn.m9261(r1)
        L_0x003e:
            throw r0
        L_0x003f:
            com.google.android.gms.common.util.zzn.m9261(r1)
            goto L_0x003e
        L_0x0043:
            r0 = move-exception
            goto L_0x0037
        L_0x0045:
            r0 = move-exception
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzabk.run():void");
    }
}
