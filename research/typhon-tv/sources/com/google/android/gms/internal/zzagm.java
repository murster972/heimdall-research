package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagm extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f8163;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8164;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagm(Context context, int i) {
        super((zzagi) null);
        this.f8164 = context;
        this.f8163 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9585() {
        SharedPreferences.Editor edit = this.f8164.getSharedPreferences("admob", 0).edit();
        edit.putInt("version_code", this.f8163);
        edit.apply();
    }
}
