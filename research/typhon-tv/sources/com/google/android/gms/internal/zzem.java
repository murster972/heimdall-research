package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzem extends zzet {
    public zzem(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 51);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12287() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.f10284) {
            zzdl zzdl = new zzdl((String) this.f10286.invoke((Object) null, new Object[0]));
            this.f10284.f8458 = zzdl.f9870;
            this.f10284.f8412 = zzdl.f9869;
        }
    }
}
