package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtv;
import java.security.GeneralSecurityException;

public final class zzdqg {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzdtv f9941 = ((zzdtv) ((zzdtv.zza) zzdtv.m12155().m12545(zzdrb.f9959)).m12165(zzdpr.m11651("TinkAead", "Aead", "AesCtrHmacAeadKey", 0, true)).m12165(zzdpr.m11651("TinkAead", "Aead", "AesEaxKey", 0, true)).m12165(zzdpr.m11651("TinkAead", "Aead", "AesGcmKey", 0, true)).m12165(zzdpr.m11651("TinkAead", "Aead", "ChaCha20Poly1305Key", 0, true)).m12165(zzdpr.m11651("TinkAead", "Aead", "KmsAeadKey", 0, true)).m12165(zzdpr.m11651("TinkAead", "Aead", "KmsEnvelopeAeadKey", 0, true)).m12166("TINK_AEAD_1_0_0").m12542());

    static {
        try {
            m11684();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m11684() throws GeneralSecurityException {
        zzdqe.m11680("TinkAead", (zzdpq) new zzdqf());
        zzdrb.m11768();
    }
}
