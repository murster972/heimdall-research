package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzpt extends NativeAd.Image {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Drawable f5315;

    /* renamed from: 麤  reason: contains not printable characters */
    private final double f5316;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Uri f5317;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzpq f5318;

    public zzpt(zzpq zzpq) {
        Drawable drawable;
        Uri uri = null;
        this.f5318 = zzpq;
        try {
            IObjectWrapper r0 = this.f5318.m13225();
            if (r0 != null) {
                drawable = (Drawable) zzn.m9307(r0);
                this.f5315 = drawable;
                uri = this.f5318.m13223();
                this.f5317 = uri;
                double d = 1.0d;
                d = this.f5318.m13224();
                this.f5316 = d;
            }
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get drawable.", e);
        }
        drawable = null;
        this.f5315 = drawable;
        try {
            uri = this.f5318.m13223();
        } catch (RemoteException e2) {
            zzakb.m4793("Failed to get uri.", e2);
        }
        this.f5317 = uri;
        double d2 = 1.0d;
        try {
            d2 = this.f5318.m13224();
        } catch (RemoteException e3) {
            zzakb.m4793("Failed to get scale.", e3);
        }
        this.f5316 = d2;
    }

    public final Drawable getDrawable() {
        return this.f5315;
    }

    public final double getScale() {
        return this.f5316;
    }

    public final Uri getUri() {
        return this.f5317;
    }
}
