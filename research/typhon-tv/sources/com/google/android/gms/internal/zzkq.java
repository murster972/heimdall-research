package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzkq extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m13069(IObjectWrapper iObjectWrapper, String str, zzux zzux, int i) throws RemoteException;
}
