package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;
import com.mopub.common.AdType;

final class zzju extends zzjr.zza<zzks> {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzjr f10764;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzjn f10765;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzux f10766;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f10767;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f10768;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzju(zzjr zzjr, Context context, zzjn zzjn, String str, zzux zzux) {
        super();
        this.f10764 = zzjr;
        this.f10768 = context;
        this.f10765 = zzjn;
        this.f10767 = str;
        this.f10766 = zzux;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13039() throws RemoteException {
        zzks r0 = this.f10764.f4809.m5447(this.f10768, this.f10765, this.f10767, this.f10766, 2);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10768, AdType.INTERSTITIAL);
        return new zzmg();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13040(zzla zzla) throws RemoteException {
        return zzla.createInterstitialAdManager(zzn.m9306(this.f10768), this.f10765, this.f10767, this.f10766, 11910000);
    }
}
