package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.cast.ApplicationMetadata;
import java.util.Arrays;

public final class zzbcn extends zzbfm {
    public static final Parcelable.Creator<zzbcn> CREATOR = new zzbco();

    /* renamed from: 连任  reason: contains not printable characters */
    private int f8671;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f8672;

    /* renamed from: 麤  reason: contains not printable characters */
    private ApplicationMetadata f8673;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f8674;

    /* renamed from: 龘  reason: contains not printable characters */
    private double f8675;

    public zzbcn() {
        this(Double.NaN, false, -1, (ApplicationMetadata) null, -1);
    }

    zzbcn(double d, boolean z, int i, ApplicationMetadata applicationMetadata, int i2) {
        this.f8675 = d;
        this.f8672 = z;
        this.f8674 = i;
        this.f8673 = applicationMetadata;
        this.f8671 = i2;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbcn)) {
            return false;
        }
        zzbcn zzbcn = (zzbcn) obj;
        return this.f8675 == zzbcn.f8675 && this.f8672 == zzbcn.f8672 && this.f8674 == zzbcn.f8674 && zzbcm.m10043(this.f8673, zzbcn.f8673) && this.f8671 == zzbcn.f8671;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Double.valueOf(this.f8675), Boolean.valueOf(this.f8672), Integer.valueOf(this.f8674), this.f8673, Integer.valueOf(this.f8671)});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10183(parcel, 2, this.f8675);
        zzbfp.m10195(parcel, 3, this.f8672);
        zzbfp.m10185(parcel, 4, this.f8674);
        zzbfp.m10189(parcel, 5, (Parcelable) this.f8673, i, false);
        zzbfp.m10185(parcel, 6, this.f8671);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final ApplicationMetadata m10044() {
        return this.f8673;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m10045() {
        return this.f8672;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m10046() {
        return this.f8671;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m10047() {
        return this.f8674;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final double m10048() {
        return this.f8675;
    }
}
