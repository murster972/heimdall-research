package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagu extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ long f8179;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8180;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagu(Context context, long j) {
        super((zzagi) null);
        this.f8180 = context;
        this.f8179 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9593() {
        SharedPreferences.Editor edit = this.f8180.getSharedPreferences("admob", 0).edit();
        edit.putLong("app_last_background_time_ms", this.f8179);
        edit.apply();
    }
}
