package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbic implements Parcelable.Creator<zzbib> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r3 = zzbfn.m10169(parcel);
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < r3) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r3);
        return new zzbib(str2, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbib[i];
    }
}
