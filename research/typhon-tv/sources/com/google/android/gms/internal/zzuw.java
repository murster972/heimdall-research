package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.mediation.customevent.CustomEventAdapter;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import java.util.Map;

@zzzv
public final class zzuw extends zzuy {

    /* renamed from: 龘  reason: contains not printable characters */
    private Map<Class<? extends NetworkExtras>, NetworkExtras> f5480;

    /* JADX WARNING: type inference failed for: r1v12, types: [com.google.ads.mediation.admob.AdMobAdapter, com.google.android.gms.ads.mediation.MediationAdapter] */
    /* renamed from: 麤  reason: contains not printable characters */
    private final zzva m5913(String str) throws RemoteException {
        try {
            zzakb.m4792("Reflection failed, retrying using direct instantiation");
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(str)) {
                return new zzvr(new AdMobAdapter());
            }
            if ("com.google.ads.mediation.AdUrlAdapter".equals(str)) {
                return new zzvr(new AdUrlAdapter());
            }
            if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
                return new zzvr(new CustomEventAdapter());
            }
            if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
                com.google.ads.mediation.customevent.CustomEventAdapter customEventAdapter = new com.google.ads.mediation.customevent.CustomEventAdapter();
                return new zzvw(customEventAdapter, (CustomEventExtras) this.f5480.get(customEventAdapter.getAdditionalParametersType()));
            }
            throw new RemoteException();
        } catch (Throwable th) {
            zzakb.m4796(new StringBuilder(String.valueOf(str).length() + 43).append("Could not instantiate mediation adapter: ").append(str).append(". ").toString(), th);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final <NETWORK_EXTRAS extends com.google.ads.mediation.NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> zzva m5914(String str) throws RemoteException {
        try {
            Class<?> cls = Class.forName(str, false, zzuw.class.getClassLoader());
            if (MediationAdapter.class.isAssignableFrom(cls)) {
                MediationAdapter newInstance = cls.newInstance();
                return new zzvw(newInstance, this.f5480.get(newInstance.getAdditionalParametersType()));
            } else if (com.google.android.gms.ads.mediation.MediationAdapter.class.isAssignableFrom(cls)) {
                return new zzvr((com.google.android.gms.ads.mediation.MediationAdapter) cls.newInstance());
            } else {
                zzakb.m4791(new StringBuilder(String.valueOf(str).length() + 64).append("Could not instantiate mediation adapter: ").append(str).append(" (not a valid adapter).").toString());
                throw new RemoteException();
            }
        } catch (Throwable th) {
            return m5913(str);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5915(String str) throws RemoteException {
        try {
            return CustomEvent.class.isAssignableFrom(Class.forName(str, false, zzuw.class.getClassLoader()));
        } catch (Throwable th) {
            zzakb.m4791(new StringBuilder(String.valueOf(str).length() + 80).append("Could not load custom event implementation class: ").append(str).append(", assuming old implementation.").toString());
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzva m5916(String str) throws RemoteException {
        return m5914(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5917(Map<Class<? extends NetworkExtras>, NetworkExtras> map) {
        this.f5480 = map;
    }
}
