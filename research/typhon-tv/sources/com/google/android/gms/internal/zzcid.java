package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzbq;

public final class zzcid {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcif f9343;

    public zzcid(zzcif zzcif) {
        zzbq.m9120(zzcif);
        this.f9343 = zzcif;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000b, code lost:
        r1 = r1.getReceiverInfo(new android.content.ComponentName(r4, "com.google.android.gms.measurement.AppMeasurementReceiver"), 2);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m10910(android.content.Context r4) {
        /*
            r0 = 0
            com.google.android.gms.common.internal.zzbq.m9120(r4)
            android.content.pm.PackageManager r1 = r4.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0020 }
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            android.content.ComponentName r2 = new android.content.ComponentName     // Catch:{ NameNotFoundException -> 0x0020 }
            java.lang.String r3 = "com.google.android.gms.measurement.AppMeasurementReceiver"
            r2.<init>(r4, r3)     // Catch:{ NameNotFoundException -> 0x0020 }
            r3 = 2
            android.content.pm.ActivityInfo r1 = r1.getReceiverInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x0020 }
            if (r1 == 0) goto L_0x000a
            boolean r1 = r1.enabled     // Catch:{ NameNotFoundException -> 0x0020 }
            if (r1 == 0) goto L_0x000a
            r0 = 1
            goto L_0x000a
        L_0x0020:
            r1 = move-exception
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcid.m10910(android.content.Context):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10911(Context context, Intent intent) {
        zzcim r3 = zzcim.m11006(context);
        zzchm r8 = r3.m11016();
        if (intent == null) {
            r8.m10834().m10849("Receiver called with null intent");
            return;
        }
        String action = intent.getAction();
        r8.m10848().m10850("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            r8.m10848().m10849("Starting wakeful intent.");
            this.f9343.m10913(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            BroadcastReceiver.PendingResult r9 = this.f9343.m10912();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                r8.m10848().m10849("Install referrer extras are null");
                if (r9 != null) {
                    r9.finish();
                    return;
                }
                return;
            }
            r8.m10836().m10850("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            Bundle r6 = r3.m11063().m11432(Uri.parse(stringExtra));
            if (r6 == null) {
                r8.m10848().m10849("No campaign defined in install referrer broadcast");
                if (r9 != null) {
                    r9.finish();
                    return;
                }
                return;
            }
            long longExtra = 1000 * intent.getLongExtra("referrer_timestamp_seconds", 0);
            if (longExtra == 0) {
                r8.m10834().m10849("Install referrer is missing timestamp");
            }
            r3.m11018().m10986((Runnable) new zzcie(this, r3, longExtra, r6, context, r8, r9));
        }
    }
}
