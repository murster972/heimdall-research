package com.google.android.gms.internal;

public abstract class zzko extends zzev implements zzkn {
    public zzko() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.google.android.gms.internal.zzld] */
    /* JADX WARNING: type inference failed for: r0v27, types: [com.google.android.gms.internal.zzkh] */
    /* JADX WARNING: type inference failed for: r0v30 */
    /* JADX WARNING: type inference failed for: r0v31 */
    /* JADX WARNING: type inference failed for: r0v32 */
    /* JADX WARNING: type inference failed for: r0v33 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r5, android.os.Parcel r6, android.os.Parcel r7, int r8) throws android.os.RemoteException {
        /*
            r4 = this;
            r0 = 0
            r1 = 1
            boolean r2 = r4.zza(r5, r6, r7, r8)
            if (r2 == 0) goto L_0x000a
            r0 = r1
        L_0x0009:
            return r0
        L_0x000a:
            switch(r5) {
                case 1: goto L_0x000f;
                case 2: goto L_0x001b;
                case 3: goto L_0x003c;
                case 4: goto L_0x004b;
                case 5: goto L_0x005a;
                case 6: goto L_0x0075;
                case 7: goto L_0x0084;
                case 8: goto L_0x00a5;
                case 9: goto L_0x00bd;
                case 10: goto L_0x00cd;
                default: goto L_0x000d;
            }
        L_0x000d:
            r0 = 0
            goto L_0x0009
        L_0x000f:
            com.google.android.gms.internal.zzkk r0 = r4.zzdi()
            r7.writeNoException()
            com.google.android.gms.internal.zzew.m12305((android.os.Parcel) r7, (android.os.IInterface) r0)
        L_0x0019:
            r0 = r1
            goto L_0x0009
        L_0x001b:
            android.os.IBinder r2 = r6.readStrongBinder()
            if (r2 != 0) goto L_0x0028
        L_0x0021:
            r4.zzb((com.google.android.gms.internal.zzkh) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0028:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.IAdListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r3 = r0 instanceof com.google.android.gms.internal.zzkh
            if (r3 == 0) goto L_0x0036
            com.google.android.gms.internal.zzkh r0 = (com.google.android.gms.internal.zzkh) r0
            goto L_0x0021
        L_0x0036:
            com.google.android.gms.internal.zzkj r0 = new com.google.android.gms.internal.zzkj
            r0.<init>(r2)
            goto L_0x0021
        L_0x003c:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzqq r0 = com.google.android.gms.internal.zzqr.m13342(r0)
            r4.zza((com.google.android.gms.internal.zzqq) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x004b:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzqt r0 = com.google.android.gms.internal.zzqu.m13345(r0)
            r4.zza((com.google.android.gms.internal.zzqt) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x005a:
            java.lang.String r0 = r6.readString()
            android.os.IBinder r2 = r6.readStrongBinder()
            com.google.android.gms.internal.zzqz r2 = com.google.android.gms.internal.zzra.m13374(r2)
            android.os.IBinder r3 = r6.readStrongBinder()
            com.google.android.gms.internal.zzqw r3 = com.google.android.gms.internal.zzqx.m13348(r3)
            r4.zza(r0, r2, r3)
            r7.writeNoException()
            goto L_0x0019
        L_0x0075:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzpe> r0 = com.google.android.gms.internal.zzpe.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.internal.zzpe r0 = (com.google.android.gms.internal.zzpe) r0
            r4.zza((com.google.android.gms.internal.zzpe) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0084:
            android.os.IBinder r2 = r6.readStrongBinder()
            if (r2 != 0) goto L_0x0091
        L_0x008a:
            r4.zzb((com.google.android.gms.internal.zzld) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x0091:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.ICorrelationIdProvider"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r3 = r0 instanceof com.google.android.gms.internal.zzld
            if (r3 == 0) goto L_0x009f
            com.google.android.gms.internal.zzld r0 = (com.google.android.gms.internal.zzld) r0
            goto L_0x008a
        L_0x009f:
            com.google.android.gms.internal.zzlf r0 = new com.google.android.gms.internal.zzlf
            r0.<init>(r2)
            goto L_0x008a
        L_0x00a5:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzrf r2 = com.google.android.gms.internal.zzrg.m13378(r0)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzjn> r0 = com.google.android.gms.internal.zzjn.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.internal.zzjn r0 = (com.google.android.gms.internal.zzjn) r0
            r4.zza(r2, r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x00bd:
            android.os.Parcelable$Creator<com.google.android.gms.ads.formats.PublisherAdViewOptions> r0 = com.google.android.gms.ads.formats.PublisherAdViewOptions.CREATOR
            android.os.Parcelable r0 = com.google.android.gms.internal.zzew.m12304((android.os.Parcel) r6, r0)
            com.google.android.gms.ads.formats.PublisherAdViewOptions r0 = (com.google.android.gms.ads.formats.PublisherAdViewOptions) r0
            r4.zza((com.google.android.gms.ads.formats.PublisherAdViewOptions) r0)
            r7.writeNoException()
            goto L_0x0019
        L_0x00cd:
            android.os.IBinder r0 = r6.readStrongBinder()
            com.google.android.gms.internal.zzrc r0 = com.google.android.gms.internal.zzrd.m13376(r0)
            r4.zza((com.google.android.gms.internal.zzrc) r0)
            r7.writeNoException()
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzko.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
