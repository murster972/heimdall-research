package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import org.json.JSONObject;

@zzzv
public final class zzadb implements zzacf {

    /* renamed from: 靐  reason: contains not printable characters */
    private zztp<JSONObject, JSONObject> f4012;

    /* renamed from: 龘  reason: contains not printable characters */
    private zztp<JSONObject, JSONObject> f4013;

    public zzadb(Context context) {
        this.f4013 = zzbs.zzev().m5869(context, zzakd.m4800()).m5871("google.afma.request.getAdDictionary", zztu.f5394, zztu.f5394);
        this.f4012 = zzbs.zzev().m5869(context, zzakd.m4800()).m5871("google.afma.sdkTyphoonApp.getSdkTyphoonApp", zztu.f5394, zztu.f5394);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zztp<JSONObject, JSONObject> m4323() {
        return this.f4012;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zztp<JSONObject, JSONObject> m4324() {
        return this.f4013;
    }
}
