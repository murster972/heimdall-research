package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

@zzzv
@TargetApi(19)
public final class zzyf extends zzyc {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f5574 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private PopupWindow f5575;

    /* renamed from: 麤  reason: contains not printable characters */
    private Object f5576 = new Object();

    zzyf(Context context, zzafp zzafp, zzanh zzanh, zzyb zzyb) {
        super(context, zzafp, zzanh, zzyb);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m6032() {
        synchronized (this.f5576) {
            this.f5574 = true;
            if ((this.f5564 instanceof Activity) && ((Activity) this.f5564).isDestroyed()) {
                this.f5575 = null;
            }
            if (this.f5575 != null) {
                if (this.f5575.isShowing()) {
                    this.f5575.dismiss();
                }
                this.f5575 = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m6033() {
        Window window = this.f5564 instanceof Activity ? ((Activity) this.f5564).getWindow() : null;
        if (window != null && window.getDecorView() != null && !((Activity) this.f5564).isDestroyed()) {
            FrameLayout frameLayout = new FrameLayout(this.f5564);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            zzanh zzanh = this.f5561;
            if (zzanh == null) {
                throw null;
            }
            frameLayout.addView((View) zzanh, -1, -1);
            synchronized (this.f5576) {
                if (!this.f5574) {
                    this.f5575 = new PopupWindow(frameLayout, 1, 1, false);
                    this.f5575.setOutsideTouchable(true);
                    this.f5575.setClippingEnabled(false);
                    zzagf.m4792("Displaying the 1x1 popup off the screen.");
                    try {
                        this.f5575.showAtLocation(window.getDecorView(), 0, -1, -1);
                    } catch (Exception e) {
                        this.f5575 = null;
                    }
                }
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m6034() {
        m6032();
        super.m6018();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6035(int i) {
        m6032();
        super.m6020(i);
    }
}
