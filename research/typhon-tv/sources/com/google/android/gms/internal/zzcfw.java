package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.location.LocationServices$zza;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

final class zzcfw extends LocationServices$zza<LocationSettingsResult> {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f9089 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationSettingsRequest f9090;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcfw(zzcfv zzcfv, GoogleApiClient googleApiClient, LocationSettingsRequest locationSettingsRequest, String str) {
        super(googleApiClient);
        this.f9090 = locationSettingsRequest;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Result m10422(Status status) {
        return new LocationSettingsResult(status);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10423(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10414(this.f9090, (zzn<LocationSettingsResult>) this, this.f9089);
    }
}
