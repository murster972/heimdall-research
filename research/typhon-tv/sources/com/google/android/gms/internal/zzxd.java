package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;

@zzzv
public final class zzxd extends zzp<zzxh> {
    public zzxd() {
        super("com.google.android.gms.ads.AdOverlayCreatorImpl");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzxe m6013(Activity activity) {
        try {
            IBinder r2 = ((zzxh) m9308((Context) activity)).m13632(zzn.m9306(activity));
            if (r2 == null) {
                return null;
            }
            IInterface queryLocalInterface = r2.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
            return queryLocalInterface instanceof zzxe ? (zzxe) queryLocalInterface : new zzxg(r2);
        } catch (RemoteException e) {
            zzakb.m4796("Could not create remote AdOverlay.", e);
            return null;
        } catch (zzq e2) {
            zzakb.m4796("Could not create remote AdOverlay.", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m6014(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlayCreator");
        return queryLocalInterface instanceof zzxh ? (zzxh) queryLocalInterface : new zzxi(iBinder);
    }
}
