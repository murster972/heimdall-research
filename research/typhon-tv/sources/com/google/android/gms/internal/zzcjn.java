package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty;
import com.google.android.gms.measurement.AppMeasurement$EventInterceptor;
import com.google.android.gms.measurement.AppMeasurement$OnEventListener;
import com.google.android.gms.measurement.AppMeasurement$zzb;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public final class zzcjn extends zzcjl {

    /* renamed from: 连任  reason: contains not printable characters */
    private final AtomicReference<String> f9490 = new AtomicReference<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private AppMeasurement$EventInterceptor f9491;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f9492;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Set<AppMeasurement$OnEventListener> f9493 = new CopyOnWriteArraySet();

    /* renamed from: 龘  reason: contains not printable characters */
    protected zzckb f9494;

    protected zzcjn(zzcim zzcim) {
        super(zzcim);
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m11118(AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty) {
        m11109();
        m11115();
        zzbq.m9120(appMeasurement$ConditionalUserProperty);
        zzbq.m9122(appMeasurement$ConditionalUserProperty.mName);
        if (!this.f9487.m11038()) {
            m11096().m10845().m10849("Conditional property not cleared since Firebase Analytics is disabled");
            return;
        }
        zzcln zzcln = new zzcln(appMeasurement$ConditionalUserProperty.mName, 0, (Object) null, (String) null);
        try {
            m11103().m11261(new zzcgl(appMeasurement$ConditionalUserProperty.mAppId, appMeasurement$ConditionalUserProperty.mOrigin, zzcln, appMeasurement$ConditionalUserProperty.mCreationTimestamp, appMeasurement$ConditionalUserProperty.mActive, appMeasurement$ConditionalUserProperty.mTriggerEventName, (zzcha) null, appMeasurement$ConditionalUserProperty.mTriggerTimeout, (zzcha) null, appMeasurement$ConditionalUserProperty.mTimeToLive, m11112().m11436(appMeasurement$ConditionalUserProperty.mExpiredEventName, appMeasurement$ConditionalUserProperty.mExpiredEventParams, appMeasurement$ConditionalUserProperty.mOrigin, appMeasurement$ConditionalUserProperty.mCreationTimestamp, true, false)));
        } catch (IllegalArgumentException e) {
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<AppMeasurement$ConditionalUserProperty> m11119(String str, String str2, String str3) {
        if (m11101().m10976()) {
            m11096().m10832().m10849("Cannot get conditional user properties from analytics worker thread");
            return Collections.emptyList();
        }
        m11101();
        if (zzcih.m10950()) {
            m11096().m10832().m10849("Cannot get conditional user properties from main thread");
            return Collections.emptyList();
        }
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.f9487.m11018().m10986((Runnable) new zzcjr(this, atomicReference, str, str2, str3));
            try {
                atomicReference.wait(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
            } catch (InterruptedException e) {
                m11096().m10834().m10851("Interrupted waiting for get conditional user properties", str, e);
            }
        }
        List<zzcgl> list = (List) atomicReference.get();
        if (list == null) {
            m11096().m10834().m10850("Timed out waiting for get conditional user properties", str);
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (zzcgl zzcgl : list) {
            AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty = new AppMeasurement$ConditionalUserProperty();
            appMeasurement$ConditionalUserProperty.mAppId = str;
            appMeasurement$ConditionalUserProperty.mOrigin = str2;
            appMeasurement$ConditionalUserProperty.mCreationTimestamp = zzcgl.f9154;
            appMeasurement$ConditionalUserProperty.mName = zzcgl.f9155.f9653;
            appMeasurement$ConditionalUserProperty.mValue = zzcgl.f9155.m11364();
            appMeasurement$ConditionalUserProperty.mActive = zzcgl.f9152;
            appMeasurement$ConditionalUserProperty.mTriggerEventName = zzcgl.f9145;
            if (zzcgl.f9146 != null) {
                appMeasurement$ConditionalUserProperty.mTimedOutEventName = zzcgl.f9146.f9203;
                if (zzcgl.f9146.f9200 != null) {
                    appMeasurement$ConditionalUserProperty.mTimedOutEventParams = zzcgl.f9146.f9200.m10660();
                }
            }
            appMeasurement$ConditionalUserProperty.mTriggerTimeout = zzcgl.f9147;
            if (zzcgl.f9149 != null) {
                appMeasurement$ConditionalUserProperty.mTriggeredEventName = zzcgl.f9149.f9203;
                if (zzcgl.f9149.f9200 != null) {
                    appMeasurement$ConditionalUserProperty.mTriggeredEventParams = zzcgl.f9149.f9200.m10660();
                }
            }
            appMeasurement$ConditionalUserProperty.mTriggeredTimestamp = zzcgl.f9155.f9650;
            appMeasurement$ConditionalUserProperty.mTimeToLive = zzcgl.f9150;
            if (zzcgl.f9151 != null) {
                appMeasurement$ConditionalUserProperty.mExpiredEventName = zzcgl.f9151.f9203;
                if (zzcgl.f9151.f9200 != null) {
                    appMeasurement$ConditionalUserProperty.mExpiredEventParams = zzcgl.f9151.f9200.m10660();
                }
            }
            arrayList.add(appMeasurement$ConditionalUserProperty);
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, Object> m11120(String str, String str2, String str3, boolean z) {
        if (m11101().m10976()) {
            m11096().m10832().m10849("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        }
        m11101();
        if (zzcih.m10950()) {
            m11096().m10832().m10849("Cannot get user properties from main thread");
            return Collections.emptyMap();
        }
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.f9487.m11018().m10986((Runnable) new zzcjs(this, atomicReference, str, str2, str3, z));
            try {
                atomicReference.wait(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
            } catch (InterruptedException e) {
                m11096().m10834().m10850("Interrupted waiting for get user properties", e);
            }
        }
        List<zzcln> list = (List) atomicReference.get();
        if (list == null) {
            m11096().m10834().m10849("Timed out waiting for get user properties");
            return Collections.emptyMap();
        }
        ArrayMap arrayMap = new ArrayMap(list.size());
        for (zzcln zzcln : list) {
            arrayMap.put(zzcln.f9653, zzcln.m11364());
        }
        return arrayMap;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11122(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        int i;
        zzbq.m9122(str);
        zzbq.m9122(str2);
        zzbq.m9120(bundle);
        m11109();
        m11115();
        if (!this.f9487.m11038()) {
            m11096().m10845().m10849("Event not sent since app measurement is disabled");
            return;
        }
        if (!this.f9492) {
            this.f9492 = true;
            try {
                try {
                    Class.forName("com.google.android.gms.tagmanager.TagManagerService").getDeclaredMethod("initialize", new Class[]{Context.class}).invoke((Object) null, new Object[]{m11097()});
                } catch (Exception e) {
                    m11096().m10834().m10850("Failed to invoke Tag Manager's initialize() method", e);
                }
            } catch (ClassNotFoundException e2) {
                m11096().m10836().m10849("Tag Manager is not found and thus will not be used");
            }
        }
        boolean equals = "am".equals(str);
        boolean r2 = zzclq.m11368(str2);
        if (z && this.f9491 != null && !r2 && !equals) {
            m11096().m10845().m10851("Passing event to registered event handler (FE)", m11111().m10805(str2), m11111().m10799(bundle));
            this.f9491.m13699(str, str2, bundle, j);
        } else if (this.f9487.m11045()) {
            int r4 = m11112().m11429(str2);
            if (r4 != 0) {
                m11112();
                this.f9487.m11063().m11443(str3, r4, "_ev", zzclq.m11378(str2, 40, true), str2 != null ? str2.length() : 0);
                return;
            }
            List singletonList = Collections.singletonList("_o");
            Bundle r13 = m11112().m11434(str2, bundle, (List<String>) singletonList, z3, true);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r13);
            long nextLong = m11112().m11419().nextLong();
            int i2 = 0;
            String[] strArr = (String[]) r13.keySet().toArray(new String[bundle.size()]);
            Arrays.sort(strArr);
            int length = strArr.length;
            int i3 = 0;
            while (i3 < length) {
                String str4 = strArr[i3];
                Object obj = r13.get(str4);
                m11112();
                Bundle[] r19 = zzclq.m11393(obj);
                if (r19 != null) {
                    r13.putInt(str4, r19.length);
                    int i4 = 0;
                    while (true) {
                        int i5 = i4;
                        if (i5 >= r19.length) {
                            break;
                        }
                        Bundle r22 = m11112().m11434("_ep", r19[i5], (List<String>) singletonList, z3, false);
                        r22.putString("_en", str2);
                        r22.putLong("_eid", nextLong);
                        r22.putString("_gn", str4);
                        r22.putInt("_ll", r19.length);
                        r22.putInt("_i", i5);
                        arrayList.add(r22);
                        i4 = i5 + 1;
                    }
                    i = r19.length + i2;
                } else {
                    i = i2;
                }
                i3++;
                i2 = i;
            }
            if (i2 != 0) {
                r13.putLong("_eid", nextLong);
                r13.putInt("_epc", i2);
            }
            zzckf r10 = m11104().m11204();
            if (r10 != null && !r13.containsKey("_sc")) {
                r10.f9554 = true;
            }
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 >= arrayList.size()) {
                    break;
                }
                Bundle bundle2 = (Bundle) arrayList.get(i7);
                String str5 = i7 != 0 ? "_ep" : str2;
                bundle2.putString("_o", str);
                if (!bundle2.containsKey("_sc")) {
                    zzckc.m11187((AppMeasurement$zzb) r10, bundle2);
                }
                Bundle r8 = z2 ? m11112().m11433(bundle2) : bundle2;
                m11096().m10845().m10851("Logging event (FE)", m11111().m10805(str2), m11111().m10799(r8));
                m11103().m11262(new zzcha(str5, new zzcgx(r8), str, j), str3);
                if (!equals) {
                    for (AppMeasurement$OnEventListener r23 : this.f9493) {
                        r23.m13700(str, str2, new Bundle(r8), j);
                    }
                }
                i6 = i7 + 1;
            }
            if (m11104().m11204() != null && "_ae".equals(str2)) {
                m11100().m11327(true);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m11123(String str, String str2, String str3, Bundle bundle) {
        long r0 = m11105().m9243();
        zzbq.m9122(str2);
        AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty = new AppMeasurement$ConditionalUserProperty();
        appMeasurement$ConditionalUserProperty.mAppId = str;
        appMeasurement$ConditionalUserProperty.mName = str2;
        appMeasurement$ConditionalUserProperty.mCreationTimestamp = r0;
        if (str3 != null) {
            appMeasurement$ConditionalUserProperty.mExpiredEventName = str3;
            appMeasurement$ConditionalUserProperty.mExpiredEventParams = bundle;
        }
        m11101().m10986((Runnable) new zzcjq(this, appMeasurement$ConditionalUserProperty));
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m11124(AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty) {
        m11109();
        m11115();
        zzbq.m9120(appMeasurement$ConditionalUserProperty);
        zzbq.m9122(appMeasurement$ConditionalUserProperty.mName);
        zzbq.m9122(appMeasurement$ConditionalUserProperty.mOrigin);
        zzbq.m9120(appMeasurement$ConditionalUserProperty.mValue);
        if (!this.f9487.m11038()) {
            m11096().m10845().m10849("Conditional property not sent since Firebase Analytics is disabled");
            return;
        }
        zzcln zzcln = new zzcln(appMeasurement$ConditionalUserProperty.mName, appMeasurement$ConditionalUserProperty.mTriggeredTimestamp, appMeasurement$ConditionalUserProperty.mValue, appMeasurement$ConditionalUserProperty.mOrigin);
        try {
            zzcha r15 = m11112().m11436(appMeasurement$ConditionalUserProperty.mTriggeredEventName, appMeasurement$ConditionalUserProperty.mTriggeredEventParams, appMeasurement$ConditionalUserProperty.mOrigin, 0, true, false);
            m11103().m11261(new zzcgl(appMeasurement$ConditionalUserProperty.mAppId, appMeasurement$ConditionalUserProperty.mOrigin, zzcln, appMeasurement$ConditionalUserProperty.mCreationTimestamp, false, appMeasurement$ConditionalUserProperty.mTriggerEventName, m11112().m11436(appMeasurement$ConditionalUserProperty.mTimedOutEventName, appMeasurement$ConditionalUserProperty.mTimedOutEventParams, appMeasurement$ConditionalUserProperty.mOrigin, 0, true, false), appMeasurement$ConditionalUserProperty.mTriggerTimeout, r15, appMeasurement$ConditionalUserProperty.mTimeToLive, m11112().m11436(appMeasurement$ConditionalUserProperty.mExpiredEventName, appMeasurement$ConditionalUserProperty.mExpiredEventParams, appMeasurement$ConditionalUserProperty.mOrigin, 0, true, false)));
        } catch (IllegalArgumentException e) {
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m11125(AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty) {
        long r0 = m11105().m9243();
        zzbq.m9120(appMeasurement$ConditionalUserProperty);
        zzbq.m9122(appMeasurement$ConditionalUserProperty.mName);
        zzbq.m9122(appMeasurement$ConditionalUserProperty.mOrigin);
        zzbq.m9120(appMeasurement$ConditionalUserProperty.mValue);
        appMeasurement$ConditionalUserProperty.mCreationTimestamp = r0;
        String str = appMeasurement$ConditionalUserProperty.mName;
        Object obj = appMeasurement$ConditionalUserProperty.mValue;
        if (m11112().m11420(str) != 0) {
            m11096().m10832().m10850("Invalid conditional user property name", m11111().m10797(str));
        } else if (m11112().m11423(str, obj) != 0) {
            m11096().m10832().m10851("Invalid conditional user property value", m11111().m10797(str), obj);
        } else {
            Object r2 = m11112().m11430(str, obj);
            if (r2 == null) {
                m11096().m10832().m10851("Unable to normalize conditional user property value", m11111().m10797(str), obj);
                return;
            }
            appMeasurement$ConditionalUserProperty.mValue = r2;
            long j = appMeasurement$ConditionalUserProperty.mTriggerTimeout;
            if (TextUtils.isEmpty(appMeasurement$ConditionalUserProperty.mTriggerEventName) || (j <= 15552000000L && j >= 1)) {
                long j2 = appMeasurement$ConditionalUserProperty.mTimeToLive;
                if (j2 > 15552000000L || j2 < 1) {
                    m11096().m10832().m10851("Invalid conditional user property time to live", m11111().m10797(str), Long.valueOf(j2));
                } else {
                    m11101().m10986((Runnable) new zzcjp(this, appMeasurement$ConditionalUserProperty));
                }
            } else {
                m11096().m10832().m10851("Invalid conditional user property timeout", m11111().m10797(str), Long.valueOf(j));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m11126(boolean z) {
        m11109();
        m11115();
        m11096().m10845().m10850("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        m11098().m10893(z);
        m11103().m11252();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11131(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        Bundle bundle2;
        if (bundle == null) {
            bundle2 = new Bundle();
        } else {
            bundle2 = new Bundle(bundle);
            for (String str4 : bundle2.keySet()) {
                Object obj = bundle2.get(str4);
                if (obj instanceof Bundle) {
                    bundle2.putBundle(str4, new Bundle((Bundle) obj));
                } else if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= parcelableArr.length) {
                            break;
                        }
                        if (parcelableArr[i2] instanceof Bundle) {
                            parcelableArr[i2] = new Bundle((Bundle) parcelableArr[i2]);
                        }
                        i = i2 + 1;
                    }
                } else if (obj instanceof ArrayList) {
                    ArrayList arrayList = (ArrayList) obj;
                    int i3 = 0;
                    while (true) {
                        int i4 = i3;
                        if (i4 >= arrayList.size()) {
                            break;
                        }
                        Object obj2 = arrayList.get(i4);
                        if (obj2 instanceof Bundle) {
                            arrayList.set(i4, new Bundle((Bundle) obj2));
                        }
                        i3 = i4 + 1;
                    }
                }
            }
        }
        m11101().m10986((Runnable) new zzcjv(this, str, str2, j, bundle2, z, z2, z3, str3));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11132(String str, String str2, long j, Object obj) {
        m11101().m10986((Runnable) new zzcjw(this, str, str2, obj, j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11133(String str, String str2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        m11131(str, str2, m11105().m9243(), bundle, true, z2, z3, (String) null);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11134(String str, String str2, Object obj, long j) {
        zzbq.m9122(str);
        zzbq.m9122(str2);
        m11109();
        m11115();
        if (!this.f9487.m11038()) {
            m11096().m10845().m10849("User property not set since app measurement is disabled");
        } else if (this.f9487.m11045()) {
            m11096().m10845().m10851("Setting user property (FE)", m11111().m10805(str2), obj);
            m11103().m11265(new zzcln(str2, j, obj, str));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m11135() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m11136() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m11137() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m11138() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m11139() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m11140() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m11141() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m11142() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m11143() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m11144() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m11145() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m11146() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m11147() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m11148() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m11149() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m11150() {
        return super.m11105();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final Task<String> m11151() {
        try {
            String r0 = m11098().m10888();
            return r0 != null ? Tasks.m13725(r0) : Tasks.m13726((Executor) m11101().m10977(), new zzcjy(this));
        } catch (Exception e) {
            m11096().m10834().m10849("Failed to schedule task for getAppInstanceId");
            return Tasks.m13724(e);
        }
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String m11152() {
        return this.f9490.get();
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final void m11153() {
        m11101().m10986((Runnable) new zzcka(this));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m11154() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzcln> m11155(boolean z) {
        m11115();
        m11096().m10845().m10849("Fetching user attributes (FE)");
        if (m11101().m10976()) {
            m11096().m10832().m10849("Cannot get all user properties from analytics worker thread");
            return Collections.emptyList();
        }
        m11101();
        if (zzcih.m10950()) {
            m11096().m10832().m10849("Cannot get all user properties from main thread");
            return Collections.emptyList();
        }
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.f9487.m11018().m10986((Runnable) new zzcjx(this, atomicReference, z));
            try {
                atomicReference.wait(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
            } catch (InterruptedException e) {
                m11096().m10834().m10850("Interrupted waiting for get user properties", e);
            }
        }
        List<zzcln> list = (List) atomicReference.get();
        if (list != null) {
            return list;
        }
        m11096().m10834().m10849("Timed out waiting for get user properties");
        return Collections.emptyList();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11156() {
        super.m11107();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11157(long j) {
        m11101().m10986((Runnable) new zzcju(this, j));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11158(AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty) {
        zzbq.m9120(appMeasurement$ConditionalUserProperty);
        zzbq.m9122(appMeasurement$ConditionalUserProperty.mAppId);
        m11110();
        m11125(new AppMeasurement$ConditionalUserProperty(appMeasurement$ConditionalUserProperty));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11159(AppMeasurement$OnEventListener appMeasurement$OnEventListener) {
        m11115();
        zzbq.m9120(appMeasurement$OnEventListener);
        if (!this.f9493.remove(appMeasurement$OnEventListener)) {
            m11096().m10834().m10849("OnEventListener had not been registered");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11160(String str, String str2, Bundle bundle) {
        m11123((String) null, str, str2, bundle);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m11161() {
        return super.m11108();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final String m11162(long j) {
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            m11101().m10986((Runnable) new zzcjz(this, atomicReference));
            try {
                atomicReference.wait(j);
            } catch (InterruptedException e) {
                m11096().m10834().m10849("Interrupted waiting for app instance id");
                return null;
            }
        }
        return (String) atomicReference.get();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11163() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<AppMeasurement$ConditionalUserProperty> m11164(String str, String str2) {
        return m11119((String) null, str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<AppMeasurement$ConditionalUserProperty> m11165(String str, String str2, String str3) {
        zzbq.m9122(str);
        m11110();
        return m11119(str, str2, str3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<String, Object> m11166(String str, String str2, String str3, boolean z) {
        zzbq.m9122(str);
        m11110();
        return m11120(str, str2, str3, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<String, Object> m11167(String str, String str2, boolean z) {
        return m11120((String) null, str, str2, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11168() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11169(long j) {
        m11101().m10986((Runnable) new zzcjt(this, j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11170(AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty) {
        zzbq.m9120(appMeasurement$ConditionalUserProperty);
        AppMeasurement$ConditionalUserProperty appMeasurement$ConditionalUserProperty2 = new AppMeasurement$ConditionalUserProperty(appMeasurement$ConditionalUserProperty);
        if (!TextUtils.isEmpty(appMeasurement$ConditionalUserProperty2.mAppId)) {
            m11096().m10834().m10849("Package name should be null when calling setConditionalUserProperty");
        }
        appMeasurement$ConditionalUserProperty2.mAppId = null;
        m11125(appMeasurement$ConditionalUserProperty2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11171(AppMeasurement$EventInterceptor appMeasurement$EventInterceptor) {
        m11109();
        m11115();
        if (!(appMeasurement$EventInterceptor == null || appMeasurement$EventInterceptor == this.f9491)) {
            zzbq.m9126(this.f9491 == null, (Object) "EventInterceptor already set.");
        }
        this.f9491 = appMeasurement$EventInterceptor;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11172(AppMeasurement$OnEventListener appMeasurement$OnEventListener) {
        m11115();
        zzbq.m9120(appMeasurement$OnEventListener);
        if (!this.f9493.add(appMeasurement$OnEventListener)) {
            m11096().m10834().m10849("OnEventListener already registered");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11173(String str) {
        this.f9490.set(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11174(String str, String str2, Bundle bundle) {
        m11133(str, str2, bundle, true, this.f9491 == null || zzclq.m11368(str2), false, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11175(String str, String str2, Bundle bundle, long j) {
        m11131(str, str2, j, bundle, false, true, true, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11176(String str, String str2, Bundle bundle, boolean z) {
        m11133(str, str2, bundle, true, this.f9491 == null || zzclq.m11368(str2), true, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11177(String str, String str2, Object obj) {
        int i = 0;
        zzbq.m9122(str);
        long r4 = m11105().m9243();
        int r1 = m11112().m11420(str2);
        if (r1 != 0) {
            m11112();
            String r2 = zzclq.m11378(str2, 24, true);
            if (str2 != null) {
                i = str2.length();
            }
            this.f9487.m11063().m11439(r1, "_ev", r2, i);
        } else if (obj != null) {
            int r12 = m11112().m11423(str2, obj);
            if (r12 != 0) {
                m11112();
                String r22 = zzclq.m11378(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i = String.valueOf(obj).length();
                }
                this.f9487.m11063().m11439(r12, "_ev", r22, i);
                return;
            }
            Object r6 = m11112().m11430(str2, obj);
            if (r6 != null) {
                m11132(str, str2, r4, r6);
            }
        } else {
            m11132(str, str2, r4, (Object) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11178(String str, String str2, String str3, Bundle bundle) {
        zzbq.m9122(str);
        m11110();
        m11123(str, str2, str3, bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11179(boolean z) {
        m11115();
        m11101().m10986((Runnable) new zzcjo(this, z));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m11180() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m11181() {
        return super.m11112();
    }
}
