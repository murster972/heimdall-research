package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(14)
final class zzhh implements Application.ActivityLifecycleCallbacks {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final List<zzhj> f10684 = new ArrayList();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<zzhw> f10685 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    private Runnable f10686;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f10687 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private long f10688;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f10689 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f10690;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f10691 = true;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Object f10692 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private Activity f10693;

    zzhh() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m12991(Activity activity) {
        synchronized (this.f10692) {
            if (!activity.getClass().getName().startsWith("com.google.android.gms.ads")) {
                this.f10693 = activity;
            }
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onActivityDestroyed(android.app.Activity r6) {
        /*
            r5 = this;
            java.lang.Object r1 = r5.f10692
            monitor-enter(r1)
            android.app.Activity r0 = r5.f10693     // Catch:{ all -> 0x0042 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
        L_0x0008:
            return
        L_0x0009:
            android.app.Activity r0 = r5.f10693     // Catch:{ all -> 0x0042 }
            boolean r0 = r0.equals(r6)     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0014
            r0 = 0
            r5.f10693 = r0     // Catch:{ all -> 0x0042 }
        L_0x0014:
            java.util.List<com.google.android.gms.internal.zzhw> r0 = r5.f10685     // Catch:{ all -> 0x0042 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0042 }
        L_0x001a:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0045
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0042 }
            com.google.android.gms.internal.zzhw r0 = (com.google.android.gms.internal.zzhw) r0     // Catch:{ all -> 0x0042 }
            boolean r0 = r0.m12999(r6)     // Catch:{ Exception -> 0x0030 }
            if (r0 == 0) goto L_0x001a
            r2.remove()     // Catch:{ Exception -> 0x0030 }
            goto L_0x001a
        L_0x0030:
            r0 = move-exception
            com.google.android.gms.internal.zzaft r3 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ all -> 0x0042 }
            java.lang.String r4 = "AppActivityTracker.ActivityListener.onActivityDestroyed"
            r3.m4505((java.lang.Throwable) r0, (java.lang.String) r4)     // Catch:{ all -> 0x0042 }
            java.lang.String r3 = "onActivityStateChangedListener threw exception."
            com.google.android.gms.internal.zzagf.m4793(r3, r0)     // Catch:{ all -> 0x0042 }
            goto L_0x001a
        L_0x0042:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            throw r0
        L_0x0045:
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhh.onActivityDestroyed(android.app.Activity):void");
    }

    public final void onActivityPaused(Activity activity) {
        m12991(activity);
        synchronized (this.f10692) {
            Iterator<zzhw> it2 = this.f10685.iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
        this.f10689 = true;
        if (this.f10686 != null) {
            zzahn.f4212.removeCallbacks(this.f10686);
        }
        Handler handler = zzahn.f4212;
        zzhi zzhi = new zzhi(this);
        this.f10686 = zzhi;
        handler.postDelayed(zzhi, this.f10688);
    }

    public final void onActivityResumed(Activity activity) {
        boolean z = false;
        m12991(activity);
        this.f10689 = false;
        if (!this.f10691) {
            z = true;
        }
        this.f10691 = true;
        if (this.f10686 != null) {
            zzahn.f4212.removeCallbacks(this.f10686);
        }
        synchronized (this.f10692) {
            Iterator<zzhw> it2 = this.f10685.iterator();
            while (it2.hasNext()) {
                it2.next();
            }
            if (z) {
                for (zzhj r0 : this.f10684) {
                    try {
                        r0.m12997(true);
                    } catch (Exception e) {
                        zzagf.m4793("OnForegroundStateChangedListener threw exception.", e);
                    }
                }
            } else {
                zzagf.m4792("App is still foreground.");
            }
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
        m12991(activity);
    }

    public final void onActivityStopped(Activity activity) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context m12993() {
        return this.f10690;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Activity m12994() {
        return this.f10693;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12995(Application application, Context context) {
        if (!this.f10687) {
            application.registerActivityLifecycleCallbacks(this);
            if (context instanceof Activity) {
                m12991((Activity) context);
            }
            this.f10690 = application;
            this.f10688 = ((Long) zzkb.m5481().m5595(zznh.f4912)).longValue();
            this.f10687 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12996(zzhj zzhj) {
        synchronized (this.f10692) {
            this.f10684.add(zzhj);
        }
    }
}
