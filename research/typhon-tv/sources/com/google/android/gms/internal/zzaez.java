package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzaez implements Parcelable.Creator<zzaey> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r9 = zzbfn.m10169(parcel);
        boolean z = false;
        boolean z2 = false;
        ArrayList<String> arrayList = null;
        boolean z3 = false;
        boolean z4 = false;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < r9) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    z4 = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 7:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 8:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r9);
        return new zzaey(str2, str, z4, z3, arrayList, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaey[i];
    }
}
