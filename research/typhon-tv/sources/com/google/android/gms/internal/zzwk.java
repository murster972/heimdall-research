package com.google.android.gms.internal;

import com.google.ads.AdRequest$ErrorCode;
import com.google.ads.AdRequest$Gender;

final /* synthetic */ class zzwk {

    /* renamed from: 靐  reason: contains not printable characters */
    private static /* synthetic */ int[] f10946 = new int[AdRequest$Gender.values().length];

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ int[] f10947 = new int[AdRequest$ErrorCode.values().length];

    static {
        try {
            f10947[AdRequest$ErrorCode.INTERNAL_ERROR.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f10947[AdRequest$ErrorCode.INVALID_REQUEST.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f10947[AdRequest$ErrorCode.NETWORK_ERROR.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f10947[AdRequest$ErrorCode.NO_FILL.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f10946[AdRequest$Gender.FEMALE.ordinal()] = 1;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f10946[AdRequest$Gender.MALE.ordinal()] = 2;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f10946[AdRequest$Gender.UNKNOWN.ordinal()] = 3;
        } catch (NoSuchFieldError e7) {
        }
    }
}
