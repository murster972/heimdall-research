package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;

public final class zzbhl {
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public int f8768 = -1;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public Map<String, String> f8769;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public int f8770 = -1;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public int f8771;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public long f8772 = 43200;

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzbhl m10244(int i) {
        this.f8770 = i;
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzbhl m10245(int i) {
        this.f8768 = i;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbhk m10246() {
        return new zzbhk(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbhl m10247(int i) {
        this.f8771 = 10300;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbhl m10248(long j) {
        this.f8772 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbhl m10249(String str, String str2) {
        if (this.f8769 == null) {
            this.f8769 = new HashMap();
        }
        this.f8769.put(str, str2);
        return this;
    }
}
