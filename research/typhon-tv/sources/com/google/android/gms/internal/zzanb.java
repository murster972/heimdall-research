package com.google.android.gms.internal;

import android.support.v4.app.NotificationCompat;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

final class zzanb implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzana f8324;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ boolean f8325 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8326;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ int f8327;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f8328;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f8329;

    zzanb(zzana zzana, String str, String str2, int i, int i2, boolean z) {
        this.f8324 = zzana;
        this.f8329 = str;
        this.f8326 = str2;
        this.f8328 = i;
        this.f8327 = i2;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, "precacheProgress");
        hashMap.put("src", this.f8329);
        hashMap.put("cachedSrc", this.f8326);
        hashMap.put("bytesLoaded", Integer.toString(this.f8328));
        hashMap.put("totalBytes", Integer.toString(this.f8327));
        hashMap.put("cacheReady", this.f8325 ? PubnativeRequest.LEGACY_ZONE_ID : "0");
        this.f8324.m4961("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
