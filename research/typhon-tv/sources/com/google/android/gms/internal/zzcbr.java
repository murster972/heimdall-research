package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzcbr extends zzbfm {
    public static final Parcelable.Creator<zzcbr> CREATOR = new zzcbs();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzaz f9028 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f9029;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9030;

    zzcbr(int i, byte[] bArr) {
        this.f9030 = i;
        this.f9029 = bArr;
        m10323();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m10323() {
        if (this.f9028 == null && this.f9029 != null) {
            return;
        }
        if (this.f9028 != null && this.f9029 == null) {
            return;
        }
        if (this.f9028 != null && this.f9029 != null) {
            throw new IllegalStateException("Invalid internal representation - full");
        } else if (this.f9028 == null && this.f9029 == null) {
            throw new IllegalStateException("Invalid internal representation - empty");
        } else {
            throw new IllegalStateException("Impossible");
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r1 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9030);
        zzbfp.m10196(parcel, 2, this.f9029 != null ? this.f9029 : zzfjs.m12871((zzfjs) this.f9028), false);
        zzbfp.m10182(parcel, r1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaz m10324() {
        if (!(this.f9028 != null)) {
            try {
                this.f9028 = (zzaz) zzfjs.m12869(new zzaz(), this.f9029);
                this.f9029 = null;
            } catch (zzfjr e) {
                throw new IllegalStateException(e);
            }
        }
        m10323();
        return this.f9028;
    }
}
