package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfjr extends IOException {
    public zzfjr(String str) {
        super(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static zzfjr m12865() {
        return new zzfjr("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static zzfjr m12866() {
        return new zzfjr("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static zzfjr m12867() {
        return new zzfjr("CodedInputStream encountered a malformed varint.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzfjr m12868() {
        return new zzfjr("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }
}
