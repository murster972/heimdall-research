package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import java.io.InputStream;

@zzzv
public final class zzii extends zzbfm {
    public static final Parcelable.Creator<zzii> CREATOR = new zzij();

    /* renamed from: 龘  reason: contains not printable characters */
    private ParcelFileDescriptor f4737;

    public zzii() {
        this((ParcelFileDescriptor) null);
    }

    public zzii(ParcelFileDescriptor parcelFileDescriptor) {
        this.f4737 = parcelFileDescriptor;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private synchronized ParcelFileDescriptor m5421() {
        return this.f4737;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 2, (Parcelable) m5421(), i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized InputStream m5422() {
        ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = null;
        synchronized (this) {
            if (this.f4737 != null) {
                autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream(this.f4737);
                this.f4737 = null;
            }
        }
        return autoCloseInputStream;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized boolean m5423() {
        return this.f4737 != null;
    }
}
