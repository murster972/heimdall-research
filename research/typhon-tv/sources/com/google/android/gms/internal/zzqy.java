package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzqy extends zzeu implements zzqw {
    zzqy(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnCustomClickListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13349(zzqm zzqm, String str) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzqm);
        v_.writeString(str);
        m12298(1, v_);
    }
}
