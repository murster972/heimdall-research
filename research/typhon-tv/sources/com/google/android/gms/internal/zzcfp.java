package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.List;

public final class zzcfp implements Parcelable.Creator<zzcfo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r9 = zzbfn.m10169(parcel);
        List<zzcdv> list = zzcfo.f4591;
        String str = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        String str2 = null;
        LocationRequest locationRequest = null;
        while (parcel.dataPosition() < r9) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    locationRequest = (LocationRequest) zzbfn.m10171(parcel, readInt, LocationRequest.CREATOR);
                    break;
                case 5:
                    list = zzbfn.m10167(parcel, readInt, zzcdv.CREATOR);
                    break;
                case 6:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 8:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 10:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r9);
        return new zzcfo(locationRequest, list, str2, z3, z2, z, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcfo[i];
    }
}
