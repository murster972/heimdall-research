package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.PriorityQueue;
import org.apache.commons.lang3.StringUtils;

@zzzv
public final class zzhr {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f4715;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzhq f4716 = new zzhv();

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f4717;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f4718;

    public zzhr(int i) {
        this.f4715 = i;
        this.f4718 = 6;
        this.f4717 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final String m5397(String str) {
        String[] split = str.split(StringUtils.LF);
        if (split.length == 0) {
            return "";
        }
        zzht zzht = new zzht();
        PriorityQueue priorityQueue = new PriorityQueue(this.f4715, new zzhs(this));
        for (String r5 : split) {
            String[] r52 = zzhu.m5400(r5, false);
            if (r52.length != 0) {
                zzhx.m5406(r52, this.f4715, this.f4718, priorityQueue);
            }
        }
        Iterator it2 = priorityQueue.iterator();
        while (it2.hasNext()) {
            try {
                zzht.m12998(this.f4716.m5396(((zzhy) it2.next()).f10705));
            } catch (IOException e) {
                zzagf.m4793("Error while writing hash to byteStream", e);
            }
        }
        return zzht.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5398(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            stringBuffer.append(((String) obj).toLowerCase(Locale.US));
            stringBuffer.append(10);
        }
        return m5397(stringBuffer.toString());
    }
}
