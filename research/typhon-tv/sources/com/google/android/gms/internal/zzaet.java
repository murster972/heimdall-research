package com.google.android.gms.internal;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;

final class zzaet implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzaes f8133;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Bitmap f8134;

    zzaet(zzaes zzaes, Bitmap bitmap) {
        this.f8133 = zzaes;
        this.f8134 = bitmap;
    }

    public final void run() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.f8134.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        synchronized (this.f8133.f4058) {
            this.f8133.f4064.f10565 = new zzfkd();
            this.f8133.f4064.f10565.f10602 = byteArrayOutputStream.toByteArray();
            this.f8133.f4064.f10565.f10601 = "image/png";
            this.f8133.f4064.f10565.f10603 = 1;
        }
    }
}
