package com.google.android.gms.internal;

public enum zzdtb implements zzfga {
    UNKNOWN_STATUS(0),
    ENABLED(1),
    DISABLED(2),
    DESTROYED(3),
    UNRECOGNIZED(-1);
    

    /* renamed from: 齉  reason: contains not printable characters */
    private static final zzfgb<zzdtb> f10103 = null;
    private final int value;

    static {
        f10103 = new zzdtc();
    }

    private zzdtb(int i) {
        this.value = i;
    }

    public static zzdtb zzfu(int i) {
        switch (i) {
            case 0:
                return UNKNOWN_STATUS;
            case 1:
                return ENABLED;
            case 2:
                return DISABLED;
            case 3:
                return DESTROYED;
            default:
                return null;
        }
    }

    public final int zzhq() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
}
