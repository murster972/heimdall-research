package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

@zzzv
final class zzakw {

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<Runnable> f4300 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4301 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4302 = new Object();

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        r1 = r0.get(r2);
        r2 = r2 + 1;
        ((java.lang.Runnable) r1).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        r0 = r0;
        r3 = r0.size();
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (r2 >= r3) goto L_0x000d;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4815() {
        /*
            r4 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Object r1 = r4.f4302
            monitor-enter(r1)
            boolean r2 = r4.f4301     // Catch:{ all -> 0x0032 }
            if (r2 == 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x0032 }
        L_0x000d:
            return
        L_0x000e:
            java.util.List<java.lang.Runnable> r2 = r4.f4300     // Catch:{ all -> 0x0032 }
            r0.addAll(r2)     // Catch:{ all -> 0x0032 }
            java.util.List<java.lang.Runnable> r2 = r4.f4300     // Catch:{ all -> 0x0032 }
            r2.clear()     // Catch:{ all -> 0x0032 }
            r2 = 1
            r4.f4301 = r2     // Catch:{ all -> 0x0032 }
            monitor-exit(r1)     // Catch:{ all -> 0x0032 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            int r3 = r0.size()
            r1 = 0
            r2 = r1
        L_0x0024:
            if (r2 >= r3) goto L_0x000d
            java.lang.Object r1 = r0.get(r2)
            int r2 = r2 + 1
            java.lang.Runnable r1 = (java.lang.Runnable) r1
            r1.run()
            goto L_0x0024
        L_0x0032:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0032 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzakw.m4815():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4816(Runnable runnable, Executor executor) {
        synchronized (this.f4302) {
            if (this.f4301) {
                executor.execute(runnable);
            } else {
                this.f4300.add(new zzakx(executor, runnable));
            }
        }
    }
}
