package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;

public final class zzcgl extends zzbfm {
    public static final Parcelable.Creator<zzcgl> CREATOR = new zzcgm();

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f9145;

    /* renamed from: ʼ  reason: contains not printable characters */
    public zzcha f9146;

    /* renamed from: ʽ  reason: contains not printable characters */
    public long f9147;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f9148;

    /* renamed from: ˑ  reason: contains not printable characters */
    public zzcha f9149;

    /* renamed from: ٴ  reason: contains not printable characters */
    public long f9150;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public zzcha f9151;

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f9152;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9153;

    /* renamed from: 麤  reason: contains not printable characters */
    public long f9154;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzcln f9155;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f9156;

    zzcgl(int i, String str, String str2, zzcln zzcln, long j, boolean z, String str3, zzcha zzcha, long j2, zzcha zzcha2, long j3, zzcha zzcha3) {
        this.f9148 = i;
        this.f9156 = str;
        this.f9153 = str2;
        this.f9155 = zzcln;
        this.f9154 = j;
        this.f9152 = z;
        this.f9145 = str3;
        this.f9146 = zzcha;
        this.f9147 = j2;
        this.f9149 = zzcha2;
        this.f9150 = j3;
        this.f9151 = zzcha3;
    }

    zzcgl(zzcgl zzcgl) {
        this.f9148 = 1;
        zzbq.m9120(zzcgl);
        this.f9156 = zzcgl.f9156;
        this.f9153 = zzcgl.f9153;
        this.f9155 = zzcgl.f9155;
        this.f9154 = zzcgl.f9154;
        this.f9152 = zzcgl.f9152;
        this.f9145 = zzcgl.f9145;
        this.f9146 = zzcgl.f9146;
        this.f9147 = zzcgl.f9147;
        this.f9149 = zzcgl.f9149;
        this.f9150 = zzcgl.f9150;
        this.f9151 = zzcgl.f9151;
    }

    zzcgl(String str, String str2, zzcln zzcln, long j, boolean z, String str3, zzcha zzcha, long j2, zzcha zzcha2, long j3, zzcha zzcha3) {
        this.f9148 = 1;
        this.f9156 = str;
        this.f9153 = str2;
        this.f9155 = zzcln;
        this.f9154 = j;
        this.f9152 = z;
        this.f9145 = str3;
        this.f9146 = zzcha;
        this.f9147 = j2;
        this.f9149 = zzcha2;
        this.f9150 = j3;
        this.f9151 = zzcha3;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9148);
        zzbfp.m10193(parcel, 2, this.f9156, false);
        zzbfp.m10193(parcel, 3, this.f9153, false);
        zzbfp.m10189(parcel, 4, (Parcelable) this.f9155, i, false);
        zzbfp.m10186(parcel, 5, this.f9154);
        zzbfp.m10195(parcel, 6, this.f9152);
        zzbfp.m10193(parcel, 7, this.f9145, false);
        zzbfp.m10189(parcel, 8, (Parcelable) this.f9146, i, false);
        zzbfp.m10186(parcel, 9, this.f9147);
        zzbfp.m10189(parcel, 10, (Parcelable) this.f9149, i, false);
        zzbfp.m10186(parcel, 11, this.f9150);
        zzbfp.m10189(parcel, 12, (Parcelable) this.f9151, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
