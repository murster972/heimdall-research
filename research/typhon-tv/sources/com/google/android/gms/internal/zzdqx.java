package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrc;
import com.google.android.gms.internal.zzdrg;
import com.google.android.gms.internal.zzdrs;
import com.google.android.gms.internal.zzdss;
import java.security.GeneralSecurityException;
import java.util.Arrays;

final class zzdqx implements zzduj {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f9952;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f9953;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdrc f9954;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzdrs f9955;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f9956;

    zzdqx(zzdtd zzdtd) throws GeneralSecurityException {
        this.f9956 = zzdtd.m12037();
        if (this.f9956.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
            try {
                zzdru r1 = zzdru.m11876(zzdtd.m12034());
                this.f9955 = (zzdrs) zzdqe.m11672(zzdtd);
                this.f9953 = r1.m11877();
            } catch (zzfge e) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e);
            }
        } else if (this.f9956.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
            try {
                zzdre r12 = zzdre.m11788(zzdtd.m12034());
                this.f9954 = (zzdrc) zzdqe.m11672(zzdtd);
                this.f9952 = r12.m11791().m11816();
                this.f9953 = r12.m11789().m11994() + this.f9952;
            } catch (zzfge e2) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e2);
            }
        } else {
            String valueOf = String.valueOf(this.f9956);
            throw new GeneralSecurityException(valueOf.length() != 0 ? "unsupported AEAD DEM key type: ".concat(valueOf) : new String("unsupported AEAD DEM key type: "));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11750() {
        return this.f9953;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdpp m11751(byte[] bArr) throws GeneralSecurityException {
        if (this.f9956.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
            return (zzdpp) zzdqe.m11673(this.f9956, (zzdrs) ((zzdrs.zza) zzdrs.m11860().m12545(this.f9955)).m11874(zzfes.zzaz(bArr)).m12542());
        } else if (this.f9956.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, this.f9952);
            byte[] copyOfRange2 = Arrays.copyOfRange(bArr, this.f9952, this.f9953);
            zzdrc.zza r0 = zzdrc.m11770().m11784(this.f9954.m11781()).m11785((zzdrg) ((zzdrg.zza) zzdrg.m11796().m12545(this.f9954.m11778())).m11812(zzfes.zzaz(copyOfRange)).m12542());
            return (zzdpp) zzdqe.m11673(this.f9956, (zzdrc) r0.m11786((zzdss) ((zzdss.zza) zzdss.m11974().m12545(this.f9954.m11780())).m11990(zzfes.zzaz(copyOfRange2)).m12542()).m12542());
        } else {
            throw new GeneralSecurityException("unknown DEM key type");
        }
    }
}
