package com.google.android.gms.internal;

import android.os.Process;

final class zzbhc implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f8751;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Runnable f8752;

    public zzbhc(Runnable runnable, int i) {
        this.f8752 = runnable;
        this.f8751 = i;
    }

    public final void run() {
        Process.setThreadPriority(this.f8751);
        this.f8752.run();
    }
}
