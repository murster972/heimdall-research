package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.zzp;
import com.google.android.gms.location.zzq;
import com.google.android.gms.location.zzs;
import com.google.android.gms.location.zzt;

public final class zzcfq extends zzbfm {
    public static final Parcelable.Creator<zzcfq> CREATOR = new zzcfr();

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzceu f9083;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzp f9084;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzcfo f9085;

    /* renamed from: 麤  reason: contains not printable characters */
    private PendingIntent f9086;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzs f9087;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9088;

    zzcfq(int i, zzcfo zzcfo, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        zzceu zzceu = null;
        this.f9088 = i;
        this.f9085 = zzcfo;
        this.f9087 = iBinder == null ? null : zzt.m13695(iBinder);
        this.f9086 = pendingIntent;
        this.f9084 = iBinder2 == null ? null : zzq.m13691(iBinder2);
        if (!(iBinder3 == null || iBinder3 == null)) {
            IInterface queryLocalInterface = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            zzceu = queryLocalInterface instanceof zzceu ? (zzceu) queryLocalInterface : new zzcew(iBinder3);
        }
        this.f9083 = zzceu;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzcfq m10417(zzp zzp, zzceu zzceu) {
        return new zzcfq(2, (zzcfo) null, (IBinder) null, (PendingIntent) null, zzp.asBinder(), zzceu != null ? zzceu.asBinder() : null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzcfq m10418(zzs zzs, zzceu zzceu) {
        return new zzcfq(2, (zzcfo) null, zzs.asBinder(), (PendingIntent) null, (IBinder) null, zzceu != null ? zzceu.asBinder() : null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        IBinder iBinder = null;
        int r2 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9088);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f9085, i, false);
        zzbfp.m10188(parcel, 3, this.f9087 == null ? null : this.f9087.asBinder(), false);
        zzbfp.m10189(parcel, 4, (Parcelable) this.f9086, i, false);
        zzbfp.m10188(parcel, 5, this.f9084 == null ? null : this.f9084.asBinder(), false);
        if (this.f9083 != null) {
            iBinder = this.f9083.asBinder();
        }
        zzbfp.m10188(parcel, 6, iBinder, false);
        zzbfp.m10182(parcel, r2);
    }
}
