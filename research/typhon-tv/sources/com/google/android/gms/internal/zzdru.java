package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrw;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdru extends zzffu<zzdru, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdru f10007;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdru> f10008;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10009;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdrw f10010;

    public static final class zza extends zzffu.zza<zzdru, zza> implements zzfhg {
        private zza() {
            super(zzdru.f10007);
        }

        /* synthetic */ zza(zzdrv zzdrv) {
            this();
        }
    }

    static {
        zzdru zzdru = new zzdru();
        f10007 = zzdru;
        zzdru.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdru.f10394.m12718();
    }

    private zzdru() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdru m11876(zzfes zzfes) throws zzfge {
        return (zzdru) zzffu.m12526(f10007, zzfes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m11877() {
        return this.f10009;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11878() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10010 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f10010 == null ? zzdrw.m11883() : this.f10010) + 0;
        }
        if (this.f10009 != 0) {
            i2 += zzffg.m5245(2, this.f10009);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdrw m11879() {
        return this.f10010 == null ? zzdrw.m11883() : this.f10010;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11880(int i, Object obj, Object obj2) {
        zzdrw.zza zza2;
        boolean z = true;
        switch (zzdrv.f10011[i - 1]) {
            case 1:
                return new zzdru();
            case 2:
                return f10007;
            case 3:
                return null;
            case 4:
                return new zza((zzdrv) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdru zzdru = (zzdru) obj2;
                this.f10010 = (zzdrw) zzh.m12572(this.f10010, zzdru.f10010);
                boolean z2 = this.f10009 != 0;
                int i2 = this.f10009;
                if (zzdru.f10009 == 0) {
                    z = false;
                }
                this.f10009 = zzh.m12569(z2, i2, z, zzdru.f10009);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 10:
                                    if (this.f10010 != null) {
                                        zzdrw zzdrw = this.f10010;
                                        zzffu.zza zza3 = (zzffu.zza) zzdrw.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdrw);
                                        zza2 = (zzdrw.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10010 = (zzdrw) zzffb.m12416(zzdrw.m11883(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10010);
                                        this.f10010 = (zzdrw) zza2.m12543();
                                        break;
                                    }
                                case 16:
                                    this.f10009 = zzffb.m12402();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10008 == null) {
                    synchronized (zzdru.class) {
                        if (f10008 == null) {
                            f10008 = new zzffu.zzb(f10007);
                        }
                    }
                }
                return f10008;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10007;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11881(zzffg zzffg) throws IOException {
        if (this.f10010 != null) {
            zzffg.m5289(1, (zzfhe) this.f10010 == null ? zzdrw.m11883() : this.f10010);
        }
        if (this.f10009 != 0) {
            zzffg.m5281(2, this.f10009);
        }
        this.f10394.m12719(zzffg);
    }
}
