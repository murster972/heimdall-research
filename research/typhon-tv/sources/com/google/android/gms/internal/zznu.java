package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@zzzv
public final class zznu {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zznu f5188;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f5189;

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<zzns> f5190 = new LinkedList();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Object f5191 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Map<String, String> f5192 = new LinkedHashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f5193;

    public zznu(boolean z, String str, String str2) {
        this.f5193 = z;
        this.f5192.put("action", str);
        this.f5192.put("ad_format", str2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5622() {
        String sb;
        StringBuilder sb2 = new StringBuilder();
        synchronized (this.f5191) {
            for (zzns next : this.f5190) {
                long r4 = next.m5618();
                String r6 = next.m5616();
                zzns r0 = next.m5617();
                if (r0 != null && r4 > 0) {
                    sb2.append(r6).append('.').append(r4 - r0.m5618()).append(',');
                }
            }
            this.f5190.clear();
            if (!TextUtils.isEmpty(this.f5189)) {
                sb2.append(this.f5189);
            } else if (sb2.length() > 0) {
                sb2.setLength(sb2.length() - 1);
            }
            sb = sb2.toString();
        }
        return sb;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzns m5623() {
        synchronized (this.f5191) {
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final Map<String, String> m5624() {
        Map<String, String> r0;
        synchronized (this.f5191) {
            zznk r02 = zzbs.zzem().m4483();
            r0 = (r02 == null || this.f5188 == null) ? this.f5192 : r02.m5609(this.f5192, this.f5188.m5624());
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzns m5625() {
        return m5626(zzbs.zzeo().m9241());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzns m5626(long j) {
        if (!this.f5193) {
            return null;
        }
        return new zzns(j, (String) null, (zzns) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5627(zznu zznu) {
        synchronized (this.f5191) {
            this.f5188 = zznu;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5628(String str) {
        if (this.f5193) {
            synchronized (this.f5191) {
                this.f5189 = str;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5629(String str, String str2) {
        zznk r0;
        if (this.f5193 && !TextUtils.isEmpty(str2) && (r0 = zzbs.zzem().m4483()) != null) {
            synchronized (this.f5191) {
                zzno r2 = r0.m5608(str);
                Map<String, String> map = this.f5192;
                map.put(str, r2.m5615(map.get(str), str2));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5630(zzns zzns, long j, String... strArr) {
        synchronized (this.f5191) {
            for (String zzns2 : strArr) {
                this.f5190.add(new zzns(j, zzns2, zzns));
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5631(zzns zzns, String... strArr) {
        if (!this.f5193 || zzns == null) {
            return false;
        }
        return m5630(zzns, zzbs.zzeo().m9241(), strArr);
    }
}
