package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdth;
import com.google.android.gms.internal.zzdtj;
import java.security.GeneralSecurityException;

public final class zzdpx {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzdth f9915;

    private zzdpx(zzdth zzdth) {
        this.f9915 = zzdth;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final zzdpx m11658(zzdth zzdth) throws GeneralSecurityException {
        if (zzdth != null && zzdth.m12069() > 0) {
            return new zzdpx(zzdth);
        }
        throw new GeneralSecurityException("empty keyset");
    }

    public final String toString() {
        zzdth zzdth = this.f9915;
        zzdtj.zza r1 = zzdtj.m12084().m12092(zzdth.m12070());
        for (zzdth.zzb next : zzdth.m12067()) {
            r1.m12093((zzdtj.zzb) zzdtj.zzb.m12096().m12111(next.m12077().m12019()).m12109(next.m12079()).m12110(next.m12075()).m12108(next.m12076()).m12542());
        }
        return ((zzdtj) r1.m12542()).toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdth m11659() {
        return this.f9915;
    }
}
