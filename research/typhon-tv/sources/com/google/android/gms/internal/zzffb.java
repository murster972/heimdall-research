package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.InputStream;

public abstract class zzffb {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile boolean f10351 = true;

    /* renamed from: 靐  reason: contains not printable characters */
    int f10352;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f10353;

    /* renamed from: 齉  reason: contains not printable characters */
    int f10354;

    /* renamed from: 龘  reason: contains not printable characters */
    int f10355;

    private zzffb() {
        this.f10352 = 100;
        this.f10354 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        this.f10353 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzffb m12397(InputStream inputStream) {
        if (inputStream != null) {
            return new zzffe(inputStream);
        }
        byte[] bArr = zzffz.f10420;
        return m12399(bArr, 0, bArr.length, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzffb m12398(byte[] bArr) {
        return m12399(bArr, 0, bArr.length, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzffb m12399(byte[] bArr, int i, int i2, boolean z) {
        zzffd zzffd = new zzffd(bArr, i, i2, z);
        try {
            zzffd.m12409(i2);
            return zzffd;
        } catch (zzfge e) {
            throw new IllegalArgumentException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract String m12400() throws IOException;

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract zzfes m12401() throws IOException;

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract int m12402() throws IOException;

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract int m12403();

    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract boolean m12404() throws IOException;

    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract int m12405() throws IOException;

    /* renamed from: ٴ  reason: contains not printable characters */
    public abstract int m12406() throws IOException;

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public abstract long m12407() throws IOException;

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract boolean m12408() throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract int m12409(int i) throws zzfge;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract long m12410() throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract int m12411() throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m12412(int i) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract long m12413() throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m12414(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m12415() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract <T extends zzffu<T, ?>> T m12416(T t, zzffm zzffm) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m12417(int i) throws zzfge;
}
