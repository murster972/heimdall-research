package com.google.android.gms.internal;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;

@zzzv
public final class zzaar extends zzaan implements zzf, zzg {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzaas f3709;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Object f3710 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzakd f3711;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzaal f3712;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzalh<zzaat> f3713;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f3714;

    public zzaar(Context context, zzakd zzakd, zzalh<zzaat> zzalh, zzaal zzaal) {
        super(zzalh, zzaal);
        this.f3714 = context;
        this.f3711 = zzakd;
        this.f3713 = zzalh;
        this.f3712 = zzaal;
        this.f3709 = new zzaas(context, ((Boolean) zzkb.m5481().m5595(zznh.f5153)).booleanValue() ? zzbs.zzew().m4718() : context.getMainLooper(), this, this, this.f3711.f4296);
        this.f3709.m9167();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzabb m4252() {
        zzabb zzabb;
        synchronized (this.f3710) {
            try {
                zzabb = this.f3709.m4257();
            } catch (DeadObjectException | IllegalStateException e) {
                zzabb = null;
            }
        }
        return zzabb;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4253() {
        synchronized (this.f3710) {
            if (this.f3709.m9161() || this.f3709.m9162()) {
                this.f3709.m9160();
            }
            Binder.flushPendingCommands();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4254(int i) {
        zzagf.m4792("Disconnected from remote ad request service.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4255(Bundle bundle) {
        m4245();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4256(ConnectionResult connectionResult) {
        zzagf.m4792("Cannot connect to remote service, fallback to local instance.");
        new zzaaq(this.f3714, this.f3713, this.f3712).m4674();
        Bundle bundle = new Bundle();
        bundle.putString("action", "gms_connection_failed_fallback_to_local");
        zzbs.zzei().m4626(this.f3714, this.f3711.f4297, "gmob-apps", bundle, true);
    }
}
