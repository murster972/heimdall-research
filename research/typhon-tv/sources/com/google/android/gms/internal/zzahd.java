package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzahd extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8197;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8198;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzahd(Context context, String str) {
        super((zzagi) null);
        this.f8198 = context;
        this.f8197 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9602() {
        SharedPreferences.Editor edit = this.f8198.getSharedPreferences("admob", 0).edit();
        edit.putString("content_url_hashes", this.f8197);
        edit.apply();
    }
}
