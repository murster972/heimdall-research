package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzec extends zzet {

    /* renamed from: 麤  reason: contains not printable characters */
    private long f10257;

    public zzec(zzdm zzdm, String str, String str2, zzaz zzaz, long j, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 25);
        this.f10257 = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12275() throws IllegalAccessException, InvocationTargetException {
        long longValue = ((Long) this.f10286.invoke((Object) null, new Object[0])).longValue();
        synchronized (this.f10284) {
            this.f10284.f8461 = Long.valueOf(longValue);
            if (this.f10257 != 0) {
                this.f10284.f8453 = Long.valueOf(longValue - this.f10257);
                this.f10284.f8429 = Long.valueOf(this.f10257);
            }
        }
    }
}
