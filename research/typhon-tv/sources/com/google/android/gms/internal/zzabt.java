package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.js.zzaa;

final class zzabt implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzabo f8065;

    zzabt(zzabo zzabo) {
        this.f8065 = zzabo;
    }

    public final void run() {
        if (this.f8065.f3889 != null) {
            this.f8065.f3889.release();
            zzaa unused = this.f8065.f3889 = null;
        }
    }
}
