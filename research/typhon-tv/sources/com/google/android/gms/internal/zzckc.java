package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$zza;
import com.google.android.gms.measurement.AppMeasurement$zzb;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public final class zzckc extends zzcjl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CopyOnWriteArrayList<AppMeasurement$zza> f9539 = new CopyOnWriteArrayList<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f9540;

    /* renamed from: ʽ  reason: contains not printable characters */
    private AppMeasurement$zzb f9541;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f9542;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<Activity, zzckf> f9543 = new ArrayMap();

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile AppMeasurement$zzb f9544;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f9545;

    /* renamed from: 齉  reason: contains not printable characters */
    private AppMeasurement$zzb f9546;

    /* renamed from: 龘  reason: contains not printable characters */
    protected zzckf f9547;

    public zzckc(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m11183(String str) {
        String[] split = str.split("\\.");
        if (split.length == 0) {
            return str.substring(0, 36);
        }
        String str2 = split[split.length - 1];
        return str2.length() > 36 ? str2.substring(0, 36) : str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11184(Activity activity, zzckf zzckf, boolean z) {
        boolean z2;
        boolean z3 = true;
        AppMeasurement$zzb appMeasurement$zzb = this.f9544 != null ? this.f9544 : (this.f9546 == null || Math.abs(m11105().m9241() - this.f9545) >= 1000) ? null : this.f9546;
        AppMeasurement$zzb appMeasurement$zzb2 = appMeasurement$zzb != null ? new AppMeasurement$zzb(appMeasurement$zzb) : null;
        this.f9540 = true;
        try {
            Iterator<AppMeasurement$zza> it2 = this.f9539.iterator();
            while (it2.hasNext()) {
                try {
                    z3 &= it2.next().m13703(appMeasurement$zzb2, zzckf);
                } catch (Exception e) {
                    m11096().m10832().m10850("onScreenChangeCallback threw exception", e);
                }
            }
            this.f9540 = false;
            z2 = z3;
        } catch (Exception e2) {
            z2 = z3;
            m11096().m10832().m10850("onScreenChangeCallback loop threw exception", e2);
            this.f9540 = false;
        } catch (Throwable th) {
            this.f9540 = false;
            throw th;
        }
        AppMeasurement$zzb appMeasurement$zzb3 = this.f9544 == null ? this.f9546 : this.f9544;
        if (z2) {
            if (zzckf.f11090 == null) {
                zzckf.f11090 = m11183(activity.getClass().getCanonicalName());
            }
            zzckf zzckf2 = new zzckf(zzckf);
            this.f9546 = this.f9544;
            this.f9545 = m11105().m9241();
            this.f9544 = zzckf2;
            m11101().m10986((Runnable) new zzckd(this, z, appMeasurement$zzb3, zzckf2));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11186(zzckf zzckf) {
        m11108().m10456(m11105().m9241());
        if (m11100().m11327(zzckf.f9554)) {
            zzckf.f9554 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m11187(AppMeasurement$zzb appMeasurement$zzb, Bundle bundle) {
        if (bundle != null && appMeasurement$zzb != null && !bundle.containsKey("_sc")) {
            if (appMeasurement$zzb.f11088 != null) {
                bundle.putString("_sn", appMeasurement$zzb.f11088);
            }
            bundle.putString("_sc", appMeasurement$zzb.f11090);
            bundle.putLong("_si", appMeasurement$zzb.f11089);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m11188() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m11189() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m11190() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m11191() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m11192() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m11193() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m11194() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m11195() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m11196() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m11197() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m11198() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m11199() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m11200() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m11201() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m11202() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m11203() {
        return super.m11105();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final zzckf m11204() {
        m11115();
        m11109();
        return this.f9547;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final AppMeasurement$zzb m11205() {
        AppMeasurement$zzb appMeasurement$zzb = this.f9544;
        if (appMeasurement$zzb == null) {
            return null;
        }
        return new AppMeasurement$zzb(appMeasurement$zzb);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m11206() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11207() {
        super.m11107();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11208(Activity activity) {
        m11184(activity, m11214(activity), false);
        zzcgd r0 = m11108();
        r0.m11101().m10986((Runnable) new zzcgg(r0, r0.m11105().m9241()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11209(AppMeasurement$zza appMeasurement$zza) {
        this.f9539.remove(appMeasurement$zza);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m11210() {
        return super.m11108();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m11211(Activity activity) {
        this.f9543.remove(activity);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11212() {
        super.m11109();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m11213(Activity activity) {
        zzckf r0 = m11214(activity);
        this.f9546 = this.f9544;
        this.f9545 = m11105().m9241();
        this.f9544 = null;
        m11101().m10986((Runnable) new zzcke(this, r0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzckf m11214(Activity activity) {
        zzbq.m9120(activity);
        zzckf zzckf = this.f9543.get(activity);
        if (zzckf != null) {
            return zzckf;
        }
        zzckf zzckf2 = new zzckf((String) null, m11183(activity.getClass().getCanonicalName()), m11112().m11418());
        this.f9543.put(activity, zzckf2);
        return zzckf2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11215() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11216(Activity activity, Bundle bundle) {
        zzckf zzckf;
        if (bundle != null && (zzckf = this.f9543.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", zzckf.f11089);
            bundle2.putString("name", zzckf.f11088);
            bundle2.putString("referrer_name", zzckf.f11090);
            bundle.putBundle("com.google.firebase.analytics.screen_service", bundle2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11217(Activity activity, String str, String str2) {
        if (activity == null) {
            m11096().m10834().m10849("setCurrentScreen must be called with a non-null activity");
            return;
        }
        m11101();
        if (!zzcih.m10950()) {
            m11096().m10834().m10849("setCurrentScreen must be called from the main thread");
        } else if (this.f9540) {
            m11096().m10834().m10849("Cannot call setCurrentScreen from onScreenChangeCallback");
        } else if (this.f9544 == null) {
            m11096().m10834().m10849("setCurrentScreen cannot be called while no activity active");
        } else if (this.f9543.get(activity) == null) {
            m11096().m10834().m10849("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = m11183(activity.getClass().getCanonicalName());
            }
            boolean equals = this.f9544.f11090.equals(str2);
            boolean r1 = zzclq.m11387(this.f9544.f11088, str);
            if (equals && r1) {
                m11096().m10835().m10849("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                m11096().m10834().m10850("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                m11096().m10848().m10851("Setting current screen to name, class", str == null ? "null" : str, str2);
                zzckf zzckf = new zzckf(str, str2, m11112().m11418());
                this.f9543.put(activity, zzckf);
                m11184(activity, zzckf, true);
            } else {
                m11096().m10834().m10850("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11218(AppMeasurement$zza appMeasurement$zza) {
        if (appMeasurement$zza == null) {
            m11096().m10834().m10849("Attempting to register null OnScreenChangeCallback");
            return;
        }
        this.f9539.remove(appMeasurement$zza);
        this.f9539.add(appMeasurement$zza);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11219(String str, AppMeasurement$zzb appMeasurement$zzb) {
        m11109();
        synchronized (this) {
            if (this.f9542 == null || this.f9542.equals(str) || appMeasurement$zzb != null) {
                this.f9542 = str;
                this.f9541 = appMeasurement$zzb;
            }
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m11220() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m11221() {
        return super.m11112();
    }
}
