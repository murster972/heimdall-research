package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzzv
public final class zzhf {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f4684;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<zzhe> f4685 = new LinkedList();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4686 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5373(zzhe zzhe) {
        synchronized (this.f4686) {
            Iterator<zzhe> it2 = this.f4685.iterator();
            while (it2.hasNext()) {
                zzhe next = it2.next();
                if (!((Boolean) zzkb.m5481().m5595(zznh.f5130)).booleanValue() || zzbs.zzem().m4508()) {
                    if (((Boolean) zzkb.m5481().m5595(zznh.f5134)).booleanValue() && !zzbs.zzem().m4487() && zzhe != next && next.m5368().equals(zzhe.m5368())) {
                        it2.remove();
                        return true;
                    }
                } else if (zzhe != next && next.m5366().equals(zzhe.m5366())) {
                    it2.remove();
                    return true;
                }
            }
            return false;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5374(zzhe zzhe) {
        synchronized (this.f4686) {
            if (this.f4685.size() >= 10) {
                zzagf.m4792(new StringBuilder(41).append("Queue is full, current size = ").append(this.f4685.size()).toString());
                this.f4685.remove(0);
            }
            int i = this.f4684;
            this.f4684 = i + 1;
            zzhe.m5370(i);
            this.f4685.add(zzhe);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzhe m5375() {
        zzhe zzhe = null;
        synchronized (this.f4686) {
            if (this.f4685.size() == 0) {
                zzagf.m4792("Queue empty");
                return null;
            } else if (this.f4685.size() >= 2) {
                int i = Integer.MIN_VALUE;
                int i2 = 0;
                int i3 = 0;
                for (zzhe next : this.f4685) {
                    int r3 = next.m5363();
                    if (r3 > i) {
                        i2 = i3;
                    } else {
                        r3 = i;
                        next = zzhe;
                    }
                    i3++;
                    i = r3;
                    zzhe = next;
                }
                this.f4685.remove(i2);
                return zzhe;
            } else {
                zzhe zzhe2 = this.f4685.get(0);
                zzhe2.m5365();
                return zzhe2;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5376(zzhe zzhe) {
        boolean z;
        synchronized (this.f4686) {
            z = this.f4685.contains(zzhe);
        }
        return z;
    }
}
