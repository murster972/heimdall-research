package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;

final class zzfho<E> extends zzfeo<E> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfho<Object> f10452;

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<E> f10453;

    static {
        zzfho<Object> zzfho = new zzfho<>();
        f10452 = zzfho;
        zzfho.m12366();
    }

    zzfho() {
        this(new ArrayList(10));
    }

    private zzfho(List<E> list) {
        this.f10453 = list;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static <E> zzfho<E> m12644() {
        return f10452;
    }

    public final void add(int i, E e) {
        m12367();
        this.f10453.add(i, e);
        this.modCount++;
    }

    public final E get(int i) {
        return this.f10453.get(i);
    }

    public final E remove(int i) {
        m12367();
        E remove = this.f10453.remove(i);
        this.modCount++;
        return remove;
    }

    public final E set(int i, E e) {
        m12367();
        E e2 = this.f10453.set(i, e);
        this.modCount++;
        return e2;
    }

    public final int size() {
        return this.f10453.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfgd m12645(int i) {
        if (i < size()) {
            throw new IllegalArgumentException();
        }
        ArrayList arrayList = new ArrayList(i);
        arrayList.addAll(this.f10453);
        return new zzfho(arrayList);
    }
}
