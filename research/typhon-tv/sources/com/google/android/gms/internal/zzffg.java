package com.google.android.gms.internal;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class zzffg extends zzfer {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Logger f4600 = Logger.getLogger(zzffg.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final boolean f4601 = zzfiq.m12749();

    /* renamed from: 龘  reason: contains not printable characters */
    zzffi f4602;

    static class zzb extends zzffg {

        /* renamed from: 连任  reason: contains not printable characters */
        private int f10373;

        /* renamed from: 靐  reason: contains not printable characters */
        private final byte[] f10374;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f10375;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f10376;

        zzb(byte[] bArr, int i, int i2) {
            super();
            if (bArr == null) {
                throw new NullPointerException("buffer");
            } else if ((i | i2 | (bArr.length - (i + i2))) < 0) {
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)}));
            } else {
                this.f10374 = bArr;
                this.f10376 = i;
                this.f10373 = i;
                this.f10375 = i + i2;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12469(int i) throws IOException {
            if (!zzffg.f4601 || m5283() < 10) {
                while ((i & -128) != 0) {
                    try {
                        byte[] bArr = this.f10374;
                        int i2 = this.f10373;
                        this.f10373 = i2 + 1;
                        bArr[i2] = (byte) ((i & 127) | 128);
                        i >>>= 7;
                    } catch (IndexOutOfBoundsException e) {
                        throw new zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f10373), Integer.valueOf(this.f10375), 1}), e);
                    }
                }
                byte[] bArr2 = this.f10374;
                int i3 = this.f10373;
                this.f10373 = i3 + 1;
                bArr2[i3] = (byte) i;
                return;
            }
            while ((i & -128) != 0) {
                byte[] bArr3 = this.f10374;
                int i4 = this.f10373;
                this.f10373 = i4 + 1;
                zzfiq.m12748(bArr3, (long) i4, (byte) ((i & 127) | 128));
                i >>>= 7;
            }
            byte[] bArr4 = this.f10374;
            int i5 = this.f10373;
            this.f10373 = i5 + 1;
            zzfiq.m12748(bArr4, (long) i5, (byte) i);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12470(int i, int i2) throws IOException {
            m5286(i, 0);
            m5285(i2);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12471(int i, long j) throws IOException {
            m5286(i, 1);
            m5278(j);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12472(int i, zzfes zzfes) throws IOException {
            m5286(1, 3);
            m5281(2, i);
            m5288(3, zzfes);
            m5286(1, 4);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12473(int i, zzfhe zzfhe) throws IOException {
            m5286(1, 3);
            m5281(2, i);
            m5289(3, zzfhe);
            m5286(1, 4);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12474(long j) throws IOException {
            try {
                byte[] bArr = this.f10374;
                int i = this.f10373;
                this.f10373 = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.f10374;
                int i2 = this.f10373;
                this.f10373 = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.f10374;
                int i3 = this.f10373;
                this.f10373 = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.f10374;
                int i4 = this.f10373;
                this.f10373 = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.f10374;
                int i5 = this.f10373;
                this.f10373 = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.f10374;
                int i6 = this.f10373;
                this.f10373 = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.f10374;
                int i7 = this.f10373;
                this.f10373 = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.f10374;
                int i8 = this.f10373;
                this.f10373 = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f10373), Integer.valueOf(this.f10375), 1}), e);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final void m12475(int i, int i2) throws IOException {
            m5286(i, 5);
            m5280(i2);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m12476(int i) throws IOException {
            try {
                byte[] bArr = this.f10374;
                int i2 = this.f10373;
                this.f10373 = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.f10374;
                int i3 = this.f10373;
                this.f10373 = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.f10374;
                int i4 = this.f10373;
                this.f10373 = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.f10374;
                int i5 = this.f10373;
                this.f10373 = i5 + 1;
                bArr4[i5] = i >> 24;
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f10373), Integer.valueOf(this.f10375), 1}), e);
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m12477(int i, int i2) throws IOException {
            m5286(i, 0);
            m5273(i2);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m12478(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.f10374, this.f10373, i2);
                this.f10373 += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f10373), Integer.valueOf(this.f10375), Integer.valueOf(i2)}), e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final int m12479() {
            return this.f10375 - this.f10373;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12480(byte b) throws IOException {
            try {
                byte[] bArr = this.f10374;
                int i = this.f10373;
                this.f10373 = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f10373), Integer.valueOf(this.f10375), 1}), e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12481(int i) throws IOException {
            if (i >= 0) {
                m5273(i);
            } else {
                m5292((long) i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12482(int i, int i2) throws IOException {
            m5273((i << 3) | i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12483(int i, long j) throws IOException {
            m5286(i, 0);
            m5292(j);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12484(int i, zzfes zzfes) throws IOException {
            m5286(i, 2);
            m5293(zzfes);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12485(int i, zzfhe zzfhe) throws IOException {
            m5286(i, 2);
            m5294(zzfhe);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12486(int i, String str) throws IOException {
            m5286(i, 2);
            m5295(str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12487(int i, boolean z) throws IOException {
            int i2 = 0;
            m5286(i, 0);
            if (z) {
                i2 = 1;
            }
            m5284((byte) i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12488(long j) throws IOException {
            if (!zzffg.f4601 || m5283() < 10) {
                while ((j & -128) != 0) {
                    try {
                        byte[] bArr = this.f10374;
                        int i = this.f10373;
                        this.f10373 = i + 1;
                        bArr[i] = (byte) ((((int) j) & 127) | 128);
                        j >>>= 7;
                    } catch (IndexOutOfBoundsException e) {
                        throw new zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.f10373), Integer.valueOf(this.f10375), 1}), e);
                    }
                }
                byte[] bArr2 = this.f10374;
                int i2 = this.f10373;
                this.f10373 = i2 + 1;
                bArr2[i2] = (byte) ((int) j);
                return;
            }
            while ((j & -128) != 0) {
                byte[] bArr3 = this.f10374;
                int i3 = this.f10373;
                this.f10373 = i3 + 1;
                zzfiq.m12748(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                j >>>= 7;
            }
            byte[] bArr4 = this.f10374;
            int i4 = this.f10373;
            this.f10373 = i4 + 1;
            zzfiq.m12748(bArr4, (long) i4, (byte) ((int) j));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12489(zzfes zzfes) throws IOException {
            m5273(zzfes.size());
            zzfes.m12379((zzfer) this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12490(zzfhe zzfhe) throws IOException {
            m5273(zzfhe.m12625());
            zzfhe.m12626(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12491(String str) throws IOException {
            int i = this.f10373;
            try {
                int r0 = m5232(str.length() * 3);
                int r2 = m5232(str.length());
                if (r2 == r0) {
                    this.f10373 = i + r2;
                    int r02 = zzfis.m12768(str, this.f10374, this.f10373, m5283());
                    this.f10373 = i;
                    m5273((r02 - i) - r2);
                    this.f10373 = r02;
                    return;
                }
                m5273(zzfis.m12767((CharSequence) str));
                this.f10373 = zzfis.m12768(str, this.f10374, this.f10373, m5283());
            } catch (zzfiv e) {
                this.f10373 = i;
                m5296(str, e);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(e2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m12492(byte[] bArr, int i, int i2) throws IOException {
            m5282(bArr, i, i2);
        }
    }

    public static class zzc extends IOException {
        zzc() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        zzc(java.lang.String r4, java.lang.Throwable r5) {
            /*
                r3 = this;
                java.lang.String r0 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                java.lang.String r1 = java.lang.String.valueOf(r0)
                java.lang.String r0 = java.lang.String.valueOf(r4)
                int r2 = r0.length()
                if (r2 == 0) goto L_0x0019
                java.lang.String r0 = r1.concat(r0)
            L_0x0015:
                r3.<init>(r0, r5)
                return
            L_0x0019:
                java.lang.String r0 = new java.lang.String
                r0.<init>(r1)
                goto L_0x0015
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffg.zzc.<init>(java.lang.String, java.lang.Throwable):void");
        }

        zzc(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }
    }

    private zzffg() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5232(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5233(int i, int i2) {
        return m5255(i) + 4;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m5234(long j) {
        return 8;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m5235(int i) {
        return m5232(m5240(i));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m5236(int i, int i2) {
        return m5255(i) + m5244(i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m5237(long j) {
        return 8;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m5238(int i) {
        return 4;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static long m5239(long j) {
        return (j << 1) ^ (j >> 63);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private static int m5240(int i) {
        return (i << 1) ^ (i >> 31);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static int m5241(int i) {
        return 4;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static int m5242(int i) {
        return m5244(i);
    }

    @Deprecated
    /* renamed from: ᐧ  reason: contains not printable characters */
    public static int m5243(int i) {
        return m5232(i);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static int m5244(int i) {
        if (i >= 0) {
            return m5232(i);
        }
        return 10;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static int m5245(int i, int i2) {
        return m5255(i) + m5232(i2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static int m5246(long j) {
        return m5259(m5239(j));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5247(int i, zzfgk zzfgk) {
        return (m5255(1) << 1) + m5245(2, i) + m5268(3, zzfgk);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5248(int i, String str) {
        return m5255(i) + m5252(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5249(int i, boolean z) {
        return m5255(i) + 1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5250(zzfes zzfes) {
        int size = zzfes.size();
        return size + m5232(size);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5251(zzfhe zzfhe) {
        int r0 = zzfhe.m12625();
        return r0 + m5232(r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5252(String str) {
        int length;
        try {
            length = zzfis.m12767((CharSequence) str);
        } catch (zzfiv e) {
            length = str.getBytes(zzffz.f10423).length;
        }
        return length + m5232(length);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5253(byte[] bArr) {
        int length = bArr.length;
        return length + m5232(length);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzffg m5254(byte[] bArr, int i, int i2) {
        return new zzb(bArr, i, i2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m5255(int i) {
        return m5232(i << 3);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m5256(int i, long j) {
        return m5255(i) + 8;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m5257(int i, zzfes zzfes) {
        return (m5255(1) << 1) + m5245(2, i) + m5261(3, zzfes);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m5258(int i, zzfhe zzfhe) {
        return (m5255(1) << 1) + m5245(2, i) + m5262(3, zzfhe);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m5259(long j) {
        long j2;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        int i = 2;
        if ((-34359738368L & j) != 0) {
            i = 6;
            j2 = j >>> 28;
        } else {
            j2 = j;
        }
        if ((-2097152 & j2) != 0) {
            i += 2;
            j2 >>>= 14;
        }
        return (j2 & -16384) != 0 ? i + 1 : i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static int m5260(int i, long j) {
        return m5255(i) + m5259(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static int m5261(int i, zzfes zzfes) {
        int r0 = m5255(i);
        int size = zzfes.size();
        return r0 + size + m5232(size);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static int m5262(int i, zzfhe zzfhe) {
        return m5255(i) + m5251(zzfhe);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static int m5263(long j) {
        return m5259(j);
    }

    @Deprecated
    /* renamed from: 齉  reason: contains not printable characters */
    public static int m5264(zzfhe zzfhe) {
        return zzfhe.m12625();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5266(double d) {
        return 8;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5267(float f) {
        return 4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5268(int i, zzfgk zzfgk) {
        int r0 = m5255(i);
        int r1 = zzfgk.m12597();
        return r0 + r1 + m5232(r1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5269(zzfgk zzfgk) {
        int r0 = zzfgk.m12597();
        return r0 + m5232(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5270(boolean z) {
        return 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzffg m5271(byte[] bArr) {
        return m5254(bArr, 0, bArr.length);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5272() {
        if (m5283() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m5273(int i) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m5274(int i, int i2) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m5275(int i, long j) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m5276(int i, zzfes zzfes) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m5277(int i, zzfhe zzfhe) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m5278(long j) throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m5279(int i, int i2) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m5280(int i) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m5281(int i, int i2) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m5282(byte[] bArr, int i, int i2) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m5283();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5284(byte b) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5285(int i) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5286(int i, int i2) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5287(int i, long j) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5288(int i, zzfes zzfes) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5289(int i, zzfhe zzfhe) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5290(int i, String str) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5291(int i, boolean z) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5292(long j) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5293(zzfes zzfes) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5294(zzfhe zzfhe) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5295(String str) throws IOException;

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5296(String str, zzfiv zzfiv) throws IOException {
        f4600.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", zzfiv);
        byte[] bytes = str.getBytes(zzffz.f10423);
        try {
            m5273(bytes.length);
            m12369(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new zzc(e);
        } catch (zzc e2) {
            throw e2;
        }
    }
}
