package com.google.android.gms.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

public final class zzcxh implements Parcelable.Creator<zzcxg> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        Intent intent = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    intent = (Intent) zzbfn.m10171(parcel, readInt, Intent.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new zzcxg(i2, i, intent);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcxg[i];
    }
}
