package com.google.android.gms.internal;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

public final class zzd extends Thread {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final boolean f9847 = zzae.f8117;

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile boolean f9848 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzf f9849;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzz f9850;

    /* renamed from: 靐  reason: contains not printable characters */
    private final BlockingQueue<zzr<?>> f9851;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzb f9852;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final BlockingQueue<zzr<?>> f9853;

    public zzd(BlockingQueue<zzr<?>> blockingQueue, BlockingQueue<zzr<?>> blockingQueue2, zzb zzb, zzz zzz) {
        this.f9851 = blockingQueue;
        this.f9853 = blockingQueue2;
        this.f9852 = zzb;
        this.f9850 = zzz;
        this.f9849 = new zzf(this);
    }

    public final void run() {
        if (f9847) {
            zzae.m9516("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.f9852.m9866();
        while (true) {
            try {
                zzr take = this.f9851.take();
                take.m13361("cache-queue-take");
                zzc r4 = this.f9852.m9865(take.m13359());
                if (r4 == null) {
                    take.m13361("cache-miss");
                    if (!this.f9849.m12356(take)) {
                        this.f9853.put(take);
                    }
                } else if (r4.m10309()) {
                    take.m13361("cache-hit-expired");
                    take.m13366(r4);
                    if (!this.f9849.m12356(take)) {
                        this.f9853.put(take);
                    }
                } else {
                    take.m13361("cache-hit");
                    zzw r5 = take.m13368(new zzp(r4.f8823, r4.f8817));
                    take.m13361("cache-hit-parsed");
                    if (!(r4.f8816 < System.currentTimeMillis())) {
                        this.f9850.m13647((zzr<?>) take, (zzw<?>) r5);
                    } else {
                        take.m13361("cache-hit-refresh-needed");
                        take.m13366(r4);
                        r5.f10932 = true;
                        if (!this.f9849.m12356(take)) {
                            this.f9850.m13648(take, r5, new zze(this, take));
                        } else {
                            this.f9850.m13647((zzr<?>) take, (zzw<?>) r5);
                        }
                    }
                }
            } catch (InterruptedException e) {
                if (this.f9848) {
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11583() {
        this.f9848 = true;
        interrupt();
    }
}
