package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagp extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8170;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8171;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagp(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8171 = context;
        this.f8170 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9588() {
        SharedPreferences sharedPreferences = this.f8171.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putString("app_settings_json", sharedPreferences.getString("app_settings_json", ""));
        bundle.putLong("app_settings_last_update_ms", sharedPreferences.getLong("app_settings_last_update_ms", 0));
        if (this.f8170 != null) {
            this.f8170.m9605(bundle);
        }
    }
}
