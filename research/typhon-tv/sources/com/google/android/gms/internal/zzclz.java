package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclz extends zzfjm<zzclz> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile zzclz[] f9702;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9703 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f9704 = null;

    public zzclz() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzclz[] m11480() {
        if (f9702 == null) {
            synchronized (zzfjq.f10546) {
                if (f9702 == null) {
                    f9702 = new zzclz[0];
                }
            }
        }
        return f9702;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclz)) {
            return false;
        }
        zzclz zzclz = (zzclz) obj;
        if (this.f9704 == null) {
            if (zzclz.f9704 != null) {
                return false;
            }
        } else if (!this.f9704.equals(zzclz.f9704)) {
            return false;
        }
        if (this.f9703 == null) {
            if (zzclz.f9703 != null) {
                return false;
            }
        } else if (!this.f9703.equals(zzclz.f9703)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclz.f10533 == null || zzclz.f10533.m12849() : this.f10533.equals(zzclz.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9703 == null ? 0 : this.f9703.hashCode()) + (((this.f9704 == null ? 0 : this.f9704.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11481() {
        int r0 = super.m12841();
        if (this.f9704 != null) {
            r0 += zzfjk.m12808(1, this.f9704);
        }
        return this.f9703 != null ? r0 + zzfjk.m12808(2, this.f9703) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11482(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f9704 = zzfjj.m12791();
                    continue;
                case 18:
                    this.f9703 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11483(zzfjk zzfjk) throws IOException {
        if (this.f9704 != null) {
            zzfjk.m12835(1, this.f9704);
        }
        if (this.f9703 != null) {
            zzfjk.m12835(2, this.f9703);
        }
        super.m12842(zzfjk);
    }
}
