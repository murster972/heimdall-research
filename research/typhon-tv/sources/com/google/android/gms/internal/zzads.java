package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardItem;

@zzzv
public final class zzads implements RewardItem {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzadh f4026;

    public zzads(zzadh zzadh) {
        this.f4026 = zzadh;
    }

    public final int getAmount() {
        if (this.f4026 == null) {
            return 0;
        }
        try {
            return this.f4026.m9465();
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward getAmount to RewardItem", e);
            return 0;
        }
    }

    public final String getType() {
        if (this.f4026 == null) {
            return null;
        }
        try {
            return this.f4026.m9466();
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward getType to RewardItem", e);
            return null;
        }
    }
}
