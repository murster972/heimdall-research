package com.google.android.gms.internal;

import android.content.Context;
import java.util.concurrent.Future;

@zzzv
public final class zzagh {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Future m4529(Context context, zzahg zzahg) {
        return (zzakv) new zzagn(context, zzahg).m4523();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Future m4530(Context context, zzahg zzahg) {
        return (zzakv) new zzagp(context, zzahg).m4523();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static Future m4531(Context context, zzahg zzahg) {
        return (zzakv) new zzags(context, zzahg).m4523();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static Future m4532(Context context, zzahg zzahg) {
        return (zzakv) new zzagv(context, zzahg).m4523();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static Future m4533(Context context, zzahg zzahg) {
        return (zzakv) new zzagx(context, zzahg).m4523();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static Future m4534(Context context, zzahg zzahg) {
        return (zzakv) new zzagz(context, zzahg).m4523();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static Future m4535(Context context, zzahg zzahg) {
        return (zzakv) new zzagl(context, zzahg).m4523();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Future m4536(Context context, int i) {
        return (zzakv) new zzagw(context, i).m4523();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Future m4537(Context context, long j) {
        return (zzakv) new zzagy(context, j).m4523();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Future m4538(Context context, zzahg zzahg) {
        return (zzakv) new zzaha(context, zzahg).m4523();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Future m4539(Context context, String str) {
        return (zzakv) new zzagj(context, str).m4523();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Future m4540(Context context, boolean z) {
        return (zzakv) new zzahb(context, z).m4523();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static Future m4541(Context context, zzahg zzahg) {
        return (zzakv) new zzahe(context, zzahg).m4523();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Future m4542(Context context, zzahg zzahg) {
        return (zzakv) new zzahc(context, zzahg).m4523();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Future m4543(Context context, String str) {
        return (zzakv) new zzagq(context, str).m4523();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Future m4544(Context context, boolean z) {
        return (zzakv) new zzagk(context, z).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzakv m4545(Context context, String str, long j) {
        return (zzakv) new zzago(context, str, j).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Future m4546(Context context) {
        return (zzakv) new zzagt(context).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Future m4547(Context context, int i) {
        return (zzakv) new zzagm(context, i).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Future m4548(Context context, long j) {
        return (zzakv) new zzagu(context, j).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Future m4549(Context context, zzahg zzahg) {
        return (zzakv) new zzagr(context, zzahg).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Future m4550(Context context, String str) {
        return (zzakv) new zzahd(context, str).m4523();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Future m4551(Context context, boolean z) {
        return (zzakv) new zzagi(context, z).m4523();
    }
}
