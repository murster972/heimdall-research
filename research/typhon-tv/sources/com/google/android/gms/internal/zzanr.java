package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzv;

@zzzv
public final class zzanr {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzakv<zzanh> m5059(Context context, zzakd zzakd, String str, zzcv zzcv, zzv zzv) {
        return zzakl.m4804(zzakl.m4802(null), new zzans(context, zzcv, zzakd, zzv, str), zzala.f4307);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzanh m5060(Context context, zzapa zzapa, String str, boolean z, boolean z2, zzcv zzcv, zzakd zzakd, zznu zznu, zzbl zzbl, zzv zzv, zzis zzis) throws zzanv {
        try {
            return (zzanh) zzajk.m4727(context, new zzanu(this, context, zzapa, str, z, z2, zzcv, zzakd, zznu, zzbl, zzv, zzis));
        } catch (Throwable th) {
            throw new zzanv(this, "Webview initialization failed.", th);
        }
    }
}
