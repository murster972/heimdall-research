package com.google.android.gms.internal;

import android.util.DisplayMetrics;
import android.view.View;
import java.lang.reflect.InvocationTargetException;

public final class zzeq extends zzet {

    /* renamed from: 麤  reason: contains not printable characters */
    private final View f10270;

    public zzeq(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2, View view) {
        super(zzdm, str, str2, zzaz, i, 57);
        this.f10270 = view;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12291() throws IllegalAccessException, InvocationTargetException {
        if (this.f10270 != null) {
            DisplayMetrics displayMetrics = this.f10287.m11623().getResources().getDisplayMetrics();
            zzds zzds = new zzds((String) this.f10286.invoke((Object) null, new Object[]{this.f10270, displayMetrics}));
            zzbb zzbb = new zzbb();
            zzbb.f8608 = zzds.f10022;
            zzbb.f8605 = zzds.f10020;
            zzbb.f8607 = zzds.f10021;
            this.f10284.f8452 = zzbb;
        }
    }
}
