package com.google.android.gms.internal;

import org.json.JSONObject;

@zzzv
public final class zzfr {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f4603;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f4604;

    /* renamed from: 靐  reason: contains not printable characters */
    private final JSONObject f4605;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f4606;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f4607;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f4608;

    public zzfr(String str, zzakd zzakd, String str2, JSONObject jSONObject, boolean z, boolean z2) {
        this.f4606 = zzakd.f4297;
        this.f4605 = jSONObject;
        this.f4607 = str;
        this.f4608 = str2;
        this.f4604 = z;
        this.f4603 = z2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m5297() {
        return this.f4603;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m5298() {
        return this.f4604;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5299() {
        return this.f4606;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m5300() {
        return this.f4607;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final JSONObject m5301() {
        return this.f4605;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5302() {
        return this.f4608;
    }
}
