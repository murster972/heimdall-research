package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtv;
import java.security.GeneralSecurityException;

public final class zzdqs {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzdtv f9946 = ((zzdtv) ((zzdtv.zza) zzdtv.m12155().m12545(zzdqg.f9941)).m12165(zzdpr.m11651("TinkHybrid", "HybridDecrypt", "EciesAeadHkdfPrivateKey", 0, true)).m12165(zzdpr.m11651("TinkHybrid", "HybridEncrypt", "EciesAeadHkdfPublicKey", 0, true)).m12166("TINK_HYBRID_1_0_0").m12542());

    static {
        try {
            zzdqe.m11680("TinkHybrid", (zzdpq) new zzdqr());
            zzdqg.m11684();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
}
