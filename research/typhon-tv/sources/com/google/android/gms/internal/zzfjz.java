package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfjz extends zzfjm<zzfjz> {

    /* renamed from: 连任  reason: contains not printable characters */
    private Integer f10584 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzfka f10585 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private byte[] f10586 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f10587 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public zzfjy[] f10588 = zzfjy.m12891();

    public zzfjz() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12895() {
        int r0 = super.m12841();
        if (this.f10585 != null) {
            r0 += zzfjk.m12807(1, (zzfjs) this.f10585);
        }
        if (this.f10588 != null && this.f10588.length > 0) {
            int i = r0;
            for (zzfjy zzfjy : this.f10588) {
                if (zzfjy != null) {
                    i += zzfjk.m12807(2, (zzfjs) zzfjy);
                }
            }
            r0 = i;
        }
        if (this.f10587 != null) {
            r0 += zzfjk.m12809(3, this.f10587);
        }
        if (this.f10586 != null) {
            r0 += zzfjk.m12809(4, this.f10586);
        }
        return this.f10584 != null ? r0 + zzfjk.m12806(5, this.f10584.intValue()) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12896(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    if (this.f10585 == null) {
                        this.f10585 = new zzfka();
                    }
                    zzfjj.m12802((zzfjs) this.f10585);
                    continue;
                case 18:
                    int r2 = zzfjv.m12883(zzfjj, 18);
                    int length = this.f10588 == null ? 0 : this.f10588.length;
                    zzfjy[] zzfjyArr = new zzfjy[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10588, 0, zzfjyArr, 0, length);
                    }
                    while (length < zzfjyArr.length - 1) {
                        zzfjyArr[length] = new zzfjy();
                        zzfjj.m12802((zzfjs) zzfjyArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzfjyArr[length] = new zzfjy();
                    zzfjj.m12802((zzfjs) zzfjyArr[length]);
                    this.f10588 = zzfjyArr;
                    continue;
                case 26:
                    this.f10587 = zzfjj.m12784();
                    continue;
                case 34:
                    this.f10586 = zzfjj.m12784();
                    continue;
                case 40:
                    this.f10584 = Integer.valueOf(zzfjj.m12798());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12897(zzfjk zzfjk) throws IOException {
        if (this.f10585 != null) {
            zzfjk.m12834(1, (zzfjs) this.f10585);
        }
        if (this.f10588 != null && this.f10588.length > 0) {
            for (zzfjy zzfjy : this.f10588) {
                if (zzfjy != null) {
                    zzfjk.m12834(2, (zzfjs) zzfjy);
                }
            }
        }
        if (this.f10587 != null) {
            zzfjk.m12837(3, this.f10587);
        }
        if (this.f10586 != null) {
            zzfjk.m12837(4, this.f10586);
        }
        if (this.f10584 != null) {
            zzfjk.m12832(5, this.f10584.intValue());
        }
        super.m12842(zzfjk);
    }
}
