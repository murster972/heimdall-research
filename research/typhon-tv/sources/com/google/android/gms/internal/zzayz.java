package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzayz extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    Bundle m9781(String str) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m9782() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m9783(Bundle bundle, int i) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    String m9784() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9785() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9786(Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9787(Bundle bundle, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9788(Bundle bundle, zzazb zzazb) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9789(String str) throws RemoteException;
}
