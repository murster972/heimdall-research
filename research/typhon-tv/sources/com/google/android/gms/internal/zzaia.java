package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.View;

@TargetApi(18)
public class zzaia extends zzahz {
    /* renamed from: 齉  reason: contains not printable characters */
    public final int m9625() {
        return 14;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m9626(View view) {
        return super.m4663(view) || view.getWindowId() != null;
    }
}
