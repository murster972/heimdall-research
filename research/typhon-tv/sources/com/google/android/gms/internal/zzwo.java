package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.CalendarContract;
import android.text.TextUtils;
import com.google.android.gms.R;
import com.google.android.gms.ads.internal.zzbs;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;

@zzzv
public final class zzwo extends zzxb {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f5506 = m5991("summary");

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f5507 = m5991("location");

    /* renamed from: 连任  reason: contains not printable characters */
    private long f5508 = m5990("end_ticks");
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context f5509;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f5510 = m5990("start_ticks");

    /* renamed from: 齉  reason: contains not printable characters */
    private String f5511 = m5991(PubnativeAsset.DESCRIPTION);

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, String> f5512;

    public zzwo(zzanh zzanh, Map<String, String> map) {
        super(zzanh, "createCalendarEvent");
        this.f5512 = map;
        this.f5509 = zzanh.m5003();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final long m5990(String str) {
        String str2 = this.f5512.get(str);
        if (str2 == null) {
            return -1;
        }
        try {
            return Long.parseLong(str2);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final String m5991(String str) {
        return TextUtils.isEmpty(this.f5512.get(str)) ? "" : this.f5512.get(str);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(14)
    /* renamed from: 靐  reason: contains not printable characters */
    public final Intent m5993() {
        Intent data = new Intent("android.intent.action.EDIT").setData(CalendarContract.Events.CONTENT_URI);
        data.putExtra(PubnativeAsset.TITLE, this.f5511);
        data.putExtra("eventLocation", this.f5507);
        data.putExtra(PubnativeAsset.DESCRIPTION, this.f5506);
        if (this.f5510 > -1) {
            data.putExtra("beginTime", this.f5510);
        }
        if (this.f5508 > -1) {
            data.putExtra("endTime", this.f5508);
        }
        data.setFlags(268435456);
        return data;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5994() {
        if (this.f5509 == null) {
            m6012("Activity context is not available.");
            return;
        }
        zzbs.zzei();
        if (!zzahn.m4559(this.f5509).m5571()) {
            m6012("This feature is not available on the device.");
            return;
        }
        zzbs.zzei();
        AlertDialog.Builder r1 = zzahn.m4573(this.f5509);
        Resources r2 = zzbs.zzem().m4476();
        r1.setTitle(r2 != null ? r2.getString(R.string.s5) : "Create calendar event");
        r1.setMessage(r2 != null ? r2.getString(R.string.s6) : "Allow Ad to create a calendar event?");
        r1.setPositiveButton(r2 != null ? r2.getString(R.string.s3) : AbstractSpiCall.HEADER_ACCEPT, new zzwp(this));
        r1.setNegativeButton(r2 != null ? r2.getString(R.string.s4) : "Decline", new zzwq(this));
        AlertDialog create = r1.create();
    }
}
