package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclu extends zzfjm<zzclu> {

    /* renamed from: 连任  reason: contains not printable characters */
    public String f9678 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Boolean f9679 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f9680 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f9681 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9682 = null;

    public zzclu() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzclu m11463(com.google.android.gms.internal.zzfjj r7) throws java.io.IOException {
        /*
            r6 = this;
        L_0x0000:
            int r0 = r7.m12800()
            switch(r0) {
                case 0: goto L_0x000d;
                case 8: goto L_0x000e;
                case 16: goto L_0x0044;
                case 26: goto L_0x004f;
                case 34: goto L_0x0056;
                case 42: goto L_0x005d;
                default: goto L_0x0007;
            }
        L_0x0007:
            boolean r0 = super.m12843(r7, r0)
            if (r0 != 0) goto L_0x0000
        L_0x000d:
            return r6
        L_0x000e:
            int r1 = r7.m12787()
            int r2 = r7.m12785()     // Catch:{ IllegalArgumentException -> 0x0035 }
            switch(r2) {
                case 0: goto L_0x003d;
                case 1: goto L_0x003d;
                case 2: goto L_0x003d;
                case 3: goto L_0x003d;
                case 4: goto L_0x003d;
                default: goto L_0x0019;
            }     // Catch:{ IllegalArgumentException -> 0x0035 }
        L_0x0019:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x0035 }
            r4 = 46
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0035 }
            r5.<init>(r4)     // Catch:{ IllegalArgumentException -> 0x0035 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ IllegalArgumentException -> 0x0035 }
            java.lang.String r4 = " is not a valid enum ComparisonType"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IllegalArgumentException -> 0x0035 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x0035 }
            r3.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x0035 }
            throw r3     // Catch:{ IllegalArgumentException -> 0x0035 }
        L_0x0035:
            r2 = move-exception
            r7.m12792(r1)
            r6.m12843(r7, r0)
            goto L_0x0000
        L_0x003d:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ IllegalArgumentException -> 0x0035 }
            r6.f9682 = r2     // Catch:{ IllegalArgumentException -> 0x0035 }
            goto L_0x0000
        L_0x0044:
            boolean r0 = r7.m12797()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6.f9679 = r0
            goto L_0x0000
        L_0x004f:
            java.lang.String r0 = r7.m12791()
            r6.f9681 = r0
            goto L_0x0000
        L_0x0056:
            java.lang.String r0 = r7.m12791()
            r6.f9680 = r0
            goto L_0x0000
        L_0x005d:
            java.lang.String r0 = r7.m12791()
            r6.f9678 = r0
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzclu.m11463(com.google.android.gms.internal.zzfjj):com.google.android.gms.internal.zzclu");
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclu)) {
            return false;
        }
        zzclu zzclu = (zzclu) obj;
        if (this.f9682 == null) {
            if (zzclu.f9682 != null) {
                return false;
            }
        } else if (!this.f9682.equals(zzclu.f9682)) {
            return false;
        }
        if (this.f9679 == null) {
            if (zzclu.f9679 != null) {
                return false;
            }
        } else if (!this.f9679.equals(zzclu.f9679)) {
            return false;
        }
        if (this.f9681 == null) {
            if (zzclu.f9681 != null) {
                return false;
            }
        } else if (!this.f9681.equals(zzclu.f9681)) {
            return false;
        }
        if (this.f9680 == null) {
            if (zzclu.f9680 != null) {
                return false;
            }
        } else if (!this.f9680.equals(zzclu.f9680)) {
            return false;
        }
        if (this.f9678 == null) {
            if (zzclu.f9678 != null) {
                return false;
            }
        } else if (!this.f9678.equals(zzclu.f9678)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclu.f10533 == null || zzclu.f10533.m12849() : this.f10533.equals(zzclu.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9678 == null ? 0 : this.f9678.hashCode()) + (((this.f9680 == null ? 0 : this.f9680.hashCode()) + (((this.f9681 == null ? 0 : this.f9681.hashCode()) + (((this.f9679 == null ? 0 : this.f9679.hashCode()) + (((this.f9682 == null ? 0 : this.f9682.intValue()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11462() {
        int r0 = super.m12841();
        if (this.f9682 != null) {
            r0 += zzfjk.m12806(1, this.f9682.intValue());
        }
        if (this.f9679 != null) {
            this.f9679.booleanValue();
            r0 += zzfjk.m12805(2) + 1;
        }
        if (this.f9681 != null) {
            r0 += zzfjk.m12808(3, this.f9681);
        }
        if (this.f9680 != null) {
            r0 += zzfjk.m12808(4, this.f9680);
        }
        return this.f9678 != null ? r0 + zzfjk.m12808(5, this.f9678) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11464(zzfjk zzfjk) throws IOException {
        if (this.f9682 != null) {
            zzfjk.m12832(1, this.f9682.intValue());
        }
        if (this.f9679 != null) {
            zzfjk.m12836(2, this.f9679.booleanValue());
        }
        if (this.f9681 != null) {
            zzfjk.m12835(3, this.f9681);
        }
        if (this.f9680 != null) {
            zzfjk.m12835(4, this.f9680);
        }
        if (this.f9678 != null) {
            zzfjk.m12835(5, this.f9678);
        }
        super.m12842(zzfjk);
    }
}
