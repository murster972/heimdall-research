package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbd extends zzfjm<zzbd> {

    /* renamed from: 靐  reason: contains not printable characters */
    public byte[] f8709 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public byte[] f8710 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public byte[] f8711 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public byte[] f8712 = null;

    public zzbd() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10115() {
        int r0 = super.m12841();
        if (this.f8712 != null) {
            r0 += zzfjk.m12809(1, this.f8712);
        }
        if (this.f8709 != null) {
            r0 += zzfjk.m12809(2, this.f8709);
        }
        if (this.f8711 != null) {
            r0 += zzfjk.m12809(3, this.f8711);
        }
        return this.f8710 != null ? r0 + zzfjk.m12809(4, this.f8710) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m10116(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f8712 = zzfjj.m12784();
                    continue;
                case 18:
                    this.f8709 = zzfjj.m12784();
                    continue;
                case 26:
                    this.f8711 = zzfjj.m12784();
                    continue;
                case 34:
                    this.f8710 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10117(zzfjk zzfjk) throws IOException {
        if (this.f8712 != null) {
            zzfjk.m12837(1, this.f8712);
        }
        if (this.f8709 != null) {
            zzfjk.m12837(2, this.f8709);
        }
        if (this.f8711 != null) {
            zzfjk.m12837(3, this.f8711);
        }
        if (this.f8710 != null) {
            zzfjk.m12837(4, this.f8710);
        }
        super.m12842(zzfjk);
    }
}
