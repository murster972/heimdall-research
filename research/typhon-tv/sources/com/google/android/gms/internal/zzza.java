package com.google.android.gms.internal;

import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;

final class zzza implements ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzyt f11006;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ WeakReference f11007;

    zzza(zzyt zzyt, WeakReference weakReference) {
        this.f11006 = zzyt;
        this.f11007 = weakReference;
    }

    public final void onScrollChanged() {
        this.f11006.m6069((WeakReference<zzanh>) this.f11007, true);
    }
}
