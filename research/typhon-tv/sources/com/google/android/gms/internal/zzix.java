package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

@zzzv
public final class zzix {

    /* renamed from: 靐  reason: contains not printable characters */
    boolean f4749;

    /* renamed from: 龘  reason: contains not printable characters */
    zzfl f4750;

    public zzix() {
    }

    public zzix(Context context) {
        zznh.m5599(context);
        if (((Boolean) zzkb.m5481().m5595(zznh.f5040)).booleanValue()) {
            try {
                this.f4750 = zzfm.m12934(DynamiteModule.m9319(context, DynamiteModule.f7976, ModuleDescriptor.MODULE_ID).m9324("com.google.android.gms.ads.clearcut.DynamiteClearcutLogger"));
                zzn.m9306(context);
                this.f4750.m12930(zzn.m9306(context), "GMA_SDK");
                this.f4749 = true;
            } catch (RemoteException | DynamiteModule.zzc | NullPointerException e) {
                zzakb.m4792("Cannot dynamite load clearcut");
            }
        }
    }

    public zzix(Context context, String str, String str2) {
        zznh.m5599(context);
        try {
            this.f4750 = zzfm.m12934(DynamiteModule.m9319(context, DynamiteModule.f7976, ModuleDescriptor.MODULE_ID).m9324("com.google.android.gms.ads.clearcut.DynamiteClearcutLogger"));
            zzn.m9306(context);
            this.f4750.m12931(zzn.m9306(context), str, (String) null);
            this.f4749 = true;
        } catch (RemoteException | DynamiteModule.zzc | NullPointerException e) {
            zzakb.m4792("Cannot dynamite load clearcut");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zziz m5435(byte[] bArr) {
        return new zziz(this, bArr);
    }
}
