package com.google.android.gms.internal;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

final class zzan extends FilterInputStream {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f8322;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f8323;

    zzan(InputStream inputStream, long j) {
        super(inputStream);
        this.f8323 = j;
    }

    public final int read() throws IOException {
        int read = super.read();
        if (read != -1) {
            this.f8322++;
        }
        return read;
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int read = super.read(bArr, i, i2);
        if (read != -1) {
            this.f8322 += (long) read;
        }
        return read;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final long m9705() {
        return this.f8323 - this.f8322;
    }
}
