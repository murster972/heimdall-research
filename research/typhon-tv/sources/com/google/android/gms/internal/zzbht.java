package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.zzm;

abstract class zzbht<R extends Result> extends zzm<R, zzbhy> {
    public zzbht(GoogleApiClient googleApiClient) {
        super(zzbhg.f8761, googleApiClient);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m10268(Context context, zzbij zzbij) throws RemoteException;

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10269(Api.zzb zzb) throws RemoteException {
        zzbhy zzbhy = (zzbhy) zzb;
        m10268(zzbhy.m9168(), (zzbij) zzbhy.m9171());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10270(Object obj) {
        super.m4198((Result) obj);
    }
}
