package com.google.android.gms.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;
import com.mopub.common.TyphoonApp;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class zzcgo extends zzcjl {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final String[] f9158 = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final String[] f9159 = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String[] f9160 = {TtmlNode.ATTR_TTS_ORIGIN, "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final String[] f9161 = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final String[] f9162 = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String[] f9163 = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;"};

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzcgr f9164 = new zzcgr(this, m11097(), "google_app_measurement.db");
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzclk f9165 = new zzclk(m11105());

    zzcgo(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private final boolean m10560() {
        return m11097().getDatabasePath("google_app_measurement.db").exists();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final long m10562(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = m10587().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final long m10563(String str, String[] strArr, long j) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = m10587().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                j = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } else if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object m10565(Cursor cursor, int i) {
        int type = cursor.getType(i);
        switch (type) {
            case 0:
                m11096().m10832().m10849("Loaded invalid null value from database");
                return null;
            case 1:
                return Long.valueOf(cursor.getLong(i));
            case 2:
                return Double.valueOf(cursor.getDouble(i));
            case 3:
                return cursor.getString(i);
            case 4:
                m11096().m10832().m10849("Loaded invalid blob type value, ignoring it");
                return null;
            default:
                m11096().m10832().m10850("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Set<String> m10566(SQLiteDatabase sQLiteDatabase, String str) {
        HashSet hashSet = new HashSet();
        Cursor rawQuery = sQLiteDatabase.rawQuery(new StringBuilder(String.valueOf(str).length() + 22).append("SELECT * FROM ").append(str).append(" LIMIT 0").toString(), (String[]) null);
        try {
            Collections.addAll(hashSet, rawQuery.getColumnNames());
            return hashSet;
        } finally {
            rawQuery.close();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10567(ContentValues contentValues, String str, Object obj) {
        zzbq.m9122(str);
        zzbq.m9120(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m10568(zzchm zzchm, SQLiteDatabase sQLiteDatabase) {
        if (zzchm == null) {
            throw new IllegalArgumentException("Monitor must not be null");
        }
        File file = new File(sQLiteDatabase.getPath());
        if (!file.setReadable(false, false)) {
            zzchm.m10834().m10849("Failed to turn off database read permission");
        }
        if (!file.setWritable(false, false)) {
            zzchm.m10834().m10849("Failed to turn off database write permission");
        }
        if (!file.setReadable(true, true)) {
            zzchm.m10834().m10849("Failed to turn on database read permission for owner");
        }
        if (!file.setWritable(true, true)) {
            zzchm.m10834().m10849("Failed to turn on database write permission for owner");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m10569(zzchm zzchm, SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String[] strArr) throws SQLiteException {
        if (zzchm == null) {
            throw new IllegalArgumentException("Monitor must not be null");
        }
        if (!m10571(zzchm, sQLiteDatabase, str)) {
            sQLiteDatabase.execSQL(str2);
        }
        try {
            m10570(zzchm, sQLiteDatabase, str, str3, strArr);
        } catch (SQLiteException e) {
            zzchm.m10832().m10850("Failed to verify columns on table that was just created", str);
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10570(zzchm zzchm, SQLiteDatabase sQLiteDatabase, String str, String str2, String[] strArr) throws SQLiteException {
        if (zzchm == null) {
            throw new IllegalArgumentException("Monitor must not be null");
        }
        Set<String> r2 = m10566(sQLiteDatabase, str);
        for (String str3 : str2.split(",")) {
            if (!r2.remove(str3)) {
                throw new SQLiteException(new StringBuilder(String.valueOf(str).length() + 35 + String.valueOf(str3).length()).append("Table ").append(str).append(" is missing required column: ").append(str3).toString());
            }
        }
        if (strArr != null) {
            for (int i = 0; i < strArr.length; i += 2) {
                if (!r2.remove(strArr[i])) {
                    sQLiteDatabase.execSQL(strArr[i + 1]);
                }
            }
        }
        if (!r2.isEmpty()) {
            zzchm.m10834().m10851("Table has extra columns. table, columns", str, TextUtils.join(", ", r2));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004a  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m10571(com.google.android.gms.internal.zzchm r10, android.database.sqlite.SQLiteDatabase r11, java.lang.String r12) {
        /*
            r8 = 0
            r9 = 0
            if (r10 != 0) goto L_0x000d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Monitor must not be null"
            r0.<init>(r1)
            throw r0
        L_0x000d:
            java.lang.String r1 = "SQLITE_MASTER"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0034, all -> 0x0047 }
            r0 = 0
            java.lang.String r3 = "name"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x0034, all -> 0x0047 }
            java.lang.String r3 = "name=?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0034, all -> 0x0047 }
            r0 = 0
            r4[r0] = r12     // Catch:{ SQLiteException -> 0x0034, all -> 0x0047 }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r11
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0034, all -> 0x0047 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0051 }
            if (r1 == 0) goto L_0x0033
            r1.close()
        L_0x0033:
            return r0
        L_0x0034:
            r0 = move-exception
            r1 = r9
        L_0x0036:
            com.google.android.gms.internal.zzcho r2 = r10.m10834()     // Catch:{ all -> 0x004e }
            java.lang.String r3 = "Error querying for table"
            r2.m10851(r3, r12, r0)     // Catch:{ all -> 0x004e }
            if (r1 == 0) goto L_0x0045
            r1.close()
        L_0x0045:
            r0 = r8
            goto L_0x0033
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r9 == 0) goto L_0x004d
            r9.close()
        L_0x004d:
            throw r0
        L_0x004e:
            r0 = move-exception
            r9 = r1
            goto L_0x0048
        L_0x0051:
            r0 = move-exception
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10571(com.google.android.gms.internal.zzchm, android.database.sqlite.SQLiteDatabase, java.lang.String):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m10572(String str, int i, zzcls zzcls) {
        m11115();
        m11109();
        zzbq.m9122(str);
        zzbq.m9120(zzcls);
        if (TextUtils.isEmpty(zzcls.f9669)) {
            m11096().m10834().m10852("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", zzchm.m10812(str), Integer.valueOf(i), String.valueOf(zzcls.f9672));
            return false;
        }
        try {
            byte[] bArr = new byte[zzcls.m12872()];
            zzfjk r2 = zzfjk.m12822(bArr, 0, bArr.length);
            zzcls.m12877(r2);
            r2.m12829();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("filter_id", zzcls.f9672);
            contentValues.put("event_name", zzcls.f9669);
            contentValues.put("data", bArr);
            try {
                if (m10587().insertWithOnConflict("event_filters", (String) null, contentValues, 5) == -1) {
                    m11096().m10832().m10850("Failed to insert event filter (got -1). appId", zzchm.m10812(str));
                }
                return true;
            } catch (SQLiteException e) {
                m11096().m10832().m10851("Error storing event filter. appId", zzchm.m10812(str), e);
                return false;
            }
        } catch (IOException e2) {
            m11096().m10832().m10851("Configuration loss. Failed to serialize event filter. appId", zzchm.m10812(str), e2);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m10573(String str, int i, zzclv zzclv) {
        m11115();
        m11109();
        zzbq.m9122(str);
        zzbq.m9120(zzclv);
        if (TextUtils.isEmpty(zzclv.f9684)) {
            m11096().m10834().m10852("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", zzchm.m10812(str), Integer.valueOf(i), String.valueOf(zzclv.f9686));
            return false;
        }
        try {
            byte[] bArr = new byte[zzclv.m12872()];
            zzfjk r2 = zzfjk.m12822(bArr, 0, bArr.length);
            zzclv.m12877(r2);
            r2.m12829();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("filter_id", zzclv.f9686);
            contentValues.put("property_name", zzclv.f9684);
            contentValues.put("data", bArr);
            try {
                if (m10587().insertWithOnConflict("property_filters", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                m11096().m10832().m10850("Failed to insert property filter (got -1). appId", zzchm.m10812(str));
                return false;
            } catch (SQLiteException e) {
                m11096().m10832().m10851("Error storing property filter. appId", zzchm.m10812(str), e);
                return false;
            }
        } catch (IOException e2) {
            m11096().m10832().m10851("Configuration loss. Failed to serialize property filter. appId", zzchm.m10812(str), e2);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m10574(String str, List<Integer> list) {
        zzbq.m9122(str);
        m11115();
        m11109();
        SQLiteDatabase r4 = m10587();
        try {
            long r0 = m10562("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(2000, m11102().m10544(str, zzchc.f9205)));
            if (r0 <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                Integer num = list.get(i);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            String sb = new StringBuilder(String.valueOf(join).length() + 2).append("(").append(join).append(")").toString();
            return r4.delete("audience_filter_values", new StringBuilder(String.valueOf(sb).length() + 140).append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ").append(sb).append(" order by rowid desc limit -1 offset ?)").toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Database error querying filters. appId", zzchm.m10812(str), e);
            return false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final long m10575(String str) {
        zzbq.m9122(str);
        return m10563("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00bd  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.google.android.gms.internal.zzcls>> m10576(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            r9 = 0
            r10.m11115()
            r10.m11109()
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r11)
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r12)
            android.support.v4.util.ArrayMap r8 = new android.support.v4.util.ArrayMap
            r8.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r10.m10587()
            java.lang.String r1 = "event_filters"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r3 = 1
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            java.lang.String r3 = "app_id=? AND event_name=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r5 = 0
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r5 = 1
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x004b
            java.util.Map r0 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x009f }
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            return r0
        L_0x004b:
            r0 = 1
            byte[] r0 = r1.getBlob(r0)     // Catch:{ SQLiteException -> 0x009f }
            r2 = 0
            int r3 = r0.length     // Catch:{ SQLiteException -> 0x009f }
            com.google.android.gms.internal.zzfjj r0 = com.google.android.gms.internal.zzfjj.m12783(r0, r2, r3)     // Catch:{ SQLiteException -> 0x009f }
            com.google.android.gms.internal.zzcls r2 = new com.google.android.gms.internal.zzcls     // Catch:{ SQLiteException -> 0x009f }
            r2.<init>()     // Catch:{ SQLiteException -> 0x009f }
            r2.m12876((com.google.android.gms.internal.zzfjj) r0)     // Catch:{ IOException -> 0x008b }
            r0 = 0
            int r3 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x009f }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x009f }
            java.lang.Object r0 = r8.get(r0)     // Catch:{ SQLiteException -> 0x009f }
            java.util.List r0 = (java.util.List) r0     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x007b
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x009f }
            r0.<init>()     // Catch:{ SQLiteException -> 0x009f }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x009f }
            r8.put(r3, r0)     // Catch:{ SQLiteException -> 0x009f }
        L_0x007b:
            r0.add(r2)     // Catch:{ SQLiteException -> 0x009f }
        L_0x007e:
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x004b
            if (r1 == 0) goto L_0x0089
            r1.close()
        L_0x0089:
            r0 = r8
            goto L_0x004a
        L_0x008b:
            r0 = move-exception
            com.google.android.gms.internal.zzchm r2 = r10.m11096()     // Catch:{ SQLiteException -> 0x009f }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x009f }
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r11)     // Catch:{ SQLiteException -> 0x009f }
            r2.m10851(r3, r4, r0)     // Catch:{ SQLiteException -> 0x009f }
            goto L_0x007e
        L_0x009f:
            r0 = move-exception
        L_0x00a0:
            com.google.android.gms.internal.zzchm r2 = r10.m11096()     // Catch:{ all -> 0x00c1 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00c1 }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r11)     // Catch:{ all -> 0x00c1 }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x00c1 }
            if (r1 == 0) goto L_0x00b7
            r1.close()
        L_0x00b7:
            r0 = r9
            goto L_0x004a
        L_0x00b9:
            r0 = move-exception
            r1 = r9
        L_0x00bb:
            if (r1 == 0) goto L_0x00c0
            r1.close()
        L_0x00c0:
            throw r0
        L_0x00c1:
            r0 = move-exception
            goto L_0x00bb
        L_0x00c3:
            r0 = move-exception
            r1 = r9
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10576(java.lang.String, java.lang.String):java.util.Map");
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final boolean m10577() {
        return m10562("select count(1) > 0 from raw_events where realtime = 1", (String[]) null) != 0;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00bd  */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.google.android.gms.internal.zzclv>> m10578(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            r9 = 0
            r10.m11115()
            r10.m11109()
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r11)
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r12)
            android.support.v4.util.ArrayMap r8 = new android.support.v4.util.ArrayMap
            r8.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r10.m10587()
            java.lang.String r1 = "property_filters"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r3 = 1
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            java.lang.String r3 = "app_id=? AND property_name=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r5 = 0
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r5 = 1
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00c3, all -> 0x00b9 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x004b
            java.util.Map r0 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x009f }
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            return r0
        L_0x004b:
            r0 = 1
            byte[] r0 = r1.getBlob(r0)     // Catch:{ SQLiteException -> 0x009f }
            r2 = 0
            int r3 = r0.length     // Catch:{ SQLiteException -> 0x009f }
            com.google.android.gms.internal.zzfjj r0 = com.google.android.gms.internal.zzfjj.m12783(r0, r2, r3)     // Catch:{ SQLiteException -> 0x009f }
            com.google.android.gms.internal.zzclv r2 = new com.google.android.gms.internal.zzclv     // Catch:{ SQLiteException -> 0x009f }
            r2.<init>()     // Catch:{ SQLiteException -> 0x009f }
            r2.m12876((com.google.android.gms.internal.zzfjj) r0)     // Catch:{ IOException -> 0x008b }
            r0 = 0
            int r3 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x009f }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x009f }
            java.lang.Object r0 = r8.get(r0)     // Catch:{ SQLiteException -> 0x009f }
            java.util.List r0 = (java.util.List) r0     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x007b
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x009f }
            r0.<init>()     // Catch:{ SQLiteException -> 0x009f }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x009f }
            r8.put(r3, r0)     // Catch:{ SQLiteException -> 0x009f }
        L_0x007b:
            r0.add(r2)     // Catch:{ SQLiteException -> 0x009f }
        L_0x007e:
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x004b
            if (r1 == 0) goto L_0x0089
            r1.close()
        L_0x0089:
            r0 = r8
            goto L_0x004a
        L_0x008b:
            r0 = move-exception
            com.google.android.gms.internal.zzchm r2 = r10.m11096()     // Catch:{ SQLiteException -> 0x009f }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x009f }
            java.lang.String r3 = "Failed to merge filter"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r11)     // Catch:{ SQLiteException -> 0x009f }
            r2.m10851(r3, r4, r0)     // Catch:{ SQLiteException -> 0x009f }
            goto L_0x007e
        L_0x009f:
            r0 = move-exception
        L_0x00a0:
            com.google.android.gms.internal.zzchm r2 = r10.m11096()     // Catch:{ all -> 0x00c1 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00c1 }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r11)     // Catch:{ all -> 0x00c1 }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x00c1 }
            if (r1 == 0) goto L_0x00b7
            r1.close()
        L_0x00b7:
            r0 = r9
            goto L_0x004a
        L_0x00b9:
            r0 = move-exception
            r1 = r9
        L_0x00bb:
            if (r1 == 0) goto L_0x00c0
            r1.close()
        L_0x00c0:
            throw r0
        L_0x00c1:
            r0 = move-exception
            goto L_0x00bb
        L_0x00c3:
            r0 = move-exception
            r1 = r9
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10578(java.lang.String, java.lang.String):java.util.Map");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final long m10579(String str, String str2) {
        long j;
        zzbq.m9122(str);
        zzbq.m9122(str2);
        m11109();
        m11115();
        SQLiteDatabase r6 = m10587();
        r6.beginTransaction();
        try {
            j = m10563(new StringBuilder(String.valueOf(str2).length() + 32).append("select ").append(str2).append(" from app2 where app_id=?").toString(), new String[]{str}, -1);
            if (j == -1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_id", str);
                contentValues.put("first_open_count", 0);
                contentValues.put("previous_install_count", 0);
                if (r6.insertWithOnConflict("app2", (String) null, contentValues, 5) == -1) {
                    m11096().m10832().m10851("Failed to insert column (got -1). appId", zzchm.m10812(str), str2);
                    r6.endTransaction();
                    return -1;
                }
                j = 0;
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str);
                contentValues2.put(str2, Long.valueOf(1 + j));
                if (((long) r6.update("app2", contentValues2, "app_id = ?", new String[]{str})) == 0) {
                    m11096().m10832().m10851("Failed to update column (got 0). appId", zzchm.m10812(str), str2);
                    r6.endTransaction();
                    return -1;
                }
                r6.setTransactionSuccessful();
                r6.endTransaction();
                return j;
            } catch (SQLiteException e) {
                e = e;
                try {
                    m11096().m10832().m10852("Error inserting column. appId", zzchm.m10812(str), str2, e);
                    return j;
                } finally {
                    r6.endTransaction();
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            j = 0;
            m11096().m10832().m10852("Error inserting column. appId", zzchm.m10812(str), str2, e);
            return j;
        }
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final long m10580() {
        long j = -1;
        Cursor cursor = null;
        try {
            cursor = m10587().rawQuery("select rowid from raw_events order by rowid desc limit 1;", (String[]) null);
            if (cursor.moveToFirst()) {
                j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
            } else if (cursor != null) {
                cursor.close();
            }
        } catch (SQLiteException e) {
            m11096().m10832().m10850("Error querying raw events", e);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10581() {
        return false;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final void m10582() {
        m11115();
        m10587().beginTransaction();
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final long m10583() {
        return m10563("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final void m10584() {
        m11115();
        m10587().setTransactionSuccessful();
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final boolean m10585() {
        return m10562("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final void m10586() {
        m11115();
        m10587().endTransaction();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public final SQLiteDatabase m10587() {
        m11109();
        try {
            return this.f9164.getWritableDatabase();
        } catch (SQLiteException e) {
            m11096().m10834().m10850("Error opening database", e);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003e  */
    /* renamed from: ᵢ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String m10588() {
        /*
            r5 = this;
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r5.m10587()
            java.lang.String r2 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x0024, all -> 0x003a }
            boolean r1 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0045 }
            if (r1 == 0) goto L_0x001e
            r1 = 0
            java.lang.String r0 = r2.getString(r1)     // Catch:{ SQLiteException -> 0x0045 }
            if (r2 == 0) goto L_0x001d
            r2.close()
        L_0x001d:
            return r0
        L_0x001e:
            if (r2 == 0) goto L_0x001d
            r2.close()
            goto L_0x001d
        L_0x0024:
            r1 = move-exception
            r2 = r0
        L_0x0026:
            com.google.android.gms.internal.zzchm r3 = r5.m11096()     // Catch:{ all -> 0x0042 }
            com.google.android.gms.internal.zzcho r3 = r3.m10832()     // Catch:{ all -> 0x0042 }
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.m10850(r4, r1)     // Catch:{ all -> 0x0042 }
            if (r2 == 0) goto L_0x001d
            r2.close()
            goto L_0x001d
        L_0x003a:
            r1 = move-exception
            r2 = r0
        L_0x003c:
            if (r2 == 0) goto L_0x0041
            r2.close()
        L_0x0041:
            throw r1
        L_0x0042:
            r0 = move-exception
            r1 = r0
            goto L_0x003c
        L_0x0045:
            r1 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10588():java.lang.String");
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final boolean m10589() {
        return m10562("select count(1) > 0 from queue where has_realtime = 1", (String[]) null) != 0;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m10590(String str, String str2) {
        zzbq.m9122(str);
        zzbq.m9122(str2);
        m11109();
        m11115();
        try {
            return m10587().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e) {
            m11096().m10832().m10852("Error deleting conditional property", zzchm.m10812(str), m11111().m10797(str2), e);
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a3  */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, com.google.android.gms.internal.zzcmf> m10591(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            r9.m11115()
            r9.m11109()
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r10)
            android.database.sqlite.SQLiteDatabase r0 = r9.m10587()
            java.lang.String r1 = "audience_filter_values"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00a9, all -> 0x009f }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00a9, all -> 0x009f }
            r3 = 1
            java.lang.String r4 = "current_results"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00a9, all -> 0x009f }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00a9, all -> 0x009f }
            r5 = 0
            r4[r5] = r10     // Catch:{ SQLiteException -> 0x00a9, all -> 0x009f }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00a9, all -> 0x009f }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0085 }
            if (r0 != 0) goto L_0x003d
            if (r1 == 0) goto L_0x003b
            r1.close()
        L_0x003b:
            r0 = r8
        L_0x003c:
            return r0
        L_0x003d:
            android.support.v4.util.ArrayMap r0 = new android.support.v4.util.ArrayMap     // Catch:{ SQLiteException -> 0x0085 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x0085 }
        L_0x0042:
            r2 = 0
            int r2 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x0085 }
            r3 = 1
            byte[] r3 = r1.getBlob(r3)     // Catch:{ SQLiteException -> 0x0085 }
            r4 = 0
            int r5 = r3.length     // Catch:{ SQLiteException -> 0x0085 }
            com.google.android.gms.internal.zzfjj r3 = com.google.android.gms.internal.zzfjj.m12783(r3, r4, r5)     // Catch:{ SQLiteException -> 0x0085 }
            com.google.android.gms.internal.zzcmf r4 = new com.google.android.gms.internal.zzcmf     // Catch:{ SQLiteException -> 0x0085 }
            r4.<init>()     // Catch:{ SQLiteException -> 0x0085 }
            r4.m12876((com.google.android.gms.internal.zzfjj) r3)     // Catch:{ IOException -> 0x006d }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0085 }
            r0.put(r2, r4)     // Catch:{ SQLiteException -> 0x0085 }
        L_0x0061:
            boolean r2 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0085 }
            if (r2 != 0) goto L_0x0042
            if (r1 == 0) goto L_0x003c
            r1.close()
            goto L_0x003c
        L_0x006d:
            r3 = move-exception
            com.google.android.gms.internal.zzchm r4 = r9.m11096()     // Catch:{ SQLiteException -> 0x0085 }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ SQLiteException -> 0x0085 }
            java.lang.String r5 = "Failed to merge filter results. appId, audienceId, error"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r10)     // Catch:{ SQLiteException -> 0x0085 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0085 }
            r4.m10852(r5, r6, r2, r3)     // Catch:{ SQLiteException -> 0x0085 }
            goto L_0x0061
        L_0x0085:
            r0 = move-exception
        L_0x0086:
            com.google.android.gms.internal.zzchm r2 = r9.m11096()     // Catch:{ all -> 0x00a7 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00a7 }
            java.lang.String r3 = "Database error querying filter results. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r10)     // Catch:{ all -> 0x00a7 }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x00a7 }
            if (r1 == 0) goto L_0x009d
            r1.close()
        L_0x009d:
            r0 = r8
            goto L_0x003c
        L_0x009f:
            r0 = move-exception
            r1 = r8
        L_0x00a1:
            if (r1 == 0) goto L_0x00a6
            r1.close()
        L_0x00a6:
            throw r0
        L_0x00a7:
            r0 = move-exception
            goto L_0x00a1
        L_0x00a9:
            r0 = move-exception
            r1 = r8
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10591(java.lang.String):java.util.Map");
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0220  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzcgh m10592(java.lang.String r12) {
        /*
            r11 = this;
            r10 = 1
            r9 = 0
            r8 = 0
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r12)
            r11.m11109()
            r11.m11115()
            android.database.sqlite.SQLiteDatabase r0 = r11.m10587()     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            java.lang.String r1 = "apps"
            r2 = 24
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 0
            java.lang.String r4 = "app_instance_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 1
            java.lang.String r4 = "gmp_app_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 2
            java.lang.String r4 = "resettable_device_id_hash"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 3
            java.lang.String r4 = "last_bundle_index"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 4
            java.lang.String r4 = "last_bundle_start_timestamp"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 5
            java.lang.String r4 = "last_bundle_end_timestamp"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 6
            java.lang.String r4 = "app_version"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 7
            java.lang.String r4 = "app_store"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 8
            java.lang.String r4 = "gmp_version"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 9
            java.lang.String r4 = "dev_cert_hash"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 10
            java.lang.String r4 = "measurement_enabled"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 11
            java.lang.String r4 = "day"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 12
            java.lang.String r4 = "daily_public_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 13
            java.lang.String r4 = "daily_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 14
            java.lang.String r4 = "daily_conversions_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 15
            java.lang.String r4 = "config_fetched_time"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 16
            java.lang.String r4 = "failed_config_fetch_time"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 17
            java.lang.String r4 = "app_version_int"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 18
            java.lang.String r4 = "firebase_instance_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 19
            java.lang.String r4 = "daily_error_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 20
            java.lang.String r4 = "daily_realtime_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 21
            java.lang.String r4 = "health_monitor_sample"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 22
            java.lang.String r4 = "android_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r3 = 23
            java.lang.String r4 = "adid_reporting_enabled"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r5 = 0
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0200, all -> 0x021c }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0226 }
            if (r0 != 0) goto L_0x00d4
            if (r1 == 0) goto L_0x00d2
            r1.close()
        L_0x00d2:
            r0 = r8
        L_0x00d3:
            return r0
        L_0x00d4:
            com.google.android.gms.internal.zzcgh r0 = new com.google.android.gms.internal.zzcgh     // Catch:{ SQLiteException -> 0x0226 }
            com.google.android.gms.internal.zzcim r2 = r11.f9487     // Catch:{ SQLiteException -> 0x0226 }
            r0.<init>(r2, r12)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10507((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 1
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10497((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 2
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10504((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 3
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10461((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 4
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10506((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 5
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10496((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 6
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10494((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 7
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10462((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 8
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10500((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 9
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10493((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 10
            boolean r2 = r1.isNull(r2)     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 != 0) goto L_0x013d
            r2 = 10
            int r2 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 == 0) goto L_0x01ec
        L_0x013d:
            r2 = r10
        L_0x013e:
            r0.m10508((boolean) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 11
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10481(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 12
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10485(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 13
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10487(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 14
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10474(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 15
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10464((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 16
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10467(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 17
            boolean r2 = r1.isNull(r2)     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 == 0) goto L_0x01ef
            r2 = -2147483648(0xffffffff80000000, double:NaN)
        L_0x0182:
            r0.m10503((long) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 18
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10501((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 19
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10470(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 20
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10469(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 21
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10465((java.lang.String) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 22
            boolean r2 = r1.isNull(r2)     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 == 0) goto L_0x01f7
            r2 = 0
        L_0x01b3:
            r0.m10510(r2)     // Catch:{ SQLiteException -> 0x0226 }
            r2 = 23
            boolean r2 = r1.isNull(r2)     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 != 0) goto L_0x01c6
            r2 = 23
            int r2 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 == 0) goto L_0x01fe
        L_0x01c6:
            r2 = r10
        L_0x01c7:
            r0.m10498((boolean) r2)     // Catch:{ SQLiteException -> 0x0226 }
            r0.m10505()     // Catch:{ SQLiteException -> 0x0226 }
            boolean r2 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0226 }
            if (r2 == 0) goto L_0x01e5
            com.google.android.gms.internal.zzchm r2 = r11.m11096()     // Catch:{ SQLiteException -> 0x0226 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x0226 }
            java.lang.String r3 = "Got multiple records for app, expected one. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x0226 }
            r2.m10850(r3, r4)     // Catch:{ SQLiteException -> 0x0226 }
        L_0x01e5:
            if (r1 == 0) goto L_0x00d3
            r1.close()
            goto L_0x00d3
        L_0x01ec:
            r2 = r9
            goto L_0x013e
        L_0x01ef:
            r2 = 17
            int r2 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x0226 }
            long r2 = (long) r2     // Catch:{ SQLiteException -> 0x0226 }
            goto L_0x0182
        L_0x01f7:
            r2 = 22
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0226 }
            goto L_0x01b3
        L_0x01fe:
            r2 = r9
            goto L_0x01c7
        L_0x0200:
            r0 = move-exception
            r1 = r8
        L_0x0202:
            com.google.android.gms.internal.zzchm r2 = r11.m11096()     // Catch:{ all -> 0x0224 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x0224 }
            java.lang.String r3 = "Error querying app. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ all -> 0x0224 }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x0224 }
            if (r1 == 0) goto L_0x0219
            r1.close()
        L_0x0219:
            r0 = r8
            goto L_0x00d3
        L_0x021c:
            r0 = move-exception
            r1 = r8
        L_0x021e:
            if (r1 == 0) goto L_0x0223
            r1.close()
        L_0x0223:
            throw r0
        L_0x0224:
            r0 = move-exception
            goto L_0x021e
        L_0x0226:
            r0 = move-exception
            goto L_0x0202
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10592(java.lang.String):com.google.android.gms.internal.zzcgh");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzcgl> m10593(String str, String str2, String str3) {
        zzbq.m9122(str);
        m11109();
        m11115();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return m10606(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10594(String str, String str2) {
        zzbq.m9122(str);
        zzbq.m9122(str2);
        m11109();
        m11115();
        try {
            m11096().m10848().m10850("Deleted user attribute rows", Integer.valueOf(m10587().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e) {
            m11096().m10832().m10852("Error deleting user attribute. appId", zzchm.m10812(str), m11111().m10797(str2), e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x015c  */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzcgl m10595(java.lang.String r22, java.lang.String r23) {
        /*
            r21 = this;
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r22)
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r23)
            r21.m11109()
            r21.m11115()
            r10 = 0
            android.database.sqlite.SQLiteDatabase r2 = r21.m10587()     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            java.lang.String r3 = "conditional_properties"
            r4 = 11
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 0
            java.lang.String r6 = "origin"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 1
            java.lang.String r6 = "value"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 2
            java.lang.String r6 = "active"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 3
            java.lang.String r6 = "trigger_event_name"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 4
            java.lang.String r6 = "trigger_timeout"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 5
            java.lang.String r6 = "timed_out_event"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 6
            java.lang.String r6 = "creation_timestamp"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 7
            java.lang.String r6 = "triggered_event"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 8
            java.lang.String r6 = "triggered_timestamp"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 9
            java.lang.String r6 = "time_to_live"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r5 = 10
            java.lang.String r6 = "expired_event"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            java.lang.String r5 = "app_id=? and name=?"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r7 = 0
            r6[r7] = r22     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r7 = 1
            r6[r7] = r23     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r20 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0131, all -> 0x0157 }
            boolean r2 = r20.moveToFirst()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            if (r2 != 0) goto L_0x007d
            if (r20 == 0) goto L_0x007b
            r20.close()
        L_0x007b:
            r5 = 0
        L_0x007c:
            return r5
        L_0x007d:
            r2 = 0
            r0 = r20
            java.lang.String r7 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2 = 1
            r0 = r21
            r1 = r20
            java.lang.Object r6 = r0.m10565((android.database.Cursor) r1, (int) r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2 = 2
            r0 = r20
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            if (r2 == 0) goto L_0x012e
            r11 = 1
        L_0x0097:
            r2 = 3
            r0 = r20
            java.lang.String r12 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2 = 4
            r0 = r20
            long r14 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzclq r2 = r21.m11112()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r3 = 5
            r0 = r20
            byte[] r3 = r0.getBlob(r3)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r4 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            android.os.Parcelable r13 = r2.m11435((byte[]) r3, r4)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzcha r13 = (com.google.android.gms.internal.zzcha) r13     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2 = 6
            r0 = r20
            long r9 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzclq r2 = r21.m11112()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r3 = 7
            r0 = r20
            byte[] r3 = r0.getBlob(r3)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r4 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            android.os.Parcelable r16 = r2.m11435((byte[]) r3, r4)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzcha r16 = (com.google.android.gms.internal.zzcha) r16     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2 = 8
            r0 = r20
            long r4 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2 = 9
            r0 = r20
            long r17 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzclq r2 = r21.m11112()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r3 = 10
            r0 = r20
            byte[] r3 = r0.getBlob(r3)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r8 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            android.os.Parcelable r19 = r2.m11435((byte[]) r3, r8)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzcha r19 = (com.google.android.gms.internal.zzcha) r19     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzcln r2 = new com.google.android.gms.internal.zzcln     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r3 = r23
            r2.<init>(r3, r4, r6, r7)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzcgl r5 = new com.google.android.gms.internal.zzcgl     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r6 = r22
            r8 = r2
            r5.<init>(r6, r7, r8, r9, r11, r12, r13, r14, r16, r17, r19)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            boolean r2 = r20.moveToNext()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            if (r2 == 0) goto L_0x0127
            com.google.android.gms.internal.zzchm r2 = r21.m11096()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            java.lang.String r3 = "Got multiple records for conditional property, expected one"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r22)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            com.google.android.gms.internal.zzchk r6 = r21.m11111()     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r0 = r23
            java.lang.String r6 = r6.m10797(r0)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
            r2.m10851(r3, r4, r6)     // Catch:{ SQLiteException -> 0x0166, all -> 0x0160 }
        L_0x0127:
            if (r20 == 0) goto L_0x007c
            r20.close()
            goto L_0x007c
        L_0x012e:
            r11 = 0
            goto L_0x0097
        L_0x0131:
            r2 = move-exception
            r3 = r10
        L_0x0133:
            com.google.android.gms.internal.zzchm r4 = r21.m11096()     // Catch:{ all -> 0x0162 }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ all -> 0x0162 }
            java.lang.String r5 = "Error querying conditional property"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r22)     // Catch:{ all -> 0x0162 }
            com.google.android.gms.internal.zzchk r7 = r21.m11111()     // Catch:{ all -> 0x0162 }
            r0 = r23
            java.lang.String r7 = r7.m10797(r0)     // Catch:{ all -> 0x0162 }
            r4.m10852(r5, r6, r7, r2)     // Catch:{ all -> 0x0162 }
            if (r3 == 0) goto L_0x0154
            r3.close()
        L_0x0154:
            r5 = 0
            goto L_0x007c
        L_0x0157:
            r2 = move-exception
            r20 = r10
        L_0x015a:
            if (r20 == 0) goto L_0x015f
            r20.close()
        L_0x015f:
            throw r2
        L_0x0160:
            r2 = move-exception
            goto L_0x015a
        L_0x0162:
            r2 = move-exception
            r20 = r3
            goto L_0x015a
        L_0x0166:
            r2 = move-exception
            r3 = r20
            goto L_0x0133
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10595(java.lang.String, java.lang.String):com.google.android.gms.internal.zzcgl");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0079  */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] m10596(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r10)
            r9.m11109()
            r9.m11115()
            android.database.sqlite.SQLiteDatabase r0 = r9.m10587()     // Catch:{ SQLiteException -> 0x005a, all -> 0x0075 }
            java.lang.String r1 = "apps"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x005a, all -> 0x0075 }
            r3 = 0
            java.lang.String r4 = "remote_config"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x005a, all -> 0x0075 }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x005a, all -> 0x0075 }
            r5 = 0
            r4[r5] = r10     // Catch:{ SQLiteException -> 0x005a, all -> 0x0075 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x005a, all -> 0x0075 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x007f }
            if (r0 != 0) goto L_0x0037
            if (r1 == 0) goto L_0x0035
            r1.close()
        L_0x0035:
            r0 = r8
        L_0x0036:
            return r0
        L_0x0037:
            r0 = 0
            byte[] r0 = r1.getBlob(r0)     // Catch:{ SQLiteException -> 0x007f }
            boolean r2 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x007f }
            if (r2 == 0) goto L_0x0054
            com.google.android.gms.internal.zzchm r2 = r9.m11096()     // Catch:{ SQLiteException -> 0x007f }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x007f }
            java.lang.String r3 = "Got multiple records for app config, expected one. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r10)     // Catch:{ SQLiteException -> 0x007f }
            r2.m10850(r3, r4)     // Catch:{ SQLiteException -> 0x007f }
        L_0x0054:
            if (r1 == 0) goto L_0x0036
            r1.close()
            goto L_0x0036
        L_0x005a:
            r0 = move-exception
            r1 = r8
        L_0x005c:
            com.google.android.gms.internal.zzchm r2 = r9.m11096()     // Catch:{ all -> 0x007d }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x007d }
            java.lang.String r3 = "Error querying remote config. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r10)     // Catch:{ all -> 0x007d }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x007d }
            if (r1 == 0) goto L_0x0073
            r1.close()
        L_0x0073:
            r0 = r8
            goto L_0x0036
        L_0x0075:
            r0 = move-exception
            r1 = r8
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()
        L_0x007c:
            throw r0
        L_0x007d:
            r0 = move-exception
            goto L_0x0077
        L_0x007f:
            r0 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10596(java.lang.String):byte[]");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m10597(String str) {
        zzbq.m9122(str);
        m11109();
        m11115();
        try {
            return (long) m10587().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, m11102().m10544(str, zzchc.f9220))))});
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Error deleting over the limit events. appId", zzchm.m10812(str), e);
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a3  */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzclp m10598(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 0
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r10)
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r11)
            r9.m11109()
            r9.m11115()
            android.database.sqlite.SQLiteDatabase r0 = r9.m10587()     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            java.lang.String r1 = "user_attributes"
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            r3 = 0
            java.lang.String r4 = "set_timestamp"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            r3 = 1
            java.lang.String r4 = "value"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            r3 = 2
            java.lang.String r4 = "origin"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            java.lang.String r3 = "app_id=? and name=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            r5 = 0
            r4[r5] = r10     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            r5 = 1
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x007d, all -> 0x00a0 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            if (r0 != 0) goto L_0x0049
            if (r7 == 0) goto L_0x0047
            r7.close()
        L_0x0047:
            r0 = r8
        L_0x0048:
            return r0
        L_0x0049:
            r0 = 0
            long r4 = r7.getLong(r0)     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            r0 = 1
            java.lang.Object r6 = r9.m10565((android.database.Cursor) r7, (int) r0)     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            r0 = 2
            java.lang.String r2 = r7.getString(r0)     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            com.google.android.gms.internal.zzclp r0 = new com.google.android.gms.internal.zzclp     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            r1 = r10
            r3 = r11
            r0.<init>(r1, r2, r3, r4, r6)     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            boolean r1 = r7.moveToNext()     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            if (r1 == 0) goto L_0x0077
            com.google.android.gms.internal.zzchm r1 = r9.m11096()     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            java.lang.String r2 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r3 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r10)     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
            r1.m10850(r2, r3)     // Catch:{ SQLiteException -> 0x00ad, all -> 0x00a7 }
        L_0x0077:
            if (r7 == 0) goto L_0x0048
            r7.close()
            goto L_0x0048
        L_0x007d:
            r0 = move-exception
            r1 = r8
        L_0x007f:
            com.google.android.gms.internal.zzchm r2 = r9.m11096()     // Catch:{ all -> 0x00aa }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00aa }
            java.lang.String r3 = "Error querying user property. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r10)     // Catch:{ all -> 0x00aa }
            com.google.android.gms.internal.zzchk r5 = r9.m11111()     // Catch:{ all -> 0x00aa }
            java.lang.String r5 = r5.m10797(r11)     // Catch:{ all -> 0x00aa }
            r2.m10852(r3, r4, r5, r0)     // Catch:{ all -> 0x00aa }
            if (r1 == 0) goto L_0x009e
            r1.close()
        L_0x009e:
            r0 = r8
            goto L_0x0048
        L_0x00a0:
            r0 = move-exception
        L_0x00a1:
            if (r8 == 0) goto L_0x00a6
            r8.close()
        L_0x00a6:
            throw r0
        L_0x00a7:
            r0 = move-exception
            r8 = r7
            goto L_0x00a1
        L_0x00aa:
            r0 = move-exception
            r8 = r1
            goto L_0x00a1
        L_0x00ad:
            r0 = move-exception
            r1 = r7
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10598(java.lang.String, java.lang.String):com.google.android.gms.internal.zzclp");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10599(zzcme zzcme) throws IOException {
        long r0;
        m11109();
        m11115();
        zzbq.m9120(zzcme);
        zzbq.m9122(zzcme.f9757);
        try {
            byte[] bArr = new byte[zzcme.m12872()];
            zzfjk r02 = zzfjk.m12822(bArr, 0, bArr.length);
            zzcme.m12877(r02);
            r02.m12829();
            zzclq r03 = m11112();
            zzbq.m9120(bArr);
            r03.m11109();
            MessageDigest r1 = zzclq.m11365("MD5");
            if (r1 == null) {
                r03.m11096().m10832().m10849("Failed to get MD5");
                r0 = 0;
            } else {
                r0 = zzclq.m11372(r1.digest(bArr));
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzcme.f9757);
            contentValues.put("metadata_fingerprint", Long.valueOf(r0));
            contentValues.put(TtmlNode.TAG_METADATA, bArr);
            try {
                m10587().insertWithOnConflict("raw_events_metadata", (String) null, contentValues, 4);
                return r0;
            } catch (SQLiteException e) {
                m11096().m10832().m10851("Error storing raw event metadata. appId", zzchm.m10812(zzcme.f9757), e);
                throw e;
            }
        } catch (IOException e2) {
            m11096().m10832().m10851("Data loss. Failed to serialize event metadata. appId", zzchm.m10812(zzcme.f9757), e2);
            throw e2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0147  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzcgp m10600(long r12, java.lang.String r14, boolean r15, boolean r16, boolean r17, boolean r18, boolean r19) {
        /*
            r11 = this;
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r14)
            r11.m11109()
            r11.m11115()
            r0 = 1
            java.lang.String[] r10 = new java.lang.String[r0]
            r0 = 0
            r10[r0] = r14
            com.google.android.gms.internal.zzcgp r8 = new com.google.android.gms.internal.zzcgp
            r8.<init>()
            r9 = 0
            android.database.sqlite.SQLiteDatabase r0 = r11.m10587()     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            java.lang.String r1 = "apps"
            r2 = 6
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r3 = 0
            java.lang.String r4 = "day"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r3 = 1
            java.lang.String r4 = "daily_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r3 = 2
            java.lang.String r4 = "daily_public_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r3 = 3
            java.lang.String r4 = "daily_conversions_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r3 = 4
            java.lang.String r4 = "daily_error_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r3 = 5
            java.lang.String r4 = "daily_realtime_events_count"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r5 = 0
            r4[r5] = r14     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0127, all -> 0x0143 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x014d }
            if (r2 != 0) goto L_0x0072
            com.google.android.gms.internal.zzchm r0 = r11.m11096()     // Catch:{ SQLiteException -> 0x014d }
            com.google.android.gms.internal.zzcho r0 = r0.m10834()     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r2 = "Not updating daily counts, app is not known. appId"
            java.lang.Object r3 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r14)     // Catch:{ SQLiteException -> 0x014d }
            r0.m10850(r2, r3)     // Catch:{ SQLiteException -> 0x014d }
            if (r1 == 0) goto L_0x0070
            r1.close()
        L_0x0070:
            r0 = r8
        L_0x0071:
            return r0
        L_0x0072:
            r2 = 0
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x014d }
            int r2 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x009e
            r2 = 1
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x014d }
            r8.f9167 = r2     // Catch:{ SQLiteException -> 0x014d }
            r2 = 2
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x014d }
            r8.f9170 = r2     // Catch:{ SQLiteException -> 0x014d }
            r2 = 3
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x014d }
            r8.f9169 = r2     // Catch:{ SQLiteException -> 0x014d }
            r2 = 4
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x014d }
            r8.f9168 = r2     // Catch:{ SQLiteException -> 0x014d }
            r2 = 5
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x014d }
            r8.f9166 = r2     // Catch:{ SQLiteException -> 0x014d }
        L_0x009e:
            if (r15 == 0) goto L_0x00a7
            long r2 = r8.f9167     // Catch:{ SQLiteException -> 0x014d }
            r4 = 1
            long r2 = r2 + r4
            r8.f9167 = r2     // Catch:{ SQLiteException -> 0x014d }
        L_0x00a7:
            if (r16 == 0) goto L_0x00b0
            long r2 = r8.f9170     // Catch:{ SQLiteException -> 0x014d }
            r4 = 1
            long r2 = r2 + r4
            r8.f9170 = r2     // Catch:{ SQLiteException -> 0x014d }
        L_0x00b0:
            if (r17 == 0) goto L_0x00b9
            long r2 = r8.f9169     // Catch:{ SQLiteException -> 0x014d }
            r4 = 1
            long r2 = r2 + r4
            r8.f9169 = r2     // Catch:{ SQLiteException -> 0x014d }
        L_0x00b9:
            if (r18 == 0) goto L_0x00c2
            long r2 = r8.f9168     // Catch:{ SQLiteException -> 0x014d }
            r4 = 1
            long r2 = r2 + r4
            r8.f9168 = r2     // Catch:{ SQLiteException -> 0x014d }
        L_0x00c2:
            if (r19 == 0) goto L_0x00cb
            long r2 = r8.f9166     // Catch:{ SQLiteException -> 0x014d }
            r4 = 1
            long r2 = r2 + r4
            r8.f9166 = r2     // Catch:{ SQLiteException -> 0x014d }
        L_0x00cb:
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x014d }
            r2.<init>()     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "day"
            java.lang.Long r4 = java.lang.Long.valueOf(r12)     // Catch:{ SQLiteException -> 0x014d }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "daily_public_events_count"
            long r4 = r8.f9170     // Catch:{ SQLiteException -> 0x014d }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x014d }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "daily_events_count"
            long r4 = r8.f9167     // Catch:{ SQLiteException -> 0x014d }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x014d }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "daily_conversions_count"
            long r4 = r8.f9169     // Catch:{ SQLiteException -> 0x014d }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x014d }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "daily_error_events_count"
            long r4 = r8.f9168     // Catch:{ SQLiteException -> 0x014d }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x014d }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "daily_realtime_events_count"
            long r4 = r8.f9166     // Catch:{ SQLiteException -> 0x014d }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x014d }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x014d }
            java.lang.String r3 = "apps"
            java.lang.String r4 = "app_id=?"
            r0.update(r3, r2, r4, r10)     // Catch:{ SQLiteException -> 0x014d }
            if (r1 == 0) goto L_0x0124
            r1.close()
        L_0x0124:
            r0 = r8
            goto L_0x0071
        L_0x0127:
            r0 = move-exception
            r1 = r9
        L_0x0129:
            com.google.android.gms.internal.zzchm r2 = r11.m11096()     // Catch:{ all -> 0x014b }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x014b }
            java.lang.String r3 = "Error updating daily counts. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r14)     // Catch:{ all -> 0x014b }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x014b }
            if (r1 == 0) goto L_0x0140
            r1.close()
        L_0x0140:
            r0 = r8
            goto L_0x0071
        L_0x0143:
            r0 = move-exception
            r1 = r9
        L_0x0145:
            if (r1 == 0) goto L_0x014a
            r1.close()
        L_0x014a:
            throw r0
        L_0x014b:
            r0 = move-exception
            goto L_0x0145
        L_0x014d:
            r0 = move-exception
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10600(long, java.lang.String, boolean, boolean, boolean, boolean, boolean):com.google.android.gms.internal.zzcgp");
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0126  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzcgw m10601(java.lang.String r19, java.lang.String r20) {
        /*
            r18 = this;
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r19)
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r20)
            r18.m11109()
            r18.m11115()
            r10 = 0
            android.database.sqlite.SQLiteDatabase r2 = r18.m10587()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            java.lang.String r3 = "events"
            r4 = 7
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 0
            java.lang.String r6 = "lifetime_count"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 1
            java.lang.String r6 = "current_bundle_count"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 2
            java.lang.String r6 = "last_fire_timestamp"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 3
            java.lang.String r6 = "last_bundled_timestamp"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 4
            java.lang.String r6 = "last_sampled_complex_event_id"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 5
            java.lang.String r6 = "last_sampling_rate"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r5 = 6
            java.lang.String r6 = "last_exempt_from_sampling"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            java.lang.String r5 = "app_id=? and name=?"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r7 = 0
            r6[r7] = r19     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r7 = 1
            r6[r7] = r20     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r17 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x0121 }
            boolean r2 = r17.moveToFirst()     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 != 0) goto L_0x0061
            if (r17 == 0) goto L_0x005f
            r17.close()
        L_0x005f:
            r3 = 0
        L_0x0060:
            return r3
        L_0x0061:
            r2 = 0
            r0 = r17
            long r6 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r2 = 1
            r0 = r17
            long r8 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r2 = 2
            r0 = r17
            long r10 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r2 = 3
            r0 = r17
            boolean r2 = r0.isNull(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x00d9
            r12 = 0
        L_0x0081:
            r2 = 4
            r0 = r17
            boolean r2 = r0.isNull(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x00e1
            r14 = 0
        L_0x008b:
            r2 = 5
            r0 = r17
            boolean r2 = r0.isNull(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x00ed
            r15 = 0
        L_0x0095:
            r16 = 0
            r2 = 6
            r0 = r17
            boolean r2 = r0.isNull(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 != 0) goto L_0x00b2
            r2 = 6
            r0 = r17
            long r2 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r4 = 1
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x00f9
            r2 = 1
        L_0x00ae:
            java.lang.Boolean r16 = java.lang.Boolean.valueOf(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
        L_0x00b2:
            com.google.android.gms.internal.zzcgw r3 = new com.google.android.gms.internal.zzcgw     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r4 = r19
            r5 = r20
            r3.<init>(r4, r5, r6, r8, r10, r12, r14, r15, r16)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            boolean r2 = r17.moveToNext()     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x00d3
            com.google.android.gms.internal.zzchm r2 = r18.m11096()     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            java.lang.String r4 = "Got multiple records for event aggregates, expected one. appId"
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r19)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r2.m10850(r4, r5)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
        L_0x00d3:
            if (r17 == 0) goto L_0x0060
            r17.close()
            goto L_0x0060
        L_0x00d9:
            r2 = 3
            r0 = r17
            long r12 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            goto L_0x0081
        L_0x00e1:
            r2 = 4
            r0 = r17
            long r2 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            java.lang.Long r14 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            goto L_0x008b
        L_0x00ed:
            r2 = 5
            r0 = r17
            long r2 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            java.lang.Long r15 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            goto L_0x0095
        L_0x00f9:
            r2 = 0
            goto L_0x00ae
        L_0x00fb:
            r2 = move-exception
            r3 = r10
        L_0x00fd:
            com.google.android.gms.internal.zzchm r4 = r18.m11096()     // Catch:{ all -> 0x012c }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ all -> 0x012c }
            java.lang.String r5 = "Error querying events. appId"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r19)     // Catch:{ all -> 0x012c }
            com.google.android.gms.internal.zzchk r7 = r18.m11111()     // Catch:{ all -> 0x012c }
            r0 = r20
            java.lang.String r7 = r7.m10805((java.lang.String) r0)     // Catch:{ all -> 0x012c }
            r4.m10852(r5, r6, r7, r2)     // Catch:{ all -> 0x012c }
            if (r3 == 0) goto L_0x011e
            r3.close()
        L_0x011e:
            r3 = 0
            goto L_0x0060
        L_0x0121:
            r2 = move-exception
            r17 = r10
        L_0x0124:
            if (r17 == 0) goto L_0x0129
            r17.close()
        L_0x0129:
            throw r2
        L_0x012a:
            r2 = move-exception
            goto L_0x0124
        L_0x012c:
            r2 = move-exception
            r17 = r3
            goto L_0x0124
        L_0x0130:
            r2 = move-exception
            r3 = r17
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10601(java.lang.String, java.lang.String):com.google.android.gms.internal.zzcgw");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String m10602(long r8) {
        /*
            r7 = this;
            r0 = 0
            r7.m11109()
            r7.m11115()
            android.database.sqlite.SQLiteDatabase r1 = r7.m10587()     // Catch:{ SQLiteException -> 0x0041, all -> 0x0057 }
            java.lang.String r2 = "select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0041, all -> 0x0057 }
            r4 = 0
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ SQLiteException -> 0x0041, all -> 0x0057 }
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x0041, all -> 0x0057 }
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x0041, all -> 0x0057 }
            boolean r1 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0062 }
            if (r1 != 0) goto L_0x0036
            com.google.android.gms.internal.zzchm r1 = r7.m11096()     // Catch:{ SQLiteException -> 0x0062 }
            com.google.android.gms.internal.zzcho r1 = r1.m10848()     // Catch:{ SQLiteException -> 0x0062 }
            java.lang.String r3 = "No expired configs for apps with pending events"
            r1.m10849(r3)     // Catch:{ SQLiteException -> 0x0062 }
            if (r2 == 0) goto L_0x0035
            r2.close()
        L_0x0035:
            return r0
        L_0x0036:
            r1 = 0
            java.lang.String r0 = r2.getString(r1)     // Catch:{ SQLiteException -> 0x0062 }
            if (r2 == 0) goto L_0x0035
            r2.close()
            goto L_0x0035
        L_0x0041:
            r1 = move-exception
            r2 = r0
        L_0x0043:
            com.google.android.gms.internal.zzchm r3 = r7.m11096()     // Catch:{ all -> 0x005f }
            com.google.android.gms.internal.zzcho r3 = r3.m10832()     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Error selecting expired configs"
            r3.m10850(r4, r1)     // Catch:{ all -> 0x005f }
            if (r2 == 0) goto L_0x0035
            r2.close()
            goto L_0x0035
        L_0x0057:
            r1 = move-exception
            r2 = r0
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()
        L_0x005e:
            throw r1
        L_0x005f:
            r0 = move-exception
            r1 = r0
            goto L_0x0059
        L_0x0062:
            r1 = move-exception
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10602(long):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b5  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.zzclp> m10603(java.lang.String r12) {
        /*
            r11 = this;
            r10 = 0
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r12)
            r11.m11109()
            r11.m11115()
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r11.m10587()     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            java.lang.String r1 = "user_attributes"
            r2 = 4
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            r3 = 0
            java.lang.String r4 = "name"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            r3 = 1
            java.lang.String r4 = "origin"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            r3 = 2
            java.lang.String r4 = "set_timestamp"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            r3 = 3
            java.lang.String r4 = "value"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            r5 = 0
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "rowid"
            java.lang.String r8 = "1000"
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00b2 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            if (r0 != 0) goto L_0x0053
            if (r7 == 0) goto L_0x0051
            r7.close()
        L_0x0051:
            r0 = r9
        L_0x0052:
            return r0
        L_0x0053:
            r0 = 0
            java.lang.String r3 = r7.getString(r0)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            r0 = 1
            java.lang.String r2 = r7.getString(r0)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            if (r2 != 0) goto L_0x0062
            java.lang.String r2 = ""
        L_0x0062:
            r0 = 2
            long r4 = r7.getLong(r0)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            r0 = 3
            java.lang.Object r6 = r11.m10565((android.database.Cursor) r7, (int) r0)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            if (r6 != 0) goto L_0x008d
            com.google.android.gms.internal.zzchm r0 = r11.m11096()     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            java.lang.String r1 = "Read invalid user property value, ignoring it. appId"
            java.lang.Object r2 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            r0.m10850(r1, r2)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
        L_0x0080:
            boolean r0 = r7.moveToNext()     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            if (r0 != 0) goto L_0x0053
            if (r7 == 0) goto L_0x008b
            r7.close()
        L_0x008b:
            r0 = r9
            goto L_0x0052
        L_0x008d:
            com.google.android.gms.internal.zzclp r0 = new com.google.android.gms.internal.zzclp     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            r1 = r12
            r0.<init>(r1, r2, r3, r4, r6)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            r9.add(r0)     // Catch:{ SQLiteException -> 0x0097, all -> 0x00b9 }
            goto L_0x0080
        L_0x0097:
            r0 = move-exception
            r1 = r7
        L_0x0099:
            com.google.android.gms.internal.zzchm r2 = r11.m11096()     // Catch:{ all -> 0x00bc }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00bc }
            java.lang.String r3 = "Error querying user properties. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ all -> 0x00bc }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x00bc }
            if (r1 == 0) goto L_0x00b0
            r1.close()
        L_0x00b0:
            r0 = r10
            goto L_0x0052
        L_0x00b2:
            r0 = move-exception
        L_0x00b3:
            if (r10 == 0) goto L_0x00b8
            r10.close()
        L_0x00b8:
            throw r0
        L_0x00b9:
            r0 = move-exception
            r10 = r7
            goto L_0x00b3
        L_0x00bc:
            r0 = move-exception
            r10 = r1
            goto L_0x00b3
        L_0x00bf:
            r0 = move-exception
            r1 = r10
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10603(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x00ef  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<android.util.Pair<com.google.android.gms.internal.zzcme, java.lang.Long>> m10604(java.lang.String r12, int r13, int r14) {
        /*
            r11 = this;
            r10 = 0
            r1 = 1
            r9 = 0
            r11.m11109()
            r11.m11115()
            if (r13 <= 0) goto L_0x0053
            r0 = r1
        L_0x000c:
            com.google.android.gms.common.internal.zzbq.m9116((boolean) r0)
            if (r14 <= 0) goto L_0x0055
        L_0x0011:
            com.google.android.gms.common.internal.zzbq.m9116((boolean) r1)
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r12)
            android.database.sqlite.SQLiteDatabase r0 = r11.m10587()     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            java.lang.String r1 = "queue"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            r3 = 0
            java.lang.String r4 = "rowid"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            r3 = 1
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            r5 = 0
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "rowid"
            java.lang.String r8 = java.lang.String.valueOf(r13)     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x00cc, all -> 0x00eb }
            boolean r0 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            if (r0 != 0) goto L_0x0057
            java.util.List r0 = java.util.Collections.emptyList()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            if (r2 == 0) goto L_0x0052
            r2.close()
        L_0x0052:
            return r0
        L_0x0053:
            r0 = r9
            goto L_0x000c
        L_0x0055:
            r1 = r9
            goto L_0x0011
        L_0x0057:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r3 = r9
        L_0x005d:
            r1 = 0
            long r4 = r2.getLong(r1)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r1 = 1
            byte[] r1 = r2.getBlob(r1)     // Catch:{ IOException -> 0x00a2 }
            com.google.android.gms.internal.zzclq r6 = r11.m11112()     // Catch:{ IOException -> 0x00a2 }
            byte[] r1 = r6.m11426((byte[]) r1)     // Catch:{ IOException -> 0x00a2 }
            boolean r6 = r0.isEmpty()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            if (r6 != 0) goto L_0x0079
            int r6 = r1.length     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            int r6 = r6 + r3
            if (r6 > r14) goto L_0x009c
        L_0x0079:
            r6 = 0
            int r7 = r1.length     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            com.google.android.gms.internal.zzfjj r6 = com.google.android.gms.internal.zzfjj.m12783(r1, r6, r7)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            com.google.android.gms.internal.zzcme r7 = new com.google.android.gms.internal.zzcme     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r7.<init>()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r7.m12876((com.google.android.gms.internal.zzfjj) r6)     // Catch:{ IOException -> 0x00b7 }
            int r1 = r1.length     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            int r1 = r1 + r3
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            android.util.Pair r3 = android.util.Pair.create(r7, r3)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r0.add(r3)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
        L_0x0094:
            boolean r3 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            if (r3 == 0) goto L_0x009c
            if (r1 <= r14) goto L_0x00fb
        L_0x009c:
            if (r2 == 0) goto L_0x0052
            r2.close()
            goto L_0x0052
        L_0x00a2:
            r1 = move-exception
            com.google.android.gms.internal.zzchm r4 = r11.m11096()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            java.lang.String r5 = "Failed to unzip queued bundle. appId"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r4.m10851(r5, r6, r1)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r1 = r3
            goto L_0x0094
        L_0x00b7:
            r1 = move-exception
            com.google.android.gms.internal.zzchm r4 = r11.m11096()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            java.lang.String r5 = "Failed to merge queued bundle. appId"
            java.lang.Object r6 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r4.m10851(r5, r6, r1)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f3 }
            r1 = r3
            goto L_0x0094
        L_0x00cc:
            r0 = move-exception
            r1 = r10
        L_0x00ce:
            com.google.android.gms.internal.zzchm r2 = r11.m11096()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00f5 }
            java.lang.String r3 = "Error querying bundles. appId"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ all -> 0x00f5 }
            r2.m10851(r3, r4, r0)     // Catch:{ all -> 0x00f5 }
            java.util.List r0 = java.util.Collections.emptyList()     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0052
            r1.close()
            goto L_0x0052
        L_0x00eb:
            r0 = move-exception
            r2 = r10
        L_0x00ed:
            if (r2 == 0) goto L_0x00f2
            r2.close()
        L_0x00f2:
            throw r0
        L_0x00f3:
            r0 = move-exception
            goto L_0x00ed
        L_0x00f5:
            r0 = move-exception
            r2 = r1
            goto L_0x00ed
        L_0x00f8:
            r0 = move-exception
            r1 = r2
            goto L_0x00ce
        L_0x00fb:
            r3 = r1
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10604(java.lang.String, int, int):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0114, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0115, code lost:
        r10 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x011e, code lost:
        r1 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0114 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x0087] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.zzclp> m10605(java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
            r11 = this;
            r10 = 0
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r12)
            r11.m11109()
            r11.m11115()
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r1 = 3
            r0.<init>(r1)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r0.add(r12)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String r1 = "app_id=?"
            r3.<init>(r1)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            boolean r1 = android.text.TextUtils.isEmpty(r13)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            if (r1 != 0) goto L_0x002f
            r0.add(r13)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String r1 = " and origin=?"
            r3.append(r1)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
        L_0x002f:
            boolean r1 = android.text.TextUtils.isEmpty(r14)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            if (r1 != 0) goto L_0x0049
            java.lang.String r1 = java.lang.String.valueOf(r14)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String r2 = "*"
            java.lang.String r1 = r1.concat(r2)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r0.add(r1)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String r1 = " and name glob ?"
            r3.append(r1)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
        L_0x0049:
            int r1 = r0.size()     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.Object[] r4 = r0.toArray(r1)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String[] r4 = (java.lang.String[]) r4     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            android.database.sqlite.SQLiteDatabase r0 = r11.m10587()     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String r1 = "user_attributes"
            r2 = 4
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r5 = 0
            java.lang.String r6 = "name"
            r2[r5] = r6     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r5 = 1
            java.lang.String r6 = "set_timestamp"
            r2[r5] = r6     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r5 = 2
            java.lang.String r6 = "value"
            r2[r5] = r6     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r5 = 3
            java.lang.String r6 = "origin"
            r2[r5] = r6     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "rowid"
            java.lang.String r8 = "1001"
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x011a, all -> 0x010d }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            if (r0 != 0) goto L_0x0095
            if (r7 == 0) goto L_0x0092
            r7.close()
        L_0x0092:
            r0 = r9
        L_0x0093:
            return r0
        L_0x0094:
            r13 = r2
        L_0x0095:
            int r0 = r9.size()     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 < r1) goto L_0x00b8
            com.google.android.gms.internal.zzchm r0 = r11.m11096()     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            java.lang.String r1 = "Read more than the max allowed user properties, ignoring excess"
            r2 = 1000(0x3e8, float:1.401E-42)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            r0.m10850(r1, r2)     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
        L_0x00b1:
            if (r7 == 0) goto L_0x00b6
            r7.close()
        L_0x00b6:
            r0 = r9
            goto L_0x0093
        L_0x00b8:
            r0 = 0
            java.lang.String r3 = r7.getString(r0)     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            r0 = 1
            long r4 = r7.getLong(r0)     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            r0 = 2
            java.lang.Object r6 = r11.m10565((android.database.Cursor) r7, (int) r0)     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            r0 = 3
            java.lang.String r2 = r7.getString(r0)     // Catch:{ SQLiteException -> 0x011d, all -> 0x0114 }
            if (r6 != 0) goto L_0x00e7
            com.google.android.gms.internal.zzchm r0 = r11.m11096()     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            java.lang.String r1 = "(2)Read invalid user property value, ignoring it"
            java.lang.Object r3 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            r0.m10852(r1, r3, r2, r14)     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
        L_0x00e0:
            boolean r0 = r7.moveToNext()     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            if (r0 != 0) goto L_0x0094
            goto L_0x00b1
        L_0x00e7:
            com.google.android.gms.internal.zzclp r0 = new com.google.android.gms.internal.zzclp     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            r1 = r12
            r0.<init>(r1, r2, r3, r4, r6)     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            r9.add(r0)     // Catch:{ SQLiteException -> 0x00f1, all -> 0x0114 }
            goto L_0x00e0
        L_0x00f1:
            r0 = move-exception
            r1 = r7
            r13 = r2
        L_0x00f4:
            com.google.android.gms.internal.zzchm r2 = r11.m11096()     // Catch:{ all -> 0x0117 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x0117 }
            java.lang.String r3 = "(2)Error querying user properties"
            java.lang.Object r4 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r12)     // Catch:{ all -> 0x0117 }
            r2.m10852(r3, r4, r13, r0)     // Catch:{ all -> 0x0117 }
            if (r1 == 0) goto L_0x010b
            r1.close()
        L_0x010b:
            r0 = r10
            goto L_0x0093
        L_0x010d:
            r0 = move-exception
        L_0x010e:
            if (r10 == 0) goto L_0x0113
            r10.close()
        L_0x0113:
            throw r0
        L_0x0114:
            r0 = move-exception
            r10 = r7
            goto L_0x010e
        L_0x0117:
            r0 = move-exception
            r10 = r1
            goto L_0x010e
        L_0x011a:
            r0 = move-exception
            r1 = r10
            goto L_0x00f4
        L_0x011d:
            r0 = move-exception
            r1 = r7
            goto L_0x00f4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10605(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0177  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.zzcgl> m10606(java.lang.String r24, java.lang.String[] r25) {
        /*
            r23 = this;
            r23.m11109()
            r23.m11115()
            java.util.ArrayList r20 = new java.util.ArrayList
            r20.<init>()
            r11 = 0
            android.database.sqlite.SQLiteDatabase r2 = r23.m10587()     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            java.lang.String r3 = "conditional_properties"
            r4 = 13
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 0
            java.lang.String r6 = "app_id"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 1
            java.lang.String r6 = "origin"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 2
            java.lang.String r6 = "name"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 3
            java.lang.String r6 = "value"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 4
            java.lang.String r6 = "active"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 5
            java.lang.String r6 = "trigger_event_name"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 6
            java.lang.String r6 = "trigger_timeout"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 7
            java.lang.String r6 = "timed_out_event"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 8
            java.lang.String r6 = "creation_timestamp"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 9
            java.lang.String r6 = "triggered_event"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 10
            java.lang.String r6 = "triggered_timestamp"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 11
            java.lang.String r6 = "time_to_live"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r5 = 12
            java.lang.String r6 = "expired_event"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            java.lang.String r10 = "1001"
            r5 = r24
            r6 = r25
            android.database.Cursor r21 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0157, all -> 0x0172 }
            boolean r2 = r21.moveToFirst()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            if (r2 != 0) goto L_0x0088
            if (r21 == 0) goto L_0x0085
            r21.close()
        L_0x0085:
            r2 = r20
        L_0x0087:
            return r2
        L_0x0088:
            int r2 = r20.size()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 < r3) goto L_0x00ac
            com.google.android.gms.internal.zzchm r2 = r23.m11096()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            java.lang.String r3 = "Read more than the max allowed conditional properties, ignoring extra"
            r4 = 1000(0x3e8, float:1.401E-42)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2.m10850(r3, r4)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
        L_0x00a4:
            if (r21 == 0) goto L_0x00a9
            r21.close()
        L_0x00a9:
            r2 = r20
            goto L_0x0087
        L_0x00ac:
            r2 = 0
            r0 = r21
            java.lang.String r8 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 1
            r0 = r21
            java.lang.String r7 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 2
            r0 = r21
            java.lang.String r3 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 3
            r0 = r23
            r1 = r21
            java.lang.Object r6 = r0.m10565((android.database.Cursor) r1, (int) r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 4
            r0 = r21
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            if (r2 == 0) goto L_0x0154
            r11 = 1
        L_0x00d4:
            r2 = 5
            r0 = r21
            java.lang.String r12 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 6
            r0 = r21
            long r14 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzclq r2 = r23.m11112()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r4 = 7
            r0 = r21
            byte[] r4 = r0.getBlob(r4)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r5 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            android.os.Parcelable r13 = r2.m11435((byte[]) r4, r5)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzcha r13 = (com.google.android.gms.internal.zzcha) r13     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 8
            r0 = r21
            long r9 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzclq r2 = r23.m11112()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r4 = 9
            r0 = r21
            byte[] r4 = r0.getBlob(r4)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r5 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            android.os.Parcelable r16 = r2.m11435((byte[]) r4, r5)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzcha r16 = (com.google.android.gms.internal.zzcha) r16     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 10
            r0 = r21
            long r4 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2 = 11
            r0 = r21
            long r17 = r0.getLong(r2)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzclq r2 = r23.m11112()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r19 = 12
            r0 = r21
            r1 = r19
            byte[] r19 = r0.getBlob(r1)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r22 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r0 = r19
            r1 = r22
            android.os.Parcelable r19 = r2.m11435((byte[]) r0, r1)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzcha r19 = (com.google.android.gms.internal.zzcha) r19     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzcln r2 = new com.google.android.gms.internal.zzcln     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r2.<init>(r3, r4, r6, r7)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            com.google.android.gms.internal.zzcgl r5 = new com.google.android.gms.internal.zzcgl     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r6 = r8
            r8 = r2
            r5.<init>(r6, r7, r8, r9, r11, r12, r13, r14, r16, r17, r19)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            r0 = r20
            r0.add(r5)     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            boolean r2 = r21.moveToNext()     // Catch:{ SQLiteException -> 0x0181, all -> 0x017b }
            if (r2 != 0) goto L_0x0088
            goto L_0x00a4
        L_0x0154:
            r11 = 0
            goto L_0x00d4
        L_0x0157:
            r2 = move-exception
            r3 = r11
        L_0x0159:
            com.google.android.gms.internal.zzchm r4 = r23.m11096()     // Catch:{ all -> 0x017d }
            com.google.android.gms.internal.zzcho r4 = r4.m10832()     // Catch:{ all -> 0x017d }
            java.lang.String r5 = "Error querying conditional user property value"
            r4.m10850(r5, r2)     // Catch:{ all -> 0x017d }
            java.util.List r2 = java.util.Collections.emptyList()     // Catch:{ all -> 0x017d }
            if (r3 == 0) goto L_0x0087
            r3.close()
            goto L_0x0087
        L_0x0172:
            r2 = move-exception
            r21 = r11
        L_0x0175:
            if (r21 == 0) goto L_0x017a
            r21.close()
        L_0x017a:
            throw r2
        L_0x017b:
            r2 = move-exception
            goto L_0x0175
        L_0x017d:
            r2 = move-exception
            r21 = r3
            goto L_0x0175
        L_0x0181:
            r2 = move-exception
            r3 = r21
            goto L_0x0159
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10606(java.lang.String, java.lang.String[]):java.util.List");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10607(zzcgh zzcgh) {
        zzbq.m9120(zzcgh);
        m11109();
        m11115();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzcgh.m10495());
        contentValues.put("app_instance_id", zzcgh.m10502());
        contentValues.put("gmp_app_id", zzcgh.m10499());
        contentValues.put("resettable_device_id_hash", zzcgh.m10492());
        contentValues.put("last_bundle_index", Long.valueOf(zzcgh.m10509()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(zzcgh.m10463()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(zzcgh.m10466()));
        contentValues.put("app_version", zzcgh.m10480());
        contentValues.put("app_store", zzcgh.m10486());
        contentValues.put("gmp_version", Long.valueOf(zzcgh.m10473()));
        contentValues.put("dev_cert_hash", Long.valueOf(zzcgh.m10468()));
        contentValues.put("measurement_enabled", Boolean.valueOf(zzcgh.m10471()));
        contentValues.put("day", Long.valueOf(zzcgh.m10478()));
        contentValues.put("daily_public_events_count", Long.valueOf(zzcgh.m10472()));
        contentValues.put("daily_events_count", Long.valueOf(zzcgh.m10475()));
        contentValues.put("daily_conversions_count", Long.valueOf(zzcgh.m10479()));
        contentValues.put("config_fetched_time", Long.valueOf(zzcgh.m10511()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(zzcgh.m10476()));
        contentValues.put("app_version_int", Long.valueOf(zzcgh.m10484()));
        contentValues.put("firebase_instance_id", zzcgh.m10460());
        contentValues.put("daily_error_events_count", Long.valueOf(zzcgh.m10483()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(zzcgh.m10482()));
        contentValues.put("health_monitor_sample", zzcgh.m10488());
        contentValues.put("android_id", Long.valueOf(zzcgh.m10490()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(zzcgh.m10491()));
        try {
            SQLiteDatabase r1 = m10587();
            if (((long) r1.update("apps", contentValues, "app_id = ?", new String[]{zzcgh.m10495()})) == 0 && r1.insertWithOnConflict("apps", (String) null, contentValues, 5) == -1) {
                m11096().m10832().m10850("Failed to insert/update app (got -1). appId", zzchm.m10812(zzcgh.m10495()));
            }
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Error storing app. appId", zzchm.m10812(zzcgh.m10495()), e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10608(zzcgw zzcgw) {
        long j = null;
        zzbq.m9120(zzcgw);
        m11109();
        m11115();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzcgw.f9195);
        contentValues.put("name", zzcgw.f9192);
        contentValues.put("lifetime_count", Long.valueOf(zzcgw.f9194));
        contentValues.put("current_bundle_count", Long.valueOf(zzcgw.f9193));
        contentValues.put("last_fire_timestamp", Long.valueOf(zzcgw.f9191));
        contentValues.put("last_bundled_timestamp", Long.valueOf(zzcgw.f9187));
        contentValues.put("last_sampled_complex_event_id", zzcgw.f9188);
        contentValues.put("last_sampling_rate", zzcgw.f9189);
        if (zzcgw.f9190 != null && zzcgw.f9190.booleanValue()) {
            j = 1L;
        }
        contentValues.put("last_exempt_from_sampling", j);
        try {
            if (m10587().insertWithOnConflict(TyphoonApp.VIDEO_TRACKING_EVENTS_KEY, (String) null, contentValues, 5) == -1) {
                m11096().m10832().m10850("Failed to insert/update event aggregates (got -1). appId", zzchm.m10812(zzcgw.f9195));
            }
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Error storing event aggregates. appId", zzchm.m10812(zzcgw.f9195), e);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r8 = r3.f9664;
        r9 = r8.length;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a8, code lost:
        if (r2 >= r9) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ae, code lost:
        if (r8[r2].f9686 != null) goto L_0x00c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b0, code lost:
        m11096().m10834().m10851("Property filter with no ID. Audience definition ignored. appId, audienceId", com.google.android.gms.internal.zzchm.m10812(r13), r3.f9666);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00c5, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00c8, code lost:
        r8 = r3.f9665;
        r9 = r8.length;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00cc, code lost:
        if (r2 >= r9) goto L_0x0147;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d4, code lost:
        if (m10572(r13, r7, r8[r2]) != false) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d6, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d7, code lost:
        if (r2 == false) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d9, code lost:
        r8 = r3.f9664;
        r9 = r8.length;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00dd, code lost:
        if (r3 >= r9) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e5, code lost:
        if (m10573(r13, r7, r8[r3]) != false) goto L_0x0128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e7, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e8, code lost:
        if (r2 != false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ea, code lost:
        m11115();
        m11109();
        com.google.android.gms.common.internal.zzbq.m9122(r13);
        r2 = m10587();
        r2.delete("property_filters", "app_id=? and audience_id=?", new java.lang.String[]{r13, java.lang.String.valueOf(r7)});
        r2.delete("event_filters", "app_id=? and audience_id=?", new java.lang.String[]{r13, java.lang.String.valueOf(r7)});
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0125, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0128, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0147, code lost:
        r2 = true;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m10609(java.lang.String r13, com.google.android.gms.internal.zzclr[] r14) {
        /*
            r12 = this;
            r4 = 1
            r0 = 0
            r12.m11115()
            r12.m11109()
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r13)
            com.google.android.gms.common.internal.zzbq.m9120(r14)
            android.database.sqlite.SQLiteDatabase r5 = r12.m10587()
            r5.beginTransaction()
            r12.m11115()     // Catch:{ all -> 0x009c }
            r12.m11109()     // Catch:{ all -> 0x009c }
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r13)     // Catch:{ all -> 0x009c }
            android.database.sqlite.SQLiteDatabase r1 = r12.m10587()     // Catch:{ all -> 0x009c }
            java.lang.String r2 = "property_filters"
            java.lang.String r3 = "app_id=?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ all -> 0x009c }
            r7 = 0
            r6[r7] = r13     // Catch:{ all -> 0x009c }
            r1.delete(r2, r3, r6)     // Catch:{ all -> 0x009c }
            java.lang.String r2 = "event_filters"
            java.lang.String r3 = "app_id=?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ all -> 0x009c }
            r7 = 0
            r6[r7] = r13     // Catch:{ all -> 0x009c }
            r1.delete(r2, r3, r6)     // Catch:{ all -> 0x009c }
            int r6 = r14.length     // Catch:{ all -> 0x009c }
            r1 = r0
        L_0x0042:
            if (r1 >= r6) goto L_0x012b
            r3 = r14[r1]     // Catch:{ all -> 0x009c }
            r12.m11115()     // Catch:{ all -> 0x009c }
            r12.m11109()     // Catch:{ all -> 0x009c }
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r13)     // Catch:{ all -> 0x009c }
            com.google.android.gms.common.internal.zzbq.m9120(r3)     // Catch:{ all -> 0x009c }
            com.google.android.gms.internal.zzcls[] r2 = r3.f9665     // Catch:{ all -> 0x009c }
            com.google.android.gms.common.internal.zzbq.m9120(r2)     // Catch:{ all -> 0x009c }
            com.google.android.gms.internal.zzclv[] r2 = r3.f9664     // Catch:{ all -> 0x009c }
            com.google.android.gms.common.internal.zzbq.m9120(r2)     // Catch:{ all -> 0x009c }
            java.lang.Integer r2 = r3.f9666     // Catch:{ all -> 0x009c }
            if (r2 != 0) goto L_0x0075
            com.google.android.gms.internal.zzchm r2 = r12.m11096()     // Catch:{ all -> 0x009c }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x009c }
            java.lang.String r3 = "Audience with no ID. appId"
            java.lang.Object r7 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r13)     // Catch:{ all -> 0x009c }
            r2.m10850(r3, r7)     // Catch:{ all -> 0x009c }
        L_0x0072:
            int r1 = r1 + 1
            goto L_0x0042
        L_0x0075:
            java.lang.Integer r2 = r3.f9666     // Catch:{ all -> 0x009c }
            int r7 = r2.intValue()     // Catch:{ all -> 0x009c }
            com.google.android.gms.internal.zzcls[] r8 = r3.f9665     // Catch:{ all -> 0x009c }
            int r9 = r8.length     // Catch:{ all -> 0x009c }
            r2 = r0
        L_0x007f:
            if (r2 >= r9) goto L_0x00a4
            r10 = r8[r2]     // Catch:{ all -> 0x009c }
            java.lang.Integer r10 = r10.f9672     // Catch:{ all -> 0x009c }
            if (r10 != 0) goto L_0x00a1
            com.google.android.gms.internal.zzchm r2 = r12.m11096()     // Catch:{ all -> 0x009c }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x009c }
            java.lang.String r7 = "Event filter with no ID. Audience definition ignored. appId, audienceId"
            java.lang.Object r8 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r13)     // Catch:{ all -> 0x009c }
            java.lang.Integer r3 = r3.f9666     // Catch:{ all -> 0x009c }
            r2.m10851(r7, r8, r3)     // Catch:{ all -> 0x009c }
            goto L_0x0072
        L_0x009c:
            r0 = move-exception
            r5.endTransaction()
            throw r0
        L_0x00a1:
            int r2 = r2 + 1
            goto L_0x007f
        L_0x00a4:
            com.google.android.gms.internal.zzclv[] r8 = r3.f9664     // Catch:{ all -> 0x009c }
            int r9 = r8.length     // Catch:{ all -> 0x009c }
            r2 = r0
        L_0x00a8:
            if (r2 >= r9) goto L_0x00c8
            r10 = r8[r2]     // Catch:{ all -> 0x009c }
            java.lang.Integer r10 = r10.f9686     // Catch:{ all -> 0x009c }
            if (r10 != 0) goto L_0x00c5
            com.google.android.gms.internal.zzchm r2 = r12.m11096()     // Catch:{ all -> 0x009c }
            com.google.android.gms.internal.zzcho r2 = r2.m10834()     // Catch:{ all -> 0x009c }
            java.lang.String r7 = "Property filter with no ID. Audience definition ignored. appId, audienceId"
            java.lang.Object r8 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r13)     // Catch:{ all -> 0x009c }
            java.lang.Integer r3 = r3.f9666     // Catch:{ all -> 0x009c }
            r2.m10851(r7, r8, r3)     // Catch:{ all -> 0x009c }
            goto L_0x0072
        L_0x00c5:
            int r2 = r2 + 1
            goto L_0x00a8
        L_0x00c8:
            com.google.android.gms.internal.zzcls[] r8 = r3.f9665     // Catch:{ all -> 0x009c }
            int r9 = r8.length     // Catch:{ all -> 0x009c }
            r2 = r0
        L_0x00cc:
            if (r2 >= r9) goto L_0x0147
            r10 = r8[r2]     // Catch:{ all -> 0x009c }
            boolean r10 = r12.m10572((java.lang.String) r13, (int) r7, (com.google.android.gms.internal.zzcls) r10)     // Catch:{ all -> 0x009c }
            if (r10 != 0) goto L_0x0125
            r2 = r0
        L_0x00d7:
            if (r2 == 0) goto L_0x00e8
            com.google.android.gms.internal.zzclv[] r8 = r3.f9664     // Catch:{ all -> 0x009c }
            int r9 = r8.length     // Catch:{ all -> 0x009c }
            r3 = r0
        L_0x00dd:
            if (r3 >= r9) goto L_0x00e8
            r10 = r8[r3]     // Catch:{ all -> 0x009c }
            boolean r10 = r12.m10573((java.lang.String) r13, (int) r7, (com.google.android.gms.internal.zzclv) r10)     // Catch:{ all -> 0x009c }
            if (r10 != 0) goto L_0x0128
            r2 = r0
        L_0x00e8:
            if (r2 != 0) goto L_0x0072
            r12.m11115()     // Catch:{ all -> 0x009c }
            r12.m11109()     // Catch:{ all -> 0x009c }
            com.google.android.gms.common.internal.zzbq.m9122((java.lang.String) r13)     // Catch:{ all -> 0x009c }
            android.database.sqlite.SQLiteDatabase r2 = r12.m10587()     // Catch:{ all -> 0x009c }
            java.lang.String r3 = "property_filters"
            java.lang.String r8 = "app_id=? and audience_id=?"
            r9 = 2
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x009c }
            r10 = 0
            r9[r10] = r13     // Catch:{ all -> 0x009c }
            r10 = 1
            java.lang.String r11 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x009c }
            r9[r10] = r11     // Catch:{ all -> 0x009c }
            r2.delete(r3, r8, r9)     // Catch:{ all -> 0x009c }
            java.lang.String r3 = "event_filters"
            java.lang.String r8 = "app_id=? and audience_id=?"
            r9 = 2
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x009c }
            r10 = 0
            r9[r10] = r13     // Catch:{ all -> 0x009c }
            r10 = 1
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x009c }
            r9[r10] = r7     // Catch:{ all -> 0x009c }
            r2.delete(r3, r8, r9)     // Catch:{ all -> 0x009c }
            goto L_0x0072
        L_0x0125:
            int r2 = r2 + 1
            goto L_0x00cc
        L_0x0128:
            int r3 = r3 + 1
            goto L_0x00dd
        L_0x012b:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x009c }
            r1.<init>()     // Catch:{ all -> 0x009c }
            int r2 = r14.length     // Catch:{ all -> 0x009c }
        L_0x0131:
            if (r0 >= r2) goto L_0x013d
            r3 = r14[r0]     // Catch:{ all -> 0x009c }
            java.lang.Integer r3 = r3.f9666     // Catch:{ all -> 0x009c }
            r1.add(r3)     // Catch:{ all -> 0x009c }
            int r0 = r0 + 1
            goto L_0x0131
        L_0x013d:
            r12.m10574((java.lang.String) r13, (java.util.List<java.lang.Integer>) r1)     // Catch:{ all -> 0x009c }
            r5.setTransactionSuccessful()     // Catch:{ all -> 0x009c }
            r5.endTransaction()
            return
        L_0x0147:
            r2 = r4
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgo.m10609(java.lang.String, com.google.android.gms.internal.zzclr[]):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10610(List<Long> list) {
        zzbq.m9120(list);
        m11109();
        m11115();
        StringBuilder sb = new StringBuilder("rowid in (");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                break;
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(list.get(i2).longValue());
            i = i2 + 1;
        }
        sb.append(")");
        int delete = m10587().delete("raw_events", sb.toString(), (String[]) null);
        if (delete != list.size()) {
            m11096().m10832().m10851("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10611(zzcgl zzcgl) {
        zzbq.m9120(zzcgl);
        m11109();
        m11115();
        if (m10598(zzcgl.f9156, zzcgl.f9155.f9653) == null) {
            if (m10562("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{zzcgl.f9156}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzcgl.f9156);
        contentValues.put(TtmlNode.ATTR_TTS_ORIGIN, zzcgl.f9153);
        contentValues.put("name", zzcgl.f9155.f9653);
        m10567(contentValues, "value", zzcgl.f9155.m11364());
        contentValues.put("active", Boolean.valueOf(zzcgl.f9152));
        contentValues.put("trigger_event_name", zzcgl.f9145);
        contentValues.put("trigger_timeout", Long.valueOf(zzcgl.f9147));
        m11112();
        contentValues.put("timed_out_event", zzclq.m11391((Parcelable) zzcgl.f9146));
        contentValues.put("creation_timestamp", Long.valueOf(zzcgl.f9154));
        m11112();
        contentValues.put("triggered_event", zzclq.m11391((Parcelable) zzcgl.f9149));
        contentValues.put("triggered_timestamp", Long.valueOf(zzcgl.f9155.f9650));
        contentValues.put("time_to_live", Long.valueOf(zzcgl.f9150));
        m11112();
        contentValues.put("expired_event", zzclq.m11391((Parcelable) zzcgl.f9151));
        try {
            if (m10587().insertWithOnConflict("conditional_properties", (String) null, contentValues, 5) == -1) {
                m11096().m10832().m10850("Failed to insert/update conditional user property (got -1)", zzchm.m10812(zzcgl.f9156));
            }
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Error storing conditional user property", zzchm.m10812(zzcgl.f9156), e);
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10612(zzcgv zzcgv, long j, boolean z) {
        m11109();
        m11115();
        zzbq.m9120(zzcgv);
        zzbq.m9122(zzcgv.f9186);
        zzcmb zzcmb = new zzcmb();
        zzcmb.f9714 = Long.valueOf(zzcgv.f9184);
        zzcmb.f9716 = new zzcmc[zzcgv.f9182.m10664()];
        Iterator<String> it2 = zzcgv.f9182.iterator();
        int i = 0;
        while (it2.hasNext()) {
            String next = it2.next();
            zzcmc zzcmc = new zzcmc();
            zzcmb.f9716[i] = zzcmc;
            zzcmc.f9722 = next;
            m11112().m11441(zzcmc, zzcgv.f9182.m10665(next));
            i++;
        }
        try {
            byte[] bArr = new byte[zzcmb.m12872()];
            zzfjk r1 = zzfjk.m12822(bArr, 0, bArr.length);
            zzcmb.m12877(r1);
            r1.m12829();
            m11096().m10848().m10851("Saving event, name, data size", m11111().m10805(zzcgv.f9183), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzcgv.f9186);
            contentValues.put("name", zzcgv.f9183);
            contentValues.put("timestamp", Long.valueOf(zzcgv.f9185));
            contentValues.put("metadata_fingerprint", Long.valueOf(j));
            contentValues.put("data", bArr);
            contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (m10587().insert("raw_events", (String) null, contentValues) != -1) {
                    return true;
                }
                m11096().m10832().m10850("Failed to insert raw event (got -1). appId", zzchm.m10812(zzcgv.f9186));
                return false;
            } catch (SQLiteException e) {
                m11096().m10832().m10851("Error storing raw event. appId", zzchm.m10812(zzcgv.f9186), e);
                return false;
            }
        } catch (IOException e2) {
            m11096().m10832().m10851("Data loss. Failed to serialize event params/data. appId", zzchm.m10812(zzcgv.f9186), e2);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10613(zzclp zzclp) {
        zzbq.m9120(zzclp);
        m11109();
        m11115();
        if (m10598(zzclp.f9658, zzclp.f9657) == null) {
            if (zzclq.m11385(zzclp.f9657)) {
                if (m10562("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{zzclp.f9658}) >= 25) {
                    return false;
                }
            } else {
                if (m10562("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{zzclp.f9658, zzclp.f9655}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzclp.f9658);
        contentValues.put(TtmlNode.ATTR_TTS_ORIGIN, zzclp.f9655);
        contentValues.put("name", zzclp.f9657);
        contentValues.put("set_timestamp", Long.valueOf(zzclp.f9656));
        m10567(contentValues, "value", zzclp.f9654);
        try {
            if (m10587().insertWithOnConflict("user_attributes", (String) null, contentValues, 5) == -1) {
                m11096().m10832().m10850("Failed to insert/update user property (got -1). appId", zzchm.m10812(zzclp.f9658));
            }
        } catch (SQLiteException e) {
            m11096().m10832().m10851("Error storing user property. appId", zzchm.m10812(zzclp.f9658), e);
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10614(zzcme zzcme, boolean z) {
        m11109();
        m11115();
        zzbq.m9120(zzcme);
        zzbq.m9122(zzcme.f9757);
        zzbq.m9120(zzcme.f9725);
        m10615();
        long r4 = m11105().m9243();
        if (zzcme.f9725.longValue() < r4 - zzcgn.m10522() || zzcme.f9725.longValue() > zzcgn.m10522() + r4) {
            m11096().m10834().m10852("Storing bundle outside of the max uploading time span. appId, now, timestamp", zzchm.m10812(zzcme.f9757), Long.valueOf(r4), zzcme.f9725);
        }
        try {
            byte[] bArr = new byte[zzcme.m12872()];
            zzfjk r3 = zzfjk.m12822(bArr, 0, bArr.length);
            zzcme.m12877(r3);
            r3.m12829();
            byte[] r0 = m11112().m11446(bArr);
            m11096().m10848().m10850("Saving bundle, size", Integer.valueOf(r0.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzcme.f9757);
            contentValues.put("bundle_end_timestamp", zzcme.f9725);
            contentValues.put("data", r0);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (m10587().insert("queue", (String) null, contentValues) != -1) {
                    return true;
                }
                m11096().m10832().m10850("Failed to insert bundle (got -1). appId", zzchm.m10812(zzcme.f9757));
                return false;
            } catch (SQLiteException e) {
                m11096().m10832().m10851("Error storing bundle. appId", zzchm.m10812(zzcme.f9757), e);
                return false;
            }
        } catch (IOException e2) {
            m11096().m10832().m10851("Data loss. Failed to serialize bundle. appId", zzchm.m10812(zzcme.f9757), e2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹳ  reason: contains not printable characters */
    public final void m10615() {
        int delete;
        m11109();
        m11115();
        if (m10560()) {
            long r0 = m11098().f9301.m10902();
            long r2 = m11105().m9241();
            if (Math.abs(r2 - r0) > zzchc.f9233.m10671().longValue()) {
                m11098().f9301.m10903(r2);
                m11109();
                m11115();
                if (m10560() && (delete = m10587().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(m11105().m9243()), String.valueOf(zzcgn.m10522())})) > 0) {
                    m11096().m10848().m10850("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                }
            }
        }
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final long m10616() {
        return m10563("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }
}
