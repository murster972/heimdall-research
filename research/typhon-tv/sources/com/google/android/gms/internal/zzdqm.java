package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;

final class zzdqm implements zzdpw<zzdpp> {
    zzdqm() {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static zzdpp m11717(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11718((zzfhe) zzdtl.m12114(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected KmsAeadKey proto", e);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzdpp m11718(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdtl)) {
            throw new GeneralSecurityException("expected KmsAeadKey proto");
        }
        zzdtl zzdtl = (zzdtl) zzfhe;
        zzdvk.m12238(zzdtl.m12121(), 0);
        String r0 = zzdtl.m12119().m12131();
        return zzdpz.m11662(r0).m11660(r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11719(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11720((zzfhe) zzdtn.m12128(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized KmsAeadKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11720(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdtn)) {
            throw new GeneralSecurityException("expected KmsAeadKeyFormat proto");
        }
        return zzdtl.m12113().m12125((zzdtn) zzfhe).m12124(0).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11721(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.KmsAeadKey").m12022(((zzdtl) m11719(zzfes)).m12361()).m12021(zzdsy.zzb.REMOTE).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11722(zzfes zzfes) throws GeneralSecurityException {
        return m11717(zzfes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11723(zzfhe zzfhe) throws GeneralSecurityException {
        return m11718(zzfhe);
    }
}
