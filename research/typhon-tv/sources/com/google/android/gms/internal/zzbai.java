package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbai extends UIController {

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8565;

    public zzbai(View view) {
        this.f8565 = view;
        this.f8565.setEnabled(false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9889() {
        this.f8565.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9890(CastSession castSession) {
        super.m8227(castSession);
        this.f8565.setEnabled(true);
    }
}
