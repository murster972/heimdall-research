package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;

@zzzv
public abstract class zzaan implements zzaal, zzaif<Void> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzaal f3705;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Object f3706 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzalh<zzaat> f3707;

    public zzaan(zzalh<zzaat> zzalh, zzaal zzaal) {
        this.f3707 = zzalh;
        this.f3705 = zzaal;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract zzabb m4244();

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* synthetic */ Object m4245() {
        zzabb r0 = m4244();
        if (r0 == null) {
            this.f3705.m9438(new zzaax(0));
            m4247();
        } else {
            this.f3707.zza(new zzaao(this, r0), new zzaap(this));
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4246() {
        m4247();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m4247();

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4248(zzaax zzaax) {
        synchronized (this.f3706) {
            this.f3705.m9438(zzaax);
            m4247();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4249(zzabb zzabb, zzaat zzaat) {
        try {
            zzabb.m9443(zzaat, (zzabe) new zzaaw(this));
            return true;
        } catch (Throwable th) {
            zzagf.m4796("Could not fetch ad response from ad request service due to an Exception.", th);
            zzbs.zzem().m4505(th, "AdRequestClientTask.getAdResponseFromService");
            this.f3705.m9438(new zzaax(0));
            return false;
        }
    }
}
