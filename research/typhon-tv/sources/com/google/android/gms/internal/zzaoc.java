package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzaoc implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzaoa f8369;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f8370;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ boolean f8371;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ boolean f8372;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ int f8373;

    zzaoc(zzaoa zzaoa, int i, int i2, boolean z, boolean z2) {
        this.f8369 = zzaoa;
        this.f8373 = i;
        this.f8370 = i2;
        this.f8372 = z;
        this.f8371 = z2;
    }

    public final void run() {
        boolean z = false;
        synchronized (this.f8369.f4527) {
            boolean z2 = this.f8373 != this.f8370;
            boolean z3 = !this.f8369.f4519 && this.f8370 == 1;
            boolean z4 = z2 && this.f8370 == 1;
            boolean z5 = z2 && this.f8370 == 2;
            boolean z6 = z2 && this.f8370 == 3;
            boolean z7 = this.f8372 != this.f8371;
            zzaoa zzaoa = this.f8369;
            if (this.f8369.f4519 || z3) {
                z = true;
            }
            boolean unused = zzaoa.f4519 = z;
            if (this.f8369.f4518 != null) {
                if (z3) {
                    try {
                        this.f8369.f4518.m13110();
                    } catch (RemoteException e) {
                        zzagf.m4796("Unable to call onVideoStart()", e);
                    }
                }
                if (z4) {
                    try {
                        this.f8369.f4518.m13107();
                    } catch (RemoteException e2) {
                        zzagf.m4796("Unable to call onVideoPlay()", e2);
                    }
                }
                if (z5) {
                    try {
                        this.f8369.f4518.m13109();
                    } catch (RemoteException e3) {
                        zzagf.m4796("Unable to call onVideoPause()", e3);
                    }
                }
                if (z6) {
                    try {
                        this.f8369.f4518.m13108();
                    } catch (RemoteException e4) {
                        zzagf.m4796("Unable to call onVideoEnd()", e4);
                    }
                }
                if (z7) {
                    try {
                        this.f8369.f4518.m13111(this.f8371);
                    } catch (RemoteException e5) {
                        zzagf.m4796("Unable to call onVideoMute()", e5);
                    }
                }
            }
        }
    }
}
