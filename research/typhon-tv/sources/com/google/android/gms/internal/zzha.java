package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

final class zzha implements zzhc {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Bundle f10681;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f10682;

    zzha(zzgu zzgu, Activity activity, Bundle bundle) {
        this.f10682 = activity;
        this.f10681 = bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12981(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivitySaveInstanceState(this.f10682, this.f10681);
    }
}
