package com.google.android.gms.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

public final class zzcxg extends zzbfm implements Result {
    public static final Parcelable.Creator<zzcxg> CREATOR = new zzcxh();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f9833;

    /* renamed from: 齉  reason: contains not printable characters */
    private Intent f9834;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9835;

    public zzcxg() {
        this(0, (Intent) null);
    }

    zzcxg(int i, int i2, Intent intent) {
        this.f9835 = i;
        this.f9833 = i2;
        this.f9834 = intent;
    }

    private zzcxg(int i, Intent intent) {
        this(2, 0, (Intent) null);
    }

    public final Status s_() {
        return this.f9833 == 0 ? Status.f7464 : Status.f7460;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9835);
        zzbfp.m10185(parcel, 2, this.f9833);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f9834, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
