package com.google.android.gms.internal;

import java.util.Map;

final class zzaja extends zzau {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Map f8249;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzajv f8250;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ byte[] f8251;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaja(zzaiv zzaiv, int i, String str, zzy zzy, zzx zzx, byte[] bArr, Map map, zzajv zzajv) {
        super(i, str, zzy, zzx);
        this.f8251 = bArr;
        this.f8249 = map;
        this.f8250 = zzajv;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Map<String, String> m9649() throws zza {
        return this.f8249 == null ? super.m13360() : this.f8249;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m9650(Object obj) {
        m9739((String) obj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9651(String str) {
        this.f8250.m4785(str);
        super.m9739(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m9652() throws zza {
        return this.f8251 == null ? super.m13373() : this.f8251;
    }
}
