package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsw extends zzffu<zzdsw, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdsw f10075;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsw> f10076;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10077;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10078;

    public static final class zza extends zzffu.zza<zzdsw, zza> implements zzfhg {
        private zza() {
            super(zzdsw.f10075);
        }

        /* synthetic */ zza(zzdsx zzdsx) {
            this();
        }
    }

    static {
        zzdsw zzdsw = new zzdsw();
        f10075 = zzdsw;
        zzdsw.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsw.f10394.m12718();
    }

    private zzdsw() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zzdsw m12000() {
        return f10075;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m12001() {
        return this.f10077;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12002() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10078 != zzdsq.UNKNOWN_HASH.zzhq()) {
            i2 = zzffg.m5236(1, this.f10078) + 0;
        }
        if (this.f10077 != 0) {
            i2 += zzffg.m5245(2, this.f10077);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdsq m12003() {
        zzdsq zzfr = zzdsq.zzfr(this.f10078);
        return zzfr == null ? zzdsq.UNRECOGNIZED : zzfr;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 171 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m12004(int r7, java.lang.Object r8, java.lang.Object r9) {
        /*
            r6 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdsx.f10079
            int r4 = r7 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x0057;
                case 7: goto L_0x00a4;
                case 8: goto L_0x00a8;
                case 9: goto L_0x00c4;
                case 10: goto L_0x00ca;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdsw r6 = new com.google.android.gms.internal.zzdsw
            r6.<init>()
        L_0x0017:
            return r6
        L_0x0018:
            com.google.android.gms.internal.zzdsw r6 = f10075
            goto L_0x0017
        L_0x001b:
            r6 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdsw$zza r6 = new com.google.android.gms.internal.zzdsw$zza
            r6.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r8 = (com.google.android.gms.internal.zzffu.zzh) r8
            com.google.android.gms.internal.zzdsw r9 = (com.google.android.gms.internal.zzdsw) r9
            int r0 = r6.f10078
            if (r0 == 0) goto L_0x004f
            r0 = r1
        L_0x002c:
            int r4 = r6.f10078
            int r3 = r9.f10078
            if (r3 == 0) goto L_0x0051
            r3 = r1
        L_0x0033:
            int r5 = r9.f10078
            int r0 = r8.m12569((boolean) r0, (int) r4, (boolean) r3, (int) r5)
            r6.f10078 = r0
            int r0 = r6.f10077
            if (r0 == 0) goto L_0x0053
            r0 = r1
        L_0x0040:
            int r3 = r6.f10077
            int r4 = r9.f10077
            if (r4 == 0) goto L_0x0055
        L_0x0046:
            int r2 = r9.f10077
            int r0 = r8.m12569((boolean) r0, (int) r3, (boolean) r1, (int) r2)
            r6.f10077 = r0
            goto L_0x0017
        L_0x004f:
            r0 = r2
            goto L_0x002c
        L_0x0051:
            r3 = r2
            goto L_0x0033
        L_0x0053:
            r0 = r2
            goto L_0x0040
        L_0x0055:
            r1 = r2
            goto L_0x0046
        L_0x0057:
            com.google.android.gms.internal.zzffb r8 = (com.google.android.gms.internal.zzffb) r8
            com.google.android.gms.internal.zzffm r9 = (com.google.android.gms.internal.zzffm) r9
            if (r9 != 0) goto L_0x0064
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0063:
            r2 = r1
        L_0x0064:
            if (r2 != 0) goto L_0x00a4
            int r0 = r8.m12415()     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
            switch(r0) {
                case 0: goto L_0x0063;
                case 8: goto L_0x0075;
                case 16: goto L_0x0089;
                default: goto L_0x006d;
            }     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
        L_0x006d:
            boolean r0 = r6.m12537((int) r0, (com.google.android.gms.internal.zzffb) r8)     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
            if (r0 != 0) goto L_0x0064
            r2 = r1
            goto L_0x0064
        L_0x0075:
            int r0 = r8.m12405()     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
            r6.f10078 = r0     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
            goto L_0x0064
        L_0x007c:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0087 }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r6)     // Catch:{ all -> 0x0087 }
            r1.<init>(r0)     // Catch:{ all -> 0x0087 }
            throw r1     // Catch:{ all -> 0x0087 }
        L_0x0087:
            r0 = move-exception
            throw r0
        L_0x0089:
            int r0 = r8.m12402()     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
            r6.f10077 = r0     // Catch:{ zzfge -> 0x007c, IOException -> 0x0090 }
            goto L_0x0064
        L_0x0090:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0087 }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x0087 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0087 }
            r2.<init>(r0)     // Catch:{ all -> 0x0087 }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r6)     // Catch:{ all -> 0x0087 }
            r1.<init>(r0)     // Catch:{ all -> 0x0087 }
            throw r1     // Catch:{ all -> 0x0087 }
        L_0x00a4:
            com.google.android.gms.internal.zzdsw r6 = f10075
            goto L_0x0017
        L_0x00a8:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsw> r0 = f10076
            if (r0 != 0) goto L_0x00bd
            java.lang.Class<com.google.android.gms.internal.zzdsw> r1 = com.google.android.gms.internal.zzdsw.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsw> r0 = f10076     // Catch:{ all -> 0x00c1 }
            if (r0 != 0) goto L_0x00bc
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00c1 }
            com.google.android.gms.internal.zzdsw r2 = f10075     // Catch:{ all -> 0x00c1 }
            r0.<init>(r2)     // Catch:{ all -> 0x00c1 }
            f10076 = r0     // Catch:{ all -> 0x00c1 }
        L_0x00bc:
            monitor-exit(r1)     // Catch:{ all -> 0x00c1 }
        L_0x00bd:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsw> r6 = f10076
            goto L_0x0017
        L_0x00c1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c1 }
            throw r0
        L_0x00c4:
            java.lang.Byte r6 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00ca:
            r6 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdsw.m12004(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12005(zzffg zzffg) throws IOException {
        if (this.f10078 != zzdsq.UNKNOWN_HASH.zzhq()) {
            zzffg.m5274(1, this.f10078);
        }
        if (this.f10077 != 0) {
            zzffg.m5281(2, this.f10077);
        }
        this.f10394.m12719(zzffg);
    }
}
