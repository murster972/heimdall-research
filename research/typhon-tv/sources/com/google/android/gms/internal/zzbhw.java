package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public final class zzbhw implements zzbhm {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Status f8781;

    /* renamed from: 麤  reason: contains not printable characters */
    private final List<byte[]> f8782;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f8783;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, TreeMap<String, byte[]>> f8784;

    public zzbhw(Status status, Map<String, TreeMap<String, byte[]>> map) {
        this(status, map, -1);
    }

    private zzbhw(Status status, Map<String, TreeMap<String, byte[]>> map, long j) {
        this(status, map, -1, (List<byte[]>) null);
    }

    public zzbhw(Status status, Map<String, TreeMap<String, byte[]>> map, long j, List<byte[]> list) {
        this.f8781 = status;
        this.f8784 = map;
        this.f8783 = j;
        this.f8782 = list;
    }

    public zzbhw(Status status, Map<String, TreeMap<String, byte[]>> map, List<byte[]> list) {
        this(status, map, -1, list);
    }

    public final Status s_() {
        return this.f8781;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m10272() {
        return this.f8783;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Map<String, Set<String>> m10273() {
        HashMap hashMap = new HashMap();
        if (this.f8784 != null) {
            for (String next : this.f8784.keySet()) {
                Map map = this.f8784.get(next);
                if (map != null) {
                    hashMap.put(next, map.keySet());
                }
            }
        }
        return hashMap;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<byte[]> m10274() {
        return this.f8782;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m10275(String str, byte[] bArr, String str2) {
        if ((this.f8784 == null || this.f8784.get(str2) == null) ? false : this.f8784.get(str2).get(str) != null) {
            return (byte[]) this.f8784.get(str2).get(str);
        }
        return null;
    }
}
