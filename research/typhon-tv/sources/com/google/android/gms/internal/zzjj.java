package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import java.util.Arrays;
import java.util.List;

@zzzv
public final class zzjj extends zzbfm {
    public static final Parcelable.Creator<zzjj> CREATOR = new zzjl();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f4753;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f4754;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean f4755;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final Bundle f4756;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Bundle f4757;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String f4758;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String f4759;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final boolean f4760;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String f4761;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final zzmn f4762;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final Location f4763;

    /* renamed from: 连任  reason: contains not printable characters */
    public final List<String> f4764;

    /* renamed from: 靐  reason: contains not printable characters */
    public final long f4765;

    /* renamed from: 麤  reason: contains not printable characters */
    public final int f4766;

    /* renamed from: 齉  reason: contains not printable characters */
    public final Bundle f4767;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f4768;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final List<String> f4769;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final String f4770;

    public zzjj(int i, long j, Bundle bundle, int i2, List<String> list, boolean z, int i3, boolean z2, String str, zzmn zzmn, Location location, String str2, Bundle bundle2, Bundle bundle3, List<String> list2, String str3, String str4, boolean z3) {
        this.f4768 = i;
        this.f4765 = j;
        this.f4767 = bundle == null ? new Bundle() : bundle;
        this.f4766 = i2;
        this.f4764 = list;
        this.f4753 = z;
        this.f4754 = i3;
        this.f4755 = z2;
        this.f4761 = str;
        this.f4762 = zzmn;
        this.f4763 = location;
        this.f4758 = str2;
        this.f4756 = bundle2 == null ? new Bundle() : bundle2;
        this.f4757 = bundle3;
        this.f4769 = list2;
        this.f4770 = str3;
        this.f4759 = str4;
        this.f4760 = z3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m5449(zzjj zzjj) {
        zzjj.f4756.putBundle("com.google.ads.mediation.admob.AdMobAdapter", zzjj.f4767);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzjj)) {
            return false;
        }
        zzjj zzjj = (zzjj) obj;
        return this.f4768 == zzjj.f4768 && this.f4765 == zzjj.f4765 && zzbg.m9113(this.f4767, zzjj.f4767) && this.f4766 == zzjj.f4766 && zzbg.m9113(this.f4764, zzjj.f4764) && this.f4753 == zzjj.f4753 && this.f4754 == zzjj.f4754 && this.f4755 == zzjj.f4755 && zzbg.m9113(this.f4761, zzjj.f4761) && zzbg.m9113(this.f4762, zzjj.f4762) && zzbg.m9113(this.f4763, zzjj.f4763) && zzbg.m9113(this.f4758, zzjj.f4758) && zzbg.m9113(this.f4756, zzjj.f4756) && zzbg.m9113(this.f4757, zzjj.f4757) && zzbg.m9113(this.f4769, zzjj.f4769) && zzbg.m9113(this.f4770, zzjj.f4770) && zzbg.m9113(this.f4759, zzjj.f4759) && this.f4760 == zzjj.f4760;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f4768), Long.valueOf(this.f4765), this.f4767, Integer.valueOf(this.f4766), this.f4764, Boolean.valueOf(this.f4753), Integer.valueOf(this.f4754), Boolean.valueOf(this.f4755), this.f4761, this.f4762, this.f4763, this.f4758, this.f4756, this.f4757, this.f4769, this.f4770, this.f4759, Boolean.valueOf(this.f4760)});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f4768);
        zzbfp.m10186(parcel, 2, this.f4765);
        zzbfp.m10187(parcel, 3, this.f4767, false);
        zzbfp.m10185(parcel, 4, this.f4766);
        zzbfp.m10178(parcel, 5, this.f4764, false);
        zzbfp.m10195(parcel, 6, this.f4753);
        zzbfp.m10185(parcel, 7, this.f4754);
        zzbfp.m10195(parcel, 8, this.f4755);
        zzbfp.m10193(parcel, 9, this.f4761, false);
        zzbfp.m10189(parcel, 10, (Parcelable) this.f4762, i, false);
        zzbfp.m10189(parcel, 11, (Parcelable) this.f4763, i, false);
        zzbfp.m10193(parcel, 12, this.f4758, false);
        zzbfp.m10187(parcel, 13, this.f4756, false);
        zzbfp.m10187(parcel, 14, this.f4757, false);
        zzbfp.m10178(parcel, 15, this.f4769, false);
        zzbfp.m10193(parcel, 16, this.f4770, false);
        zzbfp.m10193(parcel, 17, this.f4759, false);
        zzbfp.m10195(parcel, 18, this.f4760);
        zzbfp.m10182(parcel, r0);
    }
}
