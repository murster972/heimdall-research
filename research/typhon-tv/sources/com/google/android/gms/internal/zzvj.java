package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzvj extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    double m13531() throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    String m13532() throws RemoteException;

    /* renamed from: ʽ  reason: contains not printable characters */
    String m13533() throws RemoteException;

    /* renamed from: ʾ  reason: contains not printable characters */
    zzll m13534() throws RemoteException;

    /* renamed from: ʿ  reason: contains not printable characters */
    IObjectWrapper m13535() throws RemoteException;

    /* renamed from: ˈ  reason: contains not printable characters */
    Bundle m13536() throws RemoteException;

    /* renamed from: ˊ  reason: contains not printable characters */
    IObjectWrapper m13537() throws RemoteException;

    /* renamed from: ˑ  reason: contains not printable characters */
    void m13538() throws RemoteException;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean m13539() throws RemoteException;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean m13540() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    String m13541() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    List m13542() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m13543(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    zzpq m13544() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    String m13545() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13546(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13547() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13548(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: ﹶ  reason: contains not printable characters */
    zzpm m13549() throws RemoteException;

    /* renamed from: ﾞ  reason: contains not printable characters */
    IObjectWrapper m13550() throws RemoteException;
}
