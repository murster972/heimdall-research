package com.google.android.gms.internal;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@zzzv
public final class zznk {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Context f5171;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f5172;

    /* renamed from: ʽ  reason: contains not printable characters */
    private AtomicBoolean f5173;

    /* renamed from: ˑ  reason: contains not printable characters */
    private File f5174;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f5175;

    /* renamed from: 靐  reason: contains not printable characters */
    private ExecutorService f5176;

    /* renamed from: 麤  reason: contains not printable characters */
    private Map<String, zzno> f5177 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private LinkedHashMap<String, String> f5178 = new LinkedHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private BlockingQueue<zznu> f5179;

    public zznk(Context context, String str, String str2, Map<String, String> map) {
        File externalStorageDirectory;
        this.f5171 = context;
        this.f5172 = str;
        this.f5175 = str2;
        this.f5173 = new AtomicBoolean(false);
        this.f5173.set(((Boolean) zzkb.m5481().m5595(zznh.f4920)).booleanValue());
        if (this.f5173.get() && (externalStorageDirectory = Environment.getExternalStorageDirectory()) != null) {
            this.f5174 = new File(externalStorageDirectory, "sdk_csi_data.txt");
        }
        for (Map.Entry next : map.entrySet()) {
            this.f5178.put((String) next.getKey(), (String) next.getValue());
        }
        this.f5179 = new ArrayBlockingQueue(30);
        this.f5176 = Executors.newSingleThreadExecutor();
        this.f5176.execute(new zznl(this));
        this.f5177.put("action", zzno.f5180);
        this.f5177.put("ad_format", zzno.f5180);
        this.f5177.put("e", zzno.f5181);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a4 A[SYNTHETIC, Splitter:B:30:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b6 A[SYNTHETIC, Splitter:B:37:0x00b6] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0000 A[SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5606() {
        /*
            r5 = this;
        L_0x0000:
            java.util.concurrent.BlockingQueue<com.google.android.gms.internal.zznu> r0 = r5.f5179     // Catch:{ InterruptedException -> 0x004a }
            java.lang.Object r0 = r0.take()     // Catch:{ InterruptedException -> 0x004a }
            com.google.android.gms.internal.zznu r0 = (com.google.android.gms.internal.zznu) r0     // Catch:{ InterruptedException -> 0x004a }
            java.lang.String r2 = r0.m5622()     // Catch:{ InterruptedException -> 0x004a }
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 != 0) goto L_0x0000
            java.util.LinkedHashMap<java.lang.String, java.lang.String> r1 = r5.f5178
            java.util.Map r0 = r0.m5624()
            java.util.Map r0 = r5.m5609(r1, r0)
            java.lang.String r1 = r5.f5175
            android.net.Uri r1 = android.net.Uri.parse(r1)
            android.net.Uri$Builder r3 = r1.buildUpon()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r4 = r0.iterator()
        L_0x002e:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0052
            java.lang.Object r0 = r4.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.getValue()
            java.lang.String r0 = (java.lang.String) r0
            r3.appendQueryParameter(r1, r0)
            goto L_0x002e
        L_0x004a:
            r0 = move-exception
            java.lang.String r1 = "CsiReporter:reporter interrupted"
            com.google.android.gms.internal.zzagf.m4796(r1, r0)
            return
        L_0x0052:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            android.net.Uri r1 = r3.build()
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            java.lang.String r1 = "&it="
            java.lang.StringBuilder r1 = r0.append(r1)
            r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.util.concurrent.atomic.AtomicBoolean r1 = r5.f5173
            boolean r1 = r1.get()
            if (r1 == 0) goto L_0x00ca
            java.io.File r3 = r5.f5174
            if (r3 == 0) goto L_0x00c2
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x009a, all -> 0x00b2 }
            r4 = 1
            r1.<init>(r3, r4)     // Catch:{ IOException -> 0x009a, all -> 0x00b2 }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x00d8 }
            r1.write(r0)     // Catch:{ IOException -> 0x00d8 }
            r0 = 10
            r1.write(r0)     // Catch:{ IOException -> 0x00d8 }
            r1.close()     // Catch:{ IOException -> 0x0091 }
            goto L_0x0000
        L_0x0091:
            r0 = move-exception
            java.lang.String r1 = "CsiReporter: Cannot close file: sdk_csi_data.txt."
            com.google.android.gms.internal.zzagf.m4796(r1, r0)
            goto L_0x0000
        L_0x009a:
            r0 = move-exception
            r1 = r2
        L_0x009c:
            java.lang.String r2 = "CsiReporter: Cannot write to file: sdk_csi_data.txt."
            com.google.android.gms.internal.zzagf.m4796(r2, r0)     // Catch:{ all -> 0x00d6 }
            if (r1 == 0) goto L_0x0000
            r1.close()     // Catch:{ IOException -> 0x00a9 }
            goto L_0x0000
        L_0x00a9:
            r0 = move-exception
            java.lang.String r1 = "CsiReporter: Cannot close file: sdk_csi_data.txt."
            com.google.android.gms.internal.zzagf.m4796(r1, r0)
            goto L_0x0000
        L_0x00b2:
            r0 = move-exception
            r1 = r2
        L_0x00b4:
            if (r1 == 0) goto L_0x00b9
            r1.close()     // Catch:{ IOException -> 0x00ba }
        L_0x00b9:
            throw r0
        L_0x00ba:
            r1 = move-exception
            java.lang.String r2 = "CsiReporter: Cannot close file: sdk_csi_data.txt."
            com.google.android.gms.internal.zzagf.m4796(r2, r1)
            goto L_0x00b9
        L_0x00c2:
            java.lang.String r0 = "CsiReporter: File doesn't exists. Cannot write CSI data to file."
            com.google.android.gms.internal.zzagf.m4791(r0)
            goto L_0x0000
        L_0x00ca:
            com.google.android.gms.ads.internal.zzbs.zzei()
            android.content.Context r1 = r5.f5171
            java.lang.String r2 = r5.f5172
            com.google.android.gms.internal.zzahn.m4582(r1, r2, r0)
            goto L_0x0000
        L_0x00d6:
            r0 = move-exception
            goto L_0x00b4
        L_0x00d8:
            r0 = move-exception
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zznk.m5606():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzno m5608(String str) {
        zzno zzno = this.f5177.get(str);
        return zzno != null ? zzno : zzno.f5182;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<String, String> m5609(Map<String, String> map, Map<String, String> map2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        if (map2 == null) {
            return linkedHashMap;
        }
        for (Map.Entry next : map2.entrySet()) {
            String str = (String) next.getKey();
            String str2 = (String) linkedHashMap.get(str);
            linkedHashMap.put(str, m5608(str).m5615(str2, (String) next.getValue()));
        }
        return linkedHashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5610(List<String> list) {
        if (list != null && !list.isEmpty()) {
            this.f5178.put("e", TextUtils.join(",", list));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5611(zznu zznu) {
        return this.f5179.offer(zznu);
    }
}
