package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.internal.view.SupportMenu;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.evernote.android.state.StateSaver;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.search.SearchAdView;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.common.zzf;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.mopub.common.TyphoonApp;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.UUID;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzajr {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final String f4279 = SearchAdView.class.getName();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final String f4280 = AdLoader.class.getName();

    /* renamed from: 连任  reason: contains not printable characters */
    private static final String f4281 = PublisherInterstitialAd.class.getName();

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f4282 = AdView.class.getName();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final String f4283 = PublisherAdView.class.getName();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String f4284 = InterstitialAd.class.getName();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Handler f4285 = new Handler(Looper.getMainLooper());

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m4744(Context context) {
        int r0 = zzf.m4219().m4227(context);
        return r0 == 0 || r0 == 2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m4745(Context context) {
        if (context.getResources().getConfiguration().orientation != 2) {
            return false;
        }
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return ((int) (((float) displayMetrics.heightPixels) / displayMetrics.density)) < 600;
    }

    @TargetApi(17)
    /* renamed from: ʽ  reason: contains not printable characters */
    public static boolean m4746(Context context) {
        int intValue;
        int intValue2;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (zzq.m9271()) {
            defaultDisplay.getRealMetrics(displayMetrics);
            intValue = displayMetrics.heightPixels;
            intValue2 = displayMetrics.widthPixels;
        } else {
            try {
                intValue = ((Integer) Display.class.getMethod("getRawHeight", new Class[0]).invoke(defaultDisplay, new Object[0])).intValue();
                intValue2 = ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(defaultDisplay, new Object[0])).intValue();
            } catch (Exception e) {
                return false;
            }
        }
        defaultDisplay.getMetrics(displayMetrics);
        return displayMetrics.heightPixels == intValue && displayMetrics.widthPixels == intValue2;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static int m4747(Context context) {
        int identifier = context.getResources().getIdentifier("navigation_bar_width", "dimen", AbstractSpiCall.ANDROID_CLIENT_TYPE);
        if (identifier > 0) {
            return context.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static int m4748(Context context) {
        return DynamiteModule.m9316(context, ModuleDescriptor.MODULE_ID);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m4749(Context context, int i) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        return m4750(displayMetrics, i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m4750(DisplayMetrics displayMetrics, int i) {
        return Math.round(((float) i) / displayMetrics.density);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m4751(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return Settings.Secure.getString(contentResolver, "android_id");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4752() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4753(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (str.startsWith((String) zzkb.m5481().m5595(zznh.f5157))) {
            return true;
        }
        try {
            return Class.forName(str).isAnnotationPresent(zzzv.class);
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(str);
            zzakb.m4797(valueOf.length() != 0 ? "Fail to check class type for class ".concat(valueOf) : new String("Fail to check class type for class "), exc);
            return false;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m4754(Context context) {
        return DynamiteModule.m9310(context, ModuleDescriptor.MODULE_ID);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m4755() {
        UUID randomUUID = UUID.randomUUID();
        byte[] byteArray = BigInteger.valueOf(randomUUID.getLeastSignificantBits()).toByteArray();
        byte[] byteArray2 = BigInteger.valueOf(randomUUID.getMostSignificantBits()).toByteArray();
        String bigInteger = new BigInteger(1, byteArray).toString();
        for (int i = 0; i < 2; i++) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(byteArray);
                instance.update(byteArray2);
                byte[] bArr = new byte[8];
                System.arraycopy(instance.digest(), 0, bArr, 0, 8);
                bigInteger = new BigInteger(1, bArr).toString();
            } catch (NoSuchAlgorithmException e) {
            }
        }
        return bigInteger;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m4756(Context context) {
        return zzf.m4219().m4227(context) == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m4757(Context context, int i) {
        return m4758(context.getResources().getDisplayMetrics(), i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m4758(DisplayMetrics displayMetrics, int i) {
        return (int) TypedValue.applyDimension(1, (float) i, displayMetrics);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4759(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String string = contentResolver == null ? null : Settings.Secure.getString(contentResolver, "android_id");
        if (string == null || m4766()) {
            string = "emulator";
        }
        return m4760(string);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4760(String str) {
        String str2 = null;
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(str.getBytes());
                str2 = String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, instance.digest())});
                break;
            } catch (NoSuchAlgorithmException e) {
                i++;
            } catch (ArithmeticException e2) {
            }
        }
        return str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4761(StackTraceElement[] stackTraceElementArr, String str) {
        String str2;
        int i = 0;
        while (true) {
            if (i + 1 >= stackTraceElementArr.length) {
                str2 = null;
                break;
            }
            StackTraceElement stackTraceElement = stackTraceElementArr[i];
            String className = stackTraceElement.getClassName();
            if (!"loadAd".equalsIgnoreCase(stackTraceElement.getMethodName()) || (!f4282.equalsIgnoreCase(className) && !f4284.equalsIgnoreCase(className) && !f4283.equalsIgnoreCase(className) && !f4281.equalsIgnoreCase(className) && !f4279.equalsIgnoreCase(className) && !f4280.equalsIgnoreCase(className))) {
                i++;
            }
        }
        str2 = stackTraceElementArr[i + 1].getClassName();
        if (str != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(str, ".");
            StringBuilder sb = new StringBuilder();
            int i2 = 2;
            if (stringTokenizer.hasMoreElements()) {
                sb.append(stringTokenizer.nextToken());
                while (true) {
                    int i3 = i2 - 1;
                    if (i2 <= 0 || !stringTokenizer.hasMoreElements()) {
                        str = sb.toString();
                    } else {
                        sb.append(".").append(stringTokenizer.nextToken());
                        i2 = i3;
                    }
                }
                str = sb.toString();
            }
            if (str2 != null && !str2.contains(str)) {
                return str2;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Throwable m4762(Throwable th) {
        Throwable th2;
        if (((Boolean) zzkb.m5481().m5595(zznh.f5158)).booleanValue()) {
            return th;
        }
        LinkedList linkedList = new LinkedList();
        while (th != null) {
            linkedList.push(th);
            th = th.getCause();
        }
        Throwable th3 = null;
        while (!linkedList.isEmpty()) {
            Throwable th4 = (Throwable) linkedList.pop();
            StackTraceElement[] stackTrace = th4.getStackTrace();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new StackTraceElement(th4.getClass().getName(), "<filtered>", "<filtered>", 1));
            boolean z = false;
            for (StackTraceElement stackTraceElement : stackTrace) {
                if (m4753(stackTraceElement.getClassName())) {
                    arrayList.add(stackTraceElement);
                    z = true;
                } else {
                    String className = stackTraceElement.getClassName();
                    if (!TextUtils.isEmpty(className) && (className.startsWith(StateSaver.ANDROID_PREFIX) || className.startsWith(StateSaver.JAVA_PREFIX))) {
                        arrayList.add(stackTraceElement);
                    } else {
                        arrayList.add(new StackTraceElement("<filtered>", "<filtered>", "<filtered>", 1));
                    }
                }
            }
            if (z) {
                th2 = th3 == null ? new Throwable(th4.getMessage()) : new Throwable(th4.getMessage(), th3);
                th2.setStackTrace((StackTraceElement[]) arrayList.toArray(new StackTraceElement[0]));
            } else {
                th2 = th3;
            }
            th3 = th2;
        }
        return th3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4763(Context context, String str, String str2, Bundle bundle, boolean z, zzaju zzaju) {
        if (z) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext == null) {
                applicationContext = context;
            }
            bundle.putString(PubnativeRequest.Parameters.OS, Build.VERSION.RELEASE);
            bundle.putString("api", String.valueOf(Build.VERSION.SDK_INT));
            bundle.putString("appid", applicationContext.getPackageName());
            if (str == null) {
                zzf.m4219();
                str = new StringBuilder(23).append(zzf.m4218(context)).append(".11910000").toString();
            }
            bundle.putString("js", str);
        }
        Uri.Builder appendQueryParameter = new Uri.Builder().scheme(TyphoonApp.HTTPS).path("=").appendQueryParameter("id", str2);
        for (String str3 : bundle.keySet()) {
            appendQueryParameter.appendQueryParameter(str3, bundle.getString(str3));
        }
        zzaju.m9658(appendQueryParameter.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4764(ViewGroup viewGroup, zzjn zzjn, String str, int i, int i2) {
        if (viewGroup.getChildCount() == 0) {
            Context context = viewGroup.getContext();
            TextView textView = new TextView(context);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(i2);
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setBackgroundColor(i);
            int r0 = m4757(context, 3);
            frameLayout.addView(textView, new FrameLayout.LayoutParams(zzjn.f4789 - r0, zzjn.f4797 - r0, 17));
            viewGroup.addView(frameLayout, zzjn.f4789, zzjn.f4797);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4765(boolean z, HttpURLConnection httpURLConnection, String str) {
        httpURLConnection.setConnectTimeout(60000);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setReadTimeout(60000);
        if (str != null) {
            httpURLConnection.setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, str);
        }
        httpURLConnection.setUseCaches(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4766() {
        return Build.DEVICE.startsWith("generic");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4767(Context context, String str, String str2, Bundle bundle, boolean z) {
        m4763(context, (String) null, str2, bundle, true, new zzajs(this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4768(ViewGroup viewGroup, zzjn zzjn, String str) {
        m4764(viewGroup, zzjn, str, -16777216, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4769(ViewGroup viewGroup, zzjn zzjn, String str, String str2) {
        zzakb.m4791(str2);
        m4764(viewGroup, zzjn, str, (int) SupportMenu.CATEGORY_MASK, -16777216);
    }
}
