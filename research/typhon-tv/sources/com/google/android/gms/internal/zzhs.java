package com.google.android.gms.internal;

import java.util.Comparator;

final class zzhs implements Comparator<zzhy> {
    zzhs(zzhr zzhr) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzhy zzhy = (zzhy) obj;
        zzhy zzhy2 = (zzhy) obj2;
        int i = zzhy.f10706 - zzhy2.f10706;
        return i != 0 ? i : (int) (zzhy.f10707 - zzhy2.f10707);
    }
}
