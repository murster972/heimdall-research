package com.google.android.gms.internal;

@zzzv
public final class zzzc implements zzys<zzoj> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f5609;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f5610;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f5611;

    public zzzc(boolean z, boolean z2, boolean z3) {
        this.f5611 = z;
        this.f5609 = z2;
        this.f5610 = z3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0114  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ com.google.android.gms.internal.zzou m6078(com.google.android.gms.internal.zzym r19, org.json.JSONObject r20) throws org.json.JSONException, java.lang.InterruptedException, java.util.concurrent.ExecutionException {
        /*
            r18 = this;
            java.lang.String r4 = "images"
            r5 = 0
            r0 = r18
            boolean r6 = r0.f5611
            r0 = r18
            boolean r7 = r0.f5609
            r2 = r19
            r3 = r20
            java.util.List r2 = r2.m6058(r3, r4, r5, r6, r7)
            java.lang.String r3 = "app_icon"
            r4 = 1
            r0 = r18
            boolean r5 = r0.f5611
            r0 = r19
            r1 = r20
            com.google.android.gms.internal.zzakv r6 = r0.m6057(r1, r3, r4, r5)
            java.lang.String r3 = "video"
            r0 = r19
            r1 = r20
            com.google.android.gms.internal.zzakv r3 = r0.m6056((org.json.JSONObject) r1, (java.lang.String) r3)
            com.google.android.gms.internal.zzakv r12 = r19.m6055((org.json.JSONObject) r20)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r5 = r2.iterator()
        L_0x003c:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L_0x0052
            java.lang.Object r2 = r5.next()
            com.google.android.gms.internal.zzakv r2 = (com.google.android.gms.internal.zzakv) r2
            java.lang.Object r2 = r2.get()
            com.google.android.gms.internal.zzoi r2 = (com.google.android.gms.internal.zzoi) r2
            r4.add(r2)
            goto L_0x003c
        L_0x0052:
            com.google.android.gms.internal.zzanh r15 = com.google.android.gms.internal.zzym.m6050((com.google.android.gms.internal.zzakv<com.google.android.gms.internal.zzanh>) r3)
            com.google.android.gms.internal.zzoj r2 = new com.google.android.gms.internal.zzoj
            java.lang.String r3 = "headline"
            r0 = r20
            java.lang.String r5 = r0.getString(r3)
            r0 = r18
            boolean r3 = r0.f5610
            if (r3 == 0) goto L_0x0106
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r3 = com.google.android.gms.internal.zznh.f5047
            com.google.android.gms.internal.zznf r7 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r3 = r7.m5595(r3)
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x0106
            com.google.android.gms.internal.zzaft r3 = com.google.android.gms.ads.internal.zzbs.zzem()
            android.content.res.Resources r3 = r3.m4476()
            if (r3 == 0) goto L_0x0102
            int r7 = com.google.android.gms.R.string.s7
            java.lang.String r3 = r3.getString(r7)
        L_0x0089:
            if (r5 == 0) goto L_0x00b6
            java.lang.String r7 = java.lang.String.valueOf(r3)
            int r7 = r7.length()
            int r7 = r7 + 3
            java.lang.String r8 = java.lang.String.valueOf(r5)
            int r8 = r8.length()
            int r7 = r7 + r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>(r7)
            java.lang.StringBuilder r3 = r8.append(r3)
            java.lang.String r7 = " : "
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
        L_0x00b6:
            java.lang.String r5 = "body"
            r0 = r20
            java.lang.String r5 = r0.getString(r5)
            java.lang.Object r6 = r6.get()
            com.google.android.gms.internal.zzpq r6 = (com.google.android.gms.internal.zzpq) r6
            java.lang.String r7 = "call_to_action"
            r0 = r20
            java.lang.String r7 = r0.getString(r7)
            java.lang.String r8 = "rating"
            r10 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r0 = r20
            double r8 = r0.optDouble(r8, r10)
            java.lang.String r10 = "store"
            r0 = r20
            java.lang.String r10 = r0.optString(r10)
            java.lang.String r11 = "price"
            r0 = r20
            java.lang.String r11 = r0.optString(r11)
            java.lang.Object r12 = r12.get()
            com.google.android.gms.internal.zzog r12 = (com.google.android.gms.internal.zzog) r12
            android.os.Bundle r13 = new android.os.Bundle
            r13.<init>()
            if (r15 == 0) goto L_0x0108
            com.google.android.gms.internal.zzaoa r14 = r15.m4997()
        L_0x00fc:
            if (r15 == 0) goto L_0x0114
            if (r15 != 0) goto L_0x010a
            r2 = 0
            throw r2
        L_0x0102:
            java.lang.String r3 = "Test Ad"
            goto L_0x0089
        L_0x0106:
            r3 = r5
            goto L_0x00b6
        L_0x0108:
            r14 = 0
            goto L_0x00fc
        L_0x010a:
            android.view.View r15 = (android.view.View) r15
        L_0x010c:
            r16 = 0
            r17 = 0
            r2.<init>(r3, r4, r5, r6, r7, r8, r10, r11, r12, r13, r14, r15, r16, r17)
            return r2
        L_0x0114:
            r15 = 0
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzzc.m6078(com.google.android.gms.internal.zzym, org.json.JSONObject):com.google.android.gms.internal.zzou");
    }
}
