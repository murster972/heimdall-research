package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class zzduf {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f10182;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10183 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private int[] f10184;

    /* renamed from: 麤  reason: contains not printable characters */
    private int[] f10185 = new int[16];

    /* renamed from: 齉  reason: contains not printable characters */
    private int[] f10186;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzdub f10187;

    zzduf(zzdub zzdub, byte[] bArr, int i) {
        this.f10187 = zzdub;
        this.f10184 = zzdub.m12184(bArr, i);
        this.f10186 = zzdub.m12185(this.f10184);
        this.f10182 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12196(int i) {
        if (this.f10182) {
            throw new IllegalStateException("first can only be called once and before next().");
        }
        this.f10182 = true;
        this.f10183 = 8;
        ByteBuffer order = ByteBuffer.allocate(32).order(ByteOrder.LITTLE_ENDIAN);
        order.asIntBuffer().put(this.f10186, 0, 8);
        return order.array();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int[] m12197() {
        this.f10182 = true;
        System.arraycopy(this.f10186, this.f10183, this.f10185, 0, 16 - this.f10183);
        this.f10187.m12180(this.f10184);
        this.f10186 = this.f10187.m12185(this.f10184);
        System.arraycopy(this.f10186, 0, this.f10185, 16 - this.f10183, this.f10183);
        return this.f10185;
    }
}
