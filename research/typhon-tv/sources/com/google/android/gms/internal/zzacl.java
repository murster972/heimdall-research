package com.google.android.gms.internal;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzacl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f3932;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private boolean f3933;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f3934;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private final zzaat f3935;

    /* renamed from: ʽ  reason: contains not printable characters */
    private List<String> f3936;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f3937;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f3938 = -1;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f3939 = -1;

    /* renamed from: ˆ  reason: contains not printable characters */
    private String f3940 = "";

    /* renamed from: ˈ  reason: contains not printable characters */
    private List<String> f3941;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f3942 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f3943 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f3944 = true;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f3945 = true;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f3946 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f3947 = -1;

    /* renamed from: י  reason: contains not printable characters */
    private zzaeq f3948;

    /* renamed from: ـ  reason: contains not printable characters */
    private List<String> f3949;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f3950 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final long f3951 = -1;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private String f3952;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private List<String> f3953;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private zzaey f3954;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f3955 = false;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private zzaaz f3956;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f3957 = false;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private String f3958;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f3959;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f3960;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<String> f3961;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f3962;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f3963;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private List<String> f3964;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f3965 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f3966 = false;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean f3967;

    public zzacl(zzaat zzaat, String str) {
        this.f3960 = str;
        this.f3935 = zzaat;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static long m4297(Map<String, List<String>> map, String str) {
        List list = map.get(str);
        if (list != null && !list.isEmpty()) {
            String str2 = (String) list.get(0);
            try {
                return (long) (Float.parseFloat(str2) * 1000.0f);
            } catch (NumberFormatException e) {
                zzagf.m4791(new StringBuilder(String.valueOf(str).length() + 36 + String.valueOf(str2).length()).append("Could not parse float from ").append(str).append(" header: ").append(str2).toString());
            }
        }
        return -1;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean m4298(Map<String, List<String>> map, String str) {
        List list = map.get(str);
        if (list == null || list.isEmpty()) {
            return false;
        }
        return Boolean.valueOf((String) list.get(0)).booleanValue();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static List<String> m4299(Map<String, List<String>> map, String str) {
        String str2;
        List list = map.get(str);
        if (list == null || list.isEmpty() || (str2 = (String) list.get(0)) == null) {
            return null;
        }
        return Arrays.asList(str2.trim().split("\\s+"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m4300(Map<String, List<String>> map, String str) {
        List list = map.get(str);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (String) list.get(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaax m4301(long j, boolean z) {
        return new zzaax(this.f3935, this.f3960, this.f3962, this.f3961, this.f3936, this.f3947, this.f3950, -1, this.f3941, this.f3938, this.f3939, this.f3963, j, this.f3932, this.f3934, this.f3965, this.f3966, this.f3943, this.f3944, false, this.f3940, this.f3942, this.f3946, this.f3948, this.f3949, this.f3953, this.f3955, this.f3956, this.f3957, this.f3958, this.f3964, this.f3967, this.f3952, this.f3954, this.f3959, this.f3945, this.f3933, this.f3937, z ? 2 : 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4302(String str, Map<String, List<String>> map, String str2) {
        this.f3962 = str2;
        m4303(map);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4303(Map<String, List<String>> map) {
        this.f3963 = m4300(map, "X-Afma-Ad-Size");
        this.f3952 = m4300(map, "X-Afma-Ad-Slot-Size");
        List<String> r0 = m4299(map, "X-Afma-Click-Tracking-Urls");
        if (r0 != null) {
            this.f3961 = r0;
        }
        this.f3959 = m4300(map, "X-Afma-Debug-Signals");
        List list = map.get("X-Afma-Debug-Dialog");
        if (list != null && !list.isEmpty()) {
            this.f3932 = (String) list.get(0);
        }
        List<String> r02 = m4299(map, "X-Afma-Tracking-Urls");
        if (r02 != null) {
            this.f3936 = r02;
        }
        long r03 = m4297(map, "X-Afma-Interstitial-Timeout");
        if (r03 != -1) {
            this.f3947 = r03;
        }
        this.f3950 |= m4298(map, "X-Afma-Mediation");
        List<String> r04 = m4299(map, "X-Afma-Manual-Tracking-Urls");
        if (r04 != null) {
            this.f3941 = r04;
        }
        long r05 = m4297(map, "X-Afma-Refresh-Rate");
        if (r05 != -1) {
            this.f3938 = r05;
        }
        List list2 = map.get("X-Afma-Orientation");
        if (list2 != null && !list2.isEmpty()) {
            String str = (String) list2.get(0);
            if ("portrait".equalsIgnoreCase(str)) {
                this.f3939 = zzbs.zzek().m4644();
            } else if ("landscape".equalsIgnoreCase(str)) {
                this.f3939 = zzbs.zzek().m4652();
            }
        }
        this.f3934 = m4300(map, "X-Afma-ActiveView");
        List list3 = map.get("X-Afma-Use-HTTPS");
        if (list3 != null && !list3.isEmpty()) {
            this.f3943 = Boolean.valueOf((String) list3.get(0)).booleanValue();
        }
        this.f3965 |= m4298(map, "X-Afma-Custom-Rendering-Allowed");
        this.f3966 = "native".equals(m4300(map, "X-Afma-Ad-Format"));
        List list4 = map.get("X-Afma-Content-Url-Opted-Out");
        if (list4 != null && !list4.isEmpty()) {
            this.f3944 = Boolean.valueOf((String) list4.get(0)).booleanValue();
        }
        List list5 = map.get("X-Afma-Content-Vertical-Opted-Out");
        if (list5 != null && !list5.isEmpty()) {
            this.f3945 = Boolean.valueOf((String) list5.get(0)).booleanValue();
        }
        List list6 = map.get("X-Afma-Gws-Query-Id");
        if (list6 != null && !list6.isEmpty()) {
            this.f3940 = (String) list6.get(0);
        }
        String r06 = m4300(map, "X-Afma-Fluid");
        if (r06 != null && r06.equals(VastIconXmlManager.HEIGHT)) {
            this.f3942 = true;
        }
        this.f3946 = "native_express".equals(m4300(map, "X-Afma-Ad-Format"));
        this.f3948 = zzaeq.m4387(m4300(map, "X-Afma-Rewards"));
        if (this.f3949 == null) {
            this.f3949 = m4299(map, "X-Afma-Reward-Video-Start-Urls");
        }
        if (this.f3953 == null) {
            this.f3953 = m4299(map, "X-Afma-Reward-Video-Complete-Urls");
        }
        this.f3955 |= m4298(map, "X-Afma-Use-Displayed-Impression");
        this.f3957 |= m4298(map, "X-Afma-Auto-Collect-Location");
        this.f3958 = m4300(map, "Set-Cookie");
        String r07 = m4300(map, "X-Afma-Auto-Protection-Configuration");
        if (r07 == null || TextUtils.isEmpty(r07)) {
            Uri.Builder buildUpon = Uri.parse("=").buildUpon();
            buildUpon.appendQueryParameter("id", "gmob-apps-blocked-navigation");
            if (!TextUtils.isEmpty(this.f3932)) {
                buildUpon.appendQueryParameter("debugDialog", this.f3932);
            }
            boolean booleanValue = ((Boolean) zzkb.m5481().m5595(zznh.f5155)).booleanValue();
            String builder = buildUpon.toString();
            this.f3956 = new zzaaz(booleanValue, Arrays.asList(new String[]{new StringBuilder(String.valueOf(builder).length() + 18 + String.valueOf("navigationURL").length()).append(builder).append("&").append("navigationURL").append("={NAVIGATION_URL}").toString()}));
        } else {
            try {
                this.f3956 = zzaaz.m4261(new JSONObject(r07));
            } catch (JSONException e) {
                zzagf.m4796("Error parsing configuration JSON", e);
                this.f3956 = new zzaaz();
            }
        }
        List<String> r08 = m4299(map, "X-Afma-Remote-Ping-Urls");
        if (r08 != null) {
            this.f3964 = r08;
        }
        String r09 = m4300(map, "X-Afma-Safe-Browsing");
        if (!TextUtils.isEmpty(r09)) {
            try {
                this.f3954 = zzaey.m4403(new JSONObject(r09));
            } catch (JSONException e2) {
                zzagf.m4796("Error parsing safe browsing header", e2);
            }
        }
        this.f3967 |= m4298(map, "X-Afma-Render-In-Browser");
        String r010 = m4300(map, "X-Afma-Pool");
        if (!TextUtils.isEmpty(r010)) {
            try {
                this.f3933 = new JSONObject(r010).getBoolean("never_pool");
            } catch (JSONException e3) {
                zzagf.m4796("Error parsing interstitial pool header", e3);
            }
        }
        this.f3937 = m4298(map, "X-Afma-Custom-Close-Blocked");
    }
}
