package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.net.URL;
import java.util.Map;

final class zzchu implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzchq f9288;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<String, String> f9289;

    /* renamed from: 靐  reason: contains not printable characters */
    private final byte[] f9290;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f9291;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzchs f9292;

    /* renamed from: 龘  reason: contains not printable characters */
    private final URL f9293;

    public zzchu(zzchq zzchq, String str, URL url, byte[] bArr, Map<String, String> map, zzchs zzchs) {
        this.f9288 = zzchq;
        zzbq.m9122(str);
        zzbq.m9120(url);
        zzbq.m9120(zzchs);
        this.f9293 = url;
        this.f9290 = bArr;
        this.f9292 = zzchs;
        this.f9291 = str;
        this.f9289 = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0112 A[SYNTHETIC, Splitter:B:42:0x0112] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0021 A[SYNTHETIC, Splitter:B:9:0x0021] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r13 = this;
            r3 = 0
            r4 = 0
            com.google.android.gms.internal.zzchq r0 = r13.f9288
            r0.m11107()
            java.net.URL r0 = r13.f9293     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            boolean r1 = r0 instanceof java.net.HttpURLConnection     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            if (r1 != 0) goto L_0x003e
            java.io.IOException r0 = new java.io.IOException     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            java.lang.String r1 = "Failed to obtain HTTP connection"
            r0.<init>(r1)     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            throw r0     // Catch:{ IOException -> 0x001a, all -> 0x010b }
        L_0x001a:
            r9 = move-exception
            r11 = r4
            r8 = r3
            r1 = r4
            r0 = r4
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x00f2 }
        L_0x0024:
            if (r0 == 0) goto L_0x0029
            r0.disconnect()
        L_0x0029:
            com.google.android.gms.internal.zzchq r0 = r13.f9288
            com.google.android.gms.internal.zzcih r0 = r0.m11101()
            com.google.android.gms.internal.zzcht r5 = new com.google.android.gms.internal.zzcht
            java.lang.String r6 = r13.f9291
            com.google.android.gms.internal.zzchs r7 = r13.f9292
            r10 = r4
            r12 = r4
            r5.<init>(r6, r7, r8, r9, r10, r11)
            r0.m10986((java.lang.Runnable) r5)
        L_0x003d:
            return
        L_0x003e:
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            r1 = 0
            r0.setDefaultUseCaches(r1)     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            r1 = 60000(0xea60, float:8.4078E-41)
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            r1 = 61000(0xee48, float:8.5479E-41)
            r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            r1 = 0
            r0.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ IOException -> 0x001a, all -> 0x010b }
            java.util.Map<java.lang.String, java.lang.String> r1 = r13.f9289     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            if (r1 == 0) goto L_0x0087
            java.util.Map<java.lang.String, java.lang.String> r1 = r13.f9289     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.util.Set r1 = r1.entrySet()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
        L_0x0066:
            boolean r1 = r5.hasNext()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            if (r1 == 0) goto L_0x0087
            java.lang.Object r1 = r5.next()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.Object r2 = r1.getKey()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.Object r1 = r1.getValue()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            r0.addRequestProperty(r2, r1)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            goto L_0x0066
        L_0x0082:
            r9 = move-exception
            r11 = r4
            r8 = r3
            r1 = r4
            goto L_0x001f
        L_0x0087:
            byte[] r1 = r13.f9290     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            if (r1 == 0) goto L_0x00ca
            com.google.android.gms.internal.zzchq r1 = r13.f9288     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            com.google.android.gms.internal.zzclq r1 = r1.m11112()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            byte[] r2 = r13.f9290     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            byte[] r2 = r1.m11446((byte[]) r2)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            com.google.android.gms.internal.zzchq r1 = r13.f9288     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            com.google.android.gms.internal.zzchm r1 = r1.m11096()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            com.google.android.gms.internal.zzcho r1 = r1.m10848()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.String r5 = "Uploading data. size"
            int r6 = r2.length     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            r1.m10850(r5, r6)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.lang.String r1 = "Content-Encoding"
            java.lang.String r5 = "gzip"
            r0.addRequestProperty(r1, r5)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            int r1 = r2.length     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            r0.setFixedLengthStreamingMode(r1)     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            r0.connect()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            r1.write(r2)     // Catch:{ IOException -> 0x0158, all -> 0x014d }
            r1.close()     // Catch:{ IOException -> 0x0158, all -> 0x014d }
        L_0x00ca:
            int r3 = r0.getResponseCode()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            java.util.Map r6 = r0.getHeaderFields()     // Catch:{ IOException -> 0x0082, all -> 0x0147 }
            com.google.android.gms.internal.zzchq r1 = r13.f9288     // Catch:{ IOException -> 0x015d, all -> 0x0153 }
            byte[] r5 = com.google.android.gms.internal.zzchq.m10855(r0)     // Catch:{ IOException -> 0x015d, all -> 0x0153 }
            if (r0 == 0) goto L_0x00dd
            r0.disconnect()
        L_0x00dd:
            com.google.android.gms.internal.zzchq r0 = r13.f9288
            com.google.android.gms.internal.zzcih r8 = r0.m11101()
            com.google.android.gms.internal.zzcht r0 = new com.google.android.gms.internal.zzcht
            java.lang.String r1 = r13.f9291
            com.google.android.gms.internal.zzchs r2 = r13.f9292
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r8.m10986((java.lang.Runnable) r0)
            goto L_0x003d
        L_0x00f2:
            r1 = move-exception
            com.google.android.gms.internal.zzchq r2 = r13.f9288
            com.google.android.gms.internal.zzchm r2 = r2.m11096()
            com.google.android.gms.internal.zzcho r2 = r2.m10832()
            java.lang.String r3 = "Error closing HTTP compressed POST connection output stream. appId"
            java.lang.String r5 = r13.f9291
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r5)
            r2.m10851(r3, r5, r1)
            goto L_0x0024
        L_0x010b:
            r0 = move-exception
            r8 = r0
            r6 = r4
            r5 = r4
            r7 = r4
        L_0x0110:
            if (r5 == 0) goto L_0x0115
            r5.close()     // Catch:{ IOException -> 0x012f }
        L_0x0115:
            if (r7 == 0) goto L_0x011a
            r7.disconnect()
        L_0x011a:
            com.google.android.gms.internal.zzchq r0 = r13.f9288
            com.google.android.gms.internal.zzcih r9 = r0.m11101()
            com.google.android.gms.internal.zzcht r0 = new com.google.android.gms.internal.zzcht
            java.lang.String r1 = r13.f9291
            com.google.android.gms.internal.zzchs r2 = r13.f9292
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r9.m10986((java.lang.Runnable) r0)
            throw r8
        L_0x012f:
            r0 = move-exception
            com.google.android.gms.internal.zzchq r1 = r13.f9288
            com.google.android.gms.internal.zzchm r1 = r1.m11096()
            com.google.android.gms.internal.zzcho r1 = r1.m10832()
            java.lang.String r2 = "Error closing HTTP compressed POST connection output stream. appId"
            java.lang.String r5 = r13.f9291
            java.lang.Object r5 = com.google.android.gms.internal.zzchm.m10812((java.lang.String) r5)
            r1.m10851(r2, r5, r0)
            goto L_0x0115
        L_0x0147:
            r1 = move-exception
            r8 = r1
            r6 = r4
            r5 = r4
            r7 = r0
            goto L_0x0110
        L_0x014d:
            r2 = move-exception
            r8 = r2
            r6 = r4
            r5 = r1
            r7 = r0
            goto L_0x0110
        L_0x0153:
            r1 = move-exception
            r8 = r1
            r5 = r4
            r7 = r0
            goto L_0x0110
        L_0x0158:
            r9 = move-exception
            r11 = r4
            r8 = r3
            goto L_0x001f
        L_0x015d:
            r9 = move-exception
            r11 = r6
            r8 = r3
            r1 = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzchu.run():void");
    }
}
