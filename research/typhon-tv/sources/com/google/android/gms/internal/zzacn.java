package com.google.android.gms.internal;

import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONObject;

@zzzv
public final class zzacn {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f3968;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f3969;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f3970;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f3971;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f3972;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final JSONObject f3973;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f3974;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f3975;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f3976;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f3977;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<String> f3978;

    public zzacn(int i, Map<String, String> map) {
        this.f3970 = map.get("url");
        this.f3975 = map.get("base_uri");
        this.f3977 = map.get("post_parameters");
        this.f3974 = m4304(map.get("drt_include"));
        this.f3968 = map.get("request_id");
        this.f3976 = map.get(VastExtensionXmlManager.TYPE);
        this.f3978 = m4305(map.get("errors"));
        this.f3971 = i;
        this.f3969 = map.get("fetched_ad");
        this.f3972 = m4304(map.get("render_test_ad_label"));
        this.f3973 = new JSONObject();
    }

    public zzacn(JSONObject jSONObject) {
        int i = 1;
        this.f3970 = jSONObject.optString("url");
        this.f3975 = jSONObject.optString("base_uri");
        this.f3977 = jSONObject.optString("post_parameters");
        this.f3974 = m4304(jSONObject.optString("drt_include"));
        this.f3968 = jSONObject.optString("request_id");
        this.f3976 = jSONObject.optString(VastExtensionXmlManager.TYPE);
        this.f3978 = m4305(jSONObject.optString("errors"));
        this.f3971 = jSONObject.optInt("valid", 0) == 1 ? -2 : i;
        this.f3969 = jSONObject.optString("fetched_ad");
        this.f3972 = jSONObject.optBoolean("render_test_ad_label");
        JSONObject optJSONObject = jSONObject.optJSONObject("preprocessor_flags");
        this.f3973 = optJSONObject == null ? new JSONObject() : optJSONObject;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m4304(String str) {
        return str != null && (str.equals(PubnativeRequest.LEGACY_ZONE_ID) || str.equals("true"));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static List<String> m4305(String str) {
        if (str == null) {
            return null;
        }
        return Arrays.asList(str.split(","));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m4306() {
        return this.f3976;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m4307() {
        return this.f3974;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m4308() {
        return this.f3968;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m4309() {
        return this.f3969;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m4310() {
        return this.f3972;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m4311() {
        return this.f3970;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<String> m4312() {
        return this.f3978;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m4313() {
        return this.f3977;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m4314() {
        return this.f3975;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m4315() {
        return this.f3971;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4316(String str) {
        this.f3970 = str;
    }
}
