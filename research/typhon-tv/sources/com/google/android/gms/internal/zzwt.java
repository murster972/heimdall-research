package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.Map;

@zzzv
public final class zzwt {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f5532;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f5533;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzanh f5534;

    public zzwt(zzanh zzanh, Map<String, String> map) {
        this.f5534 = zzanh;
        this.f5533 = map.get("forceOrientation");
        if (map.containsKey("allowOrientationChange")) {
            this.f5532 = Boolean.parseBoolean(map.get("allowOrientationChange"));
        } else {
            this.f5532 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6002() {
        if (this.f5534 == null) {
            zzagf.m4791("AdWebView is null");
        } else {
            this.f5534.m4998("portrait".equalsIgnoreCase(this.f5533) ? zzbs.zzek().m4644() : "landscape".equalsIgnoreCase(this.f5533) ? zzbs.zzek().m4652() : this.f5532 ? -1 : zzbs.zzek().m4648());
        }
    }
}
