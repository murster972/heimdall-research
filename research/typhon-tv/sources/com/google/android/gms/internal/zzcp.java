package com.google.android.gms.internal;

import android.os.Build;
import android.os.ConditionVariable;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class zzcp {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile Random f9771 = null;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final ConditionVariable f9772 = new ConditionVariable();

    /* renamed from: 龘  reason: contains not printable characters */
    protected static volatile zzix f9773 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    protected volatile Boolean f9774;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public zzdm f9775;

    public zzcp(zzdm zzdm) {
        this.f9775 = zzdm;
        zzdm.m11622().execute(new zzcq(this));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static Random m11514() {
        if (f9771 == null) {
            synchronized (zzcp.class) {
                if (f9771 == null) {
                    f9771 = new Random();
                }
            }
        }
        return f9771;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m11515() {
        try {
            return Build.VERSION.SDK_INT >= 21 ? ThreadLocalRandom.current().nextInt() : m11514().nextInt();
        } catch (RuntimeException e) {
            return m11514().nextInt();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11517(int i, int i2, long j) throws IOException {
        try {
            f9772.block();
            if (this.f9774.booleanValue() && f9773 != null) {
                zzav zzav = new zzav();
                zzav.f8400 = this.f9775.f9887.getPackageName();
                zzav.f8397 = Long.valueOf(j);
                zziz r0 = f9773.m5435(zzfjs.m12871((zzfjs) zzav));
                r0.m13016(i2);
                r0.m13015(i);
                r0.m13017();
            }
        } catch (Exception e) {
        }
    }
}
