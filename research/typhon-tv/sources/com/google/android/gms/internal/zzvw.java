package com.google.android.gms.internal;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.Pinkamena;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

@zzzv
public final class zzvw<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzvb {

    /* renamed from: 靐  reason: contains not printable characters */
    private final NETWORK_EXTRAS f5500;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> f5501;

    public zzvw(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.f5501 = mediationAdapter;
        this.f5500 = network_extras;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final SERVER_PARAMETERS m5956(String str, int i, String str2) throws RemoteException {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                HashMap hashMap = new HashMap(jSONObject.length());
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str3 = (String) keys.next();
                    hashMap.put(str3, jSONObject.getString(str3));
                }
            } catch (Throwable th) {
                zzakb.m4796("Could not get MediationServerParameters.", th);
                throw new RemoteException();
            }
        } else {
            new HashMap(0);
        }
        Class serverParametersType = this.f5501.getServerParametersType();
        if (serverParametersType == null) {
            return null;
        }
        SERVER_PARAMETERS server_parameters = (MediationServerParameters) serverParametersType.newInstance();
        Pinkamena.DianePie();
        return server_parameters;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m5957(zzjj zzjj) {
        if (!zzjj.f4753) {
            zzkb.m5487();
            return zzajr.m4766();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5958() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m5959() {
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzvj m5960() {
        return null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean m5961() {
        return false;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final zzqm m5962() {
        return null;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final Bundle m5963() {
        return new Bundle();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzvm m5964() {
        return null;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final Bundle m5965() {
        return new Bundle();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final Bundle m5966() {
        return new Bundle();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m5967() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5968() throws RemoteException {
        if (!(this.f5501 instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(this.f5501.getClass().getCanonicalName());
            zzakb.m4791(valueOf.length() != 0 ? "MediationAdapter is not a MediationInterstitialAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzakb.m4792("Showing interstitial from adapter.");
        try {
            MediationInterstitialAdapter mediationInterstitialAdapter = this.f5501;
            Pinkamena.DianePie();
        } catch (Throwable th) {
            zzakb.m4796("Could not show interstitial from adapter.", th);
            throw new RemoteException();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5969() throws RemoteException {
        throw new RemoteException();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5970() throws RemoteException {
        try {
            this.f5501.destroy();
        } catch (Throwable th) {
            zzakb.m4796("Could not destroy adapter.", th);
            throw new RemoteException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m5971() throws RemoteException {
        if (!(this.f5501 instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(this.f5501.getClass().getCanonicalName());
            zzakb.m4791(valueOf.length() != 0 ? "MediationAdapter is not a MediationBannerAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        try {
            return zzn.m9306(this.f5501.getBannerView());
        } catch (Throwable th) {
            zzakb.m4796("Could not get banner view from adapter.", th);
            throw new RemoteException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5972(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5973(IObjectWrapper iObjectWrapper, zzaem zzaem, List<String> list) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5974(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, zzaem zzaem, String str2) throws RemoteException {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5975(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, zzvd zzvd) throws RemoteException {
        m5976(iObjectWrapper, zzjj, str, (String) null, zzvd);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5976(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, String str2, zzvd zzvd) throws RemoteException {
        if (!(this.f5501 instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(this.f5501.getClass().getCanonicalName());
            zzakb.m4791(valueOf.length() != 0 ? "MediationAdapter is not a MediationInterstitialAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzakb.m4792("Requesting interstitial ad from adapter.");
        try {
            MediationInterstitialAdapter mediationInterstitialAdapter = this.f5501;
            new zzvx(zzvd);
            Activity activity = (Activity) zzn.m9307(iObjectWrapper);
            MediationServerParameters r3 = m5956(str, zzjj.f4754, str2);
            MediationAdRequest r4 = zzwj.m5987(zzjj, m5957(zzjj));
            NETWORK_EXTRAS network_extras = this.f5500;
            Pinkamena.DianePie();
        } catch (Throwable th) {
            zzakb.m4796("Could not request interstitial ad from adapter.", th);
            throw new RemoteException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5977(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, String str2, zzvd zzvd, zzpe zzpe, List<String> list) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5978(IObjectWrapper iObjectWrapper, zzjn zzjn, zzjj zzjj, String str, zzvd zzvd) throws RemoteException {
        m5979(iObjectWrapper, zzjn, zzjj, str, (String) null, zzvd);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5979(IObjectWrapper iObjectWrapper, zzjn zzjn, zzjj zzjj, String str, String str2, zzvd zzvd) throws RemoteException {
        if (!(this.f5501 instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(this.f5501.getClass().getCanonicalName());
            zzakb.m4791(valueOf.length() != 0 ? "MediationAdapter is not a MediationBannerAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        zzakb.m4792("Requesting banner ad from adapter.");
        try {
            MediationBannerAdapter mediationBannerAdapter = this.f5501;
            new zzvx(zzvd);
            Activity activity = (Activity) zzn.m9307(iObjectWrapper);
            MediationServerParameters r3 = m5956(str, zzjj.f4754, str2);
            AdSize r4 = zzwj.m5986(zzjn);
            MediationAdRequest r5 = zzwj.m5987(zzjj, m5957(zzjj));
            NETWORK_EXTRAS network_extras = this.f5500;
            Pinkamena.DianePie();
        } catch (Throwable th) {
            zzakb.m4796("Could not request banner ad from adapter.", th);
            throw new RemoteException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5980(zzjj zzjj, String str) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5981(zzjj zzjj, String str, String str2) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5982(boolean z) {
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final zzll m5983() {
        return null;
    }
}
