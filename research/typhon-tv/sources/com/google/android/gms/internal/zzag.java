package com.google.android.gms.internal;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;

final class zzag extends zzah {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaq f8152;

    zzag(zzaq zzaq) {
        this.f8152 = zzaq;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzap m9578(zzr<?> zzr, Map<String, String> map) throws IOException, zza {
        try {
            HttpResponse r1 = this.f8152.m5229(zzr, map);
            int statusCode = r1.getStatusLine().getStatusCode();
            Header[] allHeaders = r1.getAllHeaders();
            ArrayList arrayList = new ArrayList(allHeaders.length);
            for (Header header : allHeaders) {
                arrayList.add(new zzl(header.getName(), header.getValue()));
            }
            if (r1.getEntity() == null) {
                return new zzap(statusCode, arrayList);
            }
            long contentLength = r1.getEntity().getContentLength();
            if (((long) ((int) contentLength)) == contentLength) {
                return new zzap(statusCode, arrayList, (int) r1.getEntity().getContentLength(), r1.getEntity().getContent());
            }
            throw new IOException(new StringBuilder(40).append("Response too large: ").append(contentLength).toString());
        } catch (ConnectTimeoutException e) {
            throw new SocketTimeoutException(e.getMessage());
        }
    }
}
