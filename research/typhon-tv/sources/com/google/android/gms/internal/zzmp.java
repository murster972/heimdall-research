package com.google.android.gms.internal;

import android.os.Parcel;

@zzzv
public final class zzmp extends zzjn {
    public zzmp(zzjn zzjn) {
        super(zzjn.f4798, zzjn.f4795, zzjn.f4797, zzjn.f4796, zzjn.f4794, zzjn.f4789, zzjn.f4790, zzjn.f4791, zzjn.f4792, zzjn.f4793);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f4798, false);
        zzbfp.m10185(parcel, 3, this.f4795);
        zzbfp.m10185(parcel, 6, this.f4794);
        zzbfp.m10182(parcel, r0);
    }
}
