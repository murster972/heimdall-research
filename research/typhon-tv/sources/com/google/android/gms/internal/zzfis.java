package com.google.android.gms.internal;

final class zzfis {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfit f10517 = (zzfiq.m12749() && zzfiq.m12733() ? new zzfiw() : new zzfiu());

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12759(int i) {
        if (i > -12) {
            return -1;
        }
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12760(int i, int i2) {
        if (i > -12 || i2 > -65) {
            return -1;
        }
        return (i2 << 8) ^ i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m12761(int i, int i2, int i3) {
        if (i > -12 || i2 > -65 || i3 > -65) {
            return -1;
        }
        return ((i2 << 8) ^ i) ^ (i3 << 16);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static int m12763(byte[] bArr, int i, int i2) {
        byte b = bArr[i - 1];
        switch (i2 - i) {
            case 0:
                return m12759(b);
            case 1:
                return m12760(b, bArr[i]);
            case 2:
                return m12761((int) b, (int) bArr[i], (int) bArr[i + 1]);
            default:
                throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m12767(CharSequence charSequence) {
        int i;
        int i2 = 0;
        int length = charSequence.length();
        int i3 = 0;
        while (i3 < length && charSequence.charAt(i3) < 128) {
            i3++;
        }
        int i4 = length;
        while (true) {
            if (i3 >= length) {
                i = i4;
                break;
            }
            char charAt = charSequence.charAt(i3);
            if (charAt < 2048) {
                i4 += (127 - charAt) >>> 31;
                i3++;
            } else {
                int length2 = charSequence.length();
                while (i3 < length2) {
                    char charAt2 = charSequence.charAt(i3);
                    if (charAt2 < 2048) {
                        i2 += (127 - charAt2) >>> 31;
                    } else {
                        i2 += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i3) < 65536) {
                                throw new zzfiv(i3, length2);
                            }
                            i3++;
                        }
                    }
                    i3++;
                }
                i = i4 + i2;
            }
        }
        if (i >= length) {
            return i;
        }
        throw new IllegalArgumentException(new StringBuilder(54).append("UTF-8 length does not fit in int: ").append(((long) i) + 4294967296L).toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m12768(CharSequence charSequence, byte[] bArr, int i, int i2) {
        return f10517.m12771(charSequence, bArr, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m12769(byte[] bArr, int i, int i2) {
        return f10517.m12770(0, bArr, i, i2) == 0;
    }
}
