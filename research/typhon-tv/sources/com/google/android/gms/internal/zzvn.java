package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzvn extends zzev implements zzvm {
    public zzvn() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 2:
                String r1 = m13586();
                parcel2.writeNoException();
                parcel2.writeString(r1);
                return true;
            case 3:
                List r12 = m13581();
                parcel2.writeNoException();
                parcel2.writeList(r12);
                return true;
            case 4:
                String r13 = m13584();
                parcel2.writeNoException();
                parcel2.writeString(r13);
                return true;
            case 5:
                zzpq r14 = m13583();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r14);
                return true;
            case 6:
                String r15 = m13580();
                parcel2.writeNoException();
                parcel2.writeString(r15);
                return true;
            case 7:
                String r16 = m13571();
                parcel2.writeNoException();
                parcel2.writeString(r16);
                return true;
            case 8:
                m13572();
                parcel2.writeNoException();
                return true;
            case 9:
                m13587(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 10:
                m13582(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 11:
                boolean r17 = m13573();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r17);
                return true;
            case 12:
                boolean r18 = m13577();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r18);
                return true;
            case 13:
                Bundle r19 = m13578();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r19);
                return true;
            case 14:
                m13585(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 15:
                IObjectWrapper r110 = m13579();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r110);
                return true;
            case 16:
                zzll r111 = m13576();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r111);
                return true;
            case 19:
                zzpm r112 = m13574();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r112);
                return true;
            case 20:
                IObjectWrapper r113 = m13575();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r113);
                return true;
            case 21:
                IObjectWrapper r114 = m13588();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r114);
                return true;
            default:
                return false;
        }
    }
}
