package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzaba implements Parcelable.Creator<zzaaz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r2 = zzbfn.m10169(parcel);
        boolean z = false;
        ArrayList<String> arrayList = null;
        while (parcel.dataPosition() < r2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 3:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r2);
        return new zzaaz(z, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaaz[i];
    }
}
