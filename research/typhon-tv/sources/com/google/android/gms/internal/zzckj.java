package com.google.android.gms.internal;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class zzckj implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9565;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzckg f9566;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AtomicReference f9567;

    zzckj(zzckg zzckg, AtomicReference atomicReference, zzcgi zzcgi) {
        this.f9566 = zzckg;
        this.f9567 = atomicReference;
        this.f9565 = zzcgi;
    }

    public final void run() {
        synchronized (this.f9567) {
            try {
                zzche r0 = this.f9566.f9558;
                if (r0 == null) {
                    this.f9566.m11096().m10832().m10849("Failed to get app instance id");
                    this.f9567.notify();
                    return;
                }
                this.f9567.set(r0.m10676(this.f9565));
                String str = (String) this.f9567.get();
                if (str != null) {
                    this.f9566.m11091().m11173(str);
                    this.f9566.m11098().f9313.m10909(str);
                }
                this.f9566.m11223();
                this.f9567.notify();
            } catch (RemoteException e) {
                this.f9566.m11096().m10832().m10850("Failed to get app instance id", e);
                this.f9567.notify();
            } catch (Throwable th) {
                this.f9567.notify();
                throw th;
            }
        }
    }
}
