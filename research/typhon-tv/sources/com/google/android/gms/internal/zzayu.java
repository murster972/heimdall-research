package com.google.android.gms.internal;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.zzd;
import com.google.android.gms.cast.framework.zzab;
import com.google.android.gms.cast.framework.zzh;
import com.google.android.gms.cast.framework.zzj;
import com.google.android.gms.cast.framework.zzl;
import com.google.android.gms.cast.framework.zzr;
import com.google.android.gms.cast.framework.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.Map;

public final class zzayu {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f8408 = new zzbcy("CastDynamiteModule");

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzd m9760(Service service, IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, CastMediaOptions castMediaOptions) {
        try {
            return m9765(service.getApplicationContext()).m9769(zzn.m9306(service), iObjectWrapper, iObjectWrapper2, castMediaOptions);
        } catch (RemoteException e) {
            f8408.m10091(e, "Unable to call %s on %s.", "newMediaNotificationServiceImpl", zzayx.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzj m9761(Context context, CastOptions castOptions, zzayz zzayz, Map<String, IBinder> map) {
        try {
            return m9765(context).m9770(zzn.m9306(context.getApplicationContext()), castOptions, zzayz, (Map) map);
        } catch (RemoteException e) {
            f8408.m10091(e, "Unable to call %s on %s.", "newCastContextImpl", zzayx.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzl m9762(Context context, CastOptions castOptions, IObjectWrapper iObjectWrapper, zzh zzh) {
        try {
            return m9765(context).m9771(castOptions, iObjectWrapper, zzh);
        } catch (RemoteException e) {
            f8408.m10091(e, "Unable to call %s on %s.", "newCastSessionImpl", zzayx.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzr m9763(Service service, IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) {
        try {
            return m9765(service.getApplicationContext()).m9772(zzn.m9306(service), iObjectWrapper, iObjectWrapper2);
        } catch (RemoteException e) {
            f8408.m10091(e, "Unable to call %s on %s.", "newReconnectionServiceImpl", zzayx.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzt m9764(Context context, String str, String str2, zzab zzab) {
        try {
            return m9765(context).m9773(str, str2, zzab);
        } catch (RemoteException e) {
            f8408.m10091(e, "Unable to call %s on %s.", "newSessionImpl", zzayx.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzayx m9765(Context context) {
        try {
            IBinder r1 = DynamiteModule.m9319(context, DynamiteModule.f7976, "com.google.android.gms.cast.framework.dynamite").m9324("com.google.android.gms.cast.framework.internal.CastDynamiteModuleImpl");
            if (r1 == null) {
                return null;
            }
            IInterface queryLocalInterface = r1.queryLocalInterface("com.google.android.gms.cast.framework.internal.ICastDynamiteModule");
            return queryLocalInterface instanceof zzayx ? (zzayx) queryLocalInterface : new zzayy(r1);
        } catch (DynamiteModule.zzc e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzazu m9766(Context context, AsyncTask<Uri, Long, Bitmap> asyncTask, zzazw zzazw, int i, int i2, boolean z, long j, int i3, int i4, int i5) {
        try {
            return m9765(context.getApplicationContext()).m9774(zzn.m9306(asyncTask), zzazw, i, i2, z, 2097152, 5, 333, 10000);
        } catch (RemoteException e) {
            f8408.m10091(e, "Unable to call %s on %s.", "newFetchBitmapTaskImpl", zzayx.class.getSimpleName());
            return null;
        }
    }
}
