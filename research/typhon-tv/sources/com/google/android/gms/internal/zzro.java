package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

public final class zzro extends zzrg {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final OnPublisherAdViewLoadedListener f10854;

    public zzro(OnPublisherAdViewLoadedListener onPublisherAdViewLoadedListener) {
        this.f10854 = onPublisherAdViewLoadedListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13381(zzks zzks, IObjectWrapper iObjectWrapper) {
        if (zzks != null && iObjectWrapper != null) {
            PublisherAdView publisherAdView = new PublisherAdView((Context) zzn.m9307(iObjectWrapper));
            try {
                if (zzks.zzce() instanceof zzjg) {
                    zzjg zzjg = (zzjg) zzks.zzce();
                    publisherAdView.setAdListener(zzjg != null ? zzjg.m5438() : null);
                }
            } catch (RemoteException e) {
                zzakb.m4796("Failed to get ad listener.", e);
            }
            try {
                if (zzks.zzcd() instanceof zzjp) {
                    zzjp zzjp = (zzjp) zzks.zzcd();
                    publisherAdView.setAppEventListener(zzjp != null ? zzjp.m5459() : null);
                }
            } catch (RemoteException e2) {
                zzakb.m4796("Failed to get app event listener.", e2);
            }
            zzajr.f4285.post(new zzrp(this, publisherAdView, zzks));
        }
    }
}
