package com.google.android.gms.internal;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzid {
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public zzio f4732;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f4733 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private Context f4734;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public zzik f4735;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Runnable f4736 = new zzie(this);

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5410() {
        /*
            r6 = this;
            java.lang.Object r1 = r6.f4733
            monitor-enter(r1)
            android.content.Context r0 = r6.f4734     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x000b
            com.google.android.gms.internal.zzik r0 = r6.f4735     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
        L_0x000c:
            return
        L_0x000d:
            com.google.android.gms.internal.zzig r0 = new com.google.android.gms.internal.zzig     // Catch:{ all -> 0x002f }
            r0.<init>(r6)     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.zzih r2 = new com.google.android.gms.internal.zzih     // Catch:{ all -> 0x002f }
            r2.<init>(r6)     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.zzik r3 = new com.google.android.gms.internal.zzik     // Catch:{ all -> 0x002f }
            android.content.Context r4 = r6.f4734     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.zzajf r5 = com.google.android.gms.ads.internal.zzbs.zzew()     // Catch:{ all -> 0x002f }
            android.os.Looper r5 = r5.m4718()     // Catch:{ all -> 0x002f }
            r3.<init>(r4, r5, r0, r2)     // Catch:{ all -> 0x002f }
            r6.f4735 = r3     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.zzik r0 = r6.f4735     // Catch:{ all -> 0x002f }
            r0.m9167()     // Catch:{ all -> 0x002f }
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            goto L_0x000c
        L_0x002f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzid.m5410():void");
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5414() {
        synchronized (this.f4733) {
            if (this.f4735 != null) {
                if (this.f4735.m9161() || this.f4735.m9162()) {
                    this.f4735.m9160();
                }
                this.f4735 = null;
                this.f4732 = null;
                Binder.flushPendingCommands();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzii m5418(zzil zzil) {
        zzii zzii;
        synchronized (this.f4733) {
            if (this.f4732 == null) {
                zzii = new zzii();
            } else {
                try {
                    zzii = this.f4732.m13008(zzil);
                } catch (RemoteException e) {
                    zzagf.m4793("Unable to call into cache service.", e);
                    zzii = new zzii();
                }
            }
        }
        return zzii;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5419() {
        if (((Boolean) zzkb.m5481().m5595(zznh.f5018)).booleanValue()) {
            synchronized (this.f4733) {
                m5410();
                zzbs.zzei();
                zzahn.f4212.removeCallbacks(this.f4736);
                zzbs.zzei();
                zzahn.f4212.postDelayed(this.f4736, ((Long) zzkb.m5481().m5595(zznh.f5019)).longValue());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5420(android.content.Context r4) {
        /*
            r3 = this;
            if (r4 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.Object r1 = r3.f4733
            monitor-enter(r1)
            android.content.Context r0 = r3.f4734     // Catch:{ all -> 0x000c }
            if (r0 == 0) goto L_0x000f
            monitor-exit(r1)     // Catch:{ all -> 0x000c }
            goto L_0x0002
        L_0x000c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x000c }
            throw r0
        L_0x000f:
            android.content.Context r0 = r4.getApplicationContext()     // Catch:{ all -> 0x000c }
            r3.f4734 = r0     // Catch:{ all -> 0x000c }
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f5017     // Catch:{ all -> 0x000c }
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x000c }
            java.lang.Object r0 = r2.m5595(r0)     // Catch:{ all -> 0x000c }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x000c }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x000c }
            if (r0 == 0) goto L_0x002c
            r3.m5410()     // Catch:{ all -> 0x000c }
        L_0x002a:
            monitor-exit(r1)     // Catch:{ all -> 0x000c }
            goto L_0x0002
        L_0x002c:
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f5016     // Catch:{ all -> 0x000c }
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x000c }
            java.lang.Object r0 = r2.m5595(r0)     // Catch:{ all -> 0x000c }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x000c }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x000c }
            if (r0 == 0) goto L_0x002a
            com.google.android.gms.internal.zzif r0 = new com.google.android.gms.internal.zzif     // Catch:{ all -> 0x000c }
            r0.<init>(r3)     // Catch:{ all -> 0x000c }
            com.google.android.gms.internal.zzhg r2 = com.google.android.gms.ads.internal.zzbs.zzel()     // Catch:{ all -> 0x000c }
            r2.m5380((com.google.android.gms.internal.zzhj) r0)     // Catch:{ all -> 0x000c }
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzid.m5420(android.content.Context):void");
    }
}
