package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

@zzzv
public final class zzakd extends zzbfm {
    public static final Parcelable.Creator<zzakd> CREATOR = new zzake();

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f4293;

    /* renamed from: 靐  reason: contains not printable characters */
    public int f4294;

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f4295;

    /* renamed from: 齉  reason: contains not printable characters */
    public int f4296;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f4297;

    public zzakd(int i, int i2, boolean z) {
        this(i, i2, z, false, false);
    }

    public zzakd(int i, int i2, boolean z, boolean z2) {
        this(11910000, i2, true, false, z2);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private zzakd(int r7, int r8, boolean r9, boolean r10, boolean r11) {
        /*
            r6 = this;
            java.lang.String r1 = "afma-sdk-a-v"
            if (r9 == 0) goto L_0x004b
            java.lang.String r0 = "0"
        L_0x0008:
            java.lang.String r2 = java.lang.String.valueOf(r1)
            int r2 = r2.length()
            int r2 = r2 + 24
            java.lang.String r3 = java.lang.String.valueOf(r0)
            int r3 = r3.length()
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = "."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = "."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = r0.toString()
            r0 = r6
            r2 = r7
            r3 = r8
            r4 = r9
            r5 = r11
            r0.<init>((java.lang.String) r1, (int) r2, (int) r3, (boolean) r4, (boolean) r5)
            return
        L_0x004b:
            java.lang.String r0 = "1"
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzakd.<init>(int, int, boolean, boolean, boolean):void");
    }

    zzakd(String str, int i, int i2, boolean z, boolean z2) {
        this.f4297 = str;
        this.f4294 = i;
        this.f4296 = i2;
        this.f4295 = z;
        this.f4293 = z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzakd m4800() {
        return new zzakd(11910208, 11910208, true);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f4297, false);
        zzbfp.m10185(parcel, 3, this.f4294);
        zzbfp.m10185(parcel, 4, this.f4296);
        zzbfp.m10195(parcel, 5, this.f4295);
        zzbfp.m10195(parcel, 6, this.f4293);
        zzbfp.m10182(parcel, r0);
    }
}
