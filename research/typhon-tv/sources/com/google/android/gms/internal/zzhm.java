package com.google.android.gms.internal;

import android.webkit.ValueCallback;
import android.webkit.WebView;

final class zzhm implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private ValueCallback<String> f10697 = new zzhn(this);

    /* renamed from: 靐  reason: contains not printable characters */
    final /* synthetic */ WebView f10698;

    /* renamed from: 麤  reason: contains not printable characters */
    final /* synthetic */ zzhk f10699;

    /* renamed from: 齉  reason: contains not printable characters */
    final /* synthetic */ boolean f10700;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzhe f10701;

    zzhm(zzhk zzhk, zzhe zzhe, WebView webView, boolean z) {
        this.f10699 = zzhk;
        this.f10701 = zzhe;
        this.f10698 = webView;
        this.f10700 = z;
    }

    public final void run() {
        if (this.f10698.getSettings().getJavaScriptEnabled()) {
            try {
                this.f10698.evaluateJavascript("(function() { return  {text:document.body.innerText}})();", this.f10697);
            } catch (Throwable th) {
                this.f10697.onReceiveValue("");
            }
        }
    }
}
