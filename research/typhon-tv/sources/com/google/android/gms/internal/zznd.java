package com.google.android.gms.internal;

import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONObject;

@zzzv
public final class zznd {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Collection<zzmx<String>> f4884 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Collection<zzmx<String>> f4885 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Collection<zzmx<?>> f4886 = new ArrayList();

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<String> m5588() {
        List<String> r1 = m5591();
        for (zzmx<String> r0 : this.f4885) {
            String str = (String) zzkb.m5481().m5595(r0);
            if (str != null) {
                r1.add(str);
            }
        }
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5589(zzmx<String> zzmx) {
        this.f4884.add(zzmx);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5590(zzmx<String> zzmx) {
        this.f4885.add(zzmx);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<String> m5591() {
        ArrayList arrayList = new ArrayList();
        for (zzmx<String> r0 : this.f4884) {
            String str = (String) zzkb.m5481().m5595(r0);
            if (str != null) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5592(SharedPreferences.Editor editor, int i, JSONObject jSONObject) {
        for (zzmx next : this.f4886) {
            if (next.m5583() == 1) {
                next.m5587(editor, next.m5585(jSONObject));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5593(zzmx zzmx) {
        this.f4886.add(zzmx);
    }
}
