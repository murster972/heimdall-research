package com.google.android.gms.internal;

import android.view.View;
import android.view.ViewTreeObserver;

@zzzv
public final class zzaln {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4826(View view, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        new zzalo(view, onGlobalLayoutListener).m4835();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4827(View view, ViewTreeObserver.OnScrollChangedListener onScrollChangedListener) {
        new zzalp(view, onScrollChangedListener).m4835();
    }
}
