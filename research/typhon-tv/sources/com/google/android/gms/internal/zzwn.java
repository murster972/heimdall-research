package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzl;
import com.google.android.gms.ads.internal.zzbs;

final class zzwn implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzwl f10949;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AdOverlayInfoParcel f10950;

    zzwn(zzwl zzwl, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.f10949 = zzwl;
        this.f10950 = adOverlayInfoParcel;
    }

    public final void run() {
        zzbs.zzeg();
        zzl.zza(this.f10949.f5505, this.f10950, true);
    }
}
