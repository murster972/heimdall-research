package com.google.android.gms.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class zzckq implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzckg f9587;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9588;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f9589;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ String f9590;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f9591;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AtomicReference f9592;

    zzckq(zzckg zzckg, AtomicReference atomicReference, String str, String str2, String str3, zzcgi zzcgi) {
        this.f9587 = zzckg;
        this.f9592 = atomicReference;
        this.f9589 = str;
        this.f9591 = str2;
        this.f9590 = str3;
        this.f9588 = zzcgi;
    }

    public final void run() {
        synchronized (this.f9592) {
            try {
                zzche r0 = this.f9587.f9558;
                if (r0 == null) {
                    this.f9587.m11096().m10832().m10852("Failed to get conditional properties", zzchm.m10812(this.f9589), this.f9591, this.f9590);
                    this.f9592.set(Collections.emptyList());
                    this.f9592.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f9589)) {
                    this.f9592.set(r0.m10678(this.f9591, this.f9590, this.f9588));
                } else {
                    this.f9592.set(r0.m10679(this.f9589, this.f9591, this.f9590));
                }
                this.f9587.m11223();
                this.f9592.notify();
            } catch (RemoteException e) {
                this.f9587.m11096().m10832().m10852("Failed to get conditional properties", zzchm.m10812(this.f9589), this.f9591, e);
                this.f9592.set(Collections.emptyList());
                this.f9592.notify();
            } catch (Throwable th) {
                this.f9592.notify();
                throw th;
            }
        }
    }
}
