package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbf extends zzfjm<zzbf> {

    /* renamed from: 靐  reason: contains not printable characters */
    public byte[] f8733 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private Integer f8734;

    /* renamed from: 齉  reason: contains not printable characters */
    public Integer f8735;

    /* renamed from: 龘  reason: contains not printable characters */
    public byte[][] f8736 = zzfjv.f10553;

    public zzbf() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzbf m10148(com.google.android.gms.internal.zzfjj r8) throws java.io.IOException {
        /*
            r7 = this;
            r1 = 0
        L_0x0001:
            int r0 = r8.m12800()
            switch(r0) {
                case 0: goto L_0x000e;
                case 10: goto L_0x000f;
                case 18: goto L_0x0042;
                case 24: goto L_0x0049;
                case 32: goto L_0x007f;
                default: goto L_0x0008;
            }
        L_0x0008:
            boolean r0 = super.m12843(r8, r0)
            if (r0 != 0) goto L_0x0001
        L_0x000e:
            return r7
        L_0x000f:
            r0 = 10
            int r2 = com.google.android.gms.internal.zzfjv.m12883(r8, r0)
            byte[][] r0 = r7.f8736
            if (r0 != 0) goto L_0x0035
            r0 = r1
        L_0x001a:
            int r2 = r2 + r0
            byte[][] r2 = new byte[r2][]
            if (r0 == 0) goto L_0x0024
            byte[][] r3 = r7.f8736
            java.lang.System.arraycopy(r3, r1, r2, r1, r0)
        L_0x0024:
            int r3 = r2.length
            int r3 = r3 + -1
            if (r0 >= r3) goto L_0x0039
            byte[] r3 = r8.m12784()
            r2[r0] = r3
            r8.m12800()
            int r0 = r0 + 1
            goto L_0x0024
        L_0x0035:
            byte[][] r0 = r7.f8736
            int r0 = r0.length
            goto L_0x001a
        L_0x0039:
            byte[] r3 = r8.m12784()
            r2[r0] = r3
            r7.f8736 = r2
            goto L_0x0001
        L_0x0042:
            byte[] r0 = r8.m12784()
            r7.f8733 = r0
            goto L_0x0001
        L_0x0049:
            int r2 = r8.m12787()
            int r3 = r8.m12785()     // Catch:{ IllegalArgumentException -> 0x0070 }
            switch(r3) {
                case 0: goto L_0x0078;
                case 1: goto L_0x0078;
                default: goto L_0x0054;
            }     // Catch:{ IllegalArgumentException -> 0x0070 }
        L_0x0054:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x0070 }
            r5 = 41
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0070 }
            r6.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x0070 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ IllegalArgumentException -> 0x0070 }
            java.lang.String r5 = " is not a valid enum ProtoName"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IllegalArgumentException -> 0x0070 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x0070 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x0070 }
            throw r4     // Catch:{ IllegalArgumentException -> 0x0070 }
        L_0x0070:
            r3 = move-exception
            r8.m12792(r2)
            r7.m12843(r8, r0)
            goto L_0x0001
        L_0x0078:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x0070 }
            r7.f8734 = r3     // Catch:{ IllegalArgumentException -> 0x0070 }
            goto L_0x0001
        L_0x007f:
            int r2 = r8.m12787()
            int r3 = r8.m12785()     // Catch:{ IllegalArgumentException -> 0x00a6 }
            switch(r3) {
                case 0: goto L_0x00af;
                case 1: goto L_0x00af;
                case 2: goto L_0x00af;
                default: goto L_0x008a;
            }     // Catch:{ IllegalArgumentException -> 0x00a6 }
        L_0x008a:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00a6 }
            r5 = 48
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00a6 }
            r6.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x00a6 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ IllegalArgumentException -> 0x00a6 }
            java.lang.String r5 = " is not a valid enum EncryptionMethod"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IllegalArgumentException -> 0x00a6 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x00a6 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x00a6 }
            throw r4     // Catch:{ IllegalArgumentException -> 0x00a6 }
        L_0x00a6:
            r3 = move-exception
            r8.m12792(r2)
            r7.m12843(r8, r0)
            goto L_0x0001
        L_0x00af:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x00a6 }
            r7.f8735 = r3     // Catch:{ IllegalArgumentException -> 0x00a6 }
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbf.m10148(com.google.android.gms.internal.zzfjj):com.google.android.gms.internal.zzbf");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10147() {
        int i;
        int i2;
        int r4 = super.m12841();
        if (this.f8736 == null || this.f8736.length <= 0) {
            i = r4;
        } else {
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (i3 < this.f8736.length) {
                byte[] bArr = this.f8736[i3];
                if (bArr != null) {
                    i5++;
                    i2 = zzfjk.m12810(bArr) + i4;
                } else {
                    i2 = i4;
                }
                i3++;
                i4 = i2;
            }
            i = r4 + i4 + (i5 * 1);
        }
        if (this.f8733 != null) {
            i += zzfjk.m12809(2, this.f8733);
        }
        if (this.f8734 != null) {
            i += zzfjk.m12806(3, this.f8734.intValue());
        }
        return this.f8735 != null ? i + zzfjk.m12806(4, this.f8735.intValue()) : i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10149(zzfjk zzfjk) throws IOException {
        if (this.f8736 != null && this.f8736.length > 0) {
            for (byte[] bArr : this.f8736) {
                if (bArr != null) {
                    zzfjk.m12837(1, bArr);
                }
            }
        }
        if (this.f8733 != null) {
            zzfjk.m12837(2, this.f8733);
        }
        if (this.f8734 != null) {
            zzfjk.m12832(3, this.f8734.intValue());
        }
        if (this.f8735 != null) {
            zzfjk.m12832(4, this.f8735.intValue());
        }
        super.m12842(zzfjk);
    }
}
