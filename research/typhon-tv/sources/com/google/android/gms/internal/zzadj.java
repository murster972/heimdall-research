package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzadj extends zzeu implements zzadh {
    zzadj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardItem");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9467() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m9468() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }
}
