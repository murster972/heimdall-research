package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

public interface zzafn {
    /* renamed from: 龘  reason: contains not printable characters */
    zzakv<AdvertisingIdClient.Info> m9570(Context context);

    /* renamed from: 龘  reason: contains not printable characters */
    zzakv<String> m9571(String str, PackageInfo packageInfo);
}
