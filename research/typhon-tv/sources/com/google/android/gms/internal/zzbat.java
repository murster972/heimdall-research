package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbat extends UIController {

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8591;

    public zzbat(View view) {
        this.f8591 = view;
        this.f8591.setEnabled(false);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9923() {
        RemoteMediaClient r0 = m8226();
        if (r0 == null || !r0.m4143() || r0.m4147() || r0.m4144()) {
            this.f8591.setEnabled(false);
        } else {
            this.f8591.setEnabled(true);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9924() {
        this.f8591.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9925() {
        this.f8591.setEnabled(false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9926() {
        m9923();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9927(CastSession castSession) {
        super.m8227(castSession);
        m9923();
    }
}
