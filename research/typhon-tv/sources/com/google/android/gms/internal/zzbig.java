package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

public final class zzbig implements Parcelable.Creator<zzbif> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        int i = 0;
        long j = 0;
        DataHolder dataHolder = null;
        DataHolder dataHolder2 = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    dataHolder2 = (DataHolder) zzbfn.m10171(parcel, readInt, DataHolder.CREATOR);
                    break;
                case 4:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 5:
                    dataHolder = (DataHolder) zzbfn.m10171(parcel, readInt, DataHolder.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzbif(i, dataHolder2, j, dataHolder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbif[i];
    }
}
