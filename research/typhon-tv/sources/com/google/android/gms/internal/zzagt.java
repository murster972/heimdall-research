package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagt extends zzahf {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8178;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagt(Context context) {
        super((zzagi) null);
        this.f8178 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9592() {
        SharedPreferences.Editor edit = this.f8178.getSharedPreferences("admob", 0).edit();
        edit.remove("native_advanced_settings");
        edit.apply();
    }
}
