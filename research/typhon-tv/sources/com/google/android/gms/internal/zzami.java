package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import java.util.concurrent.TimeUnit;

@zzzv
@TargetApi(14)
public final class zzami {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f4360;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4361 = true;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f4362 = TimeUnit.MILLISECONDS.toNanos(((Long) zzkb.m5481().m5595(zznh.f5022)).longValue());

    zzami() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4897() {
        this.f4361 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4898(SurfaceTexture surfaceTexture, zzama zzama) {
        if (zzama != null) {
            long timestamp = surfaceTexture.getTimestamp();
            if (this.f4361 || Math.abs(timestamp - this.f4360) >= this.f4362) {
                this.f4361 = false;
                this.f4360 = timestamp;
                zzahn.f4212.post(new zzamj(this, zzama));
            }
        }
    }
}
