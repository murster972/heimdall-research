package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

public interface zzdpw<P> {
    /* renamed from: 靐  reason: contains not printable characters */
    zzfhe m11653(zzfes zzfes) throws GeneralSecurityException;

    /* renamed from: 靐  reason: contains not printable characters */
    zzfhe m11654(zzfhe zzfhe) throws GeneralSecurityException;

    /* renamed from: 齉  reason: contains not printable characters */
    zzdsy m11655(zzfes zzfes) throws GeneralSecurityException;

    /* renamed from: 龘  reason: contains not printable characters */
    P m11656(zzfes zzfes) throws GeneralSecurityException;

    /* renamed from: 龘  reason: contains not printable characters */
    P m11657(zzfhe zzfhe) throws GeneralSecurityException;
}
