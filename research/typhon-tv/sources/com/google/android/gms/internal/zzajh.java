package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzajh extends zzagb {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f4256;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzakc f4257;

    public zzajh(Context context, String str, String str2) {
        this(str2, zzbs.zzei().m4632(context, str));
    }

    private zzajh(String str, String str2) {
        this.f4257 = new zzakc(str2);
        this.f4256 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4722() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4723() {
        this.f4257.m4799(this.f4256);
    }
}
