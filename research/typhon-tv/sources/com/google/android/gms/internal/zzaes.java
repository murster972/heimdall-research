package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.common.zzf;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzaes implements zzafb {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzaey f4057;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Object f4058 = new Object();

    /* renamed from: ʽ  reason: contains not printable characters */
    private HashSet<String> f4059 = new HashSet<>();

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f4060 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f4061 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f4062 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzafd f4063;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfjw f4064;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f4065;

    /* renamed from: 齉  reason: contains not printable characters */
    private final LinkedHashMap<String, zzfke> f4066;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f4067;

    public zzaes(Context context, zzakd zzakd, zzaey zzaey, String str, zzafd zzafd) {
        zzbq.m9121(zzaey, (Object) "SafeBrowsing config is not present.");
        this.f4065 = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.f4066 = new LinkedHashMap<>();
        this.f4063 = zzafd;
        this.f4057 = zzaey;
        for (String lowerCase : this.f4057.f4071) {
            this.f4059.add(lowerCase.toLowerCase(Locale.ENGLISH));
        }
        this.f4059.remove("cookie".toLowerCase(Locale.ENGLISH));
        zzfjw zzfjw = new zzfjw();
        zzfjw.f10577 = 8;
        zzfjw.f10574 = str;
        zzfjw.f10576 = str;
        zzfjw.f10575 = new zzfjx();
        zzfjw.f10575.f10580 = this.f4057.f4075;
        zzfkf zzfkf = new zzfkf();
        zzfkf.f10616 = zzakd.f4297;
        zzfkf.f10615 = Boolean.valueOf(zzbhf.m10231(this.f4065).m10227());
        zzf.m4219();
        long r2 = (long) zzf.m4218(this.f4065);
        if (r2 > 0) {
            zzfkf.f10614 = Long.valueOf(r2);
        }
        zzfjw.f10566 = zzfkf;
        this.f4064 = zzfjw;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzfke m4390(String str) {
        zzfke zzfke;
        synchronized (this.f4058) {
            zzfke = this.f4066.get(str);
        }
        return zzfke;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4392() {
        boolean z = true;
        if ((!this.f4067 || !this.f4057.f4070) && ((!this.f4062 || !this.f4057.f4069) && (this.f4067 || !this.f4057.f4073))) {
            z = false;
        }
        if (z) {
            synchronized (this.f4058) {
                this.f4064.f10573 = new zzfke[this.f4066.size()];
                this.f4066.values().toArray(this.f4064.f10573);
                if (zzafa.m4405()) {
                    String str = this.f4064.f10574;
                    String str2 = this.f4064.f10564;
                    StringBuilder sb = new StringBuilder(new StringBuilder(String.valueOf(str).length() + 53 + String.valueOf(str2).length()).append("Sending SB report\n  url: ").append(str).append("\n  clickUrl: ").append(str2).append("\n  resources: \n").toString());
                    for (zzfke zzfke : this.f4064.f10573) {
                        sb.append("    [");
                        sb.append(zzfke.f10609.length);
                        sb.append("] ");
                        sb.append(zzfke.f10610);
                    }
                    zzafa.m4404(sb.toString());
                }
                zzakv<String> r0 = new zzaiv(this.f4065).m4712(1, this.f4057.f4072, (Map<String, String>) null, zzfjs.m12871((zzfjs) this.f4064));
                if (zzafa.m4405()) {
                    r0.m9670(new zzaev(this), zzahh.f4211);
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4393() {
        return zzq.m9268() && this.f4057.f4074 && !this.f4061;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4394() {
        synchronized (this.f4058) {
            zzakv<Map<String, String>> r0 = this.f4063.m9568(this.f4065, this.f4066.keySet());
            r0.m9670(new zzaeu(this, r0), zzahh.f4211);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4395() {
        this.f4060 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaey m4396() {
        return this.f4057;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4397(View view) {
        if (this.f4057.f4074 && !this.f4061) {
            zzbs.zzei();
            Bitmap r0 = zzahn.m4577(view);
            if (r0 == null) {
                zzafa.m4404("Failed to capture the webview bitmap.");
                return;
            }
            this.f4061 = true;
            zzahn.m4583((Runnable) new zzaet(this, r0));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4398(String str) {
        synchronized (this.f4058) {
            this.f4064.f10564 = str;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4399(java.lang.String r9, java.util.Map<java.lang.String, java.lang.String> r10, int r11) {
        /*
            r8 = this;
            r1 = 3
            java.lang.Object r2 = r8.f4058
            monitor-enter(r2)
            if (r11 != r1) goto L_0x0009
            r0 = 1
            r8.f4062 = r0     // Catch:{ all -> 0x00af }
        L_0x0009:
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfke> r0 = r8.f4066     // Catch:{ all -> 0x00af }
            boolean r0 = r0.containsKey(r9)     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x0023
            if (r11 != r1) goto L_0x0021
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfke> r0 = r8.f4066     // Catch:{ all -> 0x00af }
            java.lang.Object r0 = r0.get(r9)     // Catch:{ all -> 0x00af }
            com.google.android.gms.internal.zzfke r0 = (com.google.android.gms.internal.zzfke) r0     // Catch:{ all -> 0x00af }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x00af }
            r0.f10611 = r1     // Catch:{ all -> 0x00af }
        L_0x0021:
            monitor-exit(r2)     // Catch:{ all -> 0x00af }
        L_0x0022:
            return
        L_0x0023:
            com.google.android.gms.internal.zzfke r3 = new com.google.android.gms.internal.zzfke     // Catch:{ all -> 0x00af }
            r3.<init>()     // Catch:{ all -> 0x00af }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x00af }
            r3.f10611 = r0     // Catch:{ all -> 0x00af }
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfke> r0 = r8.f4066     // Catch:{ all -> 0x00af }
            int r0 = r0.size()     // Catch:{ all -> 0x00af }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00af }
            r3.f10613 = r0     // Catch:{ all -> 0x00af }
            r3.f10610 = r9     // Catch:{ all -> 0x00af }
            com.google.android.gms.internal.zzfjz r0 = new com.google.android.gms.internal.zzfjz     // Catch:{ all -> 0x00af }
            r0.<init>()     // Catch:{ all -> 0x00af }
            r3.f10612 = r0     // Catch:{ all -> 0x00af }
            java.util.HashSet<java.lang.String> r0 = r8.f4059     // Catch:{ all -> 0x00af }
            int r0 = r0.size()     // Catch:{ all -> 0x00af }
            if (r0 <= 0) goto L_0x00c7
            if (r10 == 0) goto L_0x00c7
            java.util.LinkedList r4 = new java.util.LinkedList     // Catch:{ all -> 0x00af }
            r4.<init>()     // Catch:{ all -> 0x00af }
            java.util.Set r0 = r10.entrySet()     // Catch:{ all -> 0x00af }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x00af }
        L_0x005a:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x00ba
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x00af }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x00af }
            java.lang.Object r1 = r0.getKey()     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            if (r1 == 0) goto L_0x00b2
            java.lang.Object r1 = r0.getKey()     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
        L_0x0072:
            java.lang.Object r6 = r0.getValue()     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            if (r6 == 0) goto L_0x00b6
            java.lang.Object r0 = r0.getValue()     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
        L_0x007e:
            java.util.Locale r6 = java.util.Locale.ENGLISH     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            java.lang.String r6 = r1.toLowerCase(r6)     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            java.util.HashSet<java.lang.String> r7 = r8.f4059     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            boolean r6 = r7.contains(r6)     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            if (r6 == 0) goto L_0x005a
            com.google.android.gms.internal.zzfjy r6 = new com.google.android.gms.internal.zzfjy     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            r6.<init>()     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            java.lang.String r7 = "UTF-8"
            byte[] r1 = r1.getBytes(r7)     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            r6.f10583 = r1     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            r6.f10582 = r0     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            r4.add(r6)     // Catch:{ UnsupportedEncodingException -> 0x00a7 }
            goto L_0x005a
        L_0x00a7:
            r0 = move-exception
            java.lang.String r0 = "Cannot convert string to bytes, skip header."
            com.google.android.gms.internal.zzafa.m4404(r0)     // Catch:{ all -> 0x00af }
            goto L_0x005a
        L_0x00af:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00af }
            throw r0
        L_0x00b2:
            java.lang.String r1 = ""
            goto L_0x0072
        L_0x00b6:
            java.lang.String r0 = ""
            goto L_0x007e
        L_0x00ba:
            int r0 = r4.size()     // Catch:{ all -> 0x00af }
            com.google.android.gms.internal.zzfjy[] r0 = new com.google.android.gms.internal.zzfjy[r0]     // Catch:{ all -> 0x00af }
            r4.toArray(r0)     // Catch:{ all -> 0x00af }
            com.google.android.gms.internal.zzfjz r1 = r3.f10612     // Catch:{ all -> 0x00af }
            r1.f10588 = r0     // Catch:{ all -> 0x00af }
        L_0x00c7:
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfke> r0 = r8.f4066     // Catch:{ all -> 0x00af }
            r0.put(r9, r3)     // Catch:{ all -> 0x00af }
            monitor-exit(r2)     // Catch:{ all -> 0x00af }
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaes.m4399(java.lang.String, java.util.Map, int):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4400(Map<String, String> map) throws JSONException {
        if (map != null) {
            for (String next : map.keySet()) {
                JSONArray optJSONArray = new JSONObject(map.get(next)).optJSONArray("matches");
                if (optJSONArray != null) {
                    synchronized (this.f4058) {
                        int length = optJSONArray.length();
                        zzfke r6 = m4390(next);
                        if (r6 == null) {
                            String valueOf = String.valueOf(next);
                            zzafa.m4404(valueOf.length() != 0 ? "Cannot find the corresponding resource object for ".concat(valueOf) : new String("Cannot find the corresponding resource object for "));
                        } else {
                            r6.f10609 = new String[length];
                            for (int i = 0; i < length; i++) {
                                r6.f10609[i] = optJSONArray.getJSONObject(i).getString("threat_type");
                            }
                            this.f4067 = (length > 0) | this.f4067;
                        }
                    }
                }
            }
        }
    }
}
