package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

@zzzv
@TargetApi(21)
public final class zzaot extends zzaos {
    public zzaot(zzanh zzanh, boolean z) {
        super(zzanh, z);
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        if (webResourceRequest == null || webResourceRequest.getUrl() == null) {
            return null;
        }
        return m5217(webView, webResourceRequest.getUrl().toString(), webResourceRequest.getRequestHeaders());
    }
}
