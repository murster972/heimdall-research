package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzdq extends zzbt<Integer, Long> {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Long f9917;

    /* renamed from: ʼ  reason: contains not printable characters */
    public Long f9918;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Long f9919;

    /* renamed from: ʾ  reason: contains not printable characters */
    public Long f9920;

    /* renamed from: ˈ  reason: contains not printable characters */
    public Long f9921;

    /* renamed from: ˑ  reason: contains not printable characters */
    public Long f9922;

    /* renamed from: ٴ  reason: contains not printable characters */
    public Long f9923;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Long f9924;

    /* renamed from: 连任  reason: contains not printable characters */
    public Long f9925;

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f9926;

    /* renamed from: 麤  reason: contains not printable characters */
    public Long f9927;

    /* renamed from: 齉  reason: contains not printable characters */
    public Long f9928;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f9929;

    public zzdq() {
    }

    public zzdq(String str) {
        m10294(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<Integer, Long> m11663() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.f9929);
        hashMap.put(1, this.f9926);
        hashMap.put(2, this.f9928);
        hashMap.put(3, this.f9927);
        hashMap.put(4, this.f9925);
        hashMap.put(5, this.f9917);
        hashMap.put(6, this.f9918);
        hashMap.put(7, this.f9919);
        hashMap.put(8, this.f9922);
        hashMap.put(9, this.f9923);
        hashMap.put(10, this.f9924);
        hashMap.put(11, this.f9921);
        hashMap.put(12, this.f9920);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11664(String str) {
        HashMap r1 = m10292(str);
        if (r1 != null) {
            this.f9929 = (Long) r1.get(0);
            this.f9926 = (Long) r1.get(1);
            this.f9928 = (Long) r1.get(2);
            this.f9927 = (Long) r1.get(3);
            this.f9925 = (Long) r1.get(4);
            this.f9917 = (Long) r1.get(5);
            this.f9918 = (Long) r1.get(6);
            this.f9919 = (Long) r1.get(7);
            this.f9922 = (Long) r1.get(8);
            this.f9923 = (Long) r1.get(9);
            this.f9924 = (Long) r1.get(10);
            this.f9921 = (Long) r1.get(11);
            this.f9920 = (Long) r1.get(12);
        }
    }
}
