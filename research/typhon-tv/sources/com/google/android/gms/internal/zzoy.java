package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.lang.ref.WeakReference;

final class zzoy {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public String f10819;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final WeakReference<zzanh> f10820;

    public zzoy(zzanh zzanh) {
        this.f10820 = new WeakReference<>(zzanh);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13212(zzzb zzzb) {
        zzzb.m6076("/loadHtml", new zzoz(this, zzzb));
        zzzb.m6076("/showOverlay", new zzpb(this, zzzb));
        zzzb.m6076("/hideOverlay", new zzpc(this, zzzb));
        zzanh zzanh = (zzanh) this.f10820.get();
        if (zzanh != null) {
            zzanh.m4980().m5053("/sendMessageToSdk", (zzt<? super zzanh>) new zzpd(this, zzzb));
        }
    }
}
