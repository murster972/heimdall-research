package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

public final class zzqo extends zzeu implements zzqm {
    zzqo(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m13330() throws RemoteException {
        m12298(8, v_());
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m13331() throws RemoteException {
        Parcel r0 = m12300(4, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final IObjectWrapper m13332() throws RemoteException {
        Parcel r0 = m12300(9, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final IObjectWrapper m13333() throws RemoteException {
        Parcel r0 = m12300(11, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzpq m13334(String str) throws RemoteException {
        zzpq zzps;
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r1 = m12300(2, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzps = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
            zzps = queryLocalInterface instanceof zzpq ? (zzpq) queryLocalInterface : new zzps(readStrongBinder);
        }
        r1.recycle();
        return zzps;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13335() throws RemoteException {
        m12298(6, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzll m13336() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        zzll r1 = zzlm.m13094(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13337(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12298(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13338(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r0 = m12300(1, v_);
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<String> m13339() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        ArrayList<String> createStringArrayList = r0.createStringArrayList();
        r0.recycle();
        return createStringArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13340(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        Parcel r0 = m12300(10, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }
}
