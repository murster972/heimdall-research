package com.google.android.gms.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;
import java.util.Iterator;

public final class zzcgv {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f9181;

    /* renamed from: 连任  reason: contains not printable characters */
    final zzcgx f9182;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f9183;

    /* renamed from: 麤  reason: contains not printable characters */
    final long f9184;

    /* renamed from: 齉  reason: contains not printable characters */
    final long f9185;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f9186;

    zzcgv(zzcim zzcim, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        zzbq.m9122(str2);
        zzbq.m9122(str3);
        this.f9186 = str2;
        this.f9183 = str3;
        this.f9181 = TextUtils.isEmpty(str) ? null : str;
        this.f9185 = j;
        this.f9184 = j2;
        if (this.f9184 != 0 && this.f9184 > this.f9185) {
            zzcim.m11016().m10834().m10850("Event created with reverse previous/current timestamps. appId", zzchm.m10812(str2));
        }
        this.f9182 = m10653(zzcim, bundle);
    }

    private zzcgv(zzcim zzcim, String str, String str2, String str3, long j, long j2, zzcgx zzcgx) {
        zzbq.m9122(str2);
        zzbq.m9122(str3);
        zzbq.m9120(zzcgx);
        this.f9186 = str2;
        this.f9183 = str3;
        this.f9181 = TextUtils.isEmpty(str) ? null : str;
        this.f9185 = j;
        this.f9184 = j2;
        if (this.f9184 != 0 && this.f9184 > this.f9185) {
            zzcim.m11016().m10834().m10850("Event created with reverse previous/current timestamps. appId", zzchm.m10812(str2));
        }
        this.f9182 = zzcgx;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzcgx m10653(zzcim zzcim, Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return new zzcgx(new Bundle());
        }
        Bundle bundle2 = new Bundle(bundle);
        Iterator it2 = bundle2.keySet().iterator();
        while (it2.hasNext()) {
            String str = (String) it2.next();
            if (str == null) {
                zzcim.m11016().m10832().m10849("Param name can't be null");
                it2.remove();
            } else {
                Object r3 = zzcim.m11063().m11437(str, bundle2.get(str));
                if (r3 == null) {
                    zzcim.m11016().m10834().m10850("Param value can't be null", zzcim.m11064().m10794(str));
                    it2.remove();
                } else {
                    zzcim.m11063().m11440(bundle2, str, r3);
                }
            }
        }
        return new zzcgx(bundle2);
    }

    public final String toString() {
        String str = this.f9186;
        String str2 = this.f9183;
        String valueOf = String.valueOf(this.f9182);
        return new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("Event{appId='").append(str).append("', name='").append(str2).append("', params=").append(valueOf).append("}").toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcgv m10654(zzcim zzcim, long j) {
        return new zzcgv(zzcim, this.f9181, this.f9186, this.f9183, this.f9185, j, this.f9182);
    }
}
