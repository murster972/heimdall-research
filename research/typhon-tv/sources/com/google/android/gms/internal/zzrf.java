package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzrf extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m13377(zzks zzks, IObjectWrapper iObjectWrapper) throws RemoteException;
}
