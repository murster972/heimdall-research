package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public abstract class zzdub implements zzdvf {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final byte[] f10179 = new byte[16];

    /* renamed from: 龘  reason: contains not printable characters */
    static final int[] f10180 = m12177(ByteBuffer.wrap(new byte[]{101, 120, 112, 97, 110, 100, 32, 51, 50, 45, 98, 121, 116, 101, 32, 107}));

    /* renamed from: 靐  reason: contains not printable characters */
    final zzdve f10181;

    zzdub(byte[] bArr) {
        if (bArr.length != 32) {
            throw new IllegalArgumentException("The key length in bytes must be 32.");
        }
        this.f10181 = zzdve.m12227(bArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static zzdub m12174(byte[] bArr) {
        return new zzdud(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m12175(int i, int i2) {
        return (i << i2) | (i >>> (-i2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12176(ByteBuffer byteBuffer, ByteBuffer byteBuffer2, zzduf zzduf) {
        ByteBuffer order = ByteBuffer.allocate(64).order(ByteOrder.LITTLE_ENDIAN);
        while (byteBuffer2.hasRemaining()) {
            int remaining = byteBuffer2.remaining() < 64 ? byteBuffer2.remaining() : 64;
            order.asIntBuffer().put(zzduf.m12197());
            for (int i = 0; i < remaining; i++) {
                byteBuffer.put((byte) (byteBuffer2.get() ^ order.get(i)));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int[] m12177(ByteBuffer byteBuffer) {
        IntBuffer asIntBuffer = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
        int[] iArr = new int[asIntBuffer.remaining()];
        asIntBuffer.get(iArr);
        return iArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m12178(int[] iArr);

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract zzduf m12179(byte[] bArr);

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m12180(int[] iArr);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m12181();

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12182(ByteBuffer byteBuffer, byte[] bArr) throws GeneralSecurityException {
        if (bArr.length > 2147483635) {
            throw new GeneralSecurityException("plaintext too long");
        } else if (byteBuffer.remaining() < bArr.length + 12) {
            throw new IllegalArgumentException("Given ByteBuffer output is too small");
        } else {
            byte[] r0 = zzdvi.m12235(12);
            byteBuffer.put(r0);
            m12176(byteBuffer, ByteBuffer.wrap(bArr), m12179(r0));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12183(byte[] bArr) throws GeneralSecurityException {
        ByteBuffer allocate = ByteBuffer.allocate(bArr.length + 12);
        m12182(allocate, bArr);
        return allocate.array();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int[] m12184(byte[] bArr, int i);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int[] m12185(int[] iArr) {
        int[] copyOf = Arrays.copyOf(iArr, iArr.length);
        m12178(copyOf);
        for (int i = 0; i < iArr.length; i++) {
            copyOf[i] = copyOf[i] + iArr[i];
        }
        return copyOf;
    }
}
