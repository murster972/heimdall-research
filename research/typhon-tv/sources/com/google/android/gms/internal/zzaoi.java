package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Message;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.View;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzw;

@zzzv
@TargetApi(11)
public class zzaoi extends WebChromeClient {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzanh f4533;

    public zzaoi(zzanh zzanh) {
        this.f4533 = zzanh;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Context m5214(WebView webView) {
        if (!(webView instanceof zzanh)) {
            return webView.getContext();
        }
        zzanh zzanh = (zzanh) webView;
        Activity r0 = zzanh.m5003();
        return r0 == null ? zzanh.getContext() : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m5215(Context context, String str, String str2, String str3, String str4, JsResult jsResult, JsPromptResult jsPromptResult, boolean z) {
        zzw r1;
        try {
            if (this.f4533 == null || this.f4533.m4980() == null || this.f4533.m4980().m5043() == null || (r1 = this.f4533.m4980().m5043()) == null || r1.zzda()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(str2);
                if (z) {
                    LinearLayout linearLayout = new LinearLayout(context);
                    linearLayout.setOrientation(1);
                    TextView textView = new TextView(context);
                    textView.setText(str3);
                    EditText editText = new EditText(context);
                    editText.setText(str4);
                    linearLayout.addView(textView);
                    linearLayout.addView(editText);
                    AlertDialog create = builder.setView(linearLayout).setPositiveButton(17039370, new zzaoo(jsPromptResult, editText)).setNegativeButton(17039360, new zzaon(jsPromptResult)).setOnCancelListener(new zzaom(jsPromptResult)).create();
                    return true;
                }
                AlertDialog create2 = builder.setMessage(str3).setPositiveButton(17039370, new zzaol(jsResult)).setNegativeButton(17039360, new zzaok(jsResult)).setOnCancelListener(new zzaoj(jsResult)).create();
                return true;
            }
            r1.zzt(new StringBuilder(String.valueOf(str).length() + 11 + String.valueOf(str3).length()).append("window.").append(str).append("('").append(str3).append("')").toString());
            return false;
        } catch (WindowManager.BadTokenException e) {
            zzagf.m4796("Fail to display Dialog.", e);
            return true;
        }
    }

    public final void onCloseWindow(WebView webView) {
        if (!(webView instanceof zzanh)) {
            zzagf.m4791("Tried to close a WebView that wasn't an AdWebView.");
            return;
        }
        zzd r0 = ((zzanh) webView).m4981();
        if (r0 == null) {
            zzagf.m4791("Tried to close an AdWebView not associated with an overlay.");
        } else {
            r0.close();
        }
    }

    public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String message = consoleMessage.message();
        String sourceId = consoleMessage.sourceId();
        String sb = new StringBuilder(String.valueOf(message).length() + 19 + String.valueOf(sourceId).length()).append("JS: ").append(message).append(" (").append(sourceId).append(":").append(consoleMessage.lineNumber()).append(")").toString();
        if (sb.contains("Application Cache")) {
            return super.onConsoleMessage(consoleMessage);
        }
        switch (zzaop.f8382[consoleMessage.messageLevel().ordinal()]) {
            case 1:
                zzagf.m4795(sb);
                break;
            case 2:
                zzagf.m4791(sb);
                break;
            case 3:
            case 4:
                zzagf.m4794(sb);
                break;
            case 5:
                zzagf.m4792(sb);
                break;
            default:
                zzagf.m4794(sb);
                break;
        }
        return super.onConsoleMessage(consoleMessage);
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        WebView webView2 = new WebView(webView.getContext());
        webView2.setWebViewClient(this.f4533.m4980());
        ((WebView.WebViewTransport) message.obj).setWebView(webView2);
        message.sendToTarget();
        return true;
    }

    public final void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        long j4 = 5242880 - j3;
        if (j4 <= 0) {
            quotaUpdater.updateQuota(j);
            return;
        }
        if (j != 0) {
            if (j2 == 0) {
                j = Math.min(Math.min(PlaybackStateCompat.ACTION_PREPARE_FROM_URI, j4) + j, PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
            } else if (j2 <= Math.min(PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED - j, j4)) {
                j += j2;
            }
            j2 = j;
        } else if (j2 > j4 || j2 > PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            j2 = 0;
        }
        quotaUpdater.updateQuota(j2);
    }

    public final void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        boolean z;
        if (callback != null) {
            zzbs.zzei();
            if (!zzahn.m4618(this.f4533.getContext(), this.f4533.getContext().getPackageName(), "android.permission.ACCESS_FINE_LOCATION")) {
                zzbs.zzei();
                if (!zzahn.m4618(this.f4533.getContext(), this.f4533.getContext().getPackageName(), "android.permission.ACCESS_COARSE_LOCATION")) {
                    z = false;
                    callback.invoke(str, z, true);
                }
            }
            z = true;
            callback.invoke(str, z, true);
        }
    }

    public final void onHideCustomView() {
        zzd r0 = this.f4533.m4981();
        if (r0 == null) {
            zzagf.m4791("Could not get ad overlay when hiding custom view.");
        } else {
            r0.zzms();
        }
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return m5215(m5214(webView), "alert", str, str2, (String) null, jsResult, (JsPromptResult) null, false);
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return m5215(m5214(webView), "onBeforeUnload", str, str2, (String) null, jsResult, (JsPromptResult) null, false);
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        return m5215(m5214(webView), "confirm", str, str2, (String) null, jsResult, (JsPromptResult) null, false);
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return m5215(m5214(webView), "prompt", str, str2, str3, (JsResult) null, jsPromptResult, true);
    }

    public final void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
        long j3 = PlaybackStateCompat.ACTION_PREPARE_FROM_URI + j;
        if (5242880 - j2 < j3) {
            quotaUpdater.updateQuota(0);
        } else {
            quotaUpdater.updateQuota(j3);
        }
    }

    public final void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        m5216(view, -1, customViewCallback);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5216(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
        zzd r0 = this.f4533.m4981();
        if (r0 == null) {
            zzagf.m4791("Could not get ad overlay when showing custom view.");
            customViewCallback.onCustomViewHidden();
            return;
        }
        r0.zza(view, customViewCallback);
        r0.setRequestedOrientation(i);
    }
}
