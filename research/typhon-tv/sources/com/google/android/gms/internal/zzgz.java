package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;

final class zzgz implements zzhc {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f10676;

    zzgz(zzgu zzgu, Activity activity) {
        this.f10676 = activity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12977(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStopped(this.f10676);
    }
}
