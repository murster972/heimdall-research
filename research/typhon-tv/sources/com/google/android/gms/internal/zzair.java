package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzair {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f4242;

    /* renamed from: 靐  reason: contains not printable characters */
    private final double[] f4243;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int[] f4244;

    /* renamed from: 齉  reason: contains not printable characters */
    private final double[] f4245;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String[] f4246;

    private zzair(zzaiu zzaiu) {
        int size = zzaiu.f8237.size();
        this.f4246 = (String[]) zzaiu.f8239.toArray(new String[size]);
        this.f4243 = m4708((List<Double>) zzaiu.f8237);
        this.f4245 = m4708((List<Double>) zzaiu.f8238);
        this.f4244 = new int[size];
        this.f4242 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static double[] m4708(List<Double> list) {
        double[] dArr = new double[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= dArr.length) {
                return dArr;
            }
            dArr[i2] = list.get(i2).doubleValue();
            i = i2 + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzait> m4709() {
        ArrayList arrayList = new ArrayList(this.f4246.length);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f4246.length) {
                return arrayList;
            }
            arrayList.add(new zzait(this.f4246[i2], this.f4245[i2], this.f4243[i2], ((double) this.f4244[i2]) / ((double) this.f4242), this.f4244[i2]));
            i = i2 + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4710(double d) {
        this.f4242++;
        int i = 0;
        while (i < this.f4245.length) {
            if (this.f4245[i] <= d && d < this.f4243[i]) {
                int[] iArr = this.f4244;
                iArr[i] = iArr[i] + 1;
            }
            if (d >= this.f4245[i]) {
                i++;
            } else {
                return;
            }
        }
    }
}
