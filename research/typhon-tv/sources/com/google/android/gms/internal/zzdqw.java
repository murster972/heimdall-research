package com.google.android.gms.internal;

final /* synthetic */ class zzdqw {

    /* renamed from: 靐  reason: contains not printable characters */
    static final /* synthetic */ int[] f9949 = new int[zzdso.values().length];

    /* renamed from: 齉  reason: contains not printable characters */
    static final /* synthetic */ int[] f9950 = new int[zzdsa.values().length];

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ int[] f9951 = new int[zzdsq.values().length];

    static {
        try {
            f9950[zzdsa.UNCOMPRESSED.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f9950[zzdsa.COMPRESSED.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f9949[zzdso.NIST_P256.ordinal()] = 1;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f9949[zzdso.NIST_P384.ordinal()] = 2;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f9949[zzdso.NIST_P521.ordinal()] = 3;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f9951[zzdsq.SHA1.ordinal()] = 1;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f9951[zzdsq.SHA256.ordinal()] = 2;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f9951[zzdsq.SHA512.ordinal()] = 3;
        } catch (NoSuchFieldError e8) {
        }
    }
}
