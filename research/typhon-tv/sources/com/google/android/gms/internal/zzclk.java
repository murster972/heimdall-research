package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;

final class zzclk {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f9640;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzd f9641;

    public zzclk(zzd zzd) {
        zzbq.m9120(zzd);
        this.f9641 = zzd;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11332() {
        this.f9640 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11333() {
        this.f9640 = this.f9641.m9241();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11334(long j) {
        return this.f9640 == 0 || this.f9641.m9241() - this.f9640 >= 3600000;
    }
}
