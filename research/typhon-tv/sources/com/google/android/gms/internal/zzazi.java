package com.google.android.gms.internal;

import android.animation.ObjectAnimator;
import android.view.View;
import com.google.android.gms.common.util.zzq;

final class zzazi implements View.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzazh f8493;

    zzazi(zzazh zzazh) {
        this.f8493 = zzazh;
    }

    public final void onClick(View view) {
        if (zzq.m9269()) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "alpha", new float[]{0.0f});
            ofFloat.setDuration(400).addListener(new zzazj(this));
            ofFloat.start();
            return;
        }
        this.f8493.m9814();
    }
}
