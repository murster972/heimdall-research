package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InputStream;

final class zzfhu extends InputStream {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10458;

    /* renamed from: ʼ  reason: contains not printable characters */
    private /* synthetic */ zzfhq f10459;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10460;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzfey f10461;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10462;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f10463;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzfht f10464;

    public zzfhu(zzfhq zzfhq) {
        this.f10459 = zzfhq;
        m12668();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m12666() {
        if (this.f10461 != null && this.f10462 == this.f10463) {
            this.f10460 += this.f10463;
            this.f10462 = 0;
            if (this.f10464.hasNext()) {
                this.f10461 = (zzfey) this.f10464.next();
                this.f10463 = this.f10461.size();
                return;
            }
            this.f10461 = null;
            this.f10463 = 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m12667(byte[] bArr, int i, int i2) {
        int i3 = i2;
        int i4 = i;
        while (true) {
            if (i3 <= 0) {
                break;
            }
            m12666();
            if (this.f10461 != null) {
                int min = Math.min(this.f10463 - this.f10462, i3);
                if (bArr != null) {
                    this.f10461.zza(bArr, this.f10462, i4, min);
                    i4 += min;
                }
                this.f10462 += min;
                i3 -= min;
            } else if (i3 == i2) {
                return -1;
            }
        }
        return i2 - i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m12668() {
        this.f10464 = new zzfht(this.f10459);
        this.f10461 = (zzfey) this.f10464.next();
        this.f10463 = this.f10461.size();
        this.f10462 = 0;
        this.f10460 = 0;
    }

    public final int available() throws IOException {
        return this.f10459.size() - (this.f10460 + this.f10462);
    }

    public final void mark(int i) {
        this.f10458 = this.f10460 + this.f10462;
    }

    public final boolean markSupported() {
        return true;
    }

    public final int read() throws IOException {
        m12666();
        if (this.f10461 == null) {
            return -1;
        }
        zzfey zzfey = this.f10461;
        int i = this.f10462;
        this.f10462 = i + 1;
        return zzfey.zzkn(i) & 255;
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new NullPointerException();
        } else if (i >= 0 && i2 >= 0 && i2 <= bArr.length - i) {
            return m12667(bArr, i, i2);
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public final synchronized void reset() {
        m12668();
        m12667((byte[]) null, 0, this.f10458);
    }

    public final long skip(long j) {
        if (j < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (j > 2147483647L) {
            j = 2147483647L;
        }
        return (long) m12667((byte[]) null, 0, (int) j);
    }
}
