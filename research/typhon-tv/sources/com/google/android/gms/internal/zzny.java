package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzny extends zzev implements zznx {
    public zzny() {
        attachInterface(this, "com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                String r1 = m13170();
                parcel2.writeNoException();
                parcel2.writeString(r1);
                return true;
            case 2:
                String r12 = m13167();
                parcel2.writeNoException();
                parcel2.writeString(r12);
                return true;
            case 3:
                m13171(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 4:
                m13169();
                parcel2.writeNoException();
                return true;
            case 5:
                m13168();
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
