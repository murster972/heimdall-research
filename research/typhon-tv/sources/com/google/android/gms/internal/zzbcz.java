package com.google.android.gms.internal;

import android.support.v4.app.NotificationCompat;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.common.util.zzd;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbcz extends zzbbw {

    /* renamed from: 麤  reason: contains not printable characters */
    public static final String f8689 = zzbcm.m10039("com.google.cast.media");

    /* renamed from: ʻ  reason: contains not printable characters */
    private MediaStatus f8690;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<zzbde> f8691 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzbda f8692;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final zzbde f8693 = new zzbde(this.f8616, 86400000);

    /* renamed from: ʿ  reason: contains not printable characters */
    private final zzbde f8694 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˆ  reason: contains not printable characters */
    private final zzbde f8695 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzbde f8696 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˉ  reason: contains not printable characters */
    private final zzbde f8697 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˊ  reason: contains not printable characters */
    private final zzbde f8698 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˋ  reason: contains not printable characters */
    private final zzbde f8699 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˎ  reason: contains not printable characters */
    private final zzbde f8700 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˏ  reason: contains not printable characters */
    private final zzbde f8701 = new zzbde(this.f8616, 86400000);

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzbde f8702 = new zzbde(this.f8616, 86400000);

    /* renamed from: י  reason: contains not printable characters */
    private final zzbde f8703 = new zzbde(this.f8616, 86400000);

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzbde f8704 = new zzbde(this.f8616, 86400000);

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final zzbde f8705 = new zzbde(this.f8616, 86400000);

    /* renamed from: 连任  reason: contains not printable characters */
    private long f8706;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final zzbde f8707 = new zzbde(this.f8616, 86400000);

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zzbde f8708 = new zzbde(this.f8616, 86400000);

    public zzbcz(String str, zzd zzd) {
        super(f8689, zzd, "MediaControlChannel", (String) null, 1000);
        this.f8691.add(this.f8702);
        this.f8691.add(this.f8704);
        this.f8691.add(this.f8705);
        this.f8691.add(this.f8696);
        this.f8691.add(this.f8693);
        this.f8691.add(this.f8694);
        this.f8691.add(this.f8707);
        this.f8691.add(this.f8708);
        this.f8691.add(this.f8698);
        this.f8691.add(this.f8699);
        this.f8691.add(this.f8700);
        this.f8691.add(this.f8695);
        this.f8691.add(this.f8697);
        this.f8691.add(this.f8701);
        this.f8691.add(this.f8703);
        m10093();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final long m10092() throws zzbdb {
        if (this.f8690 != null) {
            return this.f8690.m7920();
        }
        throw new zzbdb();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private final void m10093() {
        this.f8706 = 0;
        this.f8690 = null;
        for (zzbde r0 : this.f8691) {
            r0.m10129();
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private final void m10094() {
        if (this.f8692 != null) {
            this.f8692.m10120();
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final void m10095() {
        if (this.f8692 != null) {
            this.f8692.m10122();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private final void m10096() {
        if (this.f8692 != null) {
            this.f8692.m10119();
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final void m10097() {
        if (this.f8692 != null) {
            this.f8692.m10121();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10098(long j, JSONObject jSONObject) throws JSONException {
        int i;
        boolean z = true;
        boolean r3 = this.f8702.m10131(j);
        boolean z2 = this.f8693.m10128() && !this.f8693.m10131(j);
        if ((!this.f8694.m10128() || this.f8694.m10131(j)) && (!this.f8707.m10128() || this.f8707.m10131(j))) {
            z = false;
        }
        int i2 = z2 ? 2 : 0;
        if (z) {
            i2 |= 1;
        }
        if (r3 || this.f8690 == null) {
            this.f8690 = new MediaStatus(jSONObject);
            this.f8706 = this.f8616.m9241();
            i = 127;
        } else {
            i = this.f8690.m7919(jSONObject, i2);
        }
        if ((i & 1) != 0) {
            this.f8706 = this.f8616.m9241();
            m10095();
        }
        if ((i & 2) != 0) {
            this.f8706 = this.f8616.m9241();
            m10095();
        }
        if ((i & 4) != 0) {
            m10096();
        }
        if ((i & 8) != 0) {
            m10097();
        }
        if ((i & 16) != 0) {
            m10094();
        }
        if ((i & 32) != 0) {
            this.f8706 = this.f8616.m9241();
            if (this.f8692 != null) {
                this.f8692.m10118();
            }
        }
        if ((i & 64) != 0) {
            this.f8706 = this.f8616.m9241();
            m10095();
        }
        for (zzbde r0 : this.f8691) {
            r0.m10133(j, 0, (Object) null);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final MediaStatus m10099() {
        return this.f8690;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final MediaInfo m10100() {
        if (this.f8690 == null) {
            return null;
        }
        return this.f8690.m7913();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final long m10101() {
        MediaInfo r0 = m10100();
        if (r0 != null) {
            return r0.m7843();
        }
        return 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m10102(zzbdd zzbdd, JSONObject jSONObject) throws IOException, zzbdb {
        JSONObject jSONObject2 = new JSONObject();
        long r2 = m9970();
        this.f8705.m10130(r2, zzbdd);
        m9960(true);
        try {
            jSONObject2.put("requestId", r2);
            jSONObject2.put(VastExtensionXmlManager.TYPE, (Object) "PLAY");
            jSONObject2.put("mediaSessionId", m10092());
            if (jSONObject != null) {
                jSONObject2.put("customData", (Object) jSONObject);
            }
        } catch (JSONException e) {
        }
        m9975(jSONObject2.toString(), r2, (String) null);
        return r2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10103(String str) {
        this.f8627.m10090("message received: %s", str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString(VastExtensionXmlManager.TYPE);
            long optLong = jSONObject.optLong("requestId", -1);
            char c = 65535;
            switch (string.hashCode()) {
                case -1830647528:
                    if (string.equals("LOAD_CANCELLED")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1125000185:
                    if (string.equals("INVALID_REQUEST")) {
                        c = 4;
                        break;
                    }
                    break;
                case -262628938:
                    if (string.equals("LOAD_FAILED")) {
                        c = 2;
                        break;
                    }
                    break;
                case 431600379:
                    if (string.equals("INVALID_PLAYER_STATE")) {
                        c = 1;
                        break;
                    }
                    break;
                case 823510221:
                    if (string.equals("MEDIA_STATUS")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    JSONArray jSONArray = jSONObject.getJSONArray(NotificationCompat.CATEGORY_STATUS);
                    if (jSONArray.length() > 0) {
                        m10098(optLong, jSONArray.getJSONObject(0));
                        return;
                    }
                    this.f8690 = null;
                    m10095();
                    m10096();
                    m10097();
                    m10094();
                    this.f8708.m10133(optLong, 0, (Object) null);
                    return;
                case 1:
                    this.f8627.m10088("received unexpected error: Invalid Player State.", new Object[0]);
                    JSONObject optJSONObject = jSONObject.optJSONObject("customData");
                    for (zzbde r0 : this.f8691) {
                        r0.m10133(optLong, 2100, optJSONObject);
                    }
                    return;
                case 2:
                    this.f8702.m10133(optLong, 2100, jSONObject.optJSONObject("customData"));
                    return;
                case 3:
                    this.f8702.m10133(optLong, 2101, jSONObject.optJSONObject("customData"));
                    return;
                case 4:
                    this.f8627.m10088("received unexpected error: Invalid Request.", new Object[0]);
                    JSONObject optJSONObject2 = jSONObject.optJSONObject("customData");
                    for (zzbde r02 : this.f8691) {
                        r02.m10133(optLong, 2100, optJSONObject2);
                    }
                    return;
                default:
                    return;
            }
        } catch (JSONException e) {
            this.f8627.m10088("Message is malformed (%s); ignoring: %s", e.getMessage(), str);
        }
        this.f8627.m10088("Message is malformed (%s); ignoring: %s", e.getMessage(), str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final long m10104() {
        MediaInfo r0 = m10100();
        if (r0 == null || this.f8706 == 0) {
            return 0;
        }
        double r8 = this.f8690.m7916();
        long r4 = this.f8690.m7902();
        int r1 = this.f8690.m7914();
        if (r8 == 0.0d || r1 != 2) {
            return r4;
        }
        long r02 = r0.m7843();
        long r6 = this.f8616.m9241() - this.f8706;
        if (r6 < 0) {
            r6 = 0;
        }
        if (r6 == 0) {
            return r4;
        }
        long j = r4 + ((long) (((double) r6) * r8));
        if (r02 <= 0 || j <= r02) {
            r02 = j < 0 ? 0 : j;
        }
        return r02;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10105(zzbdd zzbdd) throws IOException {
        JSONObject jSONObject = new JSONObject();
        long r2 = m9970();
        this.f8708.m10130(r2, zzbdd);
        m9960(true);
        try {
            jSONObject.put("requestId", r2);
            jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "GET_STATUS");
            if (this.f8690 != null) {
                jSONObject.put("mediaSessionId", this.f8690.m7920());
            }
        } catch (JSONException e) {
        }
        m9975(jSONObject.toString(), r2, (String) null);
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10106(zzbdd zzbdd, int i, long j, MediaQueueItem[] mediaQueueItemArr, int i2, Integer num, JSONObject jSONObject) throws IllegalArgumentException, IOException, zzbdb {
        if (j == -1 || j >= 0) {
            JSONObject jSONObject2 = new JSONObject();
            long r4 = m9970();
            this.f8695.m10130(r4, zzbdd);
            m9960(true);
            try {
                jSONObject2.put("requestId", r4);
                jSONObject2.put(VastExtensionXmlManager.TYPE, (Object) "QUEUE_UPDATE");
                jSONObject2.put("mediaSessionId", m10092());
                if (i != 0) {
                    jSONObject2.put("currentItemId", i);
                }
                if (i2 != 0) {
                    jSONObject2.put("jump", i2);
                }
                if (mediaQueueItemArr != null && mediaQueueItemArr.length > 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (int i3 = 0; i3 < mediaQueueItemArr.length; i3++) {
                        jSONArray.put(i3, (Object) mediaQueueItemArr[i3].m7893());
                    }
                    jSONObject2.put("items", (Object) jSONArray);
                }
                if (num != null) {
                    switch (num.intValue()) {
                        case 0:
                            jSONObject2.put("repeatMode", (Object) "REPEAT_OFF");
                            break;
                        case 1:
                            jSONObject2.put("repeatMode", (Object) "REPEAT_ALL");
                            break;
                        case 2:
                            jSONObject2.put("repeatMode", (Object) "REPEAT_SINGLE");
                            break;
                        case 3:
                            jSONObject2.put("repeatMode", (Object) "REPEAT_ALL_AND_SHUFFLE");
                            break;
                    }
                }
                if (j != -1) {
                    jSONObject2.put("currentTime", ((double) j) / 1000.0d);
                }
                if (jSONObject != null) {
                    jSONObject2.put("customData", (Object) jSONObject);
                }
            } catch (JSONException e) {
            }
            m9975(jSONObject2.toString(), r4, (String) null);
            return r4;
        }
        throw new IllegalArgumentException(new StringBuilder(53).append("playPosition cannot be negative: ").append(j).toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10107(zzbdd zzbdd, long j, int i, JSONObject jSONObject) throws IOException, zzbdb {
        JSONObject jSONObject2 = new JSONObject();
        long r2 = m9970();
        this.f8693.m10130(r2, zzbdd);
        m9960(true);
        try {
            jSONObject2.put("requestId", r2);
            jSONObject2.put(VastExtensionXmlManager.TYPE, (Object) "SEEK");
            jSONObject2.put("mediaSessionId", m10092());
            jSONObject2.put("currentTime", ((double) j) / 1000.0d);
            if (i == 1) {
                jSONObject2.put("resumeState", (Object) "PLAYBACK_START");
            } else if (i == 2) {
                jSONObject2.put("resumeState", (Object) "PLAYBACK_PAUSE");
            }
            if (jSONObject != null) {
                jSONObject2.put("customData", (Object) jSONObject);
            }
        } catch (JSONException e) {
        }
        m9975(jSONObject2.toString(), r2, (String) null);
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10108(zzbdd zzbdd, MediaInfo mediaInfo, MediaLoadOptions mediaLoadOptions) throws IOException {
        JSONObject jSONObject = new JSONObject();
        long r2 = m9970();
        this.f8702.m10130(r2, zzbdd);
        m9960(true);
        try {
            jSONObject.put("requestId", r2);
            jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "LOAD");
            jSONObject.put("media", (Object) mediaInfo.m7842());
            jSONObject.put("autoplay", mediaLoadOptions.m7865());
            jSONObject.put("currentTime", ((double) mediaLoadOptions.m7862()) / 1000.0d);
            jSONObject.put("playbackRate", mediaLoadOptions.m7864());
            long[] r4 = mediaLoadOptions.m7863();
            if (r4 != null) {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < r4.length; i++) {
                    jSONArray.put(i, r4[i]);
                }
                jSONObject.put("activeTrackIds", (Object) jSONArray);
            }
            JSONObject r0 = mediaLoadOptions.m7861();
            if (r0 != null) {
                jSONObject.put("customData", (Object) r0);
            }
        } catch (JSONException e) {
        }
        m9975(jSONObject.toString(), r2, (String) null);
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10109(zzbdd zzbdd, JSONObject jSONObject) throws IOException, zzbdb {
        JSONObject jSONObject2 = new JSONObject();
        long r2 = m9970();
        this.f8704.m10130(r2, zzbdd);
        m9960(true);
        try {
            jSONObject2.put("requestId", r2);
            jSONObject2.put(VastExtensionXmlManager.TYPE, (Object) "PAUSE");
            jSONObject2.put("mediaSessionId", m10092());
            if (jSONObject != null) {
                jSONObject2.put("customData", (Object) jSONObject);
            }
        } catch (JSONException e) {
        }
        m9975(jSONObject2.toString(), r2, (String) null);
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10110(zzbdd zzbdd, long[] jArr) throws IOException, zzbdb {
        JSONObject jSONObject = new JSONObject();
        long r2 = m9970();
        this.f8698.m10130(r2, zzbdd);
        m9960(true);
        try {
            jSONObject.put("requestId", r2);
            jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "EDIT_TRACKS_INFO");
            jSONObject.put("mediaSessionId", m10092());
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < jArr.length; i++) {
                jSONArray.put(i, jArr[i]);
            }
            jSONObject.put("activeTrackIds", (Object) jSONArray);
        } catch (JSONException e) {
        }
        m9975(jSONObject.toString(), r2, (String) null);
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10111() {
        super.m9959();
        m10093();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10112(long j, int i) {
        for (zzbde r0 : this.f8691) {
            r0.m10133(j, i, (Object) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10113(zzbda zzbda) {
        this.f8692 = zzbda;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10114(long j) {
        boolean z;
        for (zzbde r0 : this.f8691) {
            r0.m10132(j, 2102);
        }
        synchronized (zzbde.f8714) {
            Iterator<zzbde> it2 = this.f8691.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (it2.next().m10128()) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        }
        return z;
    }
}
