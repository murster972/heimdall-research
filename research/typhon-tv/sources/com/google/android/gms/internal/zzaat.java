package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@zzzv
public final class zzaat extends zzbfm {
    public static final Parcelable.Creator<zzaat> CREATOR = new zzaav();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final ApplicationInfo f3716;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final String f3717;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final PackageInfo f3718;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final String f3719;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String f3720;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final boolean f3721;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int f3722;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public final int f3723;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final List<String> f3724;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public final boolean f3725;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final String f3726;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public final String f3727;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final Bundle f3728;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final boolean f3729;

    /* renamed from: ˉ  reason: contains not printable characters */
    public final long f3730;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public final zzlr f3731;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int f3732;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public final String f3733;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final int f3734;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public final Bundle f3735;

    /* renamed from: ˎ  reason: contains not printable characters */
    public final float f3736;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public final String f3737;

    /* renamed from: ˏ  reason: contains not printable characters */
    public final String f3738;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public final String f3739;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String f3740;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public final boolean f3741;

    /* renamed from: י  reason: contains not printable characters */
    public final List<String> f3742;

    /* renamed from: יי  reason: contains not printable characters */
    public final String f3743;

    /* renamed from: ـ  reason: contains not printable characters */
    public final String f3744;

    /* renamed from: ــ  reason: contains not printable characters */
    public final Bundle f3745;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final String f3746;

    /* renamed from: ٴٴ  reason: contains not printable characters */
    public final boolean f3747;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final zzakd f3748;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final boolean f3749;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final zzpe f3750;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final boolean f3751;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final List<String> f3752;

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public final List<String> f3753;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final long f3754;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public final List<Integer> f3755;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final String f3756;

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    public final int f3757;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final float f3758;

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    public final boolean f3759;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f3760;

    /* renamed from: 靐  reason: contains not printable characters */
    public final Bundle f3761;

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzjn f3762;

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzjj f3763;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f3764;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public final int f3765;

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    public final boolean f3766;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Bundle f3767;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean f3768;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final int f3769;

    zzaat(int i, Bundle bundle, zzjj zzjj, zzjn zzjn, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, String str4, zzakd zzakd, Bundle bundle2, int i2, List<String> list, Bundle bundle3, boolean z, int i3, int i4, float f, String str5, long j, String str6, List<String> list2, String str7, zzpe zzpe, List<String> list3, long j2, String str8, float f2, boolean z2, int i5, int i6, boolean z3, boolean z4, String str9, String str10, boolean z5, int i7, Bundle bundle4, String str11, zzlr zzlr, boolean z6, Bundle bundle5, String str12, String str13, String str14, boolean z7, List<Integer> list4, String str15, List<String> list5, int i8, boolean z8, boolean z9, boolean z10) {
        this.f3764 = i;
        this.f3761 = bundle;
        this.f3763 = zzjj;
        this.f3762 = zzjn;
        this.f3760 = str;
        this.f3716 = applicationInfo;
        this.f3718 = packageInfo;
        this.f3720 = str2;
        this.f3740 = str3;
        this.f3746 = str4;
        this.f3748 = zzakd;
        this.f3728 = bundle2;
        this.f3722 = i2;
        this.f3724 = list;
        this.f3752 = list3 == null ? Collections.emptyList() : Collections.unmodifiableList(list3);
        this.f3767 = bundle3;
        this.f3768 = z;
        this.f3732 = i3;
        this.f3734 = i4;
        this.f3736 = f;
        this.f3726 = str5;
        this.f3730 = j;
        this.f3738 = str6;
        this.f3742 = list2 == null ? Collections.emptyList() : Collections.unmodifiableList(list2);
        this.f3744 = str7;
        this.f3750 = zzpe;
        this.f3754 = j2;
        this.f3756 = str8;
        this.f3758 = f2;
        this.f3721 = z2;
        this.f3765 = i5;
        this.f3769 = i6;
        this.f3749 = z3;
        this.f3751 = z4;
        this.f3717 = str9;
        this.f3719 = str10;
        this.f3725 = z5;
        this.f3723 = i7;
        this.f3745 = bundle4;
        this.f3727 = str11;
        this.f3731 = zzlr;
        this.f3729 = z6;
        this.f3735 = bundle5;
        this.f3733 = str12;
        this.f3739 = str13;
        this.f3737 = str14;
        this.f3741 = z7;
        this.f3755 = list4;
        this.f3743 = str15;
        this.f3753 = list5;
        this.f3757 = i8;
        this.f3759 = z8;
        this.f3766 = z9;
        this.f3747 = z10;
    }

    private zzaat(Bundle bundle, zzjj zzjj, zzjn zzjn, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, String str4, zzakd zzakd, Bundle bundle2, int i, List<String> list, List<String> list2, Bundle bundle3, boolean z, int i2, int i3, float f, String str5, long j, String str6, List<String> list3, String str7, zzpe zzpe, long j2, String str8, float f2, boolean z2, int i4, int i5, boolean z3, boolean z4, String str9, String str10, boolean z5, int i6, Bundle bundle4, String str11, zzlr zzlr, boolean z6, Bundle bundle5, String str12, String str13, String str14, boolean z7, List<Integer> list4, String str15, List<String> list5, int i7, boolean z8, boolean z9, boolean z10) {
        this(24, bundle, zzjj, zzjn, str, applicationInfo, packageInfo, str2, str3, str4, zzakd, bundle2, i, list, bundle3, z, i2, i3, f, str5, j, str6, list3, str7, zzpe, list2, j2, str8, f2, z2, i4, i5, z3, z4, str9, str10, z5, i6, bundle4, str11, zzlr, z6, bundle5, str12, str13, str14, z7, list4, str15, list5, i7, z8, z9, z10);
    }

    public zzaat(zzaau zzaau, long j, String str, String str2, String str3) {
        this(zzaau.f3814, zzaau.f3811, zzaau.f3813, zzaau.f3812, zzaau.f3810, zzaau.f3770, (String) zzakl.m4807(zzaau.f3789, ""), zzaau.f3772, zzaau.f3774, zzaau.f3800, zzaau.f3794, zzaau.f3801, zzaau.f3782, zzaau.f3776, zzaau.f3816, zzaau.f3817, zzaau.f3786, zzaau.f3788, zzaau.f3790, zzaau.f3780, zzaau.f3784, zzaau.f3792, zzaau.f3796, zzaau.f3798, zzaau.f3803, j, zzaau.f3805, zzaau.f3806, zzaau.f3808, zzaau.f3809, zzaau.f3815, zzaau.f3818, zzaau.f3802, (String) zzakl.m4808(zzaau.f3804, "", 1, TimeUnit.SECONDS), zzaau.f3771, zzaau.f3775, zzaau.f3773, zzaau.f3779, zzaau.f3777, zzaau.f3799, zzaau.f3781, zzaau.f3785, str, str2, str3, zzaau.f3783, zzaau.f3787, zzaau.f3793, zzaau.f3778, zzaau.f3791, zzaau.f3795, zzaau.f3807, zzaau.f3797);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f3764);
        zzbfp.m10187(parcel, 2, this.f3761, false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f3763, i, false);
        zzbfp.m10189(parcel, 4, (Parcelable) this.f3762, i, false);
        zzbfp.m10193(parcel, 5, this.f3760, false);
        zzbfp.m10189(parcel, 6, (Parcelable) this.f3716, i, false);
        zzbfp.m10189(parcel, 7, (Parcelable) this.f3718, i, false);
        zzbfp.m10193(parcel, 8, this.f3720, false);
        zzbfp.m10193(parcel, 9, this.f3740, false);
        zzbfp.m10193(parcel, 10, this.f3746, false);
        zzbfp.m10189(parcel, 11, (Parcelable) this.f3748, i, false);
        zzbfp.m10187(parcel, 12, this.f3728, false);
        zzbfp.m10185(parcel, 13, this.f3722);
        zzbfp.m10178(parcel, 14, this.f3724, false);
        zzbfp.m10187(parcel, 15, this.f3767, false);
        zzbfp.m10195(parcel, 16, this.f3768);
        zzbfp.m10185(parcel, 18, this.f3732);
        zzbfp.m10185(parcel, 19, this.f3734);
        zzbfp.m10184(parcel, 20, this.f3736);
        zzbfp.m10193(parcel, 21, this.f3726, false);
        zzbfp.m10186(parcel, 25, this.f3730);
        zzbfp.m10193(parcel, 26, this.f3738, false);
        zzbfp.m10178(parcel, 27, this.f3742, false);
        zzbfp.m10193(parcel, 28, this.f3744, false);
        zzbfp.m10189(parcel, 29, (Parcelable) this.f3750, i, false);
        zzbfp.m10178(parcel, 30, this.f3752, false);
        zzbfp.m10186(parcel, 31, this.f3754);
        zzbfp.m10193(parcel, 33, this.f3756, false);
        zzbfp.m10184(parcel, 34, this.f3758);
        zzbfp.m10185(parcel, 35, this.f3765);
        zzbfp.m10185(parcel, 36, this.f3769);
        zzbfp.m10195(parcel, 37, this.f3749);
        zzbfp.m10195(parcel, 38, this.f3751);
        zzbfp.m10193(parcel, 39, this.f3717, false);
        zzbfp.m10195(parcel, 40, this.f3721);
        zzbfp.m10193(parcel, 41, this.f3719, false);
        zzbfp.m10195(parcel, 42, this.f3725);
        zzbfp.m10185(parcel, 43, this.f3723);
        zzbfp.m10187(parcel, 44, this.f3745, false);
        zzbfp.m10193(parcel, 45, this.f3727, false);
        zzbfp.m10189(parcel, 46, (Parcelable) this.f3731, i, false);
        zzbfp.m10195(parcel, 47, this.f3729);
        zzbfp.m10187(parcel, 48, this.f3735, false);
        zzbfp.m10193(parcel, 49, this.f3733, false);
        zzbfp.m10193(parcel, 50, this.f3739, false);
        zzbfp.m10193(parcel, 51, this.f3737, false);
        zzbfp.m10195(parcel, 52, this.f3741);
        zzbfp.m10194(parcel, 53, this.f3755, false);
        zzbfp.m10193(parcel, 54, this.f3743, false);
        zzbfp.m10178(parcel, 55, this.f3753, false);
        zzbfp.m10185(parcel, 56, this.f3757);
        zzbfp.m10195(parcel, 57, this.f3759);
        zzbfp.m10195(parcel, 58, this.f3766);
        zzbfp.m10195(parcel, 59, this.f3747);
        zzbfp.m10182(parcel, r0);
    }
}
