package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzaeo extends zzeu implements zzaem {
    zzaeo(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m9550(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(8, v_);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m9551(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(10, v_);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m9552(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(6, v_);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9553(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(3, v_);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9554(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeInt(i);
        m12298(9, v_);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9555(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(5, v_);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9556(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(4, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9557(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(1, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9558(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeInt(i);
        m12298(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9559(IObjectWrapper iObjectWrapper, zzaeq zzaeq) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12306(v_, (Parcelable) zzaeq);
        m12298(7, v_);
    }
}
