package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;

@zzzv
public final class zzaei extends zzagb implements zzaeh {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<zzaeb> f4042;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final HashSet<String> f4043;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Object f4044;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzade f4045;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final long f4046;

    /* renamed from: 连任  reason: contains not printable characters */
    private final HashMap<String, zzady> f4047;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f4048;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ArrayList<String> f4049;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ArrayList<Future> f4050;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzafp f4051;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzaei(android.content.Context r7, com.google.android.gms.internal.zzafp r8, com.google.android.gms.internal.zzade r9) {
        /*
            r6 = this;
            com.google.android.gms.internal.zzmx<java.lang.Long> r0 = com.google.android.gms.internal.zznh.f4910
            com.google.android.gms.internal.zznf r1 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r1.m5595(r0)
            java.lang.Long r0 = (java.lang.Long) r0
            long r4 = r0.longValue()
            r0 = r6
            r1 = r7
            r2 = r8
            r3 = r9
            r0.<init>(r1, r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaei.<init>(android.content.Context, com.google.android.gms.internal.zzafp, com.google.android.gms.internal.zzade):void");
    }

    private zzaei(Context context, zzafp zzafp, zzade zzade, long j) {
        this.f4050 = new ArrayList<>();
        this.f4049 = new ArrayList<>();
        this.f4047 = new HashMap<>();
        this.f4042 = new ArrayList();
        this.f4043 = new HashSet<>();
        this.f4044 = new Object();
        this.f4048 = context;
        this.f4051 = zzafp;
        this.f4045 = zzade;
        this.f4046 = j;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final String m4378() {
        int i;
        StringBuilder sb = new StringBuilder("");
        if (this.f4042 == null) {
            return sb.toString();
        }
        for (zzaeb next : this.f4042) {
            if (next != null && !TextUtils.isEmpty(next.f4038)) {
                String str = next.f4038;
                switch (next.f4035) {
                    case 3:
                        i = 1;
                        break;
                    case 4:
                        i = 2;
                        break;
                    case 5:
                        i = 4;
                        break;
                    case 6:
                        i = 0;
                        break;
                    case 7:
                        i = 3;
                        break;
                    default:
                        i = 6;
                        break;
                }
                sb.append(String.valueOf(new StringBuilder(String.valueOf(str).length() + 33).append(str).append(".").append(i).append(".").append(next.f4037).toString()).concat(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
            }
        }
        return sb.substring(0, Math.max(0, sb.length() - 1));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzafo m4380(int i, String str, zzuh zzuh) {
        return new zzafo(this.f4051.f4136.f3763, (zzanh) null, this.f4051.f4133.f3860, i, this.f4051.f4133.f3857, this.f4051.f4133.f3844, this.f4051.f4133.f3849, this.f4051.f4133.f3848, this.f4051.f4136.f3740, this.f4051.f4133.f3822, zzuh, (zzva) null, str, this.f4051.f4135, (zzuk) null, this.f4051.f4133.f3824, this.f4051.f4134, this.f4051.f4133.f3820, this.f4051.f4127, this.f4051.f4133.f3826, this.f4051.f4133.f3828, this.f4051.f4129, (zzou) null, this.f4051.f4133.f3854, this.f4051.f4133.f3855, this.f4051.f4133.f3856, this.f4051.f4133.f3862, this.f4051.f4133.f3865, m4378(), this.f4051.f4133.f3821, this.f4051.f4133.f3827, this.f4051.f4130, this.f4051.f4133.f3835, this.f4051.f4131);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4381() {
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processExcHandler(RegionMaker.java:1043)
        	at jadx.core.dex.visitors.regions.RegionMaker.processTryCatchBlocks(RegionMaker.java:975)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4382() {
        /*
            r14 = this;
            r10 = 0
            com.google.android.gms.internal.zzafp r0 = r14.f4051
            com.google.android.gms.internal.zzui r0 = r0.f4135
            java.util.List<com.google.android.gms.internal.zzuh> r0 = r0.f5437
            java.util.Iterator r11 = r0.iterator()
        L_0x000b:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x00b4
            java.lang.Object r4 = r11.next()
            com.google.android.gms.internal.zzuh r4 = (com.google.android.gms.internal.zzuh) r4
            java.lang.String r3 = r4.f5412
            java.util.List<java.lang.String> r0 = r4.f5417
            java.util.Iterator r12 = r0.iterator()
        L_0x001f:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x000b
            java.lang.Object r0 = r12.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x003d
            java.lang.String r1 = "com.google.ads.mediation.customevent.CustomEventAdapter"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x01d6
        L_0x003d:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0088 }
            r0.<init>((java.lang.String) r3)     // Catch:{ JSONException -> 0x0088 }
            java.lang.String r1 = "class_name"
            java.lang.String r2 = r0.getString(r1)     // Catch:{ JSONException -> 0x0088 }
        L_0x0049:
            java.lang.Object r13 = r14.f4044
            monitor-enter(r13)
            com.google.android.gms.internal.zzade r0 = r14.f4045     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzael r6 = r0.m4351((java.lang.String) r2)     // Catch:{ all -> 0x0085 }
            if (r6 == 0) goto L_0x0060
            com.google.android.gms.internal.zzaeg r0 = r6.m4385()     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x0060
            com.google.android.gms.internal.zzva r0 = r6.m4386()     // Catch:{ all -> 0x0085 }
            if (r0 != 0) goto L_0x0090
        L_0x0060:
            java.util.List<com.google.android.gms.internal.zzaeb> r0 = r14.f4042     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzaed r1 = new com.google.android.gms.internal.zzaed     // Catch:{ all -> 0x0085 }
            r1.<init>()     // Catch:{ all -> 0x0085 }
            java.lang.String r5 = r4.f5416     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzaed r1 = r1.m9524((java.lang.String) r5)     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzaed r1 = r1.m9528((java.lang.String) r2)     // Catch:{ all -> 0x0085 }
            r6 = 0
            com.google.android.gms.internal.zzaed r1 = r1.m9527((long) r6)     // Catch:{ all -> 0x0085 }
            r2 = 7
            com.google.android.gms.internal.zzaed r1 = r1.m9526((int) r2)     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzaeb r1 = r1.m9525()     // Catch:{ all -> 0x0085 }
            r0.add(r1)     // Catch:{ all -> 0x0085 }
            monitor-exit(r13)     // Catch:{ all -> 0x0085 }
            goto L_0x001f
        L_0x0085:
            r0 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x0085 }
            throw r0
        L_0x0088:
            r0 = move-exception
            java.lang.String r1 = "Unable to determine custom event class name, skipping..."
            com.google.android.gms.internal.zzagf.m4793(r1, r0)
            goto L_0x001f
        L_0x0090:
            com.google.android.gms.internal.zzady r0 = new com.google.android.gms.internal.zzady     // Catch:{ all -> 0x0085 }
            android.content.Context r1 = r14.f4048     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzafp r5 = r14.f4051     // Catch:{ all -> 0x0085 }
            long r8 = r14.f4046     // Catch:{ all -> 0x0085 }
            r7 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0085 }
            java.util.ArrayList<java.util.concurrent.Future> r5 = r14.f4050     // Catch:{ all -> 0x0085 }
            java.lang.Object r1 = r0.m4523()     // Catch:{ all -> 0x0085 }
            com.google.android.gms.internal.zzakv r1 = (com.google.android.gms.internal.zzakv) r1     // Catch:{ all -> 0x0085 }
            r5.add(r1)     // Catch:{ all -> 0x0085 }
            java.util.ArrayList<java.lang.String> r1 = r14.f4049     // Catch:{ all -> 0x0085 }
            r1.add(r2)     // Catch:{ all -> 0x0085 }
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r1 = r14.f4047     // Catch:{ all -> 0x0085 }
            r1.put(r2, r0)     // Catch:{ all -> 0x0085 }
            monitor-exit(r13)     // Catch:{ all -> 0x0085 }
            goto L_0x001f
        L_0x00b4:
            r0 = 0
            r1 = r0
        L_0x00b6:
            java.util.ArrayList<java.util.concurrent.Future> r0 = r14.f4050
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x015c
            java.util.ArrayList<java.util.concurrent.Future> r0 = r14.f4050     // Catch:{ InterruptedException -> 0x012f, Exception -> 0x016f }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ InterruptedException -> 0x012f, Exception -> 0x016f }
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0     // Catch:{ InterruptedException -> 0x012f, Exception -> 0x016f }
            r0.get()     // Catch:{ InterruptedException -> 0x012f, Exception -> 0x016f }
            java.lang.Object r2 = r14.f4044
            monitor-enter(r2)
            java.util.ArrayList<java.lang.String> r0 = r14.f4049     // Catch:{ all -> 0x012c }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x012c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x012c }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x012c }
            if (r3 != 0) goto L_0x00ed
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r3 = r14.f4047     // Catch:{ all -> 0x012c }
            java.lang.Object r0 = r3.get(r0)     // Catch:{ all -> 0x012c }
            com.google.android.gms.internal.zzady r0 = (com.google.android.gms.internal.zzady) r0     // Catch:{ all -> 0x012c }
            if (r0 == 0) goto L_0x00ed
            java.util.List<com.google.android.gms.internal.zzaeb> r3 = r14.f4042     // Catch:{ all -> 0x012c }
            com.google.android.gms.internal.zzaeb r0 = r0.连任()     // Catch:{ all -> 0x012c }
            r3.add(r0)     // Catch:{ all -> 0x012c }
        L_0x00ed:
            monitor-exit(r2)     // Catch:{ all -> 0x012c }
            java.lang.Object r2 = r14.f4044
            monitor-enter(r2)
            java.util.HashSet<java.lang.String> r0 = r14.f4043     // Catch:{ all -> 0x01d3 }
            java.util.ArrayList<java.lang.String> r3 = r14.f4049     // Catch:{ all -> 0x01d3 }
            java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x01d3 }
            boolean r0 = r0.contains(r3)     // Catch:{ all -> 0x01d3 }
            if (r0 == 0) goto L_0x01d1
            java.util.ArrayList<java.lang.String> r0 = r14.f4049     // Catch:{ all -> 0x01d3 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x01d3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x01d3 }
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r1 = r14.f4047     // Catch:{ all -> 0x01d3 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ all -> 0x01d3 }
            if (r1 == 0) goto L_0x01ce
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r1 = r14.f4047     // Catch:{ all -> 0x01d3 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ all -> 0x01d3 }
            com.google.android.gms.internal.zzady r1 = (com.google.android.gms.internal.zzady) r1     // Catch:{ all -> 0x01d3 }
            com.google.android.gms.internal.zzuh r1 = r1.ʻ()     // Catch:{ all -> 0x01d3 }
        L_0x011b:
            r3 = -2
            com.google.android.gms.internal.zzafo r0 = r14.m4380(r3, r0, r1)     // Catch:{ all -> 0x01d3 }
            android.os.Handler r1 = com.google.android.gms.internal.zzajr.f4285     // Catch:{ all -> 0x01d3 }
            com.google.android.gms.internal.zzaej r3 = new com.google.android.gms.internal.zzaej     // Catch:{ all -> 0x01d3 }
            r3.<init>(r14, r0)     // Catch:{ all -> 0x01d3 }
            r1.post(r3)     // Catch:{ all -> 0x01d3 }
            monitor-exit(r2)     // Catch:{ all -> 0x01d3 }
        L_0x012b:
            return
        L_0x012c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x012c }
            throw r0
        L_0x012f:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x01a3 }
            r0.interrupt()     // Catch:{ all -> 0x01a3 }
            java.lang.Object r2 = r14.f4044
            monitor-enter(r2)
            java.util.ArrayList<java.lang.String> r0 = r14.f4049     // Catch:{ all -> 0x016c }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x016c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x016c }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x016c }
            if (r1 != 0) goto L_0x015b
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r1 = r14.f4047     // Catch:{ all -> 0x016c }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x016c }
            com.google.android.gms.internal.zzady r0 = (com.google.android.gms.internal.zzady) r0     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x015b
            java.util.List<com.google.android.gms.internal.zzaeb> r1 = r14.f4042     // Catch:{ all -> 0x016c }
            com.google.android.gms.internal.zzaeb r0 = r0.连任()     // Catch:{ all -> 0x016c }
            r1.add(r0)     // Catch:{ all -> 0x016c }
        L_0x015b:
            monitor-exit(r2)     // Catch:{ all -> 0x016c }
        L_0x015c:
            r0 = 3
            com.google.android.gms.internal.zzafo r0 = r14.m4380(r0, r10, r10)
            android.os.Handler r1 = com.google.android.gms.internal.zzajr.f4285
            com.google.android.gms.internal.zzaek r2 = new com.google.android.gms.internal.zzaek
            r2.<init>(r14, r0)
            r1.post(r2)
            goto L_0x012b
        L_0x016c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x016c }
            throw r0
        L_0x016f:
            r0 = move-exception
            java.lang.String r2 = "Unable to resolve rewarded adapter."
            com.google.android.gms.internal.zzagf.m4796(r2, r0)     // Catch:{ all -> 0x01a3 }
            java.lang.Object r2 = r14.f4044
            monitor-enter(r2)
            java.util.ArrayList<java.lang.String> r0 = r14.f4049     // Catch:{ all -> 0x01a0 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x01a0 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x01a0 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x01a0 }
            if (r3 != 0) goto L_0x019a
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r3 = r14.f4047     // Catch:{ all -> 0x01a0 }
            java.lang.Object r0 = r3.get(r0)     // Catch:{ all -> 0x01a0 }
            com.google.android.gms.internal.zzady r0 = (com.google.android.gms.internal.zzady) r0     // Catch:{ all -> 0x01a0 }
            if (r0 == 0) goto L_0x019a
            java.util.List<com.google.android.gms.internal.zzaeb> r3 = r14.f4042     // Catch:{ all -> 0x01a0 }
            com.google.android.gms.internal.zzaeb r0 = r0.连任()     // Catch:{ all -> 0x01a0 }
            r3.add(r0)     // Catch:{ all -> 0x01a0 }
        L_0x019a:
            monitor-exit(r2)     // Catch:{ all -> 0x01a0 }
        L_0x019b:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00b6
        L_0x01a0:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x01a0 }
            throw r0
        L_0x01a3:
            r0 = move-exception
            r2 = r0
            java.lang.Object r3 = r14.f4044
            monitor-enter(r3)
            java.util.ArrayList<java.lang.String> r0 = r14.f4049     // Catch:{ all -> 0x01cb }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x01cb }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x01cb }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x01cb }
            if (r1 != 0) goto L_0x01c9
            java.util.HashMap<java.lang.String, com.google.android.gms.internal.zzady> r1 = r14.f4047     // Catch:{ all -> 0x01cb }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzady r0 = (com.google.android.gms.internal.zzady) r0     // Catch:{ all -> 0x01cb }
            if (r0 == 0) goto L_0x01c9
            java.util.List<com.google.android.gms.internal.zzaeb> r1 = r14.f4042     // Catch:{ all -> 0x01cb }
            com.google.android.gms.internal.zzaeb r0 = r0.连任()     // Catch:{ all -> 0x01cb }
            r1.add(r0)     // Catch:{ all -> 0x01cb }
        L_0x01c9:
            monitor-exit(r3)     // Catch:{ all -> 0x01cb }
            throw r2
        L_0x01cb:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x01cb }
            throw r0
        L_0x01ce:
            r1 = r10
            goto L_0x011b
        L_0x01d1:
            monitor-exit(r2)     // Catch:{ all -> 0x01d3 }
            goto L_0x019b
        L_0x01d3:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x01d3 }
            throw r0
        L_0x01d6:
            r2 = r0
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaei.m4382():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4383(String str) {
        synchronized (this.f4044) {
            this.f4043.add(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4384(String str, int i) {
    }
}
