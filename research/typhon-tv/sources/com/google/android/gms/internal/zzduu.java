package com.google.android.gms.internal;

import com.google.android.gms.internal.zzduv;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;

public final class zzduu<T_WRAPPER extends zzduv<T_ENGINE>, T_ENGINE> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final List<Provider> f10213;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static zzduu<zzdvc, Signature> f10214 = new zzduu<>(new zzdvc());

    /* renamed from: ʽ  reason: contains not printable characters */
    private static zzduu<zzdvb, MessageDigest> f10215 = new zzduu<>(new zzdvb());

    /* renamed from: ˑ  reason: contains not printable characters */
    private static zzduu<zzduy, KeyFactory> f10216 = new zzduu<>(new zzduy());

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Logger f10217 = Logger.getLogger(zzduu.class.getName());

    /* renamed from: 靐  reason: contains not printable characters */
    public static final zzduu<zzdva, Mac> f10218 = new zzduu<>(new zzdva());

    /* renamed from: 麤  reason: contains not printable characters */
    public static final zzduu<zzduz, KeyPairGenerator> f10219 = new zzduu<>(new zzduz());

    /* renamed from: 齉  reason: contains not printable characters */
    public static final zzduu<zzdux, KeyAgreement> f10220 = new zzduu<>(new zzdux());

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzduu<zzduw, Cipher> f10221 = new zzduu<>(new zzduw());

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f10222 = true;

    /* renamed from: ٴ  reason: contains not printable characters */
    private T_WRAPPER f10223;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private List<Provider> f10224 = f10213;

    static {
        if (zzdvj.m12236()) {
            String[] strArr = {"GmsCore_OpenSSL", "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 2; i++) {
                String str = strArr[i];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    f10217.logp(Level.INFO, "com.google.crypto.tink.subtle.EngineFactory", "toProviderList", String.format("Provider %s not available", new Object[]{str}));
                }
            }
            f10213 = arrayList;
        } else {
            f10213 = new ArrayList();
        }
    }

    private zzduu(T_WRAPPER t_wrapper) {
        this.f10223 = t_wrapper;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m12216(String str, Provider provider) {
        try {
            this.f10223.m12218(str, provider);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final T_ENGINE m12217(String str) throws GeneralSecurityException {
        for (Provider next : this.f10224) {
            if (m12216(str, next)) {
                return this.f10223.m12218(str, next);
            }
        }
        if (this.f10222) {
            return this.f10223.m12218(str, (Provider) null);
        }
        throw new GeneralSecurityException("No good Provider found.");
    }
}
