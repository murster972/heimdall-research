package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclw extends zzfjm<zzclw> {

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9687 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public String[] f9688 = zzfjv.f10552;

    /* renamed from: 齉  reason: contains not printable characters */
    public Boolean f9689 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9690 = null;

    public zzclw() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzclw m11471(com.google.android.gms.internal.zzfjj r8) throws java.io.IOException {
        /*
            r7 = this;
            r1 = 0
        L_0x0001:
            int r0 = r8.m12800()
            switch(r0) {
                case 0: goto L_0x000e;
                case 8: goto L_0x000f;
                case 18: goto L_0x0045;
                case 24: goto L_0x004c;
                case 34: goto L_0x0057;
                default: goto L_0x0008;
            }
        L_0x0008:
            boolean r0 = super.m12843(r8, r0)
            if (r0 != 0) goto L_0x0001
        L_0x000e:
            return r7
        L_0x000f:
            int r2 = r8.m12787()
            int r3 = r8.m12785()     // Catch:{ IllegalArgumentException -> 0x0036 }
            switch(r3) {
                case 0: goto L_0x003e;
                case 1: goto L_0x003e;
                case 2: goto L_0x003e;
                case 3: goto L_0x003e;
                case 4: goto L_0x003e;
                case 5: goto L_0x003e;
                case 6: goto L_0x003e;
                default: goto L_0x001a;
            }     // Catch:{ IllegalArgumentException -> 0x0036 }
        L_0x001a:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x0036 }
            r5 = 41
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0036 }
            r6.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x0036 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ IllegalArgumentException -> 0x0036 }
            java.lang.String r5 = " is not a valid enum MatchType"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IllegalArgumentException -> 0x0036 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x0036 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x0036 }
            throw r4     // Catch:{ IllegalArgumentException -> 0x0036 }
        L_0x0036:
            r3 = move-exception
            r8.m12792(r2)
            r7.m12843(r8, r0)
            goto L_0x0001
        L_0x003e:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x0036 }
            r7.f9690 = r3     // Catch:{ IllegalArgumentException -> 0x0036 }
            goto L_0x0001
        L_0x0045:
            java.lang.String r0 = r8.m12791()
            r7.f9687 = r0
            goto L_0x0001
        L_0x004c:
            boolean r0 = r8.m12797()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.f9689 = r0
            goto L_0x0001
        L_0x0057:
            r0 = 34
            int r2 = com.google.android.gms.internal.zzfjv.m12883(r8, r0)
            java.lang.String[] r0 = r7.f9688
            if (r0 != 0) goto L_0x007d
            r0 = r1
        L_0x0062:
            int r2 = r2 + r0
            java.lang.String[] r2 = new java.lang.String[r2]
            if (r0 == 0) goto L_0x006c
            java.lang.String[] r3 = r7.f9688
            java.lang.System.arraycopy(r3, r1, r2, r1, r0)
        L_0x006c:
            int r3 = r2.length
            int r3 = r3 + -1
            if (r0 >= r3) goto L_0x0081
            java.lang.String r3 = r8.m12791()
            r2[r0] = r3
            r8.m12800()
            int r0 = r0 + 1
            goto L_0x006c
        L_0x007d:
            java.lang.String[] r0 = r7.f9688
            int r0 = r0.length
            goto L_0x0062
        L_0x0081:
            java.lang.String r3 = r8.m12791()
            r2[r0] = r3
            r7.f9688 = r2
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzclw.m11471(com.google.android.gms.internal.zzfjj):com.google.android.gms.internal.zzclw");
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclw)) {
            return false;
        }
        zzclw zzclw = (zzclw) obj;
        if (this.f9690 == null) {
            if (zzclw.f9690 != null) {
                return false;
            }
        } else if (!this.f9690.equals(zzclw.f9690)) {
            return false;
        }
        if (this.f9687 == null) {
            if (zzclw.f9687 != null) {
                return false;
            }
        } else if (!this.f9687.equals(zzclw.f9687)) {
            return false;
        }
        if (this.f9689 == null) {
            if (zzclw.f9689 != null) {
                return false;
            }
        } else if (!this.f9689.equals(zzclw.f9689)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9688, (Object[]) zzclw.f9688)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclw.f10533 == null || zzclw.f10533.m12849() : this.f10533.equals(zzclw.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.f9689 == null ? 0 : this.f9689.hashCode()) + (((this.f9687 == null ? 0 : this.f9687.hashCode()) + (((this.f9690 == null ? 0 : this.f9690.intValue()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31) + zzfjq.m12859((Object[]) this.f9688)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11470() {
        int i;
        int r0 = super.m12841();
        if (this.f9690 != null) {
            r0 += zzfjk.m12806(1, this.f9690.intValue());
        }
        if (this.f9687 != null) {
            r0 += zzfjk.m12808(2, this.f9687);
        }
        if (this.f9689 != null) {
            this.f9689.booleanValue();
            r0 += zzfjk.m12805(3) + 1;
        }
        if (this.f9688 == null || this.f9688.length <= 0) {
            return r0;
        }
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < this.f9688.length) {
            String str = this.f9688[i2];
            if (str != null) {
                i4++;
                i = zzfjk.m12820(str) + i3;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        return r0 + i3 + (i4 * 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11472(zzfjk zzfjk) throws IOException {
        if (this.f9690 != null) {
            zzfjk.m12832(1, this.f9690.intValue());
        }
        if (this.f9687 != null) {
            zzfjk.m12835(2, this.f9687);
        }
        if (this.f9689 != null) {
            zzfjk.m12836(3, this.f9689.booleanValue());
        }
        if (this.f9688 != null && this.f9688.length > 0) {
            for (String str : this.f9688) {
                if (str != null) {
                    zzfjk.m12835(4, str);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
