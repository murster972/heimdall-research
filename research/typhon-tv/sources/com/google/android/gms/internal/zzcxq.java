package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbt;

public final class zzcxq extends zzbfm {
    public static final Parcelable.Creator<zzcxq> CREATOR = new zzcxr();

    /* renamed from: 靐  reason: contains not printable characters */
    private final ConnectionResult f9842;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzbt f9843;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9844;

    public zzcxq(int i) {
        this(new ConnectionResult(8, (PendingIntent) null), (zzbt) null);
    }

    zzcxq(int i, ConnectionResult connectionResult, zzbt zzbt) {
        this.f9844 = i;
        this.f9842 = connectionResult;
        this.f9843 = zzbt;
    }

    private zzcxq(ConnectionResult connectionResult, zzbt zzbt) {
        this(1, connectionResult, (zzbt) null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9844);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f9842, i, false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f9843, i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzbt m11577() {
        return this.f9843;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m11578() {
        return this.f9842;
    }
}
