package com.google.android.gms.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzg;

final class zzsc implements zzg {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzrx f10871;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzalf f10872;

    zzsc(zzrx zzrx, zzalf zzalf) {
        this.f10871 = zzrx;
        this.f10872 = zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13386(ConnectionResult connectionResult) {
        synchronized (this.f10871.f5349) {
            this.f10872.m4824(new RuntimeException("Connection failed."));
        }
    }
}
