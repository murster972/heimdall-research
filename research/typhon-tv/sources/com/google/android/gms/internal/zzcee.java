package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

final class zzcee extends zzcem {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ boolean f9053;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcee(zzceb zzceb, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient);
        this.f9053 = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10341(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10415(this.f9053);
        m4198(Status.f7464);
    }
}
