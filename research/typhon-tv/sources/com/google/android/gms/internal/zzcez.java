package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationSettingsRequest;

public interface zzcez extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    LocationAvailability m10353(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    Location m10354(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10355(long j, boolean z, PendingIntent pendingIntent) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10356(PendingIntent pendingIntent) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10357(Location location) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10358(zzcdz zzcdz) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10359(zzceu zzceu) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10360(zzcfq zzcfq) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10361(LocationSettingsRequest locationSettingsRequest, zzcfb zzcfb, String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10362(boolean z) throws RemoteException;
}
