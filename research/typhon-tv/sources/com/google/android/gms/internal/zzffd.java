package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.util.Arrays;

final class zzffd extends zzffb {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10356;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f10357;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f10358;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f10359;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f10360;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f10361;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f10362;

    /* renamed from: 麤  reason: contains not printable characters */
    private final byte[] f10363;

    private zzffd(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.f10361 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        this.f10363 = bArr;
        this.f10356 = i + i2;
        this.f10358 = i;
        this.f10359 = this.f10358;
        this.f10362 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b2, code lost:
        if (((long) r4[r3]) < 0) goto L_0x00b4;
     */
    /* renamed from: ʿ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final long m12418() throws java.io.IOException {
        /*
            r10 = this;
            r8 = 0
            int r0 = r10.f10358
            int r1 = r10.f10356
            if (r1 == r0) goto L_0x00b4
            byte[] r4 = r10.f10363
            int r1 = r0 + 1
            byte r0 = r4[r0]
            if (r0 < 0) goto L_0x0014
            r10.f10358 = r1
            long r0 = (long) r0
        L_0x0013:
            return r0
        L_0x0014:
            int r2 = r10.f10356
            int r2 = r2 - r1
            r3 = 9
            if (r2 < r3) goto L_0x00b4
            int r2 = r1 + 1
            byte r1 = r4[r1]
            int r1 = r1 << 7
            r0 = r0 ^ r1
            if (r0 >= 0) goto L_0x002a
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
            long r0 = (long) r0
        L_0x0027:
            r10.f10358 = r2
            goto L_0x0013
        L_0x002a:
            int r3 = r2 + 1
            byte r1 = r4[r2]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0038
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            long r0 = (long) r0
            r2 = r3
            goto L_0x0027
        L_0x0038:
            int r2 = r3 + 1
            byte r1 = r4[r3]
            int r1 = r1 << 21
            r0 = r0 ^ r1
            if (r0 >= 0) goto L_0x0047
            r1 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r1
            long r0 = (long) r0
            goto L_0x0027
        L_0x0047:
            long r0 = (long) r0
            int r3 = r2 + 1
            byte r2 = r4[r2]
            long r6 = (long) r2
            r2 = 28
            long r6 = r6 << r2
            long r0 = r0 ^ r6
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 < 0) goto L_0x005b
            r4 = 266354560(0xfe03f80, double:1.315966377E-315)
            long r0 = r0 ^ r4
            r2 = r3
            goto L_0x0027
        L_0x005b:
            int r2 = r3 + 1
            byte r3 = r4[r3]
            long r6 = (long) r3
            r3 = 35
            long r6 = r6 << r3
            long r0 = r0 ^ r6
            int r3 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x006f
            r4 = -34093383808(0xfffffff80fe03f80, double:NaN)
            long r0 = r0 ^ r4
            goto L_0x0027
        L_0x006f:
            int r3 = r2 + 1
            byte r2 = r4[r2]
            long r6 = (long) r2
            r2 = 42
            long r6 = r6 << r2
            long r0 = r0 ^ r6
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 < 0) goto L_0x0084
            r4 = 4363953127296(0x3f80fe03f80, double:2.1560793202584E-311)
            long r0 = r0 ^ r4
            r2 = r3
            goto L_0x0027
        L_0x0084:
            int r2 = r3 + 1
            byte r3 = r4[r3]
            long r6 = (long) r3
            r3 = 49
            long r6 = r6 << r3
            long r0 = r0 ^ r6
            int r3 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x0098
            r4 = -558586000294016(0xfffe03f80fe03f80, double:NaN)
            long r0 = r0 ^ r4
            goto L_0x0027
        L_0x0098:
            int r3 = r2 + 1
            byte r2 = r4[r2]
            long r6 = (long) r2
            r2 = 56
            long r6 = r6 << r2
            long r0 = r0 ^ r6
            r6 = 71499008037633920(0xfe03f80fe03f80, double:6.838959413692434E-304)
            long r0 = r0 ^ r6
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 >= 0) goto L_0x00ba
            int r2 = r3 + 1
            byte r3 = r4[r3]
            long r4 = (long) r3
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x0027
        L_0x00b4:
            long r0 = r10.m12407()
            goto L_0x0013
        L_0x00ba:
            r2 = r3
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffd.m12418():long");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private final void m12419() {
        this.f10356 += this.f10357;
        int i = this.f10356 - this.f10359;
        if (i > this.f10361) {
            this.f10357 = i - this.f10361;
            this.f10356 -= this.f10357;
            return;
        }
        this.f10357 = 0;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private final byte m12420() throws IOException {
        if (this.f10358 == this.f10356) {
            throw zzfge.m12593();
        }
        byte[] bArr = this.f10363;
        int i = this.f10358;
        this.f10358 = i + 1;
        return bArr[i];
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int m12421() throws IOException {
        int i = this.f10358;
        if (this.f10356 - i < 4) {
            throw zzfge.m12593();
        }
        byte[] bArr = this.f10363;
        this.f10358 = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final long m12422() throws IOException {
        int i = this.f10358;
        if (this.f10356 - i < 8) {
            throw zzfge.m12593();
        }
        byte[] bArr = this.f10363;
        this.f10358 = i + 8;
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m12423() throws IOException {
        int r1 = m12406();
        if (r1 <= 0 || r1 > this.f10356 - this.f10358) {
            if (r1 == 0) {
                return "";
            }
            if (r1 <= 0) {
                throw zzfge.m12590();
            }
            throw zzfge.m12593();
        } else if (!zzfis.m12769(this.f10363, this.f10358, this.f10358 + r1)) {
            throw zzfge.m12588();
        } else {
            int i = this.f10358;
            this.f10358 += r1;
            return new String(this.f10363, i, r1, zzffz.f10423);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final zzfes m12424() throws IOException {
        byte[] bArr;
        int r1 = m12406();
        if (r1 > 0 && r1 <= this.f10356 - this.f10358) {
            zzfes zze = zzfes.zze(this.f10363, this.f10358, r1);
            this.f10358 = r1 + this.f10358;
            return zze;
        } else if (r1 == 0) {
            return zzfes.zzpfg;
        } else {
            if (r1 > 0 && r1 <= this.f10356 - this.f10358) {
                int i = this.f10358;
                this.f10358 = r1 + this.f10358;
                bArr = Arrays.copyOfRange(this.f10363, i, this.f10358);
            } else if (r1 > 0) {
                throw zzfge.m12593();
            } else if (r1 == 0) {
                bArr = zzffz.f10420;
            } else {
                throw zzfge.m12590();
            }
            return zzfes.m12372(bArr);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int m12425() throws IOException {
        return m12406();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int m12426() {
        return this.f10358 - this.f10359;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean m12427() throws IOException {
        return this.f10358 == this.f10356;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m12428() throws IOException {
        return m12406();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006a, code lost:
        if (r3[r2] < 0) goto L_0x006c;
     */
    /* renamed from: ٴ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int m12429() throws java.io.IOException {
        /*
            r5 = this;
            int r0 = r5.f10358
            int r1 = r5.f10356
            if (r1 == r0) goto L_0x006c
            byte[] r3 = r5.f10363
            int r2 = r0 + 1
            byte r0 = r3[r0]
            if (r0 < 0) goto L_0x0011
            r5.f10358 = r2
        L_0x0010:
            return r0
        L_0x0011:
            int r1 = r5.f10356
            int r1 = r1 - r2
            r4 = 9
            if (r1 < r4) goto L_0x006c
            int r1 = r2 + 1
            byte r2 = r3[r2]
            int r2 = r2 << 7
            r0 = r0 ^ r2
            if (r0 >= 0) goto L_0x0026
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
        L_0x0023:
            r5.f10358 = r1
            goto L_0x0010
        L_0x0026:
            int r2 = r1 + 1
            byte r1 = r3[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0033
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            r1 = r2
            goto L_0x0023
        L_0x0033:
            int r1 = r2 + 1
            byte r2 = r3[r2]
            int r2 = r2 << 21
            r0 = r0 ^ r2
            if (r0 >= 0) goto L_0x0041
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0023
        L_0x0041:
            int r2 = r1 + 1
            byte r1 = r3[r1]
            int r4 = r1 << 28
            r0 = r0 ^ r4
            r4 = 266354560(0xfe03f80, float:2.2112565E-29)
            r0 = r0 ^ r4
            if (r1 >= 0) goto L_0x0072
            int r1 = r2 + 1
            byte r2 = r3[r2]
            if (r2 >= 0) goto L_0x0023
            int r2 = r1 + 1
            byte r1 = r3[r1]
            if (r1 >= 0) goto L_0x0072
            int r1 = r2 + 1
            byte r2 = r3[r2]
            if (r2 >= 0) goto L_0x0023
            int r2 = r1 + 1
            byte r1 = r3[r1]
            if (r1 >= 0) goto L_0x0072
            int r1 = r2 + 1
            byte r2 = r3[r2]
            if (r2 >= 0) goto L_0x0023
        L_0x006c:
            long r0 = r5.m12407()
            int r0 = (int) r0
            goto L_0x0010
        L_0x0072:
            r1 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffd.m12429():int");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final long m12430() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte r1 = m12420();
            j |= ((long) (r1 & Byte.MAX_VALUE)) << i;
            if ((r1 & 128) == 0) {
                return j;
            }
        }
        throw zzfge.m12592();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m12431() throws IOException {
        return m12418() != 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m12432(int i) throws zzfge {
        if (i < 0) {
            throw zzfge.m12590();
        }
        int r0 = m12403() + i;
        int i2 = this.f10361;
        if (r0 > i2) {
            throw zzfge.m12593();
        }
        this.f10361 = r0;
        m12419();
        return i2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m12433() throws IOException {
        return m12418();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12434() throws IOException {
        return m12421();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m12435(int i) throws IOException {
        if (i >= 0 && i <= this.f10356 - this.f10358) {
            this.f10358 += i;
        } else if (i < 0) {
            throw zzfge.m12590();
        } else {
            throw zzfge.m12593();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m12436() throws IOException {
        return m12422();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12437(int i) {
        this.f10361 = i;
        m12419();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12438() throws IOException {
        if (m12404()) {
            this.f10360 = 0;
            return 0;
        }
        this.f10360 = m12406();
        if ((this.f10360 >>> 3) != 0) {
            return this.f10360;
        }
        throw zzfge.m12591();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends zzffu<T, ?>> T m12439(T t, zzffm zzffm) throws IOException {
        int r0 = m12406();
        if (this.f10355 >= this.f10352) {
            throw zzfge.m12586();
        }
        int r02 = m12409(r0);
        this.f10355++;
        T r1 = zzffu.m12528(t, (zzffb) this, zzffm);
        m12417(0);
        this.f10355--;
        m12414(r02);
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12440(int i) throws zzfge {
        if (this.f10360 != i) {
            throw zzfge.m12589();
        }
    }
}
