package com.google.android.gms.internal;

import android.content.Context;
import android.os.SystemClock;

@zzzv
public abstract class zzxw extends zzagb {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected zzaax f5565;

    /* renamed from: 连任  reason: contains not printable characters */
    protected final zzafp f5566;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final Context f5567;

    /* renamed from: 麤  reason: contains not printable characters */
    protected final Object f5568 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    protected final Object f5569 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzyb f5570;

    protected zzxw(Context context, zzafp zzafp, zzyb zzyb) {
        super(true);
        this.f5567 = context;
        this.f5566 = zzafp;
        this.f5565 = zzafp.f4133;
        this.f5570 = zzyb;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6022() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzafo m6023(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6024() {
        synchronized (this.f5569) {
            zzagf.m4792("AdRendererBackgroundTask started.");
            int i = this.f5566.f4132;
            try {
                m6025(SystemClock.elapsedRealtime());
            } catch (zzxz e) {
                int errorCode = e.getErrorCode();
                if (errorCode == 3 || errorCode == -1) {
                    zzagf.m4794(e.getMessage());
                } else {
                    zzagf.m4791(e.getMessage());
                }
                if (this.f5565 == null) {
                    this.f5565 = new zzaax(errorCode);
                } else {
                    this.f5565 = new zzaax(errorCode, this.f5565.f3848);
                }
                zzahn.f4212.post(new zzxx(this));
                i = errorCode;
            }
            zzahn.f4212.post(new zzxy(this, m6023(i)));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m6025(long j) throws zzxz;
}
