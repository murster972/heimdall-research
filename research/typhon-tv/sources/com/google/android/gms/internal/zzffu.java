package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import com.google.android.gms.internal.zzffu.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzffu<MessageType extends zzffu<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzfek<MessageType, BuilderType> {

    /* renamed from: 麤  reason: contains not printable characters */
    private static Map<Object, zzffu<?, ?>> f10393 = new ConcurrentHashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    protected zzfio f10394 = zzfio.m12713();

    /* renamed from: 齉  reason: contains not printable characters */
    protected int f10395 = -1;

    public static abstract class zza<MessageType extends zzffu<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzfel<MessageType, BuilderType> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MessageType f10396;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f10397 = false;

        /* renamed from: 龘  reason: contains not printable characters */
        protected MessageType f10398;

        protected zza(MessageType messagetype) {
            this.f10396 = messagetype;
            this.f10398 = (zzffu) messagetype.m12535(zzg.f10406, (Object) null, (Object) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static void m12538(MessageType messagetype, MessageType messagetype2) {
            zzf zzf = zzf.f10404;
            messagetype.m12535(zzg.f10415, (Object) zzf, (Object) messagetype2);
            messagetype.f10394 = zzf.m12573(messagetype.f10394, messagetype2.f10394);
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            MessageType messagetype;
            zza zza = (zza) ((zzffu) this.f10396).m12535(zzg.f10407, (Object) null, (Object) null);
            if (this.f10397) {
                messagetype = this.f10398;
            } else {
                MessageType messagetype2 = this.f10398;
                messagetype2.m12535(zzg.f10405, (Object) null, (Object) null);
                messagetype2.f10394.m12718();
                this.f10397 = true;
                messagetype = this.f10398;
            }
            zza.m12545((zzffu) messagetype);
            return zza;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public final /* synthetic */ zzfhe m12539() {
            return this.f10396;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final /* synthetic */ zzfhe m12540() {
            MessageType messagetype;
            boolean z;
            boolean z2 = true;
            if (this.f10397) {
                messagetype = this.f10398;
            } else {
                MessageType messagetype2 = this.f10398;
                messagetype2.m12535(zzg.f10405, (Object) null, (Object) null);
                messagetype2.f10394.m12718();
                this.f10397 = true;
                messagetype = this.f10398;
            }
            zzffu zzffu = (zzffu) messagetype;
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) zzffu.m12535(zzg.f10417, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z = true;
            } else if (byteValue == 0) {
                z = false;
            } else {
                if (zzffu.m12535(zzg.f10418, (Object) Boolean.FALSE, (Object) null) == null) {
                    z2 = false;
                }
                if (booleanValue) {
                    zzffu.m12535(zzg.f10416, (Object) z2 ? zzffu : null, (Object) null);
                }
                z = z2;
            }
            if (z) {
                return zzffu;
            }
            throw new zzfim(zzffu);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m12541() {
            if (this.f10397) {
                MessageType messagetype = (zzffu) this.f10398.m12535(zzg.f10406, (Object) null, (Object) null);
                m12538(messagetype, this.f10398);
                this.f10398 = messagetype;
                this.f10397 = false;
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final MessageType m12542() {
            MessageType messagetype;
            boolean z;
            boolean z2 = true;
            if (this.f10397) {
                messagetype = this.f10398;
            } else {
                MessageType messagetype2 = this.f10398;
                messagetype2.m12535(zzg.f10405, (Object) null, (Object) null);
                messagetype2.f10394.m12718();
                this.f10397 = true;
                messagetype = this.f10398;
            }
            MessageType messagetype3 = (zzffu) messagetype;
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) messagetype3.m12535(zzg.f10417, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z = true;
            } else if (byteValue == 0) {
                z = false;
            } else {
                if (messagetype3.m12535(zzg.f10418, (Object) Boolean.FALSE, (Object) null) == null) {
                    z2 = false;
                }
                if (booleanValue) {
                    messagetype3.m12535(zzg.f10416, (Object) z2 ? messagetype3 : null, (Object) null);
                }
                z = z2;
            }
            if (z) {
                return messagetype3;
            }
            throw new zzfim(messagetype3);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final MessageType m12543() {
            if (this.f10397) {
                return this.f10398;
            }
            MessageType messagetype = this.f10398;
            messagetype.m12535(zzg.f10405, (Object) null, (Object) null);
            messagetype.f10394.m12718();
            this.f10397 = true;
            return this.f10398;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* synthetic */ zzfel m12544() {
            return (zza) clone();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final BuilderType m12545(MessageType messagetype) {
            m12541();
            m12538(this.f10398, messagetype);
            return this;
        }
    }

    public static class zzb<T extends zzffu<T, ?>> extends zzfen<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        private T f10399;

        public zzb(T t) {
            this.f10399 = t;
        }
    }

    static class zzc implements zzh {

        /* renamed from: 靐  reason: contains not printable characters */
        private static zzffv f10400 = new zzffv();

        /* renamed from: 龘  reason: contains not printable characters */
        static final zzc f10401 = new zzc();

        private zzc() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final int m12547(boolean z, int i, boolean z2, int i2) {
            if (z == z2 && i == i2) {
                return i;
            }
            throw f10400;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzfes m12548(boolean z, zzfes zzfes, boolean z2, zzfes zzfes2) {
            if (z == z2 && zzfes.equals(zzfes2)) {
                return zzfes;
            }
            throw f10400;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final <T> zzfgd<T> m12549(zzfgd<T> zzfgd, zzfgd<T> zzfgd2) {
            if (zzfgd.equals(zzfgd2)) {
                return zzfgd;
            }
            throw f10400;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final <T extends zzfhe> T m12550(T t, T t2) {
            if (t == null && t2 == null) {
                return null;
            }
            if (t == null || t2 == null) {
                throw f10400;
            }
            T t3 = (zzffu) t;
            if (t3 == t2 || !((zzffu) t3.m12535(zzg.f10411, (Object) null, (Object) null)).getClass().isInstance(t2)) {
                return t;
            }
            zzffu zzffu = (zzffu) t2;
            t3.m12535(zzg.f10415, (Object) this, (Object) zzffu);
            t3.f10394 = m12573(t3.f10394, zzffu.f10394);
            return t;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzfio m12551(zzfio zzfio, zzfio zzfio2) {
            if (zzfio.equals(zzfio2)) {
                return zzfio;
            }
            throw f10400;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m12552(boolean z, String str, boolean z2, String str2) {
            if (z == z2 && str.equals(str2)) {
                return str;
            }
            throw f10400;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m12553(boolean z, boolean z2, boolean z3, boolean z4) {
            if (z == z3 && z2 == z4) {
                return z2;
            }
            throw f10400;
        }
    }

    public static abstract class zzd<MessageType extends zzd<MessageType, BuilderType>, BuilderType> extends zzffu<MessageType, BuilderType> implements zzfhg {

        /* renamed from: 麤  reason: contains not printable characters */
        protected zzffq<Object> f10402 = zzffq.m12512();
    }

    static class zze implements zzh {

        /* renamed from: 龘  reason: contains not printable characters */
        int f10403 = 0;

        zze() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final int m12554(boolean z, int i, boolean z2, int i2) {
            this.f10403 = (this.f10403 * 53) + i;
            return i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzfes m12555(boolean z, zzfes zzfes, boolean z2, zzfes zzfes2) {
            this.f10403 = (this.f10403 * 53) + zzfes.hashCode();
            return zzfes;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final <T> zzfgd<T> m12556(zzfgd<T> zzfgd, zzfgd<T> zzfgd2) {
            this.f10403 = (this.f10403 * 53) + zzfgd.hashCode();
            return zzfgd;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final <T extends zzfhe> T m12557(T t, T t2) {
            int i;
            if (t == null) {
                i = 37;
            } else if (t instanceof zzffu) {
                zzffu zzffu = (zzffu) t;
                if (zzffu.f10342 == 0) {
                    int i2 = this.f10403;
                    this.f10403 = 0;
                    zzffu.m12535(zzg.f10415, (Object) this, (Object) zzffu);
                    zzffu.f10394 = m12573(zzffu.f10394, zzffu.f10394);
                    zzffu.f10342 = this.f10403;
                    this.f10403 = i2;
                }
                i = zzffu.f10342;
            } else {
                i = t.hashCode();
            }
            this.f10403 = i + (this.f10403 * 53);
            return t;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzfio m12558(zzfio zzfio, zzfio zzfio2) {
            this.f10403 = (this.f10403 * 53) + zzfio.hashCode();
            return zzfio;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m12559(boolean z, String str, boolean z2, String str2) {
            this.f10403 = (this.f10403 * 53) + str.hashCode();
            return str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m12560(boolean z, boolean z2, boolean z3, boolean z4) {
            this.f10403 = (this.f10403 * 53) + zzffz.m12577(z2);
            return z2;
        }
    }

    public static class zzf implements zzh {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final zzf f10404 = new zzf();

        private zzf() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final int m12561(boolean z, int i, boolean z2, int i2) {
            return z2 ? i2 : i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzfes m12562(boolean z, zzfes zzfes, boolean z2, zzfes zzfes2) {
            return z2 ? zzfes2 : zzfes;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final <T> zzfgd<T> m12563(zzfgd<T> zzfgd, zzfgd<T> zzfgd2) {
            int size = zzfgd.size();
            int size2 = zzfgd2.size();
            if (size > 0 && size2 > 0) {
                if (!zzfgd.m12584()) {
                    zzfgd = zzfgd.m12583(size2 + size);
                }
                zzfgd.addAll(zzfgd2);
            }
            return size > 0 ? zzfgd : zzfgd2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final <T extends zzfhe> T m12564(T t, T t2) {
            return (t == null || t2 == null) ? t == null ? t2 : t : t.m12622().m12628(t2).m12627();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzfio m12565(zzfio zzfio, zzfio zzfio2) {
            return zzfio2 == zzfio.m12713() ? zzfio : zzfio.m12714(zzfio, zzfio2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m12566(boolean z, String str, boolean z2, String str2) {
            return z2 ? str2 : str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m12567(boolean z, boolean z2, boolean z3, boolean z4) {
            return z3 ? z4 : z2;
        }
    }

    /* 'enum' modifier removed */
    public static final class zzg {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final int f10405 = 6;

        /* renamed from: ʼ  reason: contains not printable characters */
        public static final int f10406 = 7;

        /* renamed from: ʽ  reason: contains not printable characters */
        public static final int f10407 = 8;

        /* renamed from: ʾ  reason: contains not printable characters */
        private static final /* synthetic */ int[] f10408 = {f10418, f10415, f10417, f10416, f10414, f10405, f10406, f10407, f10411, f10412};

        /* renamed from: ʿ  reason: contains not printable characters */
        private static final /* synthetic */ int[] f10409 = {f10413, f10410};

        /* renamed from: ˈ  reason: contains not printable characters */
        public static final int f10410 = 2;

        /* renamed from: ˑ  reason: contains not printable characters */
        public static final int f10411 = 9;

        /* renamed from: ٴ  reason: contains not printable characters */
        public static final int f10412 = 10;

        /* renamed from: ᐧ  reason: contains not printable characters */
        public static final int f10413 = 1;

        /* renamed from: 连任  reason: contains not printable characters */
        public static final int f10414 = 5;

        /* renamed from: 靐  reason: contains not printable characters */
        public static final int f10415 = 2;

        /* renamed from: 麤  reason: contains not printable characters */
        public static final int f10416 = 4;

        /* renamed from: 齉  reason: contains not printable characters */
        public static final int f10417 = 3;

        /* renamed from: 龘  reason: contains not printable characters */
        public static final int f10418 = 1;

        /* renamed from: 龘  reason: contains not printable characters */
        public static int[] m12568() {
            return (int[]) f10408.clone();
        }
    }

    public interface zzh {
        /* renamed from: 龘  reason: contains not printable characters */
        int m12569(boolean z, int i, boolean z2, int i2);

        /* renamed from: 龘  reason: contains not printable characters */
        zzfes m12570(boolean z, zzfes zzfes, boolean z2, zzfes zzfes2);

        /* renamed from: 龘  reason: contains not printable characters */
        <T> zzfgd<T> m12571(zzfgd<T> zzfgd, zzfgd<T> zzfgd2);

        /* renamed from: 龘  reason: contains not printable characters */
        <T extends zzfhe> T m12572(T t, T t2);

        /* renamed from: 龘  reason: contains not printable characters */
        zzfio m12573(zzfio zzfio, zzfio zzfio2);

        /* renamed from: 龘  reason: contains not printable characters */
        String m12574(boolean z, String str, boolean z2, String str2);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m12575(boolean z, boolean z2, boolean z3, boolean z4);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    protected static <E> zzfgd<E> m12525() {
        return zzfho.m12644();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static <T extends zzffu<T, ?>> T m12526(T t, zzfes zzfes) throws zzfge {
        boolean z;
        boolean z2;
        boolean z3 = true;
        T r1 = m12527(t, zzfes, zzffm.m12500());
        if (r1 != null) {
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) r1.m12535(zzg.f10417, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z2 = true;
            } else if (byteValue == 0) {
                z2 = false;
            } else {
                boolean z4 = r1.m12535(zzg.f10418, (Object) Boolean.FALSE, (Object) null) != null;
                if (booleanValue) {
                    r1.m12535(zzg.f10416, (Object) z4 ? r1 : null, (Object) null);
                }
                z2 = z4;
            }
            if (!z2) {
                throw new zzfim(r1).zzczt().zzi(r1);
            }
        }
        if (r1 != null) {
            boolean booleanValue2 = Boolean.TRUE.booleanValue();
            byte byteValue2 = ((Byte) r1.m12535(zzg.f10417, (Object) null, (Object) null)).byteValue();
            if (byteValue2 == 1) {
                z = true;
            } else if (byteValue2 == 0) {
                z = false;
            } else {
                if (r1.m12535(zzg.f10418, (Object) Boolean.FALSE, (Object) null) == null) {
                    z3 = false;
                }
                if (booleanValue2) {
                    r1.m12535(zzg.f10416, (Object) z3 ? r1 : null, (Object) null);
                }
                z = z3;
            }
            if (!z) {
                throw new zzfim(r1).zzczt().zzi(r1);
            }
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T extends zzffu<T, ?>> T m12527(T t, zzfes zzfes, zzffm zzffm) throws zzfge {
        T r1;
        try {
            zzffb zzcvm = zzfes.zzcvm();
            r1 = m12528(t, zzcvm, zzffm);
            zzcvm.m12417(0);
            return r1;
        } catch (zzfge e) {
            throw e.zzi(r1);
        } catch (zzfge e2) {
            throw e2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T extends zzffu<T, ?>> T m12528(T t, zzffb zzffb, zzffm zzffm) throws zzfge {
        T t2 = (zzffu) t.m12535(zzg.f10406, (Object) null, (Object) null);
        try {
            t2.m12535(zzg.f10414, (Object) zzffb, (Object) zzffm);
            t2.m12535(zzg.f10405, (Object) null, (Object) null);
            t2.f10394.m12718();
            return t2;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof zzfge) {
                throw ((zzfge) e.getCause());
            }
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static <T extends zzffu<T, ?>> T m12529(T t, byte[] bArr) throws zzfge {
        boolean z;
        boolean z2 = true;
        T r1 = m12530(t, bArr, zzffm.m12500());
        if (r1 != null) {
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) r1.m12535(zzg.f10417, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z = true;
            } else if (byteValue == 0) {
                z = false;
            } else {
                if (r1.m12535(zzg.f10418, (Object) Boolean.FALSE, (Object) null) == null) {
                    z2 = false;
                }
                if (booleanValue) {
                    r1.m12535(zzg.f10416, (Object) z2 ? r1 : null, (Object) null);
                }
                z = z2;
            }
            if (!z) {
                throw new zzfim(r1).zzczt().zzi(r1);
            }
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T extends zzffu<T, ?>> T m12530(T t, byte[] bArr, zzffm zzffm) throws zzfge {
        T r1;
        try {
            zzffb r0 = zzffb.m12398(bArr);
            r1 = m12528(t, r0, zzffm);
            r0.m12417(0);
            return r1;
        } catch (zzfge e) {
            throw e.zzi(r1);
        } catch (zzfge e2) {
            throw e2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Object m12531(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzffu) m12535(zzg.f10411, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        try {
            zzc zzc2 = zzc.f10401;
            zzffu zzffu = (zzffu) obj;
            m12535(zzg.f10415, (Object) zzc2, (Object) zzffu);
            this.f10394 = zzc2.m12573(this.f10394, zzffu.f10394);
            return true;
        } catch (zzffv e) {
            return false;
        }
    }

    public int hashCode() {
        if (this.f10342 != 0) {
            return this.f10342;
        }
        zze zze2 = new zze();
        m12535(zzg.f10415, (Object) zze2, (Object) this);
        this.f10394 = zze2.m12573(this.f10394, this.f10394);
        this.f10342 = zze2.f10403;
        return this.f10342;
    }

    public String toString() {
        return zzfhh.m12630(this, super.toString());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* synthetic */ zzfhf m12532() {
        zza zza2 = (zza) m12535(zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(this);
        return zza2;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* synthetic */ zzfhe m12533() {
        return (zzffu) m12535(zzg.f10411, (Object) null, (Object) null);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m12534() {
        if (this.f10395 == -1) {
            this.f10395 = zzfhn.m12641().m12643(getClass()).m12669(this);
        }
        return this.f10395;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Object m12535(int i, Object obj, Object obj2);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m12536(zzffg zzffg) throws IOException {
        zzfhn.m12641().m12643(getClass()).m12670(this, zzffi.m12493(zzffg));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12537(int i, zzffb zzffb) throws IOException {
        if ((i & 7) == 4) {
            return false;
        }
        if (this.f10394 == zzfio.m12713()) {
            this.f10394 = zzfio.m12712();
        }
        return this.f10394.m12722(i, zzffb);
    }
}
