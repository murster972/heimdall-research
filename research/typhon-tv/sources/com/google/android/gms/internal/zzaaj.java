package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzaaj {
    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4242(Context context, boolean z) {
        if (!z) {
            return true;
        }
        zzbs.zzei();
        int r2 = zzahn.m4567(context);
        if (r2 == 0) {
            return false;
        }
        zzbs.zzei();
        return r2 <= zzahn.m4623(context);
    }
}
