package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;

final class zzde implements zzdi {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f9861;

    zzde(zzda zzda, Activity activity) {
        this.f9861 = activity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11588(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityPaused(this.f9861);
    }
}
