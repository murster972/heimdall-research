package com.google.android.gms.internal;

import android.content.DialogInterface;

final class zzwq implements DialogInterface.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzwo f10952;

    zzwq(zzwo zzwo) {
        this.f10952 = zzwo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f10952.m6012("Operation denied by user.");
    }
}
