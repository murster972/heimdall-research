package com.google.android.gms.internal;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class zzckt implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9604;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzckg f9605;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ boolean f9606;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AtomicReference f9607;

    zzckt(zzckg zzckg, AtomicReference atomicReference, zzcgi zzcgi, boolean z) {
        this.f9605 = zzckg;
        this.f9607 = atomicReference;
        this.f9604 = zzcgi;
        this.f9606 = z;
    }

    public final void run() {
        synchronized (this.f9607) {
            try {
                zzche r0 = this.f9605.f9558;
                if (r0 == null) {
                    this.f9605.m11096().m10832().m10849("Failed to get user properties");
                    this.f9607.notify();
                    return;
                }
                this.f9607.set(r0.m10677(this.f9604, this.f9606));
                this.f9605.m11223();
                this.f9607.notify();
            } catch (RemoteException e) {
                this.f9605.m11096().m10832().m10850("Failed to get user properties", e);
                this.f9607.notify();
            } catch (Throwable th) {
                this.f9607.notify();
                throw th;
            }
        }
    }
}
