package com.google.android.gms.internal;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;
import java.util.HashMap;

final class zzjy extends zzjr.zza<zzpz> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ HashMap f10779;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzjr f10780;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ HashMap f10781;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ View f10782;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjy(zzjr zzjr, View view, HashMap hashMap, HashMap hashMap2) {
        super();
        this.f10780 = zzjr;
        this.f10782 = view;
        this.f10779 = hashMap;
        this.f10781 = hashMap2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13047() throws RemoteException {
        zzpz r0 = this.f10780.f4805.m5800(this.f10782, this.f10779, this.f10781);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10782.getContext(), "native_ad_view_holder_delegate");
        return new zzmk();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13048(zzla zzla) throws RemoteException {
        return zzla.createNativeAdViewHolderDelegate(zzn.m9306(this.f10782), zzn.m9306(this.f10779), zzn.m9306(this.f10781));
    }
}
