package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbr;

public final class zzcxo extends zzbfm {
    public static final Parcelable.Creator<zzcxo> CREATOR = new zzcxp();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzbr f9840;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9841;

    zzcxo(int i, zzbr zzbr) {
        this.f9841 = i;
        this.f9840 = zzbr;
    }

    public zzcxo(zzbr zzbr) {
        this(1, zzbr);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9841);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f9840, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
