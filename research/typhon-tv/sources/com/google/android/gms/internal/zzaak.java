package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.util.zzi;

final class zzaak implements zzaam {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8052;

    zzaak(Context context) {
        this.f8052 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9437(zzakd zzakd) {
        zzkb.m5487();
        boolean r3 = zzajr.m4756(this.f8052);
        boolean z = ((Boolean) zzkb.m5481().m5595(zznh.f5041)).booleanValue() && zzakd.f4295;
        if (zzaaj.m4242(this.f8052, zzakd.f4295) && r3 && !z) {
            if (!zzi.m9256(this.f8052)) {
                return false;
            }
            if (((Boolean) zzkb.m5481().m5595(zznh.f5160)).booleanValue()) {
                return false;
            }
        }
        return true;
    }
}
