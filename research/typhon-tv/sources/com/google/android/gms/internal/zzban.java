package com.google.android.gms.internal;

import android.widget.TextView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import java.util.ArrayList;
import java.util.List;

public final class zzban extends UIController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<String> f8569 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    private final TextView f8570;

    public zzban(TextView textView, List<String> list) {
        this.f8570 = textView;
        this.f8569.addAll(list);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9899() {
        MediaInfo r0;
        MediaMetadata r1;
        RemoteMediaClient r02 = m8226();
        if (r02 != null && r02.m4143() && (r0 = r02.m4136().m7913()) != null && (r1 = r0.m7846()) != null) {
            for (String next : this.f8569) {
                if (r1.m7885(next)) {
                    this.f8570.setText(r1.m7876(next));
                    return;
                }
            }
            this.f8570.setText("");
        }
    }
}
