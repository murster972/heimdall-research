package com.google.android.gms.internal;

final /* synthetic */ class zzfix {

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ int[] f10518 = new int[zzfiy.values().length];

    static {
        try {
            f10518[zzfiy.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f10518[zzfiy.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f10518[zzfiy.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f10518[zzfiy.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f10518[zzfiy.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f10518[zzfiy.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f10518[zzfiy.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f10518[zzfiy.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f10518[zzfiy.BYTES.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f10518[zzfiy.UINT32.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f10518[zzfiy.SFIXED32.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f10518[zzfiy.SFIXED64.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f10518[zzfiy.SINT32.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f10518[zzfiy.SINT64.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f10518[zzfiy.STRING.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f10518[zzfiy.GROUP.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f10518[zzfiy.MESSAGE.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
        try {
            f10518[zzfiy.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError e18) {
        }
    }
}
