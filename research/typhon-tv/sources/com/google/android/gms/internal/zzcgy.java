package com.google.android.gms.internal;

import java.util.Iterator;

final class zzcgy implements Iterator<String> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcgx f9197;

    /* renamed from: 龘  reason: contains not printable characters */
    private Iterator<String> f9198 = this.f9197.f9196.keySet().iterator();

    zzcgy(zzcgx zzcgx) {
        this.f9197 = zzcgx;
    }

    public final boolean hasNext() {
        return this.f9198.hasNext();
    }

    public final /* synthetic */ Object next() {
        return this.f9198.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
