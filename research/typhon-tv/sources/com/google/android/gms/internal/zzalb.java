package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

final class zzalb implements Executor {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Handler f8294 = new Handler(Looper.getMainLooper());

    zzalb() {
    }

    public final void execute(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            runnable.run();
        } else {
            this.f8294.post(runnable);
        }
    }
}
