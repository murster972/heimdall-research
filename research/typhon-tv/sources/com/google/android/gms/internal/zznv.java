package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.ads.internal.zzae;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zznv extends zzny {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f5194;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f5195;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzae f5196;

    public zznv(zzae zzae, String str, String str2) {
        this.f5196 = zzae;
        this.f5194 = str;
        this.f5195 = str2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5632() {
        return this.f5195;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5633() {
        this.f5196.zzct();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5634() {
        this.f5196.zzcs();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5635() {
        return this.f5194;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5636(IObjectWrapper iObjectWrapper) {
        if (iObjectWrapper != null) {
            this.f5196.zzh((View) zzn.m9307(iObjectWrapper));
        }
    }
}
