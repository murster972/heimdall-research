package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdrg extends zzffu<zzdrg, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdrg f9971;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdrg> f9972;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f9973 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdrk f9974;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9975;

    public static final class zza extends zzffu.zza<zzdrg, zza> implements zzfhg {
        private zza() {
            super(zzdrg.f9971);
        }

        /* synthetic */ zza(zzdrh zzdrh) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11810(int i) {
            m12541();
            ((zzdrg) this.f10398).m11799(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11811(zzdrk zzdrk) {
            m12541();
            ((zzdrg) this.f10398).m11803(zzdrk);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11812(zzfes zzfes) {
            m12541();
            ((zzdrg) this.f10398).m11797(zzfes);
            return this;
        }
    }

    static {
        zzdrg zzdrg = new zzdrg();
        f9971 = zzdrg;
        zzdrg.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdrg.f10394.m12718();
    }

    private zzdrg() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzdrg m11794() {
        return f9971;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zza m11796() {
        zzdrg zzdrg = f9971;
        zzffu.zza zza2 = (zzffu.zza) zzdrg.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdrg);
        return (zza) zza2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11797(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f9973 = zzfes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdrg m11798(zzfes zzfes) throws zzfge {
        return (zzdrg) zzffu.m12526(f9971, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11799(int i) {
        this.f9975 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11803(zzdrk zzdrk) {
        if (zzdrk == null) {
            throw new NullPointerException();
        }
        this.f9974 = zzdrk;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdrk m11804() {
        return this.f9974 == null ? zzdrk.m11821() : this.f9974;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11805() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9975 != 0) {
            i2 = zzffg.m5245(1, this.f9975) + 0;
        }
        if (this.f9974 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f9974 == null ? zzdrk.m11821() : this.f9974);
        }
        if (!this.f9973.isEmpty()) {
            i2 += zzffg.m5261(3, this.f9973);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m11806() {
        return this.f9973;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11807() {
        return this.f9975;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11808(int i, Object obj, Object obj2) {
        zzdrk.zza zza2;
        boolean z = true;
        switch (zzdrh.f9976[i - 1]) {
            case 1:
                return new zzdrg();
            case 2:
                return f9971;
            case 3:
                return null;
            case 4:
                return new zza((zzdrh) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdrg zzdrg = (zzdrg) obj2;
                this.f9975 = zzh.m12569(this.f9975 != 0, this.f9975, zzdrg.f9975 != 0, zzdrg.f9975);
                this.f9974 = (zzdrk) zzh.m12572(this.f9974, zzdrg.f9974);
                boolean z2 = this.f9973 != zzfes.zzpfg;
                zzfes zzfes = this.f9973;
                if (zzdrg.f9973 == zzfes.zzpfg) {
                    z = false;
                }
                this.f9973 = zzh.m12570(z2, zzfes, z, zzdrg.f9973);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f9975 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f9974 != null) {
                                        zzdrk zzdrk = this.f9974;
                                        zzffu.zza zza3 = (zzffu.zza) zzdrk.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdrk);
                                        zza2 = (zzdrk.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f9974 = (zzdrk) zzffb.m12416(zzdrk.m11821(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f9974);
                                        this.f9974 = (zzdrk) zza2.m12543();
                                        break;
                                    }
                                case 26:
                                    this.f9973 = zzffb.m12401();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f9972 == null) {
                    synchronized (zzdrg.class) {
                        if (f9972 == null) {
                            f9972 = new zzffu.zzb(f9971);
                        }
                    }
                }
                return f9972;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f9971;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11809(zzffg zzffg) throws IOException {
        if (this.f9975 != 0) {
            zzffg.m5281(1, this.f9975);
        }
        if (this.f9974 != null) {
            zzffg.m5289(2, (zzfhe) this.f9974 == null ? zzdrk.m11821() : this.f9974);
        }
        if (!this.f9973.isEmpty()) {
            zzffg.m5288(3, this.f9973);
        }
        this.f10394.m12719(zzffg);
    }
}
