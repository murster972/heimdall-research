package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzlm extends zzev implements zzll {
    public zzlm() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IVideoController");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzll m13094(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
        return queryLocalInterface instanceof zzll ? (zzll) queryLocalInterface : new zzln(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zzlo zzlq;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m13091();
                parcel2.writeNoException();
                break;
            case 2:
                m13088();
                parcel2.writeNoException();
                break;
            case 3:
                m13093(zzew.m12308(parcel));
                parcel2.writeNoException();
                break;
            case 4:
                boolean r0 = m13090();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r0);
                break;
            case 5:
                int r02 = m13089();
                parcel2.writeNoException();
                parcel2.writeInt(r02);
                break;
            case 6:
                float r03 = m13082();
                parcel2.writeNoException();
                parcel2.writeFloat(r03);
                break;
            case 7:
                float r04 = m13083();
                parcel2.writeNoException();
                parcel2.writeFloat(r04);
                break;
            case 8:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    zzlq = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
                    zzlq = queryLocalInterface instanceof zzlo ? (zzlo) queryLocalInterface : new zzlq(readStrongBinder);
                }
                m13092(zzlq);
                parcel2.writeNoException();
                break;
            case 9:
                float r05 = m13087();
                parcel2.writeNoException();
                parcel2.writeFloat(r05);
                break;
            case 10:
                boolean r06 = m13085();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r06);
                break;
            case 11:
                zzlo r07 = m13084();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r07);
                break;
            case 12:
                boolean r08 = m13086();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r08);
                break;
            default:
                return false;
        }
        return true;
    }
}
