package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzwd implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10939;

    zzwd(zzvx zzvx) {
        this.f10939 = zzvx;
    }

    public final void run() {
        try {
            this.f10939.f5502.m13512();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLeftApplication.", e);
        }
    }
}
