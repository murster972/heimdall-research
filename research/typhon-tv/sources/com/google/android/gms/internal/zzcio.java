package com.google.android.gms.internal;

import java.util.concurrent.Callable;

final class zzcio implements Callable<String> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcim f9425;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f9426;

    zzcio(zzcim zzcim, String str) {
        this.f9425 = zzcim;
        this.f9426 = str;
    }

    public final /* synthetic */ Object call() throws Exception {
        zzcgh r0 = this.f9425.m11024().m10592(this.f9426);
        if (r0 != null) {
            return r0.m10502();
        }
        this.f9425.m11016().m10834().m10849("App info was null when attempting to get app instance id");
        return null;
    }
}
