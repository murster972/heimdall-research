package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;

public final class zzchl extends zzd<zzche> {
    public zzchl(Context context, Looper looper, zzf zzf, zzg zzg) {
        super(context, looper, 93, zzf, zzg, (String) null);
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.measurement.START";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10809() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m10810(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        return queryLocalInterface instanceof zzche ? (zzche) queryLocalInterface : new zzchg(iBinder);
    }
}
