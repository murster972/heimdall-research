package com.google.android.gms.internal;

import com.mopub.mobileads.VastIconXmlManager;
import org.apache.oltu.oauth2.common.OAuth;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public class zzxb {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f5555;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzanh f5556;

    public zzxb(zzanh zzanh) {
        this(zzanh, "");
    }

    public zzxb(zzanh zzanh, String str) {
        this.f5556 = zzanh;
        this.f5555 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m6007(int i, int i2, int i3, int i4) {
        try {
            this.f5556.zza("onDefaultPositionReceived", new JSONObject().put("x", i).put("y", i2).put(VastIconXmlManager.WIDTH, i3).put(VastIconXmlManager.HEIGHT, i4));
        } catch (JSONException e) {
            zzagf.m4793("Error occured while dispatching default position.", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m6008(String str) {
        try {
            this.f5556.zza("onReadyEventReceived", new JSONObject().put("js", (Object) str));
        } catch (JSONException e) {
            zzagf.m4793("Error occured while dispatching ready Event.", e);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m6009(String str) {
        try {
            this.f5556.zza("onStateChanged", new JSONObject().put(OAuth.OAUTH_STATE, (Object) str));
        } catch (JSONException e) {
            zzagf.m4793("Error occured while dispatching state change.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6010(int i, int i2, int i3, int i4) {
        try {
            this.f5556.zza("onSizeChanged", new JSONObject().put("x", i).put("y", i2).put(VastIconXmlManager.WIDTH, i3).put(VastIconXmlManager.HEIGHT, i4));
        } catch (JSONException e) {
            zzagf.m4793("Error occured while dispatching size change.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6011(int i, int i2, int i3, int i4, float f, int i5) {
        try {
            this.f5556.zza("onScreenInfoChanged", new JSONObject().put(VastIconXmlManager.WIDTH, i).put(VastIconXmlManager.HEIGHT, i2).put("maxSizeWidth", i3).put("maxSizeHeight", i4).put("density", (double) f).put("rotation", i5));
        } catch (JSONException e) {
            zzagf.m4793("Error occured while obtaining screen information.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6012(String str) {
        try {
            this.f5556.zza("onError", new JSONObject().put("message", (Object) str).put("action", (Object) this.f5555));
        } catch (JSONException e) {
            zzagf.m4793("Error occurred while dispatching error event.", e);
        }
    }
}
