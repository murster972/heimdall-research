package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Map;

final class zzany implements zzt<zzanh> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzanx f8365;

    zzany(zzanx zzanx) {
        this.f8365 = zzanx;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        if (map != null) {
            String str = (String) map.get(VastIconXmlManager.HEIGHT);
            if (!TextUtils.isEmpty(str)) {
                try {
                    int parseInt = Integer.parseInt(str);
                    synchronized (this.f8365.f4509) {
                        if (this.f8365.f4502 != parseInt) {
                            int unused = this.f8365.f4502 = parseInt;
                            this.f8365.requestLayout();
                        }
                    }
                } catch (Exception e) {
                    zzagf.m4796("Exception occurred while getting webview content height", e);
                }
            }
        }
    }
}
