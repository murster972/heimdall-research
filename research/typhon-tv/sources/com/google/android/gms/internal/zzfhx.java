package com.google.android.gms.internal;

final class zzfhx {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final zzfin<?, ?> f10465 = m12677(false);

    /* renamed from: 麤  reason: contains not printable characters */
    private static final zzfin<?, ?> f10466 = new zzfip();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final zzfin<?, ?> f10467 = m12677(true);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Class<?> f10468 = m12674();

    /* renamed from: 连任  reason: contains not printable characters */
    private static Class<?> m12672() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzfin<?, ?> m12673() {
        return f10467;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static Class<?> m12674() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zzfin<?, ?> m12675() {
        return f10466;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfin<?, ?> m12676() {
        return f10465;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzfin<?, ?> m12677(boolean z) {
        try {
            Class<?> r0 = m12672();
            if (r0 == null) {
                return null;
            }
            return (zzfin) r0.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12678(Class<?> cls) {
        if (!zzffu.class.isAssignableFrom(cls) && f10468 != null && !f10468.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }
}
