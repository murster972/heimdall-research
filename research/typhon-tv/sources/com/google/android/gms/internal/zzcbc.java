package com.google.android.gms.internal;

import android.os.StrictMode;
import java.util.concurrent.Callable;

public final class zzcbc {
    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m10311(Callable<T> callable) throws Exception {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        try {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.LAX);
            T call = callable.call();
            StrictMode.setThreadPolicy(threadPolicy);
            return call;
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(threadPolicy);
            throw th;
        }
    }
}
