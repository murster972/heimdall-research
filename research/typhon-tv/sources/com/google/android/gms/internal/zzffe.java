package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class zzffe extends zzffb {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10364;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f10365;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f10366;

    /* renamed from: ˈ  reason: contains not printable characters */
    private zzfff f10367;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f10368;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f10369;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f10370;

    /* renamed from: 连任  reason: contains not printable characters */
    private final byte[] f10371;

    /* renamed from: 麤  reason: contains not printable characters */
    private final InputStream f10372;

    private zzffe(InputStream inputStream, int i) {
        super();
        this.f10370 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        this.f10367 = null;
        zzffz.m12580(inputStream, "input");
        this.f10372 = inputStream;
        this.f10371 = new byte[i];
        this.f10364 = 0;
        this.f10366 = 0;
        this.f10369 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean m12441(int i) throws IOException {
        while (this.f10366 + i > this.f10364) {
            if (i > (this.f10354 - this.f10369) - this.f10366 || this.f10369 + this.f10366 + i > this.f10370) {
                return false;
            }
            int i2 = this.f10366;
            if (i2 > 0) {
                if (this.f10364 > i2) {
                    System.arraycopy(this.f10371, i2, this.f10371, 0, this.f10364 - i2);
                }
                this.f10369 += i2;
                this.f10364 -= i2;
                this.f10366 = 0;
            }
            int read = this.f10372.read(this.f10371, this.f10364, Math.min(this.f10371.length - this.f10364, (this.f10354 - this.f10369) - this.f10364));
            if (read == 0 || read < -1 || read > this.f10371.length) {
                throw new IllegalStateException(new StringBuilder(102).append("InputStream#read(byte[]) returned invalid result: ").append(read).append("\nThe InputStream implementation is buggy.").toString());
            } else if (read <= 0) {
                return false;
            } else {
                this.f10364 = read + this.f10364;
                m12445();
                if (this.f10364 >= i) {
                    return true;
                }
            }
        }
        throw new IllegalStateException(new StringBuilder(77).append("refillBuffer() called when ").append(i).append(" bytes were already available in buffer").toString());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final byte[] m12442(int i) throws IOException {
        byte[] r0 = m12443(i);
        if (r0 != null) {
            return r0;
        }
        int i2 = this.f10366;
        int i3 = this.f10364 - this.f10366;
        this.f10369 += this.f10364;
        this.f10366 = 0;
        this.f10364 = 0;
        List<byte[]> r3 = m12447(i - i3);
        byte[] bArr = new byte[i];
        System.arraycopy(this.f10371, i2, bArr, 0, i3);
        Iterator<byte[]> it2 = r3.iterator();
        while (true) {
            int i4 = i3;
            if (!it2.hasNext()) {
                return bArr;
            }
            byte[] next = it2.next();
            System.arraycopy(next, 0, bArr, i4, next.length);
            i3 = next.length + i4;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final byte[] m12443(int i) throws IOException {
        if (i == 0) {
            return zzffz.f10420;
        }
        if (i < 0) {
            throw zzfge.m12590();
        }
        int i2 = this.f10369 + this.f10366 + i;
        if (i2 - this.f10354 > 0) {
            throw zzfge.m12587();
        } else if (i2 > this.f10370) {
            m12412((this.f10370 - this.f10369) - this.f10366);
            throw zzfge.m12593();
        } else {
            int i3 = this.f10364 - this.f10366;
            int i4 = i - i3;
            if (i4 >= 4096 && i4 > this.f10372.available()) {
                return null;
            }
            byte[] bArr = new byte[i];
            System.arraycopy(this.f10371, this.f10366, bArr, 0, i3);
            this.f10369 += this.f10364;
            this.f10366 = 0;
            this.f10364 = 0;
            while (i3 < bArr.length) {
                int read = this.f10372.read(bArr, i3, i - i3);
                if (read == -1) {
                    throw zzfge.m12593();
                }
                this.f10369 += read;
                i3 += read;
            }
            return bArr;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b2, code lost:
        if (((long) r4[r3]) < 0) goto L_0x00b4;
     */
    /* renamed from: ʿ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final long m12444() throws java.io.IOException {
        /*
            r10 = this;
            r8 = 0
            int r0 = r10.f10366
            int r1 = r10.f10364
            if (r1 == r0) goto L_0x00b4
            byte[] r4 = r10.f10371
            int r1 = r0 + 1
            byte r0 = r4[r0]
            if (r0 < 0) goto L_0x0014
            r10.f10366 = r1
            long r0 = (long) r0
        L_0x0013:
            return r0
        L_0x0014:
            int r2 = r10.f10364
            int r2 = r2 - r1
            r3 = 9
            if (r2 < r3) goto L_0x00b4
            int r2 = r1 + 1
            byte r1 = r4[r1]
            int r1 = r1 << 7
            r0 = r0 ^ r1
            if (r0 >= 0) goto L_0x002a
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
            long r0 = (long) r0
        L_0x0027:
            r10.f10366 = r2
            goto L_0x0013
        L_0x002a:
            int r3 = r2 + 1
            byte r1 = r4[r2]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0038
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            long r0 = (long) r0
            r2 = r3
            goto L_0x0027
        L_0x0038:
            int r2 = r3 + 1
            byte r1 = r4[r3]
            int r1 = r1 << 21
            r0 = r0 ^ r1
            if (r0 >= 0) goto L_0x0047
            r1 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r1
            long r0 = (long) r0
            goto L_0x0027
        L_0x0047:
            long r0 = (long) r0
            int r3 = r2 + 1
            byte r2 = r4[r2]
            long r6 = (long) r2
            r2 = 28
            long r6 = r6 << r2
            long r0 = r0 ^ r6
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 < 0) goto L_0x005b
            r4 = 266354560(0xfe03f80, double:1.315966377E-315)
            long r0 = r0 ^ r4
            r2 = r3
            goto L_0x0027
        L_0x005b:
            int r2 = r3 + 1
            byte r3 = r4[r3]
            long r6 = (long) r3
            r3 = 35
            long r6 = r6 << r3
            long r0 = r0 ^ r6
            int r3 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x006f
            r4 = -34093383808(0xfffffff80fe03f80, double:NaN)
            long r0 = r0 ^ r4
            goto L_0x0027
        L_0x006f:
            int r3 = r2 + 1
            byte r2 = r4[r2]
            long r6 = (long) r2
            r2 = 42
            long r6 = r6 << r2
            long r0 = r0 ^ r6
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 < 0) goto L_0x0084
            r4 = 4363953127296(0x3f80fe03f80, double:2.1560793202584E-311)
            long r0 = r0 ^ r4
            r2 = r3
            goto L_0x0027
        L_0x0084:
            int r2 = r3 + 1
            byte r3 = r4[r3]
            long r6 = (long) r3
            r3 = 49
            long r6 = r6 << r3
            long r0 = r0 ^ r6
            int r3 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x0098
            r4 = -558586000294016(0xfffe03f80fe03f80, double:NaN)
            long r0 = r0 ^ r4
            goto L_0x0027
        L_0x0098:
            int r3 = r2 + 1
            byte r2 = r4[r2]
            long r6 = (long) r2
            r2 = 56
            long r6 = r6 << r2
            long r0 = r0 ^ r6
            r6 = 71499008037633920(0xfe03f80fe03f80, double:6.838959413692434E-304)
            long r0 = r0 ^ r6
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 >= 0) goto L_0x00ba
            int r2 = r3 + 1
            byte r3 = r4[r3]
            long r4 = (long) r3
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x0027
        L_0x00b4:
            long r0 = r10.m12407()
            goto L_0x0013
        L_0x00ba:
            r2 = r3
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffe.m12444():long");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private final void m12445() {
        this.f10364 += this.f10365;
        int i = this.f10369 + this.f10364;
        if (i > this.f10370) {
            this.f10365 = i - this.f10370;
            this.f10364 -= this.f10365;
            return;
        }
        this.f10365 = 0;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private final byte m12446() throws IOException {
        if (this.f10366 == this.f10364) {
            m12448(1);
        }
        byte[] bArr = this.f10371;
        int i = this.f10366;
        this.f10366 = i + 1;
        return bArr[i];
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final List<byte[]> m12447(int i) throws IOException {
        ArrayList arrayList = new ArrayList();
        while (i > 0) {
            byte[] bArr = new byte[Math.min(i, 4096)];
            int i2 = 0;
            while (i2 < bArr.length) {
                int read = this.f10372.read(bArr, i2, bArr.length - i2);
                if (read == -1) {
                    throw zzfge.m12593();
                }
                this.f10369 += read;
                i2 += read;
            }
            i -= bArr.length;
            arrayList.add(bArr);
        }
        return arrayList;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m12448(int i) throws IOException {
        if (m12441(i)) {
            return;
        }
        if (i > (this.f10354 - this.f10369) - this.f10366) {
            throw zzfge.m12587();
        }
        throw zzfge.m12593();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int m12449() throws IOException {
        int i = this.f10366;
        if (this.f10364 - i < 4) {
            m12448(4);
            i = this.f10366;
        }
        byte[] bArr = this.f10371;
        this.f10366 = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final long m12450() throws IOException {
        int i = this.f10366;
        if (this.f10364 - i < 8) {
            m12448(8);
            i = this.f10366;
        }
        byte[] bArr = this.f10371;
        this.f10366 = i + 8;
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m12451() throws IOException {
        byte[] r2;
        int r3 = m12406();
        int i = this.f10366;
        if (r3 <= this.f10364 - i && r3 > 0) {
            byte[] bArr = this.f10371;
            this.f10366 = i + r3;
            r2 = bArr;
        } else if (r3 == 0) {
            return "";
        } else {
            if (r3 <= this.f10364) {
                m12448(r3);
                r2 = this.f10371;
                this.f10366 = r3;
                i = 0;
            } else {
                r2 = m12442(r3);
                i = 0;
            }
        }
        if (zzfis.m12769(r2, i, i + r3)) {
            return new String(r2, i, r3, zzffz.f10423);
        }
        throw zzfge.m12588();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final zzfes m12452() throws IOException {
        int r1 = m12406();
        if (r1 <= this.f10364 - this.f10366 && r1 > 0) {
            zzfes zze = zzfes.zze(this.f10371, this.f10366, r1);
            this.f10366 = r1 + this.f10366;
            return zze;
        } else if (r1 == 0) {
            return zzfes.zzpfg;
        } else {
            byte[] r0 = m12443(r1);
            if (r0 != null) {
                return zzfes.m12372(r0);
            }
            int i = this.f10366;
            int i2 = this.f10364 - this.f10366;
            this.f10369 += this.f10364;
            this.f10366 = 0;
            this.f10364 = 0;
            List<byte[]> r12 = m12447(r1 - i2);
            ArrayList arrayList = new ArrayList(r12.size() + 1);
            arrayList.add(zzfes.zze(this.f10371, i, i2));
            for (byte[] r02 : r12) {
                arrayList.add(zzfes.m12372(r02));
            }
            return zzfes.zzf(arrayList);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int m12453() throws IOException {
        return m12406();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int m12454() {
        return this.f10369 + this.f10366;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean m12455() throws IOException {
        return this.f10366 == this.f10364 && !m12441(1);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m12456() throws IOException {
        return m12406();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006a, code lost:
        if (r3[r2] < 0) goto L_0x006c;
     */
    /* renamed from: ٴ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int m12457() throws java.io.IOException {
        /*
            r5 = this;
            int r0 = r5.f10366
            int r1 = r5.f10364
            if (r1 == r0) goto L_0x006c
            byte[] r3 = r5.f10371
            int r2 = r0 + 1
            byte r0 = r3[r0]
            if (r0 < 0) goto L_0x0011
            r5.f10366 = r2
        L_0x0010:
            return r0
        L_0x0011:
            int r1 = r5.f10364
            int r1 = r1 - r2
            r4 = 9
            if (r1 < r4) goto L_0x006c
            int r1 = r2 + 1
            byte r2 = r3[r2]
            int r2 = r2 << 7
            r0 = r0 ^ r2
            if (r0 >= 0) goto L_0x0026
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
        L_0x0023:
            r5.f10366 = r1
            goto L_0x0010
        L_0x0026:
            int r2 = r1 + 1
            byte r1 = r3[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0033
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            r1 = r2
            goto L_0x0023
        L_0x0033:
            int r1 = r2 + 1
            byte r2 = r3[r2]
            int r2 = r2 << 21
            r0 = r0 ^ r2
            if (r0 >= 0) goto L_0x0041
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0023
        L_0x0041:
            int r2 = r1 + 1
            byte r1 = r3[r1]
            int r4 = r1 << 28
            r0 = r0 ^ r4
            r4 = 266354560(0xfe03f80, float:2.2112565E-29)
            r0 = r0 ^ r4
            if (r1 >= 0) goto L_0x0072
            int r1 = r2 + 1
            byte r2 = r3[r2]
            if (r2 >= 0) goto L_0x0023
            int r2 = r1 + 1
            byte r1 = r3[r1]
            if (r1 >= 0) goto L_0x0072
            int r1 = r2 + 1
            byte r2 = r3[r2]
            if (r2 >= 0) goto L_0x0023
            int r2 = r1 + 1
            byte r1 = r3[r1]
            if (r1 >= 0) goto L_0x0072
            int r1 = r2 + 1
            byte r2 = r3[r2]
            if (r2 >= 0) goto L_0x0023
        L_0x006c:
            long r0 = r5.m12407()
            int r0 = (int) r0
            goto L_0x0010
        L_0x0072:
            r1 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffe.m12457():int");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final long m12458() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte r1 = m12446();
            j |= ((long) (r1 & Byte.MAX_VALUE)) << i;
            if ((r1 & 128) == 0) {
                return j;
            }
        }
        throw zzfge.m12592();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m12459() throws IOException {
        return m12444() != 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m12460(int i) throws zzfge {
        if (i < 0) {
            throw zzfge.m12590();
        }
        int i2 = this.f10369 + this.f10366 + i;
        int i3 = this.f10370;
        if (i2 > i3) {
            throw zzfge.m12593();
        }
        this.f10370 = i2;
        m12445();
        return i3;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m12461() throws IOException {
        return m12444();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12462() throws IOException {
        return m12449();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m12463(int i) throws IOException {
        if (i <= this.f10364 - this.f10366 && i >= 0) {
            this.f10366 += i;
        } else if (i < 0) {
            throw zzfge.m12590();
        } else if (this.f10369 + this.f10366 + i > this.f10370) {
            m12412((this.f10370 - this.f10369) - this.f10366);
            throw zzfge.m12593();
        } else {
            int i2 = this.f10364 - this.f10366;
            this.f10366 = this.f10364;
            m12448(1);
            while (i - i2 > this.f10364) {
                i2 += this.f10364;
                this.f10366 = this.f10364;
                m12448(1);
            }
            this.f10366 = i - i2;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m12464() throws IOException {
        return m12450();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12465(int i) {
        this.f10370 = i;
        m12445();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12466() throws IOException {
        if (m12404()) {
            this.f10368 = 0;
            return 0;
        }
        this.f10368 = m12406();
        if ((this.f10368 >>> 3) != 0) {
            return this.f10368;
        }
        throw zzfge.m12591();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends zzffu<T, ?>> T m12467(T t, zzffm zzffm) throws IOException {
        int r0 = m12406();
        if (this.f10355 >= this.f10352) {
            throw zzfge.m12586();
        }
        int r02 = m12409(r0);
        this.f10355++;
        T r1 = zzffu.m12528(t, (zzffb) this, zzffm);
        m12417(0);
        this.f10355--;
        m12414(r02);
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12468(int i) throws zzfge {
        if (this.f10368 != i) {
            throw zzfge.m12589();
        }
    }
}
