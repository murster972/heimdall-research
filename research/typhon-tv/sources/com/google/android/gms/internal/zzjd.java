package com.google.android.gms.internal;

import java.io.IOException;

public final class zzjd extends zzfjm<zzjd> {

    /* renamed from: 靐  reason: contains not printable characters */
    public Integer f10751 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Integer f10752 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f10753 = null;

    public zzjd() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m13028() {
        int r0 = super.m12841();
        if (this.f10753 != null) {
            r0 += zzfjk.m12806(1, this.f10753.intValue());
        }
        if (this.f10751 != null) {
            r0 += zzfjk.m12806(2, this.f10751.intValue());
        }
        return this.f10752 != null ? r0 + zzfjk.m12806(3, this.f10752.intValue()) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m13029(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f10753 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 16:
                    this.f10751 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 24:
                    this.f10752 = Integer.valueOf(zzfjj.m12785());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13030(zzfjk zzfjk) throws IOException {
        if (this.f10753 != null) {
            zzfjk.m12832(1, this.f10753.intValue());
        }
        if (this.f10751 != null) {
            zzfjk.m12832(2, this.f10751.intValue());
        }
        if (this.f10752 != null) {
            zzfjk.m12832(3, this.f10752.intValue());
        }
        super.m12842(zzfjk);
    }
}
