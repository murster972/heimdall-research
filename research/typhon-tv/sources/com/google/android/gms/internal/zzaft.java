package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzaft implements zzahg, zzhj {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final HashMap<String, zzafz> f4158 = new HashMap<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4159 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f4160 = true;

    /* renamed from: ʾ  reason: contains not printable characters */
    private zznk f4161 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f4162 = true;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Boolean f4163 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private zzakd f4164;

    /* renamed from: ˉ  reason: contains not printable characters */
    private String f4165;

    /* renamed from: ˊ  reason: contains not printable characters */
    private zzhf f4166 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f4167;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f4168;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f4169 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f4170 = 0;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f4171 = false;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f4172 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f4173 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Context f4174;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private final AtomicInteger f4175 = new AtomicInteger(0);

    /* renamed from: ᴵ  reason: contains not printable characters */
    private String f4176 = "";

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private final zzafv f4177 = new zzafv();

    /* renamed from: ᵎ  reason: contains not printable characters */
    private long f4178 = 0;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private long f4179 = 0;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private long f4180 = 0;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f4181 = -1;

    /* renamed from: 连任  reason: contains not printable characters */
    private final HashSet<zzafq> f4182 = new HashSet<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzafx f4183 = new zzafx(zzkb.m5486());

    /* renamed from: 麤  reason: contains not printable characters */
    private BigInteger f4184 = BigInteger.ONE;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzfs f4185;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4186 = new Object();

    /* renamed from: ﹳ  reason: contains not printable characters */
    private JSONObject f4187 = new JSONObject();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f4188 = true;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private zzhk f4189 = null;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f4190 = 0;

    public zzaft(zzahn zzahn) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final Future m4463(int i) {
        Future r0;
        synchronized (this.f4186) {
            this.f4181 = i;
            r0 = zzagh.m4536(this.f4174, i);
        }
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final Future m4464(long j) {
        Future r0;
        synchronized (this.f4186) {
            this.f4179 = j;
            r0 = zzagh.m4548(this.f4174, j);
        }
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m4465() {
        boolean z;
        synchronized (this.f4186) {
            z = this.f4160 || this.f4171;
        }
        return z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m4466() {
        String str;
        synchronized (this.f4186) {
            str = this.f4167;
        }
        return str;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m4467() {
        String str;
        synchronized (this.f4186) {
            str = this.f4168;
        }
        return str;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int m4468() {
        int i;
        synchronized (this.f4186) {
            i = this.f4190;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final int m4469() {
        int i;
        synchronized (this.f4186) {
            i = this.f4181;
        }
        return i;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final Future m4470() {
        Future r0;
        synchronized (this.f4186) {
            r0 = zzagh.m4546(this.f4174);
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final long m4471() {
        long j;
        synchronized (this.f4186) {
            j = this.f4180;
        }
        return j;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final zzfs m4472() {
        return this.f4185;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m4473() {
        this.f4177.m9575();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzafs m4474() {
        zzafs zzafs;
        synchronized (this.f4186) {
            zzafs = new zzafs(this.f4176, this.f4178);
        }
        return zzafs;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m4475() {
        JSONObject jSONObject;
        synchronized (this.f4186) {
            jSONObject = this.f4187;
        }
        return jSONObject;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final Resources m4476() {
        if (this.f4164.f4295) {
            return this.f4174.getResources();
        }
        try {
            DynamiteModule r1 = DynamiteModule.m9319(this.f4174, DynamiteModule.f7976, ModuleDescriptor.MODULE_ID);
            if (r1 != null) {
                return r1.m9323().getResources();
            }
            return null;
        } catch (DynamiteModule.zzc e) {
            zzagf.m4796("Cannot load resource from dynamite apk or local jar", e);
            return null;
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final Boolean m4477() {
        Boolean bool;
        synchronized (this.f4186) {
            bool = this.f4163;
        }
        return bool;
    }

    /* renamed from: י  reason: contains not printable characters */
    public final void m4478() {
        this.f4175.incrementAndGet();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final void m4479() {
        this.f4175.decrementAndGet();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m4480() {
        boolean z;
        synchronized (this.f4186) {
            z = this.f4172;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final long m4481() {
        long j;
        synchronized (this.f4186) {
            j = this.f4179;
        }
        return j;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final int m4482() {
        return this.f4175.get();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zznk m4483() {
        zznk zznk;
        synchronized (this.f4186) {
            zznk = this.f4161;
        }
        return zznk;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Future m4484(Context context, boolean z) {
        Future future;
        synchronized (this.f4186) {
            if (z != this.f4172) {
                this.f4172 = z;
                future = zzagh.m4544(context, z);
            } else {
                future = null;
            }
        }
        return future;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Future m4485(String str) {
        Future future;
        synchronized (this.f4186) {
            if (str != null) {
                if (!str.equals(this.f4168)) {
                    this.f4168 = str;
                    future = zzagh.m4539(this.f4174, str);
                }
            }
            future = null;
        }
        return future;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4486(boolean z) {
        synchronized (this.f4186) {
            if (this.f4188 != z) {
                zzagh.m4540(this.f4174, z);
            }
            zzagh.m4540(this.f4174, z);
            this.f4188 = z;
            zzhk r0 = m4494(this.f4174);
            if (r0 != null && !r0.isAlive()) {
                zzagf.m4794("start fetching content...");
                r0.m5387();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4487() {
        boolean z;
        synchronized (this.f4186) {
            z = this.f4188;
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzafx m4488() {
        zzafx zzafx;
        synchronized (this.f4186) {
            zzafx = this.f4183;
        }
        return zzafx;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4489(boolean z) {
        long r0 = zzbs.zzeo().m9243();
        if (z) {
            if (r0 - this.f4179 > ((Long) zzkb.m5481().m5595(zznh.f4913)).longValue()) {
                this.f4183.f4199 = -1;
                return;
            }
            this.f4183.f4199 = this.f4181;
            return;
        }
        m4464(r0);
        m4463(this.f4183.f4199);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m4490() {
        String bigInteger;
        synchronized (this.f4186) {
            bigInteger = this.f4184.toString();
            this.f4184 = this.f4184.add(BigInteger.ONE);
        }
        return bigInteger;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4491(boolean z) {
        this.f4177.m9576(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m4492(Context context, zzafy zzafy, String str) {
        Bundle bundle;
        synchronized (this.f4186) {
            bundle = new Bundle();
            bundle.putBundle("app", this.f4183.m4513(context, str));
            Bundle bundle2 = new Bundle();
            for (String next : this.f4158.keySet()) {
                bundle2.putBundle(next, this.f4158.get(next).m4516());
            }
            bundle.putBundle("slots", bundle2);
            ArrayList arrayList = new ArrayList();
            Iterator<zzafq> it2 = this.f4182.iterator();
            while (it2.hasNext()) {
                arrayList.add(it2.next().m4446());
            }
            bundle.putParcelableArrayList("ads", arrayList);
            zzafy.zza(this.f4182);
            this.f4182.clear();
        }
        return bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv m4493(Context context, String str) {
        zzakv r0;
        this.f4178 = zzbs.zzeo().m9243();
        synchronized (this.f4186) {
            if (str != null) {
                if (!str.equals(this.f4176)) {
                    this.f4176 = str;
                    r0 = zzagh.m4545(context, str, this.f4178);
                }
            }
            r0 = zzakl.m4802(null);
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return null;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzhk m4494(android.content.Context r5) {
        /*
            r4 = this;
            r1 = 0
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f5001
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r2.m5595(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0015
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f5134
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r2.m5595(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x003b
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f5130
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r2.m5595(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x003b
            r0 = r1
            goto L_0x0014
        L_0x003b:
            boolean r0 = r4.m4508()
            if (r0 == 0) goto L_0x0049
            boolean r0 = r4.m4487()
            if (r0 == 0) goto L_0x0049
            r0 = r1
            goto L_0x0014
        L_0x0049:
            java.lang.Object r2 = r4.f4186
            monitor-enter(r2)
            android.os.Looper r0 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x007e }
            if (r0 == 0) goto L_0x0054
            if (r5 != 0) goto L_0x0057
        L_0x0054:
            monitor-exit(r2)     // Catch:{ all -> 0x007e }
            r0 = r1
            goto L_0x0014
        L_0x0057:
            com.google.android.gms.internal.zzhf r0 = r4.f4166     // Catch:{ all -> 0x007e }
            if (r0 != 0) goto L_0x0062
            com.google.android.gms.internal.zzhf r0 = new com.google.android.gms.internal.zzhf     // Catch:{ all -> 0x007e }
            r0.<init>()     // Catch:{ all -> 0x007e }
            r4.f4166 = r0     // Catch:{ all -> 0x007e }
        L_0x0062:
            com.google.android.gms.internal.zzhk r0 = r4.f4189     // Catch:{ all -> 0x007e }
            if (r0 != 0) goto L_0x0075
            com.google.android.gms.internal.zzhk r0 = new com.google.android.gms.internal.zzhk     // Catch:{ all -> 0x007e }
            com.google.android.gms.internal.zzhf r1 = r4.f4166     // Catch:{ all -> 0x007e }
            android.content.Context r3 = r4.f4174     // Catch:{ all -> 0x007e }
            com.google.android.gms.internal.zzzt r3 = com.google.android.gms.internal.zzzp.m6095((android.content.Context) r3)     // Catch:{ all -> 0x007e }
            r0.<init>(r1, r3)     // Catch:{ all -> 0x007e }
            r4.f4189 = r0     // Catch:{ all -> 0x007e }
        L_0x0075:
            com.google.android.gms.internal.zzhk r0 = r4.f4189     // Catch:{ all -> 0x007e }
            r0.m5387()     // Catch:{ all -> 0x007e }
            com.google.android.gms.internal.zzhk r0 = r4.f4189     // Catch:{ all -> 0x007e }
            monitor-exit(r2)     // Catch:{ all -> 0x007e }
            goto L_0x0014
        L_0x007e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x007e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaft.m4494(android.content.Context):com.google.android.gms.internal.zzhk");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Future m4495(int i) {
        Future r0;
        synchronized (this.f4186) {
            this.f4190 = i;
            r0 = zzagh.m4547(this.f4174, i);
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Future m4496(long j) {
        Future r0;
        synchronized (this.f4186) {
            this.f4180 = j;
            r0 = zzagh.m4537(this.f4174, j);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Future m4497(Context context, String str, String str2, boolean z) {
        Future r0;
        int i = 0;
        synchronized (this.f4186) {
            JSONArray optJSONArray = this.f4187.optJSONArray(str);
            JSONArray jSONArray = optJSONArray == null ? new JSONArray() : optJSONArray;
            int length = jSONArray.length();
            while (true) {
                if (i >= jSONArray.length()) {
                    i = length;
                    break;
                }
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                if (optJSONObject == null || !str2.equals(optJSONObject.optString("template_id"))) {
                    i++;
                } else if (z && optJSONObject.optBoolean("uses_media_view", false) == z) {
                    r0 = null;
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("template_id", (Object) str2);
                jSONObject.put("uses_media_view", z);
                jSONObject.put("timestamp_ms", zzbs.zzeo().m9243());
                jSONArray.put(i, (Object) jSONObject);
                this.f4187.put(str, (Object) jSONArray);
            } catch (JSONException e) {
                zzagf.m4796("Could not update native advanced settings", e);
            }
            r0 = zzagh.m4543(this.f4174, this.f4187.toString());
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Future m4498(Context context, boolean z) {
        Future future;
        synchronized (this.f4186) {
            if (z != this.f4160) {
                this.f4160 = z;
                future = zzagh.m4551(context, z);
            } else {
                future = null;
            }
        }
        return future;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Future m4499(String str) {
        Future future;
        synchronized (this.f4186) {
            if (str != null) {
                if (!str.equals(this.f4167)) {
                    this.f4167 = str;
                    future = zzagh.m4550(this.f4174, str);
                }
            }
            future = null;
        }
        return future;
    }

    @TargetApi(23)
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4500(Context context, zzakd zzakd) {
        synchronized (this.f4186) {
            if (!this.f4173) {
                this.f4174 = context.getApplicationContext();
                this.f4164 = zzakd;
                zzbs.zzel().m5380((zzhj) this);
                zzagh.m4549(context, (zzahg) this);
                zzagh.m4538(context, (zzahg) this);
                zzagh.m4531(context, this);
                zzagh.m4529(context, this);
                zzagh.m4542(context, (zzahg) this);
                zzagh.m4541(context, this);
                zzagh.m4535(context, this);
                zzagh.m4530(context, this);
                zzagh.m4532(context, this);
                zzagh.m4533(context, this);
                zzagh.m4534(context, this);
                zzzp.m6095(this.f4174);
                this.f4165 = zzbs.zzei().m4632(context, zzakd.f4297);
                if (Build.VERSION.SDK_INT >= 23 && !NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted()) {
                    this.f4171 = true;
                }
                this.f4185 = new zzfs(context.getApplicationContext(), this.f4164);
                zznj zznj = new zznj(this.f4174, this.f4164.f4297);
                try {
                    zzbs.zzeq();
                    this.f4161 = zznm.m5612(zznj);
                } catch (IllegalArgumentException e) {
                    zzagf.m4796("Cannot initialize CSI reporter.", e);
                }
                this.f4173 = true;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4501(Bundle bundle) {
        synchronized (this.f4186) {
            this.f4160 = bundle.getBoolean("use_https", this.f4160);
            this.f4170 = bundle.getInt("webview_cache_version", this.f4170);
            if (bundle.containsKey("content_url_opted_out")) {
                m4507(bundle.getBoolean("content_url_opted_out"));
            }
            if (bundle.containsKey("content_url_hashes")) {
                this.f4167 = bundle.getString("content_url_hashes");
            }
            this.f4172 = bundle.getBoolean("auto_collect_location", this.f4172);
            if (bundle.containsKey("content_vertical_opted_out")) {
                m4486(bundle.getBoolean("content_vertical_opted_out"));
            }
            if (bundle.containsKey("content_vertical_hashes")) {
                this.f4168 = bundle.getString("content_vertical_hashes");
            }
            if (bundle.containsKey("native_advanced_settings")) {
                try {
                    this.f4187 = new JSONObject(bundle.getString("native_advanced_settings"));
                } catch (JSONException e) {
                    zzagf.m4796("Could not convert native advanced settings to json object", e);
                }
            }
            if (bundle.containsKey("version_code")) {
                this.f4190 = bundle.getInt("version_code");
            }
            this.f4176 = bundle.containsKey("app_settings_json") ? bundle.getString("app_settings_json") : this.f4176;
            this.f4178 = bundle.getLong("app_settings_last_update_ms", this.f4178);
            this.f4179 = bundle.getLong("app_last_background_time_ms", this.f4179);
            this.f4181 = bundle.getInt("request_in_session_count", this.f4181);
            this.f4180 = bundle.getLong("first_ad_req_time_ms", this.f4180);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4502(zzafq zzafq) {
        synchronized (this.f4186) {
            this.f4182.add(zzafq);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4503(Boolean bool) {
        synchronized (this.f4186) {
            this.f4163 = bool;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4504(String str, zzafz zzafz) {
        synchronized (this.f4186) {
            this.f4158.put(str, zzafz);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4505(Throwable th, String str) {
        zzzp.m6095(this.f4174).m13661(th, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4506(HashSet<zzafq> hashSet) {
        synchronized (this.f4186) {
            this.f4182.addAll(hashSet);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4507(boolean z) {
        synchronized (this.f4186) {
            if (this.f4162 != z) {
                zzagh.m4540(this.f4174, z);
            }
            this.f4162 = z;
            zzhk r0 = m4494(this.f4174);
            if (r0 != null && !r0.isAlive()) {
                zzagf.m4794("start fetching content...");
                r0.m5387();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4508() {
        boolean z;
        synchronized (this.f4186) {
            z = this.f4162;
        }
        return z;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final boolean m4509() {
        return this.f4177.m9577();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean m4510() {
        return this.f4177.m9574();
    }
}
