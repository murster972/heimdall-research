package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzabd extends zzeu implements zzabb {
    zzabd(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdRequestService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaax m9445(zzaat zzaat) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzaat);
        Parcel r1 = m12300(1, v_);
        zzaax zzaax = (zzaax) zzew.m12304(r1, zzaax.CREATOR);
        r1.recycle();
        return zzaax;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9446(zzaat zzaat, zzabe zzabe) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzaat);
        zzew.m12305(v_, (IInterface) zzabe);
        m12298(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9447(zzabm zzabm, zzabh zzabh) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzabm);
        zzew.m12305(v_, (IInterface) zzabh);
        m12298(4, v_);
    }
}
