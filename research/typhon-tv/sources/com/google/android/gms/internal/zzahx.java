package com.google.android.gms.internal;

import android.content.Context;
import android.webkit.WebSettings;
import java.util.concurrent.Callable;

final class zzahx implements Callable<Boolean> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ WebSettings f8217;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8218;

    zzahx(zzahw zzahw, Context context, WebSettings webSettings) {
        this.f8218 = context;
        this.f8217 = webSettings;
    }

    public final /* synthetic */ Object call() throws Exception {
        if (this.f8218.getCacheDir() != null) {
            this.f8217.setAppCachePath(this.f8218.getCacheDir().getAbsolutePath());
            this.f8217.setAppCacheMaxSize(0);
            this.f8217.setAppCacheEnabled(true);
        }
        this.f8217.setDatabasePath(this.f8218.getDatabasePath("com.google.android.gms.ads.db").getAbsolutePath());
        this.f8217.setDatabaseEnabled(true);
        this.f8217.setDomStorageEnabled(true);
        this.f8217.setDisplayZoomControls(false);
        this.f8217.setBuiltInZoomControls(true);
        this.f8217.setSupportZoom(true);
        this.f8217.setAllowContentAccess(false);
        return true;
    }
}
