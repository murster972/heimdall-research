package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagk extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f8159;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8160;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagk(Context context, boolean z) {
        super((zzagi) null);
        this.f8160 = context;
        this.f8159 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9583() {
        SharedPreferences.Editor edit = this.f8160.getSharedPreferences("admob", 0).edit();
        edit.putBoolean("auto_collect_location", this.f8159);
        edit.apply();
    }
}
