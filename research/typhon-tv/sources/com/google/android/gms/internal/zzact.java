package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzbs;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzact extends zzacv {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f4008;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zztp<JSONObject, JSONObject> f4009;

    /* renamed from: 齉  reason: contains not printable characters */
    private SharedPreferences f4010;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4011 = new Object();

    public zzact(Context context, zztp<JSONObject, JSONObject> zztp) {
        this.f4008 = context.getApplicationContext();
        this.f4009 = zztp;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<Void> m4319() {
        synchronized (this.f4011) {
            if (this.f4010 == null) {
                this.f4010 = this.f4008.getSharedPreferences("google_ads_flags_meta", 0);
            }
        }
        if (zzbs.zzeo().m9243() - this.f4010.getLong("js_last_update", 0) < ((Long) zzkb.m5481().m5595(zznh.f4982)).longValue()) {
            return zzakl.m4802(null);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("js", (Object) zzakd.m4800().f4297);
            jSONObject.put(PubnativeRequest.Parameters.META_FIELDS, zzkb.m5481().m5595(zznh.f4983));
            jSONObject.put("cl", (Object) "179146524");
            jSONObject.put("rapid_rc", (Object) "dev");
            jSONObject.put("rapid_rollup", (Object) "HEAD");
            return zzakl.m4805(this.f4009.m5868(jSONObject), new zzacu(this), zzala.f4304);
        } catch (JSONException e) {
            zzagf.m4793("Unable to populate SDK Core TyphoonApp parameters.", e);
            return zzakl.m4802(null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Void m4320(JSONObject jSONObject) {
        zznh.m5600(this.f4008, 1, jSONObject);
        this.f4010.edit().putLong("js_last_update", zzbs.zzeo().m9243()).apply();
        return null;
    }
}
