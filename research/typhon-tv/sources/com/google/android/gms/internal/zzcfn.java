package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.location.LocationSettingsResult;

final class zzcfn extends zzcfc {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzn<LocationSettingsResult> f9082;

    public zzcfn(zzn<LocationSettingsResult> zzn) {
        zzbq.m9117(zzn != null, "listener can't be null.");
        this.f9082 = zzn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10416(LocationSettingsResult locationSettingsResult) throws RemoteException {
        this.f9082.m8947(locationSettingsResult);
        this.f9082 = null;
    }
}
