package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzly {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f4872 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzly f4873;

    /* renamed from: 麤  reason: contains not printable characters */
    private RewardedVideoAd f4874;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzlg f4875;

    private zzly() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzly m5559() {
        zzly zzly;
        synchronized (f4872) {
            if (f4873 == null) {
                f4873 = new zzly();
            }
            zzly = f4873;
        }
        return zzly;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final float m5560() {
        if (this.f4875 == null) {
            return 1.0f;
        }
        try {
            return this.f4875.zzdn();
        } catch (RemoteException e) {
            zzakb.m4793("Unable to get app volume.", e);
            return 1.0f;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5561() {
        if (this.f4875 == null) {
            return false;
        }
        try {
            return this.f4875.zzdo();
        } catch (RemoteException e) {
            zzakb.m4793("Unable to get app mute state.", e);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final RewardedVideoAd m5562(Context context) {
        RewardedVideoAd rewardedVideoAd;
        synchronized (f4872) {
            if (this.f4874 != null) {
                rewardedVideoAd = this.f4874;
            } else {
                this.f4874 = new zzadx(context, (zzadk) zzjr.m5474(context, false, new zzjz(zzkb.m5484(), context, new zzuw())));
                rewardedVideoAd = this.f4874;
            }
        }
        return rewardedVideoAd;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5563(float f) {
        boolean z = true;
        zzbq.m9117(0.0f <= f && f <= 1.0f, "The app volume must be a value between 0 and 1 inclusive.");
        if (this.f4875 == null) {
            z = false;
        }
        zzbq.m9126(z, (Object) "MobileAds.initialize() must be called prior to setting the app volume.");
        try {
            this.f4875.setAppVolume(f);
        } catch (RemoteException e) {
            zzakb.m4793("Unable to set app volume.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5564(Context context, String str) {
        zzbq.m9126(this.f4875 != null, (Object) "MobileAds.initialize() must be called prior to opening debug menu.");
        try {
            this.f4875.zzb(zzn.m9306(context), str);
        } catch (RemoteException e) {
            zzakb.m4793("Unable to open debug menu.", e);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5565(android.content.Context r4, java.lang.String r5, com.google.android.gms.internal.zzma r6) {
        /*
            r3 = this;
            java.lang.Object r1 = f4872
            monitor-enter(r1)
            com.google.android.gms.internal.zzlg r0 = r3.f4875     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
        L_0x0008:
            return
        L_0x0009:
            if (r4 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0014 }
            java.lang.String r2 = "Context cannot be null."
            r0.<init>(r2)     // Catch:{ all -> 0x0014 }
            throw r0     // Catch:{ all -> 0x0014 }
        L_0x0014:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            throw r0
        L_0x0017:
            com.google.android.gms.internal.zzjr r0 = com.google.android.gms.internal.zzkb.m5484()     // Catch:{ RemoteException -> 0x0040 }
            com.google.android.gms.internal.zzjw r2 = new com.google.android.gms.internal.zzjw     // Catch:{ RemoteException -> 0x0040 }
            r2.<init>(r0, r4)     // Catch:{ RemoteException -> 0x0040 }
            r0 = 0
            java.lang.Object r0 = com.google.android.gms.internal.zzjr.m5474((android.content.Context) r4, (boolean) r0, r2)     // Catch:{ RemoteException -> 0x0040 }
            com.google.android.gms.internal.zzlg r0 = (com.google.android.gms.internal.zzlg) r0     // Catch:{ RemoteException -> 0x0040 }
            r3.f4875 = r0     // Catch:{ RemoteException -> 0x0040 }
            com.google.android.gms.internal.zzlg r0 = r3.f4875     // Catch:{ RemoteException -> 0x0040 }
            r0.initialize()     // Catch:{ RemoteException -> 0x0040 }
            if (r5 == 0) goto L_0x003e
            com.google.android.gms.internal.zzlg r0 = r3.f4875     // Catch:{ RemoteException -> 0x0040 }
            com.google.android.gms.internal.zzlz r2 = new com.google.android.gms.internal.zzlz     // Catch:{ RemoteException -> 0x0040 }
            r2.<init>(r3, r4)     // Catch:{ RemoteException -> 0x0040 }
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.zzn.m9306(r2)     // Catch:{ RemoteException -> 0x0040 }
            r0.zza(r5, r2)     // Catch:{ RemoteException -> 0x0040 }
        L_0x003e:
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            goto L_0x0008
        L_0x0040:
            r0 = move-exception
            java.lang.String r2 = "MobileAdsSettingManager initialization failed"
            com.google.android.gms.internal.zzakb.m4796(r2, r0)     // Catch:{ all -> 0x0014 }
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzly.m5565(android.content.Context, java.lang.String, com.google.android.gms.internal.zzma):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5566(boolean z) {
        zzbq.m9126(this.f4875 != null, (Object) "MobileAds.initialize() must be called prior to setting app muted state.");
        try {
            this.f4875.setAppMuted(z);
        } catch (RemoteException e) {
            zzakb.m4793("Unable to set app mute state.", e);
        }
    }
}
