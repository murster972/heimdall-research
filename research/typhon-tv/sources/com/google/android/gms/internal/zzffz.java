package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class zzffz {

    /* renamed from: 连任  reason: contains not printable characters */
    private static zzffb f10419 = zzffb.m12398(f10420);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final byte[] f10420;

    /* renamed from: 麤  reason: contains not printable characters */
    private static ByteBuffer f10421;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Charset f10422 = Charset.forName("ISO-8859-1");

    /* renamed from: 龘  reason: contains not printable characters */
    static final Charset f10423 = Charset.forName("UTF-8");

    static {
        byte[] bArr = new byte[0];
        f10420 = bArr;
        f10421 = ByteBuffer.wrap(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m12576(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12577(boolean z) {
        return z ? 1231 : 1237;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m12578(byte[] bArr) {
        int length = bArr.length;
        int r0 = m12576(length, bArr, 0, length);
        if (r0 == 0) {
            return 1;
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> T m12579(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> T m12580(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m12581(zzfhe zzfhe) {
        return false;
    }
}
