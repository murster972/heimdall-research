package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zznz extends zzeu implements zznx {
    zznz(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m13172() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13173() throws RemoteException {
        m12298(5, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13174() throws RemoteException {
        m12298(4, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13175() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13176(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(3, v_);
    }
}
