package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcmc extends zzfjm<zzcmc> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile zzcmc[] f9717;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Float f9718 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9719 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Double f9720 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Long f9721 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f9722 = null;

    public zzcmc() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzcmc[] m11493() {
        if (f9717 == null) {
            synchronized (zzfjq.f10546) {
                if (f9717 == null) {
                    f9717 = new zzcmc[0];
                }
            }
        }
        return f9717;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcmc)) {
            return false;
        }
        zzcmc zzcmc = (zzcmc) obj;
        if (this.f9722 == null) {
            if (zzcmc.f9722 != null) {
                return false;
            }
        } else if (!this.f9722.equals(zzcmc.f9722)) {
            return false;
        }
        if (this.f9719 == null) {
            if (zzcmc.f9719 != null) {
                return false;
            }
        } else if (!this.f9719.equals(zzcmc.f9719)) {
            return false;
        }
        if (this.f9721 == null) {
            if (zzcmc.f9721 != null) {
                return false;
            }
        } else if (!this.f9721.equals(zzcmc.f9721)) {
            return false;
        }
        if (this.f9718 == null) {
            if (zzcmc.f9718 != null) {
                return false;
            }
        } else if (!this.f9718.equals(zzcmc.f9718)) {
            return false;
        }
        if (this.f9720 == null) {
            if (zzcmc.f9720 != null) {
                return false;
            }
        } else if (!this.f9720.equals(zzcmc.f9720)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcmc.f10533 == null || zzcmc.f10533.m12849() : this.f10533.equals(zzcmc.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9720 == null ? 0 : this.f9720.hashCode()) + (((this.f9718 == null ? 0 : this.f9718.hashCode()) + (((this.f9721 == null ? 0 : this.f9721.hashCode()) + (((this.f9719 == null ? 0 : this.f9719.hashCode()) + (((this.f9722 == null ? 0 : this.f9722.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11494() {
        int r0 = super.m12841();
        if (this.f9722 != null) {
            r0 += zzfjk.m12808(1, this.f9722);
        }
        if (this.f9719 != null) {
            r0 += zzfjk.m12808(2, this.f9719);
        }
        if (this.f9721 != null) {
            r0 += zzfjk.m12814(3, this.f9721.longValue());
        }
        if (this.f9718 != null) {
            this.f9718.floatValue();
            r0 += zzfjk.m12805(4) + 4;
        }
        if (this.f9720 == null) {
            return r0;
        }
        this.f9720.doubleValue();
        return r0 + zzfjk.m12805(5) + 8;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11495(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f9722 = zzfjj.m12791();
                    continue;
                case 18:
                    this.f9719 = zzfjj.m12791();
                    continue;
                case 24:
                    this.f9721 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 37:
                    this.f9718 = Float.valueOf(Float.intBitsToFloat(zzfjj.m12788()));
                    continue;
                case 41:
                    this.f9720 = Double.valueOf(Double.longBitsToDouble(zzfjj.m12789()));
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11496(zzfjk zzfjk) throws IOException {
        if (this.f9722 != null) {
            zzfjk.m12835(1, this.f9722);
        }
        if (this.f9719 != null) {
            zzfjk.m12835(2, this.f9719);
        }
        if (this.f9721 != null) {
            zzfjk.m12824(3, this.f9721.longValue());
        }
        if (this.f9718 != null) {
            zzfjk.m12831(4, this.f9718.floatValue());
        }
        if (this.f9720 != null) {
            zzfjk.m12830(5, this.f9720.doubleValue());
        }
        super.m12842(zzfjk);
    }
}
