package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzkz extends zzeu implements zzkx {
    zzkz(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAppEventListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13075(String str, String str2) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        m12298(1, v_);
    }
}
