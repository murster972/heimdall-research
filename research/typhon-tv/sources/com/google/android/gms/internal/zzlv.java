package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.concurrent.atomic.AtomicBoolean;

@zzzv
public final class zzlv {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzje f4841;

    /* renamed from: ʼ  reason: contains not printable characters */
    private AdListener f4842;

    /* renamed from: ʽ  reason: contains not printable characters */
    private AdSize[] f4843;

    /* renamed from: ʾ  reason: contains not printable characters */
    private VideoOptions f4844;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f4845;

    /* renamed from: ˈ  reason: contains not printable characters */
    private OnCustomRenderedAdLoadedListener f4846;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f4847;

    /* renamed from: ˑ  reason: contains not printable characters */
    private AppEventListener f4848;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Correlator f4849;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzks f4850;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzkd f4851;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzjm f4852;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final VideoController f4853;

    /* renamed from: 齉  reason: contains not printable characters */
    private final AtomicBoolean f4854;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzuw f4855;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private ViewGroup f4856;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f4857;

    public zzlv(ViewGroup viewGroup) {
        this(viewGroup, (AttributeSet) null, false, zzjm.f4788, 0);
    }

    public zzlv(ViewGroup viewGroup, int i) {
        this(viewGroup, (AttributeSet) null, false, zzjm.f4788, i);
    }

    public zzlv(ViewGroup viewGroup, AttributeSet attributeSet, boolean z) {
        this(viewGroup, attributeSet, z, zzjm.f4788, 0);
    }

    public zzlv(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, int i) {
        this(viewGroup, attributeSet, false, zzjm.f4788, i);
    }

    private zzlv(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, zzjm zzjm, int i) {
        this(viewGroup, attributeSet, z, zzjm, (zzks) null, i);
    }

    private zzlv(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, zzjm zzjm, zzks zzks, int i) {
        this.f4855 = new zzuw();
        this.f4853 = new VideoController();
        this.f4851 = new zzlw(this);
        this.f4856 = viewGroup;
        this.f4852 = zzjm;
        this.f4850 = null;
        this.f4854 = new AtomicBoolean(false);
        this.f4857 = i;
        if (attributeSet != null) {
            Context context = viewGroup.getContext();
            try {
                zzjq zzjq = new zzjq(context, attributeSet);
                this.f4843 = zzjq.m5463(z);
                this.f4845 = zzjq.m5462();
                if (viewGroup.isInEditMode()) {
                    zzajr r0 = zzkb.m5487();
                    AdSize adSize = this.f4843[0];
                    int i2 = this.f4857;
                    zzjn zzjn = new zzjn(context, adSize);
                    zzjn.f4793 = m5512(i2);
                    r0.m4768(viewGroup, zzjn, "Ads by Google");
                }
            } catch (IllegalArgumentException e) {
                zzkb.m5487().m4769(viewGroup, new zzjn(context, AdSize.BANNER), e.getMessage(), e.getMessage());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzjn m5511(Context context, AdSize[] adSizeArr, int i) {
        zzjn zzjn = new zzjn(context, adSizeArr);
        zzjn.f4793 = m5512(i);
        return zzjn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m5512(int i) {
        return i == 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final AppEventListener m5513() {
        return this.f4848;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final OnCustomRenderedAdLoadedListener m5514() {
        return this.f4846;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m5515() {
        try {
            if (this.f4850 != null) {
                this.f4850.pause();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to call pause.", e);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final VideoController m5516() {
        return this.f4853;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final zzll m5517() {
        if (this.f4850 == null) {
            return null;
        }
        try {
            return this.f4850.getVideoController();
        } catch (RemoteException e) {
            zzakb.m4796("Failed to retrieve VideoController.", e);
            return null;
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean m5518() {
        try {
            if (this.f4850 != null) {
                return this.f4850.isLoading();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to check if ad is loading.", e);
        }
        return false;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m5519() {
        if (!this.f4854.getAndSet(true)) {
            try {
                if (this.f4850 != null) {
                    this.f4850.zzbu();
                }
            } catch (RemoteException e) {
                zzakb.m4796("Failed to record impression.", e);
            }
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m5520() {
        try {
            if (this.f4850 != null) {
                this.f4850.resume();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to call resume.", e);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m5521() {
        try {
            if (this.f4850 != null) {
                return this.f4850.zzcp();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to get the mediation adapter class name.", e);
        }
        return null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m5522() {
        if (this.f4845 == null && this.f4850 != null) {
            try {
                this.f4845 = this.f4850.getAdUnitId();
            } catch (RemoteException e) {
                zzakb.m4796("Failed to get ad unit id.", e);
            }
        }
        return this.f4845;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final AdListener m5523() {
        return this.f4842;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5524(AdSize... adSizeArr) {
        this.f4843 = adSizeArr;
        try {
            if (this.f4850 != null) {
                this.f4850.zza(m5511(this.f4856.getContext(), this.f4843, this.f4857));
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the ad size.", e);
        }
        this.f4856.requestLayout();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final AdSize[] m5525() {
        return this.f4843;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final AdSize m5526() {
        zzjn zzbs;
        try {
            if (!(this.f4850 == null || (zzbs = this.f4850.zzbs()) == null)) {
                return zzbs.m5458();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to get the current AdSize.", e);
        }
        if (this.f4843 != null) {
            return this.f4843[0];
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5527() {
        try {
            if (this.f4850 != null) {
                this.f4850.destroy();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to destroy AdView.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5528(AdListener adListener) {
        this.f4842 = adListener;
        this.f4851.m5490(adListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5529(Correlator correlator) {
        this.f4849 = correlator;
        try {
            if (this.f4850 != null) {
                this.f4850.zza((zzld) this.f4849 == null ? null : this.f4849.zzbh());
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set correlator.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5530(VideoOptions videoOptions) {
        this.f4844 = videoOptions;
        try {
            if (this.f4850 != null) {
                this.f4850.zza(videoOptions == null ? null : new zzmr(videoOptions));
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set video options.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5531(AppEventListener appEventListener) {
        try {
            this.f4848 = appEventListener;
            if (this.f4850 != null) {
                this.f4850.zza((zzkx) appEventListener != null ? new zzjp(appEventListener) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the AppEventListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5532(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.f4846 = onCustomRenderedAdLoadedListener;
        try {
            if (this.f4850 != null) {
                this.f4850.zza((zzoa) onCustomRenderedAdLoadedListener != null ? new zzod(onCustomRenderedAdLoadedListener) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the onCustomRenderedAdLoadedListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5533(zzje zzje) {
        try {
            this.f4841 = zzje;
            if (this.f4850 != null) {
                this.f4850.zza((zzke) zzje != null ? new zzjf(zzje) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the AdClickListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5534(zzlt zzlt) {
        try {
            if (this.f4850 == null) {
                if ((this.f4843 == null || this.f4845 == null) && this.f4850 == null) {
                    throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
                }
                Context context = this.f4856.getContext();
                zzjn r3 = m5511(context, this.f4843, this.f4857);
                this.f4850 = "search_v2".equals(r3.f4798) ? (zzks) zzjr.m5474(context, false, new zzjt(zzkb.m5484(), context, r3, this.f4845)) : (zzks) zzjr.m5474(context, false, new zzjs(zzkb.m5484(), context, r3, this.f4845, this.f4855));
                this.f4850.zza((zzkh) new zzjg(this.f4851));
                if (this.f4841 != null) {
                    this.f4850.zza((zzke) new zzjf(this.f4841));
                }
                if (this.f4848 != null) {
                    this.f4850.zza((zzkx) new zzjp(this.f4848));
                }
                if (this.f4846 != null) {
                    this.f4850.zza((zzoa) new zzod(this.f4846));
                }
                if (this.f4849 != null) {
                    this.f4850.zza((zzld) this.f4849.zzbh());
                }
                if (this.f4844 != null) {
                    this.f4850.zza(new zzmr(this.f4844));
                }
                this.f4850.setManualImpressionsEnabled(this.f4847);
                try {
                    IObjectWrapper zzbr = this.f4850.zzbr();
                    if (zzbr != null) {
                        this.f4856.addView((View) zzn.m9307(zzbr));
                    }
                } catch (RemoteException e) {
                    zzakb.m4796("Failed to get an ad frame.", e);
                }
            }
            if (this.f4850.zzb(zzjm.m5452(this.f4856.getContext(), zzlt))) {
                this.f4855.m5917(zzlt.m5498());
            }
        } catch (RemoteException e2) {
            zzakb.m4796("Failed to load ad.", e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5535(String str) {
        if (this.f4845 != null) {
            throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
        }
        this.f4845 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5536(boolean z) {
        this.f4847 = z;
        try {
            if (this.f4850 != null) {
                this.f4850.setManualImpressionsEnabled(this.f4847);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set manual impressions.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5537(AdSize... adSizeArr) {
        if (this.f4843 != null) {
            throw new IllegalStateException("The ad size can only be set once on AdView.");
        }
        m5524(adSizeArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5538(zzks zzks) {
        if (zzks == null) {
            return false;
        }
        try {
            IObjectWrapper zzbr = zzks.zzbr();
            if (zzbr == null) {
                return false;
            }
            if (((View) zzn.m9307(zzbr)).getParent() != null) {
                return false;
            }
            this.f4856.addView((View) zzn.m9307(zzbr));
            this.f4850 = zzks;
            return true;
        } catch (RemoteException e) {
            zzakb.m4796("Failed to get an ad frame.", e);
            return false;
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final VideoOptions m5539() {
        return this.f4844;
    }
}
