package com.google.android.gms.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzafh implements ThreadFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicInteger f8142 = new AtomicInteger(1);

    zzafh(zzaff zzaff) {
    }

    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, new StringBuilder(42).append("AdWorker(SCION_TASK_EXECUTOR) #").append(this.f8142.getAndIncrement()).toString());
    }
}
