package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;
import com.mopub.common.AdType;

final class zzjz extends zzjr.zza<zzadk> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzux f10783;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzjr f10784;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f10785;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjz(zzjr zzjr, Context context, zzux zzux) {
        super();
        this.f10784 = zzjr;
        this.f10785 = context;
        this.f10783 = zzux;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13049() throws RemoteException {
        zzadk r0 = this.f10784.f4803.m4356(this.f10785, this.f10783);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10785, AdType.REWARDED_VIDEO);
        return new zzml();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13050(zzla zzla) throws RemoteException {
        return zzla.createRewardedVideoAd(zzn.m9306(this.f10785), this.f10783, 11910000);
    }
}
