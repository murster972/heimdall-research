package com.google.android.gms.internal;

import android.view.ViewGroup;

final class zzazg implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzaze f8486;

    zzazg(zzaze zzaze) {
        this.f8486 = zzaze;
    }

    public final void run() {
        if (this.f8486.f8484.f8477) {
            ((ViewGroup) this.f8486.f8484.f8480.getWindow().getDecorView()).removeView(this.f8486.f8484);
            if (this.f8486.f8484.f8482 != null) {
                this.f8486.f8484.f8482.m8050();
            }
            this.f8486.f8484.m9806();
        }
    }
}
