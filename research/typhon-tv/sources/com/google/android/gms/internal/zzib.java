package com.google.android.gms.internal;

import java.util.Comparator;

public final class zzib implements Comparator<zzhp> {
    public zzib(zzia zzia) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzhp zzhp = (zzhp) obj;
        zzhp zzhp2 = (zzhp) obj2;
        if (zzhp.m5391() < zzhp2.m5391()) {
            return -1;
        }
        if (zzhp.m5391() > zzhp2.m5391()) {
            return 1;
        }
        if (zzhp.m5394() < zzhp2.m5394()) {
            return -1;
        }
        if (zzhp.m5394() > zzhp2.m5394()) {
            return 1;
        }
        float r2 = (zzhp.m5392() - zzhp.m5391()) * (zzhp.m5393() - zzhp.m5394());
        float r3 = (zzhp2.m5392() - zzhp2.m5391()) * (zzhp2.m5393() - zzhp2.m5394());
        if (r2 <= r3) {
            return r2 < r3 ? 1 : 0;
        }
        return -1;
    }
}
