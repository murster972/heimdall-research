package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;
import net.pubnative.library.request.PubnativeAsset;

final class zzjs extends zzjr.zza<zzks> {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzjr f10755;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzjn f10756;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzux f10757;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f10758;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f10759;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjs(zzjr zzjr, Context context, zzjn zzjn, String str, zzux zzux) {
        super();
        this.f10755 = zzjr;
        this.f10759 = context;
        this.f10756 = zzjn;
        this.f10758 = str;
        this.f10757 = zzux;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13035() throws RemoteException {
        zzks r0 = this.f10755.f4809.m5447(this.f10759, this.f10756, this.f10758, this.f10757, 1);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10759, PubnativeAsset.BANNER);
        return new zzmg();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13036(zzla zzla) throws RemoteException {
        return zzla.createBannerAdManager(zzn.m9306(this.f10759), this.f10756, this.f10758, this.f10757, 11910000);
    }
}
