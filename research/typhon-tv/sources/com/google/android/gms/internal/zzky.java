package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzky extends zzev implements zzkx {
    public zzky() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IAppEventListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzkx m13074(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
        return queryLocalInterface instanceof zzkx ? (zzkx) queryLocalInterface : new zzkz(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        if (i != 1) {
            return false;
        }
        m13073(parcel.readString(), parcel.readString());
        parcel2.writeNoException();
        return true;
    }
}
