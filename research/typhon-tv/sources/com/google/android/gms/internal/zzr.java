package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.internal.zzae;
import java.util.Collections;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public abstract class zzr<T> implements Comparable<zzr<T>> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Integer f10839;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzv f10840;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f10841;

    /* renamed from: ʾ  reason: contains not printable characters */
    private zzc f10842;

    /* renamed from: ʿ  reason: contains not printable characters */
    private zzt f10843;

    /* renamed from: ˈ  reason: contains not printable characters */
    private zzaa f10844;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f10845;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f10846;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f10847;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzx f10848;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f10849;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f10850;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f10851;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzae.zza f10852;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Object f10853;

    public zzr(int i, String str, zzx zzx) {
        Uri parse;
        String host;
        this.f10852 = zzae.zza.f8118 ? new zzae.zza() : null;
        this.f10841 = true;
        this.f10845 = false;
        this.f10846 = false;
        this.f10847 = false;
        this.f10842 = null;
        this.f10853 = new Object();
        this.f10849 = i;
        this.f10851 = str;
        this.f10848 = zzx;
        this.f10844 = new zzh();
        this.f10850 = (TextUtils.isEmpty(str) || (parse = Uri.parse(str)) == null || (host = parse.getHost()) == null) ? 0 : host.hashCode();
    }

    public /* synthetic */ int compareTo(Object obj) {
        zzr zzr = (zzr) obj;
        zzu zzu = zzu.NORMAL;
        zzu zzu2 = zzu.NORMAL;
        return zzu == zzu2 ? this.f10839.intValue() - zzr.f10839.intValue() : zzu2.ordinal() - zzu.ordinal();
    }

    public String toString() {
        String valueOf = String.valueOf(Integer.toHexString(this.f10850));
        String concat = valueOf.length() != 0 ? "0x".concat(valueOf) : new String("0x");
        String str = this.f10851;
        String valueOf2 = String.valueOf(zzu.NORMAL);
        String valueOf3 = String.valueOf(this.f10839);
        return new StringBuilder(String.valueOf("[ ] ").length() + 3 + String.valueOf(str).length() + String.valueOf(concat).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append("[ ] ").append(str).append(StringUtils.SPACE).append(concat).append(StringUtils.SPACE).append(valueOf2).append(StringUtils.SPACE).append(valueOf3).toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzc m13352() {
        return this.f10842;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m13353() {
        return this.f10841;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int m13354() {
        return this.f10844.m9429();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m13355() {
        synchronized (this.f10853) {
            if (this.f10843 != null) {
                this.f10843.m13420(this);
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzaa m13356() {
        return this.f10844;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m13357() {
        this.f10846 = true;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m13358() {
        return this.f10846;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m13359() {
        return this.f10851;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Map<String, String> m13360() throws zza {
        return Collections.emptyMap();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13361(String str) {
        if (zzae.zza.f8118) {
            this.f10852.m9519(str, Thread.currentThread().getId());
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m13362() {
        return this.f10850;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m13363() {
        return this.f10849;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13364(String str) {
        if (this.f10840 != null) {
            this.f10840.m13452(this);
        }
        if (zzae.zza.f8118) {
            long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new zzs(this, str, id));
                return;
            }
            this.f10852.m9519(str, id);
            this.f10852.m9518(toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzr<?> m13365(int i) {
        this.f10839 = Integer.valueOf(i);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzr<?> m13366(zzc zzc) {
        this.f10842 = zzc;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzr<?> m13367(zzv zzv) {
        this.f10840 = zzv;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzw<T> m13368(zzp zzp);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13369(zzad zzad) {
        if (this.f10848 != null) {
            this.f10848.m13631(zzad);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13370(zzt zzt) {
        synchronized (this.f10853) {
            this.f10843 = zzt;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13371(zzw<?> zzw) {
        synchronized (this.f10853) {
            if (this.f10843 != null) {
                this.f10843.m13421(this, zzw);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m13372(T t);

    /* renamed from: 龘  reason: contains not printable characters */
    public byte[] m13373() throws zza {
        return null;
    }
}
