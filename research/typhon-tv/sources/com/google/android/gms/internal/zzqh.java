package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzqh extends NativeAppInstallAd {

    /* renamed from: 连任  reason: contains not printable characters */
    private final NativeAd.AdChoicesInfo f5319;

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<NativeAd.Image> f5320 = new ArrayList();

    /* renamed from: 麤  reason: contains not printable characters */
    private final VideoController f5321 = new VideoController();

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzpt f5322;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzqe f5323;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v17, resolved type: com.google.android.gms.internal.zzps} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v18, resolved type: com.google.android.gms.internal.zzps} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v20, resolved type: com.google.android.gms.internal.zzps} */
    /* JADX WARNING: type inference failed for: r2v9, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzqh(com.google.android.gms.internal.zzqe r7) {
        /*
            r6 = this;
            r3 = 0
            r6.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r6.f5320 = r1
            com.google.android.gms.ads.VideoController r1 = new com.google.android.gms.ads.VideoController
            r1.<init>()
            r6.f5321 = r1
            r6.f5323 = r7
            com.google.android.gms.internal.zzqe r1 = r6.f5323     // Catch:{ RemoteException -> 0x004e }
            java.util.List r1 = r1.m13260()     // Catch:{ RemoteException -> 0x004e }
            if (r1 == 0) goto L_0x0055
            java.util.Iterator r4 = r1.iterator()     // Catch:{ RemoteException -> 0x004e }
        L_0x0020:
            boolean r1 = r4.hasNext()     // Catch:{ RemoteException -> 0x004e }
            if (r1 == 0) goto L_0x0055
            java.lang.Object r1 = r4.next()     // Catch:{ RemoteException -> 0x004e }
            boolean r2 = r1 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x004e }
            if (r2 == 0) goto L_0x0082
            android.os.IBinder r1 = (android.os.IBinder) r1     // Catch:{ RemoteException -> 0x004e }
            if (r1 == 0) goto L_0x0082
            java.lang.String r2 = "com.google.android.gms.ads.internal.formats.client.INativeAdImage"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)     // Catch:{ RemoteException -> 0x004e }
            boolean r5 = r2 instanceof com.google.android.gms.internal.zzpq     // Catch:{ RemoteException -> 0x004e }
            if (r5 == 0) goto L_0x007b
            r0 = r2
            com.google.android.gms.internal.zzpq r0 = (com.google.android.gms.internal.zzpq) r0     // Catch:{ RemoteException -> 0x004e }
            r1 = r0
        L_0x0041:
            if (r1 == 0) goto L_0x0020
            java.util.List<com.google.android.gms.ads.formats.NativeAd$Image> r2 = r6.f5320     // Catch:{ RemoteException -> 0x004e }
            com.google.android.gms.internal.zzpt r5 = new com.google.android.gms.internal.zzpt     // Catch:{ RemoteException -> 0x004e }
            r5.<init>(r1)     // Catch:{ RemoteException -> 0x004e }
            r2.add(r5)     // Catch:{ RemoteException -> 0x004e }
            goto L_0x0020
        L_0x004e:
            r1 = move-exception
            java.lang.String r2 = "Failed to get image."
            com.google.android.gms.internal.zzakb.m4793(r2, r1)
        L_0x0055:
            com.google.android.gms.internal.zzqe r1 = r6.f5323     // Catch:{ RemoteException -> 0x0086 }
            com.google.android.gms.internal.zzpq r2 = r1.m13262()     // Catch:{ RemoteException -> 0x0086 }
            if (r2 == 0) goto L_0x0084
            com.google.android.gms.internal.zzpt r1 = new com.google.android.gms.internal.zzpt     // Catch:{ RemoteException -> 0x0086 }
            r1.<init>(r2)     // Catch:{ RemoteException -> 0x0086 }
        L_0x0062:
            r6.f5322 = r1
            com.google.android.gms.internal.zzqe r1 = r6.f5323     // Catch:{ RemoteException -> 0x008f }
            com.google.android.gms.internal.zzpm r1 = r1.m13255()     // Catch:{ RemoteException -> 0x008f }
            if (r1 == 0) goto L_0x0078
            com.google.android.gms.internal.zzpp r1 = new com.google.android.gms.internal.zzpp     // Catch:{ RemoteException -> 0x008f }
            com.google.android.gms.internal.zzqe r2 = r6.f5323     // Catch:{ RemoteException -> 0x008f }
            com.google.android.gms.internal.zzpm r2 = r2.m13255()     // Catch:{ RemoteException -> 0x008f }
            r1.<init>(r2)     // Catch:{ RemoteException -> 0x008f }
            r3 = r1
        L_0x0078:
            r6.f5319 = r3
            return
        L_0x007b:
            com.google.android.gms.internal.zzps r2 = new com.google.android.gms.internal.zzps     // Catch:{ RemoteException -> 0x004e }
            r2.<init>(r1)     // Catch:{ RemoteException -> 0x004e }
            r1 = r2
            goto L_0x0041
        L_0x0082:
            r1 = r3
            goto L_0x0041
        L_0x0084:
            r1 = r3
            goto L_0x0062
        L_0x0086:
            r1 = move-exception
            java.lang.String r2 = "Failed to get image."
            com.google.android.gms.internal.zzakb.m4793(r2, r1)
            r1 = r3
            goto L_0x0062
        L_0x008f:
            r1 = move-exception
            java.lang.String r2 = "Failed to get attribution info."
            com.google.android.gms.internal.zzakb.m4793(r2, r1)
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzqh.<init>(com.google.android.gms.internal.zzqe):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final IObjectWrapper m5793() {
        try {
            return this.f5323.m13258();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to retrieve native ad engine.", e);
            return null;
        }
    }

    public final void destroy() {
        try {
            this.f5323.m13256();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to destroy", e);
        }
    }

    public final NativeAd.AdChoicesInfo getAdChoicesInfo() {
        return this.f5319;
    }

    public final CharSequence getBody() {
        try {
            return this.f5323.m13263();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get body.", e);
            return null;
        }
    }

    public final CharSequence getCallToAction() {
        try {
            return this.f5323.m13259();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get call to action.", e);
            return null;
        }
    }

    public final Bundle getExtras() {
        try {
            return this.f5323.m13253();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get extras", e);
            return null;
        }
    }

    public final CharSequence getHeadline() {
        try {
            return this.f5323.m13265();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get headline.", e);
            return null;
        }
    }

    public final NativeAd.Image getIcon() {
        return this.f5322;
    }

    public final List<NativeAd.Image> getImages() {
        return this.f5320;
    }

    public final CharSequence getMediationAdapterClassName() {
        try {
            return this.f5323.m13254();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get mediation adapter class name.", e);
            return null;
        }
    }

    public final CharSequence getPrice() {
        try {
            return this.f5323.m13252();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get price.", e);
            return null;
        }
    }

    public final Double getStarRating() {
        try {
            double r2 = this.f5323.m13250();
            if (r2 == -1.0d) {
                return null;
            }
            return Double.valueOf(r2);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get star rating.", e);
            return null;
        }
    }

    public final CharSequence getStore() {
        try {
            return this.f5323.m13251();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get store", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            if (this.f5323.m13257() != null) {
                this.f5321.zza(this.f5323.m13257());
            }
        } catch (RemoteException e) {
            zzakb.m4793("Exception occurred while getting video controller", e);
        }
        return this.f5321;
    }

    public final void performClick(Bundle bundle) {
        try {
            this.f5323.m13266(bundle);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to perform click.", e);
        }
    }

    public final boolean recordImpression(Bundle bundle) {
        try {
            return this.f5323.m13261(bundle);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to record impression.", e);
            return false;
        }
    }

    public final void reportTouchEvent(Bundle bundle) {
        try {
            this.f5323.m13264(bundle);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to report touch event.", e);
        }
    }
}
