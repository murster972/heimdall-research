package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.PublisherAdView;

final class zzrp implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzks f10855;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzro f10856;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ PublisherAdView f10857;

    zzrp(zzro zzro, PublisherAdView publisherAdView, zzks zzks) {
        this.f10856 = zzro;
        this.f10857 = publisherAdView;
        this.f10855 = zzks;
    }

    public final void run() {
        if (this.f10857.zza(this.f10855)) {
            this.f10856.f10854.onPublisherAdViewLoaded(this.f10857);
        } else {
            zzakb.m4791("Could not bind ad manager");
        }
    }
}
