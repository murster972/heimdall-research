package com.google.android.gms.internal;

final class zzdqr implements zzdpq {
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzdpw m11743(java.lang.String r6, java.lang.String r7, int r8) throws java.security.GeneralSecurityException {
        /*
            r5 = this;
            r0 = -1
            r3 = 1
            r1 = 0
            java.lang.String r2 = r7.toLowerCase()
            int r4 = r2.hashCode()
            switch(r4) {
                case 275448849: goto L_0x0023;
                case 1420614889: goto L_0x002e;
                default: goto L_0x000e;
            }
        L_0x000e:
            r2 = r0
        L_0x000f:
            switch(r2) {
                case 0: goto L_0x0039;
                case 1: goto L_0x007e;
                default: goto L_0x0012;
            }
        L_0x0012:
            java.security.GeneralSecurityException r0 = new java.security.GeneralSecurityException
            java.lang.String r2 = "No support for primitive '%s'."
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r3[r1] = r7
            java.lang.String r1 = java.lang.String.format(r2, r3)
            r0.<init>(r1)
            throw r0
        L_0x0023:
            java.lang.String r4 = "hybriddecrypt"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x000e
            r2 = r1
            goto L_0x000f
        L_0x002e:
            java.lang.String r4 = "hybridencrypt"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x000e
            r2 = r3
            goto L_0x000f
        L_0x0039:
            int r2 = r6.hashCode()
            switch(r2) {
                case -80133005: goto L_0x0054;
                default: goto L_0x0040;
            }
        L_0x0040:
            switch(r0) {
                case 0: goto L_0x005f;
                default: goto L_0x0043;
            }
        L_0x0043:
            java.security.GeneralSecurityException r0 = new java.security.GeneralSecurityException
            java.lang.String r2 = "No support for primitive 'HybridEncrypt' with key type '%s'."
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r3[r1] = r6
            java.lang.String r1 = java.lang.String.format(r2, r3)
            r0.<init>(r1)
            throw r0
        L_0x0054:
            java.lang.String r2 = "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey"
            boolean r2 = r6.equals(r2)
            if (r2 == 0) goto L_0x0040
            r0 = r1
            goto L_0x0040
        L_0x005f:
            com.google.android.gms.internal.zzdqp r0 = new com.google.android.gms.internal.zzdqp
            r0.<init>()
        L_0x0064:
            if (r8 <= 0) goto L_0x00aa
            java.security.GeneralSecurityException r0 = new java.security.GeneralSecurityException
            java.lang.String r2 = "No key manager for key type '%s' with version at least %d."
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r4[r1] = r6
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)
            r4[r3] = r1
            java.lang.String r1 = java.lang.String.format(r2, r4)
            r0.<init>(r1)
            throw r0
        L_0x007e:
            int r2 = r6.hashCode()
            switch(r2) {
                case 396454335: goto L_0x0099;
                default: goto L_0x0085;
            }
        L_0x0085:
            switch(r0) {
                case 0: goto L_0x00a4;
                default: goto L_0x0088;
            }
        L_0x0088:
            java.security.GeneralSecurityException r0 = new java.security.GeneralSecurityException
            java.lang.String r2 = "No support for primitive 'HybridEncrypt' with key type '%s'."
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r3[r1] = r6
            java.lang.String r1 = java.lang.String.format(r2, r3)
            r0.<init>(r1)
            throw r0
        L_0x0099:
            java.lang.String r2 = "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey"
            boolean r2 = r6.equals(r2)
            if (r2 == 0) goto L_0x0085
            r0 = r1
            goto L_0x0085
        L_0x00a4:
            com.google.android.gms.internal.zzdqq r0 = new com.google.android.gms.internal.zzdqq
            r0.<init>()
            goto L_0x0064
        L_0x00aa:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdqr.m11743(java.lang.String, java.lang.String, int):com.google.android.gms.internal.zzdpw");
    }
}
