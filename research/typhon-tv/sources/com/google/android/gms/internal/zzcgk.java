package com.google.android.gms.internal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzcgk extends zzcjl {
    zzcgk(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Boolean m10512(double d, zzclu zzclu) {
        try {
            return m10519(new BigDecimal(d), zzclu, Math.ulp(d));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Boolean m10513(long j, zzclu zzclu) {
        try {
            return m10519(new BigDecimal(j), zzclu, 0.0d);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Boolean m10514(zzcls zzcls, zzcmb zzcmb, long j) {
        Boolean r0;
        if (zzcls.f9670 != null) {
            Boolean r02 = m10513(j, zzcls.f9670);
            if (r02 == null) {
                return null;
            }
            if (!r02.booleanValue()) {
                return false;
            }
        }
        HashSet hashSet = new HashSet();
        for (zzclt zzclt : zzcls.f9671) {
            if (TextUtils.isEmpty(zzclt.f9675)) {
                m11096().m10834().m10850("null or empty param name in filter. event", m11111().m10805(zzcmb.f9713));
                return null;
            }
            hashSet.add(zzclt.f9675);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (zzcmc zzcmc : zzcmb.f9716) {
            if (hashSet.contains(zzcmc.f9722)) {
                if (zzcmc.f9721 != null) {
                    arrayMap.put(zzcmc.f9722, zzcmc.f9721);
                } else if (zzcmc.f9720 != null) {
                    arrayMap.put(zzcmc.f9722, zzcmc.f9720);
                } else if (zzcmc.f9719 != null) {
                    arrayMap.put(zzcmc.f9722, zzcmc.f9719);
                } else {
                    m11096().m10834().m10851("Unknown value for param. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(zzcmc.f9722));
                    return null;
                }
            }
        }
        for (zzclt zzclt2 : zzcls.f9671) {
            boolean equals = Boolean.TRUE.equals(zzclt2.f9676);
            String str = zzclt2.f9675;
            if (TextUtils.isEmpty(str)) {
                m11096().m10834().m10850("Event has empty param name. event", m11111().m10805(zzcmb.f9713));
                return null;
            }
            Object obj = arrayMap.get(str);
            if (obj instanceof Long) {
                if (zzclt2.f9674 == null) {
                    m11096().m10834().m10851("No number filter for long param. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(str));
                    return null;
                }
                Boolean r03 = m10513(((Long) obj).longValue(), zzclt2.f9674);
                if (r03 == null) {
                    return null;
                }
                if ((!r03.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof Double) {
                if (zzclt2.f9674 == null) {
                    m11096().m10834().m10851("No number filter for double param. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(str));
                    return null;
                }
                Boolean r04 = m10512(((Double) obj).doubleValue(), zzclt2.f9674);
                if (r04 == null) {
                    return null;
                }
                if ((!r04.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof String) {
                if (zzclt2.f9677 != null) {
                    r0 = m10518((String) obj, zzclt2.f9677);
                } else if (zzclt2.f9674 == null) {
                    m11096().m10834().m10851("No filter for String param. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(str));
                    return null;
                } else if (zzclq.m11369((String) obj)) {
                    r0 = m10517((String) obj, zzclt2.f9674);
                } else {
                    m11096().m10834().m10851("Invalid param value for number filter. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(str));
                    return null;
                }
                if (r0 == null) {
                    return null;
                }
                if ((!r0.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj == null) {
                m11096().m10848().m10851("Missing param for filter. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(str));
                return false;
            } else {
                m11096().m10834().m10851("Unknown param type. event, param", m11111().m10805(zzcmb.f9713), m11111().m10794(str));
                return null;
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Boolean m10515(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Boolean m10516(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException e) {
                    m11096().m10834().m10850("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Boolean m10517(String str, zzclu zzclu) {
        if (!zzclq.m11369(str)) {
            return null;
        }
        try {
            return m10519(new BigDecimal(str), zzclu, 0.0d);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Boolean m10518(String str, zzclw zzclw) {
        List arrayList;
        String str2 = null;
        zzbq.m9120(zzclw);
        if (str == null || zzclw.f9690 == null || zzclw.f9690.intValue() == 0) {
            return null;
        }
        if (zzclw.f9690.intValue() == 6) {
            if (zzclw.f9688 == null || zzclw.f9688.length == 0) {
                return null;
            }
        } else if (zzclw.f9687 == null) {
            return null;
        }
        int intValue = zzclw.f9690.intValue();
        boolean z = zzclw.f9689 != null && zzclw.f9689.booleanValue();
        String upperCase = (z || intValue == 1 || intValue == 6) ? zzclw.f9687 : zzclw.f9687.toUpperCase(Locale.ENGLISH);
        if (zzclw.f9688 == null) {
            arrayList = null;
        } else {
            String[] strArr = zzclw.f9688;
            if (z) {
                arrayList = Arrays.asList(strArr);
            } else {
                arrayList = new ArrayList();
                for (String upperCase2 : strArr) {
                    arrayList.add(upperCase2.toUpperCase(Locale.ENGLISH));
                }
            }
        }
        if (intValue == 1) {
            str2 = upperCase;
        }
        return m10516(str, intValue, z, upperCase, arrayList, str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007d, code lost:
        if (r5 != null) goto L_0x007f;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Boolean m10519(java.math.BigDecimal r10, com.google.android.gms.internal.zzclu r11, double r12) {
        /*
            r8 = 4
            r7 = -1
            r1 = 0
            r0 = 1
            r2 = 0
            com.google.android.gms.common.internal.zzbq.m9120(r11)
            java.lang.Integer r3 = r11.f9682
            if (r3 == 0) goto L_0x0014
            java.lang.Integer r3 = r11.f9682
            int r3 = r3.intValue()
            if (r3 != 0) goto L_0x0016
        L_0x0014:
            r0 = r2
        L_0x0015:
            return r0
        L_0x0016:
            java.lang.Integer r3 = r11.f9682
            int r3 = r3.intValue()
            if (r3 != r8) goto L_0x0028
            java.lang.String r3 = r11.f9680
            if (r3 == 0) goto L_0x0026
            java.lang.String r3 = r11.f9678
            if (r3 != 0) goto L_0x002e
        L_0x0026:
            r0 = r2
            goto L_0x0015
        L_0x0028:
            java.lang.String r3 = r11.f9681
            if (r3 != 0) goto L_0x002e
            r0 = r2
            goto L_0x0015
        L_0x002e:
            java.lang.Integer r3 = r11.f9682
            int r6 = r3.intValue()
            java.lang.Integer r3 = r11.f9682
            int r3 = r3.intValue()
            if (r3 != r8) goto L_0x0066
            java.lang.String r3 = r11.f9680
            boolean r3 = com.google.android.gms.internal.zzclq.m11369(r3)
            if (r3 == 0) goto L_0x004c
            java.lang.String r3 = r11.f9678
            boolean r3 = com.google.android.gms.internal.zzclq.m11369(r3)
            if (r3 != 0) goto L_0x004e
        L_0x004c:
            r0 = r2
            goto L_0x0015
        L_0x004e:
            java.math.BigDecimal r4 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0063 }
            java.lang.String r3 = r11.f9680     // Catch:{ NumberFormatException -> 0x0063 }
            r4.<init>(r3)     // Catch:{ NumberFormatException -> 0x0063 }
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0063 }
            java.lang.String r5 = r11.f9678     // Catch:{ NumberFormatException -> 0x0063 }
            r3.<init>(r5)     // Catch:{ NumberFormatException -> 0x0063 }
            r5 = r2
        L_0x005d:
            if (r6 != r8) goto L_0x007d
            if (r4 != 0) goto L_0x007f
            r0 = r2
            goto L_0x0015
        L_0x0063:
            r0 = move-exception
            r0 = r2
            goto L_0x0015
        L_0x0066:
            java.lang.String r3 = r11.f9681
            boolean r3 = com.google.android.gms.internal.zzclq.m11369(r3)
            if (r3 != 0) goto L_0x0070
            r0 = r2
            goto L_0x0015
        L_0x0070:
            java.math.BigDecimal r5 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x007a }
            java.lang.String r3 = r11.f9681     // Catch:{ NumberFormatException -> 0x007a }
            r5.<init>(r3)     // Catch:{ NumberFormatException -> 0x007a }
            r3 = r2
            r4 = r2
            goto L_0x005d
        L_0x007a:
            r0 = move-exception
            r0 = r2
            goto L_0x0015
        L_0x007d:
            if (r5 == 0) goto L_0x0082
        L_0x007f:
            switch(r6) {
                case 1: goto L_0x0084;
                case 2: goto L_0x0091;
                case 3: goto L_0x009f;
                case 4: goto L_0x00ed;
                default: goto L_0x0082;
            }
        L_0x0082:
            r0 = r2
            goto L_0x0015
        L_0x0084:
            int r2 = r10.compareTo(r5)
            if (r2 != r7) goto L_0x008f
        L_0x008a:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x0015
        L_0x008f:
            r0 = r1
            goto L_0x008a
        L_0x0091:
            int r2 = r10.compareTo(r5)
            if (r2 != r0) goto L_0x009d
        L_0x0097:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x0015
        L_0x009d:
            r0 = r1
            goto L_0x0097
        L_0x009f:
            r2 = 0
            int r2 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x00df
            java.math.BigDecimal r2 = new java.math.BigDecimal
            r2.<init>(r12)
            java.math.BigDecimal r3 = new java.math.BigDecimal
            r4 = 2
            r3.<init>(r4)
            java.math.BigDecimal r2 = r2.multiply(r3)
            java.math.BigDecimal r2 = r5.subtract(r2)
            int r2 = r10.compareTo(r2)
            if (r2 != r0) goto L_0x00dd
            java.math.BigDecimal r2 = new java.math.BigDecimal
            r2.<init>(r12)
            java.math.BigDecimal r3 = new java.math.BigDecimal
            r4 = 2
            r3.<init>(r4)
            java.math.BigDecimal r2 = r2.multiply(r3)
            java.math.BigDecimal r2 = r5.add(r2)
            int r2 = r10.compareTo(r2)
            if (r2 != r7) goto L_0x00dd
        L_0x00d7:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x0015
        L_0x00dd:
            r0 = r1
            goto L_0x00d7
        L_0x00df:
            int r2 = r10.compareTo(r5)
            if (r2 != 0) goto L_0x00eb
        L_0x00e5:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x0015
        L_0x00eb:
            r0 = r1
            goto L_0x00e5
        L_0x00ed:
            int r2 = r10.compareTo(r4)
            if (r2 == r7) goto L_0x00ff
            int r2 = r10.compareTo(r3)
            if (r2 == r0) goto L_0x00ff
        L_0x00f9:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x0015
        L_0x00ff:
            r0 = r1
            goto L_0x00f9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcgk.m10519(java.math.BigDecimal, com.google.android.gms.internal.zzclu, double):java.lang.Boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10520() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcma[] m10521(String str, zzcmb[] zzcmbArr, zzcmg[] zzcmgArr) {
        Map map;
        Boolean bool;
        zzcgw r5;
        Map map2;
        zzbq.m9122(str);
        HashSet hashSet = new HashSet();
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        Map<Integer, zzcmf> r8 = m11095().m10591(str);
        if (r8 != null) {
            for (Integer intValue : r8.keySet()) {
                int intValue2 = intValue.intValue();
                zzcmf zzcmf = r8.get(Integer.valueOf(intValue2));
                BitSet bitSet = (BitSet) arrayMap2.get(Integer.valueOf(intValue2));
                BitSet bitSet2 = (BitSet) arrayMap3.get(Integer.valueOf(intValue2));
                if (bitSet == null) {
                    bitSet = new BitSet();
                    arrayMap2.put(Integer.valueOf(intValue2), bitSet);
                    bitSet2 = new BitSet();
                    arrayMap3.put(Integer.valueOf(intValue2), bitSet2);
                }
                for (int i = 0; i < (zzcmf.f9761.length << 6); i++) {
                    if (zzclq.m11390(zzcmf.f9761, i)) {
                        m11096().m10848().m10851("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue2), Integer.valueOf(i));
                        bitSet2.set(i);
                        if (zzclq.m11390(zzcmf.f9760, i)) {
                            bitSet.set(i);
                        }
                    }
                }
                zzcma zzcma = new zzcma();
                arrayMap.put(Integer.valueOf(intValue2), zzcma);
                zzcma.f9708 = false;
                zzcma.f9709 = zzcmf;
                zzcma.f9707 = new zzcmf();
                zzcma.f9707.f9760 = zzclq.m11392(bitSet);
                zzcma.f9707.f9761 = zzclq.m11392(bitSet2);
            }
        }
        if (zzcmbArr != null) {
            ArrayMap arrayMap4 = new ArrayMap();
            int length = zzcmbArr.length;
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= length) {
                    break;
                }
                zzcmb zzcmb = zzcmbArr[i3];
                zzcgw r4 = m11095().m10601(str, zzcmb.f9713);
                if (r4 == null) {
                    m11096().m10834().m10851("Event aggregate wasn't created during raw event logging. appId, event", zzchm.m10812(str), m11111().m10805(zzcmb.f9713));
                    r5 = new zzcgw(str, zzcmb.f9713, 1, 1, zzcmb.f9715.longValue(), 0, (Long) null, (Long) null, (Boolean) null);
                } else {
                    r5 = r4.m10656();
                }
                m11095().m10608(r5);
                long j = r5.f9194;
                Map map3 = (Map) arrayMap4.get(zzcmb.f9713);
                if (map3 == null) {
                    Map r42 = m11095().m10576(str, zzcmb.f9713);
                    if (r42 == null) {
                        r42 = new ArrayMap();
                    }
                    arrayMap4.put(zzcmb.f9713, r42);
                    map2 = r42;
                } else {
                    map2 = map3;
                }
                for (Integer intValue3 : map2.keySet()) {
                    int intValue4 = intValue3.intValue();
                    if (hashSet.contains(Integer.valueOf(intValue4))) {
                        m11096().m10848().m10850("Skipping failed audience ID", Integer.valueOf(intValue4));
                    } else {
                        zzcma zzcma2 = (zzcma) arrayMap.get(Integer.valueOf(intValue4));
                        BitSet bitSet3 = (BitSet) arrayMap2.get(Integer.valueOf(intValue4));
                        BitSet bitSet4 = (BitSet) arrayMap3.get(Integer.valueOf(intValue4));
                        if (zzcma2 == null) {
                            zzcma zzcma3 = new zzcma();
                            arrayMap.put(Integer.valueOf(intValue4), zzcma3);
                            zzcma3.f9708 = true;
                            bitSet3 = new BitSet();
                            arrayMap2.put(Integer.valueOf(intValue4), bitSet3);
                            bitSet4 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue4), bitSet4);
                        }
                        for (zzcls zzcls : (List) map2.get(Integer.valueOf(intValue4))) {
                            if (m11096().m10844(2)) {
                                m11096().m10848().m10852("Evaluating filter. audience, filter, event", Integer.valueOf(intValue4), zzcls.f9672, m11111().m10805(zzcls.f9669));
                                m11096().m10848().m10850("Filter definition", m11111().m10802(zzcls));
                            }
                            if (zzcls.f9672 == null || zzcls.f9672.intValue() > 256) {
                                m11096().m10834().m10851("Invalid event filter ID. appId, id", zzchm.m10812(str), String.valueOf(zzcls.f9672));
                            } else if (bitSet3.get(zzcls.f9672.intValue())) {
                                m11096().m10848().m10851("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue4), zzcls.f9672);
                            } else {
                                Boolean r9 = m10514(zzcls, zzcmb, j);
                                m11096().m10848().m10850("Event filter result", r9 == null ? "null" : r9);
                                if (r9 == null) {
                                    hashSet.add(Integer.valueOf(intValue4));
                                } else {
                                    bitSet4.set(zzcls.f9672.intValue());
                                    if (r9.booleanValue()) {
                                        bitSet3.set(zzcls.f9672.intValue());
                                    }
                                }
                            }
                        }
                    }
                }
                i2 = i3 + 1;
            }
        }
        if (zzcmgArr != null) {
            ArrayMap arrayMap5 = new ArrayMap();
            for (zzcmg zzcmg : zzcmgArr) {
                Map map4 = (Map) arrayMap5.get(zzcmg.f9765);
                if (map4 == null) {
                    Map r43 = m11095().m10578(str, zzcmg.f9765);
                    if (r43 == null) {
                        r43 = new ArrayMap();
                    }
                    arrayMap5.put(zzcmg.f9765, r43);
                    map = r43;
                } else {
                    map = map4;
                }
                for (Integer intValue5 : map.keySet()) {
                    int intValue6 = intValue5.intValue();
                    if (!hashSet.contains(Integer.valueOf(intValue6))) {
                        zzcma zzcma4 = (zzcma) arrayMap.get(Integer.valueOf(intValue6));
                        BitSet bitSet5 = (BitSet) arrayMap2.get(Integer.valueOf(intValue6));
                        BitSet bitSet6 = (BitSet) arrayMap3.get(Integer.valueOf(intValue6));
                        if (zzcma4 == null) {
                            zzcma zzcma5 = new zzcma();
                            arrayMap.put(Integer.valueOf(intValue6), zzcma5);
                            zzcma5.f9708 = true;
                            bitSet5 = new BitSet();
                            arrayMap2.put(Integer.valueOf(intValue6), bitSet5);
                            bitSet6 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue6), bitSet6);
                        }
                        Iterator it2 = ((List) map.get(Integer.valueOf(intValue6))).iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            zzclv zzclv = (zzclv) it2.next();
                            if (m11096().m10844(2)) {
                                m11096().m10848().m10852("Evaluating filter. audience, filter, property", Integer.valueOf(intValue6), zzclv.f9686, m11111().m10797(zzclv.f9684));
                                m11096().m10848().m10850("Filter definition", m11111().m10803(zzclv));
                            }
                            if (zzclv.f9686 == null || zzclv.f9686.intValue() > 256) {
                                m11096().m10834().m10851("Invalid property filter ID. appId, id", zzchm.m10812(str), String.valueOf(zzclv.f9686));
                                hashSet.add(Integer.valueOf(intValue6));
                            } else if (bitSet5.get(zzclv.f9686.intValue())) {
                                m11096().m10848().m10851("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue6), zzclv.f9686);
                            } else {
                                zzclt zzclt = zzclv.f9685;
                                if (zzclt == null) {
                                    m11096().m10834().m10850("Missing property filter. property", m11111().m10797(zzcmg.f9765));
                                    bool = null;
                                } else {
                                    boolean equals = Boolean.TRUE.equals(zzclt.f9676);
                                    if (zzcmg.f9766 != null) {
                                        if (zzclt.f9674 == null) {
                                            m11096().m10834().m10850("No number filter for long property. property", m11111().m10797(zzcmg.f9765));
                                            bool = null;
                                        } else {
                                            bool = m10515(m10513(zzcmg.f9766.longValue(), zzclt.f9674), equals);
                                        }
                                    } else if (zzcmg.f9764 != null) {
                                        if (zzclt.f9674 == null) {
                                            m11096().m10834().m10850("No number filter for double property. property", m11111().m10797(zzcmg.f9765));
                                            bool = null;
                                        } else {
                                            bool = m10515(m10512(zzcmg.f9764.doubleValue(), zzclt.f9674), equals);
                                        }
                                    } else if (zzcmg.f9767 == null) {
                                        m11096().m10834().m10850("User property has no value, property", m11111().m10797(zzcmg.f9765));
                                        bool = null;
                                    } else if (zzclt.f9677 == null) {
                                        if (zzclt.f9674 == null) {
                                            m11096().m10834().m10850("No string or number filter defined. property", m11111().m10797(zzcmg.f9765));
                                        } else if (zzclq.m11369(zzcmg.f9767)) {
                                            bool = m10515(m10517(zzcmg.f9767, zzclt.f9674), equals);
                                        } else {
                                            m11096().m10834().m10851("Invalid user property value for Numeric number filter. property, value", m11111().m10797(zzcmg.f9765), zzcmg.f9767);
                                        }
                                        bool = null;
                                    } else {
                                        bool = m10515(m10518(zzcmg.f9767, zzclt.f9677), equals);
                                    }
                                }
                                m11096().m10848().m10850("Property filter result", bool == null ? "null" : bool);
                                if (bool == null) {
                                    hashSet.add(Integer.valueOf(intValue6));
                                } else {
                                    bitSet6.set(zzclv.f9686.intValue());
                                    if (bool.booleanValue()) {
                                        bitSet5.set(zzclv.f9686.intValue());
                                    }
                                }
                            }
                        }
                    } else {
                        m11096().m10848().m10850("Skipping failed audience ID", Integer.valueOf(intValue6));
                    }
                }
            }
        }
        zzcma[] zzcmaArr = new zzcma[arrayMap2.size()];
        int i4 = 0;
        for (Integer intValue7 : arrayMap2.keySet()) {
            int intValue8 = intValue7.intValue();
            if (!hashSet.contains(Integer.valueOf(intValue8))) {
                zzcma zzcma6 = (zzcma) arrayMap.get(Integer.valueOf(intValue8));
                zzcma zzcma7 = zzcma6 == null ? new zzcma() : zzcma6;
                int i5 = i4 + 1;
                zzcmaArr[i4] = zzcma7;
                zzcma7.f9710 = Integer.valueOf(intValue8);
                zzcma7.f9707 = new zzcmf();
                zzcma7.f9707.f9760 = zzclq.m11392((BitSet) arrayMap2.get(Integer.valueOf(intValue8)));
                zzcma7.f9707.f9761 = zzclq.m11392((BitSet) arrayMap3.get(Integer.valueOf(intValue8)));
                zzcgo r52 = m11095();
                zzcmf zzcmf2 = zzcma7.f9707;
                r52.m11115();
                r52.m11109();
                zzbq.m9122(str);
                zzbq.m9120(zzcmf2);
                try {
                    byte[] bArr = new byte[zzcmf2.m12872()];
                    zzfjk r11 = zzfjk.m12822(bArr, 0, bArr.length);
                    zzcmf2.m12877(r11);
                    r11.m12829();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str);
                    contentValues.put("audience_id", Integer.valueOf(intValue8));
                    contentValues.put("current_results", bArr);
                    try {
                        if (r52.m10587().insertWithOnConflict("audience_filter_values", (String) null, contentValues, 5) == -1) {
                            r52.m11096().m10832().m10850("Failed to insert filter results (got -1). appId", zzchm.m10812(str));
                        }
                        i4 = i5;
                    } catch (SQLiteException e) {
                        r52.m11096().m10832().m10851("Error storing filter results. appId", zzchm.m10812(str), e);
                        i4 = i5;
                    }
                } catch (IOException e2) {
                    r52.m11096().m10832().m10851("Configuration loss. Failed to serialize filter results. appId", zzchm.m10812(str), e2);
                    i4 = i5;
                }
            }
        }
        return (zzcma[]) Arrays.copyOf(zzcmaArr, i4);
    }
}
