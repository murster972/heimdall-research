package com.google.android.gms.internal;

import java.io.IOException;

public class zzfge extends IOException {
    private zzfhe zzphw = null;

    public zzfge(String str) {
        super(str);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static zzfgf m12585() {
        return new zzfgf("Protocol message tag had invalid wire type.");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static zzfge m12586() {
        return new zzfge("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    static zzfge m12587() {
        return new zzfge("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    static zzfge m12588() {
        return new zzfge("Protocol message had invalid UTF-8.");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    static zzfge m12589() {
        return new zzfge("Protocol message end-group tag did not match expected tag.");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static zzfge m12590() {
        return new zzfge("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static zzfge m12591() {
        return new zzfge("Protocol message contained an invalid tag (zero).");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static zzfge m12592() {
        return new zzfge("CodedInputStream encountered a malformed varint.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzfge m12593() {
        return new zzfge("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    public final zzfge zzi(zzfhe zzfhe) {
        this.zzphw = zzfhe;
        return this;
    }
}
