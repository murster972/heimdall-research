package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.search.SearchAdRequest;

@zzzv
public final class zzmn extends zzbfm {
    public static final Parcelable.Creator<zzmn> CREATOR = new zzmo();

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f4876;

    public zzmn(SearchAdRequest searchAdRequest) {
        this.f4876 = searchAdRequest.getQuery();
    }

    zzmn(String str) {
        this.f4876 = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 15, this.f4876, false);
        zzbfp.m10182(parcel, r0);
    }
}
