package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;

public final class zzcgx extends zzbfm implements Iterable<String> {
    public static final Parcelable.Creator<zzcgx> CREATOR = new zzcgz();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle f9196;

    zzcgx(Bundle bundle) {
        this.f9196 = bundle;
    }

    public final Iterator<String> iterator() {
        return new zzcgy(this);
    }

    public final String toString() {
        return this.f9196.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10187(parcel, 2, m10660(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Bundle m10660() {
        return new Bundle(this.f9196);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final Long m10661(String str) {
        return Long.valueOf(this.f9196.getLong(str));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final String m10662(String str) {
        return this.f9196.getString(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final Double m10663(String str) {
        return Double.valueOf(this.f9196.getDouble(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10664() {
        return this.f9196.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m10665(String str) {
        return this.f9196.get(str);
    }
}
