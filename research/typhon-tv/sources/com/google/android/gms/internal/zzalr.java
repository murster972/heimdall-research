package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.TextureView;
import com.google.android.gms.ads.internal.zzbs;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@zzzv
@TargetApi(14)
public final class zzalr extends zzamb implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Map<Integer, String> f4322 = new HashMap();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f4323 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f4324 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MediaPlayer f4325;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f4326;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f4327;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f4328;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f4329;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public zzama f4330;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Uri f4331;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f4332;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f4333;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f4334;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzamq f4335;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private zzamn f4336;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f4337;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f4322.put(-1004, "MEDIA_ERROR_IO");
            f4322.put(-1007, "MEDIA_ERROR_MALFORMED");
            f4322.put(-1010, "MEDIA_ERROR_UNSUPPORTED");
            f4322.put(-110, "MEDIA_ERROR_TIMED_OUT");
            f4322.put(3, "MEDIA_INFO_VIDEO_RENDERING_START");
        }
        f4322.put(100, "MEDIA_ERROR_SERVER_DIED");
        f4322.put(1, "MEDIA_ERROR_UNKNOWN");
        f4322.put(1, "MEDIA_INFO_UNKNOWN");
        f4322.put(700, "MEDIA_INFO_VIDEO_TRACK_LAGGING");
        f4322.put(701, "MEDIA_INFO_BUFFERING_START");
        f4322.put(702, "MEDIA_INFO_BUFFERING_END");
        f4322.put(800, "MEDIA_INFO_BAD_INTERLEAVING");
        f4322.put(801, "MEDIA_INFO_NOT_SEEKABLE");
        f4322.put(802, "MEDIA_INFO_METADATA_UPDATE");
        if (Build.VERSION.SDK_INT >= 19) {
            f4322.put(901, "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
            f4322.put(902, "MEDIA_INFO_SUBTITLE_TIMED_OUT");
        }
    }

    public zzalr(Context context, boolean z, boolean z2, zzamo zzamo, zzamq zzamq) {
        super(context);
        setSurfaceTextureListener(this);
        this.f4335 = zzamq;
        this.f4337 = z;
        this.f4334 = z2;
        this.f4335.m4938(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m4837() {
        SurfaceTexture surfaceTexture;
        zzagf.m4527("AdMediaPlayerView init MediaPlayer");
        SurfaceTexture surfaceTexture2 = getSurfaceTexture();
        if (this.f4331 != null && surfaceTexture2 != null) {
            m4843(false);
            try {
                zzbs.zzex();
                this.f4325 = new MediaPlayer();
                this.f4325.setOnBufferingUpdateListener(this);
                this.f4325.setOnCompletionListener(this);
                this.f4325.setOnErrorListener(this);
                this.f4325.setOnInfoListener(this);
                this.f4325.setOnPreparedListener(this);
                this.f4325.setOnVideoSizeChangedListener(this);
                this.f4328 = 0;
                if (this.f4337) {
                    this.f4336 = new zzamn(getContext());
                    this.f4336.m4916(surfaceTexture2, getWidth(), getHeight());
                    this.f4336.start();
                    surfaceTexture = this.f4336.m4912();
                    if (surfaceTexture == null) {
                        this.f4336.m4911();
                        this.f4336 = null;
                    }
                    this.f4325.setDataSource(getContext(), this.f4331);
                    zzbs.zzey();
                    this.f4325.setSurface(new Surface(surfaceTexture));
                    this.f4325.setAudioStreamType(3);
                    this.f4325.setScreenOnWhilePlaying(true);
                    this.f4325.prepareAsync();
                    m4840(1);
                }
                surfaceTexture = surfaceTexture2;
                this.f4325.setDataSource(getContext(), this.f4331);
                zzbs.zzey();
                this.f4325.setSurface(new Surface(surfaceTexture));
                this.f4325.setAudioStreamType(3);
                this.f4325.setScreenOnWhilePlaying(true);
                this.f4325.prepareAsync();
                m4840(1);
            } catch (IOException | IllegalArgumentException | IllegalStateException e) {
                String valueOf = String.valueOf(this.f4331);
                zzagf.m4796(new StringBuilder(String.valueOf(valueOf).length() + 36).append("Failed to initialize MediaPlayer at ").append(valueOf).toString(), e);
                onError(this.f4325, 1, 0);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0035 A[LOOP:0: B:9:0x0035->B:14:0x0050, LOOP_START] */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m4838() {
        /*
            r8 = this;
            boolean r0 = r8.f4334
            if (r0 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            boolean r0 = r8.m4839()
            if (r0 == 0) goto L_0x0004
            android.media.MediaPlayer r0 = r8.f4325
            int r0 = r0.getCurrentPosition()
            if (r0 <= 0) goto L_0x0004
            int r0 = r8.f4324
            r1 = 3
            if (r0 == r1) goto L_0x0004
            java.lang.String r0 = "AdMediaPlayerView nudging MediaPlayer"
            com.google.android.gms.internal.zzagf.m4527(r0)
            r0 = 0
            r8.m4842((float) r0)
            android.media.MediaPlayer r0 = r8.f4325
            r0.start()
            android.media.MediaPlayer r0 = r8.f4325
            int r0 = r0.getCurrentPosition()
            com.google.android.gms.common.util.zzd r1 = com.google.android.gms.ads.internal.zzbs.zzeo()
            long r2 = r1.m9243()
        L_0x0035:
            boolean r1 = r8.m4839()
            if (r1 == 0) goto L_0x0052
            android.media.MediaPlayer r1 = r8.f4325
            int r1 = r1.getCurrentPosition()
            if (r1 != r0) goto L_0x0052
            com.google.android.gms.common.util.zzd r1 = com.google.android.gms.ads.internal.zzbs.zzeo()
            long r4 = r1.m9243()
            long r4 = r4 - r2
            r6 = 250(0xfa, double:1.235E-321)
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x0035
        L_0x0052:
            android.media.MediaPlayer r0 = r8.f4325
            r0.pause()
            r8.m4852()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzalr.m4838():void");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final boolean m4839() {
        return (this.f4325 == null || this.f4323 == -1 || this.f4323 == 0 || this.f4323 == 1) ? false : true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4840(int i) {
        if (i == 3) {
            this.f4335.m4936();
            this.f4338.m4943();
        } else if (this.f4323 == 3) {
            this.f4335.m4935();
            this.f4338.m4944();
        }
        this.f4323 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4842(float f) {
        if (this.f4325 != null) {
            try {
                this.f4325.setVolume(f, f);
            } catch (IllegalStateException e) {
            }
        } else {
            zzagf.m4791("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4843(boolean z) {
        zzagf.m4527("AdMediaPlayerView release");
        if (this.f4336 != null) {
            this.f4336.m4911();
            this.f4336 = null;
        }
        if (this.f4325 != null) {
            this.f4325.reset();
            this.f4325.release();
            this.f4325 = null;
            m4840(0);
            if (z) {
                this.f4324 = 0;
                this.f4324 = 0;
            }
        }
    }

    public final int getCurrentPosition() {
        if (m4839()) {
            return this.f4325.getCurrentPosition();
        }
        return 0;
    }

    public final int getDuration() {
        if (m4839()) {
            return this.f4325.getDuration();
        }
        return -1;
    }

    public final int getVideoHeight() {
        if (this.f4325 != null) {
            return this.f4325.getVideoHeight();
        }
        return 0;
    }

    public final int getVideoWidth() {
        if (this.f4325 != null) {
            return this.f4325.getVideoWidth();
        }
        return 0;
    }

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        this.f4328 = i;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        zzagf.m4527("AdMediaPlayerView completion");
        m4840(5);
        this.f4324 = 5;
        zzahn.f4212.post(new zzalt(this));
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        String str = f4322.get(Integer.valueOf(i));
        String str2 = f4322.get(Integer.valueOf(i2));
        zzagf.m4791(new StringBuilder(String.valueOf(str).length() + 38 + String.valueOf(str2).length()).append("AdMediaPlayerView MediaPlayer error: ").append(str).append(":").append(str2).toString());
        m4840(-1);
        this.f4324 = -1;
        zzahn.f4212.post(new zzalu(this, str, str2));
        return true;
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        String str = f4322.get(Integer.valueOf(i));
        String str2 = f4322.get(Integer.valueOf(i2));
        zzagf.m4527(new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(str2).length()).append("AdMediaPlayerView MediaPlayer info: ").append(str).append(":").append(str2).toString());
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0090, code lost:
        if (r1 > r2) goto L_0x003f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            r3 = 1073741824(0x40000000, float:2.0)
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            int r0 = r7.f4332
            int r1 = getDefaultSize(r0, r8)
            int r0 = r7.f4333
            int r0 = getDefaultSize(r0, r9)
            int r2 = r7.f4332
            if (r2 <= 0) goto L_0x0092
            int r2 = r7.f4333
            if (r2 <= 0) goto L_0x0092
            com.google.android.gms.internal.zzamn r2 = r7.f4336
            if (r2 != 0) goto L_0x0092
            int r4 = android.view.View.MeasureSpec.getMode(r8)
            int r2 = android.view.View.MeasureSpec.getSize(r8)
            int r5 = android.view.View.MeasureSpec.getMode(r9)
            int r0 = android.view.View.MeasureSpec.getSize(r9)
            if (r4 != r3) goto L_0x0078
            if (r5 != r3) goto L_0x0078
            int r1 = r7.f4332
            int r1 = r1 * r0
            int r3 = r7.f4333
            int r3 = r3 * r2
            if (r1 >= r3) goto L_0x0069
            int r1 = r7.f4332
            int r1 = r1 * r0
            int r2 = r7.f4333
            int r1 = r1 / r2
            r2 = r1
        L_0x003f:
            r7.setMeasuredDimension(r2, r0)
            com.google.android.gms.internal.zzamn r1 = r7.f4336
            if (r1 == 0) goto L_0x004b
            com.google.android.gms.internal.zzamn r1 = r7.f4336
            r1.m4915((int) r2, (int) r0)
        L_0x004b:
            int r1 = android.os.Build.VERSION.SDK_INT
            r3 = 16
            if (r1 != r3) goto L_0x0068
            int r1 = r7.f4326
            if (r1 <= 0) goto L_0x0059
            int r1 = r7.f4326
            if (r1 != r2) goto L_0x0061
        L_0x0059:
            int r1 = r7.f4327
            if (r1 <= 0) goto L_0x0064
            int r1 = r7.f4327
            if (r1 == r0) goto L_0x0064
        L_0x0061:
            r7.m4838()
        L_0x0064:
            r7.f4326 = r2
            r7.f4327 = r0
        L_0x0068:
            return
        L_0x0069:
            int r1 = r7.f4332
            int r1 = r1 * r0
            int r3 = r7.f4333
            int r3 = r3 * r2
            if (r1 <= r3) goto L_0x003f
            int r0 = r7.f4333
            int r0 = r0 * r2
            int r1 = r7.f4332
            int r0 = r0 / r1
            goto L_0x003f
        L_0x0078:
            if (r4 != r3) goto L_0x0086
            int r1 = r7.f4333
            int r1 = r1 * r2
            int r3 = r7.f4332
            int r1 = r1 / r3
            if (r5 != r6) goto L_0x0084
            if (r1 > r0) goto L_0x003f
        L_0x0084:
            r0 = r1
            goto L_0x003f
        L_0x0086:
            if (r5 != r3) goto L_0x0094
            int r1 = r7.f4332
            int r1 = r1 * r0
            int r3 = r7.f4333
            int r1 = r1 / r3
            if (r4 != r6) goto L_0x0092
            if (r1 > r2) goto L_0x003f
        L_0x0092:
            r2 = r1
            goto L_0x003f
        L_0x0094:
            int r1 = r7.f4332
            int r3 = r7.f4333
            if (r5 != r6) goto L_0x00ad
            if (r3 <= r0) goto L_0x00ad
            int r1 = r7.f4332
            int r1 = r1 * r0
            int r3 = r7.f4333
            int r1 = r1 / r3
        L_0x00a2:
            if (r4 != r6) goto L_0x0092
            if (r1 <= r2) goto L_0x0092
            int r0 = r7.f4333
            int r0 = r0 * r2
            int r1 = r7.f4332
            int r0 = r0 / r1
            goto L_0x003f
        L_0x00ad:
            r0 = r3
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzalr.onMeasure(int, int):void");
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        zzagf.m4527("AdMediaPlayerView prepared");
        m4840(2);
        this.f4335.m4937();
        zzahn.f4212.post(new zzals(this));
        this.f4332 = mediaPlayer.getVideoWidth();
        this.f4333 = mediaPlayer.getVideoHeight();
        if (this.f4329 != 0) {
            m4858(this.f4329);
        }
        m4838();
        int i = this.f4332;
        zzagf.m4794(new StringBuilder(62).append("AdMediaPlayerView stream dimensions: ").append(i).append(" x ").append(this.f4333).toString());
        if (this.f4324 == 3) {
            m4855();
        }
        m4852();
    }

    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        zzagf.m4527("AdMediaPlayerView surface created");
        m4837();
        zzahn.f4212.post(new zzalv(this));
    }

    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        zzagf.m4527("AdMediaPlayerView surface destroyed");
        if (this.f4325 != null && this.f4329 == 0) {
            this.f4329 = this.f4325.getCurrentPosition();
        }
        if (this.f4336 != null) {
            this.f4336.m4911();
        }
        zzahn.f4212.post(new zzalx(this));
        m4843(true);
        return true;
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        boolean z = true;
        zzagf.m4527("AdMediaPlayerView surface changed");
        boolean z2 = this.f4324 == 3;
        if (!(this.f4332 == i && this.f4333 == i2)) {
            z = false;
        }
        if (this.f4325 != null && z2 && z) {
            if (this.f4329 != 0) {
                m4858(this.f4329);
            }
            m4855();
        }
        if (this.f4336 != null) {
            this.f4336.m4915(i, i2);
        }
        zzahn.f4212.post(new zzalw(this, i, i2));
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.f4335.m4934(this);
        this.f4339.m4898(surfaceTexture, this.f4330);
    }

    public final void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        zzagf.m4527(new StringBuilder(57).append("AdMediaPlayerView size changed: ").append(i).append(" x ").append(i2).toString());
        this.f4332 = mediaPlayer.getVideoWidth();
        this.f4333 = mediaPlayer.getVideoHeight();
        if (this.f4332 != 0 && this.f4333 != 0) {
            requestLayout();
        }
    }

    public final void setVideoPath(String str) {
        Uri parse = Uri.parse(str);
        zzil r1 = zzil.m5427(parse);
        if (r1 != null) {
            parse = Uri.parse(r1.f4745);
        }
        this.f4331 = parse;
        this.f4329 = 0;
        m4837();
        requestLayout();
        invalidate();
    }

    public final String toString() {
        String name = getClass().getName();
        String hexString = Integer.toHexString(hashCode());
        return new StringBuilder(String.valueOf(name).length() + 1 + String.valueOf(hexString).length()).append(name).append("@").append(hexString).toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4844() {
        m4842(this.f4338.m4945());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4845() {
        zzagf.m4527("AdMediaPlayerView stop");
        if (this.f4325 != null) {
            this.f4325.stop();
            this.f4325.release();
            this.f4325 = null;
            m4840(0);
            this.f4324 = 0;
        }
        this.f4335.m4933();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4846() {
        zzagf.m4527("AdMediaPlayerView pause");
        if (m4839() && this.f4325.isPlaying()) {
            this.f4325.pause();
            m4840(4);
            zzahn.f4212.post(new zzalz(this));
        }
        this.f4324 = 4;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4847() {
        zzagf.m4527("AdMediaPlayerView play");
        if (m4839()) {
            this.f4325.start();
            m4840(3);
            this.f4339.m4897();
            zzahn.f4212.post(new zzaly(this));
        }
        this.f4324 = 3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m4848() {
        String valueOf = String.valueOf(this.f4337 ? " spherical" : "");
        return valueOf.length() != 0 ? "MediaPlayer".concat(valueOf) : new String("MediaPlayer");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4849(float f, float f2) {
        if (this.f4336 != null) {
            this.f4336.m4914(f, f2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4850(int i) {
        zzagf.m4527(new StringBuilder(34).append("AdMediaPlayerView seek ").append(i).toString());
        if (m4839()) {
            this.f4325.seekTo(i);
            this.f4329 = 0;
            return;
        }
        this.f4329 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4851(zzama zzama) {
        this.f4330 = zzama;
    }
}
