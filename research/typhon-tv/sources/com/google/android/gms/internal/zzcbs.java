package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzcbs implements Parcelable.Creator<zzcbr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r2 = zzbfn.m10169(parcel);
        int i = 0;
        byte[] bArr = null;
        while (parcel.dataPosition() < r2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    bArr = zzbfn.m10154(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r2);
        return new zzcbr(i, bArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcbr[i];
    }
}
