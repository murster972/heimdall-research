package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzki extends zzev implements zzkh {
    public zzki() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IAdListener");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m13060();
                break;
            case 2:
                m13061(parcel.readInt());
                break;
            case 3:
                m13057();
                break;
            case 4:
                m13059();
                break;
            case 5:
                m13058();
                break;
            case 6:
                m13056();
                break;
            case 7:
                m13055();
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
