package com.google.android.gms.internal;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzm;

public abstract class zzbbv<R extends Result> extends zzm<R, zzbcf> {
    public zzbbv(GoogleApiClient googleApiClient) {
        super(Cast.f6963, googleApiClient);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9957(int i) {
        m4198(m4195(new Status(2001)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ void m9958(Object obj) {
        super.m4198((Result) obj);
    }
}
