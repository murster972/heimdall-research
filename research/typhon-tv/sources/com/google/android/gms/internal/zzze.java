package com.google.android.gms.internal;

import android.support.v4.util.SimpleArrayMap;
import android.view.View;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzze implements zzys<zzon> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f5615;

    public zzze(boolean z) {
        this.f5615 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <K, V> SimpleArrayMap<K, V> m6080(SimpleArrayMap<K, Future<V>> simpleArrayMap) throws InterruptedException, ExecutionException {
        SimpleArrayMap<K, V> simpleArrayMap2 = new SimpleArrayMap<>();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= simpleArrayMap.size()) {
                return simpleArrayMap2;
            }
            simpleArrayMap2.put(simpleArrayMap.keyAt(i2), simpleArrayMap.valueAt(i2).get());
            i = i2 + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzou m6081(zzym zzym, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException {
        View view;
        SimpleArrayMap simpleArrayMap = new SimpleArrayMap();
        SimpleArrayMap simpleArrayMap2 = new SimpleArrayMap();
        zzakv<zzog> r4 = zzym.m6055(jSONObject);
        zzakv<zzanh> r5 = zzym.m6056(jSONObject, "video");
        JSONArray jSONArray = jSONObject.getJSONArray("custom_assets");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            String string = jSONObject2.getString(VastExtensionXmlManager.TYPE);
            if ("string".equals(string)) {
                simpleArrayMap2.put(jSONObject2.getString("name"), jSONObject2.getString("string_value"));
            } else if ("image".equals(string)) {
                simpleArrayMap.put(jSONObject2.getString("name"), zzym.m6059(jSONObject2, "image_value", this.f5615));
            } else {
                String valueOf = String.valueOf(string);
                zzagf.m4791(valueOf.length() != 0 ? "Unknown custom asset type: ".concat(valueOf) : new String("Unknown custom asset type: "));
            }
        }
        zzanh r6 = zzym.m6050(r5);
        String string2 = jSONObject.getString("custom_template_id");
        SimpleArrayMap r2 = m6080(simpleArrayMap);
        zzog zzog = (zzog) r4.get();
        zzaoa r52 = r6 != null ? r6.m4997() : null;
        if (r6 == null) {
            view = null;
        } else if (r6 == null) {
            throw null;
        } else {
            view = (View) r6;
        }
        return new zzon(string2, r2, simpleArrayMap2, zzog, r52, view);
    }
}
