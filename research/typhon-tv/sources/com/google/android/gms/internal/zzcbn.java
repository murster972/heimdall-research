package com.google.android.gms.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class zzcbn implements zzf, zzg {

    /* renamed from: 连任  reason: contains not printable characters */
    private final HandlerThread f9020 = new HandlerThread("GassClient");

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f9021;

    /* renamed from: 麤  reason: contains not printable characters */
    private final LinkedBlockingQueue<zzaz> f9022;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f9023;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcbo f9024;

    public zzcbn(Context context, String str, String str2) {
        this.f9021 = str;
        this.f9023 = str2;
        this.f9020.start();
        this.f9024 = new zzcbo(context, this.f9020.getLooper(), this, this);
        this.f9022 = new LinkedBlockingQueue<>();
        this.f9024.m9167();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m10313() {
        if (this.f9024 == null) {
            return;
        }
        if (this.f9024.m9161() || this.f9024.m9162()) {
            this.f9024.m9160();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzaz m10314() {
        zzaz zzaz = new zzaz();
        zzaz.f8455 = Long.valueOf(PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID);
        return zzaz;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcbt m10315() {
        try {
            return this.f9024.m10320();
        } catch (DeadObjectException | IllegalStateException e) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzaz m10316(int i) {
        zzaz zzaz;
        try {
            zzaz = this.f9022.poll(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            zzaz = null;
        }
        return zzaz == null ? m10314() : zzaz;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10317(int i) {
        try {
            this.f9022.put(m10314());
        } catch (InterruptedException e) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0039, code lost:
        m10313();
        r4.f9020.quit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0038, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0038 A[ExcHandler:  FINALLY, Splitter:B:2:0x0006] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m10318(android.os.Bundle r5) {
        /*
            r4 = this;
            com.google.android.gms.internal.zzcbt r0 = r4.m10315()
            if (r0 == 0) goto L_0x0024
            com.google.android.gms.internal.zzcbp r1 = new com.google.android.gms.internal.zzcbp     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            java.lang.String r2 = r4.f9021     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            java.lang.String r3 = r4.f9023     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            r1.<init>(r2, r3)     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            com.google.android.gms.internal.zzcbr r0 = r0.m10325(r1)     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            com.google.android.gms.internal.zzaz r0 = r0.m10324()     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.zzaz> r1 = r4.f9022     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            r1.put(r0)     // Catch:{ Throwable -> 0x0025, all -> 0x0038 }
            r4.m10313()
            android.os.HandlerThread r0 = r4.f9020
            r0.quit()
        L_0x0024:
            return
        L_0x0025:
            r0 = move-exception
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.zzaz> r0 = r4.f9022     // Catch:{ InterruptedException -> 0x0042, all -> 0x0038 }
            com.google.android.gms.internal.zzaz r1 = m10314()     // Catch:{ InterruptedException -> 0x0042, all -> 0x0038 }
            r0.put(r1)     // Catch:{ InterruptedException -> 0x0042, all -> 0x0038 }
        L_0x002f:
            r4.m10313()
            android.os.HandlerThread r0 = r4.f9020
            r0.quit()
            goto L_0x0024
        L_0x0038:
            r0 = move-exception
            r4.m10313()
            android.os.HandlerThread r1 = r4.f9020
            r1.quit()
            throw r0
        L_0x0042:
            r0 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcbn.m10318(android.os.Bundle):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10319(ConnectionResult connectionResult) {
        try {
            this.f9022.put(m10314());
        } catch (InterruptedException e) {
        }
    }
}
