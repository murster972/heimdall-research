package com.google.android.gms.internal;

import com.google.ads.AdRequest$ErrorCode;
import com.google.ads.AdRequest$Gender;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.zzb;
import java.util.Date;
import java.util.HashSet;

@zzzv
public final class zzwj {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5985(AdRequest$ErrorCode adRequest$ErrorCode) {
        switch (zzwk.f10947[adRequest$ErrorCode.ordinal()]) {
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            default:
                return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static AdSize m5986(zzjn zzjn) {
        AdSize[] adSizeArr = {AdSize.SMART_BANNER, AdSize.BANNER, AdSize.IAB_MRECT, AdSize.IAB_BANNER, AdSize.IAB_LEADERBOARD, AdSize.IAB_WIDE_SKYSCRAPER};
        for (int i = 0; i < 6; i++) {
            if (adSizeArr[i].getWidth() == zzjn.f4794 && adSizeArr[i].getHeight() == zzjn.f4795) {
                return adSizeArr[i];
            }
        }
        return new AdSize(zzb.zza(zzjn.f4794, zzjn.f4795, zzjn.f4798));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediationAdRequest m5987(zzjj zzjj, boolean z) {
        AdRequest$Gender adRequest$Gender;
        HashSet hashSet = zzjj.f4764 != null ? new HashSet(zzjj.f4764) : null;
        Date date = new Date(zzjj.f4765);
        switch (zzjj.f4766) {
            case 1:
                adRequest$Gender = AdRequest$Gender.MALE;
                break;
            case 2:
                adRequest$Gender = AdRequest$Gender.FEMALE;
                break;
            default:
                adRequest$Gender = AdRequest$Gender.UNKNOWN;
                break;
        }
        return new MediationAdRequest(date, adRequest$Gender, hashSet, z, zzjj.f4763);
    }
}
