package com.google.android.gms.internal;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

final class zzdvn {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ReferenceQueue<Throwable> f10237 = new ReferenceQueue<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final ConcurrentHashMap<zzdvo, List<Throwable>> f10238 = new ConcurrentHashMap<>(16, 0.75f, 10);

    zzdvn() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<Throwable> m12243(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.f10237.poll();
        while (poll != null) {
            this.f10238.remove(poll);
            poll = this.f10237.poll();
        }
        return this.f10238.get(new zzdvo(th, (ReferenceQueue<Throwable>) null));
    }
}
