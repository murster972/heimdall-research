package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class zzbbt extends zzbfm {
    public static final Parcelable.Creator<zzbbt> CREATOR = new zzbbu();

    /* renamed from: 龘  reason: contains not printable characters */
    private String f8611;

    public zzbbt() {
        this((String) null);
    }

    zzbbt(String str) {
        this.f8611 = str;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbbt)) {
            return false;
        }
        return zzbcm.m10043(this.f8611, ((zzbbt) obj).f8611);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f8611});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f8611, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m9956() {
        return this.f8611;
    }
}
