package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbc extends zzfjm<zzbc> {

    /* renamed from: 连任  reason: contains not printable characters */
    private Long f8621 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private Integer f8622 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private int[] f8623 = zzfjv.f10563;

    /* renamed from: 齉  reason: contains not printable characters */
    private Boolean f8624 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Long f8625 = null;

    public zzbc() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9962() {
        int i;
        int i2 = 0;
        int r0 = super.m12841();
        if (this.f8625 != null) {
            r0 += zzfjk.m12814(1, this.f8625.longValue());
        }
        if (this.f8622 != null) {
            r0 += zzfjk.m12806(2, this.f8622.intValue());
        }
        if (this.f8624 != null) {
            this.f8624.booleanValue();
            r0 += zzfjk.m12805(3) + 1;
        }
        if (this.f8623 != null && this.f8623.length > 0) {
            int i3 = 0;
            while (true) {
                i = i2;
                if (i3 >= this.f8623.length) {
                    break;
                }
                i2 = zzfjk.m12816(this.f8623[i3]) + i;
                i3++;
            }
            r0 = r0 + i + (this.f8623.length * 1);
        }
        if (this.f8621 == null) {
            return r0;
        }
        return r0 + zzfjk.m12805(5) + zzfjk.m12817(this.f8621.longValue());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m9963(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f8625 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 16:
                    this.f8622 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 24:
                    this.f8624 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 32:
                    int r2 = zzfjv.m12883(zzfjj, 32);
                    int length = this.f8623 == null ? 0 : this.f8623.length;
                    int[] iArr = new int[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f8623, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = zzfjj.m12785();
                        zzfjj.m12800();
                        length++;
                    }
                    iArr[length] = zzfjj.m12785();
                    this.f8623 = iArr;
                    continue;
                case 34:
                    int r3 = zzfjj.m12799(zzfjj.m12785());
                    int r22 = zzfjj.m12787();
                    int i = 0;
                    while (zzfjj.m12790() > 0) {
                        zzfjj.m12785();
                        i++;
                    }
                    zzfjj.m12792(r22);
                    int length2 = this.f8623 == null ? 0 : this.f8623.length;
                    int[] iArr2 = new int[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f8623, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = zzfjj.m12785();
                        length2++;
                    }
                    this.f8623 = iArr2;
                    zzfjj.m12796(r3);
                    continue;
                case 40:
                    this.f8621 = Long.valueOf(zzfjj.m12786());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9964(zzfjk zzfjk) throws IOException {
        if (this.f8625 != null) {
            zzfjk.m12824(1, this.f8625.longValue());
        }
        if (this.f8622 != null) {
            zzfjk.m12832(2, this.f8622.intValue());
        }
        if (this.f8624 != null) {
            zzfjk.m12836(3, this.f8624.booleanValue());
        }
        if (this.f8623 != null && this.f8623.length > 0) {
            for (int r2 : this.f8623) {
                zzfjk.m12832(4, r2);
            }
        }
        if (this.f8621 != null) {
            zzfjk.m12833(5, this.f8621.longValue());
        }
        super.m12842(zzfjk);
    }
}
