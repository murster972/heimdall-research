package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdth;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class zzdqc<P> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Charset f9930 = Charset.forName("UTF-8");

    /* renamed from: 靐  reason: contains not printable characters */
    private ConcurrentMap<String, List<zzdqd<P>>> f9931 = new ConcurrentHashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private zzdqd<P> f9932;

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdqd<P> m11666() {
        return this.f9932;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdqd<P> m11667(P p, zzdth.zzb zzb) throws GeneralSecurityException {
        byte[] bArr;
        switch (zzdpt.f9914[zzb.m12075().ordinal()]) {
            case 1:
            case 2:
                bArr = ByteBuffer.allocate(5).put((byte) 0).putInt(zzb.m12076()).array();
                break;
            case 3:
                bArr = ByteBuffer.allocate(5).put((byte) 1).putInt(zzb.m12076()).array();
                break;
            case 4:
                bArr = zzdps.f9913;
                break;
            default:
                throw new GeneralSecurityException("unknown output prefix type");
        }
        zzdqd<P> zzdqd = new zzdqd<>(p, bArr, zzb.m12079(), zzb.m12075());
        ArrayList arrayList = new ArrayList();
        arrayList.add(zzdqd);
        String str = new String(zzdqd.m11669(), f9930);
        List list = (List) this.f9931.put(str, Collections.unmodifiableList(arrayList));
        if (list != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(list);
            arrayList2.add(zzdqd);
            this.f9931.put(str, Collections.unmodifiableList(arrayList2));
        }
        return zzdqd;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11668(zzdqd<P> zzdqd) {
        this.f9932 = zzdqd;
    }
}
