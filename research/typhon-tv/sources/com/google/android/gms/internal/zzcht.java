package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.util.List;
import java.util.Map;

final class zzcht implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<String, List<String>> f9282;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f9283;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f9284;

    /* renamed from: 麤  reason: contains not printable characters */
    private final byte[] f9285;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Throwable f9286;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzchs f9287;

    private zzcht(String str, zzchs zzchs, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        zzbq.m9120(zzchs);
        this.f9287 = zzchs;
        this.f9284 = i;
        this.f9286 = th;
        this.f9285 = bArr;
        this.f9283 = str;
        this.f9282 = map;
    }

    public final void run() {
        this.f9287.m10880(this.f9283, this.f9284, this.f9286, this.f9285, this.f9282);
    }
}
