package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;

final class zzfhh {
    /* renamed from: 龘  reason: contains not printable characters */
    static String m12630(zzfhe zzfhe, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("# ").append(str);
        m12632(zzfhe, sb, 0);
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String m12631(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12632(zzfhe zzfhe, StringBuilder sb, int i) {
        boolean booleanValue;
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        TreeSet<String> treeSet = new TreeSet<>();
        for (Method method : zzfhe.getClass().getDeclaredMethods()) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    treeSet.add(method.getName());
                }
            }
        }
        for (String replaceFirst : treeSet) {
            String replaceFirst2 = replaceFirst.replaceFirst("get", "");
            if (replaceFirst2.endsWith("List") && !replaceFirst2.endsWith("OrBuilderList")) {
                String valueOf = String.valueOf(replaceFirst2.substring(0, 1).toLowerCase());
                String valueOf2 = String.valueOf(replaceFirst2.substring(1, replaceFirst2.length() - 4));
                String concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                String valueOf3 = String.valueOf(replaceFirst2);
                Method method2 = (Method) hashMap.get(valueOf3.length() != 0 ? "get".concat(valueOf3) : new String("get"));
                if (method2 != null && method2.getReturnType().equals(List.class)) {
                    m12633(sb, i, m12631(concat), zzffu.m12531(method2, (Object) zzfhe, new Object[0]));
                }
            }
            String valueOf4 = String.valueOf(replaceFirst2);
            if (((Method) hashMap2.get(valueOf4.length() != 0 ? "set".concat(valueOf4) : new String("set"))) != null) {
                if (replaceFirst2.endsWith("Bytes")) {
                    String valueOf5 = String.valueOf(replaceFirst2.substring(0, replaceFirst2.length() - 5));
                    if (hashMap.containsKey(valueOf5.length() != 0 ? "get".concat(valueOf5) : new String("get"))) {
                    }
                }
                String valueOf6 = String.valueOf(replaceFirst2.substring(0, 1).toLowerCase());
                String valueOf7 = String.valueOf(replaceFirst2.substring(1));
                String concat2 = valueOf7.length() != 0 ? valueOf6.concat(valueOf7) : new String(valueOf6);
                String valueOf8 = String.valueOf(replaceFirst2);
                Method method3 = (Method) hashMap.get(valueOf8.length() != 0 ? "get".concat(valueOf8) : new String("get"));
                String valueOf9 = String.valueOf(replaceFirst2);
                Method method4 = (Method) hashMap.get(valueOf9.length() != 0 ? "has".concat(valueOf9) : new String("has"));
                if (method3 != null) {
                    Object r2 = zzffu.m12531(method3, (Object) zzfhe, new Object[0]);
                    if (method4 == null) {
                        booleanValue = !(r2 instanceof Boolean ? !((Boolean) r2).booleanValue() : r2 instanceof Integer ? ((Integer) r2).intValue() == 0 : r2 instanceof Float ? (((Float) r2).floatValue() > 0.0f ? 1 : (((Float) r2).floatValue() == 0.0f ? 0 : -1)) == 0 : r2 instanceof Double ? (((Double) r2).doubleValue() > 0.0d ? 1 : (((Double) r2).doubleValue() == 0.0d ? 0 : -1)) == 0 : r2 instanceof String ? r2.equals("") : r2 instanceof zzfes ? r2.equals(zzfes.zzpfg) : r2 instanceof zzfhe ? r2 == ((zzfhe) r2).m12629() : r2 instanceof Enum ? ((Enum) r2).ordinal() == 0 : false);
                    } else {
                        booleanValue = ((Boolean) zzffu.m12531(method4, (Object) zzfhe, new Object[0])).booleanValue();
                    }
                    if (booleanValue) {
                        m12633(sb, i, m12631(concat2), r2);
                    }
                }
            }
        }
        if (zzfhe instanceof zzffu.zzd) {
            Iterator<Map.Entry<Object, Object>> r0 = ((zzffu.zzd) zzfhe).f10402.m12515();
            if (r0.hasNext()) {
                r0.next().getKey();
                throw new NoSuchMethodError();
            }
        }
        if (((zzffu) zzfhe).f10394 != null) {
            ((zzffu) zzfhe).f10394.m12721(sb, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final void m12633(StringBuilder sb, int i, String str, Object obj) {
        if (obj instanceof List) {
            for (Object r1 : (List) obj) {
                m12633(sb, i, str, r1);
            }
            return;
        }
        sb.append(10);
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(' ');
        }
        sb.append(str);
        if (obj instanceof String) {
            sb.append(": \"").append(zzfih.m12703(zzfes.zztr((String) obj))).append('\"');
        } else if (obj instanceof zzfes) {
            sb.append(": \"").append(zzfih.m12703((zzfes) obj)).append('\"');
        } else if (obj instanceof zzffu) {
            sb.append(" {");
            m12632((zzffu) obj, sb, i + 2);
            sb.append(StringUtils.LF);
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append("}");
        } else {
            sb.append(": ").append(obj.toString());
        }
    }
}
