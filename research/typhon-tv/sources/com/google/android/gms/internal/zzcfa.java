package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationSettingsRequest;

public final class zzcfa extends zzeu implements zzcez {
    zzcfa(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final LocationAvailability m10364(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r1 = m12300(34, v_);
        LocationAvailability r0 = zzew.m12304(r1, LocationAvailability.CREATOR);
        r1.recycle();
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Location m10365(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r1 = m12300(21, v_);
        Location location = (Location) zzew.m12304(r1, Location.CREATOR);
        r1.recycle();
        return location;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10366(long j, boolean z, PendingIntent pendingIntent) throws RemoteException {
        Parcel v_ = v_();
        v_.writeLong(j);
        zzew.m12307(v_, true);
        zzew.m12306(v_, (Parcelable) pendingIntent);
        m12298(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10367(PendingIntent pendingIntent) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) pendingIntent);
        m12298(6, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10368(Location location) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) location);
        m12298(13, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10369(zzcdz zzcdz) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcdz);
        m12298(75, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10370(zzceu zzceu) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzceu);
        m12298(67, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10371(zzcfq zzcfq) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcfq);
        m12298(59, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10372(LocationSettingsRequest locationSettingsRequest, zzcfb zzcfb, String str) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) locationSettingsRequest);
        zzew.m12305(v_, (IInterface) zzcfb);
        v_.writeString(str);
        m12298(63, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10373(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(12, v_);
    }
}
