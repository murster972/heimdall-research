package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzahc extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8195;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8196;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzahc(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8196 = context;
        this.f8195 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9601() {
        SharedPreferences sharedPreferences = this.f8196.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putBoolean("content_url_opted_out", sharedPreferences.getBoolean("content_url_opted_out", true));
        if (this.f8195 != null) {
            this.f8195.m9605(bundle);
        }
    }
}
