package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public abstract class zzet implements Callable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f10280;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f10281;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f10282;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f10283;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final zzaz f10284;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f10285 = getClass().getSimpleName();

    /* renamed from: 齉  reason: contains not printable characters */
    protected Method f10286;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzdm f10287;

    public zzet(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        this.f10287 = zzdm;
        this.f10283 = str;
        this.f10280 = str2;
        this.f10284 = zzaz;
        this.f10281 = i;
        this.f10282 = i2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Void call() throws Exception {
        try {
            long nanoTime = System.nanoTime();
            this.f10286 = this.f10287.m11624(this.f10283, this.f10280);
            if (this.f10286 != null) {
                m12297();
                zzcp r2 = this.f10287.m11611();
                if (!(r2 == null || this.f10281 == Integer.MIN_VALUE)) {
                    r2.m11517(this.f10282, this.f10281, (System.nanoTime() - nanoTime) / 1000);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m12297() throws IllegalAccessException, InvocationTargetException;
}
