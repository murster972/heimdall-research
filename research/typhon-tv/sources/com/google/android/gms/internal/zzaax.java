package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Collections;
import java.util.List;

@zzzv
public final class zzaax extends zzbfm {
    public static final Parcelable.Creator<zzaax> CREATOR = new zzaay();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final long f3820;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final List<String> f3821;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean f3822;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final String f3823;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final long f3824;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final boolean f3825;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final long f3826;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public final String f3827;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final String f3828;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public final zzaey f3829;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final boolean f3830;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public final boolean f3831;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String f3832;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final int f3833;

    /* renamed from: ˉ  reason: contains not printable characters */
    public final boolean f3834;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public final boolean f3835;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String f3836;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private int f3837;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final boolean f3838;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private zzaat f3839;

    /* renamed from: ˎ  reason: contains not printable characters */
    public final boolean f3840;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private Bundle f3841;

    /* renamed from: ˏ  reason: contains not printable characters */
    public final boolean f3842;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private zzabj f3843;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final List<String> f3844;

    /* renamed from: י  reason: contains not printable characters */
    public String f3845;

    /* renamed from: ـ  reason: contains not printable characters */
    public final String f3846;

    /* renamed from: ــ  reason: contains not printable characters */
    public final boolean f3847;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final long f3848;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int f3849;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final boolean f3850;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final boolean f3851;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public String f3852;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final boolean f3853;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final zzaeq f3854;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final List<String> f3855;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final List<String> f3856;

    /* renamed from: 连任  reason: contains not printable characters */
    public final List<String> f3857;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f3858;

    /* renamed from: 麤  reason: contains not printable characters */
    public final int f3859;

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<String> f3860;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f3861;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public final boolean f3862;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final boolean f3863;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final String f3864;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final zzaaz f3865;

    public zzaax(int i) {
        this(19, (String) null, (String) null, (List<String>) null, i, (List<String>) null, -1, false, -1, (List<String>) null, -1, -1, (String) null, -1, (String) null, false, (String) null, (String) null, false, false, false, true, false, (zzabj) null, (String) null, (String) null, false, false, (zzaeq) null, (List<String>) null, (List<String>) null, false, (zzaaz) null, false, (String) null, (List<String>) null, false, (String) null, (zzaey) null, (String) null, true, false, (Bundle) null, false, 0);
    }

    public zzaax(int i, long j) {
        this(19, (String) null, (String) null, (List<String>) null, i, (List<String>) null, -1, false, -1, (List<String>) null, j, -1, (String) null, -1, (String) null, false, (String) null, (String) null, false, false, false, true, false, (zzabj) null, (String) null, (String) null, false, false, (zzaeq) null, (List<String>) null, (List<String>) null, false, (zzaaz) null, false, (String) null, (List<String>) null, false, (String) null, (zzaey) null, (String) null, true, false, (Bundle) null, false, 0);
    }

    zzaax(int i, String str, String str2, List<String> list, int i2, List<String> list2, long j, boolean z, long j2, List<String> list3, long j3, int i3, String str3, long j4, String str4, boolean z2, String str5, String str6, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, zzabj zzabj, String str7, String str8, boolean z8, boolean z9, zzaeq zzaeq, List<String> list4, List<String> list5, boolean z10, zzaaz zzaaz, boolean z11, String str9, List<String> list6, boolean z12, String str10, zzaey zzaey, String str11, boolean z13, boolean z14, Bundle bundle, boolean z15, int i4) {
        zzabx zzabx;
        this.f3837 = i;
        this.f3861 = str;
        this.f3858 = str2;
        this.f3860 = list != null ? Collections.unmodifiableList(list) : null;
        this.f3859 = i2;
        this.f3857 = list2 != null ? Collections.unmodifiableList(list2) : null;
        this.f3820 = j;
        this.f3822 = z;
        this.f3824 = j2;
        this.f3844 = list3 != null ? Collections.unmodifiableList(list3) : null;
        this.f3848 = j3;
        this.f3849 = i3;
        this.f3832 = str3;
        this.f3826 = j4;
        this.f3828 = str4;
        this.f3863 = z2;
        this.f3864 = str5;
        this.f3836 = str6;
        this.f3838 = z3;
        this.f3840 = z4;
        this.f3830 = z5;
        this.f3834 = z6;
        this.f3847 = z13;
        this.f3842 = z7;
        this.f3843 = zzabj;
        this.f3845 = str7;
        this.f3846 = str8;
        if (this.f3858 == null && this.f3843 != null && (zzabx = (zzabx) this.f3843.m4264(zzabx.CREATOR)) != null && !TextUtils.isEmpty(zzabx.f3893)) {
            this.f3858 = zzabx.f3893;
        }
        this.f3851 = z8;
        this.f3853 = z9;
        this.f3854 = zzaeq;
        this.f3855 = list4;
        this.f3856 = list5;
        this.f3862 = z10;
        this.f3865 = zzaaz;
        this.f3850 = z11;
        this.f3852 = str9;
        this.f3821 = list6;
        this.f3825 = z12;
        this.f3823 = str10;
        this.f3829 = zzaey;
        this.f3827 = str11;
        this.f3831 = z14;
        this.f3841 = bundle;
        this.f3835 = z15;
        this.f3833 = i4;
    }

    public zzaax(zzaat zzaat, String str, String str2, List<String> list, List<String> list2, long j, boolean z, long j2, List<String> list3, long j3, int i, String str3, long j4, String str4, String str5, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, String str6, boolean z7, boolean z8, zzaeq zzaeq, List<String> list4, List<String> list5, boolean z9, zzaaz zzaaz, boolean z10, String str7, List<String> list6, boolean z11, String str8, zzaey zzaey, String str9, boolean z12, boolean z13, boolean z14, int i2) {
        this(19, str, str2, list, -2, list2, j, z, -1, list3, j3, i, str3, j4, str4, false, (String) null, str5, z2, z3, z4, z5, false, (zzabj) null, (String) null, str6, z7, z8, zzaeq, list4, list5, z9, zzaaz, z10, str7, list6, z11, str8, zzaey, str9, z12, z13, (Bundle) null, z14, i2);
        this.f3839 = zzaat;
    }

    public zzaax(zzaat zzaat, String str, String str2, List<String> list, List<String> list2, long j, boolean z, long j2, List<String> list3, long j3, int i, String str3, long j4, String str4, boolean z2, String str5, String str6, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str7, boolean z8, boolean z9, zzaeq zzaeq, List<String> list4, List<String> list5, boolean z10, zzaaz zzaaz, boolean z11, String str8, List<String> list6, boolean z12, String str9, zzaey zzaey, String str10, boolean z13, boolean z14, boolean z15, int i2) {
        this(19, str, str2, list, -2, list2, j, z, j2, list3, j3, i, str3, j4, str4, z2, str5, str6, z3, z4, z5, z6, z7, (zzabj) null, (String) null, str7, z8, z9, zzaeq, list4, list5, z10, zzaaz, z11, str8, list6, z12, str9, zzaey, str10, z13, z14, (Bundle) null, z15, 0);
        this.f3839 = zzaat;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        if (this.f3839 != null && this.f3839.f3764 >= 9 && !TextUtils.isEmpty(this.f3858)) {
            this.f3843 = new zzabj((zzbfq) new zzabx(this.f3858));
            this.f3858 = null;
        }
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f3837);
        zzbfp.m10193(parcel, 2, this.f3861, false);
        zzbfp.m10193(parcel, 3, this.f3858, false);
        zzbfp.m10178(parcel, 4, this.f3860, false);
        zzbfp.m10185(parcel, 5, this.f3859);
        zzbfp.m10178(parcel, 6, this.f3857, false);
        zzbfp.m10186(parcel, 7, this.f3820);
        zzbfp.m10195(parcel, 8, this.f3822);
        zzbfp.m10186(parcel, 9, this.f3824);
        zzbfp.m10178(parcel, 10, this.f3844, false);
        zzbfp.m10186(parcel, 11, this.f3848);
        zzbfp.m10185(parcel, 12, this.f3849);
        zzbfp.m10193(parcel, 13, this.f3832, false);
        zzbfp.m10186(parcel, 14, this.f3826);
        zzbfp.m10193(parcel, 15, this.f3828, false);
        zzbfp.m10195(parcel, 18, this.f3863);
        zzbfp.m10193(parcel, 19, this.f3864, false);
        zzbfp.m10193(parcel, 21, this.f3836, false);
        zzbfp.m10195(parcel, 22, this.f3838);
        zzbfp.m10195(parcel, 23, this.f3840);
        zzbfp.m10195(parcel, 24, this.f3830);
        zzbfp.m10195(parcel, 25, this.f3834);
        zzbfp.m10195(parcel, 26, this.f3842);
        zzbfp.m10189(parcel, 28, (Parcelable) this.f3843, i, false);
        zzbfp.m10193(parcel, 29, this.f3845, false);
        zzbfp.m10193(parcel, 30, this.f3846, false);
        zzbfp.m10195(parcel, 31, this.f3851);
        zzbfp.m10195(parcel, 32, this.f3853);
        zzbfp.m10189(parcel, 33, (Parcelable) this.f3854, i, false);
        zzbfp.m10178(parcel, 34, this.f3855, false);
        zzbfp.m10178(parcel, 35, this.f3856, false);
        zzbfp.m10195(parcel, 36, this.f3862);
        zzbfp.m10189(parcel, 37, (Parcelable) this.f3865, i, false);
        zzbfp.m10195(parcel, 38, this.f3850);
        zzbfp.m10193(parcel, 39, this.f3852, false);
        zzbfp.m10178(parcel, 40, this.f3821, false);
        zzbfp.m10195(parcel, 42, this.f3825);
        zzbfp.m10193(parcel, 43, this.f3823, false);
        zzbfp.m10189(parcel, 44, (Parcelable) this.f3829, i, false);
        zzbfp.m10193(parcel, 45, this.f3827, false);
        zzbfp.m10195(parcel, 46, this.f3847);
        zzbfp.m10195(parcel, 47, this.f3831);
        zzbfp.m10187(parcel, 48, this.f3841, false);
        zzbfp.m10195(parcel, 49, this.f3835);
        zzbfp.m10185(parcel, 50, this.f3833);
        zzbfp.m10182(parcel, r0);
    }
}
