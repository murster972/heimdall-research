package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;

final class zzdqj implements zzdpw<zzdpp> {
    zzdqj() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdpp m11702(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdrm r1 = zzdrm.m11830(zzfes);
            if (!(r1 instanceof zzdrm)) {
                throw new GeneralSecurityException("expected AesEaxKey proto");
            }
            zzdrm zzdrm = r1;
            zzdvk.m12238(zzdrm.m11839(), 0);
            zzdvk.m12237(zzdrm.m11838().size());
            if (zzdrm.m11836().m11855() == 12 || zzdrm.m11836().m11855() == 16) {
                return new zzdty(zzdrm.m11838().toByteArray(), zzdrm.m11836().m11855());
            }
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesEaxKey proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11699(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11700((zzfhe) zzdro.m11846(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesEaxKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11700(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdro)) {
            throw new GeneralSecurityException("expected AesEaxKeyFormat proto");
        }
        zzdro zzdro = (zzdro) zzfhe;
        zzdvk.m12237(zzdro.m11847());
        if (zzdro.m11849().m11855() == 12 || zzdro.m11849().m11855() == 16) {
            return zzdrm.m11828().m11844(zzfes.zzaz(zzdvi.m12235(zzdro.m11847()))).m11843(zzdro.m11849()).m11842(0).m12542();
        }
        throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11701(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.AesEaxKey").m12022(((zzdrm) m11699(zzfes)).m12361()).m12021(zzdsy.zzb.SYMMETRIC).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11703(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdrm)) {
            throw new GeneralSecurityException("expected AesEaxKey proto");
        }
        zzdrm zzdrm = (zzdrm) zzfhe;
        zzdvk.m12238(zzdrm.m11839(), 0);
        zzdvk.m12237(zzdrm.m11838().size());
        if (zzdrm.m11836().m11855() == 12 || zzdrm.m11836().m11855() == 16) {
            return new zzdty(zzdrm.m11838().toByteArray(), zzdrm.m11836().m11855());
        }
        throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
    }
}
