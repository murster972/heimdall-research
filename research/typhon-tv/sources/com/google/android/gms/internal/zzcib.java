package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.util.Pair;
import com.google.android.gms.common.internal.zzbq;

public final class zzcib {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzchx f9333;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f9334;

    /* renamed from: 麤  reason: contains not printable characters */
    private final long f9335;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f9336;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f9337;

    private zzcib(zzchx zzchx, String str, long j) {
        this.f9333 = zzchx;
        zzbq.m9122(str);
        zzbq.m9116(j > 0);
        this.f9337 = String.valueOf(str).concat(":start");
        this.f9334 = String.valueOf(str).concat(":count");
        this.f9336 = String.valueOf(str).concat(":value");
        this.f9335 = j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m10904() {
        this.f9333.m11109();
        long r0 = this.f9333.m11105().m9243();
        SharedPreferences.Editor edit = this.f9333.m10885().edit();
        edit.remove(this.f9334);
        edit.remove(this.f9336);
        edit.putLong(this.f9337, r0);
        edit.apply();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final long m10905() {
        return this.f9333.m10885().getLong(this.f9337, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Pair<String, Long> m10906() {
        long abs;
        this.f9333.m11109();
        this.f9333.m11109();
        long r0 = m10905();
        if (r0 == 0) {
            m10904();
            abs = 0;
        } else {
            abs = Math.abs(r0 - this.f9333.m11105().m9243());
        }
        if (abs < this.f9335) {
            return null;
        }
        if (abs > (this.f9335 << 1)) {
            m10904();
            return null;
        }
        String string = this.f9333.m10885().getString(this.f9336, (String) null);
        long j = this.f9333.m10885().getLong(this.f9334, 0);
        m10904();
        return (string == null || j <= 0) ? zzchx.f9300 : new Pair<>(string, Long.valueOf(j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10907(String str, long j) {
        this.f9333.m11109();
        if (m10905() == 0) {
            m10904();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.f9333.m10885().getLong(this.f9334, 0);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = this.f9333.m10885().edit();
            edit.putString(this.f9336, str);
            edit.putLong(this.f9334, 1);
            edit.apply();
            return;
        }
        boolean z = (this.f9333.m11112().m11419().nextLong() & Long.MAX_VALUE) < Long.MAX_VALUE / (j2 + 1);
        SharedPreferences.Editor edit2 = this.f9333.m10885().edit();
        if (z) {
            edit2.putString(this.f9336, str);
        }
        edit2.putLong(this.f9334, j2 + 1);
        edit2.apply();
    }
}
