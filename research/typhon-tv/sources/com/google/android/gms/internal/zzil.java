package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.zzbs;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzil extends zzbfm {
    public static final Parcelable.Creator<zzil> CREATOR = new zzim();

    /* renamed from: ʻ  reason: contains not printable characters */
    private Bundle f4738;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4739;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f4740;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f4741;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f4742;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f4743;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f4744;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f4745;

    zzil(String str, long j, String str2, String str3, String str4, Bundle bundle, boolean z, long j2) {
        this.f4745 = str;
        this.f4742 = j;
        this.f4744 = str2 == null ? "" : str2;
        this.f4743 = str3 == null ? "" : str3;
        this.f4741 = str4 == null ? "" : str4;
        this.f4738 = bundle == null ? new Bundle() : bundle;
        this.f4739 = z;
        this.f4740 = j2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzil m5427(Uri uri) {
        long j = 0;
        try {
            if (!"gcache".equals(uri.getScheme())) {
                return null;
            }
            List<String> pathSegments = uri.getPathSegments();
            if (pathSegments.size() != 2) {
                zzagf.m4791(new StringBuilder(62).append("Expected 2 path parts for namespace and id, found :").append(pathSegments.size()).toString());
                return null;
            }
            String str = pathSegments.get(0);
            String str2 = pathSegments.get(1);
            String host = uri.getHost();
            String queryParameter = uri.getQueryParameter("url");
            boolean equals = PubnativeRequest.LEGACY_ZONE_ID.equals(uri.getQueryParameter("read_only"));
            String queryParameter2 = uri.getQueryParameter("expiration");
            if (queryParameter2 != null) {
                j = Long.parseLong(queryParameter2);
            }
            Bundle bundle = new Bundle();
            for (String next : zzbs.zzek().m4657(uri)) {
                if (next.startsWith("tag.")) {
                    bundle.putString(next.substring(4), uri.getQueryParameter(next));
                }
            }
            return new zzil(queryParameter, j, host, str, str2, bundle, equals, 0);
        } catch (NullPointerException | NumberFormatException e) {
            zzagf.m4796("Unable to parse Uri into cache offering.", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzil m5428(String str) {
        return m5427(Uri.parse(str));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f4745, false);
        zzbfp.m10186(parcel, 3, this.f4742);
        zzbfp.m10193(parcel, 4, this.f4744, false);
        zzbfp.m10193(parcel, 5, this.f4743, false);
        zzbfp.m10193(parcel, 6, this.f4741, false);
        zzbfp.m10187(parcel, 7, this.f4738, false);
        zzbfp.m10195(parcel, 8, this.f4739);
        zzbfp.m10186(parcel, 9, this.f4740);
        zzbfp.m10182(parcel, r0);
    }
}
