package com.google.android.gms.internal;

import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;

final class zzyz implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzyt f11004;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ WeakReference f11005;

    zzyz(zzyt zzyt, WeakReference weakReference) {
        this.f11004 = zzyt;
        this.f11005 = weakReference;
    }

    public final void onGlobalLayout() {
        this.f11004.m6069((WeakReference<zzanh>) this.f11005, false);
    }
}
