package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.common.internal.zzbg;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;

@zzzv
public final class zzaeq extends zzbfm {
    public static final Parcelable.Creator<zzaeq> CREATOR = new zzaer();

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f4055;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f4056;

    public zzaeq(RewardItem rewardItem) {
        this(rewardItem.getType(), rewardItem.getAmount());
    }

    public zzaeq(String str, int i) {
        this.f4056 = str;
        this.f4055 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzaeq m4387(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return m4388(new JSONArray(str));
        } catch (JSONException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzaeq m4388(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        return new zzaeq(jSONArray.getJSONObject(0).optString("rb_type"), jSONArray.getJSONObject(0).optInt("rb_amount"));
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof zzaeq)) {
            return false;
        }
        zzaeq zzaeq = (zzaeq) obj;
        return zzbg.m9113(this.f4056, zzaeq.f4056) && zzbg.m9113(Integer.valueOf(this.f4055), Integer.valueOf(zzaeq.f4055));
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f4056, Integer.valueOf(this.f4055)});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f4056, false);
        zzbfp.m10185(parcel, 3, this.f4055);
        zzbfp.m10182(parcel, r0);
    }
}
