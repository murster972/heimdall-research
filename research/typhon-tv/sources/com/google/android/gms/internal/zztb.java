package com.google.android.gms.internal;

import android.os.RemoteException;

final class zztb implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zztd f10884;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zztc f10885;

    zztb(zzse zzse, zztc zztc, zztd zztd) {
        this.f10885 = zztc;
        this.f10884 = zztd;
    }

    public final void run() {
        try {
            this.f10885.m13423(this.f10884);
        } catch (RemoteException e) {
            zzagf.m4796("Could not propagate interstitial ad event.", e);
        }
    }
}
