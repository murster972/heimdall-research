package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbe extends zzfjm<zzbe> {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f8723 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f8724 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f8725 = null;

    public zzbe() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10139() {
        int r0 = super.m12841();
        if (this.f8725 != null) {
            r0 += zzfjk.m12814(1, this.f8725.longValue());
        }
        if (this.f8723 != null) {
            r0 += zzfjk.m12808(3, this.f8723);
        }
        return this.f8724 != null ? r0 + zzfjk.m12809(4, this.f8724) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m10140(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f8725 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 26:
                    this.f8723 = zzfjj.m12791();
                    continue;
                case 34:
                    this.f8724 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10141(zzfjk zzfjk) throws IOException {
        if (this.f8725 != null) {
            zzfjk.m12824(1, this.f8725.longValue());
        }
        if (this.f8723 != null) {
            zzfjk.m12835(3, this.f8723);
        }
        if (this.f8724 != null) {
            zzfjk.m12837(4, this.f8724);
        }
        super.m12842(zzfjk);
    }
}
