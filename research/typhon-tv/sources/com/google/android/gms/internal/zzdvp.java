package com.google.android.gms.internal;

import java.io.PrintWriter;
import java.util.List;

final class zzdvp extends zzdvm {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzdvn f10240 = new zzdvn();

    zzdvp() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12244(Throwable th, PrintWriter printWriter) {
        th.printStackTrace(printWriter);
        List<Throwable> r1 = this.f10240.m12243(th, false);
        if (r1 != null) {
            synchronized (r1) {
                for (Throwable printStackTrace : r1) {
                    printWriter.print("Suppressed: ");
                    printStackTrace.printStackTrace(printWriter);
                }
            }
        }
    }
}
