package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzmh implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzmg f10800;

    zzmh(zzmg zzmg) {
        this.f10800 = zzmg;
    }

    public final void run() {
        if (this.f10800.f10799 != null) {
            try {
                this.f10800.f10799.m13061(1);
            } catch (RemoteException e) {
                zzakb.m4796("Could not notify onAdFailedToLoad event.", e);
            }
        }
    }
}
