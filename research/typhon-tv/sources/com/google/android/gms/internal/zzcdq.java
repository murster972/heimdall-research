package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

final class zzcdq extends zzcds {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ PendingIntent f9032;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ long f9033;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcdq(zzcdp zzcdp, GoogleApiClient googleApiClient, long j, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.f9033 = j;
        this.f9032 = pendingIntent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10329(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10405(this.f9033, this.f9032);
        m4198(Status.f7464);
    }
}
