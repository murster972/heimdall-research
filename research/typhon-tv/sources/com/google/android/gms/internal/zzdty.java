package com.google.android.gms.internal;

import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzdty implements zzdpp {

    /* renamed from: 靐  reason: contains not printable characters */
    private final byte[] f10173;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f10174;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SecretKeySpec f10175;

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f10176;

    public zzdty(byte[] bArr, int i) throws GeneralSecurityException {
        if (i == 12 || i == 16) {
            this.f10174 = i;
            this.f10175 = new SecretKeySpec(bArr, "AES");
            Cipher instance = Cipher.getInstance("AES/ECB/NOPADDING");
            instance.init(1, this.f10175);
            this.f10176 = m12170(instance.doFinal(new byte[16]));
            this.f10173 = m12170(this.f10176);
            return;
        }
        throw new IllegalArgumentException("IV size should be either 12 or 16 bytes");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static byte[] m12168(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        byte[] bArr3 = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr3[i] = (byte) (bArr[i] ^ bArr2[i]);
        }
        return bArr3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] m12169(Cipher cipher, int i, byte[] bArr, int i2, int i3) throws IllegalBlockSizeException, BadPaddingException {
        byte[] bArr2;
        byte[] bArr3 = new byte[16];
        bArr3[15] = (byte) i;
        if (i3 == 0) {
            return cipher.doFinal(m12168(bArr3, this.f10176));
        }
        int i4 = 0;
        byte[] doFinal = cipher.doFinal(bArr3);
        while (i3 - i4 > 16) {
            for (int i5 = 0; i5 < 16; i5++) {
                doFinal[i5] = (byte) (doFinal[i5] ^ bArr[(i2 + i4) + i5]);
            }
            doFinal = cipher.doFinal(doFinal);
            i4 += 16;
        }
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i2 + i4, i2 + i3);
        if (copyOfRange.length == 16) {
            bArr2 = m12168(copyOfRange, this.f10176);
        } else {
            byte[] copyOf = Arrays.copyOf(this.f10173, 16);
            for (int i6 = 0; i6 < copyOfRange.length; i6++) {
                copyOf[i6] = (byte) (copyOf[i6] ^ copyOfRange[i6]);
            }
            copyOf[copyOfRange.length] = (byte) (copyOf[copyOfRange.length] ^ 128);
            bArr2 = copyOf;
        }
        return cipher.doFinal(m12168(doFinal, bArr2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m12170(byte[] bArr) {
        int i = 0;
        byte[] bArr2 = new byte[16];
        for (int i2 = 0; i2 < 15; i2++) {
            bArr2[i2] = (byte) ((bArr[i2] << 1) ^ ((bArr[i2 + 1] & 255) >>> 7));
        }
        int i3 = bArr[15] << 1;
        if ((bArr[0] & 128) != 0) {
            i = TsExtractor.TS_STREAM_TYPE_E_AC3;
        }
        bArr2[15] = (byte) (i ^ i3);
        return bArr2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12171(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        if (bArr.length > (MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - this.f10174) - 16) {
            throw new GeneralSecurityException("plaintext too long");
        }
        byte[] bArr3 = new byte[(this.f10174 + bArr.length + 16)];
        byte[] r3 = zzdvi.m12235(this.f10174);
        System.arraycopy(r3, 0, bArr3, 0, this.f10174);
        Cipher instance = Cipher.getInstance("AES/ECB/NOPADDING");
        instance.init(1, this.f10175);
        byte[] r0 = m12169(instance, 0, r3, 0, r3.length);
        byte[] r10 = m12169(instance, 1, bArr2, 0, bArr2.length);
        Cipher instance2 = Cipher.getInstance("AES/CTR/NOPADDING");
        instance2.init(1, this.f10175, new IvParameterSpec(r0));
        instance2.doFinal(bArr, 0, bArr.length, bArr3, this.f10174);
        byte[] r1 = m12169(instance, 2, bArr3, this.f10174, bArr.length);
        int length = bArr.length + this.f10174;
        for (int i = 0; i < 16; i++) {
            bArr3[length + i] = (byte) ((r10[i] ^ r0[i]) ^ r1[i]);
        }
        return bArr3;
    }
}
