package com.google.android.gms.internal;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.List;

@zzzv
public final class zzoj extends zzqf implements zzov {

    /* renamed from: ʻ  reason: contains not printable characters */
    private double f5220;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f5221;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f5222;

    /* renamed from: ʾ  reason: contains not printable characters */
    private IObjectWrapper f5223;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f5224;

    /* renamed from: ˈ  reason: contains not printable characters */
    private View f5225;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzog f5226;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Bundle f5227;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzll f5228;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f5229;

    /* renamed from: 靐  reason: contains not printable characters */
    private List<zzoi> f5230;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzpq f5231;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f5232;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f5233;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Object f5234 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public zzos f5235;

    public zzoj(String str, List<zzoi> list, String str2, zzpq zzpq, String str3, double d, String str4, String str5, zzog zzog, Bundle bundle, zzll zzll, View view, IObjectWrapper iObjectWrapper, String str6) {
        this.f5233 = str;
        this.f5230 = list;
        this.f5232 = str2;
        this.f5231 = zzpq;
        this.f5229 = str3;
        this.f5220 = d;
        this.f5221 = str4;
        this.f5222 = str5;
        this.f5226 = zzog;
        this.f5227 = bundle;
        this.f5228 = zzll;
        this.f5225 = view;
        this.f5223 = iObjectWrapper;
        this.f5224 = str6;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final double m5655() {
        return this.f5220;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5656() {
        return this.f5221;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m5657() {
        return this.f5222;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzog m5658() {
        return this.f5226;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Bundle m5659() {
        return this.f5227;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m5660() {
        return "";
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m5661() {
        return this.f5224;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzpm m5662() {
        return this.f5226;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m5663() {
        zzahn.f4212.post(new zzok(this));
        this.f5233 = null;
        this.f5230 = null;
        this.f5232 = null;
        this.f5231 = null;
        this.f5229 = null;
        this.f5220 = 0.0d;
        this.f5221 = null;
        this.f5222 = null;
        this.f5226 = null;
        this.f5227 = null;
        this.f5234 = null;
        this.f5228 = null;
        this.f5225 = null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzll m5664() {
        return this.f5228;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final IObjectWrapper m5665() {
        return zzn.m9306(this.f5235);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m5666() {
        return "2";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m5667() {
        return this.f5229;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m5668() {
        return this.f5230;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5669(Bundle bundle) {
        boolean r0;
        synchronized (this.f5234) {
            if (this.f5235 == null) {
                zzagf.m4795("Attempt to record impression before app install ad initialized.");
                r0 = false;
            } else {
                r0 = this.f5235.m13202(bundle);
            }
        }
        return r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzpq m5670() {
        return this.f5231;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m5671() {
        return this.f5232;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5672(Bundle bundle) {
        synchronized (this.f5234) {
            if (this.f5235 == null) {
                zzagf.m4795("Attempt to perform click before app install ad initialized.");
            } else {
                this.f5235.m13192(bundle);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5673() {
        return this.f5233;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5674(Bundle bundle) {
        synchronized (this.f5234) {
            if (this.f5235 == null) {
                zzagf.m4795("Attempt to perform click before app install ad initialized.");
            } else {
                this.f5235.m13188(bundle);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5675(zzos zzos) {
        synchronized (this.f5234) {
            this.f5235 = zzos;
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final View m5676() {
        return this.f5225;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final IObjectWrapper m5677() {
        return this.f5223;
    }
}
