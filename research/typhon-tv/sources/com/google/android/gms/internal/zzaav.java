package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzaav implements Parcelable.Creator<zzaat> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r59 = zzbfn.m10169(parcel);
        int i = 0;
        Bundle bundle = null;
        zzjj zzjj = null;
        zzjn zzjn = null;
        String str = null;
        ApplicationInfo applicationInfo = null;
        PackageInfo packageInfo = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        zzakd zzakd = null;
        Bundle bundle2 = null;
        int i2 = 0;
        ArrayList<String> arrayList = null;
        Bundle bundle3 = null;
        boolean z = false;
        int i3 = 0;
        int i4 = 0;
        float f = 0.0f;
        String str5 = null;
        long j = 0;
        String str6 = null;
        ArrayList<String> arrayList2 = null;
        String str7 = null;
        zzpe zzpe = null;
        ArrayList<String> arrayList3 = null;
        long j2 = 0;
        String str8 = null;
        float f2 = 0.0f;
        boolean z2 = false;
        int i5 = 0;
        int i6 = 0;
        boolean z3 = false;
        boolean z4 = false;
        String str9 = null;
        String str10 = null;
        boolean z5 = false;
        int i7 = 0;
        Bundle bundle4 = null;
        String str11 = null;
        zzlr zzlr = null;
        boolean z6 = false;
        Bundle bundle5 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        boolean z7 = false;
        ArrayList<Integer> arrayList4 = null;
        String str15 = null;
        ArrayList<String> arrayList5 = null;
        int i8 = 0;
        boolean z8 = false;
        boolean z9 = false;
        boolean z10 = false;
        while (parcel.dataPosition() < r59) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 3:
                    zzjj = (zzjj) zzbfn.m10171(parcel, readInt, zzjj.CREATOR);
                    break;
                case 4:
                    zzjn = (zzjn) zzbfn.m10171(parcel, readInt, zzjn.CREATOR);
                    break;
                case 5:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    applicationInfo = (ApplicationInfo) zzbfn.m10171(parcel, readInt, ApplicationInfo.CREATOR);
                    break;
                case 7:
                    packageInfo = (PackageInfo) zzbfn.m10171(parcel, readInt, PackageInfo.CREATOR);
                    break;
                case 8:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 9:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 10:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 11:
                    zzakd = (zzakd) zzbfn.m10171(parcel, readInt, zzakd.CREATOR);
                    break;
                case 12:
                    bundle2 = zzbfn.m10153(parcel, readInt);
                    break;
                case 13:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 14:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 15:
                    bundle3 = zzbfn.m10153(parcel, readInt);
                    break;
                case 16:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 18:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 19:
                    i4 = zzbfn.m10166(parcel, readInt);
                    break;
                case 20:
                    f = zzbfn.m10151(parcel, readInt);
                    break;
                case 21:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 25:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 26:
                    str6 = zzbfn.m10162(parcel, readInt);
                    break;
                case 27:
                    arrayList2 = zzbfn.m10159(parcel, readInt);
                    break;
                case 28:
                    str7 = zzbfn.m10162(parcel, readInt);
                    break;
                case 29:
                    zzpe = (zzpe) zzbfn.m10171(parcel, readInt, zzpe.CREATOR);
                    break;
                case 30:
                    arrayList3 = zzbfn.m10159(parcel, readInt);
                    break;
                case 31:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 33:
                    str8 = zzbfn.m10162(parcel, readInt);
                    break;
                case 34:
                    f2 = zzbfn.m10151(parcel, readInt);
                    break;
                case 35:
                    i5 = zzbfn.m10166(parcel, readInt);
                    break;
                case 36:
                    i6 = zzbfn.m10166(parcel, readInt);
                    break;
                case 37:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 38:
                    z4 = zzbfn.m10168(parcel, readInt);
                    break;
                case 39:
                    str9 = zzbfn.m10162(parcel, readInt);
                    break;
                case 40:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 41:
                    str10 = zzbfn.m10162(parcel, readInt);
                    break;
                case 42:
                    z5 = zzbfn.m10168(parcel, readInt);
                    break;
                case 43:
                    i7 = zzbfn.m10166(parcel, readInt);
                    break;
                case 44:
                    bundle4 = zzbfn.m10153(parcel, readInt);
                    break;
                case 45:
                    str11 = zzbfn.m10162(parcel, readInt);
                    break;
                case 46:
                    zzlr = (zzlr) zzbfn.m10171(parcel, readInt, zzlr.CREATOR);
                    break;
                case 47:
                    z6 = zzbfn.m10168(parcel, readInt);
                    break;
                case 48:
                    bundle5 = zzbfn.m10153(parcel, readInt);
                    break;
                case 49:
                    str12 = zzbfn.m10162(parcel, readInt);
                    break;
                case 50:
                    str13 = zzbfn.m10162(parcel, readInt);
                    break;
                case 51:
                    str14 = zzbfn.m10162(parcel, readInt);
                    break;
                case 52:
                    z7 = zzbfn.m10168(parcel, readInt);
                    break;
                case 53:
                    arrayList4 = zzbfn.m10158(parcel, readInt);
                    break;
                case 54:
                    str15 = zzbfn.m10162(parcel, readInt);
                    break;
                case 55:
                    arrayList5 = zzbfn.m10159(parcel, readInt);
                    break;
                case 56:
                    i8 = zzbfn.m10166(parcel, readInt);
                    break;
                case 57:
                    z8 = zzbfn.m10168(parcel, readInt);
                    break;
                case 58:
                    z9 = zzbfn.m10168(parcel, readInt);
                    break;
                case 59:
                    z10 = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r59);
        return new zzaat(i, bundle, zzjj, zzjn, str, applicationInfo, packageInfo, str2, str3, str4, zzakd, bundle2, i2, arrayList, bundle3, z, i3, i4, f, str5, j, str6, arrayList2, str7, zzpe, arrayList3, j2, str8, f2, z2, i5, i6, z3, z4, str9, str10, z5, i7, bundle4, str11, zzlr, z6, bundle5, str12, str13, str14, z7, arrayList4, str15, arrayList5, i8, z8, z9, z10);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaat[i];
    }
}
