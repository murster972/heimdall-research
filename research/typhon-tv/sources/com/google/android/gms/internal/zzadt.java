package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;

@zzzv
public final class zzadt extends zzp<zzadn> {
    public zzadt() {
        super("com.google.android.gms.ads.reward.RewardedVideoAdCreatorImpl");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzadk m4356(Context context, zzux zzux) {
        try {
            IBinder r2 = ((zzadn) m9308(context)).m9496(zzn.m9306(context), zzux, 11910000);
            if (r2 == null) {
                return null;
            }
            IInterface queryLocalInterface = r2.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
            return queryLocalInterface instanceof zzadk ? (zzadk) queryLocalInterface : new zzadm(r2);
        } catch (RemoteException | zzq e) {
            zzakb.m4796("Could not get remote RewardedVideoAd.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m4357(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdCreator");
        return queryLocalInterface instanceof zzadn ? (zzadn) queryLocalInterface : new zzado(iBinder);
    }
}
