package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;

final class zzafv {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile int f8145;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile long f8146;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f8147;

    private zzafv() {
        this.f8147 = new Object();
        this.f8145 = zzafw.f8151;
        this.f8146 = 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m9572() {
        long r2 = zzbs.zzeo().m9243();
        synchronized (this.f8147) {
            if (this.f8145 == zzafw.f8150) {
                if (this.f8146 + ((Long) zzkb.m5481().m5595(zznh.f5043)).longValue() <= r2) {
                    this.f8145 = zzafw.f8151;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m9573(int r6, int r7) {
        /*
            r5 = this;
            r5.m9572()
            com.google.android.gms.common.util.zzd r0 = com.google.android.gms.ads.internal.zzbs.zzeo()
            long r0 = r0.m9243()
            java.lang.Object r2 = r5.f8147
            monitor-enter(r2)
            int r3 = r5.f8145     // Catch:{ all -> 0x0020 }
            if (r3 == r6) goto L_0x0014
            monitor-exit(r2)     // Catch:{ all -> 0x0020 }
        L_0x0013:
            return
        L_0x0014:
            r5.f8145 = r7     // Catch:{ all -> 0x0020 }
            int r3 = r5.f8145     // Catch:{ all -> 0x0020 }
            int r4 = com.google.android.gms.internal.zzafw.f8150     // Catch:{ all -> 0x0020 }
            if (r3 != r4) goto L_0x001e
            r5.f8146 = r0     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r2)     // Catch:{ all -> 0x0020 }
            goto L_0x0013
        L_0x0020:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0020 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzafv.m9573(int, int):void");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9574() {
        m9572();
        return this.f8145 == zzafw.f8150;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9575() {
        m9573(zzafw.f8148, zzafw.f8150);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9576(boolean z) {
        if (z) {
            m9573(zzafw.f8151, zzafw.f8148);
        } else {
            m9573(zzafw.f8148, zzafw.f8151);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9577() {
        m9572();
        return this.f8145 == zzafw.f8148;
    }
}
