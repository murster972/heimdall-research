package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.internal.zzbs;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzacb extends zzabc {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzacb f3918;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f3919 = new Object();

    /* renamed from: 连任  reason: contains not printable characters */
    private final ScheduledExecutorService f3920 = Executors.newSingleThreadScheduledExecutor();

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzaca f3921;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f3922;

    private zzacb(Context context, zzaca zzaca) {
        this.f3922 = context;
        this.f3921 = zzaca;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzaax m4277(Context context, zzaca zzaca, zzaat zzaat, ScheduledExecutorService scheduledExecutorService) {
        String string;
        zzagf.m4792("Starting ad request from service using: google.afma.request.getAdDictionary");
        zznu zznu = new zznu(((Boolean) zzkb.m5481().m5595(zznh.f4893)).booleanValue(), "load_ad", zzaat.f3762.f4798);
        if (zzaat.f3764 > 10 && zzaat.f3754 != -1) {
            zznu.m5631(zznu.m5626(zzaat.f3754), "cts");
        }
        zzns r12 = zznu.m5625();
        zzakv<V> r4 = zzakl.m4803(zzaca.f3908.m13634(context), ((Long) zzkb.m5481().m5595(zznh.f5013)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService);
        zzakv<V> r6 = zzakl.m4803(zzaca.f3907.m9463(context), ((Long) zzkb.m5481().m5595(zznh.f4954)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService);
        zzakv<String> r9 = zzaca.f3916.m9569(zzaat.f3718.packageName);
        zzakv<String> r7 = zzaca.f3910.m9571(zzaat.f3720, zzaat.f3718);
        Future<zzaco> r13 = zzbs.zzes().m4318(context);
        zzakv r3 = zzakl.m4802(null);
        Bundle bundle = zzaat.f3763.f4767;
        boolean z = (bundle == null || bundle.getString("_ad") == null) ? false : true;
        if (zzaat.f3751 && !z) {
            r3 = zzaca.f3906.m13439(zzaat.f3716);
        }
        zzakv r14 = zzakl.m4803(r3, ((Long) zzkb.m5481().m5595(zznh.f5004)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService);
        Future r5 = ((Boolean) zzkb.m5481().m5595(zznh.f4914)).booleanValue() ? zzakl.m4803(zzaca.f3910.m9570(context), ((Long) zzkb.m5481().m5595(zznh.f4915)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService) : zzakl.m4802(null);
        Bundle bundle2 = (zzaat.f3764 < 4 || zzaat.f3767 == null) ? null : zzaat.f3767;
        ((Boolean) zzkb.m5481().m5595(zznh.f5148)).booleanValue();
        zzbs.zzei();
        if (zzahn.m4618(context, context.getPackageName(), "android.permission.ACCESS_NETWORK_STATE") && ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            zzagf.m4792("Device is offline.");
        }
        String uuid = zzaat.f3764 >= 7 ? zzaat.f3738 : UUID.randomUUID().toString();
        new zzach(context, uuid, zzaat.f3716.packageName);
        if (zzaat.f3763.f4767 != null && (string = zzaat.f3763.f4767.getString("_ad")) != null) {
            return zzacg.m4285(context, zzaat, string);
        }
        List<String> r15 = zzaca.f3915.m13146(zzaat.f3742);
        Bundle bundle3 = (Bundle) zzakl.m4808(r4, null, ((Long) zzkb.m5481().m5595(zznh.f5013)).longValue(), TimeUnit.MILLISECONDS);
        zzacy zzacy = (zzacy) zzakl.m4807(r6, null);
        Location location = (Location) zzakl.m4807(r14, null);
        AdvertisingIdClient.Info info = (AdvertisingIdClient.Info) zzakl.m4807(r5, null);
        String str = (String) zzakl.m4807(r7, null);
        String str2 = (String) zzakl.m4807(r9, null);
        zzaco zzaco = (zzaco) zzakl.m4807(r13, null);
        if (zzaco == null) {
            zzagf.m4791("Error fetching device info. This is not recoverable.");
            return new zzaax(0);
        }
        zzabz zzabz = new zzabz();
        zzabz.f3898 = zzaat;
        zzabz.f3899 = zzaco;
        zzabz.f3901 = zzacy;
        zzabz.f3903 = location;
        zzabz.f3902 = bundle3;
        zzabz.f3895 = str;
        zzabz.f3896 = info;
        if (r15 == null) {
            zzabz.f3904.clear();
        }
        zzabz.f3904 = r15;
        zzabz.f3905 = bundle2;
        zzabz.f3894 = str2;
        zzabz.f3900 = zzaca.f3914.m13007(context);
        zzabz.f3897 = zzaca.f3911;
        JSONObject r2 = zzacg.m4289(context, zzabz);
        if (r2 == null) {
            return new zzaax(0);
        }
        if (zzaat.f3764 < 7) {
            try {
                r2.put("request_id", (Object) uuid);
            } catch (JSONException e) {
            }
        }
        r2.toString();
        zznu.m5631(r12, "arc");
        zznu.m5625();
        zzakv<V> r22 = zzakl.m4803(zzakl.m4804(zzaca.f3912.m9454().m5868(r2), zzacc.f8066, (Executor) scheduledExecutorService), 10, TimeUnit.SECONDS, scheduledExecutorService);
        zzakv<Void> r32 = zzaca.f3913.m4321();
        if (r32 != null) {
            zzakj.m4801(r32, "AdRequestServiceImpl.loadAd.flags");
        }
        zzacn zzacn = (zzacn) zzakl.m4807(r22, null);
        if (zzacn == null) {
            return new zzaax(0);
        }
        if (zzacn.m4315() != -2) {
            return new zzaax(zzacn.m4315());
        }
        zznu.m5623();
        zzaax zzaax = null;
        if (!TextUtils.isEmpty(zzacn.m4309())) {
            zzaax = zzacg.m4285(context, zzaat, zzacn.m4309());
        }
        if (zzaax == null && !TextUtils.isEmpty(zzacn.m4311())) {
            zzaax = m4278(zzaat, context, zzaat.f3748.f4297, zzacn.m4311(), str2, zzacn, zznu, zzaca);
        }
        if (zzaax == null) {
            zzaax = new zzaax(0);
        }
        zznu.m5631(r12, "tts");
        zzaax.f3845 = zznu.m5622();
        return zzaax;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b8, code lost:
        r7 = r8.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r5 = new java.io.InputStreamReader(r2.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        com.google.android.gms.ads.internal.zzbs.zzei();
        r6 = com.google.android.gms.internal.zzahn.m4601(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        com.google.android.gms.common.util.zzn.m9261(r5);
        r12.m4785(r6);
        m4281(r7, (java.util.Map<java.lang.String, java.util.List<java.lang.String>>) r13, r6, r3);
        r9.m4302(r7, r13, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d9, code lost:
        if (r20 == null) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00db, code lost:
        r20.m5631(r4, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e9, code lost:
        r3 = r9.m4301(r10, r19.m4310());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x012c, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x012d, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        com.google.android.gms.common.util.zzn.m9261(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0131, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01c7, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01c8, code lost:
        r4 = r5;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:55:0x0123=Splitter:B:55:0x0123, B:64:0x012e=Splitter:B:64:0x012e} */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.zzaax m4278(com.google.android.gms.internal.zzaat r14, android.content.Context r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, com.google.android.gms.internal.zzacn r19, com.google.android.gms.internal.zznu r20, com.google.android.gms.internal.zzaca r21) {
        /*
            if (r20 == 0) goto L_0x00f6
            com.google.android.gms.internal.zzns r2 = r20.m5625()
            r4 = r2
        L_0x0007:
            com.google.android.gms.internal.zzacl r9 = new com.google.android.gms.internal.zzacl     // Catch:{ IOException -> 0x0101 }
            java.lang.String r2 = r19.m4314()     // Catch:{ IOException -> 0x0101 }
            r9.<init>(r14, r2)     // Catch:{ IOException -> 0x0101 }
            java.lang.String r3 = "AdRequestServiceImpl: Sending request: "
            java.lang.String r2 = java.lang.String.valueOf(r17)     // Catch:{ IOException -> 0x0101 }
            int r5 = r2.length()     // Catch:{ IOException -> 0x0101 }
            if (r5 == 0) goto L_0x00fa
            java.lang.String r2 = r3.concat(r2)     // Catch:{ IOException -> 0x0101 }
        L_0x0021:
            com.google.android.gms.internal.zzagf.m4792(r2)     // Catch:{ IOException -> 0x0101 }
            java.net.URL r3 = new java.net.URL     // Catch:{ IOException -> 0x0101 }
            r0 = r17
            r3.<init>(r0)     // Catch:{ IOException -> 0x0101 }
            r2 = 0
            com.google.android.gms.common.util.zzd r5 = com.google.android.gms.ads.internal.zzbs.zzeo()     // Catch:{ IOException -> 0x0101 }
            long r10 = r5.m9241()     // Catch:{ IOException -> 0x0101 }
            r7 = r2
            r8 = r3
        L_0x0036:
            java.net.URLConnection r2 = r8.openConnection()     // Catch:{ IOException -> 0x0101 }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ IOException -> 0x0101 }
            com.google.android.gms.internal.zzahn r3 = com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0127 }
            r5 = 0
            r0 = r16
            r3.m4636((android.content.Context) r15, (java.lang.String) r0, (boolean) r5, (java.net.HttpURLConnection) r2)     // Catch:{ all -> 0x0127 }
            boolean r3 = android.text.TextUtils.isEmpty(r18)     // Catch:{ all -> 0x0127 }
            if (r3 != 0) goto L_0x005a
            boolean r3 = r19.m4307()     // Catch:{ all -> 0x0127 }
            if (r3 == 0) goto L_0x005a
            java.lang.String r3 = "x-afma-drt-cookie"
            r0 = r18
            r2.addRequestProperty(r3, r0)     // Catch:{ all -> 0x0127 }
        L_0x005a:
            java.lang.String r3 = r14.f3717     // Catch:{ all -> 0x0127 }
            boolean r5 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x0127 }
            if (r5 != 0) goto L_0x006e
            java.lang.String r5 = "Sending webview cookie in ad request header."
            com.google.android.gms.internal.zzagf.m4792(r5)     // Catch:{ all -> 0x0127 }
            java.lang.String r5 = "Cookie"
            r2.addRequestProperty(r5, r3)     // Catch:{ all -> 0x0127 }
        L_0x006e:
            r3 = 0
            if (r19 == 0) goto L_0x009b
            java.lang.String r5 = r19.m4313()     // Catch:{ all -> 0x0127 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0127 }
            if (r5 != 0) goto L_0x009b
            r3 = 1
            r2.setDoOutput(r3)     // Catch:{ all -> 0x0127 }
            java.lang.String r3 = r19.m4313()     // Catch:{ all -> 0x0127 }
            byte[] r3 = r3.getBytes()     // Catch:{ all -> 0x0127 }
            int r5 = r3.length     // Catch:{ all -> 0x0127 }
            r2.setFixedLengthStreamingMode(r5)     // Catch:{ all -> 0x0127 }
            r6 = 0
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0121 }
            java.io.OutputStream r12 = r2.getOutputStream()     // Catch:{ all -> 0x0121 }
            r5.<init>(r12)     // Catch:{ all -> 0x0121 }
            r5.write(r3)     // Catch:{ all -> 0x01cb }
            com.google.android.gms.common.util.zzn.m9261(r5)     // Catch:{ all -> 0x0127 }
        L_0x009b:
            com.google.android.gms.internal.zzajv r12 = new com.google.android.gms.internal.zzajv     // Catch:{ all -> 0x0127 }
            java.lang.String r5 = r14.f3738     // Catch:{ all -> 0x0127 }
            r12.<init>(r5)     // Catch:{ all -> 0x0127 }
            r12.m4788((java.net.HttpURLConnection) r2, (byte[]) r3)     // Catch:{ all -> 0x0127 }
            int r3 = r2.getResponseCode()     // Catch:{ all -> 0x0127 }
            java.util.Map r13 = r2.getHeaderFields()     // Catch:{ all -> 0x0127 }
            r12.m4787((java.net.HttpURLConnection) r2, (int) r3)     // Catch:{ all -> 0x0127 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r3 < r5) goto L_0x0132
            r5 = 300(0x12c, float:4.2E-43)
            if (r3 >= r5) goto L_0x0132
            java.lang.String r7 = r8.toString()     // Catch:{ all -> 0x0127 }
            r6 = 0
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ all -> 0x012c }
            java.io.InputStream r8 = r2.getInputStream()     // Catch:{ all -> 0x012c }
            r5.<init>(r8)     // Catch:{ all -> 0x012c }
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x01c7 }
            java.lang.String r6 = com.google.android.gms.internal.zzahn.m4601((java.io.InputStreamReader) r5)     // Catch:{ all -> 0x01c7 }
            com.google.android.gms.common.util.zzn.m9261(r5)     // Catch:{ all -> 0x0127 }
            r12.m4785((java.lang.String) r6)     // Catch:{ all -> 0x0127 }
            m4281((java.lang.String) r7, (java.util.Map<java.lang.String, java.util.List<java.lang.String>>) r13, (java.lang.String) r6, (int) r3)     // Catch:{ all -> 0x0127 }
            r9.m4302(r7, r13, r6)     // Catch:{ all -> 0x0127 }
            if (r20 == 0) goto L_0x00e9
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ all -> 0x0127 }
            r5 = 0
            java.lang.String r6 = "ufe"
            r3[r5] = r6     // Catch:{ all -> 0x0127 }
            r0 = r20
            r0.m5631((com.google.android.gms.internal.zzns) r4, (java.lang.String[]) r3)     // Catch:{ all -> 0x0127 }
        L_0x00e9:
            boolean r3 = r19.m4310()     // Catch:{ all -> 0x0127 }
            com.google.android.gms.internal.zzaax r3 = r9.m4301((long) r10, (boolean) r3)     // Catch:{ all -> 0x0127 }
            r2.disconnect()     // Catch:{ IOException -> 0x0101 }
            r2 = r3
        L_0x00f5:
            return r2
        L_0x00f6:
            r2 = 0
            r4 = r2
            goto L_0x0007
        L_0x00fa:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x0101 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0101 }
            goto L_0x0021
        L_0x0101:
            r2 = move-exception
            java.lang.String r3 = "Error while connecting to ad server: "
            java.lang.String r2 = r2.getMessage()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r4 = r2.length()
            if (r4 == 0) goto L_0x01c0
            java.lang.String r2 = r3.concat(r2)
        L_0x0117:
            com.google.android.gms.internal.zzagf.m4791(r2)
            com.google.android.gms.internal.zzaax r2 = new com.google.android.gms.internal.zzaax
            r3 = 2
            r2.<init>(r3)
            goto L_0x00f5
        L_0x0121:
            r3 = move-exception
            r4 = r6
        L_0x0123:
            com.google.android.gms.common.util.zzn.m9261(r4)     // Catch:{ all -> 0x0127 }
            throw r3     // Catch:{ all -> 0x0127 }
        L_0x0127:
            r3 = move-exception
            r2.disconnect()     // Catch:{ IOException -> 0x0101 }
            throw r3     // Catch:{ IOException -> 0x0101 }
        L_0x012c:
            r3 = move-exception
            r4 = r6
        L_0x012e:
            com.google.android.gms.common.util.zzn.m9261(r4)     // Catch:{ all -> 0x0127 }
            throw r3     // Catch:{ all -> 0x0127 }
        L_0x0132:
            java.lang.String r5 = r8.toString()     // Catch:{ all -> 0x0127 }
            r6 = 0
            m4281((java.lang.String) r5, (java.util.Map<java.lang.String, java.util.List<java.lang.String>>) r13, (java.lang.String) r6, (int) r3)     // Catch:{ all -> 0x0127 }
            r5 = 300(0x12c, float:4.2E-43)
            if (r3 < r5) goto L_0x018b
            r5 = 400(0x190, float:5.6E-43)
            if (r3 >= r5) goto L_0x018b
            java.lang.String r3 = "Location"
            java.lang.String r3 = r2.getHeaderField(r3)     // Catch:{ all -> 0x0127 }
            boolean r5 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x0127 }
            if (r5 == 0) goto L_0x0160
            java.lang.String r3 = "No location header to follow redirect."
            com.google.android.gms.internal.zzagf.m4791(r3)     // Catch:{ all -> 0x0127 }
            com.google.android.gms.internal.zzaax r3 = new com.google.android.gms.internal.zzaax     // Catch:{ all -> 0x0127 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ all -> 0x0127 }
            r2.disconnect()     // Catch:{ IOException -> 0x0101 }
            r2 = r3
            goto L_0x00f5
        L_0x0160:
            java.net.URL r6 = new java.net.URL     // Catch:{ all -> 0x0127 }
            r6.<init>(r3)     // Catch:{ all -> 0x0127 }
            int r5 = r7 + 1
            com.google.android.gms.internal.zzmx<java.lang.Integer> r3 = com.google.android.gms.internal.zznh.f5042     // Catch:{ all -> 0x0127 }
            com.google.android.gms.internal.zznf r7 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x0127 }
            java.lang.Object r3 = r7.m5595(r3)     // Catch:{ all -> 0x0127 }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ all -> 0x0127 }
            int r3 = r3.intValue()     // Catch:{ all -> 0x0127 }
            if (r5 <= r3) goto L_0x01b0
            java.lang.String r3 = "Too many redirects."
            com.google.android.gms.internal.zzagf.m4791(r3)     // Catch:{ all -> 0x0127 }
            com.google.android.gms.internal.zzaax r3 = new com.google.android.gms.internal.zzaax     // Catch:{ all -> 0x0127 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ all -> 0x0127 }
            r2.disconnect()     // Catch:{ IOException -> 0x0101 }
            r2 = r3
            goto L_0x00f5
        L_0x018b:
            r4 = 46
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0127 }
            r5.<init>(r4)     // Catch:{ all -> 0x0127 }
            java.lang.String r4 = "Received error HTTP response code: "
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ all -> 0x0127 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ all -> 0x0127 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0127 }
            com.google.android.gms.internal.zzagf.m4791(r3)     // Catch:{ all -> 0x0127 }
            com.google.android.gms.internal.zzaax r3 = new com.google.android.gms.internal.zzaax     // Catch:{ all -> 0x0127 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ all -> 0x0127 }
            r2.disconnect()     // Catch:{ IOException -> 0x0101 }
            r2 = r3
            goto L_0x00f5
        L_0x01b0:
            r9.m4303(r13)     // Catch:{ all -> 0x0127 }
            r2.disconnect()     // Catch:{ IOException -> 0x0101 }
            if (r21 == 0) goto L_0x01bc
            r7 = r5
            r8 = r6
            goto L_0x0036
        L_0x01bc:
            r7 = r5
            r8 = r6
            goto L_0x0036
        L_0x01c0:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r3)
            goto L_0x0117
        L_0x01c7:
            r3 = move-exception
            r4 = r5
            goto L_0x012e
        L_0x01cb:
            r3 = move-exception
            r4 = r5
            goto L_0x0123
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzacb.m4278(com.google.android.gms.internal.zzaat, android.content.Context, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.zzacn, com.google.android.gms.internal.zznu, com.google.android.gms.internal.zzaca):com.google.android.gms.internal.zzaax");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzacb m4279(Context context, zzaca zzaca) {
        zzacb zzacb;
        synchronized (f3919) {
            if (f3918 == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zznh.m5599(context);
                f3918 = new zzacb(context, zzaca);
                if (context.getApplicationContext() != null) {
                    zzbs.zzei().m4630(context);
                }
                zzagd.m4526(context);
            }
            zzacb = f3918;
        }
        return zzacb;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4281(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzagf.m4798(2)) {
            zzagf.m4527(new StringBuilder(String.valueOf(str).length() + 39).append("Http Response: {\n  URL:\n    ").append(str).append("\n  Headers:").toString());
            if (map != null) {
                for (String next : map.keySet()) {
                    zzagf.m4527(new StringBuilder(String.valueOf(next).length() + 5).append("    ").append(next).append(":").toString());
                    for (String valueOf : map.get(next)) {
                        String valueOf2 = String.valueOf(valueOf);
                        zzagf.m4527(valueOf2.length() != 0 ? "      ".concat(valueOf2) : new String("      "));
                    }
                }
            }
            zzagf.m4527("  Body:");
            if (str2 != null) {
                for (int i2 = 0; i2 < Math.min(str2.length(), DefaultOggSeeker.MATCH_BYTE_RANGE); i2 += 1000) {
                    zzagf.m4527(str2.substring(i2, Math.min(str2.length(), i2 + 1000)));
                }
            } else {
                zzagf.m4527("    null");
            }
            zzagf.m4527(new StringBuilder(34).append("  Response Code:\n    ").append(i).append("\n}").toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaax m4282(zzaat zzaat) {
        return m4277(this.f3922, this.f3921, zzaat, this.f3920);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4283(zzaat zzaat, zzabe zzabe) {
        zzbs.zzem().m4500(this.f3922, zzaat.f3748);
        zzakv<Void> r0 = zzahh.m4555((Runnable) new zzacd(this, zzaat, zzabe));
        zzbs.zzew().m4718();
        zzbs.zzew().m4717().postDelayed(new zzace(this, r0), 60000);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4284(zzabm zzabm, zzabh zzabh) {
        zzagf.m4527("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }
}
