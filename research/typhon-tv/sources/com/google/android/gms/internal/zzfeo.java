package com.google.android.gms.internal;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

abstract class zzfeo<E> extends AbstractList<E> implements zzfgd<E> {

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f10344 = true;

    zzfeo() {
    }

    public void add(int i, E e) {
        m12367();
        super.add(i, e);
    }

    public boolean add(E e) {
        m12367();
        return super.add(e);
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        m12367();
        return super.addAll(i, collection);
    }

    public boolean addAll(Collection<? extends E> collection) {
        m12367();
        return super.addAll(collection);
    }

    public void clear() {
        m12367();
        super.clear();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        if (!(obj instanceof RandomAccess)) {
            return super.equals(obj);
        }
        List list = (List) obj;
        int size = size();
        if (size != list.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!get(i).equals(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    public E remove(int i) {
        m12367();
        return super.remove(i);
    }

    public boolean remove(Object obj) {
        m12367();
        return super.remove(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        m12367();
        return super.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        m12367();
        return super.retainAll(collection);
    }

    public E set(int i, E e) {
        m12367();
        return super.set(i, e);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12366() {
        this.f10344 = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12367() {
        if (!this.f10344) {
            throw new UnsupportedOperationException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12368() {
        return this.f10344;
    }
}
