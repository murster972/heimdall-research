package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement;

public final class zzchm extends zzcjl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzcho f9263;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzcho f9264;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzcho f9265;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzcho f9266;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzcho f9267;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzcho f9268;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final zzcho f9269;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzcho f9270;

    /* renamed from: 靐  reason: contains not printable characters */
    private final char f9271;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzcho f9272;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f9273 = 11910;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f9274 = zzchc.f9239.m10671();

    zzchm(zzcim zzcim) {
        super(zzcim);
        if (m11102().m10538()) {
            this.f9271 = 'C';
        } else {
            this.f9271 = 'c';
        }
        this.f9272 = new zzcho(this, 6, false, false);
        this.f9270 = new zzcho(this, 6, true, false);
        this.f9263 = new zzcho(this, 6, false, true);
        this.f9264 = new zzcho(this, 5, false, false);
        this.f9265 = new zzcho(this, 5, true, false);
        this.f9267 = new zzcho(this, 5, false, true);
        this.f9268 = new zzcho(this, 4, false, false);
        this.f9269 = new zzcho(this, 3, false, false);
        this.f9266 = new zzcho(this, 2, false, false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m10811(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf != -1 ? str.substring(0, lastIndexOf) : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static Object m10812(String str) {
        if (str == null) {
            return null;
        }
        return new zzchp(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m10813(boolean z, Object obj) {
        StackTraceElement stackTraceElement;
        String className;
        if (obj == null) {
            return "";
        }
        Object valueOf = obj instanceof Integer ? Long.valueOf((long) ((Integer) obj).intValue()) : obj;
        if (valueOf instanceof Long) {
            if (!z) {
                return String.valueOf(valueOf);
            }
            if (Math.abs(((Long) valueOf).longValue()) < 100) {
                return String.valueOf(valueOf);
            }
            String str = String.valueOf(valueOf).charAt(0) == '-' ? "-" : "";
            String valueOf2 = String.valueOf(Math.abs(((Long) valueOf).longValue()));
            return new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(str).length()).append(str).append(Math.round(Math.pow(10.0d, (double) (valueOf2.length() - 1)))).append("...").append(str).append(Math.round(Math.pow(10.0d, (double) valueOf2.length()) - 1.0d)).toString();
        } else if (valueOf instanceof Boolean) {
            return String.valueOf(valueOf);
        } else {
            if (!(valueOf instanceof Throwable)) {
                return valueOf instanceof zzchp ? ((zzchp) valueOf).f9281 : z ? "-" : String.valueOf(valueOf);
            }
            Throwable th = (Throwable) valueOf;
            StringBuilder sb = new StringBuilder(z ? th.getClass().getName() : th.toString());
            String r4 = m10811(AppMeasurement.class.getCanonicalName());
            String r5 = m10811(zzcim.class.getCanonicalName());
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                stackTraceElement = stackTrace[i];
                if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null) {
                    String r7 = m10811(className);
                    if (r7.equals(r4) || r7.equals(r5)) {
                        sb.append(": ");
                        sb.append(stackTraceElement);
                    }
                }
                i++;
            }
            sb.append(": ");
            sb.append(stackTraceElement);
            return sb.toString();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m10814(boolean z, String str, Object obj, Object obj2, Object obj3) {
        if (str == null) {
            str = "";
        }
        String r1 = m10813(z, obj);
        String r2 = m10813(z, obj2);
        String r3 = m10813(z, obj3);
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(r1)) {
            sb.append(str2);
            sb.append(r1);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(r2)) {
            sb.append(str2);
            sb.append(r2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(r3)) {
            sb.append(str2);
            sb.append(r3);
        }
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10815() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10816() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10817() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10818() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10819() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10820() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10821() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10822() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10823() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10824() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10825() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10826() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10827() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10828() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10829() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10830() {
        return super.m11105();
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final String m10831() {
        Pair<String, Long> r0 = m11098().f9318.m10906();
        if (r0 == null || r0 == zzchx.f9300) {
            return null;
        }
        String valueOf = String.valueOf(r0.second);
        String str = (String) r0.first;
        return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length()).append(valueOf).append(":").append(str).toString();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final zzcho m10832() {
        return this.f9272;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final zzcho m10833() {
        return this.f9270;
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final zzcho m10834() {
        return this.f9264;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final zzcho m10835() {
        return this.f9267;
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final zzcho m10836() {
        return this.f9268;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10837() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10838() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10839() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10840() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10841() {
        super.m11110();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10842(int i, String str) {
        Log.println(i, this.f9274, str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10843(int i, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && m10844(i)) {
            m10842(i, m10814(false, str, obj, obj2, obj3));
        }
        if (!z2 && i >= 5) {
            zzbq.m9120(str);
            zzcih r5 = this.f9487.m11032();
            if (r5 == null) {
                m10842(6, "Scheduler not set. Not logging error/warn");
            } else if (!r5.m11113()) {
                m10842(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                int i2 = i < 0 ? 0 : i;
                if (i2 >= 9) {
                    i2 = 8;
                }
                char charAt = "01VDIWEA?".charAt(i2);
                char c = this.f9271;
                long j = this.f9273;
                String r10 = m10814(true, str, obj, obj2, obj3);
                String sb = new StringBuilder(String.valueOf("2").length() + 23 + String.valueOf(r10).length()).append("2").append(charAt).append(c).append(j).append(":").append(r10).toString();
                if (sb.length() > 1024) {
                    sb = str.substring(0, 1024);
                }
                r5.m10986((Runnable) new zzchn(this, sb));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10844(int i) {
        return Log.isLoggable(this.f9274, i);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public final zzcho m10845() {
        return this.f9269;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10846() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10847() {
        return super.m11112();
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final zzcho m10848() {
        return this.f9266;
    }
}
