package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagl extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8161;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8162;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagl(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8162 = context;
        this.f8161 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9584() {
        SharedPreferences sharedPreferences = this.f8162.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putBoolean("auto_collect_location", sharedPreferences.getBoolean("auto_collect_location", false));
        if (this.f8161 != null) {
            this.f8161.m9605(bundle);
        }
    }
}
