package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import java.lang.ref.WeakReference;

public final class zzdt implements Application.ActivityLifecycleCallbacks, View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Handler f10088 = new Handler(Looper.getMainLooper());

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzdm f10089;

    /* renamed from: ʼ  reason: contains not printable characters */
    private BroadcastReceiver f10090;

    /* renamed from: ʽ  reason: contains not printable characters */
    private WeakReference<ViewTreeObserver> f10091;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f10092 = -3;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f10093 = -1;

    /* renamed from: ˑ  reason: contains not printable characters */
    private WeakReference<View> f10094;

    /* renamed from: ٴ  reason: contains not printable characters */
    private zzda f10095;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f10096 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private final KeyguardManager f10097;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f10098;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PowerManager f10099;

    /* renamed from: 齉  reason: contains not printable characters */
    private Application f10100;

    public zzdt(zzdm zzdm, View view) {
        this.f10089 = zzdm;
        this.f10098 = zzdm.f9887;
        this.f10099 = (PowerManager) this.f10098.getSystemService("power");
        this.f10097 = (KeyguardManager) this.f10098.getSystemService("keyguard");
        if (this.f10098 instanceof Application) {
            this.f10100 = (Application) this.f10098;
            this.f10095 = new zzda((Application) this.f10098, this);
        }
        m12031(view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m12024() {
        f10088.post(new zzdu(this));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m12025(View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            this.f10091 = new WeakReference<>(viewTreeObserver);
            viewTreeObserver.addOnScrollChangedListener(this);
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
        if (this.f10090 == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.f10090 = new zzdv(this);
            this.f10098.registerReceiver(this.f10090, intentFilter);
        }
        if (this.f10100 != null) {
            try {
                this.f10100.registerActivityLifecycleCallbacks(this.f10095);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        if (r1 == false) goto L_0x0092;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m12026() {
        /*
            r9 = this;
            r3 = 1
            r2 = 0
            java.lang.ref.WeakReference<android.view.View> r0 = r9.f10094
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.lang.ref.WeakReference<android.view.View> r0 = r9.f10094
            java.lang.Object r0 = r0.get()
            android.view.View r0 = (android.view.View) r0
            if (r0 != 0) goto L_0x0018
            r0 = -3
            r9.f10092 = r0
            r9.f10096 = r2
            goto L_0x0006
        L_0x0018:
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            boolean r5 = r0.getGlobalVisibleRect(r1)
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            boolean r6 = r0.getLocalVisibleRect(r1)
            com.google.android.gms.internal.zzdm r1 = r9.f10089
            boolean r1 = r1.m11616()
            if (r1 != 0) goto L_0x0053
            android.app.KeyguardManager r1 = r9.f10097
            boolean r1 = r1.inKeyguardRestrictedInputMode()
            if (r1 == 0) goto L_0x0092
            android.app.Activity r1 = com.google.android.gms.internal.zzdr.m11762((android.view.View) r0)
            if (r1 == 0) goto L_0x0090
            android.view.Window r1 = r1.getWindow()
            if (r1 != 0) goto L_0x008b
            r1 = 0
        L_0x0047:
            if (r1 == 0) goto L_0x0090
            int r1 = r1.flags
            r4 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 & r4
            if (r1 == 0) goto L_0x0090
            r1 = r3
        L_0x0051:
            if (r1 == 0) goto L_0x0092
        L_0x0053:
            r1 = r3
        L_0x0054:
            int r4 = r0.getWindowVisibility()
            int r7 = r9.f10093
            r8 = -1
            if (r7 == r8) goto L_0x005f
            int r4 = r9.f10093
        L_0x005f:
            int r7 = r0.getVisibility()
            if (r7 != 0) goto L_0x0094
            boolean r0 = r0.isShown()
            if (r0 == 0) goto L_0x0094
            android.os.PowerManager r0 = r9.f10099
            boolean r0 = r0.isScreenOn()
            if (r0 == 0) goto L_0x0094
            if (r1 == 0) goto L_0x0094
            if (r6 == 0) goto L_0x0094
            if (r5 == 0) goto L_0x0094
            if (r4 != 0) goto L_0x0094
        L_0x007b:
            boolean r0 = r9.f10096
            if (r0 == r3) goto L_0x0006
            if (r3 == 0) goto L_0x0096
            long r0 = android.os.SystemClock.elapsedRealtime()
        L_0x0085:
            r9.f10092 = r0
            r9.f10096 = r3
            goto L_0x0006
        L_0x008b:
            android.view.WindowManager$LayoutParams r1 = r1.getAttributes()
            goto L_0x0047
        L_0x0090:
            r1 = r2
            goto L_0x0051
        L_0x0092:
            r1 = r2
            goto L_0x0054
        L_0x0094:
            r3 = r2
            goto L_0x007b
        L_0x0096:
            r0 = -2
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdt.m12026():void");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m12027(View view) {
        try {
            if (this.f10091 != null) {
                ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f10091.get();
                if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnScrollChangedListener(this);
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
                this.f10091 = null;
            }
        } catch (Exception e) {
        }
        try {
            ViewTreeObserver viewTreeObserver2 = view.getViewTreeObserver();
            if (viewTreeObserver2.isAlive()) {
                viewTreeObserver2.removeOnScrollChangedListener(this);
                viewTreeObserver2.removeGlobalOnLayoutListener(this);
            }
        } catch (Exception e2) {
        }
        if (this.f10090 != null) {
            try {
                this.f10098.unregisterReceiver(this.f10090);
            } catch (Exception e3) {
            }
            this.f10090 = null;
        }
        if (this.f10100 != null) {
            try {
                this.f10100.unregisterActivityLifecycleCallbacks(this.f10095);
            } catch (Exception e4) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m12028(Activity activity, int i) {
        Window window;
        if (this.f10094 != null && (window = activity.getWindow()) != null) {
            View peekDecorView = window.peekDecorView();
            View view = (View) this.f10094.get();
            if (view != null && peekDecorView != null && view.getRootView() == peekDecorView.getRootView()) {
                this.f10093 = i;
            }
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        m12028(activity, 0);
        m12026();
    }

    public final void onActivityDestroyed(Activity activity) {
        m12026();
    }

    public final void onActivityPaused(Activity activity) {
        m12028(activity, 4);
        m12026();
    }

    public final void onActivityResumed(Activity activity) {
        m12028(activity, 0);
        m12026();
        m12024();
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        m12026();
    }

    public final void onActivityStarted(Activity activity) {
        m12028(activity, 0);
        m12026();
    }

    public final void onActivityStopped(Activity activity) {
        m12026();
    }

    public final void onGlobalLayout() {
        m12026();
    }

    public final void onScrollChanged() {
        m12026();
    }

    public final void onViewAttachedToWindow(View view) {
        this.f10093 = -1;
        m12025(view);
        m12026();
    }

    public final void onViewDetachedFromWindow(View view) {
        this.f10093 = -1;
        m12026();
        m12024();
        m12027(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m12030() {
        if (this.f10092 == -2 && this.f10094.get() == null) {
            this.f10092 = -3;
        }
        return this.f10092;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12031(View view) {
        View view2 = this.f10094 != null ? (View) this.f10094.get() : null;
        if (view2 != null) {
            view2.removeOnAttachStateChangeListener(this);
            m12027(view2);
        }
        this.f10094 = new WeakReference<>(view);
        if (view != null) {
            if ((view.getWindowToken() == null && view.getWindowVisibility() == 8) ? false : true) {
                m12025(view);
            }
            view.addOnAttachStateChangeListener(this);
            this.f10092 = -2;
            return;
        }
        this.f10092 = -3;
    }
}
