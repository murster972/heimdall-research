package com.google.android.gms.internal;

import android.widget.TextView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbaw extends UIController {

    /* renamed from: 龘  reason: contains not printable characters */
    private final TextView f8596;

    public zzbaw(TextView textView) {
        this.f8596 = textView;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9938() {
        MediaInfo r0;
        MediaMetadata r1;
        RemoteMediaClient r02 = m8226();
        if (r02 != null && (r0 = r02.m4137()) != null && (r1 = r0.m7846()) != null) {
            String str = "com.google.android.gms.cast.metadata.SUBTITLE";
            if (!r1.m7885(str)) {
                switch (r1.m7880()) {
                    case 1:
                        str = "com.google.android.gms.cast.metadata.STUDIO";
                        break;
                    case 2:
                        str = "com.google.android.gms.cast.metadata.SERIES_TITLE";
                        break;
                    case 3:
                        if (!r1.m7885("com.google.android.gms.cast.metadata.ARTIST")) {
                            if (!r1.m7885("com.google.android.gms.cast.metadata.ALBUM_ARTIST")) {
                                if (r1.m7885("com.google.android.gms.cast.metadata.COMPOSER")) {
                                    str = "com.google.android.gms.cast.metadata.COMPOSER";
                                    break;
                                }
                            } else {
                                str = "com.google.android.gms.cast.metadata.ALBUM_ARTIST";
                                break;
                            }
                        }
                        break;
                    case 4:
                        str = "com.google.android.gms.cast.metadata.ARTIST";
                        break;
                }
            }
            if (r1.m7885(str)) {
                this.f8596.setText(r1.m7876(str));
            }
        }
    }
}
