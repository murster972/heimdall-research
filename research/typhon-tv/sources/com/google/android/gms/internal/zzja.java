package com.google.android.gms.internal;

import java.io.IOException;

public final class zzja extends zzfjm<zzja> {

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f10744 = null;

    public zzja() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzja m13020(com.google.android.gms.internal.zzfjj r7) throws java.io.IOException {
        /*
            r6 = this;
        L_0x0000:
            int r0 = r7.m12800()
            switch(r0) {
                case 0: goto L_0x000d;
                case 56: goto L_0x000e;
                default: goto L_0x0007;
            }
        L_0x0007:
            boolean r0 = super.m12843(r7, r0)
            if (r0 != 0) goto L_0x0000
        L_0x000d:
            return r6
        L_0x000e:
            int r1 = r7.m12787()
            int r2 = r7.m12785()     // Catch:{ IllegalArgumentException -> 0x0035 }
            switch(r2) {
                case 0: goto L_0x003d;
                case 1: goto L_0x003d;
                case 2: goto L_0x003d;
                case 3: goto L_0x003d;
                case 4: goto L_0x003d;
                case 5: goto L_0x003d;
                case 6: goto L_0x003d;
                case 7: goto L_0x003d;
                case 8: goto L_0x003d;
                case 9: goto L_0x003d;
                default: goto L_0x0019;
            }     // Catch:{ IllegalArgumentException -> 0x0035 }
        L_0x0019:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x0035 }
            r4 = 43
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0035 }
            r5.<init>(r4)     // Catch:{ IllegalArgumentException -> 0x0035 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ IllegalArgumentException -> 0x0035 }
            java.lang.String r4 = " is not a valid enum AdInitiater"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IllegalArgumentException -> 0x0035 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x0035 }
            r3.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x0035 }
            throw r3     // Catch:{ IllegalArgumentException -> 0x0035 }
        L_0x0035:
            r2 = move-exception
            r7.m12792(r1)
            r6.m12843(r7, r0)
            goto L_0x0000
        L_0x003d:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ IllegalArgumentException -> 0x0035 }
            r6.f10744 = r2     // Catch:{ IllegalArgumentException -> 0x0035 }
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzja.m13020(com.google.android.gms.internal.zzfjj):com.google.android.gms.internal.zzja");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m13019() {
        int r0 = super.m12841();
        return this.f10744 != null ? r0 + zzfjk.m12806(7, this.f10744.intValue()) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13021(zzfjk zzfjk) throws IOException {
        if (this.f10744 != null) {
            zzfjk.m12832(7, this.f10744.intValue());
        }
        super.m12842(zzfjk);
    }
}
