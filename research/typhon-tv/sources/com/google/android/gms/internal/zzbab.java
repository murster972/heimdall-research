package com.google.android.gms.internal;

import android.content.Intent;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.KeyEvent;

final class zzbab extends MediaSessionCompat.Callback {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzazy f8550;

    zzbab(zzazy zzazy) {
        this.f8550 = zzazy;
    }

    public final boolean onMediaButtonEvent(Intent intent) {
        KeyEvent keyEvent = (KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
        if (keyEvent == null) {
            return true;
        }
        if (keyEvent.getKeyCode() != 127 && keyEvent.getKeyCode() != 126) {
            return true;
        }
        this.f8550.f8516.m4142();
        return true;
    }

    public final void onPause() {
        this.f8550.f8516.m4142();
    }

    public final void onPlay() {
        this.f8550.f8516.m4142();
    }

    public final void onStop() {
        if (this.f8550.f8516.m4147()) {
            this.f8550.f8516.m4142();
        }
    }
}
