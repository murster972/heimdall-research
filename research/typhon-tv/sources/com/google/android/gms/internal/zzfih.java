package com.google.android.gms.internal;

final class zzfih {
    /* renamed from: 龘  reason: contains not printable characters */
    static String m12703(zzfes zzfes) {
        zzfii zzfii = new zzfii(zzfes);
        StringBuilder sb = new StringBuilder(zzfii.m12707());
        for (int i = 0; i < zzfii.m12707(); i++) {
            byte r3 = zzfii.m12706(i);
            switch (r3) {
                case 7:
                    sb.append("\\a");
                    break;
                case 8:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 11:
                    sb.append("\\v");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case 34:
                    sb.append("\\\"");
                    break;
                case 39:
                    sb.append("\\'");
                    break;
                case 92:
                    sb.append("\\\\");
                    break;
                default:
                    if (r3 >= 32 && r3 <= 126) {
                        sb.append((char) r3);
                        break;
                    } else {
                        sb.append('\\');
                        sb.append((char) (((r3 >>> 6) & 3) + 48));
                        sb.append((char) (((r3 >>> 3) & 7) + 48));
                        sb.append((char) ((r3 & 7) + 48));
                        break;
                    }
            }
        }
        return sb.toString();
    }
}
