package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbt;

public final class zzcxr implements Parcelable.Creator<zzcxq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        int i = 0;
        zzbt zzbt = null;
        ConnectionResult connectionResult = null;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    connectionResult = (ConnectionResult) zzbfn.m10171(parcel, readInt, ConnectionResult.CREATOR);
                    break;
                case 3:
                    zzbt = (zzbt) zzbfn.m10171(parcel, readInt, zzbt.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new zzcxq(i, connectionResult, zzbt);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcxq[i];
    }
}
