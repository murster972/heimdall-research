package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import java.lang.Thread;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public final class zzcih extends zzcjl {
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final AtomicLong f9358 = new AtomicLong(Long.MIN_VALUE);

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Thread.UncaughtExceptionHandler f9359 = new zzcij(this, "Thread death: Uncaught exception on worker thread");

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Thread.UncaughtExceptionHandler f9360 = new zzcij(this, "Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Object f9361 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final Semaphore f9362 = new Semaphore(2);
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public volatile boolean f9363;

    /* renamed from: 连任  reason: contains not printable characters */
    private final BlockingQueue<zzcik<?>> f9364 = new LinkedBlockingQueue();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public zzcil f9365;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PriorityBlockingQueue<zzcik<?>> f9366 = new PriorityBlockingQueue<>();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public zzcil f9367;

    /* renamed from: 龘  reason: contains not printable characters */
    private ExecutorService f9368;

    zzcih(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static boolean m10950() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10959(zzcik<?> zzcik) {
        synchronized (this.f9361) {
            this.f9366.add(zzcik);
            if (this.f9365 == null) {
                this.f9365 = new zzcil(this, "Measurement Worker", this.f9366);
                this.f9365.setUncaughtExceptionHandler(this.f9359);
                this.f9365.start();
            } else {
                this.f9365.m10990();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10960() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10961() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10962() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10963() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10964() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10965() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10966() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10967() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10968() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10969() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10970() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10971() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10972() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10973() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10974() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10975() {
        return super.m11105();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final boolean m10976() {
        return Thread.currentThread() == this.f9365;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public final ExecutorService m10977() {
        ExecutorService executorService;
        synchronized (this.f9361) {
            if (this.f9368 == null) {
                this.f9368 = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new ArrayBlockingQueue(100));
            }
            executorService = this.f9368;
        }
        return executorService;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10978() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <V> Future<V> m10979(Callable<V> callable) throws IllegalStateException {
        m11115();
        zzbq.m9120(callable);
        zzcik zzcik = new zzcik(this, callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.f9365) {
            zzcik.run();
        } else {
            m10959((zzcik<?>) zzcik);
        }
        return zzcik;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10980() {
        if (Thread.currentThread() != this.f9367) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10981(Runnable runnable) throws IllegalStateException {
        m11115();
        zzbq.m9120(runnable);
        zzcik zzcik = new zzcik(this, runnable, false, "Task exception on network thread");
        synchronized (this.f9361) {
            this.f9364.add(zzcik);
            if (this.f9367 == null) {
                this.f9367 = new zzcil(this, "Measurement Network", this.f9364);
                this.f9367.setUncaughtExceptionHandler(this.f9360);
                this.f9367.start();
            } else {
                this.f9367.m10990();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10982() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10983() {
        if (Thread.currentThread() != this.f9365) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <V> Future<V> m10984(Callable<V> callable) throws IllegalStateException {
        m11115();
        zzbq.m9120(callable);
        zzcik zzcik = new zzcik(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.f9365) {
            if (!this.f9366.isEmpty()) {
                m11096().m10834().m10849("Callable skipped the worker queue.");
            }
            zzcik.run();
        } else {
            m10959((zzcik<?>) zzcik);
        }
        return zzcik;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10985() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10986(Runnable runnable) throws IllegalStateException {
        m11115();
        zzbq.m9120(runnable);
        m10959((zzcik<?>) new zzcik(this, runnable, false, "Task exception on worker thread"));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10987() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10988() {
        return super.m11112();
    }
}
