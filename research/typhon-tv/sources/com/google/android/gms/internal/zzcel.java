package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzcel extends zzcem {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ PendingIntent f9064;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcel(zzceb zzceb, GoogleApiClient googleApiClient, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.f9064 = pendingIntent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10348(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10407(this.f9064, (zzceu) new zzcen(this));
    }
}
