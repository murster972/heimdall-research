package com.google.android.gms.internal;

final class zzckv implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcku f9611;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzche f9612;

    zzckv(zzcku zzcku, zzche zzche) {
        this.f9611 = zzcku;
        this.f9612 = zzche;
    }

    public final void run() {
        synchronized (this.f9611) {
            boolean unused = this.f9611.f9608 = false;
            if (!this.f9611.f9610.m11251()) {
                this.f9611.f9610.m11096().m10848().m10849("Connected to service");
                this.f9611.f9610.m11263(this.f9612);
            }
        }
    }
}
