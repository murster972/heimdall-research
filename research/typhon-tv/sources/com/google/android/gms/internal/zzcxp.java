package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbr;

public final class zzcxp implements Parcelable.Creator<zzcxo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r2 = zzbfn.m10169(parcel);
        int i = 0;
        zzbr zzbr = null;
        while (parcel.dataPosition() < r2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    zzbr = (zzbr) zzbfn.m10171(parcel, readInt, zzbr.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r2);
        return new zzcxo(i, zzbr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcxo[i];
    }
}
