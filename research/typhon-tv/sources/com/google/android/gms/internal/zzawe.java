package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzawe extends zzeu implements zzawd {
    zzawe(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
    }
}
