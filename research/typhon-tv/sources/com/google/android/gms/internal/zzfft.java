package com.google.android.gms.internal;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

final class zzfft implements zzfhd {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfft f10391 = new zzfft();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<Class<?>, Method> f10392 = new HashMap();

    private zzfft() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfft m12522() {
        return f10391;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhc m12523(Class<?> cls) {
        if (!zzffu.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            Method method = this.f10392.get(cls);
            if (method == null) {
                method = cls.getDeclaredMethod("buildMessageInfo", new Class[0]);
                method.setAccessible(true);
                this.f10392.put(cls, method);
            }
            return (zzfhc) method.invoke((Object) null, new Object[0]);
        } catch (Exception e) {
            Exception exc = e;
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), exc);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12524(Class<?> cls) {
        return zzffu.class.isAssignableFrom(cls);
    }
}
