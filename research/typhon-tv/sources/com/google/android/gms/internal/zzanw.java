package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.Pinkamena;
import com.google.android.gms.R;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import java.util.Map;
import org.json.JSONObject;

@zzzv
final class zzanw extends FrameLayout implements zzanh {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f4474 = Color.argb(0, 0, 0, 0);

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzanh f4475;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzamg f4476;

    public zzanw(zzanh zzanh) {
        super(zzanh.getContext());
        this.f4475 = zzanh;
        this.f4476 = new zzamg(zzanh.m5017(), this, this);
        zzani r0 = this.f4475.m4980();
        if (r0 != null) {
            r0.f4470 = this;
        }
        zzanh zzanh2 = this.f4475;
        if (zzanh2 == null) {
            throw null;
        }
        View view = (View) zzanh2;
        Pinkamena.DianePie();
    }

    public final void destroy() {
        this.f4475.destroy();
    }

    public final void loadData(String str, String str2, String str3) {
        zzanh zzanh = this.f4475;
        Pinkamena.DianePie();
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        zzanh zzanh = this.f4475;
        String str6 = str;
        String str7 = str2;
        String str8 = str3;
        String str9 = str4;
        String str10 = str5;
        Pinkamena.DianePie();
    }

    public final void loadUrl(String str) {
        zzanh zzanh = this.f4475;
        Pinkamena.DianePie();
    }

    public final void onPause() {
        this.f4476.m4891();
        this.f4475.onPause();
    }

    public final void onResume() {
        this.f4475.onResume();
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.f4475.setOnClickListener(onClickListener);
    }

    public final void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.f4475.setOnTouchListener(onTouchListener);
    }

    public final void setWebChromeClient(WebChromeClient webChromeClient) {
        this.f4475.setWebChromeClient(webChromeClient);
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        this.f4475.setWebViewClient(webViewClient);
    }

    public final void stopLoading() {
        this.f4475.stopLoading();
    }

    public final void zza(String str, Map<String, ?> map) {
        this.f4475.zza(str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        this.f4475.zza(str, jSONObject);
    }

    public final void zzb(String str, JSONObject jSONObject) {
        this.f4475.zzb(str, jSONObject);
    }

    public final void zzcq() {
        this.f4475.zzcq();
    }

    public final void zzcr() {
        this.f4475.zzcr();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5061() {
        this.f4475.m4920();
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final zzoq m5062() {
        return this.f4475.m4971();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5063() {
        return this.f4475.m4972();
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final void m5064() {
        TextView textView = new TextView(getContext());
        Resources r0 = zzbs.zzem().m4476();
        textView.setText(r0 != null ? r0.getString(R.string.s7) : "Test Ad");
        textView.setTextSize(15.0f);
        textView.setTextColor(-1);
        textView.setPadding(5, 0, 5, 0);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setColor(-12303292);
        gradientDrawable.setCornerRadius(8.0f);
        if (Build.VERSION.SDK_INT >= 16) {
            textView.setBackground(gradientDrawable);
        } else {
            textView.setBackgroundDrawable(gradientDrawable);
        }
        new FrameLayout.LayoutParams(-2, -2, 49);
        Pinkamena.DianePie();
        bringChildToFront(textView);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zznt m5065() {
        return this.f4475.m4974();
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final void m5066() {
        setBackgroundColor(f4474);
        this.f4475.setBackgroundColor(f4474);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m5067() {
        this.f4475.m4976();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m5068() {
        this.f4475.m4977();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final String m5069() {
        return this.f4475.m4978();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final WebView m5070() {
        return this.f4475.m4979();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final zzani m5071() {
        return this.f4475.m4980();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final zzd m5072() {
        return this.f4475.m4981();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzd m5073() {
        return this.f4475.m4982();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final zzapa m5074() {
        return this.f4475.m4983();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final boolean m5075() {
        return this.f4475.m4984();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzakd m5076() {
        return this.f4475.m4985();
    }

    /* renamed from: י  reason: contains not printable characters */
    public final zzcv m5077() {
        return this.f4475.m4986();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final boolean m5078() {
        return this.f4475.m4987();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m5079() {
        return getMeasuredHeight();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int m5080() {
        return getMeasuredWidth();
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final void m5081() {
        this.f4475.m4988();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final int m5082() {
        return this.f4475.m4989();
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final View.OnClickListener m5083() {
        return this.f4475.m4990();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final boolean m5084() {
        return this.f4475.m4991();
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final void m5085() {
        this.f4476.m4892();
        this.f4475.m4992();
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final boolean m5086() {
        return this.f4475.m4993();
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final boolean m5087() {
        return this.f4475.m4994();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzv m5088() {
        return this.f4475.m4995();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m5089(boolean z) {
        this.f4475.m4996(z);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzaoa m5090() {
        return this.f4475.m4997();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5091(int i) {
        this.f4475.m4998(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5092(zzd zzd) {
        this.f4475.m4999(zzd);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5093(String str) {
        this.f4475.m5000(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5094(String str, zzt<? super zzanh> zzt) {
        this.f4475.m5001(str, zzt);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5095(boolean z) {
        this.f4475.m5002(z);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Activity m5096() {
        return this.f4475.m5003();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5097(boolean z) {
        this.f4475.m5004(z);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzns m5098() {
        return this.f4475.m5005();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5099(boolean z) {
        this.f4475.m5006(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzamg m5100() {
        return this.f4476;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5101(int i) {
        this.f4475.m5007(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5102(Context context) {
        this.f4475.m5008(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5103(zzc zzc) {
        this.f4475.m9722(zzc);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5104(zzd zzd) {
        this.f4475.m5009(zzd);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5105(zzaoa zzaoa) {
        this.f4475.m5010(zzaoa);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5106(zzapa zzapa) {
        this.f4475.m5011(zzapa);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5107(zzgs zzgs) {
        this.f4475.m12971(zzgs);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5108(zzoq zzoq) {
        this.f4475.m5012(zzoq);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5109(String str) {
        this.f4475.m5013(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5110(String str, zzt<? super zzanh> zzt) {
        this.f4475.m5014(str, zzt);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5111(boolean z) {
        this.f4475.m4932(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5112(boolean z, int i) {
        this.f4475.m9723(z, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5113(boolean z, int i, String str) {
        this.f4475.m9724(z, i, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5114(boolean z, int i, String str, String str2) {
        this.f4475.m9725(z, i, str, str2);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public final boolean m5115() {
        return this.f4475.m5015();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m5116() {
        this.f4475.m5016();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final Context m5117() {
        return this.f4475.m5017();
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final void m5118() {
        this.f4475.m5018();
    }
}
