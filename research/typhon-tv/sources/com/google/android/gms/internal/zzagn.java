package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagn extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8165;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8166;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagn(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8166 = context;
        this.f8165 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9586() {
        SharedPreferences sharedPreferences = this.f8166.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putInt("version_code", sharedPreferences.getInt("version_code", 0));
        if (this.f8165 != null) {
            this.f8165.m9605(bundle);
        }
    }
}
