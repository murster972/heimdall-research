package com.google.android.gms.internal;

import java.util.Collections;
import java.util.List;
import java.util.Map;

final class zzfhz extends zzfhy<FieldDescriptorType, Object> {
    zzfhz(int i) {
        super(i, (zzfhz) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12694() {
        if (!m12689()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= m12691()) {
                    break;
                }
                Map.Entry r2 = m12688(i2);
                if (((zzffs) r2.getKey()).m12519()) {
                    r2.setValue(Collections.unmodifiableList((List) r2.getValue()));
                }
                i = i2 + 1;
            }
            for (Map.Entry entry : m12690()) {
                if (((zzffs) entry.getKey()).m12519()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.m12693();
    }
}
