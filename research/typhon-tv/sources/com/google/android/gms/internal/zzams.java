package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import com.google.android.exoplayer2.util.MimeTypes;

@zzzv
@TargetApi(14)
public final class zzams implements AudioManager.OnAudioFocusChangeListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f4425 = 1.0f;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4426;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzamt f4427;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f4428;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4429;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AudioManager f4430;

    public zzams(Context context, zzamt zzamt) {
        this.f4430 = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        this.f4427 = zzamt;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final void m4942() {
        boolean z = true;
        boolean z2 = this.f4428 && !this.f4426 && this.f4425 > 0.0f;
        if (z2 && !this.f4429) {
            if (this.f4430 != null && !this.f4429) {
                if (this.f4430.requestAudioFocus(this, 3, 2) != 1) {
                    z = false;
                }
                this.f4429 = z;
            }
            this.f4427.m9704();
        } else if (!z2 && this.f4429) {
            if (this.f4430 != null && this.f4429) {
                if (this.f4430.abandonAudioFocus(this) != 0) {
                    z = false;
                }
                this.f4429 = z;
            }
            this.f4427.m9704();
        }
    }

    public final void onAudioFocusChange(int i) {
        this.f4429 = i > 0;
        this.f4427.m9704();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4943() {
        this.f4428 = true;
        m4942();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4944() {
        this.f4428 = false;
        m4942();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final float m4945() {
        float f = this.f4426 ? 0.0f : this.f4425;
        if (this.f4429) {
            return f;
        }
        return 0.0f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4946(float f) {
        this.f4425 = f;
        m4942();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4947(boolean z) {
        this.f4426 = z;
        m4942();
    }
}
