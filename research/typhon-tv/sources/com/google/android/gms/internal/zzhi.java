package com.google.android.gms.internal;

final class zzhi implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzhh f10694;

    zzhi(zzhh zzhh) {
        this.f10694 = zzhh;
    }

    public final void run() {
        synchronized (this.f10694.f10692) {
            if (!this.f10694.f10691 || !this.f10694.f10689) {
                zzagf.m4792("App is still foreground");
            } else {
                boolean unused = this.f10694.f10691 = false;
                zzagf.m4792("App went background");
                for (zzhj r0 : this.f10694.f10684) {
                    try {
                        r0.m12997(false);
                    } catch (Exception e) {
                        zzagf.m4793("OnForegroundStateChangedListener threw exception.", e);
                    }
                }
            }
        }
    }
}
