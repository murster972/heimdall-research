package com.google.android.gms.internal;

import android.util.Log;

@zzzv
public class zzakb {
    /* renamed from: 连任  reason: contains not printable characters */
    public static void m4791(String str) {
        if (m4798(5)) {
            Log.w("Ads", str);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4792(String str) {
        if (m4798(3)) {
            Log.d("Ads", str);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4793(String str, Throwable th) {
        if (m4798(6)) {
            Log.e("Ads", str, th);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static void m4794(String str) {
        if (m4798(4)) {
            Log.i("Ads", str);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m4795(String str) {
        if (m4798(6)) {
            Log.e("Ads", str);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m4796(String str, Throwable th) {
        if (m4798(5)) {
            Log.w("Ads", str, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4797(String str, Throwable th) {
        if (m4798(3)) {
            Log.d("Ads", str, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4798(int i) {
        return i >= 5 || Log.isLoggable("Ads", i);
    }
}
