package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzip extends zzeu implements zzio {
    zzip(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.cache.ICacheService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzii m13009(zzil zzil) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzil);
        Parcel r1 = m12300(1, v_);
        zzii zzii = (zzii) zzew.m12304(r1, zzii.CREATOR);
        r1.recycle();
        return zzii;
    }
}
