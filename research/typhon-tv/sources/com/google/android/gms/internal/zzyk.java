package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.internal.zzzb;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@zzzv
public abstract class zzyk<T extends zzzb> implements zzzb<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final HashMap<String, List<zzt<? super T>>> f5583 = new HashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6039(String str, zzt<? super T> zzt) {
        List list = this.f5583.get(str);
        if (list != null) {
            list.remove(zzt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6040(String str, zzt<? super T> zzt) {
        List list = this.f5583.get(str);
        if (list == null) {
            list = new CopyOnWriteArrayList();
            this.f5583.put(str, list);
        }
        list.add(zzt);
    }
}
