package com.google.android.gms.internal;

public enum zzdsa implements zzfga {
    UNKNOWN_FORMAT(0),
    UNCOMPRESSED(1),
    COMPRESSED(2),
    UNRECOGNIZED(-1);
    

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfgb<zzdsa> f10024 = null;
    private final int value;

    static {
        f10024 = new zzdsb();
    }

    private zzdsa(int i) {
        this.value = i;
    }

    public static zzdsa zzfn(int i) {
        switch (i) {
            case 0:
                return UNKNOWN_FORMAT;
            case 1:
                return UNCOMPRESSED;
            case 2:
                return COMPRESSED;
            default:
                return null;
        }
    }

    public final int zzhq() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
}
