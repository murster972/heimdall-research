package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.ads.internal.zzv;
import java.util.Map;

@zzzv
public interface zzamp extends zzaov, zzaoy {
    Context getContext();

    void setBackgroundColor(int i);

    void zza(String str, Map<String, ?> map);

    /* renamed from: ʻ  reason: contains not printable characters */
    void m4920();

    /* renamed from: ʼ  reason: contains not printable characters */
    String m4921();

    /* renamed from: ʽ  reason: contains not printable characters */
    zznt m4922();

    /* renamed from: ˑ  reason: contains not printable characters */
    zzakd m4923();

    /* renamed from: ٴ  reason: contains not printable characters */
    int m4924();

    /* renamed from: ᐧ  reason: contains not printable characters */
    int m4925();

    /* renamed from: 连任  reason: contains not printable characters */
    zzv m4926();

    /* renamed from: 靐  reason: contains not printable characters */
    zzaoa m4927();

    /* renamed from: 麤  reason: contains not printable characters */
    Activity m4928();

    /* renamed from: 齉  reason: contains not printable characters */
    zzns m4929();

    /* renamed from: 龘  reason: contains not printable characters */
    zzamg m4930();

    /* renamed from: 龘  reason: contains not printable characters */
    void m4931(zzaoa zzaoa);

    /* renamed from: 龘  reason: contains not printable characters */
    void m4932(boolean z);
}
