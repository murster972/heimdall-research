package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.api.internal.zzck;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

public final class zzcfk extends zzcdt {

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzcfd f9081;

    public zzcfk(Context context, Looper looper, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener, String str) {
        this(context, looper, connectionCallbacks, onConnectionFailedListener, str, zzr.m4205(context));
    }

    public zzcfk(Context context, Looper looper, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener, String str, zzr zzr) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, str, zzr);
        this.f9081 = new zzcfd(context, this.f9036);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m10401() {
        synchronized (this.f9081) {
            if (m9161()) {
                try {
                    this.f9081.m10380();
                    this.f9081.m10379();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.m9160();
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final LocationAvailability m10402() throws RemoteException {
        return this.f9081.m10377();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Location m10403() throws RemoteException {
        return this.f9081.m10381();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10404(zzck<LocationCallback> zzck, zzceu zzceu) throws RemoteException {
        this.f9081.m10378(zzck, zzceu);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10405(long j, PendingIntent pendingIntent) throws RemoteException {
        m9169();
        zzbq.m9120(pendingIntent);
        zzbq.m9117(j >= 0, "detectionIntervalMillis must be >= 0");
        ((zzcez) m9171()).m10355(j, true, pendingIntent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10406(PendingIntent pendingIntent) throws RemoteException {
        m9169();
        zzbq.m9120(pendingIntent);
        ((zzcez) m9171()).m10356(pendingIntent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10407(PendingIntent pendingIntent, zzceu zzceu) throws RemoteException {
        this.f9081.m10382(pendingIntent, zzceu);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10408(Location location) throws RemoteException {
        this.f9081.m10383(location);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10409(zzck<LocationListener> zzck, zzceu zzceu) throws RemoteException {
        this.f9081.m10384(zzck, zzceu);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10410(zzceu zzceu) throws RemoteException {
        this.f9081.m10385(zzceu);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10411(zzcfo zzcfo, zzci<LocationCallback> zzci, zzceu zzceu) throws RemoteException {
        synchronized (this.f9081) {
            this.f9081.m10386(zzcfo, zzci, zzceu);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10412(LocationRequest locationRequest, PendingIntent pendingIntent, zzceu zzceu) throws RemoteException {
        this.f9081.m10387(locationRequest, pendingIntent, zzceu);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10413(LocationRequest locationRequest, zzci<LocationListener> zzci, zzceu zzceu) throws RemoteException {
        synchronized (this.f9081) {
            this.f9081.m10388(locationRequest, zzci, zzceu);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10414(LocationSettingsRequest locationSettingsRequest, zzn<LocationSettingsResult> zzn, String str) throws RemoteException {
        boolean z = true;
        m9169();
        zzbq.m9117(locationSettingsRequest != null, "locationSettingsRequest can't be null nor empty.");
        if (zzn == null) {
            z = false;
        }
        zzbq.m9117(z, "listener can't be null.");
        ((zzcez) m9171()).m10361(locationSettingsRequest, (zzcfb) new zzcfn(zzn), str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10415(boolean z) throws RemoteException {
        this.f9081.m10389(z);
    }
}
