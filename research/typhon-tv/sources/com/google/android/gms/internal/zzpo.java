package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public final class zzpo extends zzeu implements zzpm {
    zzpo(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzpq> m13221() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        ArrayList r1 = zzew.m12301(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13222() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }
}
