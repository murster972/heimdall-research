package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import java.util.Map;

public abstract class zzbii extends zzev implements zzbih {
    public zzbii() {
        attachInterface(this, "com.google.android.gms.config.internal.IConfigCallbacks");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m10288((Status) zzew.m12304(parcel, Status.CREATOR), parcel.createByteArray());
                break;
            case 2:
                m10287((Status) zzew.m12304(parcel, Status.CREATOR), (Map) zzew.m12303(parcel));
                break;
            case 3:
                m10285((Status) zzew.m12304(parcel, Status.CREATOR));
                break;
            case 4:
                m10286((Status) zzew.m12304(parcel, Status.CREATOR), (zzbif) zzew.m12304(parcel, zzbif.CREATOR));
                break;
            default:
                return false;
        }
        return true;
    }
}
