package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzvi extends zzeu implements zzvg {
    zzvi(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IMediationResponseMetadata");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m13530() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }
}
