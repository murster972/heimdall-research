package com.google.android.gms.internal;

import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.api.internal.zzcm;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

final class zzcei extends zzcem {

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ Looper f9058;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ LocationCallback f9059;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationRequest f9060;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcei(zzceb zzceb, GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationCallback locationCallback, Looper looper) {
        super(googleApiClient);
        this.f9060 = locationRequest;
        this.f9059 = locationCallback;
        this.f9058 = looper;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10345(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10411(zzcfo.m5230(this.f9060), (zzci<LocationCallback>) zzcm.m8858(this.f9059, zzcgc.m10426(this.f9058), LocationCallback.class.getSimpleName()), (zzceu) new zzcen(this));
    }
}
