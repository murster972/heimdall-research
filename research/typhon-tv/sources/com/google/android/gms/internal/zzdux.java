package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Provider;
import javax.crypto.KeyAgreement;

public final class zzdux implements zzduv<KeyAgreement> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m12220(String str, Provider provider) throws GeneralSecurityException {
        return provider == null ? KeyAgreement.getInstance(str) : KeyAgreement.getInstance(str, provider);
    }
}
