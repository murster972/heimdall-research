package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrg;
import com.google.android.gms.internal.zzdss;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdrc extends zzffu<zzdrc, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdrc f9960;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdrc> f9961;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzdss f9962;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdrg f9963;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9964;

    public static final class zza extends zzffu.zza<zzdrc, zza> implements zzfhg {
        private zza() {
            super(zzdrc.f9960);
        }

        /* synthetic */ zza(zzdrd zzdrd) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11784(int i) {
            m12541();
            ((zzdrc) this.f10398).m11772(i);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11785(zzdrg zzdrg) {
            m12541();
            ((zzdrc) this.f10398).m11776(zzdrg);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11786(zzdss zzdss) {
            m12541();
            ((zzdrc) this.f10398).m11777(zzdss);
            return this;
        }
    }

    static {
        zzdrc zzdrc = new zzdrc();
        f9960 = zzdrc;
        zzdrc.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdrc.f10394.m12718();
    }

    private zzdrc() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zza m11770() {
        zzdrc zzdrc = f9960;
        zzffu.zza zza2 = (zzffu.zza) zzdrc.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdrc);
        return (zza) zza2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdrc m11771(zzfes zzfes) throws zzfge {
        return (zzdrc) zzffu.m12526(f9960, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11772(int i) {
        this.f9964 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11776(zzdrg zzdrg) {
        if (zzdrg == null) {
            throw new NullPointerException();
        }
        this.f9963 = zzdrg;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11777(zzdss zzdss) {
        if (zzdss == null) {
            throw new NullPointerException();
        }
        this.f9962 = zzdss;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdrg m11778() {
        return this.f9963 == null ? zzdrg.m11794() : this.f9963;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11779() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9964 != 0) {
            i2 = zzffg.m5245(1, this.f9964) + 0;
        }
        if (this.f9963 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f9963 == null ? zzdrg.m11794() : this.f9963);
        }
        if (this.f9962 != null) {
            i2 += zzffg.m5262(3, (zzfhe) this.f9962 == null ? zzdss.m11972() : this.f9962);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdss m11780() {
        return this.f9962 == null ? zzdss.m11972() : this.f9962;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11781() {
        return this.f9964;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11782(int i, Object obj, Object obj2) {
        zzdss.zza zza2;
        zzdrg.zza zza3;
        boolean z = true;
        switch (zzdrd.f9965[i - 1]) {
            case 1:
                return new zzdrc();
            case 2:
                return f9960;
            case 3:
                return null;
            case 4:
                return new zza((zzdrd) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdrc zzdrc = (zzdrc) obj2;
                boolean z2 = this.f9964 != 0;
                int i2 = this.f9964;
                if (zzdrc.f9964 == 0) {
                    z = false;
                }
                this.f9964 = zzh.m12569(z2, i2, z, zzdrc.f9964);
                this.f9963 = (zzdrg) zzh.m12572(this.f9963, zzdrc.f9963);
                this.f9962 = (zzdss) zzh.m12572(this.f9962, zzdrc.f9962);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f9964 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f9963 != null) {
                                        zzdrg zzdrg = this.f9963;
                                        zzffu.zza zza4 = (zzffu.zza) zzdrg.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza4.m12545(zzdrg);
                                        zza3 = (zzdrg.zza) zza4;
                                    } else {
                                        zza3 = null;
                                    }
                                    this.f9963 = (zzdrg) zzffb.m12416(zzdrg.m11794(), zzffm);
                                    if (zza3 == null) {
                                        break;
                                    } else {
                                        zza3.m12545(this.f9963);
                                        this.f9963 = (zzdrg) zza3.m12543();
                                        break;
                                    }
                                case 26:
                                    if (this.f9962 != null) {
                                        zzdss zzdss = this.f9962;
                                        zzffu.zza zza5 = (zzffu.zza) zzdss.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza5.m12545(zzdss);
                                        zza2 = (zzdss.zza) zza5;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f9962 = (zzdss) zzffb.m12416(zzdss.m11972(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f9962);
                                        this.f9962 = (zzdss) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f9961 == null) {
                    synchronized (zzdrc.class) {
                        if (f9961 == null) {
                            f9961 = new zzffu.zzb(f9960);
                        }
                    }
                }
                return f9961;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f9960;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11783(zzffg zzffg) throws IOException {
        if (this.f9964 != 0) {
            zzffg.m5281(1, this.f9964);
        }
        if (this.f9963 != null) {
            zzffg.m5289(2, (zzfhe) this.f9963 == null ? zzdrg.m11794() : this.f9963);
        }
        if (this.f9962 != null) {
            zzffg.m5289(3, (zzfhe) this.f9962 == null ? zzdss.m11972() : this.f9962);
        }
        this.f10394.m12719(zzffg);
    }
}
