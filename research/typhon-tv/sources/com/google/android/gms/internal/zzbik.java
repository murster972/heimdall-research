package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzbik extends zzeu implements zzbij {
    zzbik(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.config.internal.IConfigService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10290(zzbih zzbih, zzbid zzbid) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzbih);
        zzew.m12306(v_, (Parcelable) zzbid);
        m12298(8, v_);
    }
}
