package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.Pinkamena;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

@zzzv
public final class zzlx {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzks f4858;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f4859;

    /* renamed from: ʽ  reason: contains not printable characters */
    private AppEventListener f4860;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4861;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f4862;

    /* renamed from: ˈ  reason: contains not printable characters */
    private RewardedVideoAdListener f4863;

    /* renamed from: ˑ  reason: contains not printable characters */
    private PublisherInterstitialAd f4864;

    /* renamed from: ٴ  reason: contains not printable characters */
    private OnCustomRenderedAdLoadedListener f4865;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Correlator f4866;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzje f4867;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f4868;

    /* renamed from: 麤  reason: contains not printable characters */
    private AdListener f4869;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzjm f4870;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzuw f4871;

    public zzlx(Context context) {
        this(context, zzjm.f4788, (PublisherInterstitialAd) null);
    }

    public zzlx(Context context, PublisherInterstitialAd publisherInterstitialAd) {
        this(context, zzjm.f4788, publisherInterstitialAd);
    }

    private zzlx(Context context, zzjm zzjm, PublisherInterstitialAd publisherInterstitialAd) {
        this.f4871 = new zzuw();
        this.f4868 = context;
        this.f4870 = zzjm;
        this.f4864 = publisherInterstitialAd;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m5540(String str) {
        if (this.f4858 == null) {
            throw new IllegalStateException(new StringBuilder(String.valueOf(str).length() + 63).append("The ad unit ID must be set on InterstitialAd before ").append(str).append(" is called.").toString());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m5541() {
        try {
            if (this.f4858 == null) {
                return false;
            }
            return this.f4858.isLoading();
        } catch (RemoteException e) {
            zzakb.m4796("Failed to check if ad is loading.", e);
            return false;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5542() {
        try {
            if (this.f4858 != null) {
                return this.f4858.zzcp();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to get the mediation adapter class name.", e);
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m5543() {
        try {
            m5540("show");
            zzks zzks = this.f4858;
            Pinkamena.DianePie();
        } catch (RemoteException e) {
            zzakb.m4796("Failed to show interstitial.", e);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m5544() {
        try {
            if (this.f4858 == null) {
                return false;
            }
            return this.f4858.isReady();
        } catch (RemoteException e) {
            zzakb.m4796("Failed to check if ad is ready.", e);
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5545() {
        return this.f4859;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5546(boolean z) {
        try {
            this.f4862 = z;
            if (this.f4858 != null) {
                this.f4858.setImmersiveMode(z);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set immersive mode", e);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final OnCustomRenderedAdLoadedListener m5547() {
        return this.f4865;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final AppEventListener m5548() {
        return this.f4860;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final AdListener m5549() {
        return this.f4869;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5550(AdListener adListener) {
        try {
            this.f4869 = adListener;
            if (this.f4858 != null) {
                this.f4858.zza((zzkh) adListener != null ? new zzjg(adListener) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the AdListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5551(Correlator correlator) {
        this.f4866 = correlator;
        try {
            if (this.f4858 != null) {
                this.f4858.zza((zzld) this.f4866 == null ? null : this.f4866.zzbh());
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set correlator.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5552(AppEventListener appEventListener) {
        try {
            this.f4860 = appEventListener;
            if (this.f4858 != null) {
                this.f4858.zza((zzkx) appEventListener != null ? new zzjp(appEventListener) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the AppEventListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5553(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        try {
            this.f4865 = onCustomRenderedAdLoadedListener;
            if (this.f4858 != null) {
                this.f4858.zza((zzoa) onCustomRenderedAdLoadedListener != null ? new zzod(onCustomRenderedAdLoadedListener) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the OnCustomRenderedAdLoadedListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5554(RewardedVideoAdListener rewardedVideoAdListener) {
        try {
            this.f4863 = rewardedVideoAdListener;
            if (this.f4858 != null) {
                this.f4858.zza((zzadp) rewardedVideoAdListener != null ? new zzadu(rewardedVideoAdListener) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the AdListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5555(zzje zzje) {
        try {
            this.f4867 = zzje;
            if (this.f4858 != null) {
                this.f4858.zza((zzke) zzje != null ? new zzjf(zzje) : null);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to set the AdClickListener.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5556(zzlt zzlt) {
        try {
            if (this.f4858 == null) {
                if (this.f4859 == null) {
                    m5540("loadAd");
                }
                zzjn r3 = this.f4861 ? zzjn.m5456() : new zzjn();
                zzjr r1 = zzkb.m5484();
                Context context = this.f4868;
                this.f4858 = (zzks) zzjr.m5474(context, false, new zzju(r1, context, r3, this.f4859, this.f4871));
                if (this.f4869 != null) {
                    this.f4858.zza((zzkh) new zzjg(this.f4869));
                }
                if (this.f4867 != null) {
                    this.f4858.zza((zzke) new zzjf(this.f4867));
                }
                if (this.f4860 != null) {
                    this.f4858.zza((zzkx) new zzjp(this.f4860));
                }
                if (this.f4865 != null) {
                    this.f4858.zza((zzoa) new zzod(this.f4865));
                }
                if (this.f4866 != null) {
                    this.f4858.zza((zzld) this.f4866.zzbh());
                }
                if (this.f4863 != null) {
                    this.f4858.zza((zzadp) new zzadu(this.f4863));
                }
                this.f4858.setImmersiveMode(this.f4862);
            }
            if (this.f4858.zzb(zzjm.m5452(this.f4868, zzlt))) {
                this.f4871.m5917(zzlt.m5498());
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to load ad.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5557(String str) {
        if (this.f4859 != null) {
            throw new IllegalStateException("The ad unit ID can only be set once on InterstitialAd.");
        }
        this.f4859 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5558(boolean z) {
        this.f4861 = true;
    }
}
