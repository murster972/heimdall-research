package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.gms.common.util.zzq;
import java.io.InputStream;

final class zzyq implements zzajb<zzoi> {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzym f10987;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ double f10988;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ String f10989;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ boolean f10990;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ boolean f10991;

    zzyq(zzym zzym, boolean z, double d, boolean z2, String str) {
        this.f10987 = zzym;
        this.f10991 = z;
        this.f10988 = d;
        this.f10990 = z2;
        this.f10989 = str;
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    /* renamed from: 靐  reason: contains not printable characters */
    public final zzoi m13642(InputStream inputStream) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDensity = (int) (160.0d * this.f10988);
        if (!this.f10990) {
            options.inPreferredConfig = Bitmap.Config.RGB_565;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        try {
            bitmap = BitmapFactory.decodeStream(inputStream, (Rect) null, options);
        } catch (Exception e) {
            zzagf.m4793("Error grabbing image.", e);
            bitmap = null;
        }
        if (bitmap == null) {
            this.f10987.m6060(2, this.f10991);
            return null;
        }
        long uptimeMillis2 = SystemClock.uptimeMillis();
        if (zzq.m9268() && zzagf.m4528()) {
            int width = bitmap.getWidth();
            zzagf.m4527(new StringBuilder(108).append("Decoded image w: ").append(width).append(" h:").append(bitmap.getHeight()).append(" bytes: ").append(bitmap.getAllocationByteCount()).append(" time: ").append(uptimeMillis2 - uptimeMillis).append(" on ui thread: ").append(Looper.getMainLooper().getThread() == Thread.currentThread()).toString());
        }
        return new zzoi(new BitmapDrawable(Resources.getSystem(), bitmap), Uri.parse(this.f10989), this.f10988);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13641() {
        this.f10987.m6060(2, this.f10991);
        return null;
    }
}
