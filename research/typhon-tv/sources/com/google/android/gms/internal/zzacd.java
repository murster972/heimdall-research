package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzbs;

final class zzacd implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzabe f8067;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzacb f8068;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzaat f8069;

    zzacd(zzacb zzacb, zzaat zzaat, zzabe zzabe) {
        this.f8068 = zzacb;
        this.f8069 = zzaat;
        this.f8067 = zzabe;
    }

    public final void run() {
        zzaax zzaax;
        try {
            zzaax = this.f8068.m4282(this.f8069);
        } catch (Exception e) {
            zzbs.zzem().m4505((Throwable) e, "AdRequestServiceImpl.loadAdAsync");
            zzagf.m4796("Could not fetch ad response due to an Exception.", e);
            zzaax = null;
        }
        if (zzaax == null) {
            zzaax = new zzaax(0);
        }
        try {
            this.f8067.m9448(zzaax);
        } catch (RemoteException e2) {
            zzagf.m4796("Fail to forward ad response.", e2);
        }
    }
}
