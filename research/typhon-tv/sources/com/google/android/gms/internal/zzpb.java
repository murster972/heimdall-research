package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final class zzpb implements zzt<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzoy f10825;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzzb f10826;

    zzpb(zzoy zzoy, zzzb zzzb) {
        this.f10825 = zzoy;
        this.f10826 = zzzb;
    }

    public final void zza(Object obj, Map<String, String> map) {
        zzanh zzanh = (zzanh) this.f10825.f10820.get();
        if (zzanh == null) {
            this.f10826.m6072("/showOverlay", this);
        } else if (zzanh == null) {
            throw null;
        } else {
            ((View) zzanh).setVisibility(0);
        }
    }
}
