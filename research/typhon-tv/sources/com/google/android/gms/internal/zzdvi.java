package com.google.android.gms.internal;

import java.security.SecureRandom;

public final class zzdvi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final SecureRandom f10231 = new SecureRandom();

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m12235(int i) {
        byte[] bArr = new byte[i];
        f10231.nextBytes(bArr);
        return bArr;
    }
}
