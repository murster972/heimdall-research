package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

@zzzv
public final class zzrm extends zzqx {

    /* renamed from: 龘  reason: contains not printable characters */
    private final NativeCustomTemplateAd.OnCustomClickListener f5335;

    public zzrm(NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
        this.f5335 = onCustomClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5804(zzqm zzqm, String str) {
        this.f5335.onCustomClick(zzqp.m5796(zzqm), str);
    }
}
