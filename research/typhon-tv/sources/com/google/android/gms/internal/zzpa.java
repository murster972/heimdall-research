package com.google.android.gms.internal;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class zzpa implements zzanm {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzoz f10823;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Map f10824;

    zzpa(zzoz zzoz, Map map) {
        this.f10823 = zzoz;
        this.f10824 = map;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13213(zzanh zzanh, boolean z) {
        String unused = this.f10823.f10821.f10819 = (String) this.f10824.get("id");
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("messageType", (Object) "htmlLoaded");
            jSONObject.put("id", (Object) this.f10823.f10821.f10819);
            this.f10823.f10822.m6077("sendMessageToNativeJs", jSONObject);
        } catch (JSONException e) {
            zzagf.m4793("Unable to dispatch sendMessageToNativeJs event", e);
        }
    }
}
