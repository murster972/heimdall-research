package com.google.android.gms.internal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.mopub.mobileads.resource.DrawableTyphoonApp;

final class zzain implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8227;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ boolean f8228;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ boolean f8229;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ Context f8230;

    zzain(zzaim zzaim, Context context, String str, boolean z, boolean z2) {
        this.f8230 = context;
        this.f8227 = str;
        this.f8229 = z;
        this.f8228 = z2;
    }

    public final void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f8230);
        builder.setMessage(this.f8227);
        if (this.f8229) {
            builder.setTitle("Error");
        } else {
            builder.setTitle("Info");
        }
        if (this.f8228) {
            builder.setNeutralButton("Dismiss", (DialogInterface.OnClickListener) null);
        } else {
            builder.setPositiveButton(DrawableTyphoonApp.CtaButton.DEFAULT_CTA_TEXT, new zzaio(this));
            builder.setNegativeButton("Dismiss", (DialogInterface.OnClickListener) null);
        }
        AlertDialog create = builder.create();
    }
}
