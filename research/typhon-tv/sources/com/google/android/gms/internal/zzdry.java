package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdry extends zzffu<zzdry, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdry f10015;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdry> f10016;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzfes f10017 = zzfes.zzpfg;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10018;

    public static final class zza extends zzffu.zza<zzdry, zza> implements zzfhg {
        private zza() {
            super(zzdry.f10015);
        }

        /* synthetic */ zza(zzdrz zzdrz) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11899(int i) {
            m12541();
            ((zzdry) this.f10398).m11891(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11900(zzfes zzfes) {
            m12541();
            ((zzdry) this.f10398).m11888(zzfes);
            return this;
        }
    }

    static {
        zzdry zzdry = new zzdry();
        f10015 = zzdry;
        zzdry.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdry.f10394.m12718();
    }

    private zzdry() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11888(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10017 = zzfes;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zza m11889() {
        zzdry zzdry = f10015;
        zzffu.zza zza2 = (zzffu.zza) zzdry.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdry);
        return (zza) zza2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdry m11890(zzfes zzfes) throws zzfge {
        return (zzdry) zzffu.m12526(f10015, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11891(int i) {
        this.f10018 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfes m11894() {
        return this.f10017;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11895() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10018 != 0) {
            i2 = zzffg.m5245(1, this.f10018) + 0;
        }
        if (!this.f10017.isEmpty()) {
            i2 += zzffg.m5261(2, this.f10017);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11896() {
        return this.f10018;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 171 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m11897(int r7, java.lang.Object r8, java.lang.Object r9) {
        /*
            r6 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdrz.f10019
            int r4 = r7 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x005b;
                case 7: goto L_0x00a8;
                case 8: goto L_0x00ac;
                case 9: goto L_0x00c8;
                case 10: goto L_0x00ce;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdry r6 = new com.google.android.gms.internal.zzdry
            r6.<init>()
        L_0x0017:
            return r6
        L_0x0018:
            com.google.android.gms.internal.zzdry r6 = f10015
            goto L_0x0017
        L_0x001b:
            r6 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdry$zza r6 = new com.google.android.gms.internal.zzdry$zza
            r6.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r8 = (com.google.android.gms.internal.zzffu.zzh) r8
            com.google.android.gms.internal.zzdry r9 = (com.google.android.gms.internal.zzdry) r9
            int r0 = r6.f10018
            if (r0 == 0) goto L_0x0053
            r0 = r1
        L_0x002c:
            int r4 = r6.f10018
            int r3 = r9.f10018
            if (r3 == 0) goto L_0x0055
            r3 = r1
        L_0x0033:
            int r5 = r9.f10018
            int r0 = r8.m12569((boolean) r0, (int) r4, (boolean) r3, (int) r5)
            r6.f10018 = r0
            com.google.android.gms.internal.zzfes r0 = r6.f10017
            com.google.android.gms.internal.zzfes r3 = com.google.android.gms.internal.zzfes.zzpfg
            if (r0 == r3) goto L_0x0057
            r0 = r1
        L_0x0042:
            com.google.android.gms.internal.zzfes r3 = r6.f10017
            com.google.android.gms.internal.zzfes r4 = r9.f10017
            com.google.android.gms.internal.zzfes r5 = com.google.android.gms.internal.zzfes.zzpfg
            if (r4 == r5) goto L_0x0059
        L_0x004a:
            com.google.android.gms.internal.zzfes r2 = r9.f10017
            com.google.android.gms.internal.zzfes r0 = r8.m12570((boolean) r0, (com.google.android.gms.internal.zzfes) r3, (boolean) r1, (com.google.android.gms.internal.zzfes) r2)
            r6.f10017 = r0
            goto L_0x0017
        L_0x0053:
            r0 = r2
            goto L_0x002c
        L_0x0055:
            r3 = r2
            goto L_0x0033
        L_0x0057:
            r0 = r2
            goto L_0x0042
        L_0x0059:
            r1 = r2
            goto L_0x004a
        L_0x005b:
            com.google.android.gms.internal.zzffb r8 = (com.google.android.gms.internal.zzffb) r8
            com.google.android.gms.internal.zzffm r9 = (com.google.android.gms.internal.zzffm) r9
            if (r9 != 0) goto L_0x0068
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0067:
            r2 = r1
        L_0x0068:
            if (r2 != 0) goto L_0x00a8
            int r0 = r8.m12415()     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
            switch(r0) {
                case 0: goto L_0x0067;
                case 8: goto L_0x0079;
                case 18: goto L_0x008d;
                default: goto L_0x0071;
            }     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
        L_0x0071:
            boolean r0 = r6.m12537((int) r0, (com.google.android.gms.internal.zzffb) r8)     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
            if (r0 != 0) goto L_0x0068
            r2 = r1
            goto L_0x0068
        L_0x0079:
            int r0 = r8.m12402()     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
            r6.f10018 = r0     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
            goto L_0x0068
        L_0x0080:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r6)     // Catch:{ all -> 0x008b }
            r1.<init>(r0)     // Catch:{ all -> 0x008b }
            throw r1     // Catch:{ all -> 0x008b }
        L_0x008b:
            r0 = move-exception
            throw r0
        L_0x008d:
            com.google.android.gms.internal.zzfes r0 = r8.m12401()     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
            r6.f10017 = r0     // Catch:{ zzfge -> 0x0080, IOException -> 0x0094 }
            goto L_0x0068
        L_0x0094:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x008b }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x008b }
            r2.<init>(r0)     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r6)     // Catch:{ all -> 0x008b }
            r1.<init>(r0)     // Catch:{ all -> 0x008b }
            throw r1     // Catch:{ all -> 0x008b }
        L_0x00a8:
            com.google.android.gms.internal.zzdry r6 = f10015
            goto L_0x0017
        L_0x00ac:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdry> r0 = f10016
            if (r0 != 0) goto L_0x00c1
            java.lang.Class<com.google.android.gms.internal.zzdry> r1 = com.google.android.gms.internal.zzdry.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdry> r0 = f10016     // Catch:{ all -> 0x00c5 }
            if (r0 != 0) goto L_0x00c0
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00c5 }
            com.google.android.gms.internal.zzdry r2 = f10015     // Catch:{ all -> 0x00c5 }
            r0.<init>(r2)     // Catch:{ all -> 0x00c5 }
            f10016 = r0     // Catch:{ all -> 0x00c5 }
        L_0x00c0:
            monitor-exit(r1)     // Catch:{ all -> 0x00c5 }
        L_0x00c1:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdry> r6 = f10016
            goto L_0x0017
        L_0x00c5:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c5 }
            throw r0
        L_0x00c8:
            java.lang.Byte r6 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00ce:
            r6 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdry.m11897(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11898(zzffg zzffg) throws IOException {
        if (this.f10018 != 0) {
            zzffg.m5281(1, this.f10018);
        }
        if (!this.f10017.isEmpty()) {
            zzffg.m5288(2, this.f10017);
        }
        this.f10394.m12719(zzffg);
    }
}
