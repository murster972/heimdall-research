package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import com.google.android.gms.cast.framework.media.ImageHints;

public final class zzazn implements zzazr {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Bitmap f8503;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f8504;

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzazo f8505;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzazs f8506;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ImageHints f8507;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzazp f8508;

    /* renamed from: 齉  reason: contains not printable characters */
    private Uri f8509;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f8510;

    public zzazn(Context context) {
        this(context, new ImageHints(-1, 0, 0));
    }

    public zzazn(Context context, ImageHints imageHints) {
        this.f8510 = context;
        this.f8507 = imageHints;
        this.f8506 = new zzazs();
        m9833();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m9833() {
        if (this.f8508 != null) {
            this.f8508.cancel(true);
            this.f8508 = null;
        }
        this.f8509 = null;
        this.f8503 = null;
        this.f8504 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9834() {
        m9833();
        this.f8505 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9835(Bitmap bitmap) {
        this.f8503 = bitmap;
        this.f8504 = true;
        if (this.f8505 != null) {
            this.f8505.m9838(this.f8503);
        }
        this.f8508 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9836(zzazo zzazo) {
        this.f8505 = zzazo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9837(Uri uri) {
        if (uri == null) {
            m9833();
            return true;
        } else if (uri.equals(this.f8509)) {
            return this.f8504;
        } else {
            m9833();
            this.f8509 = uri;
            if (this.f8507.m8144() == 0 || this.f8507.m8145() == 0) {
                this.f8508 = new zzazp(this.f8510, this);
            } else {
                this.f8508 = new zzazp(this.f8510, this.f8507.m8144(), this.f8507.m8145(), false, this);
            }
            this.f8508.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Uri[]{this.f8509});
            return false;
        }
    }
}
