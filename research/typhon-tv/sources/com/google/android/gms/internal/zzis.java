package com.google.android.gms.internal;

import com.google.android.gms.internal.zziu;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzis {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzjc f4746;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f4747;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzix f4748;

    private zzis() {
        this.f4747 = false;
        this.f4748 = new zzix();
        this.f4746 = new zzjc();
        m5430();
    }

    public zzis(zzix zzix, boolean z) {
        this.f4748 = zzix;
        this.f4747 = z;
        this.f4746 = new zzjc();
        m5430();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final synchronized void m5430() {
        this.f4746.f10748 = new zzja();
        this.f4746.f10749 = new zzjb();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static long[] m5431() {
        List<String> r0 = zznh.m5597();
        ArrayList arrayList = new ArrayList();
        for (String split : r0) {
            for (String valueOf : split.split(",")) {
                try {
                    arrayList.add(Long.valueOf(valueOf));
                } catch (NumberFormatException e) {
                    zzagf.m4527("Experiment ID is not a number");
                }
            }
        }
        long[] jArr = new long[arrayList.size()];
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            jArr[i2] = ((Long) obj).longValue();
            i2++;
        }
        return jArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzis m5432() {
        return new zzis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m5433(zzit zzit) {
        if (this.f4747) {
            zzit.m13010(this.f4746);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m5434(zziu.zza.zzb zzb) {
        if (this.f4747) {
            this.f4746.f10747 = m5431();
            this.f4748.m5435(zzfjs.m12871((zzfjs) this.f4746)).m13015(zzb.zzhq()).m13017();
            String valueOf = String.valueOf(Integer.toString(zzb.zzhq(), 10));
            zzagf.m4527(valueOf.length() != 0 ? "Logging Event with event code : ".concat(valueOf) : new String("Logging Event with event code : "));
        }
    }
}
