package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

public final class zzbbz extends GoogleApi<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Api.zzf<zzbcd> f8618 = new Api.zzf<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Api<Object> f8619 = new Api<>("CastApi.API", f8620, f8618);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Api.zza<zzbcd, Object> f8620 = new zzbca();

    public zzbbz(Context context) {
        super(context, f8619, null, GoogleApi.zza.f7431);
    }
}
