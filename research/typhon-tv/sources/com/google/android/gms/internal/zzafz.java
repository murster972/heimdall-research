package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzafz {

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f4200;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f4201;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzaft f4202;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f4203;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4204;

    private zzafz(zzaft zzaft, String str) {
        this.f4204 = new Object();
        this.f4202 = zzaft;
        this.f4200 = str;
    }

    public zzafz(String str) {
        this(zzbs.zzem(), str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m4516() {
        Bundle bundle;
        synchronized (this.f4204) {
            bundle = new Bundle();
            bundle.putInt("pmnli", this.f4201);
            bundle.putInt("pmnll", this.f4203);
        }
        return bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4517(int i, int i2) {
        synchronized (this.f4204) {
            this.f4201 = i;
            this.f4203 = i2;
            this.f4202.m4504(this.f4200, this);
        }
    }
}
