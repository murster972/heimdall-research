package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfka extends zzfjm<zzfka> {

    /* renamed from: 靐  reason: contains not printable characters */
    private byte[] f10589 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f10590 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private byte[] f10591 = null;

    public zzfka() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12898() {
        int r0 = super.m12841();
        if (this.f10591 != null) {
            r0 += zzfjk.m12809(1, this.f10591);
        }
        if (this.f10589 != null) {
            r0 += zzfjk.m12809(2, this.f10589);
        }
        return this.f10590 != null ? r0 + zzfjk.m12809(3, this.f10590) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12899(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10591 = zzfjj.m12784();
                    continue;
                case 18:
                    this.f10589 = zzfjj.m12784();
                    continue;
                case 26:
                    this.f10590 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12900(zzfjk zzfjk) throws IOException {
        if (this.f10591 != null) {
            zzfjk.m12837(1, this.f10591);
        }
        if (this.f10589 != null) {
            zzfjk.m12837(2, this.f10589);
        }
        if (this.f10590 != null) {
            zzfjk.m12837(3, this.f10590);
        }
        super.m12842(zzfjk);
    }
}
