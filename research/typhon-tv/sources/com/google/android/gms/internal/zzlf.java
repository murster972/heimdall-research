package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzlf extends zzeu implements zzld {
    zzlf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m13079() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        long readLong = r0.readLong();
        r0.recycle();
        return readLong;
    }
}
