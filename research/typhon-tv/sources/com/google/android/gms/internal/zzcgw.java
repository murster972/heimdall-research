package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;

final class zzcgw {

    /* renamed from: ʻ  reason: contains not printable characters */
    final long f9187;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Long f9188;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Long f9189;

    /* renamed from: ˑ  reason: contains not printable characters */
    final Boolean f9190;

    /* renamed from: 连任  reason: contains not printable characters */
    final long f9191;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f9192;

    /* renamed from: 麤  reason: contains not printable characters */
    final long f9193;

    /* renamed from: 齉  reason: contains not printable characters */
    final long f9194;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f9195;

    zzcgw(String str, String str2, long j, long j2, long j3, long j4, Long l, Long l2, Boolean bool) {
        zzbq.m9122(str);
        zzbq.m9122(str2);
        zzbq.m9116(j >= 0);
        zzbq.m9116(j2 >= 0);
        zzbq.m9116(j4 >= 0);
        this.f9195 = str;
        this.f9192 = str2;
        this.f9194 = j;
        this.f9193 = j2;
        this.f9191 = j3;
        this.f9187 = j4;
        this.f9188 = l;
        this.f9189 = l2;
        this.f9190 = bool;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final zzcgw m10655(long j) {
        return new zzcgw(this.f9195, this.f9192, this.f9194, this.f9193, this.f9191, j, this.f9188, this.f9189, this.f9190);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcgw m10656() {
        return new zzcgw(this.f9195, this.f9192, this.f9194 + 1, this.f9193 + 1, this.f9191, this.f9187, this.f9188, this.f9189, this.f9190);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcgw m10657(long j) {
        return new zzcgw(this.f9195, this.f9192, this.f9194, this.f9193, j, this.f9187, this.f9188, this.f9189, this.f9190);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcgw m10658(Long l, Long l2, Boolean bool) {
        return new zzcgw(this.f9195, this.f9192, this.f9194, this.f9193, this.f9191, this.f9187, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
    }
}
