package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

final class zzfht implements Iterator<zzfey> {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzfey f10456;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Stack<zzfhq> f10457;

    private zzfht(zzfes zzfes) {
        this.f10457 = new Stack<>();
        this.f10456 = m12665(zzfes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzfey m12664() {
        while (!this.f10457.isEmpty()) {
            zzfey r0 = m12665(this.f10457.pop().zzpjj);
            if (!r0.isEmpty()) {
                return r0;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzfey m12665(zzfes zzfes) {
        zzfes zzfes2 = zzfes;
        while (zzfes2 instanceof zzfhq) {
            zzfhq zzfhq = (zzfhq) zzfes2;
            this.f10457.push(zzfhq);
            zzfes2 = zzfhq.zzpji;
        }
        return (zzfey) zzfes2;
    }

    public final boolean hasNext() {
        return this.f10456 != null;
    }

    public final /* synthetic */ Object next() {
        if (this.f10456 == null) {
            throw new NoSuchElementException();
        }
        zzfey zzfey = this.f10456;
        this.f10456 = m12664();
        return zzfey;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
