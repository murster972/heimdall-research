package com.google.android.gms.internal;

import android.text.format.DateUtils;
import android.widget.TextView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbay extends UIController implements RemoteMediaClient.ProgressListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f8600;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f8601 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f8602;

    /* renamed from: 龘  reason: contains not printable characters */
    private final TextView f8603;

    public zzbay(TextView textView, long j, String str) {
        this.f8603 = textView;
        this.f8600 = j;
        this.f8602 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9944() {
        this.f8603.setText(this.f8602);
        if (m8226() != null) {
            m8226().m4166((RemoteMediaClient.ProgressListener) this);
        }
        super.m8223();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9945(long j) {
        this.f8603.setText(DateUtils.formatElapsedTime(j / 1000));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9946(long j, long j2) {
        if (this.f8601) {
            this.f8603.setText(DateUtils.formatElapsedTime(j / 1000));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9947(CastSession castSession) {
        super.m8227(castSession);
        RemoteMediaClient r0 = m8226();
        if (r0 != null) {
            r0.m4168((RemoteMediaClient.ProgressListener) this, this.f8600);
            if (r0.m4143()) {
                this.f8603.setText(DateUtils.formatElapsedTime(r0.m4148() / 1000));
            } else {
                this.f8603.setText(this.f8602);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9948(boolean z) {
        this.f8601 = z;
    }
}
