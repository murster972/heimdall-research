package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzqn extends zzev implements zzqm {
    public zzqn() {
        attachInterface(this, "com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzqm m13329(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
        return queryLocalInterface instanceof zzqm ? (zzqm) queryLocalInterface : new zzqo(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                String r1 = m13326(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(r1);
                return true;
            case 2:
                zzpq r12 = m13322(parcel.readString());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r12);
                return true;
            case 3:
                List<String> r13 = m13327();
                parcel2.writeNoException();
                parcel2.writeStringList(r13);
                return true;
            case 4:
                String r14 = m13319();
                parcel2.writeNoException();
                parcel2.writeString(r14);
                return true;
            case 5:
                m13325(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                m13323();
                parcel2.writeNoException();
                return true;
            case 7:
                zzll r15 = m13324();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r15);
                return true;
            case 8:
                m13318();
                parcel2.writeNoException();
                return true;
            case 9:
                IObjectWrapper r16 = m13320();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r16);
                return true;
            case 10:
                boolean r17 = m13328(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzew.m12307(parcel2, r17);
                return true;
            case 11:
                IObjectWrapper r18 = m13321();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r18);
                return true;
            default:
                return false;
        }
    }
}
