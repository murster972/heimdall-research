package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.PopupWindow;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.AdActivity;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzn;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.mopub.mobileads.VastIconXmlManager;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzahn {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Handler f4212 = new zzagg(Looper.getMainLooper());

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f4213 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Pattern f4214;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Pattern f4215;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4216 = false;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f4217 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public String f4218;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f4219 = true;

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Bitmap m4558(View view) {
        Bitmap bitmap = null;
        try {
            boolean isDrawingCacheEnabled = view.isDrawingCacheEnabled();
            view.setDrawingCacheEnabled(true);
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                bitmap = Bitmap.createBitmap(drawingCache);
            }
            view.setDrawingCacheEnabled(isDrawingCacheEnabled);
        } catch (RuntimeException e) {
            zzagf.m4793("Fail to capture the web view", e);
        }
        return bitmap;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzmt m4559(Context context) {
        return new zzmt(context);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String m4560() {
        StringBuffer stringBuffer = new StringBuffer(256);
        stringBuffer.append("Mozilla/5.0 (Linux; U; Android");
        if (Build.VERSION.RELEASE != null) {
            stringBuffer.append(StringUtils.SPACE).append(Build.VERSION.RELEASE);
        }
        stringBuffer.append("; ").append(Locale.getDefault());
        if (Build.DEVICE != null) {
            stringBuffer.append("; ").append(Build.DEVICE);
            if (Build.DISPLAY != null) {
                stringBuffer.append(" Build/").append(Build.DISPLAY);
            }
        }
        stringBuffer.append(") AppleWebKit/533 Version/4.0 Safari/533");
        return stringBuffer.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m4561(String str) {
        return TextUtils.isEmpty(str) ? "" : str.split(";")[0].trim();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m4562(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String[] split = str.split(";");
        if (split.length == 1) {
            return "";
        }
        for (int i = 1; i < split.length; i++) {
            if (split[i].trim().startsWith("charset")) {
                String[] split2 = split[i].trim().split("=");
                if (split2.length > 1) {
                    return split2[1].trim();
                }
            }
        }
        return "";
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m4563(Context context) {
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
            if (activityManager == null || keyguardManager == null) {
                return false;
            }
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            Iterator<ActivityManager.RunningAppProcessInfo> it2 = runningAppProcesses.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it2.next();
                if (Process.myPid() == next.pid) {
                    if (next.importance == 100 && !keyguardManager.inKeyguardRestrictedInputMode()) {
                        PowerManager powerManager = (PowerManager) context.getSystemService("power");
                        if (powerManager == null ? false : powerManager.isScreenOn()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int[] m4564() {
        return new int[]{0, 0};
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static Bitmap m4565(Context context) {
        Bitmap bitmap;
        if (!(context instanceof Activity)) {
            return null;
        }
        try {
            if (((Boolean) zzkb.m5481().m5595(zznh.f4980)).booleanValue()) {
                Window window = ((Activity) context).getWindow();
                if (window != null) {
                    bitmap = m4558(window.getDecorView().getRootView());
                }
                bitmap = null;
            } else {
                bitmap = m4574(((Activity) context).getWindow().getDecorView());
            }
        } catch (RuntimeException e) {
            zzagf.m4793("Fail to capture screen shot", e);
        }
        return bitmap;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static boolean m4566(Context context) {
        try {
            context.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi");
            return false;
        } catch (ClassNotFoundException e) {
            return true;
        } catch (Throwable th) {
            zzagf.m4793("Error loading class.", th);
            zzbs.zzem().m4505(th, "AdUtil.isLiteSdk");
            return false;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static int m4567(Context context) {
        return DynamiteModule.m9310(context, ModuleDescriptor.MODULE_ID);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r1 = m4569(r2);
     */
    @android.annotation.TargetApi(16)
    /* renamed from: ˈ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m4568(android.content.Context r2) {
        /*
            r0 = 0
            if (r2 == 0) goto L_0x0009
            boolean r1 = com.google.android.gms.common.util.zzq.m9269()
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            android.app.KeyguardManager r1 = m4569(r2)
            if (r1 == 0) goto L_0x0009
            boolean r1 = r1.isKeyguardLocked()
            if (r1 == 0) goto L_0x0009
            r0 = 1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzahn.m4568(android.content.Context):boolean");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static KeyguardManager m4569(Context context) {
        Object systemService = context.getSystemService("keyguard");
        if (systemService == null || !(systemService instanceof KeyguardManager)) {
            return null;
        }
        return (KeyguardManager) systemService;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static AudioManager m4570(Context context) {
        return (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static float m4571(Context context) {
        AudioManager r1 = m4570(context);
        if (r1 == null) {
            return 0.0f;
        }
        int streamMaxVolume = r1.getStreamMaxVolume(3);
        int streamVolume = r1.getStreamVolume(3);
        if (streamMaxVolume != 0) {
            return ((float) streamVolume) / ((float) streamMaxVolume);
        }
        return 0.0f;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static int m4572(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null) {
            return 0;
        }
        return applicationInfo.targetSdkVersion;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static AlertDialog.Builder m4573(Context context) {
        return new AlertDialog.Builder(context);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static Bitmap m4574(View view) {
        try {
            int width = view.getWidth();
            int height = view.getHeight();
            if (width == 0 || height == 0) {
                zzagf.m4791("Width or height of view is zero");
                return null;
            }
            Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(createBitmap);
            view.layout(0, 0, width, height);
            view.draw(canvas);
            return createBitmap;
        } catch (RuntimeException e) {
            zzagf.m4793("Fail to capture the webview", e);
            return null;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static Bundle m4575() {
        Bundle bundle = new Bundle();
        try {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5145)).booleanValue()) {
                Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
                Debug.getMemoryInfo(memoryInfo);
                bundle.putParcelable("debug_memory_info", memoryInfo);
            }
            if (((Boolean) zzkb.m5481().m5595(zznh.f5147)).booleanValue()) {
                Runtime runtime = Runtime.getRuntime();
                bundle.putLong("runtime_free_memory", runtime.freeMemory());
                bundle.putLong("runtime_max_memory", runtime.maxMemory());
                bundle.putLong("runtime_total_memory", runtime.totalMemory());
            }
            bundle.putInt("web_view_count", zzbs.zzem().m4482());
        } catch (Exception e) {
            zzagf.m4796("Unable to gather memory stats", e);
        }
        return bundle;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m4576(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            String valueOf = String.valueOf(e);
            zzagf.m4791(new StringBuilder(String.valueOf(valueOf).length() + 22).append("Could not parse value:").append(valueOf).toString());
            return 0;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Bitmap m4577(View view) {
        if (view == null) {
            return null;
        }
        Bitmap r0 = m4558(view);
        return r0 == null ? m4574(view) : r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m4578() {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        return str2.startsWith(str) ? str2 : new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length()).append(str).append(StringUtils.SPACE).append(str2).toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m4579(Context context, String str) {
        try {
            return new String(zzn.m9262(context.openFileInput(str), true), "UTF-8");
        } catch (IOException e) {
            zzagf.m4792("Error reading from internal storage.");
            return "";
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4580(Activity activity, ViewTreeObserver.OnScrollChangedListener onScrollChangedListener) {
        Window window = activity.getWindow();
        if (window != null && window.getDecorView() != null && window.getDecorView().getViewTreeObserver() != null) {
            window.getDecorView().getViewTreeObserver().removeOnScrollChangedListener(onScrollChangedListener);
        }
    }

    @TargetApi(18)
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4581(Context context, Intent intent) {
        if (intent != null && zzq.m9270()) {
            Bundle extras = intent.getExtras() != null ? intent.getExtras() : new Bundle();
            extras.putBinder("android.support.customtabs.extra.SESSION", (IBinder) null);
            extras.putString("com.android.browser.application_id", context.getPackageName());
            intent.putExtras(extras);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4582(Context context, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        m4610(context, str, (List<String>) arrayList);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4583(Runnable runnable) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            runnable.run();
        } else {
            zzahh.m4555(runnable);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m4584(View view) {
        if (view == null) {
            return -1;
        }
        ViewParent parent = view.getParent();
        while (parent != null && !(parent instanceof AdapterView)) {
            parent = parent.getParent();
        }
        if (parent == null) {
            return -1;
        }
        return ((AdapterView) parent).getPositionForView(view);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    protected static String m4585(Context context) {
        try {
            return new WebView(context).getSettings().getUserAgentString();
        } catch (Throwable th) {
            return m4560();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m4586() {
        return zzbs.zzff().m4670();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static float m4587() {
        return zzbs.zzff().m4671();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m4588(Context context, String str, String str2) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 0);
            openFileOutput.write(str2.getBytes("UTF-8"));
            openFileOutput.close();
        } catch (Exception e) {
            zzagf.m4793("Error writing to file in internal storage.", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0018  */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m4589(android.view.View r4) {
        /*
            r2 = 0
            r1 = 0
            android.view.View r0 = r4.getRootView()
            if (r0 == 0) goto L_0x0016
            android.content.Context r0 = r0.getContext()
            boolean r3 = r0 instanceof android.app.Activity
            if (r3 == 0) goto L_0x0016
            android.app.Activity r0 = (android.app.Activity) r0
        L_0x0012:
            if (r0 != 0) goto L_0x0018
            r0 = r1
        L_0x0015:
            return r0
        L_0x0016:
            r0 = r2
            goto L_0x0012
        L_0x0018:
            android.view.Window r0 = r0.getWindow()
            if (r0 != 0) goto L_0x002a
            r0 = r2
        L_0x001f:
            if (r0 == 0) goto L_0x002f
            int r0 = r0.flags
            r2 = 524288(0x80000, float:7.34684E-40)
            r0 = r0 & r2
            if (r0 == 0) goto L_0x002f
            r0 = 1
            goto L_0x0015
        L_0x002a:
            android.view.WindowManager$LayoutParams r0 = r0.getAttributes()
            goto L_0x001f
        L_0x002f:
            r0 = r1
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzahn.m4589(android.view.View):boolean");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m4590(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return str.matches("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|webp))$)");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bitmap m4591(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap createBitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return createBitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Uri m4592(String str, String str2, String str3) throws UnsupportedOperationException {
        int indexOf = str.indexOf("&adurl");
        if (indexOf == -1) {
            indexOf = str.indexOf("?adurl");
        }
        return indexOf != -1 ? Uri.parse(str.substring(0, indexOf + 1) + str2 + "=" + str3 + "&" + str.substring(indexOf + 1)) : Uri.parse(str).buildUpon().appendQueryParameter(str2, str3).build();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bundle m4593(zzhk zzhk) {
        String r4;
        String r1;
        String str;
        if (zzhk == null) {
            return null;
        }
        if (!((Boolean) zzkb.m5481().m5595(zznh.f5130)).booleanValue()) {
            if (!((Boolean) zzkb.m5481().m5595(zznh.f5134)).booleanValue()) {
                return null;
            }
        }
        if (zzbs.zzem().m4508() && zzbs.zzem().m4487()) {
            return null;
        }
        if (zzhk.m5385()) {
            zzhk.m5386();
        }
        zzhe r0 = zzhk.m5384();
        if (r0 != null) {
            r4 = r0.m5366();
            str = r0.m5369();
            String r02 = r0.m5368();
            if (r4 != null) {
                zzbs.zzem().m4499(r4);
            }
            if (r02 != null) {
                zzbs.zzem().m4485(r02);
                r1 = r02;
            } else {
                r1 = r02;
            }
        } else {
            r4 = zzbs.zzem().m4466();
            r1 = zzbs.zzem().m4467();
            str = null;
        }
        Bundle bundle = new Bundle(1);
        if (r1 != null) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5134)).booleanValue() && !zzbs.zzem().m4487()) {
                bundle.putString("v_fp_vertical", r1);
            }
        }
        if (r4 != null) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5130)).booleanValue() && !zzbs.zzem().m4508()) {
                bundle.putString("fingerprint", r4);
                if (!r4.equals(str)) {
                    bundle.putString("v_fp", str);
                }
            }
        }
        if (!bundle.isEmpty()) {
            return bundle;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DisplayMetrics m4594(WindowManager windowManager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PopupWindow m4595(View view, int i, int i2, boolean z) {
        return new PopupWindow(view, i, i2, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4597() {
        return UUID.randomUUID().toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4598(Context context, View view, zzjn zzjn) {
        if (!((Boolean) zzkb.m5481().m5595(zznh.f4895)).booleanValue()) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(VastIconXmlManager.WIDTH, zzjn.f4794);
            jSONObject2.put(VastIconXmlManager.HEIGHT, zzjn.f4795);
            jSONObject.put("size", (Object) jSONObject2);
            jSONObject.put("activity", (Object) m4624(context));
            if (!zzjn.f4796) {
                JSONArray jSONArray = new JSONArray();
                while (view != null) {
                    ViewParent parent = view.getParent();
                    if (parent != null) {
                        int i = -1;
                        if (parent instanceof ViewGroup) {
                            i = ((ViewGroup) parent).indexOfChild(view);
                        }
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put(VastExtensionXmlManager.TYPE, (Object) parent.getClass().getName());
                        jSONObject3.put("index_of_child", i);
                        jSONArray.put((Object) jSONObject3);
                    }
                    view = (parent == null || !(parent instanceof View)) ? null : (View) parent;
                }
                if (jSONArray.length() > 0) {
                    jSONObject.put("parents", (Object) jSONArray);
                }
            }
            return jSONObject.toString();
        } catch (JSONException e) {
            zzagf.m4796("Fail to get view hierarchy json", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4599(Context context, zzcv zzcv, String str, View view, Activity activity) {
        if (zzcv == null) {
            return str;
        }
        try {
            Uri parse = Uri.parse(str);
            if (zzcv.m11536(parse)) {
                parse = zzcv.m11538(parse, context, view, activity);
            }
            return parse.toString();
        } catch (Exception e) {
            return str;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4601(InputStreamReader inputStreamReader) throws IOException {
        StringBuilder sb = new StringBuilder(8192);
        char[] cArr = new char[2048];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read == -1) {
                return sb.toString();
            }
            sb.append(cArr, 0, read);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4602(String str) {
        return Uri.parse(str).buildUpon().query((String) null).build().toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Map<String, String> m4603(Uri uri) {
        if (uri == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (String next : zzbs.zzek().m4657(uri)) {
            hashMap.put(next, uri.getQueryParameter(next));
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final JSONArray m4604(Collection<?> collection) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (Object r2 : collection) {
            m4613(jSONArray, (Object) r2);
        }
        return jSONArray;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final JSONObject m4605(Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        for (String str : bundle.keySet()) {
            m4614(jSONObject, str, bundle.get(str));
        }
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4606(Activity activity, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        Window window = activity.getWindow();
        if (window != null && window.getDecorView() != null && window.getDecorView().getViewTreeObserver() != null) {
            window.getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4607(Activity activity, ViewTreeObserver.OnScrollChangedListener onScrollChangedListener) {
        Window window = activity.getWindow();
        if (window != null && window.getDecorView() != null && window.getDecorView().getViewTreeObserver() != null) {
            window.getDecorView().getViewTreeObserver().addOnScrollChangedListener(onScrollChangedListener);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4608(Context context, Intent intent) {
        try {
            context.startActivity(intent);
        } catch (Throwable th) {
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }

    @TargetApi(18)
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4609(Context context, Uri uri) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            if (((Boolean) zzkb.m5481().m5595(zznh.f5025)).booleanValue()) {
                m4581(context, intent);
            }
            bundle.putString("com.android.browser.application_id", context.getPackageName());
            context.startActivity(intent);
            String uri2 = uri.toString();
            zzagf.m4792(new StringBuilder(String.valueOf(uri2).length() + 26).append("Opening ").append(uri2).append(" in a new browser.").toString());
        } catch (ActivityNotFoundException e) {
            zzagf.m4793("No browser is found.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4610(Context context, String str, List<String> list) {
        for (String zzajh : list) {
            new zzajh(context, str, zzajh).m4523();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4611(zzjj zzjj, boolean z) {
        Bundle bundle;
        Bundle bundle2 = zzjj.f4756 != null ? zzjj.f4756 : new Bundle();
        if (bundle2.getBundle(AdMobAdapter.class.getName()) != null) {
            bundle = bundle2.getBundle(AdMobAdapter.class.getName());
        } else {
            Bundle bundle3 = new Bundle();
            bundle2.putBundle(AdMobAdapter.class.getName(), bundle3);
            bundle = bundle3;
        }
        bundle.putBoolean("render_test_label", true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4612(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            runnable.run();
        } else {
            f4212.post(runnable);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4613(JSONArray jSONArray, Object obj) throws JSONException {
        if (obj instanceof Bundle) {
            jSONArray.put((Object) m4605((Bundle) obj));
        } else if (obj instanceof Map) {
            jSONArray.put((Object) m4634((Map<String, ?>) (Map) obj));
        } else if (obj instanceof Collection) {
            jSONArray.put((Object) m4604((Collection<?>) (Collection) obj));
        } else if (obj instanceof Object[]) {
            JSONArray jSONArray2 = new JSONArray();
            for (Object r3 : (Object[]) obj) {
                m4613(jSONArray2, r3);
            }
            jSONArray.put((Object) jSONArray2);
        } else {
            jSONArray.put(obj);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4614(JSONObject jSONObject, String str, Object obj) throws JSONException {
        if (obj instanceof Bundle) {
            jSONObject.put(str, (Object) m4605((Bundle) obj));
        } else if (obj instanceof Map) {
            jSONObject.put(str, (Object) m4634((Map<String, ?>) (Map) obj));
        } else if (obj instanceof Collection) {
            if (str == null) {
                str = "null";
            }
            jSONObject.put(str, (Object) m4604((Collection<?>) (Collection) obj));
        } else if (obj instanceof Object[]) {
            jSONObject.put(str, (Object) m4604((Collection<?>) Arrays.asList((Object[]) obj)));
        } else {
            jSONObject.put(str, obj);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m4615(int i, int i2, int i3) {
        return Math.abs(i - i2) <= i3;
    }

    @TargetApi(24)
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4616(Activity activity, Configuration configuration) {
        zzkb.m5487();
        int r3 = zzajr.m4757((Context) activity, configuration.screenHeightDp);
        int r4 = zzajr.m4757((Context) activity, configuration.screenWidthDp);
        DisplayMetrics r0 = m4594((WindowManager) activity.getApplicationContext().getSystemService("window"));
        int i = r0.heightPixels;
        int i2 = r0.widthPixels;
        int identifier = activity.getResources().getIdentifier("status_bar_height", "dimen", AbstractSpiCall.ANDROID_CLIENT_TYPE);
        int dimensionPixelSize = identifier > 0 ? activity.getResources().getDimensionPixelSize(identifier) : 0;
        int intValue = ((Integer) zzkb.m5481().m5595(zznh.f5036)).intValue() * ((int) Math.round(((double) activity.getResources().getDisplayMetrics().density) + 0.5d));
        return m4615(i, dimensionPixelSize + r3, intValue) && m4615(i2, r4, intValue);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4617(Context context) {
        boolean z;
        Intent intent = new Intent();
        intent.setClassName(context, AdActivity.CLASS_NAME);
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 65536);
        if (resolveActivity == null || resolveActivity.activityInfo == null) {
            zzagf.m4791("Could not find com.google.android.gms.ads.AdActivity, please make sure it is declared in AndroidManifest.xml.");
            return false;
        }
        if ((resolveActivity.activityInfo.configChanges & 16) == 0) {
            zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"keyboard"}));
            z = false;
        } else {
            z = true;
        }
        if ((resolveActivity.activityInfo.configChanges & 32) == 0) {
            zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"keyboardHidden"}));
            z = false;
        }
        if ((resolveActivity.activityInfo.configChanges & 128) == 0) {
            zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"orientation"}));
            z = false;
        }
        if ((resolveActivity.activityInfo.configChanges & 256) == 0) {
            zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"screenLayout"}));
            z = false;
        }
        if ((resolveActivity.activityInfo.configChanges & 512) == 0) {
            zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"uiMode"}));
            z = false;
        }
        if ((resolveActivity.activityInfo.configChanges & 1024) == 0) {
            zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"screenSize"}));
            z = false;
        }
        if ((resolveActivity.activityInfo.configChanges & 2048) != 0) {
            return z;
        }
        zzagf.m4791(String.format("com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".", new Object[]{"smallestScreenSize"}));
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4618(Context context, String str, String str2) {
        return zzbhf.m10231(context).m10225(str2, str) == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4620(zzjj zzjj) {
        Bundle bundle = zzjj.f4756 != null ? zzjj.f4756 : new Bundle();
        return (bundle.getBundle(AdMobAdapter.class.getName()) != null ? bundle.getBundle(AdMobAdapter.class.getName()) : new Bundle()).getBoolean("render_test_label", false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4621(ClassLoader classLoader, Class<?> cls, String str) {
        try {
            return cls.isAssignableFrom(Class.forName(str, false, classLoader));
        } catch (Throwable th) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int[] m4622(Activity activity) {
        View findViewById;
        Window window = activity.getWindow();
        if (window == null || (findViewById = window.findViewById(16908290)) == null) {
            return m4564();
        }
        return new int[]{findViewById.getWidth(), findViewById.getHeight()};
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static int m4623(Context context) {
        return DynamiteModule.m9316(context, ModuleDescriptor.MODULE_ID);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static String m4624(Context context) {
        ActivityManager.RunningTaskInfo runningTaskInfo;
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager == null) {
                return null;
            }
            List<ActivityManager.RunningTaskInfo> runningTasks = activityManager.getRunningTasks(1);
            if (!(runningTasks == null || runningTasks.isEmpty() || (runningTaskInfo = runningTasks.get(0)) == null || runningTaskInfo.topActivity == null)) {
                return runningTaskInfo.topActivity.getClassName();
            }
            return null;
        } catch (Exception e) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (((java.lang.String) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4902)).equals(r3.f4215.pattern()) == false) goto L_0x0026;
     */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m4625(java.lang.String r4) {
        /*
            r3 = this;
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 == 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            monitor-enter(r3)     // Catch:{ PatternSyntaxException -> 0x0047 }
            java.util.regex.Pattern r0 = r3.f4215     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x0026
            com.google.android.gms.internal.zzmx<java.lang.String> r0 = com.google.android.gms.internal.zznh.f4902     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x0044 }
            java.lang.Object r0 = r2.m5595(r0)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0044 }
            java.util.regex.Pattern r2 = r3.f4215     // Catch:{ all -> 0x0044 }
            java.lang.String r2 = r2.pattern()     // Catch:{ all -> 0x0044 }
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x0044 }
            if (r0 != 0) goto L_0x0038
        L_0x0026:
            com.google.android.gms.internal.zzmx<java.lang.String> r0 = com.google.android.gms.internal.zznh.f4902     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x0044 }
            java.lang.Object r0 = r2.m5595(r0)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0044 }
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)     // Catch:{ all -> 0x0044 }
            r3.f4215 = r0     // Catch:{ all -> 0x0044 }
        L_0x0038:
            java.util.regex.Pattern r0 = r3.f4215     // Catch:{ all -> 0x0044 }
            java.util.regex.Matcher r0 = r0.matcher(r4)     // Catch:{ all -> 0x0044 }
            boolean r0 = r0.matches()     // Catch:{ all -> 0x0044 }
            monitor-exit(r3)     // Catch:{ all -> 0x0044 }
            goto L_0x0008
        L_0x0044:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0044 }
            throw r0     // Catch:{ PatternSyntaxException -> 0x0047 }
        L_0x0047:
            r0 = move-exception
            r0 = r1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzahn.m4625(java.lang.String):boolean");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4626(Context context, String str, String str2, Bundle bundle, boolean z) {
        if (((Boolean) zzkb.m5481().m5595(zznh.f4950)).booleanValue()) {
            m4635(context, str, str2, bundle, z);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4627(Context context) {
        if (this.f4216) {
            return false;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        context.getApplicationContext().registerReceiver(new zzahs(this, (zzaho) null), intentFilter);
        this.f4216 = true;
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int[] m4628(Activity activity) {
        int[] r0 = m4622(activity);
        zzkb.m5487();
        zzkb.m5487();
        return new int[]{zzajr.m4749((Context) activity, r0[0]), zzajr.m4749((Context) activity, r0[1])};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (((java.lang.String) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4901)).equals(r3.f4214.pattern()) == false) goto L_0x0026;
     */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m4629(java.lang.String r4) {
        /*
            r3 = this;
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 == 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            monitor-enter(r3)     // Catch:{ PatternSyntaxException -> 0x0047 }
            java.util.regex.Pattern r0 = r3.f4214     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x0026
            com.google.android.gms.internal.zzmx<java.lang.String> r0 = com.google.android.gms.internal.zznh.f4901     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x0044 }
            java.lang.Object r0 = r2.m5595(r0)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0044 }
            java.util.regex.Pattern r2 = r3.f4214     // Catch:{ all -> 0x0044 }
            java.lang.String r2 = r2.pattern()     // Catch:{ all -> 0x0044 }
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x0044 }
            if (r0 != 0) goto L_0x0038
        L_0x0026:
            com.google.android.gms.internal.zzmx<java.lang.String> r0 = com.google.android.gms.internal.zznh.f4901     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x0044 }
            java.lang.Object r0 = r2.m5595(r0)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0044 }
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)     // Catch:{ all -> 0x0044 }
            r3.f4214 = r0     // Catch:{ all -> 0x0044 }
        L_0x0038:
            java.util.regex.Pattern r0 = r3.f4214     // Catch:{ all -> 0x0044 }
            java.util.regex.Matcher r0 = r0.matcher(r4)     // Catch:{ all -> 0x0044 }
            boolean r0 = r0.matches()     // Catch:{ all -> 0x0044 }
            monitor-exit(r3)     // Catch:{ all -> 0x0044 }
            goto L_0x0008
        L_0x0044:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0044 }
            throw r0     // Catch:{ PatternSyntaxException -> 0x0047 }
        L_0x0047:
            r0 = move-exception
            r0 = r1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzahn.m4629(java.lang.String):boolean");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m4630(Context context) {
        if (this.f4213) {
            return false;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.google.android.ads.intent.DEBUG_LOGGING_ENABLEMENT_CHANGED");
        context.getApplicationContext().registerReceiver(new zzahr(this, (zzaho) null), intentFilter);
        this.f4213 = true;
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0009, code lost:
        r1 = r0.findViewById(16908290);
     */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int[] m4631(android.app.Activity r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = 0
            android.view.Window r0 = r7.getWindow()
            if (r0 == 0) goto L_0x0039
            r1 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r1 = r0.findViewById(r1)
            if (r1 == 0) goto L_0x0039
            int[] r0 = new int[r5]
            int r2 = r1.getTop()
            r0[r3] = r2
            int r1 = r1.getBottom()
            r0[r4] = r1
        L_0x0020:
            int[] r1 = new int[r5]
            com.google.android.gms.internal.zzkb.m5487()
            r2 = r0[r3]
            int r2 = com.google.android.gms.internal.zzajr.m4749((android.content.Context) r7, (int) r2)
            r1[r3] = r2
            com.google.android.gms.internal.zzkb.m5487()
            r0 = r0[r4]
            int r0 = com.google.android.gms.internal.zzajr.m4749((android.content.Context) r7, (int) r0)
            r1[r4] = r0
            return r1
        L_0x0039:
            int[] r0 = m4564()
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzahn.m4631(android.app.Activity):int[]");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m4632(Context context, String str) {
        String str2;
        synchronized (this.f4217) {
            if (this.f4218 != null) {
                str2 = this.f4218;
            } else if (str == null) {
                str2 = m4560();
            } else {
                try {
                    this.f4218 = zzbs.zzek().m4655(context);
                } catch (Exception e) {
                }
                if (TextUtils.isEmpty(this.f4218)) {
                    zzkb.m5487();
                    if (!zzajr.m4752()) {
                        this.f4218 = null;
                        f4212.post(new zzahp(this, context));
                        while (this.f4218 == null) {
                            try {
                                this.f4217.wait();
                            } catch (InterruptedException e2) {
                                this.f4218 = m4560();
                                String valueOf = String.valueOf(this.f4218);
                                zzagf.m4791(valueOf.length() != 0 ? "Interrupted, use default user agent: ".concat(valueOf) : new String("Interrupted, use default user agent: "));
                            }
                        }
                    } else {
                        this.f4218 = m4585(context);
                    }
                }
                String valueOf2 = String.valueOf(this.f4218);
                this.f4218 = new StringBuilder(String.valueOf(valueOf2).length() + 10 + String.valueOf(str).length()).append(valueOf2).append(" (Mobile; ").append(str).toString();
                try {
                    if (zzbhf.m10231(context).m10227()) {
                        this.f4218 = String.valueOf(this.f4218).concat(";aia");
                    }
                } catch (Exception e3) {
                    zzbs.zzem().m4505((Throwable) e3, "AdUtil.getUserAgent");
                }
                this.f4218 = String.valueOf(this.f4218).concat(")");
                str2 = this.f4218;
            }
        }
        return str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m4633(Bundle bundle, JSONObject jSONObject) {
        if (bundle == null) {
            return null;
        }
        try {
            return m4605(bundle);
        } catch (JSONException e) {
            zzagf.m4793("Error converting Bundle to JSON", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m4634(Map<String, ?> map) throws JSONException {
        try {
            JSONObject jSONObject = new JSONObject();
            for (String next : map.keySet()) {
                m4614(jSONObject, next, (Object) map.get(next));
            }
            return jSONObject;
        } catch (ClassCastException e) {
            String valueOf = String.valueOf(e.getMessage());
            throw new JSONException(valueOf.length() != 0 ? "Could not convert map to JSON: ".concat(valueOf) : new String("Could not convert map to JSON: "));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4635(Context context, String str, String str2, Bundle bundle, boolean z) {
        if (z) {
            zzbs.zzei();
            bundle.putString("device", m4578());
            bundle.putString("eids", TextUtils.join(",", zznh.m5598()));
        }
        zzkb.m5487();
        zzajr.m4763(context, str, str2, bundle, z, new zzahq(this, context, str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4636(Context context, String str, boolean z, HttpURLConnection httpURLConnection) {
        httpURLConnection.setConnectTimeout(60000);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setReadTimeout(60000);
        httpURLConnection.setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, m4632(context, str));
        httpURLConnection.setUseCaches(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4637(Context context, List<String> list) {
        if (!(context instanceof Activity) || TextUtils.isEmpty(zzftr.m12949((Activity) context))) {
            return;
        }
        if (list == null) {
            zzagf.m4527("Cannot ping urls: empty list.");
        } else if (!zzoe.m5638(context)) {
            zzagf.m4527("Cannot ping url because custom tabs is not supported");
        } else {
            zzoe zzoe = new zzoe();
            zzoe.m5640((zzof) new zzaho(this, list, zzoe, context));
            zzoe.m5639((Activity) context);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4638(View view, Context context) {
        PowerManager powerManager = null;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            powerManager = (PowerManager) applicationContext.getSystemService("power");
        }
        return m4639(view, powerManager, m4569(context));
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0071  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m4639(android.view.View r5, android.os.PowerManager r6, android.app.KeyguardManager r7) {
        /*
            r4 = this;
            r2 = 1
            r1 = 0
            com.google.android.gms.internal.zzahn r0 = com.google.android.gms.ads.internal.zzbs.zzei()
            boolean r0 = r0.f4219
            if (r0 != 0) goto L_0x0027
            if (r7 != 0) goto L_0x006a
            r0 = r1
        L_0x000d:
            if (r0 == 0) goto L_0x0027
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f4946
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r3.m5595(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x006f
            boolean r0 = m4589((android.view.View) r5)
            if (r0 == 0) goto L_0x006f
        L_0x0027:
            r0 = r2
        L_0x0028:
            int r3 = r5.getVisibility()
            if (r3 != 0) goto L_0x0073
            boolean r3 = r5.isShown()
            if (r3 == 0) goto L_0x0073
            if (r6 == 0) goto L_0x003c
            boolean r3 = r6.isScreenOn()
            if (r3 == 0) goto L_0x0071
        L_0x003c:
            r3 = r2
        L_0x003d:
            if (r3 == 0) goto L_0x0073
            if (r0 == 0) goto L_0x0073
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f4943
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r3.m5595(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0069
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            boolean r0 = r5.getLocalVisibleRect(r0)
            if (r0 != 0) goto L_0x0069
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            boolean r0 = r5.getGlobalVisibleRect(r0)
            if (r0 == 0) goto L_0x0073
        L_0x0069:
            return r2
        L_0x006a:
            boolean r0 = r7.inKeyguardRestrictedInputMode()
            goto L_0x000d
        L_0x006f:
            r0 = r1
            goto L_0x0028
        L_0x0071:
            r3 = r1
            goto L_0x003d
        L_0x0073:
            r2 = r1
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzahn.m4639(android.view.View, android.os.PowerManager, android.app.KeyguardManager):boolean");
    }
}
