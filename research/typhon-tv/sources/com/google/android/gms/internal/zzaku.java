package com.google.android.gms.internal;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@zzzv
final class zzaku<T> implements zzakv<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzakw f4298 = new zzakw();

    /* renamed from: 龘  reason: contains not printable characters */
    private final T f4299;

    zzaku(T t) {
        this.f4299 = t;
        this.f4298.m4815();
    }

    public final boolean cancel(boolean z) {
        return false;
    }

    public final T get() {
        return this.f4299;
    }

    public final T get(long j, TimeUnit timeUnit) {
        return this.f4299;
    }

    public final boolean isCancelled() {
        return false;
    }

    public final boolean isDone() {
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4814(Runnable runnable, Executor executor) {
        this.f4298.m4816(runnable, executor);
    }
}
