package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.util.zzq;

public final class zzbhd {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Boolean f8753;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Context f8754;

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized boolean m10221(Context context) {
        boolean booleanValue;
        synchronized (zzbhd.class) {
            Context applicationContext = context.getApplicationContext();
            if (f8754 == null || f8753 == null || f8754 != applicationContext) {
                f8753 = null;
                if (zzq.m9267()) {
                    f8753 = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        f8753 = true;
                    } catch (ClassNotFoundException e) {
                        f8753 = false;
                    }
                }
                f8754 = applicationContext;
                booleanValue = f8753.booleanValue();
            } else {
                booleanValue = f8753.booleanValue();
            }
        }
        return booleanValue;
    }
}
