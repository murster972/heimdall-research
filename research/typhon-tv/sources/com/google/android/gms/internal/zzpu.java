package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzpu extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m13230(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13231() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13232(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13233(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13234(String str, IObjectWrapper iObjectWrapper) throws RemoteException;
}
