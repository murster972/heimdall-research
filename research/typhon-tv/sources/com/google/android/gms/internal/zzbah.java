package com.google.android.gms.internal;

import android.graphics.Bitmap;

final class zzbah implements zzazo {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbag f8564;

    zzbah(zzbag zzbag) {
        this.f8564 = zzbag;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9888(Bitmap bitmap) {
        if (bitmap != null) {
            if (this.f8564.f8561 != null) {
                this.f8564.f8561.setVisibility(4);
            }
            this.f8564.f8563.setVisibility(0);
            this.f8564.f8563.setImageBitmap(bitmap);
        }
    }
}
