package com.google.android.gms.internal;

import java.nio.charset.Charset;
import java.util.regex.Pattern;

public final class zzexg {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Pattern f10307 = Pattern.compile("^(1|true|t|yes|y|on)$", 2);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Pattern f10308 = Pattern.compile("^(0|false|f|no|n|off|)$", 2);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Charset f10309 = Charset.forName("UTF-8");
}
