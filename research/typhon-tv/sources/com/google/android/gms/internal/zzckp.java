package com.google.android.gms.internal;

import android.os.RemoteException;
import android.text.TextUtils;

final class zzckp implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzckg f9581;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzcgl f9582;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f9583;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9584;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzcgl f9585;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ boolean f9586 = true;

    zzckp(zzckg zzckg, boolean z, boolean z2, zzcgl zzcgl, zzcgi zzcgi, zzcgl zzcgl2) {
        this.f9581 = zzckg;
        this.f9583 = z2;
        this.f9585 = zzcgl;
        this.f9584 = zzcgi;
        this.f9582 = zzcgl2;
    }

    public final void run() {
        zzche r1 = this.f9581.f9558;
        if (r1 == null) {
            this.f9581.m11096().m10832().m10849("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.f9586) {
            this.f9581.m11264(r1, this.f9583 ? null : this.f9585, this.f9584);
        } else {
            try {
                if (TextUtils.isEmpty(this.f9582.f9156)) {
                    r1.m10685(this.f9585, this.f9584);
                } else {
                    r1.m10684(this.f9585);
                }
            } catch (RemoteException e) {
                this.f9581.m11096().m10832().m10850("Failed to send conditional user property to the service", e);
            }
        }
        this.f9581.m11223();
    }
}
