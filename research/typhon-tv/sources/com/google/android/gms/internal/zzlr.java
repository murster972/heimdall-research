package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

@zzzv
public final class zzlr extends zzbfm {
    public static final Parcelable.Creator<zzlr> CREATOR = new zzls();

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f4824;

    public zzlr(int i) {
        this.f4824 = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 2, this.f4824);
        zzbfp.m10182(parcel, r0);
    }
}
