package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final class zzyo implements zzt<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzym f10977;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzon f10978;

    zzyo(zzym zzym, zzon zzon) {
        this.f10977 = zzym;
        this.f10978 = zzon;
    }

    public final void zza(Object obj, Map<String, String> map) {
        this.f10977.m6053((zzqm) this.f10978, map.get("asset"));
    }
}
