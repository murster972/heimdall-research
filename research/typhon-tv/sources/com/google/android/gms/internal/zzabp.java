package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.js.zzaa;

final class zzabp implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzabo f8058;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzafp f8059;

    zzabp(zzabo zzabo, zzafp zzafp) {
        this.f8058 = zzabo;
        this.f8059 = zzafp;
    }

    public final void run() {
        this.f8058.f3887.zza(this.f8059);
        if (this.f8058.f3889 != null) {
            this.f8058.f3889.release();
            zzaa unused = this.f8058.f3889 = null;
        }
    }
}
