package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.Provider;

public final class zzdvb implements zzduv<MessageDigest> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m12224(String str, Provider provider) throws GeneralSecurityException {
        return provider == null ? MessageDigest.getInstance(str) : MessageDigest.getInstance(str, provider);
    }
}
