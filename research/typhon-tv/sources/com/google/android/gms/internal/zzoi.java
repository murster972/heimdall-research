package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzoi extends zzpr {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Uri f5217;

    /* renamed from: 齉  reason: contains not printable characters */
    private final double f5218;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Drawable f5219;

    public zzoi(Drawable drawable, Uri uri, double d) {
        this.f5219 = drawable;
        this.f5217 = uri;
        this.f5218 = d;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Uri m5650() throws RemoteException {
        return this.f5217;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final double m5651() {
        return this.f5218;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m5652() throws RemoteException {
        return zzn.m9306(this.f5219);
    }
}
