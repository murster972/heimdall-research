package com.google.android.gms.internal;

import android.net.TrafficStats;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public final class zzn extends Thread {

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile boolean f10804 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzm f10805;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzz f10806;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzb f10807;

    /* renamed from: 龘  reason: contains not printable characters */
    private final BlockingQueue<zzr<?>> f10808;

    public zzn(BlockingQueue<zzr<?>> blockingQueue, zzm zzm, zzb zzb, zzz zzz) {
        this.f10808 = blockingQueue;
        this.f10805 = zzm;
        this.f10807 = zzb;
        this.f10806 = zzz;
    }

    public final void run() {
        Process.setThreadPriority(10);
        while (true) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                zzr take = this.f10808.take();
                try {
                    take.m13361("network-queue-take");
                    TrafficStats.setThreadStatsTag(take.m13362());
                    zzp r1 = this.f10805.m13117(take);
                    take.m13361("network-http-complete");
                    if (!r1.f5277 || !take.m13358()) {
                        zzw r12 = take.m13368(r1);
                        take.m13361("network-parse-complete");
                        if (take.m13353() && r12.f10931 != null) {
                            this.f10807.m9867(take.m13359(), r12.f10931);
                            take.m13361("network-cache-written");
                        }
                        take.m13357();
                        this.f10806.m13647((zzr<?>) take, (zzw<?>) r12);
                        take.m13371((zzw<?>) r12);
                    } else {
                        take.m13364("not-modified");
                        take.m13355();
                    }
                } catch (zzad e) {
                    e.m9464(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.f10806.m13646((zzr<?>) take, e);
                    take.m13355();
                } catch (Exception e2) {
                    zzae.m9517(e2, "Unhandled exception %s", e2.toString());
                    zzad zzad = new zzad((Throwable) e2);
                    zzad.m9464(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.f10806.m13646((zzr<?>) take, zzad);
                    take.m13355();
                }
            } catch (InterruptedException e3) {
                if (this.f10804) {
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13153() {
        this.f10804 = true;
        interrupt();
    }
}
