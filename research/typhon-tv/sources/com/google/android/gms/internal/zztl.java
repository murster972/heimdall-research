package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.Pinkamena;
import com.google.android.gms.ads.internal.zzak;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.dynamic.IObjectWrapper;

@zzzv
public final class zztl extends zzkt {

    /* renamed from: 连任  reason: contains not printable characters */
    private final zztd f5377;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f5378;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzak f5379;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzsd f5380;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f5381;

    public zztl(Context context, String str, zzux zzux, zzakd zzakd, zzv zzv) {
        this(str, new zzsd(context, zzux, zzakd, zzv));
    }

    private zztl(String str, zzsd zzsd) {
        this.f5381 = str;
        this.f5380 = zzsd;
        this.f5377 = new zztd();
        zzbs.zzeu().m5838(zzsd);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5863() {
        if (this.f5379 == null) {
            this.f5379 = this.f5380.m5821(this.f5381);
            this.f5377.m5824(this.f5379);
        }
    }

    public final void destroy() throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.destroy();
        }
    }

    public final String getAdUnitId() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        if (this.f5379 != null) {
            return this.f5379.getMediationAdapterClassName();
        }
        return null;
    }

    public final zzll getVideoController() {
        throw new IllegalStateException("getVideoController not implemented for interstitials");
    }

    public final boolean isLoading() throws RemoteException {
        return this.f5379 != null && this.f5379.isLoading();
    }

    public final boolean isReady() throws RemoteException {
        return this.f5379 != null && this.f5379.isReady();
    }

    public final void pause() throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.pause();
        }
    }

    public final void resume() throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.resume();
        }
    }

    public final void setImmersiveMode(boolean z) {
        this.f5378 = z;
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
        m5863();
        if (this.f5379 != null) {
            this.f5379.setManualImpressionsEnabled(z);
        }
    }

    public final void setUserId(String str) {
    }

    public final void showInterstitial() throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.setImmersiveMode(this.f5378);
            zzak zzak = this.f5379;
            Pinkamena.DianePie();
            return;
        }
        zzagf.m4791("Interstitial ad must be loaded before showInterstitial().");
    }

    public final void stopLoading() throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.stopLoading();
        }
    }

    public final void zza(zzadp zzadp) {
        this.f5377.f5357 = zzadp;
        if (this.f5379 != null) {
            this.f5377.m5824(this.f5379);
        }
    }

    public final void zza(zzjn zzjn) throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.zza(zzjn);
        }
    }

    public final void zza(zzke zzke) throws RemoteException {
        this.f5377.f5359 = zzke;
        if (this.f5379 != null) {
            this.f5377.m5824(this.f5379);
        }
    }

    public final void zza(zzkh zzkh) throws RemoteException {
        this.f5377.f5361 = zzkh;
        if (this.f5379 != null) {
            this.f5377.m5824(this.f5379);
        }
    }

    public final void zza(zzkx zzkx) throws RemoteException {
        this.f5377.f5358 = zzkx;
        if (this.f5379 != null) {
            this.f5377.m5824(this.f5379);
        }
    }

    public final void zza(zzld zzld) throws RemoteException {
        m5863();
        if (this.f5379 != null) {
            this.f5379.zza(zzld);
        }
    }

    public final void zza(zzlr zzlr) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzmr zzmr) {
        throw new IllegalStateException("getVideoController not implemented for interstitials");
    }

    public final void zza(zzoa zzoa) throws RemoteException {
        this.f5377.f5360 = zzoa;
        if (this.f5379 != null) {
            this.f5377.m5824(this.f5379);
        }
    }

    public final void zza(zzxl zzxl) throws RemoteException {
        zzagf.m4791("setInAppPurchaseListener is deprecated and should not be called.");
    }

    public final void zza(zzxr zzxr, String str) throws RemoteException {
        zzagf.m4791("setPlayStorePurchaseParams is deprecated and should not be called.");
    }

    public final boolean zzb(zzjj zzjj) throws RemoteException {
        if (!zztg.m5831(zzjj).contains("gw")) {
            m5863();
        }
        if (zztg.m5831(zzjj).contains("_skipMediation")) {
            m5863();
        }
        if (zzjj.f4762 != null) {
            m5863();
        }
        if (this.f5379 != null) {
            return this.f5379.zzb(zzjj);
        }
        zztg zzeu = zzbs.zzeu();
        if (zztg.m5831(zzjj).contains("_ad")) {
            zzeu.m5835(zzjj, this.f5381);
        }
        zztj r0 = zzeu.m5836(zzjj, this.f5381);
        if (r0 != null) {
            if (!r0.f10890) {
                r0.m13431();
                zztk.m5852().m5858();
            } else {
                zztk.m5852().m5860();
            }
            this.f5379 = r0.f10894;
            r0.f10893.m5823(this.f5377);
            this.f5377.m5824(this.f5379);
            return r0.f10888;
        }
        m5863();
        zztk.m5852().m5858();
        return this.f5379.zzb(zzjj);
    }

    public final IObjectWrapper zzbr() throws RemoteException {
        if (this.f5379 != null) {
            return this.f5379.zzbr();
        }
        return null;
    }

    public final zzjn zzbs() throws RemoteException {
        if (this.f5379 != null) {
            return this.f5379.zzbs();
        }
        return null;
    }

    public final void zzbu() throws RemoteException {
        if (this.f5379 != null) {
            this.f5379.zzbu();
        } else {
            zzagf.m4791("Interstitial ad must be loaded before pingManualTrackingUrl().");
        }
    }

    public final zzkx zzcd() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }

    public final zzkh zzce() {
        throw new IllegalStateException("getIAdListener not implemented");
    }

    public final String zzcp() throws RemoteException {
        if (this.f5379 != null) {
            return this.f5379.zzcp();
        }
        return null;
    }
}
