package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzcbq implements Parcelable.Creator<zzcbp> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new zzcbp(i, str2, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcbp[i];
    }
}
