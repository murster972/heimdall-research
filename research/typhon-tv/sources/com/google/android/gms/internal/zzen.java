package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzen extends zzet {

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzdt f10267;

    public zzen(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2, zzdt zzdt) {
        super(zzdm, str, str2, zzaz, i, 53);
        this.f10267 = zzdt;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12288() throws IllegalAccessException, InvocationTargetException {
        if (this.f10267 != null) {
            this.f10284.f8428 = (Long) this.f10286.invoke((Object) null, new Object[]{Long.valueOf(this.f10267.m12030())});
        }
    }
}
