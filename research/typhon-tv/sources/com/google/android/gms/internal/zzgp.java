package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzbs;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;

@zzzv
@TargetApi(14)
public final class zzgp implements Application.ActivityLifecycleCallbacks, View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final long f4653 = ((Long) zzkb.m5481().m5595(zznh.f4945)).longValue();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final KeyguardManager f4654;

    /* renamed from: ʼ  reason: contains not printable characters */
    private BroadcastReceiver f4655;

    /* renamed from: ʽ  reason: contains not printable characters */
    private WeakReference<ViewTreeObserver> f4656;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f4657 = -1;

    /* renamed from: ʿ  reason: contains not printable characters */
    private HashSet<zzgt> f4658 = new HashSet<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f4659 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private WeakReference<View> f4660;

    /* renamed from: ٴ  reason: contains not printable characters */
    private zzgu f4661;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzaji f4662 = new zzaji(f4653);

    /* renamed from: 连任  reason: contains not printable characters */
    private final PowerManager f4663;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f4664;

    /* renamed from: 麤  reason: contains not printable characters */
    private final WindowManager f4665;

    /* renamed from: 齉  reason: contains not printable characters */
    private Application f4666;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private DisplayMetrics f4667;

    public zzgp(Context context, View view) {
        this.f4664 = context.getApplicationContext();
        this.f4665 = (WindowManager) context.getSystemService("window");
        this.f4663 = (PowerManager) this.f4664.getSystemService("power");
        this.f4654 = (KeyguardManager) context.getSystemService("keyguard");
        if (this.f4664 instanceof Application) {
            this.f4666 = (Application) this.f4664;
            this.f4661 = new zzgu((Application) this.f4664, this);
        }
        this.f4667 = context.getResources().getDisplayMetrics();
        View view2 = this.f4660 != null ? (View) this.f4660.get() : null;
        if (view2 != null) {
            view2.removeOnAttachStateChangeListener(this);
            m5349(view2);
        }
        this.f4660 = new WeakReference<>(view);
        if (view != null) {
            if (zzbs.zzek().m4663(view)) {
                m5353(view);
            }
            view.addOnAttachStateChangeListener(this);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final int m5347(int i) {
        return (int) (((float) i) / this.f4667.density);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m5348() {
        zzbs.zzei();
        zzahn.f4212.post(new zzgq(this));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m5349(View view) {
        try {
            if (this.f4656 != null) {
                ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f4656.get();
                if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnScrollChangedListener(this);
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
                this.f4656 = null;
            }
        } catch (Exception e) {
            zzagf.m4793("Error while unregistering listeners from the last ViewTreeObserver.", e);
        }
        try {
            ViewTreeObserver viewTreeObserver2 = view.getViewTreeObserver();
            if (viewTreeObserver2.isAlive()) {
                viewTreeObserver2.removeOnScrollChangedListener(this);
                viewTreeObserver2.removeGlobalOnLayoutListener(this);
            }
        } catch (Exception e2) {
            zzagf.m4793("Error while unregistering listeners from the ViewTreeObserver.", e2);
        }
        if (this.f4655 != null) {
            try {
                zzbs.zzfg().m4733(this.f4664, this.f4655);
            } catch (IllegalStateException e3) {
                zzagf.m4793("Failed trying to unregister the receiver", e3);
            } catch (Exception e4) {
                zzbs.zzem().m4505((Throwable) e4, "ActiveViewUnit.stopScreenStatusMonitoring");
            }
            this.f4655 = null;
        }
        if (this.f4666 != null) {
            try {
                this.f4666.unregisterActivityLifecycleCallbacks(this.f4661);
            } catch (Exception e5) {
                zzagf.m4793("Error registering activity lifecycle callbacks.", e5);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Rect m5350(Rect rect) {
        return new Rect(m5347(rect.left), m5347(rect.top), m5347(rect.right), m5347(rect.bottom));
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5351(int i) {
        if (this.f4658.size() != 0 && this.f4660 != null) {
            View view = (View) this.f4660.get();
            boolean z = i == 1;
            boolean z2 = view == null;
            Rect rect = new Rect();
            Rect rect2 = new Rect();
            boolean z3 = false;
            Rect rect3 = new Rect();
            boolean z4 = false;
            Rect rect4 = new Rect();
            Rect rect5 = new Rect();
            rect5.right = this.f4665.getDefaultDisplay().getWidth();
            rect5.bottom = this.f4665.getDefaultDisplay().getHeight();
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            if (view != null) {
                z3 = view.getGlobalVisibleRect(rect2);
                z4 = view.getLocalVisibleRect(rect3);
                view.getHitRect(rect4);
                try {
                    view.getLocationOnScreen(iArr);
                    view.getLocationInWindow(iArr2);
                } catch (Exception e) {
                    zzagf.m4793("Failure getting view location.", e);
                }
                rect.left = iArr[0];
                rect.top = iArr[1];
                rect.right = rect.left + view.getWidth();
                rect.bottom = rect.top + view.getHeight();
            }
            int windowVisibility = view != null ? view.getWindowVisibility() : 8;
            if (this.f4657 != -1) {
                windowVisibility = this.f4657;
            }
            boolean z5 = !z2 && zzbs.zzei().m4639(view, this.f4663, this.f4654) && z3 && z4 && windowVisibility == 0;
            if (z && !this.f4662.m4724() && z5 == this.f4659) {
                return;
            }
            if (z5 || this.f4659 || i != 1) {
                zzgs zzgs = new zzgs(zzbs.zzeo().m9241(), this.f4663.isScreenOn(), view != null ? zzbs.zzek().m4663(view) : false, view != null ? view.getWindowVisibility() : 8, m5350(rect5), m5350(rect), m5350(rect2), z3, m5350(rect3), z4, m5350(rect4), this.f4667.density, z5);
                Iterator<zzgt> it2 = this.f4658.iterator();
                while (it2.hasNext()) {
                    it2.next().m12971(zzgs);
                }
                this.f4659 = z5;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5352(Activity activity, int i) {
        Window window;
        if (this.f4660 != null && (window = activity.getWindow()) != null) {
            View peekDecorView = window.peekDecorView();
            View view = (View) this.f4660.get();
            if (view != null && peekDecorView != null && view.getRootView() == peekDecorView.getRootView()) {
                this.f4657 = i;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5353(View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            this.f4656 = new WeakReference<>(viewTreeObserver);
            viewTreeObserver.addOnScrollChangedListener(this);
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
        if (this.f4655 == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.f4655 = new zzgr(this);
            zzbs.zzfg().m4734(this.f4664, this.f4655, intentFilter);
        }
        if (this.f4666 != null) {
            try {
                this.f4666.registerActivityLifecycleCallbacks(this.f4661);
            } catch (Exception e) {
                zzagf.m4793("Error registering activity lifecycle callbacks.", e);
            }
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        m5352(activity, 0);
        m5351(3);
        m5348();
    }

    public final void onActivityDestroyed(Activity activity) {
        m5351(3);
        m5348();
    }

    public final void onActivityPaused(Activity activity) {
        m5352(activity, 4);
        m5351(3);
        m5348();
    }

    public final void onActivityResumed(Activity activity) {
        m5352(activity, 0);
        m5351(3);
        m5348();
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        m5351(3);
        m5348();
    }

    public final void onActivityStarted(Activity activity) {
        m5352(activity, 0);
        m5351(3);
        m5348();
    }

    public final void onActivityStopped(Activity activity) {
        m5351(3);
        m5348();
    }

    public final void onGlobalLayout() {
        m5351(2);
        m5348();
    }

    public final void onScrollChanged() {
        m5351(1);
    }

    public final void onViewAttachedToWindow(View view) {
        this.f4657 = -1;
        m5353(view);
        m5351(3);
    }

    public final void onViewDetachedFromWindow(View view) {
        this.f4657 = -1;
        m5351(3);
        m5348();
        m5349(view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5355(zzgt zzgt) {
        this.f4658.remove(zzgt);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5356() {
        m5351(4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5357(zzgt zzgt) {
        this.f4658.add(zzgt);
        m5351(3);
    }
}
