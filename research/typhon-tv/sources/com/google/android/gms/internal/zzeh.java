package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzeh extends zzet {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f10263 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile String f10264 = null;

    public zzeh(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12282() throws IllegalAccessException, InvocationTargetException {
        this.f10284.f8469 = "E";
        if (f10264 == null) {
            synchronized (f10263) {
                if (f10264 == null) {
                    f10264 = (String) this.f10286.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.f10284) {
            this.f10284.f8469 = f10264;
        }
    }
}
