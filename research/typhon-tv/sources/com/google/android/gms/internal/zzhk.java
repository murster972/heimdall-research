package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzq;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
@TargetApi(14)
public final class zzhk extends Thread {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzzt f4690;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f4691;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f4692;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f4693;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f4694;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f4695;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f4696;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f4697;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f4698;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzhf f4699;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f4700 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Object f4701;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4702 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f4703 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final String f4704;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final boolean f4705;

    public zzhk(zzhf zzhf, zzzt zzzt) {
        this.f4699 = zzhf;
        this.f4690 = zzzt;
        this.f4701 = new Object();
        this.f4692 = ((Integer) zzkb.m5481().m5595(zznh.f4974)).intValue();
        this.f4696 = ((Integer) zzkb.m5481().m5595(zznh.f5140)).intValue();
        this.f4697 = ((Integer) zzkb.m5481().m5595(zznh.f5028)).intValue();
        this.f4698 = ((Integer) zzkb.m5481().m5595(zznh.f5082)).intValue();
        this.f4695 = ((Integer) zzkb.m5481().m5595(zznh.f5109)).intValue();
        this.f4693 = ((Integer) zzkb.m5481().m5595(zznh.f5132)).intValue();
        this.f4694 = ((Integer) zzkb.m5481().m5595(zznh.f5136)).intValue();
        this.f4691 = ((Integer) zzkb.m5481().m5595(zznh.f5055)).intValue();
        this.f4704 = (String) zzkb.m5481().m5595(zznh.f5138);
        this.f4705 = ((Boolean) zzkb.m5481().m5595(zznh.f5152)).booleanValue();
        setName("ContentFetchTask");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m5381() {
        synchronized (this.f4701) {
            this.f4700 = true;
            zzagf.m4792(new StringBuilder(42).append("ContentFetchThread: paused, mPause = ").append(this.f4700).toString());
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean m5382() {
        try {
            Context r3 = zzbs.zzel().m5377();
            if (r3 == null) {
                return false;
            }
            ActivityManager activityManager = (ActivityManager) r3.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) r3.getSystemService("keyguard");
            if (activityManager == null || keyguardManager == null) {
                return false;
            }
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            Iterator<ActivityManager.RunningAppProcessInfo> it2 = runningAppProcesses.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it2.next();
                if (Process.myPid() == next.pid) {
                    if (next.importance == 100 && !keyguardManager.inKeyguardRestrictedInputMode()) {
                        PowerManager powerManager = (PowerManager) r3.getSystemService("power");
                        if (powerManager == null ? false : powerManager.isScreenOn()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            zzbs.zzem().m4505(th, "ContentFetchTask.isInForeground");
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzho m5383(View view, zzhe zzhe) {
        boolean z;
        if (view == null) {
            return new zzho(this, 0, 0);
        }
        Context r1 = zzbs.zzel().m5377();
        if (r1 != null) {
            String str = (String) view.getTag(r1.getResources().getIdentifier((String) zzkb.m5481().m5595(zznh.f5150), "id", r1.getPackageName()));
            if (!TextUtils.isEmpty(this.f4704) && str != null && str.equals(this.f4704)) {
                return new zzho(this, 0, 0);
            }
        }
        boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
        if ((view instanceof TextView) && !(view instanceof EditText)) {
            CharSequence text = ((TextView) view).getText();
            if (TextUtils.isEmpty(text)) {
                return new zzho(this, 0, 0);
            }
            zzhe.m5367(text.toString(), globalVisibleRect, view.getX(), view.getY(), (float) view.getWidth(), (float) view.getHeight());
            return new zzho(this, 1, 0);
        } else if ((view instanceof WebView) && !(view instanceof zzanh)) {
            zzhe.m5361();
            WebView webView = (WebView) view;
            if (!zzq.m9268()) {
                z = false;
            } else {
                zzhe.m5361();
                webView.post(new zzhm(this, zzhe, webView, globalVisibleRect));
                z = true;
            }
            return z ? new zzho(this, 0, 1) : new zzho(this, 0, 0);
        } else if (!(view instanceof ViewGroup)) {
            return new zzho(this, 0, 0);
        } else {
            ViewGroup viewGroup = (ViewGroup) view;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (i < viewGroup.getChildCount()) {
                zzho r3 = m5383(viewGroup.getChildAt(i), zzhe);
                i3 += r3.f4707;
                i++;
                i2 += r3.f4706;
            }
            return new zzho(this, i3, i2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0065, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        com.google.android.gms.internal.zzagf.m4793("Error in ContentFetchTask", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0080, code lost:
        com.google.android.gms.internal.zzagf.m4793("Error in ContentFetchTask", r0);
        r4.f4690.m13661(r0, "ContentFetchTask.run");
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0065 A[ExcHandler: InterruptedException (r0v1 'e' java.lang.InterruptedException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
        L_0x0000:
            boolean r0 = m5382()     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            if (r0 == 0) goto L_0x008f
            com.google.android.gms.internal.zzhg r0 = com.google.android.gms.ads.internal.zzbs.zzel()     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            android.app.Activity r1 = r0.m5378()     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            if (r1 != 0) goto L_0x0036
            java.lang.String r0 = "ContentFetchThread: no activity. Sleeping."
            com.google.android.gms.internal.zzagf.m4792(r0)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            r4.m5381()     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
        L_0x0019:
            int r0 = r4.f4691     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
        L_0x0021:
            java.lang.Object r1 = r4.f4701
            monitor-enter(r1)
        L_0x0024:
            boolean r0 = r4.f4700     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0099
            java.lang.String r0 = "ContentFetchTask: waiting"
            com.google.android.gms.internal.zzagf.m4792(r0)     // Catch:{ InterruptedException -> 0x0034 }
            java.lang.Object r0 = r4.f4701     // Catch:{ InterruptedException -> 0x0034 }
            r0.wait()     // Catch:{ InterruptedException -> 0x0034 }
            goto L_0x0024
        L_0x0034:
            r0 = move-exception
            goto L_0x0024
        L_0x0036:
            if (r1 == 0) goto L_0x0019
            r0 = 0
            android.view.Window r2 = r1.getWindow()     // Catch:{ Exception -> 0x006d, InterruptedException -> 0x0065 }
            if (r2 == 0) goto L_0x0058
            android.view.Window r2 = r1.getWindow()     // Catch:{ Exception -> 0x006d, InterruptedException -> 0x0065 }
            android.view.View r2 = r2.getDecorView()     // Catch:{ Exception -> 0x006d, InterruptedException -> 0x0065 }
            if (r2 == 0) goto L_0x0058
            android.view.Window r1 = r1.getWindow()     // Catch:{ Exception -> 0x006d, InterruptedException -> 0x0065 }
            android.view.View r1 = r1.getDecorView()     // Catch:{ Exception -> 0x006d, InterruptedException -> 0x0065 }
            r2 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r0 = r1.findViewById(r2)     // Catch:{ Exception -> 0x006d, InterruptedException -> 0x0065 }
        L_0x0058:
            if (r0 == 0) goto L_0x0019
            if (r0 == 0) goto L_0x0019
            com.google.android.gms.internal.zzhl r1 = new com.google.android.gms.internal.zzhl     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            r1.<init>(r4, r0)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            r0.post(r1)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            goto L_0x0019
        L_0x0065:
            r0 = move-exception
            java.lang.String r1 = "Error in ContentFetchTask"
            com.google.android.gms.internal.zzagf.m4793(r1, r0)
            goto L_0x0021
        L_0x006d:
            r1 = move-exception
            com.google.android.gms.internal.zzaft r2 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            java.lang.String r3 = "ContentFetchTask.extractContent"
            r2.m4505((java.lang.Throwable) r1, (java.lang.String) r3)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            java.lang.String r1 = "Failed getting root view of activity. Content not extracted."
            com.google.android.gms.internal.zzagf.m4792(r1)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            goto L_0x0058
        L_0x007f:
            r0 = move-exception
            java.lang.String r1 = "Error in ContentFetchTask"
            com.google.android.gms.internal.zzagf.m4793(r1, r0)
            com.google.android.gms.internal.zzzt r1 = r4.f4690
            java.lang.String r2 = "ContentFetchTask.run"
            r1.m13661(r0, r2)
            goto L_0x0021
        L_0x008f:
            java.lang.String r0 = "ContentFetchTask: sleeping"
            com.google.android.gms.internal.zzagf.m4792(r0)     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            r4.m5381()     // Catch:{ InterruptedException -> 0x0065, Exception -> 0x007f }
            goto L_0x0019
        L_0x0099:
            monitor-exit(r1)     // Catch:{ all -> 0x009c }
            goto L_0x0000
        L_0x009c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x009c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhk.run():void");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzhe m5384() {
        return this.f4699.m5375();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m5385() {
        return this.f4700;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5386() {
        synchronized (this.f4701) {
            this.f4700 = false;
            this.f4701.notifyAll();
            zzagf.m4792("ContentFetchThread: wakeup");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5387() {
        synchronized (this.f4701) {
            if (this.f4703) {
                zzagf.m4792("Content hash thread already started, quiting...");
                return;
            }
            this.f4703 = true;
            start();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5388(View view) {
        try {
            zzhe zzhe = new zzhe(this.f4692, this.f4696, this.f4697, this.f4698, this.f4695, this.f4693, this.f4694);
            zzho r1 = m5383(view, zzhe);
            zzhe.m5362();
            if (r1.f4707 != 0 || r1.f4706 != 0) {
                if (r1.f4706 != 0 || zzhe.m5364() != 0) {
                    if (r1.f4706 != 0 || !this.f4699.m5376(zzhe)) {
                        this.f4699.m5374(zzhe);
                    }
                }
            }
        } catch (Exception e) {
            zzagf.m4793("Exception in fetchContentOnUIThread", e);
            this.f4690.m13661(e, "ContentFetchTask.fetchContent");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5389(zzhe zzhe, WebView webView, String str, boolean z) {
        zzhe.m5360();
        try {
            if (!TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString(MimeTypes.BASE_TYPE_TEXT);
                if (this.f4705 || TextUtils.isEmpty(webView.getTitle())) {
                    zzhe.m5371(optString, z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                } else {
                    String title = webView.getTitle();
                    zzhe.m5371(new StringBuilder(String.valueOf(title).length() + 1 + String.valueOf(optString).length()).append(title).append(StringUtils.LF).append(optString).toString(), z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                }
            }
            if (zzhe.m5372()) {
                this.f4699.m5373(zzhe);
            }
        } catch (JSONException e) {
            zzagf.m4792("Json string may be malformed.");
        } catch (Throwable th) {
            zzagf.m4797("Failed to get webview content.", th);
            this.f4690.m13661(th, "ContentFetchTask.processWebViewContent");
        }
    }
}
