package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzep extends zzet {
    public zzep(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 48);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12290() throws IllegalAccessException, InvocationTargetException {
        this.f10284.f8475 = 2;
        boolean booleanValue = ((Boolean) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()})).booleanValue();
        synchronized (this.f10284) {
            if (booleanValue) {
                this.f10284.f8475 = 1;
            } else {
                this.f10284.f8475 = 0;
            }
        }
    }
}
