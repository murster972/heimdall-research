package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcmd extends zzfjm<zzcmd> {

    /* renamed from: 龘  reason: contains not printable characters */
    public zzcme[] f9723 = zzcme.m11500();

    public zzcmd() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcmd)) {
            return false;
        }
        zzcmd zzcmd = (zzcmd) obj;
        if (!zzfjq.m12863((Object[]) this.f9723, (Object[]) zzcmd.f9723)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcmd.f10533 == null || zzcmd.f10533.m12849() : this.f10533.equals(zzcmd.f10533);
    }

    public final int hashCode() {
        return ((this.f10533 == null || this.f10533.m12849()) ? 0 : this.f10533.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + zzfjq.m12859((Object[]) this.f9723)) * 31);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11497() {
        int r1 = super.m12841();
        if (this.f9723 != null && this.f9723.length > 0) {
            for (zzcme zzcme : this.f9723) {
                if (zzcme != null) {
                    r1 += zzfjk.m12807(1, (zzfjs) zzcme);
                }
            }
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11498(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    int r2 = zzfjv.m12883(zzfjj, 10);
                    int length = this.f9723 == null ? 0 : this.f9723.length;
                    zzcme[] zzcmeArr = new zzcme[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9723, 0, zzcmeArr, 0, length);
                    }
                    while (length < zzcmeArr.length - 1) {
                        zzcmeArr[length] = new zzcme();
                        zzfjj.m12802((zzfjs) zzcmeArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzcmeArr[length] = new zzcme();
                    zzfjj.m12802((zzfjs) zzcmeArr[length]);
                    this.f9723 = zzcmeArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11499(zzfjk zzfjk) throws IOException {
        if (this.f9723 != null && this.f9723.length > 0) {
            for (zzcme zzcme : this.f9723) {
                if (zzcme != null) {
                    zzfjk.m12834(1, (zzfjs) zzcme);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
