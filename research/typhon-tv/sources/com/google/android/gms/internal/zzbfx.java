package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzbfx extends zzbga {
    zzbfx(zzbfw zzbfw, GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10208(Api.zzb zzb) throws RemoteException {
        ((zzbge) ((zzbgb) zzb).m9171()).m10214(new zzbfy(this));
    }
}
