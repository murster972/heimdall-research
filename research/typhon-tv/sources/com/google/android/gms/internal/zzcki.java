package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzcki implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzckg f9563;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9564;

    zzcki(zzckg zzckg, zzcgi zzcgi) {
        this.f9563 = zzckg;
        this.f9564 = zzcgi;
    }

    public final void run() {
        zzche r0 = this.f9563.f9558;
        if (r0 == null) {
            this.f9563.m11096().m10832().m10849("Failed to reset data on the service; null service");
            return;
        }
        try {
            r0.m10675(this.f9564);
        } catch (RemoteException e) {
            this.f9563.m11096().m10832().m10850("Failed to reset data on the service", e);
        }
        this.f9563.m11223();
    }
}
