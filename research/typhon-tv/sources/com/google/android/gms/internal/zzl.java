package com.google.android.gms.internal;

import android.text.TextUtils;

public final class zzl {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f10791;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f10792;

    public zzl(String str, String str2) {
        this.f10792 = str;
        this.f10791 = str2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        zzl zzl = (zzl) obj;
        return TextUtils.equals(this.f10792, zzl.f10792) && TextUtils.equals(this.f10791, zzl.f10791);
    }

    public final int hashCode() {
        return (this.f10792.hashCode() * 31) + this.f10791.hashCode();
    }

    public final String toString() {
        String str = this.f10792;
        String str2 = this.f10791;
        return new StringBuilder(String.valueOf(str).length() + 20 + String.valueOf(str2).length()).append("Header[name=").append(str).append(",value=").append(str2).append("]").toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m13076() {
        return this.f10791;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13077() {
        return this.f10792;
    }
}
