package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@zzzv
public final class zzlt {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f4825;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Bundle f4826;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Map<Class<? extends NetworkExtras>, NetworkExtras> f4827;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Set<String> f4828;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Bundle f4829;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f4830;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f4831;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final String f4832;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final SearchAdRequest f4833;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Location f4834;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f4835;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Set<String> f4836;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f4837;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Date f4838;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Set<String> f4839;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final boolean f4840;

    public zzlt(zzlu zzlu) {
        this(zzlu, (SearchAdRequest) null);
    }

    public zzlt(zzlu zzlu, SearchAdRequest searchAdRequest) {
        this.f4838 = zzlu.龘(zzlu);
        this.f4835 = zzlu.靐(zzlu);
        this.f4837 = zzlu.齉(zzlu);
        this.f4836 = Collections.unmodifiableSet(zzlu.麤(zzlu));
        this.f4834 = zzlu.连任(zzlu);
        this.f4825 = zzlu.ʻ(zzlu);
        this.f4826 = zzlu.ʼ(zzlu);
        this.f4827 = Collections.unmodifiableMap(zzlu.ʽ(zzlu));
        this.f4831 = zzlu.ˑ(zzlu);
        this.f4832 = zzlu.ٴ(zzlu);
        this.f4833 = searchAdRequest;
        this.f4830 = zzlu.ᐧ(zzlu);
        this.f4828 = Collections.unmodifiableSet(zzlu.ˈ(zzlu));
        this.f4829 = zzlu.ʾ(zzlu);
        this.f4839 = Collections.unmodifiableSet(zzlu.ʿ(zzlu));
        this.f4840 = zzlu.ﹶ(zzlu);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m5491() {
        return this.f4825;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5492() {
        return this.f4831;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m5493() {
        return this.f4832;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final Bundle m5494() {
        return this.f4829;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Set<String> m5495() {
        return this.f4839;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int m5496() {
        return this.f4830;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final SearchAdRequest m5497() {
        return this.f4833;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final Map<Class<? extends NetworkExtras>, NetworkExtras> m5498() {
        return this.f4827;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final Bundle m5499() {
        return this.f4826;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Location m5500() {
        return this.f4834;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Bundle m5501(Class<? extends MediationAdapter> cls) {
        return this.f4826.getBundle(cls.getName());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5502() {
        return this.f4835;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Set<String> m5503() {
        return this.f4836;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m5504() {
        return this.f4837;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Bundle m5505(Class<? extends CustomEvent> cls) {
        Bundle bundle = this.f4826.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter");
        if (bundle != null) {
            return bundle.getBundle(cls.getName());
        }
        return null;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends NetworkExtras> T m5506(Class<T> cls) {
        return (NetworkExtras) this.f4827.get(cls);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Date m5507() {
        return this.f4838;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5508(Context context) {
        Set<String> set = this.f4828;
        zzkb.m5487();
        return set.contains(zzajr.m4759(context));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final boolean m5509() {
        return this.f4840;
    }
}
