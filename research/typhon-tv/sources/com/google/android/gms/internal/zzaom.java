package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

final class zzaom implements DialogInterface.OnCancelListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ JsPromptResult f8378;

    zzaom(JsPromptResult jsPromptResult) {
        this.f8378 = jsPromptResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f8378.cancel();
    }
}
