package com.google.android.gms.internal;

public final class zzw<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzc f10931;

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f10932;

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzad f10933;

    /* renamed from: 龘  reason: contains not printable characters */
    public final T f10934;

    private zzw(zzad zzad) {
        this.f10932 = false;
        this.f10934 = null;
        this.f10931 = null;
        this.f10933 = zzad;
    }

    private zzw(T t, zzc zzc) {
        this.f10932 = false;
        this.f10934 = t;
        this.f10931 = zzc;
        this.f10933 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> zzw<T> m13619(zzad zzad) {
        return new zzw<>(zzad);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> zzw<T> m13620(T t, zzc zzc) {
        return new zzw<>(t, zzc);
    }
}
