package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;

public final class zzchd<V> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzbey<V> f9245;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f9246;

    /* renamed from: 龘  reason: contains not printable characters */
    private final V f9247;

    private zzchd(String str, zzbey<V> zzbey, V v) {
        zzbq.m9120(zzbey);
        this.f9245 = zzbey;
        this.f9247 = v;
        this.f9246 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzchd<Integer> m10667(String str, int i, int i2) {
        return new zzchd<>(str, zzbey.m10142(str, Integer.valueOf(i2)), Integer.valueOf(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzchd<Long> m10668(String str, long j, long j2) {
        return new zzchd<>(str, zzbey.m10143(str, Long.valueOf(j2)), Long.valueOf(j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzchd<String> m10669(String str, String str2, String str3) {
        return new zzchd<>(str, zzbey.m10144(str, str3), str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzchd<Boolean> m10670(String str, boolean z, boolean z2) {
        return new zzchd<>(str, zzbey.m10145(str, z2), Boolean.valueOf(z));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final V m10671() {
        return this.f9247;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final V m10672(V v) {
        return v != null ? v : this.f9247;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10673() {
        return this.f9246;
    }
}
