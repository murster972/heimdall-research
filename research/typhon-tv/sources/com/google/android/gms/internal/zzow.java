package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.mopub.mobileads.VastIconXmlManager;
import java.lang.ref.WeakReference;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public class zzow implements zzos {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzzb f5264;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzou f5265;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzcv f5266;

    /* renamed from: ˈ  reason: contains not printable characters */
    private WeakReference<View> f5267 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzakd f5268;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f5269;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzafe f5270;

    /* renamed from: 连任  reason: contains not printable characters */
    private final JSONObject f5271;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f5272 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f5273;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzot f5274;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f5275;

    public zzow(Context context, zzot zzot, zzzb zzzb, zzcv zzcv, JSONObject jSONObject, zzou zzou, zzakd zzakd, String str) {
        this.f5273 = context;
        this.f5274 = zzot;
        this.f5264 = zzzb;
        this.f5266 = zzcv;
        this.f5271 = jSONObject;
        this.f5265 = zzou;
        this.f5268 = zzakd;
        this.f5269 = str;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static JSONObject m5731(View view) {
        JSONObject jSONObject = new JSONObject();
        if (view != null) {
            try {
                zzbs.zzei();
                jSONObject.put("contained_in_scroll_view", zzahn.m4584(view) != -1);
            } catch (Exception e) {
            }
        }
        return jSONObject;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final JSONObject m5732(View view) {
        JSONObject jSONObject = new JSONObject();
        if (view != null) {
            try {
                zzbs.zzei();
                jSONObject.put("can_show_on_lock_screen", zzahn.m4589(view));
                zzbs.zzei();
                jSONObject.put("is_keyguard_locked", zzahn.m4568(this.f5273));
            } catch (JSONException e) {
                zzagf.m4791("Unable to get lock screen information");
            }
        }
        return jSONObject;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final JSONObject m5733(View view) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        if (view != null) {
            try {
                int[] r2 = m5734(view);
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put(VastIconXmlManager.WIDTH, m5736(view.getMeasuredWidth()));
                jSONObject3.put(VastIconXmlManager.HEIGHT, m5736(view.getMeasuredHeight()));
                jSONObject3.put("x", m5736(r2[0]));
                jSONObject3.put("y", m5736(r2[1]));
                jSONObject3.put("relative_to", (Object) "window");
                jSONObject2.put("frame", (Object) jSONObject3);
                Rect rect = new Rect();
                if (view.getGlobalVisibleRect(rect)) {
                    jSONObject = m5737(rect);
                } else {
                    jSONObject = new JSONObject();
                    jSONObject.put(VastIconXmlManager.WIDTH, 0);
                    jSONObject.put(VastIconXmlManager.HEIGHT, 0);
                    jSONObject.put("x", m5736(r2[0]));
                    jSONObject.put("y", m5736(r2[1]));
                    jSONObject.put("relative_to", (Object) "window");
                }
                jSONObject2.put("visible_bounds", (Object) jSONObject);
            } catch (Exception e) {
                zzagf.m4791("Unable to get native ad view bounding box");
            }
        }
        return jSONObject2;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static int[] m5734(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return iArr;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean m5735(View view) {
        return view.isShown() && view.getGlobalVisibleRect(new Rect(), (Point) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m5736(int i) {
        zzkb.m5487();
        return zzajr.m4749(this.f5273, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final JSONObject m5737(Rect rect) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(VastIconXmlManager.WIDTH, m5736(rect.right - rect.left));
        jSONObject.put(VastIconXmlManager.HEIGHT, m5736(rect.bottom - rect.top));
        jSONObject.put("x", m5736(rect.left));
        jSONObject.put("y", m5736(rect.top));
        jSONObject.put("relative_to", (Object) "self");
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final JSONObject m5738(Map<String, WeakReference<View>> map, View view) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        if (map == null || view == null) {
            return jSONObject2;
        }
        int[] r4 = m5734(view);
        synchronized (map) {
            for (Map.Entry next : map.entrySet()) {
                View view2 = (View) ((WeakReference) next.getValue()).get();
                if (view2 != null) {
                    int[] r6 = m5734(view2);
                    JSONObject jSONObject3 = new JSONObject();
                    JSONObject jSONObject4 = new JSONObject();
                    try {
                        jSONObject4.put(VastIconXmlManager.WIDTH, m5736(view2.getMeasuredWidth()));
                        jSONObject4.put(VastIconXmlManager.HEIGHT, m5736(view2.getMeasuredHeight()));
                        jSONObject4.put("x", m5736(r6[0] - r4[0]));
                        jSONObject4.put("y", m5736(r6[1] - r4[1]));
                        jSONObject4.put("relative_to", (Object) "ad_view");
                        jSONObject3.put("frame", (Object) jSONObject4);
                        Rect rect = new Rect();
                        if (view2.getLocalVisibleRect(rect)) {
                            jSONObject = m5737(rect);
                        } else {
                            jSONObject = new JSONObject();
                            jSONObject.put(VastIconXmlManager.WIDTH, 0);
                            jSONObject.put(VastIconXmlManager.HEIGHT, 0);
                            jSONObject.put("x", m5736(r6[0] - r4[0]));
                            jSONObject.put("y", m5736(r6[1] - r4[1]));
                            jSONObject.put("relative_to", (Object) "ad_view");
                        }
                        jSONObject3.put("visible_bounds", (Object) jSONObject);
                        if (view2 instanceof TextView) {
                            TextView textView = (TextView) view2;
                            jSONObject3.put("text_color", textView.getCurrentTextColor());
                            jSONObject3.put("font_size", (double) textView.getTextSize());
                            jSONObject3.put(MimeTypes.BASE_TYPE_TEXT, (Object) textView.getText());
                        }
                        jSONObject2.put((String) next.getKey(), (Object) jSONObject3);
                    } catch (JSONException e) {
                        zzagf.m4791("Unable to get asset views information");
                    }
                }
            }
        }
        return jSONObject2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5739(View view, JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4, String str, JSONObject jSONObject5, JSONObject jSONObject6) {
        boolean z = true;
        zzbq.m9115("performClick must be called on the main UI thread.");
        try {
            JSONObject jSONObject7 = new JSONObject();
            jSONObject7.put("ad", (Object) this.f5271);
            if (jSONObject2 != null) {
                jSONObject7.put("asset_view_signal", (Object) jSONObject2);
            }
            if (jSONObject != null) {
                jSONObject7.put("ad_view_signal", (Object) jSONObject);
            }
            if (jSONObject5 != null) {
                jSONObject7.put("click_signal", (Object) jSONObject5);
            }
            if (jSONObject3 != null) {
                jSONObject7.put("scroll_view_signal", (Object) jSONObject3);
            }
            if (jSONObject4 != null) {
                jSONObject7.put("lock_screen_signal", (Object) jSONObject4);
            }
            JSONObject jSONObject8 = new JSONObject();
            jSONObject8.put("asset_id", (Object) str);
            jSONObject8.put("template", (Object) this.f5265.m13205());
            zzbs.zzek();
            jSONObject8.put("is_privileged_process", zzaht.m4640());
            jSONObject8.put("has_custom_click_handler", this.f5274.zzs(this.f5265.m13204()) != null);
            if (this.f5274.zzs(this.f5265.m13204()) == null) {
                z = false;
            }
            jSONObject7.put("has_custom_click_handler", z);
            try {
                JSONObject optJSONObject = this.f5271.optJSONObject("tracking_urls_and_actions");
                if (optJSONObject == null) {
                    optJSONObject = new JSONObject();
                }
                jSONObject8.put("click_signals", (Object) this.f5266.m11539().zza(this.f5273, optJSONObject.optString("click_string"), view));
            } catch (Exception e) {
                zzagf.m4793("Exception obtaining click signals", e);
            }
            jSONObject7.put("click", (Object) jSONObject8);
            if (jSONObject6 != null) {
                jSONObject7.put("provided_signals", (Object) jSONObject6);
            }
            jSONObject7.put("ads_id", (Object) this.f5269);
            zzakj.m4801(this.f5264.m6071(jSONObject7), "NativeAdEngineImpl.performClick");
        } catch (JSONException e2) {
            zzagf.m4793("Unable to create click JSON.", e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m5740(String str) {
        JSONObject optJSONObject = this.f5271 == null ? null : this.f5271.optJSONObject("allow_pub_event_reporting");
        if (optJSONObject == null) {
            return false;
        }
        return optJSONObject.optBoolean(str, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m5741(JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4, JSONObject jSONObject5) {
        zzbq.m9115("recordImpression must be called on the main UI thread.");
        if (this.f5275) {
            return true;
        }
        this.f5275 = true;
        try {
            JSONObject jSONObject6 = new JSONObject();
            jSONObject6.put("ad", (Object) this.f5271);
            jSONObject6.put("ads_id", (Object) this.f5269);
            if (jSONObject2 != null) {
                jSONObject6.put("asset_view_signal", (Object) jSONObject2);
            }
            if (jSONObject != null) {
                jSONObject6.put("ad_view_signal", (Object) jSONObject);
            }
            if (jSONObject3 != null) {
                jSONObject6.put("scroll_view_signal", (Object) jSONObject3);
            }
            if (jSONObject4 != null) {
                jSONObject6.put("lock_screen_signal", (Object) jSONObject4);
            }
            if (jSONObject5 != null) {
                jSONObject6.put("provided_signals", (Object) jSONObject5);
            }
            zzakj.m4801(this.f5264.m6073(jSONObject6), "NativeAdEngineImpl.recordImpression");
            this.f5274.zza((zzos) this);
            this.f5274.zzcc();
            return true;
        } catch (JSONException e) {
            zzagf.m4793("Unable to create impression JSON.", e);
            return false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m5742() {
        this.f5264.m6075();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m5743() {
        this.f5274.zzcv();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final View m5744() {
        if (this.f5267 != null) {
            return (View) this.f5267.get();
        }
        return null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final Context m5745() {
        return this.f5273;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final zzafe m5746() {
        if (!zzbs.zzfd().m4432(this.f5273)) {
            return null;
        }
        if (this.f5270 == null) {
            this.f5270 = new zzafe(this.f5273, this.f5274.getAdUnitId());
        }
        return this.f5270;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public zzanh m5747() throws zzanv {
        if (this.f5271 == null || this.f5271.optJSONObject("overlay") == null) {
            return null;
        }
        zzanr zzej = zzbs.zzej();
        Context context = this.f5273;
        zzjn r3 = zzjn.m5457(this.f5273);
        zzanh r1 = zzej.m5060(context, zzapa.m5224(r3), r3.f4798, false, false, this.f5266, this.f5268, (zznu) null, (zzbl) null, (zzv) null, zzis.m5432());
        if (r1 == null) {
            return r1;
        }
        if (r1 == null) {
            throw null;
        }
        ((View) r1).setVisibility(8);
        new zzoy(r1).m13212(this.f5264);
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5748(Bundle bundle) {
        if (bundle == null) {
            zzagf.m4792("Click data is null. No click is reported.");
        } else if (!m5740("click_reporting")) {
            zzagf.m4795("The ad slot cannot handle external click events. You must be whitelisted to be able to report your click events.");
        } else {
            m5739((View) null, (JSONObject) null, (JSONObject) null, (JSONObject) null, (JSONObject) null, bundle.getBundle("click_signal").getString("asset_id"), (JSONObject) null, zzbs.zzei().m4633(bundle, (JSONObject) null));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5749(View view) {
        this.f5267 = new WeakReference<>(view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m5750(View view, Map<String, WeakReference<View>> map) {
        if (!((Boolean) zzkb.m5481().m5595(zznh.f4988)).booleanValue()) {
            view.setOnTouchListener((View.OnTouchListener) null);
            view.setClickable(false);
            view.setOnClickListener((View.OnClickListener) null);
            if (map != null) {
                synchronized (map) {
                    for (Map.Entry<String, WeakReference<View>> value : map.entrySet()) {
                        View view2 = (View) ((WeakReference) value.getValue()).get();
                        if (view2 != null) {
                            view2.setOnTouchListener((View.OnTouchListener) null);
                            view2.setClickable(false);
                            view2.setOnClickListener((View.OnClickListener) null);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m5751() {
        return this.f5271 != null && this.f5271.optBoolean("allow_pub_owned_ad_view", false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5752(View view, zzoq zzoq) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
        View r2 = this.f5265.m13207();
        if (r2 == null) {
            return false;
        }
        ViewParent parent = r2.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(r2);
        }
        ((FrameLayout) view).removeAllViews();
        ((FrameLayout) view).addView(r2, layoutParams);
        this.f5274.zza(zzoq);
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5753(Bundle bundle) {
        if (bundle == null) {
            zzagf.m4792("Touch event data is null. No touch event is reported.");
        } else if (!m5740("touch_reporting")) {
            zzagf.m4795("The ad slot cannot handle external touch events. You must be whitelisted to be able to report your touch events.");
        } else {
            int i = bundle.getInt("duration_ms");
            this.f5266.m11539().zza((int) bundle.getFloat("x"), (int) bundle.getFloat("y"), i);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5754(View view, Map<String, WeakReference<View>> map) {
        synchronized (this.f5272) {
            if (!this.f5275) {
                if (m5735(view)) {
                    m5760(view, map);
                    return;
                }
                if (((Boolean) zzkb.m5481().m5595(zznh.f4997)).booleanValue() && map != null) {
                    synchronized (map) {
                        for (Map.Entry<String, WeakReference<View>> value : map.entrySet()) {
                            View view2 = (View) ((WeakReference) value.getValue()).get();
                            if (view2 != null && m5735(view2)) {
                                m5760(view, map);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public View m5755(View.OnClickListener onClickListener, boolean z) {
        zzog r0 = this.f5265.m13203();
        if (r0 == null) {
            return null;
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        if (!z) {
            switch (r0.m5643()) {
                case 0:
                    layoutParams.addRule(10);
                    layoutParams.addRule(9);
                    break;
                case 2:
                    layoutParams.addRule(12);
                    layoutParams.addRule(11);
                    break;
                case 3:
                    layoutParams.addRule(12);
                    layoutParams.addRule(9);
                    break;
                default:
                    layoutParams.addRule(10);
                    layoutParams.addRule(11);
                    break;
            }
        }
        zzoh zzoh = new zzoh(this.f5273, r0, layoutParams);
        zzoh.setOnClickListener(onClickListener);
        zzoh.setContentDescription((CharSequence) zzkb.m5481().m5595(zznh.f4991));
        return zzoh;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5756(MotionEvent motionEvent) {
        this.f5266.m11540(motionEvent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5757(View view) {
        zzcr r0;
        if (((Boolean) zzkb.m5481().m5595(zznh.f4966)).booleanValue() && this.f5266 != null && (r0 = this.f5266.m11539()) != null) {
            r0.zzb(view);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5758(View view, zzoq zzoq) {
        if (!m5752(view, zzoq)) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
            ((FrameLayout) view).removeAllViews();
            if (this.f5265 instanceof zzov) {
                zzov zzov = (zzov) this.f5265;
                if (zzov.m13208() != null && zzov.m13208().size() > 0) {
                    Object obj = zzov.m13208().get(0);
                    zzpq r0 = obj instanceof IBinder ? zzpr.m13226((IBinder) obj) : null;
                    if (r0 != null) {
                        try {
                            IObjectWrapper r02 = r0.m13225();
                            if (r02 != null) {
                                ImageView imageView = new ImageView(this.f5273);
                                imageView.setImageDrawable((Drawable) zzn.m9307(r02));
                                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                ((FrameLayout) view).addView(imageView, layoutParams);
                            }
                        } catch (RemoteException e) {
                            zzagf.m4791("Could not get drawable from image");
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5759(View view, String str, Bundle bundle, Map<String, WeakReference<View>> map, View view2) {
        JSONObject jSONObject;
        JSONObject r3 = m5738(map, view2);
        JSONObject r2 = m5733(view2);
        JSONObject r4 = m5731(view2);
        JSONObject r5 = m5732(view2);
        try {
            JSONObject r0 = zzbs.zzei().m4633(bundle, (JSONObject) null);
            jSONObject = new JSONObject();
            try {
                jSONObject.put("click_point", (Object) r0);
                jSONObject.put("asset_id", (Object) str);
            } catch (Exception e) {
                e = e;
                zzagf.m4793("Error occurred while grabbing click signals.", e);
                m5739(view, r2, r3, r4, r5, str, jSONObject, (JSONObject) null);
            }
        } catch (Exception e2) {
            e = e2;
            jSONObject = null;
            zzagf.m4793("Error occurred while grabbing click signals.", e);
            m5739(view, r2, r3, r4, r5, str, jSONObject, (JSONObject) null);
        }
        m5739(view, r2, r3, r4, r5, str, jSONObject, (JSONObject) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m5760(View view, Map<String, WeakReference<View>> map) {
        m5741(m5733(view), m5738(map, view), m5731(view), m5732(view), (JSONObject) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m5761(View view, Map<String, WeakReference<View>> map, Bundle bundle, View view2) {
        zzbq.m9115("performClick must be called on the main UI thread.");
        if (map != null) {
            synchronized (map) {
                for (Map.Entry next : map.entrySet()) {
                    if (view.equals((View) ((WeakReference) next.getValue()).get())) {
                        m5759(view, (String) next.getKey(), bundle, map, view2);
                        return;
                    }
                }
            }
        }
        if ("2".equals(this.f5265.m13205())) {
            m5759(view, "2099", bundle, map, view2);
        } else if (PubnativeRequest.LEGACY_ZONE_ID.equals(this.f5265.m13205())) {
            m5759(view, "1099", bundle, map, view2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m5762(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, View.OnTouchListener onTouchListener, View.OnClickListener onClickListener) {
        if (((Boolean) zzkb.m5481().m5595(zznh.f4989)).booleanValue()) {
            view.setOnTouchListener(onTouchListener);
            view.setClickable(true);
            view.setOnClickListener(onClickListener);
            if (map != null) {
                synchronized (map) {
                    for (Map.Entry<String, WeakReference<View>> value : map.entrySet()) {
                        View view2 = (View) ((WeakReference) value.getValue()).get();
                        if (view2 != null) {
                            view2.setOnTouchListener(onTouchListener);
                            view2.setClickable(true);
                            view2.setOnClickListener(onClickListener);
                        }
                    }
                }
            }
            if (map2 != null) {
                synchronized (map2) {
                    for (Map.Entry<String, WeakReference<View>> value2 : map2.entrySet()) {
                        View view3 = (View) ((WeakReference) value2.getValue()).get();
                        if (view3 != null) {
                            view3.setOnTouchListener(onTouchListener);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5763(Map<String, WeakReference<View>> map) {
        if (this.f5265.m13207() == null) {
            return;
        }
        if ("2".equals(this.f5265.m13205())) {
            zzbs.zzem().m4497(this.f5273, this.f5274.getAdUnitId(), this.f5265.m13205(), map.containsKey(NativeAppInstallAd.ASSET_MEDIA_VIDEO));
        } else if (PubnativeRequest.LEGACY_ZONE_ID.equals(this.f5265.m13205())) {
            zzbs.zzem().m4497(this.f5273, this.f5274.getAdUnitId(), this.f5265.m13205(), map.containsKey(NativeContentAd.ASSET_MEDIA_VIDEO));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m5764() {
        zzog r0 = this.f5265.m13203();
        return r0 != null && r0.m5644();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5765(Bundle bundle) {
        if (!m5740("impression_reporting")) {
            zzagf.m4795("The ad slot cannot handle external impression events. You must be whitelisted to whitelisted to be able to report your impression events.");
            return false;
        }
        return m5741((JSONObject) null, (JSONObject) null, (JSONObject) null, (JSONObject) null, zzbs.zzei().m4633(bundle, (JSONObject) null));
    }
}
