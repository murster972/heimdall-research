package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbg;
import java.util.Arrays;

public final class zzait {

    /* renamed from: 连任  reason: contains not printable characters */
    private double f8232;

    /* renamed from: 靐  reason: contains not printable characters */
    public final double f8233;

    /* renamed from: 麤  reason: contains not printable characters */
    private double f8234;

    /* renamed from: 齉  reason: contains not printable characters */
    public final int f8235;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f8236;

    public zzait(String str, double d, double d2, double d3, int i) {
        this.f8236 = str;
        this.f8232 = d;
        this.f8234 = d2;
        this.f8233 = d3;
        this.f8235 = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzait)) {
            return false;
        }
        zzait zzait = (zzait) obj;
        return zzbg.m9113(this.f8236, zzait.f8236) && this.f8234 == zzait.f8234 && this.f8232 == zzait.f8232 && this.f8235 == zzait.f8235 && Double.compare(this.f8233, zzait.f8233) == 0;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f8236, Double.valueOf(this.f8234), Double.valueOf(this.f8232), Double.valueOf(this.f8233), Integer.valueOf(this.f8235)});
    }

    public final String toString() {
        return zzbg.m9112(this).m9114("name", this.f8236).m9114("minBound", Double.valueOf(this.f8232)).m9114("maxBound", Double.valueOf(this.f8234)).m9114("percent", Double.valueOf(this.f8233)).m9114("count", Integer.valueOf(this.f8235)).toString();
    }
}
