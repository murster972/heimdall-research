package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagx extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8185;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8186;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagx(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8186 = context;
        this.f8185 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9596() {
        SharedPreferences sharedPreferences = this.f8186.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putInt("request_in_session_count", sharedPreferences.getInt("request_in_session_count", -1));
        if (this.f8185 != null) {
            this.f8185.m9605(bundle);
        }
    }
}
