package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagj extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8157;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8158;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagj(Context context, String str) {
        super((zzagi) null);
        this.f8158 = context;
        this.f8157 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9582() {
        SharedPreferences.Editor edit = this.f8158.getSharedPreferences("admob", 0).edit();
        edit.putString("content_vertical_hashes", this.f8157);
        edit.apply();
    }
}
