package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzfl extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    void m12927(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m12928() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m12929(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m12930(IObjectWrapper iObjectWrapper, String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m12931(IObjectWrapper iObjectWrapper, String str, String str2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m12932(byte[] bArr) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m12933(int[] iArr) throws RemoteException;
}
