package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;

@zzzv
public final class zzaoz extends MutableContextWrapper {

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f4534;

    /* renamed from: 齉  reason: contains not printable characters */
    private Context f4535;

    /* renamed from: 龘  reason: contains not printable characters */
    private Activity f4536;

    public zzaoz(Context context) {
        super(context);
        setBaseContext(context);
    }

    public final Object getSystemService(String str) {
        return this.f4535.getSystemService(str);
    }

    public final void setBaseContext(Context context) {
        this.f4534 = context.getApplicationContext();
        this.f4536 = context instanceof Activity ? (Activity) context : null;
        this.f4535 = context;
        super.setBaseContext(this.f4534);
    }

    public final void startActivity(Intent intent) {
        if (this.f4536 != null) {
            this.f4536.startActivity(intent);
            return;
        }
        intent.setFlags(268435456);
        this.f4534.startActivity(intent);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context m5219() {
        return this.f4535;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Activity m5220() {
        return this.f4536;
    }
}
