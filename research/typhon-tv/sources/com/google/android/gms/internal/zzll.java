package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzll extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    float m13082() throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    float m13083() throws RemoteException;

    /* renamed from: ʽ  reason: contains not printable characters */
    zzlo m13084() throws RemoteException;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean m13085() throws RemoteException;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean m13086() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    float m13087() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m13088() throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    int m13089() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m13090() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13091() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13092(zzlo zzlo) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13093(boolean z) throws RemoteException;
}
