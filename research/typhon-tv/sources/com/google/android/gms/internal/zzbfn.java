package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.internal.view.SupportMenu;
import java.util.ArrayList;

public final class zzbfn {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Long m10150(Parcel parcel, int i) {
        int r0 = m10170(parcel, i);
        if (r0 == 0) {
            return null;
        }
        m10173(parcel, i, r0, 8);
        return Long.valueOf(parcel.readLong());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static float m10151(Parcel parcel, int i) {
        m10172(parcel, i, 4);
        return parcel.readFloat();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static Float m10152(Parcel parcel, int i) {
        int r0 = m10170(parcel, i);
        if (r0 == 0) {
            return null;
        }
        m10173(parcel, i, r0, 4);
        return Float.valueOf(parcel.readFloat());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static Bundle m10153(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(r1 + dataPosition);
        return readBundle;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static byte[] m10154(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(r1 + dataPosition);
        return createByteArray;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public static void m10155(Parcel parcel, int i) {
        if (parcel.dataPosition() != i) {
            throw new zzbfo(new StringBuilder(37).append("Overread allowed size end=").append(i).toString(), parcel);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static IBinder m10156(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(r1 + dataPosition);
        return readStrongBinder;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static String[] m10157(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(r1 + dataPosition);
        return createStringArray;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static ArrayList<Integer> m10158(Parcel parcel, int i) {
        int r2 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r2 == 0) {
            return null;
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        int readInt = parcel.readInt();
        for (int i2 = 0; i2 < readInt; i2++) {
            arrayList.add(Integer.valueOf(parcel.readInt()));
        }
        parcel.setDataPosition(dataPosition + r2);
        return arrayList;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static ArrayList<String> m10159(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(r1 + dataPosition);
        return createStringArrayList;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static double m10160(Parcel parcel, int i) {
        m10172(parcel, i, 8);
        return parcel.readDouble();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static Double m10161(Parcel parcel, int i) {
        int r0 = m10170(parcel, i);
        if (r0 == 0) {
            return null;
        }
        m10173(parcel, i, r0, 8);
        return Double.valueOf(parcel.readDouble());
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static String m10162(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(r1 + dataPosition);
        return readString;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static long m10163(Parcel parcel, int i) {
        m10172(parcel, i, 8);
        return parcel.readLong();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m10164(Parcel parcel, int i) {
        parcel.setDataPosition(m10170(parcel, i) + parcel.dataPosition());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> T[] m10165(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        T[] createTypedArray = parcel.createTypedArray(creator);
        parcel.setDataPosition(r1 + dataPosition);
        return createTypedArray;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m10166(Parcel parcel, int i) {
        m10172(parcel, i, 4);
        return parcel.readInt();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static <T> ArrayList<T> m10167(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(r1 + dataPosition);
        return createTypedArrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m10168(Parcel parcel, int i) {
        m10172(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m10169(Parcel parcel) {
        int readInt = parcel.readInt();
        int r1 = m10170(parcel, readInt);
        int dataPosition = parcel.dataPosition();
        if ((65535 & readInt) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(readInt));
            throw new zzbfo(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i = dataPosition + r1;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        throw new zzbfo(new StringBuilder(54).append("Size read is invalid start=").append(dataPosition).append(" end=").append(i).toString(), parcel);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m10170(Parcel parcel, int i) {
        return (i & SupportMenu.CATEGORY_MASK) != -65536 ? (i >> 16) & 65535 : parcel.readInt();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends Parcelable> T m10171(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        T t = (Parcelable) creator.createFromParcel(parcel);
        parcel.setDataPosition(r1 + dataPosition);
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10172(Parcel parcel, int i, int i2) {
        int r0 = m10170(parcel, i);
        if (r0 != i2) {
            String hexString = Integer.toHexString(r0);
            throw new zzbfo(new StringBuilder(String.valueOf(hexString).length() + 46).append("Expected size ").append(i2).append(" got ").append(r0).append(" (0x").append(hexString).append(")").toString(), parcel);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10173(Parcel parcel, int i, int i2, int i3) {
        if (i2 != i3) {
            String hexString = Integer.toHexString(i2);
            throw new zzbfo(new StringBuilder(String.valueOf(hexString).length() + 46).append("Expected size ").append(i3).append(" got ").append(i2).append(" (0x").append(hexString).append(")").toString(), parcel);
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static int[] m10174(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(r1 + dataPosition);
        return createIntArray;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static long[] m10175(Parcel parcel, int i) {
        int r1 = m10170(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (r1 == 0) {
            return null;
        }
        long[] createLongArray = parcel.createLongArray();
        parcel.setDataPosition(r1 + dataPosition);
        return createLongArray;
    }
}
