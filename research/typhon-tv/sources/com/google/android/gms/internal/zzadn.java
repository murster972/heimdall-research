package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzadn extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m9496(IObjectWrapper iObjectWrapper, zzux zzux, int i) throws RemoteException;
}
