package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;

final class zzafm implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzalf f8143;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8144;

    zzafm(zzafl zzafl, Context context, zzalf zzalf) {
        this.f8144 = context;
        this.f8143 = zzalf;
    }

    public final void run() {
        try {
            this.f8143.m4822(AdvertisingIdClient.getAdvertisingIdInfo(this.f8144));
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e) {
            this.f8143.m4824(e);
            zzakb.m4793("Exception while getting advertising Id info", e);
        }
    }
}
