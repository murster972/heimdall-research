package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

public final class zzdr {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final char[] f9958 = "0123456789abcdef".toCharArray();

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m11759() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m11760(String str) {
        return str == null || str.isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m11761(double d, int i, DisplayMetrics displayMetrics) {
        return Math.round((((double) i) * d) / ((double) displayMetrics.density));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Activity m11762(View view) {
        int i = 0;
        View rootView = view.getRootView();
        if (rootView != null) {
            view = rootView;
        }
        Context context = view.getContext();
        while ((context instanceof ContextWrapper) && i < 10) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
            i++;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Long m11763() {
        return Long.valueOf(Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles")).getTime().getTime());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m11764(String str) {
        if (str == null || !str.matches("^[a-fA-F0-9]{8}-([a-fA-F0-9]{4}-){3}[a-fA-F0-9]{12}$")) {
            return str;
        }
        UUID fromString = UUID.fromString(str);
        byte[] bArr = new byte[16];
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        wrap.putLong(fromString.getMostSignificantBits());
        wrap.putLong(fromString.getLeastSignificantBits());
        return zzbu.m10295(bArr, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m11765(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        zzdvl.m12240(th, new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m11766(DisplayMetrics displayMetrics) {
        return (displayMetrics == null || displayMetrics.density == 0.0f) ? false : true;
    }
}
