package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzeo extends zzet {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f10268 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile Long f10269 = null;

    public zzeo(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 33);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12289() throws IllegalAccessException, InvocationTargetException {
        if (f10269 == null) {
            synchronized (f10268) {
                if (f10269 == null) {
                    f10269 = (Long) this.f10286.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.f10284) {
            this.f10284.f8441 = f10269;
        }
    }
}
