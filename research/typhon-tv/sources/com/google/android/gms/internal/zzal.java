package com.google.android.gms.internal;

import android.os.SystemClock;
import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class zzal implements zzb {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f8290;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f8291;

    /* renamed from: 齉  reason: contains not printable characters */
    private final File f8292;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, zzam> f8293;

    public zzal(File file) {
        this(file, 5242880);
    }

    private zzal(File file, int i) {
        this.f8293 = new LinkedHashMap(16, 0.75f, true);
        this.f8290 = 0;
        this.f8292 = file;
        this.f8291 = 5242880;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9671(String str) {
        zzam remove = this.f8293.remove(str);
        if (remove != null) {
            this.f8290 -= remove.f8317;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static long m9672(InputStream inputStream) throws IOException {
        return 0 | (((long) m9676(inputStream)) & 255) | ((((long) m9676(inputStream)) & 255) << 8) | ((((long) m9676(inputStream)) & 255) << 16) | ((((long) m9676(inputStream)) & 255) << 24) | ((((long) m9676(inputStream)) & 255) << 32) | ((((long) m9676(inputStream)) & 255) << 40) | ((((long) m9676(inputStream)) & 255) << 48) | ((((long) m9676(inputStream)) & 255) << 56);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static List<zzl> m9673(zzan zzan) throws IOException {
        int r2 = m9678((InputStream) zzan);
        List<zzl> emptyList = r2 == 0 ? Collections.emptyList() : new ArrayList<>(r2);
        for (int i = 0; i < r2; i++) {
            emptyList.add(new zzl(m9680(zzan).intern(), m9680(zzan).intern()));
        }
        return emptyList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final synchronized void m9674(String str) {
        boolean delete = m9675(str).delete();
        m9671(str);
        if (!delete) {
            zzae.m9513("Could not delete cache entry for key=%s, filename=%s", str, m9677(str));
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final File m9675(String str) {
        return new File(this.f8292, m9677(str));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m9676(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m9677(String str) {
        int length = str.length() / 2;
        String valueOf = String.valueOf(String.valueOf(str.substring(0, length).hashCode()));
        String valueOf2 = String.valueOf(String.valueOf(str.substring(length).hashCode()));
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m9678(InputStream inputStream) throws IOException {
        return m9676(inputStream) | 0 | (m9676(inputStream) << 8) | (m9676(inputStream) << 16) | (m9676(inputStream) << 24);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static InputStream m9679(File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m9680(zzan zzan) throws IOException {
        return new String(m9685(zzan, m9672((InputStream) zzan)), "UTF-8");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m9681(OutputStream outputStream, int i) throws IOException {
        outputStream.write(i & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write(i >>> 24);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m9682(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) j));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m9683(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        m9682(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9684(String str, zzam zzam) {
        if (!this.f8293.containsKey(str)) {
            this.f8290 += zzam.f8317;
        } else {
            this.f8290 = (zzam.f8317 - this.f8293.get(str).f8317) + this.f8290;
        }
        this.f8293.put(str, zzam);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m9685(zzan zzan, long j) throws IOException {
        long r0 = zzan.m9705();
        if (j < 0 || j > r0 || ((long) ((int) j)) != j) {
            throw new IOException(new StringBuilder(73).append("streamToBytes length=").append(j).append(", maxLength=").append(r0).toString());
        }
        byte[] bArr = new byte[((int) j)];
        new DataInputStream(zzan).readFully(bArr);
        return bArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized zzc m9686(String str) {
        zzc zzc;
        zzan zzan;
        zzam zzam = this.f8293.get(str);
        if (zzam == null) {
            zzc = null;
        } else {
            File r3 = m9675(str);
            try {
                zzan = new zzan(new BufferedInputStream(m9679(r3)), r3.length());
                zzam r2 = zzam.m9692(zzan);
                if (!TextUtils.equals(str, r2.f8314)) {
                    zzae.m9513("%s: key=%s, found=%s", r3.getAbsolutePath(), str, r2.f8314);
                    m9671(str);
                    zzan.close();
                    zzc = null;
                } else {
                    byte[] r5 = m9685(zzan, zzan.m9705());
                    zzc zzc2 = new zzc();
                    zzc2.f8823 = r5;
                    zzc2.f8820 = zzam.f8316;
                    zzc2.f8822 = zzam.f8315;
                    zzc2.f8821 = zzam.f8313;
                    zzc2.f8819 = zzam.f8310;
                    zzc2.f8816 = zzam.f8311;
                    zzc2.f8817 = zzao.m9719(zzam.f8312);
                    zzc2.f8818 = Collections.unmodifiableList(zzam.f8312);
                    zzan.close();
                    zzc = zzc2;
                }
            } catch (IOException e) {
                zzae.m9513("%s: %s", r3.getAbsolutePath(), e.toString());
                m9674(str);
                zzc = null;
            } catch (Throwable th) {
                zzan.close();
                throw th;
            }
        }
        return zzc;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m9687() {
        zzan zzan;
        if (this.f8292.exists()) {
            File[] listFiles = this.f8292.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    try {
                        long length = file.length();
                        zzan = new zzan(new BufferedInputStream(m9679(file)), length);
                        zzam r0 = zzam.m9692(zzan);
                        r0.f8317 = length;
                        m9684(r0.f8314, r0);
                        zzan.close();
                    } catch (IOException e) {
                        file.delete();
                    } catch (Throwable th) {
                        zzan.close();
                        throw th;
                    }
                }
            }
        } else if (!this.f8292.mkdirs()) {
            zzae.m9515("Unable to create cache dir %s", this.f8292.getAbsolutePath());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m9688(String str, zzc zzc) {
        int i;
        int i2 = 0;
        synchronized (this) {
            int length = zzc.f8823.length;
            if (this.f8290 + ((long) length) >= ((long) this.f8291)) {
                if (zzae.f8117) {
                    zzae.m9516("Pruning old cache entries.", new Object[0]);
                }
                long j = this.f8290;
                long elapsedRealtime = SystemClock.elapsedRealtime();
                Iterator<Map.Entry<String, zzam>> it2 = this.f8293.entrySet().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        i = i2;
                        break;
                    }
                    zzam zzam = (zzam) it2.next().getValue();
                    if (m9675(zzam.f8314).delete()) {
                        this.f8290 -= zzam.f8317;
                    } else {
                        zzae.m9513("Could not delete cache entry for key=%s, filename=%s", zzam.f8314, m9677(zzam.f8314));
                    }
                    it2.remove();
                    i = i2 + 1;
                    if (((float) (this.f8290 + ((long) length))) < ((float) this.f8291) * 0.9f) {
                        break;
                    }
                    i2 = i;
                }
                if (zzae.f8117) {
                    zzae.m9516("pruned %d files, %d bytes, %d ms", Integer.valueOf(i), Long.valueOf(this.f8290 - j), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
                }
            }
            File r0 = m9675(str);
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(r0));
                zzam zzam2 = new zzam(str, zzc);
                if (!zzam2.m9693((OutputStream) bufferedOutputStream)) {
                    bufferedOutputStream.close();
                    zzae.m9513("Failed to write header for %s", r0.getAbsolutePath());
                    throw new IOException();
                }
                bufferedOutputStream.write(zzc.f8823);
                bufferedOutputStream.close();
                m9684(str, zzam2);
            } catch (IOException e) {
                if (!r0.delete()) {
                    zzae.m9513("Could not clean up file %s", r0.getAbsolutePath());
                }
            }
        }
    }
}
