package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzlj extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m13080(IObjectWrapper iObjectWrapper, int i) throws RemoteException;
}
