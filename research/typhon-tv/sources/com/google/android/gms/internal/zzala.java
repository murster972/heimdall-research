package com.google.android.gms.internal;

import java.util.concurrent.Executor;

@zzzv
public final class zzala {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Executor f4304 = new zzalc();

    /* renamed from: 麤  reason: contains not printable characters */
    private static zzakz f4305 = m4818(f4304);

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzakz f4306 = m4818(f4307);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Executor f4307 = new zzalb();

    /* JADX WARNING: type inference failed for: r0v0, types: [com.google.android.gms.internal.zzald, com.google.android.gms.internal.zzakz] */
    /* renamed from: 龘  reason: contains not printable characters */
    private static zzakz m4818(Executor executor) {
        return new zzald(executor, (zzalb) null);
    }
}
