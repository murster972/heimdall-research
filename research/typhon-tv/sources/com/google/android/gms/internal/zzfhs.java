package com.google.android.gms.internal;

import java.util.Arrays;
import java.util.Stack;

final class zzfhs {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Stack<zzfes> f10455;

    private zzfhs() {
        this.f10455 = new Stack<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m12660(int i) {
        int binarySearch = Arrays.binarySearch(zzfhq.f10454, i);
        return binarySearch < 0 ? (-(binarySearch + 1)) - 1 : binarySearch;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzfes m12661(zzfes zzfes, zzfes zzfes2) {
        m12663(zzfes);
        m12663(zzfes2);
        zzfhq pop = this.f10455.pop();
        while (!this.f10455.isEmpty()) {
            pop = new zzfhq(this.f10455.pop(), pop);
        }
        return pop;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m12663(zzfes zzfes) {
        zzfes zzfes2 = zzfes;
        while (!zzfes2.m12375()) {
            if (zzfes2 instanceof zzfhq) {
                zzfhq zzfhq = (zzfhq) zzfes2;
                m12663(zzfhq.zzpji);
                zzfes2 = zzfhq.zzpjj;
            } else {
                String valueOf = String.valueOf(zzfes2.getClass());
                throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 49).append("Has a new type of ByteString been created? Found ").append(valueOf).toString());
            }
        }
        int r2 = m12660(zzfes2.size());
        int i = zzfhq.f10454[r2 + 1];
        if (this.f10455.isEmpty() || this.f10455.peek().size() >= i) {
            this.f10455.push(zzfes2);
            return;
        }
        int i2 = zzfhq.f10454[r2];
        zzfhq pop = this.f10455.pop();
        while (!this.f10455.isEmpty() && this.f10455.peek().size() < i2) {
            pop = new zzfhq(this.f10455.pop(), pop);
        }
        zzfhq zzfhq2 = new zzfhq(pop, zzfes2);
        while (!this.f10455.isEmpty()) {
            if (this.f10455.peek().size() >= zzfhq.f10454[m12660(zzfhq2.size()) + 1]) {
                break;
            }
            zzfhq2 = new zzfhq(this.f10455.pop(), zzfhq2);
        }
        this.f10455.push(zzfhq2);
    }
}
