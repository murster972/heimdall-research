package com.google.android.gms.internal;

@zzzv
final class zzaai extends Exception {
    private final int mErrorCode;

    public zzaai(String str, int i) {
        super(str);
        this.mErrorCode = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m4241() {
        return this.mErrorCode;
    }
}
