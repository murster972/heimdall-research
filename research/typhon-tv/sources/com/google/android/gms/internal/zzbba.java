package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbba extends UIController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f8609;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8610;

    public zzbba(View view, int i) {
        this.f8610 = view;
        this.f8609 = i;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9952() {
        RemoteMediaClient r0 = m8226();
        if (r0 == null || !r0.m4143()) {
            this.f8610.setVisibility(this.f8609);
        } else {
            this.f8610.setVisibility(0);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9953() {
        this.f8610.setVisibility(this.f8609);
        super.m8223();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9954() {
        m9952();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9955(CastSession castSession) {
        super.m8227(castSession);
        m9952();
    }
}
