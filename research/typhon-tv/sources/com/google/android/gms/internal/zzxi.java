package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzxi extends zzeu implements zzxh {
    zzxi(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.overlay.client.IAdOverlayCreator");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m13633(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        Parcel r0 = m12300(1, v_);
        IBinder readStrongBinder = r0.readStrongBinder();
        r0.recycle();
        return readStrongBinder;
    }
}
