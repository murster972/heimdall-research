package com.google.android.gms.internal;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class zzbhp {
    /* renamed from: 靐  reason: contains not printable characters */
    private static AppMeasurement m10254(Context context) {
        try {
            return AppMeasurement.getInstance(context);
        } catch (NoClassDefFoundError e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<zzbhn> m10255(Context context) {
        Map map;
        AppMeasurement r1 = m10254(context);
        if (r1 != null) {
            try {
                map = r1.getUserProperties(false);
            } catch (NullPointerException e) {
                if (Log.isLoggable("FRCAnalytics", 3)) {
                    Log.d("FRCAnalytics", "Unable to get user properties.", e);
                }
                map = null;
            }
            if (map == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Map.Entry entry : map.entrySet()) {
                if (entry.getValue() != null) {
                    arrayList.add(new zzbhn((String) entry.getKey(), entry.getValue().toString()));
                }
            }
            return arrayList;
        } else if (!Log.isLoggable("FRCAnalytics", 3)) {
            return null;
        } else {
            Log.d("FRCAnalytics", "Unable to get user properties: analytics library is missing.");
            return null;
        }
    }
}
