package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.Map;
import java.util.WeakHashMap;

@zzzv
public final class zzajn {

    /* renamed from: 连任  reason: contains not printable characters */
    private Context f4264;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<BroadcastReceiver, IntentFilter> f4265 = new WeakHashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f4266;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4267 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final BroadcastReceiver f4268 = new zzajo(this);

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m4730(Context context, Intent intent) {
        for (Map.Entry next : this.f4265.entrySet()) {
            if (((IntentFilter) next.getValue()).hasAction(intent.getAction())) {
                ((BroadcastReceiver) next.getKey()).onReceive(context, intent);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m4732(Context context) {
        if (!this.f4267) {
            this.f4264 = context.getApplicationContext();
            if (this.f4264 == null) {
                this.f4264 = context;
            }
            zznh.m5599(this.f4264);
            this.f4266 = ((Boolean) zzkb.m5481().m5595(zznh.f4994)).booleanValue();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.f4264.registerReceiver(this.f4268, intentFilter);
            this.f4267 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m4733(Context context, BroadcastReceiver broadcastReceiver) {
        if (this.f4266) {
            this.f4265.remove(broadcastReceiver);
        } else {
            context.unregisterReceiver(broadcastReceiver);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m4734(Context context, BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        if (this.f4266) {
            this.f4265.put(broadcastReceiver, intentFilter);
        } else {
            context.registerReceiver(broadcastReceiver, intentFilter);
        }
    }
}
