package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.gmsg.zza;
import com.google.android.gms.ads.internal.gmsg.zzaa;
import com.google.android.gms.ads.internal.gmsg.zzab;
import com.google.android.gms.ads.internal.gmsg.zzb;
import com.google.android.gms.ads.internal.gmsg.zzd;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.gmsg.zzx;
import com.google.android.gms.ads.internal.gmsg.zzz;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzl;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzq;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzw;
import com.google.android.gms.common.util.zzr;
import com.mopub.common.TyphoonApp;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.oltu.oauth2.common.OAuth;

@zzzv
public class zzani extends WebViewClient {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final String[] f4444 = {"NOT_YET_VALID", "EXPIRED", "ID_MISMATCH", "UNTRUSTED", "DATE_INVALID", "INVALID"};

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String[] f4445 = {"UNKNOWN", "HOST_LOOKUP", "UNSUPPORTED_AUTH_SCHEME", "AUTHENTICATION", "PROXY_AUTHENTICATION", "CONNECT", "IO", "TIMEOUT", "REDIRECT_LOOP", "UNSUPPORTED_SCHEME", "FAILED_SSL_HANDSHAKE", "BAD_URL", "FILE", "FILE_NOT_FOUND", "TOO_MANY_REQUESTS"};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Object f4446;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzje f4447;

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzn f4448;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4449;

    /* renamed from: ʿ  reason: contains not printable characters */
    private zzx f4450;

    /* renamed from: ˆ  reason: contains not printable characters */
    private zzq f4451;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public zzano f4452;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final zzxa f4453;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ViewTreeObserver.OnGlobalLayoutListener f4454;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ViewTreeObserver.OnScrollChangedListener f4455;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f4456;

    /* renamed from: ˏ  reason: contains not printable characters */
    private zzw f4457;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzanm f4458;

    /* renamed from: י  reason: contains not printable characters */
    private zzwr f4459;

    /* renamed from: ـ  reason: contains not printable characters */
    private zzxc f4460;

    /* renamed from: ٴ  reason: contains not printable characters */
    private zzann f4461;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzb f4462;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private zzanq f4463;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f4464;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f4465;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f4466;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f4467;

    /* renamed from: 连任  reason: contains not printable characters */
    private final HashMap<String, List<zzt<? super zzanh>>> f4468;

    /* renamed from: 靐  reason: contains not printable characters */
    protected zzafb f4469;

    /* renamed from: 龘  reason: contains not printable characters */
    protected zzanh f4470;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private View.OnAttachStateChangeListener f4471;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f4472;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f4473;

    public zzani(zzanh zzanh, boolean z) {
        this(zzanh, z, new zzxa(zzanh, zzanh.m5017(), new zzmt(zzanh.getContext())), (zzwr) null);
    }

    private zzani(zzanh zzanh, boolean z, zzxa zzxa, zzwr zzwr) {
        this.f4468 = new HashMap<>();
        this.f4446 = new Object();
        this.f4449 = false;
        this.f4470 = zzanh;
        this.f4472 = z;
        this.f4453 = zzxa;
        this.f4459 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final WebResourceResponse m5019(String str) throws IOException {
        HttpURLConnection httpURLConnection;
        String r7;
        String r8;
        URL url = new URL(str);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (i2 <= 20) {
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(10000);
                openConnection.setReadTimeout(10000);
                if (!(openConnection instanceof HttpURLConnection)) {
                    throw new IOException("Invalid protocol.");
                }
                httpURLConnection = (HttpURLConnection) openConnection;
                zzbs.zzei().m4636(this.f4470.getContext(), this.f4470.m4985().f4297, false, httpURLConnection);
                zzajv zzajv = new zzajv();
                zzajv.m4788(httpURLConnection, (byte[]) null);
                int responseCode = httpURLConnection.getResponseCode();
                zzbs.zzei();
                r7 = zzahn.m4561(httpURLConnection.getContentType());
                zzbs.zzei();
                r8 = zzahn.m4562(httpURLConnection.getContentType());
                zzajv.m4787(httpURLConnection, responseCode);
                if (responseCode >= 300 && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        throw new IOException("Missing Location header in redirect");
                    }
                    URL url2 = new URL(url, headerField);
                    String protocol = url2.getProtocol();
                    if (protocol == null) {
                        zzagf.m4791("Protocol is null");
                        return null;
                    } else if (protocol.equals(TyphoonApp.HTTP) || protocol.equals(TyphoonApp.HTTPS)) {
                        String valueOf = String.valueOf(headerField);
                        zzagf.m4792(valueOf.length() != 0 ? "Redirecting to ".concat(valueOf) : new String("Redirecting to "));
                        httpURLConnection.disconnect();
                        i = i2;
                        url = url2;
                    } else {
                        String valueOf2 = String.valueOf(protocol);
                        zzagf.m4791(valueOf2.length() != 0 ? "Unsupported scheme: ".concat(valueOf2) : new String("Unsupported scheme: "));
                        return null;
                    }
                }
            } else {
                throw new IOException(new StringBuilder(32).append("Too many redirects (20)").toString());
            }
        }
        return new WebResourceResponse(r7, r8, httpURLConnection.getInputStream());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5022(Context context, String str, String str2, String str3) {
        String str4;
        if (((Boolean) zzkb.m5481().m5595(zznh.f4951)).booleanValue()) {
            Bundle bundle = new Bundle();
            bundle.putString(NotificationCompat.CATEGORY_ERROR, str);
            bundle.putString(OAuth.OAUTH_CODE, str2);
            if (!TextUtils.isEmpty(str3)) {
                Uri parse = Uri.parse(str3);
                if (parse.getHost() != null) {
                    str4 = parse.getHost();
                    bundle.putString("host", str4);
                    zzbs.zzei().m4635(context, this.f4470.m4985().f4297, "gmob-apps", bundle, true);
                }
            }
            str4 = "";
            bundle.putString("host", str4);
            zzbs.zzei().m4635(context, this.f4470.m4985().f4297, "gmob-apps", bundle, true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5023(Uri uri) {
        String path = uri.getPath();
        List<zzt> list = this.f4468.get(path);
        if (list != null) {
            zzbs.zzei();
            Map<String, String> r3 = zzahn.m4603(uri);
            if (zzagf.m4798(2)) {
                String valueOf = String.valueOf(path);
                zzagf.m4527(valueOf.length() != 0 ? "Received GMSG: ".concat(valueOf) : new String("Received GMSG: "));
                for (String next : r3.keySet()) {
                    String str = r3.get(next);
                    zzagf.m4527(new StringBuilder(String.valueOf(next).length() + 4 + String.valueOf(str).length()).append("  ").append(next).append(": ").append(str).toString());
                }
            }
            for (zzt zza : list) {
                zza.zza(this.f4470, r3);
            }
            return;
        }
        String valueOf2 = String.valueOf(uri);
        zzagf.m4527(new StringBuilder(String.valueOf(valueOf2).length() + 32).append("No GMSG handler found for GMSG: ").append(valueOf2).toString());
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5024(View view, zzafb zzafb, int i) {
        if (zzafb.m9560() && i > 0) {
            zzafb.m9564(view);
            if (zzafb.m9560()) {
                zzahn.f4212.postDelayed(new zzanj(this, view, zzafb, i), 100);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5025(AdOverlayInfoParcel adOverlayInfoParcel) {
        boolean z = false;
        boolean r0 = this.f4459 != null ? this.f4459.m6001() : false;
        zzbs.zzeg();
        Context context = this.f4470.getContext();
        if (!r0) {
            z = true;
        }
        zzl.zza(context, adOverlayInfoParcel, z);
        if (this.f4469 != null) {
            String str = adOverlayInfoParcel.url;
            if (str == null && adOverlayInfoParcel.zzciv != null) {
                str = adOverlayInfoParcel.zzciv.url;
            }
            this.f4469.m9565(str);
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final void m5027() {
        if (this.f4471 != null) {
            zzanh zzanh = this.f4470;
            if (zzanh == null) {
                throw null;
            }
            ((View) zzanh).removeOnAttachStateChangeListener(this.f4471);
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final void m5028() {
        if (this.f4458 != null && ((this.f4465 && this.f4467 <= 0) || this.f4466)) {
            this.f4458.m9707(this.f4470, !this.f4466);
            this.f4458 = null;
        }
        this.f4470.m4988();
    }

    public final void onLoadResource(WebView webView, String str) {
        String valueOf = String.valueOf(str);
        zzagf.m4527(valueOf.length() != 0 ? "Loading resource: ".concat(valueOf) : new String("Loading resource: "));
        Uri parse = Uri.parse(str);
        if ("gmsg".equalsIgnoreCase(parse.getScheme()) && "mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            m5023(parse);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        r2.f4461.m9708(r2.f4470);
        r2.f4461 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        m5028();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        r2.f4465 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if (r2.f4461 == null) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onPageFinished(android.webkit.WebView r3, java.lang.String r4) {
        /*
            r2 = this;
            java.lang.Object r1 = r2.f4446
            monitor-enter(r1)
            boolean r0 = r2.f4464     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0014
            java.lang.String r0 = "Blank page loaded, 1..."
            com.google.android.gms.internal.zzagf.m4527(r0)     // Catch:{ all -> 0x002a }
            com.google.android.gms.internal.zzanh r0 = r2.f4470     // Catch:{ all -> 0x002a }
            r0.m4992()     // Catch:{ all -> 0x002a }
            monitor-exit(r1)     // Catch:{ all -> 0x002a }
        L_0x0013:
            return
        L_0x0014:
            monitor-exit(r1)     // Catch:{ all -> 0x002a }
            r0 = 1
            r2.f4465 = r0
            com.google.android.gms.internal.zzann r0 = r2.f4461
            if (r0 == 0) goto L_0x0026
            com.google.android.gms.internal.zzann r0 = r2.f4461
            com.google.android.gms.internal.zzanh r1 = r2.f4470
            r0.m9708(r1)
            r0 = 0
            r2.f4461 = r0
        L_0x0026:
            r2.m5028()
            goto L_0x0013
        L_0x002a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzani.onPageFinished(android.webkit.WebView, java.lang.String):void");
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        m5022(this.f4470.getContext(), "http_err", (i >= 0 || (-i) + -1 >= f4445.length) ? String.valueOf(i) : f4445[(-i) - 1], str2);
        super.onReceivedError(webView, i, str, str2);
    }

    public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (sslError != null) {
            int primaryError = sslError.getPrimaryError();
            m5022(this.f4470.getContext(), "ssl_err", (primaryError < 0 || primaryError >= f4444.length) ? String.valueOf(primaryError) : f4444[primaryError], zzbs.zzek().m4656(sslError));
        }
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
    }

    @TargetApi(11)
    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        zzii r2;
        try {
            String r0 = zzafi.m4437(str, this.f4470.getContext());
            if (!r0.equals(str)) {
                return m5019(r0);
            }
            zzil r02 = zzil.m5428(str);
            if (r02 != null && (r2 = zzbs.zzen().m5418(r02)) != null && r2.m5423()) {
                return new WebResourceResponse("", "", r2.m5422());
            }
            if (zzajv.m4775()) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f4939)).booleanValue()) {
                    return m5019(str);
                }
            }
            return null;
        } catch (Exception | NoClassDefFoundError e) {
            zzbs.zzem().m4505(e, "AdWebViewClient.interceptRequest");
            return null;
        }
    }

    public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 222:
                return true;
            default:
                return false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Uri uri;
        Uri uri2;
        String valueOf = String.valueOf(str);
        zzagf.m4527(valueOf.length() != 0 ? "AdWebView shouldOverrideUrlLoading: ".concat(valueOf) : new String("AdWebView shouldOverrideUrlLoading: "));
        Uri parse = Uri.parse(str);
        if (!"gmsg".equalsIgnoreCase(parse.getScheme()) || !"mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            if (this.f4449 && webView == this.f4470.m4979()) {
                String scheme = parse.getScheme();
                if (TyphoonApp.HTTP.equalsIgnoreCase(scheme) || TyphoonApp.HTTPS.equalsIgnoreCase(scheme)) {
                    if (this.f4447 != null) {
                        if (((Boolean) zzkb.m5481().m5595(zznh.f4894)).booleanValue()) {
                            this.f4447.onAdClicked();
                            if (this.f4469 != null) {
                                this.f4469.m9565(str);
                            }
                            this.f4447 = null;
                        }
                    }
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            }
            if (!this.f4470.m4979().willNotDraw()) {
                try {
                    zzcv r2 = this.f4470.m4986();
                    if (r2 == null || !r2.m11541(parse)) {
                        uri2 = parse;
                    } else {
                        Context context = this.f4470.getContext();
                        zzanh zzanh = this.f4470;
                        if (zzanh == null) {
                            throw null;
                        }
                        uri2 = r2.m11538(parse, context, (View) zzanh, this.f4470.m5003());
                    }
                    uri = uri2;
                } catch (zzcw e) {
                    String valueOf2 = String.valueOf(str);
                    zzagf.m4791(valueOf2.length() != 0 ? "Unable to append parameter to URL: ".concat(valueOf2) : new String("Unable to append parameter to URL: "));
                    uri = parse;
                }
                if (this.f4457 == null || this.f4457.zzda()) {
                    m5047(new zzc("android.intent.action.VIEW", uri.toString(), (String) null, (String) null, (String) null, (String) null, (String) null));
                } else {
                    this.f4457.zzt(str);
                }
            } else {
                String valueOf3 = String.valueOf(str);
                zzagf.m4791(valueOf3.length() != 0 ? "AdWebView unable to handle URL: ".concat(valueOf3) : new String("AdWebView unable to handle URL: "));
            }
        } else {
            m5023(parse);
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m5029() {
        boolean z;
        synchronized (this.f4446) {
            z = this.f4456;
        }
        return z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m5030() {
        synchronized (this.f4446) {
            zzagf.m4527("Loading blank page in WebView, 2...");
            this.f4464 = true;
            this.f4470.m5013("about:blank");
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m5031() {
        zzafb zzafb = this.f4469;
        if (zzafb != null) {
            WebView r1 = this.f4470.m4979();
            if (ViewCompat.isAttachedToWindow(r1)) {
                m5024((View) r1, zzafb, 10);
                return;
            }
            m5027();
            this.f4471 = new zzank(this, zzafb);
            zzanh zzanh = this.f4470;
            if (zzanh == null) {
                throw null;
            }
            ((View) zzanh).addOnAttachStateChangeListener(this.f4471);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzanq m5032() {
        return this.f4463;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m5033() {
        synchronized (this.f4446) {
            this.f4449 = false;
            this.f4472 = true;
            zzbs.zzei();
            zzahn.m4612((Runnable) new zzanl(this));
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m5034() {
        if (this.f4469 != null) {
            this.f4469.m9561();
            this.f4469 = null;
        }
        m5027();
        synchronized (this.f4446) {
            this.f4468.clear();
            this.f4447 = null;
            this.f4448 = null;
            this.f4458 = null;
            this.f4461 = null;
            this.f4462 = null;
            this.f4449 = false;
            this.f4472 = false;
            this.f4473 = false;
            this.f4456 = false;
            this.f4451 = null;
            this.f4452 = null;
            if (this.f4459 != null) {
                this.f4459.m6000(true);
                this.f4459 = null;
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m5035() {
        synchronized (this.f4446) {
            this.f4456 = true;
        }
        this.f4467++;
        m5028();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m5036() {
        this.f4467--;
        m5028();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m5037() {
        this.f4466 = true;
        m5028();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final ViewTreeObserver.OnScrollChangedListener m5038() {
        ViewTreeObserver.OnScrollChangedListener onScrollChangedListener;
        synchronized (this.f4446) {
            onScrollChangedListener = this.f4455;
        }
        return onScrollChangedListener;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5039(String str, zzt<? super zzanh> zzt) {
        synchronized (this.f4446) {
            List list = this.f4468.get(str);
            if (list != null) {
                list.remove(zzt);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5040() {
        boolean z;
        synchronized (this.f4446) {
            z = this.f4472;
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final ViewTreeObserver.OnGlobalLayoutListener m5041() {
        ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
        synchronized (this.f4446) {
            onGlobalLayoutListener = this.f4454;
        }
        return onGlobalLayoutListener;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5042() {
        boolean z;
        synchronized (this.f4446) {
            z = this.f4473;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzw m5043() {
        return this.f4457;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5044(int i, int i2) {
        if (this.f4459 != null) {
            this.f4459.m5997(i, i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5045(int i, int i2, boolean z) {
        this.f4453.m6006(i, i2);
        if (this.f4459 != null) {
            this.f4459.m5998(i, i2, z);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5046(ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener onScrollChangedListener) {
        synchronized (this.f4446) {
            this.f4473 = true;
            this.f4470.m5018();
            this.f4454 = onGlobalLayoutListener;
            this.f4455 = onScrollChangedListener;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5047(zzc zzc) {
        zzn zzn = null;
        boolean r1 = this.f4470.m4987();
        zzje zzje = (!r1 || this.f4470.m4983().m5227()) ? this.f4447 : null;
        if (!r1) {
            zzn = this.f4448;
        }
        m5025(new AdOverlayInfoParcel(zzc, zzje, zzn, this.f4451, this.f4470.m4985()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5048(zzanm zzanm) {
        this.f4458 = zzanm;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5049(zzann zzann) {
        this.f4461 = zzann;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5050(zzano zzano) {
        this.f4452 = zzano;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5051(zzanq zzanq) {
        this.f4463 = zzanq;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5052(zzje zzje, zzn zzn, zzb zzb, zzq zzq, boolean z, zzx zzx, zzw zzw, zzxc zzxc, zzafb zzafb) {
        zzw zzw2 = zzw == null ? new zzw(this.f4470.getContext(), zzafb, (zzaaz) null) : zzw;
        this.f4459 = new zzwr(this.f4470, zzxc);
        this.f4469 = zzafb;
        m5053("/appEvent", (zzt<? super zzanh>) new zza(zzb));
        m5053("/backButton", (zzt<? super zzanh>) zzd.zzbxg);
        m5053("/refresh", (zzt<? super zzanh>) zzd.zzbxh);
        m5053("/canOpenURLs", (zzt<? super zzanh>) zzd.zzbwx);
        m5053("/canOpenIntents", (zzt<? super zzanh>) zzd.zzbwy);
        m5053("/click", (zzt<? super zzanh>) zzd.zzbwz);
        m5053("/close", (zzt<? super zzanh>) zzd.zzbxa);
        m5053("/customClose", (zzt<? super zzanh>) zzd.zzbxb);
        m5053("/instrument", (zzt<? super zzanh>) zzd.zzbxk);
        m5053("/delayPageLoaded", (zzt<? super zzanh>) zzd.zzbxm);
        m5053("/delayPageClosed", (zzt<? super zzanh>) zzd.zzbxn);
        m5053("/getLocationInfo", (zzt<? super zzanh>) zzd.zzbxo);
        m5053("/httpTrack", (zzt<? super zzanh>) zzd.zzbxc);
        m5053("/log", (zzt<? super zzanh>) zzd.zzbxd);
        m5053("/mraid", (zzt<? super zzanh>) new zzaa(zzw2, this.f4459));
        m5053("/mraidLoaded", (zzt<? super zzanh>) this.f4453);
        m5053("/open", (zzt<? super zzanh>) new zzab(this.f4470.getContext(), this.f4470.m4985(), this.f4470.m4986(), zzq, zzje, zzb, zzn, zzw2, this.f4459));
        m5053("/precache", (zzt<? super zzanh>) new zzane());
        m5053("/touch", (zzt<? super zzanh>) zzd.zzbxf);
        m5053("/video", (zzt<? super zzanh>) zzd.zzbxi);
        m5053("/videoMeta", (zzt<? super zzanh>) zzd.zzbxj);
        if (zzbs.zzfd().m4436(this.f4470.getContext())) {
            m5053("/logScionEvent", (zzt<? super zzanh>) new zzz(this.f4470.getContext()));
        }
        if (zzx != null) {
            m5053("/setInterstitialProperties", (zzt<? super zzanh>) new com.google.android.gms.ads.internal.gmsg.zzw(zzx));
        }
        this.f4447 = zzje;
        this.f4448 = zzn;
        this.f4462 = zzb;
        this.f4451 = zzq;
        this.f4457 = zzw2;
        this.f4460 = zzxc;
        this.f4450 = zzx;
        this.f4449 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5053(String str, zzt<? super zzanh> zzt) {
        synchronized (this.f4446) {
            List list = this.f4468.get(str);
            if (list == null) {
                list = new CopyOnWriteArrayList();
                this.f4468.put(str, list);
            }
            list.add(zzt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5054(String str, zzr<zzt<? super zzanh>> zzr) {
        synchronized (this.f4446) {
            List<zzt> list = this.f4468.get(str);
            if (list != null) {
                ArrayList arrayList = new ArrayList();
                for (zzt zzt : list) {
                    if (zzr.m9273(zzt)) {
                        arrayList.add(zzt);
                    }
                }
                list.removeAll(arrayList);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5055(boolean z) {
        this.f4449 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5056(boolean z, int i) {
        m5025(new AdOverlayInfoParcel((!this.f4470.m4987() || this.f4470.m4983().m5227()) ? this.f4447 : null, this.f4448, this.f4451, this.f4470, z, i, this.f4470.m4985()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5057(boolean z, int i, String str) {
        zzanp zzanp = null;
        boolean r3 = this.f4470.m4987();
        zzje zzje = (!r3 || this.f4470.m4983().m5227()) ? this.f4447 : null;
        if (!r3) {
            zzanp = new zzanp(this.f4470, this.f4448);
        }
        m5025(new AdOverlayInfoParcel(zzje, zzanp, this.f4462, this.f4451, this.f4470, z, i, str, this.f4470.m4985()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5058(boolean z, int i, String str, String str2) {
        zzanp zzanp = null;
        boolean r3 = this.f4470.m4987();
        zzje zzje = (!r3 || this.f4470.m4983().m5227()) ? this.f4447 : null;
        if (!r3) {
            zzanp = new zzanp(this.f4470, this.f4448);
        }
        m5025(new AdOverlayInfoParcel(zzje, zzanp, this.f4462, this.f4451, this.f4470, z, i, str, str2, this.f4470.m4985()));
    }
}
