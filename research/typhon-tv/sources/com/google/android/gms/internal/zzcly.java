package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcly extends zzfjm<zzcly> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Integer f9696 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public zzclr[] f9697 = zzclr.m11449();

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9698 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public zzclx[] f9699 = zzclx.m11473();

    /* renamed from: 齉  reason: contains not printable characters */
    public zzclz[] f9700 = zzclz.m11480();

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f9701 = null;

    public zzcly() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcly)) {
            return false;
        }
        zzcly zzcly = (zzcly) obj;
        if (this.f9701 == null) {
            if (zzcly.f9701 != null) {
                return false;
            }
        } else if (!this.f9701.equals(zzcly.f9701)) {
            return false;
        }
        if (this.f9698 == null) {
            if (zzcly.f9698 != null) {
                return false;
            }
        } else if (!this.f9698.equals(zzcly.f9698)) {
            return false;
        }
        if (this.f9696 == null) {
            if (zzcly.f9696 != null) {
                return false;
            }
        } else if (!this.f9696.equals(zzcly.f9696)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9700, (Object[]) zzcly.f9700)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9699, (Object[]) zzcly.f9699)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9697, (Object[]) zzcly.f9697)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcly.f10533 == null || zzcly.f10533.m12849() : this.f10533.equals(zzcly.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.f9696 == null ? 0 : this.f9696.hashCode()) + (((this.f9698 == null ? 0 : this.f9698.hashCode()) + (((this.f9701 == null ? 0 : this.f9701.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31) + zzfjq.m12859((Object[]) this.f9700)) * 31) + zzfjq.m12859((Object[]) this.f9699)) * 31) + zzfjq.m12859((Object[]) this.f9697)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11477() {
        int r0 = super.m12841();
        if (this.f9701 != null) {
            r0 += zzfjk.m12814(1, this.f9701.longValue());
        }
        if (this.f9698 != null) {
            r0 += zzfjk.m12808(2, this.f9698);
        }
        if (this.f9696 != null) {
            r0 += zzfjk.m12806(3, this.f9696.intValue());
        }
        if (this.f9700 != null && this.f9700.length > 0) {
            int i = r0;
            for (zzclz zzclz : this.f9700) {
                if (zzclz != null) {
                    i += zzfjk.m12807(4, (zzfjs) zzclz);
                }
            }
            r0 = i;
        }
        if (this.f9699 != null && this.f9699.length > 0) {
            int i2 = r0;
            for (zzclx zzclx : this.f9699) {
                if (zzclx != null) {
                    i2 += zzfjk.m12807(5, (zzfjs) zzclx);
                }
            }
            r0 = i2;
        }
        if (this.f9697 != null && this.f9697.length > 0) {
            for (zzclr zzclr : this.f9697) {
                if (zzclr != null) {
                    r0 += zzfjk.m12807(6, (zzfjs) zzclr);
                }
            }
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11478(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9701 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 18:
                    this.f9698 = zzfjj.m12791();
                    continue;
                case 24:
                    this.f9696 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 34:
                    int r2 = zzfjv.m12883(zzfjj, 34);
                    int length = this.f9700 == null ? 0 : this.f9700.length;
                    zzclz[] zzclzArr = new zzclz[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9700, 0, zzclzArr, 0, length);
                    }
                    while (length < zzclzArr.length - 1) {
                        zzclzArr[length] = new zzclz();
                        zzfjj.m12802((zzfjs) zzclzArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzclzArr[length] = new zzclz();
                    zzfjj.m12802((zzfjs) zzclzArr[length]);
                    this.f9700 = zzclzArr;
                    continue;
                case 42:
                    int r22 = zzfjv.m12883(zzfjj, 42);
                    int length2 = this.f9699 == null ? 0 : this.f9699.length;
                    zzclx[] zzclxArr = new zzclx[(r22 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f9699, 0, zzclxArr, 0, length2);
                    }
                    while (length2 < zzclxArr.length - 1) {
                        zzclxArr[length2] = new zzclx();
                        zzfjj.m12802((zzfjs) zzclxArr[length2]);
                        zzfjj.m12800();
                        length2++;
                    }
                    zzclxArr[length2] = new zzclx();
                    zzfjj.m12802((zzfjs) zzclxArr[length2]);
                    this.f9699 = zzclxArr;
                    continue;
                case 50:
                    int r23 = zzfjv.m12883(zzfjj, 50);
                    int length3 = this.f9697 == null ? 0 : this.f9697.length;
                    zzclr[] zzclrArr = new zzclr[(r23 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.f9697, 0, zzclrArr, 0, length3);
                    }
                    while (length3 < zzclrArr.length - 1) {
                        zzclrArr[length3] = new zzclr();
                        zzfjj.m12802((zzfjs) zzclrArr[length3]);
                        zzfjj.m12800();
                        length3++;
                    }
                    zzclrArr[length3] = new zzclr();
                    zzfjj.m12802((zzfjs) zzclrArr[length3]);
                    this.f9697 = zzclrArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11479(zzfjk zzfjk) throws IOException {
        if (this.f9701 != null) {
            zzfjk.m12824(1, this.f9701.longValue());
        }
        if (this.f9698 != null) {
            zzfjk.m12835(2, this.f9698);
        }
        if (this.f9696 != null) {
            zzfjk.m12832(3, this.f9696.intValue());
        }
        if (this.f9700 != null && this.f9700.length > 0) {
            for (zzclz zzclz : this.f9700) {
                if (zzclz != null) {
                    zzfjk.m12834(4, (zzfjs) zzclz);
                }
            }
        }
        if (this.f9699 != null && this.f9699.length > 0) {
            for (zzclx zzclx : this.f9699) {
                if (zzclx != null) {
                    zzfjk.m12834(5, (zzfjs) zzclx);
                }
            }
        }
        if (this.f9697 != null && this.f9697.length > 0) {
            for (zzclr zzclr : this.f9697) {
                if (zzclr != null) {
                    zzfjk.m12834(6, (zzfjs) zzclr);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
