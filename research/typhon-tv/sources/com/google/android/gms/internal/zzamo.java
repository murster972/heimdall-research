package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzamo {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f4399;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4400;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f4401;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f4402;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f4403;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f4404;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f4405;

    public zzamo(String str) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = null;
        if (str != null) {
            try {
                jSONObject = new JSONObject(str);
            } catch (JSONException e) {
            }
        } else {
            jSONObject = null;
        }
        jSONObject2 = jSONObject;
        this.f4402 = m4919(jSONObject2, "aggressive_media_codec_release", zznh.f5139);
        this.f4405 = m4918(jSONObject2, "exo_player_version", zznh.f4892);
        this.f4404 = m4917(jSONObject2, "exo_cache_buffer_size", zznh.f5048);
        this.f4403 = m4917(jSONObject2, "exo_connect_timeout_millis", zznh.f4918);
        this.f4401 = m4917(jSONObject2, "exo_read_timeout_millis", zznh.f4944);
        this.f4399 = m4917(jSONObject2, "load_check_interval_bytes", zznh.f5135);
        this.f4400 = m4919(jSONObject2, "use_cache_data_source", zznh.f5020);
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.google.android.gms.internal.zzmx<java.lang.Integer>, com.google.android.gms.internal.zzmx] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int m4917(org.json.JSONObject r1, java.lang.String r2, com.google.android.gms.internal.zzmx<java.lang.Integer> r3) {
        /*
            if (r1 == 0) goto L_0x0008
            int r0 = r1.getInt(r2)     // Catch:{ JSONException -> 0x0007 }
        L_0x0006:
            return r0
        L_0x0007:
            r0 = move-exception
        L_0x0008:
            com.google.android.gms.internal.zznf r0 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r0.m5595(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzamo.m4917(org.json.JSONObject, java.lang.String, com.google.android.gms.internal.zzmx):int");
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.google.android.gms.internal.zzmx<java.lang.String>, com.google.android.gms.internal.zzmx] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m4918(org.json.JSONObject r1, java.lang.String r2, com.google.android.gms.internal.zzmx<java.lang.String> r3) {
        /*
            if (r1 == 0) goto L_0x0008
            java.lang.String r0 = r1.getString(r2)     // Catch:{ JSONException -> 0x0007 }
        L_0x0006:
            return r0
        L_0x0007:
            r0 = move-exception
        L_0x0008:
            com.google.android.gms.internal.zznf r0 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r0.m5595(r3)
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzamo.m4918(org.json.JSONObject, java.lang.String, com.google.android.gms.internal.zzmx):java.lang.String");
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.google.android.gms.internal.zzmx<java.lang.Boolean>, com.google.android.gms.internal.zzmx] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m4919(org.json.JSONObject r1, java.lang.String r2, com.google.android.gms.internal.zzmx<java.lang.Boolean> r3) {
        /*
            if (r1 == 0) goto L_0x0008
            boolean r0 = r1.getBoolean(r2)     // Catch:{ JSONException -> 0x0007 }
        L_0x0006:
            return r0
        L_0x0007:
            r0 = move-exception
        L_0x0008:
            com.google.android.gms.internal.zznf r0 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r0.m5595(r3)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzamo.m4919(org.json.JSONObject, java.lang.String, com.google.android.gms.internal.zzmx):boolean");
    }
}
