package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbib extends zzbfm {
    public static final Parcelable.Creator<zzbib> CREATOR = new zzbic();

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8786;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8787;

    public zzbib(String str, String str2) {
        this.f8787 = str;
        this.f8786 = str2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f8787, false);
        zzbfp.m10193(parcel, 3, this.f8786, false);
        zzbfp.m10182(parcel, r0);
    }
}
