package com.google.android.gms.internal;

import android.view.View;

public final class zzfx implements zzhd {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzos f10637;

    public zzfx(zzos zzos) {
        this.f10637 = zzos;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12954() {
        return this.f10637 == null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzhd m12955() {
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m12956() {
        if (this.f10637 != null) {
            return this.f10637.m13186();
        }
        return null;
    }
}
