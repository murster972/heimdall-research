package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;

public final class zzbid extends zzbfm {
    public static final Parcelable.Creator<zzbid> CREATOR = new zzbie();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f8788;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<String> f8789;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f8790;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final List<zzbhn> f8791;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f8792;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f8793;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f8794;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f8795;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f8796;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DataHolder f8797;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8798;

    public zzbid(String str, long j, DataHolder dataHolder, String str2, String str3, String str4, List<String> list, int i, List<zzbhn> list2, int i2, int i3) {
        this.f8798 = str;
        this.f8795 = j;
        this.f8797 = dataHolder;
        this.f8796 = str2;
        this.f8794 = str3;
        this.f8788 = str4;
        this.f8789 = list;
        this.f8790 = i;
        this.f8791 = list2;
        this.f8792 = i2;
        this.f8793 = i3;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f8798, false);
        zzbfp.m10186(parcel, 3, this.f8795);
        zzbfp.m10189(parcel, 4, (Parcelable) this.f8797, i, false);
        zzbfp.m10193(parcel, 5, this.f8796, false);
        zzbfp.m10193(parcel, 6, this.f8794, false);
        zzbfp.m10193(parcel, 7, this.f8788, false);
        zzbfp.m10178(parcel, 8, this.f8789, false);
        zzbfp.m10185(parcel, 9, this.f8790);
        zzbfp.m10180(parcel, 10, this.f8791, false);
        zzbfp.m10185(parcel, 11, this.f8792);
        zzbfp.m10185(parcel, 12, this.f8793);
        zzbfp.m10182(parcel, r0);
    }
}
