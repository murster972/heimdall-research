package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

final class zzam {

    /* renamed from: ʻ  reason: contains not printable characters */
    final long f8310;

    /* renamed from: ʼ  reason: contains not printable characters */
    final long f8311;

    /* renamed from: ʽ  reason: contains not printable characters */
    final List<zzl> f8312;

    /* renamed from: 连任  reason: contains not printable characters */
    final long f8313;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f8314;

    /* renamed from: 麤  reason: contains not printable characters */
    final long f8315;

    /* renamed from: 齉  reason: contains not printable characters */
    final String f8316;

    /* renamed from: 龘  reason: contains not printable characters */
    long f8317;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    zzam(String str, zzc zzc) {
        this(str, zzc.f8820, zzc.f8822, zzc.f8821, zzc.f8819, zzc.f8816, zzc.f8818 != null ? zzc.f8818 : zzao.m9713(zzc.f8817));
        this.f8317 = (long) zzc.f8823.length;
    }

    private zzam(String str, String str2, long j, long j2, long j3, long j4, List<zzl> list) {
        this.f8314 = str;
        this.f8316 = "".equals(str2) ? null : str2;
        this.f8315 = j;
        this.f8313 = j2;
        this.f8310 = j3;
        this.f8311 = j4;
        this.f8312 = list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzam m9692(zzan zzan) throws IOException {
        if (zzal.m9678((InputStream) zzan) == 538247942) {
            return new zzam(zzal.m9680(zzan), zzal.m9680(zzan), zzal.m9672((InputStream) zzan), zzal.m9672((InputStream) zzan), zzal.m9672((InputStream) zzan), zzal.m9672((InputStream) zzan), zzal.m9673(zzan));
        }
        throw new IOException();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9693(OutputStream outputStream) {
        try {
            zzal.m9681(outputStream, 538247942);
            zzal.m9683(outputStream, this.f8314);
            zzal.m9683(outputStream, this.f8316 == null ? "" : this.f8316);
            zzal.m9682(outputStream, this.f8315);
            zzal.m9682(outputStream, this.f8313);
            zzal.m9682(outputStream, this.f8310);
            zzal.m9682(outputStream, this.f8311);
            List<zzl> list = this.f8312;
            if (list != null) {
                zzal.m9681(outputStream, list.size());
                for (zzl next : list) {
                    zzal.m9683(outputStream, next.m13077());
                    zzal.m9683(outputStream, next.m13076());
                }
            } else {
                zzal.m9681(outputStream, 0);
            }
            outputStream.flush();
            return true;
        } catch (IOException e) {
            zzae.m9513("%s", e.toString());
            return false;
        }
    }
}
