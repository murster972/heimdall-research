package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzva extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m13455() throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean m13456() throws RemoteException;

    /* renamed from: ʽ  reason: contains not printable characters */
    zzvj m13457() throws RemoteException;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean m13458() throws RemoteException;

    /* renamed from: ʿ  reason: contains not printable characters */
    zzqm m13459() throws RemoteException;

    /* renamed from: ˈ  reason: contains not printable characters */
    Bundle m13460() throws RemoteException;

    /* renamed from: ˑ  reason: contains not printable characters */
    zzvm m13461() throws RemoteException;

    /* renamed from: ٴ  reason: contains not printable characters */
    Bundle m13462() throws RemoteException;

    /* renamed from: ᐧ  reason: contains not printable characters */
    Bundle m13463() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    void m13464() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m13465() throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m13466() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13467() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m13468() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13469(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13470(IObjectWrapper iObjectWrapper, zzaem zzaem, List<String> list) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13471(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, zzaem zzaem, String str2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13472(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, zzvd zzvd) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13473(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, String str2, zzvd zzvd) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13474(IObjectWrapper iObjectWrapper, zzjj zzjj, String str, String str2, zzvd zzvd, zzpe zzpe, List<String> list) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13475(IObjectWrapper iObjectWrapper, zzjn zzjn, zzjj zzjj, String str, zzvd zzvd) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13476(IObjectWrapper iObjectWrapper, zzjn zzjn, zzjj zzjj, String str, String str2, zzvd zzvd) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13477(zzjj zzjj, String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13478(zzjj zzjj, String str, String str2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13479(boolean z) throws RemoteException;

    /* renamed from: ﹶ  reason: contains not printable characters */
    zzll m13480() throws RemoteException;
}
