package com.google.android.gms.internal;

import android.os.Handler;
import java.util.LinkedList;
import java.util.List;

@zzzv
final class zzse {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zztc> f5356 = new LinkedList();

    zzse() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5823(zztd zztd) {
        Handler handler = zzahn.f4212;
        for (zztc zztb : this.f5356) {
            handler.post(new zztb(this, zztb, zztd));
        }
        this.f5356.clear();
    }
}
