package com.google.android.gms.internal;

@zzzv
public final class zzapa {

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f4537;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f4538;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f4539;

    private zzapa(int i, int i2, int i3) {
        this.f4538 = i;
        this.f4537 = i2;
        this.f4539 = i3;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzapa m5221() {
        return new zzapa(4, 0, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzapa m5222() {
        return new zzapa(0, 0, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzapa m5223(int i, int i2) {
        return new zzapa(1, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzapa m5224(zzjn zzjn) {
        return zzjn.f4796 ? new zzapa(3, 0, 0) : zzjn.f4792 ? new zzapa(2, 0, 0) : zzjn.f4791 ? m5222() : m5223(zzjn.f4789, zzjn.f4797);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m5225() {
        return this.f4538 == 4;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m5226() {
        return this.f4538 == 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m5227() {
        return this.f4538 == 3;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5228() {
        return this.f4538 == 2;
    }
}
