package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzqf extends zzev implements zzqe {
    public zzqf() {
        attachInterface(this, "com.google.android.gms.ads.internal.formats.client.INativeAppInstallAd");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 2:
                IObjectWrapper r0 = m13258();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r0);
                break;
            case 3:
                String r02 = m13265();
                parcel2.writeNoException();
                parcel2.writeString(r02);
                break;
            case 4:
                List r03 = m13260();
                parcel2.writeNoException();
                parcel2.writeList(r03);
                break;
            case 5:
                String r04 = m13263();
                parcel2.writeNoException();
                parcel2.writeString(r04);
                break;
            case 6:
                zzpq r05 = m13262();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r05);
                break;
            case 7:
                String r06 = m13259();
                parcel2.writeNoException();
                parcel2.writeString(r06);
                break;
            case 8:
                double r2 = m13250();
                parcel2.writeNoException();
                parcel2.writeDouble(r2);
                break;
            case 9:
                String r07 = m13251();
                parcel2.writeNoException();
                parcel2.writeString(r07);
                break;
            case 10:
                String r08 = m13252();
                parcel2.writeNoException();
                parcel2.writeString(r08);
                break;
            case 11:
                Bundle r09 = m13253();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r09);
                break;
            case 12:
                m13256();
                parcel2.writeNoException();
                break;
            case 13:
                zzll r010 = m13257();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r010);
                break;
            case 14:
                m13266((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 15:
                boolean r011 = m13261((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                zzew.m12307(parcel2, r011);
                break;
            case 16:
                m13264((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 17:
                zzpm r012 = m13255();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r012);
                break;
            case 18:
                IObjectWrapper r013 = m13267();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r013);
                break;
            case 19:
                String r014 = m13254();
                parcel2.writeNoException();
                parcel2.writeString(r014);
                break;
            default:
                return false;
        }
        return true;
    }
}
