package com.google.android.gms.internal;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class zzae {

    /* renamed from: 靐  reason: contains not printable characters */
    private static String f8116 = "Volley";

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean f8117 = Log.isLoggable("Volley", 2);

    static class zza {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final boolean f8118 = zzae.f8117;

        /* renamed from: 靐  reason: contains not printable characters */
        private final List<zzaf> f8119 = new ArrayList();

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f8120 = false;

        zza() {
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            if (!this.f8120) {
                m9518("Request on the loose");
                zzae.m9515("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final synchronized void m9518(String str) {
            long j;
            this.f8120 = true;
            if (this.f8119.size() == 0) {
                j = 0;
            } else {
                j = this.f8119.get(this.f8119.size() - 1).f8138 - this.f8119.get(0).f8138;
            }
            if (j > 0) {
                long j2 = this.f8119.get(0).f8138;
                zzae.m9513("(%-4d ms) %s", Long.valueOf(j), str);
                long j3 = j2;
                for (zzaf next : this.f8119) {
                    long j4 = next.f8138;
                    zzae.m9513("(+%-4d) [%2d] %s", Long.valueOf(j4 - j3), Long.valueOf(next.f8137), next.f8139);
                    j3 = j4;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final synchronized void m9519(String str, long j) {
            if (this.f8120) {
                throw new IllegalStateException("Marker added to finished log");
            }
            this.f8119.add(new zzaf(str, j, SystemClock.elapsedRealtime()));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m9513(String str, Object... objArr) {
        Log.d(f8116, m9514(str, objArr));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static String m9514(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClass().equals(zzae.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                String substring2 = substring.substring(substring.lastIndexOf(36) + 1);
                String methodName = stackTrace[i].getMethodName();
                str2 = new StringBuilder(String.valueOf(substring2).length() + 1 + String.valueOf(methodName).length()).append(substring2).append(".").append(methodName).toString();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", new Object[]{Long.valueOf(Thread.currentThread().getId()), str2, str});
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m9515(String str, Object... objArr) {
        Log.e(f8116, m9514(str, objArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9516(String str, Object... objArr) {
        if (f8117) {
            Log.v(f8116, m9514(str, objArr));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9517(Throwable th, String str, Object... objArr) {
        Log.e(f8116, m9514(str, objArr), th);
    }
}
