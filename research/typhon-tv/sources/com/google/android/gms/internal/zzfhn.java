package com.google.android.gms.internal;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzfhn {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfhn f10449 = new zzfhn();

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzfhw f10450;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ConcurrentMap<Class<?>, zzfhv<?>> f10451 = new ConcurrentHashMap();

    private zzfhn() {
        zzfhw zzfhw = null;
        String[] strArr = {"com.google.protobuf.AndroidProto3SchemaFactory"};
        for (int i = 0; i <= 0; i++) {
            zzfhw = m12642(strArr[0]);
            if (zzfhw != null) {
                break;
            }
        }
        this.f10450 = zzfhw == null ? new zzfgq() : zzfhw;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfhn m12641() {
        return f10449;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzfhw m12642(String str) {
        try {
            return (zzfhw) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T> zzfhv<T> m12643(Class<T> cls) {
        zzffz.m12580(cls, "messageType");
        zzfhv<T> zzfhv = (zzfhv) this.f10451.get(cls);
        if (zzfhv != null) {
            return zzfhv;
        }
        zzfhv<T> r1 = this.f10450.m12671(cls);
        zzffz.m12580(cls, "messageType");
        zzffz.m12580(r1, "schema");
        zzfhv<T> putIfAbsent = this.f10451.putIfAbsent(cls, r1);
        return putIfAbsent != null ? putIfAbsent : r1;
    }
}
