package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzea extends zzet {
    public zzea(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 5);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12273() throws IllegalAccessException, InvocationTargetException {
        this.f10284.f8467 = -1L;
        this.f10284.f8465 = -1L;
        int[] iArr = (int[]) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()});
        synchronized (this.f10284) {
            this.f10284.f8467 = Long.valueOf((long) iArr[0]);
            this.f10284.f8465 = Long.valueOf((long) iArr[1]);
            if (((Boolean) zzkb.m5481().m5595(zznh.f4963)).booleanValue() && iArr[2] != Integer.MIN_VALUE) {
                this.f10284.f8434 = Long.valueOf((long) iArr[2]);
            }
        }
    }
}
