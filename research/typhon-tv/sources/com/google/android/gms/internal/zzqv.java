package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzqv extends zzeu implements zzqt {
    zzqv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnContentAdLoadedListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13346(zzqi zzqi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzqi);
        m12298(1, v_);
    }
}
