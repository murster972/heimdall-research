package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfjx extends zzfjm<zzfjx> {

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10580 = null;

    public zzfjx() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12888() {
        int r0 = super.m12841();
        return this.f10580 != null ? r0 + zzfjk.m12808(1, this.f10580) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12889(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10580 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12890(zzfjk zzfjk) throws IOException {
        if (this.f10580 != null) {
            zzfjk.m12835(1, this.f10580);
        }
        super.m12842(zzfjk);
    }
}
