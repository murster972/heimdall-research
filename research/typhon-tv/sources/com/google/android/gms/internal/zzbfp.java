package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public final class zzbfp {
    /* renamed from: 靐  reason: contains not printable characters */
    private static int m10176(Parcel parcel, int i) {
        parcel.writeInt(-65536 | i);
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m10177(Parcel parcel, int i, int i2) {
        if (i2 >= 65535) {
            parcel.writeInt(-65536 | i);
            parcel.writeInt(i2);
            return;
        }
        parcel.writeInt((i2 << 16) | i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m10178(Parcel parcel, int i, List<String> list, boolean z) {
        if (list != null) {
            int r0 = m10176(parcel, i);
            parcel.writeStringList(list);
            m10179(parcel, r0);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m10179(Parcel parcel, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(i - 4);
        parcel.writeInt(dataPosition - i);
        parcel.setDataPosition(dataPosition);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static <T extends Parcelable> void m10180(Parcel parcel, int i, List<T> list, boolean z) {
        if (list != null) {
            int r3 = m10176(parcel, i);
            int size = list.size();
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                Parcelable parcelable = (Parcelable) list.get(i2);
                if (parcelable == null) {
                    parcel.writeInt(0);
                } else {
                    m10201(parcel, parcelable, 0);
                }
            }
            m10179(parcel, r3);
        } else if (z) {
            m10177(parcel, i, 0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m10181(Parcel parcel) {
        return m10176(parcel, 20293);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10182(Parcel parcel, int i) {
        m10179(parcel, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10183(Parcel parcel, int i, double d) {
        m10177(parcel, i, 8);
        parcel.writeDouble(d);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10184(Parcel parcel, int i, float f) {
        m10177(parcel, i, 4);
        parcel.writeFloat(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10185(Parcel parcel, int i, int i2) {
        m10177(parcel, i, 4);
        parcel.writeInt(i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10186(Parcel parcel, int i, long j) {
        m10177(parcel, i, 8);
        parcel.writeLong(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10187(Parcel parcel, int i, Bundle bundle, boolean z) {
        if (bundle != null) {
            int r0 = m10176(parcel, i);
            parcel.writeBundle(bundle);
            m10179(parcel, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10188(Parcel parcel, int i, IBinder iBinder, boolean z) {
        if (iBinder != null) {
            int r0 = m10176(parcel, i);
            parcel.writeStrongBinder(iBinder);
            m10179(parcel, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10189(Parcel parcel, int i, Parcelable parcelable, int i2, boolean z) {
        if (parcelable != null) {
            int r0 = m10176(parcel, i);
            parcelable.writeToParcel(parcel, i2);
            m10179(parcel, r0);
        } else if (z) {
            m10177(parcel, i, 0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10190(Parcel parcel, int i, Double d, boolean z) {
        if (d != null) {
            m10177(parcel, i, 8);
            parcel.writeDouble(d.doubleValue());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10191(Parcel parcel, int i, Float f, boolean z) {
        if (f != null) {
            m10177(parcel, i, 4);
            parcel.writeFloat(f.floatValue());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10192(Parcel parcel, int i, Long l, boolean z) {
        if (l != null) {
            m10177(parcel, i, 8);
            parcel.writeLong(l.longValue());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10193(Parcel parcel, int i, String str, boolean z) {
        if (str != null) {
            int r0 = m10176(parcel, i);
            parcel.writeString(str);
            m10179(parcel, r0);
        } else if (z) {
            m10177(parcel, i, 0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10194(Parcel parcel, int i, List<Integer> list, boolean z) {
        if (list != null) {
            int r2 = m10176(parcel, i);
            int size = list.size();
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                parcel.writeInt(list.get(i2).intValue());
            }
            m10179(parcel, r2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10195(Parcel parcel, int i, boolean z) {
        m10177(parcel, i, 4);
        parcel.writeInt(z ? 1 : 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10196(Parcel parcel, int i, byte[] bArr, boolean z) {
        if (bArr != null) {
            int r0 = m10176(parcel, i);
            parcel.writeByteArray(bArr);
            m10179(parcel, r0);
        } else if (z) {
            m10177(parcel, i, 0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10197(Parcel parcel, int i, int[] iArr, boolean z) {
        if (iArr != null) {
            int r0 = m10176(parcel, i);
            parcel.writeIntArray(iArr);
            m10179(parcel, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10198(Parcel parcel, int i, long[] jArr, boolean z) {
        if (jArr != null) {
            int r0 = m10176(parcel, i);
            parcel.writeLongArray(jArr);
            m10179(parcel, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends Parcelable> void m10199(Parcel parcel, int i, T[] tArr, int i2, boolean z) {
        if (tArr != null) {
            int r2 = m10176(parcel, i);
            parcel.writeInt(r3);
            for (T t : tArr) {
                if (t == null) {
                    parcel.writeInt(0);
                } else {
                    m10201(parcel, t, i2);
                }
            }
            m10179(parcel, r2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m10200(Parcel parcel, int i, String[] strArr, boolean z) {
        if (strArr != null) {
            int r0 = m10176(parcel, i);
            parcel.writeStringArray(strArr);
            m10179(parcel, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T extends Parcelable> void m10201(Parcel parcel, T t, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        t.writeToParcel(parcel, i);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }
}
