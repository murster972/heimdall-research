package com.google.android.gms.internal;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.HashMap;
import java.util.Map;

final class zzand implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzana f8334;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8335;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ String f8336;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f8337;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f8338;

    zzand(zzana zzana, String str, String str2, String str3, String str4) {
        this.f8334 = zzana;
        this.f8338 = str;
        this.f8335 = str2;
        this.f8337 = str3;
        this.f8336 = str4;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, "precacheCanceled");
        hashMap.put("src", this.f8338);
        if (!TextUtils.isEmpty(this.f8335)) {
            hashMap.put("cachedSrc", this.f8335);
        }
        hashMap.put(VastExtensionXmlManager.TYPE, zzana.m4958(this.f8337));
        hashMap.put("reason", this.f8337);
        if (!TextUtils.isEmpty(this.f8336)) {
            hashMap.put("message", this.f8336);
        }
        this.f8334.m4961("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
