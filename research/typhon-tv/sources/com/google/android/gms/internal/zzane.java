package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.zzbs;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzane implements zzt<zzamp> {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzana f4439;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Integer m4967(Map<String, String> map, String str) {
        if (!map.containsKey(str)) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(map.get(str)));
        } catch (NumberFormatException e) {
            String str2 = map.get(str);
            zzagf.m4791(new StringBuilder(String.valueOf(str).length() + 39 + String.valueOf(str2).length()).append("Precache invalid numeric parameter '").append(str).append("': ").append(str2).toString());
            return null;
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        boolean z;
        zzamp zzamp = (zzamp) obj;
        zzbs.zzfb();
        if (!map.containsKey("abort")) {
            String str = (String) map.get("src");
            if (str != null) {
                if (this.f4439 != null) {
                    zzagf.m4791("Threadless precache task has already started.");
                    return;
                } else if (zzamz.m4953(zzamp) != null) {
                    zzagf.m4791("Precache task is already running.");
                    return;
                } else if (zzamp.m4926() == null) {
                    zzagf.m4791("Precache requires a dependency provider.");
                    return;
                } else {
                    zzamo zzamo = new zzamo((String) map.get("flags"));
                    Integer r5 = m4967(map, "notifyBytes");
                    Integer r6 = m4967(map, "notifyMillis");
                    Integer r1 = m4967(map, "player");
                    if (r1 == null) {
                        r1 = 0;
                    }
                    zzana r7 = zzamp.m4926().zzaol.m9706(zzamp, r1.intValue(), (String) null, zzamo);
                    if ((r5 == null && r6 == null) ? false : true) {
                        String[] split = zzamo.f4405.split(",");
                        int length = split.length;
                        int i = 0;
                        while (true) {
                            if (i >= length) {
                                z = false;
                                break;
                            }
                            String str2 = split[i];
                            if (!str2.equals(PubnativeRequest.LEGACY_ZONE_ID) && !str2.equals("2")) {
                                z = true;
                                break;
                            }
                            i++;
                        }
                        if (z) {
                            if (r5 != null) {
                                r5.intValue();
                            } else {
                                r6.intValue();
                            }
                            this.f4439 = r7;
                        }
                    }
                    new zzamx(zzamp, r7, str).m4523();
                }
            } else if (zzamz.m4953(zzamp) == null && this.f4439 == null) {
                zzagf.m4791("Precache must specify a source.");
                return;
            }
            Integer r0 = m4967(map, "minBufferMs");
            if (r0 != null) {
                r0.intValue();
            }
            Integer r02 = m4967(map, "maxBufferMs");
            if (r02 != null) {
                r02.intValue();
            }
            Integer r03 = m4967(map, "bufferForPlaybackMs");
            if (r03 != null) {
                r03.intValue();
            }
            Integer r04 = m4967(map, "bufferForPlaybackAfterRebufferMs");
            if (r04 != null) {
                r04.intValue();
            }
        } else if (this.f4439 != null) {
            this.f4439.m4962();
        } else if (!zzamz.m4954(zzamp)) {
            zzagf.m4791("Precache abort but no precache task running.");
        }
    }
}
