package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.AdRequest$ErrorCode;

final class zzwh implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzvx f10943;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AdRequest$ErrorCode f10944;

    zzwh(zzvx zzvx, AdRequest$ErrorCode adRequest$ErrorCode) {
        this.f10943 = zzvx;
        this.f10944 = adRequest$ErrorCode;
    }

    public final void run() {
        try {
            this.f10943.f5502.m13514(zzwj.m5985(this.f10944));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdFailedToLoad.", e);
        }
    }
}
