package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbal extends UIController {

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8568;

    public zzbal(View view) {
        this.f8568 = view;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9894() {
        RemoteMediaClient r0 = m8226();
        if (r0 == null || !r0.m4143() || r0.m4139()) {
            this.f8568.setVisibility(0);
        } else {
            this.f8568.setVisibility(8);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9895() {
        this.f8568.setVisibility(8);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9896() {
        this.f8568.setVisibility(0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9897() {
        m9894();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9898(CastSession castSession) {
        super.m8227(castSession);
        m9894();
    }
}
