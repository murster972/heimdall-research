package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class zzda implements Application.ActivityLifecycleCallbacks {

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakReference<Application.ActivityLifecycleCallbacks> f9854;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f9855 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Application f9856;

    public zzda(Application application, Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        this.f9854 = new WeakReference<>(activityLifecycleCallbacks);
        this.f9856 = application;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11584(zzdi zzdi) {
        try {
            Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = (Application.ActivityLifecycleCallbacks) this.f9854.get();
            if (activityLifecycleCallbacks != null) {
                zzdi.m11592(activityLifecycleCallbacks);
            } else if (!this.f9855) {
                this.f9856.unregisterActivityLifecycleCallbacks(this);
                this.f9855 = true;
            }
        } catch (Exception e) {
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        m11584(new zzdb(this, activity, bundle));
    }

    public final void onActivityDestroyed(Activity activity) {
        m11584(new zzdh(this, activity));
    }

    public final void onActivityPaused(Activity activity) {
        m11584(new zzde(this, activity));
    }

    public final void onActivityResumed(Activity activity) {
        m11584(new zzdd(this, activity));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        m11584(new zzdg(this, activity, bundle));
    }

    public final void onActivityStarted(Activity activity) {
        m11584(new zzdc(this, activity));
    }

    public final void onActivityStopped(Activity activity) {
        m11584(new zzdf(this, activity));
    }
}
