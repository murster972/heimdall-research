package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagy extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ long f8187;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8188;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagy(Context context, long j) {
        super((zzagi) null);
        this.f8188 = context;
        this.f8187 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9597() {
        SharedPreferences.Editor edit = this.f8188.getSharedPreferences("admob", 0).edit();
        edit.putLong("first_ad_req_time_ms", this.f8187);
        edit.apply();
    }
}
