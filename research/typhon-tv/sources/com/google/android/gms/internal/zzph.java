package com.google.android.gms.internal;

import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.zzbs;

final class zzph implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzpg f10831;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzow f10832;

    zzph(zzpg zzpg, zzow zzow) {
        this.f10831 = zzpg;
        this.f10832 = zzow;
    }

    public final void run() {
        zzanh zzanh;
        if (this.f10831.f5289) {
            if (this.f10831.f5297 == null) {
                View unused = this.f10831.f5297 = new View(this.f10831.f5298.getContext());
                this.f10831.f5297.setLayoutParams(new FrameLayout.LayoutParams(-1, 0));
            }
            if (this.f10831.f5298 != this.f10831.f5297.getParent()) {
                this.f10831.f5298.addView(this.f10831.f5297);
            }
        }
        try {
            zzanh = this.f10832.m5747();
        } catch (Exception e) {
            zzbs.zzek();
            if (zzaht.m4640()) {
                zzagf.m4791("Privileged processes cannot create HTML overlays.");
                zzanh = null;
            } else {
                zzagf.m4793("Error obtaining overlay.", e);
                zzanh = null;
            }
        }
        if (!(zzanh == null || this.f10831.f5300 == null)) {
            FrameLayout frameLayout = this.f10831.f5300;
            if (zzanh == null) {
                throw null;
            }
            frameLayout.addView((View) zzanh);
        }
        this.f10831.m5774(this.f10832);
    }
}
