package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final class zzaci implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzach f8071;

    zzaci(zzach zzach) {
        this.f8071 = zzach;
    }

    public final void zza(Object obj, Map<String, String> map) {
        synchronized (this.f8071.f3928) {
            if (!this.f8071.f3927.isDone()) {
                if (this.f8071.f3930.equals(map.get("request_id"))) {
                    zzacn zzacn = new zzacn(1, map);
                    String r2 = zzacn.m4306();
                    String valueOf = String.valueOf(zzacn.m4312());
                    zzagf.m4791(new StringBuilder(String.valueOf(r2).length() + 24 + String.valueOf(valueOf).length()).append("Invalid ").append(r2).append(" request error: ").append(valueOf).toString());
                    this.f8071.f3927.m4822(zzacn);
                }
            }
        }
    }
}
