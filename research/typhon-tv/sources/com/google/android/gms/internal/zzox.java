package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzt;
import java.lang.ref.WeakReference;
import java.util.Map;

public final class zzox implements zzt<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f10817;

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<zzot> f10818;

    public zzox(zzot zzot, String str) {
        this.f10818 = new WeakReference<>(zzot);
        this.f10817 = str;
    }

    public final void zza(Object obj, Map<String, String> map) {
        zzot zzot;
        String str = map.get("ads_id");
        String str2 = map.get("eventName");
        if (!TextUtils.isEmpty(str) && this.f10817.equals(str)) {
            try {
                Integer.parseInt(map.get("eventType"));
            } catch (Exception e) {
                zzagf.m4793("Parse Scion log event type error", e);
            }
            if ("_ai".equals(str2)) {
                zzot zzot2 = (zzot) this.f10818.get();
                if (zzot2 != null) {
                    zzot2.zzbz();
                }
            } else if ("_ac".equals(str2) && (zzot = (zzot) this.f10818.get()) != null) {
                zzot.zzca();
            }
        }
    }
}
