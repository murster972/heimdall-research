package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;
import java.util.HashMap;

@zzzv
public final class zzrj extends zzp<zzqc> {
    public zzrj() {
        super("com.google.android.gms.ads.NativeAdViewHolderDelegateCreatorImpl");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzpz m5800(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        try {
            IBinder r2 = ((zzqc) m9308(view.getContext())).m13248(zzn.m9306(view), zzn.m9306(hashMap), zzn.m9306(hashMap2));
            if (r2 == null) {
                return null;
            }
            IInterface queryLocalInterface = r2.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegate");
            return queryLocalInterface instanceof zzpz ? (zzpz) queryLocalInterface : new zzqb(r2);
        } catch (RemoteException | zzq e) {
            zzakb.m4796("Could not create remote NativeAdViewHolderDelegate.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m5801(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegateCreator");
        return queryLocalInterface instanceof zzqc ? (zzqc) queryLocalInterface : new zzqd(iBinder);
    }
}
