package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfek;
import com.google.android.gms.internal.zzfel;

public abstract class zzfel<MessageType extends zzfek<MessageType, BuilderType>, BuilderType extends zzfel<MessageType, BuilderType>> implements zzfhf {
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract BuilderType clone();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract BuilderType m12364(MessageType messagetype);

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfhf m12365(zzfhe zzfhe) {
        if (m12629().getClass().isInstance(zzfhe)) {
            return m12364((zzfek) zzfhe);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
