package com.google.android.gms.internal;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class zzdvo extends WeakReference<Throwable> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f10239;

    public zzdvo(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, (ReferenceQueue) null);
        if (th == null) {
            throw new NullPointerException("The referent cannot be null");
        }
        this.f10239 = System.identityHashCode(th);
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zzdvo zzdvo = (zzdvo) obj;
        return this.f10239 == zzdvo.f10239 && get() == zzdvo.get();
    }

    public final int hashCode() {
        return this.f10239;
    }
}
