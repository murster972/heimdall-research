package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzmf implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzme f10798;

    zzmf(zzme zzme) {
        this.f10798 = zzme;
    }

    public final void run() {
        if (this.f10798.f10797.f10796 != null) {
            try {
                this.f10798.f10797.f10796.m13061(1);
            } catch (RemoteException e) {
                zzakb.m4796("Could not notify onAdFailedToLoad event.", e);
            }
        }
    }
}
