package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.common.util.zzd;
import com.mopub.mobileads.VastExtensionXmlManager;

public final class zzchi extends zzcjl {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f9257;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzchj f9258 = new zzchj(this, m11097(), "google_app_measurement_local.db");

    zzchi(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final SQLiteDatabase m10736() {
        if (this.f9257) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.f9258.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.f9257 = true;
        return null;
    }

    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m10737(int i, byte[] bArr) {
        m11109();
        if (this.f9257) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(VastExtensionXmlManager.TYPE, Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i2 = 5;
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < 5) {
                SQLiteDatabase sQLiteDatabase = null;
                Cursor cursor = null;
                try {
                    sQLiteDatabase = m10736();
                    if (sQLiteDatabase == null) {
                        this.f9257 = true;
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return false;
                    }
                    sQLiteDatabase.beginTransaction();
                    long j = 0;
                    Cursor rawQuery = sQLiteDatabase.rawQuery("select count(1) from messages", (String[]) null);
                    if (rawQuery != null && rawQuery.moveToFirst()) {
                        j = rawQuery.getLong(0);
                    }
                    if (j >= 100000) {
                        m11096().m10832().m10849("Data loss, local db full");
                        long j2 = (100000 - j) + 1;
                        long delete = (long) sQLiteDatabase.delete("messages", "rowid in (select rowid from messages order by rowid asc limit ?)", new String[]{Long.toString(j2)});
                        if (delete != j2) {
                            m11096().m10832().m10852("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                        }
                    }
                    sQLiteDatabase.insertOrThrow("messages", (String) null, contentValues);
                    sQLiteDatabase.setTransactionSuccessful();
                    sQLiteDatabase.endTransaction();
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                    return true;
                } catch (SQLiteFullException e) {
                    m11096().m10832().m10850("Error writing entry to local database", e);
                    this.f9257 = true;
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                } catch (SQLiteException e2) {
                    if (Build.VERSION.SDK_INT < 11 || !(e2 instanceof SQLiteDatabaseLockedException)) {
                        if (sQLiteDatabase != null) {
                            if (sQLiteDatabase.inTransaction()) {
                                sQLiteDatabase.endTransaction();
                            }
                        }
                        m11096().m10832().m10850("Error writing entry to local database", e2);
                        this.f9257 = true;
                    } else {
                        SystemClock.sleep((long) i2);
                        i2 += 20;
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                    throw th;
                }
            } else {
                m11096().m10834().m10849("Failed to write entry to local database");
                return false;
            }
            i3 = i4 + 1;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10738() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10739() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10740() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10741() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10742() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10743() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10744() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10745() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10746() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10747() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10748() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10749() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10750() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10751() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10752() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10753() {
        return super.m11105();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final void m10754() {
        m11109();
        try {
            int delete = m10736().delete("messages", (String) null, (String[]) null) + 0;
            if (delete > 0) {
                m11096().m10848().m10850("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            m11096().m10832().m10850("Error resetting local analytics data. error", e);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10755() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10756() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10757() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10758() {
        super.m11109();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x00ba A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00df A[Catch:{ all -> 0x01ff }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00fe  */
    @android.annotation.TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.zzbfm> m10759(int r14) {
        /*
            r13 = this;
            r13.m11109()
            boolean r0 = r13.f9257
            if (r0 == 0) goto L_0x0009
            r0 = 0
        L_0x0008:
            return r0
        L_0x0009:
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            android.content.Context r0 = r13.m11097()
            java.lang.String r1 = "google_app_measurement_local.db"
            java.io.File r0 = r0.getDatabasePath(r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0021
            r0 = r10
            goto L_0x0008
        L_0x0021:
            r9 = 5
            r0 = 0
            r12 = r0
        L_0x0024:
            r0 = 5
            if (r12 >= r0) goto L_0x01e4
            r3 = 0
            r11 = 0
            android.database.sqlite.SQLiteDatabase r0 = r13.m10736()     // Catch:{ SQLiteFullException -> 0x020d, SQLiteException -> 0x0203, all -> 0x01f5 }
            if (r0 != 0) goto L_0x0039
            r1 = 1
            r13.f9257 = r1     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            if (r0 == 0) goto L_0x0037
            r0.close()
        L_0x0037:
            r0 = 0
            goto L_0x0008
        L_0x0039:
            r0.beginTransaction()     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            java.lang.String r1 = "messages"
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            r3 = 0
            java.lang.String r4 = "rowid"
            r2[r3] = r4     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            r3 = 1
            java.lang.String r4 = "type"
            r2[r3] = r4     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            r3 = 2
            java.lang.String r4 = "entry"
            r2[r3] = r4     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "rowid asc"
            r8 = 100
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteFullException -> 0x0212, SQLiteException -> 0x0208, all -> 0x01fa }
            r4 = -1
        L_0x0067:
            boolean r1 = r2.moveToNext()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            if (r1 == 0) goto L_0x018a
            r1 = 0
            long r4 = r2.getLong(r1)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r1 = 1
            int r1 = r2.getInt(r1)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r3 = 2
            byte[] r6 = r2.getBlob(r3)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            if (r1 != 0) goto L_0x0102
            android.os.Parcel r3 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r1 = 0
            int r7 = r6.length     // Catch:{ zzbfo -> 0x00c0 }
            r3.unmarshall(r6, r1, r7)     // Catch:{ zzbfo -> 0x00c0 }
            r1 = 0
            r3.setDataPosition(r1)     // Catch:{ zzbfo -> 0x00c0 }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcha> r1 = com.google.android.gms.internal.zzcha.CREATOR     // Catch:{ zzbfo -> 0x00c0 }
            java.lang.Object r1 = r1.createFromParcel(r3)     // Catch:{ zzbfo -> 0x00c0 }
            com.google.android.gms.internal.zzcha r1 = (com.google.android.gms.internal.zzcha) r1     // Catch:{ zzbfo -> 0x00c0 }
            r3.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            if (r1 == 0) goto L_0x0067
            r10.add(r1)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            goto L_0x0067
        L_0x009c:
            r1 = move-exception
            r3 = r0
        L_0x009e:
            com.google.android.gms.internal.zzchm r0 = r13.m11096()     // Catch:{ all -> 0x01ff }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ all -> 0x01ff }
            java.lang.String r4 = "Error reading entries from local database"
            r0.m10850(r4, r1)     // Catch:{ all -> 0x01ff }
            r0 = 1
            r13.f9257 = r0     // Catch:{ all -> 0x01ff }
            if (r2 == 0) goto L_0x00b4
            r2.close()
        L_0x00b4:
            if (r3 == 0) goto L_0x0217
            r3.close()
            r0 = r9
        L_0x00ba:
            int r1 = r12 + 1
            r12 = r1
            r9 = r0
            goto L_0x0024
        L_0x00c0:
            r1 = move-exception
            com.google.android.gms.internal.zzchm r1 = r13.m11096()     // Catch:{ all -> 0x00f0 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ all -> 0x00f0 }
            java.lang.String r6 = "Failed to load event from local database"
            r1.m10849(r6)     // Catch:{ all -> 0x00f0 }
            r3.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            goto L_0x0067
        L_0x00d3:
            r1 = move-exception
            r3 = r0
        L_0x00d5:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x01ff }
            r4 = 11
            if (r0 < r4) goto L_0x01c5
            boolean r0 = r1 instanceof android.database.sqlite.SQLiteDatabaseLockedException     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x01c5
            long r0 = (long) r9     // Catch:{ all -> 0x01ff }
            android.os.SystemClock.sleep(r0)     // Catch:{ all -> 0x01ff }
            int r0 = r9 + 20
        L_0x00e5:
            if (r2 == 0) goto L_0x00ea
            r2.close()
        L_0x00ea:
            if (r3 == 0) goto L_0x00ba
            r3.close()
            goto L_0x00ba
        L_0x00f0:
            r1 = move-exception
            r3.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            throw r1     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
        L_0x00f5:
            r1 = move-exception
            r3 = r0
        L_0x00f7:
            if (r2 == 0) goto L_0x00fc
            r2.close()
        L_0x00fc:
            if (r3 == 0) goto L_0x0101
            r3.close()
        L_0x0101:
            throw r1
        L_0x0102:
            r3 = 1
            if (r1 != r3) goto L_0x013e
            android.os.Parcel r7 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r3 = 0
            r1 = 0
            int r8 = r6.length     // Catch:{ zzbfo -> 0x0125 }
            r7.unmarshall(r6, r1, r8)     // Catch:{ zzbfo -> 0x0125 }
            r1 = 0
            r7.setDataPosition(r1)     // Catch:{ zzbfo -> 0x0125 }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcln> r1 = com.google.android.gms.internal.zzcln.CREATOR     // Catch:{ zzbfo -> 0x0125 }
            java.lang.Object r1 = r1.createFromParcel(r7)     // Catch:{ zzbfo -> 0x0125 }
            com.google.android.gms.internal.zzcln r1 = (com.google.android.gms.internal.zzcln) r1     // Catch:{ zzbfo -> 0x0125 }
            r7.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
        L_0x011e:
            if (r1 == 0) goto L_0x0067
            r10.add(r1)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            goto L_0x0067
        L_0x0125:
            r1 = move-exception
            com.google.android.gms.internal.zzchm r1 = r13.m11096()     // Catch:{ all -> 0x0139 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ all -> 0x0139 }
            java.lang.String r6 = "Failed to load user property from local database"
            r1.m10849(r6)     // Catch:{ all -> 0x0139 }
            r7.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r1 = r3
            goto L_0x011e
        L_0x0139:
            r1 = move-exception
            r7.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            throw r1     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
        L_0x013e:
            r3 = 2
            if (r1 != r3) goto L_0x017a
            android.os.Parcel r7 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r3 = 0
            r1 = 0
            int r8 = r6.length     // Catch:{ zzbfo -> 0x0161 }
            r7.unmarshall(r6, r1, r8)     // Catch:{ zzbfo -> 0x0161 }
            r1 = 0
            r7.setDataPosition(r1)     // Catch:{ zzbfo -> 0x0161 }
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzcgl> r1 = com.google.android.gms.internal.zzcgl.CREATOR     // Catch:{ zzbfo -> 0x0161 }
            java.lang.Object r1 = r1.createFromParcel(r7)     // Catch:{ zzbfo -> 0x0161 }
            com.google.android.gms.internal.zzcgl r1 = (com.google.android.gms.internal.zzcgl) r1     // Catch:{ zzbfo -> 0x0161 }
            r7.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
        L_0x015a:
            if (r1 == 0) goto L_0x0067
            r10.add(r1)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            goto L_0x0067
        L_0x0161:
            r1 = move-exception
            com.google.android.gms.internal.zzchm r1 = r13.m11096()     // Catch:{ all -> 0x0175 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ all -> 0x0175 }
            java.lang.String r6 = "Failed to load user property from local database"
            r1.m10849(r6)     // Catch:{ all -> 0x0175 }
            r7.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r1 = r3
            goto L_0x015a
        L_0x0175:
            r1 = move-exception
            r7.recycle()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            throw r1     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
        L_0x017a:
            com.google.android.gms.internal.zzchm r1 = r13.m11096()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            java.lang.String r3 = "Unknown record type in local database"
            r1.m10849(r3)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            goto L_0x0067
        L_0x018a:
            java.lang.String r1 = "messages"
            java.lang.String r3 = "rowid <= ?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r7 = 0
            java.lang.String r4 = java.lang.Long.toString(r4)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r6[r7] = r4     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            int r1 = r0.delete(r1, r3, r6)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            int r3 = r10.size()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            if (r1 >= r3) goto L_0x01b2
            com.google.android.gms.internal.zzchm r1 = r13.m11096()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            java.lang.String r3 = "Fewer entries removed from local database than expected"
            r1.m10849(r3)     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
        L_0x01b2:
            r0.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            r0.endTransaction()     // Catch:{ SQLiteFullException -> 0x009c, SQLiteException -> 0x00d3, all -> 0x00f5 }
            if (r2 == 0) goto L_0x01bd
            r2.close()
        L_0x01bd:
            if (r0 == 0) goto L_0x01c2
            r0.close()
        L_0x01c2:
            r0 = r10
            goto L_0x0008
        L_0x01c5:
            if (r3 == 0) goto L_0x01d0
            boolean r0 = r3.inTransaction()     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x01d0
            r3.endTransaction()     // Catch:{ all -> 0x01ff }
        L_0x01d0:
            com.google.android.gms.internal.zzchm r0 = r13.m11096()     // Catch:{ all -> 0x01ff }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ all -> 0x01ff }
            java.lang.String r4 = "Error reading entries from local database"
            r0.m10850(r4, r1)     // Catch:{ all -> 0x01ff }
            r0 = 1
            r13.f9257 = r0     // Catch:{ all -> 0x01ff }
            r0 = r9
            goto L_0x00e5
        L_0x01e4:
            com.google.android.gms.internal.zzchm r0 = r13.m11096()
            com.google.android.gms.internal.zzcho r0 = r0.m10834()
            java.lang.String r1 = "Failed to read events from database in reasonable time"
            r0.m10849(r1)
            r0 = 0
            goto L_0x0008
        L_0x01f5:
            r0 = move-exception
            r1 = r0
            r2 = r11
            goto L_0x00f7
        L_0x01fa:
            r1 = move-exception
            r2 = r11
            r3 = r0
            goto L_0x00f7
        L_0x01ff:
            r0 = move-exception
            r1 = r0
            goto L_0x00f7
        L_0x0203:
            r0 = move-exception
            r1 = r0
            r2 = r11
            goto L_0x00d5
        L_0x0208:
            r1 = move-exception
            r2 = r11
            r3 = r0
            goto L_0x00d5
        L_0x020d:
            r0 = move-exception
            r1 = r0
            r2 = r11
            goto L_0x009e
        L_0x0212:
            r1 = move-exception
            r2 = r11
            r3 = r0
            goto L_0x009e
        L_0x0217:
            r0 = r9
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzchi.m10759(int):java.util.List");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10760() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10761(zzcgl zzcgl) {
        m11112();
        byte[] r0 = zzclq.m11391((Parcelable) zzcgl);
        if (r0.length <= 131072) {
            return m10737(2, r0);
        }
        m11096().m10834().m10849("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10762(zzcha zzcha) {
        Parcel obtain = Parcel.obtain();
        zzcha.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return m10737(0, marshall);
        }
        m11096().m10834().m10849("Event is too long for local database. Sending event directly to service");
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10763(zzcln zzcln) {
        Parcel obtain = Parcel.obtain();
        zzcln.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return m10737(1, marshall);
        }
        m11096().m10834().m10849("User property too long for local database. Sending directly to service");
        return false;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10764() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10765() {
        return super.m11112();
    }
}
