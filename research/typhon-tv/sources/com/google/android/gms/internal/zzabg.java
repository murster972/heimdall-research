package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzabg extends zzeu implements zzabe {
    zzabg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdResponseListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9449(zzaax zzaax) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzaax);
        m12298(1, v_);
    }
}
