package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.js.zzaj;
import java.util.Map;

final class zzgn implements zzt<zzaj> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzgf f10652;

    zzgn(zzgf zzgf) {
        this.f10652 = zzgf;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzaj zzaj = (zzaj) obj;
        if (this.f10652.f4652.m5334((Map<String, String>) map)) {
            this.f10652.f4651.zza(zzaj, map);
        }
    }
}
