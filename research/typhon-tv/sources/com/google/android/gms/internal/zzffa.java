package com.google.android.gms.internal;

final class zzffa implements zzfew {
    private zzffa() {
    }

    /* synthetic */ zzffa(zzfet zzfet) {
        this();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12396(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
