package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;

final class zzdf implements zzdi {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f9862;

    zzdf(zzda zzda, Activity activity) {
        this.f9862 = activity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11589(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStopped(this.f9862);
    }
}
