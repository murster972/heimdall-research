package com.google.android.gms.internal;

final class zzcks implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcln f9600;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzckg f9601;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9602;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ boolean f9603;

    zzcks(zzckg zzckg, boolean z, zzcln zzcln, zzcgi zzcgi) {
        this.f9601 = zzckg;
        this.f9603 = z;
        this.f9600 = zzcln;
        this.f9602 = zzcgi;
    }

    public final void run() {
        zzche r1 = this.f9601.f9558;
        if (r1 == null) {
            this.f9601.m11096().m10832().m10849("Discarding data. Failed to set user attribute");
            return;
        }
        this.f9601.m11264(r1, this.f9603 ? null : this.f9600, this.f9602);
        this.f9601.m11223();
    }
}
