package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzcx {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Cipher f9812 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Object f9813 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Object f9814 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private final SecureRandom f9815 = null;

    public zzcx(SecureRandom secureRandom) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Cipher m11542() throws NoSuchAlgorithmException, NoSuchPaddingException {
        Cipher cipher;
        synchronized (f9813) {
            if (f9812 == null) {
                f9812 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            }
            cipher = f9812;
        }
        return cipher;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m11543(byte[] bArr, byte[] bArr2) throws zzcy {
        byte[] doFinal;
        byte[] iv;
        if (bArr.length != 16) {
            throw new zzcy(this);
        }
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            synchronized (f9814) {
                m11542().init(1, secretKeySpec, (SecureRandom) null);
                doFinal = m11542().doFinal(bArr2);
                iv = m11542().getIV();
            }
            int length = doFinal.length + iv.length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            allocate.put(iv).put(doFinal);
            allocate.flip();
            byte[] bArr3 = new byte[length];
            allocate.get(bArr3);
            return zzbu.m10295(bArr3, false);
        } catch (NoSuchAlgorithmException e) {
            throw new zzcy(this, e);
        } catch (InvalidKeyException e2) {
            throw new zzcy(this, e2);
        } catch (IllegalBlockSizeException e3) {
            throw new zzcy(this, e3);
        } catch (NoSuchPaddingException e4) {
            throw new zzcy(this, e4);
        } catch (BadPaddingException e5) {
            throw new zzcy(this, e5);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m11544(String str) throws zzcy {
        try {
            byte[] r1 = zzbu.m10296(str, false);
            if (r1.length != 32) {
                throw new zzcy(this);
            }
            byte[] bArr = new byte[16];
            ByteBuffer.wrap(r1, 4, 16).get(bArr);
            for (int i = 0; i < 16; i++) {
                bArr[i] = (byte) (bArr[i] ^ 68);
            }
            return bArr;
        } catch (IllegalArgumentException e) {
            throw new zzcy(this, e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m11545(byte[] bArr, String str) throws zzcy {
        byte[] doFinal;
        if (bArr.length != 16) {
            throw new zzcy(this);
        }
        try {
            byte[] r0 = zzbu.m10296(str, false);
            if (r0.length <= 16) {
                throw new zzcy(this);
            }
            ByteBuffer allocate = ByteBuffer.allocate(r0.length);
            allocate.put(r0);
            allocate.flip();
            byte[] bArr2 = new byte[16];
            byte[] bArr3 = new byte[(r0.length - 16)];
            allocate.get(bArr2);
            allocate.get(bArr3);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            synchronized (f9814) {
                m11542().init(2, secretKeySpec, new IvParameterSpec(bArr2));
                doFinal = m11542().doFinal(bArr3);
            }
            return doFinal;
        } catch (NoSuchAlgorithmException e) {
            throw new zzcy(this, e);
        } catch (InvalidKeyException e2) {
            throw new zzcy(this, e2);
        } catch (IllegalBlockSizeException e3) {
            throw new zzcy(this, e3);
        } catch (NoSuchPaddingException e4) {
            throw new zzcy(this, e4);
        } catch (BadPaddingException e5) {
            throw new zzcy(this, e5);
        } catch (InvalidAlgorithmParameterException e6) {
            throw new zzcy(this, e6);
        } catch (IllegalArgumentException e7) {
            throw new zzcy(this, e7);
        }
    }
}
