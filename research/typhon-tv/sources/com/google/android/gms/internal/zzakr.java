package com.google.android.gms.internal;

import java.util.concurrent.Executor;

final /* synthetic */ class zzakr implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private final Executor f8279;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzakv f8280;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzakg f8281;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Class f8282;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzalf f8283;

    zzakr(zzalf zzalf, zzakv zzakv, Class cls, zzakg zzakg, Executor executor) {
        this.f8283 = zzalf;
        this.f8280 = zzakv;
        this.f8282 = cls;
        this.f8281 = zzakg;
        this.f8279 = executor;
    }

    public final void run() {
        zzakl.m4813(this.f8283, this.f8280, this.f8282, this.f8281, this.f8279);
    }
}
