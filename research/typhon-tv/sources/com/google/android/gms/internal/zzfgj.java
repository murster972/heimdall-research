package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzfgj<K> implements Iterator<Map.Entry<K, Object>> {

    /* renamed from: 龘  reason: contains not printable characters */
    private Iterator<Map.Entry<K, Object>> f10425;

    public zzfgj(Iterator<Map.Entry<K, Object>> it2) {
        this.f10425 = it2;
    }

    public final boolean hasNext() {
        return this.f10425.hasNext();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.f10425.next();
        return next.getValue() instanceof zzfgg ? new zzfgi(next) : next;
    }

    public final void remove() {
        this.f10425.remove();
    }
}
