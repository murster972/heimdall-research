package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzdtx implements zzdvf {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f10170;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f10171 = zzduu.f10221.m12217("AES/CTR/NoPadding").getBlockSize();

    /* renamed from: 龘  reason: contains not printable characters */
    private final SecretKeySpec f10172;

    public zzdtx(byte[] bArr, int i) throws GeneralSecurityException {
        this.f10172 = new SecretKeySpec(bArr, "AES");
        if (i < 12 || i > this.f10171) {
            throw new GeneralSecurityException("invalid IV size");
        }
        this.f10170 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12167(byte[] bArr) throws GeneralSecurityException {
        if (bArr.length > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - this.f10170) {
            throw new GeneralSecurityException(new StringBuilder(43).append("plaintext length can not exceed ").append(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - this.f10170).toString());
        }
        byte[] bArr2 = new byte[(this.f10170 + bArr.length)];
        byte[] r1 = zzdvi.m12235(this.f10170);
        System.arraycopy(r1, 0, bArr2, 0, this.f10170);
        int length = bArr.length;
        int i = this.f10170;
        Cipher r0 = zzduu.f10221.m12217("AES/CTR/NoPadding");
        byte[] bArr3 = new byte[this.f10171];
        System.arraycopy(r1, 0, bArr3, 0, this.f10170);
        r0.init(1, this.f10172, new IvParameterSpec(bArr3));
        if (r0.doFinal(bArr, 0, length, bArr2, i) == length) {
            return bArr2;
        }
        throw new GeneralSecurityException("stored output's length does not match input's length");
    }
}
