package com.google.android.gms.internal;

public class zzfgk {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzffm f10426 = zzffm.m12500();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzfes f10427;

    /* renamed from: 麤  reason: contains not printable characters */
    private volatile zzfes f10428;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile zzfhe f10429;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzfhe m12596(zzfhe zzfhe) {
        if (this.f10429 == null) {
            synchronized (this) {
                if (this.f10429 == null) {
                    try {
                        this.f10429 = zzfhe;
                        this.f10428 = zzfes.zzpfg;
                    } catch (zzfge e) {
                        this.f10429 = zzfhe;
                        this.f10428 = zzfes.zzpfg;
                    }
                }
            }
        }
        return this.f10429;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfgk)) {
            return false;
        }
        zzfgk zzfgk = (zzfgk) obj;
        zzfhe zzfhe = this.f10429;
        zzfhe zzfhe2 = zzfgk.f10429;
        return (zzfhe == null && zzfhe2 == null) ? m12598().equals(zzfgk.m12598()) : (zzfhe == null || zzfhe2 == null) ? zzfhe != null ? zzfhe.equals(zzfgk.m12596(zzfhe.m12629())) : m12596(zzfhe2.m12629()).equals(zzfhe2) : zzfhe.equals(zzfhe2);
    }

    public int hashCode() {
        return 1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m12597() {
        if (this.f10428 != null) {
            return this.f10428.size();
        }
        if (this.f10429 != null) {
            return this.f10429.m12625();
        }
        return 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m12598() {
        if (this.f10428 != null) {
            return this.f10428;
        }
        synchronized (this) {
            if (this.f10428 != null) {
                zzfes zzfes = this.f10428;
                return zzfes;
            }
            if (this.f10429 == null) {
                this.f10428 = zzfes.zzpfg;
            } else {
                this.f10428 = this.f10429.m12623();
            }
            zzfes zzfes2 = this.f10428;
            return zzfes2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzfhe m12599(zzfhe zzfhe) {
        zzfhe zzfhe2 = this.f10429;
        this.f10427 = null;
        this.f10428 = null;
        this.f10429 = zzfhe;
        return zzfhe2;
    }
}
