package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

@zzzv
public final class zzafq {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f4137;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f4138;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f4139;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f4140;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f4141;

    /* renamed from: ٴ  reason: contains not printable characters */
    private long f4142;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f4143;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f4144;

    /* renamed from: 靐  reason: contains not printable characters */
    private final LinkedList<zzafr> f4145;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f4146;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Object f4147;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaft f4148;

    private zzafq(zzaft zzaft, String str, String str2) {
        this.f4147 = new Object();
        this.f4137 = -1;
        this.f4138 = -1;
        this.f4139 = false;
        this.f4141 = -1;
        this.f4142 = 0;
        this.f4143 = -1;
        this.f4140 = -1;
        this.f4148 = zzaft;
        this.f4146 = str;
        this.f4144 = str2;
        this.f4145 = new LinkedList<>();
    }

    public zzafq(String str, String str2) {
        this(zzbs.zzem(), str, str2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4443() {
        synchronized (this.f4147) {
            if (this.f4140 != -1) {
                zzafr zzafr = new zzafr();
                zzafr.m4454();
                this.f4145.add(zzafr);
                this.f4142++;
                this.f4148.m4488().m4514();
                this.f4148.m4502(this);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4444(long j) {
        synchronized (this.f4147) {
            if (this.f4140 != -1) {
                this.f4137 = j;
                this.f4148.m4502(this);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4445(boolean z) {
        synchronized (this.f4147) {
            if (this.f4140 != -1) {
                this.f4139 = z;
                this.f4148.m4502(this);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Bundle m4446() {
        Bundle bundle;
        synchronized (this.f4147) {
            bundle = new Bundle();
            bundle.putString("seq_num", this.f4146);
            bundle.putString("slotid", this.f4144);
            bundle.putBoolean("ismediation", this.f4139);
            bundle.putLong("treq", this.f4143);
            bundle.putLong("tresponse", this.f4140);
            bundle.putLong("timp", this.f4138);
            bundle.putLong("tload", this.f4141);
            bundle.putLong("pcc", this.f4142);
            bundle.putLong("tfetch", this.f4137);
            ArrayList arrayList = new ArrayList();
            Iterator it2 = this.f4145.iterator();
            while (it2.hasNext()) {
                arrayList.add(((zzafr) it2.next()).m4453());
            }
            bundle.putParcelableArrayList("tclick", arrayList);
        }
        return bundle;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4447() {
        synchronized (this.f4147) {
            if (this.f4140 != -1 && !this.f4145.isEmpty()) {
                zzafr last = this.f4145.getLast();
                if (last.m4455() == -1) {
                    last.m4452();
                    this.f4148.m4502(this);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4448() {
        synchronized (this.f4147) {
            if (this.f4140 != -1 && this.f4138 == -1) {
                this.f4138 = SystemClock.elapsedRealtime();
                this.f4148.m4502(this);
            }
            this.f4148.m4488().m4512();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4449(long j) {
        synchronized (this.f4147) {
            this.f4140 = j;
            if (this.f4140 != -1) {
                this.f4148.m4502(this);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4450(zzjj zzjj) {
        synchronized (this.f4147) {
            this.f4143 = SystemClock.elapsedRealtime();
            this.f4148.m4488().m4515(zzjj, this.f4143);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4451(boolean z) {
        synchronized (this.f4147) {
            if (this.f4140 != -1) {
                this.f4141 = SystemClock.elapsedRealtime();
                if (!z) {
                    this.f4138 = this.f4141;
                    this.f4148.m4502(this);
                }
            }
        }
    }
}
