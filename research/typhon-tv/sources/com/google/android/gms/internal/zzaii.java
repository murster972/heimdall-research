package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.content.Intent;
import com.google.android.gms.ads.internal.zzbs;

final class zzaii implements DialogInterface.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzaig f8223;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f8224;

    zzaii(zzaig zzaig, String str) {
        this.f8223 = zzaig;
        this.f8224 = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        zzbs.zzei();
        zzahn.m4608(this.f8223.f4235, Intent.createChooser(new Intent("android.intent.action.SEND").setType("text/plain").putExtra("android.intent.extra.TEXT", this.f8224), "Share via"));
    }
}
