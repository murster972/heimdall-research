package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.measurement.AppMeasurement$Event;
import com.google.android.gms.measurement.AppMeasurement$Param;
import com.google.android.gms.measurement.AppMeasurement$UserProperty;
import org.apache.commons.lang3.StringUtils;

public final class zzchk extends zzcjl {

    /* renamed from: 靐  reason: contains not printable characters */
    private static String[] f9260 = new String[AppMeasurement$Param.f11085.length];

    /* renamed from: 齉  reason: contains not printable characters */
    private static String[] f9261 = new String[AppMeasurement$UserProperty.f11087.length];

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] f9262 = new String[AppMeasurement$Event.f11083.length];

    zzchk(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final boolean m10766() {
        return this.f9487.m11016().m10844(3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final String m10767(zzcgx zzcgx) {
        if (zzcgx == null) {
            return null;
        }
        return !m10766() ? zzcgx.toString() : m10799(zzcgx.m10660());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m10768(String str, String[] strArr, String[] strArr2, String[] strArr3) {
        boolean z = true;
        int i = 0;
        zzbq.m9120(strArr);
        zzbq.m9120(strArr2);
        zzbq.m9120(strArr3);
        zzbq.m9116(strArr.length == strArr2.length);
        if (strArr.length != strArr3.length) {
            z = false;
        }
        zzbq.m9116(z);
        while (true) {
            if (i >= strArr.length) {
                break;
            } else if (zzclq.m11387(str, strArr[i])) {
                synchronized (strArr3) {
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str = strArr3[i];
                }
            } else {
                i++;
            }
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10769(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10770(StringBuilder sb, int i, zzclt zzclt) {
        if (zzclt != null) {
            m10769(sb, i);
            sb.append("filter {\n");
            m10773(sb, i, "complement", (Object) zzclt.f9676);
            m10773(sb, i, "param_name", (Object) m10794(zzclt.f9675));
            int i2 = i + 1;
            zzclw zzclw = zzclt.f9677;
            if (zzclw != null) {
                m10769(sb, i2);
                sb.append("string_filter");
                sb.append(" {\n");
                if (zzclw.f9690 != null) {
                    String str = "UNKNOWN_MATCH_TYPE";
                    switch (zzclw.f9690.intValue()) {
                        case 1:
                            str = "REGEXP";
                            break;
                        case 2:
                            str = "BEGINS_WITH";
                            break;
                        case 3:
                            str = "ENDS_WITH";
                            break;
                        case 4:
                            str = "PARTIAL";
                            break;
                        case 5:
                            str = "EXACT";
                            break;
                        case 6:
                            str = "IN_LIST";
                            break;
                    }
                    m10773(sb, i2, "match_type", (Object) str);
                }
                m10773(sb, i2, "expression", (Object) zzclw.f9687);
                m10773(sb, i2, "case_sensitive", (Object) zzclw.f9689);
                if (zzclw.f9688.length > 0) {
                    m10769(sb, i2 + 1);
                    sb.append("expression_list {\n");
                    for (String append : zzclw.f9688) {
                        m10769(sb, i2 + 2);
                        sb.append(append);
                        sb.append(StringUtils.LF);
                    }
                    sb.append("}\n");
                }
                m10769(sb, i2);
                sb.append("}\n");
            }
            m10771(sb, i + 1, "number_filter", zzclt.f9674);
            m10769(sb, i);
            sb.append("}\n");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10771(StringBuilder sb, int i, String str, zzclu zzclu) {
        if (zzclu != null) {
            m10769(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (zzclu.f9682 != null) {
                String str2 = "UNKNOWN_COMPARISON_TYPE";
                switch (zzclu.f9682.intValue()) {
                    case 1:
                        str2 = "LESS_THAN";
                        break;
                    case 2:
                        str2 = "GREATER_THAN";
                        break;
                    case 3:
                        str2 = "EQUAL";
                        break;
                    case 4:
                        str2 = "BETWEEN";
                        break;
                }
                m10773(sb, i, "comparison_type", (Object) str2);
            }
            m10773(sb, i, "match_as_float", (Object) zzclu.f9679);
            m10773(sb, i, "comparison_value", (Object) zzclu.f9681);
            m10773(sb, i, "min_comparison_value", (Object) zzclu.f9680);
            m10773(sb, i, "max_comparison_value", (Object) zzclu.f9678);
            m10769(sb, i);
            sb.append("}\n");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10772(StringBuilder sb, int i, String str, zzcmf zzcmf) {
        if (zzcmf != null) {
            int i2 = i + 1;
            m10769(sb, i2);
            sb.append(str);
            sb.append(" {\n");
            if (zzcmf.f9760 != null) {
                m10769(sb, i2 + 1);
                sb.append("results: ");
                long[] jArr = zzcmf.f9760;
                int length = jArr.length;
                int i3 = 0;
                int i4 = 0;
                while (i3 < length) {
                    Long valueOf = Long.valueOf(jArr[i3]);
                    int i5 = i4 + 1;
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(valueOf);
                    i3++;
                    i4 = i5;
                }
                sb.append(10);
            }
            if (zzcmf.f9761 != null) {
                m10769(sb, i2 + 1);
                sb.append("status: ");
                long[] jArr2 = zzcmf.f9761;
                int length2 = jArr2.length;
                int i6 = 0;
                int i7 = 0;
                while (i6 < length2) {
                    Long valueOf2 = Long.valueOf(jArr2[i6]);
                    int i8 = i7 + 1;
                    if (i7 != 0) {
                        sb.append(", ");
                    }
                    sb.append(valueOf2);
                    i6++;
                    i7 = i8;
                }
                sb.append(10);
            }
            m10769(sb, i2);
            sb.append("}\n");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10773(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            m10769(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10774(StringBuilder sb, int i, zzcma[] zzcmaArr) {
        if (zzcmaArr != null) {
            for (zzcma zzcma : zzcmaArr) {
                if (zzcma != null) {
                    m10769(sb, 2);
                    sb.append("audience_membership {\n");
                    m10773(sb, 2, "audience_id", (Object) zzcma.f9710);
                    m10773(sb, 2, "new_audience", (Object) zzcma.f9708);
                    m10772(sb, 2, "current_data", zzcma.f9707);
                    m10772(sb, 2, "previous_data", zzcma.f9709);
                    m10769(sb, 2);
                    sb.append("}\n");
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10775(StringBuilder sb, int i, zzcmb[] zzcmbArr) {
        if (zzcmbArr != null) {
            for (zzcmb zzcmb : zzcmbArr) {
                if (zzcmb != null) {
                    m10769(sb, 2);
                    sb.append("event {\n");
                    m10773(sb, 2, "name", (Object) m10805(zzcmb.f9713));
                    m10773(sb, 2, "timestamp_millis", (Object) zzcmb.f9715);
                    m10773(sb, 2, "previous_timestamp_millis", (Object) zzcmb.f9714);
                    m10773(sb, 2, "count", (Object) zzcmb.f9712);
                    zzcmc[] zzcmcArr = zzcmb.f9716;
                    if (zzcmcArr != null) {
                        for (zzcmc zzcmc : zzcmcArr) {
                            if (zzcmc != null) {
                                m10769(sb, 3);
                                sb.append("param {\n");
                                m10773(sb, 3, "name", (Object) m10794(zzcmc.f9722));
                                m10773(sb, 3, "string_value", (Object) zzcmc.f9719);
                                m10773(sb, 3, "int_value", (Object) zzcmc.f9721);
                                m10773(sb, 3, "double_value", (Object) zzcmc.f9720);
                                m10769(sb, 3);
                                sb.append("}\n");
                            }
                        }
                    }
                    m10769(sb, 2);
                    sb.append("}\n");
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10776(StringBuilder sb, int i, zzcmg[] zzcmgArr) {
        if (zzcmgArr != null) {
            for (zzcmg zzcmg : zzcmgArr) {
                if (zzcmg != null) {
                    m10769(sb, 2);
                    sb.append("user_property {\n");
                    m10773(sb, 2, "set_timestamp_millis", (Object) zzcmg.f9768);
                    m10773(sb, 2, "name", (Object) m10797(zzcmg.f9765));
                    m10773(sb, 2, "string_value", (Object) zzcmg.f9767);
                    m10773(sb, 2, "int_value", (Object) zzcmg.f9766);
                    m10773(sb, 2, "double_value", (Object) zzcmg.f9764);
                    m10769(sb, 2);
                    sb.append("}\n");
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10777() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10778() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10779() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10780() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10781() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10782() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10783() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10784() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10785() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10786() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10787() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10788() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10789() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10790() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10791() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10792() {
        return super.m11105();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10793() {
        return super.m11106();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10794(String str) {
        if (str == null) {
            return null;
        }
        return m10766() ? m10768(str, AppMeasurement$Param.f11084, AppMeasurement$Param.f11085, f9260) : str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10795() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10796() {
        return super.m11108();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final String m10797(String str) {
        if (str == null) {
            return null;
        }
        if (!m10766()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return m10768(str, AppMeasurement$UserProperty.f11086, AppMeasurement$UserProperty.f11087, f9261);
        }
        return "experiment_id" + "(" + str + ")";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10798() {
        super.m11109();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10799(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!m10766()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        for (String str : bundle.keySet()) {
            if (sb.length() != 0) {
                sb.append(", ");
            } else {
                sb.append("Bundle[{");
            }
            sb.append(m10794(str));
            sb.append("=");
            sb.append(bundle.get(str));
        }
        sb.append("}]");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10800(zzcgv zzcgv) {
        if (zzcgv == null) {
            return null;
        }
        if (!m10766()) {
            return zzcgv.toString();
        }
        return "Event{appId='" + zzcgv.f9186 + "', name='" + m10805(zzcgv.f9183) + "', params=" + m10767(zzcgv.f9182) + "}";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10801(zzcha zzcha) {
        if (zzcha == null) {
            return null;
        }
        if (!m10766()) {
            return zzcha.toString();
        }
        return "origin=" + zzcha.f9202 + ",name=" + m10805(zzcha.f9203) + ",params=" + m10767(zzcha.f9200);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10802(zzcls zzcls) {
        if (zzcls == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        m10773(sb, 0, "filter_id", (Object) zzcls.f9672);
        m10773(sb, 0, "event_name", (Object) m10805(zzcls.f9669));
        m10771(sb, 1, "event_count_filter", zzcls.f9670);
        sb.append("  filters {\n");
        for (zzclt r4 : zzcls.f9671) {
            m10770(sb, 2, r4);
        }
        m10769(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10803(zzclv zzclv) {
        if (zzclv == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        m10773(sb, 0, "filter_id", (Object) zzclv.f9686);
        m10773(sb, 0, "property_name", (Object) m10797(zzclv.f9684));
        m10770(sb, 1, zzclv.f9685);
        sb.append("}\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10804(zzcmd zzcmd) {
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        if (zzcmd.f9723 != null) {
            for (zzcme zzcme : zzcmd.f9723) {
                if (!(zzcme == null || zzcme == null)) {
                    m10769(sb, 1);
                    sb.append("bundle {\n");
                    m10773(sb, 1, "protocol_version", (Object) zzcme.f9755);
                    m10773(sb, 1, "platform", (Object) zzcme.f9739);
                    m10773(sb, 1, "gmp_version", (Object) zzcme.f9735);
                    m10773(sb, 1, "uploading_gmp_version", (Object) zzcme.f9736);
                    m10773(sb, 1, "config_version", (Object) zzcme.f9759);
                    m10773(sb, 1, "gmp_app_id", (Object) zzcme.f9745);
                    m10773(sb, 1, "app_id", (Object) zzcme.f9757);
                    m10773(sb, 1, "app_version", (Object) zzcme.f9758);
                    m10773(sb, 1, "app_version_major", (Object) zzcme.f9750);
                    m10773(sb, 1, "firebase_instance_id", (Object) zzcme.f9749);
                    m10773(sb, 1, "dev_cert_hash", (Object) zzcme.f9738);
                    m10773(sb, 1, "app_store", (Object) zzcme.f9731);
                    m10773(sb, 1, "upload_timestamp_millis", (Object) zzcme.f9753);
                    m10773(sb, 1, "start_timestamp_millis", (Object) zzcme.f9751);
                    m10773(sb, 1, "end_timestamp_millis", (Object) zzcme.f9725);
                    m10773(sb, 1, "previous_bundle_start_timestamp_millis", (Object) zzcme.f9726);
                    m10773(sb, 1, "previous_bundle_end_timestamp_millis", (Object) zzcme.f9728);
                    m10773(sb, 1, "app_instance_id", (Object) zzcme.f9734);
                    m10773(sb, 1, "resettable_device_id", (Object) zzcme.f9737);
                    m10773(sb, 1, "device_id", (Object) zzcme.f9756);
                    m10773(sb, 1, "limited_ad_tracking", (Object) zzcme.f9732);
                    m10773(sb, 1, "os_version", (Object) zzcme.f9742);
                    m10773(sb, 1, "device_model", (Object) zzcme.f9743);
                    m10773(sb, 1, "user_default_language", (Object) zzcme.f9733);
                    m10773(sb, 1, "time_zone_offset_minutes", (Object) zzcme.f9730);
                    m10773(sb, 1, "bundle_sequential_index", (Object) zzcme.f9740);
                    m10773(sb, 1, "service_upload", (Object) zzcme.f9747);
                    m10773(sb, 1, "health_monitor", (Object) zzcme.f9741);
                    if (zzcme.f9744.longValue() != 0) {
                        m10773(sb, 1, "android_id", (Object) zzcme.f9744);
                    }
                    m10776(sb, 1, zzcme.f9754);
                    m10774(sb, 1, zzcme.f9748);
                    m10775(sb, 1, zzcme.f9752);
                    m10769(sb, 1);
                    sb.append("}\n");
                }
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10805(String str) {
        if (str == null) {
            return null;
        }
        return m10766() ? m10768(str, AppMeasurement$Event.f11082, AppMeasurement$Event.f11083, f9262) : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10806() {
        super.m11110();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10807() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10808() {
        return super.m11112();
    }
}
