package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;

final class zzgy implements zzhc {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f10675;

    zzgy(zzgu zzgu, Activity activity) {
        this.f10675 = activity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12976(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityPaused(this.f10675);
    }
}
