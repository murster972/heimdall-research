package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import org.json.JSONObject;

@zzzv
public interface zzzb<T> {
    /* renamed from: 靐  reason: contains not printable characters */
    zzakv<JSONObject> m6071(JSONObject jSONObject);

    /* renamed from: 靐  reason: contains not printable characters */
    void m6072(String str, zzt<? super T> zzt);

    /* renamed from: 齉  reason: contains not printable characters */
    zzakv<JSONObject> m6073(JSONObject jSONObject);

    /* renamed from: 龘  reason: contains not printable characters */
    zzakv<JSONObject> m6074(JSONObject jSONObject);

    /* renamed from: 龘  reason: contains not printable characters */
    void m6075();

    /* renamed from: 龘  reason: contains not printable characters */
    void m6076(String str, zzt<? super T> zzt);

    /* renamed from: 龘  reason: contains not printable characters */
    void m6077(String str, JSONObject jSONObject);
}
