package com.google.android.gms.internal;

import android.content.Context;
import java.io.File;
import java.util.Map;
import org.apache.oltu.oauth2.common.OAuth;

@zzzv
public final class zzaiv {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f4247 = new Object();
    @Deprecated

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzajb<Void> f4248 = new zzaiw();

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzv f4249;

    public zzaiv(Context context) {
        m4711(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzv m4711(Context context) {
        zzv zzv;
        zzv zzv2;
        synchronized (f4247) {
            if (f4249 == null) {
                Context applicationContext = context.getApplicationContext();
                zznh.m5599(applicationContext);
                if (((Boolean) zzkb.m5481().m5595(zznh.f5021)).booleanValue()) {
                    zzv2 = zzaip.m4706(applicationContext);
                } else {
                    zzv2 = new zzv(new zzal(new File(applicationContext.getCacheDir(), "volley")), new zzai((zzah) new zzar()));
                    zzv2.m13454();
                }
                f4249 = zzv2;
            }
            zzv = f4249;
        }
        return zzv;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<String> m4712(int i, String str, Map<String, String> map, byte[] bArr) {
        zzajc zzajc = new zzajc((zzaiw) null);
        zzaiz zzaiz = new zzaiz(this, str, zzajc);
        zzajv zzajv = new zzajv((String) null);
        zzaja zzaja = new zzaja(this, i, str, zzajc, zzaiz, bArr, map, zzajv);
        if (zzajv.m4775()) {
            try {
                zzajv.m4786(str, OAuth.HttpMethod.GET, zzaja.m13360(), zzaja.m13373());
            } catch (zza e) {
                zzagf.m4791(e.getMessage());
            }
        }
        f4249.m13453(zzaja);
        return zzajc;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public final <T> zzakv<T> m4713(String str, zzajb<T> zzajb) {
        zzalf zzalf = new zzalf();
        f4249.m13453(new zzajd(str, zzalf));
        return zzakl.m4806(zzakl.m4805(zzalf, new zzaiy(this, zzajb), zzala.f4304), Throwable.class, new zzaix(this, zzajb), zzala.f4304);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<String> m4714(String str, Map<String, String> map) {
        return m4712(0, str, map, (byte[]) null);
    }
}
