package com.google.android.gms.internal;

import android.os.Handler;
import java.util.concurrent.Executor;

final class zzj implements Executor {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Handler f10743;

    zzj(zzi zzi, Handler handler) {
        this.f10743 = handler;
    }

    public final void execute(Runnable runnable) {
        this.f10743.post(runnable);
    }
}
