package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.Pinkamena;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import java.util.List;

@zzzv
public final class zzoh extends RelativeLayout {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final float[] f5215 = {5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f};

    /* renamed from: 靐  reason: contains not printable characters */
    private AnimationDrawable f5216;

    public zzoh(Context context, zzog zzog, RelativeLayout.LayoutParams layoutParams) {
        super(context);
        zzbq.m9120(zzog);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(f5215, (RectF) null, (float[]) null));
        shapeDrawable.getPaint().setColor(zzog.m5647());
        setLayoutParams(layoutParams);
        zzbs.zzek().m4659((View) this, (Drawable) shapeDrawable);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        if (!TextUtils.isEmpty(zzog.m5649())) {
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            TextView textView = new TextView(context);
            textView.setLayoutParams(layoutParams3);
            textView.setId(1195835393);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setText(zzog.m5649());
            textView.setTextColor(zzog.m5645());
            textView.setTextSize((float) zzog.m5641());
            zzkb.m5487();
            int r1 = zzajr.m4757(context, 4);
            zzkb.m5487();
            textView.setPadding(r1, 0, zzajr.m4757(context, 4), 0);
            Pinkamena.DianePie();
            layoutParams2.addRule(1, textView.getId());
        }
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(layoutParams2);
        imageView.setId(1195835394);
        List<zzoi> r0 = zzog.m5648();
        if (r0 != null && r0.size() > 1) {
            this.f5216 = new AnimationDrawable();
            for (zzoi r02 : r0) {
                try {
                    this.f5216.addFrame((Drawable) zzn.m9307(r02.m5652()), zzog.m5642());
                } catch (Exception e) {
                    zzagf.m4793("Error while getting drawable.", e);
                }
            }
            zzbs.zzek().m4659((View) imageView, (Drawable) this.f5216);
        } else if (r0.size() == 1) {
            try {
                imageView.setImageDrawable((Drawable) zzn.m9307(r0.get(0).m5652()));
            } catch (Exception e2) {
                zzagf.m4793("Error while getting drawable.", e2);
            }
        }
        Pinkamena.DianePie();
    }

    public final void onAttachedToWindow() {
        if (this.f5216 != null) {
            this.f5216.start();
        }
        super.onAttachedToWindow();
    }
}
