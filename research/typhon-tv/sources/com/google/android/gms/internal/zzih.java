package com.google.android.gms.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzg;

final class zzih implements zzg {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzid f10712;

    zzih(zzid zzid) {
        this.f10712 = zzid;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13006(ConnectionResult connectionResult) {
        synchronized (this.f10712.f4733) {
            zzio unused = this.f10712.f4732 = null;
            if (this.f10712.f4735 != null) {
                zzik unused2 = this.f10712.f4735 = null;
            }
            this.f10712.f4733.notifyAll();
        }
    }
}
