package com.google.android.gms.internal;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzbs;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzft implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Context f4615;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final WindowManager f4616;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final PowerManager f4617;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4618 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f4619 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    private zzaji f4620;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f4621;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final HashSet<zzgo> f4622 = new HashSet<>();

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f4623;

    /* renamed from: ˋ  reason: contains not printable characters */
    private BroadcastReceiver f4624;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final HashSet<Object> f4625 = new HashSet<>();

    /* renamed from: ˏ  reason: contains not printable characters */
    private final Rect f4626 = new Rect();

    /* renamed from: ˑ  reason: contains not printable characters */
    private final KeyguardManager f4627;

    /* renamed from: י  reason: contains not printable characters */
    private final zzfw f4628;

    /* renamed from: ـ  reason: contains not printable characters */
    private float f4629;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final DisplayMetrics f4630;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzga f4631;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzhd f4632;

    /* renamed from: 靐  reason: contains not printable characters */
    private Object f4633 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private WeakReference<ViewTreeObserver> f4634;

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<zzafo> f4635;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzfr f4636;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f4637;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f4638;

    public zzft(Context context, zzjn zzjn, zzafo zzafo, zzakd zzakd, zzhd zzhd) {
        this.f4635 = new WeakReference<>(zzafo);
        this.f4632 = zzhd;
        this.f4634 = new WeakReference<>((Object) null);
        this.f4637 = true;
        this.f4623 = false;
        this.f4620 = new zzaji(200);
        this.f4636 = new zzfr(UUID.randomUUID().toString(), zzakd, zzjn.f4798, zzafo.f4109, zzafo.m4442(), zzjn.f4791);
        this.f4616 = (WindowManager) context.getSystemService("window");
        this.f4617 = (PowerManager) context.getApplicationContext().getSystemService("power");
        this.f4627 = (KeyguardManager) context.getSystemService("keyguard");
        this.f4615 = context;
        this.f4628 = new zzfw(this, new Handler());
        this.f4615.getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.f4628);
        this.f4630 = context.getResources().getDisplayMetrics();
        Display defaultDisplay = this.f4616.getDefaultDisplay();
        this.f4626.right = defaultDisplay.getWidth();
        this.f4626.bottom = defaultDisplay.getHeight();
        m5329();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final void m5313() {
        if (this.f4631 != null) {
            this.f4631.m12963(this);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final void m5314() {
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f4634.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnScrollChangedListener(this);
            viewTreeObserver.removeGlobalOnLayoutListener(this);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final JSONObject m5315() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject put = jSONObject.put("afmaVersion", (Object) this.f4636.m5299()).put("activeViewJSON", (Object) this.f4636.m5301()).put("timestamp", zzbs.zzeo().m9241()).put("adFormat", (Object) this.f4636.m5302()).put("hashCode", (Object) this.f4636.m5300()).put("isMraid", this.f4636.m5298()).put("isStopped", this.f4619).put("isPaused", this.f4618).put("isNative", this.f4636.m5297()).put("isScreenOn", m5316());
        zzbs.zzei();
        JSONObject put2 = put.put("appMuted", zzahn.m4586());
        zzbs.zzei();
        put2.put("appVolume", (double) zzahn.m4587()).put("deviceVolume", (double) this.f4629);
        return jSONObject;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean m5316() {
        return Build.VERSION.SDK_INT >= 20 ? this.f4617.isInteractive() : this.f4617.isScreenOn();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m5317(int i, DisplayMetrics displayMetrics) {
        return (int) (((float) i) / displayMetrics.density);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final JSONObject m5318(View view, Boolean bool) throws JSONException {
        if (view == null) {
            return m5315().put("isAttachedToWindow", false).put("isScreenOn", m5316()).put("isVisible", false);
        }
        boolean r1 = zzbs.zzek().m4663(view);
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        try {
            view.getLocationOnScreen(iArr);
            view.getLocationInWindow(iArr2);
        } catch (Exception e) {
            zzagf.m4793("Failure getting view location.", e);
        }
        Rect rect = new Rect();
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        Rect rect2 = new Rect();
        boolean globalVisibleRect = view.getGlobalVisibleRect(rect2, (Point) null);
        Rect rect3 = new Rect();
        boolean localVisibleRect = view.getLocalVisibleRect(rect3);
        Rect rect4 = new Rect();
        view.getHitRect(rect4);
        JSONObject r0 = m5315();
        r0.put("windowVisibility", view.getWindowVisibility()).put("isAttachedToWindow", r1).put("viewBox", (Object) new JSONObject().put("top", m5317(this.f4626.top, this.f4630)).put("bottom", m5317(this.f4626.bottom, this.f4630)).put(TtmlNode.LEFT, m5317(this.f4626.left, this.f4630)).put(TtmlNode.RIGHT, m5317(this.f4626.right, this.f4630))).put("adBox", (Object) new JSONObject().put("top", m5317(rect.top, this.f4630)).put("bottom", m5317(rect.bottom, this.f4630)).put(TtmlNode.LEFT, m5317(rect.left, this.f4630)).put(TtmlNode.RIGHT, m5317(rect.right, this.f4630))).put("globalVisibleBox", (Object) new JSONObject().put("top", m5317(rect2.top, this.f4630)).put("bottom", m5317(rect2.bottom, this.f4630)).put(TtmlNode.LEFT, m5317(rect2.left, this.f4630)).put(TtmlNode.RIGHT, m5317(rect2.right, this.f4630))).put("globalVisibleBoxVisible", globalVisibleRect).put("localVisibleBox", (Object) new JSONObject().put("top", m5317(rect3.top, this.f4630)).put("bottom", m5317(rect3.bottom, this.f4630)).put(TtmlNode.LEFT, m5317(rect3.left, this.f4630)).put(TtmlNode.RIGHT, m5317(rect3.right, this.f4630))).put("localVisibleBoxVisible", localVisibleRect).put("hitBox", (Object) new JSONObject().put("top", m5317(rect4.top, this.f4630)).put("bottom", m5317(rect4.bottom, this.f4630)).put(TtmlNode.LEFT, m5317(rect4.left, this.f4630)).put(TtmlNode.RIGHT, m5317(rect4.right, this.f4630))).put("screenDensity", (double) this.f4630.density);
        if (bool == null) {
            bool = Boolean.valueOf(zzbs.zzei().m4639(view, this.f4617, this.f4627));
        }
        r0.put("isVisible", bool.booleanValue());
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static JSONObject m5319(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        jSONArray.put((Object) jSONObject);
        jSONObject2.put("units", (Object) jSONArray);
        return jSONObject2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m5320(JSONObject jSONObject, boolean z) {
        try {
            JSONObject r3 = m5319(jSONObject);
            ArrayList arrayList = new ArrayList(this.f4622);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                ((zzgo) obj).m12969(r3, z);
            }
        } catch (Throwable th) {
            zzagf.m4793("Skipping active view message.", th);
        }
    }

    public final void onGlobalLayout() {
        m5330(2);
    }

    public final void onScrollChanged() {
        m5330(1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5321() {
        synchronized (this.f4633) {
            this.f4618 = false;
            m5330(3);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m5322() {
        synchronized (this.f4633) {
            this.f4618 = true;
            m5330(3);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5323() {
        synchronized (this.f4633) {
            if (this.f4637) {
                this.f4638 = true;
                try {
                    JSONObject r0 = m5315();
                    r0.put("doneReasonCode", (Object) "u");
                    m5320(r0, true);
                } catch (JSONException e) {
                    zzagf.m4793("JSON failure while processing active view data.", e);
                } catch (RuntimeException e2) {
                    zzagf.m4793("Failure while processing active view data.", e2);
                }
                String valueOf = String.valueOf(this.f4636.m5300());
                zzagf.m4792(valueOf.length() != 0 ? "Untracking ad unit: ".concat(valueOf) : new String("Untracking ad unit: "));
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5324(zzgo zzgo) {
        this.f4622.remove(zzgo);
        zzgo.m12968();
        if (this.f4622.isEmpty()) {
            synchronized (this.f4633) {
                m5314();
                synchronized (this.f4633) {
                    if (this.f4624 != null) {
                        try {
                            zzbs.zzfg().m4733(this.f4615, this.f4624);
                        } catch (IllegalStateException e) {
                            zzagf.m4793("Failed trying to unregister the receiver", e);
                        } catch (Exception e2) {
                            zzbs.zzem().m4505((Throwable) e2, "ActiveViewUnit.stopScreenStatusMonitoring");
                        }
                        this.f4624 = null;
                    }
                }
                this.f4615.getContentResolver().unregisterContentObserver(this.f4628);
                this.f4637 = false;
                m5313();
                ArrayList arrayList = new ArrayList(this.f4622);
                int size = arrayList.size();
                int i = 0;
                while (i < size) {
                    Object obj = arrayList.get(i);
                    i++;
                    m5324((zzgo) obj);
                }
            }
            return;
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5325(Map<String, String> map) {
        m5330(3);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5326() {
        synchronized (this.f4633) {
            this.f4619 = true;
            m5330(3);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5327(Map<String, String> map) {
        if (map.containsKey("isVisible")) {
            if (!PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("isVisible"))) {
                "true".equals(map.get("isVisible"));
            }
            Iterator<Object> it2 = this.f4625.iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5328() {
        boolean z;
        synchronized (this.f4633) {
            z = this.f4637;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5329() {
        zzbs.zzei();
        this.f4629 = zzahn.m4571(this.f4615);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00cb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00cc, code lost:
        com.google.android.gms.internal.zzagf.m4797("Active view update failed.", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5330(int r8) {
        /*
            r7 = this;
            r2 = 0
            r1 = 1
            java.lang.Object r4 = r7.f4633
            monitor-enter(r4)
            java.util.HashSet<com.google.android.gms.internal.zzgo> r0 = r7.f4622     // Catch:{ all -> 0x005d }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x005d }
        L_0x000b:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0026
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x005d }
            com.google.android.gms.internal.zzgo r0 = (com.google.android.gms.internal.zzgo) r0     // Catch:{ all -> 0x005d }
            boolean r0 = r0.m12970()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x000b
            r0 = r1
        L_0x001e:
            if (r0 == 0) goto L_0x0024
            boolean r0 = r7.f4637     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x0028
        L_0x0024:
            monitor-exit(r4)     // Catch:{ all -> 0x005d }
        L_0x0025:
            return
        L_0x0026:
            r0 = r2
            goto L_0x001e
        L_0x0028:
            com.google.android.gms.internal.zzhd r0 = r7.f4632     // Catch:{ all -> 0x005d }
            android.view.View r5 = r0.m12986()     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x0060
            com.google.android.gms.internal.zzahn r0 = com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x005d }
            android.os.PowerManager r3 = r7.f4617     // Catch:{ all -> 0x005d }
            android.app.KeyguardManager r6 = r7.f4627     // Catch:{ all -> 0x005d }
            boolean r0 = r0.m4639((android.view.View) r5, (android.os.PowerManager) r3, (android.app.KeyguardManager) r6)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0060
            r3 = r1
        L_0x003f:
            if (r5 == 0) goto L_0x0062
            if (r3 == 0) goto L_0x0062
            android.graphics.Rect r0 = new android.graphics.Rect     // Catch:{ all -> 0x005d }
            r0.<init>()     // Catch:{ all -> 0x005d }
            r6 = 0
            boolean r0 = r5.getGlobalVisibleRect(r0, r6)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0062
            r0 = r1
        L_0x0050:
            com.google.android.gms.internal.zzhd r2 = r7.f4632     // Catch:{ all -> 0x005d }
            boolean r2 = r2.m12984()     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x0064
            r7.m5323()     // Catch:{ all -> 0x005d }
            monitor-exit(r4)     // Catch:{ all -> 0x005d }
            goto L_0x0025
        L_0x005d:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x005d }
            throw r0
        L_0x0060:
            r3 = r2
            goto L_0x003f
        L_0x0062:
            r0 = r2
            goto L_0x0050
        L_0x0064:
            if (r8 != r1) goto L_0x0074
            com.google.android.gms.internal.zzaji r2 = r7.f4620     // Catch:{ all -> 0x005d }
            boolean r2 = r2.m4724()     // Catch:{ all -> 0x005d }
            if (r2 != 0) goto L_0x0074
            boolean r2 = r7.f4623     // Catch:{ all -> 0x005d }
            if (r0 != r2) goto L_0x0074
            monitor-exit(r4)     // Catch:{ all -> 0x005d }
            goto L_0x0025
        L_0x0074:
            if (r0 != 0) goto L_0x007e
            boolean r2 = r7.f4623     // Catch:{ all -> 0x005d }
            if (r2 != 0) goto L_0x007e
            if (r8 != r1) goto L_0x007e
            monitor-exit(r4)     // Catch:{ all -> 0x005d }
            goto L_0x0025
        L_0x007e:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r3)     // Catch:{ JSONException -> 0x00d3, RuntimeException -> 0x00cb }
            org.json.JSONObject r1 = r7.m5318((android.view.View) r5, (java.lang.Boolean) r1)     // Catch:{ JSONException -> 0x00d3, RuntimeException -> 0x00cb }
            r2 = 0
            r7.m5320((org.json.JSONObject) r1, (boolean) r2)     // Catch:{ JSONException -> 0x00d3, RuntimeException -> 0x00cb }
            r7.f4623 = r0     // Catch:{ JSONException -> 0x00d3, RuntimeException -> 0x00cb }
        L_0x008c:
            com.google.android.gms.internal.zzhd r0 = r7.f4632     // Catch:{ all -> 0x005d }
            com.google.android.gms.internal.zzhd r0 = r0.m12985()     // Catch:{ all -> 0x005d }
            android.view.View r1 = r0.m12986()     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x00c5
            java.lang.ref.WeakReference<android.view.ViewTreeObserver> r0 = r7.f4634     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x005d }
            android.view.ViewTreeObserver r0 = (android.view.ViewTreeObserver) r0     // Catch:{ all -> 0x005d }
            android.view.ViewTreeObserver r1 = r1.getViewTreeObserver()     // Catch:{ all -> 0x005d }
            if (r1 == r0) goto L_0x00c5
            r7.m5314()     // Catch:{ all -> 0x005d }
            boolean r2 = r7.f4621     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x00b5
            if (r0 == 0) goto L_0x00be
            boolean r0 = r0.isAlive()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x00be
        L_0x00b5:
            r0 = 1
            r7.f4621 = r0     // Catch:{ all -> 0x005d }
            r1.addOnScrollChangedListener(r7)     // Catch:{ all -> 0x005d }
            r1.addOnGlobalLayoutListener(r7)     // Catch:{ all -> 0x005d }
        L_0x00be:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x005d }
            r0.<init>(r1)     // Catch:{ all -> 0x005d }
            r7.f4634 = r0     // Catch:{ all -> 0x005d }
        L_0x00c5:
            r7.m5313()     // Catch:{ all -> 0x005d }
            monitor-exit(r4)     // Catch:{ all -> 0x005d }
            goto L_0x0025
        L_0x00cb:
            r0 = move-exception
        L_0x00cc:
            java.lang.String r1 = "Active view update failed."
            com.google.android.gms.internal.zzagf.m4797(r1, r0)     // Catch:{ all -> 0x005d }
            goto L_0x008c
        L_0x00d3:
            r0 = move-exception
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzft.m5330(int):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5331(zzga zzga) {
        synchronized (this.f4633) {
            this.f4631 = zzga;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5332(zzgo zzgo) {
        if (this.f4622.isEmpty()) {
            synchronized (this.f4633) {
                if (this.f4624 == null) {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.SCREEN_ON");
                    intentFilter.addAction("android.intent.action.SCREEN_OFF");
                    this.f4624 = new zzfu(this);
                    zzbs.zzfg().m4734(this.f4615, this.f4624, intentFilter);
                }
            }
            m5330(3);
        }
        this.f4622.add(zzgo);
        try {
            zzgo.m12969(m5319(m5318(this.f4632.m12986(), (Boolean) null)), false);
        } catch (JSONException e) {
            zzagf.m4793("Skipping measurement update for new client.", e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5333(zzgo zzgo, Map<String, String> map) {
        String valueOf = String.valueOf(this.f4636.m5300());
        zzagf.m4792(valueOf.length() != 0 ? "Received request to untrack: ".concat(valueOf) : new String("Received request to untrack: "));
        m5324(zzgo);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5334(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        String str = map.get("hashCode");
        return !TextUtils.isEmpty(str) && str.equals(this.f4636.m5300());
    }
}
