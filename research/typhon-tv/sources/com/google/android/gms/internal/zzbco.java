package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.cast.ApplicationMetadata;

public final class zzbco implements Parcelable.Creator<zzbcn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        double d = 0.0d;
        ApplicationMetadata applicationMetadata = null;
        int i = 0;
        int i2 = 0;
        boolean z = false;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    d = zzbfn.m10160(parcel, readInt);
                    break;
                case 3:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 4:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    applicationMetadata = (ApplicationMetadata) zzbfn.m10171(parcel, readInt, ApplicationMetadata.CREATOR);
                    break;
                case 6:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzbcn(d, z, i2, applicationMetadata, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbcn[i];
    }
}
