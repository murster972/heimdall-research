package com.google.android.gms.internal;

import android.os.Handler;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import java.util.concurrent.atomic.AtomicReference;

final class zzbch extends zzbcu {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Handler f8659;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicReference<zzbcf> f8660;

    public zzbch(zzbcf zzbcf) {
        this.f8660 = new AtomicReference<>(zzbcf);
        this.f8659 = new Handler(zzbcf.m9163());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m10022(zzbcf zzbcf, long j, int i) {
        zzn zzn;
        synchronized (zzbcf.f8646) {
            zzn = (zzn) zzbcf.f8646.remove(Long.valueOf(j));
        }
        if (zzn != null) {
            zzn.m8947(new Status(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m10023(zzbcf zzbcf, int i) {
        synchronized (zzbcf.f8630) {
            if (zzbcf.f8650 == null) {
                return false;
            }
            zzbcf.f8650.m8947(new Status(i));
            zzn unused = zzbcf.f8650 = null;
            return true;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m10024(int i) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            String unused = zzbcf.f8639 = null;
            String unused2 = zzbcf.f8643 = null;
            m10023(zzbcf, i);
            if (zzbcf.f8633 != null) {
                this.f8659.post(new zzbci(this, zzbcf, i));
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10025(int i) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            synchronized (zzbcf.f8629) {
                if (zzbcf.f8649 != null) {
                    zzbcf.f8649.m8947(new zzbcg(new Status(i)));
                    zzn unused = zzbcf.f8649 = null;
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m10026() {
        return this.f8660.get() == null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10027(int i) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            m10023(zzbcf, i);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10028(int i) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            m10023(zzbcf, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbcf m10029() {
        zzbcf andSet = this.f8660.getAndSet((Object) null);
        if (andSet == null) {
            return null;
        }
        andSet.m9981();
        return andSet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10030(int i) {
        zzbcf r0 = m10029();
        if (r0 != null) {
            zzbcf.f8631.m10090("ICastDeviceControllerListener.onDisconnected: %d", Integer.valueOf(i));
            if (i != 0) {
                r0.m9176(2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10031(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            ApplicationMetadata unused = zzbcf.f8651 = applicationMetadata;
            String unused2 = zzbcf.f8639 = applicationMetadata.m7789();
            String unused3 = zzbcf.f8643 = str2;
            String unused4 = zzbcf.f8638 = str;
            synchronized (zzbcf.f8629) {
                if (zzbcf.f8649 != null) {
                    zzbcf.f8649.m8947(new zzbcg(new Status(0), applicationMetadata, str, str2, z));
                    zzn unused5 = zzbcf.f8649 = null;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10032(zzbbt zzbbt) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            zzbcf.f8631.m10090("onApplicationStatusChanged", new Object[0]);
            this.f8659.post(new zzbck(this, zzbcf, zzbbt));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10033(zzbcn zzbcn) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            zzbcf.f8631.m10090("onDeviceStatusChanged", new Object[0]);
            this.f8659.post(new zzbcj(this, zzbcf, zzbcn));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10034(String str, double d, boolean z) {
        zzbcf.f8631.m10090("Deprecated callback: \"onStatusreceived\"", new Object[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10035(String str, long j) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            m10022(zzbcf, j, 0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10036(String str, long j, int i) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            m10022(zzbcf, j, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10037(String str, String str2) {
        zzbcf zzbcf = this.f8660.get();
        if (zzbcf != null) {
            zzbcf.f8631.m10090("Receive (type=text, ns=%s) %s", str, str2);
            this.f8659.post(new zzbcl(this, zzbcf, str, str2));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10038(String str, byte[] bArr) {
        if (this.f8660.get() != null) {
            zzbcf.f8631.m10090("IGNORING: Receive (type=binary, ns=%s) <%d bytes>", str, Integer.valueOf(bArr.length));
        }
    }
}
