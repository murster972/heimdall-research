package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public final class zzbie implements Parcelable.Creator<zzbid> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r15 = zzbfn.m10169(parcel);
        String str = null;
        long j = 0;
        DataHolder dataHolder = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        ArrayList<String> arrayList = null;
        int i = 0;
        ArrayList<zzbhn> arrayList2 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < r15) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    dataHolder = (DataHolder) zzbfn.m10171(parcel, readInt, DataHolder.CREATOR);
                    break;
                case 5:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 9:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 10:
                    arrayList2 = zzbfn.m10167(parcel, readInt, zzbhn.CREATOR);
                    break;
                case 11:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 12:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r15);
        return new zzbid(str, j, dataHolder, str2, str3, str4, arrayList, i, arrayList2, i2, i3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbid[i];
    }
}
