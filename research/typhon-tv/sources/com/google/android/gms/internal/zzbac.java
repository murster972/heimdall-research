package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.google.android.gms.cast.AdBreakInfo;
import java.util.List;

public final class zzbac extends View {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f8551 = 1;

    /* renamed from: 麤  reason: contains not printable characters */
    private Paint f8552;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<AdBreakInfo> f8553;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f8554;

    public zzbac(Context context) {
        super(context);
        Context context2 = getContext();
        this.f8554 = context2 == null ? (int) Math.round(3.0d) : (int) Math.round(((double) context2.getResources().getDisplayMetrics().density) * 3.0d);
    }

    /* access modifiers changed from: protected */
    public final synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f8553 != null && !this.f8553.isEmpty()) {
            int round = Math.round(((float) getMeasuredHeight()) / 2.0f);
            int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            for (AdBreakInfo next : this.f8553) {
                if (next != null) {
                    long r4 = next.m7778();
                    if (r4 >= 0 && r4 <= ((long) this.f8551)) {
                        canvas.drawCircle((float) (((int) ((((double) r4) * ((double) measuredWidth)) / ((double) this.f8551))) + getPaddingLeft()), (float) round, (float) this.f8554, this.f8552);
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m9874(int i) {
        this.f8551 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m9875(List<AdBreakInfo> list, int i) {
        this.f8553 = list;
        this.f8552 = new Paint(1);
        this.f8552.setColor(-1);
        this.f8552.setStyle(Paint.Style.FILL);
        invalidate();
    }
}
