package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.zzcz;
import com.google.android.gms.common.api.internal.zzg;

public final class zzbhx extends GoogleApi<Object> {
    public zzbhx(Context context) {
        super(context, zzbhg.f8761, null, (zzcz) new zzg());
    }
}
