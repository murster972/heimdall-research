package com.google.android.gms.internal;

import android.content.SharedPreferences;
import org.json.JSONObject;

@zzzv
public abstract class zzmx<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f4881;

    /* renamed from: 齉  reason: contains not printable characters */
    private final T f4882;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f4883;

    private zzmx(int i, String str, T t) {
        this.f4883 = i;
        this.f4881 = str;
        this.f4882 = t;
        zzkb.m5483().m5593(this);
    }

    /* synthetic */ zzmx(int i, String str, Object obj, zzmy zzmy) {
        this(i, str, obj);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzmx<String> m5575(int i, String str) {
        zzmx<String> r0 = m5581(i, str, (String) null);
        zzkb.m5483().m5590(r0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzmx<String> m5576(int i, String str) {
        zzmx<String> r0 = m5581(i, str, (String) null);
        zzkb.m5483().m5589(r0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzmx<Float> m5577(int i, String str, float f) {
        return new zznb(i, str, Float.valueOf(0.0f));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzmx<Integer> m5578(int i, String str, int i2) {
        return new zzmz(i, str, Integer.valueOf(i2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzmx<Long> m5579(int i, String str, long j) {
        return new zzna(i, str, Long.valueOf(j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzmx<Boolean> m5580(int i, String str, Boolean bool) {
        return new zzmy(i, str, bool);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzmx<String> m5581(int i, String str, String str2) {
        return new zznc(i, str, str2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final T m5582() {
        return this.f4882;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m5583() {
        return this.f4883;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m5584(SharedPreferences sharedPreferences);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m5585(JSONObject jSONObject);

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5586() {
        return this.f4881;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m5587(SharedPreferences.Editor editor, T t);
}
