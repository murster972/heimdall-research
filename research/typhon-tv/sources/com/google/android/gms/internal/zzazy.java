package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.R;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.ReconnectionService;
import com.google.android.gms.cast.framework.media.MediaNotificationService;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.util.zzq;

public final class zzazy implements RemoteMediaClient.Listener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzazn f8515;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public RemoteMediaClient f8516;

    /* renamed from: ʽ  reason: contains not printable characters */
    private CastDevice f8517;

    /* renamed from: ˑ  reason: contains not printable characters */
    private MediaSessionCompat f8518;

    /* renamed from: ٴ  reason: contains not printable characters */
    private MediaSessionCompat.Callback f8519;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f8520;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzazn f8521;

    /* renamed from: 靐  reason: contains not printable characters */
    private final CastOptions f8522;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ComponentName f8523;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzazm f8524;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f8525;

    public zzazy(Context context, CastOptions castOptions, zzazm zzazm) {
        this.f8525 = context;
        this.f8522 = castOptions;
        this.f8524 = zzazm;
        if (this.f8522.m7986() == null || TextUtils.isEmpty(this.f8522.m7986().m8137())) {
            this.f8523 = null;
        } else {
            this.f8523 = new ComponentName(this.f8525, this.f8522.m7986().m8137());
        }
        this.f8521 = new zzazn(this.f8525);
        this.f8521.m9836((zzazo) new zzazz(this));
        this.f8515 = new zzazn(this.f8525);
        this.f8515.m9836((zzazo) new zzbaa(this));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final MediaMetadataCompat.Builder m9847() {
        MediaMetadataCompat metadata = this.f8518.getController().getMetadata();
        return metadata == null ? new MediaMetadataCompat.Builder() : new MediaMetadataCompat.Builder(metadata);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final void m9848() {
        if (this.f8522.m7986().m8135() != null) {
            Intent intent = new Intent(this.f8525, MediaNotificationService.class);
            intent.setPackage(this.f8525.getPackageName());
            intent.setAction("com.google.android.gms.cast.framework.action.UPDATE_NOTIFICATION");
            this.f8525.stopService(intent);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final void m9849() {
        if (this.f8522.m7987()) {
            Intent intent = new Intent(this.f8525, ReconnectionService.class);
            intent.setPackage(this.f8525.getPackageName());
            this.f8525.stopService(intent);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Uri m9850(MediaMetadata mediaMetadata, int i) {
        WebImage r0 = this.f8522.m7986().m8136() != null ? this.f8522.m7986().m8136().m4120(mediaMetadata, i) : mediaMetadata.m7875() ? mediaMetadata.m7878().get(0) : null;
        if (r0 == null) {
            return null;
        }
        return r0.m9037();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9852(int i, MediaInfo mediaInfo) {
        PendingIntent activity;
        if (i == 0) {
            this.f8518.setPlaybackState(new PlaybackStateCompat.Builder().setState(0, 0, 1.0f).build());
            this.f8518.setMetadata(new MediaMetadataCompat.Builder().build());
            return;
        }
        this.f8518.setPlaybackState(new PlaybackStateCompat.Builder().setState(i, 0, 1.0f).setActions(mediaInfo.m7844() == 2 ? 5 : 512).build());
        MediaSessionCompat mediaSessionCompat = this.f8518;
        if (this.f8523 == null) {
            activity = null;
        } else {
            Intent intent = new Intent();
            intent.setComponent(this.f8523);
            activity = PendingIntent.getActivity(this.f8525, 0, intent, 134217728);
        }
        mediaSessionCompat.setSessionActivity(activity);
        MediaMetadata r0 = mediaInfo.m7846();
        this.f8518.setMetadata(m9847().putString(MediaMetadataCompat.METADATA_KEY_TITLE, r0.m7876("com.google.android.gms.cast.metadata.TITLE")).putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, r0.m7876("com.google.android.gms.cast.metadata.TITLE")).putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, r0.m7876("com.google.android.gms.cast.metadata.SUBTITLE")).putLong(MediaMetadataCompat.METADATA_KEY_DURATION, mediaInfo.m7843()).build());
        Uri r1 = m9850(r0, 0);
        if (r1 != null) {
            this.f8521.m9837(r1);
        } else {
            m9853((Bitmap) null, 0);
        }
        Uri r02 = m9850(r0, 3);
        if (r02 != null) {
            this.f8515.m9837(r02);
        } else {
            m9853((Bitmap) null, 3);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9853(Bitmap bitmap, int i) {
        if (i == 0) {
            if (bitmap != null) {
                this.f8518.setMetadata(m9847().putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap).build());
                return;
            }
            Bitmap createBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            createBitmap.eraseColor(0);
            this.f8518.setMetadata(m9847().putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, createBitmap).build());
        } else if (i == 3) {
            this.f8518.setMetadata(m9847().putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap).build());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m9855() {
        m9863(false);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m9856() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9857() {
        m9863(false);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9858() {
        m9863(false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9859() {
        m9863(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9860() {
        m9863(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9861(int i) {
        if (this.f8520) {
            this.f8520 = false;
            if (this.f8516 != null) {
                this.f8516.m4151((RemoteMediaClient.Listener) this);
            }
            if (!zzq.m9265()) {
                ((AudioManager) this.f8525.getSystemService(MimeTypes.BASE_TYPE_AUDIO)).abandonAudioFocus((AudioManager.OnAudioFocusChangeListener) null);
            }
            this.f8524.m9831((MediaSessionCompat) null);
            if (this.f8521 != null) {
                this.f8521.m9834();
            }
            if (this.f8515 != null) {
                this.f8515.m9834();
            }
            if (this.f8518 != null) {
                this.f8518.setSessionActivity((PendingIntent) null);
                this.f8518.setCallback((MediaSessionCompat.Callback) null);
                this.f8518.setMetadata(new MediaMetadataCompat.Builder().build());
                m9852(0, (MediaInfo) null);
                this.f8518.setActive(false);
                this.f8518.release();
                this.f8518 = null;
            }
            this.f8516 = null;
            this.f8517 = null;
            this.f8519 = null;
            m9848();
            if (i == 0) {
                m9849();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9862(RemoteMediaClient remoteMediaClient, CastDevice castDevice) {
        if (!this.f8520 && this.f8522 != null && this.f8522.m7986() != null && remoteMediaClient != null && castDevice != null) {
            this.f8516 = remoteMediaClient;
            this.f8516.m4165((RemoteMediaClient.Listener) this);
            this.f8517 = castDevice;
            if (!zzq.m9265()) {
                ((AudioManager) this.f8525.getSystemService(MimeTypes.BASE_TYPE_AUDIO)).requestAudioFocus((AudioManager.OnAudioFocusChangeListener) null, 3, 3);
            }
            ComponentName componentName = new ComponentName(this.f8525, this.f8522.m7986().m8138());
            Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
            intent.setComponent(componentName);
            this.f8518 = new MediaSessionCompat(this.f8525, "CastMediaSession", componentName, PendingIntent.getBroadcast(this.f8525, 0, intent, 0));
            this.f8518.setFlags(3);
            m9852(0, (MediaInfo) null);
            if (this.f8517 != null && !TextUtils.isEmpty(this.f8517.m7826())) {
                this.f8518.setMetadata(new MediaMetadataCompat.Builder().putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ARTIST, this.f8525.getResources().getString(R.string.cast_casting_to_device, new Object[]{this.f8517.m7826()})).build());
            }
            this.f8519 = new zzbab(this);
            this.f8518.setCallback(this.f8519);
            this.f8518.setActive(true);
            this.f8524.m9831(this.f8518);
            this.f8520 = true;
            m9863(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9863(boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        int i = 2;
        if (this.f8516 != null) {
            MediaStatus r7 = this.f8516.m4136();
            MediaInfo r5 = r7 == null ? null : r7.m7913();
            MediaMetadata r0 = r5 == null ? null : r5.m7846();
            if (r7 != null && r5 != null && r0 != null) {
                switch (this.f8516.m4145()) {
                    case 1:
                        int r8 = r7.m7917();
                        boolean z5 = this.f8516.m4147() && r8 == 2;
                        int r9 = r7.m7912();
                        boolean z6 = r9 != 0 && (r8 == 1 || r8 == 3);
                        if (!z5) {
                            MediaQueueItem r02 = r7.m7921(r9);
                            if (r02 == null) {
                                z2 = z6;
                                i = 0;
                                break;
                            } else {
                                r5 = r02.m7898();
                                i = 6;
                                z2 = z6;
                                break;
                            }
                        } else {
                            z2 = z6;
                            break;
                        }
                    case 2:
                        z2 = false;
                        i = 3;
                        break;
                    case 3:
                        z2 = false;
                        break;
                    case 4:
                        z2 = false;
                        i = 6;
                        break;
                    default:
                        z2 = false;
                        i = 0;
                        break;
                }
            } else {
                z2 = false;
                i = 0;
            }
            m9852(i, r5);
            if (i == 0) {
                m9848();
                m9849();
                return;
            }
            if (!(this.f8522.m7986().m8135() == null || this.f8516 == null)) {
                Intent intent = new Intent(this.f8525, MediaNotificationService.class);
                intent.putExtra("extra_media_notification_force_update", z);
                intent.setPackage(this.f8525.getPackageName());
                intent.setAction("com.google.android.gms.cast.framework.action.UPDATE_NOTIFICATION");
                intent.putExtra("extra_media_info", this.f8516.m4137());
                intent.putExtra("extra_remote_media_client_player_state", this.f8516.m4145());
                intent.putExtra("extra_cast_device", this.f8517);
                intent.putExtra("extra_media_session_token", this.f8518 == null ? null : this.f8518.getSessionToken());
                MediaStatus r4 = this.f8516.m4136();
                if (r4 != null) {
                    switch (r4.m7905()) {
                        case 1:
                        case 2:
                        case 3:
                            z3 = true;
                            z4 = true;
                            break;
                        default:
                            Integer r6 = r4.m7918(r4.m7911());
                            if (r6 == null) {
                                z3 = false;
                                z4 = false;
                                break;
                            } else {
                                z3 = r6.intValue() > 0;
                                if (r6.intValue() >= r4.m7906() - 1) {
                                    z4 = false;
                                    break;
                                } else {
                                    z4 = true;
                                    break;
                                }
                            }
                    }
                    intent.putExtra("extra_can_skip_next", z4);
                    intent.putExtra("extra_can_skip_prev", z3);
                }
                this.f8525.startService(intent);
            }
            if (!z2 && this.f8522.m7987()) {
                Intent intent2 = new Intent(this.f8525, ReconnectionService.class);
                intent2.setPackage(this.f8525.getPackageName());
                this.f8525.startService(intent2);
            }
        }
    }
}
