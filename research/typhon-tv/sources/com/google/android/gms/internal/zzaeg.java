package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzaeg extends zzaen {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile zzaeh f4039;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile zzaef f4040;

    /* renamed from: 龘  reason: contains not printable characters */
    private volatile zzaee f4041;

    public zzaeg(zzaef zzaef) {
        this.f4040 = zzaef;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m4366(IObjectWrapper iObjectWrapper) {
        if (this.f4040 != null) {
            this.f4040.m9531();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m4367(IObjectWrapper iObjectWrapper) {
        if (this.f4040 != null) {
            this.f4040.m9532();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4368(IObjectWrapper iObjectWrapper) {
        if (this.f4040 != null) {
            this.f4040.m9533();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4369(IObjectWrapper iObjectWrapper) {
        if (this.f4039 != null) {
            this.f4039.m9537(zzn.m9307(iObjectWrapper).getClass().getName());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4370(IObjectWrapper iObjectWrapper, int i) {
        if (this.f4039 != null) {
            this.f4039.m9538(zzn.m9307(iObjectWrapper).getClass().getName(), i);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4371(IObjectWrapper iObjectWrapper) {
        if (this.f4040 != null) {
            this.f4040.m9535();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4372(IObjectWrapper iObjectWrapper) {
        if (this.f4040 != null) {
            this.f4040.m9534();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4373(IObjectWrapper iObjectWrapper) {
        if (this.f4041 != null) {
            this.f4041.m9529();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4374(IObjectWrapper iObjectWrapper, int i) {
        if (this.f4041 != null) {
            this.f4041.m9530(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4375(IObjectWrapper iObjectWrapper, zzaeq zzaeq) {
        if (this.f4040 != null) {
            this.f4040.m9536(zzaeq);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4376(zzaee zzaee) {
        this.f4041 = zzaee;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4377(zzaeh zzaeh) {
        this.f4039 = zzaeh;
    }
}
