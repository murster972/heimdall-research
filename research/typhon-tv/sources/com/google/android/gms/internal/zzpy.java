package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzpy extends zzeu implements zzpx {
    zzpy(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegateCreator");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m13242(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) iObjectWrapper2);
        zzew.m12305(v_, (IInterface) iObjectWrapper3);
        v_.writeInt(11910000);
        Parcel r0 = m12300(1, v_);
        IBinder readStrongBinder = r0.readStrongBinder();
        r0.recycle();
        return readStrongBinder;
    }
}
