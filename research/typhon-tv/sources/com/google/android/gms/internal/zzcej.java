package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

final class zzcej extends zzcem {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ PendingIntent f9061;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationRequest f9062;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcej(zzceb zzceb, GoogleApiClient googleApiClient, LocationRequest locationRequest, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.f9062 = locationRequest;
        this.f9061 = pendingIntent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10346(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10412(this.f9062, this.f9061, (zzceu) new zzcen(this));
    }
}
