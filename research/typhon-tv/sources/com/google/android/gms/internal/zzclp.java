package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;

final class zzclp {

    /* renamed from: 连任  reason: contains not printable characters */
    final Object f9654;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f9655;

    /* renamed from: 麤  reason: contains not printable characters */
    final long f9656;

    /* renamed from: 齉  reason: contains not printable characters */
    final String f9657;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f9658;

    zzclp(String str, String str2, String str3, long j, Object obj) {
        zzbq.m9122(str);
        zzbq.m9122(str3);
        zzbq.m9120(obj);
        this.f9658 = str;
        this.f9655 = str2;
        this.f9657 = str3;
        this.f9656 = j;
        this.f9654 = obj;
    }
}
