package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzaay implements Parcelable.Creator<zzaax> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r3 = zzbfn.m10169(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        ArrayList<String> arrayList = null;
        int i2 = 0;
        ArrayList<String> arrayList2 = null;
        long j = 0;
        boolean z = false;
        long j2 = 0;
        ArrayList<String> arrayList3 = null;
        long j3 = 0;
        int i3 = 0;
        String str3 = null;
        long j4 = 0;
        String str4 = null;
        boolean z2 = false;
        String str5 = null;
        String str6 = null;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        boolean z7 = false;
        zzabj zzabj = null;
        String str7 = null;
        String str8 = null;
        boolean z8 = false;
        boolean z9 = false;
        zzaeq zzaeq = null;
        ArrayList<String> arrayList4 = null;
        ArrayList<String> arrayList5 = null;
        boolean z10 = false;
        zzaaz zzaaz = null;
        boolean z11 = false;
        String str9 = null;
        ArrayList<String> arrayList6 = null;
        boolean z12 = false;
        String str10 = null;
        zzaey zzaey = null;
        String str11 = null;
        boolean z13 = false;
        boolean z14 = false;
        Bundle bundle = null;
        boolean z15 = false;
        int i4 = 0;
        while (parcel.dataPosition() < r3) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 5:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 6:
                    arrayList2 = zzbfn.m10159(parcel, readInt);
                    break;
                case 7:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 8:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 10:
                    arrayList3 = zzbfn.m10159(parcel, readInt);
                    break;
                case 11:
                    j3 = zzbfn.m10163(parcel, readInt);
                    break;
                case 12:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 13:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 14:
                    j4 = zzbfn.m10163(parcel, readInt);
                    break;
                case 15:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 18:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 19:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 21:
                    str6 = zzbfn.m10162(parcel, readInt);
                    break;
                case 22:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 23:
                    z4 = zzbfn.m10168(parcel, readInt);
                    break;
                case 24:
                    z5 = zzbfn.m10168(parcel, readInt);
                    break;
                case 25:
                    z6 = zzbfn.m10168(parcel, readInt);
                    break;
                case 26:
                    z7 = zzbfn.m10168(parcel, readInt);
                    break;
                case 28:
                    zzabj = (zzabj) zzbfn.m10171(parcel, readInt, zzabj.CREATOR);
                    break;
                case 29:
                    str7 = zzbfn.m10162(parcel, readInt);
                    break;
                case 30:
                    str8 = zzbfn.m10162(parcel, readInt);
                    break;
                case 31:
                    z8 = zzbfn.m10168(parcel, readInt);
                    break;
                case 32:
                    z9 = zzbfn.m10168(parcel, readInt);
                    break;
                case 33:
                    zzaeq = (zzaeq) zzbfn.m10171(parcel, readInt, zzaeq.CREATOR);
                    break;
                case 34:
                    arrayList4 = zzbfn.m10159(parcel, readInt);
                    break;
                case 35:
                    arrayList5 = zzbfn.m10159(parcel, readInt);
                    break;
                case 36:
                    z10 = zzbfn.m10168(parcel, readInt);
                    break;
                case 37:
                    zzaaz = (zzaaz) zzbfn.m10171(parcel, readInt, zzaaz.CREATOR);
                    break;
                case 38:
                    z11 = zzbfn.m10168(parcel, readInt);
                    break;
                case 39:
                    str9 = zzbfn.m10162(parcel, readInt);
                    break;
                case 40:
                    arrayList6 = zzbfn.m10159(parcel, readInt);
                    break;
                case 42:
                    z12 = zzbfn.m10168(parcel, readInt);
                    break;
                case 43:
                    str10 = zzbfn.m10162(parcel, readInt);
                    break;
                case 44:
                    zzaey = (zzaey) zzbfn.m10171(parcel, readInt, zzaey.CREATOR);
                    break;
                case 45:
                    str11 = zzbfn.m10162(parcel, readInt);
                    break;
                case 46:
                    z13 = zzbfn.m10168(parcel, readInt);
                    break;
                case 47:
                    z14 = zzbfn.m10168(parcel, readInt);
                    break;
                case 48:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 49:
                    z15 = zzbfn.m10168(parcel, readInt);
                    break;
                case 50:
                    i4 = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r3);
        return new zzaax(i, str, str2, arrayList, i2, arrayList2, j, z, j2, arrayList3, j3, i3, str3, j4, str4, z2, str5, str6, z3, z4, z5, z6, z7, zzabj, str7, str8, z8, z9, zzaeq, arrayList4, arrayList5, z10, zzaaz, z11, str9, arrayList6, z12, str10, zzaey, str11, z13, z14, bundle, z15, i4);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaax[i];
    }
}
