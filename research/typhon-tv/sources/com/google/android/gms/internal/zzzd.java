package com.google.android.gms.internal;

@zzzv
public final class zzzd implements zzys<zzol> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f5612;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f5613;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f5614;

    public zzzd(boolean z, boolean z2, boolean z3) {
        this.f5614 = z;
        this.f5612 = z2;
        this.f5613 = z3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e2  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ com.google.android.gms.internal.zzou m6079(com.google.android.gms.internal.zzym r14, org.json.JSONObject r15) throws org.json.JSONException, java.lang.InterruptedException, java.util.concurrent.ExecutionException {
        /*
            r13 = this;
            r3 = 0
            r11 = 0
            java.lang.String r2 = "images"
            boolean r4 = r13.f5614
            boolean r5 = r13.f5612
            r0 = r14
            r1 = r15
            java.util.List r0 = r0.m6058(r1, r2, r3, r4, r5)
            java.lang.String r1 = "secondary_image"
            boolean r2 = r13.f5614
            com.google.android.gms.internal.zzakv r4 = r14.m6057(r15, r1, r3, r2)
            com.google.android.gms.internal.zzakv r7 = r14.m6055((org.json.JSONObject) r15)
            java.lang.String r1 = "video"
            com.google.android.gms.internal.zzakv r1 = r14.m6056((org.json.JSONObject) r15, (java.lang.String) r1)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r3 = r0.iterator()
        L_0x002c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0042
            java.lang.Object r0 = r3.next()
            com.google.android.gms.internal.zzakv r0 = (com.google.android.gms.internal.zzakv) r0
            java.lang.Object r0 = r0.get()
            com.google.android.gms.internal.zzoi r0 = (com.google.android.gms.internal.zzoi) r0
            r2.add(r0)
            goto L_0x002c
        L_0x0042:
            com.google.android.gms.internal.zzanh r10 = com.google.android.gms.internal.zzym.m6050((com.google.android.gms.internal.zzakv<com.google.android.gms.internal.zzanh>) r1)
            com.google.android.gms.internal.zzol r0 = new com.google.android.gms.internal.zzol
            java.lang.String r1 = "headline"
            java.lang.String r3 = r15.getString(r1)
            boolean r1 = r13.f5613
            if (r1 == 0) goto L_0x00d7
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r1 = com.google.android.gms.internal.zznh.f5047
            com.google.android.gms.internal.zznf r5 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r1 = r5.m5595(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x00d7
            com.google.android.gms.internal.zzaft r1 = com.google.android.gms.ads.internal.zzbs.zzem()
            android.content.res.Resources r1 = r1.m4476()
            if (r1 == 0) goto L_0x00d3
            int r5 = com.google.android.gms.R.string.s7
            java.lang.String r1 = r1.getString(r5)
        L_0x0075:
            if (r3 == 0) goto L_0x00a2
            java.lang.String r5 = java.lang.String.valueOf(r1)
            int r5 = r5.length()
            int r5 = r5 + 3
            java.lang.String r6 = java.lang.String.valueOf(r3)
            int r6 = r6.length()
            int r5 = r5 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            java.lang.StringBuilder r1 = r6.append(r1)
            java.lang.String r5 = " : "
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
        L_0x00a2:
            java.lang.String r3 = "body"
            java.lang.String r3 = r15.getString(r3)
            java.lang.Object r4 = r4.get()
            com.google.android.gms.internal.zzpq r4 = (com.google.android.gms.internal.zzpq) r4
            java.lang.String r5 = "call_to_action"
            java.lang.String r5 = r15.getString(r5)
            java.lang.String r6 = "advertiser"
            java.lang.String r6 = r15.getString(r6)
            java.lang.Object r7 = r7.get()
            com.google.android.gms.internal.zzog r7 = (com.google.android.gms.internal.zzog) r7
            android.os.Bundle r8 = new android.os.Bundle
            r8.<init>()
            if (r10 == 0) goto L_0x00d9
            com.google.android.gms.internal.zzaoa r9 = r10.m4997()
        L_0x00ce:
            if (r10 == 0) goto L_0x00e2
            if (r10 != 0) goto L_0x00db
            throw r11
        L_0x00d3:
            java.lang.String r1 = "Test Ad"
            goto L_0x0075
        L_0x00d7:
            r1 = r3
            goto L_0x00a2
        L_0x00d9:
            r9 = r11
            goto L_0x00ce
        L_0x00db:
            android.view.View r10 = (android.view.View) r10
        L_0x00dd:
            r12 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return r0
        L_0x00e2:
            r10 = r11
            goto L_0x00dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzzd.m6079(com.google.android.gms.internal.zzym, org.json.JSONObject):com.google.android.gms.internal.zzou");
    }
}
