package com.google.android.gms.internal;

import android.content.Context;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.ads.internal.zzbs;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzzv
public final class zzrx implements zzm {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f5348;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final Object f5349 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f5350;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public zzrq f5351;

    public zzrx(Context context) {
        this.f5350 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Future<ParcelFileDescriptor> m5813(zzrr zzrr) {
        zzry zzry = new zzry(this);
        zzrz zzrz = new zzrz(this, zzry, zzrr);
        zzsc zzsc = new zzsc(this, zzry);
        synchronized (this.f5349) {
            this.f5351 = new zzrq(this.f5350, zzbs.zzew().m4718(), zzrz, zzsc);
            this.f5351.m9167();
        }
        return zzry;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5814() {
        synchronized (this.f5349) {
            if (this.f5351 != null) {
                this.f5351.m9160();
                this.f5351 = null;
                Binder.flushPendingCommands();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzp m5817(zzr<?> zzr) throws zzad {
        zzp zzp;
        zzrr r1 = zzrr.m5809(zzr);
        long intValue = (long) ((Integer) zzkb.m5481().m5595(zznh.f5024)).intValue();
        long r10 = zzbs.zzeo().m9241();
        try {
            zzrt zzrt = (zzrt) new zzabj(m5813(r1).get(intValue, TimeUnit.MILLISECONDS)).m4264(zzrt.CREATOR);
            if (zzrt.f5347) {
                throw new zzad(zzrt.f5344);
            }
            if (zzrt.f5343.length != zzrt.f5340.length) {
                zzp = null;
            } else {
                HashMap hashMap = new HashMap();
                for (int i = 0; i < zzrt.f5343.length; i++) {
                    hashMap.put(zzrt.f5343[i], zzrt.f5340[i]);
                }
                zzp = new zzp(zzrt.f5346, zzrt.f5345, (Map<String, String>) hashMap, zzrt.f5341, zzrt.f5342);
            }
            zzagf.m4527(new StringBuilder(52).append("Http assets remote cache took ").append(zzbs.zzeo().m9241() - r10).append("ms").toString());
            return zzp;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            zzagf.m4527(new StringBuilder(52).append("Http assets remote cache took ").append(zzbs.zzeo().m9241() - r10).append("ms").toString());
            return null;
        } catch (Throwable th) {
            zzagf.m4527(new StringBuilder(52).append("Http assets remote cache took ").append(zzbs.zzeo().m9241() - r10).append("ms").toString());
            throw th;
        }
    }
}
