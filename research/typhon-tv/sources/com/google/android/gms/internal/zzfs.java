package com.google.android.gms.internal;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.internal.js.zzn;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

@zzzv
public final class zzfs implements zzga {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzn f4609;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzakd f4610;

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakHashMap<zzafo, zzft> f4611 = new WeakHashMap<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f4612;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ArrayList<zzft> f4613 = new ArrayList<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4614 = new Object();

    public zzfs(Context context, zzakd zzakd) {
        this.f4612 = context.getApplicationContext();
        this.f4610 = zzakd;
        this.f4609 = new zzn(context.getApplicationContext(), zzakd, (String) zzkb.m5481().m5595(zznh.f5159));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean m5303(zzafo zzafo) {
        boolean z;
        synchronized (this.f4614) {
            zzft zzft = this.f4611.get(zzafo);
            z = zzft != null && zzft.m5328();
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5304(zzafo zzafo) {
        synchronized (this.f4614) {
            zzft zzft = this.f4611.get(zzafo);
            if (zzft != null) {
                zzft.m5326();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5305(zzafo zzafo) {
        synchronized (this.f4614) {
            zzft zzft = this.f4611.get(zzafo);
            if (zzft != null) {
                zzft.m5321();
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5306(zzafo zzafo) {
        synchronized (this.f4614) {
            zzft zzft = this.f4611.get(zzafo);
            if (zzft != null) {
                zzft.m5322();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5307(zzafo zzafo) {
        synchronized (this.f4614) {
            zzft zzft = this.f4611.get(zzafo);
            if (zzft != null) {
                zzft.m5323();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5308(zzft zzft) {
        synchronized (this.f4614) {
            if (!zzft.m5328()) {
                this.f4613.remove(zzft);
                Iterator<Map.Entry<zzafo, zzft>> it2 = this.f4611.entrySet().iterator();
                while (it2.hasNext()) {
                    if (it2.next().getValue() == zzft) {
                        it2.remove();
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5309(zzjn zzjn, zzafo zzafo) {
        zzanh zzanh = zzafo.f4119;
        if (zzanh == null) {
            throw null;
        }
        m5310(zzjn, zzafo, (View) zzanh);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5310(zzjn zzjn, zzafo zzafo, View view) {
        m5312(zzjn, zzafo, (zzhd) new zzfz(view, zzafo), (zzanh) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5311(zzjn zzjn, zzafo zzafo, View view, zzanh zzanh) {
        m5312(zzjn, zzafo, (zzhd) new zzfz(view, zzafo), zzanh);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5312(zzjn zzjn, zzafo zzafo, zzhd zzhd, zzanh zzanh) {
        zzft zzft;
        synchronized (this.f4614) {
            if (m5303(zzafo)) {
                zzft = this.f4611.get(zzafo);
            } else {
                zzft = new zzft(this.f4612, zzjn, zzafo, this.f4610, zzhd);
                zzft.m5331((zzga) this);
                this.f4611.put(zzafo, zzft);
                this.f4613.add(zzft);
            }
            if (zzanh != null) {
                zzft.m5332((zzgo) new zzgb(zzft, zzanh));
            } else {
                zzft.m5332((zzgo) new zzgf(zzft, this.f4609, this.f4612));
            }
        }
    }
}
