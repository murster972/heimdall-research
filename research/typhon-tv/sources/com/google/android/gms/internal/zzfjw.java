package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfjw extends zzfjm<zzfjw> {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f10564 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public zzfkd f10565 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public zzfkf f10566 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private String f10567 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Boolean f10568 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String[] f10569 = zzfjv.f10552;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Integer f10570 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f10571 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Boolean f10572 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public zzfke[] f10573 = zzfke.m12912();

    /* renamed from: 靐  reason: contains not printable characters */
    public String f10574 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public zzfjx f10575 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f10576 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f10577 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Boolean f10578 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private byte[] f10579 = null;

    public zzfjw() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzfjw m12886(com.google.android.gms.internal.zzfjj r8) throws java.io.IOException {
        /*
            r7 = this;
            r1 = 0
        L_0x0001:
            int r0 = r8.m12800()
            switch(r0) {
                case 0: goto L_0x000e;
                case 10: goto L_0x000f;
                case 18: goto L_0x0016;
                case 26: goto L_0x001d;
                case 34: goto L_0x0024;
                case 40: goto L_0x0063;
                case 50: goto L_0x006e;
                case 58: goto L_0x00a2;
                case 64: goto L_0x00aa;
                case 72: goto L_0x00b6;
                case 80: goto L_0x00c2;
                case 88: goto L_0x00fa;
                case 98: goto L_0x0132;
                case 106: goto L_0x0144;
                case 114: goto L_0x014c;
                case 122: goto L_0x015e;
                case 138: goto L_0x0166;
                default: goto L_0x0008;
            }
        L_0x0008:
            boolean r0 = super.m12843(r8, r0)
            if (r0 != 0) goto L_0x0001
        L_0x000e:
            return r7
        L_0x000f:
            java.lang.String r0 = r8.m12791()
            r7.f10574 = r0
            goto L_0x0001
        L_0x0016:
            java.lang.String r0 = r8.m12791()
            r7.f10576 = r0
            goto L_0x0001
        L_0x001d:
            java.lang.String r0 = r8.m12791()
            r7.f10571 = r0
            goto L_0x0001
        L_0x0024:
            r0 = 34
            int r2 = com.google.android.gms.internal.zzfjv.m12883(r8, r0)
            com.google.android.gms.internal.zzfke[] r0 = r7.f10573
            if (r0 != 0) goto L_0x0050
            r0 = r1
        L_0x002f:
            int r2 = r2 + r0
            com.google.android.gms.internal.zzfke[] r2 = new com.google.android.gms.internal.zzfke[r2]
            if (r0 == 0) goto L_0x0039
            com.google.android.gms.internal.zzfke[] r3 = r7.f10573
            java.lang.System.arraycopy(r3, r1, r2, r1, r0)
        L_0x0039:
            int r3 = r2.length
            int r3 = r3 + -1
            if (r0 >= r3) goto L_0x0054
            com.google.android.gms.internal.zzfke r3 = new com.google.android.gms.internal.zzfke
            r3.<init>()
            r2[r0] = r3
            r3 = r2[r0]
            r8.m12802((com.google.android.gms.internal.zzfjs) r3)
            r8.m12800()
            int r0 = r0 + 1
            goto L_0x0039
        L_0x0050:
            com.google.android.gms.internal.zzfke[] r0 = r7.f10573
            int r0 = r0.length
            goto L_0x002f
        L_0x0054:
            com.google.android.gms.internal.zzfke r3 = new com.google.android.gms.internal.zzfke
            r3.<init>()
            r2[r0] = r3
            r0 = r2[r0]
            r8.m12802((com.google.android.gms.internal.zzfjs) r0)
            r7.f10573 = r2
            goto L_0x0001
        L_0x0063:
            boolean r0 = r8.m12797()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.f10572 = r0
            goto L_0x0001
        L_0x006e:
            r0 = 50
            int r2 = com.google.android.gms.internal.zzfjv.m12883(r8, r0)
            java.lang.String[] r0 = r7.f10569
            if (r0 != 0) goto L_0x0094
            r0 = r1
        L_0x0079:
            int r2 = r2 + r0
            java.lang.String[] r2 = new java.lang.String[r2]
            if (r0 == 0) goto L_0x0083
            java.lang.String[] r3 = r7.f10569
            java.lang.System.arraycopy(r3, r1, r2, r1, r0)
        L_0x0083:
            int r3 = r2.length
            int r3 = r3 + -1
            if (r0 >= r3) goto L_0x0098
            java.lang.String r3 = r8.m12791()
            r2[r0] = r3
            r8.m12800()
            int r0 = r0 + 1
            goto L_0x0083
        L_0x0094:
            java.lang.String[] r0 = r7.f10569
            int r0 = r0.length
            goto L_0x0079
        L_0x0098:
            java.lang.String r3 = r8.m12791()
            r2[r0] = r3
            r7.f10569 = r2
            goto L_0x0001
        L_0x00a2:
            java.lang.String r0 = r8.m12791()
            r7.f10567 = r0
            goto L_0x0001
        L_0x00aa:
            boolean r0 = r8.m12797()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.f10568 = r0
            goto L_0x0001
        L_0x00b6:
            boolean r0 = r8.m12797()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.f10578 = r0
            goto L_0x0001
        L_0x00c2:
            int r2 = r8.m12787()
            int r3 = r8.m12798()     // Catch:{ IllegalArgumentException -> 0x00e9 }
            switch(r3) {
                case 0: goto L_0x00f2;
                case 1: goto L_0x00f2;
                case 2: goto L_0x00f2;
                case 3: goto L_0x00f2;
                case 4: goto L_0x00f2;
                case 5: goto L_0x00f2;
                case 6: goto L_0x00f2;
                case 7: goto L_0x00f2;
                case 8: goto L_0x00f2;
                case 9: goto L_0x00f2;
                default: goto L_0x00cd;
            }     // Catch:{ IllegalArgumentException -> 0x00e9 }
        L_0x00cd:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00e9 }
            r5 = 42
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00e9 }
            r6.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x00e9 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ IllegalArgumentException -> 0x00e9 }
            java.lang.String r5 = " is not a valid enum ReportType"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IllegalArgumentException -> 0x00e9 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x00e9 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x00e9 }
            throw r4     // Catch:{ IllegalArgumentException -> 0x00e9 }
        L_0x00e9:
            r3 = move-exception
            r8.m12792(r2)
            r7.m12843(r8, r0)
            goto L_0x0001
        L_0x00f2:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x00e9 }
            r7.f10577 = r3     // Catch:{ IllegalArgumentException -> 0x00e9 }
            goto L_0x0001
        L_0x00fa:
            int r2 = r8.m12787()
            int r3 = r8.m12798()     // Catch:{ IllegalArgumentException -> 0x0121 }
            switch(r3) {
                case 0: goto L_0x012a;
                case 1: goto L_0x012a;
                case 2: goto L_0x012a;
                case 3: goto L_0x012a;
                case 4: goto L_0x012a;
                default: goto L_0x0105;
            }     // Catch:{ IllegalArgumentException -> 0x0121 }
        L_0x0105:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x0121 }
            r5 = 39
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0121 }
            r6.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x0121 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ IllegalArgumentException -> 0x0121 }
            java.lang.String r5 = " is not a valid enum Verdict"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IllegalArgumentException -> 0x0121 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x0121 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x0121 }
            throw r4     // Catch:{ IllegalArgumentException -> 0x0121 }
        L_0x0121:
            r3 = move-exception
            r8.m12792(r2)
            r7.m12843(r8, r0)
            goto L_0x0001
        L_0x012a:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x0121 }
            r7.f10570 = r3     // Catch:{ IllegalArgumentException -> 0x0121 }
            goto L_0x0001
        L_0x0132:
            com.google.android.gms.internal.zzfjx r0 = r7.f10575
            if (r0 != 0) goto L_0x013d
            com.google.android.gms.internal.zzfjx r0 = new com.google.android.gms.internal.zzfjx
            r0.<init>()
            r7.f10575 = r0
        L_0x013d:
            com.google.android.gms.internal.zzfjx r0 = r7.f10575
            r8.m12802((com.google.android.gms.internal.zzfjs) r0)
            goto L_0x0001
        L_0x0144:
            java.lang.String r0 = r8.m12791()
            r7.f10564 = r0
            goto L_0x0001
        L_0x014c:
            com.google.android.gms.internal.zzfkd r0 = r7.f10565
            if (r0 != 0) goto L_0x0157
            com.google.android.gms.internal.zzfkd r0 = new com.google.android.gms.internal.zzfkd
            r0.<init>()
            r7.f10565 = r0
        L_0x0157:
            com.google.android.gms.internal.zzfkd r0 = r7.f10565
            r8.m12802((com.google.android.gms.internal.zzfjs) r0)
            goto L_0x0001
        L_0x015e:
            byte[] r0 = r8.m12784()
            r7.f10579 = r0
            goto L_0x0001
        L_0x0166:
            com.google.android.gms.internal.zzfkf r0 = r7.f10566
            if (r0 != 0) goto L_0x0171
            com.google.android.gms.internal.zzfkf r0 = new com.google.android.gms.internal.zzfkf
            r0.<init>()
            r7.f10566 = r0
        L_0x0171:
            com.google.android.gms.internal.zzfkf r0 = r7.f10566
            r8.m12802((com.google.android.gms.internal.zzfjs) r0)
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfjw.m12886(com.google.android.gms.internal.zzfjj):com.google.android.gms.internal.zzfjw");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12885() {
        int r0 = super.m12841();
        if (this.f10574 != null) {
            r0 += zzfjk.m12808(1, this.f10574);
        }
        if (this.f10576 != null) {
            r0 += zzfjk.m12808(2, this.f10576);
        }
        if (this.f10571 != null) {
            r0 += zzfjk.m12808(3, this.f10571);
        }
        if (this.f10573 != null && this.f10573.length > 0) {
            int i = r0;
            for (zzfke zzfke : this.f10573) {
                if (zzfke != null) {
                    i += zzfjk.m12807(4, (zzfjs) zzfke);
                }
            }
            r0 = i;
        }
        if (this.f10572 != null) {
            this.f10572.booleanValue();
            r0 += zzfjk.m12805(5) + 1;
        }
        if (this.f10569 != null && this.f10569.length > 0) {
            int i2 = 0;
            int i3 = 0;
            for (String str : this.f10569) {
                if (str != null) {
                    i3++;
                    i2 += zzfjk.m12820(str);
                }
            }
            r0 = r0 + i2 + (i3 * 1);
        }
        if (this.f10567 != null) {
            r0 += zzfjk.m12808(7, this.f10567);
        }
        if (this.f10568 != null) {
            this.f10568.booleanValue();
            r0 += zzfjk.m12805(8) + 1;
        }
        if (this.f10578 != null) {
            this.f10578.booleanValue();
            r0 += zzfjk.m12805(9) + 1;
        }
        if (this.f10577 != null) {
            r0 += zzfjk.m12806(10, this.f10577.intValue());
        }
        if (this.f10570 != null) {
            r0 += zzfjk.m12806(11, this.f10570.intValue());
        }
        if (this.f10575 != null) {
            r0 += zzfjk.m12807(12, (zzfjs) this.f10575);
        }
        if (this.f10564 != null) {
            r0 += zzfjk.m12808(13, this.f10564);
        }
        if (this.f10565 != null) {
            r0 += zzfjk.m12807(14, (zzfjs) this.f10565);
        }
        if (this.f10579 != null) {
            r0 += zzfjk.m12809(15, this.f10579);
        }
        return this.f10566 != null ? r0 + zzfjk.m12807(17, (zzfjs) this.f10566) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12887(zzfjk zzfjk) throws IOException {
        if (this.f10574 != null) {
            zzfjk.m12835(1, this.f10574);
        }
        if (this.f10576 != null) {
            zzfjk.m12835(2, this.f10576);
        }
        if (this.f10571 != null) {
            zzfjk.m12835(3, this.f10571);
        }
        if (this.f10573 != null && this.f10573.length > 0) {
            for (zzfke zzfke : this.f10573) {
                if (zzfke != null) {
                    zzfjk.m12834(4, (zzfjs) zzfke);
                }
            }
        }
        if (this.f10572 != null) {
            zzfjk.m12836(5, this.f10572.booleanValue());
        }
        if (this.f10569 != null && this.f10569.length > 0) {
            for (String str : this.f10569) {
                if (str != null) {
                    zzfjk.m12835(6, str);
                }
            }
        }
        if (this.f10567 != null) {
            zzfjk.m12835(7, this.f10567);
        }
        if (this.f10568 != null) {
            zzfjk.m12836(8, this.f10568.booleanValue());
        }
        if (this.f10578 != null) {
            zzfjk.m12836(9, this.f10578.booleanValue());
        }
        if (this.f10577 != null) {
            zzfjk.m12832(10, this.f10577.intValue());
        }
        if (this.f10570 != null) {
            zzfjk.m12832(11, this.f10570.intValue());
        }
        if (this.f10575 != null) {
            zzfjk.m12834(12, (zzfjs) this.f10575);
        }
        if (this.f10564 != null) {
            zzfjk.m12835(13, this.f10564);
        }
        if (this.f10565 != null) {
            zzfjk.m12834(14, (zzfjs) this.f10565);
        }
        if (this.f10579 != null) {
            zzfjk.m12837(15, this.f10579);
        }
        if (this.f10566 != null) {
            zzfjk.m12834(17, (zzfjs) this.f10566);
        }
        super.m12842(zzfjk);
    }
}
