package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.measurement.AppMeasurement$zzb;

final class zzckl implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzckg f9570;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AppMeasurement$zzb f9571;

    zzckl(zzckg zzckg, AppMeasurement$zzb appMeasurement$zzb) {
        this.f9570 = zzckg;
        this.f9571 = appMeasurement$zzb;
    }

    public final void run() {
        zzche r1 = this.f9570.f9558;
        if (r1 == null) {
            this.f9570.m11096().m10832().m10849("Failed to send current screen to service");
            return;
        }
        try {
            if (this.f9571 == null) {
                r1.m10682(0, (String) null, (String) null, this.f9570.m11097().getPackageName());
            } else {
                r1.m10682(this.f9571.f11089, this.f9571.f11088, this.f9571.f11090, this.f9570.m11097().getPackageName());
            }
            this.f9570.m11223();
        } catch (RemoteException e) {
            this.f9570.m11096().m10832().m10850("Failed to send current screen to the service", e);
        }
    }
}
