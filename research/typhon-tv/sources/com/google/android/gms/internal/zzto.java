package com.google.android.gms.internal;

@zzzv
public final class zzto extends Exception {
    public zzto() {
    }

    public zzto(String str) {
        super(str);
    }
}
