package com.google.android.gms.internal;

import com.google.android.gms.ads.reward.RewardedVideoAdListener;

@zzzv
public final class zzadu extends zzadq {

    /* renamed from: 龘  reason: contains not printable characters */
    private final RewardedVideoAdListener f4027;

    public zzadu(RewardedVideoAdListener rewardedVideoAdListener) {
        this.f4027 = rewardedVideoAdListener;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4358() {
        if (this.f4027 != null) {
            this.f4027.onRewardedVideoAdLeftApplication();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4359() {
        if (this.f4027 != null) {
            this.f4027.onRewardedVideoAdOpened();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4360() {
        if (this.f4027 != null) {
            this.f4027.onRewardedVideoAdClosed();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4361() {
        if (this.f4027 != null) {
            this.f4027.onRewardedVideoStarted();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4362() {
        if (this.f4027 != null) {
            this.f4027.onRewardedVideoAdLoaded();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4363(int i) {
        if (this.f4027 != null) {
            this.f4027.onRewardedVideoAdFailedToLoad(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4364(zzadh zzadh) {
        if (this.f4027 != null) {
            this.f4027.onRewarded(new zzads(zzadh));
        }
    }
}
