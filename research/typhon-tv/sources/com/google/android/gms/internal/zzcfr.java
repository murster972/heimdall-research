package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class zzcfr implements Parcelable.Creator<zzcfq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        int i = 1;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder3 = null;
        zzcfo zzcfo = null;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    zzcfo = (zzcfo) zzbfn.m10171(parcel, readInt, zzcfo.CREATOR);
                    break;
                case 3:
                    iBinder3 = zzbfn.m10156(parcel, readInt);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) zzbfn.m10171(parcel, readInt, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = zzbfn.m10156(parcel, readInt);
                    break;
                case 6:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new zzcfq(i, zzcfo, iBinder3, pendingIntent, iBinder2, iBinder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcfq[i];
    }
}
