package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.TextureView;

@zzzv
@TargetApi(14)
public abstract class zzamb extends TextureView implements zzamt {

    /* renamed from: 靐  reason: contains not printable characters */
    protected final zzams f4338;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzami f4339 = new zzami();

    public zzamb(Context context) {
        super(context);
        this.f4338 = new zzams(context, this);
    }

    public abstract int getCurrentPosition();

    public abstract int getDuration();

    public abstract int getVideoHeight();

    public abstract int getVideoWidth();

    public abstract void setVideoPath(String str);

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract void m4852();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m4853();

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m4854();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m4855();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m4856();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m4857(float f, float f2);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m4858(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m4859(zzama zzama);
}
