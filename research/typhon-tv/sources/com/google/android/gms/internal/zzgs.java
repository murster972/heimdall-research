package com.google.android.gms.internal;

import android.graphics.Rect;

public final class zzgs {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Rect f10655;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Rect f10656;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Rect f10657;

    /* renamed from: ʾ  reason: contains not printable characters */
    private float f10658;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Rect f10659;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f10660;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Rect f10661;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f10662;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10663;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f10664;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f10665;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f10666;

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean f10667;

    public zzgs(long j, boolean z, boolean z2, int i, Rect rect, Rect rect2, Rect rect3, boolean z3, Rect rect4, boolean z4, Rect rect5, float f, boolean z5) {
        this.f10664 = j;
        this.f10666 = z;
        this.f10665 = z2;
        this.f10663 = i;
        this.f10655 = rect;
        this.f10656 = rect2;
        this.f10657 = rect3;
        this.f10660 = z3;
        this.f10661 = rect4;
        this.f10662 = z4;
        this.f10659 = rect5;
        this.f10658 = f;
        this.f10667 = z5;
    }
}
