package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.AdRequest$ErrorCode;

final class zzwc implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzvx f10937;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AdRequest$ErrorCode f10938;

    zzwc(zzvx zzvx, AdRequest$ErrorCode adRequest$ErrorCode) {
        this.f10937 = zzvx;
        this.f10938 = adRequest$ErrorCode;
    }

    public final void run() {
        try {
            this.f10937.f5502.m13514(zzwj.m5985(this.f10938));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdFailedToLoad.", e);
        }
    }
}
