package com.google.android.gms.internal;

import android.util.JsonWriter;
import java.util.Map;

final /* synthetic */ class zzajw implements zzaka {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8260;

    /* renamed from: 麤  reason: contains not printable characters */
    private final byte[] f8261;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Map f8262;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8263;

    zzajw(String str, String str2, Map map, byte[] bArr) {
        this.f8263 = str;
        this.f8260 = str2;
        this.f8262 = map;
        this.f8261 = bArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9659(JsonWriter jsonWriter) {
        zzajv.m4781(this.f8263, this.f8260, this.f8262, this.f8261, jsonWriter);
    }
}
