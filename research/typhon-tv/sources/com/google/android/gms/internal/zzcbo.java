package com.google.android.gms.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;

public final class zzcbo extends zzd<zzcbt> {
    public zzcbo(Context context, Looper looper, zzf zzf, zzg zzg) {
        super(context, looper, 116, zzf, zzg, (String) null);
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.gass.START";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzcbt m10320() throws DeadObjectException {
        return (zzcbt) super.m9171();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10321() {
        return "com.google.android.gms.gass.internal.IGassService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m10322(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gass.internal.IGassService");
        return queryLocalInterface instanceof zzcbt ? (zzcbt) queryLocalInterface : new zzcbu(iBinder);
    }
}
