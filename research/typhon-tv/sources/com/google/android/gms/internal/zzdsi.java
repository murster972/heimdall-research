package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsk;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsi extends zzffu<zzdsi, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdsi f10039;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsi> f10040;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f10041 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdsk f10042;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10043;

    public static final class zza extends zzffu.zza<zzdsi, zza> implements zzfhg {
        private zza() {
            super(zzdsi.f10039);
        }

        /* synthetic */ zza(zzdsj zzdsj) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11938(int i) {
            m12541();
            ((zzdsi) this.f10398).m11927(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11939(zzdsk zzdsk) {
            m12541();
            ((zzdsi) this.f10398).m11931(zzdsk);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11940(zzfes zzfes) {
            m12541();
            ((zzdsi) this.f10398).m11925(zzfes);
            return this;
        }
    }

    static {
        zzdsi zzdsi = new zzdsi();
        f10039 = zzdsi;
        zzdsi.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsi.f10394.m12718();
    }

    private zzdsi() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zza m11924() {
        zzdsi zzdsi = f10039;
        zzffu.zza zza2 = (zzffu.zza) zzdsi.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdsi);
        return (zza) zza2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11925(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10041 = zzfes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdsi m11926(zzfes zzfes) throws zzfge {
        return (zzdsi) zzffu.m12526(f10039, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11927(int i) {
        this.f10043 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11931(zzdsk zzdsk) {
        if (zzdsk == null) {
            throw new NullPointerException();
        }
        this.f10042 = zzdsk;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdsk m11932() {
        return this.f10042 == null ? zzdsk.m11942() : this.f10042;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11933() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10043 != 0) {
            i2 = zzffg.m5245(1, this.f10043) + 0;
        }
        if (this.f10042 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10042 == null ? zzdsk.m11942() : this.f10042);
        }
        if (!this.f10041.isEmpty()) {
            i2 += zzffg.m5261(3, this.f10041);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m11934() {
        return this.f10041;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11935() {
        return this.f10043;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11936(int i, Object obj, Object obj2) {
        zzdsk.zza zza2;
        boolean z = true;
        switch (zzdsj.f10044[i - 1]) {
            case 1:
                return new zzdsi();
            case 2:
                return f10039;
            case 3:
                return null;
            case 4:
                return new zza((zzdsj) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdsi zzdsi = (zzdsi) obj2;
                this.f10043 = zzh.m12569(this.f10043 != 0, this.f10043, zzdsi.f10043 != 0, zzdsi.f10043);
                this.f10042 = (zzdsk) zzh.m12572(this.f10042, zzdsi.f10042);
                boolean z2 = this.f10041 != zzfes.zzpfg;
                zzfes zzfes = this.f10041;
                if (zzdsi.f10041 == zzfes.zzpfg) {
                    z = false;
                }
                this.f10041 = zzh.m12570(z2, zzfes, z, zzdsi.f10041);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f10043 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f10042 != null) {
                                        zzdsk zzdsk = this.f10042;
                                        zzffu.zza zza3 = (zzffu.zza) zzdsk.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdsk);
                                        zza2 = (zzdsk.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10042 = (zzdsk) zzffb.m12416(zzdsk.m11942(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10042);
                                        this.f10042 = (zzdsk) zza2.m12543();
                                        break;
                                    }
                                case 26:
                                    this.f10041 = zzffb.m12401();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10040 == null) {
                    synchronized (zzdsi.class) {
                        if (f10040 == null) {
                            f10040 = new zzffu.zzb(f10039);
                        }
                    }
                }
                return f10040;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10039;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11937(zzffg zzffg) throws IOException {
        if (this.f10043 != 0) {
            zzffg.m5281(1, this.f10043);
        }
        if (this.f10042 != null) {
            zzffg.m5289(2, (zzfhe) this.f10042 == null ? zzdsk.m11942() : this.f10042);
        }
        if (!this.f10041.isEmpty()) {
            zzffg.m5288(3, this.f10041);
        }
        this.f10394.m12719(zzffg);
    }
}
