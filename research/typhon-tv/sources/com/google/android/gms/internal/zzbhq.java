package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzd;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Pattern;

public final class zzbhq implements zzbhi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Pattern f8775 = Pattern.compile("^(1|true|t|yes|y|on)$", 2);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Pattern f8776 = Pattern.compile("^(0|false|f|no|n|off|)$", 2);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Charset f8777 = Charset.forName("UTF-8");

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static Status m10256(int i) {
        String str;
        switch (i) {
            case -6508:
                str = "SUCCESS_CACHE_STALE";
                break;
            case -6506:
                str = "SUCCESS_CACHE";
                break;
            case -6505:
                str = "SUCCESS_FRESH";
                break;
            case 6500:
                str = "NOT_AUTHORIZED_TO_FETCH";
                break;
            case 6501:
                str = "ANOTHER_FETCH_INFLIGHT";
                break;
            case 6502:
                str = "FETCH_THROTTLED";
                break;
            case 6503:
                str = "NOT_AVAILABLE";
                break;
            case 6504:
                str = "FAILURE_CACHE";
                break;
            case 6507:
                str = "FETCH_THROTTLED_STALE";
                break;
            default:
                str = CommonStatusCodes.m8526(i);
                break;
        }
        return new Status(i, str);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static HashMap<String, TreeMap<String, byte[]>> m10258(zzbif zzbif) {
        DataHolder r1;
        if (zzbif == null || (r1 = zzbif.m10281()) == null) {
            return null;
        }
        zzbil zzbil = (zzbil) new zzd(r1, zzbil.CREATOR).m9014(0);
        zzbif.m10280();
        HashMap<String, TreeMap<String, byte[]>> hashMap = new HashMap<>();
        for (String str : zzbil.m10291().keySet()) {
            TreeMap treeMap = new TreeMap();
            hashMap.put(str, treeMap);
            Bundle bundle = zzbil.m10291().getBundle(str);
            for (String str2 : bundle.keySet()) {
                treeMap.put(str2, bundle.getByteArray(str2));
            }
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static List<byte[]> m10260(zzbif zzbif) {
        DataHolder r2;
        if (zzbif == null || (r2 = zzbif.m10282()) == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (zzbhz r0 : new zzd(r2, zzbhz.CREATOR)) {
            arrayList.add(r0.m10278());
        }
        zzbif.m10279();
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final PendingResult<zzbhm> m10261(GoogleApiClient googleApiClient, zzbhk zzbhk) {
        if (googleApiClient == null || zzbhk == null) {
            return null;
        }
        return googleApiClient.zzd(new zzbhr(this, googleApiClient, zzbhk));
    }
}
