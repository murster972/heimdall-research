package com.google.android.gms.internal;

public final class zzduo {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzdve f10204;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzdve f10205;

    public zzduo(byte[] bArr, byte[] bArr2) {
        this.f10205 = zzdve.m12227(bArr);
        this.f10204 = zzdve.m12227(bArr2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final byte[] m12208() {
        if (this.f10204 == null) {
            return null;
        }
        return this.f10204.m12228();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12209() {
        if (this.f10205 == null) {
            return null;
        }
        return this.f10205.m12228();
    }
}
