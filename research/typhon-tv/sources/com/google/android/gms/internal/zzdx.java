package com.google.android.gms.internal;

import android.provider.Settings;
import java.lang.reflect.InvocationTargetException;

public final class zzdx extends zzet {
    public zzdx(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 49);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12269() throws IllegalAccessException, InvocationTargetException {
        this.f10284.f8456 = 2;
        try {
            this.f10284.f8456 = Integer.valueOf(((Boolean) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()})).booleanValue() ? 1 : 0);
        } catch (InvocationTargetException e) {
            if (!(e.getTargetException() instanceof Settings.SettingNotFoundException)) {
                throw e;
            }
        }
    }
}
