package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

@zzzv
public final class zzadv extends zzbfm {
    public static final Parcelable.Creator<zzadv> CREATOR = new zzadw();

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f4028;

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzjj f4029;

    public zzadv(zzjj zzjj, String str) {
        this.f4029 = zzjj;
        this.f4028 = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f4029, i, false);
        zzbfp.m10193(parcel, 3, this.f4028, false);
        zzbfp.m10182(parcel, r0);
    }
}
