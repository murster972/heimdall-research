package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzceg extends zzcem {
    zzceg(zzceb zzceb, GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10343(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10410((zzceu) new zzcen(this));
    }
}
