package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Provider;
import javax.crypto.Cipher;

public final class zzduw implements zzduv<Cipher> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m12219(String str, Provider provider) throws GeneralSecurityException {
        return provider == null ? Cipher.getInstance(str) : Cipher.getInstance(str, provider);
    }
}
