package com.google.android.gms.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class zzffm {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile boolean f10379 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Class<?> f10380 = m12499();

    /* renamed from: 龘  reason: contains not printable characters */
    static final zzffm f10381 = new zzffm(true);

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<Object, Object> f10382;

    zzffm() {
        this.f10382 = new HashMap();
    }

    private zzffm(boolean z) {
        this.f10382 = Collections.emptyMap();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Class<?> m12499() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzffm m12500() {
        return zzffl.m12497();
    }
}
