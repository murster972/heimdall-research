package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import java.util.Map;

public interface zzbih extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m10285(Status status) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10286(Status status, zzbif zzbif) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10287(Status status, Map map) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10288(Status status, byte[] bArr) throws RemoteException;
}
