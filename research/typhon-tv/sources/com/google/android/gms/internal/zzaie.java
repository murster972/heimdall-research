package com.google.android.gms.internal;

@zzzv
public final class zzaie {

    /* renamed from: 靐  reason: contains not printable characters */
    private float f4224 = 1.0f;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f4225 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private final synchronized boolean m4669() {
        return this.f4224 >= 0.0f;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized boolean m4670() {
        return this.f4225;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized float m4671() {
        return m4669() ? this.f4224 : 1.0f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m4672(float f) {
        this.f4224 = f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m4673(boolean z) {
        this.f4225 = z;
    }
}
