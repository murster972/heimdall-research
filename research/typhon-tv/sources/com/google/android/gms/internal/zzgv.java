package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

final class zzgv implements zzhc {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Bundle f10671;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f10672;

    zzgv(zzgu zzgu, Activity activity, Bundle bundle) {
        this.f10672 = activity;
        this.f10671 = bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12973(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityCreated(this.f10672, this.f10671);
    }
}
