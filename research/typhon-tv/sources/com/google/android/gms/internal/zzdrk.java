package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdrk extends zzffu<zzdrk, zza> implements zzfhg {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzfhk<zzdrk> f9982;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final zzdrk f9983;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9984;

    public static final class zza extends zzffu.zza<zzdrk, zza> implements zzfhg {
        private zza() {
            super(zzdrk.f9983);
        }

        /* synthetic */ zza(zzdrl zzdrl) {
            this();
        }
    }

    static {
        zzdrk zzdrk = new zzdrk();
        f9983 = zzdrk;
        zzdrk.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdrk.f10394.m12718();
    }

    private zzdrk() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzdrk m11821() {
        return f9983;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11823() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9984 != 0) {
            i2 = zzffg.m5245(1, this.f9984) + 0;
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11824() {
        return this.f9984;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 161 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m11825(int r6, java.lang.Object r7, java.lang.Object r8) {
        /*
            r5 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdrl.f9985
            int r4 = r6 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x003f;
                case 7: goto L_0x0085;
                case 8: goto L_0x0088;
                case 9: goto L_0x00a4;
                case 10: goto L_0x00aa;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdrk r5 = new com.google.android.gms.internal.zzdrk
            r5.<init>()
        L_0x0017:
            return r5
        L_0x0018:
            com.google.android.gms.internal.zzdrk r5 = f9983
            goto L_0x0017
        L_0x001b:
            r5 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdrk$zza r5 = new com.google.android.gms.internal.zzdrk$zza
            r5.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r7 = (com.google.android.gms.internal.zzffu.zzh) r7
            com.google.android.gms.internal.zzdrk r8 = (com.google.android.gms.internal.zzdrk) r8
            int r0 = r5.f9984
            if (r0 == 0) goto L_0x003b
            r0 = r1
        L_0x002c:
            int r3 = r5.f9984
            int r4 = r8.f9984
            if (r4 == 0) goto L_0x003d
        L_0x0032:
            int r2 = r8.f9984
            int r0 = r7.m12569((boolean) r0, (int) r3, (boolean) r1, (int) r2)
            r5.f9984 = r0
            goto L_0x0017
        L_0x003b:
            r0 = r2
            goto L_0x002c
        L_0x003d:
            r1 = r2
            goto L_0x0032
        L_0x003f:
            com.google.android.gms.internal.zzffb r7 = (com.google.android.gms.internal.zzffb) r7
            com.google.android.gms.internal.zzffm r8 = (com.google.android.gms.internal.zzffm) r8
            if (r8 != 0) goto L_0x004c
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x004b:
            r2 = r1
        L_0x004c:
            if (r2 != 0) goto L_0x0085
            int r0 = r7.m12415()     // Catch:{ zzfge -> 0x0064, IOException -> 0x0071 }
            switch(r0) {
                case 0: goto L_0x004b;
                case 8: goto L_0x005d;
                default: goto L_0x0055;
            }     // Catch:{ zzfge -> 0x0064, IOException -> 0x0071 }
        L_0x0055:
            boolean r0 = r5.m12537((int) r0, (com.google.android.gms.internal.zzffb) r7)     // Catch:{ zzfge -> 0x0064, IOException -> 0x0071 }
            if (r0 != 0) goto L_0x004c
            r2 = r1
            goto L_0x004c
        L_0x005d:
            int r0 = r7.m12402()     // Catch:{ zzfge -> 0x0064, IOException -> 0x0071 }
            r5.f9984 = r0     // Catch:{ zzfge -> 0x0064, IOException -> 0x0071 }
            goto L_0x004c
        L_0x0064:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x006f }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r5)     // Catch:{ all -> 0x006f }
            r1.<init>(r0)     // Catch:{ all -> 0x006f }
            throw r1     // Catch:{ all -> 0x006f }
        L_0x006f:
            r0 = move-exception
            throw r0
        L_0x0071:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x006f }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x006f }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x006f }
            r2.<init>(r0)     // Catch:{ all -> 0x006f }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r5)     // Catch:{ all -> 0x006f }
            r1.<init>(r0)     // Catch:{ all -> 0x006f }
            throw r1     // Catch:{ all -> 0x006f }
        L_0x0085:
            com.google.android.gms.internal.zzdrk r5 = f9983
            goto L_0x0017
        L_0x0088:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdrk> r0 = f9982
            if (r0 != 0) goto L_0x009d
            java.lang.Class<com.google.android.gms.internal.zzdrk> r1 = com.google.android.gms.internal.zzdrk.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdrk> r0 = f9982     // Catch:{ all -> 0x00a1 }
            if (r0 != 0) goto L_0x009c
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00a1 }
            com.google.android.gms.internal.zzdrk r2 = f9983     // Catch:{ all -> 0x00a1 }
            r0.<init>(r2)     // Catch:{ all -> 0x00a1 }
            f9982 = r0     // Catch:{ all -> 0x00a1 }
        L_0x009c:
            monitor-exit(r1)     // Catch:{ all -> 0x00a1 }
        L_0x009d:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdrk> r5 = f9982
            goto L_0x0017
        L_0x00a1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00a1 }
            throw r0
        L_0x00a4:
            java.lang.Byte r5 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00aa:
            r5 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdrk.m11825(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11826(zzffg zzffg) throws IOException {
        if (this.f9984 != 0) {
            zzffg.m5281(1, this.f9984);
        }
        this.f10394.m12719(zzffg);
    }
}
