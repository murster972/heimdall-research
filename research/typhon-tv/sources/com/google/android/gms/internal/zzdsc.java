package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtd;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsc extends zzffu<zzdsc, zza> implements zzfhg {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsc> f10025;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final zzdsc f10026;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdtd f10027;

    public static final class zza extends zzffu.zza<zzdsc, zza> implements zzfhg {
        private zza() {
            super(zzdsc.f10026);
        }

        /* synthetic */ zza(zzdsd zzdsd) {
            this();
        }
    }

    static {
        zzdsc zzdsc = new zzdsc();
        f10026 = zzdsc;
        zzdsc.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsc.f10394.m12718();
    }

    private zzdsc() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzdsc m11903() {
        return f10026;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11905() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10027 != null) {
            i2 = zzffg.m5262(2, (zzfhe) this.f10027 == null ? zzdtd.m12033() : this.f10027) + 0;
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdtd m11906() {
        return this.f10027 == null ? zzdtd.m12033() : this.f10027;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11907(int i, Object obj, Object obj2) {
        zzdtd.zza zza2;
        switch (zzdsd.f10028[i - 1]) {
            case 1:
                return new zzdsc();
            case 2:
                return f10026;
            case 3:
                return null;
            case 4:
                return new zza((zzdsd) null);
            case 5:
                this.f10027 = (zzdtd) ((zzffu.zzh) obj).m12572(this.f10027, ((zzdsc) obj2).f10027);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z = true;
                                    break;
                                case 18:
                                    if (this.f10027 != null) {
                                        zzdtd zzdtd = this.f10027;
                                        zzffu.zza zza3 = (zzffu.zza) zzdtd.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdtd);
                                        zza2 = (zzdtd.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10027 = (zzdtd) zzffb.m12416(zzdtd.m12033(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10027);
                                        this.f10027 = (zzdtd) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10025 == null) {
                    synchronized (zzdsc.class) {
                        if (f10025 == null) {
                            f10025 = new zzffu.zzb(f10026);
                        }
                    }
                }
                return f10025;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10026;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11908(zzffg zzffg) throws IOException {
        if (this.f10027 != null) {
            zzffg.m5289(2, (zzfhe) this.f10027 == null ? zzdtd.m12033() : this.f10027);
        }
        this.f10394.m12719(zzffg);
    }
}
