package com.google.android.gms.internal;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class zzbhk {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f8762;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f8763;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, String> f8764;

    /* renamed from: 麤  reason: contains not printable characters */
    private final List<zzbhn> f8765;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f8766;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f8767;

    private zzbhk(zzbhl zzbhl) {
        this.f8767 = zzbhl.f8772;
        this.f8764 = zzbhl.f8769;
        this.f8766 = zzbhl.f8771;
        this.f8765 = null;
        this.f8763 = zzbhl.f8770;
        this.f8762 = zzbhl.f8768;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m10234() {
        return this.f8763;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Map<String, String> m10235() {
        return this.f8764 == null ? Collections.emptyMap() : this.f8764;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m10236() {
        return this.f8762;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m10237() {
        return this.f8766;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10238() {
        return this.f8767;
    }
}
