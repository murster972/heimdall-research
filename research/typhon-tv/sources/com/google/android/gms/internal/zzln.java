package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzln extends zzeu implements zzll {
    zzln(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IVideoController");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final float m13095() throws RemoteException {
        Parcel r0 = m12300(6, v_());
        float readFloat = r0.readFloat();
        r0.recycle();
        return readFloat;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final float m13096() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        float readFloat = r0.readFloat();
        r0.recycle();
        return readFloat;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzlo m13097() throws RemoteException {
        zzlo zzlq;
        Parcel r1 = m12300(11, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzlq = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
            zzlq = queryLocalInterface instanceof zzlo ? (zzlo) queryLocalInterface : new zzlq(readStrongBinder);
        }
        r1.recycle();
        return zzlq;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m13098() throws RemoteException {
        Parcel r0 = m12300(10, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m13099() throws RemoteException {
        Parcel r0 = m12300(12, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final float m13100() throws RemoteException {
        Parcel r0 = m12300(9, v_());
        float readFloat = r0.readFloat();
        r0.recycle();
        return readFloat;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13101() throws RemoteException {
        m12298(2, v_());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m13102() throws RemoteException {
        Parcel r0 = m12300(5, v_());
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m13103() throws RemoteException {
        Parcel r0 = m12300(4, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13104() throws RemoteException {
        m12298(1, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13105(zzlo zzlo) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzlo);
        m12298(8, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13106(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(3, v_);
    }
}
