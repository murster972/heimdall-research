package com.google.android.gms.internal;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.JsonWriter;
import com.google.android.gms.common.util.zzb;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.util.zzh;
import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;

@zzzv
public final class zzajv {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Set<String> f4286 = new HashSet(Arrays.asList(new String[0]));

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f4287 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private static zzd f4288 = zzh.m9250();

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean f4289 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object f4290 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<String> f4291;

    public zzajv() {
        this((String) null);
    }

    public zzajv(String str) {
        List<String> asList;
        if (!m4775()) {
            asList = new ArrayList<>();
        } else {
            String uuid = UUID.randomUUID().toString();
            if (str == null) {
                String[] strArr = new String[1];
                String valueOf = String.valueOf(uuid);
                strArr[0] = valueOf.length() != 0 ? "network_request_".concat(valueOf) : new String("network_request_");
                asList = Arrays.asList(strArr);
            } else {
                String[] strArr2 = new String[2];
                String valueOf2 = String.valueOf(str);
                strArr2[0] = valueOf2.length() != 0 ? "ad_request_".concat(valueOf2) : new String("ad_request_");
                String valueOf3 = String.valueOf(uuid);
                strArr2[1] = valueOf3.length() != 0 ? "network_request_".concat(valueOf3) : new String("network_request_");
                asList = Arrays.asList(strArr2);
            }
        }
        this.f4291 = asList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4770(String str) {
        m4780("onNetworkRequestError", (zzaka) new zzajz(str));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4771(String str, String str2, Map<String, ?> map, byte[] bArr) {
        m4780("onNetworkRequest", (zzaka) new zzajw(str, str2, map, bArr));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4772(Map<String, ?> map, int i) {
        m4780("onNetworkResponse", (zzaka) new zzajx(i, map));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4773() {
        boolean z;
        synchronized (f4290) {
            z = f4287;
        }
        return z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static synchronized void m4774(String str) {
        synchronized (zzajv.class) {
            zzakb.m4794("GMA Debug BEGIN");
            for (int i = 0; i < str.length(); i += 4000) {
                String valueOf = String.valueOf(str.substring(i, Math.min(i + 4000, str.length())));
                zzakb.m4794(valueOf.length() != 0 ? "GMA Debug CONTENT ".concat(valueOf) : new String("GMA Debug CONTENT "));
            }
            zzakb.m4794("GMA Debug FINISH");
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m4775() {
        boolean z;
        synchronized (f4290) {
            z = f4287 && f4289;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4776() {
        synchronized (f4290) {
            f4287 = false;
            f4289 = false;
            zzakb.m4791("Ad debug logging enablement is out of date.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4777(int i, Map map, JsonWriter jsonWriter) throws IOException {
        jsonWriter.name("params").beginObject();
        jsonWriter.name("firstline").beginObject();
        jsonWriter.name(OAuth.OAUTH_CODE).value((long) i);
        jsonWriter.endObject();
        m4778(jsonWriter, (Map<String, ?>) map);
        jsonWriter.endObject();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4778(JsonWriter jsonWriter, Map<String, ?> map) throws IOException {
        if (map != null) {
            jsonWriter.name("headers").beginArray();
            Iterator<Map.Entry<String, ?>> it2 = map.entrySet().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Map.Entry next = it2.next();
                String str = (String) next.getKey();
                if (!f4286.contains(str)) {
                    if (!(next.getValue() instanceof List)) {
                        if (!(next.getValue() instanceof String)) {
                            zzakb.m4795("Connection headers should be either Map<String, String> or Map<String, List<String>>");
                            break;
                        }
                        jsonWriter.beginObject();
                        jsonWriter.name("name").value(str);
                        jsonWriter.name("value").value((String) next.getValue());
                        jsonWriter.endObject();
                    } else {
                        for (String value : (List) next.getValue()) {
                            jsonWriter.beginObject();
                            jsonWriter.name("name").value(str);
                            jsonWriter.name("value").value(value);
                            jsonWriter.endObject();
                        }
                    }
                }
            }
            jsonWriter.endArray();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4779(String str, JsonWriter jsonWriter) throws IOException {
        jsonWriter.name("params").beginObject();
        if (str != null) {
            jsonWriter.name(OAuthError.OAUTH_ERROR_DESCRIPTION).value(str);
        }
        jsonWriter.endObject();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4780(String str, zzaka zzaka) {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);
        try {
            jsonWriter.beginObject();
            jsonWriter.name("timestamp").value(f4288.m9243());
            jsonWriter.name(NotificationCompat.CATEGORY_EVENT).value(str);
            jsonWriter.name("components").beginArray();
            for (String value : this.f4291) {
                jsonWriter.value(value);
            }
            jsonWriter.endArray();
            zzaka.m9663(jsonWriter);
            jsonWriter.endObject();
            jsonWriter.flush();
            jsonWriter.close();
        } catch (IOException e) {
            zzakb.m4793("unable to log", e);
        }
        m4774(stringWriter.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4781(String str, String str2, Map map, byte[] bArr, JsonWriter jsonWriter) throws IOException {
        jsonWriter.name("params").beginObject();
        jsonWriter.name("firstline").beginObject();
        jsonWriter.name("uri").value(str);
        jsonWriter.name("verb").value(str2);
        jsonWriter.endObject();
        m4778(jsonWriter, (Map<String, ?>) map);
        if (bArr != null) {
            jsonWriter.name(TtmlNode.TAG_BODY).value(zzb.m9239(bArr));
        }
        jsonWriter.endObject();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m4782(boolean z) {
        synchronized (f4290) {
            f4287 = true;
            f4289 = z;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4783(byte[] bArr, JsonWriter jsonWriter) throws IOException {
        jsonWriter.name("params").beginObject();
        int length = bArr.length;
        String r1 = zzb.m9239(bArr);
        if (length < 10000) {
            jsonWriter.name(TtmlNode.TAG_BODY).value(r1);
        } else {
            String r12 = zzajr.m4760(r1);
            if (r12 != null) {
                jsonWriter.name("bodydigest").value(r12);
            }
        }
        jsonWriter.name("bodylength").value((long) length);
        jsonWriter.endObject();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m4784(Context context) {
        if (Build.VERSION.SDK_INT < 17) {
            return false;
        }
        return ((Boolean) zzkb.m5481().m5595(zznh.f4938)).booleanValue() && Settings.Global.getInt(context.getContentResolver(), "development_settings_enabled", 0) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4785(String str) {
        if (m4775() && str != null) {
            m4790(str.getBytes());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4786(String str, String str2, Map<String, ?> map, byte[] bArr) {
        if (m4775()) {
            m4771(str, str2, map, bArr);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4787(HttpURLConnection httpURLConnection, int i) {
        String str = null;
        if (m4775()) {
            m4772(httpURLConnection.getHeaderFields() == null ? null : new HashMap(httpURLConnection.getHeaderFields()), i);
            if (i < 200 || i >= 300) {
                try {
                    str = httpURLConnection.getResponseMessage();
                } catch (IOException e) {
                    String valueOf = String.valueOf(e.getMessage());
                    zzakb.m4791(valueOf.length() != 0 ? "Can not get error message from error HttpURLConnection\n".concat(valueOf) : new String("Can not get error message from error HttpURLConnection\n"));
                }
                m4770(str);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4788(HttpURLConnection httpURLConnection, byte[] bArr) {
        if (m4775()) {
            m4771(new String(httpURLConnection.getURL().toString()), new String(httpURLConnection.getRequestMethod()), httpURLConnection.getRequestProperties() == null ? null : new HashMap(httpURLConnection.getRequestProperties()), bArr);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4789(Map<String, ?> map, int i) {
        if (m4775()) {
            m4772(map, i);
            if (i < 200 || i >= 300) {
                m4770((String) null);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4790(byte[] bArr) {
        m4780("onNetworkResponseBody", (zzaka) new zzajy(bArr));
    }
}
