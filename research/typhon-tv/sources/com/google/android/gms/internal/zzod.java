package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;

@zzzv
public final class zzod extends zzob {

    /* renamed from: 龘  reason: contains not printable characters */
    private final OnCustomRenderedAdLoadedListener f5198;

    public zzod(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.f5198 = onCustomRenderedAdLoadedListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5637(zznx zznx) {
        this.f5198.onCustomRenderedAdLoaded(new zznw(zznx));
    }
}
