package com.google.android.gms.internal;

import android.graphics.Bitmap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@zzzv
public final class zzajg {

    /* renamed from: 靐  reason: contains not printable characters */
    private AtomicInteger f4254 = new AtomicInteger(0);

    /* renamed from: 龘  reason: contains not printable characters */
    private Map<Integer, Bitmap> f4255 = new ConcurrentHashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4719(Integer num) {
        this.f4255.remove(num);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m4720(Bitmap bitmap) {
        if (bitmap == null) {
            zzagf.m4792("Bitmap is null. Skipping putting into the Memory Map.");
            return -1;
        }
        int andIncrement = this.f4254.getAndIncrement();
        this.f4255.put(Integer.valueOf(andIncrement), bitmap);
        return andIncrement;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bitmap m4721(Integer num) {
        return this.f4255.get(num);
    }
}
