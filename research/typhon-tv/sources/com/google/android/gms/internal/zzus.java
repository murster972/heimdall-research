package com.google.android.gms.internal;

import java.util.concurrent.Callable;

final class zzus implements Callable<zzuo> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzur f10912;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzul f10913;

    zzus(zzur zzur, zzul zzul) {
        this.f10912 = zzur;
        this.f10913 = zzul;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzuo call() throws Exception {
        synchronized (this.f10912.f5456) {
            if (this.f10912.f5457) {
                return null;
            }
            return this.f10913.龘(this.f10912.f5450, this.f10912.f5451);
        }
    }
}
