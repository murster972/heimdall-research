package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class zzdvd {
    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m12226(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, int i) throws GeneralSecurityException {
        Mac r0 = zzduu.f10218.m12217(str);
        if (i > r0.getMacLength() * 255) {
            throw new GeneralSecurityException("size too large");
        }
        if (bArr2 == null || bArr2.length == 0) {
            r0.init(new SecretKeySpec(new byte[r0.getMacLength()], str));
        } else {
            r0.init(new SecretKeySpec(bArr2, str));
        }
        byte[] bArr4 = new byte[i];
        int i2 = 1;
        r0.init(new SecretKeySpec(r0.doFinal(bArr), str));
        byte[] bArr5 = new byte[0];
        int i3 = 0;
        while (true) {
            r0.update(bArr5);
            r0.update(bArr3);
            r0.update((byte) i2);
            bArr5 = r0.doFinal();
            if (bArr5.length + i3 < i) {
                System.arraycopy(bArr5, 0, bArr4, i3, bArr5.length);
                i3 += bArr5.length;
                i2++;
            } else {
                System.arraycopy(bArr5, 0, bArr4, i3, i - i3);
                return bArr4;
            }
        }
    }
}
