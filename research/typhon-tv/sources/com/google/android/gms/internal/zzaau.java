package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import java.util.List;
import java.util.concurrent.Future;

@zzzv
public final class zzaau {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final PackageInfo f3770;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final String f3771;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String f3772;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final int f3773;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String f3774;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final boolean f3775;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final List<String> f3776;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public final String f3777;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final List<String> f3778;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public final Bundle f3779;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final String f3780;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public final boolean f3781;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final List<String> f3782;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final boolean f3783;

    /* renamed from: ˉ  reason: contains not printable characters */
    public final long f3784;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public final Bundle f3785;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int f3786;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public final List<Integer> f3787;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final int f3788;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public final Future<String> f3789;

    /* renamed from: ˎ  reason: contains not printable characters */
    public final float f3790;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public final int f3791;

    /* renamed from: ˏ  reason: contains not printable characters */
    public final String f3792;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public final String f3793;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final Bundle f3794;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public final boolean f3795;

    /* renamed from: י  reason: contains not printable characters */
    public final List<String> f3796;

    /* renamed from: יי  reason: contains not printable characters */
    public final boolean f3797;

    /* renamed from: ـ  reason: contains not printable characters */
    public final String f3798;

    /* renamed from: ــ  reason: contains not printable characters */
    public final zzlr f3799;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final zzakd f3800;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int f3801;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public final boolean f3802;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final zzpe f3803;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final Future<String> f3804;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String f3805;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final float f3806;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public final boolean f3807;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final boolean f3808;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final int f3809;

    /* renamed from: 连任  reason: contains not printable characters */
    public final ApplicationInfo f3810;

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzjj f3811;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f3812;

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzjn f3813;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle f3814;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public final int f3815;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Bundle f3816;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean f3817;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public final boolean f3818;

    public zzaau(Bundle bundle, zzjj zzjj, zzjn zzjn, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, zzakd zzakd, Bundle bundle2, List<String> list, List<String> list2, Bundle bundle3, boolean z, int i, int i2, float f, String str4, long j, String str5, List<String> list3, String str6, zzpe zzpe, String str7, float f2, boolean z2, int i3, int i4, boolean z3, boolean z4, Future<String> future, String str8, boolean z5, int i5, Bundle bundle4, String str9, zzlr zzlr, boolean z6, Bundle bundle5, boolean z7, Future<String> future2, List<Integer> list4, String str10, List<String> list5, int i6, boolean z8, boolean z9, boolean z10) {
        this.f3814 = bundle;
        this.f3811 = zzjj;
        this.f3813 = zzjn;
        this.f3812 = str;
        this.f3810 = applicationInfo;
        this.f3770 = packageInfo;
        this.f3772 = str2;
        this.f3774 = str3;
        this.f3800 = zzakd;
        this.f3794 = bundle2;
        this.f3817 = z;
        this.f3786 = i;
        this.f3788 = i2;
        this.f3790 = f;
        if (list == null || list.size() <= 0) {
            this.f3801 = 0;
            this.f3782 = null;
            this.f3776 = null;
        } else {
            this.f3801 = 3;
            this.f3782 = list;
            this.f3776 = list2;
        }
        this.f3816 = bundle3;
        this.f3780 = str4;
        this.f3784 = j;
        this.f3792 = str5;
        this.f3796 = list3;
        this.f3798 = str6;
        this.f3803 = zzpe;
        this.f3805 = str7;
        this.f3806 = f2;
        this.f3808 = z2;
        this.f3809 = i3;
        this.f3815 = i4;
        this.f3818 = z3;
        this.f3802 = z4;
        this.f3804 = future;
        this.f3771 = str8;
        this.f3775 = z5;
        this.f3773 = i5;
        this.f3779 = bundle4;
        this.f3777 = str9;
        this.f3799 = zzlr;
        this.f3781 = z6;
        this.f3785 = bundle5;
        this.f3783 = z7;
        this.f3789 = future2;
        this.f3787 = list4;
        this.f3793 = str10;
        this.f3778 = list5;
        this.f3791 = i6;
        this.f3795 = z8;
        this.f3807 = z9;
        this.f3797 = z10;
    }
}
