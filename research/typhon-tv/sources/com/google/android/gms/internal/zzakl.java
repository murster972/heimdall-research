package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@zzzv
public final class zzakl {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> zzaku<T> m4802(T t) {
        return new zzaku<>(t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <V> zzakv<V> m4803(zzakv<V> zzakv, long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        zzalf zzalf = new zzalf();
        m4811(zzalf, zzakv);
        ScheduledFuture<?> schedule = scheduledExecutorService.schedule(new zzakp(zzalf), j, timeUnit);
        m4810(zzakv, zzalf);
        zzalf.m4823(new zzakq(schedule), zzala.f4304);
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <A, B> zzakv<B> m4804(zzakv<A> zzakv, zzakg<? super A, ? extends B> zzakg, Executor executor) {
        zzalf zzalf = new zzalf();
        zzakv.m9670(new zzako(zzalf, zzakg, zzakv), executor);
        m4811(zzalf, zzakv);
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <A, B> zzakv<B> m4805(zzakv<A> zzakv, zzakh<A, B> zzakh, Executor executor) {
        zzalf zzalf = new zzalf();
        zzakv.m9670(new zzakn(zzalf, zzakh, zzakv), executor);
        m4811(zzalf, zzakv);
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <V, X extends Throwable> zzakv<V> m4806(zzakv<? extends V> zzakv, Class<X> cls, zzakg<? super X, ? extends V> zzakg, Executor executor) {
        zzalf zzalf = new zzalf();
        m4811(zzalf, zzakv);
        zzakv.m9670(new zzakr(zzalf, zzakv, cls, zzakg, executor), zzala.f4304);
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m4807(Future<T> future, T t) {
        try {
            return future.get(((Long) zzkb.m5481().m5595(zznh.f4958)).longValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            future.cancel(true);
            zzagf.m4796("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            zzbs.zzem().m4505((Throwable) e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            future.cancel(true);
            zzagf.m4793("Error waiting for future.", e2);
            zzbs.zzem().m4505((Throwable) e2, "Futures.resolveFuture");
            return t;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m4808(Future<T> future, T t, long j, TimeUnit timeUnit) {
        try {
            return future.get(j, timeUnit);
        } catch (InterruptedException e) {
            future.cancel(true);
            zzagf.m4796("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            zzbs.zzem().m4505((Throwable) e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            future.cancel(true);
            zzagf.m4793("Error waiting for future.", e2);
            zzbs.zzem().m4505((Throwable) e2, "Futures.resolveFuture");
            return t;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <V> void m4809(zzakv<V> zzakv, zzaki<V> zzaki, Executor executor) {
        zzakv.m9670(new zzakm(zzaki, zzakv), executor);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <V> void m4810(zzakv<? extends V> zzakv, zzalf<V> zzalf) {
        m4811(zzalf, zzakv);
        zzakv.m9670(new zzaks(zzalf, zzakv), zzala.f4304);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <A, B> void m4811(zzakv<A> zzakv, Future<B> future) {
        zzakv.m9670(new zzakt(zzakv, future), zzala.f4304);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4812(zzalf zzalf, zzakg zzakg, zzakv zzakv) {
        if (!zzalf.isCancelled()) {
            try {
                m4810(zzakg.m9664(zzakv.get()), zzalf);
            } catch (CancellationException e) {
                zzalf.cancel(true);
            } catch (ExecutionException e2) {
                zzalf.m4824(e2.getCause());
            } catch (InterruptedException e3) {
                Thread.currentThread().interrupt();
                zzalf.m4824(e3);
            } catch (Exception e4) {
                zzalf.m4824(e4);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4813(zzalf zzalf, zzakv zzakv, Class cls, zzakg zzakg, Executor executor) {
        try {
            zzalf.m4822(zzakv.get());
            return;
        } catch (ExecutionException e) {
            e = e.getCause();
        } catch (InterruptedException e2) {
            e = e2;
            Thread.currentThread().interrupt();
        } catch (Exception e3) {
            e = e3;
        }
        if (cls.isInstance(e)) {
            m4810(m4804(m4802(e), zzakg, executor), zzalf);
        } else {
            zzalf.m4824(e);
        }
    }
}
