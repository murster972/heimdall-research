package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcmb extends zzfjm<zzcmb> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzcmb[] f9711;

    /* renamed from: 连任  reason: contains not printable characters */
    public Integer f9712 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9713 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Long f9714 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Long f9715 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public zzcmc[] f9716 = zzcmc.m11493();

    public zzcmb() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzcmb[] m11489() {
        if (f9711 == null) {
            synchronized (zzfjq.f10546) {
                if (f9711 == null) {
                    f9711 = new zzcmb[0];
                }
            }
        }
        return f9711;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcmb)) {
            return false;
        }
        zzcmb zzcmb = (zzcmb) obj;
        if (!zzfjq.m12863((Object[]) this.f9716, (Object[]) zzcmb.f9716)) {
            return false;
        }
        if (this.f9713 == null) {
            if (zzcmb.f9713 != null) {
                return false;
            }
        } else if (!this.f9713.equals(zzcmb.f9713)) {
            return false;
        }
        if (this.f9715 == null) {
            if (zzcmb.f9715 != null) {
                return false;
            }
        } else if (!this.f9715.equals(zzcmb.f9715)) {
            return false;
        }
        if (this.f9714 == null) {
            if (zzcmb.f9714 != null) {
                return false;
            }
        } else if (!this.f9714.equals(zzcmb.f9714)) {
            return false;
        }
        if (this.f9712 == null) {
            if (zzcmb.f9712 != null) {
                return false;
            }
        } else if (!this.f9712.equals(zzcmb.f9712)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcmb.f10533 == null || zzcmb.f10533.m12849() : this.f10533.equals(zzcmb.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9712 == null ? 0 : this.f9712.hashCode()) + (((this.f9714 == null ? 0 : this.f9714.hashCode()) + (((this.f9715 == null ? 0 : this.f9715.hashCode()) + (((this.f9713 == null ? 0 : this.f9713.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + zzfjq.m12859((Object[]) this.f9716)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11490() {
        int r1 = super.m12841();
        if (this.f9716 != null && this.f9716.length > 0) {
            for (zzcmc zzcmc : this.f9716) {
                if (zzcmc != null) {
                    r1 += zzfjk.m12807(1, (zzfjs) zzcmc);
                }
            }
        }
        if (this.f9713 != null) {
            r1 += zzfjk.m12808(2, this.f9713);
        }
        if (this.f9715 != null) {
            r1 += zzfjk.m12814(3, this.f9715.longValue());
        }
        if (this.f9714 != null) {
            r1 += zzfjk.m12814(4, this.f9714.longValue());
        }
        return this.f9712 != null ? r1 + zzfjk.m12806(5, this.f9712.intValue()) : r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11491(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    int r2 = zzfjv.m12883(zzfjj, 10);
                    int length = this.f9716 == null ? 0 : this.f9716.length;
                    zzcmc[] zzcmcArr = new zzcmc[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9716, 0, zzcmcArr, 0, length);
                    }
                    while (length < zzcmcArr.length - 1) {
                        zzcmcArr[length] = new zzcmc();
                        zzfjj.m12802((zzfjs) zzcmcArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzcmcArr[length] = new zzcmc();
                    zzfjj.m12802((zzfjs) zzcmcArr[length]);
                    this.f9716 = zzcmcArr;
                    continue;
                case 18:
                    this.f9713 = zzfjj.m12791();
                    continue;
                case 24:
                    this.f9715 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 32:
                    this.f9714 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 40:
                    this.f9712 = Integer.valueOf(zzfjj.m12785());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11492(zzfjk zzfjk) throws IOException {
        if (this.f9716 != null && this.f9716.length > 0) {
            for (zzcmc zzcmc : this.f9716) {
                if (zzcmc != null) {
                    zzfjk.m12834(1, (zzfjs) zzcmc);
                }
            }
        }
        if (this.f9713 != null) {
            zzfjk.m12835(2, this.f9713);
        }
        if (this.f9715 != null) {
            zzfjk.m12824(3, this.f9715.longValue());
        }
        if (this.f9714 != null) {
            zzfjk.m12824(4, this.f9714.longValue());
        }
        if (this.f9712 != null) {
            zzfjk.m12832(5, this.f9712.intValue());
        }
        super.m12842(zzfjk);
    }
}
