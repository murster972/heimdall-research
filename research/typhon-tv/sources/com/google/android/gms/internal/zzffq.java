package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffs;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class zzffq<FieldDescriptorType extends zzffs<FieldDescriptorType>> {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final zzffq f10385 = new zzffq(true);

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f10386;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f10387 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzfhy<FieldDescriptorType, Object> f10388 = zzfhy.m12685(16);

    private zzffq() {
    }

    private zzffq(boolean z) {
        if (!this.f10386) {
            this.f10388.m12693();
            this.f10386 = true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m12508(zzffs<?> zzffs, Object obj) {
        int i = 0;
        zzfiy r1 = zzffs.m12518();
        int r2 = zzffs.m12521();
        if (!zzffs.m12519()) {
            return m12510(r1, r2, obj);
        }
        if (zzffs.m12517()) {
            for (Object r4 : (List) obj) {
                i += m12509(r1, r4);
            }
            return zzffg.m5243(i) + zzffg.m5255(r2) + i;
        }
        for (Object r42 : (List) obj) {
            i += m12510(r1, r2, r42);
        }
        return i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m12509(zzfiy zzfiy, Object obj) {
        switch (zzffr.f10389[zzfiy.ordinal()]) {
            case 1:
                return zzffg.m5266(((Double) obj).doubleValue());
            case 2:
                return zzffg.m5267(((Float) obj).floatValue());
            case 3:
                return zzffg.m5263(((Long) obj).longValue());
            case 4:
                return zzffg.m5259(((Long) obj).longValue());
            case 5:
                return zzffg.m5244(((Integer) obj).intValue());
            case 6:
                return zzffg.m5234(((Long) obj).longValue());
            case 7:
                return zzffg.m5238(((Integer) obj).intValue());
            case 8:
                return zzffg.m5270(((Boolean) obj).booleanValue());
            case 9:
                return zzffg.m5264((zzfhe) obj);
            case 10:
                return obj instanceof zzfgg ? zzffg.m5269((zzfgk) (zzfgg) obj) : zzffg.m5251((zzfhe) obj);
            case 11:
                return obj instanceof zzfes ? zzffg.m5250((zzfes) obj) : zzffg.m5252((String) obj);
            case 12:
                return obj instanceof zzfes ? zzffg.m5250((zzfes) obj) : zzffg.m5253((byte[]) obj);
            case 13:
                return zzffg.m5232(((Integer) obj).intValue());
            case 14:
                return zzffg.m5241(((Integer) obj).intValue());
            case 15:
                return zzffg.m5237(((Long) obj).longValue());
            case 16:
                return zzffg.m5235(((Integer) obj).intValue());
            case 17:
                return zzffg.m5246(((Long) obj).longValue());
            case 18:
                return obj instanceof zzfga ? zzffg.m5242(((zzfga) obj).zzhq()) : zzffg.m5242(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m12510(zzfiy zzfiy, int i, Object obj) {
        int i2;
        int r1 = zzffg.m5255(i);
        if (zzfiy == zzfiy.GROUP) {
            zzffz.m12581((zzfhe) obj);
            i2 = r1 << 1;
        } else {
            i2 = r1;
        }
        return i2 + m12509(zzfiy, obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m12511(Map.Entry<FieldDescriptorType, Object> entry) {
        zzffs zzffs = (zzffs) entry.getKey();
        Object value = entry.getValue();
        return (zzffs.m12520() != zzfjd.MESSAGE || zzffs.m12519() || zzffs.m12517()) ? m12508((zzffs<?>) zzffs, value) : value instanceof zzfgg ? zzffg.m5247(((zzffs) entry.getKey()).m12521(), (zzfgk) (zzfgg) value) : zzffg.m5258(((zzffs) entry.getKey()).m12521(), (zzfhe) value);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends zzffs<T>> zzffq<T> m12512() {
        return new zzffq<>();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.util.ArrayList} */
    /* JADX WARNING: type inference failed for: r1v0, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m12513(FieldDescriptorType r7, java.lang.Object r8) {
        /*
            r6 = this;
            boolean r0 = r7.m12519()
            if (r0 == 0) goto L_0x0035
            boolean r0 = r8 instanceof java.util.List
            if (r0 != 0) goto L_0x0013
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Wrong object type used with protocol message reflection."
            r0.<init>(r1)
            throw r0
        L_0x0013:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.List r8 = (java.util.List) r8
            r1.addAll(r8)
            r0 = r1
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            int r3 = r0.size()
            r2 = 0
        L_0x0025:
            if (r2 >= r3) goto L_0x003d
            java.lang.Object r4 = r0.get(r2)
            int r2 = r2 + 1
            com.google.android.gms.internal.zzfiy r5 = r7.m12518()
            m12514((com.google.android.gms.internal.zzfiy) r5, (java.lang.Object) r4)
            goto L_0x0025
        L_0x0035:
            com.google.android.gms.internal.zzfiy r0 = r7.m12518()
            m12514((com.google.android.gms.internal.zzfiy) r0, (java.lang.Object) r8)
            r1 = r8
        L_0x003d:
            boolean r0 = r1 instanceof com.google.android.gms.internal.zzfgg
            if (r0 == 0) goto L_0x0044
            r0 = 1
            r6.f10387 = r0
        L_0x0044:
            com.google.android.gms.internal.zzfhy<FieldDescriptorType, java.lang.Object> r0 = r6.f10388
            r0.put(r7, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffq.m12513(com.google.android.gms.internal.zzffs, java.lang.Object):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12514(zzfiy zzfiy, Object obj) {
        boolean z = false;
        zzffz.m12579(obj);
        switch (zzffr.f10390[zzfiy.zzdad().ordinal()]) {
            case 1:
                z = obj instanceof Integer;
                break;
            case 2:
                z = obj instanceof Long;
                break;
            case 3:
                z = obj instanceof Float;
                break;
            case 4:
                z = obj instanceof Double;
                break;
            case 5:
                z = obj instanceof Boolean;
                break;
            case 6:
                z = obj instanceof String;
                break;
            case 7:
                if ((obj instanceof zzfes) || (obj instanceof byte[])) {
                    z = true;
                    break;
                }
            case 8:
                if ((obj instanceof Integer) || (obj instanceof zzfga)) {
                    z = true;
                    break;
                }
            case 9:
                if ((obj instanceof zzfhe) || (obj instanceof zzfgg)) {
                    z = true;
                    break;
                }
        }
        if (!z) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzffq zzffq = new zzffq();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f10388.m12691()) {
                break;
            }
            Map.Entry<FieldDescriptorType, Object> r3 = this.f10388.m12688(i2);
            zzffq.m12513((zzffs) r3.getKey(), r3.getValue());
            i = i2 + 1;
        }
        for (Map.Entry next : this.f10388.m12690()) {
            zzffq.m12513((zzffs) next.getKey(), next.getValue());
        }
        zzffq.f10387 = this.f10387;
        return zzffq;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzffq)) {
            return false;
        }
        return this.f10388.equals(((zzffq) obj).f10388);
    }

    public final int hashCode() {
        return this.f10388.hashCode();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Iterator<Map.Entry<FieldDescriptorType, Object>> m12515() {
        return this.f10387 ? new zzfgj(this.f10388.entrySet().iterator()) : this.f10388.entrySet().iterator();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12516() {
        int i;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            i = i2;
            if (i3 >= this.f10388.m12691()) {
                break;
            }
            i2 = m12511(this.f10388.m12688(i3)) + i;
            i3++;
        }
        for (Map.Entry<FieldDescriptorType, Object> r0 : this.f10388.m12690()) {
            i += m12511(r0);
        }
        return i;
    }
}
