package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzfq extends zzeu implements zzfo {
    zzfq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12946() throws RemoteException {
        Parcel r0 = m12300(6, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m12947() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12948(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, true);
        Parcel r0 = m12300(2, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }
}
