package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzaer implements Parcelable.Creator<zzaeq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r2 = zzbfn.m10169(parcel);
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < r2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r2);
        return new zzaeq(str, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaeq[i];
    }
}
