package com.google.android.gms.internal;

import com.google.android.gms.measurement.AppMeasurement$zzb;

final class zzcjj implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzcir f9482;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f9483;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ long f9484;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f9485;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f9486;

    zzcjj(zzcir zzcir, String str, String str2, String str3, long j) {
        this.f9482 = zzcir;
        this.f9486 = str;
        this.f9483 = str2;
        this.f9485 = str3;
        this.f9484 = j;
    }

    public final void run() {
        if (this.f9486 == null) {
            this.f9482.f9431.m11027().m11219(this.f9483, (AppMeasurement$zzb) null);
            return;
        }
        AppMeasurement$zzb appMeasurement$zzb = new AppMeasurement$zzb();
        appMeasurement$zzb.f11088 = this.f9485;
        appMeasurement$zzb.f11090 = this.f9486;
        appMeasurement$zzb.f11089 = this.f9484;
        this.f9482.f9431.m11027().m11219(this.f9483, appMeasurement$zzb);
    }
}
