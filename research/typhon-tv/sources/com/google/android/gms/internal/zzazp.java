package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.RemoteException;

public final class zzazp extends AsyncTask<Uri, Long, Bitmap> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f8511 = new zzbcy("FetchBitmapTask");

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzazu f8512;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzazr f8513;

    private zzazp(Context context, int i, int i2, boolean z, long j, int i3, int i4, int i5, zzazr zzazr) {
        this.f8512 = zzayu.m9766(context.getApplicationContext(), this, new zzazt(this), i, i2, z, 2097152, 5, 333, 10000);
        this.f8513 = zzazr;
    }

    public zzazp(Context context, int i, int i2, boolean z, zzazr zzazr) {
        this(context, i, i2, false, 2097152, 5, 333, 10000, zzazr);
    }

    public zzazp(Context context, zzazr zzazr) {
        this(context, 0, 0, false, 2097152, 5, 333, 10000, zzazr);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Bitmap doInBackground(Uri... uriArr) {
        if (uriArr.length != 1 || uriArr[0] == null) {
            return null;
        }
        try {
            return this.f8512.m9843(uriArr[0]);
        } catch (RemoteException e) {
            f8511.m10091(e, "Unable to call %s on %s.", "doFetch", zzazu.class.getSimpleName());
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (this.f8513 != null) {
            this.f8513.m9841(bitmap);
        }
    }
}
