package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzbt;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzade extends zzd implements zzaef {

    /* renamed from: ٴ  reason: contains not printable characters */
    private static zzade f4020;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final zzuw f4021 = new zzuw();

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f4022;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f4023;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Map<String, zzael> f4024 = new HashMap();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private zzafe f4025;

    public zzade(Context context, zzv zzv, zzjn zzjn, zzux zzux, zzakd zzakd) {
        super(context, zzjn, (String) null, zzux, zzakd, zzv);
        f4020 = this;
        this.f4025 = new zzafe(context, (String) null);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzade m4340() {
        return f4020;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzafp m4341(zzafp zzafp) {
        zzagf.m4527("Creating mediation ad response for non-mediated rewarded ad.");
        try {
            String jSONObject = zzacg.m4290(zzafp.f4133).toString();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("pubid", (Object) zzafp.f4136.f3760);
            return new zzafp(zzafp.f4136, zzafp.f4133, new zzui(Arrays.asList(new zzuh[]{new zzuh(jSONObject, (String) null, Arrays.asList(new String[]{"com.google.ads.mediation.admob.AdMobAdapter"}), (String) null, (String) null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), jSONObject2.toString(), (String) null, Collections.emptyList(), Collections.emptyList(), (String) null, (String) null, (String) null, (List<String>) null, (String) null, Collections.emptyList(), (String) null, -1)}), ((Long) zzkb.m5481().m5595(zznh.f4960)).longValue(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false, "", -1, 0, 1, (String) null, 0, -1, -1, false), zzafp.f4134, zzafp.f4132, zzafp.f4127, zzafp.f4128, zzafp.f4129, zzafp.f4130, (Boolean) null);
        } catch (JSONException e) {
            zzagf.m4793("Unable to generate ad state for non-mediated rewarded video.", e);
            return new zzafp(zzafp.f4136, zzafp.f4133, (zzui) null, zzafp.f4134, 0, zzafp.f4127, zzafp.f4128, zzafp.f4129, zzafp.f4130, (Boolean) null);
        }
    }

    public final void destroy() {
        zzbq.m9115("destroy must be called on the main UI thread.");
        for (String next : this.f4024.keySet()) {
            try {
                zzael zzael = this.f4024.get(next);
                if (!(zzael == null || zzael.m4386() == null)) {
                    zzael.m4386().m13467();
                }
            } catch (RemoteException e) {
                String valueOf = String.valueOf(next);
                zzagf.m4791(valueOf.length() != 0 ? "Fail to destroy adapter: ".concat(valueOf) : new String("Fail to destroy adapter: "));
            }
        }
    }

    public final void pause() {
        zzbq.m9115("pause must be called on the main UI thread.");
        for (String next : this.f4024.keySet()) {
            try {
                zzael zzael = this.f4024.get(next);
                if (!(zzael == null || zzael.m4386() == null)) {
                    zzael.m4386().m13466();
                }
            } catch (RemoteException e) {
                String valueOf = String.valueOf(next);
                zzagf.m4791(valueOf.length() != 0 ? "Fail to pause adapter: ".concat(valueOf) : new String("Fail to pause adapter: "));
            }
        }
    }

    public final void resume() {
        zzbq.m9115("resume must be called on the main UI thread.");
        for (String next : this.f4024.keySet()) {
            try {
                zzael zzael = this.f4024.get(next);
                if (!(zzael == null || zzael.m4386() == null)) {
                    zzael.m4386().m13464();
                }
            } catch (RemoteException e) {
                String valueOf = String.valueOf(next);
                zzagf.m4791(valueOf.length() != 0 ? "Fail to resume adapter: ".concat(valueOf) : new String("Fail to resume adapter: "));
            }
        }
    }

    public final void setImmersiveMode(boolean z) {
        zzbq.m9115("setImmersiveMode must be called on the main UI thread.");
        this.f4023 = z;
    }

    public final void zza(zzafp zzafp, zznu zznu) {
        if (zzafp.f4132 != -2) {
            zzahn.f4212.post(new zzadg(this, zzafp));
            return;
        }
        this.连任.zzaue = zzafp;
        if (zzafp.f4135 == null) {
            this.连任.zzaue = m4341(zzafp);
        }
        this.连任.zzauz = 0;
        zzbt zzbt = this.连任;
        zzbs.zzeh();
        zzaei zzaei = new zzaei(this.连任.zzair, this.连任.zzaue, this);
        String valueOf = String.valueOf(zzaei.getClass().getName());
        zzagf.m4792(valueOf.length() != 0 ? "AdRenderer: ".concat(valueOf) : new String("AdRenderer: "));
        zzaei.m4674();
        zzbt.zzaub = zzaei;
    }

    public final boolean zza(zzafo zzafo, zzafo zzafo2) {
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m4343() {
        zzbq.m9115("showAd must be called on the main UI thread.");
        if (!m4347()) {
            zzagf.m4791("The reward video has not loaded.");
            return;
        }
        this.f4022 = true;
        zzael r0 = m4351(this.连任.zzaud.f4125);
        if (r0 != null && r0.m4386() != null) {
            try {
                r0.m4386().m13479(this.f4023);
                r0.m4386().m13455();
            } catch (RemoteException e) {
                zzagf.m4796("Could not call showVideo.", e);
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m4344() {
        onAdClicked();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m4345() {
        靐();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m4346() {
        if (zzbs.zzfd().m4426(this.连任.zzair)) {
            this.f4025.m4408(false);
        }
        龘();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m4347() {
        zzbq.m9115("isLoaded must be called on the main UI thread.");
        return this.连任.zzaua == null && this.连任.zzaub == null && this.连任.zzaud != null && !this.f4022;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m4348() {
        if (zzbs.zzfd().m4426(this.连任.zzair)) {
            this.f4025.m4408(true);
        }
        龘(this.连任.zzaud, false);
        齉();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m4349() {
        if (!(this.连任.zzaud == null || this.连任.zzaud.f4096 == null)) {
            zzbs.zzez();
            zzuq.m5896(this.连任.zzair, this.连任.zzaty.f4297, this.连任.zzaud, this.连任.zzatw, false, this.连任.zzaud.f4096.f5413);
        }
        连任();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4350(zzaeq zzaeq) {
        if (!(this.连任.zzaud == null || this.连任.zzaud.f4101 == null || TextUtils.isEmpty(this.连任.zzaud.f4101.f5431))) {
            zzaeq = new zzaeq(this.连任.zzaud.f4101.f5431, this.连任.zzaud.f4101.f5432);
        }
        if (!(this.连任.zzaud == null || this.连任.zzaud.f4096 == null)) {
            zzbs.zzez();
            zzuq.m5897(this.连任.zzair, this.连任.zzaty.f4297, this.连任.zzaud.f4096.f5407, this.连任.zzauv, zzaeq);
        }
        if (zzbs.zzfd().m4426(this.连任.zzair) && zzaeq != null) {
            zzbs.zzfd().m4435(this.连任.zzair, zzbs.zzfd().m4423(this.连任.zzair), this.连任.zzatw, zzaeq.f4056, zzaeq.f4055);
        }
        龘(zzaeq);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0040  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzael m4351(java.lang.String r6) {
        /*
            r5 = this;
            java.util.Map<java.lang.String, com.google.android.gms.internal.zzael> r0 = r5.f4024
            java.lang.Object r0 = r0.get(r6)
            com.google.android.gms.internal.zzael r0 = (com.google.android.gms.internal.zzael) r0
            if (r0 != 0) goto L_0x0027
            com.google.android.gms.internal.zzux r1 = r5.ˑ     // Catch:{ Exception -> 0x0028 }
            java.lang.String r2 = "com.google.ads.mediation.admob.AdMobAdapter"
            boolean r2 = r2.equals(r6)     // Catch:{ Exception -> 0x0028 }
            if (r2 == 0) goto L_0x0049
            com.google.android.gms.internal.zzuw r1 = f4021     // Catch:{ Exception -> 0x0028 }
            r2 = r1
        L_0x0018:
            com.google.android.gms.internal.zzael r1 = new com.google.android.gms.internal.zzael     // Catch:{ Exception -> 0x0028 }
            com.google.android.gms.internal.zzva r2 = r2.m13448(r6)     // Catch:{ Exception -> 0x0028 }
            r1.<init>(r2, r5)     // Catch:{ Exception -> 0x0028 }
            java.util.Map<java.lang.String, com.google.android.gms.internal.zzael> r0 = r5.f4024     // Catch:{ Exception -> 0x0046 }
            r0.put(r6, r1)     // Catch:{ Exception -> 0x0046 }
            r0 = r1
        L_0x0027:
            return r0
        L_0x0028:
            r2 = move-exception
            r1 = r0
        L_0x002a:
            java.lang.String r3 = "Fail to instantiate adapter "
            java.lang.String r0 = java.lang.String.valueOf(r6)
            int r4 = r0.length()
            if (r4 == 0) goto L_0x0040
            java.lang.String r0 = r3.concat(r0)
        L_0x003b:
            com.google.android.gms.internal.zzagf.m4796(r0, r2)
            r0 = r1
            goto L_0x0027
        L_0x0040:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r3)
            goto L_0x003b
        L_0x0046:
            r0 = move-exception
            r2 = r0
            goto L_0x002a
        L_0x0049:
            r2 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzade.m4351(java.lang.String):com.google.android.gms.internal.zzael");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4352() {
        this.连任.zzaud = null;
        zzade.super.龘();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4353(Context context) {
        for (zzael r0 : this.f4024.values()) {
            try {
                r0.m4386().m13469(zzn.m9306(context));
            } catch (RemoteException e) {
                zzagf.m4793("Unable to call Adapter.onContextChanged.", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4354(zzadv zzadv) {
        zzbq.m9115("loadAd must be called on the main UI thread.");
        if (TextUtils.isEmpty(zzadv.f4028)) {
            zzagf.m4791("Invalid ad unit id. Aborting.");
            zzahn.f4212.post(new zzadf(this));
            return;
        }
        this.f4022 = false;
        this.连任.zzatw = zzadv.f4028;
        this.f4025.m4407(zzadv.f4028);
        zzade.super.zzb(zzadv.f4029);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4355(zzjj zzjj, zzafo zzafo, boolean z) {
        return false;
    }
}
