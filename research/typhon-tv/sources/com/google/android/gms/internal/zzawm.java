package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzawm extends zzeu implements zzawl {
    zzawm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.internal.IAuthService");
    }
}
