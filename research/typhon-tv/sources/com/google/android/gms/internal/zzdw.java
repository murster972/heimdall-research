package com.google.android.gms.internal;

import android.app.Activity;
import android.view.View;
import java.lang.reflect.InvocationTargetException;

public final class zzdw extends zzet {

    /* renamed from: 连任  reason: contains not printable characters */
    private final View f10246;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Activity f10247;

    public zzdw(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2, View view, Activity activity) {
        super(zzdm, str, str2, zzaz, i, 62);
        this.f10246 = view;
        this.f10247 = activity;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12268() throws IllegalAccessException, InvocationTargetException {
        if (this.f10246 != null) {
            long[] jArr = (long[]) this.f10286.invoke((Object) null, new Object[]{this.f10246, this.f10247});
            synchronized (this.f10284) {
                this.f10284.f8442 = Long.valueOf(jArr[0]);
                this.f10284.f8440 = Long.valueOf(jArr[1]);
            }
        }
    }
}
