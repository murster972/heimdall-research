package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.api.internal.zzck;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.zzp;
import com.google.android.gms.location.zzs;
import java.util.HashMap;
import java.util.Map;

public final class zzcfd {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<zzck<Object>, zzcfh> f9069 = new HashMap();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Map<zzck<LocationCallback>, zzcfe> f9070 = new HashMap();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<zzck<LocationListener>, zzcfi> f9071 = new HashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f9072;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f9073 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private ContentProviderClient f9074 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcfu<zzcez> f9075;

    public zzcfd(Context context, zzcfu<zzcez> zzcfu) {
        this.f9072 = context;
        this.f9075 = zzcfu;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzcfe m10375(zzci<LocationCallback> zzci) {
        zzcfe zzcfe;
        synchronized (this.f9070) {
            zzcfe = this.f9070.get(zzci.m8852());
            if (zzcfe == null) {
                zzcfe = new zzcfe(zzci);
            }
            this.f9070.put(zzci.m8852(), zzcfe);
        }
        return zzcfe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcfi m10376(zzci<LocationListener> zzci) {
        zzcfi zzcfi;
        synchronized (this.f9071) {
            zzcfi = this.f9071.get(zzci.m8852());
            if (zzcfi == null) {
                zzcfi = new zzcfi(zzci);
            }
            this.f9071.put(zzci.m8852(), zzcfi);
        }
        return zzcfi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final LocationAvailability m10377() throws RemoteException {
        this.f9075.m10420();
        return this.f9075.m10419().m10353(this.f9072.getPackageName());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10378(zzck<LocationCallback> zzck, zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        zzbq.m9121(zzck, (Object) "Invalid null listener key");
        synchronized (this.f9070) {
            zzcfe remove = this.f9070.remove(zzck);
            if (remove != null) {
                remove.m10390();
                this.f9075.m10419().m10360(zzcfq.m10417((zzp) remove, zzceu));
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10379() throws RemoteException {
        if (this.f9073) {
            m10389(false);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10380() throws RemoteException {
        synchronized (this.f9071) {
            for (zzcfi next : this.f9071.values()) {
                if (next != null) {
                    this.f9075.m10419().m10360(zzcfq.m10418((zzs) next, (zzceu) null));
                }
            }
            this.f9071.clear();
        }
        synchronized (this.f9070) {
            for (zzcfe next2 : this.f9070.values()) {
                if (next2 != null) {
                    this.f9075.m10419().m10360(zzcfq.m10417((zzp) next2, (zzceu) null));
                }
            }
            this.f9070.clear();
        }
        synchronized (this.f9069) {
            for (zzcfh next3 : this.f9069.values()) {
                if (next3 != null) {
                    this.f9075.m10419().m10358(new zzcdz(2, (zzcdx) null, next3.asBinder(), (IBinder) null));
                }
            }
            this.f9069.clear();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Location m10381() throws RemoteException {
        this.f9075.m10420();
        return this.f9075.m10419().m10354(this.f9072.getPackageName());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10382(PendingIntent pendingIntent, zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10360(new zzcfq(2, (zzcfo) null, (IBinder) null, pendingIntent, (IBinder) null, zzceu != null ? zzceu.asBinder() : null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10383(Location location) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10357(location);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10384(zzck<LocationListener> zzck, zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        zzbq.m9121(zzck, (Object) "Invalid null listener key");
        synchronized (this.f9071) {
            zzcfi remove = this.f9071.remove(zzck);
            if (remove != null) {
                remove.m10397();
                this.f9075.m10419().m10360(zzcfq.m10418((zzs) remove, zzceu));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10385(zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10359(zzceu);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10386(zzcfo zzcfo, zzci<LocationCallback> zzci, zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10360(new zzcfq(1, zzcfo, (IBinder) null, (PendingIntent) null, m10375(zzci).asBinder(), zzceu != null ? zzceu.asBinder() : null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10387(LocationRequest locationRequest, PendingIntent pendingIntent, zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10360(new zzcfq(1, zzcfo.m5230(locationRequest), (IBinder) null, pendingIntent, (IBinder) null, zzceu != null ? zzceu.asBinder() : null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10388(LocationRequest locationRequest, zzci<LocationListener> zzci, zzceu zzceu) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10360(new zzcfq(1, zzcfo.m5230(locationRequest), m10376(zzci).asBinder(), (PendingIntent) null, (IBinder) null, zzceu != null ? zzceu.asBinder() : null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10389(boolean z) throws RemoteException {
        this.f9075.m10420();
        this.f9075.m10419().m10362(z);
        this.f9073 = z;
    }
}
