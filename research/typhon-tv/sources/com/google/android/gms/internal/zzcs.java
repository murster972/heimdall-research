package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class zzcs implements zzcr {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected long f9777 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected long f9778 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected long f9779 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected float f9780;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected float f9781;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected float f9782;

    /* renamed from: ˊ  reason: contains not printable characters */
    private double f9783;

    /* renamed from: ˋ  reason: contains not printable characters */
    private double f9784;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f9785 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected long f9786 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected double f9787;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected float f9788;

    /* renamed from: 连任  reason: contains not printable characters */
    protected long f9789 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    protected LinkedList<MotionEvent> f9790 = new LinkedList<>();

    /* renamed from: 麤  reason: contains not printable characters */
    protected long f9791 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    protected long f9792 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    protected MotionEvent f9793;

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected boolean f9794 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected DisplayMetrics f9795;

    protected zzcs(Context context) {
        try {
            if (((Boolean) zzkb.m5481().m5595(zznh.f4972)).booleanValue()) {
                zzbw.m10304();
            } else {
                zzbz.m10308();
            }
            this.f9795 = context.getResources().getDisplayMetrics();
        } catch (Throwable th) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final String m11518(Context context, String str, boolean z, View view, Activity activity, byte[] bArr) {
        zzaz r0;
        if (z) {
            try {
                r0 = m11520(context, view, activity);
                this.f9785 = true;
            } catch (UnsupportedEncodingException | GeneralSecurityException e) {
                return Integer.toString(7);
            } catch (Throwable th) {
                return Integer.toString(3);
            }
        } else {
            r0 = m11521(context, (zzaw) null);
        }
        return (r0 == null || r0.m12872() == 0) ? Integer.toString(5) : zzbw.m10301(r0, str);
    }

    public final String zza(Context context) {
        if (zzdr.m11759()) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f4975)).booleanValue()) {
                throw new IllegalStateException("The caller must not be called from the UI thread.");
            }
        }
        return m11518(context, (String) null, false, (View) null, (Activity) null, (byte[]) null);
    }

    public final String zza(Context context, String str, View view) {
        return zza(context, str, view, (Activity) null);
    }

    public final String zza(Context context, String str, View view, Activity activity) {
        return m11518(context, str, true, view, activity, (byte[]) null);
    }

    public final void zza(int i, int i2, int i3) {
        if (this.f9793 != null) {
            this.f9793.recycle();
        }
        if (this.f9795 != null) {
            this.f9793 = MotionEvent.obtain(0, (long) i3, 1, ((float) i) * this.f9795.density, ((float) i2) * this.f9795.density, 0.0f, 0.0f, 0, 0.0f, 0.0f, 0, 0);
        } else {
            this.f9793 = null;
        }
        this.f9794 = false;
    }

    public final void zza(MotionEvent motionEvent) {
        if (this.f9785) {
            this.f9777 = 0;
            this.f9789 = 0;
            this.f9791 = 0;
            this.f9792 = 0;
            this.f9778 = 0;
            this.f9786 = 0;
            this.f9779 = 0;
            Iterator it2 = this.f9790.iterator();
            while (it2.hasNext()) {
                ((MotionEvent) it2.next()).recycle();
            }
            this.f9790.clear();
            this.f9793 = null;
            this.f9785 = false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.f9787 = 0.0d;
                this.f9783 = (double) motionEvent.getRawX();
                this.f9784 = (double) motionEvent.getRawY();
                break;
            case 1:
            case 2:
                double rawX = (double) motionEvent.getRawX();
                double rawY = (double) motionEvent.getRawY();
                double d = rawX - this.f9783;
                double d2 = rawY - this.f9784;
                this.f9787 = Math.sqrt((d * d) + (d2 * d2)) + this.f9787;
                this.f9783 = rawX;
                this.f9784 = rawY;
                break;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (((Boolean) zzkb.m5481().m5595(zznh.f4968)).booleanValue()) {
                    this.f9788 = motionEvent.getX();
                    this.f9782 = motionEvent.getY();
                    this.f9780 = motionEvent.getRawX();
                    this.f9781 = motionEvent.getRawY();
                }
                this.f9792++;
                break;
            case 1:
                this.f9793 = MotionEvent.obtain(motionEvent);
                this.f9790.add(this.f9793);
                if (this.f9790.size() > 6) {
                    this.f9790.remove().recycle();
                }
                this.f9789++;
                try {
                    this.f9778 = m11519(new Throwable().getStackTrace());
                    break;
                } catch (zzdj e) {
                    break;
                }
            case 2:
                this.f9791 += (long) (motionEvent.getHistorySize() + 1);
                try {
                    zzdq r1 = m11522(motionEvent);
                    if ((r1 == null || r1.f9927 == null || r1.f9918 == null) ? false : true) {
                        this.f9779 += r1.f9927.longValue() + r1.f9918.longValue();
                    }
                    if ((this.f9795 == null || r1 == null || r1.f9925 == null || r1.f9919 == null) ? false : true) {
                        this.f9786 = r1.f9919.longValue() + r1.f9925.longValue() + this.f9786;
                        break;
                    }
                } catch (zzdj e2) {
                    break;
                }
                break;
            case 3:
                this.f9777++;
                break;
        }
        this.f9794 = true;
    }

    public void zzb(View view) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract long m11519(StackTraceElement[] stackTraceElementArr) throws zzdj;

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzaz m11520(Context context, View view, Activity activity);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzaz m11521(Context context, zzaw zzaw);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract zzdq m11522(MotionEvent motionEvent) throws zzdj;
}
