package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsg;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsk extends zzffu<zzdsk, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final zzdsk f10045;

    /* renamed from: ˑ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsk> f10046;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f10047 = zzfes.zzpfg;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzfes f10048 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdsg f10049;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10050;

    public static final class zza extends zzffu.zza<zzdsk, zza> implements zzfhg {
        private zza() {
            super(zzdsk.f10045);
        }

        /* synthetic */ zza(zzdsl zzdsl) {
            this();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final zza m11960(zzfes zzfes) {
            m12541();
            ((zzdsk) this.f10398).m11946(zzfes);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11961(int i) {
            m12541();
            ((zzdsk) this.f10398).m11948(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11962(zzdsg zzdsg) {
            m12541();
            ((zzdsk) this.f10398).m11949(zzdsg);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11963(zzfes zzfes) {
            m12541();
            ((zzdsk) this.f10398).m11945(zzfes);
            return this;
        }
    }

    static {
        zzdsk zzdsk = new zzdsk();
        f10045 = zzdsk;
        zzdsk.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsk.f10394.m12718();
    }

    private zzdsk() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zza m11941() {
        zzdsk zzdsk = f10045;
        zzffu.zza zza2 = (zzffu.zza) zzdsk.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdsk);
        return (zza) zza2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static zzdsk m11942() {
        return f10045;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11945(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10047 = zzfes;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m11946(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10048 = zzfes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdsk m11947(zzfes zzfes) throws zzfge {
        return (zzdsk) zzffu.m12526(f10045, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11948(int i) {
        this.f10050 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11949(zzdsg zzdsg) {
        if (zzdsg == null) {
            throw new NullPointerException();
        }
        this.f10049 = zzdsg;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzfes m11953() {
        return this.f10048;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdsg m11954() {
        return this.f10049 == null ? zzdsg.m11916() : this.f10049;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11955() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10050 != 0) {
            i2 = zzffg.m5245(1, this.f10050) + 0;
        }
        if (this.f10049 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10049 == null ? zzdsg.m11916() : this.f10049);
        }
        if (!this.f10047.isEmpty()) {
            i2 += zzffg.m5261(3, this.f10047);
        }
        if (!this.f10048.isEmpty()) {
            i2 += zzffg.m5261(4, this.f10048);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m11956() {
        return this.f10047;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11957() {
        return this.f10050;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11958(int i, Object obj, Object obj2) {
        zzdsg.zza zza2;
        boolean z = true;
        switch (zzdsl.f10051[i - 1]) {
            case 1:
                return new zzdsk();
            case 2:
                return f10045;
            case 3:
                return null;
            case 4:
                return new zza((zzdsl) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdsk zzdsk = (zzdsk) obj2;
                this.f10050 = zzh.m12569(this.f10050 != 0, this.f10050, zzdsk.f10050 != 0, zzdsk.f10050);
                this.f10049 = (zzdsg) zzh.m12572(this.f10049, zzdsk.f10049);
                this.f10047 = zzh.m12570(this.f10047 != zzfes.zzpfg, this.f10047, zzdsk.f10047 != zzfes.zzpfg, zzdsk.f10047);
                boolean z2 = this.f10048 != zzfes.zzpfg;
                zzfes zzfes = this.f10048;
                if (zzdsk.f10048 == zzfes.zzpfg) {
                    z = false;
                }
                this.f10048 = zzh.m12570(z2, zzfes, z, zzdsk.f10048);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f10050 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f10049 != null) {
                                        zzdsg zzdsg = this.f10049;
                                        zzffu.zza zza3 = (zzffu.zza) zzdsg.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdsg);
                                        zza2 = (zzdsg.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10049 = (zzdsg) zzffb.m12416(zzdsg.m11916(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10049);
                                        this.f10049 = (zzdsg) zza2.m12543();
                                        break;
                                    }
                                case 26:
                                    this.f10047 = zzffb.m12401();
                                    break;
                                case 34:
                                    this.f10048 = zzffb.m12401();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10046 == null) {
                    synchronized (zzdsk.class) {
                        if (f10046 == null) {
                            f10046 = new zzffu.zzb(f10045);
                        }
                    }
                }
                return f10046;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10045;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11959(zzffg zzffg) throws IOException {
        if (this.f10050 != 0) {
            zzffg.m5281(1, this.f10050);
        }
        if (this.f10049 != null) {
            zzffg.m5289(2, (zzfhe) this.f10049 == null ? zzdsg.m11916() : this.f10049);
        }
        if (!this.f10047.isEmpty()) {
            zzffg.m5288(3, this.f10047);
        }
        if (!this.f10048.isEmpty()) {
            zzffg.m5288(4, this.f10048);
        }
        this.f10394.m12719(zzffg);
    }
}
