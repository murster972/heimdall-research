package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbb extends zzfjm<zzbb> {

    /* renamed from: 连任  reason: contains not printable characters */
    private Long f8604 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f8605 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private Long f8606 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Long f8607 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f8608 = null;

    public zzbb() {
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9949() {
        int r0 = super.m12841();
        if (this.f8606 != null) {
            r0 += zzfjk.m12814(1, this.f8606.longValue());
        }
        if (this.f8604 != null) {
            r0 += zzfjk.m12814(2, this.f8604.longValue());
        }
        if (this.f8608 != null) {
            r0 += zzfjk.m12814(3, this.f8608.longValue());
        }
        if (this.f8605 != null) {
            r0 += zzfjk.m12814(4, this.f8605.longValue());
        }
        return this.f8607 != null ? r0 + zzfjk.m12814(5, this.f8607.longValue()) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m9950(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f8606 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 16:
                    this.f8604 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 24:
                    this.f8608 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 32:
                    this.f8605 = Long.valueOf(zzfjj.m12786());
                    continue;
                case 40:
                    this.f8607 = Long.valueOf(zzfjj.m12786());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9951(zzfjk zzfjk) throws IOException {
        if (this.f8606 != null) {
            zzfjk.m12824(1, this.f8606.longValue());
        }
        if (this.f8604 != null) {
            zzfjk.m12824(2, this.f8604.longValue());
        }
        if (this.f8608 != null) {
            zzfjk.m12824(3, this.f8608.longValue());
        }
        if (this.f8605 != null) {
            zzfjk.m12824(4, this.f8605.longValue());
        }
        if (this.f8607 != null) {
            zzfjk.m12824(5, this.f8607.longValue());
        }
        super.m12842(zzfjk);
    }
}
