package com.google.android.gms.internal;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import java.math.BigInteger;
import java.util.Locale;

@zzzv
public final class zzaga {

    /* renamed from: 靐  reason: contains not printable characters */
    private static String f4205;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f4206 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4518() {
        String str;
        synchronized (f4206) {
            str = f4205;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4519(Context context, String str, String str2) {
        String str3;
        synchronized (f4206) {
            if (f4205 == null && !TextUtils.isEmpty(str)) {
                try {
                    ClassLoader classLoader = context.createPackageContext(str2, 3).getClassLoader();
                    Class<?> cls = Class.forName("com.google.ads.mediation.MediationAdapter", false, classLoader);
                    BigInteger bigInteger = new BigInteger(new byte[1]);
                    String[] split = str.split(",");
                    for (int i = 0; i < split.length; i++) {
                        zzbs.zzei();
                        if (zzahn.m4621(classLoader, cls, split[i])) {
                            bigInteger = bigInteger.setBit(i);
                        }
                    }
                    f4205 = String.format(Locale.US, "%X", new Object[]{bigInteger});
                } catch (Throwable th) {
                    f4205 = NotificationCompat.CATEGORY_ERROR;
                }
            }
            str3 = f4205;
        }
        return str3;
    }
}
