package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;

public final class zzcln extends zzbfm {
    public static final Parcelable.Creator<zzcln> CREATOR = new zzclo();

    /* renamed from: ʻ  reason: contains not printable characters */
    private Float f9646;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f9647;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Double f9648;

    /* renamed from: 连任  reason: contains not printable characters */
    private Long f9649;

    /* renamed from: 靐  reason: contains not printable characters */
    public final long f9650;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9651;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f9652;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f9653;

    zzcln(int i, String str, long j, Long l, Float f, String str2, String str3, Double d) {
        Double d2 = null;
        this.f9651 = i;
        this.f9653 = str;
        this.f9650 = j;
        this.f9649 = l;
        this.f9646 = null;
        if (i == 1) {
            this.f9648 = f != null ? Double.valueOf(f.doubleValue()) : d2;
        } else {
            this.f9648 = d;
        }
        this.f9647 = str2;
        this.f9652 = str3;
    }

    zzcln(zzclp zzclp) {
        this(zzclp.f9657, zzclp.f9656, zzclp.f9654, zzclp.f9655);
    }

    zzcln(String str, long j, Object obj, String str2) {
        zzbq.m9122(str);
        this.f9651 = 2;
        this.f9653 = str;
        this.f9650 = j;
        this.f9652 = str2;
        if (obj == null) {
            this.f9649 = null;
            this.f9646 = null;
            this.f9648 = null;
            this.f9647 = null;
        } else if (obj instanceof Long) {
            this.f9649 = (Long) obj;
            this.f9646 = null;
            this.f9648 = null;
            this.f9647 = null;
        } else if (obj instanceof String) {
            this.f9649 = null;
            this.f9646 = null;
            this.f9648 = null;
            this.f9647 = (String) obj;
        } else if (obj instanceof Double) {
            this.f9649 = null;
            this.f9646 = null;
            this.f9648 = (Double) obj;
            this.f9647 = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9651);
        zzbfp.m10193(parcel, 2, this.f9653, false);
        zzbfp.m10186(parcel, 3, this.f9650);
        zzbfp.m10192(parcel, 4, this.f9649, false);
        zzbfp.m10191(parcel, 5, (Float) null, false);
        zzbfp.m10193(parcel, 6, this.f9647, false);
        zzbfp.m10193(parcel, 7, this.f9652, false);
        zzbfp.m10190(parcel, 8, this.f9648, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11364() {
        if (this.f9649 != null) {
            return this.f9649;
        }
        if (this.f9648 != null) {
            return this.f9648;
        }
        if (this.f9647 != null) {
            return this.f9647;
        }
        return null;
    }
}
