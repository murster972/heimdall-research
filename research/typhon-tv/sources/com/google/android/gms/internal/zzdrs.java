package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrw;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdrs extends zzffu<zzdrs, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdrs f10001;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdrs> f10002;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f10003 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdrw f10004;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10005;

    public static final class zza extends zzffu.zza<zzdrs, zza> implements zzfhg {
        private zza() {
            super(zzdrs.f10001);
        }

        /* synthetic */ zza(zzdrt zzdrt) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11872(int i) {
            m12541();
            ((zzdrs) this.f10398).m11862(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11873(zzdrw zzdrw) {
            m12541();
            ((zzdrs) this.f10398).m11866(zzdrw);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11874(zzfes zzfes) {
            m12541();
            ((zzdrs) this.f10398).m11859(zzfes);
            return this;
        }
    }

    static {
        zzdrs zzdrs = new zzdrs();
        f10001 = zzdrs;
        zzdrs.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdrs.f10394.m12718();
    }

    private zzdrs() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11859(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10003 = zzfes;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zza m11860() {
        zzdrs zzdrs = f10001;
        zzffu.zza zza2 = (zzffu.zza) zzdrs.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdrs);
        return (zza) zza2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdrs m11861(zzfes zzfes) throws zzfge {
        return (zzdrs) zzffu.m12526(f10001, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11862(int i) {
        this.f10005 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11866(zzdrw zzdrw) {
        if (zzdrw == null) {
            throw new NullPointerException();
        }
        this.f10004 = zzdrw;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfes m11867() {
        return this.f10003;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11868() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10005 != 0) {
            i2 = zzffg.m5245(1, this.f10005) + 0;
        }
        if (this.f10004 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10004 == null ? zzdrw.m11883() : this.f10004);
        }
        if (!this.f10003.isEmpty()) {
            i2 += zzffg.m5261(3, this.f10003);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11869() {
        return this.f10005;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11870(int i, Object obj, Object obj2) {
        zzdrw.zza zza2;
        boolean z = true;
        switch (zzdrt.f10006[i - 1]) {
            case 1:
                return new zzdrs();
            case 2:
                return f10001;
            case 3:
                return null;
            case 4:
                return new zza((zzdrt) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdrs zzdrs = (zzdrs) obj2;
                this.f10005 = zzh.m12569(this.f10005 != 0, this.f10005, zzdrs.f10005 != 0, zzdrs.f10005);
                this.f10004 = (zzdrw) zzh.m12572(this.f10004, zzdrs.f10004);
                boolean z2 = this.f10003 != zzfes.zzpfg;
                zzfes zzfes = this.f10003;
                if (zzdrs.f10003 == zzfes.zzpfg) {
                    z = false;
                }
                this.f10003 = zzh.m12570(z2, zzfes, z, zzdrs.f10003);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f10005 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f10004 != null) {
                                        zzdrw zzdrw = this.f10004;
                                        zzffu.zza zza3 = (zzffu.zza) zzdrw.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdrw);
                                        zza2 = (zzdrw.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10004 = (zzdrw) zzffb.m12416(zzdrw.m11883(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10004);
                                        this.f10004 = (zzdrw) zza2.m12543();
                                        break;
                                    }
                                case 26:
                                    this.f10003 = zzffb.m12401();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10002 == null) {
                    synchronized (zzdrs.class) {
                        if (f10002 == null) {
                            f10002 = new zzffu.zzb(f10001);
                        }
                    }
                }
                return f10002;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10001;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11871(zzffg zzffg) throws IOException {
        if (this.f10005 != 0) {
            zzffg.m5281(1, this.f10005);
        }
        if (this.f10004 != null) {
            zzffg.m5289(2, (zzfhe) this.f10004 == null ? zzdrw.m11883() : this.f10004);
        }
        if (!this.f10003.isEmpty()) {
            zzffg.m5288(3, this.f10003);
        }
        this.f10394.m12719(zzffg);
    }
}
