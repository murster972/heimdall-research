package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public final class zzaj {

    /* renamed from: 连任  reason: contains not printable characters */
    private static Comparator<byte[]> f8244 = new zzak();

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<byte[]> f8245 = new ArrayList(64);

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f8246 = 4096;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f8247 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<byte[]> f8248 = new LinkedList();

    public zzaj(int i) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final synchronized void m9646() {
        while (this.f8247 > this.f8246) {
            byte[] remove = this.f8248.remove(0);
            this.f8245.remove(remove);
            this.f8247 -= remove.length;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m9647(byte[] bArr) {
        if (bArr != null) {
            if (bArr.length <= this.f8246) {
                this.f8248.add(bArr);
                int binarySearch = Collections.binarySearch(this.f8245, bArr, f8244);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 1;
                }
                this.f8245.add(binarySearch, bArr);
                this.f8247 += bArr.length;
                m9646();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0 = new byte[r5];
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized byte[] m9648(int r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 0
            r1 = r0
        L_0x0003:
            java.util.List<byte[]> r0 = r4.f8245     // Catch:{ all -> 0x002f }
            int r0 = r0.size()     // Catch:{ all -> 0x002f }
            if (r1 >= r0) goto L_0x002c
            java.util.List<byte[]> r0 = r4.f8245     // Catch:{ all -> 0x002f }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x002f }
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x002f }
            int r2 = r0.length     // Catch:{ all -> 0x002f }
            if (r2 < r5) goto L_0x0028
            int r2 = r4.f8247     // Catch:{ all -> 0x002f }
            int r3 = r0.length     // Catch:{ all -> 0x002f }
            int r2 = r2 - r3
            r4.f8247 = r2     // Catch:{ all -> 0x002f }
            java.util.List<byte[]> r2 = r4.f8245     // Catch:{ all -> 0x002f }
            r2.remove(r1)     // Catch:{ all -> 0x002f }
            java.util.List<byte[]> r1 = r4.f8248     // Catch:{ all -> 0x002f }
            r1.remove(r0)     // Catch:{ all -> 0x002f }
        L_0x0026:
            monitor-exit(r4)
            return r0
        L_0x0028:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0003
        L_0x002c:
            byte[] r0 = new byte[r5]     // Catch:{ all -> 0x002f }
            goto L_0x0026
        L_0x002f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaj.m9648(int):byte[]");
    }
}
