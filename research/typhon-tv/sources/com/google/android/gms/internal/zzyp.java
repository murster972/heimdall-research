package com.google.android.gms.internal;

import java.util.List;

final class zzyp implements zzakh<List<zzoi>, zzog> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ int f10979;

    /* renamed from: ʼ  reason: contains not printable characters */
    private /* synthetic */ int f10980;

    /* renamed from: ʽ  reason: contains not printable characters */
    private /* synthetic */ boolean f10981;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ int f10982;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Integer f10983;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ int f10984;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ Integer f10985;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f10986;

    zzyp(zzym zzym, String str, Integer num, Integer num2, int i, int i2, int i3, int i4, boolean z) {
        this.f10986 = str;
        this.f10983 = num;
        this.f10985 = num2;
        this.f10984 = i;
        this.f10982 = i2;
        this.f10979 = i3;
        this.f10980 = i4;
        this.f10981 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13639(Object obj) {
        Integer num = null;
        List list = (List) obj;
        if (list == null || list.isEmpty()) {
            return null;
        }
        String str = this.f10986;
        Integer num2 = this.f10983;
        Integer num3 = this.f10985;
        if (this.f10984 > 0) {
            num = Integer.valueOf(this.f10984);
        }
        return new zzog(str, list, num2, num3, num, this.f10982 + this.f10979, this.f10980, this.f10981);
    }
}
