package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzvk extends zzev implements zzvj {
    public zzvk() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 2:
                String r1 = m13547();
                parcel2.writeNoException();
                parcel2.writeString(r1);
                return true;
            case 3:
                List r12 = m13542();
                parcel2.writeNoException();
                parcel2.writeList(r12);
                return true;
            case 4:
                String r13 = m13545();
                parcel2.writeNoException();
                parcel2.writeString(r13);
                return true;
            case 5:
                zzpq r14 = m13544();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r14);
                return true;
            case 6:
                String r15 = m13541();
                parcel2.writeNoException();
                parcel2.writeString(r15);
                return true;
            case 7:
                double r2 = m13531();
                parcel2.writeNoException();
                parcel2.writeDouble(r2);
                return true;
            case 8:
                String r16 = m13532();
                parcel2.writeNoException();
                parcel2.writeString(r16);
                return true;
            case 9:
                String r17 = m13533();
                parcel2.writeNoException();
                parcel2.writeString(r17);
                return true;
            case 10:
                m13538();
                parcel2.writeNoException();
                return true;
            case 11:
                m13548(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 12:
                m13543(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 13:
                boolean r18 = m13539();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r18);
                return true;
            case 14:
                boolean r19 = m13540();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r19);
                return true;
            case 15:
                Bundle r110 = m13536();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r110);
                return true;
            case 16:
                m13546(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 17:
                zzll r111 = m13534();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r111);
                return true;
            case 18:
                IObjectWrapper r112 = m13535();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r112);
                return true;
            case 19:
                zzpm r113 = m13549();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r113);
                return true;
            case 20:
                IObjectWrapper r114 = m13550();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r114);
                return true;
            case 21:
                IObjectWrapper r115 = m13537();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r115);
                return true;
            default:
                return false;
        }
    }
}
