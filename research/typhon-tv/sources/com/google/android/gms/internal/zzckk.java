package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzckk implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzckg f9568;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9569;

    zzckk(zzckg zzckg, zzcgi zzcgi) {
        this.f9568 = zzckg;
        this.f9569 = zzcgi;
    }

    public final void run() {
        zzche r0 = this.f9568.f9558;
        if (r0 == null) {
            this.f9568.m11096().m10832().m10849("Discarding data. Failed to send app launch");
            return;
        }
        try {
            r0.m10683(this.f9569);
            this.f9568.m11264(r0, (zzbfm) null, this.f9569);
            this.f9568.m11223();
        } catch (RemoteException e) {
            this.f9568.m11096().m10832().m10850("Failed to send app launch to the service", e);
        }
    }
}
