package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzazc extends zzeu implements zzazb {
    zzazc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.internal.IMediaRouterCallback");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9799(String str, Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(2, v_);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9800(String str, Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(4, v_);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9801(String str, Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(3, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9802(String str, Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(1, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9803(String str, Bundle bundle, int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12306(v_, (Parcelable) bundle);
        v_.writeInt(i);
        m12298(6, v_);
    }
}
