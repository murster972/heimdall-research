package com.google.android.gms.internal;

import android.os.RemoteException;
import android.text.TextUtils;

final class zzcko implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzckg f9575;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ String f9576;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f9577;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9578;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzcha f9579;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ boolean f9580 = true;

    zzcko(zzckg zzckg, boolean z, boolean z2, zzcha zzcha, zzcgi zzcgi, String str) {
        this.f9575 = zzckg;
        this.f9577 = z2;
        this.f9579 = zzcha;
        this.f9578 = zzcgi;
        this.f9576 = str;
    }

    public final void run() {
        zzche r1 = this.f9575.f9558;
        if (r1 == null) {
            this.f9575.m11096().m10832().m10849("Discarding data. Failed to send event to service");
            return;
        }
        if (this.f9580) {
            this.f9575.m11264(r1, this.f9577 ? null : this.f9579, this.f9578);
        } else {
            try {
                if (TextUtils.isEmpty(this.f9576)) {
                    r1.m10686(this.f9579, this.f9578);
                } else {
                    r1.m10687(this.f9579, this.f9576, this.f9575.m11096().m10831());
                }
            } catch (RemoteException e) {
                this.f9575.m11096().m10832().m10850("Failed to send event to the service", e);
            }
        }
        this.f9575.m11223();
    }
}
