package com.google.android.gms.internal;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

@zzzv
final class zzaky<V> extends FutureTask<V> implements zzakv<V> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzakw f4303 = new zzakw();

    zzaky(Runnable runnable, V v) {
        super(runnable, v);
    }

    zzaky(Callable<V> callable) {
        super(callable);
    }

    /* access modifiers changed from: protected */
    public final void done() {
        this.f4303.m4815();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4817(Runnable runnable, Executor executor) {
        this.f4303.m4816(runnable, executor);
    }
}
