package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;

public interface zzos {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m13184();

    /* renamed from: ʼ  reason: contains not printable characters */
    void m13185();

    /* renamed from: ʽ  reason: contains not printable characters */
    View m13186();

    /* renamed from: ˑ  reason: contains not printable characters */
    Context m13187();

    /* renamed from: 靐  reason: contains not printable characters */
    void m13188(Bundle bundle);

    /* renamed from: 靐  reason: contains not printable characters */
    void m13189(View view);

    /* renamed from: 靐  reason: contains not printable characters */
    void m13190(View view, Map<String, WeakReference<View>> map);

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m13191();

    /* renamed from: 齉  reason: contains not printable characters */
    void m13192(Bundle bundle);

    /* renamed from: 齉  reason: contains not printable characters */
    void m13193(View view, Map<String, WeakReference<View>> map);

    /* renamed from: 龘  reason: contains not printable characters */
    View m13194(View.OnClickListener onClickListener, boolean z);

    /* renamed from: 龘  reason: contains not printable characters */
    void m13195(MotionEvent motionEvent);

    /* renamed from: 龘  reason: contains not printable characters */
    void m13196(View view);

    /* renamed from: 龘  reason: contains not printable characters */
    void m13197(View view, zzoq zzoq);

    /* renamed from: 龘  reason: contains not printable characters */
    void m13198(View view, String str, Bundle bundle, Map<String, WeakReference<View>> map, View view2);

    /* renamed from: 龘  reason: contains not printable characters */
    void m13199(View view, Map<String, WeakReference<View>> map);

    /* renamed from: 龘  reason: contains not printable characters */
    void m13200(View view, Map<String, WeakReference<View>> map, Bundle bundle, View view2);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m13201();

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m13202(Bundle bundle);
}
