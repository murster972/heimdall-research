package com.google.android.gms.internal;

import com.mopub.common.TyphoonApp;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

@zzzv
public final class zzalg {
    /* renamed from: 龘  reason: contains not printable characters */
    public static HttpURLConnection m4825(String str, int i) throws IOException {
        URL url = new URL(str);
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            if (i3 <= 20) {
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(i);
                openConnection.setReadTimeout(i);
                if (!(openConnection instanceof HttpURLConnection)) {
                    throw new IOException("Invalid protocol.");
                }
                HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
                zzajv zzajv = new zzajv();
                zzajv.m4788(httpURLConnection, (byte[]) null);
                httpURLConnection.setInstanceFollowRedirects(false);
                int responseCode = httpURLConnection.getResponseCode();
                zzajv.m4787(httpURLConnection, responseCode);
                if (responseCode / 100 != 3) {
                    return httpURLConnection;
                }
                String headerField = httpURLConnection.getHeaderField("Location");
                if (headerField == null) {
                    throw new IOException("Missing Location header in redirect");
                }
                URL url2 = new URL(url, headerField);
                String protocol = url2.getProtocol();
                if (protocol == null) {
                    throw new IOException("Protocol is null");
                } else if (protocol.equals(TyphoonApp.HTTP) || protocol.equals(TyphoonApp.HTTPS)) {
                    String valueOf = String.valueOf(headerField);
                    zzagf.m4792(valueOf.length() != 0 ? "Redirecting to ".concat(valueOf) : new String("Redirecting to "));
                    httpURLConnection.disconnect();
                    i2 = i3;
                    url = url2;
                } else {
                    String valueOf2 = String.valueOf(protocol);
                    throw new IOException(valueOf2.length() != 0 ? "Unsupported scheme: ".concat(valueOf2) : new String("Unsupported scheme: "));
                }
            } else {
                throw new IOException("Too many redirects (20)");
            }
        }
    }
}
