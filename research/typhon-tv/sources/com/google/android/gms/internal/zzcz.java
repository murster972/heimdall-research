package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzcz extends zzbt<Integer, Long> {

    /* renamed from: 靐  reason: contains not printable characters */
    public long f9845;

    /* renamed from: 龘  reason: contains not printable characters */
    public long f9846;

    public zzcz() {
        this.f9846 = -1;
        this.f9845 = -1;
    }

    public zzcz(String str) {
        this();
        m10294(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<Integer, Long> m11579() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, Long.valueOf(this.f9846));
        hashMap.put(1, Long.valueOf(this.f9845));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11580(String str) {
        HashMap r1 = m10292(str);
        if (r1 != null) {
            this.f9846 = ((Long) r1.get(0)).longValue();
            this.f9845 = ((Long) r1.get(1)).longValue();
        }
    }
}
