package com.google.android.gms.internal;

import java.io.IOException;

class zzfez extends zzfey {
    protected final byte[] zzjng;

    zzfez(byte[] bArr) {
        this.zzjng = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfes)) {
            return false;
        }
        if (size() != ((zzfes) obj).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        if (!(obj instanceof zzfez)) {
            return obj.equals(this);
        }
        int r1 = m12376();
        int r0 = ((zzfez) obj).m12376();
        if (r1 == 0 || r0 == 0 || r1 == r0) {
            return m12390((zzfez) obj, 0, size());
        }
        return false;
    }

    public int size() {
        return this.zzjng.length;
    }

    public final zzffb zzcvm() {
        return zzffb.m12399(this.zzjng, m12391(), size(), true);
    }

    public byte zzkn(int i) {
        return this.zzjng[i];
    }

    public final zzfes zzx(int i, int i2) {
        int r1 = m12370(i, i2, size());
        return r1 == 0 ? zzfes.zzpfg : new zzfev(this.zzjng, m12391() + i, r1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m12391() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12392(int i, int i2, int i3) {
        return zzffz.m12576(i, this.zzjng, m12391() + i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12393(zzfer zzfer) throws IOException {
        zzfer.m12369(this.zzjng, m12391(), size());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m12394(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.zzjng, i, bArr, i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12395(zzfes zzfes, int i, int i2) {
        if (i2 > zzfes.size()) {
            throw new IllegalArgumentException(new StringBuilder(40).append("Length too large: ").append(i2).append(size()).toString());
        } else if (i + i2 > zzfes.size()) {
            throw new IllegalArgumentException(new StringBuilder(59).append("Ran off end of other: ").append(i).append(", ").append(i2).append(", ").append(zzfes.size()).toString());
        } else if (!(zzfes instanceof zzfez)) {
            return zzfes.zzx(i, i + i2).equals(zzx(0, i2));
        } else {
            zzfez zzfez = (zzfez) zzfes;
            byte[] bArr = this.zzjng;
            byte[] bArr2 = zzfez.zzjng;
            int r5 = m12391() + i2;
            int r2 = m12391();
            int r1 = zzfez.m12391() + i;
            while (r2 < r5) {
                if (bArr[r2] != bArr2[r1]) {
                    return false;
                }
                r2++;
                r1++;
            }
            return true;
        }
    }
}
