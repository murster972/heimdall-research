package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzbz;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzd;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class zzbhr extends zzbhu {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzbhk f8778;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbhr(zzbhq zzbhq, GoogleApiClient googleApiClient, zzbhk zzbhk) {
        super(googleApiClient);
        this.f8778 = zzbhk;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Result m10262(Status status) {
        return new zzbhw(status, new HashMap());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10263(Context context, zzbij zzbij) throws RemoteException {
        String str;
        String str2;
        DataHolder.zza r2 = zzd.m9027();
        for (Map.Entry next : this.f8778.m10235().entrySet()) {
            zzd.m9028(r2, new zzbib((String) next.getKey(), (String) next.getValue()));
        }
        DataHolder r4 = r2.m9026(0);
        String r5 = zzbz.m4202(context) == Status.f7464 ? zzbz.m4204() : null;
        try {
            str = FirebaseInstanceId.m13786().m13796();
            try {
                str2 = FirebaseInstanceId.m13786().m13795();
            } catch (IllegalStateException e) {
                e = e;
            }
        } catch (IllegalStateException e2) {
            e = e2;
            str = null;
        }
        try {
            zzbij.m10289(this.f8779, new zzbid(context.getPackageName(), this.f8778.m10238(), r4, r5, str, str2, (List<String>) null, this.f8778.m10237(), zzbhp.m10255(context), this.f8778.m10236(), this.f8778.m10234()));
        } finally {
            r4.close();
        }
        if (Log.isLoggable("ConfigApiImpl", 3)) {
            Log.d("ConfigApiImpl", "Cannot retrieve instanceId or instanceIdToken.", e);
        }
        str2 = null;
        zzbij.m10289(this.f8779, new zzbid(context.getPackageName(), this.f8778.m10238(), r4, r5, str, str2, (List<String>) null, this.f8778.m10237(), zzbhp.m10255(context), this.f8778.m10236(), this.f8778.m10234()));
    }
}
