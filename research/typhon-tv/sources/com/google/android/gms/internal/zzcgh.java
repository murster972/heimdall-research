package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;

final class zzcgh {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f9103;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f9104;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f9105;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f9106;

    /* renamed from: ʿ  reason: contains not printable characters */
    private long f9107;

    /* renamed from: ˆ  reason: contains not printable characters */
    private long f9108;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f9109;

    /* renamed from: ˉ  reason: contains not printable characters */
    private long f9110;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f9111;

    /* renamed from: ˋ  reason: contains not printable characters */
    private long f9112;

    /* renamed from: ˎ  reason: contains not printable characters */
    private long f9113;

    /* renamed from: ˏ  reason: contains not printable characters */
    private long f9114;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f9115;

    /* renamed from: י  reason: contains not printable characters */
    private long f9116;

    /* renamed from: ـ  reason: contains not printable characters */
    private String f9117;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f9118;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f9119;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f9120;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private long f9121;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private long f9122;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f9123;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f9124;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f9125;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f9126;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcim f9127;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f9128;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private long f9129;

    zzcgh(zzcim zzcim, String str) {
        zzbq.m9120(zzcim);
        zzbq.m9122(str);
        this.f9127 = zzcim;
        this.f9124 = str;
        this.f9127.m11018().m11109();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m10460() {
        this.f9127.m11018().m11109();
        return this.f9103;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m10461(long j) {
        boolean z = true;
        zzbq.m9116(j >= 0);
        this.f9127.m11018().m11109();
        boolean z2 = this.f9120;
        if (this.f9104 == j) {
            z = false;
        }
        this.f9120 = z2 | z;
        this.f9104 = j;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m10462(String str) {
        this.f9127.m11018().m11109();
        this.f9120 = (!zzclq.m11387(this.f9109, str)) | this.f9120;
        this.f9109 = str;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final long m10463() {
        this.f9127.m11018().m11109();
        return this.f9105;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m10464(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9121 != j) | this.f9120;
        this.f9121 = j;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m10465(String str) {
        this.f9127.m11018().m11109();
        this.f9120 = (!zzclq.m11387(this.f9117, str)) | this.f9120;
        this.f9117 = str;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final long m10466() {
        this.f9127.m11018().m11109();
        return this.f9115;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m10467(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9122 != j) | this.f9120;
        this.f9122 = j;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final long m10468() {
        this.f9127.m11018().m11109();
        return this.f9107;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m10469(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9116 != j) | this.f9120;
        this.f9116 = j;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m10470(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9114 != j) | this.f9120;
        this.f9114 = j;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final boolean m10471() {
        this.f9127.m11018().m11109();
        return this.f9128;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final long m10472() {
        this.f9127.m11018().m11109();
        return this.f9113;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final long m10473() {
        this.f9127.m11018().m11109();
        return this.f9106;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m10474(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9110 != j) | this.f9120;
        this.f9110 = j;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final long m10475() {
        this.f9127.m11018().m11109();
        return this.f9108;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final long m10476() {
        this.f9127.m11018().m11109();
        return this.f9122;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m10477() {
        this.f9127.m11018().m11109();
        long j = this.f9104 + 1;
        if (j > 2147483647L) {
            this.f9127.m11016().m10834().m10850("Bundle index overflow. appId", zzchm.m10812(this.f9124));
            j = 0;
        }
        this.f9120 = true;
        this.f9104 = j;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final long m10478() {
        this.f9127.m11018().m11109();
        return this.f9112;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final long m10479() {
        this.f9127.m11018().m11109();
        return this.f9110;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m10480() {
        this.f9127.m11018().m11109();
        return this.f9118;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m10481(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9112 != j) | this.f9120;
        this.f9112 = j;
    }

    /* renamed from: י  reason: contains not printable characters */
    public final long m10482() {
        this.f9127.m11018().m11109();
        return this.f9116;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final long m10483() {
        this.f9127.m11018().m11109();
        return this.f9114;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final long m10484() {
        this.f9127.m11018().m11109();
        return this.f9119;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m10485(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9113 != j) | this.f9120;
        this.f9113 = j;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m10486() {
        this.f9127.m11018().m11109();
        return this.f9109;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m10487(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9108 != j) | this.f9120;
        this.f9108 = j;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final String m10488() {
        this.f9127.m11018().m11109();
        return this.f9117;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String m10489() {
        this.f9127.m11018().m11109();
        String str = this.f9117;
        m10465((String) null);
        return str;
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final long m10490() {
        this.f9127.m11018().m11109();
        return this.f9129;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final boolean m10491() {
        this.f9127.m11018().m11109();
        return this.f9111;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m10492() {
        this.f9127.m11018().m11109();
        return this.f9123;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m10493(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9107 != j) | this.f9120;
        this.f9107 = j;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m10494(String str) {
        this.f9127.m11018().m11109();
        this.f9120 = (!zzclq.m11387(this.f9118, str)) | this.f9120;
        this.f9118 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10495() {
        this.f9127.m11018().m11109();
        return this.f9124;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10496(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9115 != j) | this.f9120;
        this.f9115 = j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10497(String str) {
        this.f9127.m11018().m11109();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f9120 = (!zzclq.m11387(this.f9125, str)) | this.f9120;
        this.f9125 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10498(boolean z) {
        this.f9127.m11018().m11109();
        this.f9120 = this.f9111 != z;
        this.f9111 = z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m10499() {
        this.f9127.m11018().m11109();
        return this.f9125;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10500(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9106 != j) | this.f9120;
        this.f9106 = j;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10501(String str) {
        this.f9127.m11018().m11109();
        this.f9120 = (!zzclq.m11387(this.f9103, str)) | this.f9120;
        this.f9103 = str;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m10502() {
        this.f9127.m11018().m11109();
        return this.f9126;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10503(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9119 != j) | this.f9120;
        this.f9119 = j;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10504(String str) {
        this.f9127.m11018().m11109();
        this.f9120 = (!zzclq.m11387(this.f9123, str)) | this.f9120;
        this.f9123 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10505() {
        this.f9127.m11018().m11109();
        this.f9120 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10506(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9105 != j) | this.f9120;
        this.f9105 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10507(String str) {
        this.f9127.m11018().m11109();
        this.f9120 = (!zzclq.m11387(this.f9126, str)) | this.f9120;
        this.f9126 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10508(boolean z) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9128 != z) | this.f9120;
        this.f9128 = z;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final long m10509() {
        this.f9127.m11018().m11109();
        return this.f9104;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m10510(long j) {
        this.f9127.m11018().m11109();
        this.f9120 = (this.f9129 != j) | this.f9120;
        this.f9129 = j;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final long m10511() {
        this.f9127.m11018().m11109();
        return this.f9121;
    }
}
