package com.google.android.gms.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface zzbhm extends Result {
    Status s_();

    /* renamed from: 靐  reason: contains not printable characters */
    long m10250();

    /* renamed from: 麤  reason: contains not printable characters */
    Map<String, Set<String>> m10251();

    /* renamed from: 齉  reason: contains not printable characters */
    List<byte[]> m10252();

    /* renamed from: 龘  reason: contains not printable characters */
    byte[] m10253(String str, byte[] bArr, String str2);
}
