package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.gmsg.zzz;
import com.google.android.gms.ads.internal.js.zzaa;
import com.google.android.gms.ads.internal.js.zzaj;
import com.google.android.gms.ads.internal.js.zzn;
import com.google.android.gms.ads.internal.zzbs;
import org.json.JSONObject;

@zzzv
public final class zzgf implements zzgo {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzt<zzaj> f4644 = new zzgk(this);

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzt<zzaj> f4645 = new zzgl(this);

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzt<zzaj> f4646 = new zzgm(this);

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzt<zzaj> f4647 = new zzgn(this);
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f4648;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f4649;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzaa f4650;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzz f4651;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzft f4652;

    public zzgf(zzft zzft, zzn zzn, Context context) {
        this.f4652 = zzft;
        this.f4649 = context;
        this.f4651 = new zzz(this.f4649);
        this.f4650 = zzn.zzb((zzcv) null);
        this.f4650.zza(new zzgg(this), new zzgh(this));
        String valueOf = String.valueOf(this.f4652.f4636.m5300());
        zzagf.m4792(valueOf.length() != 0 ? "Core JS tracking ad unit: ".concat(valueOf) : new String("Core JS tracking ad unit: "));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5342() {
        this.f4650.zza(new zzgj(this), new zzalj());
        this.f4650.release();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5343(zzaj zzaj) {
        zzaj.zzb("/visibilityChanged", this.f4646);
        zzaj.zzb("/untrackActiveViewUnit", this.f4645);
        zzaj.zzb("/updateActiveView", this.f4644);
        if (zzbs.zzfd().m4436(this.f4649)) {
            zzaj.zzb("/logScionEvent", this.f4647);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5344(zzaj zzaj) {
        zzaj.zza("/updateActiveView", this.f4644);
        zzaj.zza("/untrackActiveViewUnit", this.f4645);
        zzaj.zza("/visibilityChanged", this.f4646);
        if (zzbs.zzfd().m4436(this.f4649)) {
            zzaj.zza("/logScionEvent", this.f4647);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5345(JSONObject jSONObject, boolean z) {
        this.f4650.zza(new zzgi(this, jSONObject), new zzalj());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5346() {
        return this.f4648;
    }
}
