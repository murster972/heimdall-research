package com.google.android.gms.internal;

@zzzv
public final class zzaco {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f3979;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean f3980;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean f3981;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int f3982;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final int f3983;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final int f3984;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String f3985;

    /* renamed from: ˉ  reason: contains not printable characters */
    public final int f3986;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int f3987;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final int f3988;

    /* renamed from: ˎ  reason: contains not printable characters */
    public final float f3989;

    /* renamed from: ˏ  reason: contains not printable characters */
    public final double f3990;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean f3991;

    /* renamed from: י  reason: contains not printable characters */
    public final boolean f3992;

    /* renamed from: ـ  reason: contains not printable characters */
    public final boolean f3993;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final String f3994;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String f3995;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final int f3996;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String f3997;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final String f3998;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final boolean f3999;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f4000;

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f4001;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f4002;

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean f4003;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f4004;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final int f4005;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final int f4006;

    zzaco(int i, boolean z, boolean z2, String str, String str2, boolean z3, boolean z4, boolean z5, boolean z6, String str3, String str4, String str5, int i2, int i3, int i4, int i5, int i6, int i7, float f, int i8, int i9, double d, boolean z7, boolean z8, int i10, String str6, boolean z9, String str7) {
        this.f4004 = i;
        this.f4001 = z;
        this.f4003 = z2;
        this.f4002 = str;
        this.f4000 = str2;
        this.f3979 = z3;
        this.f3980 = z4;
        this.f3981 = z5;
        this.f3991 = z6;
        this.f3994 = str3;
        this.f3995 = str4;
        this.f3982 = i2;
        this.f3983 = i3;
        this.f4005 = i4;
        this.f4006 = i5;
        this.f3987 = i6;
        this.f3988 = i7;
        this.f3989 = f;
        this.f3984 = i8;
        this.f3986 = i9;
        this.f3990 = d;
        this.f3992 = z7;
        this.f3993 = z8;
        this.f3996 = i10;
        this.f3997 = str6;
        this.f3999 = z9;
        this.f3985 = str5;
        this.f3998 = str7;
    }
}
