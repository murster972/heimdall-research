package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzaha extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8191;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8192;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaha(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8192 = context;
        this.f8191 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9599() {
        SharedPreferences sharedPreferences = this.f8192.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putInt("webview_cache_version", sharedPreferences.getInt("webview_cache_version", 0));
        if (this.f8191 != null) {
            this.f8191.m9605(bundle);
        }
    }
}
