package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition$zza;

abstract class zzcds extends ActivityRecognition$zza<Status> {
    public zzcds(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Result m10331(Status status) {
        return status;
    }
}
