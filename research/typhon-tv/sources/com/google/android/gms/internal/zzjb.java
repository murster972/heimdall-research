package com.google.android.gms.internal;

import java.io.IOException;

public final class zzjb extends zzfjm<zzjb> {

    /* renamed from: 靐  reason: contains not printable characters */
    public zzjd f10745 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10746 = null;

    public zzjb() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m13022() {
        int r0 = super.m12841();
        if (this.f10746 != null) {
            r0 += zzfjk.m12808(1, this.f10746);
        }
        return this.f10745 != null ? r0 + zzfjk.m12807(4, (zzfjs) this.f10745) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m13023(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10746 = zzfjj.m12791();
                    continue;
                case 34:
                    if (this.f10745 == null) {
                        this.f10745 = new zzjd();
                    }
                    zzfjj.m12802((zzfjs) this.f10745);
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13024(zzfjk zzfjk) throws IOException {
        if (this.f10746 != null) {
            zzfjk.m12835(1, this.f10746);
        }
        if (this.f10745 != null) {
            zzfjk.m12834(4, (zzfjs) this.f10745);
        }
        super.m12842(zzfjk);
    }
}
