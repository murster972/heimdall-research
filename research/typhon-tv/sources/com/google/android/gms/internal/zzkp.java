package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;

public final class zzkp extends zzeu implements zzkn {
    zzkp(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }

    public final void zza(PublisherAdViewOptions publisherAdViewOptions) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) publisherAdViewOptions);
        m12298(9, v_);
    }

    public final void zza(zzpe zzpe) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzpe);
        m12298(6, v_);
    }

    public final void zza(zzqq zzqq) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzqq);
        m12298(3, v_);
    }

    public final void zza(zzqt zzqt) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzqt);
        m12298(4, v_);
    }

    public final void zza(zzrc zzrc) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzrc);
        m12298(10, v_);
    }

    public final void zza(zzrf zzrf, zzjn zzjn) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzrf);
        zzew.m12306(v_, (Parcelable) zzjn);
        m12298(8, v_);
    }

    public final void zza(String str, zzqz zzqz, zzqw zzqw) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) zzqz);
        zzew.m12305(v_, (IInterface) zzqw);
        m12298(5, v_);
    }

    public final void zzb(zzkh zzkh) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzkh);
        m12298(2, v_);
    }

    public final void zzb(zzld zzld) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzld);
        m12298(7, v_);
    }

    public final zzkk zzdi() throws RemoteException {
        zzkk zzkm;
        Parcel r1 = m12300(1, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzkm = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoader");
            zzkm = queryLocalInterface instanceof zzkk ? (zzkk) queryLocalInterface : new zzkm(readStrongBinder);
        }
        r1.recycle();
        return zzkm;
    }
}
