package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class zzcea implements Parcelable.Creator<zzcdz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r5 = zzbfn.m10169(parcel);
        int i = 1;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        zzcdx zzcdx = null;
        while (parcel.dataPosition() < r5) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    zzcdx = (zzcdx) zzbfn.m10171(parcel, readInt, zzcdx.CREATOR);
                    break;
                case 3:
                    iBinder2 = zzbfn.m10156(parcel, readInt);
                    break;
                case 4:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r5);
        return new zzcdz(i, zzcdx, iBinder2, iBinder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcdz[i];
    }
}
