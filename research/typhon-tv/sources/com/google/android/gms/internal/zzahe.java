package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzahe extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8199;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8200;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzahe(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8200 = context;
        this.f8199 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9603() {
        SharedPreferences sharedPreferences = this.f8200.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putString("content_url_hashes", sharedPreferences.getString("content_url_hashes", ""));
        if (this.f8199 != null) {
            this.f8199.m9605(bundle);
        }
    }
}
