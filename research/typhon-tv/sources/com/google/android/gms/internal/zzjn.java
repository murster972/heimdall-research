package com.google.android.gms.internal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.zzb;

@zzzv
public class zzjn extends zzbfm {
    public static final Parcelable.Creator<zzjn> CREATOR = new zzjo();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int f4789;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final zzjn[] f4790;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean f4791;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean f4792;

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean f4793;

    /* renamed from: 连任  reason: contains not printable characters */
    public final int f4794;

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f4795;

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean f4796;

    /* renamed from: 齉  reason: contains not printable characters */
    public final int f4797;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f4798;

    public zzjn() {
        this("interstitial_mb", 0, 0, true, 0, 0, (zzjn[]) null, false, false, false);
    }

    public zzjn(Context context, AdSize adSize) {
        this(context, new AdSize[]{adSize});
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzjn(android.content.Context r13, com.google.android.gms.ads.AdSize[] r14) {
        /*
            r12 = this;
            r1 = 1
            r2 = 0
            r12.<init>()
            r6 = r14[r2]
            r12.f4796 = r2
            boolean r0 = r6.isFluid()
            r12.f4792 = r0
            boolean r0 = r12.f4792
            if (r0 == 0) goto L_0x00be
            com.google.android.gms.ads.AdSize r0 = com.google.android.gms.ads.AdSize.BANNER
            int r0 = r0.getWidth()
            r12.f4794 = r0
            com.google.android.gms.ads.AdSize r0 = com.google.android.gms.ads.AdSize.BANNER
            int r0 = r0.getHeight()
            r12.f4795 = r0
        L_0x0023:
            int r0 = r12.f4794
            r3 = -1
            if (r0 != r3) goto L_0x00cc
            r0 = r1
        L_0x0029:
            int r3 = r12.f4795
            r4 = -2
            if (r3 != r4) goto L_0x00cf
            r3 = r1
        L_0x002f:
            android.content.res.Resources r4 = r13.getResources()
            android.util.DisplayMetrics r7 = r4.getDisplayMetrics()
            if (r0 == 0) goto L_0x00d7
            com.google.android.gms.internal.zzkb.m5487()
            boolean r4 = com.google.android.gms.internal.zzajr.m4745(r13)
            if (r4 == 0) goto L_0x00d2
            com.google.android.gms.internal.zzkb.m5487()
            boolean r4 = com.google.android.gms.internal.zzajr.m4746(r13)
            if (r4 == 0) goto L_0x00d2
            int r4 = r7.widthPixels
            com.google.android.gms.internal.zzkb.m5487()
            int r5 = com.google.android.gms.internal.zzajr.m4747(r13)
            int r4 = r4 - r5
            r12.f4789 = r4
        L_0x0057:
            int r4 = r12.f4789
            float r4 = (float) r4
            float r5 = r7.density
            float r4 = r4 / r5
            double r8 = (double) r4
            int r4 = (int) r8
            int r5 = (int) r8
            double r10 = (double) r5
            double r8 = r8 - r10
            r10 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            int r5 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r5 < 0) goto L_0x006d
            int r4 = r4 + 1
        L_0x006d:
            r5 = r4
        L_0x006e:
            if (r3 == 0) goto L_0x00e6
            int r4 = m5454(r7)
        L_0x0074:
            com.google.android.gms.internal.zzkb.m5487()
            int r7 = com.google.android.gms.internal.zzajr.m4758((android.util.DisplayMetrics) r7, (int) r4)
            r12.f4797 = r7
            if (r0 != 0) goto L_0x0081
            if (r3 == 0) goto L_0x00e9
        L_0x0081:
            r0 = 26
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r0)
            java.lang.StringBuilder r0 = r3.append(r5)
            java.lang.String r3 = "x"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r3 = "_as"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r12.f4798 = r0
        L_0x00a4:
            int r0 = r14.length
            if (r0 <= r1) goto L_0x00fa
            int r0 = r14.length
            com.google.android.gms.internal.zzjn[] r0 = new com.google.android.gms.internal.zzjn[r0]
            r12.f4790 = r0
            r0 = r2
        L_0x00ad:
            int r1 = r14.length
            if (r0 >= r1) goto L_0x00fd
            com.google.android.gms.internal.zzjn[] r1 = r12.f4790
            com.google.android.gms.internal.zzjn r3 = new com.google.android.gms.internal.zzjn
            r4 = r14[r0]
            r3.<init>((android.content.Context) r13, (com.google.android.gms.ads.AdSize) r4)
            r1[r0] = r3
            int r0 = r0 + 1
            goto L_0x00ad
        L_0x00be:
            int r0 = r6.getWidth()
            r12.f4794 = r0
            int r0 = r6.getHeight()
            r12.f4795 = r0
            goto L_0x0023
        L_0x00cc:
            r0 = r2
            goto L_0x0029
        L_0x00cf:
            r3 = r2
            goto L_0x002f
        L_0x00d2:
            int r4 = r7.widthPixels
            r12.f4789 = r4
            goto L_0x0057
        L_0x00d7:
            int r4 = r12.f4794
            com.google.android.gms.internal.zzkb.m5487()
            int r5 = r12.f4794
            int r5 = com.google.android.gms.internal.zzajr.m4758((android.util.DisplayMetrics) r7, (int) r5)
            r12.f4789 = r5
            r5 = r4
            goto L_0x006e
        L_0x00e6:
            int r4 = r12.f4795
            goto L_0x0074
        L_0x00e9:
            boolean r0 = r12.f4792
            if (r0 == 0) goto L_0x00f3
            java.lang.String r0 = "320x50_mb"
            r12.f4798 = r0
            goto L_0x00a4
        L_0x00f3:
            java.lang.String r0 = r6.toString()
            r12.f4798 = r0
            goto L_0x00a4
        L_0x00fa:
            r0 = 0
            r12.f4790 = r0
        L_0x00fd:
            r12.f4791 = r2
            r12.f4793 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzjn.<init>(android.content.Context, com.google.android.gms.ads.AdSize[]):void");
    }

    public zzjn(zzjn zzjn, zzjn[] zzjnArr) {
        this(zzjn.f4798, zzjn.f4795, zzjn.f4797, zzjn.f4796, zzjn.f4794, zzjn.f4789, zzjnArr, zzjn.f4791, zzjn.f4792, zzjn.f4793);
    }

    zzjn(String str, int i, int i2, boolean z, int i3, int i4, zzjn[] zzjnArr, boolean z2, boolean z3, boolean z4) {
        this.f4798 = str;
        this.f4795 = i;
        this.f4797 = i2;
        this.f4796 = z;
        this.f4794 = i3;
        this.f4789 = i4;
        this.f4790 = zzjnArr;
        this.f4791 = z2;
        this.f4792 = z3;
        this.f4793 = z4;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m5453(DisplayMetrics displayMetrics) {
        return (int) (((float) m5454(displayMetrics)) * displayMetrics.density);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m5454(DisplayMetrics displayMetrics) {
        int i = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i <= 400) {
            return 32;
        }
        return i <= 720 ? 50 : 90;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m5455(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzjn m5456() {
        return new zzjn("reward_mb", 0, 0, true, 0, 0, (zzjn[]) null, false, false, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzjn m5457(Context context) {
        return new zzjn("320x50_mb", 0, 0, false, 0, 0, (zzjn[]) null, true, false, false);
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f4798, false);
        zzbfp.m10185(parcel, 3, this.f4795);
        zzbfp.m10185(parcel, 4, this.f4797);
        zzbfp.m10195(parcel, 5, this.f4796);
        zzbfp.m10185(parcel, 6, this.f4794);
        zzbfp.m10185(parcel, 7, this.f4789);
        zzbfp.m10199(parcel, 8, (T[]) this.f4790, i, false);
        zzbfp.m10195(parcel, 9, this.f4791);
        zzbfp.m10195(parcel, 10, this.f4792);
        zzbfp.m10195(parcel, 11, this.f4793);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final AdSize m5458() {
        return zzb.zza(this.f4794, this.f4795, this.f4798);
    }
}
