package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfek;
import com.google.android.gms.internal.zzfel;
import java.io.IOException;

public abstract class zzfek<MessageType extends zzfek<MessageType, BuilderType>, BuilderType extends zzfel<MessageType, BuilderType>> implements zzfhe {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f10341 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    protected int f10342 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final zzfes m12361() {
        try {
            zzfex r0 = zzfes.m12373(m12625());
            m12626(r0.m12386());
            return r0.m12387();
        } catch (IOException e) {
            String name = getClass().getName();
            throw new RuntimeException(new StringBuilder(String.valueOf(name).length() + 62 + String.valueOf("ByteString").length()).append("Serializing ").append(name).append(" to a ").append("ByteString").append(" threw an IOException (should never happen).").toString(), e);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final byte[] m12362() {
        try {
            byte[] bArr = new byte[m12625()];
            zzffg r1 = zzffg.m5271(bArr);
            m12626(r1);
            r1.m5272();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            throw new RuntimeException(new StringBuilder(String.valueOf(name).length() + 62 + String.valueOf("byte array").length()).append("Serializing ").append(name).append(" to a ").append("byte array").append(" threw an IOException (should never happen).").toString(), e);
        }
    }
}
