package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.zzd;
import com.google.android.gms.cast.framework.zzab;
import com.google.android.gms.cast.framework.zzh;
import com.google.android.gms.cast.framework.zzj;
import com.google.android.gms.cast.framework.zzl;
import com.google.android.gms.cast.framework.zzr;
import com.google.android.gms.cast.framework.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzazu;
import java.util.Map;

public final class zzayy extends zzeu implements zzayx {
    zzayy(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.internal.ICastDynamiteModule");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzd m9775(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3, CastMediaOptions castMediaOptions) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) iObjectWrapper2);
        zzew.m12305(v_, (IInterface) iObjectWrapper3);
        zzew.m12306(v_, (Parcelable) castMediaOptions);
        Parcel r0 = m12300(4, v_);
        zzd r1 = zzd.zza.m8348(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzj m9776(IObjectWrapper iObjectWrapper, CastOptions castOptions, zzayz zzayz, Map map) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12306(v_, (Parcelable) castOptions);
        zzew.m12305(v_, (IInterface) zzayz);
        v_.writeMap(map);
        Parcel r0 = m12300(1, v_);
        zzj r1 = zzj.zza.m8390(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzl m9777(CastOptions castOptions, IObjectWrapper iObjectWrapper, zzh zzh) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) castOptions);
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) zzh);
        Parcel r0 = m12300(3, v_);
        zzl r1 = zzl.zza.m8401(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzr m9778(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) iObjectWrapper2);
        zzew.m12305(v_, (IInterface) iObjectWrapper3);
        Parcel r0 = m12300(5, v_);
        zzr r1 = zzr.zza.m8414(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzt m9779(String str, String str2, zzab zzab) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        zzew.m12305(v_, (IInterface) zzab);
        Parcel r0 = m12300(2, v_);
        zzt r1 = zzt.zza.m8428(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzazu m9780(IObjectWrapper iObjectWrapper, zzazw zzazw, int i, int i2, boolean z, long j, int i3, int i4, int i5) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        zzew.m12305(v_, (IInterface) zzazw);
        v_.writeInt(i);
        v_.writeInt(i2);
        zzew.m12307(v_, z);
        v_.writeLong(j);
        v_.writeInt(i3);
        v_.writeInt(i4);
        v_.writeInt(i5);
        Parcel r0 = m12300(6, v_);
        zzazu r1 = zzazu.zza.m9844(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
