package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzkm extends zzeu implements zzkk {
    zzkm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdLoader");
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    public final boolean isLoading() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    public final void zza(zzjj zzjj, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzjj);
        v_.writeInt(i);
        m12298(5, v_);
    }

    public final String zzcp() throws RemoteException {
        Parcel r0 = m12300(4, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    public final void zzd(zzjj zzjj) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzjj);
        m12298(1, v_);
    }
}
