package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzcew extends zzeu implements zzceu {
    zzcew(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10352(zzceo zzceo) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzceo);
        m12299(1, v_);
    }
}
