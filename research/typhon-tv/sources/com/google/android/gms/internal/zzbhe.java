package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;
import com.google.android.gms.common.util.zzq;

public final class zzbhe {

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f8755;

    public zzbhe(Context context) {
        this.f8755 = context;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final PackageInfo m10222(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f8755.getPackageManager().getPackageInfo(str, i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final CharSequence m10223(String str) throws PackageManager.NameNotFoundException {
        return this.f8755.getPackageManager().getApplicationLabel(this.f8755.getPackageManager().getApplicationInfo(str, 0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10224(String str) {
        return this.f8755.checkCallingOrSelfPermission(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10225(String str, String str2) {
        return this.f8755.getPackageManager().checkPermission(str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ApplicationInfo m10226(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f8755.getPackageManager().getApplicationInfo(str, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10227() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return zzbhd.m10221(this.f8755);
        }
        if (!zzq.m9267() || (nameForUid = this.f8755.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.f8755.getPackageManager().isInstantApp(nameForUid);
    }

    @TargetApi(19)
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10228(int i, String str) {
        if (zzq.m9268()) {
            try {
                ((AppOpsManager) this.f8755.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException e) {
                return false;
            }
        } else {
            String[] packagesForUid = this.f8755.getPackageManager().getPackagesForUid(i);
            if (str == null || packagesForUid == null) {
                return false;
            }
            for (String equals : packagesForUid) {
                if (str.equals(equals)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String[] m10229(int i) {
        return this.f8755.getPackageManager().getPackagesForUid(i);
    }
}
