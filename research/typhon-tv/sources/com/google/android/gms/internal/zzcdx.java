package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.location.zze;
import java.util.Collections;
import java.util.List;

public final class zzcdx extends zzbfm {
    public static final Parcelable.Creator<zzcdx> CREATOR = new zzcdy();

    /* renamed from: 靐  reason: contains not printable characters */
    static final zze f9040 = new zze();

    /* renamed from: 龘  reason: contains not printable characters */
    static final List<zzcdv> f9041 = Collections.emptyList();

    /* renamed from: 连任  reason: contains not printable characters */
    private String f9042;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<zzcdv> f9043;

    /* renamed from: 齉  reason: contains not printable characters */
    private zze f9044;

    zzcdx(zze zze, List<zzcdv> list, String str) {
        this.f9044 = zze;
        this.f9043 = list;
        this.f9042 = str;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzcdx)) {
            return false;
        }
        zzcdx zzcdx = (zzcdx) obj;
        return zzbg.m9113(this.f9044, zzcdx.f9044) && zzbg.m9113(this.f9043, zzcdx.f9043) && zzbg.m9113(this.f9042, zzcdx.f9042);
    }

    public final int hashCode() {
        return this.f9044.hashCode();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 1, (Parcelable) this.f9044, i, false);
        zzbfp.m10180(parcel, 2, this.f9043, false);
        zzbfp.m10193(parcel, 3, this.f9042, false);
        zzbfp.m10182(parcel, r0);
    }
}
