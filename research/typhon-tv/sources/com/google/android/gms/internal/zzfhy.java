package com.google.android.gms.internal;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzfhy<K extends Comparable<K>, V> extends AbstractMap<K, V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Map<K, V> f10469;

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile zzfif f10470;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public List<zzfid> f10471;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f10472;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public Map<K, V> f10473;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f10474;

    private zzfhy(int i) {
        this.f10474 = i;
        this.f10471 = Collections.emptyList();
        this.f10473 = Collections.emptyMap();
        this.f10469 = Collections.emptyMap();
    }

    /* synthetic */ zzfhy(int i, zzfhz zzfhz) {
        this(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final SortedMap<K, V> m12679() {
        m12680();
        if (this.f10473.isEmpty() && !(this.f10473 instanceof TreeMap)) {
            this.f10473 = new TreeMap();
            this.f10469 = ((TreeMap) this.f10473).descendingMap();
        }
        return (SortedMap) this.f10473;
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m12680() {
        if (this.f10472) {
            throw new UnsupportedOperationException();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final V m12682(int i) {
        m12680();
        V value = this.f10471.remove(i).getValue();
        if (!this.f10473.isEmpty()) {
            Iterator it2 = m12679().entrySet().iterator();
            this.f10471.add(new zzfid(this, (Map.Entry) it2.next()));
            it2.remove();
        }
        return value;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m12684(K k) {
        int i = 0;
        int size = this.f10471.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.f10471.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i2 = size;
        while (i <= i2) {
            int i3 = (i + i2) / 2;
            int compareTo2 = k.compareTo((Comparable) this.f10471.get(i3).getKey());
            if (compareTo2 < 0) {
                i2 = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i = i3 + 1;
            }
        }
        return -(i + 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <FieldDescriptorType extends zzffs<FieldDescriptorType>> zzfhy<FieldDescriptorType, Object> m12685(int i) {
        return new zzfhz(i);
    }

    public void clear() {
        m12680();
        if (!this.f10471.isEmpty()) {
            this.f10471.clear();
        }
        if (!this.f10473.isEmpty()) {
            this.f10473.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return m12684(comparable) >= 0 || this.f10473.containsKey(comparable);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.f10470 == null) {
            this.f10470 = new zzfif(this, (zzfhz) null);
        }
        return this.f10470;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfhy)) {
            return super.equals(obj);
        }
        zzfhy zzfhy = (zzfhy) obj;
        int size = size();
        if (size != zzfhy.size()) {
            return false;
        }
        int r4 = m12691();
        if (r4 != zzfhy.m12691()) {
            return entrySet().equals(zzfhy.entrySet());
        }
        for (int i = 0; i < r4; i++) {
            if (!m12688(i).equals(zzfhy.m12688(i))) {
                return false;
            }
        }
        if (r4 != size) {
            return this.f10473.equals(zzfhy.f10473);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int r0 = m12684(comparable);
        return r0 >= 0 ? this.f10471.get(r0).getValue() : this.f10473.get(comparable);
    }

    public int hashCode() {
        int r3 = m12691();
        int i = 0;
        for (int i2 = 0; i2 < r3; i2++) {
            i += this.f10471.get(i2).hashCode();
        }
        return this.f10473.size() > 0 ? this.f10473.hashCode() + i : i;
    }

    public V remove(Object obj) {
        m12680();
        Comparable comparable = (Comparable) obj;
        int r0 = m12684(comparable);
        if (r0 >= 0) {
            return m12682(r0);
        }
        if (this.f10473.isEmpty()) {
            return null;
        }
        return this.f10473.remove(comparable);
    }

    public int size() {
        return this.f10471.size() + this.f10473.size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Map.Entry<K, V> m12688(int i) {
        return this.f10471.get(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12689() {
        return this.f10472;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Iterable<Map.Entry<K, V>> m12690() {
        return this.f10473.isEmpty() ? zzfia.m12696() : this.f10473.entrySet();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12691() {
        return this.f10471.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final V put(K k, V v) {
        m12680();
        int r0 = m12684(k);
        if (r0 >= 0) {
            return this.f10471.get(r0).setValue(v);
        }
        m12680();
        if (this.f10471.isEmpty() && !(this.f10471 instanceof ArrayList)) {
            this.f10471 = new ArrayList(this.f10474);
        }
        int i = -(r0 + 1);
        if (i >= this.f10474) {
            return m12679().put(k, v);
        }
        if (this.f10471.size() == this.f10474) {
            zzfid remove = this.f10471.remove(this.f10474 - 1);
            m12679().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.f10471.add(i, new zzfid(this, k, v));
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m12693() {
        if (!this.f10472) {
            this.f10473 = this.f10473.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.f10473);
            this.f10469 = this.f10469.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.f10469);
            this.f10472 = true;
        }
    }
}
