package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;

public final class zzcfo extends zzbfm {
    public static final Parcelable.Creator<zzcfo> CREATOR = new zzcfp();

    /* renamed from: 龘  reason: contains not printable characters */
    static final List<zzcdv> f4591 = Collections.emptyList();

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f4592;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f4593;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f4594;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f4595 = true;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4596;

    /* renamed from: 靐  reason: contains not printable characters */
    private LocationRequest f4597;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f4598;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<zzcdv> f4599;

    zzcfo(LocationRequest locationRequest, List<zzcdv> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.f4597 = locationRequest;
        this.f4599 = list;
        this.f4598 = str;
        this.f4596 = z;
        this.f4592 = z2;
        this.f4593 = z3;
        this.f4594 = str2;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzcfo m5230(LocationRequest locationRequest) {
        return new zzcfo(locationRequest, f4591, (String) null, false, false, false, (String) null);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzcfo)) {
            return false;
        }
        zzcfo zzcfo = (zzcfo) obj;
        return zzbg.m9113(this.f4597, zzcfo.f4597) && zzbg.m9113(this.f4599, zzcfo.f4599) && zzbg.m9113(this.f4598, zzcfo.f4598) && this.f4596 == zzcfo.f4596 && this.f4592 == zzcfo.f4592 && this.f4593 == zzcfo.f4593 && zzbg.m9113(this.f4594, zzcfo.f4594);
    }

    public final int hashCode() {
        return this.f4597.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f4597.toString());
        if (this.f4598 != null) {
            sb.append(" tag=").append(this.f4598);
        }
        if (this.f4594 != null) {
            sb.append(" moduleId=").append(this.f4594);
        }
        sb.append(" hideAppOps=").append(this.f4596);
        sb.append(" clients=").append(this.f4599);
        sb.append(" forceCoarseLocation=").append(this.f4592);
        if (this.f4593) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 1, (Parcelable) this.f4597, i, false);
        zzbfp.m10180(parcel, 5, this.f4599, false);
        zzbfp.m10193(parcel, 6, this.f4598, false);
        zzbfp.m10195(parcel, 7, this.f4596);
        zzbfp.m10195(parcel, 8, this.f4592);
        zzbfp.m10195(parcel, 9, this.f4593);
        zzbfp.m10193(parcel, 10, this.f4594, false);
        zzbfp.m10182(parcel, r0);
    }
}
