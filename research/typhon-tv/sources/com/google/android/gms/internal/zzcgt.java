package com.google.android.gms.internal;

import android.os.Looper;

final class zzcgt implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcgs f9177;

    zzcgt(zzcgs zzcgs) {
        this.f9177 = zzcgs;
    }

    public final void run() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            this.f9177.f9176.m11018().m10986((Runnable) this);
            return;
        }
        boolean r0 = this.f9177.m10623();
        long unused = this.f9177.f9174 = 0;
        if (r0 && this.f9177.f9173) {
            this.f9177.m10625();
        }
    }
}
