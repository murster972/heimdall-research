package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzadl extends zzev implements zzadk {
    public zzadl() {
        attachInterface(this, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzadk m9482(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
        return queryLocalInterface instanceof zzadk ? (zzadk) queryLocalInterface : new zzadm(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zzadp zzadr;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m9479((zzadv) zzew.m12304(parcel, zzadv.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                m9476();
                parcel2.writeNoException();
                break;
            case 3:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    zzadr = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
                    zzadr = queryLocalInterface instanceof zzadp ? (zzadp) queryLocalInterface : new zzadr(readStrongBinder);
                }
                m9478(zzadr);
                parcel2.writeNoException();
                break;
            case 5:
                boolean r0 = m9472();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r0);
                break;
            case 6:
                m9474();
                parcel2.writeNoException();
                break;
            case 7:
                m9473();
                parcel2.writeNoException();
                break;
            case 8:
                m9470();
                parcel2.writeNoException();
                break;
            case 9:
                m9477(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 10:
                m9471(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 11:
                m9475(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 12:
                String r02 = m9469();
                parcel2.writeNoException();
                parcel2.writeString(r02);
                break;
            case 13:
                m9480(parcel.readString());
                parcel2.writeNoException();
                break;
            case 34:
                m9481(zzew.m12308(parcel));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
