package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.measurement.AppMeasurement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class zzdvs {
    /* renamed from: 靐  reason: contains not printable characters */
    private static int m12251(AppMeasurement appMeasurement, String str) {
        try {
            Method declaredMethod = AppMeasurement.class.getDeclaredMethod("getMaxUserProperties", new Class[]{String.class});
            declaredMethod.setAccessible(true);
            return ((Integer) declaredMethod.invoke(appMeasurement, new Object[]{str})).intValue();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return 20;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m12252(Object obj) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        return (String) Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty").getField("mValue").get(obj);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static List<Object> m12253(List<byte[]> list, List<Object> list2) {
        boolean z;
        ArrayList arrayList = new ArrayList();
        for (Object next : list2) {
            try {
                Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
                String r5 = m12262(next);
                String r6 = m12252(next);
                Iterator<byte[]> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z = true;
                        break;
                    }
                    zzfku r0 = m12258(it2.next());
                    if (r0 != null) {
                        if (r0.f10631.equals(r5) && r0.f10628.equals(r6)) {
                            z = false;
                            break;
                        }
                    } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                        Log.v("FirebaseAbtUtil", "Couldn't deserialize the payload; skipping.");
                    }
                }
                if (z) {
                    arrayList.add(next);
                }
            } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e) {
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            }
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m12254(Context context) {
        if (m12259(context) != null) {
            try {
                Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
                return true;
            } catch (ClassNotFoundException e) {
                if (!Log.isLoggable("FirebaseAbtUtil", 2)) {
                    return false;
                }
                Log.v("FirebaseAbtUtil", "Firebase Analytics library is missing support for abt. Please update to a more recent version.");
                return false;
            }
        } else if (!Log.isLoggable("FirebaseAbtUtil", 2)) {
            return false;
        } else {
            Log.v("FirebaseAbtUtil", "Firebase Analytics not available");
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m12255(long j, List<byte[]> list) {
        zzfku r0;
        if (list != null) {
            for (byte[] next : list) {
                if (!(next == null || (r0 = m12258(next)) == null || r0.f10630 <= j)) {
                    j = r0.f10630;
                }
            }
        }
        return j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Bundle m12256(zzfku zzfku) {
        return m12257(zzfku.f10631, zzfku.f10628);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Bundle m12257(String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString(str, str2);
        return bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzfku m12258(byte[] bArr) {
        try {
            return zzfku.m12923(bArr);
        } catch (zzfjr e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static AppMeasurement m12259(Context context) {
        try {
            return AppMeasurement.getInstance(context);
        } catch (NoClassDefFoundError e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object m12260(zzfku zzfku, String str, zzdvr zzdvr) {
        Object obj;
        String str2 = null;
        try {
            Class<?> cls = Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
            Bundle r4 = m12256(zzfku);
            obj = cls.getConstructor(new Class[0]).newInstance(new Object[0]);
            try {
                cls.getField("mOrigin").set(obj, str);
                cls.getField("mCreationTimestamp").set(obj, Long.valueOf(zzfku.f10630));
                cls.getField("mName").set(obj, zzfku.f10631);
                cls.getField("mValue").set(obj, zzfku.f10628);
                if (!TextUtils.isEmpty(zzfku.f10629)) {
                    str2 = zzfku.f10629;
                }
                cls.getField("mTriggerEventName").set(obj, str2);
                cls.getField("mTimedOutEventName").set(obj, !TextUtils.isEmpty(zzfku.f10625) ? zzfku.f10625 : zzdvr.m12249());
                cls.getField("mTimedOutEventParams").set(obj, r4);
                cls.getField("mTriggerTimeout").set(obj, Long.valueOf(zzfku.f10627));
                cls.getField("mTriggeredEventName").set(obj, !TextUtils.isEmpty(zzfku.f10621) ? zzfku.f10621 : zzdvr.m12247());
                cls.getField("mTriggeredEventParams").set(obj, r4);
                cls.getField("mTimeToLive").set(obj, Long.valueOf(zzfku.f10619));
                cls.getField("mExpiredEventName").set(obj, !TextUtils.isEmpty(zzfku.f10626) ? zzfku.f10626 : zzdvr.m12248());
                cls.getField("mExpiredEventParams").set(obj, r4);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
                e = e;
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
                return obj;
            }
        } catch (ClassNotFoundException e2) {
            e = e2;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        } catch (NoSuchMethodException e3) {
            e = e3;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        } catch (IllegalAccessException e4) {
            e = e4;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        } catch (InvocationTargetException e5) {
            e = e5;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        } catch (NoSuchFieldException e6) {
            e = e6;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        } catch (InstantiationException e7) {
            e = e7;
            obj = null;
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            return obj;
        }
        return obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m12261(zzfku zzfku, zzdvr zzdvr) {
        return (zzfku == null || TextUtils.isEmpty(zzfku.f10624)) ? zzdvr.m12246() : zzfku.f10624;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m12262(Object obj) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        return (String) Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty").getField("mName").get(obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<Object> m12263(AppMeasurement appMeasurement, String str) {
        List<Object> list;
        ArrayList arrayList = new ArrayList();
        try {
            Method declaredMethod = AppMeasurement.class.getDeclaredMethod("getConditionalUserProperties", new Class[]{String.class, String.class});
            declaredMethod.setAccessible(true);
            list = (List) declaredMethod.invoke(appMeasurement, new Object[]{str, ""});
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            list = arrayList;
        }
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str).length() + 55).append("Number of currently set _Es for origin: ").append(str).append(" is ").append(list.size()).toString());
        }
        return list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<zzfku> m12264(List<byte[]> list, List<Object> list2) {
        boolean z;
        ArrayList arrayList = new ArrayList();
        for (byte[] r0 : list) {
            zzfku r4 = m12258(r0);
            if (r4 != null) {
                Iterator<Object> it2 = list2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z = false;
                        break;
                    }
                    Object next = it2.next();
                    try {
                        Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
                        m12262(next);
                        String r6 = m12252(next);
                        if (r4.f10631.equals(m12262(next)) && r4.f10628.equals(r6)) {
                            z = true;
                            break;
                        }
                    } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e) {
                        Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
                    }
                }
                if (!z) {
                    arrayList.add(r4);
                }
            } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                Log.v("FirebaseAbtUtil", "Couldn't deserialize the payload; skipping.");
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12265(Context context, String str, String str2, String str3, String str4) {
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            String valueOf = String.valueOf(str);
            Log.v("FirebaseAbtUtil", valueOf.length() != 0 ? "_CE(experimentId) called by ".concat(valueOf) : new String("_CE(experimentId) called by "));
        }
        if (m12254(context)) {
            AppMeasurement r0 = m12259(context);
            try {
                Method declaredMethod = AppMeasurement.class.getDeclaredMethod("clearConditionalUserProperty", new Class[]{String.class, String.class, Bundle.class});
                declaredMethod.setAccessible(true);
                if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                    Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str2).length() + 17 + String.valueOf(str3).length()).append("Clearing _E: [").append(str2).append(", ").append(str3).append("]").toString());
                }
                declaredMethod.invoke(r0, new Object[]{str2, str4, m12257(str2, str3)});
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12266(Context context, String str, List<byte[]> list, int i, zzdvr zzdvr, long j) {
        if (Log.isLoggable("FirebaseAbtUtil", 2)) {
            String valueOf = String.valueOf(str);
            Log.v("FirebaseAbtUtil", valueOf.length() != 0 ? "_UE called by ".concat(valueOf) : new String("_UE called by "));
        }
        if (m12254(context)) {
            AppMeasurement r2 = m12259(context);
            try {
                Class.forName("com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty");
                List<Object> r3 = m12263(r2, str);
                new ArrayList();
                List<zzfku> r4 = m12264(list, r3);
                for (Object next : m12253(list, r3)) {
                    String r6 = m12262(next);
                    String r5 = m12252(next);
                    if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(r6).length() + 30).append("Clearing _E as part of _UE: [").append(r6).append("]").toString());
                    }
                    m12265(context, str, r6, r5, m12261((zzfku) null, zzdvr));
                }
                for (zzfku next2 : r4) {
                    if (next2.f10630 > j) {
                        String str2 = next2.f10631;
                        String str3 = next2.f10628;
                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str2).length() + 106 + String.valueOf(str3).length()).append("Setting _E as part of _UE: [").append(str2).append(", ").append(str3).append(", ").append(next2.f10630).append("], latestOriginKnownExpStartTime: ").append(j).toString());
                        m12267(r2, context, str, next2, zzdvr, 1);
                    } else if (Log.isLoggable("FirebaseAbtUtil", 2)) {
                        String str4 = next2.f10631;
                        String str5 = next2.f10628;
                        Log.v("FirebaseAbtUtil", new StringBuilder(String.valueOf(str4).length() + 118 + String.valueOf(str5).length()).append("Not setting _E, due to lastUpdateTime: [").append(str4).append(", ").append(str5).append(", ").append(next2.f10630).append("], latestOriginKnownExpStartTime: ").append(j).toString());
                    }
                }
            } catch (ClassNotFoundException | NoSuchFieldException e) {
                e = e;
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            } catch (IllegalAccessException e2) {
                e = e2;
                Log.e("FirebaseAbtUtil", "Could not complete the operation due to an internal error.", e);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0127, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0183, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01da, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01da A[Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }, ExcHandler: NoSuchFieldException (e java.lang.NoSuchFieldException), Splitter:B:4:0x004a] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m12267(com.google.android.gms.measurement.AppMeasurement r7, android.content.Context r8, java.lang.String r9, com.google.android.gms.internal.zzfku r10, com.google.android.gms.internal.zzdvr r11, int r12) {
        /*
            r1 = 1
            r2 = 2
            java.lang.String r0 = "FirebaseAbtUtil"
            boolean r0 = android.util.Log.isLoggable(r0, r2)
            if (r0 == 0) goto L_0x0047
            java.lang.String r0 = "FirebaseAbtUtil"
            java.lang.String r2 = r10.f10631
            java.lang.String r3 = r10.f10628
            java.lang.String r4 = java.lang.String.valueOf(r2)
            int r4 = r4.length()
            int r4 = r4 + 7
            java.lang.String r5 = java.lang.String.valueOf(r3)
            int r5 = r5.length()
            int r4 = r4 + r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "_SEI: "
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r0, r2)
        L_0x0047:
            java.lang.String r0 = "com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty"
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.util.List r2 = m12263((com.google.android.gms.measurement.AppMeasurement) r7, (java.lang.String) r9)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r0 = m12251((com.google.android.gms.measurement.AppMeasurement) r7, (java.lang.String) r9)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.util.List r3 = m12263((com.google.android.gms.measurement.AppMeasurement) r7, (java.lang.String) r9)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3.size()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r3 < r0) goto L_0x00b0
            int r0 = r10.f10623     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r0 == 0) goto L_0x0132
            int r0 = r10.f10623     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
        L_0x0065:
            if (r0 != r1) goto L_0x0135
            r0 = 0
            java.lang.Object r0 = r2.get(r0)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r1 = m12262((java.lang.Object) r0)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r0 = m12252((java.lang.Object) r0)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = "FirebaseAbtUtil"
            r4 = 2
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r3 == 0) goto L_0x00a9
            java.lang.String r3 = "FirebaseAbtUtil"
            java.lang.String r4 = java.lang.String.valueOf(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4 + 38
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            r5.<init>(r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r4 = "Clearing _E due to overflow policy: ["
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r5 = "]"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r4 = r4.toString()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            android.util.Log.v(r3, r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
        L_0x00a9:
            java.lang.String r3 = m12261((com.google.android.gms.internal.zzfku) r10, (com.google.android.gms.internal.zzdvr) r11)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            m12265(r8, r9, r1, r0, r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
        L_0x00b0:
            java.util.Iterator r0 = r2.iterator()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
        L_0x00b4:
            boolean r1 = r0.hasNext()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r1 == 0) goto L_0x0185
            java.lang.Object r1 = r0.next()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r2 = m12262((java.lang.Object) r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r1 = m12252((java.lang.Object) r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = r10.f10631     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            boolean r3 = r2.equals(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r3 == 0) goto L_0x00b4
            java.lang.String r3 = r10.f10628     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            boolean r3 = r1.equals(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r3 != 0) goto L_0x00b4
            java.lang.String r3 = "FirebaseAbtUtil"
            r4 = 2
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r3 == 0) goto L_0x00b4
            java.lang.String r3 = "FirebaseAbtUtil"
            java.lang.String r4 = java.lang.String.valueOf(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4 + 77
            java.lang.String r5 = java.lang.String.valueOf(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r5 = r5.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4 + r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            r5.<init>(r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r4 = "Clearing _E, as only one _V of the same _E can be set atany given time: ["
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r5 = ", "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r5 = "]."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r4 = r4.toString()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            android.util.Log.v(r3, r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = m12261((com.google.android.gms.internal.zzfku) r10, (com.google.android.gms.internal.zzdvr) r11)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            m12265(r8, r9, r2, r1, r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            goto L_0x00b4
        L_0x0127:
            r0 = move-exception
        L_0x0128:
            java.lang.String r1 = "FirebaseAbtUtil"
            java.lang.String r2 = "Could not complete the operation due to an internal error."
            android.util.Log.e(r1, r2, r0)
        L_0x0131:
            return
        L_0x0132:
            r0 = r1
            goto L_0x0065
        L_0x0135:
            java.lang.String r0 = "FirebaseAbtUtil"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r0 == 0) goto L_0x0131
            java.lang.String r0 = "FirebaseAbtUtil"
            java.lang.String r1 = r10.f10631     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r2 = r10.f10628     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = java.lang.String.valueOf(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3 + 44
            java.lang.String r4 = java.lang.String.valueOf(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            r4.<init>(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = "_E won't be set due to overflow policy. ["
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = ", "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r2 = "]"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r1 = r1.toString()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            android.util.Log.v(r0, r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            goto L_0x0131
        L_0x0183:
            r0 = move-exception
            goto L_0x0128
        L_0x0185:
            java.lang.Object r1 = m12260(r10, r9, r11)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r1 != 0) goto L_0x01dd
            java.lang.String r0 = "FirebaseAbtUtil"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r0 == 0) goto L_0x0131
            java.lang.String r0 = "FirebaseAbtUtil"
            java.lang.String r1 = r10.f10631     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r2 = r10.f10628     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = java.lang.String.valueOf(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3 + 42
            java.lang.String r4 = java.lang.String.valueOf(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r4 = r4.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            r4.<init>(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = "Could not create _CUP for: ["
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = ", "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r2 = "]. Skipping."
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r1 = r1.toString()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            android.util.Log.v(r0, r1)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            goto L_0x0131
        L_0x01da:
            r0 = move-exception
            goto L_0x0128
        L_0x01dd:
            java.lang.String r0 = "FirebaseAbtUtil"
            r2 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            if (r0 == 0) goto L_0x0240
            java.lang.String r0 = "FirebaseAbtUtil"
            java.lang.String r2 = r10.f10631     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = r10.f10628     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r4 = r10.f10629     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r5 = java.lang.String.valueOf(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r5 = r5.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r5 = r5 + 27
            java.lang.String r6 = java.lang.String.valueOf(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r6 = r6.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r5 = r5 + r6
            java.lang.String r6 = java.lang.String.valueOf(r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r6 = r6.length()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            int r5 = r5 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            r6.<init>(r5)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r5 = "Setting _CUP for _E: ["
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r5 = ", "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = ", "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r3 = "]"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            android.util.Log.v(r0, r2)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
        L_0x0240:
            java.lang.String r0 = "com.google.android.gms.measurement.AppMeasurement$ConditionalUserProperty"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            java.lang.Class<com.google.android.gms.measurement.AppMeasurement> r2 = com.google.android.gms.measurement.AppMeasurement.class
            java.lang.String r3 = "setConditionalUserProperty"
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            r5 = 0
            r4[r5] = r0     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            java.lang.reflect.Method r2 = r2.getDeclaredMethod(r3, r4)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            r0 = 1
            r2.setAccessible(r0)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            java.lang.String r0 = r10.f10620     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            if (r0 != 0) goto L_0x0282
            java.lang.String r0 = r10.f10620     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
        L_0x0264:
            android.os.Bundle r3 = m12256((com.google.android.gms.internal.zzfku) r10)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            r7.logEventInternal(r9, r0, r3)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            r3 = 0
            r0[r3] = r1     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            r2.invoke(r7, r0)     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            goto L_0x0131
        L_0x0276:
            r0 = move-exception
        L_0x0277:
            java.lang.String r1 = "FirebaseAbtUtil"
            java.lang.String r2 = "Could not complete the operation due to an internal error."
            android.util.Log.e(r1, r2, r0)     // Catch:{ ClassNotFoundException -> 0x0127, IllegalAccessException -> 0x0183, NoSuchFieldException -> 0x01da }
            goto L_0x0131
        L_0x0282:
            java.lang.String r0 = r11.m12250()     // Catch:{ ClassNotFoundException -> 0x0276, NoSuchMethodException -> 0x0289, IllegalAccessException -> 0x0287, InvocationTargetException -> 0x028b, NoSuchFieldException -> 0x01da }
            goto L_0x0264
        L_0x0287:
            r0 = move-exception
            goto L_0x0277
        L_0x0289:
            r0 = move-exception
            goto L_0x0277
        L_0x028b:
            r0 = move-exception
            goto L_0x0277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdvs.m12267(com.google.android.gms.measurement.AppMeasurement, android.content.Context, java.lang.String, com.google.android.gms.internal.zzfku, com.google.android.gms.internal.zzdvr, int):void");
    }
}
