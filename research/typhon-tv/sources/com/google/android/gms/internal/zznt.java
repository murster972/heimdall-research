package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;

@zzzv
public final class zznt {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zznu f5186;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, zzns> f5187 = new HashMap();

    public zznt(zznu zznu) {
        this.f5186 = zznu;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zznu m5619() {
        return this.f5186;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5620(String str, zzns zzns) {
        this.f5187.put(str, zzns);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5621(String str, String str2, long j) {
        zznu zznu = this.f5186;
        zzns zzns = this.f5187.get(str2);
        String[] strArr = {str};
        if (!(zznu == null || zzns == null)) {
            zznu.m5630(zzns, j, strArr);
        }
        Map<String, zzns> map = this.f5187;
        zznu zznu2 = this.f5186;
        map.put(str, zznu2 == null ? null : zznu2.m5626(j));
    }
}
