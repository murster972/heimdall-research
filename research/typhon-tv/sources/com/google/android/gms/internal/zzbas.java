package com.google.android.gms.internal;

import android.widget.SeekBar;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbas extends UIController implements RemoteMediaClient.ProgressListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f8588;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f8589 = true;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SeekBar f8590;

    public zzbas(SeekBar seekBar, long j) {
        this.f8590 = seekBar;
        this.f8588 = j;
        this.f8590.setEnabled(false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9918() {
        if (m8226() != null) {
            m8226().m4166((RemoteMediaClient.ProgressListener) this);
        }
        this.f8590.setMax(1);
        this.f8590.setProgress(0);
        this.f8590.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9919() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9920(long j, long j2) {
        if (this.f8589) {
            this.f8590.setMax((int) j2);
            this.f8590.setProgress((int) j);
            this.f8590.setEnabled(true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9921(CastSession castSession) {
        super.m8227(castSession);
        RemoteMediaClient r0 = m8226();
        if (r0 != null) {
            r0.m4168((RemoteMediaClient.ProgressListener) this, this.f8588);
            if (r0.m4143()) {
                this.f8590.setMax((int) r0.m4135());
                this.f8590.setProgress((int) r0.m4148());
                this.f8590.setEnabled(true);
                return;
            }
        }
        this.f8590.setMax(1);
        this.f8590.setProgress(0);
        this.f8590.setEnabled(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9922(boolean z) {
        this.f8589 = z;
    }
}
