package com.google.android.gms.internal;

import android.view.View;
import java.lang.ref.WeakReference;

public final class zzfv implements zzhd {

    /* renamed from: 龘  reason: contains not printable characters */
    private WeakReference<zzos> f10635;

    public zzfv(zzos zzos) {
        this.f10635 = new WeakReference<>(zzos);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12951() {
        return this.f10635.get() == null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzhd m12952() {
        return new zzfx((zzos) this.f10635.get());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m12953() {
        zzos zzos = (zzos) this.f10635.get();
        if (zzos != null) {
            return zzos.m13186();
        }
        return null;
    }
}
