package com.google.android.gms.internal;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzvu extends zzvn {

    /* renamed from: 龘  reason: contains not printable characters */
    private final NativeContentAdMapper f5489;

    public zzvu(NativeContentAdMapper nativeContentAdMapper) {
        this.f5489 = nativeContentAdMapper;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m5938() {
        return this.f5489.getAdvertiser();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m5939() {
        this.f5489.recordImpression();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m5940() {
        return this.f5489.getOverrideImpressionRecording();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzpm m5941() {
        return null;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final IObjectWrapper m5942() {
        View zzul = this.f5489.zzul();
        if (zzul == null) {
            return null;
        }
        return zzn.m9306(zzul);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final zzll m5943() {
        if (this.f5489.getVideoController() != null) {
            return this.f5489.getVideoController().zzbj();
        }
        return null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m5944() {
        return this.f5489.getOverrideClickHandling();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final Bundle m5945() {
        return this.f5489.getExtras();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final IObjectWrapper m5946() {
        View adChoicesContent = this.f5489.getAdChoicesContent();
        if (adChoicesContent == null) {
            return null;
        }
        return zzn.m9306(adChoicesContent);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m5947() {
        return this.f5489.getCallToAction();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m5948() {
        List<NativeAd.Image> images = this.f5489.getImages();
        if (images == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (NativeAd.Image next : images) {
            arrayList.add(new zzoi(next.getDrawable(), next.getUri(), next.getScale()));
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5949(IObjectWrapper iObjectWrapper) {
        this.f5489.trackView((View) zzn.m9307(iObjectWrapper));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzpq m5950() {
        NativeAd.Image logo = this.f5489.getLogo();
        if (logo != null) {
            return new zzoi(logo.getDrawable(), logo.getUri(), logo.getScale());
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m5951() {
        return this.f5489.getBody();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5952(IObjectWrapper iObjectWrapper) {
        this.f5489.untrackView((View) zzn.m9307(iObjectWrapper));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5953() {
        return this.f5489.getHeadline();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5954(IObjectWrapper iObjectWrapper) {
        this.f5489.handleClick((View) zzn.m9307(iObjectWrapper));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final IObjectWrapper m5955() {
        return null;
    }
}
