package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;

final class zzjv extends zzjr.zza<zzkn> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f10769;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzjr f10770;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzux f10771;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f10772;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjv(zzjr zzjr, Context context, String str, zzux zzux) {
        super();
        this.f10770 = zzjr;
        this.f10772 = context;
        this.f10769 = str;
        this.f10771 = zzux;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13041() throws RemoteException {
        zzkn r0 = this.f10770.f4808.m5445(this.f10772, this.f10769, this.f10771);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10772, "native_ad");
        return new zzmc();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13042(zzla zzla) throws RemoteException {
        return zzla.createAdLoaderBuilder(zzn.m9306(this.f10772), this.f10769, this.f10771, 11910000);
    }
}
