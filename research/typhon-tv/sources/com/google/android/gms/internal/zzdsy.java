package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsy extends zzffu<zzdsy, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdsy f10080;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsy> f10081;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10082;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzfes f10083 = zzfes.zzpfg;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f10084 = "";

    public static final class zza extends zzffu.zza<zzdsy, zza> implements zzfhg {
        private zza() {
            super(zzdsy.f10080);
        }

        /* synthetic */ zza(zzdsz zzdsz) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12021(zzb zzb) {
            m12541();
            ((zzdsy) this.f10398).m12009(zzb);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12022(zzfes zzfes) {
            m12541();
            ((zzdsy) this.f10398).m12013(zzfes);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m12023(String str) {
            m12541();
            ((zzdsy) this.f10398).m12014(str);
            return this;
        }
    }

    public enum zzb implements zzfga {
        UNKNOWN_KEYMATERIAL(0),
        SYMMETRIC(1),
        ASYMMETRIC_PRIVATE(2),
        ASYMMETRIC_PUBLIC(3),
        REMOTE(4),
        UNRECOGNIZED(-1);
        

        /* renamed from: 龘  reason: contains not printable characters */
        private static final zzfgb<zzb> f10086 = null;
        private final int value;

        static {
            f10086 = new zzdta();
        }

        private zzb(int i) {
            this.value = i;
        }

        public static zzb zzft(int i) {
            switch (i) {
                case 0:
                    return UNKNOWN_KEYMATERIAL;
                case 1:
                    return SYMMETRIC;
                case 2:
                    return ASYMMETRIC_PRIVATE;
                case 3:
                    return ASYMMETRIC_PUBLIC;
                case 4:
                    return REMOTE;
                default:
                    return null;
            }
        }

        public final int zzhq() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
    }

    static {
        zzdsy zzdsy = new zzdsy();
        f10080 = zzdsy;
        zzdsy.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsy.f10394.m12718();
    }

    private zzdsy() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzdsy m12006() {
        return f10080;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zza m12008() {
        zzdsy zzdsy = f10080;
        zzffu.zza zza2 = (zzffu.zza) zzdsy.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdsy);
        return (zza) zza2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12009(zzb zzb2) {
        if (zzb2 == null) {
            throw new NullPointerException();
        }
        this.f10082 = zzb2.zzhq();
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12013(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f10083 = zzfes;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12014(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f10084 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfes m12015() {
        return this.f10083;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12016() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.f10084.isEmpty()) {
            i2 = zzffg.m5248(1, this.f10084) + 0;
        }
        if (!this.f10083.isEmpty()) {
            i2 += zzffg.m5261(2, this.f10083);
        }
        if (this.f10082 != zzb.UNKNOWN_KEYMATERIAL.zzhq()) {
            i2 += zzffg.m5236(3, this.f10082);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzb m12017() {
        zzb zzft = zzb.zzft(this.f10082);
        return zzft == null ? zzb.UNRECOGNIZED : zzft;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 181 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m12018(int r7, java.lang.Object r8, java.lang.Object r9) {
        /*
            r6 = this;
            r0 = 0
            r2 = 0
            r1 = 1
            int[] r3 = com.google.android.gms.internal.zzdsz.f10087
            int r4 = r7 + -1
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0018;
                case 3: goto L_0x001b;
                case 4: goto L_0x001d;
                case 5: goto L_0x0023;
                case 6: goto L_0x007b;
                case 7: goto L_0x00cf;
                case 8: goto L_0x00d3;
                case 9: goto L_0x00ef;
                case 10: goto L_0x00f5;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>()
            throw r0
        L_0x0012:
            com.google.android.gms.internal.zzdsy r6 = new com.google.android.gms.internal.zzdsy
            r6.<init>()
        L_0x0017:
            return r6
        L_0x0018:
            com.google.android.gms.internal.zzdsy r6 = f10080
            goto L_0x0017
        L_0x001b:
            r6 = r0
            goto L_0x0017
        L_0x001d:
            com.google.android.gms.internal.zzdsy$zza r6 = new com.google.android.gms.internal.zzdsy$zza
            r6.<init>(r0)
            goto L_0x0017
        L_0x0023:
            com.google.android.gms.internal.zzffu$zzh r8 = (com.google.android.gms.internal.zzffu.zzh) r8
            com.google.android.gms.internal.zzdsy r9 = (com.google.android.gms.internal.zzdsy) r9
            java.lang.String r0 = r6.f10084
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x006f
            r0 = r1
        L_0x0030:
            java.lang.String r4 = r6.f10084
            java.lang.String r3 = r9.f10084
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x0071
            r3 = r1
        L_0x003b:
            java.lang.String r5 = r9.f10084
            java.lang.String r0 = r8.m12574((boolean) r0, (java.lang.String) r4, (boolean) r3, (java.lang.String) r5)
            r6.f10084 = r0
            com.google.android.gms.internal.zzfes r0 = r6.f10083
            com.google.android.gms.internal.zzfes r3 = com.google.android.gms.internal.zzfes.zzpfg
            if (r0 == r3) goto L_0x0073
            r0 = r1
        L_0x004a:
            com.google.android.gms.internal.zzfes r4 = r6.f10083
            com.google.android.gms.internal.zzfes r3 = r9.f10083
            com.google.android.gms.internal.zzfes r5 = com.google.android.gms.internal.zzfes.zzpfg
            if (r3 == r5) goto L_0x0075
            r3 = r1
        L_0x0053:
            com.google.android.gms.internal.zzfes r5 = r9.f10083
            com.google.android.gms.internal.zzfes r0 = r8.m12570((boolean) r0, (com.google.android.gms.internal.zzfes) r4, (boolean) r3, (com.google.android.gms.internal.zzfes) r5)
            r6.f10083 = r0
            int r0 = r6.f10082
            if (r0 == 0) goto L_0x0077
            r0 = r1
        L_0x0060:
            int r3 = r6.f10082
            int r4 = r9.f10082
            if (r4 == 0) goto L_0x0079
        L_0x0066:
            int r2 = r9.f10082
            int r0 = r8.m12569((boolean) r0, (int) r3, (boolean) r1, (int) r2)
            r6.f10082 = r0
            goto L_0x0017
        L_0x006f:
            r0 = r2
            goto L_0x0030
        L_0x0071:
            r3 = r2
            goto L_0x003b
        L_0x0073:
            r0 = r2
            goto L_0x004a
        L_0x0075:
            r3 = r2
            goto L_0x0053
        L_0x0077:
            r0 = r2
            goto L_0x0060
        L_0x0079:
            r1 = r2
            goto L_0x0066
        L_0x007b:
            com.google.android.gms.internal.zzffb r8 = (com.google.android.gms.internal.zzffb) r8
            com.google.android.gms.internal.zzffm r9 = (com.google.android.gms.internal.zzffm) r9
            if (r9 != 0) goto L_0x0088
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0087:
            r2 = r1
        L_0x0088:
            if (r2 != 0) goto L_0x00cf
            int r0 = r8.m12415()     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            switch(r0) {
                case 0: goto L_0x0087;
                case 10: goto L_0x0099;
                case 18: goto L_0x00ad;
                case 24: goto L_0x00c8;
                default: goto L_0x0091;
            }     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
        L_0x0091:
            boolean r0 = r6.m12537((int) r0, (com.google.android.gms.internal.zzffb) r8)     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            if (r0 != 0) goto L_0x0088
            r2 = r1
            goto L_0x0088
        L_0x0099:
            java.lang.String r0 = r8.m12400()     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            r6.f10084 = r0     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            goto L_0x0088
        L_0x00a0:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.zzfge r0 = r0.zzi(r6)     // Catch:{ all -> 0x00ab }
            r1.<init>(r0)     // Catch:{ all -> 0x00ab }
            throw r1     // Catch:{ all -> 0x00ab }
        L_0x00ab:
            r0 = move-exception
            throw r0
        L_0x00ad:
            com.google.android.gms.internal.zzfes r0 = r8.m12401()     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            r6.f10083 = r0     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            goto L_0x0088
        L_0x00b4:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.zzfge r2 = new com.google.android.gms.internal.zzfge     // Catch:{ all -> 0x00ab }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00ab }
            r2.<init>(r0)     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.zzfge r0 = r2.zzi(r6)     // Catch:{ all -> 0x00ab }
            r1.<init>(r0)     // Catch:{ all -> 0x00ab }
            throw r1     // Catch:{ all -> 0x00ab }
        L_0x00c8:
            int r0 = r8.m12405()     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            r6.f10082 = r0     // Catch:{ zzfge -> 0x00a0, IOException -> 0x00b4 }
            goto L_0x0088
        L_0x00cf:
            com.google.android.gms.internal.zzdsy r6 = f10080
            goto L_0x0017
        L_0x00d3:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsy> r0 = f10081
            if (r0 != 0) goto L_0x00e8
            java.lang.Class<com.google.android.gms.internal.zzdsy> r1 = com.google.android.gms.internal.zzdsy.class
            monitor-enter(r1)
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsy> r0 = f10081     // Catch:{ all -> 0x00ec }
            if (r0 != 0) goto L_0x00e7
            com.google.android.gms.internal.zzffu$zzb r0 = new com.google.android.gms.internal.zzffu$zzb     // Catch:{ all -> 0x00ec }
            com.google.android.gms.internal.zzdsy r2 = f10080     // Catch:{ all -> 0x00ec }
            r0.<init>(r2)     // Catch:{ all -> 0x00ec }
            f10081 = r0     // Catch:{ all -> 0x00ec }
        L_0x00e7:
            monitor-exit(r1)     // Catch:{ all -> 0x00ec }
        L_0x00e8:
            com.google.android.gms.internal.zzfhk<com.google.android.gms.internal.zzdsy> r6 = f10081
            goto L_0x0017
        L_0x00ec:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ec }
            throw r0
        L_0x00ef:
            java.lang.Byte r6 = java.lang.Byte.valueOf(r1)
            goto L_0x0017
        L_0x00f5:
            r6 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdsy.m12018(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m12019() {
        return this.f10084;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12020(zzffg zzffg) throws IOException {
        if (!this.f10084.isEmpty()) {
            zzffg.m5290(1, this.f10084);
        }
        if (!this.f10083.isEmpty()) {
            zzffg.m5288(2, this.f10083);
        }
        if (this.f10082 != zzb.UNKNOWN_KEYMATERIAL.zzhq()) {
            zzffg.m5274(3, this.f10082);
        }
        this.f10394.m12719(zzffg);
    }
}
