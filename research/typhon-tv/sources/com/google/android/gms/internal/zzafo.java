package com.google.android.gms.internal;

import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

@zzzv
public final class zzafo {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final List<String> f4088;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public final zzis f4089;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f4090;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public final boolean f4091;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final long f4092;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public final boolean f4093;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean f4094;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private long f4095;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final zzuh f4096;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private zzaaz f4097;

    /* renamed from: ˆ  reason: contains not printable characters */
    public final zzjn f4098;

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f4099;

    /* renamed from: ˉ  reason: contains not printable characters */
    public final zzaeq f4100;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final zzui f4101;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final zzuk f4102;

    /* renamed from: ˎ  reason: contains not printable characters */
    public final String f4103;

    /* renamed from: ˏ  reason: contains not printable characters */
    public final List<String> f4104;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String f4105;

    /* renamed from: י  reason: contains not printable characters */
    public final List<String> f4106;

    /* renamed from: ـ  reason: contains not printable characters */
    public final long f4107;

    /* renamed from: ــ  reason: contains not printable characters */
    private long f4108;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final JSONObject f4109;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean f4110;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public boolean f4111;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final long f4112;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public final List<String> f4113;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String f4114;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final String f4115;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final zzou f4116;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public boolean f4117;

    /* renamed from: 连任  reason: contains not printable characters */
    public final List<String> f4118;

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzanh f4119;

    /* renamed from: 麤  reason: contains not printable characters */
    public final int f4120;

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<String> f4121;

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzjj f4122;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public boolean f4123;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final zzva f4124;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final String f4125;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public boolean f4126;

    public zzafo(zzafp zzafp, zzanh zzanh, zzuh zzuh, zzva zzva, String str, zzuk zzuk, zzou zzou, String str2) {
        this(zzafp.f4136.f3763, (zzanh) null, zzafp.f4133.f3860, zzafp.f4132, zzafp.f4133.f3857, zzafp.f4133.f3844, zzafp.f4133.f3849, zzafp.f4133.f3848, zzafp.f4136.f3740, zzafp.f4133.f3822, (zzuh) null, (zzva) null, (String) null, zzafp.f4135, (zzuk) null, zzafp.f4133.f3824, zzafp.f4134, zzafp.f4133.f3820, zzafp.f4127, zzafp.f4128, zzafp.f4133.f3828, zzafp.f4129, (zzou) null, zzafp.f4133.f3854, zzafp.f4133.f3855, zzafp.f4133.f3855, zzafp.f4133.f3862, zzafp.f4133.f3865, (String) null, zzafp.f4133.f3821, zzafp.f4133.f3827, zzafp.f4130, zzafp.f4133.f3835, zzafp.f4131);
    }

    public zzafo(zzjj zzjj, zzanh zzanh, List<String> list, int i, List<String> list2, List<String> list3, int i2, long j, String str, boolean z, zzuh zzuh, zzva zzva, String str2, zzui zzui, zzuk zzuk, long j2, zzjn zzjn, long j3, long j4, long j5, String str3, JSONObject jSONObject, zzou zzou, zzaeq zzaeq, List<String> list4, List<String> list5, boolean z2, zzaaz zzaaz, String str4, List<String> list6, String str5, zzis zzis, boolean z3, boolean z4) {
        this.f4117 = false;
        this.f4123 = false;
        this.f4126 = false;
        this.f4111 = false;
        this.f4122 = zzjj;
        this.f4119 = zzanh;
        this.f4121 = m4441(list);
        this.f4120 = i;
        this.f4118 = m4441(list2);
        this.f4088 = m4441(list3);
        this.f4090 = i2;
        this.f4092 = j;
        this.f4105 = str;
        this.f4094 = z;
        this.f4096 = zzuh;
        this.f4124 = zzva;
        this.f4125 = str2;
        this.f4101 = zzui;
        this.f4102 = zzuk;
        this.f4095 = j2;
        this.f4098 = zzjn;
        this.f4108 = j3;
        this.f4107 = j4;
        this.f4112 = j5;
        this.f4114 = str3;
        this.f4109 = jSONObject;
        this.f4116 = zzou;
        this.f4100 = zzaeq;
        this.f4104 = m4441(list4);
        this.f4106 = m4441(list5);
        this.f4110 = z2;
        this.f4097 = zzaaz;
        this.f4103 = str4;
        this.f4113 = m4441(list6);
        this.f4115 = str5;
        this.f4089 = zzis;
        this.f4093 = z3;
        this.f4091 = z4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> List<T> m4441(List<T> list) {
        if (list == null) {
            return null;
        }
        return Collections.unmodifiableList(list);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4442() {
        if (this.f4119 == null || this.f4119.m4980() == null) {
            return false;
        }
        return this.f4119.m4980().m5040();
    }
}
