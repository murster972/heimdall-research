package com.google.android.gms.internal;

import android.view.MotionEvent;
import android.view.View;

final class zzpl implements zzoq {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzpj f10837;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ View f10838;

    zzpl(zzpj zzpj, View view) {
        this.f10837 = zzpj;
        this.f10838 = view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13216() {
        if (this.f10837.m5789(zzpj.f5301)) {
            this.f10837.onClick(this.f10838);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13217(MotionEvent motionEvent) {
        this.f10837.onTouch((View) null, motionEvent);
    }
}
