package com.google.android.gms.internal;

import android.content.SharedPreferences;
import org.json.JSONObject;

final class zzmy extends zzmx<Boolean> {
    zzmy(int i, String str, Boolean bool) {
        super(i, str, bool, (zzmy) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13147(SharedPreferences sharedPreferences) {
        return Boolean.valueOf(sharedPreferences.getBoolean(m5586(), ((Boolean) m5582()).booleanValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13148(JSONObject jSONObject) {
        return Boolean.valueOf(jSONObject.optBoolean(m5586(), ((Boolean) m5582()).booleanValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m13149(SharedPreferences.Editor editor, Object obj) {
        editor.putBoolean(m5586(), ((Boolean) obj).booleanValue());
    }
}
