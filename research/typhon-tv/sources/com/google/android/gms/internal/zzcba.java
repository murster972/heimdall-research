package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzcba extends zzeu implements zzcay {
    zzcba(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.flags.IFlagProvider");
    }

    public final boolean getBooleanFlagValue(String str, boolean z, int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12307(v_, z);
        v_.writeInt(i);
        Parcel r0 = m12300(2, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    public final int getIntFlagValue(String str, int i, int i2) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeInt(i);
        v_.writeInt(i2);
        Parcel r0 = m12300(3, v_);
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    public final long getLongFlagValue(String str, long j, int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeLong(j);
        v_.writeInt(i);
        Parcel r0 = m12300(4, v_);
        long readLong = r0.readLong();
        r0.recycle();
        return readLong;
    }

    public final String getStringFlagValue(String str, String str2, int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        v_.writeInt(i);
        Parcel r0 = m12300(5, v_);
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    public final void init(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(1, v_);
    }
}
