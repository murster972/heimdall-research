package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.gms.common.internal.zzbq;

@zzzv
public final class zzmt {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f4880;

    public zzmt(Context context) {
        zzbq.m9121(context, (Object) "Context can not be null");
        this.f4880 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m5569(Intent intent) {
        zzbq.m9121(intent, (Object) "Intent can not be null");
        return !this.f4880.getPackageManager().queryIntentActivities(intent, 0).isEmpty();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5570() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:"));
        return m5569(intent);
    }

    @TargetApi(14)
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m5571() {
        return m5569(new Intent("android.intent.action.INSERT").setType("vnd.android.cursor.dir/event"));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m5572() {
        return ((Boolean) zzajk.m4728(this.f4880, new zzmu())).booleanValue() && zzbhf.m10231(this.f4880).m10224("android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5573() {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:"));
        return m5569(intent);
    }
}
