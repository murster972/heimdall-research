package com.google.android.gms.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.zzbq;

public final class zzcic {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzchx f9338;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f9339 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f9340;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f9341;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f9342;

    public zzcic(zzchx zzchx, String str, String str2) {
        this.f9338 = zzchx;
        zzbq.m9122(str);
        this.f9342 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m10908() {
        if (!this.f9341) {
            this.f9341 = true;
            this.f9340 = this.f9338.m10885().getString(this.f9342, (String) null);
        }
        return this.f9340;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10909(String str) {
        if (!zzclq.m11387(str, this.f9340)) {
            SharedPreferences.Editor edit = this.f9338.m10885().edit();
            edit.putString(this.f9342, str);
            edit.apply();
            this.f9340 = str;
        }
    }
}
