package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzvm extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    String m13571() throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    void m13572() throws RemoteException;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean m13573() throws RemoteException;

    /* renamed from: ʾ  reason: contains not printable characters */
    zzpm m13574() throws RemoteException;

    /* renamed from: ʿ  reason: contains not printable characters */
    IObjectWrapper m13575() throws RemoteException;

    /* renamed from: ˈ  reason: contains not printable characters */
    zzll m13576() throws RemoteException;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean m13577() throws RemoteException;

    /* renamed from: ٴ  reason: contains not printable characters */
    Bundle m13578() throws RemoteException;

    /* renamed from: ᐧ  reason: contains not printable characters */
    IObjectWrapper m13579() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    String m13580() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    List m13581() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m13582(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    zzpq m13583() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    String m13584() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13585(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13586() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13587(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: ﹶ  reason: contains not printable characters */
    IObjectWrapper m13588() throws RemoteException;
}
