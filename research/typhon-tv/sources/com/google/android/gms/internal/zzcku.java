package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.stats.zza;

public final class zzcku implements ServiceConnection, zzf, zzg {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public volatile boolean f9608;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile zzchl f9609;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzckg f9610;

    protected zzcku(zzckg zzckg) {
        this.f9610 = zzckg;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0094 A[SYNTHETIC, Splitter:B:39:0x0094] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r5, android.os.IBinder r6) {
        /*
            r4 = this;
            r1 = 0
            java.lang.String r0 = "MeasurementServiceConnection.onServiceConnected"
            com.google.android.gms.common.internal.zzbq.m9115((java.lang.String) r0)
            monitor-enter(r4)
            if (r6 != 0) goto L_0x001f
            r0 = 0
            r4.f9608 = r0     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzckg r0 = r4.f9610     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzchm r0 = r0.m11096()     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = "Service connected with null binder"
            r0.m10849(r1)     // Catch:{ all -> 0x0058 }
            monitor-exit(r4)     // Catch:{ all -> 0x0058 }
        L_0x001e:
            return
        L_0x001f:
            java.lang.String r0 = r6.getInterfaceDescriptor()     // Catch:{ RemoteException -> 0x006f }
            java.lang.String r2 = "com.google.android.gms.measurement.internal.IMeasurementService"
            boolean r2 = r2.equals(r0)     // Catch:{ RemoteException -> 0x006f }
            if (r2 == 0) goto L_0x0082
            if (r6 != 0) goto L_0x005b
            r0 = r1
        L_0x002f:
            com.google.android.gms.internal.zzckg r1 = r4.f9610     // Catch:{ RemoteException -> 0x00a5 }
            com.google.android.gms.internal.zzchm r1 = r1.m11096()     // Catch:{ RemoteException -> 0x00a5 }
            com.google.android.gms.internal.zzcho r1 = r1.m10848()     // Catch:{ RemoteException -> 0x00a5 }
            java.lang.String r2 = "Bound to IMeasurementService interface"
            r1.m10849(r2)     // Catch:{ RemoteException -> 0x00a5 }
        L_0x003f:
            if (r0 != 0) goto L_0x0094
            r0 = 0
            r4.f9608 = r0     // Catch:{ all -> 0x0058 }
            com.google.android.gms.common.stats.zza.m9235()     // Catch:{ IllegalArgumentException -> 0x00a3 }
            com.google.android.gms.internal.zzckg r0 = r4.f9610     // Catch:{ IllegalArgumentException -> 0x00a3 }
            android.content.Context r0 = r0.m11097()     // Catch:{ IllegalArgumentException -> 0x00a3 }
            com.google.android.gms.internal.zzckg r1 = r4.f9610     // Catch:{ IllegalArgumentException -> 0x00a3 }
            com.google.android.gms.internal.zzcku r1 = r1.f9561     // Catch:{ IllegalArgumentException -> 0x00a3 }
            r0.unbindService(r1)     // Catch:{ IllegalArgumentException -> 0x00a3 }
        L_0x0056:
            monitor-exit(r4)     // Catch:{ all -> 0x0058 }
            goto L_0x001e
        L_0x0058:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0058 }
            throw r0
        L_0x005b:
            java.lang.String r0 = "com.google.android.gms.measurement.internal.IMeasurementService"
            android.os.IInterface r0 = r6.queryLocalInterface(r0)     // Catch:{ RemoteException -> 0x006f }
            boolean r2 = r0 instanceof com.google.android.gms.internal.zzche     // Catch:{ RemoteException -> 0x006f }
            if (r2 == 0) goto L_0x0069
            com.google.android.gms.internal.zzche r0 = (com.google.android.gms.internal.zzche) r0     // Catch:{ RemoteException -> 0x006f }
            goto L_0x002f
        L_0x0069:
            com.google.android.gms.internal.zzchg r0 = new com.google.android.gms.internal.zzchg     // Catch:{ RemoteException -> 0x006f }
            r0.<init>(r6)     // Catch:{ RemoteException -> 0x006f }
            goto L_0x002f
        L_0x006f:
            r0 = move-exception
            r0 = r1
        L_0x0071:
            com.google.android.gms.internal.zzckg r1 = r4.f9610     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzchm r1 = r1.m11096()     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzcho r1 = r1.m10832()     // Catch:{ all -> 0x0058 }
            java.lang.String r2 = "Service connect failed to get IMeasurementService"
            r1.m10849(r2)     // Catch:{ all -> 0x0058 }
            goto L_0x003f
        L_0x0082:
            com.google.android.gms.internal.zzckg r2 = r4.f9610     // Catch:{ RemoteException -> 0x006f }
            com.google.android.gms.internal.zzchm r2 = r2.m11096()     // Catch:{ RemoteException -> 0x006f }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ RemoteException -> 0x006f }
            java.lang.String r3 = "Got binder with a wrong descriptor"
            r2.m10850(r3, r0)     // Catch:{ RemoteException -> 0x006f }
            r0 = r1
            goto L_0x003f
        L_0x0094:
            com.google.android.gms.internal.zzckg r1 = r4.f9610     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzcih r1 = r1.m11101()     // Catch:{ all -> 0x0058 }
            com.google.android.gms.internal.zzckv r2 = new com.google.android.gms.internal.zzckv     // Catch:{ all -> 0x0058 }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x0058 }
            r1.m10986((java.lang.Runnable) r2)     // Catch:{ all -> 0x0058 }
            goto L_0x0056
        L_0x00a3:
            r0 = move-exception
            goto L_0x0056
        L_0x00a5:
            r1 = move-exception
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcku.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        zzbq.m9115("MeasurementServiceConnection.onServiceDisconnected");
        this.f9610.m11096().m10845().m10849("Service disconnected");
        this.f9610.m11101().m10986((Runnable) new zzckw(this, componentName));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11278() {
        this.f9610.m11109();
        Context r0 = this.f9610.m11097();
        synchronized (this) {
            if (this.f9608) {
                this.f9610.m11096().m10848().m10849("Connection attempt already in progress");
            } else if (this.f9609 != null) {
                this.f9610.m11096().m10848().m10849("Already awaiting connection attempt");
            } else {
                this.f9609 = new zzchl(r0, Looper.getMainLooper(), this, this);
                this.f9610.m11096().m10848().m10849("Connecting to remote service");
                this.f9608 = true;
                this.f9609.m9167();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11279(int i) {
        zzbq.m9115("MeasurementServiceConnection.onConnectionSuspended");
        this.f9610.m11096().m10845().m10849("Service connection suspended");
        this.f9610.m11101().m10986((Runnable) new zzcky(this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11280(Intent intent) {
        this.f9610.m11109();
        Context r0 = this.f9610.m11097();
        zza r1 = zza.m9235();
        synchronized (this) {
            if (this.f9608) {
                this.f9610.m11096().m10848().m10849("Connection attempt already in progress");
                return;
            }
            this.f9610.m11096().m10848().m10849("Using local app measurement service");
            this.f9608 = true;
            r1.m9236(r0, intent, this.f9610.f9561, 129);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11281(Bundle bundle) {
        zzbq.m9115("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.f9609 = null;
                this.f9610.m11101().m10986((Runnable) new zzckx(this, (zzche) this.f9609.m9171()));
            } catch (DeadObjectException | IllegalStateException e) {
                this.f9609 = null;
                this.f9608 = false;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11282(ConnectionResult connectionResult) {
        zzbq.m9115("MeasurementServiceConnection.onConnectionFailed");
        zzchm r0 = this.f9610.f9487.m11017();
        if (r0 != null) {
            r0.m10834().m10850("Service connection failed", connectionResult);
        }
        synchronized (this) {
            this.f9608 = false;
            this.f9609 = null;
        }
        this.f9610.m11101().m10986((Runnable) new zzckz(this));
    }
}
