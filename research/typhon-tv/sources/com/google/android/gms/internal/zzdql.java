package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;

final class zzdql implements zzdpw<zzdpp> {
    zzdql() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdpp m11715(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdry r0 = zzdry.m11890(zzfes);
            if (!(r0 instanceof zzdry)) {
                throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
            }
            zzdry zzdry = r0;
            zzdvk.m12238(zzdry.m11896(), 0);
            if (zzdry.m11894().size() == 32) {
                return zzdug.m12200(zzdry.m11894().toByteArray());
            }
            throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
        } catch (zzfge e) {
            throw new GeneralSecurityException("invalid ChaCha20Poly1305 key", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzdry m11711() throws GeneralSecurityException {
        return (zzdry) zzdry.m11889().m11899(0).m11900(zzfes.zzaz(zzdvi.m12235(32))).m12542();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11712(zzfes zzfes) throws GeneralSecurityException {
        return m11711();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11713(zzfhe zzfhe) throws GeneralSecurityException {
        return m11711();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11714(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key").m12022(m11711().m12361()).m12021(zzdsy.zzb.SYMMETRIC).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11716(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdry)) {
            throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
        }
        zzdry zzdry = (zzdry) zzfhe;
        zzdvk.m12238(zzdry.m11896(), 0);
        if (zzdry.m11894().size() == 32) {
            return zzdug.m12200(zzdry.m11894().toByteArray());
        }
        throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
    }
}
