package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.gms.ads.AdActivity;
import io.fabric.sdk.android.services.common.AbstractSpiCall;

@zzzv
public final class zzafx {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Object f4191 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f4192;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f4193 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f4194 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    private long f4195 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f4196 = -1;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f4197 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f4198 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    int f4199 = -1;

    public zzafx(String str) {
        this.f4192 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m4511(Context context) {
        int identifier = context.getResources().getIdentifier("Theme.Translucent", TtmlNode.TAG_STYLE, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        if (identifier == 0) {
            zzagf.m4794("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        }
        try {
            if (identifier == context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), AdActivity.CLASS_NAME), 0).theme) {
                return true;
            }
            zzagf.m4794("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            zzagf.m4791("Fail to fetch AdActivity theme");
            zzagf.m4794("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4512() {
        synchronized (this.f4191) {
            this.f4194++;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m4513(Context context, String str) {
        Bundle bundle;
        synchronized (this.f4191) {
            bundle = new Bundle();
            bundle.putString("session_id", this.f4192);
            bundle.putLong("basets", this.f4198);
            bundle.putLong("currts", this.f4196);
            bundle.putString("seq_num", str);
            bundle.putInt("preqs", this.f4197);
            bundle.putInt("preqs_in_session", this.f4199);
            bundle.putLong("time_in_session", this.f4195);
            bundle.putInt("pclick", this.f4193);
            bundle.putInt("pimp", this.f4194);
            bundle.putBoolean("support_transparent_background", m4511(context));
        }
        return bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4514() {
        synchronized (this.f4191) {
            this.f4193++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4515(com.google.android.gms.internal.zzjj r11, long r12) {
        /*
            r10 = this;
            java.lang.Object r1 = r10.f4191
            monitor-enter(r1)
            com.google.android.gms.internal.zzaft r0 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ all -> 0x005a }
            long r2 = r0.m4481()     // Catch:{ all -> 0x005a }
            com.google.android.gms.common.util.zzd r0 = com.google.android.gms.ads.internal.zzbs.zzeo()     // Catch:{ all -> 0x005a }
            long r4 = r0.m9243()     // Catch:{ all -> 0x005a }
            long r6 = r10.f4198     // Catch:{ all -> 0x005a }
            r8 = -1
            int r0 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x005d
            long r2 = r4 - r2
            com.google.android.gms.internal.zzmx<java.lang.Long> r0 = com.google.android.gms.internal.zznh.f4913     // Catch:{ all -> 0x005a }
            com.google.android.gms.internal.zznf r6 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x005a }
            java.lang.Object r0 = r6.m5595(r0)     // Catch:{ all -> 0x005a }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x005a }
            long r6 = r0.longValue()     // Catch:{ all -> 0x005a }
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x004f
            r0 = -1
            r10.f4199 = r0     // Catch:{ all -> 0x005a }
        L_0x0034:
            r10.f4198 = r12     // Catch:{ all -> 0x005a }
            long r2 = r10.f4198     // Catch:{ all -> 0x005a }
            r10.f4196 = r2     // Catch:{ all -> 0x005a }
        L_0x003a:
            if (r11 == 0) goto L_0x0060
            android.os.Bundle r0 = r11.f4767     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0060
            android.os.Bundle r0 = r11.f4767     // Catch:{ all -> 0x005a }
            java.lang.String r2 = "gw"
            r3 = 2
            int r0 = r0.getInt(r2, r3)     // Catch:{ all -> 0x005a }
            r2 = 1
            if (r0 != r2) goto L_0x0060
            monitor-exit(r1)     // Catch:{ all -> 0x005a }
        L_0x004e:
            return
        L_0x004f:
            com.google.android.gms.internal.zzaft r0 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ all -> 0x005a }
            int r0 = r0.m4469()     // Catch:{ all -> 0x005a }
            r10.f4199 = r0     // Catch:{ all -> 0x005a }
            goto L_0x0034
        L_0x005a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005a }
            throw r0
        L_0x005d:
            r10.f4196 = r12     // Catch:{ all -> 0x005a }
            goto L_0x003a
        L_0x0060:
            int r0 = r10.f4197     // Catch:{ all -> 0x005a }
            int r0 = r0 + 1
            r10.f4197 = r0     // Catch:{ all -> 0x005a }
            int r0 = r10.f4199     // Catch:{ all -> 0x005a }
            int r0 = r0 + 1
            r10.f4199 = r0     // Catch:{ all -> 0x005a }
            int r0 = r10.f4199     // Catch:{ all -> 0x005a }
            if (r0 != 0) goto L_0x007d
            r2 = 0
            r10.f4195 = r2     // Catch:{ all -> 0x005a }
            com.google.android.gms.internal.zzaft r0 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ all -> 0x005a }
            r0.m4496((long) r4)     // Catch:{ all -> 0x005a }
        L_0x007b:
            monitor-exit(r1)     // Catch:{ all -> 0x005a }
            goto L_0x004e
        L_0x007d:
            com.google.android.gms.internal.zzaft r0 = com.google.android.gms.ads.internal.zzbs.zzem()     // Catch:{ all -> 0x005a }
            long r2 = r0.m4471()     // Catch:{ all -> 0x005a }
            long r2 = r4 - r2
            r10.f4195 = r2     // Catch:{ all -> 0x005a }
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzafx.m4515(com.google.android.gms.internal.zzjj, long):void");
    }
}
