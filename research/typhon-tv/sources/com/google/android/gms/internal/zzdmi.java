package com.google.android.gms.internal;

import android.animation.Animator;

public final class zzdmi extends zzdmh {

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdmm f9892 = new zzdmj(this);
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Runnable f9893;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9894;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f9895;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Animator f9896;

    private zzdmi(Animator animator, int i, Runnable runnable) {
        this.f9896 = animator;
        this.f9895 = -1;
        this.f9893 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ int m11630(zzdmi zzdmi) {
        int i = zzdmi.f9894;
        zzdmi.f9894 = i + 1;
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m11631(Animator animator, int i, Runnable runnable) {
        animator.addListener(new zzdmi(animator, -1, (Runnable) null));
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11632() {
        return this.f9895 != -1 && this.f9894 >= this.f9895;
    }

    public final void onAnimationEnd(Animator animator) {
        if (!m11627(animator)) {
            zzdmk.m11634().m11635(this.f9892);
        }
    }
}
