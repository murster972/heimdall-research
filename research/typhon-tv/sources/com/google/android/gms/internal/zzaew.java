package com.google.android.gms.internal;

import android.content.Context;

@zzzv
public final class zzaew implements zzafc {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzafd f4068;

    public zzaew(zzafd zzafd) {
        this.f4068 = zzafd;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzafb m4401(Context context, zzakd zzakd, zzaax zzaax) {
        if (zzaax.f3829 == null) {
            return null;
        }
        return new zzaes(context, zzakd, zzaax.f3829, zzaax.f3861, this.f4068);
    }
}
