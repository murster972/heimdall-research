package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;

@zzzv
public final class zzji extends zzp<zzkv> {
    public zzji() {
        super("com.google.android.gms.ads.AdManagerCreatorImpl");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzks m5447(Context context, zzjn zzjn, String str, zzux zzux, int i) {
        try {
            IBinder r1 = ((zzkv) m9308(context)).m13071(zzn.m9306(context), zzjn, str, zzux, 11910000, i);
            if (r1 == null) {
                return null;
            }
            IInterface queryLocalInterface = r1.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            return queryLocalInterface instanceof zzks ? (zzks) queryLocalInterface : new zzku(r1);
        } catch (RemoteException | zzq e) {
            zzakb.m4797("Could not create remote AdManager.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m5448(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManagerCreator");
        return queryLocalInterface instanceof zzkv ? (zzkv) queryLocalInterface : new zzkw(iBinder);
    }
}
