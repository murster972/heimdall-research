package com.google.android.gms.internal;

import com.google.firebase.remoteconfig.FirebaseRemoteConfigInfo;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public final class zzexf implements FirebaseRemoteConfigInfo {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f10304;

    /* renamed from: 齉  reason: contains not printable characters */
    private FirebaseRemoteConfigSettings f10305;

    /* renamed from: 龘  reason: contains not printable characters */
    private long f10306;

    /* renamed from: 龘  reason: contains not printable characters */
    public final FirebaseRemoteConfigSettings m12321() {
        return this.f10305;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12322(int i) {
        this.f10304 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12323(long j) {
        this.f10306 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12324(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.f10305 = firebaseRemoteConfigSettings;
    }
}
