package com.google.android.gms.internal;

import java.util.Arrays;

final class zzfju {

    /* renamed from: 靐  reason: contains not printable characters */
    final byte[] f10550;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f10551;

    zzfju(int i, byte[] bArr) {
        this.f10551 = i;
        this.f10550 = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfju)) {
            return false;
        }
        zzfju zzfju = (zzfju) obj;
        return this.f10551 == zzfju.f10551 && Arrays.equals(this.f10550, zzfju.f10550);
    }

    public final int hashCode() {
        return ((this.f10551 + 527) * 31) + Arrays.hashCode(this.f10550);
    }
}
