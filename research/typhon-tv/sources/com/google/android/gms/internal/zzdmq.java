package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.Choreographer;

@TargetApi(16)
final class zzdmq extends zzdmk {

    /* renamed from: 龘  reason: contains not printable characters */
    private Choreographer f9904 = Choreographer.getInstance();

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11640(zzdmm zzdmm) {
        this.f9904.postFrameCallback(zzdmm.m11637());
    }
}
