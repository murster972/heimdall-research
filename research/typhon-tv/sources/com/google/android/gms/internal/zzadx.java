package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.dynamic.zzn;

@zzzv
public final class zzadx implements RewardedVideoAd {

    /* renamed from: 连任  reason: contains not printable characters */
    private String f4030;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f4031;

    /* renamed from: 麤  reason: contains not printable characters */
    private RewardedVideoAdListener f4032;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Object f4033 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzadk f4034;

    public zzadx(Context context, zzadk zzadk) {
        this.f4034 = zzadk;
        this.f4031 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4365(String str, zzlt zzlt) {
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    this.f4034.m9479(new zzadv(zzjm.m5452(this.f4031, zzlt), str));
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward loadAd to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public final void destroy() {
        destroy((Context) null);
    }

    public final void destroy(Context context) {
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    this.f4034.m9475(zzn.m9306(context));
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward destroy to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public final String getMediationAdapterClassName() {
        try {
            if (this.f4034 != null) {
                return this.f4034.m9469();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Failed to get the mediation adapter class name.", e);
        }
        return null;
    }

    public final RewardedVideoAdListener getRewardedVideoAdListener() {
        RewardedVideoAdListener rewardedVideoAdListener;
        synchronized (this.f4033) {
            rewardedVideoAdListener = this.f4032;
        }
        return rewardedVideoAdListener;
    }

    public final String getUserId() {
        String str;
        synchronized (this.f4033) {
            str = this.f4030;
        }
        return str;
    }

    public final boolean isLoaded() {
        boolean z = false;
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    z = this.f4034.m9472();
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward isLoaded to RewardedVideoAd", e);
                }
            }
        }
        return z;
    }

    public final void loadAd(String str, AdRequest adRequest) {
        m4365(str, adRequest.zzbg());
    }

    public final void loadAd(String str, PublisherAdRequest publisherAdRequest) {
        m4365(str, publisherAdRequest.zzbg());
    }

    public final void pause() {
        pause((Context) null);
    }

    public final void pause(Context context) {
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    this.f4034.m9477(zzn.m9306(context));
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward pause to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public final void resume() {
        resume((Context) null);
    }

    public final void resume(Context context) {
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    this.f4034.m9471(zzn.m9306(context));
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward resume to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public final void setImmersiveMode(boolean z) {
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    this.f4034.m9481(z);
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward setImmersiveMode to RewardedVideoAd", e);
                }
            }
        }
    }

    public final void setRewardedVideoAdListener(RewardedVideoAdListener rewardedVideoAdListener) {
        synchronized (this.f4033) {
            this.f4032 = rewardedVideoAdListener;
            if (this.f4034 != null) {
                try {
                    this.f4034.m9478((zzadp) new zzadu(rewardedVideoAdListener));
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward setRewardedVideoAdListener to RewardedVideoAd", e);
                }
            }
        }
    }

    public final void setUserId(String str) {
        synchronized (this.f4033) {
            this.f4030 = str;
            if (this.f4034 != null) {
                try {
                    this.f4034.m9480(str);
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward setUserId to RewardedVideoAd", e);
                }
            }
        }
    }

    public final void show() {
        synchronized (this.f4033) {
            if (this.f4034 != null) {
                try {
                    this.f4034.m9476();
                } catch (RemoteException e) {
                    zzakb.m4796("Could not forward show to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }
}
