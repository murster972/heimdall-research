package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.internal.zzf;

final class zzig implements zzf {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzid f10711;

    zzig(zzid zzid) {
        this.f10711 = zzid;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13004(int i) {
        synchronized (this.f10711.f4733) {
            zzio unused = this.f10711.f4732 = null;
            this.f10711.f4733.notifyAll();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13005(Bundle bundle) {
        synchronized (this.f10711.f4733) {
            try {
                if (this.f10711.f4735 != null) {
                    zzio unused = this.f10711.f4732 = this.f10711.f4735.m5424();
                }
            } catch (DeadObjectException e) {
                zzagf.m4793("Unable to obtain a cache service instance.", e);
                this.f10711.m5414();
            }
            this.f10711.f4733.notifyAll();
        }
    }
}
