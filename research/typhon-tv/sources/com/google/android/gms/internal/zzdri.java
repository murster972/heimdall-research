package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdri extends zzffu<zzdri, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdri f9977;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdri> f9978;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f9979;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdrk f9980;

    public static final class zza extends zzffu.zza<zzdri, zza> implements zzfhg {
        private zza() {
            super(zzdri.f9977);
        }

        /* synthetic */ zza(zzdrj zzdrj) {
            this();
        }
    }

    static {
        zzdri zzdri = new zzdri();
        f9977 = zzdri;
        zzdri.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdri.f10394.m12718();
    }

    private zzdri() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zzdri m11814() {
        return f9977;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdri m11815(zzfes zzfes) throws zzfge {
        return (zzdri) zzffu.m12526(f9977, zzfes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m11816() {
        return this.f9979;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11817() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9980 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f9980 == null ? zzdrk.m11821() : this.f9980) + 0;
        }
        if (this.f9979 != 0) {
            i2 += zzffg.m5245(2, this.f9979);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdrk m11818() {
        return this.f9980 == null ? zzdrk.m11821() : this.f9980;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11819(int i, Object obj, Object obj2) {
        zzdrk.zza zza2;
        boolean z = true;
        switch (zzdrj.f9981[i - 1]) {
            case 1:
                return new zzdri();
            case 2:
                return f9977;
            case 3:
                return null;
            case 4:
                return new zza((zzdrj) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdri zzdri = (zzdri) obj2;
                this.f9980 = (zzdrk) zzh.m12572(this.f9980, zzdri.f9980);
                boolean z2 = this.f9979 != 0;
                int i2 = this.f9979;
                if (zzdri.f9979 == 0) {
                    z = false;
                }
                this.f9979 = zzh.m12569(z2, i2, z, zzdri.f9979);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 10:
                                    if (this.f9980 != null) {
                                        zzdrk zzdrk = this.f9980;
                                        zzffu.zza zza3 = (zzffu.zza) zzdrk.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdrk);
                                        zza2 = (zzdrk.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f9980 = (zzdrk) zzffb.m12416(zzdrk.m11821(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f9980);
                                        this.f9980 = (zzdrk) zza2.m12543();
                                        break;
                                    }
                                case 16:
                                    this.f9979 = zzffb.m12402();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f9978 == null) {
                    synchronized (zzdri.class) {
                        if (f9978 == null) {
                            f9978 = new zzffu.zzb(f9977);
                        }
                    }
                }
                return f9978;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f9977;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11820(zzffg zzffg) throws IOException {
        if (this.f9980 != null) {
            zzffg.m5289(1, (zzfhe) this.f9980 == null ? zzdrk.m11821() : this.f9980);
        }
        if (this.f9979 != 0) {
            zzffg.m5281(2, this.f9979);
        }
        this.f10394.m12719(zzffg);
    }
}
