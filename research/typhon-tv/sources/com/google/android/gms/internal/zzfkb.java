package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfkb extends zzfjm<zzfkb> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private byte[] f10592 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private Integer f10593 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzfjy[] f10594 = zzfjy.m12891();

    /* renamed from: 麤  reason: contains not printable characters */
    private byte[] f10595 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f10596 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzfkc f10597 = null;

    public zzfkb() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12901() {
        int r0 = super.m12841();
        if (this.f10597 != null) {
            r0 += zzfjk.m12807(1, (zzfjs) this.f10597);
        }
        if (this.f10594 != null && this.f10594.length > 0) {
            int i = r0;
            for (zzfjy zzfjy : this.f10594) {
                if (zzfjy != null) {
                    i += zzfjk.m12807(2, (zzfjs) zzfjy);
                }
            }
            r0 = i;
        }
        if (this.f10596 != null) {
            r0 += zzfjk.m12809(3, this.f10596);
        }
        if (this.f10595 != null) {
            r0 += zzfjk.m12809(4, this.f10595);
        }
        if (this.f10593 != null) {
            r0 += zzfjk.m12806(5, this.f10593.intValue());
        }
        return this.f10592 != null ? r0 + zzfjk.m12809(6, this.f10592) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12902(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    if (this.f10597 == null) {
                        this.f10597 = new zzfkc();
                    }
                    zzfjj.m12802((zzfjs) this.f10597);
                    continue;
                case 18:
                    int r2 = zzfjv.m12883(zzfjj, 18);
                    int length = this.f10594 == null ? 0 : this.f10594.length;
                    zzfjy[] zzfjyArr = new zzfjy[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10594, 0, zzfjyArr, 0, length);
                    }
                    while (length < zzfjyArr.length - 1) {
                        zzfjyArr[length] = new zzfjy();
                        zzfjj.m12802((zzfjs) zzfjyArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzfjyArr[length] = new zzfjy();
                    zzfjj.m12802((zzfjs) zzfjyArr[length]);
                    this.f10594 = zzfjyArr;
                    continue;
                case 26:
                    this.f10596 = zzfjj.m12784();
                    continue;
                case 34:
                    this.f10595 = zzfjj.m12784();
                    continue;
                case 40:
                    this.f10593 = Integer.valueOf(zzfjj.m12798());
                    continue;
                case 50:
                    this.f10592 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12903(zzfjk zzfjk) throws IOException {
        if (this.f10597 != null) {
            zzfjk.m12834(1, (zzfjs) this.f10597);
        }
        if (this.f10594 != null && this.f10594.length > 0) {
            for (zzfjy zzfjy : this.f10594) {
                if (zzfjy != null) {
                    zzfjk.m12834(2, (zzfjs) zzfjy);
                }
            }
        }
        if (this.f10596 != null) {
            zzfjk.m12837(3, this.f10596);
        }
        if (this.f10595 != null) {
            zzfjk.m12837(4, this.f10595);
        }
        if (this.f10593 != null) {
            zzfjk.m12832(5, this.f10593.intValue());
        }
        if (this.f10592 != null) {
            zzfjk.m12837(6, this.f10592);
        }
        super.m12842(zzfjk);
    }
}
