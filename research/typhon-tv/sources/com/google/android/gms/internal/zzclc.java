package com.google.android.gms.internal;

import android.app.job.JobParameters;

final /* synthetic */ class zzclc implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzchm f9625;

    /* renamed from: 齉  reason: contains not printable characters */
    private final JobParameters f9626;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcla f9627;

    zzclc(zzcla zzcla, zzchm zzchm, JobParameters jobParameters) {
        this.f9627 = zzcla;
        this.f9625 = zzchm;
        this.f9626 = jobParameters;
    }

    public final void run() {
        this.f9627.m11294(this.f9625, this.f9626);
    }
}
