package com.google.android.gms.internal;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@zzzv
public final class zzahh {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ThreadPoolExecutor f4210 = new ThreadPoolExecutor(5, 5, 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), m4557("Loader"));

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ThreadPoolExecutor f4211 = new ThreadPoolExecutor(20, 20, 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), m4557("Default"));

    static {
        f4211.allowCoreThreadTimeOut(true);
        f4210.allowCoreThreadTimeOut(true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzakv<Void> m4554(int i, Runnable runnable) {
        return i == 1 ? m4556((ExecutorService) f4210, new zzahi(runnable)) : m4556((ExecutorService) f4211, new zzahj(runnable));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzakv<Void> m4555(Runnable runnable) {
        return m4554(0, runnable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> zzakv<T> m4556(ExecutorService executorService, Callable<T> callable) {
        zzalf zzalf = new zzalf();
        try {
            zzalf.m4823(new zzahl(zzalf, executorService.submit(new zzahk(zzalf, callable))), zzala.f4307);
        } catch (RejectedExecutionException e) {
            zzagf.m4796("Thread execution is rejected.", e);
            zzalf.m4824(e);
        }
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ThreadFactory m4557(String str) {
        return new zzahm(str);
    }
}
