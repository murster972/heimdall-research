package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzr;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public final class zzbcf extends zzab<zzbcr> {
    /* access modifiers changed from: private */

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static final Object f8629 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static final Object f8630 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final zzbcy f8631 = new zzbcy("CastClientImpl");
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final CastDevice f8632;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Cast.Listener f8633;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Map<String, Cast.MessageReceivedCallback> f8634 = new HashMap();

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f8635;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f8636;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final AtomicLong f8637 = new AtomicLong(0);
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f8638;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public String f8639;

    /* renamed from: ˊ  reason: contains not printable characters */
    private double f8640;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f8641;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f8642;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public String f8643;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final long f8644;

    /* renamed from: י  reason: contains not printable characters */
    private Bundle f8645;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public final Map<Long, zzn<Status>> f8646 = new HashMap();

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Bundle f8647;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzbch f8648;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public zzn<Cast.ApplicationConnectionResult> f8649;
    /* access modifiers changed from: private */

    /* renamed from: ᵎ  reason: contains not printable characters */
    public zzn<Status> f8650;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public ApplicationMetadata f8651;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f8652;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f8653;

    public zzbcf(Context context, Looper looper, zzr zzr, CastDevice castDevice, long j, Cast.Listener listener, Bundle bundle, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 10, zzr, connectionCallbacks, onConnectionFailedListener);
        this.f8632 = castDevice;
        this.f8633 = listener;
        this.f8644 = j;
        this.f8647 = bundle;
        m9981();
    }

    /* access modifiers changed from: private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public final void m9981() {
        this.f8653 = false;
        this.f8641 = -1;
        this.f8642 = -1;
        this.f8651 = null;
        this.f8638 = null;
        this.f8640 = 0.0d;
        this.f8635 = false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m9986(zzn<Status> zzn) {
        synchronized (f8630) {
            if (this.f8650 != null) {
                zzn.m8947(new Status(2001));
            } else {
                this.f8650 = zzn;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9993(zzn<Cast.ApplicationConnectionResult> zzn) {
        synchronized (f8629) {
            if (this.f8649 != null) {
                this.f8649.m8947(new zzbcg(new Status(2002)));
            }
            this.f8649 = zzn;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9994(zzbbt zzbbt) {
        boolean z;
        String r0 = zzbbt.m9956();
        if (!zzbcm.m10043(r0, this.f8638)) {
            this.f8638 = r0;
            z = true;
        } else {
            z = false;
        }
        f8631.m10090("hasChanged=%b, mFirstApplicationStatusUpdate=%b", Boolean.valueOf(z), Boolean.valueOf(this.f8636));
        if (this.f8633 != null && (z || this.f8636)) {
            this.f8633.m7813();
        }
        this.f8636 = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9998(zzbcn zzbcn) {
        boolean z;
        boolean z2;
        boolean z3;
        ApplicationMetadata r0 = zzbcn.m10044();
        if (!zzbcm.m10043(r0, this.f8651)) {
            this.f8651 = r0;
            this.f8633.m7815(this.f8651);
        }
        double r4 = zzbcn.m10048();
        if (Double.isNaN(r4) || Math.abs(r4 - this.f8640) <= 1.0E-7d) {
            z = false;
        } else {
            this.f8640 = r4;
            z = true;
        }
        boolean r3 = zzbcn.m10045();
        if (r3 != this.f8635) {
            this.f8635 = r3;
            z = true;
        }
        f8631.m10090("hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z), Boolean.valueOf(this.f8652));
        if (this.f8633 != null && (z || this.f8652)) {
            this.f8633.m7810();
        }
        int r02 = zzbcn.m10047();
        if (r02 != this.f8641) {
            this.f8641 = r02;
            z2 = true;
        } else {
            z2 = false;
        }
        f8631.m10090("hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z2), Boolean.valueOf(this.f8652));
        if (this.f8633 != null && (z2 || this.f8652)) {
            this.f8633.m7811(this.f8641);
        }
        int r03 = zzbcn.m10046();
        if (r03 != this.f8642) {
            this.f8642 = r03;
            z3 = true;
        } else {
            z3 = false;
        }
        f8631.m10090("hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z3), Boolean.valueOf(this.f8652));
        if (this.f8633 != null && (z3 || this.f8652)) {
            this.f8633.m7812(this.f8642);
        }
        this.f8652 = false;
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final void m9999() {
        f8631.m10090("removing all MessageReceivedCallbacks", new Object[0]);
        synchronized (this.f8634) {
            this.f8634.clear();
        }
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final void m10000() throws IllegalStateException {
        if (!this.f8653 || this.f8648 == null || this.f8648.m10026()) {
            throw new IllegalStateException("Not connected to a device");
        }
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m10001() {
        f8631.m10090("disconnect(); ServiceListener=%s, isConnected=%b", this.f8648, Boolean.valueOf(m9161()));
        zzbch zzbch = this.f8648;
        this.f8648 = null;
        if (zzbch == null || zzbch.m10029() == null) {
            f8631.m10090("already disposed, so short-circuiting", new Object[0]);
            return;
        }
        m9999();
        try {
            ((zzbcr) super.m9171()).m10054();
        } catch (RemoteException | IllegalStateException e) {
            f8631.m10091(e, "Error while disconnecting the controller interface: %s", e.getMessage());
        } finally {
            super.m9160();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public final Bundle m10002() {
        Bundle bundle = new Bundle();
        f8631.m10090("getRemoteService(): mLastApplicationId=%s, mLastSessionId=%s", this.f8639, this.f8643);
        this.f8632.m7827(bundle);
        bundle.putLong("com.google.android.gms.cast.EXTRA_CAST_FLAGS", this.f8644);
        if (this.f8647 != null) {
            bundle.putAll(this.f8647);
        }
        this.f8648 = new zzbch(this);
        bundle.putParcelable("listener", new BinderWrapper(this.f8648.asBinder()));
        if (this.f8639 != null) {
            bundle.putString("last_application_id", this.f8639);
            if (this.f8643 != null) {
                bundle.putString("last_session_id", this.f8643);
            }
        }
        return bundle;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final boolean m10003() throws IllegalStateException {
        m10000();
        return this.f8635;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final double m10004() throws IllegalStateException {
        m10000();
        return this.f8640;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10005() {
        return "com.google.android.gms.cast.internal.ICastDeviceController";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m10006() {
        if (this.f8645 == null) {
            return super.m9179();
        }
        Bundle bundle = this.f8645;
        this.f8645 = null;
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m10007(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.internal.ICastDeviceController");
        return queryLocalInterface instanceof zzbcr ? (zzbcr) queryLocalInterface : new zzbcs(iBinder);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10008(double d) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            throw new IllegalArgumentException(new StringBuilder(41).append("Volume cannot be ").append(d).toString());
        }
        ((zzbcr) super.m9171()).m10055(d, this.f8640, this.f8635);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10009(int i, IBinder iBinder, Bundle bundle, int i2) {
        f8631.m10090("in onPostInitHandler; statusCode=%d", Integer.valueOf(i));
        if (i == 0 || i == 1001) {
            this.f8653 = true;
            this.f8636 = true;
            this.f8652 = true;
        } else {
            this.f8653 = false;
        }
        if (i == 1001) {
            this.f8645 = new Bundle();
            this.f8645.putBoolean("com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING", true);
            i = 0;
        }
        super.m9183(i, iBinder, bundle, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10010(ConnectionResult connectionResult) {
        super.m9185(connectionResult);
        m9999();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10011(String str) throws IllegalArgumentException, RemoteException {
        Cast.MessageReceivedCallback remove;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Channel namespace cannot be null or empty");
        }
        synchronized (this.f8634) {
            remove = this.f8634.remove(str);
        }
        if (remove != null) {
            try {
                ((zzbcr) super.m9171()).m10053(str);
            } catch (IllegalStateException e) {
                f8631.m10091(e, "Error unregistering namespace (%s): %s", str, e.getMessage());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10012(String str, Cast.MessageReceivedCallback messageReceivedCallback) throws IllegalArgumentException, IllegalStateException, RemoteException {
        zzbcm.m10042(str);
        m10011(str);
        if (messageReceivedCallback != null) {
            synchronized (this.f8634) {
                this.f8634.put(str, messageReceivedCallback);
            }
            ((zzbcr) super.m9171()).m10052(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10013(String str, LaunchOptions launchOptions, zzn<Cast.ApplicationConnectionResult> zzn) throws IllegalStateException, RemoteException {
        m9993(zzn);
        ((zzbcr) super.m9171()).m10057(str, launchOptions);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10014(String str, zzn<Status> zzn) throws IllegalStateException, RemoteException {
        m9986(zzn);
        ((zzbcr) super.m9171()).m10056(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10015(String str, String str2, com.google.android.gms.cast.zzab zzab, zzn<Cast.ApplicationConnectionResult> zzn) throws IllegalStateException, RemoteException {
        m9993(zzn);
        if (zzab == null) {
            zzab = new com.google.android.gms.cast.zzab();
        }
        ((zzbcr) super.m9171()).m10059(str, str2, zzab);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10016(String str, String str2, zzn<Status> zzn) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (TextUtils.isEmpty(str2)) {
            throw new IllegalArgumentException("The message payload cannot be null or empty");
        } else if (str2.length() > 65536) {
            throw new IllegalArgumentException("Message exceeds maximum size");
        } else {
            zzbcm.m10042(str);
            m10000();
            long incrementAndGet = this.f8637.incrementAndGet();
            try {
                this.f8646.put(Long.valueOf(incrementAndGet), zzn);
                ((zzbcr) super.m9171()).m10058(str, str2, incrementAndGet);
            } catch (Throwable th) {
                this.f8646.remove(Long.valueOf(incrementAndGet));
                throw th;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10017(boolean z) throws IllegalStateException, RemoteException {
        ((zzbcr) super.m9171()).m10060(z, this.f8640, this.f8635);
    }
}
