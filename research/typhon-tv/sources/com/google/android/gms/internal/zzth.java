package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzp;
import java.util.Arrays;

@zzzv
final class zzth {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object[] f5365;

    zzth(zzjj zzjj, String str, int i) {
        this.f5365 = zzp.zza((String) zzkb.m5481().m5595(zznh.f4926), zzjj, str, i, (zzjn) null);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzth)) {
            return false;
        }
        return Arrays.equals(this.f5365, ((zzth) obj).f5365);
    }

    public final int hashCode() {
        return Arrays.hashCode(this.f5365);
    }

    public final String toString() {
        String arrays = Arrays.toString(this.f5365);
        return new StringBuilder(String.valueOf(arrays).length() + 24).append("[InterstitialAdPoolKey ").append(arrays).append("]").toString();
    }
}
