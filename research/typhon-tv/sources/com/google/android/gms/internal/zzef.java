package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzef extends zzet {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f10260 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile Long f10261 = null;

    public zzef(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 22);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12280() throws IllegalAccessException, InvocationTargetException {
        if (f10261 == null) {
            synchronized (f10260) {
                if (f10261 == null) {
                    f10261 = (Long) this.f10286.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.f10284) {
            this.f10284.f8435 = f10261;
        }
    }
}
