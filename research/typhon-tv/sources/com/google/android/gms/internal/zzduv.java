package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Provider;

public interface zzduv<T> {
    /* renamed from: 龘  reason: contains not printable characters */
    T m12218(String str, Provider provider) throws GeneralSecurityException;
}
