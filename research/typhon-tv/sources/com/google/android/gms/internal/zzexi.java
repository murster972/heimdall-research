package com.google.android.gms.internal;

import java.io.IOException;

public final class zzexi extends zzfjm<zzexi> {

    /* renamed from: 靐  reason: contains not printable characters */
    public long f10315 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    public byte[][] f10316 = zzfjv.f10553;

    /* renamed from: 龘  reason: contains not printable characters */
    public zzexl[] f10317 = zzexl.m12345();

    public zzexi() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzexi)) {
            return false;
        }
        zzexi zzexi = (zzexi) obj;
        if (!zzfjq.m12863((Object[]) this.f10317, (Object[]) zzexi.f10317)) {
            return false;
        }
        if (this.f10315 != zzexi.f10315) {
            return false;
        }
        if (!zzfjq.m12864(this.f10316, zzexi.f10316)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzexi.f10533 == null || zzexi.f10533.m12849() : this.f10533.equals(zzexi.f10533);
    }

    public final int hashCode() {
        return ((this.f10533 == null || this.f10533.m12849()) ? 0 : this.f10533.hashCode()) + ((((((((getClass().getName().hashCode() + 527) * 31) + zzfjq.m12859((Object[]) this.f10317)) * 31) + ((int) (this.f10315 ^ (this.f10315 >>> 32)))) * 31) + zzfjq.m12860(this.f10316)) * 31);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12335() {
        int r2 = super.m12841();
        if (this.f10317 != null && this.f10317.length > 0) {
            for (zzexl zzexl : this.f10317) {
                if (zzexl != null) {
                    r2 += zzfjk.m12807(1, (zzfjs) zzexl);
                }
            }
        }
        if (this.f10315 != 0) {
            r2 += zzfjk.m12805(2) + 8;
        }
        if (this.f10316 == null || this.f10316.length <= 0) {
            return r2;
        }
        int i = 0;
        int i2 = 0;
        for (byte[] bArr : this.f10316) {
            if (bArr != null) {
                i2++;
                i += zzfjk.m12810(bArr);
            }
        }
        return r2 + i + (i2 * 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12336(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    int r2 = zzfjv.m12883(zzfjj, 10);
                    int length = this.f10317 == null ? 0 : this.f10317.length;
                    zzexl[] zzexlArr = new zzexl[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10317, 0, zzexlArr, 0, length);
                    }
                    while (length < zzexlArr.length - 1) {
                        zzexlArr[length] = new zzexl();
                        zzfjj.m12802((zzfjs) zzexlArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzexlArr[length] = new zzexl();
                    zzfjj.m12802((zzfjs) zzexlArr[length]);
                    this.f10317 = zzexlArr;
                    continue;
                case 17:
                    this.f10315 = zzfjj.m12789();
                    continue;
                case 26:
                    int r22 = zzfjv.m12883(zzfjj, 26);
                    int length2 = this.f10316 == null ? 0 : this.f10316.length;
                    byte[][] bArr = new byte[(r22 + length2)][];
                    if (length2 != 0) {
                        System.arraycopy(this.f10316, 0, bArr, 0, length2);
                    }
                    while (length2 < bArr.length - 1) {
                        bArr[length2] = zzfjj.m12784();
                        zzfjj.m12800();
                        length2++;
                    }
                    bArr[length2] = zzfjj.m12784();
                    this.f10316 = bArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12337(zzfjk zzfjk) throws IOException {
        if (this.f10317 != null && this.f10317.length > 0) {
            for (zzexl zzexl : this.f10317) {
                if (zzexl != null) {
                    zzfjk.m12834(1, (zzfjs) zzexl);
                }
            }
        }
        if (this.f10315 != 0) {
            zzfjk.m12827(2, this.f10315);
        }
        if (this.f10316 != null && this.f10316.length > 0) {
            for (byte[] bArr : this.f10316) {
                if (bArr != null) {
                    zzfjk.m12837(3, bArr);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
