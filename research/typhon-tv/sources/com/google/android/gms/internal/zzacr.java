package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import java.util.concurrent.Callable;

final class zzacr implements Callable<zzaco> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzacq f8102;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8103;

    zzacr(zzacq zzacq, Context context) {
        this.f8102 = zzacq;
        this.f8103 = context;
    }

    public final /* synthetic */ Object call() throws Exception {
        zzaco r0;
        zzacs zzacs = (zzacs) this.f8102.f4007.get(this.f8103);
        if (zzacs != null) {
            if (!(zzacs.f8105 + ((Long) zzkb.m5481().m5595(zznh.f4949)).longValue() < zzbs.zzeo().m9243())) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f4948)).booleanValue()) {
                    r0 = new zzacp(this.f8103, zzacs.f8104).m9461();
                    this.f8102.f4007.put(this.f8103, new zzacs(this.f8102, r0));
                    return r0;
                }
            }
        }
        r0 = new zzacp(this.f8103).m9461();
        this.f8102.f4007.put(this.f8103, new zzacs(this.f8102, r0));
        return r0;
    }
}
