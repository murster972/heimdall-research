package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.api.Releasable;
import java.lang.ref.WeakReference;
import java.util.Map;

@zzzv
public abstract class zzana implements Releasable {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f4436;

    /* renamed from: 齉  reason: contains not printable characters */
    private WeakReference<zzamp> f4437;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Context f4438;

    public zzana(zzamp zzamp) {
        this.f4438 = zzamp.getContext();
        this.f4436 = zzbs.zzei().m4632(this.f4438, zzamp.m4923().f4297);
        this.f4437 = new WeakReference<>(zzamp);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m4958(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -1396664534:
                if (str.equals("badUrl")) {
                    c = 6;
                    break;
                }
                break;
            case -1347010958:
                if (str.equals("inProgress")) {
                    c = 2;
                    break;
                }
                break;
            case -918817863:
                if (str.equals("downloadTimeout")) {
                    c = 7;
                    break;
                }
                break;
            case -659376217:
                if (str.equals("contentLengthMissing")) {
                    c = 3;
                    break;
                }
                break;
            case -642208130:
                if (str.equals("playerFailed")) {
                    c = 1;
                    break;
                }
                break;
            case -354048396:
                if (str.equals("sizeExceeded")) {
                    c = 8;
                    break;
                }
                break;
            case -32082395:
                if (str.equals("externalAbort")) {
                    c = 9;
                    break;
                }
                break;
            case 96784904:
                if (str.equals("error")) {
                    c = 0;
                    break;
                }
                break;
            case 580119100:
                if (str.equals("expireFailed")) {
                    c = 5;
                    break;
                }
                break;
            case 725497484:
                if (str.equals("noCacheDir")) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
                return "internal";
            case 4:
            case 5:
                return "io";
            case 6:
            case 7:
                return "network";
            case 8:
            case 9:
                return "policy";
            default:
                return "internal";
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4961(String str, Map<String, String> map) {
        zzamp zzamp = (zzamp) this.f4437.get();
        if (zzamp != null) {
            zzamp.zza(str, map);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m4962();

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4963() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4964(String str, String str2, int i) {
        zzajr.f4285.post(new zzanc(this, str, str2, i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4965(String str, String str2, String str3, String str4) {
        zzajr.f4285.post(new zzand(this, str, str2, str3, str4));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m4966(String str);
}
