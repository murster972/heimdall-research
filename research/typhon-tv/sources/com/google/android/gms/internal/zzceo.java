package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

public final class zzceo extends zzbfm implements Result {
    public static final Parcelable.Creator<zzceo> CREATOR = new zzcep();

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzceo f9066 = new zzceo(Status.f7464);

    /* renamed from: 靐  reason: contains not printable characters */
    private final Status f9067;

    public zzceo(Status status) {
        this.f9067 = status;
    }

    public final Status s_() {
        return this.f9067;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 1, (Parcelable) s_(), i, false);
        zzbfp.m10182(parcel, r0);
    }
}
