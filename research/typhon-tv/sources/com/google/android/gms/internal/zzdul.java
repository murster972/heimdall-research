package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.interfaces.ECPublicKey;

public final class zzdul implements zzdpv {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final byte[] f10196 = new byte[0];

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzduj f10197;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzdus f10198;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzdun f10199;

    /* renamed from: 麤  reason: contains not printable characters */
    private final byte[] f10200;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f10201;

    public zzdul(ECPublicKey eCPublicKey, byte[] bArr, String str, zzdus zzdus, zzduj zzduj) throws GeneralSecurityException {
        zzdup.m12214(eCPublicKey.getW(), eCPublicKey.getParams().getCurve());
        this.f10199 = new zzdun(eCPublicKey);
        this.f10200 = bArr;
        this.f10201 = str;
        this.f10198 = zzdus;
        this.f10197 = zzduj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12206(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        zzduo r0 = this.f10199.m12207(this.f10201, this.f10200, bArr2, this.f10197.m12204(), this.f10198);
        byte[] r1 = this.f10197.m12205(r0.m12208()).m11649(bArr, f10196);
        byte[] r02 = r0.m12209();
        return ByteBuffer.allocate(r02.length + r1.length).put(r02).put(r1).array();
    }
}
