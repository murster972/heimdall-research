package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzae;
import com.google.android.gms.ads.internal.js.zzaa;
import org.json.JSONException;
import org.json.JSONObject;

final class zzud implements zzae {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzalf f10906;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzua f10907;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaa f10908;

    public zzud(zzua zzua, zzaa zzaa, zzalf zzalf) {
        this.f10907 = zzua;
        this.f10908 = zzaa;
        this.f10906 = zzalf;
    }

    public final void zzau(String str) {
        if (str == null) {
            try {
                this.f10906.m4824(new zzto());
            } catch (IllegalStateException e) {
                this.f10908.release();
                return;
            } catch (Throwable th) {
                this.f10908.release();
                throw th;
            }
        } else {
            this.f10906.m4824(new zzto(str));
        }
        this.f10908.release();
    }

    public final void zze(JSONObject jSONObject) {
        try {
            this.f10906.m4822(this.f10907.f5401.m13432(jSONObject));
        } catch (IllegalStateException e) {
        } catch (JSONException e2) {
            this.f10906.m4822(e2);
        } finally {
            this.f10908.release();
        }
    }
}
