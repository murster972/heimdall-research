package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbau extends UIController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f8592;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8593;

    public zzbau(View view, int i) {
        this.f8593 = view;
        this.f8592 = i;
        this.f8593.setEnabled(false);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9928() {
        boolean z;
        RemoteMediaClient r3 = m8226();
        if (r3 == null || !r3.m4143()) {
            this.f8593.setEnabled(false);
            return;
        }
        MediaStatus r0 = r3.m4136();
        if (r0.m7905() == 0) {
            Integer r4 = r0.m7918(r0.m7911());
            z = r4 != null && r4.intValue() < r0.m7906() + -1;
        } else {
            z = true;
        }
        if (!z || r3.m4144()) {
            this.f8593.setVisibility(this.f8592);
            this.f8593.setEnabled(false);
            return;
        }
        this.f8593.setVisibility(0);
        this.f8593.setEnabled(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9929() {
        this.f8593.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9930() {
        this.f8593.setEnabled(false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9931() {
        m9928();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9932(CastSession castSession) {
        super.m8227(castSession);
        m9928();
    }
}
