package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;

public final class zzfjj {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10521;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f10522;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f10523 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f10524;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f10525 = 64;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f10526 = 67108864;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10527;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f10528;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f10529;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f10530;

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f10531;

    private zzfjj(byte[] bArr, int i, int i2) {
        this.f10531 = bArr;
        this.f10528 = i;
        int i3 = i + i2;
        this.f10529 = i3;
        this.f10530 = i3;
        this.f10521 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m12780(int i) throws IOException {
        if (i < 0) {
            throw zzfjr.m12865();
        } else if (this.f10521 + i > this.f10523) {
            m12780(this.f10523 - this.f10521);
            throw zzfjr.m12868();
        } else if (i <= this.f10529 - this.f10521) {
            this.f10521 += i;
        } else {
            throw zzfjr.m12868();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private final void m12781() {
        this.f10529 += this.f10527;
        int i = this.f10529;
        if (i > this.f10523) {
            this.f10527 = i - this.f10523;
            this.f10529 -= this.f10527;
            return;
        }
        this.f10527 = 0;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private final byte m12782() throws IOException {
        if (this.f10521 == this.f10529) {
            throw zzfjr.m12868();
        }
        byte[] bArr = this.f10531;
        int i = this.f10521;
        this.f10521 = i + 1;
        return bArr[i];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfjj m12783(byte[] bArr, int i, int i2) {
        return new zzfjj(bArr, 0, i2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final byte[] m12784() throws IOException {
        int r1 = m12785();
        if (r1 < 0) {
            throw zzfjr.m12865();
        } else if (r1 == 0) {
            return zzfjv.f10554;
        } else {
            if (r1 > this.f10529 - this.f10521) {
                throw zzfjr.m12868();
            }
            byte[] bArr = new byte[r1];
            System.arraycopy(this.f10531, this.f10521, bArr, 0, r1);
            this.f10521 = r1 + this.f10521;
            return bArr;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int m12785() throws IOException {
        byte r0 = m12782();
        if (r0 >= 0) {
            return r0;
        }
        byte b = r0 & Byte.MAX_VALUE;
        byte r1 = m12782();
        if (r1 >= 0) {
            return b | (r1 << 7);
        }
        byte b2 = b | ((r1 & Byte.MAX_VALUE) << 7);
        byte r12 = m12782();
        if (r12 >= 0) {
            return b2 | (r12 << 14);
        }
        byte b3 = b2 | ((r12 & Byte.MAX_VALUE) << 14);
        byte r13 = m12782();
        if (r13 >= 0) {
            return b3 | (r13 << 21);
        }
        byte b4 = b3 | ((r13 & Byte.MAX_VALUE) << 21);
        byte r14 = m12782();
        byte b5 = b4 | (r14 << 28);
        if (r14 >= 0) {
            return b5;
        }
        for (int i = 0; i < 5; i++) {
            if (m12782() >= 0) {
                return b5;
            }
        }
        throw zzfjr.m12867();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final long m12786() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte r3 = m12782();
            j |= ((long) (r3 & Byte.MAX_VALUE)) << i;
            if ((r3 & 128) == 0) {
                return j;
            }
        }
        throw zzfjr.m12867();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int m12787() {
        return this.f10521 - this.f10528;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m12788() throws IOException {
        return (m12782() & 255) | ((m12782() & 255) << 8) | ((m12782() & 255) << 16) | ((m12782() & 255) << 24);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final long m12789() throws IOException {
        byte r0 = m12782();
        byte r1 = m12782();
        return ((((long) r1) & 255) << 8) | (((long) r0) & 255) | ((((long) m12782()) & 255) << 16) | ((((long) m12782()) & 255) << 24) | ((((long) m12782()) & 255) << 32) | ((((long) m12782()) & 255) << 40) | ((((long) m12782()) & 255) << 48) | ((((long) m12782()) & 255) << 56);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int m12790() {
        if (this.f10523 == Integer.MAX_VALUE) {
            return -1;
        }
        return this.f10523 - this.f10521;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m12791() throws IOException {
        int r0 = m12785();
        if (r0 < 0) {
            throw zzfjr.m12865();
        } else if (r0 > this.f10529 - this.f10521) {
            throw zzfjr.m12868();
        } else {
            String str = new String(this.f10531, this.f10521, r0, zzfjq.f10548);
            this.f10521 = r0 + this.f10521;
            return str;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m12792(int i) {
        m12794(i, this.f10522);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m12793() throws IOException {
        return m12786();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12794(int i, int i2) {
        if (i > this.f10521 - this.f10528) {
            throw new IllegalArgumentException(new StringBuilder(50).append("Position ").append(i).append(" is beyond current ").append(this.f10521 - this.f10528).toString());
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuilder(24).append("Bad position ").append(i).toString());
        } else {
            this.f10521 = this.f10528 + i;
            this.f10522 = i2;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12795(int i) throws IOException {
        int r1;
        switch (i & 7) {
            case 0:
                m12785();
                return true;
            case 1:
                m12789();
                return true;
            case 2:
                m12780(m12785());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                m12788();
                return true;
            default:
                throw new zzfjr("Protocol message tag had invalid wire type.");
        }
        do {
            r1 = m12800();
            if (r1 == 0 || !m12795(r1)) {
                m12801(((i >>> 3) << 3) | 4);
                return true;
            }
            r1 = m12800();
            m12801(((i >>> 3) << 3) | 4);
            return true;
        } while (!m12795(r1));
        m12801(((i >>> 3) << 3) | 4);
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m12796(int i) {
        this.f10523 = i;
        m12781();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m12797() throws IOException {
        return m12785() != 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12798() throws IOException {
        return m12785();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m12799(int i) throws zzfjr {
        if (i < 0) {
            throw zzfjr.m12865();
        }
        int i2 = this.f10521 + i;
        int i3 = this.f10523;
        if (i2 > i3) {
            throw zzfjr.m12868();
        }
        this.f10523 = i2;
        m12781();
        return i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12800() throws IOException {
        if (this.f10521 == this.f10529) {
            this.f10522 = 0;
            return 0;
        }
        this.f10522 = m12785();
        if (this.f10522 != 0) {
            return this.f10522;
        }
        throw new zzfjr("Protocol message contained an invalid tag (zero).");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12801(int i) throws zzfjr {
        if (this.f10522 != i) {
            throw new zzfjr("Protocol message end-group tag did not match expected tag.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12802(zzfjs zzfjs) throws IOException {
        int r0 = m12785();
        if (this.f10524 >= this.f10525) {
            throw zzfjr.m12866();
        }
        int r02 = m12799(r0);
        this.f10524++;
        zzfjs.m12876(this);
        m12801(0);
        this.f10524--;
        m12796(r02);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12803(int i, int i2) {
        if (i2 == 0) {
            return zzfjv.f10554;
        }
        byte[] bArr = new byte[i2];
        System.arraycopy(this.f10531, this.f10528 + i, bArr, 0, i2);
        return bArr;
    }
}
