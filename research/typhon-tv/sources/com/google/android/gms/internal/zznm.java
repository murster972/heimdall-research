package com.google.android.gms.internal;

import android.text.TextUtils;

@zzzv
public final class zznm {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zznk m5612(zznj zznj) {
        if (!zznj.m5605()) {
            zzagf.m4527("CsiReporterFactory: CSI is not enabled. No CSI reporter created.");
            return null;
        } else if (zznj.m5604() == null) {
            throw new IllegalArgumentException("Context can't be null. Please set up context in CsiConfiguration.");
        } else if (!TextUtils.isEmpty(zznj.m5603())) {
            return new zznk(zznj.m5604(), zznj.m5603(), zznj.m5602(), zznj.m5601());
        } else {
            throw new IllegalArgumentException("AfmaVersion can't be null or empty. Please set up afmaVersion in CsiConfiguration.");
        }
    }
}
