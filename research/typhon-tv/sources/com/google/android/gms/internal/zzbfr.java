package com.google.android.gms.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;
import java.util.ArrayList;

public final class zzbfr {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends zzbfq> T m10202(byte[] bArr, Parcelable.Creator<T> creator) {
        zzbq.m9120(creator);
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        T t = (zzbfq) creator.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends zzbfq> ArrayList<T> m10203(Intent intent, String str, Parcelable.Creator<T> creator) {
        ArrayList arrayList = (ArrayList) intent.getSerializableExtra(str);
        if (arrayList == null) {
            return null;
        }
        ArrayList<T> arrayList2 = new ArrayList<>(arrayList.size());
        ArrayList arrayList3 = arrayList;
        int size = arrayList3.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList3.get(i);
            i++;
            arrayList2.add(m10202((byte[]) obj, creator));
        }
        return arrayList2;
    }
}
