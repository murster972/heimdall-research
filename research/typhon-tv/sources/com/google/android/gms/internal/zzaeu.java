package com.google.android.gms.internal;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;

final class zzaeu implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzaes f8135;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzakv f8136;

    zzaeu(zzaes zzaes, zzakv zzakv) {
        this.f8135 = zzaes;
        this.f8136 = zzakv;
    }

    public final void run() {
        Throwable th;
        try {
            this.f8135.m4400((Map<String, String>) (Map) this.f8136.get());
            if (this.f8135.f4067) {
                synchronized (this.f8135.f4058) {
                    this.f8135.f4064.f10577 = 9;
                }
            }
            this.f8135.m4392();
            return;
        } catch (InterruptedException e) {
            th = e;
        } catch (ExecutionException e2) {
            th = e2;
        } catch (JSONException e3) {
            th = e3;
        }
        if (((Boolean) zzkb.m5481().m5595(zznh.f5014)).booleanValue()) {
            zzagf.m4797("Failed to get SafeBrowsing metadata", th);
        }
    }
}
