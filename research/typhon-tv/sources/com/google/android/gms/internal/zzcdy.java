package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.zze;
import java.util.List;

public final class zzcdy implements Parcelable.Creator<zzcdx> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        zze zze = zzcdx.f9040;
        List<zzcdv> list = zzcdx.f9041;
        String str = null;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    zze = (zze) zzbfn.m10171(parcel, readInt, zze.CREATOR);
                    break;
                case 2:
                    list = zzbfn.m10167(parcel, readInt, zzcdv.CREATOR);
                    break;
                case 3:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new zzcdx(zze, list, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcdx[i];
    }
}
