package com.google.android.gms.internal;

import java.util.concurrent.Future;

final /* synthetic */ class zzakt implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Future f8286;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzakv f8287;

    zzakt(zzakv zzakv, Future future) {
        this.f8287 = zzakv;
        this.f8286 = future;
    }

    public final void run() {
        zzakv zzakv = this.f8287;
        Future future = this.f8286;
        if (zzakv.isCancelled()) {
            future.cancel(true);
        }
    }
}
