package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclt extends zzfjm<zzclt> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile zzclt[] f9673;

    /* renamed from: 靐  reason: contains not printable characters */
    public zzclu f9674 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f9675 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Boolean f9676 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public zzclw f9677 = null;

    public zzclt() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzclt[] m11457() {
        if (f9673 == null) {
            synchronized (zzfjq.f10546) {
                if (f9673 == null) {
                    f9673 = new zzclt[0];
                }
            }
        }
        return f9673;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclt)) {
            return false;
        }
        zzclt zzclt = (zzclt) obj;
        if (this.f9677 == null) {
            if (zzclt.f9677 != null) {
                return false;
            }
        } else if (!this.f9677.equals(zzclt.f9677)) {
            return false;
        }
        if (this.f9674 == null) {
            if (zzclt.f9674 != null) {
                return false;
            }
        } else if (!this.f9674.equals(zzclt.f9674)) {
            return false;
        }
        if (this.f9676 == null) {
            if (zzclt.f9676 != null) {
                return false;
            }
        } else if (!this.f9676.equals(zzclt.f9676)) {
            return false;
        }
        if (this.f9675 == null) {
            if (zzclt.f9675 != null) {
                return false;
            }
        } else if (!this.f9675.equals(zzclt.f9675)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclt.f10533 == null || zzclt.f10533.m12849() : this.f10533.equals(zzclt.f10533);
    }

    public final int hashCode() {
        int i = 0;
        zzclw zzclw = this.f9677;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        int hashCode2 = zzclw == null ? 0 : zzclw.hashCode();
        zzclu zzclu = this.f9674;
        int hashCode3 = ((this.f9675 == null ? 0 : this.f9675.hashCode()) + (((this.f9676 == null ? 0 : this.f9676.hashCode()) + (((zzclu == null ? 0 : zzclu.hashCode()) + ((hashCode2 + hashCode) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode3 + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11458() {
        int r0 = super.m12841();
        if (this.f9677 != null) {
            r0 += zzfjk.m12807(1, (zzfjs) this.f9677);
        }
        if (this.f9674 != null) {
            r0 += zzfjk.m12807(2, (zzfjs) this.f9674);
        }
        if (this.f9676 != null) {
            this.f9676.booleanValue();
            r0 += zzfjk.m12805(3) + 1;
        }
        return this.f9675 != null ? r0 + zzfjk.m12808(4, this.f9675) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11459(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    if (this.f9677 == null) {
                        this.f9677 = new zzclw();
                    }
                    zzfjj.m12802((zzfjs) this.f9677);
                    continue;
                case 18:
                    if (this.f9674 == null) {
                        this.f9674 = new zzclu();
                    }
                    zzfjj.m12802((zzfjs) this.f9674);
                    continue;
                case 24:
                    this.f9676 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 34:
                    this.f9675 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11460(zzfjk zzfjk) throws IOException {
        if (this.f9677 != null) {
            zzfjk.m12834(1, (zzfjs) this.f9677);
        }
        if (this.f9674 != null) {
            zzfjk.m12834(2, (zzfjs) this.f9674);
        }
        if (this.f9676 != null) {
            zzfjk.m12836(3, this.f9676.booleanValue());
        }
        if (this.f9675 != null) {
            zzfjk.m12835(4, this.f9675);
        }
        super.m12842(zzfjk);
    }
}
