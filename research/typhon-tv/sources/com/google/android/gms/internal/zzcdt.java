package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzr;

public class zzcdt extends zzab<zzcez> {

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f9035;

    /* renamed from: 麤  reason: contains not printable characters */
    protected final zzcfu<zzcez> f9036 = new zzcdu(this);

    public zzcdt(Context context, Looper looper, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener, String str, zzr zzr) {
        super(context, looper, 23, zzr, connectionCallbacks, onConnectionFailedListener);
        this.f9035 = str;
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public final Bundle m10333() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.f9035);
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10334() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m10335(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof zzcez ? (zzcez) queryLocalInterface : new zzcfa(iBinder);
    }
}
