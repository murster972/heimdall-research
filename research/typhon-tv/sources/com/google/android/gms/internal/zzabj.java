package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzn;
import java.io.DataInputStream;
import java.io.IOException;

@zzzv
public final class zzabj extends zzbfm {
    public static final Parcelable.Creator<zzabj> CREATOR = new zzabl();

    /* renamed from: 靐  reason: contains not printable characters */
    private Parcelable f3868;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f3869;

    /* renamed from: 龘  reason: contains not printable characters */
    private ParcelFileDescriptor f3870;

    public zzabj(ParcelFileDescriptor parcelFileDescriptor) {
        this.f3870 = parcelFileDescriptor;
        this.f3868 = null;
        this.f3869 = true;
    }

    public zzabj(zzbfq zzbfq) {
        this.f3870 = null;
        this.f3868 = zzbfq;
        this.f3869 = false;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    private final ParcelFileDescriptor m4262() {
        if (this.f3870 == null) {
            Parcel obtain = Parcel.obtain();
            try {
                this.f3868.writeToParcel(obtain, 0);
                byte[] marshall = obtain.marshall();
                obtain.recycle();
                this.f3870 = m4263(marshall);
            } catch (Throwable th) {
                obtain.recycle();
                throw th;
            }
        }
        return this.f3870;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final <T> ParcelFileDescriptor m4263(byte[] bArr) {
        ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream;
        try {
            ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
            autoCloseOutputStream = new ParcelFileDescriptor.AutoCloseOutputStream(createPipe[1]);
            try {
                new Thread(new zzabk(this, autoCloseOutputStream, bArr)).start();
                return createPipe[0];
            } catch (IOException e) {
                e = e;
            }
        } catch (IOException e2) {
            e = e2;
            autoCloseOutputStream = null;
            zzagf.m4793("Error transporting the ad response", e);
            zzbs.zzem().m4505((Throwable) e, "LargeParcelTeleporter.pipeData.2");
            zzn.m9261(autoCloseOutputStream);
            return null;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        m4262();
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f3870, i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends zzbfq> T m4264(Parcelable.Creator<T> creator) {
        if (this.f3869) {
            if (this.f3870 == null) {
                zzagf.m4795("File descriptor is empty, returning null.");
                return null;
            }
            DataInputStream dataInputStream = new DataInputStream(new ParcelFileDescriptor.AutoCloseInputStream(this.f3870));
            try {
                byte[] bArr = new byte[dataInputStream.readInt()];
                dataInputStream.readFully(bArr, 0, bArr.length);
                zzn.m9261(dataInputStream);
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.unmarshall(bArr, 0, bArr.length);
                    obtain.setDataPosition(0);
                    this.f3868 = (zzbfq) creator.createFromParcel(obtain);
                    obtain.recycle();
                    this.f3869 = false;
                } catch (Throwable th) {
                    obtain.recycle();
                    throw th;
                }
            } catch (IOException e) {
                zzagf.m4793("Could not read from parcel file descriptor", e);
                zzn.m9261(dataInputStream);
                return null;
            } catch (Throwable th2) {
                zzn.m9261(dataInputStream);
                throw th2;
            }
        }
        return (zzbfq) this.f3868;
    }
}
