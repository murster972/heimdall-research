package com.google.android.gms.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class zzckr implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ zzcgi f9593;

    /* renamed from: ʼ  reason: contains not printable characters */
    private /* synthetic */ zzckg f9594;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ boolean f9595;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f9596;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ String f9597;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f9598;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AtomicReference f9599;

    zzckr(zzckg zzckg, AtomicReference atomicReference, String str, String str2, String str3, boolean z, zzcgi zzcgi) {
        this.f9594 = zzckg;
        this.f9599 = atomicReference;
        this.f9596 = str;
        this.f9598 = str2;
        this.f9597 = str3;
        this.f9595 = z;
        this.f9593 = zzcgi;
    }

    public final void run() {
        synchronized (this.f9599) {
            try {
                zzche r0 = this.f9594.f9558;
                if (r0 == null) {
                    this.f9594.m11096().m10832().m10852("Failed to get user properties", zzchm.m10812(this.f9596), this.f9598, this.f9597);
                    this.f9599.set(Collections.emptyList());
                    this.f9599.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f9596)) {
                    this.f9599.set(r0.m10681(this.f9598, this.f9597, this.f9595, this.f9593));
                } else {
                    this.f9599.set(r0.m10680(this.f9596, this.f9598, this.f9597, this.f9595));
                }
                this.f9594.m11223();
                this.f9599.notify();
            } catch (RemoteException e) {
                this.f9594.m11096().m10832().m10852("Failed to get user properties", zzchm.m10812(this.f9596), this.f9598, e);
                this.f9599.set(Collections.emptyList());
                this.f9599.notify();
            } catch (Throwable th) {
                this.f9599.notify();
                throw th;
            }
        }
    }
}
