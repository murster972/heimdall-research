package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsw;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsu extends zzffu<zzdsu, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdsu f10070;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsu> f10071;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10072;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdsw f10073;

    public static final class zza extends zzffu.zza<zzdsu, zza> implements zzfhg {
        private zza() {
            super(zzdsu.f10070);
        }

        /* synthetic */ zza(zzdsv zzdsv) {
            this();
        }
    }

    static {
        zzdsu zzdsu = new zzdsu();
        f10070 = zzdsu;
        zzdsu.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsu.f10394.m12718();
    }

    private zzdsu() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zzdsu m11992() {
        return f10070;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdsu m11993(zzfes zzfes) throws zzfge {
        return (zzdsu) zzffu.m12526(f10070, zzfes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m11994() {
        return this.f10072;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11995() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10073 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f10073 == null ? zzdsw.m12000() : this.f10073) + 0;
        }
        if (this.f10072 != 0) {
            i2 += zzffg.m5245(2, this.f10072);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdsw m11996() {
        return this.f10073 == null ? zzdsw.m12000() : this.f10073;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11997(int i, Object obj, Object obj2) {
        zzdsw.zza zza2;
        boolean z = true;
        switch (zzdsv.f10074[i - 1]) {
            case 1:
                return new zzdsu();
            case 2:
                return f10070;
            case 3:
                return null;
            case 4:
                return new zza((zzdsv) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdsu zzdsu = (zzdsu) obj2;
                this.f10073 = (zzdsw) zzh.m12572(this.f10073, zzdsu.f10073);
                boolean z2 = this.f10072 != 0;
                int i2 = this.f10072;
                if (zzdsu.f10072 == 0) {
                    z = false;
                }
                this.f10072 = zzh.m12569(z2, i2, z, zzdsu.f10072);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 10:
                                    if (this.f10073 != null) {
                                        zzdsw zzdsw = this.f10073;
                                        zzffu.zza zza3 = (zzffu.zza) zzdsw.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdsw);
                                        zza2 = (zzdsw.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10073 = (zzdsw) zzffb.m12416(zzdsw.m12000(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10073);
                                        this.f10073 = (zzdsw) zza2.m12543();
                                        break;
                                    }
                                case 16:
                                    this.f10072 = zzffb.m12402();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10071 == null) {
                    synchronized (zzdsu.class) {
                        if (f10071 == null) {
                            f10071 = new zzffu.zzb(f10070);
                        }
                    }
                }
                return f10071;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10070;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11998(zzffg zzffg) throws IOException {
        if (this.f10073 != null) {
            zzffg.m5289(1, (zzfhe) this.f10073 == null ? zzdsw.m12000() : this.f10073);
        }
        if (this.f10072 != 0) {
            zzffg.m5281(2, this.f10072);
        }
        this.f10394.m12719(zzffg);
    }
}
