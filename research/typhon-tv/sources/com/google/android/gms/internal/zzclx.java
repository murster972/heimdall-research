package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclx extends zzfjm<zzclx> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile zzclx[] f9691;

    /* renamed from: 靐  reason: contains not printable characters */
    public Boolean f9692 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer f9693 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Boolean f9694 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f9695 = null;

    public zzclx() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzclx[] m11473() {
        if (f9691 == null) {
            synchronized (zzfjq.f10546) {
                if (f9691 == null) {
                    f9691 = new zzclx[0];
                }
            }
        }
        return f9691;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclx)) {
            return false;
        }
        zzclx zzclx = (zzclx) obj;
        if (this.f9695 == null) {
            if (zzclx.f9695 != null) {
                return false;
            }
        } else if (!this.f9695.equals(zzclx.f9695)) {
            return false;
        }
        if (this.f9692 == null) {
            if (zzclx.f9692 != null) {
                return false;
            }
        } else if (!this.f9692.equals(zzclx.f9692)) {
            return false;
        }
        if (this.f9694 == null) {
            if (zzclx.f9694 != null) {
                return false;
            }
        } else if (!this.f9694.equals(zzclx.f9694)) {
            return false;
        }
        if (this.f9693 == null) {
            if (zzclx.f9693 != null) {
                return false;
            }
        } else if (!this.f9693.equals(zzclx.f9693)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclx.f10533 == null || zzclx.f10533.m12849() : this.f10533.equals(zzclx.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f9693 == null ? 0 : this.f9693.hashCode()) + (((this.f9694 == null ? 0 : this.f9694.hashCode()) + (((this.f9692 == null ? 0 : this.f9692.hashCode()) + (((this.f9695 == null ? 0 : this.f9695.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11474() {
        int r0 = super.m12841();
        if (this.f9695 != null) {
            r0 += zzfjk.m12808(1, this.f9695);
        }
        if (this.f9692 != null) {
            this.f9692.booleanValue();
            r0 += zzfjk.m12805(2) + 1;
        }
        if (this.f9694 != null) {
            this.f9694.booleanValue();
            r0 += zzfjk.m12805(3) + 1;
        }
        return this.f9693 != null ? r0 + zzfjk.m12806(4, this.f9693.intValue()) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11475(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f9695 = zzfjj.m12791();
                    continue;
                case 16:
                    this.f9692 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 24:
                    this.f9694 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 32:
                    this.f9693 = Integer.valueOf(zzfjj.m12785());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11476(zzfjk zzfjk) throws IOException {
        if (this.f9695 != null) {
            zzfjk.m12835(1, this.f9695);
        }
        if (this.f9692 != null) {
            zzfjk.m12836(2, this.f9692.booleanValue());
        }
        if (this.f9694 != null) {
            zzfjk.m12836(3, this.f9694.booleanValue());
        }
        if (this.f9693 != null) {
            zzfjk.m12832(4, this.f9693.intValue());
        }
        super.m12842(zzfjk);
    }
}
