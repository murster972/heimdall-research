package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfkf extends zzfjm<zzfkf> {

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f10614 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public Boolean f10615 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10616 = null;

    public zzfkf() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12916() {
        int r0 = super.m12841();
        if (this.f10616 != null) {
            r0 += zzfjk.m12808(1, this.f10616);
        }
        if (this.f10614 != null) {
            r0 += zzfjk.m12814(2, this.f10614.longValue());
        }
        if (this.f10615 == null) {
            return r0;
        }
        this.f10615.booleanValue();
        return r0 + zzfjk.m12805(3) + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12917(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10616 = zzfjj.m12791();
                    continue;
                case 16:
                    this.f10614 = Long.valueOf(zzfjj.m12793());
                    continue;
                case 24:
                    this.f10615 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12918(zzfjk zzfjk) throws IOException {
        if (this.f10616 != null) {
            zzfjk.m12835(1, this.f10616);
        }
        if (this.f10614 != null) {
            zzfjk.m12824(2, this.f10614.longValue());
        }
        if (this.f10615 != null) {
            zzfjk.m12836(3, this.f10615.booleanValue());
        }
        super.m12842(zzfjk);
    }
}
