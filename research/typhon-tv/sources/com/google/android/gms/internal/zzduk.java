package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.interfaces.ECPrivateKey;

public final class zzduk implements zzdpu {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final byte[] f10189 = new byte[0];

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzdus f10190;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzduj f10191;

    /* renamed from: 连任  reason: contains not printable characters */
    private final byte[] f10192;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ECPrivateKey f10193;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f10194;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzdum f10195;

    public zzduk(ECPrivateKey eCPrivateKey, byte[] bArr, String str, zzdus zzdus, zzduj zzduj) throws GeneralSecurityException {
        this.f10193 = eCPrivateKey;
        this.f10195 = new zzdum(eCPrivateKey);
        this.f10192 = bArr;
        this.f10194 = str;
        this.f10190 = zzdus;
        this.f10191 = zzduj;
    }
}
