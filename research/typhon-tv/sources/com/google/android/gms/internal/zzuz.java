package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzuz extends zzeu implements zzux {
    zzuz(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13450(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r0 = m12300(2, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzva m13451(String str) throws RemoteException {
        zzva zzvc;
        Parcel v_ = v_();
        v_.writeString(str);
        Parcel r1 = m12300(1, v_);
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzvc = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
            zzvc = queryLocalInterface instanceof zzva ? (zzva) queryLocalInterface : new zzvc(readStrongBinder);
        }
        r1.recycle();
        return zzvc;
    }
}
