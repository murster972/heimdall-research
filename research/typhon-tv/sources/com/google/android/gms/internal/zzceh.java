package com.google.android.gms.internal;

import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.api.internal.zzcm;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

final class zzceh extends zzcem {

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ Looper f9055;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ LocationListener f9056;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationRequest f9057;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzceh(zzceb zzceb, GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener, Looper looper) {
        super(googleApiClient);
        this.f9057 = locationRequest;
        this.f9056 = locationListener;
        this.f9055 = looper;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10344(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10413(this.f9057, (zzci<LocationListener>) zzcm.m8858(this.f9056, zzcgc.m10426(this.f9055), LocationListener.class.getSimpleName()), (zzceu) new zzcen(this));
    }
}
