package com.google.android.gms.internal;

import android.view.View;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;

@zzzv
abstract class zzalq {

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<View> f4321;

    public zzalq(View view) {
        this.f4321 = new WeakReference<>(view);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final ViewTreeObserver m4832() {
        View view = (View) this.f4321.get();
        if (view == null) {
            return null;
        }
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver == null || !viewTreeObserver.isAlive()) {
            return null;
        }
        return viewTreeObserver;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4833() {
        ViewTreeObserver r0 = m4832();
        if (r0 != null) {
            m4834(r0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m4834(ViewTreeObserver viewTreeObserver);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4835() {
        ViewTreeObserver r0 = m4832();
        if (r0 != null) {
            m4836(r0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m4836(ViewTreeObserver viewTreeObserver);
}
