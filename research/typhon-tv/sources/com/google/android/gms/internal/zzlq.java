package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzlq extends zzeu implements zzlo {
    zzlq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13112() throws RemoteException {
        m12298(2, v_());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13113() throws RemoteException {
        m12298(4, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13114() throws RemoteException {
        m12298(3, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13115() throws RemoteException {
        m12298(1, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13116(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(5, v_);
    }
}
