package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.webkit.WebSettings;
import java.util.concurrent.Callable;

final class zzajm implements Callable<String> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Context f8256;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8257;

    zzajm(zzajl zzajl, Context context, Context context2) {
        this.f8257 = context;
        this.f8256 = context2;
    }

    public final /* synthetic */ Object call() throws Exception {
        SharedPreferences sharedPreferences;
        boolean z = false;
        if (this.f8257 != null) {
            zzagf.m4527("Attempting to read user agent from Google Play Services.");
            sharedPreferences = this.f8257.getSharedPreferences("admob_user_agent", 0);
        } else {
            zzagf.m4527("Attempting to read user agent from local cache.");
            sharedPreferences = this.f8256.getSharedPreferences("admob_user_agent", 0);
            z = true;
        }
        String string = sharedPreferences.getString("user_agent", "");
        if (TextUtils.isEmpty(string)) {
            zzagf.m4527("Reading user agent from WebSettings");
            string = WebSettings.getDefaultUserAgent(this.f8256);
            if (z) {
                sharedPreferences.edit().putString("user_agent", string).apply();
                zzagf.m4527("Persisting user agent.");
            }
        }
        return string;
    }
}
