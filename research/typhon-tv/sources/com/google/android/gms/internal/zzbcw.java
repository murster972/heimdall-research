package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzbcw extends zzeu implements zzbcv {
    zzbcw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.internal.ICastService");
    }
}
