package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzaem extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m9539(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    void m9540(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    void m9541(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m9542(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m9543(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m9544(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m9545(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9546(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9547(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9548(IObjectWrapper iObjectWrapper, zzaeq zzaeq) throws RemoteException;
}
