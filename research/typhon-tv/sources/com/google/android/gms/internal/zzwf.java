package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzwf implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10941;

    zzwf(zzvx zzvx) {
        this.f10941 = zzvx;
    }

    public final void run() {
        try {
            this.f10941.f5502.m13509();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLoaded.", e);
        }
    }
}
