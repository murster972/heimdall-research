package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

@zzzv
public final class zzrn extends zzra {

    /* renamed from: 龘  reason: contains not printable characters */
    private final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener f5336;

    public zzrn(NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener) {
        this.f5336 = onCustomTemplateAdLoadedListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5805(zzqm zzqm) {
        this.f5336.onCustomTemplateAdLoaded(zzqp.m5796(zzqm));
    }
}
