package com.google.android.gms.internal;

import java.util.concurrent.Callable;

public final class zzee implements Callable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzaz f10258;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzdm f10259;

    public zzee(zzdm zzdm, zzaz zzaz) {
        this.f10259 = zzdm;
        this.f10258 = zzaz;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Void call() throws Exception {
        if (this.f10259.m11614() != null) {
            this.f10259.m11614().get();
        }
        zzaz r0 = this.f10259.m11617();
        if (r0 == null) {
            return null;
        }
        try {
            synchronized (this.f10258) {
                zzfjs.m12869(this.f10258, zzfjs.m12871((zzfjs) r0));
            }
            return null;
        } catch (zzfjr e) {
            return null;
        }
    }
}
