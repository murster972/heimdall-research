package com.google.android.gms.internal;

import org.apache.commons.lang3.StringUtils;

final class zzaiz implements zzx {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzajc f8242;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f8243;

    zzaiz(zzaiv zzaiv, String str, zzajc zzajc) {
        this.f8243 = str;
        this.f8242 = zzajc;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9645(zzad zzad) {
        String str = this.f8243;
        String zzad2 = zzad.toString();
        zzagf.m4791(new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(zzad2).length()).append("Failed to load URL: ").append(str).append(StringUtils.LF).append(zzad2).toString());
        this.f8242.m9653(null);
    }
}
