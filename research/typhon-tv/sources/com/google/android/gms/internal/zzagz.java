package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagz extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8189;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8190;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagz(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8190 = context;
        this.f8189 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9598() {
        SharedPreferences sharedPreferences = this.f8190.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putLong("first_ad_req_time_ms", sharedPreferences.getLong("first_ad_req_time_ms", 0));
        if (this.f8189 != null) {
            this.f8189.m9605(bundle);
        }
    }
}
