package com.google.android.gms.internal;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;

final class zzdqq implements zzdpw<zzdpv> {
    zzdqq() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdpv m11741(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdsk r0 = zzdsk.m11947(zzfes);
            if (!(r0 instanceof zzdsk)) {
                throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
            }
            zzdsk zzdsk = r0;
            zzdvk.m12238(zzdsk.m11957(), 0);
            zzdqv.m11749(zzdsk.m11954());
            zzdsg r4 = zzdsk.m11954();
            zzdsm r3 = r4.m11920();
            zzdur r1 = zzdqv.m11746(r3.m11969());
            byte[] byteArray = zzdsk.m11956().toByteArray();
            byte[] byteArray2 = zzdsk.m11953().toByteArray();
            ECParameterSpec r12 = zzdup.m12212(r1);
            ECPoint eCPoint = new ECPoint(new BigInteger(1, byteArray), new BigInteger(1, byteArray2));
            zzdup.m12214(eCPoint, r12.getCurve());
            ECPublicKeySpec eCPublicKeySpec = new ECPublicKeySpec(eCPoint, r12);
            return new zzdul((ECPublicKey) KeyFactory.getInstance("EC").generatePublic(eCPublicKeySpec), r3.m11968().toByteArray(), zzdqv.m11748(r3.m11966()), zzdqv.m11747(r4.m11919()), new zzdqx(r4.m11917().m11906()));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPublicKey proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11738(zzfes zzfes) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11739(zzfhe zzfhe) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11740(zzfes zzfes) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11742(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdsk)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
        }
        zzdsk zzdsk = (zzdsk) zzfhe;
        zzdvk.m12238(zzdsk.m11957(), 0);
        zzdqv.m11749(zzdsk.m11954());
        zzdsg r4 = zzdsk.m11954();
        zzdsm r3 = r4.m11920();
        zzdur r0 = zzdqv.m11746(r3.m11969());
        byte[] byteArray = zzdsk.m11956().toByteArray();
        byte[] byteArray2 = zzdsk.m11953().toByteArray();
        ECParameterSpec r02 = zzdup.m12212(r0);
        ECPoint eCPoint = new ECPoint(new BigInteger(1, byteArray), new BigInteger(1, byteArray2));
        zzdup.m12214(eCPoint, r02.getCurve());
        ECPublicKeySpec eCPublicKeySpec = new ECPublicKeySpec(eCPoint, r02);
        return new zzdul((ECPublicKey) KeyFactory.getInstance("EC").generatePublic(eCPublicKeySpec), r3.m11968().toByteArray(), zzdqv.m11748(r3.m11966()), zzdqv.m11747(r4.m11919()), new zzdqx(r4.m11917().m11906()));
    }
}
