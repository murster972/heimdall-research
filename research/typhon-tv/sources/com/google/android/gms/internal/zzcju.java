package com.google.android.gms.internal;

final class zzcju implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcjn f9514;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ long f9515;

    zzcju(zzcjn zzcjn, long j) {
        this.f9514 = zzcjn;
        this.f9515 = j;
    }

    public final void run() {
        this.f9514.m11098().f9316.m10903(this.f9515);
        this.f9514.m11096().m10845().m10850("Session timeout duration set", Long.valueOf(this.f9515));
    }
}
