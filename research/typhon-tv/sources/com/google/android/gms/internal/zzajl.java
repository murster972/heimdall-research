package com.google.android.gms.internal;

import android.annotation.TargetApi;

@zzzv
@TargetApi(17)
public final class zzajl {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzajl f4262 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    String f4263;

    private zzajl() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzajl m4729() {
        if (f4262 == null) {
            f4262 = new zzajl();
        }
        return f4262;
    }
}
