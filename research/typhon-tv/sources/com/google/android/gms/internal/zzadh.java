package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzadh extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    int m9465() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m9466() throws RemoteException;
}
