package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzpv extends zzev implements zzpu {
    public zzpv() {
        attachInterface(this, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzpu m13235(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
        return queryLocalInterface instanceof zzpu ? (zzpu) queryLocalInterface : new zzpw(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m13234(parcel.readString(), IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 2:
                IObjectWrapper r1 = m13230(parcel.readString());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r1);
                return true;
            case 3:
                m13232(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 4:
                m13231();
                parcel2.writeNoException();
                return true;
            case 5:
                m13233(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
