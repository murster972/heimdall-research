package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement$zzb;

final class zzckd implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ AppMeasurement$zzb f9548;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzckc f9549;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzckf f9550;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ boolean f9551;

    zzckd(zzckc zzckc, boolean z, AppMeasurement$zzb appMeasurement$zzb, zzckf zzckf) {
        this.f9549 = zzckc;
        this.f9551 = z;
        this.f9548 = appMeasurement$zzb;
        this.f9550 = zzckf;
    }

    public final void run() {
        if (this.f9551 && this.f9549.f9547 != null) {
            this.f9549.m11186(this.f9549.f9547);
        }
        if (this.f9548 == null || this.f9548.f11089 != this.f9550.f11089 || !zzclq.m11387(this.f9548.f11090, this.f9550.f11090) || !zzclq.m11387(this.f9548.f11088, this.f9550.f11088)) {
            Bundle bundle = new Bundle();
            zzckc.m11187((AppMeasurement$zzb) this.f9550, bundle);
            if (this.f9548 != null) {
                if (this.f9548.f11088 != null) {
                    bundle.putString("_pn", this.f9548.f11088);
                }
                bundle.putString("_pc", this.f9548.f11090);
                bundle.putLong("_pi", this.f9548.f11089);
            }
            this.f9549.m11091().m11174("auto", "_vs", bundle);
        }
        this.f9549.f9547 = this.f9550;
        this.f9549.m11103().m11266((AppMeasurement$zzb) this.f9550);
    }
}
