package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public final class zzp {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f5276;

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean f5277;

    /* renamed from: 靐  reason: contains not printable characters */
    public final byte[] f5278;

    /* renamed from: 麤  reason: contains not printable characters */
    public final List<zzl> f5279;

    /* renamed from: 齉  reason: contains not printable characters */
    public final Map<String, String> f5280;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f5281;

    private zzp(int i, byte[] bArr, Map<String, String> map, List<zzl> list, boolean z, long j) {
        this.f5281 = i;
        this.f5278 = bArr;
        this.f5280 = map;
        if (list == null) {
            this.f5279 = null;
        } else {
            this.f5279 = Collections.unmodifiableList(list);
        }
        this.f5277 = z;
        this.f5276 = j;
    }

    @Deprecated
    public zzp(int i, byte[] bArr, Map<String, String> map, boolean z, long j) {
        this(i, bArr, map, m5766(map), z, j);
    }

    public zzp(int i, byte[] bArr, boolean z, long j, List<zzl> list) {
        this(i, bArr, m5767(list), list, z, j);
    }

    @Deprecated
    public zzp(byte[] bArr, Map<String, String> map) {
        this(200, bArr, map, false, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<zzl> m5766(Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(new zzl((String) next.getKey(), (String) next.getValue()));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<String, String> m5767(List<zzl> list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (zzl next : list) {
            treeMap.put(next.m13077(), next.m13076());
        }
        return treeMap;
    }
}
