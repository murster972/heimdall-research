package com.google.android.gms.internal;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

final class zzyr implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f10992;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ List f10993;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzalf f10994;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AtomicInteger f10995;

    zzyr(AtomicInteger atomicInteger, int i, zzalf zzalf, List list) {
        this.f10995 = atomicInteger;
        this.f10992 = i;
        this.f10994 = zzalf;
        this.f10993 = list;
    }

    public final void run() {
        if (this.f10995.incrementAndGet() >= this.f10992) {
            try {
                this.f10994.m4822(zzym.m6044(this.f10993));
            } catch (InterruptedException | ExecutionException e) {
                zzagf.m4796("Unable to convert list of futures to a future of list", e);
            }
        }
    }
}
