package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzjo implements Parcelable.Creator<zzjn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r12 = zzbfn.m10169(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        zzjn[] zzjnArr = null;
        int i = 0;
        int i2 = 0;
        boolean z4 = false;
        int i3 = 0;
        int i4 = 0;
        String str = null;
        while (parcel.dataPosition() < r12) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    i4 = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    z4 = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 7:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 8:
                    zzjnArr = (zzjn[]) zzbfn.m10165(parcel, readInt, zzjn.CREATOR);
                    break;
                case 9:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 10:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 11:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r12);
        return new zzjn(str, i4, i3, z4, i2, i, zzjnArr, z3, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzjn[i];
    }
}
