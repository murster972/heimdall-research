package com.google.android.gms.internal;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzol extends zzqj implements zzov {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f5236;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzog f5237;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Bundle f5238;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Object f5239 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public zzos f5240;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f5241;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzll f5242;

    /* renamed from: ٴ  reason: contains not printable characters */
    private View f5243;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private IObjectWrapper f5244;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f5245;

    /* renamed from: 靐  reason: contains not printable characters */
    private List<zzoi> f5246;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzpq f5247;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f5248;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f5249;

    public zzol(String str, List<zzoi> list, String str2, zzpq zzpq, String str3, String str4, zzog zzog, Bundle bundle, zzll zzll, View view, IObjectWrapper iObjectWrapper, String str5) {
        this.f5249 = str;
        this.f5246 = list;
        this.f5248 = str2;
        this.f5247 = zzpq;
        this.f5245 = str3;
        this.f5236 = str4;
        this.f5237 = zzog;
        this.f5238 = bundle;
        this.f5242 = zzll;
        this.f5243 = view;
        this.f5244 = iObjectWrapper;
        this.f5241 = str5;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzpq m5680() {
        return this.f5247;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m5681() {
        return this.f5245;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m5682() {
        return this.f5236;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzog m5683() {
        return this.f5237;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Bundle m5684() {
        return this.f5238;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m5685() {
        return "";
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m5686() {
        zzahn.f4212.post(new zzom(this));
        this.f5249 = null;
        this.f5246 = null;
        this.f5248 = null;
        this.f5247 = null;
        this.f5245 = null;
        this.f5236 = null;
        this.f5237 = null;
        this.f5238 = null;
        this.f5239 = null;
        this.f5242 = null;
        this.f5243 = null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzll m5687() {
        return this.f5242;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final IObjectWrapper m5688() {
        return zzn.m9306(this.f5240);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m5689() {
        return PubnativeRequest.LEGACY_ZONE_ID;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m5690() {
        return this.f5248;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m5691() {
        return this.f5246;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5692(Bundle bundle) {
        boolean r0;
        synchronized (this.f5239) {
            if (this.f5240 == null) {
                zzagf.m4795("Attempt to record impression before content ad initialized.");
                r0 = false;
            } else {
                r0 = this.f5240.m13202(bundle);
            }
        }
        return r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m5693() {
        return this.f5241;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final IObjectWrapper m5694() {
        return this.f5244;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5695(Bundle bundle) {
        synchronized (this.f5239) {
            if (this.f5240 == null) {
                zzagf.m4795("Attempt to perform click before app install ad initialized.");
            } else {
                this.f5240.m13192(bundle);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5696() {
        return this.f5249;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5697(Bundle bundle) {
        synchronized (this.f5239) {
            if (this.f5240 == null) {
                zzagf.m4795("Attempt to perform click before content ad initialized.");
            } else {
                this.f5240.m13188(bundle);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5698(zzos zzos) {
        synchronized (this.f5239) {
            this.f5240 = zzos;
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final View m5699() {
        return this.f5243;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final zzpm m5700() {
        return this.f5237;
    }
}
