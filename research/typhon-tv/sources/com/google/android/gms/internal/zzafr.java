package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.SystemClock;

@zzzv
final class zzafr {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f4149 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    private long f4150 = -1;

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4452() {
        this.f4149 = SystemClock.elapsedRealtime();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Bundle m4453() {
        Bundle bundle = new Bundle();
        bundle.putLong("topen", this.f4150);
        bundle.putLong("tclose", this.f4149);
        return bundle;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4454() {
        this.f4150 = SystemClock.elapsedRealtime();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m4455() {
        return this.f4149;
    }
}
