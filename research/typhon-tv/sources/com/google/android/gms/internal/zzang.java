package com.google.android.gms.internal;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzzv
public final class zzang extends zzana {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Set<String> f4440 = Collections.synchronizedSet(new HashSet());

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DecimalFormat f4441 = new DecimalFormat("#,###");

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4442;

    /* renamed from: 麤  reason: contains not printable characters */
    private File f4443;

    public zzang(zzamp zzamp) {
        super(zzamp);
        File cacheDir = this.f4438.getCacheDir();
        if (cacheDir == null) {
            zzagf.m4791("Context.getCacheDir() returned null");
            return;
        }
        this.f4443 = new File(cacheDir, "admobVideoStreams");
        if (!this.f4443.isDirectory() && !this.f4443.mkdirs()) {
            String valueOf = String.valueOf(this.f4443.getAbsolutePath());
            zzagf.m4791(valueOf.length() != 0 ? "Could not create preload cache directory at ".concat(valueOf) : new String("Could not create preload cache directory at "));
            this.f4443 = null;
        } else if (!this.f4443.setReadable(true, false) || !this.f4443.setExecutable(true, false)) {
            String valueOf2 = String.valueOf(this.f4443.getAbsolutePath());
            zzagf.m4791(valueOf2.length() != 0 ? "Could not set cache file permissions at ".concat(valueOf2) : new String("Could not set cache file permissions at "));
            this.f4443 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final File m4968(File file) {
        return new File(this.f4443, String.valueOf(file.getName()).concat(".done"));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4969() {
        this.f4442 = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r7 = r3.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0267, code lost:
        if (r7 >= 0) goto L_0x029f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0269, code lost:
        r2 = java.lang.String.valueOf(r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0274, code lost:
        if (r2.length() == 0) goto L_0x0294;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0276, code lost:
        r2 = "Stream cache aborted, missing content-length header at ".concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x027a, code lost:
        com.google.android.gms.internal.zzagf.m4791(r2);
        m4965(r27, r13.getAbsolutePath(), "contentLengthMissing", (java.lang.String) null);
        f4440.remove(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0294, code lost:
        r2 = new java.lang.String("Stream cache aborted, missing content-length header at ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x029a, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x029b, code lost:
        r3 = null;
        r4 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x029f, code lost:
        r4 = f4441.format((long) r7);
        r15 = ((java.lang.Integer) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f5143)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x02ba, code lost:
        if (r7 <= r15) goto L_0x0325;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x02bc, code lost:
        com.google.android.gms.internal.zzagf.m4791(new java.lang.StringBuilder((java.lang.String.valueOf(r4).length() + 33) + java.lang.String.valueOf(r27).length()).append("Content length ").append(r4).append(" exceeds limit at ").append(r27).toString());
        r2 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02fe, code lost:
        if (r2.length() == 0) goto L_0x031a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0300, code lost:
        r2 = "File too big for full file cache. Size: ".concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0304, code lost:
        m4965(r27, r13.getAbsolutePath(), "sizeExceeded", r2);
        f4440.remove(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x031a, code lost:
        r2 = new java.lang.String("File too big for full file cache. Size: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0320, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0321, code lost:
        r3 = null;
        r4 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0325, code lost:
        com.google.android.gms.internal.zzagf.m4792(new java.lang.StringBuilder((java.lang.String.valueOf(r4).length() + 20) + java.lang.String.valueOf(r27).length()).append("Caching ").append(r4).append(" bytes from ").append(r27).toString());
        r16 = java.nio.channels.Channels.newChannel(r3.getInputStream());
        r12 = new java.io.FileOutputStream(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r17 = r12.getChannel();
        r18 = java.nio.ByteBuffer.allocate(1048576);
        r19 = com.google.android.gms.ads.internal.zzbs.zzeo();
        r6 = 0;
        r20 = r19.m9243();
        r0 = new com.google.android.gms.internal.zzaji(((java.lang.Long) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4996)).longValue());
        r24 = ((java.lang.Long) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4970)).longValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x03a3, code lost:
        r2 = r16.read(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x03ab, code lost:
        if (r2 < 0) goto L_0x046d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x03ad, code lost:
        r6 = r6 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x03ae, code lost:
        if (r6 <= r15) goto L_0x03e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x03c2, code lost:
        if (r3.length() == 0) goto L_0x03d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03c4, code lost:
        r3 = "File too big for full file cache. Size: ".concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x03d0, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x03d1, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x03d2, code lost:
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        r3 = new java.lang.String("File too big for full file cache. Size: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x03db, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x03dc, code lost:
        r3 = null;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        r18.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x03e7, code lost:
        if (r17.write(r18) > 0) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x03e9, code lost:
        r18.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x03f8, code lost:
        if ((r19.m9243() - r20) <= (1000 * r24)) goto L_0x0433;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        r2 = java.lang.Long.toString(r24);
        r3 = new java.lang.StringBuilder(java.lang.String.valueOf(r2).length() + 29).append("Timeout exceeded. Limit: ").append(r2).append(" sec").toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x042e, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x042f, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0430, code lost:
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0437, code lost:
        if (r26.f4442 == false) goto L_0x044a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0444, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0445, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0446, code lost:
        r3 = null;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x044e, code lost:
        if (r0.m4724() == false) goto L_0x03a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0450, code lost:
        com.google.android.gms.internal.zzajr.f4285.post(new com.google.android.gms.internal.zzanb(r26, r27, r13.getAbsolutePath(), r6, r7, false));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0467, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0468, code lost:
        r3 = null;
        r4 = "error";
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x046d, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0475, code lost:
        if (com.google.android.gms.internal.zzagf.m4798(3) == false) goto L_0x04b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0477, code lost:
        r2 = f4441.format((long) r6);
        com.google.android.gms.internal.zzagf.m4792(new java.lang.StringBuilder((java.lang.String.valueOf(r2).length() + 22) + java.lang.String.valueOf(r27).length()).append("Preloaded ").append(r2).append(" bytes from ").append(r27).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x04b5, code lost:
        r13.setReadable(true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x04be, code lost:
        if (r14.isFile() == false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x04c0, code lost:
        r14.setLastModified(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:?, code lost:
        r14.createNewFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x04e0, code lost:
        com.google.android.gms.internal.zzagf.m4796(new java.lang.StringBuilder(java.lang.String.valueOf(r27).length() + 25).append("Preload failed for URL \"").append(r27).append("\"").toString(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x050c, code lost:
        r2 = new java.lang.String("Could not delete partial cache file at ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x0519, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x051a, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0520, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x0521, code lost:
        r3 = null;
        r4 = "error";
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0168, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        com.google.android.gms.ads.internal.zzbs.zzet();
        r3 = com.google.android.gms.internal.zzalg.m4825(r27, ((java.lang.Integer) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f5162)).intValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0188, code lost:
        if ((r3 instanceof java.net.HttpURLConnection) == false) goto L_0x0263;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x018a, code lost:
        r2 = r3.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0194, code lost:
        if (r2 < 400) goto L_0x0263;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0196, code lost:
        r4 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01a8, code lost:
        if (r3.length() == 0) goto L_0x0258;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01aa, code lost:
        r3 = "HTTP request failed. Code: ".concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01de, code lost:
        throw new java.io.IOException(new java.lang.StringBuilder(java.lang.String.valueOf(r27).length() + 32).append("HTTP status code ").append(r2).append(" at ").append(r27).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01df, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01e2, code lost:
        if ((r2 instanceof java.lang.RuntimeException) != false) goto L_0x01e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01e4, code lost:
        com.google.android.gms.ads.internal.zzbs.zzem().m4505(r2, "VideoStreamFullFileCache.preload");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01f5, code lost:
        if (r26.f4442 != false) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01f7, code lost:
        com.google.android.gms.internal.zzagf.m4794(new java.lang.StringBuilder(java.lang.String.valueOf(r27).length() + 26).append("Preload aborted for URL \"").append(r27).append("\"").toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x022d, code lost:
        r2 = java.lang.String.valueOf(r13.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x023c, code lost:
        if (r2.length() != 0) goto L_0x023e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x023e, code lost:
        r2 = "Could not delete partial cache file at ".concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0242, code lost:
        com.google.android.gms.internal.zzagf.m4791(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0245, code lost:
        m4965(r27, r13.getAbsolutePath(), r4, r3);
        f4440.remove(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        r3 = new java.lang.String("HTTP request failed. Code: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x025f, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0260, code lost:
        r3 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x04e0  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x050c  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x0520 A[ExcHandler: RuntimeException (e java.lang.RuntimeException), Splitter:B:120:0x0369] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01f7  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x023e  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m4970(java.lang.String r27) {
        /*
            r26 = this;
            r0 = r26
            java.io.File r2 = r0.f4443
            if (r2 != 0) goto L_0x0014
            r2 = 0
            java.lang.String r3 = "noCacheDir"
            r4 = 0
            r0 = r26
            r1 = r27
            r0.m4965(r1, r2, r3, r4)
            r2 = 0
        L_0x0013:
            return r2
        L_0x0014:
            r0 = r26
            java.io.File r2 = r0.f4443
            if (r2 != 0) goto L_0x004b
            r2 = 0
            r3 = r2
        L_0x001c:
            com.google.android.gms.internal.zzmx<java.lang.Integer> r2 = com.google.android.gms.internal.zznh.f5141
            com.google.android.gms.internal.zznf r4 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r2 = r4.m5595(r2)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            if (r3 <= r2) goto L_0x00b6
            r0 = r26
            java.io.File r2 = r0.f4443
            if (r2 != 0) goto L_0x006e
            r2 = 0
        L_0x0035:
            if (r2 != 0) goto L_0x0014
            java.lang.String r2 = "Unable to expire stream cache"
            com.google.android.gms.internal.zzagf.m4791(r2)
            r2 = 0
            java.lang.String r3 = "expireFailed"
            r4 = 0
            r0 = r26
            r1 = r27
            r0.m4965(r1, r2, r3, r4)
            r2 = 0
            goto L_0x0013
        L_0x004b:
            r2 = 0
            r0 = r26
            java.io.File r3 = r0.f4443
            java.io.File[] r4 = r3.listFiles()
            int r5 = r4.length
            r3 = 0
        L_0x0056:
            if (r3 >= r5) goto L_0x006c
            r6 = r4[r3]
            java.lang.String r6 = r6.getName()
            java.lang.String r7 = ".done"
            boolean r6 = r6.endsWith(r7)
            if (r6 != 0) goto L_0x0069
            int r2 = r2 + 1
        L_0x0069:
            int r3 = r3 + 1
            goto L_0x0056
        L_0x006c:
            r3 = r2
            goto L_0x001c
        L_0x006e:
            r7 = 0
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0 = r26
            java.io.File r2 = r0.f4443
            java.io.File[] r9 = r2.listFiles()
            int r10 = r9.length
            r2 = 0
            r8 = r2
        L_0x007f:
            if (r8 >= r10) goto L_0x009d
            r6 = r9[r8]
            java.lang.String r2 = r6.getName()
            java.lang.String r3 = ".done"
            boolean r2 = r2.endsWith(r3)
            if (r2 != 0) goto L_0x0526
            long r2 = r6.lastModified()
            int r11 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r11 >= 0) goto L_0x0526
        L_0x0098:
            int r8 = r8 + 1
            r4 = r2
            r7 = r6
            goto L_0x007f
        L_0x009d:
            r2 = 0
            if (r7 == 0) goto L_0x0035
            boolean r2 = r7.delete()
            r0 = r26
            java.io.File r3 = r0.m4968((java.io.File) r7)
            boolean r4 = r3.isFile()
            if (r4 == 0) goto L_0x0035
            boolean r3 = r3.delete()
            r2 = r2 & r3
            goto L_0x0035
        L_0x00b6:
            com.google.android.gms.internal.zzkb.m5487()
            java.lang.String r2 = com.google.android.gms.internal.zzajr.m4760((java.lang.String) r27)
            java.io.File r13 = new java.io.File
            r0 = r26
            java.io.File r3 = r0.f4443
            r13.<init>(r3, r2)
            r0 = r26
            java.io.File r14 = r0.m4968((java.io.File) r13)
            boolean r2 = r13.isFile()
            if (r2 == 0) goto L_0x0105
            boolean r2 = r14.isFile()
            if (r2 == 0) goto L_0x0105
            long r2 = r13.length()
            int r3 = (int) r2
            java.lang.String r4 = "Stream cache hit at "
            java.lang.String r2 = java.lang.String.valueOf(r27)
            int r5 = r2.length()
            if (r5 == 0) goto L_0x00ff
            java.lang.String r2 = r4.concat(r2)
        L_0x00ee:
            com.google.android.gms.internal.zzagf.m4792(r2)
            java.lang.String r2 = r13.getAbsolutePath()
            r0 = r26
            r1 = r27
            r0.m4964((java.lang.String) r1, (java.lang.String) r2, (int) r3)
            r2 = 1
            goto L_0x0013
        L_0x00ff:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r4)
            goto L_0x00ee
        L_0x0105:
            r0 = r26
            java.io.File r2 = r0.f4443
            java.lang.String r2 = r2.getAbsolutePath()
            java.lang.String r3 = java.lang.String.valueOf(r2)
            java.lang.String r2 = java.lang.String.valueOf(r27)
            int r4 = r2.length()
            if (r4 == 0) goto L_0x0155
            java.lang.String r2 = r3.concat(r2)
            r9 = r2
        L_0x0120:
            java.util.Set<java.lang.String> r3 = f4440
            monitor-enter(r3)
            java.util.Set<java.lang.String> r2 = f4440     // Catch:{ all -> 0x0152 }
            boolean r2 = r2.contains(r9)     // Catch:{ all -> 0x0152 }
            if (r2 == 0) goto L_0x0162
            java.lang.String r4 = "Stream cache already in progress at "
            java.lang.String r2 = java.lang.String.valueOf(r27)     // Catch:{ all -> 0x0152 }
            int r5 = r2.length()     // Catch:{ all -> 0x0152 }
            if (r5 == 0) goto L_0x015c
            java.lang.String r2 = r4.concat(r2)     // Catch:{ all -> 0x0152 }
        L_0x013c:
            com.google.android.gms.internal.zzagf.m4791(r2)     // Catch:{ all -> 0x0152 }
            java.lang.String r2 = r13.getAbsolutePath()     // Catch:{ all -> 0x0152 }
            java.lang.String r4 = "inProgress"
            r5 = 0
            r0 = r26
            r1 = r27
            r0.m4965(r1, r2, r4, r5)     // Catch:{ all -> 0x0152 }
            r2 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x0152 }
            goto L_0x0013
        L_0x0152:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0152 }
            throw r2
        L_0x0155:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r3)
            r9 = r2
            goto L_0x0120
        L_0x015c:
            java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x0152 }
            r2.<init>(r4)     // Catch:{ all -> 0x0152 }
            goto L_0x013c
        L_0x0162:
            java.util.Set<java.lang.String> r2 = f4440     // Catch:{ all -> 0x0152 }
            r2.add(r9)     // Catch:{ all -> 0x0152 }
            monitor-exit(r3)     // Catch:{ all -> 0x0152 }
            r5 = 0
            java.lang.String r11 = "error"
            r10 = 0
            com.google.android.gms.ads.internal.zzbs.zzet()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            com.google.android.gms.internal.zzmx<java.lang.Integer> r2 = com.google.android.gms.internal.zznh.f5162     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.Object r2 = r3.m5595(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2.intValue()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r0 = r27
            java.net.HttpURLConnection r3 = com.google.android.gms.internal.zzalg.m4825(r0, r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            boolean r2 = r3 instanceof java.net.HttpURLConnection     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            if (r2 == 0) goto L_0x0263
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2 = r0
            int r2 = r2.getResponseCode()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r4 = 400(0x190, float:5.6E-43)
            if (r2 < r4) goto L_0x0263
            java.lang.String r4 = "badUrl"
            java.lang.String r6 = "HTTP request failed. Code: "
            java.lang.String r3 = java.lang.Integer.toString(r2)     // Catch:{ IOException -> 0x025f, RuntimeException -> 0x0519 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ IOException -> 0x025f, RuntimeException -> 0x0519 }
            int r7 = r3.length()     // Catch:{ IOException -> 0x025f, RuntimeException -> 0x0519 }
            if (r7 == 0) goto L_0x0258
            java.lang.String r3 = r6.concat(r3)     // Catch:{ IOException -> 0x025f, RuntimeException -> 0x0519 }
        L_0x01ae:
            java.io.IOException r6 = new java.io.IOException     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            java.lang.String r7 = java.lang.String.valueOf(r27)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            int r7 = r7.length()     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            int r7 = r7 + 32
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            r8.<init>(r7)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            java.lang.String r7 = "HTTP status code "
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            java.lang.StringBuilder r2 = r7.append(r2)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            java.lang.String r7 = " at "
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            r6.<init>(r2)     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
            throw r6     // Catch:{ IOException -> 0x01df, RuntimeException -> 0x051d }
        L_0x01df:
            r2 = move-exception
        L_0x01e0:
            boolean r6 = r2 instanceof java.lang.RuntimeException
            if (r6 == 0) goto L_0x01ee
            com.google.android.gms.internal.zzaft r6 = com.google.android.gms.ads.internal.zzbs.zzem()
            java.lang.String r7 = "VideoStreamFullFileCache.preload"
            r6.m4505((java.lang.Throwable) r2, (java.lang.String) r7)
        L_0x01ee:
            r5.close()     // Catch:{ IOException -> 0x0513, NullPointerException -> 0x0516 }
        L_0x01f1:
            r0 = r26
            boolean r5 = r0.f4442
            if (r5 == 0) goto L_0x04e0
            java.lang.String r2 = java.lang.String.valueOf(r27)
            int r2 = r2.length()
            int r2 = r2 + 26
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r2)
            java.lang.String r2 = "Preload aborted for URL \""
            java.lang.StringBuilder r2 = r5.append(r2)
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r5 = "\""
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            com.google.android.gms.internal.zzagf.m4794(r2)
        L_0x0221:
            boolean r2 = r13.exists()
            if (r2 == 0) goto L_0x0245
            boolean r2 = r13.delete()
            if (r2 != 0) goto L_0x0245
            java.lang.String r5 = "Could not delete partial cache file at "
            java.lang.String r2 = r13.getAbsolutePath()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r6 = r2.length()
            if (r6 == 0) goto L_0x050c
            java.lang.String r2 = r5.concat(r2)
        L_0x0242:
            com.google.android.gms.internal.zzagf.m4791(r2)
        L_0x0245:
            java.lang.String r2 = r13.getAbsolutePath()
            r0 = r26
            r1 = r27
            r0.m4965(r1, r2, r4, r3)
            java.util.Set<java.lang.String> r2 = f4440
            r2.remove(r9)
            r2 = 0
            goto L_0x0013
        L_0x0258:
            java.lang.String r3 = new java.lang.String     // Catch:{ IOException -> 0x025f, RuntimeException -> 0x0519 }
            r3.<init>(r6)     // Catch:{ IOException -> 0x025f, RuntimeException -> 0x0519 }
            goto L_0x01ae
        L_0x025f:
            r2 = move-exception
            r3 = r10
            goto L_0x01e0
        L_0x0263:
            int r7 = r3.getContentLength()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            if (r7 >= 0) goto L_0x029f
            java.lang.String r3 = "Stream cache aborted, missing content-length header at "
            java.lang.String r2 = java.lang.String.valueOf(r27)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r4 = r2.length()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            if (r4 == 0) goto L_0x0294
            java.lang.String r2 = r3.concat(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
        L_0x027a:
            com.google.android.gms.internal.zzagf.m4791(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r2 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r3 = "contentLengthMissing"
            r4 = 0
            r0 = r26
            r1 = r27
            r0.m4965(r1, r2, r3, r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.util.Set<java.lang.String> r2 = f4440     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2.remove(r9)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2 = 0
            goto L_0x0013
        L_0x0294:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            goto L_0x027a
        L_0x029a:
            r2 = move-exception
            r3 = r10
            r4 = r11
            goto L_0x01e0
        L_0x029f:
            java.text.DecimalFormat r2 = f4441     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            long r0 = (long) r7     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r16 = r0
            r0 = r16
            java.lang.String r4 = r2.format(r0)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            com.google.android.gms.internal.zzmx<java.lang.Integer> r2 = com.google.android.gms.internal.zznh.f5143     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            com.google.android.gms.internal.zznf r6 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.Object r2 = r6.m5595(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r15 = r2.intValue()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            if (r7 <= r15) goto L_0x0325
            java.lang.String r2 = java.lang.String.valueOf(r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2.length()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2 + 33
            java.lang.String r3 = java.lang.String.valueOf(r27)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r3 = r3.length()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r2 = "Content length "
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r3 = " exceeds limit at "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            com.google.android.gms.internal.zzagf.m4791(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r3 = "File too big for full file cache. Size: "
            java.lang.String r2 = java.lang.String.valueOf(r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r4 = r2.length()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            if (r4 == 0) goto L_0x031a
            java.lang.String r2 = r3.concat(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
        L_0x0304:
            java.lang.String r3 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r4 = "sizeExceeded"
            r0 = r26
            r1 = r27
            r0.m4965(r1, r3, r4, r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.util.Set<java.lang.String> r2 = f4440     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2.remove(r9)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2 = 0
            goto L_0x0013
        L_0x031a:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            goto L_0x0304
        L_0x0320:
            r2 = move-exception
            r3 = r10
            r4 = r11
            goto L_0x01e0
        L_0x0325:
            java.lang.String r2 = java.lang.String.valueOf(r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2.length()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2 + 20
            java.lang.String r6 = java.lang.String.valueOf(r27)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r6 = r6.length()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            int r2 = r2 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r6.<init>(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r2 = "Caching "
            java.lang.StringBuilder r2 = r6.append(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r4 = " bytes from "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            com.google.android.gms.internal.zzagf.m4792(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.io.InputStream r2 = r3.getInputStream()     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.nio.channels.ReadableByteChannel r16 = java.nio.channels.Channels.newChannel(r2)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.io.FileOutputStream r12 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            r12.<init>(r13)     // Catch:{ IOException -> 0x029a, RuntimeException -> 0x0320 }
            java.nio.channels.FileChannel r17 = r12.getChannel()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r2 = 1048576(0x100000, float:1.469368E-39)
            java.nio.ByteBuffer r18 = java.nio.ByteBuffer.allocate(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.common.util.zzd r19 = com.google.android.gms.ads.internal.zzbs.zzeo()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r6 = 0
            long r20 = r19.m9243()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zzmx<java.lang.Long> r2 = com.google.android.gms.internal.zznh.f4996     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.Object r2 = r3.m5595(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            long r2 = r2.longValue()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zzaji r22 = new com.google.android.gms.internal.zzaji     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r0 = r22
            r0.<init>(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zzmx<java.lang.Long> r2 = com.google.android.gms.internal.zznh.f4970     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.Object r2 = r3.m5595(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            long r24 = r2.longValue()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
        L_0x03a3:
            r0 = r16
            r1 = r18
            int r2 = r0.read(r1)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            if (r2 < 0) goto L_0x046d
            int r6 = r6 + r2
            if (r6 <= r15) goto L_0x03e0
            java.lang.String r4 = "sizeExceeded"
            java.lang.String r2 = "File too big for full file cache. Size: "
            java.lang.String r3 = java.lang.Integer.toString(r6)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            int r5 = r3.length()     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            if (r5 == 0) goto L_0x03d5
            java.lang.String r3 = r2.concat(r3)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
        L_0x03c8:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x03d1, RuntimeException -> 0x042f }
            java.lang.String r5 = "stream cache file size limit exceeded"
            r2.<init>(r5)     // Catch:{ IOException -> 0x03d1, RuntimeException -> 0x042f }
            throw r2     // Catch:{ IOException -> 0x03d1, RuntimeException -> 0x042f }
        L_0x03d1:
            r2 = move-exception
            r5 = r12
            goto L_0x01e0
        L_0x03d5:
            java.lang.String r3 = new java.lang.String     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            goto L_0x03c8
        L_0x03db:
            r2 = move-exception
            r3 = r10
            r5 = r12
            goto L_0x01e0
        L_0x03e0:
            r18.flip()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
        L_0x03e3:
            int r2 = r17.write(r18)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            if (r2 > 0) goto L_0x03e3
            r18.clear()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            long r2 = r19.m9243()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            long r2 = r2 - r20
            r4 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r24
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0433
            java.lang.String r4 = "downloadTimeout"
            java.lang.String r2 = java.lang.Long.toString(r24)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.String r3 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            int r3 = r3.length()     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            int r3 = r3 + 29
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            r5.<init>(r3)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.String r3 = "Timeout exceeded. Limit: "
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.String r3 = " sec"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.String r3 = r2.toString()     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x03d1, RuntimeException -> 0x042f }
            java.lang.String r5 = "stream cache time limit exceeded"
            r2.<init>(r5)     // Catch:{ IOException -> 0x03d1, RuntimeException -> 0x042f }
            throw r2     // Catch:{ IOException -> 0x03d1, RuntimeException -> 0x042f }
        L_0x042f:
            r2 = move-exception
            r5 = r12
            goto L_0x01e0
        L_0x0433:
            r0 = r26
            boolean r2 = r0.f4442     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            if (r2 == 0) goto L_0x044a
            java.lang.String r4 = "externalAbort"
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            java.lang.String r3 = "abort requested"
            r2.<init>(r3)     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
            throw r2     // Catch:{ IOException -> 0x03db, RuntimeException -> 0x0445 }
        L_0x0445:
            r2 = move-exception
            r3 = r10
            r5 = r12
            goto L_0x01e0
        L_0x044a:
            boolean r2 = r22.m4724()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            if (r2 == 0) goto L_0x03a3
            java.lang.String r5 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            android.os.Handler r23 = com.google.android.gms.internal.zzajr.f4285     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zzanb r2 = new com.google.android.gms.internal.zzanb     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r8 = 0
            r3 = r26
            r4 = r27
            r2.<init>(r3, r4, r5, r6, r7, r8)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r0 = r23
            r0.post(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            goto L_0x03a3
        L_0x0467:
            r2 = move-exception
            r3 = r10
            r4 = r11
            r5 = r12
            goto L_0x01e0
        L_0x046d:
            r12.close()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r2 = 3
            boolean r2 = com.google.android.gms.internal.zzagf.m4798(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            if (r2 == 0) goto L_0x04b5
            java.text.DecimalFormat r2 = f4441     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            long r4 = (long) r6     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.String r2 = r2.format(r4)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.String r3 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            int r3 = r3.length()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            int r3 = r3 + 22
            java.lang.String r4 = java.lang.String.valueOf(r27)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            int r4 = r4.length()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.String r3 = "Preloaded "
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.String r3 = " bytes from "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            com.google.android.gms.internal.zzagf.m4792(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
        L_0x04b5:
            r2 = 1
            r3 = 0
            r13.setReadable(r2, r3)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            boolean r2 = r14.isFile()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            if (r2 == 0) goto L_0x04da
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r14.setLastModified(r2)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
        L_0x04c7:
            java.lang.String r2 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r0 = r26
            r1 = r27
            r0.m4964((java.lang.String) r1, (java.lang.String) r2, (int) r6)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            java.util.Set<java.lang.String> r2 = f4440     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r2.remove(r9)     // Catch:{ IOException -> 0x0467, RuntimeException -> 0x0520 }
            r2 = 1
            goto L_0x0013
        L_0x04da:
            r14.createNewFile()     // Catch:{ IOException -> 0x04de, RuntimeException -> 0x0520 }
            goto L_0x04c7
        L_0x04de:
            r2 = move-exception
            goto L_0x04c7
        L_0x04e0:
            java.lang.String r5 = java.lang.String.valueOf(r27)
            int r5 = r5.length()
            int r5 = r5 + 25
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            java.lang.String r5 = "Preload failed for URL \""
            java.lang.StringBuilder r5 = r6.append(r5)
            r0 = r27
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = "\""
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.google.android.gms.internal.zzagf.m4796(r5, r2)
            goto L_0x0221
        L_0x050c:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r5)
            goto L_0x0242
        L_0x0513:
            r5 = move-exception
            goto L_0x01f1
        L_0x0516:
            r5 = move-exception
            goto L_0x01f1
        L_0x0519:
            r2 = move-exception
            r3 = r10
            goto L_0x01e0
        L_0x051d:
            r2 = move-exception
            goto L_0x01e0
        L_0x0520:
            r2 = move-exception
            r3 = r10
            r4 = r11
            r5 = r12
            goto L_0x01e0
        L_0x0526:
            r2 = r4
            r6 = r7
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzang.m4970(java.lang.String):boolean");
    }
}
