package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.media.MediaCodecInfo;
import android.util.Range;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@zzzv
public final class zzajp {

    /* renamed from: 靐  reason: contains not printable characters */
    private static List<MediaCodecInfo> f4269;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Object f4270 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<String, List<Map<String, Object>>> f4271 = new HashMap();

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    @android.annotation.TargetApi(16)
    /* renamed from: 龘  reason: contains not printable characters */
    public static java.util.List<java.util.Map<java.lang.String, java.lang.Object>> m4735(java.lang.String r15) {
        /*
            r14 = 21
            r2 = 0
            java.lang.Object r3 = f4270
            monitor-enter(r3)
            java.util.Map<java.lang.String, java.util.List<java.util.Map<java.lang.String, java.lang.Object>>> r0 = f4271     // Catch:{ all -> 0x00c3 }
            boolean r0 = r0.containsKey(r15)     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x0018
            java.util.Map<java.lang.String, java.util.List<java.util.Map<java.lang.String, java.lang.Object>>> r0 = f4271     // Catch:{ all -> 0x00c3 }
            java.lang.Object r0 = r0.get(r15)     // Catch:{ all -> 0x00c3 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x00c3 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c3 }
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.Object r1 = f4270     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            monitor-enter(r1)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.util.List<android.media.MediaCodecInfo> r0 = f4269     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0086
            monitor-exit(r1)     // Catch:{ all -> 0x009c }
        L_0x0020:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r1.<init>()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.util.List<android.media.MediaCodecInfo> r0 = f4269     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
        L_0x002b:
            boolean r0 = r4.hasNext()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            if (r0 == 0) goto L_0x016b
            java.lang.Object r0 = r4.next()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            android.media.MediaCodecInfo r0 = (android.media.MediaCodecInfo) r0     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            boolean r5 = r0.isEncoder()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            if (r5 != 0) goto L_0x002b
            java.lang.String[] r5 = r0.getSupportedTypes()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.util.List r5 = java.util.Arrays.asList(r5)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            boolean r5 = r5.contains(r15)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            if (r5 == 0) goto L_0x002b
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.<init>()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r6 = "codecName"
            java.lang.String r7 = r0.getName()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r6, r7)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            android.media.MediaCodecInfo$CodecCapabilities r6 = r0.getCapabilitiesForType(r15)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r7.<init>()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            android.media.MediaCodecInfo$CodecProfileLevel[] r8 = r6.profileLevels     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            int r9 = r8.length     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r0 = r2
        L_0x0067:
            if (r0 >= r9) goto L_0x00ed
            r10 = r8[r0]     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r11 = 2
            java.lang.Integer[] r11 = new java.lang.Integer[r11]     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r12 = 0
            int r13 = r10.profile     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r11[r12] = r13     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r12 = 1
            int r10 = r10.level     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r11[r12] = r10     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r7.add(r11)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            int r0 = r0 + 1
            goto L_0x0067
        L_0x0086:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x009c }
            if (r0 < r14) goto L_0x00c6
            android.media.MediaCodecList r0 = new android.media.MediaCodecList     // Catch:{ all -> 0x009c }
            r4 = 0
            r0.<init>(r4)     // Catch:{ all -> 0x009c }
            android.media.MediaCodecInfo[] r0 = r0.getCodecInfos()     // Catch:{ all -> 0x009c }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ all -> 0x009c }
            f4269 = r0     // Catch:{ all -> 0x009c }
        L_0x009a:
            monitor-exit(r1)     // Catch:{ all -> 0x009c }
            goto L_0x0020
        L_0x009c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x009c }
            throw r0     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
        L_0x009f:
            r0 = move-exception
        L_0x00a0:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x00c3 }
            r1.<init>()     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "error"
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x00c3 }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x00c3 }
            r1.put(r2, r0)     // Catch:{ all -> 0x00c3 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00c3 }
            r0.<init>()     // Catch:{ all -> 0x00c3 }
            r0.add(r1)     // Catch:{ all -> 0x00c3 }
            java.util.Map<java.lang.String, java.util.List<java.util.Map<java.lang.String, java.lang.Object>>> r1 = f4271     // Catch:{ all -> 0x00c3 }
            r1.put(r15, r0)     // Catch:{ all -> 0x00c3 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c3 }
            goto L_0x0017
        L_0x00c3:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00c3 }
            throw r0
        L_0x00c6:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x009c }
            r4 = 16
            if (r0 < r4) goto L_0x00e6
            int r4 = android.media.MediaCodecList.getCodecCount()     // Catch:{ all -> 0x009c }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x009c }
            r0.<init>(r4)     // Catch:{ all -> 0x009c }
            f4269 = r0     // Catch:{ all -> 0x009c }
            r0 = r2
        L_0x00d8:
            if (r0 >= r4) goto L_0x009a
            android.media.MediaCodecInfo r5 = android.media.MediaCodecList.getCodecInfoAt(r0)     // Catch:{ all -> 0x009c }
            java.util.List<android.media.MediaCodecInfo> r6 = f4269     // Catch:{ all -> 0x009c }
            r6.add(r5)     // Catch:{ all -> 0x009c }
            int r0 = r0 + 1
            goto L_0x00d8
        L_0x00e6:
            java.util.List r0 = java.util.Collections.emptyList()     // Catch:{ all -> 0x009c }
            f4269 = r0     // Catch:{ all -> 0x009c }
            goto L_0x009a
        L_0x00ed:
            java.lang.String r0 = "profileLevels"
            r5.put(r0, r7)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            if (r0 < r14) goto L_0x014f
            android.media.MediaCodecInfo$VideoCapabilities r0 = r6.getVideoCapabilities()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r7 = "bitRatesBps"
            android.util.Range r8 = r0.getBitrateRange()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer[] r8 = m4736((android.util.Range<java.lang.Integer>) r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r7, r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r7 = "widthAlignment"
            int r8 = r0.getWidthAlignment()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r7, r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r7 = "heightAlignment"
            int r8 = r0.getHeightAlignment()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r7, r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r7 = "frameRates"
            android.util.Range r8 = r0.getSupportedFrameRates()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer[] r8 = m4736((android.util.Range<java.lang.Integer>) r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r7, r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r7 = "widths"
            android.util.Range r8 = r0.getSupportedWidths()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer[] r8 = m4736((android.util.Range<java.lang.Integer>) r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r7, r8)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.String r7 = "heights"
            android.util.Range r0 = r0.getSupportedHeights()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer[] r0 = m4736((android.util.Range<java.lang.Integer>) r0)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r7, r0)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
        L_0x014f:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r7 = 23
            if (r0 < r7) goto L_0x0163
            java.lang.String r0 = "instancesLimit"
            int r6 = r6.getMaxSupportedInstances()     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r5.put(r0, r6)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
        L_0x0163:
            r1.add(r5)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            goto L_0x002b
        L_0x0168:
            r0 = move-exception
            goto L_0x00a0
        L_0x016b:
            java.util.Map<java.lang.String, java.util.List<java.util.Map<java.lang.String, java.lang.Object>>> r0 = f4271     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            r0.put(r15, r1)     // Catch:{ RuntimeException -> 0x009f, LinkageError -> 0x0168 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c3 }
            r0 = r1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzajp.m4735(java.lang.String):java.util.List");
    }

    @TargetApi(21)
    /* renamed from: 龘  reason: contains not printable characters */
    private static Integer[] m4736(Range<Integer> range) {
        return new Integer[]{range.getLower(), range.getUpper()};
    }
}
