package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzv {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzm f10917;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzz f10918;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzn[] f10919;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzd f10920;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final List<Object> f10921;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzb f10922;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Set<zzr<?>> f10923;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PriorityBlockingQueue<zzr<?>> f10924;

    /* renamed from: 齉  reason: contains not printable characters */
    private final PriorityBlockingQueue<zzr<?>> f10925;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicInteger f10926;

    public zzv(zzb zzb, zzm zzm) {
        this(zzb, zzm, 4);
    }

    private zzv(zzb zzb, zzm zzm, int i) {
        this(zzb, zzm, 4, new zzi(new Handler(Looper.getMainLooper())));
    }

    private zzv(zzb zzb, zzm zzm, int i, zzz zzz) {
        this.f10926 = new AtomicInteger();
        this.f10923 = new HashSet();
        this.f10925 = new PriorityBlockingQueue<>();
        this.f10924 = new PriorityBlockingQueue<>();
        this.f10921 = new ArrayList();
        this.f10922 = zzb;
        this.f10917 = zzm;
        this.f10919 = new zzn[4];
        this.f10918 = zzz;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final <T> void m13452(zzr<T> zzr) {
        synchronized (this.f10923) {
            this.f10923.remove(zzr);
        }
        synchronized (this.f10921) {
            Iterator<Object> it2 = this.f10921.iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T> zzr<T> m13453(zzr<T> zzr) {
        zzr.m13367(this);
        synchronized (this.f10923) {
            this.f10923.add(zzr);
        }
        zzr.m13365(this.f10926.incrementAndGet());
        zzr.m13361("add-to-queue");
        if (!zzr.m13353()) {
            this.f10924.add(zzr);
        } else {
            this.f10925.add(zzr);
        }
        return zzr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13454() {
        if (this.f10920 != null) {
            this.f10920.m11583();
        }
        for (zzn zzn : this.f10919) {
            if (zzn != null) {
                zzn.m13153();
            }
        }
        this.f10920 = new zzd(this.f10925, this.f10924, this.f10922, this.f10918);
        this.f10920.start();
        for (int i = 0; i < this.f10919.length; i++) {
            zzn zzn2 = new zzn(this.f10924, this.f10917, this.f10922, this.f10918);
            this.f10919[i] = zzn2;
            zzn2.start();
        }
    }
}
