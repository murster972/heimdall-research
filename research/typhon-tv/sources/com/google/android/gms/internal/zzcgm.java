package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzcgm implements Parcelable.Creator<zzcgl> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r3 = zzbfn.m10169(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        zzcln zzcln = null;
        long j = 0;
        boolean z = false;
        String str3 = null;
        zzcha zzcha = null;
        long j2 = 0;
        zzcha zzcha2 = null;
        long j3 = 0;
        zzcha zzcha3 = null;
        while (parcel.dataPosition() < r3) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    zzcln = (zzcln) zzbfn.m10171(parcel, readInt, zzcln.CREATOR);
                    break;
                case 5:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 6:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 7:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    zzcha = (zzcha) zzbfn.m10171(parcel, readInt, zzcha.CREATOR);
                    break;
                case 9:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 10:
                    zzcha2 = (zzcha) zzbfn.m10171(parcel, readInt, zzcha.CREATOR);
                    break;
                case 11:
                    j3 = zzbfn.m10163(parcel, readInt);
                    break;
                case 12:
                    zzcha3 = (zzcha) zzbfn.m10171(parcel, readInt, zzcha.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r3);
        return new zzcgl(i, str, str2, zzcln, j, z, str3, zzcha, j2, zzcha2, j3, zzcha3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcgl[i];
    }
}
