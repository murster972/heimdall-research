package com.google.android.gms.internal;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public final class zzap {

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<zzl> f8383;

    /* renamed from: 麤  reason: contains not printable characters */
    private final InputStream f8384;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f8385;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f8386;

    public zzap(int i, List<zzl> list) {
        this(i, list, -1, (InputStream) null);
    }

    public zzap(int i, List<zzl> list, int i2, InputStream inputStream) {
        this.f8386 = i;
        this.f8383 = list;
        this.f8385 = i2;
        this.f8384 = inputStream;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzl> m9728() {
        return Collections.unmodifiableList(this.f8383);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final InputStream m9729() {
        return this.f8384;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m9730() {
        return this.f8385;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9731() {
        return this.f8386;
    }
}
