package com.google.android.gms.internal;

import java.util.Random;

@zzzv
public final class zzkc extends zzle {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f4819;

    /* renamed from: 齉  reason: contains not printable characters */
    private Object f4820 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Random f4821 = new Random();

    public zzkc() {
        m5489();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m5488() {
        return this.f4819;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5489() {
        synchronized (this.f4820) {
            int i = 3;
            long j = 0;
            while (true) {
                i--;
                if (i <= 0) {
                    break;
                }
                j = ((long) this.f4821.nextInt()) + 2147483648L;
                if (j != this.f4819 && j != 0) {
                    break;
                }
            }
            this.f4819 = j;
        }
    }
}
