package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.common.util.zzq;
import java.util.Locale;

public final class zzacp {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f8074;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f8075;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f8076;

    /* renamed from: ʾ  reason: contains not printable characters */
    private double f8077;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f8078;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f8079;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f8080;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f8081;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f8082;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f8083;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f8084;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String f8085;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f8086;

    /* renamed from: י  reason: contains not printable characters */
    private String f8087;

    /* renamed from: ـ  reason: contains not printable characters */
    private float f8088;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f8089;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f8090;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f8091;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f8092;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private String f8093;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f8094;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f8095;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f8096;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f8097;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f8098;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f8099;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String f8100;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f8101;

    public zzacp(Context context) {
        DisplayMetrics displayMetrics;
        boolean z = true;
        PackageManager packageManager = context.getPackageManager();
        m9460(context);
        m9456(context);
        m9457(context);
        Locale locale = Locale.getDefault();
        this.f8082 = m9458(packageManager, "geo:0,0?q=donuts") != null;
        this.f8083 = m9458(packageManager, "http://www.google.com") == null ? false : z;
        this.f8084 = locale.getCountry();
        zzkb.m5487();
        this.f8079 = zzajr.m4766();
        this.f8081 = zzi.m9256(context);
        this.f8085 = locale.getLanguage();
        this.f8087 = m9455(context, packageManager);
        this.f8093 = m9459(context, packageManager);
        Resources resources = context.getResources();
        if (resources != null && (displayMetrics = resources.getDisplayMetrics()) != null) {
            this.f8088 = displayMetrics.density;
            this.f8091 = displayMetrics.widthPixels;
            this.f8092 = displayMetrics.heightPixels;
        }
    }

    public zzacp(Context context, zzaco zzaco) {
        context.getPackageManager();
        m9460(context);
        m9456(context);
        m9457(context);
        this.f8100 = Build.FINGERPRINT;
        this.f8101 = Build.DEVICE;
        this.f8094 = zzq.m9272() && zzoe.m5638(context);
        this.f8082 = zzaco.f4001;
        this.f8083 = zzaco.f4003;
        this.f8084 = zzaco.f4000;
        this.f8079 = zzaco.f3979;
        this.f8081 = zzaco.f3980;
        this.f8085 = zzaco.f3994;
        this.f8087 = zzaco.f3995;
        this.f8093 = zzaco.f3985;
        this.f8088 = zzaco.f3989;
        this.f8091 = zzaco.f3984;
        this.f8092 = zzaco.f3986;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m9455(Context context, PackageManager packageManager) {
        ActivityInfo activityInfo;
        ResolveInfo r1 = m9458(packageManager, "market://details?id=com.google.android.gms.ads");
        if (r1 == null || (activityInfo = r1.activityInfo) == null) {
            return null;
        }
        try {
            PackageInfo r2 = zzbhf.m10231(context).m10222(activityInfo.packageName, 0);
            if (r2 == null) {
                return null;
            }
            int i = r2.versionCode;
            String str = activityInfo.packageName;
            return new StringBuilder(String.valueOf(str).length() + 12).append(i).append(".").append(str).toString();
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    @TargetApi(16)
    /* renamed from: 靐  reason: contains not printable characters */
    private final void m9456(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        this.f8075 = telephonyManager.getNetworkOperator();
        this.f8086 = telephonyManager.getNetworkType();
        this.f8089 = telephonyManager.getPhoneType();
        this.f8076 = -2;
        this.f8090 = false;
        this.f8080 = -1;
        zzbs.zzei();
        if (zzahn.m4618(context, context.getPackageName(), "android.permission.ACCESS_NETWORK_STATE")) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                this.f8076 = activeNetworkInfo.getType();
                this.f8080 = activeNetworkInfo.getDetailedState().ordinal();
            } else {
                this.f8076 = -1;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                this.f8090 = connectivityManager.isActiveNetworkMetered();
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m9457(Context context) {
        boolean z = false;
        Intent registerReceiver = context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra(NotificationCompat.CATEGORY_STATUS, -1);
            this.f8077 = (double) (((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1)));
            if (intExtra == 2 || intExtra == 5) {
                z = true;
            }
            this.f8078 = z;
            return;
        }
        this.f8077 = -1.0d;
        this.f8078 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ResolveInfo m9458(PackageManager packageManager, String str) {
        try {
            return packageManager.resolveActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)), 65536);
        } catch (Throwable th) {
            zzbs.zzem().m4505(th, "DeviceInfo.getResolveInfo");
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m9459(Context context, PackageManager packageManager) {
        try {
            PackageInfo r1 = zzbhf.m10231(context).m10222("com.android.vending", 128);
            if (r1 == null) {
                return null;
            }
            int i = r1.versionCode;
            String str = r1.packageName;
            return new StringBuilder(String.valueOf(str).length() + 12).append(i).append(".").append(str).toString();
        } catch (Exception e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9460(Context context) {
        zzbs.zzei();
        AudioManager r0 = zzahn.m4570(context);
        if (r0 != null) {
            try {
                this.f8099 = r0.getMode();
                this.f8096 = r0.isMusicActive();
                this.f8098 = r0.isSpeakerphoneOn();
                this.f8097 = r0.getStreamVolume(3);
                this.f8095 = r0.getRingerMode();
                this.f8074 = r0.getStreamVolume(2);
                return;
            } catch (Throwable th) {
                zzbs.zzem().m4505(th, "DeviceInfo.gatherAudioInfo");
            }
        }
        this.f8099 = -2;
        this.f8096 = false;
        this.f8098 = false;
        this.f8097 = 0;
        this.f8095 = 0;
        this.f8074 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzaco m9461() {
        return new zzaco(this.f8099, this.f8082, this.f8083, this.f8075, this.f8084, this.f8079, this.f8081, this.f8096, this.f8098, this.f8085, this.f8087, this.f8093, this.f8097, this.f8076, this.f8086, this.f8089, this.f8095, this.f8074, this.f8088, this.f8091, this.f8092, this.f8077, this.f8078, this.f8090, this.f8080, this.f8100, this.f8094, this.f8101);
    }
}
