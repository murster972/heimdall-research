package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.common.util.zzd;

public final class zzcll extends zzcjl {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzcgs f9642;

    /* renamed from: 齉  reason: contains not printable characters */
    private Integer f9643;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AlarmManager f9644 = ((AlarmManager) m11097().getSystemService(NotificationCompat.CATEGORY_ALARM));

    protected zzcll(zzcim zzcim) {
        super(zzcim);
        this.f9642 = new zzclm(this, zzcim);
    }

    @TargetApi(24)
    /* renamed from: ᵎ  reason: contains not printable characters */
    private final void m11335() {
        m11096().m10848().m10850("Cancelling job. JobID", Integer.valueOf(m11336()));
        ((JobScheduler) m11097().getSystemService("jobscheduler")).cancel(m11336());
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final int m11336() {
        if (this.f9643 == null) {
            String valueOf = String.valueOf(m11097().getPackageName());
            this.f9643 = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f9643.intValue();
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final PendingIntent m11337() {
        Intent className = new Intent().setClassName(m11097(), "com.google.android.gms.measurement.AppMeasurementReceiver");
        className.setAction("com.google.android.gms.measurement.UPLOAD");
        return PendingIntent.getBroadcast(m11097(), 0, className, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m11338() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m11339() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m11340() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m11341() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m11342() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m11343() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m11344() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m11345() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m11346() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m11347() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m11348() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m11349() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m11350() {
        return super.m11103();
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m11351() {
        this.f9644.cancel(m11337());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        m11335();
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m11352() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m11353() {
        return super.m11105();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public final void m11354() {
        m11115();
        this.f9644.cancel(m11337());
        this.f9642.m10624();
        if (Build.VERSION.SDK_INT >= 24) {
            m11335();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m11355() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11356() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m11357() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11358() {
        super.m11109();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m11359() {
        super.m11110();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11360(long j) {
        m11115();
        if (!zzcid.m10910(m11097())) {
            m11096().m10845().m10849("Receiver not registered/enabled");
        }
        if (!zzcla.m11286(m11097(), false)) {
            m11096().m10845().m10849("Service not registered/enabled");
        }
        m11354();
        long r2 = m11105().m9241() + j;
        if (j < Math.max(0, zzchc.f9232.m10671().longValue()) && !this.f9642.m10623()) {
            m11096().m10848().m10849("Scheduling upload with DelayedRunnable");
            this.f9642.m10626(j);
        }
        if (Build.VERSION.SDK_INT >= 24) {
            m11096().m10848().m10849("Scheduling upload with JobScheduler");
            JobInfo.Builder builder = new JobInfo.Builder(m11336(), new ComponentName(m11097(), "com.google.android.gms.measurement.AppMeasurementJobService"));
            builder.setMinimumLatency(j);
            builder.setOverrideDeadline(j << 1);
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString("action", "com.google.android.gms.measurement.UPLOAD");
            builder.setExtras(persistableBundle);
            JobInfo build = builder.build();
            m11096().m10848().m10850("Scheduling job. JobID", Integer.valueOf(m11336()));
            ((JobScheduler) m11097().getSystemService("jobscheduler")).schedule(build);
            return;
        }
        m11096().m10848().m10849("Scheduling upload with AlarmManager");
        this.f9644.setInexactRepeating(2, r2, Math.max(zzchc.f9217.m10671().longValue(), j), m11337());
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m11361() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m11362() {
        return super.m11112();
    }
}
