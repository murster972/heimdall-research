package com.google.android.gms.internal;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@zzzv
public abstract class zzhq {

    /* renamed from: 靐  reason: contains not printable characters */
    private static MessageDigest f4713 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Object f4714 = new Object();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final MessageDigest m5395() {
        MessageDigest messageDigest;
        synchronized (this.f4714) {
            if (f4713 != null) {
                messageDigest = f4713;
            } else {
                for (int i = 0; i < 2; i++) {
                    try {
                        f4713 = MessageDigest.getInstance("MD5");
                    } catch (NoSuchAlgorithmException e) {
                    }
                }
                messageDigest = f4713;
            }
        }
        return messageDigest;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract byte[] m5396(String str);
}
