package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;

final class zzdqo implements zzdpw<zzdpp> {
    zzdqo() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdpp m11729(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdtp r1 = zzdtp.m12135(zzfes);
            if (!(r1 instanceof zzdtp)) {
                throw new GeneralSecurityException("expected KmsEnvelopeAeadKey proto");
            }
            zzdtp zzdtp = r1;
            zzdvk.m12238(zzdtp.m12142(), 0);
            String r2 = zzdtp.m12140().m12153();
            return new zzdqn(zzdtp.m12140().m12150(), zzdpz.m11662(r2).m11660(r2));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized KmSEnvelopeAeadKey proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11726(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11727((zzfhe) zzdtr.m12149(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized KmsEnvelopeAeadKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11727(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdtr)) {
            throw new GeneralSecurityException("expected KmsEnvelopeAeadKeyFormat proto");
        }
        return zzdtp.m12134().m12146((zzdtr) zzfhe).m12145(0).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11728(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey").m12022(((zzdtp) m11726(zzfes)).m12361()).m12021(zzdsy.zzb.REMOTE).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11730(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdtp)) {
            throw new GeneralSecurityException("expected KmsEnvelopeAeadKey proto");
        }
        zzdtp zzdtp = (zzdtp) zzfhe;
        zzdvk.m12238(zzdtp.m12142(), 0);
        String r0 = zzdtp.m12140().m12153();
        return new zzdqn(zzdtp.m12140().m12150(), zzdpz.m11662(r0).m11660(r0));
    }
}
