package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public final class zzej extends zzet {

    /* renamed from: 麤  reason: contains not printable characters */
    private List<Long> f10265 = null;

    public zzej(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 31);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12284() throws IllegalAccessException, InvocationTargetException {
        this.f10284.f8474 = -1L;
        this.f10284.f8439 = -1L;
        if (this.f10265 == null) {
            this.f10265 = (List) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()});
        }
        if (this.f10265 != null && this.f10265.size() == 2) {
            synchronized (this.f10284) {
                this.f10284.f8474 = Long.valueOf(this.f10265.get(0).longValue());
                this.f10284.f8439 = Long.valueOf(this.f10265.get(1).longValue());
            }
        }
    }
}
