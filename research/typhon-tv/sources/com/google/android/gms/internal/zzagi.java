package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

final class zzagi extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f8155;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8156;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagi(Context context, boolean z) {
        super((zzagi) null);
        this.f8156 = context;
        this.f8155 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9581() {
        SharedPreferences.Editor edit = this.f8156.getSharedPreferences("admob", 0).edit();
        edit.putBoolean("use_https", this.f8155);
        edit.apply();
    }
}
