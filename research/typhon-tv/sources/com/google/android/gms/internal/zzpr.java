package com.google.android.gms.internal;

import android.net.Uri;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzpr extends zzev implements zzpq {
    public zzpr() {
        attachInterface(this, "com.google.android.gms.ads.internal.formats.client.INativeAdImage");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzpq m13226(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
        return queryLocalInterface instanceof zzpq ? (zzpq) queryLocalInterface : new zzps(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                IObjectWrapper r1 = m13225();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r1);
                return true;
            case 2:
                Uri r12 = m13223();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r12);
                return true;
            case 3:
                double r2 = m13224();
                parcel2.writeNoException();
                parcel2.writeDouble(r2);
                return true;
            default:
                return false;
        }
    }
}
