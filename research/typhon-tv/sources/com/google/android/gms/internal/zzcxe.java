package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;

public final class zzcxe implements Api.ApiOptions.Optional {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzcxe f9824 = new zzcxe(false, false, (String) null, false, (String) null, false, (Long) null, (Long) null);

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f9825 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f9826 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Long f9827 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Long f9828 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f9829 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f9830 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f9831 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f9832 = false;

    static {
        new zzcxf();
    }

    private zzcxe(boolean z, boolean z2, String str, boolean z3, String str2, boolean z4, Long l, Long l2) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m11552() {
        return this.f9826;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Long m11553() {
        return this.f9827;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Long m11554() {
        return this.f9828;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m11555() {
        return this.f9825;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m11556() {
        return this.f9832;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m11557() {
        return this.f9829;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m11558() {
        return this.f9831;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11559() {
        return this.f9830;
    }
}
