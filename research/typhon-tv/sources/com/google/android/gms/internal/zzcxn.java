package com.google.android.gms.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.internal.zzz;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbr;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzm;
import com.google.android.gms.common.internal.zzr;

public final class zzcxn extends zzab<zzcxl> implements zzcxd {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Bundle f9836;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Integer f9837;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzr f9838;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f9839;

    private zzcxn(Context context, Looper looper, boolean z, zzr zzr, Bundle bundle, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 44, zzr, connectionCallbacks, onConnectionFailedListener);
        this.f9839 = true;
        this.f9838 = zzr;
        this.f9836 = bundle;
        this.f9837 = zzr.m4210();
    }

    public zzcxn(Context context, Looper looper, boolean z, zzr zzr, zzcxe zzcxe, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, true, zzr, m11568(zzr), connectionCallbacks, onConnectionFailedListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bundle m11568(zzr zzr) {
        zzcxe r0 = zzr.m4209();
        Integer r1 = zzr.m4210();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", zzr.m4212());
        if (r1 != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", r1.intValue());
        }
        if (r0 != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", r0.m11559());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", r0.m11556());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", r0.m11558());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", r0.m11557());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", r0.m11555());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", r0.m11552());
            if (r0.m11553() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", r0.m11553().longValue());
            }
            if (r0.m11554() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", r0.m11554().longValue());
            }
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public final String r_() {
        return "com.google.android.gms.signin.service.START";
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m11569() {
        m9187((zzj) new zzm(this));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public final Bundle m11570() {
        if (!m9168().getPackageName().equals(this.f9838.m4207())) {
            this.f9836.putString("com.google.android.gms.signin.internal.realClientPackageName", this.f9838.m4207());
        }
        return this.f9836;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m11571() {
        return this.f9839;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m11572() {
        try {
            ((zzcxl) m9171()).m11562(this.f9837.intValue());
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m11573() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ IInterface m11574(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        return queryLocalInterface instanceof zzcxl ? (zzcxl) queryLocalInterface : new zzcxm(iBinder);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11575(zzan zzan, boolean z) {
        try {
            ((zzcxl) m9171()).m11563(zzan, this.f9837.intValue(), z);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11576(zzcxj zzcxj) {
        zzbq.m9121(zzcxj, (Object) "Expecting a valid ISignInCallbacks");
        try {
            Account r1 = this.f9838.m4214();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(r1.name)) {
                googleSignInAccount = zzz.m7754(m9168()).m7758();
            }
            ((zzcxl) m9171()).m11564(new zzcxo(new zzbr(r1, this.f9837.intValue(), googleSignInAccount)), zzcxj);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                zzcxj.m11561(new zzcxq(8));
            } catch (RemoteException e2) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }
}
