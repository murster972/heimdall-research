package com.google.android.gms.internal;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

class zzfif extends AbstractSet<Map.Entry<K, V>> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzfhy f10484;

    private zzfif(zzfhy zzfhy) {
        this.f10484 = zzfhy;
    }

    /* synthetic */ zzfif(zzfhy zzfhy, zzfhz zzfhz) {
        this(zzfhy);
    }

    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.f10484.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    public void clear() {
        this.f10484.clear();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.f10484.get(entry.getKey());
        Object value = entry.getValue();
        return obj2 == value || (obj2 != null && obj2.equals(value));
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        return new zzfie(this.f10484, (zzfhz) null);
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.f10484.remove(entry.getKey());
        return true;
    }

    public int size() {
        return this.f10484.size();
    }
}
