package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdri;
import com.google.android.gms.internal.zzdsu;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdre extends zzffu<zzdre, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdre f9966;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdre> f9967;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdsu f9968;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdri f9969;

    public static final class zza extends zzffu.zza<zzdre, zza> implements zzfhg {
        private zza() {
            super(zzdre.f9966);
        }

        /* synthetic */ zza(zzdrf zzdrf) {
            this();
        }
    }

    static {
        zzdre zzdre = new zzdre();
        f9966 = zzdre;
        zzdre.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdre.f10394.m12718();
    }

    private zzdre() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdre m11788(zzfes zzfes) throws zzfge {
        return (zzdre) zzffu.m12526(f9966, zzfes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdsu m11789() {
        return this.f9968 == null ? zzdsu.m11992() : this.f9968;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11790() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9969 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f9969 == null ? zzdri.m11814() : this.f9969) + 0;
        }
        if (this.f9968 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f9968 == null ? zzdsu.m11992() : this.f9968);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdri m11791() {
        return this.f9969 == null ? zzdri.m11814() : this.f9969;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11792(int i, Object obj, Object obj2) {
        zzdsu.zza zza2;
        zzdri.zza zza3;
        switch (zzdrf.f9970[i - 1]) {
            case 1:
                return new zzdre();
            case 2:
                return f9966;
            case 3:
                return null;
            case 4:
                return new zza((zzdrf) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdre zzdre = (zzdre) obj2;
                this.f9969 = (zzdri) zzh.m12572(this.f9969, zzdre.f9969);
                this.f9968 = (zzdsu) zzh.m12572(this.f9968, zzdre.f9968);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z = true;
                                    break;
                                case 10:
                                    if (this.f9969 != null) {
                                        zzdri zzdri = this.f9969;
                                        zzffu.zza zza4 = (zzffu.zza) zzdri.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza4.m12545(zzdri);
                                        zza3 = (zzdri.zza) zza4;
                                    } else {
                                        zza3 = null;
                                    }
                                    this.f9969 = (zzdri) zzffb.m12416(zzdri.m11814(), zzffm);
                                    if (zza3 == null) {
                                        break;
                                    } else {
                                        zza3.m12545(this.f9969);
                                        this.f9969 = (zzdri) zza3.m12543();
                                        break;
                                    }
                                case 18:
                                    if (this.f9968 != null) {
                                        zzdsu zzdsu = this.f9968;
                                        zzffu.zza zza5 = (zzffu.zza) zzdsu.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza5.m12545(zzdsu);
                                        zza2 = (zzdsu.zza) zza5;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f9968 = (zzdsu) zzffb.m12416(zzdsu.m11992(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f9968);
                                        this.f9968 = (zzdsu) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f9967 == null) {
                    synchronized (zzdre.class) {
                        if (f9967 == null) {
                            f9967 = new zzffu.zzb(f9966);
                        }
                    }
                }
                return f9967;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f9966;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11793(zzffg zzffg) throws IOException {
        if (this.f9969 != null) {
            zzffg.m5289(1, (zzfhe) this.f9969 == null ? zzdri.m11814() : this.f9969);
        }
        if (this.f9968 != null) {
            zzffg.m5289(2, (zzfhe) this.f9968 == null ? zzdsu.m11992() : this.f9968);
        }
        this.f10394.m12719(zzffg);
    }
}
