package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.common.util.zzq;

@zzzv
public final class zzwl implements MediationInterstitialAdapter {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MediationInterstitialListener f5503;

    /* renamed from: 齉  reason: contains not printable characters */
    private Uri f5504;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public Activity f5505;

    public final void onDestroy() {
        zzakb.m4792("Destroying AdMobCustomTabsAdapter adapter.");
    }

    public final void onPause() {
        zzakb.m4792("Pausing AdMobCustomTabsAdapter adapter.");
    }

    public final void onResume() {
        zzakb.m4792("Resuming AdMobCustomTabsAdapter adapter.");
    }

    public final void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.f5503 = mediationInterstitialListener;
        if (this.f5503 == null) {
            zzakb.m4791("Listener not set for mediation. Returning.");
        } else if (!(context instanceof Activity)) {
            zzakb.m4791("AdMobCustomTabs can only work with Activity context. Bailing out.");
            this.f5503.onAdFailedToLoad(this, 0);
        } else {
            if (!(zzq.m9272() && zzoe.m5638(context))) {
                zzakb.m4791("Default browser does not support custom tabs. Bailing out.");
                this.f5503.onAdFailedToLoad(this, 0);
                return;
            }
            String string = bundle.getString("tab_url");
            if (TextUtils.isEmpty(string)) {
                zzakb.m4791("The tab_url retrieved from mediation metadata is empty. Bailing out.");
                this.f5503.onAdFailedToLoad(this, 0);
                return;
            }
            this.f5505 = (Activity) context;
            this.f5504 = Uri.parse(string);
            this.f5503.onAdLoaded(this);
        }
    }

    public final void showInterstitial() {
        CustomTabsIntent build = new CustomTabsIntent.Builder().build();
        build.intent.setData(this.f5504);
        zzahn.f4212.post(new zzwn(this, new AdOverlayInfoParcel(new zzc(build.intent), (zzje) null, new zzwm(this), (com.google.android.gms.ads.internal.overlay.zzq) null, new zzakd(0, 0, false))));
        zzbs.zzem().m4473();
    }
}
