package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

final class zzagv extends zzahf {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzahg f8181;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f8182;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzagv(Context context, zzahg zzahg) {
        super((zzagi) null);
        this.f8182 = context;
        this.f8181 = zzahg;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9594() {
        SharedPreferences sharedPreferences = this.f8182.getSharedPreferences("admob", 0);
        Bundle bundle = new Bundle();
        bundle.putLong("app_last_background_time_ms", sharedPreferences.getLong("app_last_background_time_ms", 0));
        if (this.f8181 != null) {
            this.f8181.m9605(bundle);
        }
    }
}
