package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;

final class zzdqp implements zzdpw<zzdpu> {
    zzdqp() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdpu m11735(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdsi r0 = zzdsi.m11926(zzfes);
            if (!(r0 instanceof zzdsi)) {
                throw new GeneralSecurityException("expected EciesAeadHkdfPrivateKey proto");
            }
            zzdsi zzdsi = r0;
            zzdvk.m12238(zzdsi.m11935(), 0);
            zzdqv.m11749(zzdsi.m11932().m11954());
            zzdsg r4 = zzdsi.m11932().m11954();
            zzdsm r3 = r4.m11920();
            zzdur r1 = zzdqv.m11746(r3.m11969());
            byte[] byteArray = zzdsi.m11934().toByteArray();
            ECPrivateKeySpec eCPrivateKeySpec = new ECPrivateKeySpec(new BigInteger(1, byteArray), zzdup.m12212(r1));
            return new zzduk((ECPrivateKey) KeyFactory.getInstance("EC").generatePrivate(eCPrivateKeySpec), r3.m11968().toByteArray(), zzdqv.m11748(r3.m11966()), zzdqv.m11747(r4.m11919()), new zzdqx(r4.m11917().m11906()));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPrivateKey proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11732(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11733((zzfhe) zzdse.m11910(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("invalid EciesAeadHkdf key format", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11733(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdse)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfKeyFormat proto");
        }
        zzdse zzdse = (zzdse) zzfhe;
        zzdqv.m11749(zzdse.m11912());
        ECParameterSpec r0 = zzdup.m12212(zzdqv.m11746(zzdse.m11912().m11920().m11969()));
        KeyPairGenerator instance = KeyPairGenerator.getInstance("EC");
        instance.initialize(r0);
        KeyPair generateKeyPair = instance.generateKeyPair();
        ECPoint w = ((ECPublicKey) generateKeyPair.getPublic()).getW();
        return zzdsi.m11924().m11938(0).m11939((zzdsk) zzdsk.m11941().m11961(0).m11962(zzdse.m11912()).m11963(zzfes.zzaz(w.getAffineX().toByteArray())).m11960(zzfes.zzaz(w.getAffineY().toByteArray())).m12542()).m11940(zzfes.zzaz(((ECPrivateKey) generateKeyPair.getPrivate()).getS().toByteArray())).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11734(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey").m12022(((zzdsi) m11732(zzfes)).m12361()).m12021(zzdsy.zzb.ASYMMETRIC_PRIVATE).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11736(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdsi)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfPrivateKey proto");
        }
        zzdsi zzdsi = (zzdsi) zzfhe;
        zzdvk.m12238(zzdsi.m11935(), 0);
        zzdqv.m11749(zzdsi.m11932().m11954());
        zzdsg r4 = zzdsi.m11932().m11954();
        zzdsm r3 = r4.m11920();
        zzdur r0 = zzdqv.m11746(r3.m11969());
        byte[] byteArray = zzdsi.m11934().toByteArray();
        ECPrivateKeySpec eCPrivateKeySpec = new ECPrivateKeySpec(new BigInteger(1, byteArray), zzdup.m12212(r0));
        return new zzduk((ECPrivateKey) KeyFactory.getInstance("EC").generatePrivate(eCPrivateKeySpec), r3.m11968().toByteArray(), zzdqv.m11748(r3.m11966()), zzdqv.m11747(r4.m11919()), new zzdqx(r4.m11917().m11906()));
    }
}
