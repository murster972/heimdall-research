package com.google.android.gms.internal;

@zzzv
final class zzamr implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f4423 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzamd f4424;

    zzamr(zzamd zzamd) {
        this.f4424 = zzamd;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m4939() {
        zzahn.f4212.removeCallbacks(this);
        zzahn.f4212.postDelayed(this, 250);
    }

    public final void run() {
        if (!this.f4423) {
            this.f4424.m4890();
            m4939();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4940() {
        this.f4423 = false;
        m4939();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4941() {
        this.f4423 = true;
    }
}
