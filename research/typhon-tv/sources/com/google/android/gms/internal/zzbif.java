package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

public final class zzbif extends zzbfm {
    public static final Parcelable.Creator<zzbif> CREATOR = new zzbig();

    /* renamed from: 靐  reason: contains not printable characters */
    private final DataHolder f8799;

    /* renamed from: 麤  reason: contains not printable characters */
    private final DataHolder f8800;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f8801;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f8802;

    public zzbif(int i, DataHolder dataHolder, long j, DataHolder dataHolder2) {
        this.f8802 = i;
        this.f8799 = dataHolder;
        this.f8801 = j;
        this.f8800 = dataHolder2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 2, this.f8802);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f8799, i, false);
        zzbfp.m10186(parcel, 4, this.f8801);
        zzbfp.m10189(parcel, 5, (Parcelable) this.f8800, i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m10279() {
        if (this.f8800 != null && !this.f8800.m9018()) {
            this.f8800.close();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m10280() {
        if (this.f8799 != null && !this.f8799.m9018()) {
            this.f8799.close();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final DataHolder m10281() {
        return this.f8799;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final DataHolder m10282() {
        return this.f8800;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m10283() {
        return this.f8801;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10284() {
        return this.f8802;
    }
}
