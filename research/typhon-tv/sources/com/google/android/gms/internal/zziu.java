package com.google.android.gms.internal;

import android.support.v4.view.PointerIconCompat;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zziu {

    public static final class zza extends zzffu<zza, C0024zza> implements zzfhg {

        /* renamed from: 连任  reason: contains not printable characters */
        private static volatile zzfhk<zza> f10713;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public static final zza f10714;

        /* renamed from: com.google.android.gms.internal.zziu$zza$zza  reason: collision with other inner class name */
        public static final class C0024zza extends zzffu.zza<zza, C0024zza> implements zzfhg {
            private C0024zza() {
                super(zza.f10714);
            }

            /* synthetic */ C0024zza(zziv zziv) {
                this();
            }
        }

        public enum zzb implements zzfga {
            UNKNOWN_EVENT_TYPE(0),
            AD_REQUEST(1),
            AD_LOADED(2),
            AD_FAILED_TO_LOAD(3),
            AD_FAILED_TO_LOAD_NO_FILL(4),
            AD_IMPRESSION(5),
            AD_FIRST_CLICK(6),
            AD_SUBSEQUENT_CLICK(7),
            REQUEST_WILL_START(8),
            REQUEST_DID_END(9),
            REQUEST_WILL_UPDATE_SIGNALS(10),
            REQUEST_DID_UPDATE_SIGNALS(11),
            REQUEST_WILL_BUILD_URL(12),
            REQUEST_DID_BUILD_URL(13),
            REQUEST_WILL_MAKE_NETWORK_REQUEST(14),
            REQUEST_DID_RECEIVE_NETWORK_RESPONSE(15),
            REQUEST_WILL_PROCESS_RESPONSE(16),
            REQUEST_DID_PROCESS_RESPONSE(17),
            REQUEST_WILL_RENDER(18),
            REQUEST_DID_RENDER(19),
            REQUEST_WILL_UPDATE_GMS_SIGNALS(1000),
            REQUEST_DID_UPDATE_GMS_SIGNALS(1001),
            REQUEST_FAILED_TO_UPDATE_GMS_SIGNALS(PointerIconCompat.TYPE_HAND),
            REQUEST_FAILED_TO_BUILD_URL(PointerIconCompat.TYPE_HELP),
            REQUEST_FAILED_TO_MAKE_NETWORK_REQUEST(PointerIconCompat.TYPE_WAIT),
            REQUEST_FAILED_TO_PROCESS_RESPONSE(1005);
            

            /* renamed from: ˏ  reason: contains not printable characters */
            private static final zzfgb<zzb> f10726 = null;
            private final int value;

            static {
                f10726 = new zziw();
            }

            private zzb(int i) {
                this.value = i;
            }

            public final int zzhq() {
                return this.value;
            }
        }

        static {
            zza zza = new zza();
            f10714 = zza;
            zza.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
            zza.f10394.m12718();
        }

        private zza() {
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final int m13012() {
            int i = this.f10395;
            if (i != -1) {
                return i;
            }
            int r0 = this.f10394.m12716() + 0;
            this.f10395 = r0;
            return r0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final Object m13013(int i, Object obj, Object obj2) {
            switch (zziv.f10738[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return f10714;
                case 3:
                    return null;
                case 4:
                    return new C0024zza((zziv) null);
                case 5:
                    return this;
                case 6:
                    zzffb zzffb = (zzffb) obj;
                    if (((zzffm) obj2) != null) {
                        boolean z = false;
                        while (!z) {
                            try {
                                int r2 = zzffb.m12415();
                                switch (r2) {
                                    case 0:
                                        z = true;
                                        break;
                                    default:
                                        if (m12537(r2, zzffb)) {
                                            break;
                                        } else {
                                            z = true;
                                            break;
                                        }
                                }
                            } catch (zzfge e) {
                                throw new RuntimeException(e.zzi(this));
                            } catch (IOException e2) {
                                throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                            }
                        }
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                case 7:
                    break;
                case 8:
                    if (f10713 == null) {
                        synchronized (zza.class) {
                            if (f10713 == null) {
                                f10713 = new zzffu.zzb(f10714);
                            }
                        }
                    }
                    return f10713;
                case 9:
                    return (byte) 1;
                case 10:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
            return f10714;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m13014(zzffg zzffg) throws IOException {
            this.f10394.m12719(zzffg);
        }
    }
}
