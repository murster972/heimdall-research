package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;

class zzdqh implements zzdpw<zzdpp> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Logger f9942 = Logger.getLogger(zzdqh.class.getName());

    zzdqh() throws GeneralSecurityException {
        zzdqe.m11681("type.googleapis.com/google.crypto.tink.AesCtrKey", new zzdqi());
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdpp m11689(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdrc r1 = zzdrc.m11771(zzfes);
            if (!(r1 instanceof zzdrc)) {
                throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
            }
            zzdrc zzdrc = r1;
            zzdvk.m12238(zzdrc.m11781(), 0);
            return new zzdut((zzdvf) zzdqe.m11673("type.googleapis.com/google.crypto.tink.AesCtrKey", zzdrc.m11778()), (zzdqa) zzdqe.m11673("type.googleapis.com/google.crypto.tink.HmacKey", zzdrc.m11780()), zzdrc.m11780().m11982().m12001());
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKey proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11686(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11687((zzfhe) zzdre.m11788(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11687(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdre)) {
            throw new GeneralSecurityException("expected AesCtrHmacAeadKeyFormat proto");
        }
        zzdre zzdre = (zzdre) zzfhe;
        return zzdrc.m11770().m11785((zzdrg) zzdqe.m11677("type.googleapis.com/google.crypto.tink.AesCtrKey", (zzfhe) zzdre.m11791())).m11786((zzdss) zzdqe.m11677("type.googleapis.com/google.crypto.tink.HmacKey", (zzfhe) zzdre.m11789())).m11784(0).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11688(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey").m12022(((zzdrc) m11686(zzfes)).m12361()).m12021(zzdsy.zzb.SYMMETRIC).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11690(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdrc)) {
            throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
        }
        zzdrc zzdrc = (zzdrc) zzfhe;
        zzdvk.m12238(zzdrc.m11781(), 0);
        return new zzdut((zzdvf) zzdqe.m11673("type.googleapis.com/google.crypto.tink.AesCtrKey", zzdrc.m11778()), (zzdqa) zzdqe.m11673("type.googleapis.com/google.crypto.tink.HmacKey", zzdrc.m11780()), zzdrc.m11780().m11982().m12001());
    }
}
