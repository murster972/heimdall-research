package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.View;
import android.view.ViewGroup;

@TargetApi(19)
public class zzaic extends zzaia {
    /* renamed from: 麤  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m9631() {
        return new ViewGroup.LayoutParams(-1, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9632(View view) {
        return view.isAttachedToWindow();
    }
}
