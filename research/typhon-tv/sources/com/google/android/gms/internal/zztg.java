package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Base64;
import com.google.android.gms.ads.internal.zzbs;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;

@zzzv
public final class zztg {

    /* renamed from: 靐  reason: contains not printable characters */
    private final LinkedList<zzth> f5362 = new LinkedList<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private zzsd f5363;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<zzth, zzti> f5364 = new HashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    static zzjj m5825(zzjj zzjj) {
        zzjj r0 = m5828(zzjj);
        Bundle bundle = r0.f4756.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle != null) {
            bundle.putBoolean("_skipMediation", true);
        }
        r0.f4767.putBoolean("_skipMediation", true);
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final String m5826() {
        try {
            StringBuilder sb = new StringBuilder();
            Iterator it2 = this.f5362.iterator();
            while (it2.hasNext()) {
                sb.append(Base64.encodeToString(((zzth) it2.next()).toString().getBytes("UTF-8"), 0));
                if (it2.hasNext()) {
                    sb.append("\u0000");
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m5827(String str) {
        try {
            return Pattern.matches((String) zzkb.m5481().m5595(zznh.f4931), str);
        } catch (RuntimeException e) {
            zzbs.zzem().m4505((Throwable) e, "InterstitialAdPool.isExcludedAdUnit");
            return false;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static zzjj m5828(zzjj zzjj) {
        Parcel obtain = Parcel.obtain();
        zzjj.writeToParcel(obtain, 0);
        obtain.setDataPosition(0);
        zzjj createFromParcel = zzjj.CREATOR.createFromParcel(obtain);
        obtain.recycle();
        if (((Boolean) zzkb.m5481().m5595(zznh.f4919)).booleanValue()) {
            zzjj.m5449(createFromParcel);
        }
        return createFromParcel;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzjj m5829(zzjj zzjj) {
        zzjj r1 = m5828(zzjj);
        for (String str : ((String) zzkb.m5481().m5595(zznh.f4927)).split(",")) {
            m5832(r1.f4756, str);
            if (str.startsWith("com.google.ads.mediation.admob.AdMobAdapter/")) {
                m5832(r1.f4767, str.replaceFirst("com.google.ads.mediation.admob.AdMobAdapter/", ""));
            }
        }
        return r1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m5830(String str) {
        try {
            Matcher matcher = Pattern.compile("([^/]+/[0-9]+).*").matcher(str);
            return matcher.matches() ? matcher.group(1) : str;
        } catch (RuntimeException e) {
            return str;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Set<String> m5831(zzjj zzjj) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(zzjj.f4767.keySet());
        Bundle bundle = zzjj.f4756.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle != null) {
            hashSet.addAll(bundle.keySet());
        }
        return hashSet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m5832(Bundle bundle, String str) {
        while (true) {
            String[] split = str.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, 2);
            if (split.length != 0) {
                String str2 = split[0];
                if (split.length == 1) {
                    bundle.remove(str2);
                    return;
                }
                bundle = bundle.getBundle(str2);
                if (bundle != null) {
                    str = split[1];
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m5833(String str, zzth zzth) {
        if (zzagf.m4798(2)) {
            zzagf.m4527(String.format(str, new Object[]{zzth}));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] m5834(String str) {
        try {
            String[] split = str.split("\u0000");
            for (int i = 0; i < split.length; i++) {
                split[i] = new String(Base64.decode(split[i], 0), "UTF-8");
            }
            return split;
        } catch (UnsupportedEncodingException e) {
            return new String[0];
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5835(zzjj zzjj, String str) {
        if (this.f5363 != null) {
            int i = new zzacp(this.f5363.m5820()).m9461().f3983;
            zzjj r2 = m5829(zzjj);
            String r3 = m5830(str);
            zzth zzth = new zzth(r2, r3, i);
            zzti zzti = this.f5364.get(zzth);
            if (zzti == null) {
                m5833("Interstitial pool created at %s.", zzth);
                zzti = new zzti(r2, r3, i);
                this.f5364.put(zzth, zzti);
            }
            zzti.m5850(this.f5363, zzjj);
            zzti.m5842();
            m5833("Inline entry added to the queue at %s.", zzth);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zztj m5836(zzjj zzjj, String str) {
        zzti zzti;
        if (m5827(str)) {
            return null;
        }
        int i = new zzacp(this.f5363.m5820()).m9461().f3983;
        zzjj r4 = m5829(zzjj);
        String r2 = m5830(str);
        zzth zzth = new zzth(r4, r2, i);
        zzti zzti2 = this.f5364.get(zzth);
        if (zzti2 == null) {
            m5833("Interstitial pool created at %s.", zzth);
            zzti zzti3 = new zzti(r4, r2, i);
            this.f5364.put(zzth, zzti3);
            zzti = zzti3;
        } else {
            zzti = zzti2;
        }
        this.f5362.remove(zzth);
        this.f5362.add(zzth);
        zzti.m5842();
        while (true) {
            if (this.f5362.size() <= ((Integer) zzkb.m5481().m5595(zznh.f4928)).intValue()) {
                break;
            }
            zzth remove = this.f5362.remove();
            zzti zzti4 = this.f5364.get(remove);
            m5833("Evicting interstitial queue for %s.", remove);
            while (zzti4.m5846() > 0) {
                zztj r6 = zzti4.m5849((zzjj) null);
                if (r6.f10890) {
                    zztk.m5852().m5861();
                }
                r6.f10894.zzdk();
            }
            this.f5364.remove(remove);
        }
        while (zzti.m5846() > 0) {
            zztj r1 = zzti.m5849(r4);
            if (r1.f10890) {
                if (zzbs.zzeo().m9243() - r1.f10892 > 1000 * ((long) ((Integer) zzkb.m5481().m5595(zznh.f4930)).intValue())) {
                    m5833("Expired interstitial at %s.", zzth);
                    zztk.m5852().m5859();
                }
            }
            String str2 = r1.f10891 != null ? " (inline) " : StringUtils.SPACE;
            m5833(new StringBuilder(String.valueOf(str2).length() + 34).append("Pooled interstitial").append(str2).append("returned at %s.").toString(), zzth);
            return r1;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002f, code lost:
        r2 = r0.m5846();
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5837() {
        /*
            r9 = this;
            r8 = 2
            r7 = 0
            com.google.android.gms.internal.zzsd r0 = r9.f5363
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.util.Map<com.google.android.gms.internal.zzth, com.google.android.gms.internal.zzti> r0 = r9.f5364
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r4 = r0.iterator()
        L_0x0011:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x008e
            java.lang.Object r0 = r4.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            com.google.android.gms.internal.zzth r1 = (com.google.android.gms.internal.zzth) r1
            java.lang.Object r0 = r0.getValue()
            com.google.android.gms.internal.zzti r0 = (com.google.android.gms.internal.zzti) r0
            boolean r2 = com.google.android.gms.internal.zzagf.m4798(r8)
            if (r2 == 0) goto L_0x0057
            int r2 = r0.m5846()
            int r3 = r0.m5844()
            if (r3 >= r2) goto L_0x0057
            java.lang.String r5 = "Loading %s/%s pooled interstitials for %s."
            r6 = 3
            java.lang.Object[] r6 = new java.lang.Object[r6]
            int r3 = r2 - r3
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r6[r7] = r3
            r3 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r6[r3] = r2
            r6[r8] = r1
            java.lang.String r2 = java.lang.String.format(r5, r6)
            com.google.android.gms.internal.zzagf.m4527(r2)
        L_0x0057:
            int r2 = r0.m5841()
            int r2 = r2 + 0
            r3 = r2
        L_0x005e:
            int r5 = r0.m5846()
            com.google.android.gms.internal.zzmx<java.lang.Integer> r2 = com.google.android.gms.internal.zznh.f4929
            com.google.android.gms.internal.zznf r6 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r2 = r6.m5595(r2)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            if (r5 >= r2) goto L_0x0086
            java.lang.String r2 = "Pooling and loading one new interstitial for %s."
            m5833((java.lang.String) r2, (com.google.android.gms.internal.zzth) r1)
            com.google.android.gms.internal.zzsd r2 = r9.f5363
            boolean r2 = r0.m5851((com.google.android.gms.internal.zzsd) r2)
            if (r2 == 0) goto L_0x005e
            int r2 = r3 + 1
            r3 = r2
            goto L_0x005e
        L_0x0086:
            com.google.android.gms.internal.zztk r0 = com.google.android.gms.internal.zztk.m5852()
            r0.m5862(r3)
            goto L_0x0011
        L_0x008e:
            com.google.android.gms.internal.zzsd r0 = r9.f5363
            if (r0 == 0) goto L_0x0006
            com.google.android.gms.internal.zzsd r0 = r9.f5363
            android.content.Context r0 = r0.m5820()
            java.lang.String r1 = "com.google.android.gms.ads.internal.interstitial.InterstitialAdPool"
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r7)
            android.content.SharedPreferences$Editor r2 = r0.edit()
            r2.clear()
            java.util.Map<com.google.android.gms.internal.zzth, com.google.android.gms.internal.zzti> r0 = r9.f5364
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x00b0:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00e5
            java.lang.Object r0 = r3.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            com.google.android.gms.internal.zzth r1 = (com.google.android.gms.internal.zzth) r1
            java.lang.Object r0 = r0.getValue()
            com.google.android.gms.internal.zzti r0 = (com.google.android.gms.internal.zzti) r0
            boolean r4 = r0.m5843()
            if (r4 == 0) goto L_0x00b0
            com.google.android.gms.internal.zztm r4 = new com.google.android.gms.internal.zztm
            r4.<init>(r0)
            java.lang.String r0 = r4.m5865()
            java.lang.String r4 = r1.toString()
            r2.putString(r4, r0)
            java.lang.String r0 = "Saved interstitial queue for %s."
            m5833((java.lang.String) r0, (com.google.android.gms.internal.zzth) r1)
            goto L_0x00b0
        L_0x00e5:
            java.lang.String r0 = "PoolKeys"
            java.lang.String r1 = r9.m5826()
            r2.putString(r0, r1)
            r2.apply()
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zztg.m5837():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5838(zzsd zzsd) {
        if (this.f5363 == null) {
            this.f5363 = zzsd.m5819();
            if (this.f5363 != null) {
                SharedPreferences sharedPreferences = this.f5363.m5820().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
                while (this.f5362.size() > 0) {
                    zzth remove = this.f5362.remove();
                    zzti zzti = this.f5364.get(remove);
                    m5833("Flushing interstitial queue for %s.", remove);
                    while (zzti.m5846() > 0) {
                        zzti.m5849((zzjj) null).f10894.zzdk();
                    }
                    this.f5364.remove(remove);
                }
                try {
                    HashMap hashMap = new HashMap();
                    for (Map.Entry next : sharedPreferences.getAll().entrySet()) {
                        if (!((String) next.getKey()).equals("PoolKeys")) {
                            zztm r0 = zztm.m5864((String) next.getValue());
                            zzth zzth = new zzth(r0.f5384, r0.f5382, r0.f5383);
                            if (!this.f5364.containsKey(zzth)) {
                                this.f5364.put(zzth, new zzti(r0.f5384, r0.f5382, r0.f5383));
                                hashMap.put(zzth.toString(), zzth);
                                m5833("Restored interstitial queue for %s.", zzth);
                            }
                        }
                    }
                    for (String str : m5834(sharedPreferences.getString("PoolKeys", ""))) {
                        zzth zzth2 = (zzth) hashMap.get(str);
                        if (this.f5364.containsKey(zzth2)) {
                            this.f5362.add(zzth2);
                        }
                    }
                } catch (IOException | RuntimeException e) {
                    zzbs.zzem().m4505(e, "InterstitialAdPool.restore");
                    zzagf.m4796("Malformed preferences value for InterstitialAdPool.", e);
                    this.f5364.clear();
                    this.f5362.clear();
                }
            }
        }
    }
}
