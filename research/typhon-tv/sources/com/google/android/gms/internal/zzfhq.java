package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

final class zzfhq extends zzfes {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int[] f10454;
    private final int zzpjh;
    /* access modifiers changed from: private */
    public final zzfes zzpji;
    /* access modifiers changed from: private */
    public final zzfes zzpjj;
    private final int zzpjk;
    private final int zzpjl;

    static {
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 1;
        while (i > 0) {
            arrayList.add(Integer.valueOf(i));
            int i3 = i2 + i;
            i2 = i;
            i = i3;
        }
        arrayList.add(Integer.valueOf(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT));
        f10454 = new int[arrayList.size()];
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 < f10454.length) {
                f10454[i5] = ((Integer) arrayList.get(i5)).intValue();
                i4 = i5 + 1;
            } else {
                return;
            }
        }
    }

    private zzfhq(zzfes zzfes, zzfes zzfes2) {
        this.zzpji = zzfes;
        this.zzpjj = zzfes2;
        this.zzpjk = zzfes.size();
        this.zzpjh = this.zzpjk + zzfes2.size();
        this.zzpjl = Math.max(zzfes.m12377(), zzfes2.m12377()) + 1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzfes m12650(zzfes zzfes, zzfes zzfes2) {
        int size = zzfes.size();
        int size2 = zzfes2.size();
        byte[] bArr = new byte[(size + size2)];
        zzfes.zza(bArr, 0, 0, size);
        zzfes2.zza(bArr, 0, size, size2);
        return zzfes.m12372(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzfes m12653(zzfes zzfes, zzfes zzfes2) {
        if (zzfes2.size() == 0) {
            return zzfes;
        }
        if (zzfes.size() == 0) {
            return zzfes2;
        }
        int size = zzfes2.size() + zzfes.size();
        if (size < 128) {
            return m12650(zzfes, zzfes2);
        }
        if (zzfes instanceof zzfhq) {
            zzfhq zzfhq = (zzfhq) zzfes;
            if (zzfhq.zzpjj.size() + zzfes2.size() < 128) {
                return new zzfhq(zzfhq.zzpji, m12650(zzfhq.zzpjj, zzfes2));
            } else if (zzfhq.zzpji.m12377() > zzfhq.zzpjj.m12377() && zzfhq.m12377() > zzfes2.m12377()) {
                return new zzfhq(zzfhq.zzpji, new zzfhq(zzfhq.zzpjj, zzfes2));
            }
        }
        return size >= f10454[Math.max(zzfes.m12377(), zzfes2.m12377()) + 1] ? new zzfhq(zzfes, zzfes2) : new zzfhs().m12661(zzfes, zzfes2);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfes)) {
            return false;
        }
        zzfes zzfes = (zzfes) obj;
        if (this.zzpjh != zzfes.size()) {
            return false;
        }
        if (this.zzpjh == 0) {
            return true;
        }
        int r0 = m12376();
        int r1 = zzfes.m12376();
        if (r0 != 0 && r1 != 0 && r0 != r1) {
            return false;
        }
        zzfht zzfht = new zzfht(this);
        zzfht zzfht2 = new zzfht(zzfes);
        int i = 0;
        zzfey zzfey = (zzfey) zzfht2.next();
        int i2 = 0;
        zzfey zzfey2 = (zzfey) zzfht.next();
        int i3 = 0;
        while (true) {
            int size = zzfey2.size() - i3;
            int size2 = zzfey.size() - i2;
            int min = Math.min(size, size2);
            if (!(i3 == 0 ? zzfey2.m12390(zzfey, i2, min) : zzfey.m12390(zzfey2, i3, min))) {
                return false;
            }
            int i4 = i + min;
            if (i4 < this.zzpjh) {
                if (min == size) {
                    zzfey2 = (zzfey) zzfht.next();
                    i3 = 0;
                } else {
                    i3 += min;
                }
                if (min == size2) {
                    i = i4;
                    zzfey = (zzfey) zzfht2.next();
                    i2 = 0;
                } else {
                    i = i4;
                    i2 += min;
                }
            } else if (i4 == this.zzpjh) {
                return true;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public final int size() {
        return this.zzpjh;
    }

    public final zzffb zzcvm() {
        return zzffb.m12397((InputStream) new zzfhu(this));
    }

    public final byte zzkn(int i) {
        m12374(i, this.zzpjh);
        return i < this.zzpjk ? this.zzpji.zzkn(i) : this.zzpjj.zzkn(i - this.zzpjk);
    }

    public final zzfes zzx(int i, int i2) {
        int r0 = m12370(i, i2, this.zzpjh);
        if (r0 == 0) {
            return zzfes.zzpfg;
        }
        if (r0 == this.zzpjh) {
            return this;
        }
        if (i2 <= this.zzpjk) {
            return this.zzpji.zzx(i, i2);
        }
        if (i >= this.zzpjk) {
            return this.zzpjj.zzx(i - this.zzpjk, i2 - this.zzpjk);
        }
        zzfes zzfes = this.zzpji;
        return new zzfhq(zzfes.zzx(i, zzfes.size()), this.zzpjj.zzx(0, i2 - this.zzpjk));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12655() {
        return this.zzpjh >= f10454[this.zzpjl];
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12656() {
        return this.zzpjl;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12657(int i, int i2, int i3) {
        if (i2 + i3 <= this.zzpjk) {
            return this.zzpji.m12378(i, i2, i3);
        }
        if (i2 >= this.zzpjk) {
            return this.zzpjj.m12378(i, i2 - this.zzpjk, i3);
        }
        int i4 = this.zzpjk - i2;
        return this.zzpjj.m12378(this.zzpji.m12378(i, i2, i4), 0, i3 - i4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12658(zzfer zzfer) throws IOException {
        this.zzpji.m12379(zzfer);
        this.zzpjj.m12379(zzfer);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12659(byte[] bArr, int i, int i2, int i3) {
        if (i + i3 <= this.zzpjk) {
            this.zzpji.m12380(bArr, i, i2, i3);
        } else if (i >= this.zzpjk) {
            this.zzpjj.m12380(bArr, i - this.zzpjk, i2, i3);
        } else {
            int i4 = this.zzpjk - i;
            this.zzpji.m12380(bArr, i, i2, i4);
            this.zzpjj.m12380(bArr, 0, i2 + i4, i3 - i4);
        }
    }
}
