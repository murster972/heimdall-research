package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzjl implements Parcelable.Creator<zzjj> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r22 = zzbfn.m10169(parcel);
        int i = 0;
        long j = 0;
        Bundle bundle = null;
        int i2 = 0;
        ArrayList<String> arrayList = null;
        boolean z = false;
        int i3 = 0;
        boolean z2 = false;
        String str = null;
        zzmn zzmn = null;
        Location location = null;
        String str2 = null;
        Bundle bundle2 = null;
        Bundle bundle3 = null;
        ArrayList<String> arrayList2 = null;
        String str3 = null;
        String str4 = null;
        boolean z3 = false;
        while (parcel.dataPosition() < r22) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 4:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 6:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 7:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 8:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 10:
                    zzmn = (zzmn) zzbfn.m10171(parcel, readInt, zzmn.CREATOR);
                    break;
                case 11:
                    location = (Location) zzbfn.m10171(parcel, readInt, Location.CREATOR);
                    break;
                case 12:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 13:
                    bundle2 = zzbfn.m10153(parcel, readInt);
                    break;
                case 14:
                    bundle3 = zzbfn.m10153(parcel, readInt);
                    break;
                case 15:
                    arrayList2 = zzbfn.m10159(parcel, readInt);
                    break;
                case 16:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 17:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 18:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r22);
        return new zzjj(i, j, bundle, i2, arrayList, z, i3, z2, str, zzmn, location, str2, bundle2, bundle3, arrayList2, str3, str4, z3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzjj[i];
    }
}
