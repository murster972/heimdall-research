package com.google.android.gms.internal;

import android.net.Uri;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzpq extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    Uri m13223() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    double m13224() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m13225() throws RemoteException;
}
