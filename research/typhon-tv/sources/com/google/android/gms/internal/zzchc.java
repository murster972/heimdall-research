package com.google.android.gms.internal;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.mopub.common.TyphoonApp;

public final class zzchc {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static zzchd<Long> f9204 = zzchd.m10668("measurement.config.cache_time", 86400000, 3600000);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public static zzchd<Integer> f9205 = zzchd.m10667("measurement.audience.filter_result_max_count", 200, 200);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static zzchd<String> f9206 = zzchd.m10669("measurement.config.url_scheme", TyphoonApp.HTTPS, TyphoonApp.HTTPS);

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private static zzchd<Boolean> f9207 = zzchd.m10670("measurement.service_enabled", true, true);

    /* renamed from: ʽ  reason: contains not printable characters */
    public static zzchd<String> f9208 = zzchd.m10669("measurement.config.url_authority", "app-measurement.com", "app-measurement.com");

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public static zzchd<Long> f9209 = zzchd.m10668("measurement.service_client.idle_disconnect_millis", (long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, (long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);

    /* renamed from: ʾ  reason: contains not printable characters */
    public static zzchd<Integer> f9210 = zzchd.m10667("measurement.upload.max_events_per_day", (int) DefaultOggSeeker.MATCH_BYTE_RANGE, (int) DefaultOggSeeker.MATCH_BYTE_RANGE);

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private static zzchd<Boolean> f9211 = zzchd.m10670("measurement.log_third_party_store_events_enabled", false, false);

    /* renamed from: ʿ  reason: contains not printable characters */
    public static zzchd<Integer> f9212 = zzchd.m10667("measurement.upload.max_error_events_per_day", 1000, 1000);

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private static zzchd<Boolean> f9213 = zzchd.m10670("measurement.service_client_enabled", true, true);

    /* renamed from: ˆ  reason: contains not printable characters */
    public static zzchd<Long> f9214 = zzchd.m10668("measurement.upload.backoff_period", 43200000, 43200000);

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private static zzchd<Boolean> f9215 = zzchd.m10670("measurement.log_upgrades_enabled", false, false);

    /* renamed from: ˈ  reason: contains not printable characters */
    public static zzchd<Integer> f9216 = zzchd.m10667("measurement.upload.max_events_per_bundle", 1000, 1000);

    /* renamed from: ˉ  reason: contains not printable characters */
    public static zzchd<Long> f9217 = zzchd.m10668("measurement.upload.window_interval", 3600000, 3600000);

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private static zzchd<Boolean> f9218 = zzchd.m10670("measurement.log_androidId_enabled", false, false);

    /* renamed from: ˊ  reason: contains not printable characters */
    public static zzchd<Integer> f9219 = zzchd.m10667("measurement.upload.max_realtime_events_per_day", 10, 10);

    /* renamed from: ˋ  reason: contains not printable characters */
    public static zzchd<Integer> f9220 = zzchd.m10667("measurement.store.max_stored_events_per_app", (int) DefaultOggSeeker.MATCH_BYTE_RANGE, (int) DefaultOggSeeker.MATCH_BYTE_RANGE);

    /* renamed from: ˎ  reason: contains not printable characters */
    public static zzchd<String> f9221 = zzchd.m10669("measurement.upload.url", "=", "=");

    /* renamed from: ˏ  reason: contains not printable characters */
    public static zzchd<Long> f9222 = zzchd.m10668("measurement.upload.interval", 3600000, 3600000);

    /* renamed from: ˑ  reason: contains not printable characters */
    public static zzchd<Integer> f9223 = zzchd.m10667("measurement.upload.max_bundles", 100, 100);

    /* renamed from: י  reason: contains not printable characters */
    public static zzchd<Long> f9224 = zzchd.m10668("measurement.upload.realtime_upload_interval", 10000, 10000);

    /* renamed from: ـ  reason: contains not printable characters */
    public static zzchd<Long> f9225 = zzchd.m10668("measurement.upload.debug_upload_interval", 1000, 1000);

    /* renamed from: ــ  reason: contains not printable characters */
    private static zzchd<Boolean> f9226 = zzchd.m10670("measurement.log_installs_enabled", false, false);

    /* renamed from: ٴ  reason: contains not printable characters */
    public static zzchd<Integer> f9227 = zzchd.m10667("measurement.upload.max_batch_size", 65536, 65536);

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static zzchd<Integer> f9228 = zzchd.m10667("measurement.upload.max_bundle_size", 65536, 65536);

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public static zzchd<Long> f9229 = zzchd.m10668("measurement.upload.max_queue_time", 2419200000L, 2419200000L);

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static zzchd<Long> f9230 = zzchd.m10668("measurement.upload.minimum_delay", 500, 500);

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public static zzchd<Integer> f9231 = zzchd.m10667("measurement.lifetimevalue.max_currency_tracked", 4, 4);

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static zzchd<Long> f9232 = zzchd.m10668("measurement.alarm_manager.minimum_interval", 60000, 60000);

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static zzchd<Long> f9233 = zzchd.m10668("measurement.upload.stale_data_deletion_interval", 86400000, 86400000);

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static zzchd<Long> f9234 = zzchd.m10668("measurement.upload.refresh_blacklisted_config_interval", 604800000, 604800000);

    /* renamed from: ⁱ  reason: contains not printable characters */
    public static zzchd<Long> f9235 = zzchd.m10668("measurement.upload.initial_upload_delay_time", 15000, 15000);

    /* renamed from: 连任  reason: contains not printable characters */
    public static zzchd<Long> f9236 = zzchd.m10668("measurement.monitoring.sample_period_millis", 86400000, 86400000);

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzchd<Boolean> f9237 = zzchd.m10670("measurement.event_sampling_enabled", false, false);

    /* renamed from: 麤  reason: contains not printable characters */
    public static zzchd<Long> f9238 = zzchd.m10668("measurement.ad_id_cache_time", 10000, 10000);

    /* renamed from: 齉  reason: contains not printable characters */
    public static zzchd<String> f9239 = zzchd.m10669("measurement.log_tag", "FA", "FA-SVC");

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzchd<Boolean> f9240 = zzchd.m10670("measurement.upload_dsid_enabled", false, false);

    /* renamed from: ﹳ  reason: contains not printable characters */
    public static zzchd<Long> f9241 = zzchd.m10668("measurement.upload.retry_time", 1800000, 1800000);

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static zzchd<Integer> f9242 = zzchd.m10667("measurement.upload.max_public_events_per_day", 50000, 50000);

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static zzchd<Integer> f9243 = zzchd.m10667("measurement.upload.max_conversions_per_day", 500, 500);

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public static zzchd<Integer> f9244 = zzchd.m10667("measurement.upload.retry_count", 6, 6);
}
