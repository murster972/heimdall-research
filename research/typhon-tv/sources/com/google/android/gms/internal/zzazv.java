package com.google.android.gms.internal;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzazv extends zzeu implements zzazu {
    zzazv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.internal.IFetchBitmapTask");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bitmap m9845(Uri uri) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) uri);
        Parcel r1 = m12300(1, v_);
        Bitmap bitmap = (Bitmap) zzew.m12304(r1, Bitmap.CREATOR);
        r1.recycle();
        return bitmap;
    }
}
