package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import java.util.Map;

final class zzacj implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzach f8072;

    zzacj(zzach zzach) {
        this.f8072 = zzach;
    }

    public final void zza(Object obj, Map<String, String> map) {
        synchronized (this.f8072.f3928) {
            if (!this.f8072.f3927.isDone()) {
                zzacn zzacn = new zzacn(-2, map);
                if (this.f8072.f3930.equals(zzacn.m4308())) {
                    String r3 = zzacn.m4311();
                    if (r3 == null) {
                        zzagf.m4791("URL missing in loadAdUrl GMSG.");
                        return;
                    }
                    if (r3.contains("%40mediation_adapters%40")) {
                        String replaceAll = r3.replaceAll("%40mediation_adapters%40", zzaga.m4519(this.f8072.f3931, map.get("check_adapters"), this.f8072.f3929));
                        zzacn.m4316(replaceAll);
                        String valueOf = String.valueOf(replaceAll);
                        zzagf.m4527(valueOf.length() != 0 ? "Ad request URL modified to ".concat(valueOf) : new String("Ad request URL modified to "));
                    }
                    this.f8072.f3927.m4822(zzacn);
                }
            }
        }
    }
}
