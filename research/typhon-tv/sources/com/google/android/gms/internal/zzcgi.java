package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;

public final class zzcgi extends zzbfm {
    public static final Parcelable.Creator<zzcgi> CREATOR = new zzcgj();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final long f9130;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String f9131;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean f9132;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final long f9133;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final int f9134;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final long f9135;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean f9136;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final long f9137;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String f9138;

    /* renamed from: 连任  reason: contains not printable characters */
    public final long f9139;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f9140;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f9141;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f9142;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f9143;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final boolean f9144;

    zzcgi(String str, String str2, String str3, long j, String str4, long j2, long j3, String str5, boolean z, boolean z2, String str6, long j4, long j5, int i, boolean z3) {
        zzbq.m9122(str);
        this.f9143 = str;
        this.f9140 = TextUtils.isEmpty(str2) ? null : str2;
        this.f9142 = str3;
        this.f9137 = j;
        this.f9141 = str4;
        this.f9139 = j2;
        this.f9130 = j3;
        this.f9131 = str5;
        this.f9132 = z;
        this.f9136 = z2;
        this.f9138 = str6;
        this.f9135 = j4;
        this.f9133 = j5;
        this.f9134 = i;
        this.f9144 = z3;
    }

    zzcgi(String str, String str2, String str3, String str4, long j, long j2, String str5, boolean z, boolean z2, long j3, String str6, long j4, long j5, int i, boolean z3) {
        this.f9143 = str;
        this.f9140 = str2;
        this.f9142 = str3;
        this.f9137 = j3;
        this.f9141 = str4;
        this.f9139 = j;
        this.f9130 = j2;
        this.f9131 = str5;
        this.f9132 = z;
        this.f9136 = z2;
        this.f9138 = str6;
        this.f9135 = j4;
        this.f9133 = j5;
        this.f9134 = i;
        this.f9144 = z3;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f9143, false);
        zzbfp.m10193(parcel, 3, this.f9140, false);
        zzbfp.m10193(parcel, 4, this.f9142, false);
        zzbfp.m10193(parcel, 5, this.f9141, false);
        zzbfp.m10186(parcel, 6, this.f9139);
        zzbfp.m10186(parcel, 7, this.f9130);
        zzbfp.m10193(parcel, 8, this.f9131, false);
        zzbfp.m10195(parcel, 9, this.f9132);
        zzbfp.m10195(parcel, 10, this.f9136);
        zzbfp.m10186(parcel, 11, this.f9137);
        zzbfp.m10193(parcel, 12, this.f9138, false);
        zzbfp.m10186(parcel, 13, this.f9135);
        zzbfp.m10186(parcel, 14, this.f9133);
        zzbfp.m10185(parcel, 15, this.f9134);
        zzbfp.m10195(parcel, 16, this.f9144);
        zzbfp.m10182(parcel, r0);
    }
}
