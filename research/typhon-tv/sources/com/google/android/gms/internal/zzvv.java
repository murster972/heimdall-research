package com.google.android.gms.internal;

import android.location.Location;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzvv implements NativeMediationAdRequest {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f5490;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzpe f5491;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final List<String> f5492 = new ArrayList();

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f5493;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Map<String, Boolean> f5494 = new HashMap();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Location f5495;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f5496;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f5497;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Set<String> f5498;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Date f5499;

    public zzvv(Date date, int i, Set<String> set, Location location, boolean z, int i2, zzpe zzpe, List<String> list, boolean z2) {
        this.f5499 = date;
        this.f5496 = i;
        this.f5498 = set;
        this.f5495 = location;
        this.f5497 = z;
        this.f5490 = i2;
        this.f5491 = zzpe;
        this.f5493 = z2;
        if (list != null) {
            for (String next : list) {
                if (next.startsWith("custom:")) {
                    String[] split = next.split(":", 3);
                    if (split.length == 3) {
                        if ("true".equals(split[2])) {
                            this.f5494.put(split[1], true);
                        } else if ("false".equals(split[2])) {
                            this.f5494.put(split[1], false);
                        }
                    }
                } else {
                    this.f5492.add(next);
                }
            }
        }
    }

    public final float getAdVolume() {
        return zzly.m5559().m5560();
    }

    public final Date getBirthday() {
        return this.f5499;
    }

    public final int getGender() {
        return this.f5496;
    }

    public final Set<String> getKeywords() {
        return this.f5498;
    }

    public final Location getLocation() {
        return this.f5495;
    }

    public final NativeAdOptions getNativeAdOptions() {
        if (this.f5491 == null) {
            return null;
        }
        NativeAdOptions.Builder requestMultipleImages = new NativeAdOptions.Builder().setReturnUrlsForImageAssets(this.f5491.f5284).setImageOrientation(this.f5491.f5286).setRequestMultipleImages(this.f5491.f5285);
        if (this.f5491.f5287 >= 2) {
            requestMultipleImages.setAdChoicesPlacement(this.f5491.f5283);
        }
        if (this.f5491.f5287 >= 3 && this.f5491.f5282 != null) {
            requestMultipleImages.setVideoOptions(new VideoOptions(this.f5491.f5282));
        }
        return requestMultipleImages.build();
    }

    public final boolean isAdMuted() {
        return zzly.m5559().m5561();
    }

    public final boolean isAppInstallAdRequested() {
        return this.f5492 != null && this.f5492.contains("2");
    }

    public final boolean isContentAdRequested() {
        return this.f5492 != null && this.f5492.contains(PubnativeRequest.LEGACY_ZONE_ID);
    }

    public final boolean isDesignedForFamilies() {
        return this.f5493;
    }

    public final boolean isTesting() {
        return this.f5497;
    }

    public final int taggedForChildDirectedTreatment() {
        return this.f5490;
    }

    public final boolean zzmo() {
        return this.f5492 != null && this.f5492.contains("3");
    }

    public final Map<String, Boolean> zzmp() {
        return this.f5494;
    }
}
