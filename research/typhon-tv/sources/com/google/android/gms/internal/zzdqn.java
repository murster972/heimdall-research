package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

public final class zzdqn implements zzdpp {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final byte[] f9943 = new byte[0];

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzdtd f9944;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzdpp f9945;

    public zzdqn(zzdtd zzdtd, zzdpp zzdpp) {
        this.f9944 = zzdtd;
        this.f9945 = zzdpp;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m11724(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] r0 = zzdqe.m11672(this.f9944).m12624();
        byte[] r1 = this.f9945.m11649(r0, f9943);
        byte[] r02 = ((zzdpp) zzdqe.m11679(this.f9944.m12037(), r0)).m11649(bArr, bArr2);
        return ByteBuffer.allocate(r1.length + 4 + r02.length).putInt(r1.length).put(r1).put(r02).array();
    }
}
