package com.google.android.gms.internal;

import java.io.UnsupportedEncodingException;

public class zzau extends zzr<String> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzy<String> f8390;

    public zzau(int i, String str, zzy<String> zzy, zzx zzx) {
        super(i, str, zzx);
        this.f8390 = zzy;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzw<String> m9738(zzp zzp) {
        String str;
        try {
            str = new String(zzp.f5278, zzao.m9717(zzp.f5280));
        } catch (UnsupportedEncodingException e) {
            str = new String(zzp.f5278);
        }
        return zzw.m13620(str, zzao.m9715(zzp));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m9739(String str) {
        if (this.f8390 != null) {
            this.f8390.m13638(str);
        }
    }
}
