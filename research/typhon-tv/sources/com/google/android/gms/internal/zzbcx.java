package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;

public final class zzbcx {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Charset f8676;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static String f8677 = zzbcm.m10039("com.google.cast.multizone");

    /* renamed from: 连任  reason: contains not printable characters */
    private static Api.zzf<Object> f8678 = new Api.zzf<>();

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Api.zzf<Object> f8679 = new Api.zzf<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private static Api.zzf<Object> f8680 = new Api.zzf<>();

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Api.zzf<Object> f8681 = new Api.zzf<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Api.zzf<zzbcf> f8682 = new Api.zzf<>();

    static {
        Charset charset = null;
        try {
            charset = Charset.forName("UTF-8");
        } catch (IllegalCharsetNameException | UnsupportedCharsetException e) {
        }
        f8676 = charset;
    }
}
