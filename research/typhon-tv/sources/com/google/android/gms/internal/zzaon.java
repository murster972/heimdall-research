package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

final class zzaon implements DialogInterface.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ JsPromptResult f8379;

    zzaon(JsPromptResult jsPromptResult) {
        this.f8379 = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f8379.cancel();
    }
}
