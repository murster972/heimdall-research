package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzcbp extends zzbfm {
    public static final Parcelable.Creator<zzcbp> CREATOR = new zzcbq();

    /* renamed from: 靐  reason: contains not printable characters */
    private String f9025;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f9026;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f9027;

    zzcbp(int i, String str, String str2) {
        this.f9027 = i;
        this.f9025 = str;
        this.f9026 = str2;
    }

    public zzcbp(String str, String str2) {
        this(1, str, str2);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f9027);
        zzbfp.m10193(parcel, 2, this.f9025, false);
        zzbfp.m10193(parcel, 3, this.f9026, false);
        zzbfp.m10182(parcel, r0);
    }
}
