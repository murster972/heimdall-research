package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.util.zzs;
import java.lang.reflect.InvocationTargetException;
import net.pubnative.library.request.PubnativeRequest;

public final class zzcgn extends zzcjk {

    /* renamed from: 龘  reason: contains not printable characters */
    private Boolean f9157;

    zzcgn(zzcim zzcim) {
        super(zzcim);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static long m10522() {
        return zzchc.f9229.m10671().longValue();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static long m10523() {
        return zzchc.f9204.m10671().longValue();
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static boolean m10524() {
        return zzchc.f9240.m10671().booleanValue();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcjn m10525() {
        return super.m11091();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchh m10526() {
        return super.m11092();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgu m10527() {
        return super.m11093();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchi m10528() {
        return super.m11094();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgo m10529() {
        return super.m11095();
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchm m10530() {
        return super.m11096();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Context m10531() {
        return super.m11097();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchx m10532() {
        return super.m11098();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcig m10533() {
        return super.m11099();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclf m10534() {
        return super.m11100();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcih m10535() {
        return super.m11101();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgn m10536() {
        return super.m11102();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckg m10537() {
        return super.m11103();
    }

    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10538() {
        if (this.f9157 == null) {
            synchronized (this) {
                if (this.f9157 == null) {
                    ApplicationInfo applicationInfo = m11097().getApplicationInfo();
                    String r1 = zzs.m9274();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.f9157 = Boolean.valueOf(str != null && str.equals(r1));
                    }
                    if (this.f9157 == null) {
                        this.f9157 = Boolean.TRUE;
                        m11096().m10832().m10849("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.f9157.booleanValue();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final boolean m10539() {
        Boolean r0 = m10545("firebase_analytics_collection_deactivated");
        return r0 != null && r0.booleanValue();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzckc m10540() {
        return super.m11104();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzd m10541() {
        return super.m11105();
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public final String m10542() {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke((Object) null, new Object[]{"debug.firebase.analytics.app", ""});
        } catch (ClassNotFoundException e) {
            m11096().m10832().m10850("Could not find SystemProperties class", e);
        } catch (NoSuchMethodException e2) {
            m11096().m10832().m10850("Could not find SystemProperties.get() method", e2);
        } catch (IllegalAccessException e3) {
            m11096().m10832().m10850("Could not access SystemProperties.get()", e3);
        } catch (InvocationTargetException e4) {
            m11096().m10832().m10850("SystemProperties.get() threw an exception", e4);
        }
        return "";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgk m10543() {
        return super.m11106();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m10544(String str, zzchd<Integer> zzchd) {
        if (str == null) {
            return zzchd.m10671().intValue();
        }
        String r0 = m11099().m10945(str, zzchd.m10673());
        if (TextUtils.isEmpty(r0)) {
            return zzchd.m10671().intValue();
        }
        try {
            return zzchd.m10672(Integer.valueOf(Integer.valueOf(r0).intValue())).intValue();
        } catch (NumberFormatException e) {
            return zzchd.m10671().intValue();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final Boolean m10545(String str) {
        zzbq.m9122(str);
        try {
            if (m11097().getPackageManager() == null) {
                m11096().m10832().m10849("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo r1 = zzbhf.m10231(m11097()).m10226(m11097().getPackageName(), 128);
            if (r1 == null) {
                m11096().m10832().m10849("Failed to load metadata: ApplicationInfo is null");
                return null;
            } else if (r1.metaData == null) {
                m11096().m10832().m10849("Failed to load metadata: Metadata bundle is null");
                return null;
            } else if (r1.metaData.containsKey(str)) {
                return Boolean.valueOf(r1.metaData.getBoolean(str));
            } else {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e) {
            m11096().m10832().m10850("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10546() {
        super.m11107();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzcgd m10547() {
        return super.m11108();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10548() {
        super.m11109();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m10549(String str) {
        return PubnativeRequest.LEGACY_ZONE_ID.equals(m11099().m10945(str, "gaia_collection_enabled"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m10550(String str) {
        return m10544(str, zzchc.f9219);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m10551(String str, zzchd<Long> zzchd) {
        if (str == null) {
            return zzchd.m10671().longValue();
        }
        String r0 = m11099().m10945(str, zzchd.m10673());
        if (TextUtils.isEmpty(r0)) {
            return zzchd.m10671().longValue();
        }
        try {
            return zzchd.m10672(Long.valueOf(Long.valueOf(r0).longValue())).longValue();
        } catch (NumberFormatException e) {
            return zzchd.m10671().longValue();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m10552() {
        super.m11110();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzchk m10553() {
        return super.m11111();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ zzclq m10554() {
        return super.m11112();
    }
}
