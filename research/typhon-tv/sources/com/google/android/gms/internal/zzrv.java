package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface zzrv extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    ParcelFileDescriptor m13382(zzrr zzrr) throws RemoteException;
}
