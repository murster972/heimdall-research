package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public final class zzchg extends zzeu implements zzche {
    zzchg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10690(zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgi);
        m12298(6, v_);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10691(zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgi);
        m12298(18, v_);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m10692(zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgi);
        Parcel r0 = m12300(11, v_);
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcln> m10693(zzcgi zzcgi, boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgi);
        zzew.m12307(v_, z);
        Parcel r0 = m12300(7, v_);
        ArrayList<zzcln> createTypedArrayList = r0.createTypedArrayList(zzcln.CREATOR);
        r0.recycle();
        return createTypedArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcgl> m10694(String str, String str2, zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        zzew.m12306(v_, (Parcelable) zzcgi);
        Parcel r0 = m12300(16, v_);
        ArrayList<zzcgl> createTypedArrayList = r0.createTypedArrayList(zzcgl.CREATOR);
        r0.recycle();
        return createTypedArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcgl> m10695(String str, String str2, String str3) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        v_.writeString(str3);
        Parcel r0 = m12300(17, v_);
        ArrayList<zzcgl> createTypedArrayList = r0.createTypedArrayList(zzcgl.CREATOR);
        r0.recycle();
        return createTypedArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcln> m10696(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        v_.writeString(str3);
        zzew.m12307(v_, z);
        Parcel r0 = m12300(15, v_);
        ArrayList<zzcln> createTypedArrayList = r0.createTypedArrayList(zzcln.CREATOR);
        r0.recycle();
        return createTypedArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzcln> m10697(String str, String str2, boolean z, zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        zzew.m12307(v_, z);
        zzew.m12306(v_, (Parcelable) zzcgi);
        Parcel r0 = m12300(14, v_);
        ArrayList<zzcln> createTypedArrayList = r0.createTypedArrayList(zzcln.CREATOR);
        r0.recycle();
        return createTypedArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10698(long j, String str, String str2, String str3) throws RemoteException {
        Parcel v_ = v_();
        v_.writeLong(j);
        v_.writeString(str);
        v_.writeString(str2);
        v_.writeString(str3);
        m12298(10, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10699(zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgi);
        m12298(4, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10700(zzcgl zzcgl) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgl);
        m12298(13, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10701(zzcgl zzcgl, zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcgl);
        zzew.m12306(v_, (Parcelable) zzcgi);
        m12298(12, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10702(zzcha zzcha, zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcha);
        zzew.m12306(v_, (Parcelable) zzcgi);
        m12298(1, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10703(zzcha zzcha, String str, String str2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcha);
        v_.writeString(str);
        v_.writeString(str2);
        m12298(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10704(zzcln zzcln, zzcgi zzcgi) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcln);
        zzew.m12306(v_, (Parcelable) zzcgi);
        m12298(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m10705(zzcha zzcha, String str) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzcha);
        v_.writeString(str);
        Parcel r0 = m12300(9, v_);
        byte[] createByteArray = r0.createByteArray();
        r0.recycle();
        return createByteArray;
    }
}
