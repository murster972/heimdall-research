package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbg;

@zzzv
public final class zzadc extends zzadi {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f4014;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f4015;

    public zzadc(String str, int i) {
        this.f4015 = str;
        this.f4014 = i;
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof zzadc)) {
            return false;
        }
        zzadc zzadc = (zzadc) obj;
        return zzbg.m9113(this.f4015, zzadc.f4015) && zzbg.m9113(Integer.valueOf(this.f4014), Integer.valueOf(zzadc.f4014));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m4325() {
        return this.f4014;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m4326() {
        return this.f4015;
    }
}
