package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbbu implements Parcelable.Creator<zzbbt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        String str = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzbbt(str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbbt[i];
    }
}
