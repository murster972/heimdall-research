package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrq;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdrm extends zzffu<zzdrm, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdrm f9986;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdrm> f9987;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzfes f9988 = zzfes.zzpfg;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdrq f9989;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f9990;

    public static final class zza extends zzffu.zza<zzdrm, zza> implements zzfhg {
        private zza() {
            super(zzdrm.f9986);
        }

        /* synthetic */ zza(zzdrn zzdrn) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11842(int i) {
            m12541();
            ((zzdrm) this.f10398).m11831(0);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11843(zzdrq zzdrq) {
            m12541();
            ((zzdrm) this.f10398).m11835(zzdrq);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m11844(zzfes zzfes) {
            m12541();
            ((zzdrm) this.f10398).m11829(zzfes);
            return this;
        }
    }

    static {
        zzdrm zzdrm = new zzdrm();
        f9986 = zzdrm;
        zzdrm.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdrm.f10394.m12718();
    }

    private zzdrm() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zza m11828() {
        zzdrm zzdrm = f9986;
        zzffu.zza zza2 = (zzffu.zza) zzdrm.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
        zza2.m12545(zzdrm);
        return (zza) zza2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11829(zzfes zzfes) {
        if (zzfes == null) {
            throw new NullPointerException();
        }
        this.f9988 = zzfes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdrm m11830(zzfes zzfes) throws zzfge {
        return (zzdrm) zzffu.m12526(f9986, zzfes);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11831(int i) {
        this.f9990 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11835(zzdrq zzdrq) {
        if (zzdrq == null) {
            throw new NullPointerException();
        }
        this.f9989 = zzdrq;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdrq m11836() {
        return this.f9989 == null ? zzdrq.m11852() : this.f9989;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11837() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f9990 != 0) {
            i2 = zzffg.m5245(1, this.f9990) + 0;
        }
        if (this.f9989 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f9989 == null ? zzdrq.m11852() : this.f9989);
        }
        if (!this.f9988.isEmpty()) {
            i2 += zzffg.m5261(3, this.f9988);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzfes m11838() {
        return this.f9988;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11839() {
        return this.f9990;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11840(int i, Object obj, Object obj2) {
        zzdrq.zza zza2;
        boolean z = true;
        switch (zzdrn.f9991[i - 1]) {
            case 1:
                return new zzdrm();
            case 2:
                return f9986;
            case 3:
                return null;
            case 4:
                return new zza((zzdrn) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdrm zzdrm = (zzdrm) obj2;
                this.f9990 = zzh.m12569(this.f9990 != 0, this.f9990, zzdrm.f9990 != 0, zzdrm.f9990);
                this.f9989 = (zzdrq) zzh.m12572(this.f9989, zzdrm.f9989);
                boolean z2 = this.f9988 != zzfes.zzpfg;
                zzfes zzfes = this.f9988;
                if (zzdrm.f9988 == zzfes.zzpfg) {
                    z = false;
                }
                this.f9988 = zzh.m12570(z2, zzfes, z, zzdrm.f9988);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 8:
                                    this.f9990 = zzffb.m12402();
                                    break;
                                case 18:
                                    if (this.f9989 != null) {
                                        zzdrq zzdrq = this.f9989;
                                        zzffu.zza zza3 = (zzffu.zza) zzdrq.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdrq);
                                        zza2 = (zzdrq.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f9989 = (zzdrq) zzffb.m12416(zzdrq.m11852(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f9989);
                                        this.f9989 = (zzdrq) zza2.m12543();
                                        break;
                                    }
                                case 26:
                                    this.f9988 = zzffb.m12401();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f9987 == null) {
                    synchronized (zzdrm.class) {
                        if (f9987 == null) {
                            f9987 = new zzffu.zzb(f9986);
                        }
                    }
                }
                return f9987;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f9986;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11841(zzffg zzffg) throws IOException {
        if (this.f9990 != 0) {
            zzffg.m5281(1, this.f9990);
        }
        if (this.f9989 != null) {
            zzffg.m5289(2, (zzfhe) this.f9989 == null ? zzdrq.m11852() : this.f9989);
        }
        if (!this.f9988.isEmpty()) {
            zzffg.m5288(3, this.f9988);
        }
        this.f10394.m12719(zzffg);
    }
}
