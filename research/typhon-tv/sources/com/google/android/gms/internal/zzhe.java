package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;

@zzzv
public final class zzhe {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Object f4668 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private ArrayList<String> f4669 = new ArrayList<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private ArrayList<String> f4670 = new ArrayList<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f4671;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f4672 = "";

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f4673 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ArrayList<zzhp> f4674 = new ArrayList<>();

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f4675 = 0;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f4676 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzia f4677;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f4678;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzhr f4679;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f4680;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f4681;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String f4682 = "";

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f4683 = "";

    public zzhe(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f4681 = i;
        this.f4678 = i2;
        this.f4680 = i3;
        this.f4679 = new zzhr(i4);
        this.f4677 = new zzia(i5, i6, i7);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m5358(String str, boolean z, float f, float f2, float f3, float f4) {
        if (str != null && str.length() >= this.f4680) {
            synchronized (this.f4668) {
                this.f4669.add(str);
                this.f4675 += str.length();
                if (z) {
                    this.f4670.add(str);
                    this.f4674.add(new zzhp(f, f2, f3, f4, this.f4670.size() - 1));
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m5359(ArrayList<String> arrayList, int i) {
        if (arrayList.isEmpty()) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList2.get(i2);
            i2++;
            stringBuffer.append((String) obj);
            stringBuffer.append(' ');
            if (stringBuffer.length() > 100) {
                break;
            }
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        String stringBuffer2 = stringBuffer.toString();
        return stringBuffer2.length() >= 100 ? stringBuffer2.substring(0, 100) : stringBuffer2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzhe)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zzhe zzhe = (zzhe) obj;
        return zzhe.f4672 != null && zzhe.f4672.equals(this.f4672);
    }

    public final int hashCode() {
        return this.f4672.hashCode();
    }

    public final String toString() {
        int i = this.f4676;
        int i2 = this.f4671;
        int i3 = this.f4675;
        String r3 = m5359(this.f4669, 100);
        String r4 = m5359(this.f4670, 100);
        String str = this.f4672;
        String str2 = this.f4682;
        String str3 = this.f4683;
        return new StringBuilder(String.valueOf(r3).length() + 165 + String.valueOf(r4).length() + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str3).length()).append("ActivityContent fetchId: ").append(i).append(" score:").append(i2).append(" total_length:").append(i3).append("\n text: ").append(r3).append("\n viewableText").append(r4).append("\n signture: ").append(str).append("\n viewableSignture: ").append(str2).append("\n viewableSignatureForVertical: ").append(str3).toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5360() {
        synchronized (this.f4668) {
            this.f4673--;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m5361() {
        synchronized (this.f4668) {
            this.f4673++;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m5362() {
        synchronized (this.f4668) {
            int i = (this.f4675 * this.f4681) + (this.f4676 * this.f4678);
            if (i > this.f4671) {
                this.f4671 = i;
                if (((Boolean) zzkb.m5481().m5595(zznh.f5130)).booleanValue() && !zzbs.zzem().m4508()) {
                    this.f4672 = this.f4679.m5398(this.f4669);
                    this.f4682 = this.f4679.m5398(this.f4670);
                }
                if (((Boolean) zzkb.m5481().m5595(zznh.f5134)).booleanValue() && !zzbs.zzem().m4487()) {
                    this.f4683 = this.f4677.m5409(this.f4670, this.f4674);
                }
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m5363() {
        return this.f4671;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m5364() {
        return this.f4675;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m5365() {
        synchronized (this.f4668) {
            this.f4671 -= 100;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5366() {
        return this.f4672;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5367(String str, boolean z, float f, float f2, float f3, float f4) {
        m5358(str, z, f, f2, f3, f4);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m5368() {
        return this.f4683;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m5369() {
        return this.f4682;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5370(int i) {
        this.f4676 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5371(String str, boolean z, float f, float f2, float f3, float f4) {
        m5358(str, z, f, f2, f3, f4);
        synchronized (this.f4668) {
            if (this.f4673 < 0) {
                zzagf.m4792("ActivityContent: negative number of WebViews.");
            }
            m5362();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5372() {
        boolean z;
        synchronized (this.f4668) {
            z = this.f4673 == 0;
        }
        return z;
    }
}
