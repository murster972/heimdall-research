package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzei extends zzet {
    public zzei(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12283() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.f10284) {
            zzcz zzcz = new zzcz((String) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()}));
            synchronized (this.f10284) {
                this.f10284.f8468 = Long.valueOf(zzcz.f9846);
                this.f10284.f8438 = Long.valueOf(zzcz.f9845);
            }
        }
    }
}
