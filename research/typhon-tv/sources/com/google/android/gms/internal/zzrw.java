package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzrw extends zzeu implements zzrv {
    zzrw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ParcelFileDescriptor m13383(zzrr zzrr) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzrr);
        Parcel r1 = m12300(1, v_);
        ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) zzew.m12304(r1, ParcelFileDescriptor.CREATOR);
        r1.recycle();
        return parcelFileDescriptor;
    }
}
