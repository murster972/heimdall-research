package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzcle;

public final class zzcla<T extends Context & zzcle> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final T f9620;

    public zzcla(T t) {
        zzbq.m9120(t);
        this.f9620 = t;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzchm m11284() {
        return zzcim.m11006((Context) this.f9620).m11016();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m11285(Runnable runnable) {
        zzcim r0 = zzcim.m11006((Context) this.f9620);
        r0.m11016();
        r0.m11018().m10986((Runnable) new zzcld(this, r0, runnable));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m11286(Context context, boolean z) {
        zzbq.m9120(context);
        return Build.VERSION.SDK_INT >= 24 ? zzclq.m11381(context, "com.google.android.gms.measurement.AppMeasurementJobService") : zzclq.m11381(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m11287() {
        zzcim.m11006((Context) this.f9620).m11016().m10848().m10849("Local AppMeasurementService is shutting down");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m11288(Intent intent) {
        if (intent == null) {
            m11284().m10832().m10849("onUnbind called with null intent");
        } else {
            m11284().m10848().m10850("onUnbind called for intent. action", intent.getAction());
        }
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m11289(Intent intent) {
        if (intent == null) {
            m11284().m10832().m10849("onRebind called with null intent");
            return;
        }
        m11284().m10848().m10850("onRebind called. action", intent.getAction());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11290(Intent intent, int i, int i2) {
        zzchm r0 = zzcim.m11006((Context) this.f9620).m11016();
        if (intent == null) {
            r0.m10834().m10849("AppMeasurementService started with null intent");
        } else {
            String action = intent.getAction();
            r0.m10848().m10851("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
            if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
                m11285((Runnable) new zzclb(this, i2, r0, intent));
            }
        }
        return 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m11291(Intent intent) {
        if (intent == null) {
            m11284().m10832().m10849("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new zzcir(zzcim.m11006((Context) this.f9620));
        }
        m11284().m10834().m10850("onBind received unknown action", action);
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11292() {
        zzcim.m11006((Context) this.f9620).m11016().m10848().m10849("Local AppMeasurementService is starting up");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m11293(int i, zzchm zzchm, Intent intent) {
        if (((zzcle) this.f9620).m11298(i)) {
            zzchm.m10848().m10850("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            m11284().m10848().m10849("Completed wakeful intent.");
            ((zzcle) this.f9620).m11297(intent);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m11294(zzchm zzchm, JobParameters jobParameters) {
        zzchm.m10848().m10849("AppMeasurementJobService processed last upload request.");
        ((zzcle) this.f9620).m11296(jobParameters, false);
    }

    @TargetApi(24)
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11295(JobParameters jobParameters) {
        zzchm r0 = zzcim.m11006((Context) this.f9620).m11016();
        String string = jobParameters.getExtras().getString("action");
        r0.m10848().m10850("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        m11285((Runnable) new zzclc(this, r0, jobParameters));
        return true;
    }
}
