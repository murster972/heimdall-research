package com.google.android.gms.internal;

import android.os.Process;
import com.google.android.gms.ads.internal.zzbs;
import java.util.concurrent.Callable;

final class zzahk implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Callable f8203;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzalf f8204;

    zzahk(zzalf zzalf, Callable callable) {
        this.f8204 = zzalf;
        this.f8203 = callable;
    }

    public final void run() {
        try {
            Process.setThreadPriority(10);
            this.f8204.m4822(this.f8203.call());
        } catch (Exception e) {
            zzbs.zzem().m4505((Throwable) e, "AdThreadPool.submit");
            this.f8204.m4824(e);
        }
    }
}
