package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;

public class zzew {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ClassLoader f10290 = zzew.class.getClassLoader();

    private zzew() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static ArrayList m12301(Parcel parcel) {
        return parcel.readArrayList(f10290);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m12302(Parcel parcel, Parcelable parcelable) {
        if (parcelable == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcelable.writeToParcel(parcel, 1);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static HashMap m12303(Parcel parcel) {
        return parcel.readHashMap(f10290);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends Parcelable> T m12304(Parcel parcel, Parcelable.Creator<T> creator) {
        if (parcel.readInt() == 0) {
            return null;
        }
        return (Parcelable) creator.createFromParcel(parcel);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12305(Parcel parcel, IInterface iInterface) {
        if (iInterface == null) {
            parcel.writeStrongBinder((IBinder) null);
        } else {
            parcel.writeStrongBinder(iInterface.asBinder());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12306(Parcel parcel, Parcelable parcelable) {
        if (parcelable == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcelable.writeToParcel(parcel, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m12307(Parcel parcel, boolean z) {
        parcel.writeInt(z ? 1 : 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m12308(Parcel parcel) {
        return parcel.readInt() != 0;
    }
}
