package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.dynamic.zzn;
import java.util.List;
import java.util.WeakHashMap;

@zzzv
public final class zzqp implements NativeCustomTemplateAd {

    /* renamed from: 龘  reason: contains not printable characters */
    private static WeakHashMap<IBinder, zzqp> f5329 = new WeakHashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzqm f5330;

    /* renamed from: 麤  reason: contains not printable characters */
    private final VideoController f5331 = new VideoController();

    /* renamed from: 齉  reason: contains not printable characters */
    private final MediaView f5332;

    private zzqp(zzqm zzqm) {
        Context context;
        MediaView mediaView = null;
        this.f5330 = zzqm;
        try {
            context = (Context) zzn.m9307(zzqm.m13320());
        } catch (RemoteException | NullPointerException e) {
            zzakb.m4793("Unable to inflate MediaView.", e);
            context = null;
        }
        if (context != null) {
            MediaView mediaView2 = new MediaView(context);
            try {
                mediaView = !this.f5330.m13328(zzn.m9306(mediaView2)) ? null : mediaView2;
            } catch (RemoteException e2) {
                zzakb.m4793("Unable to render video in MediaView.", e2);
            }
        }
        this.f5332 = mediaView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzqp m5796(zzqm zzqm) {
        zzqp zzqp;
        synchronized (f5329) {
            zzqp = f5329.get(zzqm.asBinder());
            if (zzqp == null) {
                zzqp = new zzqp(zzqm);
                f5329.put(zzqm.asBinder(), zzqp);
            }
        }
        return zzqp;
    }

    public final void destroy() {
        try {
            this.f5330.m13318();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to destroy ad.", e);
        }
    }

    public final List<String> getAvailableAssetNames() {
        try {
            return this.f5330.m13327();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get available asset names.", e);
            return null;
        }
    }

    public final String getCustomTemplateId() {
        try {
            return this.f5330.m13319();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get custom template id.", e);
            return null;
        }
    }

    public final NativeAd.Image getImage(String str) {
        try {
            zzpq r1 = this.f5330.m13322(str);
            if (r1 != null) {
                return new zzpt(r1);
            }
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get image.", e);
        }
        return null;
    }

    public final CharSequence getText(String str) {
        try {
            return this.f5330.m13326(str);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to get string.", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            zzll r0 = this.f5330.m13324();
            if (r0 != null) {
                this.f5331.zza(r0);
            }
        } catch (RemoteException e) {
            zzakb.m4793("Exception occurred while getting video controller", e);
        }
        return this.f5331;
    }

    public final MediaView getVideoMediaView() {
        return this.f5332;
    }

    public final void performClick(String str) {
        try {
            this.f5330.m13325(str);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to perform click.", e);
        }
    }

    public final void recordImpression() {
        try {
            this.f5330.m13323();
        } catch (RemoteException e) {
            zzakb.m4793("Failed to record impression.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzqm m5797() {
        return this.f5330;
    }
}
