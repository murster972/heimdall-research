package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.AppEventListener;

@zzzv
public final class zzjp extends zzky {

    /* renamed from: 龘  reason: contains not printable characters */
    private final AppEventListener f4799;

    public zzjp(AppEventListener appEventListener) {
        this.f4799 = appEventListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final AppEventListener m5459() {
        return this.f4799;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5460(String str, String str2) {
        this.f4799.onAppEvent(str, str2);
    }
}
