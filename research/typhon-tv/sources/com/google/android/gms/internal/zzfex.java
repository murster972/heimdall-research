package com.google.android.gms.internal;

final class zzfex {

    /* renamed from: 靐  reason: contains not printable characters */
    private final byte[] f10349;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzffg f10350;

    private zzfex(int i) {
        this.f10349 = new byte[i];
        this.f10350 = zzffg.m5271(this.f10349);
    }

    /* synthetic */ zzfex(int i, zzfet zzfet) {
        this(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzffg m12386() {
        return this.f10350;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzfes m12387() {
        this.f10350.m5272();
        return new zzfez(this.f10349);
    }
}
