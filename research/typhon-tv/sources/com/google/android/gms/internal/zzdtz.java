package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzdtz implements zzdpp {

    /* renamed from: 龘  reason: contains not printable characters */
    private final SecretKey f10177;

    public zzdtz(byte[] bArr) {
        this.f10177 = new SecretKeySpec(bArr, "AES");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12172(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        if (bArr.length > 2147483619) {
            throw new GeneralSecurityException("plaintext too long");
        }
        byte[] bArr3 = new byte[(bArr.length + 12 + 16)];
        byte[] r1 = zzdvi.m12235(12);
        System.arraycopy(r1, 0, bArr3, 0, 12);
        Cipher r0 = zzduu.f10221.m12217("AES/GCM/NoPadding");
        r0.init(1, this.f10177, new GCMParameterSpec(128, r1));
        r0.updateAAD(bArr2);
        r0.doFinal(bArr, 0, bArr.length, bArr3, 12);
        return bArr3;
    }
}
