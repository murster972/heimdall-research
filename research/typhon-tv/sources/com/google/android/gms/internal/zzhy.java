package com.google.android.gms.internal;

public final class zzhy {

    /* renamed from: 靐  reason: contains not printable characters */
    final String f10705;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f10706;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f10707;

    zzhy(long j, String str, int i) {
        this.f10707 = j;
        this.f10705 = str;
        this.f10706 = i;
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof zzhy)) {
            return false;
        }
        return ((zzhy) obj).f10707 == this.f10707 && ((zzhy) obj).f10706 == this.f10706;
    }

    public final int hashCode() {
        return (int) this.f10707;
    }
}
