package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsc;
import com.google.android.gms.internal.zzdsm;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdsg extends zzffu<zzdsg, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final zzdsg f10033;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static volatile zzfhk<zzdsg> f10034;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f10035;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdsc f10036;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdsm f10037;

    public static final class zza extends zzffu.zza<zzdsg, zza> implements zzfhg {
        private zza() {
            super(zzdsg.f10033);
        }

        /* synthetic */ zza(zzdsh zzdsh) {
            this();
        }
    }

    static {
        zzdsg zzdsg = new zzdsg();
        f10033 = zzdsg;
        zzdsg.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdsg.f10394.m12718();
    }

    private zzdsg() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static zzdsg m11916() {
        return f10033;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdsc m11917() {
        return this.f10036 == null ? zzdsc.m11903() : this.f10036;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11918() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10037 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f10037 == null ? zzdsm.m11965() : this.f10037) + 0;
        }
        if (this.f10036 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10036 == null ? zzdsc.m11903() : this.f10036);
        }
        if (this.f10035 != zzdsa.UNKNOWN_FORMAT.zzhq()) {
            i2 += zzffg.m5236(3, this.f10035);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsa m11919() {
        zzdsa zzfn = zzdsa.zzfn(this.f10035);
        return zzfn == null ? zzdsa.UNRECOGNIZED : zzfn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdsm m11920() {
        return this.f10037 == null ? zzdsm.m11965() : this.f10037;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11921(int i, Object obj, Object obj2) {
        zzdsc.zza zza2;
        zzdsm.zza zza3;
        boolean z = true;
        switch (zzdsh.f10038[i - 1]) {
            case 1:
                return new zzdsg();
            case 2:
                return f10033;
            case 3:
                return null;
            case 4:
                return new zza((zzdsh) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdsg zzdsg = (zzdsg) obj2;
                this.f10037 = (zzdsm) zzh.m12572(this.f10037, zzdsg.f10037);
                this.f10036 = (zzdsc) zzh.m12572(this.f10036, zzdsg.f10036);
                boolean z2 = this.f10035 != 0;
                int i2 = this.f10035;
                if (zzdsg.f10035 == 0) {
                    z = false;
                }
                this.f10035 = zzh.m12569(z2, i2, z, zzdsg.f10035);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 10:
                                    if (this.f10037 != null) {
                                        zzdsm zzdsm = this.f10037;
                                        zzffu.zza zza4 = (zzffu.zza) zzdsm.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza4.m12545(zzdsm);
                                        zza3 = (zzdsm.zza) zza4;
                                    } else {
                                        zza3 = null;
                                    }
                                    this.f10037 = (zzdsm) zzffb.m12416(zzdsm.m11965(), zzffm);
                                    if (zza3 == null) {
                                        break;
                                    } else {
                                        zza3.m12545(this.f10037);
                                        this.f10037 = (zzdsm) zza3.m12543();
                                        break;
                                    }
                                case 18:
                                    if (this.f10036 != null) {
                                        zzdsc zzdsc = this.f10036;
                                        zzffu.zza zza5 = (zzffu.zza) zzdsc.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza5.m12545(zzdsc);
                                        zza2 = (zzdsc.zza) zza5;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10036 = (zzdsc) zzffb.m12416(zzdsc.m11903(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10036);
                                        this.f10036 = (zzdsc) zza2.m12543();
                                        break;
                                    }
                                case 24:
                                    this.f10035 = zzffb.m12405();
                                    break;
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10034 == null) {
                    synchronized (zzdsg.class) {
                        if (f10034 == null) {
                            f10034 = new zzffu.zzb(f10033);
                        }
                    }
                }
                return f10034;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10033;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11922(zzffg zzffg) throws IOException {
        if (this.f10037 != null) {
            zzffg.m5289(1, (zzfhe) this.f10037 == null ? zzdsm.m11965() : this.f10037);
        }
        if (this.f10036 != null) {
            zzffg.m5289(2, (zzfhe) this.f10036 == null ? zzdsc.m11903() : this.f10036);
        }
        if (this.f10035 != zzdsa.UNKNOWN_FORMAT.zzhq()) {
            zzffg.m5274(3, this.f10035);
        }
        this.f10394.m12719(zzffg);
    }
}
