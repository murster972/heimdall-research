package com.google.android.gms.internal;

import java.util.List;
import java.util.RandomAccess;

public interface zzfgd<E> extends List<E>, RandomAccess {
    /* renamed from: 靐  reason: contains not printable characters */
    void m12582();

    /* renamed from: 龘  reason: contains not printable characters */
    zzfgd<E> m12583(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m12584();
}
