package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfku extends zzfjm<zzfku> {

    /* renamed from: ʻ  reason: contains not printable characters */
    public long f10619 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f10620 = "";

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f10621 = "";

    /* renamed from: ʾ  reason: contains not printable characters */
    public zzfkt[] f10622 = zzfkt.m12919();

    /* renamed from: ˈ  reason: contains not printable characters */
    public int f10623 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    public String f10624 = "";

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f10625 = "";

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f10626 = "";

    /* renamed from: 连任  reason: contains not printable characters */
    public long f10627 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f10628 = "";

    /* renamed from: 麤  reason: contains not printable characters */
    public String f10629 = "";

    /* renamed from: 齉  reason: contains not printable characters */
    public long f10630 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10631 = "";

    public zzfku() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfku m12923(byte[] bArr) throws zzfjr {
        return (zzfku) zzfjs.m12869(new zzfku(), bArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12924() {
        int r0 = super.m12841();
        if (this.f10631 != null && !this.f10631.equals("")) {
            r0 += zzfjk.m12808(1, this.f10631);
        }
        if (this.f10628 != null && !this.f10628.equals("")) {
            r0 += zzfjk.m12808(2, this.f10628);
        }
        if (this.f10630 != 0) {
            r0 += zzfjk.m12814(3, this.f10630);
        }
        if (this.f10629 != null && !this.f10629.equals("")) {
            r0 += zzfjk.m12808(4, this.f10629);
        }
        if (this.f10627 != 0) {
            r0 += zzfjk.m12814(5, this.f10627);
        }
        if (this.f10619 != 0) {
            r0 += zzfjk.m12814(6, this.f10619);
        }
        if (this.f10620 != null && !this.f10620.equals("")) {
            r0 += zzfjk.m12808(7, this.f10620);
        }
        if (this.f10621 != null && !this.f10621.equals("")) {
            r0 += zzfjk.m12808(8, this.f10621);
        }
        if (this.f10624 != null && !this.f10624.equals("")) {
            r0 += zzfjk.m12808(9, this.f10624);
        }
        if (this.f10625 != null && !this.f10625.equals("")) {
            r0 += zzfjk.m12808(10, this.f10625);
        }
        if (this.f10626 != null && !this.f10626.equals("")) {
            r0 += zzfjk.m12808(11, this.f10626);
        }
        if (this.f10623 != 0) {
            r0 += zzfjk.m12806(12, this.f10623);
        }
        if (this.f10622 == null || this.f10622.length <= 0) {
            return r0;
        }
        int i = r0;
        for (zzfkt zzfkt : this.f10622) {
            if (zzfkt != null) {
                i += zzfjk.m12807(13, (zzfjs) zzfkt);
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12925(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10631 = zzfjj.m12791();
                    continue;
                case 18:
                    this.f10628 = zzfjj.m12791();
                    continue;
                case 24:
                    this.f10630 = zzfjj.m12793();
                    continue;
                case 34:
                    this.f10629 = zzfjj.m12791();
                    continue;
                case 40:
                    this.f10627 = zzfjj.m12793();
                    continue;
                case 48:
                    this.f10619 = zzfjj.m12793();
                    continue;
                case 58:
                    this.f10620 = zzfjj.m12791();
                    continue;
                case 66:
                    this.f10621 = zzfjj.m12791();
                    continue;
                case 74:
                    this.f10624 = zzfjj.m12791();
                    continue;
                case 82:
                    this.f10625 = zzfjj.m12791();
                    continue;
                case 90:
                    this.f10626 = zzfjj.m12791();
                    continue;
                case 96:
                    this.f10623 = zzfjj.m12798();
                    continue;
                case 106:
                    int r2 = zzfjv.m12883(zzfjj, 106);
                    int length = this.f10622 == null ? 0 : this.f10622.length;
                    zzfkt[] zzfktArr = new zzfkt[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10622, 0, zzfktArr, 0, length);
                    }
                    while (length < zzfktArr.length - 1) {
                        zzfktArr[length] = new zzfkt();
                        zzfjj.m12802((zzfjs) zzfktArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzfktArr[length] = new zzfkt();
                    zzfjj.m12802((zzfjs) zzfktArr[length]);
                    this.f10622 = zzfktArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12926(zzfjk zzfjk) throws IOException {
        if (this.f10631 != null && !this.f10631.equals("")) {
            zzfjk.m12835(1, this.f10631);
        }
        if (this.f10628 != null && !this.f10628.equals("")) {
            zzfjk.m12835(2, this.f10628);
        }
        if (this.f10630 != 0) {
            zzfjk.m12824(3, this.f10630);
        }
        if (this.f10629 != null && !this.f10629.equals("")) {
            zzfjk.m12835(4, this.f10629);
        }
        if (this.f10627 != 0) {
            zzfjk.m12824(5, this.f10627);
        }
        if (this.f10619 != 0) {
            zzfjk.m12824(6, this.f10619);
        }
        if (this.f10620 != null && !this.f10620.equals("")) {
            zzfjk.m12835(7, this.f10620);
        }
        if (this.f10621 != null && !this.f10621.equals("")) {
            zzfjk.m12835(8, this.f10621);
        }
        if (this.f10624 != null && !this.f10624.equals("")) {
            zzfjk.m12835(9, this.f10624);
        }
        if (this.f10625 != null && !this.f10625.equals("")) {
            zzfjk.m12835(10, this.f10625);
        }
        if (this.f10626 != null && !this.f10626.equals("")) {
            zzfjk.m12835(11, this.f10626);
        }
        if (this.f10623 != 0) {
            zzfjk.m12832(12, this.f10623);
        }
        if (this.f10622 != null && this.f10622.length > 0) {
            for (zzfkt zzfkt : this.f10622) {
                if (zzfkt != null) {
                    zzfjk.m12834(13, (zzfjs) zzfkt);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
