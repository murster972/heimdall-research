package com.google.android.gms.internal;

import java.lang.ref.WeakReference;

@zzzv
public final class zzaaw extends zzabf {

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<zzaal> f3819;

    public zzaaw(zzaal zzaal) {
        this.f3819 = new WeakReference<>(zzaal);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4260(zzaax zzaax) {
        zzaal zzaal = (zzaal) this.f3819.get();
        if (zzaal != null) {
            zzaal.m9438(zzaax);
        }
    }
}
