package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzwa implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10935;

    zzwa(zzvx zzvx) {
        this.f10935 = zzvx;
    }

    public final void run() {
        try {
            this.f10935.f5502.m13509();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLoaded.", e);
        }
    }
}
