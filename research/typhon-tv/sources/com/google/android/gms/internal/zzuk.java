package com.google.android.gms.internal;

@zzzv
public final class zzuk extends zzve {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzup f5440;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzuj f5441;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f5442 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5881() {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzco();
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m5882() {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzci();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5883() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.f5442
            monitor-enter(r1)
            com.google.android.gms.internal.zzup r0 = r3.f5440     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0012
            com.google.android.gms.internal.zzup r0 = r3.f5440     // Catch:{ all -> 0x001d }
            r2 = 0
            r0.m13444(r2)     // Catch:{ all -> 0x001d }
            r0 = 0
            r3.f5440 = r0     // Catch:{ all -> 0x001d }
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
        L_0x0011:
            return
        L_0x0012:
            com.google.android.gms.internal.zzuj r0 = r3.f5441     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            com.google.android.gms.internal.zzuj r0 = r3.f5441     // Catch:{ all -> 0x001d }
            r0.zzcn()     // Catch:{ all -> 0x001d }
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            goto L_0x0011
        L_0x001d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzuk.m5883():void");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m5884() {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzck();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5885() {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzcm();
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5886() {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzcl();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5887() {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzcj();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5888(int i) {
        synchronized (this.f5442) {
            if (this.f5440 != null) {
                this.f5440.m13444(i == 3 ? 1 : 2);
                this.f5440 = null;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5889(zzqm zzqm, String str) {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zza(zzqm, str);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5890(zzuj zzuj) {
        synchronized (this.f5442) {
            this.f5441 = zzuj;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5891(zzup zzup) {
        synchronized (this.f5442) {
            this.f5440 = zzup;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5892(com.google.android.gms.internal.zzvg r4) {
        /*
            r3 = this;
            java.lang.Object r1 = r3.f5442
            monitor-enter(r1)
            com.google.android.gms.internal.zzup r0 = r3.f5440     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0012
            com.google.android.gms.internal.zzup r0 = r3.f5440     // Catch:{ all -> 0x001d }
            r2 = 0
            r0.m13445(r2, r4)     // Catch:{ all -> 0x001d }
            r0 = 0
            r3.f5440 = r0     // Catch:{ all -> 0x001d }
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
        L_0x0011:
            return
        L_0x0012:
            com.google.android.gms.internal.zzuj r0 = r3.f5441     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            com.google.android.gms.internal.zzuj r0 = r3.f5441     // Catch:{ all -> 0x001d }
            r0.zzcn()     // Catch:{ all -> 0x001d }
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            goto L_0x0011
        L_0x001d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzuk.m5892(com.google.android.gms.internal.zzvg):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5893(String str, String str2) {
        synchronized (this.f5442) {
            if (this.f5441 != null) {
                this.f5441.zzc(str, str2);
            }
        }
    }
}
