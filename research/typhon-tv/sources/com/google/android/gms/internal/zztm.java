package com.google.android.gms.internal;

import android.os.Parcel;
import android.util.Base64;
import com.google.android.gms.ads.internal.zzbs;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@zzzv
final class zztm {

    /* renamed from: 靐  reason: contains not printable characters */
    final String f5382;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f5383;

    /* renamed from: 龘  reason: contains not printable characters */
    final zzjj f5384;

    private zztm(zzjj zzjj, String str, int i) {
        this.f5384 = zzjj;
        this.f5382 = str;
        this.f5383 = i;
    }

    zztm(zzti zzti) {
        this(zzti.m5848(), zzti.m5847(), zzti.m5845());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zztm m5864(String str) throws IOException {
        String[] split = str.split("\u0000");
        if (split.length != 3) {
            throw new IOException("Incorrect field count for QueueSeed.");
        }
        Parcel obtain = Parcel.obtain();
        try {
            String str2 = new String(Base64.decode(split[0], 0), "UTF-8");
            int parseInt = Integer.parseInt(split[1]);
            byte[] decode = Base64.decode(split[2], 0);
            obtain.unmarshall(decode, 0, decode.length);
            obtain.setDataPosition(0);
            zztm zztm = new zztm(zzjj.CREATOR.createFromParcel(obtain), str2, parseInt);
            obtain.recycle();
            return zztm;
        } catch (zzbfo | IllegalArgumentException | IllegalStateException e) {
            zzbs.zzem().m4505(e, "QueueSeed.decode");
            throw new IOException("Malformed QueueSeed encoding.", e);
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5865() {
        Parcel obtain = Parcel.obtain();
        try {
            String encodeToString = Base64.encodeToString(this.f5382.getBytes("UTF-8"), 0);
            String num = Integer.toString(this.f5383);
            this.f5384.writeToParcel(obtain, 0);
            String encodeToString2 = Base64.encodeToString(obtain.marshall(), 0);
            String sb = new StringBuilder(String.valueOf(encodeToString).length() + 2 + String.valueOf(num).length() + String.valueOf(encodeToString2).length()).append(encodeToString).append("\u0000").append(num).append("\u0000").append(encodeToString2).toString();
            obtain.recycle();
            return sb;
        } catch (UnsupportedEncodingException e) {
            zzagf.m4795("QueueSeed encode failed because UTF-8 is not available.");
            obtain.recycle();
            return "";
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }
}
