package com.google.android.gms.internal;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public final class zzed extends zzet {
    public zzed(zzdm zzdm, String str, String str2, zzaz zzaz, int i, int i2) {
        super(zzdm, str, str2, zzaz, i, 24);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m12276() {
        AdvertisingIdClient r0 = this.f10287.m11612();
        if (r0 != null) {
            try {
                AdvertisingIdClient.Info info = r0.getInfo();
                String r1 = zzdr.m11764(info.getId());
                if (r1 != null) {
                    synchronized (this.f10284) {
                        this.f10284.f8446 = r1;
                        this.f10284.f8448 = Boolean.valueOf(info.isLimitAdTrackingEnabled());
                        this.f10284.f8444 = 5;
                    }
                }
            } catch (IOException e) {
            }
        }
    }

    public final /* synthetic */ Object call() throws Exception {
        return call();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Void m12277() throws Exception {
        if (this.f10287.m11620()) {
            return super.call();
        }
        if (this.f10287.m11610()) {
            m12276();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12278() throws IllegalAccessException, InvocationTargetException {
        if (this.f10287.m11610()) {
            m12276();
            return;
        }
        synchronized (this.f10284) {
            this.f10284.f8446 = (String) this.f10286.invoke((Object) null, new Object[]{this.f10287.m11623()});
        }
    }
}
