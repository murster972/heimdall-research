package com.google.android.gms.internal;

public final class zzcho {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f9277;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzchm f9278;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f9279;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f9280;

    zzcho(zzchm zzchm, int i, boolean z, boolean z2) {
        this.f9278 = zzchm;
        this.f9280 = i;
        this.f9277 = z;
        this.f9279 = z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10849(String str) {
        this.f9278.m10843(this.f9280, this.f9277, this.f9279, str, (Object) null, (Object) null, (Object) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10850(String str, Object obj) {
        this.f9278.m10843(this.f9280, this.f9277, this.f9279, str, obj, (Object) null, (Object) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10851(String str, Object obj, Object obj2) {
        this.f9278.m10843(this.f9280, this.f9277, this.f9279, str, obj, obj2, (Object) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10852(String str, Object obj, Object obj2, Object obj3) {
        this.f9278.m10843(this.f9280, this.f9277, this.f9279, str, obj, obj2, obj3);
    }
}
