package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import io.presage.ads.NewAd;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzui {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final List<String> f5421;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean f5422;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String f5423;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final long f5424;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final boolean f5425;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int f5426;

    /* renamed from: ˊ  reason: contains not printable characters */
    public int f5427;

    /* renamed from: ˋ  reason: contains not printable characters */
    public int f5428;

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f5429;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final long f5430;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final String f5431;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int f5432;

    /* renamed from: 连任  reason: contains not printable characters */
    public final List<String> f5433;

    /* renamed from: 靐  reason: contains not printable characters */
    public final long f5434;

    /* renamed from: 麤  reason: contains not printable characters */
    public final List<String> f5435;

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<String> f5436;

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<zzuh> f5437;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final boolean f5438;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean f5439;

    public zzui(String str) throws JSONException {
        this(new JSONObject(str));
    }

    public zzui(List<zzuh> list, long j, List<String> list2, List<String> list3, List<String> list4, List<String> list5, boolean z, String str, long j2, int i, int i2, String str2, int i3, int i4, long j3, boolean z2) {
        this.f5437 = list;
        this.f5434 = j;
        this.f5436 = list2;
        this.f5435 = list3;
        this.f5433 = list4;
        this.f5421 = list5;
        this.f5422 = z;
        this.f5423 = str;
        this.f5430 = -1;
        this.f5427 = 0;
        this.f5428 = 1;
        this.f5431 = null;
        this.f5432 = 0;
        this.f5426 = -1;
        this.f5424 = -1;
        this.f5425 = false;
        this.f5438 = false;
        this.f5439 = false;
        this.f5429 = false;
    }

    public zzui(JSONObject jSONObject) throws JSONException {
        if (zzagf.m4798(2)) {
            String valueOf = String.valueOf(jSONObject.toString(2));
            zzagf.m4527(valueOf.length() != 0 ? "Mediation Response JSON: ".concat(valueOf) : new String("Mediation Response JSON: "));
        }
        JSONArray jSONArray = jSONObject.getJSONArray("ad_networks");
        ArrayList arrayList = new ArrayList(jSONArray.length());
        int i = -1;
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            zzuh zzuh = new zzuh(jSONArray.getJSONObject(i2));
            if (zzuh.m5879()) {
                this.f5429 = true;
            }
            arrayList.add(zzuh);
            if (i < 0 && m5880(zzuh)) {
                i = i2;
            }
        }
        this.f5427 = i;
        this.f5428 = jSONArray.length();
        this.f5437 = Collections.unmodifiableList(arrayList);
        this.f5423 = jSONObject.optString("qdata");
        this.f5426 = jSONObject.optInt("fs_model_type", -1);
        this.f5424 = jSONObject.optLong("timeout_ms", -1);
        JSONObject optJSONObject = jSONObject.optJSONObject("settings");
        if (optJSONObject != null) {
            this.f5434 = optJSONObject.optLong("ad_network_timeout_millis", -1);
            zzbs.zzez();
            this.f5436 = zzuq.m5895(optJSONObject, "click_urls");
            zzbs.zzez();
            this.f5435 = zzuq.m5895(optJSONObject, "imp_urls");
            zzbs.zzez();
            this.f5433 = zzuq.m5895(optJSONObject, "nofill_urls");
            zzbs.zzez();
            this.f5421 = zzuq.m5895(optJSONObject, "remote_ping_urls");
            this.f5422 = optJSONObject.optBoolean("render_in_browser", false);
            long optLong = optJSONObject.optLong("refresh", -1);
            this.f5430 = optLong > 0 ? optLong * 1000 : -1;
            zzaeq r2 = zzaeq.m4388(optJSONObject.optJSONArray(NewAd.EVENT_REWARD));
            if (r2 == null) {
                this.f5431 = null;
                this.f5432 = 0;
            } else {
                this.f5431 = r2.f4056;
                this.f5432 = r2.f4055;
            }
            this.f5425 = optJSONObject.optBoolean("use_displayed_impression", false);
            this.f5438 = optJSONObject.optBoolean("allow_pub_rendered_attribution", false);
            this.f5439 = optJSONObject.optBoolean("allow_pub_owned_ad_view", false);
            return;
        }
        this.f5434 = -1;
        this.f5436 = null;
        this.f5435 = null;
        this.f5433 = null;
        this.f5421 = null;
        this.f5430 = -1;
        this.f5431 = null;
        this.f5432 = 0;
        this.f5425 = false;
        this.f5422 = false;
        this.f5438 = false;
        this.f5439 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m5880(zzuh zzuh) {
        for (String equals : zzuh.f5417) {
            if (equals.equals("com.google.ads.mediation.admob.AdMobAdapter")) {
                return true;
            }
        }
        return false;
    }
}
