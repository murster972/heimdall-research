package com.google.android.gms.internal;

import android.content.SharedPreferences;
import org.json.JSONObject;

final class zznb extends zzmx<Float> {
    zznb(int i, String str, Float f) {
        super(i, str, f, (zzmy) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13157(SharedPreferences sharedPreferences) {
        return Float.valueOf(sharedPreferences.getFloat(m5586(), ((Float) m5582()).floatValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13158(JSONObject jSONObject) {
        return Float.valueOf((float) jSONObject.optDouble(m5586(), (double) ((Float) m5582()).floatValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m13159(SharedPreferences.Editor editor, Object obj) {
        editor.putFloat(m5586(), ((Float) obj).floatValue());
    }
}
