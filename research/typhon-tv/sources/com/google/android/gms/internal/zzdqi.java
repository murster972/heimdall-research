package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;

final class zzdqi implements zzdpw<zzdvf> {
    zzdqi() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdtx m11696(zzfes zzfes) throws GeneralSecurityException {
        try {
            zzdrg r1 = zzdrg.m11798(zzfes);
            if (!(r1 instanceof zzdrg)) {
                throw new GeneralSecurityException("expected AesCtrKey proto");
            }
            zzdrg zzdrg = r1;
            zzdvk.m12238(zzdrg.m11807(), 0);
            zzdvk.m12237(zzdrg.m11806().size());
            m11692(zzdrg.m11804());
            return new zzdtx(zzdrg.m11806().toByteArray(), zzdrg.m11804().m11824());
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesCtrKey proto", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m11692(zzdrk zzdrk) throws GeneralSecurityException {
        if (zzdrk.m11824() < 12 || zzdrk.m11824() > 16) {
            throw new GeneralSecurityException("invalid IV size");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11693(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11694((zzfhe) zzdri.m11815(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesCtrKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11694(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdri)) {
            throw new GeneralSecurityException("expected AesCtrKeyFormat proto");
        }
        zzdri zzdri = (zzdri) zzfhe;
        zzdvk.m12237(zzdri.m11816());
        m11692(zzdri.m11818());
        return zzdrg.m11796().m11811(zzdri.m11818()).m11812(zzfes.zzaz(zzdvi.m12235(zzdri.m11816()))).m11810(0).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11695(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.AesCtrKey").m12022(((zzdrg) m11693(zzfes)).m12361()).m12021(zzdsy.zzb.SYMMETRIC).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11697(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdrg)) {
            throw new GeneralSecurityException("expected AesCtrKey proto");
        }
        zzdrg zzdrg = (zzdrg) zzfhe;
        zzdvk.m12238(zzdrg.m11807(), 0);
        zzdvk.m12237(zzdrg.m11806().size());
        m11692(zzdrg.m11804());
        return new zzdtx(zzdrg.m11806().toByteArray(), zzdrg.m11804().m11824());
    }
}
