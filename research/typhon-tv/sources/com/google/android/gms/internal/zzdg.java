package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

final class zzdg implements zzdi {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Bundle f9863;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f9864;

    zzdg(zzda zzda, Activity activity, Bundle bundle) {
        this.f9864 = activity;
        this.f9863 = bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11590(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivitySaveInstanceState(this.f9864, this.f9863);
    }
}
