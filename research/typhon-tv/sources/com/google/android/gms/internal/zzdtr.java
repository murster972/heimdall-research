package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdtd;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdtr extends zzffu<zzdtr, zza> implements zzfhg {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final zzdtr f10157;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile zzfhk<zzdtr> f10158;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzdtd f10159;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f10160 = "";

    public static final class zza extends zzffu.zza<zzdtr, zza> implements zzfhg {
        private zza() {
            super(zzdtr.f10157);
        }

        /* synthetic */ zza(zzdts zzdts) {
            this();
        }
    }

    static {
        zzdtr zzdtr = new zzdtr();
        f10157 = zzdtr;
        zzdtr.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdtr.f10394.m12718();
    }

    private zzdtr() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static zzdtr m12148() {
        return f10157;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdtr m12149(zzfes zzfes) throws zzfge {
        return (zzdtr) zzffu.m12526(f10157, zzfes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzdtd m12150() {
        return this.f10159 == null ? zzdtd.m12033() : this.f10159;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12151() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.f10160.isEmpty()) {
            i2 = zzffg.m5248(1, this.f10160) + 0;
        }
        if (this.f10159 != null) {
            i2 += zzffg.m5262(2, (zzfhe) this.f10159 == null ? zzdtd.m12033() : this.f10159);
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m12152(int i, Object obj, Object obj2) {
        zzdtd.zza zza2;
        boolean z = true;
        switch (zzdts.f10161[i - 1]) {
            case 1:
                return new zzdtr();
            case 2:
                return f10157;
            case 3:
                return null;
            case 4:
                return new zza((zzdts) null);
            case 5:
                zzffu.zzh zzh = (zzffu.zzh) obj;
                zzdtr zzdtr = (zzdtr) obj2;
                boolean z2 = !this.f10160.isEmpty();
                String str = this.f10160;
                if (zzdtr.f10160.isEmpty()) {
                    z = false;
                }
                this.f10160 = zzh.m12574(z2, str, z, zzdtr.f10160);
                this.f10159 = (zzdtd) zzh.m12572(this.f10159, zzdtr.f10159);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z3 = false;
                    while (!z3) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z3 = true;
                                    break;
                                case 10:
                                    this.f10160 = zzffb.m12400();
                                    break;
                                case 18:
                                    if (this.f10159 != null) {
                                        zzdtd zzdtd = this.f10159;
                                        zzffu.zza zza3 = (zzffu.zza) zzdtd.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdtd);
                                        zza2 = (zzdtd.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10159 = (zzdtd) zzffb.m12416(zzdtd.m12033(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10159);
                                        this.f10159 = (zzdtd) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z3 = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10158 == null) {
                    synchronized (zzdtr.class) {
                        if (f10158 == null) {
                            f10158 = new zzffu.zzb(f10157);
                        }
                    }
                }
                return f10158;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10157;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m12153() {
        return this.f10160;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12154(zzffg zzffg) throws IOException {
        if (!this.f10160.isEmpty()) {
            zzffg.m5290(1, this.f10160);
        }
        if (this.f10159 != null) {
            zzffg.m5289(2, (zzfhe) this.f10159 == null ? zzdtd.m12033() : this.f10159);
        }
        this.f10394.m12719(zzffg);
    }
}
