package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzadk extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    String m9469() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    void m9470() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m9471(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m9472() throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m9473() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m9474() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m9475(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9476() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9477(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9478(zzadp zzadp) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9479(zzadv zzadv) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9480(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9481(boolean z) throws RemoteException;
}
