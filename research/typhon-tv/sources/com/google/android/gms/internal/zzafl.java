package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

@zzzv
public final class zzafl implements zzafn {
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<AdvertisingIdClient.Info> m4439(Context context) {
        zzalf zzalf = new zzalf();
        zzkb.m5487();
        if (zzajr.m4744(context)) {
            zzahh.m4555((Runnable) new zzafm(this, context, zzalf));
        }
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<String> m4440(String str, PackageInfo packageInfo) {
        return zzakl.m4802(str);
    }
}
