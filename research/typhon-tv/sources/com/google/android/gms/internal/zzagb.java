package com.google.android.gms.internal;

@zzzv
public abstract class zzagb implements zzaif<zzakv> {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public volatile Thread f4207;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4208 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Runnable f4209 = new zzagc(this);

    public zzagb() {
    }

    public zzagb(boolean z) {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzakv m4521() {
        return this.f4208 ? zzahh.m4554(1, this.f4209) : zzahh.m4555(this.f4209);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m4522();

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* synthetic */ Object m4523() {
        return this.f4208 ? zzahh.m4554(1, this.f4209) : zzahh.m4555(this.f4209);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4524() {
        m4522();
        if (this.f4207 != null) {
            this.f4207.interrupt();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m4525();
}
