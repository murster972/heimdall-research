package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzrb extends zzeu implements zzqz {
    zzrb(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnCustomTemplateAdLoadedListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13375(zzqm zzqm) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzqm);
        m12298(1, v_);
    }
}
