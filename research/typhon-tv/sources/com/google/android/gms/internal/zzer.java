package com.google.android.gms.internal;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class zzer {

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f10271 = zzer.class.getSimpleName();

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile Method f10272 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Class<?>[] f10273;

    /* renamed from: ʽ  reason: contains not printable characters */
    private CountDownLatch f10274 = new CountDownLatch(1);

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f10275 = 2;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzdm f10276;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f10277;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f10278;

    public zzer(zzdm zzdm, String str, String str2, Class<?>... clsArr) {
        this.f10276 = zzdm;
        this.f10278 = str;
        this.f10277 = str2;
        this.f10273 = clsArr;
        this.f10276.m11622().submit(new zzes(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m12292() {
        try {
            Class loadClass = this.f10276.m11621().loadClass(m12293(this.f10276.m11609(), this.f10278));
            if (loadClass != null) {
                this.f10272 = loadClass.getMethod(m12293(this.f10276.m11609(), this.f10277), this.f10273);
                if (this.f10272 == null) {
                    this.f10274.countDown();
                } else {
                    this.f10274.countDown();
                }
            }
        } catch (zzcy e) {
        } catch (UnsupportedEncodingException e2) {
        } catch (ClassNotFoundException e3) {
        } catch (NoSuchMethodException e4) {
        } catch (NullPointerException e5) {
        } finally {
            this.f10274.countDown();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final String m12293(byte[] bArr, String str) throws zzcy, UnsupportedEncodingException {
        return new String(this.f10276.m11618().m11545(bArr, str), "UTF-8");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Method m12295() {
        if (this.f10272 != null) {
            return this.f10272;
        }
        try {
            if (this.f10274.await(2, TimeUnit.SECONDS)) {
                return this.f10272;
            }
            return null;
        } catch (InterruptedException e) {
            return null;
        }
    }
}
