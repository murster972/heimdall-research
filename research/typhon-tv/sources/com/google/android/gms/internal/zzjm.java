package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@zzzv
public final class zzjm {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzjm f4788 = new zzjm();

    protected zzjm() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzjj m5452(Context context, zzlt zzlt) {
        Date r2 = zzlt.m5507();
        long time = r2 != null ? r2.getTime() : -1;
        String r15 = zzlt.m5502();
        int r7 = zzlt.m5504();
        Set<String> r22 = zzlt.m5503();
        List unmodifiableList = !r22.isEmpty() ? Collections.unmodifiableList(new ArrayList(r22)) : null;
        boolean r9 = zzlt.m5508(context);
        int r10 = zzlt.m5496();
        Location r14 = zzlt.m5500();
        Bundle r6 = zzlt.m5501(AdMobAdapter.class);
        boolean r11 = zzlt.m5491();
        String r12 = zzlt.m5492();
        SearchAdRequest r23 = zzlt.m5497();
        zzmn zzmn = r23 != null ? new zzmn(r23) : null;
        String str = null;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            String packageName = applicationContext.getPackageName();
            zzkb.m5487();
            str = zzajr.m4761(Thread.currentThread().getStackTrace(), packageName);
        }
        return new zzjj(7, time, r6, r7, unmodifiableList, r9, r10, r11, r12, zzmn, r14, r15, zzlt.m5499(), zzlt.m5494(), Collections.unmodifiableList(new ArrayList(zzlt.m5495())), zzlt.m5493(), str, zzlt.m5509());
    }
}
