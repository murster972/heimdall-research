package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzqs extends zzeu implements zzqq {
    zzqs(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnAppInstallAdLoadedListener");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13343(zzqe zzqe) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzqe);
        m12298(1, v_);
    }
}
