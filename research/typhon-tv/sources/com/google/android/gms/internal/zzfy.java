package com.google.android.gms.internal;

import android.view.View;

public final class zzfy implements zzhd {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzafo f10638;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f10639;

    public zzfy(View view, zzafo zzafo) {
        this.f10639 = view;
        this.f10638 = zzafo;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m12957() {
        return this.f10638 == null || this.f10639 == null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzhd m12958() {
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m12959() {
        return this.f10639;
    }
}
