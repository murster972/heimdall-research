package com.google.android.gms.internal;

public final class zzdqb {
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0012 A[Catch:{ zzfge -> 0x0045 }] */
    @java.lang.Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.google.android.gms.internal.zzdpx m5231(byte[] r5) throws java.security.GeneralSecurityException {
        /*
            com.google.android.gms.internal.zzdth r1 = com.google.android.gms.internal.zzdth.m12066((byte[]) r5)     // Catch:{ zzfge -> 0x0045 }
            java.util.List r0 = r1.m12067()     // Catch:{ zzfge -> 0x0045 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ zzfge -> 0x0045 }
        L_0x000c:
            boolean r0 = r2.hasNext()     // Catch:{ zzfge -> 0x0045 }
            if (r0 == 0) goto L_0x004f
            java.lang.Object r0 = r2.next()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdth$zzb r0 = (com.google.android.gms.internal.zzdth.zzb) r0     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy r3 = r0.m12077()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy$zzb r3 = r3.m12017()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy$zzb r4 = com.google.android.gms.internal.zzdsy.zzb.UNKNOWN_KEYMATERIAL     // Catch:{ zzfge -> 0x0045 }
            if (r3 == r4) goto L_0x003c
            com.google.android.gms.internal.zzdsy r3 = r0.m12077()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy$zzb r3 = r3.m12017()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy$zzb r4 = com.google.android.gms.internal.zzdsy.zzb.SYMMETRIC     // Catch:{ zzfge -> 0x0045 }
            if (r3 == r4) goto L_0x003c
            com.google.android.gms.internal.zzdsy r0 = r0.m12077()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy$zzb r0 = r0.m12017()     // Catch:{ zzfge -> 0x0045 }
            com.google.android.gms.internal.zzdsy$zzb r3 = com.google.android.gms.internal.zzdsy.zzb.ASYMMETRIC_PRIVATE     // Catch:{ zzfge -> 0x0045 }
            if (r0 != r3) goto L_0x000c
        L_0x003c:
            java.security.GeneralSecurityException r0 = new java.security.GeneralSecurityException     // Catch:{ zzfge -> 0x0045 }
            java.lang.String r1 = "keyset contains secret key material"
            r0.<init>(r1)     // Catch:{ zzfge -> 0x0045 }
            throw r0     // Catch:{ zzfge -> 0x0045 }
        L_0x0045:
            r0 = move-exception
            java.security.GeneralSecurityException r0 = new java.security.GeneralSecurityException
            java.lang.String r1 = "invalid keyset"
            r0.<init>(r1)
            throw r0
        L_0x004f:
            com.google.android.gms.internal.zzdpx r0 = com.google.android.gms.internal.zzdpx.m11658(r1)     // Catch:{ zzfge -> 0x0045 }
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdqb.m5231(byte[]):com.google.android.gms.internal.zzdpx");
    }
}
