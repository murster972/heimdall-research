package com.google.android.gms.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.zzbq;

public final class zzchz {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzchx f9322;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f9323 = true;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f9324;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f9325;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f9326;

    public zzchz(zzchx zzchx, String str, boolean z) {
        this.f9322 = zzchx;
        zzbq.m9122(str);
        this.f9326 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10899(boolean z) {
        SharedPreferences.Editor edit = this.f9322.m10885().edit();
        edit.putBoolean(this.f9326, z);
        edit.apply();
        this.f9324 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10900() {
        if (!this.f9325) {
            this.f9325 = true;
            this.f9324 = this.f9322.m10885().getBoolean(this.f9326, this.f9323);
        }
        return this.f9324;
    }
}
