package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.gmsg.zzae;
import com.google.android.gms.ads.internal.gmsg.zzd;
import com.google.android.gms.ads.internal.js.zzaa;
import com.google.android.gms.ads.internal.js.zzaj;
import com.google.android.gms.ads.internal.js.zzn;
import com.google.android.gms.ads.internal.zzbs;
import org.json.JSONObject;

@zzzv
public final class zzua<I, O> implements zztp<I, O> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzts<I> f5398;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f5399;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzn f5400;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zztr<O> f5401;

    zzua(zzn zzn, String str, zzts<I> zzts, zztr<O> zztr) {
        this.f5400 = zzn;
        this.f5399 = str;
        this.f5398 = zzts;
        this.f5401 = zztr;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5873(zzaa zzaa, zzaj zzaj, I i, zzalf<O> zzalf) {
        try {
            zzbs.zzei();
            String r0 = zzahn.m4597();
            zzd.zzbxl.zza(r0, (zzae) new zzud(this, zzaa, zzalf));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", (Object) r0);
            jSONObject.put("args", (Object) this.f5398.m13433(i));
            zzaj.zzb(this.f5399, jSONObject);
        } catch (Exception e) {
            zzalf.m4824(e);
            zzagf.m4793("Unable to invokeJavaScript", e);
            zzaa.release();
        } catch (Throwable th) {
            zzaa.release();
            throw th;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzakv<O> m5875(I i) {
        zzalf zzalf = new zzalf();
        zzaa zzb = this.f5400.zzb((zzcv) null);
        zzb.zza(new zzub(this, zzb, i, zzalf), new zzuc(this, zzalf, zzb));
        return zzalf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakv<O> m5876(I i) throws Exception {
        return m5875(i);
    }
}
