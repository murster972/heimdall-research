package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdrw extends zzffu<zzdrw, zza> implements zzfhg {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile zzfhk<zzdrw> f10012;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final zzdrw f10013;

    public static final class zza extends zzffu.zza<zzdrw, zza> implements zzfhg {
        private zza() {
            super(zzdrw.f10013);
        }

        /* synthetic */ zza(zzdrx zzdrx) {
            this();
        }
    }

    static {
        zzdrw zzdrw = new zzdrw();
        f10013 = zzdrw;
        zzdrw.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdrw.f10394.m12718();
    }

    private zzdrw() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdrw m11883() {
        return f10013;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11884() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int r0 = this.f10394.m12716() + 0;
        this.f10395 = r0;
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11885(int i, Object obj, Object obj2) {
        switch (zzdrx.f10014[i - 1]) {
            case 1:
                return new zzdrw();
            case 2:
                return f10013;
            case 3:
                return null;
            case 4:
                return new zza((zzdrx) null);
            case 5:
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                if (((zzffm) obj2) != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int r2 = zzffb.m12415();
                            switch (r2) {
                                case 0:
                                    z = true;
                                    break;
                                default:
                                    if (m12537(r2, zzffb)) {
                                        break;
                                    } else {
                                        z = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10012 == null) {
                    synchronized (zzdrw.class) {
                        if (f10012 == null) {
                            f10012 = new zzffu.zzb(f10013);
                        }
                    }
                }
                return f10012;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10013;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11886(zzffg zzffg) throws IOException {
        this.f10394.m12719(zzffg);
    }
}
