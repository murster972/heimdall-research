package com.google.android.gms.internal;

import android.content.DialogInterface;
import android.net.Uri;
import com.google.android.gms.ads.internal.zzbs;

final class zzaio implements DialogInterface.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzain f8231;

    zzaio(zzain zzain) {
        this.f8231 = zzain;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        zzbs.zzei();
        zzahn.m4609(this.f8231.f8230, Uri.parse("https://support.google.com/dfp_premium/answer/7160685#push"));
    }
}
