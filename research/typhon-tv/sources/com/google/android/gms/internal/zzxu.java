package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.atomic.AtomicBoolean;

@zzzv
public abstract class zzxu implements zzaif<Void>, zzanm {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Runnable f5557;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Object f5558 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public AtomicBoolean f5559 = new AtomicBoolean(true);

    /* renamed from: 连任  reason: contains not printable characters */
    private zzafp f5560;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final zzanh f5561;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzyb f5562;

    /* renamed from: 齉  reason: contains not printable characters */
    protected zzaax f5563;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Context f5564;

    protected zzxu(Context context, zzafp zzafp, zzanh zzanh, zzyb zzyb) {
        this.f5564 = context;
        this.f5560 = zzafp;
        this.f5563 = this.f5560.f4133;
        this.f5561 = zzanh;
        this.f5562 = zzyb;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final /* synthetic */ Object m6017() {
        zzbq.m9115("Webview render task needs to be called on UI thread.");
        this.f5557 = new zzxv(this);
        zzahn.f4212.postDelayed(this.f5557, ((Long) zzkb.m5481().m5595(zznh.f4960)).longValue());
        m6019();
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m6018() {
        if (this.f5559.getAndSet(false)) {
            this.f5561.stopLoading();
            zzbs.zzek();
            zzaht.m4642(this.f5561);
            m6020(-1);
            zzahn.f4212.removeCallbacks(this.f5557);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m6019();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6020(int i) {
        if (i != -2) {
            this.f5563 = new zzaax(i, this.f5563.f3848);
        }
        this.f5561.m5016();
        zzyb zzyb = this.f5562;
        zzaat zzaat = this.f5560.f4136;
        zzyb zzyb2 = zzyb;
        zzyb2.zzb(new zzafo(zzaat.f3763, this.f5561, this.f5563.f3860, i, this.f5563.f3857, this.f5563.f3844, this.f5563.f3849, this.f5563.f3848, zzaat.f3740, this.f5563.f3822, (zzuh) null, (zzva) null, (String) null, (zzui) null, (zzuk) null, this.f5563.f3824, this.f5560.f4134, this.f5563.f3820, this.f5560.f4127, this.f5563.f3826, this.f5563.f3828, this.f5560.f4129, (zzou) null, this.f5563.f3854, this.f5563.f3855, this.f5563.f3856, this.f5563.f3862, this.f5563.f3865, (String) null, this.f5563.f3821, this.f5563.f3827, this.f5560.f4130, this.f5560.f4133.f3835, this.f5560.f4131));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6021(zzanh zzanh, boolean z) {
        int i = 0;
        zzagf.m4792("WebView finished loading.");
        if (this.f5559.getAndSet(false)) {
            if (z) {
                i = -2;
            }
            m6020(i);
            zzahn.f4212.removeCallbacks(this.f5557);
        }
    }
}
