package com.google.android.gms.internal;

final class zzcq implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcp f9776;

    zzcq(zzcp zzcp) {
        this.f9776 = zzcp;
    }

    public final void run() {
        if (this.f9776.f9774 == null) {
            synchronized (zzcp.f9772) {
                if (this.f9776.f9774 == null) {
                    boolean booleanValue = ((Boolean) zzkb.m5481().m5595(zznh.f4961)).booleanValue();
                    if (booleanValue) {
                        try {
                            zzcp.f9773 = new zzix(this.f9776.f9775.f9887, "ADSHIELD", (String) null);
                        } catch (Throwable th) {
                            booleanValue = false;
                        }
                    }
                    this.f9776.f9774 = Boolean.valueOf(booleanValue);
                    zzcp.f9772.open();
                }
            }
        }
    }
}
