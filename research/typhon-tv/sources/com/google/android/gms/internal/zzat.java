package com.google.android.gms.internal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class zzat extends ByteArrayOutputStream {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaj f8389;

    public zzat(zzaj zzaj, int i) {
        this.f8389 = zzaj;
        this.buf = this.f8389.m9648(Math.max(i, 256));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9737(int i) {
        if (this.count + i > this.buf.length) {
            byte[] r0 = this.f8389.m9648((this.count + i) << 1);
            System.arraycopy(this.buf, 0, r0, 0, this.count);
            this.f8389.m9647(this.buf);
            this.buf = r0;
        }
    }

    public final void close() throws IOException {
        this.f8389.m9647(this.buf);
        this.buf = null;
        super.close();
    }

    public final void finalize() {
        this.f8389.m9647(this.buf);
    }

    public final synchronized void write(int i) {
        m9737(1);
        super.write(i);
    }

    public final synchronized void write(byte[] bArr, int i, int i2) {
        m9737(i2);
        super.write(bArr, i, i2);
    }
}
