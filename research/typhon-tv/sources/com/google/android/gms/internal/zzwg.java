package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzwg implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10942;

    zzwg(zzvx zzvx) {
        this.f10942 = zzvx;
    }

    public final void run() {
        try {
            this.f10942.f5502.m13510();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClosed.", e);
        }
    }
}
