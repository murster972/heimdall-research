package com.google.android.gms.internal;

final class zzfgt implements zzfhd {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzfhd[] f10437;

    zzfgt(zzfhd... zzfhdArr) {
        this.f10437 = zzfhdArr;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhc m12609(Class<?> cls) {
        for (zzfhd zzfhd : this.f10437) {
            if (zzfhd.m12621(cls)) {
                return zzfhd.m12620(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12610(Class<?> cls) {
        for (zzfhd r4 : this.f10437) {
            if (r4.m12621(cls)) {
                return true;
            }
        }
        return false;
    }
}
