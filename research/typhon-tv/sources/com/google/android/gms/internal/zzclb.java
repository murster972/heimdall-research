package com.google.android.gms.internal;

import android.content.Intent;

final /* synthetic */ class zzclb implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f9621;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Intent f9622;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzchm f9623;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcla f9624;

    zzclb(zzcla zzcla, int i, zzchm zzchm, Intent intent) {
        this.f9624 = zzcla;
        this.f9621 = i;
        this.f9623 = zzchm;
        this.f9622 = intent;
    }

    public final void run() {
        this.f9624.m11293(this.f9621, this.f9623, this.f9622);
    }
}
