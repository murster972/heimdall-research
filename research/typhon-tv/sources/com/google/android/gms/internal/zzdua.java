package com.google.android.gms.internal;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.security.GeneralSecurityException;

public final class zzdua {
    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m12173(byte[]... bArr) throws GeneralSecurityException {
        int i = 0;
        for (byte[] bArr2 : bArr) {
            if (i > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - bArr2.length) {
                throw new GeneralSecurityException("exceeded size limit");
            }
            i += bArr2.length;
        }
        byte[] bArr3 = new byte[i];
        int i2 = 0;
        for (byte[] bArr4 : bArr) {
            System.arraycopy(bArr4, 0, bArr3, i2, bArr4.length);
            i2 += bArr4.length;
        }
        return bArr3;
    }
}
