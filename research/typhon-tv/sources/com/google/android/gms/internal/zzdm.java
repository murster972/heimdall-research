package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.zzf;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzdm {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f9871 = zzdm.class.getSimpleName();

    /* renamed from: ʻ  reason: contains not printable characters */
    private byte[] f9872;

    /* renamed from: ʼ  reason: contains not printable characters */
    private volatile AdvertisingIdClient f9873 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private volatile boolean f9874 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    private zzcp f9875;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f9876 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Future f9877 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f9878 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean f9879 = true;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f9880 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Future f9881 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f9882;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public volatile zzaz f9883 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzcx f9884;

    /* renamed from: 麤  reason: contains not printable characters */
    private DexClassLoader f9885;

    /* renamed from: 齉  reason: contains not printable characters */
    private ExecutorService f9886;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Context f9887;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f9888 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Map<Pair<String, String>, zzer> f9889;

    final class zza extends BroadcastReceiver {
        private zza() {
        }

        /* synthetic */ zza(zzdm zzdm, zzdn zzdn) {
            this();
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                boolean unused = zzdm.this.f9879 = true;
            } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                boolean unused2 = zzdm.this.f9879 = false;
            }
        }
    }

    private zzdm(Context context) {
        boolean z = true;
        Context applicationContext = context.getApplicationContext();
        this.f9882 = applicationContext == null ? false : z;
        this.f9887 = this.f9882 ? applicationContext : context;
        this.f9889 = new HashMap();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m11597(int i, zzaz zzaz) {
        if (i < 4) {
            if (zzaz == null) {
                return true;
            }
            if (((Boolean) zzkb.m5481().m5595(zznh.f4977)).booleanValue() && (zzaz.f8431 == null || zzaz.f8431.equals("0000000000000000000000000000000000000000000000000000000000000000"))) {
                return true;
            }
            if (((Boolean) zzkb.m5481().m5595(zznh.f4978)).booleanValue() && (zzaz.f8450 == null || zzaz.f8450.f8725 == null || zzaz.f8450.f8725.longValue() == -2)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00cc A[SYNTHETIC, Splitter:B:42:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00d1 A[SYNTHETIC, Splitter:B:45:0x00d1] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00dd A[SYNTHETIC, Splitter:B:51:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e2 A[SYNTHETIC, Splitter:B:54:0x00e2] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m11598(java.io.File r13, java.lang.String r14) {
        /*
            r12 = this;
            r3 = 0
            r7 = 2
            r1 = 1
            r2 = 0
            java.io.File r5 = new java.io.File
            java.lang.String r0 = "%s/%s.tmp"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            r4[r2] = r13
            r4[r1] = r14
            java.lang.String r0 = java.lang.String.format(r0, r4)
            r5.<init>(r0)
            boolean r0 = r5.exists()
            if (r0 != 0) goto L_0x001e
            r0 = r2
        L_0x001d:
            return r0
        L_0x001e:
            java.io.File r6 = new java.io.File
            java.lang.String r0 = "%s/%s.dex"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            r4[r2] = r13
            r4[r1] = r14
            java.lang.String r0 = java.lang.String.format(r0, r4)
            r6.<init>(r0)
            boolean r0 = r6.exists()
            if (r0 == 0) goto L_0x0038
            r0 = r2
            goto L_0x001d
        L_0x0038:
            long r8 = r5.length()     // Catch:{ IOException -> 0x0110, NoSuchAlgorithmException -> 0x0105, zzcy -> 0x00c7, all -> 0x00d7 }
            r10 = 0
            int r0 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r0 > 0) goto L_0x0047
            m11602((java.io.File) r5)     // Catch:{ IOException -> 0x0110, NoSuchAlgorithmException -> 0x0105, zzcy -> 0x00c7, all -> 0x00d7 }
            r0 = r2
            goto L_0x001d
        L_0x0047:
            int r0 = (int) r8     // Catch:{ IOException -> 0x0110, NoSuchAlgorithmException -> 0x0105, zzcy -> 0x00c7, all -> 0x00d7 }
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0110, NoSuchAlgorithmException -> 0x0105, zzcy -> 0x00c7, all -> 0x00d7 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0110, NoSuchAlgorithmException -> 0x0105, zzcy -> 0x00c7, all -> 0x00d7 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0110, NoSuchAlgorithmException -> 0x0105, zzcy -> 0x00c7, all -> 0x00d7 }
            int r7 = r4.read(r0)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            if (r7 > 0) goto L_0x0065
            java.lang.String r0 = f9871     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            java.lang.String r1 = "Cannot read the cache data."
            android.util.Log.d(r0, r1)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            m11602((java.io.File) r5)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r4.close()     // Catch:{ IOException -> 0x00e6 }
        L_0x0063:
            r0 = r2
            goto L_0x001d
        L_0x0065:
            com.google.android.gms.internal.zzbd r7 = new com.google.android.gms.internal.zzbd     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r7.<init>()     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            com.google.android.gms.internal.zzfjs r0 = com.google.android.gms.internal.zzfjs.m12869(r7, r0)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            com.google.android.gms.internal.zzbd r0 = (com.google.android.gms.internal.zzbd) r0     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            java.lang.String r7 = new java.lang.String     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r8 = r0.f8711     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            boolean r7 = r14.equals(r7)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            if (r7 == 0) goto L_0x0099
            byte[] r7 = r0.f8709     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r8 = r0.f8712     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r8 = com.google.android.gms.internal.zzbw.m10305((byte[]) r8)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            boolean r7 = java.util.Arrays.equals(r7, r8)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            if (r7 == 0) goto L_0x0099
            byte[] r7 = r0.f8710     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            java.lang.String r8 = android.os.Build.VERSION.SDK     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r8 = r8.getBytes()     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            boolean r7 = java.util.Arrays.equals(r7, r8)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            if (r7 != 0) goto L_0x00a2
        L_0x0099:
            m11602((java.io.File) r5)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r4.close()     // Catch:{ IOException -> 0x00e9 }
        L_0x009f:
            r0 = r2
            goto L_0x001d
        L_0x00a2:
            com.google.android.gms.internal.zzcx r5 = r12.f9884     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r7 = r12.f9872     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            java.lang.String r8 = new java.lang.String     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r0 = r0.f8712     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r8.<init>(r0)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            byte[] r5 = r5.m11545((byte[]) r7, (java.lang.String) r8)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r6.createNewFile()     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r0.<init>(r6)     // Catch:{ IOException -> 0x0114, NoSuchAlgorithmException -> 0x0109, zzcy -> 0x00fe, all -> 0x00f7 }
            r3 = 0
            int r6 = r5.length     // Catch:{ IOException -> 0x0118, NoSuchAlgorithmException -> 0x010d, zzcy -> 0x0102, all -> 0x00fb }
            r0.write(r5, r3, r6)     // Catch:{ IOException -> 0x0118, NoSuchAlgorithmException -> 0x010d, zzcy -> 0x0102, all -> 0x00fb }
            r4.close()     // Catch:{ IOException -> 0x00eb }
        L_0x00c1:
            r0.close()     // Catch:{ IOException -> 0x00ed }
        L_0x00c4:
            r0 = r1
            goto L_0x001d
        L_0x00c7:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x00ca:
            if (r1 == 0) goto L_0x00cf
            r1.close()     // Catch:{ IOException -> 0x00ef }
        L_0x00cf:
            if (r0 == 0) goto L_0x00d4
            r0.close()     // Catch:{ IOException -> 0x00f1 }
        L_0x00d4:
            r0 = r2
            goto L_0x001d
        L_0x00d7:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r3
        L_0x00db:
            if (r4 == 0) goto L_0x00e0
            r4.close()     // Catch:{ IOException -> 0x00f3 }
        L_0x00e0:
            if (r2 == 0) goto L_0x00e5
            r2.close()     // Catch:{ IOException -> 0x00f5 }
        L_0x00e5:
            throw r1
        L_0x00e6:
            r0 = move-exception
            goto L_0x0063
        L_0x00e9:
            r0 = move-exception
            goto L_0x009f
        L_0x00eb:
            r2 = move-exception
            goto L_0x00c1
        L_0x00ed:
            r0 = move-exception
            goto L_0x00c4
        L_0x00ef:
            r1 = move-exception
            goto L_0x00cf
        L_0x00f1:
            r0 = move-exception
            goto L_0x00d4
        L_0x00f3:
            r0 = move-exception
            goto L_0x00e0
        L_0x00f5:
            r0 = move-exception
            goto L_0x00e5
        L_0x00f7:
            r0 = move-exception
            r1 = r0
            r2 = r3
            goto L_0x00db
        L_0x00fb:
            r1 = move-exception
            r2 = r0
            goto L_0x00db
        L_0x00fe:
            r0 = move-exception
            r0 = r3
            r1 = r4
            goto L_0x00ca
        L_0x0102:
            r1 = move-exception
            r1 = r4
            goto L_0x00ca
        L_0x0105:
            r0 = move-exception
            r0 = r3
            r1 = r3
            goto L_0x00ca
        L_0x0109:
            r0 = move-exception
            r0 = r3
            r1 = r4
            goto L_0x00ca
        L_0x010d:
            r1 = move-exception
            r1 = r4
            goto L_0x00ca
        L_0x0110:
            r0 = move-exception
            r0 = r3
            r1 = r3
            goto L_0x00ca
        L_0x0114:
            r0 = move-exception
            r0 = r3
            r1 = r4
            goto L_0x00ca
        L_0x0118:
            r1 = move-exception
            r1 = r4
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdm.m11598(java.io.File, java.lang.String):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdm m11600(Context context, String str, String str2, boolean z) {
        File file;
        File file2;
        boolean z2 = true;
        zzdm zzdm = new zzdm(context);
        try {
            zzdm.f9886 = Executors.newCachedThreadPool();
            zzdm.f9874 = z;
            if (z) {
                zzdm.f9881 = zzdm.f9886.submit(new zzdn(zzdm));
            }
            zzdm.f9886.execute(new zzdp(zzdm));
            try {
                zzf r4 = zzf.m4219();
                zzdm.f9876 = zzf.m4218(zzdm.f9887) > 0;
                if (r4.m4227(zzdm.f9887) != 0) {
                    z2 = false;
                }
                zzdm.f9888 = z2;
            } catch (Throwable th) {
            }
            zzdm.m11625(0, true);
            if (zzdr.m11759()) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f4973)).booleanValue()) {
                    throw new IllegalStateException("Task Context initialization must not be called from the UI thread.");
                }
            }
            zzdm.f9884 = new zzcx((SecureRandom) null);
            zzdm.f9872 = zzdm.f9884.m11544(str);
            File cacheDir = zzdm.f9887.getCacheDir();
            if (cacheDir == null && (cacheDir = zzdm.f9887.getDir("dex", 0)) == null) {
                throw new zzdj();
            }
            file = cacheDir;
            file2 = new File(String.format("%s/%s.jar", new Object[]{file, "1505450608132"}));
            if (!file2.exists()) {
                byte[] r0 = zzdm.f9884.m11545(zzdm.f9872, str2);
                file2.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                fileOutputStream.write(r0, 0, r0.length);
                fileOutputStream.close();
            }
            zzdm.m11598(file, "1505450608132");
            zzdm.f9885 = new DexClassLoader(file2.getAbsolutePath(), file.getAbsolutePath(), (String) null, zzdm.f9887.getClassLoader());
            m11602(file2);
            zzdm.m11603(file, "1505450608132");
            m11604(String.format("%s/%s.dex", new Object[]{file, "1505450608132"}));
            if (((Boolean) zzkb.m5481().m5595(zznh.f4964)).booleanValue() && !zzdm.f9880) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.USER_PRESENT");
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                zzdm.f9887.registerReceiver(new zza(zzdm, (zzdn) null), intentFilter);
                zzdm.f9880 = true;
            }
            zzdm.f9875 = new zzcp(zzdm);
            zzdm.f9878 = true;
            return zzdm;
        } catch (zzcy e) {
            throw new zzdj(e);
        } catch (FileNotFoundException e2) {
            throw new zzdj(e2);
        } catch (IOException e3) {
            throw new zzdj(e3);
        } catch (zzcy e4) {
            throw new zzdj(e4);
        } catch (NullPointerException e5) {
            throw new zzdj(e5);
        } catch (zzdj e6) {
        } catch (Throwable th2) {
            m11602(file2);
            zzdm.m11603(file, "1505450608132");
            m11604(String.format("%s/%s.dex", new Object[]{file, "1505450608132"}));
            throw th2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m11602(File file) {
        if (!file.exists()) {
            Log.d(f9871, String.format("File %s not found. No need for deletion", new Object[]{file.getAbsolutePath()}));
            return;
        }
        file.delete();
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x009e A[SYNTHETIC, Splitter:B:27:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a3 A[SYNTHETIC, Splitter:B:30:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b1 A[SYNTHETIC, Splitter:B:36:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b6 A[SYNTHETIC, Splitter:B:39:0x00b6] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m11603(java.io.File r9, java.lang.String r10) {
        /*
            r8 = this;
            r2 = 0
            r7 = 2
            r6 = 1
            r4 = 0
            java.io.File r3 = new java.io.File
            java.lang.String r0 = "%s/%s.tmp"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            r1[r4] = r9
            r1[r6] = r10
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r3.<init>(r0)
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x001d
        L_0x001c:
            return
        L_0x001d:
            java.io.File r5 = new java.io.File
            java.lang.String r0 = "%s/%s.dex"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            r1[r4] = r9
            r1[r6] = r10
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r5.<init>(r0)
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x001c
            long r0 = r5.length()
            r6 = 0
            int r4 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x001c
            int r0 = (int) r0
            byte[] r0 = new byte[r0]
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00e1, NoSuchAlgorithmException -> 0x00d8, zzcy -> 0x0099, all -> 0x00ab }
            r1.<init>(r5)     // Catch:{ IOException -> 0x00e1, NoSuchAlgorithmException -> 0x00d8, zzcy -> 0x0099, all -> 0x00ab }
            int r4 = r1.read(r0)     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            if (r4 > 0) goto L_0x0054
            r1.close()     // Catch:{ IOException -> 0x00bd }
        L_0x0050:
            m11602((java.io.File) r5)
            goto L_0x001c
        L_0x0054:
            com.google.android.gms.internal.zzbd r4 = new com.google.android.gms.internal.zzbd     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r4.<init>()     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            java.lang.String r6 = android.os.Build.VERSION.SDK     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            byte[] r6 = r6.getBytes()     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r4.f8710 = r6     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            byte[] r6 = r10.getBytes()     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r4.f8711 = r6     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            com.google.android.gms.internal.zzcx r6 = r8.f9884     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            byte[] r7 = r8.f9872     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            java.lang.String r0 = r6.m11543((byte[]) r7, (byte[]) r0)     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r4.f8712 = r0     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            byte[] r0 = com.google.android.gms.internal.zzbw.m10305((byte[]) r0)     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r4.f8709 = r0     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r3.createNewFile()     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            r0.<init>(r3)     // Catch:{ IOException -> 0x00e5, NoSuchAlgorithmException -> 0x00dc, zzcy -> 0x00d3, all -> 0x00cb }
            byte[] r2 = com.google.android.gms.internal.zzfjs.m12871((com.google.android.gms.internal.zzfjs) r4)     // Catch:{ IOException -> 0x00e8, NoSuchAlgorithmException -> 0x00df, zzcy -> 0x00d6, all -> 0x00cf }
            r3 = 0
            int r4 = r2.length     // Catch:{ IOException -> 0x00e8, NoSuchAlgorithmException -> 0x00df, zzcy -> 0x00d6, all -> 0x00cf }
            r0.write(r2, r3, r4)     // Catch:{ IOException -> 0x00e8, NoSuchAlgorithmException -> 0x00df, zzcy -> 0x00d6, all -> 0x00cf }
            r0.close()     // Catch:{ IOException -> 0x00e8, NoSuchAlgorithmException -> 0x00df, zzcy -> 0x00d6, all -> 0x00cf }
            r1.close()     // Catch:{ IOException -> 0x00bf }
        L_0x0092:
            r0.close()     // Catch:{ IOException -> 0x00c1 }
        L_0x0095:
            m11602((java.io.File) r5)
            goto L_0x001c
        L_0x0099:
            r0 = move-exception
            r0 = r2
            r1 = r2
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x00a1:
            if (r0 == 0) goto L_0x00a6
            r0.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x00a6:
            m11602((java.io.File) r5)
            goto L_0x001c
        L_0x00ab:
            r0 = move-exception
            r3 = r0
            r4 = r2
            r1 = r2
        L_0x00af:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ IOException -> 0x00c7 }
        L_0x00b4:
            if (r4 == 0) goto L_0x00b9
            r4.close()     // Catch:{ IOException -> 0x00c9 }
        L_0x00b9:
            m11602((java.io.File) r5)
            throw r3
        L_0x00bd:
            r0 = move-exception
            goto L_0x0050
        L_0x00bf:
            r1 = move-exception
            goto L_0x0092
        L_0x00c1:
            r0 = move-exception
            goto L_0x0095
        L_0x00c3:
            r1 = move-exception
            goto L_0x00a1
        L_0x00c5:
            r0 = move-exception
            goto L_0x00a6
        L_0x00c7:
            r0 = move-exception
            goto L_0x00b4
        L_0x00c9:
            r0 = move-exception
            goto L_0x00b9
        L_0x00cb:
            r0 = move-exception
            r3 = r0
            r4 = r2
            goto L_0x00af
        L_0x00cf:
            r2 = move-exception
            r3 = r2
            r4 = r0
            goto L_0x00af
        L_0x00d3:
            r0 = move-exception
            r0 = r2
            goto L_0x009c
        L_0x00d6:
            r2 = move-exception
            goto L_0x009c
        L_0x00d8:
            r0 = move-exception
            r0 = r2
            r1 = r2
            goto L_0x009c
        L_0x00dc:
            r0 = move-exception
            r0 = r2
            goto L_0x009c
        L_0x00df:
            r2 = move-exception
            goto L_0x009c
        L_0x00e1:
            r0 = move-exception
            r0 = r2
            r1 = r2
            goto L_0x009c
        L_0x00e5:
            r0 = move-exception
            r0 = r2
            goto L_0x009c
        L_0x00e8:
            r2 = move-exception
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdm.m11603(java.io.File, java.lang.String):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m11604(String str) {
        m11602(new File(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m11607() {
        try {
            if (this.f9873 == null && this.f9882) {
                AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(this.f9887);
                advertisingIdClient.start();
                this.f9873 = advertisingIdClient;
            }
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException e) {
            this.f9873 = null;
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zzaz m11608() {
        try {
            return zzcbm.m10312(this.f9887, this.f9887.getPackageName(), Integer.toString(this.f9887.getPackageManager().getPackageInfo(this.f9887.getPackageName(), 0).versionCode));
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final byte[] m11609() {
        return this.f9872;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m11610() {
        return this.f9876;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzcp m11611() {
        return this.f9875;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final AdvertisingIdClient m11612() {
        if (!this.f9874) {
            return null;
        }
        if (this.f9873 != null) {
            return this.f9873;
        }
        if (this.f9881 != null) {
            try {
                this.f9881.get(2000, TimeUnit.MILLISECONDS);
                this.f9881 = null;
            } catch (InterruptedException | ExecutionException e) {
            } catch (TimeoutException e2) {
                this.f9881.cancel(true);
            }
        }
        return this.f9873;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final int m11613() {
        if (this.f9875 != null) {
            return zzcp.m11515();
        }
        return Integer.MIN_VALUE;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final Future m11614() {
        return this.f9877;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m11615() {
        return this.f9888;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m11616() {
        return this.f9879;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final zzaz m11617() {
        return this.f9883;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzcx m11618() {
        return this.f9884;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final zzaz m11619(int i, boolean z) {
        if (i > 0 && z) {
            try {
                Thread.sleep((long) (i * 1000));
            } catch (InterruptedException e) {
            }
        }
        return m11608();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m11620() {
        return this.f9878;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final DexClassLoader m11621() {
        return this.f9885;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final ExecutorService m11622() {
        return this.f9886;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context m11623() {
        return this.f9887;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Method m11624(String str, String str2) {
        zzer zzer = this.f9889.get(new Pair(str, str2));
        if (zzer == null) {
            return null;
        }
        return zzer.m12295();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11625(int i, boolean z) {
        if (this.f9888) {
            Future<?> submit = this.f9886.submit(new zzdo(this, i, z));
            if (i == 0) {
                this.f9877 = submit;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m11626(String str, String str2, Class<?>... clsArr) {
        if (this.f9889.containsKey(new Pair(str, str2))) {
            return false;
        }
        this.f9889.put(new Pair(str, str2), new zzer(this, str, str2, clsArr));
        return true;
    }
}
