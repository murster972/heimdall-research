package com.google.android.gms.internal;

import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbax extends UIController implements RemoteMediaClient.ProgressListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f8597;

    /* renamed from: 齉  reason: contains not printable characters */
    private final View f8598;

    /* renamed from: 龘  reason: contains not printable characters */
    private final TextView f8599;

    public zzbax(TextView textView, String str, View view) {
        this.f8599 = textView;
        this.f8597 = str;
        this.f8598 = view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9939(long j, boolean z) {
        RemoteMediaClient r0 = m8226();
        if (r0 == null || !r0.m4143()) {
            this.f8599.setVisibility(0);
            this.f8599.setText(this.f8597);
            if (this.f8598 != null) {
                this.f8598.setVisibility(4);
            }
        } else if (!r0.m4147()) {
            if (z) {
                j = r0.m4135();
            }
            this.f8599.setVisibility(0);
            this.f8599.setText(DateUtils.formatElapsedTime(j / 1000));
            if (this.f8598 != null) {
                this.f8598.setVisibility(4);
            }
        } else {
            this.f8599.setText(this.f8597);
            if (this.f8598 != null) {
                this.f8599.setVisibility(4);
                this.f8598.setVisibility(0);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9940() {
        this.f8599.setText(this.f8597);
        if (m8226() != null) {
            m8226().m4166((RemoteMediaClient.ProgressListener) this);
        }
        super.m8223();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9941() {
        m9939(-1, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9942(long j, long j2) {
        m9939(j2, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9943(CastSession castSession) {
        super.m8227(castSession);
        if (m8226() != null) {
            m8226().m4168((RemoteMediaClient.ProgressListener) this, 1000);
        }
        m9939(-1, true);
    }
}
