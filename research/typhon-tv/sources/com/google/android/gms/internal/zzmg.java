package com.google.android.gms.internal;

import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzmg extends zzkt {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public zzkh f10799;

    public final void destroy() {
    }

    public final String getAdUnitId() {
        return null;
    }

    public final String getMediationAdapterClassName() {
        return null;
    }

    public final zzll getVideoController() {
        return null;
    }

    public final boolean isLoading() {
        return false;
    }

    public final boolean isReady() {
        return false;
    }

    public final void pause() {
    }

    public final void resume() {
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final void setManualImpressionsEnabled(boolean z) {
    }

    public final void setUserId(String str) {
    }

    public final void showInterstitial() {
    }

    public final void stopLoading() {
    }

    public final void zza(zzadp zzadp) {
    }

    public final void zza(zzjn zzjn) {
    }

    public final void zza(zzke zzke) {
    }

    public final void zza(zzkh zzkh) {
        this.f10799 = zzkh;
    }

    public final void zza(zzkx zzkx) {
    }

    public final void zza(zzld zzld) {
    }

    public final void zza(zzlr zzlr) {
    }

    public final void zza(zzmr zzmr) {
    }

    public final void zza(zzoa zzoa) {
    }

    public final void zza(zzxl zzxl) {
    }

    public final void zza(zzxr zzxr, String str) {
    }

    public final boolean zzb(zzjj zzjj) {
        zzakb.m4795("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzajr.f4285.post(new zzmh(this));
        return false;
    }

    public final IObjectWrapper zzbr() {
        return null;
    }

    public final zzjn zzbs() {
        return null;
    }

    public final void zzbu() {
    }

    public final zzkx zzcd() {
        return null;
    }

    public final zzkh zzce() {
        return null;
    }

    public final String zzcp() {
        return null;
    }
}
