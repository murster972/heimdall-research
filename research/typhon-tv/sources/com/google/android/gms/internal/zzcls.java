package com.google.android.gms.internal;

import java.io.IOException;

public final class zzcls extends zzfjm<zzcls> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile zzcls[] f9667;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Boolean f9668 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9669 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public zzclu f9670 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzclt[] f9671 = zzclt.m11457();

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9672 = null;

    public zzcls() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzcls[] m11453() {
        if (f9667 == null) {
            synchronized (zzfjq.f10546) {
                if (f9667 == null) {
                    f9667 = new zzcls[0];
                }
            }
        }
        return f9667;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzcls)) {
            return false;
        }
        zzcls zzcls = (zzcls) obj;
        if (this.f9672 == null) {
            if (zzcls.f9672 != null) {
                return false;
            }
        } else if (!this.f9672.equals(zzcls.f9672)) {
            return false;
        }
        if (this.f9669 == null) {
            if (zzcls.f9669 != null) {
                return false;
            }
        } else if (!this.f9669.equals(zzcls.f9669)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f9671, (Object[]) zzcls.f9671)) {
            return false;
        }
        if (this.f9668 == null) {
            if (zzcls.f9668 != null) {
                return false;
            }
        } else if (!this.f9668.equals(zzcls.f9668)) {
            return false;
        }
        if (this.f9670 == null) {
            if (zzcls.f9670 != null) {
                return false;
            }
        } else if (!this.f9670.equals(zzcls.f9670)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzcls.f10533 == null || zzcls.f10533.m12849() : this.f10533.equals(zzcls.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f9668 == null ? 0 : this.f9668.hashCode()) + (((((this.f9669 == null ? 0 : this.f9669.hashCode()) + (((this.f9672 == null ? 0 : this.f9672.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31) + zzfjq.m12859((Object[]) this.f9671)) * 31);
        zzclu zzclu = this.f9670;
        int hashCode2 = ((zzclu == null ? 0 : zzclu.hashCode()) + (hashCode * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode2 + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11454() {
        int r0 = super.m12841();
        if (this.f9672 != null) {
            r0 += zzfjk.m12806(1, this.f9672.intValue());
        }
        if (this.f9669 != null) {
            r0 += zzfjk.m12808(2, this.f9669);
        }
        if (this.f9671 != null && this.f9671.length > 0) {
            int i = r0;
            for (zzclt zzclt : this.f9671) {
                if (zzclt != null) {
                    i += zzfjk.m12807(3, (zzfjs) zzclt);
                }
            }
            r0 = i;
        }
        if (this.f9668 != null) {
            this.f9668.booleanValue();
            r0 += zzfjk.m12805(4) + 1;
        }
        return this.f9670 != null ? r0 + zzfjk.m12807(5, (zzfjs) this.f9670) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11455(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9672 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 18:
                    this.f9669 = zzfjj.m12791();
                    continue;
                case 26:
                    int r2 = zzfjv.m12883(zzfjj, 26);
                    int length = this.f9671 == null ? 0 : this.f9671.length;
                    zzclt[] zzcltArr = new zzclt[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f9671, 0, zzcltArr, 0, length);
                    }
                    while (length < zzcltArr.length - 1) {
                        zzcltArr[length] = new zzclt();
                        zzfjj.m12802((zzfjs) zzcltArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzcltArr[length] = new zzclt();
                    zzfjj.m12802((zzfjs) zzcltArr[length]);
                    this.f9671 = zzcltArr;
                    continue;
                case 32:
                    this.f9668 = Boolean.valueOf(zzfjj.m12797());
                    continue;
                case 42:
                    if (this.f9670 == null) {
                        this.f9670 = new zzclu();
                    }
                    zzfjj.m12802((zzfjs) this.f9670);
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11456(zzfjk zzfjk) throws IOException {
        if (this.f9672 != null) {
            zzfjk.m12832(1, this.f9672.intValue());
        }
        if (this.f9669 != null) {
            zzfjk.m12835(2, this.f9669);
        }
        if (this.f9671 != null && this.f9671.length > 0) {
            for (zzclt zzclt : this.f9671) {
                if (zzclt != null) {
                    zzfjk.m12834(3, (zzfjs) zzclt);
                }
            }
        }
        if (this.f9668 != null) {
            zzfjk.m12836(4, this.f9668.booleanValue());
        }
        if (this.f9670 != null) {
            zzfjk.m12834(5, (zzfjs) this.f9670);
        }
        super.m12842(zzfjk);
    }
}
