package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.overlay.zzn;

final class zzwm implements zzn {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzwl f10948;

    zzwm(zzwl zzwl) {
        this.f10948 = zzwl;
    }

    public final void onPause() {
        zzakb.m4792("AdMobCustomTabsAdapter overlay is paused.");
    }

    public final void onResume() {
        zzakb.m4792("AdMobCustomTabsAdapter overlay is resumed.");
    }

    public final void zzcg() {
        zzakb.m4792("AdMobCustomTabsAdapter overlay is closed.");
        this.f10948.f5503.onAdClosed(this.f10948);
    }

    public final void zzch() {
        zzakb.m4792("Opening AdMobCustomTabsAdapter overlay.");
        this.f10948.f5503.onAdOpened(this.f10948);
    }
}
