package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class zzdui extends zzdug {
    private zzdui(zzdub zzdub) {
        super(zzdub);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12203(byte[] bArr, ByteBuffer byteBuffer) {
        int r0 = zzdug.m12198(bArr.length);
        int remaining = byteBuffer.remaining();
        int r2 = zzdug.m12198(remaining);
        ByteBuffer order = ByteBuffer.allocate(r0 + r2 + 16).order(ByteOrder.LITTLE_ENDIAN);
        order.put(bArr);
        order.position(r0);
        order.put(byteBuffer);
        order.position(r0 + r2);
        order.putLong((long) bArr.length);
        order.putLong((long) remaining);
        return order.array();
    }
}
