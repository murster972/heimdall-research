package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices$zza;

abstract class zzcem extends LocationServices$zza<Status> {
    public zzcem(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Result m10349(Status status) {
        return status;
    }
}
