package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzck;
import com.google.android.gms.common.api.internal.zzcm;
import com.google.android.gms.location.LocationListener;

final class zzcek extends zzcem {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LocationListener f9063;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcek(zzceb zzceb, GoogleApiClient googleApiClient, LocationListener locationListener) {
        super(googleApiClient);
        this.f9063 = locationListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m10347(Api.zzb zzb) throws RemoteException {
        ((zzcfk) zzb).m10409((zzck<LocationListener>) zzcm.m8859(this.f9063, LocationListener.class.getSimpleName()), (zzceu) new zzcen(this));
    }
}
