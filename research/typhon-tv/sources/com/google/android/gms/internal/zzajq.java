package com.google.android.gms.internal;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.internal.zzbs;

@zzzv
public final class zzajq {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ViewTreeObserver.OnGlobalLayoutListener f4272;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ViewTreeObserver.OnScrollChangedListener f4273;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4274;

    /* renamed from: 靐  reason: contains not printable characters */
    private Activity f4275;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f4276;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4277;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f4278;

    public zzajq(Activity activity, View view, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener onScrollChangedListener) {
        this.f4275 = activity;
        this.f4278 = view;
        this.f4272 = onGlobalLayoutListener;
        this.f4273 = onScrollChangedListener;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m4737() {
        if (this.f4275 != null && this.f4277) {
            if (!(this.f4272 == null || this.f4275 == null)) {
                zzbs.zzek().m4658(this.f4275, this.f4272);
            }
            if (!(this.f4273 == null || this.f4275 == null)) {
                zzbs.zzei();
                zzahn.m4580(this.f4275, this.f4273);
            }
            this.f4277 = false;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m4738() {
        if (!this.f4277) {
            if (this.f4272 != null) {
                if (this.f4275 != null) {
                    zzbs.zzei();
                    zzahn.m4606(this.f4275, this.f4272);
                }
                zzbs.zzfc();
                zzaln.m4826(this.f4278, this.f4272);
            }
            if (this.f4273 != null) {
                if (this.f4275 != null) {
                    zzbs.zzei();
                    zzahn.m4607(this.f4275, this.f4273);
                }
                zzbs.zzfc();
                zzaln.m4827(this.f4278, this.f4273);
            }
            this.f4277 = true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4739() {
        this.f4274 = false;
        m4737();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4740() {
        this.f4276 = false;
        m4737();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4741() {
        this.f4276 = true;
        if (this.f4274) {
            m4738();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4742() {
        this.f4274 = true;
        if (this.f4276) {
            m4738();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4743(Activity activity) {
        this.f4275 = activity;
    }
}
