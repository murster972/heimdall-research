package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzbs;
import java.util.Random;

final class zzte extends zzki {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzkh f10886;

    zzte(zzkh zzkh) {
        this.f10886 = zzkh;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m13424() throws RemoteException {
        this.f10886.m13055();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m13425() throws RemoteException {
        this.f10886.m13056();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13426() throws RemoteException {
        this.f10886.m13057();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m13427() throws RemoteException {
        this.f10886.m13058();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13428() throws RemoteException {
        this.f10886.m13059();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13429() throws RemoteException {
        if (zztn.m5867()) {
            int intValue = ((Integer) zzkb.m5481().m5595(zznh.f4932)).intValue();
            int intValue2 = ((Integer) zzkb.m5481().m5595(zznh.f4933)).intValue();
            if (intValue <= 0 || intValue2 < 0) {
                zzbs.zzeu().m5837();
            } else {
                zzahn.f4212.postDelayed(zztf.f10887, (long) (new Random().nextInt(intValue2 + 1) + intValue));
            }
        }
        this.f10886.m13060();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13430(int i) throws RemoteException {
        this.f10886.m13061(i);
    }
}
