package com.google.android.gms.internal;

import android.os.Handler;
import java.util.concurrent.Executor;

public final class zzi implements zzz {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Executor f10708;

    public zzi(Handler handler) {
        this.f10708 = new zzj(this, handler);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13000(zzr<?> zzr, zzad zzad) {
        zzr.m13361("post-error");
        this.f10708.execute(new zzk(this, zzr, zzw.m13619(zzad), (Runnable) null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13001(zzr<?> zzr, zzw<?> zzw) {
        m13002(zzr, zzw, (Runnable) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13002(zzr<?> zzr, zzw<?> zzw, Runnable runnable) {
        zzr.m13357();
        zzr.m13361("post-response");
        this.f10708.execute(new zzk(this, zzr, zzw, runnable));
    }
}
