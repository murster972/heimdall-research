package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.BlockingQueue;

final class zzcil extends Thread {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BlockingQueue<zzcik<?>> f9375;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzcih f9376;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f9377 = new Object();

    public zzcil(zzcih zzcih, String str, BlockingQueue<zzcik<?>> blockingQueue) {
        this.f9376 = zzcih;
        zzbq.m9120(str);
        zzbq.m9120(blockingQueue);
        this.f9375 = blockingQueue;
        setName(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m10989(InterruptedException interruptedException) {
        this.f9376.m11096().m10834().m10850(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008b, code lost:
        r1 = r6.f9376.f9361;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0091, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        r6.f9376.f9362.release();
        r6.f9376.f9361.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        if (r6 != r6.f9376.f9365) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ac, code lost:
        com.google.android.gms.internal.zzcil unused = r6.f9376.f9365 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b2, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b3, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c2, code lost:
        if (r6 != r6.f9376.f9367) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c4, code lost:
        com.google.android.gms.internal.zzcil unused = r6.f9376.f9367 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r6.f9376.m11096().m10832().m10849("Current scheduler thread is neither worker nor network");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r0 = 0
            r1 = r0
        L_0x0002:
            if (r1 != 0) goto L_0x0015
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ InterruptedException -> 0x0010 }
            java.util.concurrent.Semaphore r0 = r0.f9362     // Catch:{ InterruptedException -> 0x0010 }
            r0.acquire()     // Catch:{ InterruptedException -> 0x0010 }
            r0 = 1
            r1 = r0
            goto L_0x0002
        L_0x0010:
            r0 = move-exception
            r6.m10989(r0)
            goto L_0x0002
        L_0x0015:
            int r0 = android.os.Process.myTid()     // Catch:{ all -> 0x0033 }
            int r2 = android.os.Process.getThreadPriority(r0)     // Catch:{ all -> 0x0033 }
        L_0x001d:
            java.util.concurrent.BlockingQueue<com.google.android.gms.internal.zzcik<?>> r0 = r6.f9375     // Catch:{ all -> 0x0033 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0033 }
            com.google.android.gms.internal.zzcik r0 = (com.google.android.gms.internal.zzcik) r0     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0060
            boolean r1 = r0.f9374     // Catch:{ all -> 0x0033 }
            if (r1 == 0) goto L_0x005d
            r1 = r2
        L_0x002c:
            android.os.Process.setThreadPriority(r1)     // Catch:{ all -> 0x0033 }
            r0.run()     // Catch:{ all -> 0x0033 }
            goto L_0x001d
        L_0x0033:
            r0 = move-exception
            com.google.android.gms.internal.zzcih r1 = r6.f9376
            java.lang.Object r1 = r1.f9361
            monitor-enter(r1)
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            java.util.concurrent.Semaphore r2 = r2.f9362     // Catch:{ all -> 0x00f5 }
            r2.release()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            java.lang.Object r2 = r2.f9361     // Catch:{ all -> 0x00f5 }
            r2.notifyAll()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzcil r2 = r2.f9365     // Catch:{ all -> 0x00f5 }
            if (r6 != r2) goto L_0x00e5
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            r3 = 0
            com.google.android.gms.internal.zzcil unused = r2.f9365 = null     // Catch:{ all -> 0x00f5 }
        L_0x005b:
            monitor-exit(r1)     // Catch:{ all -> 0x00f5 }
            throw r0
        L_0x005d:
            r1 = 10
            goto L_0x002c
        L_0x0060:
            java.lang.Object r1 = r6.f9377     // Catch:{ all -> 0x0033 }
            monitor-enter(r1)     // Catch:{ all -> 0x0033 }
            java.util.concurrent.BlockingQueue<com.google.android.gms.internal.zzcik<?>> r0 = r6.f9375     // Catch:{ all -> 0x00b9 }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00b9 }
            if (r0 != 0) goto L_0x007a
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00b9 }
            boolean r0 = r0.f9363     // Catch:{ all -> 0x00b9 }
            if (r0 != 0) goto L_0x007a
            java.lang.Object r0 = r6.f9377     // Catch:{ InterruptedException -> 0x00b4 }
            r4 = 30000(0x7530, double:1.4822E-319)
            r0.wait(r4)     // Catch:{ InterruptedException -> 0x00b4 }
        L_0x007a:
            monitor-exit(r1)     // Catch:{ all -> 0x00b9 }
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x0033 }
            java.lang.Object r1 = r0.f9361     // Catch:{ all -> 0x0033 }
            monitor-enter(r1)     // Catch:{ all -> 0x0033 }
            java.util.concurrent.BlockingQueue<com.google.android.gms.internal.zzcik<?>> r0 = r6.f9375     // Catch:{ all -> 0x00e2 }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00e2 }
            if (r0 != 0) goto L_0x00df
            monitor-exit(r1)     // Catch:{ all -> 0x00e2 }
            com.google.android.gms.internal.zzcih r0 = r6.f9376
            java.lang.Object r1 = r0.f9361
            monitor-enter(r1)
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            java.util.concurrent.Semaphore r0 = r0.f9362     // Catch:{ all -> 0x00cb }
            r0.release()     // Catch:{ all -> 0x00cb }
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            java.lang.Object r0 = r0.f9361     // Catch:{ all -> 0x00cb }
            r0.notifyAll()     // Catch:{ all -> 0x00cb }
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            com.google.android.gms.internal.zzcil r0 = r0.f9365     // Catch:{ all -> 0x00cb }
            if (r6 != r0) goto L_0x00bc
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            r2 = 0
            com.google.android.gms.internal.zzcil unused = r0.f9365 = null     // Catch:{ all -> 0x00cb }
        L_0x00b2:
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            return
        L_0x00b4:
            r0 = move-exception
            r6.m10989(r0)     // Catch:{ all -> 0x00b9 }
            goto L_0x007a
        L_0x00b9:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00b9 }
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x00bc:
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            com.google.android.gms.internal.zzcil r0 = r0.f9367     // Catch:{ all -> 0x00cb }
            if (r6 != r0) goto L_0x00ce
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            r2 = 0
            com.google.android.gms.internal.zzcil unused = r0.f9367 = null     // Catch:{ all -> 0x00cb }
            goto L_0x00b2
        L_0x00cb:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            throw r0
        L_0x00ce:
            com.google.android.gms.internal.zzcih r0 = r6.f9376     // Catch:{ all -> 0x00cb }
            com.google.android.gms.internal.zzchm r0 = r0.m11096()     // Catch:{ all -> 0x00cb }
            com.google.android.gms.internal.zzcho r0 = r0.m10832()     // Catch:{ all -> 0x00cb }
            java.lang.String r2 = "Current scheduler thread is neither worker nor network"
            r0.m10849(r2)     // Catch:{ all -> 0x00cb }
            goto L_0x00b2
        L_0x00df:
            monitor-exit(r1)     // Catch:{ all -> 0x00e2 }
            goto L_0x001d
        L_0x00e2:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00e2 }
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x00e5:
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzcil r2 = r2.f9367     // Catch:{ all -> 0x00f5 }
            if (r6 != r2) goto L_0x00f8
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            r3 = 0
            com.google.android.gms.internal.zzcil unused = r2.f9367 = null     // Catch:{ all -> 0x00f5 }
            goto L_0x005b
        L_0x00f5:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f5 }
            throw r0
        L_0x00f8:
            com.google.android.gms.internal.zzcih r2 = r6.f9376     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzchm r2 = r2.m11096()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.zzcho r2 = r2.m10832()     // Catch:{ all -> 0x00f5 }
            java.lang.String r3 = "Current scheduler thread is neither worker nor network"
            r2.m10849(r3)     // Catch:{ all -> 0x00f5 }
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcil.run():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10990() {
        synchronized (this.f9377) {
            this.f9377.notifyAll();
        }
    }
}
