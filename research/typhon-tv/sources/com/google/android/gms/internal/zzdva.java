package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Provider;
import javax.crypto.Mac;

public final class zzdva implements zzduv<Mac> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m12223(String str, Provider provider) throws GeneralSecurityException {
        return provider == null ? Mac.getInstance(str) : Mac.getInstance(str, provider);
    }
}
