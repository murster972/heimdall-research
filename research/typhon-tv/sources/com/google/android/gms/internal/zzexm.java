package com.google.android.gms.internal;

import java.io.IOException;

public final class zzexm extends zzfjm<zzexm> {

    /* renamed from: 连任  reason: contains not printable characters */
    public zzexn[] f10327 = zzexn.m12352();

    /* renamed from: 靐  reason: contains not printable characters */
    public zzexi f10328 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public zzexk f10329 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzexi f10330 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public zzexi f10331 = null;

    public zzexm() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzexm)) {
            return false;
        }
        zzexm zzexm = (zzexm) obj;
        if (this.f10331 == null) {
            if (zzexm.f10331 != null) {
                return false;
            }
        } else if (!this.f10331.equals(zzexm.f10331)) {
            return false;
        }
        if (this.f10328 == null) {
            if (zzexm.f10328 != null) {
                return false;
            }
        } else if (!this.f10328.equals(zzexm.f10328)) {
            return false;
        }
        if (this.f10330 == null) {
            if (zzexm.f10330 != null) {
                return false;
            }
        } else if (!this.f10330.equals(zzexm.f10330)) {
            return false;
        }
        if (this.f10329 == null) {
            if (zzexm.f10329 != null) {
                return false;
            }
        } else if (!this.f10329.equals(zzexm.f10329)) {
            return false;
        }
        if (!zzfjq.m12863((Object[]) this.f10327, (Object[]) zzexm.f10327)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzexm.f10533 == null || zzexm.f10533.m12849() : this.f10533.equals(zzexm.f10533);
    }

    public final int hashCode() {
        int i = 0;
        zzexi zzexi = this.f10331;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        int hashCode2 = zzexi == null ? 0 : zzexi.hashCode();
        zzexi zzexi2 = this.f10328;
        int i2 = (hashCode2 + hashCode) * 31;
        int hashCode3 = zzexi2 == null ? 0 : zzexi2.hashCode();
        zzexi zzexi3 = this.f10330;
        int i3 = (hashCode3 + i2) * 31;
        int hashCode4 = zzexi3 == null ? 0 : zzexi3.hashCode();
        zzexk zzexk = this.f10329;
        int hashCode5 = ((((zzexk == null ? 0 : zzexk.hashCode()) + ((hashCode4 + i3) * 31)) * 31) + zzfjq.m12859((Object[]) this.f10327)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode5 + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12349() {
        int r0 = super.m12841();
        if (this.f10331 != null) {
            r0 += zzfjk.m12807(1, (zzfjs) this.f10331);
        }
        if (this.f10328 != null) {
            r0 += zzfjk.m12807(2, (zzfjs) this.f10328);
        }
        if (this.f10330 != null) {
            r0 += zzfjk.m12807(3, (zzfjs) this.f10330);
        }
        if (this.f10329 != null) {
            r0 += zzfjk.m12807(4, (zzfjs) this.f10329);
        }
        if (this.f10327 == null || this.f10327.length <= 0) {
            return r0;
        }
        int i = r0;
        for (zzexn zzexn : this.f10327) {
            if (zzexn != null) {
                i += zzfjk.m12807(5, (zzfjs) zzexn);
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12350(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    if (this.f10331 == null) {
                        this.f10331 = new zzexi();
                    }
                    zzfjj.m12802((zzfjs) this.f10331);
                    continue;
                case 18:
                    if (this.f10328 == null) {
                        this.f10328 = new zzexi();
                    }
                    zzfjj.m12802((zzfjs) this.f10328);
                    continue;
                case 26:
                    if (this.f10330 == null) {
                        this.f10330 = new zzexi();
                    }
                    zzfjj.m12802((zzfjs) this.f10330);
                    continue;
                case 34:
                    if (this.f10329 == null) {
                        this.f10329 = new zzexk();
                    }
                    zzfjj.m12802((zzfjs) this.f10329);
                    continue;
                case 42:
                    int r2 = zzfjv.m12883(zzfjj, 42);
                    int length = this.f10327 == null ? 0 : this.f10327.length;
                    zzexn[] zzexnArr = new zzexn[(r2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f10327, 0, zzexnArr, 0, length);
                    }
                    while (length < zzexnArr.length - 1) {
                        zzexnArr[length] = new zzexn();
                        zzfjj.m12802((zzfjs) zzexnArr[length]);
                        zzfjj.m12800();
                        length++;
                    }
                    zzexnArr[length] = new zzexn();
                    zzfjj.m12802((zzfjs) zzexnArr[length]);
                    this.f10327 = zzexnArr;
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12351(zzfjk zzfjk) throws IOException {
        if (this.f10331 != null) {
            zzfjk.m12834(1, (zzfjs) this.f10331);
        }
        if (this.f10328 != null) {
            zzfjk.m12834(2, (zzfjs) this.f10328);
        }
        if (this.f10330 != null) {
            zzfjk.m12834(3, (zzfjs) this.f10330);
        }
        if (this.f10329 != null) {
            zzfjk.m12834(4, (zzfjs) this.f10329);
        }
        if (this.f10327 != null && this.f10327.length > 0) {
            for (zzexn zzexn : this.f10327) {
                if (zzexn != null) {
                    zzfjk.m12834(5, (zzfjs) zzexn);
                }
            }
        }
        super.m12842(zzfjk);
    }
}
