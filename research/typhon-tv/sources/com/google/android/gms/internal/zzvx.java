package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.AdRequest$ErrorCode;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;

@zzzv
public final class zzvx<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzvd f5502;

    public zzvx(zzvd zzvd) {
        this.f5502 = zzvd;
    }

    public final void onClick(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzakb.m4792("Adapter called onClick.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onClick must be called on the main UI thread.");
            zzajr.f4285.post(new zzvy(this));
            return;
        }
        try {
            this.f5502.m13513();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClicked.", e);
        }
    }

    public final void onDismissScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzakb.m4792("Adapter called onDismissScreen.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onDismissScreen must be called on the main UI thread.");
            zzajr.f4285.post(new zzwb(this));
            return;
        }
        try {
            this.f5502.m13510();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClosed.", e);
        }
    }

    public final void onDismissScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzakb.m4792("Adapter called onDismissScreen.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onDismissScreen must be called on the main UI thread.");
            zzajr.f4285.post(new zzwg(this));
            return;
        }
        try {
            this.f5502.m13510();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdClosed.", e);
        }
    }

    public final void onFailedToReceiveAd(MediationBannerAdapter<?, ?> mediationBannerAdapter, AdRequest$ErrorCode adRequest$ErrorCode) {
        String valueOf = String.valueOf(adRequest$ErrorCode);
        zzakb.m4792(new StringBuilder(String.valueOf(valueOf).length() + 47).append("Adapter called onFailedToReceiveAd with error. ").append(valueOf).toString());
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onFailedToReceiveAd must be called on the main UI thread.");
            zzajr.f4285.post(new zzwc(this, adRequest$ErrorCode));
            return;
        }
        try {
            this.f5502.m13514(zzwj.m5985(adRequest$ErrorCode));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdFailedToLoad.", e);
        }
    }

    public final void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, AdRequest$ErrorCode adRequest$ErrorCode) {
        String valueOf = String.valueOf(adRequest$ErrorCode);
        zzakb.m4792(new StringBuilder(String.valueOf(valueOf).length() + 47).append("Adapter called onFailedToReceiveAd with error ").append(valueOf).append(".").toString());
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onFailedToReceiveAd must be called on the main UI thread.");
            zzajr.f4285.post(new zzwh(this, adRequest$ErrorCode));
            return;
        }
        try {
            this.f5502.m13514(zzwj.m5985(adRequest$ErrorCode));
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdFailedToLoad.", e);
        }
    }

    public final void onLeaveApplication(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzakb.m4792("Adapter called onLeaveApplication.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onLeaveApplication must be called on the main UI thread.");
            zzajr.f4285.post(new zzwd(this));
            return;
        }
        try {
            this.f5502.m13512();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLeftApplication.", e);
        }
    }

    public final void onLeaveApplication(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzakb.m4792("Adapter called onLeaveApplication.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onLeaveApplication must be called on the main UI thread.");
            zzajr.f4285.post(new zzwi(this));
            return;
        }
        try {
            this.f5502.m13512();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLeftApplication.", e);
        }
    }

    public final void onPresentScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzakb.m4792("Adapter called onPresentScreen.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onPresentScreen must be called on the main UI thread.");
            zzajr.f4285.post(new zzwe(this));
            return;
        }
        try {
            this.f5502.m13511();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdOpened.", e);
        }
    }

    public final void onPresentScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzakb.m4792("Adapter called onPresentScreen.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onPresentScreen must be called on the main UI thread.");
            zzajr.f4285.post(new zzvz(this));
            return;
        }
        try {
            this.f5502.m13511();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdOpened.", e);
        }
    }

    public final void onReceivedAd(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzakb.m4792("Adapter called onReceivedAd.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onReceivedAd must be called on the main UI thread.");
            zzajr.f4285.post(new zzwf(this));
            return;
        }
        try {
            this.f5502.m13509();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLoaded.", e);
        }
    }

    public final void onReceivedAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzakb.m4792("Adapter called onReceivedAd.");
        zzkb.m5487();
        if (!zzajr.m4752()) {
            zzakb.m4791("onReceivedAd must be called on the main UI thread.");
            zzajr.f4285.post(new zzwa(this));
            return;
        }
        try {
            this.f5502.m13509();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLoaded.", e);
        }
    }
}
