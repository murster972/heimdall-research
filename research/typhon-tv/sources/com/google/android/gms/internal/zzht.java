package com.google.android.gms.internal;

import android.util.Base64OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

final class zzht {

    /* renamed from: 靐  reason: contains not printable characters */
    private Base64OutputStream f10703 = new Base64OutputStream(this.f10704, 10);

    /* renamed from: 龘  reason: contains not printable characters */
    private ByteArrayOutputStream f10704 = new ByteArrayOutputStream(4096);

    public final String toString() {
        String str;
        try {
            this.f10703.close();
        } catch (IOException e) {
            zzagf.m4793("HashManager: Unable to convert to Base64.", e);
        }
        try {
            this.f10704.close();
            str = this.f10704.toString();
        } catch (IOException e2) {
            zzagf.m4793("HashManager: Unable to convert to Base64.", e2);
            str = "";
        } finally {
            this.f10704 = null;
            this.f10703 = null;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12998(byte[] bArr) throws IOException {
        this.f10703.write(bArr);
    }
}
