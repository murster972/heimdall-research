package com.google.android.gms.internal;

import android.os.DeadObjectException;
import android.os.IInterface;

public interface zzcfu<T extends IInterface> {
    /* renamed from: 靐  reason: contains not printable characters */
    T m10419() throws DeadObjectException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m10420();
}
