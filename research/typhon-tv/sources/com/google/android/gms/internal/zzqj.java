package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzqj extends zzev implements zzqi {
    public zzqj() {
        attachInterface(this, "com.google.android.gms.ads.internal.formats.client.INativeContentAd");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 2:
                IObjectWrapper r0 = m13292();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r0);
                break;
            case 3:
                String r02 = m13299();
                parcel2.writeNoException();
                parcel2.writeString(r02);
                break;
            case 4:
                List r03 = m13294();
                parcel2.writeNoException();
                parcel2.writeList(r03);
                break;
            case 5:
                String r04 = m13293();
                parcel2.writeNoException();
                parcel2.writeString(r04);
                break;
            case 6:
                zzpq r05 = m13286();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r05);
                break;
            case 7:
                String r06 = m13287();
                parcel2.writeNoException();
                parcel2.writeString(r06);
                break;
            case 8:
                String r07 = m13288();
                parcel2.writeNoException();
                parcel2.writeString(r07);
                break;
            case 9:
                Bundle r08 = m13289();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r08);
                break;
            case 10:
                m13290();
                parcel2.writeNoException();
                break;
            case 11:
                zzll r09 = m13291();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r09);
                break;
            case 12:
                m13300((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                boolean r010 = m13295((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                zzew.m12307(parcel2, r010);
                break;
            case 14:
                m13298((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 15:
                zzpm r011 = m13301();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r011);
                break;
            case 16:
                IObjectWrapper r012 = m13297();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r012);
                break;
            case 17:
                String r013 = m13296();
                parcel2.writeNoException();
                parcel2.writeString(r013);
                break;
            default:
                return false;
        }
        return true;
    }
}
