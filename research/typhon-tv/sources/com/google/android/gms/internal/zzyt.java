package com.google.android.gms.internal;

import android.content.Context;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.internal.gmsg.zzd;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.zzba;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzbs;
import java.lang.ref.WeakReference;

@zzzv
public final class zzyt {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzba f5598;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ViewTreeObserver.OnGlobalLayoutListener f5599;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ViewTreeObserver.OnScrollChangedListener f5600;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzaji f5601;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f5602 = -1;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f5603 = -1;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zznu f5604;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f5605;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzafp f5606;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzcv f5607;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f5608 = new Object();

    public zzyt(Context context, zzcv zzcv, zzafp zzafp, zznu zznu, zzba zzba) {
        this.f5605 = context;
        this.f5607 = zzcv;
        this.f5606 = zzafp;
        this.f5604 = zznu;
        this.f5598 = zzba;
        this.f5601 = new zzaji(200);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final ViewTreeObserver.OnScrollChangedListener m6062(WeakReference<zzanh> weakReference) {
        if (this.f5600 == null) {
            this.f5600 = new zzza(this, weakReference);
        }
        return this.f5600;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final ViewTreeObserver.OnGlobalLayoutListener m6064(WeakReference<zzanh> weakReference) {
        if (this.f5599 == null) {
            this.f5599 = new zzyz(this, weakReference);
        }
        return this.f5599;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6066(zzanh zzanh) {
        zzani r0 = zzanh.m4980();
        r0.m5053("/video", (zzt<? super zzanh>) zzd.zzbxi);
        r0.m5053("/videoMeta", (zzt<? super zzanh>) zzd.zzbxj);
        r0.m5053("/precache", (zzt<? super zzanh>) new zzane());
        r0.m5053("/delayPageLoaded", (zzt<? super zzanh>) zzd.zzbxm);
        r0.m5053("/instrument", (zzt<? super zzanh>) zzd.zzbxk);
        r0.m5053("/log", (zzt<? super zzanh>) zzd.zzbxd);
        r0.m5053("/videoClicked", (zzt<? super zzanh>) zzd.zzbxe);
        r0.m5053("/trackActiveViewUnit", (zzt<? super zzanh>) new zzyx(this));
        r0.m5053("/untrackActiveViewUnit", (zzt<? super zzanh>) new zzyy(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6069(WeakReference<zzanh> weakReference, boolean z) {
        zzanh zzanh;
        if (weakReference != null && (zzanh = (zzanh) weakReference.get()) != null) {
            if (zzanh == null) {
                throw null;
            } else if (((View) zzanh) == null) {
            } else {
                if (z && !this.f5601.m4724()) {
                    return;
                }
                if (zzanh == null) {
                    throw null;
                }
                int[] iArr = new int[2];
                ((View) zzanh).getLocationOnScreen(iArr);
                zzkb.m5487();
                int r1 = zzajr.m4749(this.f5605, iArr[0]);
                zzkb.m5487();
                int r4 = zzajr.m4749(this.f5605, iArr[1]);
                synchronized (this.f5608) {
                    if (!(this.f5602 == r1 && this.f5603 == r4)) {
                        this.f5602 = r1;
                        this.f5603 = r4;
                        zzanh.m4980().m5045(this.f5602, this.f5603, !z);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzanh m6070() throws zzanv {
        return zzbs.zzej().m5060(this.f5605, zzapa.m5222(), "native-video", false, false, this.f5607, this.f5606.f4136.f3748, this.f5604, (zzbl) null, this.f5598.zzbq(), this.f5606.f4130);
    }
}
