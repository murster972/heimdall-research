package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@zzzv
public final class zzaff {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final AtomicReference<Object> f4080 = new AtomicReference<>((Object) null);

    /* renamed from: ʼ  reason: contains not printable characters */
    private final AtomicReference<Object> f4081 = new AtomicReference<>((Object) null);

    /* renamed from: ʽ  reason: contains not printable characters */
    private ConcurrentMap<String, Method> f4082 = new ConcurrentHashMap(9);

    /* renamed from: 连任  reason: contains not printable characters */
    private final AtomicInteger f4083 = new AtomicInteger(-1);

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f4084 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private AtomicBoolean f4085 = new AtomicBoolean(false);

    /* renamed from: 齉  reason: contains not printable characters */
    private String f4086 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicReference<ThreadPoolExecutor> f4087 = new AtomicReference<>((Object) null);

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Method m4409(Context context, String str) {
        Method method = (Method) this.f4082.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[]{String.class});
            this.f4082.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            m4417(e, str, false);
            return null;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Method m4410(Context context, String str) {
        Method method = (Method) this.f4082.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[0]);
            this.f4082.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            m4417(e, str, false);
            return null;
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Method m4411(Context context, String str) {
        Method method = (Method) this.f4082.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics").getDeclaredMethod(str, new Class[]{Activity.class, String.class, String.class});
            this.f4082.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            m4417(e, str, false);
            return null;
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Method m4412(Context context) {
        Method method = (Method) this.f4082.get("logEventInternal");
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod("logEventInternal", new Class[]{String.class, String.class, Bundle.class});
            this.f4082.put("logEventInternal", declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            m4417(e, "logEventInternal", true);
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4413(Context context, String str, String str2) {
        if (m4418(context, "com.google.android.gms.measurement.AppMeasurement", this.f4080, true)) {
            Method r0 = m4409(context, str2);
            try {
                r0.invoke(this.f4080.get(), new Object[]{str});
                zzagf.m4527(new StringBuilder(String.valueOf(str2).length() + 37 + String.valueOf(str).length()).append("Invoke Firebase method ").append(str2).append(", Ad Unit Id: ").append(str).toString());
            } catch (Exception e) {
                m4417(e, str2, false);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Bundle m4414(Context context, String str, boolean z) {
        Bundle bundle = new Bundle();
        try {
            bundle.putLong("_aeid", Long.parseLong(str));
        } catch (NullPointerException | NumberFormatException e) {
            String valueOf = String.valueOf(str);
            zzagf.m4793(valueOf.length() != 0 ? "Invalid event ID: ".concat(valueOf) : new String("Invalid event ID: "), e);
        }
        if (z) {
            bundle.putInt("_r", 1);
        }
        return bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object m4415(String str, Context context) {
        if (!m4418(context, "com.google.android.gms.measurement.AppMeasurement", this.f4080, true)) {
            return null;
        }
        try {
            return m4410(context, str).invoke(this.f4080.get(), new Object[0]);
        } catch (Exception e) {
            m4417(e, str, true);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4416(Context context, String str, Bundle bundle) {
        if (m4436(context) && m4418(context, "com.google.android.gms.measurement.AppMeasurement", this.f4080, true)) {
            Method r0 = m4412(context);
            try {
                r0.invoke(this.f4080.get(), new Object[]{"am", str, bundle});
            } catch (Exception e) {
                m4417(e, "logEventInternal", true);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4417(Exception exc, String str, boolean z) {
        if (!this.f4085.get()) {
            zzagf.m4791(new StringBuilder(String.valueOf(str).length() + 30).append("Invoke Firebase method ").append(str).append(" error.").toString());
            if (z) {
                zzagf.m4791("The Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires the latest Firebase SDK jar, but Firebase SDK is either missing or out of date");
                this.f4085.set(true);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m4418(Context context, String str, AtomicReference<Object> atomicReference, boolean z) {
        if (atomicReference.get() == null) {
            try {
                atomicReference.compareAndSet((Object) null, context.getClassLoader().loadClass(str).getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke((Object) null, new Object[]{context}));
            } catch (Exception e) {
                m4417(e, "getInstance", z);
                return false;
            }
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m4419(Context context) {
        if (!m4436(context) || !m4418(context, "com.google.android.gms.measurement.AppMeasurement", this.f4080, true)) {
            return "";
        }
        try {
            String str = (String) m4410(context, "getCurrentScreenName").invoke(this.f4080.get(), new Object[0]);
            if (str == null) {
                str = (String) m4410(context, "getCurrentScreenClass").invoke(this.f4080.get(), new Object[0]);
            }
            return str == null ? "" : str;
        } catch (Exception e) {
            m4417(e, "getCurrentScreenName", false);
            return "";
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m4420(Context context, String str) {
        m4434(context, "_aq", str);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m4421(Context context) {
        if (!m4436(context)) {
            return null;
        }
        synchronized (this.f4084) {
            if (this.f4086 != null) {
                String str = this.f4086;
                return str;
            }
            this.f4086 = (String) m4415("getGmpAppId", context);
            String str2 = this.f4086;
            return str2;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m4422(Context context) {
        if (!m4436(context)) {
            return null;
        }
        long longValue = ((Long) zzkb.m5481().m5595(zznh.f4906)).longValue();
        if (longValue < 0) {
            return (String) m4415("getAppInstanceId", context);
        }
        if (this.f4087.get() == null) {
            this.f4087.compareAndSet((Object) null, new ThreadPoolExecutor(((Integer) zzkb.m5481().m5595(zznh.f4907)).intValue(), ((Integer) zzkb.m5481().m5595(zznh.f4907)).intValue(), 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), new zzafh(this)));
        }
        Future submit = this.f4087.get().submit(new zzafg(this, context));
        try {
            return (String) submit.get(longValue, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            submit.cancel(true);
            if (e instanceof TimeoutException) {
                return "TIME_OUT";
            }
            return null;
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m4423(Context context) {
        Object r1;
        if (m4436(context) && (r1 = m4415("generateEventId", context)) != null) {
            return r1.toString();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final /* synthetic */ String m4424(Context context) throws Exception {
        return (String) m4415("getAppInstanceId", context);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m4425(Context context, String str) {
        m4434(context, "_ai", str);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m4426(Context context) {
        return ((Boolean) zzkb.m5481().m5595(zznh.f4900)).booleanValue() && m4436(context);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4427(Context context, String str) {
        if (m4436(context)) {
            m4413(context, str, "endAdUnitExposure");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m4428(Context context) {
        return ((Boolean) zzkb.m5481().m5595(zznh.f4897)).booleanValue() && m4436(context);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4429(Context context, String str) {
        m4434(context, "_ac", str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m4430(Context context) {
        return ((Boolean) zzkb.m5481().m5595(zznh.f4899)).booleanValue() && m4436(context);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4431(Context context, String str) {
        if (m4436(context) && (context instanceof Activity) && m4418(context, "com.google.firebase.analytics.FirebaseAnalytics", this.f4081, false)) {
            Method r2 = m4411(context, "setCurrentScreen");
            try {
                r2.invoke(this.f4081.get(), new Object[]{(Activity) context, str, context.getPackageName()});
            } catch (Exception e) {
                m4417(e, "setCurrentScreen", false);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m4432(Context context) {
        return ((Boolean) zzkb.m5481().m5595(zznh.f4898)).booleanValue() && m4436(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4433(Context context, String str) {
        if (m4436(context)) {
            m4413(context, str, "beginAdUnitExposure");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4434(Context context, String str, String str2) {
        if (m4436(context)) {
            m4416(context, str, m4414(context, str2, "_ac".equals(str)));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4435(Context context, String str, String str2, String str3, int i) {
        if (m4436(context)) {
            Bundle r0 = m4414(context, str, false);
            r0.putString("_ai", str2);
            r0.putString(VastExtensionXmlManager.TYPE, str3);
            r0.putInt("value", i);
            m4416(context, "_ar", r0);
            zzagf.m4527(new StringBuilder(String.valueOf(str3).length() + 75).append("Log a Firebase reward video event, reward type: ").append(str3).append(", reward value: ").append(i).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4436(Context context) {
        if (!((Boolean) zzkb.m5481().m5595(zznh.f4896)).booleanValue() || this.f4085.get()) {
            return false;
        }
        if (this.f4083.get() == -1) {
            zzkb.m5487();
            if (!zzajr.m4756(context)) {
                zzkb.m5487();
                if (zzajr.m4744(context)) {
                    zzagf.m4791("Google Play Service is out of date, the Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires updated Google Play Service.");
                    this.f4083.set(0);
                }
            }
            this.f4083.set(1);
        }
        return this.f4083.get() == 1;
    }
}
