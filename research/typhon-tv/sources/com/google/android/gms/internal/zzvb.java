package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzvb extends zzev implements zzva {
    public zzvb() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zzvd zzvf;
        zzvd zzvf2;
        zzvd zzvf3;
        zzvd zzvd = null;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                IObjectWrapper r1 = IObjectWrapper.zza.m9305(parcel.readStrongBinder());
                zzjn zzjn = (zzjn) zzew.m12304(parcel, zzjn.CREATOR);
                zzjj zzjj = (zzjj) zzew.m12304(parcel, zzjj.CREATOR);
                String readString = parcel.readString();
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    zzvf3 = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzvf3 = queryLocalInterface instanceof zzvd ? (zzvd) queryLocalInterface : new zzvf(readStrongBinder);
                }
                m13475(r1, zzjn, zzjj, readString, zzvf3);
                parcel2.writeNoException();
                break;
            case 2:
                IObjectWrapper r0 = m13468();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r0);
                break;
            case 3:
                IObjectWrapper r2 = IObjectWrapper.zza.m9305(parcel.readStrongBinder());
                zzjj zzjj2 = (zzjj) zzew.m12304(parcel, zzjj.CREATOR);
                String readString2 = parcel.readString();
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzvd = queryLocalInterface2 instanceof zzvd ? (zzvd) queryLocalInterface2 : new zzvf(readStrongBinder2);
                }
                m13472(r2, zzjj2, readString2, zzvd);
                parcel2.writeNoException();
                break;
            case 4:
                m13465();
                parcel2.writeNoException();
                break;
            case 5:
                m13467();
                parcel2.writeNoException();
                break;
            case 6:
                IObjectWrapper r12 = IObjectWrapper.zza.m9305(parcel.readStrongBinder());
                zzjn zzjn2 = (zzjn) zzew.m12304(parcel, zzjn.CREATOR);
                zzjj zzjj3 = (zzjj) zzew.m12304(parcel, zzjj.CREATOR);
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzvd = queryLocalInterface3 instanceof zzvd ? (zzvd) queryLocalInterface3 : new zzvf(readStrongBinder3);
                }
                m13476(r12, zzjn2, zzjj3, readString3, readString4, zzvd);
                parcel2.writeNoException();
                break;
            case 7:
                IObjectWrapper r13 = IObjectWrapper.zza.m9305(parcel.readStrongBinder());
                zzjj zzjj4 = (zzjj) zzew.m12304(parcel, zzjj.CREATOR);
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 == null) {
                    zzvf2 = null;
                } else {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzvf2 = queryLocalInterface4 instanceof zzvd ? (zzvd) queryLocalInterface4 : new zzvf(readStrongBinder4);
                }
                m13473(r13, zzjj4, readString5, readString6, zzvf2);
                parcel2.writeNoException();
                break;
            case 8:
                m13466();
                parcel2.writeNoException();
                break;
            case 9:
                m13464();
                parcel2.writeNoException();
                break;
            case 10:
                m13471(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), (zzjj) zzew.m12304(parcel, zzjj.CREATOR), parcel.readString(), zzaen.m9549(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                m13477((zzjj) zzew.m12304(parcel, zzjj.CREATOR), parcel.readString());
                parcel2.writeNoException();
                break;
            case 12:
                m13455();
                parcel2.writeNoException();
                break;
            case 13:
                boolean r02 = m13456();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r02);
                break;
            case 14:
                IObjectWrapper r14 = IObjectWrapper.zza.m9305(parcel.readStrongBinder());
                zzjj zzjj5 = (zzjj) zzew.m12304(parcel, zzjj.CREATOR);
                String readString7 = parcel.readString();
                String readString8 = parcel.readString();
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 == null) {
                    zzvf = null;
                } else {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzvf = queryLocalInterface5 instanceof zzvd ? (zzvd) queryLocalInterface5 : new zzvf(readStrongBinder5);
                }
                m13474(r14, zzjj5, readString7, readString8, zzvf, (zzpe) zzew.m12304(parcel, zzpe.CREATOR), parcel.createStringArrayList());
                parcel2.writeNoException();
                break;
            case 15:
                zzvj r03 = m13457();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r03);
                break;
            case 16:
                zzvm r04 = m13461();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r04);
                break;
            case 17:
                Bundle r05 = m13462();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r05);
                break;
            case 18:
                Bundle r06 = m13463();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r06);
                break;
            case 19:
                Bundle r07 = m13460();
                parcel2.writeNoException();
                zzew.m12302(parcel2, r07);
                break;
            case 20:
                m13478((zzjj) zzew.m12304(parcel, zzjj.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 21:
                m13469(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 22:
                boolean r08 = m13458();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r08);
                break;
            case 23:
                m13470(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), zzaen.m9549(parcel.readStrongBinder()), (List<String>) parcel.createStringArrayList());
                parcel2.writeNoException();
                break;
            case 24:
                zzqm r09 = m13459();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r09);
                break;
            case 25:
                m13479(zzew.m12308(parcel));
                parcel2.writeNoException();
                break;
            case 26:
                zzll r010 = m13480();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r010);
                break;
            default:
                return false;
        }
        return true;
    }
}
