package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzpz extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m13243() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13244(IObjectWrapper iObjectWrapper) throws RemoteException;
}
