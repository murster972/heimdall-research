package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzamw implements zzt<zzamp> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        zzaoa zzaoa;
        zzamp zzamp = (zzamp) obj;
        if (((Boolean) zzkb.m5481().m5595(zznh.f4953)).booleanValue()) {
            zzaoa r0 = zzamp.m4927();
            if (r0 == null) {
                try {
                    zzaoa zzaoa2 = new zzaoa(zzamp, Float.parseFloat((String) map.get(VastIconXmlManager.DURATION)), PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("customControlsAllowed")), PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("clickToExpandAllowed")));
                    zzamp.m4931(zzaoa2);
                    zzaoa = zzaoa2;
                } catch (NullPointerException | NumberFormatException e) {
                    zzagf.m4793("Unable to parse videoMeta message.", e);
                    zzbs.zzem().m4505(e, "VideoMetaGmsgHandler.onGmsg");
                    return;
                }
            } else {
                zzaoa = r0;
            }
            boolean equals = PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("muted"));
            float parseFloat = Float.parseFloat((String) map.get("currentTime"));
            int parseInt = Integer.parseInt((String) map.get("playbackState"));
            int i = (parseInt < 0 || 3 < parseInt) ? 0 : parseInt;
            String str = (String) map.get("aspectRatio");
            float parseFloat2 = TextUtils.isEmpty(str) ? 0.0f : Float.parseFloat(str);
            if (zzagf.m4798(3)) {
                zzagf.m4792(new StringBuilder(String.valueOf(str).length() + 79).append("Video Meta GMSG: isMuted : ").append(equals).append(" , playbackState : ").append(i).append(" , aspectRatio : ").append(str).toString());
            }
            zzaoa.m5210(parseFloat, i, equals, parseFloat2);
        }
    }
}
