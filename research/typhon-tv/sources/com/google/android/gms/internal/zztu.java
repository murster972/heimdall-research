package com.google.android.gms.internal;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zztu {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Charset f5392 = Charset.forName("UTF-8");

    /* renamed from: 齉  reason: contains not printable characters */
    private static zztr<InputStream> f5393 = zztv.f10895;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zztt<JSONObject> f5394 = new zztw();

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ InputStream m5870(JSONObject jSONObject) throws JSONException {
        return new ByteArrayInputStream(jSONObject.toString().getBytes(f5392));
    }
}
