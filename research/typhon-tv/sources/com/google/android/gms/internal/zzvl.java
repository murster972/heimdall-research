package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

public final class zzvl extends zzeu implements zzvj {
    zzvl(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final double m13551() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        double readDouble = r0.readDouble();
        r0.recycle();
        return readDouble;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m13552() throws RemoteException {
        Parcel r0 = m12300(8, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m13553() throws RemoteException {
        Parcel r0 = m12300(9, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzll m13554() throws RemoteException {
        Parcel r0 = m12300(17, v_());
        zzll r1 = zzlm.m13094(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final IObjectWrapper m13555() throws RemoteException {
        Parcel r0 = m12300(18, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final Bundle m13556() throws RemoteException {
        Parcel r1 = m12300(15, v_());
        Bundle bundle = (Bundle) zzew.m12304(r1, Bundle.CREATOR);
        r1.recycle();
        return bundle;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final IObjectWrapper m13557() throws RemoteException {
        Parcel r0 = m12300(21, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m13558() throws RemoteException {
        m12298(10, v_());
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m13559() throws RemoteException {
        Parcel r0 = m12300(13, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m13560() throws RemoteException {
        Parcel r0 = m12300(14, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m13561() throws RemoteException {
        Parcel r0 = m12300(6, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m13562() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        ArrayList r1 = zzew.m12301(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m13563(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(12, v_);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzpq m13564() throws RemoteException {
        Parcel r0 = m12300(5, v_());
        zzpq r1 = zzpr.m13226(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m13565() throws RemoteException {
        Parcel r0 = m12300(4, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m13566(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(16, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13567() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13568(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(11, v_);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final zzpm m13569() throws RemoteException {
        Parcel r0 = m12300(19, v_());
        zzpm r1 = zzpn.m13220(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final IObjectWrapper m13570() throws RemoteException {
        Parcel r0 = m12300(20, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
