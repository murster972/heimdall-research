package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.zzab;

public final class zzbcs extends zzeu implements zzbcr {
    zzbcs(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.internal.ICastDeviceController");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10061(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12299(11, v_);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10062(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12299(12, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10063() throws RemoteException {
        m12299(1, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10064(double d, double d2, boolean z) throws RemoteException {
        Parcel v_ = v_();
        v_.writeDouble(d);
        v_.writeDouble(d2);
        zzew.m12307(v_, z);
        m12299(7, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10065(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12299(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10066(String str, LaunchOptions launchOptions) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12306(v_, (Parcelable) launchOptions);
        m12299(13, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10067(String str, String str2, long j) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        v_.writeLong(j);
        m12299(9, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10068(String str, String str2, zzab zzab) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        v_.writeString(str2);
        zzew.m12306(v_, (Parcelable) zzab);
        m12299(14, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10069(boolean z, double d, boolean z2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        v_.writeDouble(d);
        zzew.m12307(v_, z2);
        m12299(8, v_);
    }
}
