package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzbhb implements ThreadFactory {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f8747;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ThreadFactory f8748;

    /* renamed from: 齉  reason: contains not printable characters */
    private final AtomicInteger f8749;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8750;

    public zzbhb(String str) {
        this(str, 0);
    }

    private zzbhb(String str, int i) {
        this.f8749 = new AtomicInteger();
        this.f8748 = Executors.defaultThreadFactory();
        this.f8750 = (String) zzbq.m9121(str, (Object) "Name must not be null");
        this.f8747 = 0;
    }

    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.f8748.newThread(new zzbhc(runnable, 0));
        String str = this.f8750;
        newThread.setName(new StringBuilder(String.valueOf(str).length() + 13).append(str).append("[").append(this.f8749.getAndIncrement()).append("]").toString());
        return newThread;
    }
}
