package com.google.android.gms.internal;

import java.util.Arrays;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

final class zzdvh {
    /* renamed from: 龘  reason: contains not printable characters */
    private static long m12231(byte[] bArr, int i) {
        return ((long) ((bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24))) & InternalZipTyphoonApp.ZIP_64_LIMIT;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m12232(byte[] bArr, int i, int i2) {
        return (m12231(bArr, i) >> i2) & 67108863;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m12233(byte[] bArr, long j, int i) {
        int i2 = 0;
        while (i2 < 4) {
            bArr[i + i2] = (byte) ((int) (255 & j));
            i2++;
            j >>= 8;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static byte[] m12234(byte[] bArr, byte[] bArr2) {
        if (bArr.length != 32) {
            throw new IllegalArgumentException("The key length in bytes must be 32.");
        }
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        long r16 = 67108863 & m12232(bArr, 0, 0);
        long r18 = 67108611 & m12232(bArr, 3, 2);
        long r20 = 67092735 & m12232(bArr, 6, 4);
        long r22 = 66076671 & m12232(bArr, 9, 6);
        long r24 = 1048575 & m12232(bArr, 12, 8);
        long j6 = r18 * 5;
        long j7 = r20 * 5;
        long j8 = r22 * 5;
        long j9 = r24 * 5;
        byte[] bArr3 = new byte[17];
        for (int i = 0; i < bArr2.length; i += 16) {
            int min = Math.min(16, bArr2.length - i);
            System.arraycopy(bArr2, i, bArr3, 0, min);
            bArr3[min] = 1;
            if (min != 16) {
                Arrays.fill(bArr3, min + 1, 17, (byte) 0);
            }
            long r14 = j + m12232(bArr3, 0, 0);
            long r12 = j2 + m12232(bArr3, 3, 2);
            long r10 = j3 + m12232(bArr3, 6, 4);
            long r8 = j4 + m12232(bArr3, 9, 6);
            long r6 = j5 + (m12232(bArr3, 12, 8) | ((long) (bArr3[16] << 24)));
            long j10 = (r14 * r16) + (r12 * j9) + (r10 * j8) + (r8 * j7) + (r6 * j6);
            long j11 = (r14 * r18) + (r12 * r16) + (r10 * j9) + (r8 * j8) + (r6 * j7);
            long j12 = (r14 * r20) + (r12 * r18) + (r10 * r16) + (r8 * j9) + (r6 * j8);
            long j13 = r10 * r20;
            long j14 = r8 * r18;
            long j15 = r6 * r16;
            long j16 = j15 + j14 + j13 + (r12 * r22) + (r14 * r24);
            long j17 = j10 & 67108863;
            long j18 = (j10 >> 26) + j11;
            long j19 = j18 & 67108863;
            long j20 = j12 + (j18 >> 26);
            j3 = 67108863 & j20;
            long j21 = (r14 * r22) + (r12 * r20) + (r10 * r18) + (r8 * r16) + (r6 * j9) + (j20 >> 26);
            long j22 = j21 >> 26;
            j4 = j21 & 67108863;
            long j23 = j16 + j22;
            long j24 = j23 >> 26;
            j5 = j23 & 67108863;
            long j25 = j17 + (j24 * 5);
            j = 67108863 & j25;
            j2 = j19 + (j25 >> 26);
        }
        long j26 = (j2 >> 26) + j3;
        long j27 = j26 >> 26;
        long j28 = j26 & 67108863;
        long j29 = j4 + j27;
        long j30 = j29 >> 26;
        long j31 = j29 & 67108863;
        long j32 = j5 + j30;
        long j33 = j32 >> 26;
        long j34 = j32 & 67108863;
        long j35 = (j33 * 5) + j;
        long j36 = j35 >> 26;
        long j37 = j35 & 67108863;
        long j38 = (j2 & 67108863) + j36;
        long j39 = 5 + j37;
        long j40 = (j39 >> 26) + j38;
        long j41 = (j40 >> 26) + j28;
        long j42 = (j41 >> 26) + j31;
        long j43 = ((j42 >> 26) + j34) - 67108864;
        long j44 = j43 >> 63;
        long j45 = j37 & j44;
        long j46 = j38 & j44;
        long j47 = j28 & j44;
        long j48 = j31 & j44;
        long j49 = j34 & j44;
        long j50 = j44 ^ -1;
        long j51 = j46 | (j40 & 67108863 & j50);
        long j52 = j47 | (j41 & 67108863 & j50);
        long j53 = j48 | (j42 & 67108863 & j50);
        long j54 = (j45 | (j39 & 67108863 & j50) | (j51 << 26)) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        long j55 = ((j51 >> 6) | (j52 << 20)) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        long j56 = ((j52 >> 12) | (j53 << 14)) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        long j57 = (((j49 | (j43 & j50)) << 8) | (j53 >> 18)) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        long r82 = m12231(bArr, 16) + j54;
        long j58 = InternalZipTyphoonApp.ZIP_64_LIMIT & r82;
        long r83 = (r82 >> 32) + j55 + m12231(bArr, 20);
        long j59 = InternalZipTyphoonApp.ZIP_64_LIMIT & r83;
        long r4 = j56 + m12231(bArr, 24) + (r83 >> 32);
        long j60 = InternalZipTyphoonApp.ZIP_64_LIMIT & r4;
        long r42 = ((r4 >> 32) + j57 + m12231(bArr, 28)) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        byte[] bArr4 = new byte[16];
        m12233(bArr4, j58, 0);
        m12233(bArr4, j59, 4);
        m12233(bArr4, j60, 8);
        m12233(bArr4, r42, 12);
        return bArr4;
    }
}
