package com.google.android.gms.internal;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@zzzv
public class zzall<T> implements zzalh<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f4315 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private T f4316;

    /* renamed from: 齉  reason: contains not printable characters */
    private BlockingQueue<zzalm> f4317 = new LinkedBlockingQueue();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4318 = new Object();

    public final int getStatus() {
        return this.f4315;
    }

    public final void reject() {
        synchronized (this.f4318) {
            if (this.f4315 != 0) {
                throw new UnsupportedOperationException();
            }
            this.f4315 = -1;
            for (zzalm zzalm : this.f4317) {
                zzalm.f8296.m9689();
            }
            this.f4317.clear();
        }
    }

    public final void zza(zzalk<T> zzalk, zzali zzali) {
        synchronized (this.f4318) {
            if (this.f4315 == 1) {
                zzalk.m9691(this.f4316);
            } else if (this.f4315 == -1) {
                zzali.m9689();
            } else if (this.f4315 == 0) {
                this.f4317.add(new zzalm(this, zzalk, zzali));
            }
        }
    }

    public final void zzk(T t) {
        synchronized (this.f4318) {
            if (this.f4315 != 0) {
                throw new UnsupportedOperationException();
            }
            this.f4316 = t;
            this.f4315 = 1;
            for (zzalm zzalm : this.f4317) {
                zzalm.f8297.m9691(t);
            }
            this.f4317.clear();
        }
    }
}
