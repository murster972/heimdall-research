package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzuh {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final List<String> f5402;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final List<String> f5403;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final List<String> f5404;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final String f5405;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final String f5406;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final List<String> f5407;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String f5408;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final long f5409;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f5410;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final List<String> f5411;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final String f5412;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final List<String> f5413;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f5414;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f5415;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f5416;

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<String> f5417;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f5418;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final String f5419;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final List<String> f5420;

    public zzuh(String str, String str2, List<String> list, String str3, String str4, List<String> list2, List<String> list3, List<String> list4, String str5, String str6, List<String> list5, List<String> list6, String str7, String str8, String str9, List<String> list7, String str10, List<String> list8, String str11, long j) {
        this.f5418 = str;
        this.f5415 = null;
        this.f5417 = list;
        this.f5416 = null;
        this.f5414 = null;
        this.f5402 = list2;
        this.f5403 = list3;
        this.f5404 = list4;
        this.f5412 = str5;
        this.f5413 = list5;
        this.f5407 = list6;
        this.f5405 = null;
        this.f5406 = null;
        this.f5419 = null;
        this.f5420 = null;
        this.f5408 = null;
        this.f5411 = list8;
        this.f5410 = null;
        this.f5409 = -1;
    }

    public zzuh(JSONObject jSONObject) throws JSONException {
        List<String> list;
        this.f5415 = jSONObject.optString("id");
        JSONArray jSONArray = jSONObject.getJSONArray("adapters");
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        this.f5417 = Collections.unmodifiableList(arrayList);
        this.f5416 = jSONObject.optString("allocation_id", (String) null);
        zzbs.zzez();
        this.f5402 = zzuq.m5895(jSONObject, "clickurl");
        zzbs.zzez();
        this.f5403 = zzuq.m5895(jSONObject, "imp_urls");
        zzbs.zzez();
        this.f5411 = zzuq.m5895(jSONObject, "fill_urls");
        zzbs.zzez();
        this.f5413 = zzuq.m5895(jSONObject, "video_start_urls");
        zzbs.zzez();
        this.f5407 = zzuq.m5895(jSONObject, "video_complete_urls");
        JSONObject optJSONObject = jSONObject.optJSONObject("ad");
        if (optJSONObject != null) {
            zzbs.zzez();
            list = zzuq.m5895(optJSONObject, "manual_impression_urls");
        } else {
            list = null;
        }
        this.f5404 = list;
        this.f5418 = optJSONObject != null ? optJSONObject.toString() : null;
        JSONObject optJSONObject2 = jSONObject.optJSONObject("data");
        this.f5412 = optJSONObject2 != null ? optJSONObject2.toString() : null;
        this.f5414 = optJSONObject2 != null ? optJSONObject2.optString("class_name") : null;
        this.f5405 = jSONObject.optString("html_template", (String) null);
        this.f5406 = jSONObject.optString("ad_base_url", (String) null);
        JSONObject optJSONObject3 = jSONObject.optJSONObject("assets");
        this.f5419 = optJSONObject3 != null ? optJSONObject3.toString() : null;
        zzbs.zzez();
        this.f5420 = zzuq.m5895(jSONObject, "template_ids");
        JSONObject optJSONObject4 = jSONObject.optJSONObject("ad_loader_options");
        this.f5408 = optJSONObject4 != null ? optJSONObject4.toString() : null;
        this.f5410 = jSONObject.optString(OAuth.OAUTH_RESPONSE_TYPE, (String) null);
        this.f5409 = jSONObject.optLong("ad_network_timeout_millis", -1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m5878() {
        return "native".equalsIgnoreCase(this.f5410);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5879() {
        return PubnativeAsset.BANNER.equalsIgnoreCase(this.f5410);
    }
}
