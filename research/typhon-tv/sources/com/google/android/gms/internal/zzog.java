package com.google.android.gms.internal;

import android.graphics.Color;
import java.util.ArrayList;
import java.util.List;

@zzzv
public final class zzog extends zzpn {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int f5202;

    /* renamed from: 麤  reason: contains not printable characters */
    private static int f5203 = f5205;

    /* renamed from: 齉  reason: contains not printable characters */
    private static int f5204;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f5205 = Color.rgb(12, 174, 206);

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<zzoi> f5206 = new ArrayList();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<zzpq> f5207 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f5208;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final boolean f5209;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f5210;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f5211;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f5212;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f5213;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f5214;

    static {
        int rgb = Color.rgb(204, 204, 204);
        f5202 = rgb;
        f5204 = rgb;
    }

    public zzog(String str, List<zzoi> list, Integer num, Integer num2, Integer num3, int i, int i2, boolean z) {
        this.f5214 = str;
        if (list != null) {
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= list.size()) {
                    break;
                }
                zzoi zzoi = list.get(i4);
                this.f5206.add(zzoi);
                this.f5207.add(zzoi);
                i3 = i4 + 1;
            }
        }
        this.f5208 = num != null ? num.intValue() : f5204;
        this.f5211 = num2 != null ? num2.intValue() : f5203;
        this.f5212 = num3 != null ? num3.intValue() : 12;
        this.f5213 = i;
        this.f5210 = i2;
        this.f5209 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int m5641() {
        return this.f5212;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int m5642() {
        return this.f5213;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int m5643() {
        return this.f5210;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m5644() {
        return this.f5209;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m5645() {
        return this.f5211;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<zzpq> m5646() {
        return this.f5207;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m5647() {
        return this.f5208;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<zzoi> m5648() {
        return this.f5206;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5649() {
        return this.f5214;
    }
}
