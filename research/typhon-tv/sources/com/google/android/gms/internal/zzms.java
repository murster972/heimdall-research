package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzms implements Parcelable.Creator<zzmr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 3:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 4:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new zzmr(z3, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzmr[i];
    }
}
