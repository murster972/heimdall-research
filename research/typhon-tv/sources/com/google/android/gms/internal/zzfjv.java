package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfjv {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final String[] f10552 = new String[0];

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final byte[][] f10553 = new byte[0][];

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final byte[] f10554 = new byte[0];

    /* renamed from: ˈ  reason: contains not printable characters */
    private static int f10555 = 26;

    /* renamed from: ˑ  reason: contains not printable characters */
    private static int f10556 = 11;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static int f10557 = 12;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static int f10558 = 16;

    /* renamed from: 连任  reason: contains not printable characters */
    public static final boolean[] f10559 = new boolean[0];

    /* renamed from: 靐  reason: contains not printable characters */
    public static final long[] f10560 = new long[0];

    /* renamed from: 麤  reason: contains not printable characters */
    public static final double[] f10561 = new double[0];

    /* renamed from: 齉  reason: contains not printable characters */
    public static final float[] f10562 = new float[0];

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int[] f10563 = new int[0];

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int m12883(zzfjj zzfjj, int i) throws IOException {
        int i2 = 1;
        int r1 = zzfjj.m12787();
        zzfjj.m12795(i);
        while (zzfjj.m12800() == i) {
            zzfjj.m12795(i);
            i2++;
        }
        zzfjj.m12794(r1, i);
        return i2;
    }
}
