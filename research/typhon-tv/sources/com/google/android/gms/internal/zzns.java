package com.google.android.gms.internal;

@zzzv
public final class zzns {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f5183;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzns f5184;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f5185;

    public zzns(long j, String str, zzns zzns) {
        this.f5185 = j;
        this.f5183 = str;
        this.f5184 = zzns;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5616() {
        return this.f5183;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzns m5617() {
        return this.f5184;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m5618() {
        return this.f5185;
    }
}
