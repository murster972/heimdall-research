package com.google.android.gms.internal;

abstract class zzcjl extends zzcjk {

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f9488;

    zzcjl(zzcim zzcim) {
        super(zzcim);
        this.f9487.m11058(this);
    }

    /* access modifiers changed from: protected */
    public void t_() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final boolean m11113() {
        return this.f9488;
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public final void m11114() {
        if (this.f9488) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!m11117()) {
            this.f9487.m11033();
            this.f9488 = true;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˋˋ  reason: contains not printable characters */
    public final void m11115() {
        if (!m11113()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public final void m11116() {
        if (this.f9488) {
            throw new IllegalStateException("Can't initialize twice");
        }
        t_();
        this.f9487.m11033();
        this.f9488 = true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public abstract boolean m11117();
}
