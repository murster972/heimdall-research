package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class zzchf extends zzev implements zzche {
    public zzchf() {
        attachInterface(this, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m10686((zzcha) zzew.m12304(parcel, zzcha.CREATOR), (zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                m10688((zzcln) zzew.m12304(parcel, zzcln.CREATOR), (zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                break;
            case 4:
                m10683((zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                m10687((zzcha) zzew.m12304(parcel, zzcha.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                m10674((zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                break;
            case 7:
                List<zzcln> r0 = m10677((zzcgi) zzew.m12304(parcel, zzcgi.CREATOR), zzew.m12308(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(r0);
                break;
            case 9:
                byte[] r02 = m10689((zzcha) zzew.m12304(parcel, zzcha.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(r02);
                break;
            case 10:
                m10682(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                String r03 = m10676((zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(r03);
                break;
            case 12:
                m10685((zzcgl) zzew.m12304(parcel, zzcgl.CREATOR), (zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                m10684((zzcgl) zzew.m12304(parcel, zzcgl.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                List<zzcln> r04 = m10681(parcel.readString(), parcel.readString(), zzew.m12308(parcel), (zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(r04);
                break;
            case 15:
                List<zzcln> r05 = m10680(parcel.readString(), parcel.readString(), parcel.readString(), zzew.m12308(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(r05);
                break;
            case 16:
                List<zzcgl> r06 = m10678(parcel.readString(), parcel.readString(), (zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(r06);
                break;
            case 17:
                List<zzcgl> r07 = m10679(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(r07);
                break;
            case 18:
                m10675((zzcgi) zzew.m12304(parcel, zzcgi.CREATOR));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
