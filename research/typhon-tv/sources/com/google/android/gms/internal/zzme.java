package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzme extends zzkl {

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzmc f10797;

    private zzme(zzmc zzmc) {
        this.f10797 = zzmc;
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return null;
    }

    public final boolean isLoading() throws RemoteException {
        return false;
    }

    public final void zza(zzjj zzjj, int i) throws RemoteException {
        zzakb.m4795("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzajr.f4285.post(new zzmf(this));
    }

    public final String zzcp() throws RemoteException {
        return null;
    }

    public final void zzd(zzjj zzjj) throws RemoteException {
        zza(zzjj, 1);
    }
}
