package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;

public final class zzbft {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Api<Object> f8737 = new Api<>("Common.API", f8738, f8740);

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Api.zza<zzbgb, Object> f8738 = new zzbfu();

    /* renamed from: 齉  reason: contains not printable characters */
    public static final zzbfv f8739 = new zzbfw();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Api.zzf<zzbgb> f8740 = new Api.zzf<>();
}
