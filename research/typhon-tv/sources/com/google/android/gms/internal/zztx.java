package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.js.zzc;
import com.google.android.gms.ads.internal.js.zzn;

@zzzv
public final class zztx {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzaiq<zzc> f5395 = new zztz();

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzaiq<zzc> f5396 = new zzty();

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzn f5397;

    public zztx(Context context, zzakd zzakd, String str) {
        this.f5397 = new zzn(context, zzakd, str, f5396, f5395);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <I, O> zztp<I, O> m5871(String str, zzts<I> zzts, zztr<O> zztr) {
        return new zzua(this.f5397, str, zzts, zztr);
    }
}
