package com.google.android.gms.internal;

import android.content.SharedPreferences;
import org.json.JSONObject;

final class zzmz extends zzmx<Integer> {
    zzmz(int i, String str, Integer num) {
        super(i, str, num, (zzmy) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13150(SharedPreferences sharedPreferences) {
        return Integer.valueOf(sharedPreferences.getInt(m5586(), ((Integer) m5582()).intValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13151(JSONObject jSONObject) {
        return Integer.valueOf(jSONObject.optInt(m5586(), ((Integer) m5582()).intValue()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m13152(SharedPreferences.Editor editor, Object obj) {
        editor.putInt(m5586(), ((Integer) obj).intValue());
    }
}
