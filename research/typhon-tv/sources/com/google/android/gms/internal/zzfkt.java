package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfkt extends zzfjm<zzfkt> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile zzfkt[] f10617;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10618 = "";

    public zzfkt() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzfkt[] m12919() {
        if (f10617 == null) {
            synchronized (zzfjq.f10546) {
                if (f10617 == null) {
                    f10617 = new zzfkt[0];
                }
            }
        }
        return f10617;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12920() {
        int r0 = super.m12841();
        return (this.f10618 == null || this.f10618.equals("")) ? r0 : r0 + zzfjk.m12808(1, this.f10618);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12921(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10618 = zzfjj.m12791();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12922(zzfjk zzfjk) throws IOException {
        if (this.f10618 != null && !this.f10618.equals("")) {
            zzfjk.m12835(1, this.f10618);
        }
        super.m12842(zzfjk);
    }
}
