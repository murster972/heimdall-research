package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.zzbs;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzzv
@TargetApi(11)
public class zzaos extends zzani {
    public zzaos(zzanh zzanh, boolean z) {
        super(zzanh, z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final WebResourceResponse m5217(WebView webView, String str, Map<String, String> map) {
        if (!(webView instanceof zzanh)) {
            zzagf.m4791("Tried to intercept request from a WebView that wasn't an AdWebView.");
            return null;
        }
        zzanh zzanh = (zzanh) webView;
        if (this.f4469 != null) {
            this.f4469.m9566(str, map, 1);
        }
        if (!"mraid.js".equalsIgnoreCase(new File(str).getName())) {
            return super.shouldInterceptRequest(webView, str);
        }
        if (zzanh.m4980() != null) {
            zzanh.m4980().m5033();
        }
        String str2 = zzanh.m4983().m5227() ? (String) zzkb.m5481().m5595(zznh.f5146) : zzanh.m4987() ? (String) zzkb.m5481().m5595(zznh.f5144) : (String) zzkb.m5481().m5595(zznh.f5165);
        try {
            Context context = zzanh.getContext();
            String str3 = zzanh.m4985().f4297;
            HashMap hashMap = new HashMap();
            hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, zzbs.zzei().m4632(context, str3));
            hashMap.put("Cache-Control", "max-stale=3600");
            String str4 = (String) new zzaiv(context).m4714(str2, (Map<String, String>) hashMap).get(60, TimeUnit.SECONDS);
            if (str4 == null) {
                return null;
            }
            return new WebResourceResponse("application/javascript", "UTF-8", new ByteArrayInputStream(str4.getBytes("UTF-8")));
        } catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
            String valueOf = String.valueOf(e.getMessage());
            zzagf.m4791(valueOf.length() != 0 ? "Could not fetch MRAID JS. ".concat(valueOf) : new String("Could not fetch MRAID JS. "));
            return null;
        }
    }
}
