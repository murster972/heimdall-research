package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzqe extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    double m13250() throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    String m13251() throws RemoteException;

    /* renamed from: ʽ  reason: contains not printable characters */
    String m13252() throws RemoteException;

    /* renamed from: ʿ  reason: contains not printable characters */
    Bundle m13253() throws RemoteException;

    /* renamed from: ˊ  reason: contains not printable characters */
    String m13254() throws RemoteException;

    /* renamed from: ˋ  reason: contains not printable characters */
    zzpm m13255() throws RemoteException;

    /* renamed from: ˎ  reason: contains not printable characters */
    void m13256() throws RemoteException;

    /* renamed from: ˑ  reason: contains not printable characters */
    zzll m13257() throws RemoteException;

    /* renamed from: ٴ  reason: contains not printable characters */
    IObjectWrapper m13258() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    String m13259() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    List m13260() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m13261(Bundle bundle) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    zzpq m13262() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    String m13263() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13264(Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13265() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13266(Bundle bundle) throws RemoteException;

    /* renamed from: ﾞ  reason: contains not printable characters */
    IObjectWrapper m13267() throws RemoteException;
}
