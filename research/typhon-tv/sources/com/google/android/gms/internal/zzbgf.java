package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzbgf extends zzeu implements zzbge {
    zzbgf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10215(zzbgc zzbgc) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzbgc);
        m12299(1, v_);
    }
}
