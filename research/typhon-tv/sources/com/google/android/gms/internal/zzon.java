package com.google.android.gms.internal;

import android.os.Bundle;
import android.support.v4.util.SimpleArrayMap;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@zzzv
public final class zzon extends zzqn implements zzou {

    /* renamed from: ʻ  reason: contains not printable characters */
    private View f5250;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Object f5251 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public zzos f5252;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzll f5253;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f5254;

    /* renamed from: 麤  reason: contains not printable characters */
    private final SimpleArrayMap<String, String> f5255;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SimpleArrayMap<String, zzoi> f5256;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzog f5257;

    public zzon(String str, SimpleArrayMap<String, zzoi> simpleArrayMap, SimpleArrayMap<String, String> simpleArrayMap2, zzog zzog, zzll zzll, View view) {
        this.f5254 = str;
        this.f5256 = simpleArrayMap;
        this.f5255 = simpleArrayMap2;
        this.f5257 = zzog;
        this.f5253 = zzll;
        this.f5250 = view;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m5703() {
        zzahn.f4212.post(new zzop(this));
        this.f5253 = null;
        this.f5250 = null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzog m5704() {
        return this.f5257;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m5705() {
        return this.f5254;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m5706() {
        return "3";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final IObjectWrapper m5707() {
        return zzn.m9306(this.f5252.m13187().getApplicationContext());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final IObjectWrapper m5708() {
        return zzn.m9306(this.f5252);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzpq m5709(String str) {
        return this.f5256.get(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m5710() {
        synchronized (this.f5251) {
            if (this.f5252 == null) {
                zzakb.m4795("Attempt to perform recordImpression before ad initialized.");
            } else {
                this.f5252.m13199((View) null, (Map<String, WeakReference<View>>) null);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzll m5711() {
        return this.f5253;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m5712(String str) {
        synchronized (this.f5251) {
            if (this.f5252 == null) {
                zzakb.m4795("Attempt to call performClick before ad initialized.");
            } else {
                this.f5252.m13198((View) null, str, (Bundle) null, (Map<String, WeakReference<View>>) null, (View) null);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m5713(String str) {
        return this.f5255.get(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<String> m5714() {
        int i = 0;
        String[] strArr = new String[(this.f5256.size() + this.f5255.size())];
        int i2 = 0;
        for (int i3 = 0; i3 < this.f5256.size(); i3++) {
            strArr[i2] = this.f5256.keyAt(i3);
            i2++;
        }
        while (i < this.f5255.size()) {
            strArr[i2] = this.f5255.keyAt(i);
            i++;
            i2++;
        }
        return Arrays.asList(strArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5715(zzos zzos) {
        synchronized (this.f5251) {
            this.f5252 = zzos;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5716(IObjectWrapper iObjectWrapper) {
        if (this.f5252 == null) {
            zzakb.m4795("Attempt to call renderVideoInMediaView before ad initialized.");
            return false;
        } else if (this.f5250 == null) {
            return false;
        } else {
            zzoo zzoo = new zzoo(this);
            this.f5252.m13197((View) (FrameLayout) zzn.m9307(iObjectWrapper), (zzoq) zzoo);
            return true;
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final View m5717() {
        return this.f5250;
    }
}
