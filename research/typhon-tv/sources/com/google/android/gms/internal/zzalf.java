package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzzv
public class zzalf<T> implements zzakv<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzakw f4309 = new zzakw();

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f4310;

    /* renamed from: 靐  reason: contains not printable characters */
    private T f4311;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f4312;

    /* renamed from: 齉  reason: contains not printable characters */
    private Throwable f4313;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4314 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m4821() {
        return this.f4313 != null || this.f4312;
    }

    public boolean cancel(boolean z) {
        if (!z) {
            return false;
        }
        synchronized (this.f4314) {
            if (m4821()) {
                return false;
            }
            this.f4310 = true;
            this.f4312 = true;
            this.f4314.notifyAll();
            this.f4309.m4815();
            return true;
        }
    }

    public T get() throws CancellationException, ExecutionException, InterruptedException {
        T t;
        synchronized (this.f4314) {
            if (!m4821()) {
                try {
                    this.f4314.wait();
                } catch (InterruptedException e) {
                    throw e;
                }
            }
            if (this.f4313 != null) {
                throw new ExecutionException(this.f4313);
            } else if (this.f4310) {
                throw new CancellationException("SettableFuture was cancelled.");
            } else {
                t = this.f4311;
            }
        }
        return t;
    }

    public T get(long j, TimeUnit timeUnit) throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
        T t;
        synchronized (this.f4314) {
            if (!m4821()) {
                try {
                    long millis = timeUnit.toMillis(j);
                    if (millis != 0) {
                        this.f4314.wait(millis);
                    }
                } catch (InterruptedException e) {
                    throw e;
                }
            }
            if (this.f4313 != null) {
                throw new ExecutionException(this.f4313);
            } else if (!this.f4312) {
                throw new TimeoutException("SettableFuture timed out.");
            } else if (this.f4310) {
                throw new CancellationException("SettableFuture was cancelled.");
            } else {
                t = this.f4311;
            }
        }
        return t;
    }

    public boolean isCancelled() {
        boolean z;
        synchronized (this.f4314) {
            z = this.f4310;
        }
        return z;
    }

    public boolean isDone() {
        boolean r0;
        synchronized (this.f4314) {
            r0 = m4821();
        }
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4822(T t) {
        synchronized (this.f4314) {
            if (!this.f4310) {
                if (m4821()) {
                    zzbs.zzem().m4505((Throwable) new IllegalStateException("Provided SettableFuture with multiple values."), "SettableFuture.set");
                    return;
                }
                this.f4312 = true;
                this.f4311 = t;
                this.f4314.notifyAll();
                this.f4309.m4815();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4823(Runnable runnable, Executor executor) {
        this.f4309.m4816(runnable, executor);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4824(Throwable th) {
        synchronized (this.f4314) {
            if (!this.f4310) {
                if (m4821()) {
                    zzbs.zzem().m4505((Throwable) new IllegalStateException("Provided SettableFuture with multiple values."), "SettableFuture.setException");
                    return;
                }
                this.f4313 = th;
                this.f4314.notifyAll();
                this.f4309.m4815();
            }
        }
    }
}
