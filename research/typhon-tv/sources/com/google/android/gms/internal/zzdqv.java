package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

final class zzdqv {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdur m11746(zzdso zzdso) throws GeneralSecurityException {
        switch (zzdqw.f9949[zzdso.ordinal()]) {
            case 1:
                return zzdur.NIST_P256;
            case 2:
                return zzdur.NIST_P384;
            case 3:
                return zzdur.NIST_P521;
            default:
                String valueOf = String.valueOf(zzdso);
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(valueOf).length() + 20).append("unknown curve type: ").append(valueOf).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdus m11747(zzdsa zzdsa) throws GeneralSecurityException {
        switch (zzdqw.f9950[zzdsa.ordinal()]) {
            case 1:
                return zzdus.UNCOMPRESSED;
            case 2:
                return zzdus.COMPRESSED;
            default:
                String valueOf = String.valueOf(zzdsa);
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(valueOf).length() + 22).append("unknown point format: ").append(valueOf).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m11748(zzdsq zzdsq) throws NoSuchAlgorithmException {
        switch (zzdqw.f9951[zzdsq.ordinal()]) {
            case 1:
                return "HmacSha1";
            case 2:
                return "HmacSha256";
            case 3:
                return "HmacSha512";
            default:
                String valueOf = String.valueOf(zzdsq);
                throw new NoSuchAlgorithmException(new StringBuilder(String.valueOf(valueOf).length() + 27).append("hash unsupported for HMAC: ").append(valueOf).toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m11749(zzdsg zzdsg) throws GeneralSecurityException {
        zzdup.m12212(m11746(zzdsg.m11920().m11969()));
        m11748(zzdsg.m11920().m11966());
        if (zzdsg.m11919() == zzdsa.UNKNOWN_FORMAT) {
            throw new GeneralSecurityException("unknown EC point format");
        }
        zzdqe.m11676(zzdsg.m11917().m11906());
    }
}
