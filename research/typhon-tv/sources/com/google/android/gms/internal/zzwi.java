package com.google.android.gms.internal;

import android.os.RemoteException;

final class zzwi implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvx f10945;

    zzwi(zzvx zzvx) {
        this.f10945 = zzvx;
    }

    public final void run() {
        try {
            this.f10945.f5502.m13512();
        } catch (RemoteException e) {
            zzakb.m4796("Could not call onAdLeftApplication.", e);
        }
    }
}
