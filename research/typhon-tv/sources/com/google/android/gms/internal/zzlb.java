package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzlb extends zzev implements zzla {
    public zzlb() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IClientApi");
    }

    public static zzla asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IClientApi");
        return queryLocalInterface instanceof zzla ? (zzla) queryLocalInterface : new zzlc(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                zzks createBannerAdManager = createBannerAdManager(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), (zzjn) zzew.m12304(parcel, zzjn.CREATOR), parcel.readString(), zzuy.m13449(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createBannerAdManager);
                break;
            case 2:
                zzks createInterstitialAdManager = createInterstitialAdManager(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), (zzjn) zzew.m12304(parcel, zzjn.CREATOR), parcel.readString(), zzuy.m13449(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createInterstitialAdManager);
                break;
            case 3:
                zzkn createAdLoaderBuilder = createAdLoaderBuilder(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readString(), zzuy.m13449(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createAdLoaderBuilder);
                break;
            case 4:
                zzlg mobileAdsSettingsManager = getMobileAdsSettingsManager(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) mobileAdsSettingsManager);
                break;
            case 5:
                zzpu createNativeAdViewDelegate = createNativeAdViewDelegate(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createNativeAdViewDelegate);
                break;
            case 6:
                zzadk createRewardedVideoAd = createRewardedVideoAd(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), zzuy.m13449(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createRewardedVideoAd);
                break;
            case 7:
                zzxo createInAppPurchaseManager = createInAppPurchaseManager(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createInAppPurchaseManager);
                break;
            case 8:
                zzxe createAdOverlay = createAdOverlay(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createAdOverlay);
                break;
            case 9:
                zzlg mobileAdsSettingsManagerWithClientJarVersion = getMobileAdsSettingsManagerWithClientJarVersion(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) mobileAdsSettingsManagerWithClientJarVersion);
                break;
            case 10:
                zzks createSearchAdManager = createSearchAdManager(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), (zzjn) zzew.m12304(parcel, zzjn.CREATOR), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createSearchAdManager);
                break;
            case 11:
                zzpz createNativeAdViewHolderDelegate = createNativeAdViewHolderDelegate(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), IObjectWrapper.zza.m9305(parcel.readStrongBinder()), IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) createNativeAdViewHolderDelegate);
                break;
            default:
                return false;
        }
        return true;
    }
}
