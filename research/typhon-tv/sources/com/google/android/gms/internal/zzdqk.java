package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;

final class zzdqk implements zzdpw<zzdpp> {
    zzdqk() {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static zzdpp m11704(zzfes zzfes) throws GeneralSecurityException {
        try {
            return new zzdtz(zzdrs.m11861(zzfes).m11867().toByteArray());
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11705(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11706((zzfhe) zzdru.m11876(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized AesGcmKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11706(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdru)) {
            throw new GeneralSecurityException("expected AesGcmKeyFormat proto");
        }
        zzdru zzdru = (zzdru) zzfhe;
        zzdvk.m12237(zzdru.m11877());
        return zzdrs.m11860().m11874(zzfes.zzaz(zzdvi.m12235(zzdru.m11877()))).m11873(zzdru.m11879()).m11872(0).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11707(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.AesGcmKey").m12022(((zzdrs) m11705(zzfes)).m12361()).m12021(zzdsy.zzb.SYMMETRIC).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11708(zzfes zzfes) throws GeneralSecurityException {
        return m11704(zzfes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11709(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdrs)) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
        zzdrs zzdrs = (zzdrs) zzfhe;
        zzdvk.m12238(zzdrs.m11869(), 0);
        zzdvk.m12237(zzdrs.m11867().size());
        return new zzdtz(zzdrs.m11867().toByteArray());
    }
}
