package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.Mac;

public final class zzdvg implements zzdqa {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f10227;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Key f10228;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f10229;

    /* renamed from: 龘  reason: contains not printable characters */
    private Mac f10230;

    public zzdvg(String str, Key key, int i) throws GeneralSecurityException {
        this.f10229 = str;
        this.f10227 = i;
        this.f10228 = key;
        this.f10230 = zzduu.f10218.m12217(str);
        this.f10230.init(key);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m12230(byte[] bArr) throws GeneralSecurityException {
        Mac r0;
        try {
            r0 = (Mac) this.f10230.clone();
        } catch (CloneNotSupportedException e) {
            r0 = zzduu.f10218.m12217(this.f10229);
            r0.init(this.f10228);
        }
        r0.update(bArr);
        byte[] bArr2 = new byte[this.f10227];
        System.arraycopy(r0.doFinal(), 0, bArr2, 0, this.f10227);
        return bArr2;
    }
}
