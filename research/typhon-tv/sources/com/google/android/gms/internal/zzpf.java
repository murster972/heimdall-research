package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzpf implements Parcelable.Creator<zzpe> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        zzmr zzmr = null;
        int i = 0;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 3:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 6:
                    zzmr = (zzmr) zzbfn.m10171(parcel, readInt, zzmr.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new zzpe(i3, z2, i2, z, i, zzmr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzpe[i];
    }
}
