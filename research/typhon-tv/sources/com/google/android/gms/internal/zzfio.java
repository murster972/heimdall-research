package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;
import java.util.Arrays;

public final class zzfio {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzfio f10486 = new zzfio(0, new int[0], new Object[0], false);

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f10487;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f10488;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f10489;

    /* renamed from: 麤  reason: contains not printable characters */
    private Object[] f10490;

    /* renamed from: 齉  reason: contains not printable characters */
    private int[] f10491;

    private zzfio() {
        this(0, new int[8], new Object[8], true);
    }

    private zzfio(int i, int[] iArr, Object[] objArr, boolean z) {
        this.f10488 = -1;
        this.f10489 = i;
        this.f10491 = iArr;
        this.f10490 = objArr;
        this.f10487 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m12711() {
        if (!this.f10487) {
            throw new UnsupportedOperationException();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static zzfio m12712() {
        return new zzfio();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzfio m12713() {
        return f10486;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzfio m12714(zzfio zzfio, zzfio zzfio2) {
        int i = zzfio.f10489 + zzfio2.f10489;
        int[] copyOf = Arrays.copyOf(zzfio.f10491, i);
        System.arraycopy(zzfio2.f10491, 0, copyOf, zzfio.f10489, zzfio2.f10489);
        Object[] copyOf2 = Arrays.copyOf(zzfio.f10490, i);
        System.arraycopy(zzfio2.f10490, 0, copyOf2, zzfio.f10489, zzfio2.f10489);
        return new zzfio(i, copyOf, copyOf2, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m12715(int i, Object obj) {
        m12711();
        if (this.f10489 == this.f10491.length) {
            int i2 = (this.f10489 < 4 ? 8 : this.f10489 >> 1) + this.f10489;
            this.f10491 = Arrays.copyOf(this.f10491, i2);
            this.f10490 = Arrays.copyOf(this.f10490, i2);
        }
        this.f10491[this.f10489] = i;
        this.f10490[this.f10489] = obj;
        this.f10489++;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof zzfio)) {
            return false;
        }
        zzfio zzfio = (zzfio) obj;
        if (this.f10489 == zzfio.f10489) {
            int[] iArr = this.f10491;
            int[] iArr2 = zzfio.f10491;
            int i = this.f10489;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.f10490;
                Object[] objArr2 = zzfio.f10490;
                int i3 = this.f10489;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int hashCode() {
        return ((((this.f10489 + 527) * 31) + Arrays.hashCode(this.f10491)) * 31) + Arrays.deepHashCode(this.f10490);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m12716() {
        int r0;
        int i = this.f10488;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f10489; i2++) {
                int i3 = this.f10491[i2];
                int i4 = i3 >>> 3;
                switch (i3 & 7) {
                    case 0:
                        r0 = zzffg.m5260(i4, ((Long) this.f10490[i2]).longValue());
                        break;
                    case 1:
                        r0 = zzffg.m5256(i4, ((Long) this.f10490[i2]).longValue());
                        break;
                    case 2:
                        r0 = zzffg.m5261(i4, (zzfes) this.f10490[i2]);
                        break;
                    case 3:
                        r0 = ((zzfio) this.f10490[i2]).m12716() + (zzffg.m5255(i4) << 1);
                        break;
                    case 5:
                        r0 = zzffg.m5233(i4, ((Integer) this.f10490[i2]).intValue());
                        break;
                    default:
                        throw new IllegalStateException(zzfge.m12585());
                }
                i += r0;
            }
            this.f10488 = i;
        }
        return i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12717() {
        int i = this.f10488;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f10489; i2++) {
                i += zzffg.m5257(this.f10491[i2] >>> 3, (zzfes) this.f10490[i2]);
            }
            this.f10488 = i;
        }
        return i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m12718() {
        this.f10487 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12719(zzffg zzffg) throws IOException {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f10489) {
                int i3 = this.f10491[i2];
                int i4 = i3 >>> 3;
                switch (i3 & 7) {
                    case 0:
                        zzffg.m5287(i4, ((Long) this.f10490[i2]).longValue());
                        break;
                    case 1:
                        zzffg.m5275(i4, ((Long) this.f10490[i2]).longValue());
                        break;
                    case 2:
                        zzffg.m5288(i4, (zzfes) this.f10490[i2]);
                        break;
                    case 3:
                        zzffg.m5286(i4, 3);
                        ((zzfio) this.f10490[i2]).m12719(zzffg);
                        zzffg.m5286(i4, 4);
                        break;
                    case 5:
                        zzffg.m5279(i4, ((Integer) this.f10490[i2]).intValue());
                        break;
                    default:
                        throw zzfge.m12585();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12720(zzfji zzfji) {
        if (zzfji.m12778() == zzffu.zzg.f10410) {
            for (int i = this.f10489 - 1; i >= 0; i--) {
                zzfji.m12779(this.f10491[i] >>> 3, this.f10490[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.f10489; i2++) {
            zzfji.m12779(this.f10491[i2] >>> 3, this.f10490[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12721(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.f10489; i2++) {
            zzfhh.m12633(sb, i, String.valueOf(this.f10491[i2] >>> 3), this.f10490[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12722(int i, zzffb zzffb) throws IOException {
        int r3;
        m12711();
        int i2 = i >>> 3;
        switch (i & 7) {
            case 0:
                m12715(i, (Object) Long.valueOf(zzffb.m12410()));
                return true;
            case 1:
                m12715(i, (Object) Long.valueOf(zzffb.m12413()));
                return true;
            case 2:
                m12715(i, (Object) zzffb.m12401());
                return true;
            case 3:
                zzfio zzfio = new zzfio();
                do {
                    r3 = zzffb.m12415();
                    if (r3 == 0 || !zzfio.m12722(r3, zzffb)) {
                        zzffb.m12417((i2 << 3) | 4);
                        m12715(i, (Object) zzfio);
                        return true;
                    }
                    r3 = zzffb.m12415();
                    zzffb.m12417((i2 << 3) | 4);
                    m12715(i, (Object) zzfio);
                    return true;
                } while (!zzfio.m12722(r3, zzffb));
                zzffb.m12417((i2 << 3) | 4);
                m12715(i, (Object) zzfio);
                return true;
            case 4:
                return false;
            case 5:
                m12715(i, (Object) Integer.valueOf(zzffb.m12411()));
                return true;
            default:
                throw zzfge.m12585();
        }
    }
}
