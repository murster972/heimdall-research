package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfjm;
import java.io.IOException;

public abstract class zzfjm<M extends zzfjm<M>> extends zzfjs {

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    protected zzfjo f10533;

    /* renamed from: 麤  reason: contains not printable characters */
    public /* synthetic */ zzfjs m12839() throws CloneNotSupportedException {
        return (zzfjm) clone();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public M clone() throws CloneNotSupportedException {
        M m = (zzfjm) super.clone();
        zzfjq.m12861(this, (zzfjm) m);
        return m;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m12841() {
        int i = 0;
        if (this.f10533 == null) {
            return 0;
        }
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= this.f10533.m12850()) {
                return i3;
            }
            i = this.f10533.m12848(i2).m12855() + i3;
            i2++;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m12842(zzfjk zzfjk) throws IOException {
        if (this.f10533 != null) {
            for (int i = 0; i < this.f10533.m12850(); i++) {
                this.f10533.m12848(i).m12856(zzfjk);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m12843(zzfjj zzfjj, int i) throws IOException {
        int r0 = zzfjj.m12787();
        if (!zzfjj.m12795(i)) {
            return false;
        }
        int i2 = i >>> 3;
        zzfju zzfju = new zzfju(i, zzfjj.m12803(r0, zzfjj.m12787() - r0));
        zzfjp zzfjp = null;
        if (this.f10533 == null) {
            this.f10533 = new zzfjo();
        } else {
            zzfjp = this.f10533.m12851(i2);
        }
        if (zzfjp == null) {
            zzfjp = new zzfjp();
            this.f10533.m12852(i2, zzfjp);
        }
        zzfjp.m12857(zzfju);
        return true;
    }
}
