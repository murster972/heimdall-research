package com.google.android.gms.internal;

import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.internal.zzbs;
import java.lang.ref.WeakReference;

@zzzv
final class zzalo extends zzalq implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<ViewTreeObserver.OnGlobalLayoutListener> f4319;

    public zzalo(View view, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        super(view);
        this.f4319 = new WeakReference<>(onGlobalLayoutListener);
    }

    public final void onGlobalLayout() {
        ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = (ViewTreeObserver.OnGlobalLayoutListener) this.f4319.get();
        if (onGlobalLayoutListener != null) {
            onGlobalLayoutListener.onGlobalLayout();
        } else {
            m4833();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4828(ViewTreeObserver viewTreeObserver) {
        zzbs.zzek().m4660(viewTreeObserver, (ViewTreeObserver.OnGlobalLayoutListener) this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4829(ViewTreeObserver viewTreeObserver) {
        viewTreeObserver.addOnGlobalLayoutListener(this);
    }
}
