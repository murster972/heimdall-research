package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeContentAd;

@zzzv
public final class zzrl extends zzqu {

    /* renamed from: 龘  reason: contains not printable characters */
    private final NativeContentAd.OnContentAdLoadedListener f5334;

    public zzrl(NativeContentAd.OnContentAdLoadedListener onContentAdLoadedListener) {
        this.f5334 = onContentAdLoadedListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5803(zzqi zzqi) {
        this.f5334.onContentAdLoaded(new zzql(zzqi));
    }
}
