package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.js.zza;
import com.google.android.gms.ads.internal.js.zzm;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzv;
import java.util.Map;
import org.json.JSONObject;

@zzzv
public interface zzanh extends zza, zzm, zzbl, zzamp, zzaog, zzaoh, zzaou, zzaow, zzaox, zzaoy, zzgt {
    void destroy();

    Context getContext();

    int getHeight();

    ViewGroup.LayoutParams getLayoutParams();

    void getLocationOnScreen(int[] iArr);

    ViewParent getParent();

    int getWidth();

    void loadData(String str, String str2, String str3);

    void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5);

    void loadUrl(String str);

    void measure(int i, int i2);

    void onPause();

    void onResume();

    void setBackgroundColor(int i);

    void setOnClickListener(View.OnClickListener onClickListener);

    void setOnTouchListener(View.OnTouchListener onTouchListener);

    void setWebChromeClient(WebChromeClient webChromeClient);

    void setWebViewClient(WebViewClient webViewClient);

    void stopLoading();

    void zza(String str, Map<String, ?> map);

    void zza(String str, JSONObject jSONObject);

    void zzb(String str, JSONObject jSONObject);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    zzoq m4971();

    /* renamed from: ʼ  reason: contains not printable characters */
    String m4972();

    /* renamed from: ʼʼ  reason: contains not printable characters */
    void m4973();

    /* renamed from: ʽ  reason: contains not printable characters */
    zznt m4974();

    /* renamed from: ʽʽ  reason: contains not printable characters */
    void m4975();

    /* renamed from: ʾ  reason: contains not printable characters */
    void m4976();

    /* renamed from: ʿ  reason: contains not printable characters */
    void m4977();

    /* renamed from: ˆ  reason: contains not printable characters */
    String m4978();

    /* renamed from: ˈ  reason: contains not printable characters */
    WebView m4979();

    /* renamed from: ˉ  reason: contains not printable characters */
    zzani m4980();

    /* renamed from: ˊ  reason: contains not printable characters */
    zzd m4981();

    /* renamed from: ˋ  reason: contains not printable characters */
    zzd m4982();

    /* renamed from: ˎ  reason: contains not printable characters */
    zzapa m4983();

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean m4984();

    /* renamed from: ˑ  reason: contains not printable characters */
    zzakd m4985();

    /* renamed from: י  reason: contains not printable characters */
    zzcv m4986();

    /* renamed from: ـ  reason: contains not printable characters */
    boolean m4987();

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    void m4988();

    /* renamed from: ᴵ  reason: contains not printable characters */
    int m4989();

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    View.OnClickListener m4990();

    /* renamed from: ᵎ  reason: contains not printable characters */
    boolean m4991();

    /* renamed from: ᵔ  reason: contains not printable characters */
    void m4992();

    /* renamed from: ᵢ  reason: contains not printable characters */
    boolean m4993();

    /* renamed from: ⁱ  reason: contains not printable characters */
    boolean m4994();

    /* renamed from: 连任  reason: contains not printable characters */
    zzv m4995();

    /* renamed from: 连任  reason: contains not printable characters */
    void m4996(boolean z);

    /* renamed from: 靐  reason: contains not printable characters */
    zzaoa m4997();

    /* renamed from: 靐  reason: contains not printable characters */
    void m4998(int i);

    /* renamed from: 靐  reason: contains not printable characters */
    void m4999(zzd zzd);

    /* renamed from: 靐  reason: contains not printable characters */
    void m5000(String str);

    /* renamed from: 靐  reason: contains not printable characters */
    void m5001(String str, zzt<? super zzanh> zzt);

    /* renamed from: 靐  reason: contains not printable characters */
    void m5002(boolean z);

    /* renamed from: 麤  reason: contains not printable characters */
    Activity m5003();

    /* renamed from: 麤  reason: contains not printable characters */
    void m5004(boolean z);

    /* renamed from: 齉  reason: contains not printable characters */
    zzns m5005();

    /* renamed from: 齉  reason: contains not printable characters */
    void m5006(boolean z);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5007(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5008(Context context);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5009(zzd zzd);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5010(zzaoa zzaoa);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5011(zzapa zzapa);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5012(zzoq zzoq);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5013(String str);

    /* renamed from: 龘  reason: contains not printable characters */
    void m5014(String str, zzt<? super zzanh> zzt);

    /* renamed from: ﹳ  reason: contains not printable characters */
    boolean m5015();

    /* renamed from: ﹶ  reason: contains not printable characters */
    void m5016();

    /* renamed from: ﾞ  reason: contains not printable characters */
    Context m5017();

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    void m5018();
}
