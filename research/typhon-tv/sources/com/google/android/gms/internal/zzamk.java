package com.google.android.gms.internal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Display;
import android.view.WindowManager;

@zzzv
final class zzamk implements SensorEventListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float[] f4363;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Handler f4364;

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzamm f4365;

    /* renamed from: 连任  reason: contains not printable characters */
    private final float[] f4366 = new float[9];

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f4367 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private final float[] f4368 = new float[9];

    /* renamed from: 齉  reason: contains not printable characters */
    private final Display f4369;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SensorManager f4370;

    zzamk(Context context) {
        this.f4370 = (SensorManager) context.getSystemService("sensor");
        this.f4369 = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4899(int i, int i2) {
        float f = this.f4366[i];
        this.f4366[i] = this.f4366[i2];
        this.f4366[i2] = f;
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        if (fArr[0] != 0.0f || fArr[1] != 0.0f || fArr[2] != 0.0f) {
            synchronized (this.f4367) {
                if (this.f4363 == null) {
                    this.f4363 = new float[9];
                }
            }
            SensorManager.getRotationMatrixFromVector(this.f4368, fArr);
            switch (this.f4369.getRotation()) {
                case 1:
                    SensorManager.remapCoordinateSystem(this.f4368, 2, 129, this.f4366);
                    break;
                case 2:
                    SensorManager.remapCoordinateSystem(this.f4368, 129, 130, this.f4366);
                    break;
                case 3:
                    SensorManager.remapCoordinateSystem(this.f4368, 130, 1, this.f4366);
                    break;
                default:
                    System.arraycopy(this.f4368, 0, this.f4366, 0, 9);
                    break;
            }
            m4899(1, 3);
            m4899(2, 6);
            m4899(5, 7);
            synchronized (this.f4367) {
                System.arraycopy(this.f4366, 0, this.f4363, 0, 9);
            }
            if (this.f4365 != null) {
                this.f4365.m9703();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4900() {
        if (this.f4364 != null) {
            this.f4370.unregisterListener(this);
            this.f4364.post(new zzaml(this));
            this.f4364 = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4901() {
        if (this.f4364 == null) {
            Sensor defaultSensor = this.f4370.getDefaultSensor(11);
            if (defaultSensor == null) {
                zzagf.m4795("No Sensor of TYPE_ROTATION_VECTOR");
                return;
            }
            HandlerThread handlerThread = new HandlerThread("OrientationMonitor");
            handlerThread.start();
            this.f4364 = new Handler(handlerThread.getLooper());
            if (!this.f4370.registerListener(this, defaultSensor, 0, this.f4364)) {
                zzagf.m4795("SensorManager.registerListener failed.");
                m4900();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4902(zzamm zzamm) {
        this.f4365 = zzamm;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4903(float[] fArr) {
        boolean z = false;
        synchronized (this.f4367) {
            if (this.f4363 != null) {
                System.arraycopy(this.f4363, 0, fArr, 0, this.f4363.length);
                z = true;
            }
        }
        return z;
    }
}
