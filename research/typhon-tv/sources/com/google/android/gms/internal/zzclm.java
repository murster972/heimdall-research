package com.google.android.gms.internal;

import android.content.Intent;

final class zzclm extends zzcgs {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzcll f9645;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzclm(zzcll zzcll, zzcim zzcim) {
        super(zzcim);
        this.f9645 = zzcll;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11363() {
        this.f9645.m11354();
        this.f9645.m11096().m10848().m10849("Sending upload intent from DelayedRunnable");
        Intent className = new Intent().setClassName(this.f9645.m11097(), "com.google.android.gms.measurement.AppMeasurementReceiver");
        className.setAction("com.google.android.gms.measurement.UPLOAD");
        this.f9645.m11097().sendBroadcast(className);
    }
}
