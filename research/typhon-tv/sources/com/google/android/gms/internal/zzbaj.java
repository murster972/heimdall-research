package com.google.android.gms.internal;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.SeekBar;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import com.google.android.gms.common.util.zzq;

public final class zzbaj extends UIController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final SeekBar f8566;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SeekBar f8567;

    public zzbaj(SeekBar seekBar, SeekBar seekBar2) {
        this.f8567 = seekBar;
        this.f8566 = seekBar2;
        this.f8567.setClickable(false);
        if (zzq.m9268()) {
            this.f8567.setThumb((Drawable) null);
        } else {
            this.f8567.setThumb(new ColorDrawable(0));
        }
        this.f8567.setMax(1);
        this.f8567.setProgress(1);
        this.f8567.setOnTouchListener(new zzbak(this));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9891() {
        int i = 4;
        RemoteMediaClient r0 = m8226();
        if (r0 != null && r0.m4143()) {
            boolean r3 = r0.m4147();
            this.f8567.setVisibility(r3 ? 0 : 4);
            SeekBar seekBar = this.f8566;
            if (!r3) {
                i = 0;
            }
            seekBar.setVisibility(i);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9892() {
        m9891();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9893(CastSession castSession) {
        super.m8227(castSession);
        m9891();
    }
}
