package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

@zzzv
public final class zzabx extends zzbfm {
    public static final Parcelable.Creator<zzabx> CREATOR = new zzaby();

    /* renamed from: 龘  reason: contains not printable characters */
    String f3893;

    public zzabx(String str) {
        this.f3893 = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f3893, false);
        zzbfp.m10182(parcel, r0);
    }
}
