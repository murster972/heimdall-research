package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.location.LocationSettingsResult;

public interface zzcfb extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m10374(LocationSettingsResult locationSettingsResult) throws RemoteException;
}
