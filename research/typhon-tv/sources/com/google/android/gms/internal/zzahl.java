package com.google.android.gms.internal;

import java.util.concurrent.Future;

final class zzahl implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Future f8205;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzalf f8206;

    zzahl(zzalf zzalf, Future future) {
        this.f8206 = zzalf;
        this.f8205 = future;
    }

    public final void run() {
        if (this.f8206.isCancelled()) {
            this.f8205.cancel(true);
        }
    }
}
