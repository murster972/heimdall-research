package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.zzbs;

@TargetApi(21)
public final class zzaid extends zzaic {
    /* renamed from: ʻ  reason: contains not printable characters */
    public final int m9633() {
        return 16974374;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final CookieManager m9634(Context context) {
        if (m4640()) {
            return null;
        }
        try {
            return CookieManager.getInstance();
        } catch (Throwable th) {
            zzagf.m4793("Failed to obtain CookieManager.", th);
            zzbs.zzem().m4505(th, "ApiLevelUtil.getCookieManager");
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzani m9635(zzanh zzanh, boolean z) {
        return new zzaot(zzanh, z);
    }
}
