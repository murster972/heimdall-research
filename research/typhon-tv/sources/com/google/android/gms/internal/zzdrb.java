package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

public final class zzdrb {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzdtv f9959 = ((zzdtv) zzdtv.m12155().m12166("TINK_MAC_1_0_0").m12165(zzdpr.m11651("TinkMac", "Mac", "HmacKey", 0, true)).m12542());

    static {
        try {
            m11768();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m11768() throws GeneralSecurityException {
        zzdqe.m11680("TinkMac", (zzdpq) new zzdra());
    }
}
