package com.google.android.gms.internal;

import java.io.IOException;

public final class zzclv extends zzfjm<zzclv> {

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile zzclv[] f9683;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f9684 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public zzclt f9685 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f9686 = null;

    public zzclv() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzclv[] m11465() {
        if (f9683 == null) {
            synchronized (zzfjq.f10546) {
                if (f9683 == null) {
                    f9683 = new zzclv[0];
                }
            }
        }
        return f9683;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzclv)) {
            return false;
        }
        zzclv zzclv = (zzclv) obj;
        if (this.f9686 == null) {
            if (zzclv.f9686 != null) {
                return false;
            }
        } else if (!this.f9686.equals(zzclv.f9686)) {
            return false;
        }
        if (this.f9684 == null) {
            if (zzclv.f9684 != null) {
                return false;
            }
        } else if (!this.f9684.equals(zzclv.f9684)) {
            return false;
        }
        if (this.f9685 == null) {
            if (zzclv.f9685 != null) {
                return false;
            }
        } else if (!this.f9685.equals(zzclv.f9685)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzclv.f10533 == null || zzclv.f10533.m12849() : this.f10533.equals(zzclv.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.f9684 == null ? 0 : this.f9684.hashCode()) + (((this.f9686 == null ? 0 : this.f9686.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31);
        zzclt zzclt = this.f9685;
        int hashCode2 = ((zzclt == null ? 0 : zzclt.hashCode()) + (hashCode * 31)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode2 + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m11466() {
        int r0 = super.m12841();
        if (this.f9686 != null) {
            r0 += zzfjk.m12806(1, this.f9686.intValue());
        }
        if (this.f9684 != null) {
            r0 += zzfjk.m12808(2, this.f9684);
        }
        return this.f9685 != null ? r0 + zzfjk.m12807(3, (zzfjs) this.f9685) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m11467(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 8:
                    this.f9686 = Integer.valueOf(zzfjj.m12785());
                    continue;
                case 18:
                    this.f9684 = zzfjj.m12791();
                    continue;
                case 26:
                    if (this.f9685 == null) {
                        this.f9685 = new zzclt();
                    }
                    zzfjj.m12802((zzfjs) this.f9685);
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11468(zzfjk zzfjk) throws IOException {
        if (this.f9686 != null) {
            zzfjk.m12832(1, this.f9686.intValue());
        }
        if (this.f9684 != null) {
            zzfjk.m12835(2, this.f9684);
        }
        if (this.f9685 != null) {
            zzfjk.m12834(3, (zzfjs) this.f9685);
        }
        super.m12842(zzfjk);
    }
}
