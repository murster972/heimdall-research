package com.google.android.gms.internal;

final class zzchn implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzchm f9275;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f9276;

    zzchn(zzchm zzchm, String str) {
        this.f9275 = zzchm;
        this.f9276 = str;
    }

    public final void run() {
        zzchx r0 = this.f9275.f9487.m11040();
        if (!r0.m11113()) {
            this.f9275.m10842(6, "Persisted config not initialized. Not logging error/warn");
        } else {
            r0.f9318.m10907(this.f9276, 1);
        }
    }
}
