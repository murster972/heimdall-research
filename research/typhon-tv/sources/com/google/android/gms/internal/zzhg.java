package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;

@zzzv
public final class zzhg {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzhh f4687 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f4688 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f4689 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context m5377() {
        Context r0;
        synchronized (this.f4689) {
            r0 = this.f4687 != null ? this.f4687.m12993() : null;
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Activity m5378() {
        Activity r0;
        synchronized (this.f4689) {
            r0 = this.f4687 != null ? this.f4687.m12994() : null;
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5379(android.content.Context r5) {
        /*
            r4 = this;
            java.lang.Object r2 = r4.f4689
            monitor-enter(r2)
            boolean r0 = r4.f4688     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x0049
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f4911     // Catch:{ all -> 0x0033 }
            com.google.android.gms.internal.zznf r1 = com.google.android.gms.internal.zzkb.m5481()     // Catch:{ all -> 0x0033 }
            java.lang.Object r0 = r1.m5595(r0)     // Catch:{ all -> 0x0033 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0033 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x001b
            monitor-exit(r2)     // Catch:{ all -> 0x0033 }
        L_0x001a:
            return
        L_0x001b:
            r1 = 0
            android.content.Context r0 = r5.getApplicationContext()     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x0023
            r0 = r5
        L_0x0023:
            boolean r3 = r0 instanceof android.app.Application     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x004b
            android.app.Application r0 = (android.app.Application) r0     // Catch:{ all -> 0x0033 }
        L_0x0029:
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "Can not cast Context to Application"
            com.google.android.gms.internal.zzagf.m4791(r0)     // Catch:{ all -> 0x0033 }
            monitor-exit(r2)     // Catch:{ all -> 0x0033 }
            goto L_0x001a
        L_0x0033:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0033 }
            throw r0
        L_0x0036:
            com.google.android.gms.internal.zzhh r1 = r4.f4687     // Catch:{ all -> 0x0033 }
            if (r1 != 0) goto L_0x0041
            com.google.android.gms.internal.zzhh r1 = new com.google.android.gms.internal.zzhh     // Catch:{ all -> 0x0033 }
            r1.<init>()     // Catch:{ all -> 0x0033 }
            r4.f4687 = r1     // Catch:{ all -> 0x0033 }
        L_0x0041:
            com.google.android.gms.internal.zzhh r1 = r4.f4687     // Catch:{ all -> 0x0033 }
            r1.m12995((android.app.Application) r0, (android.content.Context) r5)     // Catch:{ all -> 0x0033 }
            r0 = 1
            r4.f4688 = r0     // Catch:{ all -> 0x0033 }
        L_0x0049:
            monitor-exit(r2)     // Catch:{ all -> 0x0033 }
            goto L_0x001a
        L_0x004b:
            r0 = r1
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhg.m5379(android.content.Context):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5380(zzhj zzhj) {
        synchronized (this.f4689) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f4911)).booleanValue()) {
                if (this.f4687 == null) {
                    this.f4687 = new zzhh();
                }
                this.f4687.m12996(zzhj);
            }
        }
    }
}
