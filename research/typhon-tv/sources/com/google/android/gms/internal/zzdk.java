package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzdk extends zzbt<Integer, Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    public Boolean f9866;

    /* renamed from: 齉  reason: contains not printable characters */
    public Boolean f9867;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f9868;

    public zzdk() {
    }

    public zzdk(String str) {
        m10294(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<Integer, Object> m11593() {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.put(0, this.f9868);
        hashMap.put(1, this.f9866);
        hashMap.put(2, this.f9867);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11594(String str) {
        HashMap r1 = m10292(str);
        if (r1 != null) {
            this.f9868 = (Long) r1.get(0);
            this.f9866 = (Boolean) r1.get(1);
            this.f9867 = (Boolean) r1.get(2);
        }
    }
}
