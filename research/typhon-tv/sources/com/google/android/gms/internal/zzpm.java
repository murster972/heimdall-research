package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface zzpm extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    List<zzpq> m13218() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13219() throws RemoteException;
}
