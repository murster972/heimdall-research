package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzba;
import com.google.android.gms.common.util.zzq;

@zzzv
public final class zzya {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzaif m6026(Context context, zza zza, zzafp zzafp, zzcv zzcv, zzanh zzanh, zzux zzux, zzyb zzyb, zznu zznu) {
        zzyg zzyd;
        zzaax zzaax = zzafp.f4133;
        if (zzaax.f3822) {
            zzyd = new zzyg(context, zzafp, zzux, zzyb, zznu, zzanh);
        } else if (zzaax.f3840 || (zza instanceof zzba)) {
            zzyd = (!zzaax.f3840 || !(zza instanceof zzba)) ? new zzyd(zzafp, zzyb) : new zzyi(context, (zzba) zza, zzafp, zzcv, zzyb, zznu);
        } else {
            zzyd = (!((Boolean) zzkb.m5481().m5595(zznh.f5142)).booleanValue() || !zzq.m9268() || zzq.m9265() || zzanh == null || !zzanh.m4983().m5227()) ? new zzyc(context, zzafp, zzanh, zzyb) : new zzyf(context, zzafp, zzanh, zzyb);
        }
        String valueOf = String.valueOf(zzyd.getClass().getName());
        zzagf.m4792(valueOf.length() != 0 ? "AdRenderer: ".concat(valueOf) : new String("AdRenderer: "));
        zzyd.m4674();
        return zzyd;
    }
}
