package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;

final class zzjw extends zzjr.zza<zzlg> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzjr f10773;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f10774;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjw(zzjr zzjr, Context context) {
        super();
        this.f10773 = zzjr;
        this.f10774 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13043() throws RemoteException {
        zzlg r0 = this.f10773.f4806.m5567(this.f10774);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10774, "mobile_ads_settings");
        return new zzmi();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13044(zzla zzla) throws RemoteException {
        return zzla.getMobileAdsSettingsManagerWithClientJarVersion(zzn.m9306(this.f10774), 11910000);
    }
}
