package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeAppInstallAd;

@zzzv
public final class zzrk extends zzqr {

    /* renamed from: 龘  reason: contains not printable characters */
    private final NativeAppInstallAd.OnAppInstallAdLoadedListener f5333;

    public zzrk(NativeAppInstallAd.OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
        this.f5333 = onAppInstallAdLoadedListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5802(zzqe zzqe) {
        this.f5333.onAppInstallAdLoaded(new zzqh(zzqe));
    }
}
