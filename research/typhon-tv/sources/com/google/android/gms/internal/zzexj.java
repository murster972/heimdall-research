package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzexj extends zzfjm<zzexj> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile zzexj[] f10318;

    /* renamed from: 靐  reason: contains not printable characters */
    public byte[] f10319 = zzfjv.f10554;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f10320 = "";

    public zzexj() {
        this.f10533 = null;
        this.f10549 = -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzexj[] m12338() {
        if (f10318 == null) {
            synchronized (zzfjq.f10546) {
                if (f10318 == null) {
                    f10318 = new zzexj[0];
                }
            }
        }
        return f10318;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzexj)) {
            return false;
        }
        zzexj zzexj = (zzexj) obj;
        if (this.f10320 == null) {
            if (zzexj.f10320 != null) {
                return false;
            }
        } else if (!this.f10320.equals(zzexj.f10320)) {
            return false;
        }
        if (!Arrays.equals(this.f10319, zzexj.f10319)) {
            return false;
        }
        return (this.f10533 == null || this.f10533.m12849()) ? zzexj.f10533 == null || zzexj.f10533.m12849() : this.f10533.equals(zzexj.f10533);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.f10320 == null ? 0 : this.f10320.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + Arrays.hashCode(this.f10319)) * 31;
        if (this.f10533 != null && !this.f10533.m12849()) {
            i = this.f10533.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12339() {
        int r0 = super.m12841();
        if (this.f10320 != null && !this.f10320.equals("")) {
            r0 += zzfjk.m12808(1, this.f10320);
        }
        return !Arrays.equals(this.f10319, zzfjv.f10554) ? r0 + zzfjk.m12809(2, this.f10319) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzfjs m12340(zzfjj zzfjj) throws IOException {
        while (true) {
            int r0 = zzfjj.m12800();
            switch (r0) {
                case 0:
                    break;
                case 10:
                    this.f10320 = zzfjj.m12791();
                    continue;
                case 18:
                    this.f10319 = zzfjj.m12784();
                    continue;
                default:
                    if (!super.m12843(zzfjj, r0)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12341(zzfjk zzfjk) throws IOException {
        if (this.f10320 != null && !this.f10320.equals("")) {
            zzfjk.m12835(1, this.f10320);
        }
        if (!Arrays.equals(this.f10319, zzfjv.f10554)) {
            zzfjk.m12837(2, this.f10319);
        }
        super.m12842(zzfjk);
    }
}
