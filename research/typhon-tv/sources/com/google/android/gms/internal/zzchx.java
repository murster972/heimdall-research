package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

final class zzchx extends zzcjl {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Pair<String, Long> f9300 = new Pair<>("", 0L);

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzcia f9301 = new zzcia(this, "last_delete_stale", 0);

    /* renamed from: ʼ  reason: contains not printable characters */
    public final zzcia f9302 = new zzcia(this, "midnight_offset", 0);

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzcia f9303 = new zzcia(this, "first_open_time", 0);

    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzcia f9304 = new zzcia(this, "last_pause_time", 0);

    /* renamed from: ʿ  reason: contains not printable characters */
    public final zzcia f9305 = new zzcia(this, "time_active", 0);

    /* renamed from: ˆ  reason: contains not printable characters */
    private long f9306;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final zzchz f9307 = new zzchz(this, "start_new_session", true);

    /* renamed from: ˉ  reason: contains not printable characters */
    private String f9308;

    /* renamed from: ˊ  reason: contains not printable characters */
    private SharedPreferences f9309;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f9310;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f9311;

    /* renamed from: ˏ  reason: contains not printable characters */
    private long f9312;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzcic f9313 = new zzcic(this, "app_instance_id", (String) null);

    /* renamed from: י  reason: contains not printable characters */
    private final Object f9314 = new Object();

    /* renamed from: ٴ  reason: contains not printable characters */
    public final zzcia f9315 = new zzcia(this, "time_before_start", 10000);

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final zzcia f9316 = new zzcia(this, "session_timeout", 1800000);

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzcia f9317 = new zzcia(this, "backoff", 0);

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzcib f9318 = new zzcib(this, "health_monitor", Math.max(0, zzchc.f9236.m10671().longValue()));

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzcia f9319 = new zzcia(this, "last_upload_attempt", 0);

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzcia f9320 = new zzcia(this, "last_upload", 0);

    /* renamed from: ﹶ  reason: contains not printable characters */
    public boolean f9321;

    zzchx(zzcim zzcim) {
        super(zzcim);
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹳ  reason: contains not printable characters */
    public final SharedPreferences m10885() {
        m11109();
        m11115();
        return this.f9309;
    }

    /* access modifiers changed from: protected */
    public final void t_() {
        this.f9309 = m11097().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.f9321 = this.f9309.getBoolean("has_been_opened", false);
        if (!this.f9321) {
            SharedPreferences.Editor edit = this.f9309.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public final boolean m10886() {
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public final String m10887() {
        m11109();
        return m10885().getString("gmp_app_id", (String) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final String m10888() {
        String str;
        synchronized (this.f9314) {
            str = Math.abs(m11105().m9241() - this.f9312) < 1000 ? this.f9308 : null;
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public final Boolean m10889() {
        m11109();
        if (!m10885().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(m10885().getBoolean("use_service", false));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵢ  reason: contains not printable characters */
    public final void m10890() {
        boolean z = true;
        m11109();
        m11096().m10848().m10849("Clearing collection preferences.");
        boolean contains = m10885().contains("measurement_enabled");
        if (contains) {
            z = m10896(true);
        }
        SharedPreferences.Editor edit = m10885().edit();
        edit.clear();
        edit.apply();
        if (contains) {
            m10893(z);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public final String m10891() {
        m11109();
        String string = m10885().getString("previous_os_version", (String) null);
        m11093().m11115();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = m10885().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10892(String str) {
        m11109();
        String str2 = (String) m10897(str).first;
        MessageDigest r1 = zzclq.m11365("MD5");
        if (r1 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, r1.digest(str2.getBytes()))});
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m10893(boolean z) {
        m11109();
        m11096().m10848().m10850("Setting measurementEnabled", Boolean.valueOf(z));
        SharedPreferences.Editor edit = m10885().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m10894(String str) {
        synchronized (this.f9314) {
            this.f9308 = str;
            this.f9312 = m11105().m9241();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10895(String str) {
        m11109();
        SharedPreferences.Editor edit = m10885().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m10896(boolean z) {
        m11109();
        return m10885().getBoolean("measurement_enabled", z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Pair<String, Boolean> m10897(String str) {
        m11109();
        long r0 = m11105().m9241();
        if (this.f9310 != null && r0 < this.f9306) {
            return new Pair<>(this.f9310, Boolean.valueOf(this.f9311));
        }
        this.f9306 = r0 + m11102().m10551(str, zzchc.f9238);
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(m11097());
            if (advertisingIdInfo != null) {
                this.f9310 = advertisingIdInfo.getId();
                this.f9311 = advertisingIdInfo.isLimitAdTrackingEnabled();
            }
            if (this.f9310 == null) {
                this.f9310 = "";
            }
        } catch (Throwable th) {
            m11096().m10845().m10850("Unable to get advertising id", th);
            this.f9310 = "";
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair<>(this.f9310, Boolean.valueOf(this.f9311));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10898(boolean z) {
        m11109();
        m11096().m10848().m10850("Setting useService", Boolean.valueOf(z));
        SharedPreferences.Editor edit = m10885().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }
}
