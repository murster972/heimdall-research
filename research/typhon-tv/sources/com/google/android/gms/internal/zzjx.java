package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzjr;

final class zzjx extends zzjr.zza<zzpu> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ FrameLayout f10775;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzjr f10776;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ Context f10777;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ FrameLayout f10778;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzjx(zzjr zzjr, FrameLayout frameLayout, FrameLayout frameLayout2, Context context) {
        super();
        this.f10776 = zzjr;
        this.f10778 = frameLayout;
        this.f10775 = frameLayout2;
        this.f10777 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13045() throws RemoteException {
        zzpu r0 = this.f10776.f4802.m5798(this.f10777, this.f10778, this.f10775);
        if (r0 != null) {
            return r0;
        }
        zzjr.m5475(this.f10777, "native_ad_view_delegate");
        return new zzmj();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13046(zzla zzla) throws RemoteException {
        return zzla.createNativeAdViewDelegate(zzn.m9306(this.f10778), zzn.m9306(this.f10775));
    }
}
