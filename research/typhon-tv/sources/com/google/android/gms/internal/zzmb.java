package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;

@zzzv
public final class zzmb extends zzp<zzlj> {
    public zzmb() {
        super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzlg m5567(Context context) {
        try {
            IBinder r2 = ((zzlj) m9308(context)).m13080(zzn.m9306(context), 11910000);
            if (r2 == null) {
                return null;
            }
            IInterface queryLocalInterface = r2.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            return queryLocalInterface instanceof zzlg ? (zzlg) queryLocalInterface : new zzli(r2);
        } catch (RemoteException e) {
            zzakb.m4796("Could not get remote MobileAdsSettingManager.", e);
            return null;
        } catch (zzq e2) {
            zzakb.m4796("Could not get remote MobileAdsSettingManager.", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m5568(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManagerCreator");
        return queryLocalInterface instanceof zzlj ? (zzlj) queryLocalInterface : new zzlk(iBinder);
    }
}
