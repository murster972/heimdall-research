package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsg;
import com.google.android.gms.internal.zzffu;
import java.io.IOException;

public final class zzdse extends zzffu<zzdse, zza> implements zzfhg {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static volatile zzfhk<zzdse> f10029;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final zzdse f10030;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzdsg f10031;

    public static final class zza extends zzffu.zza<zzdse, zza> implements zzfhg {
        private zza() {
            super(zzdse.f10030);
        }

        /* synthetic */ zza(zzdsf zzdsf) {
            this();
        }
    }

    static {
        zzdse zzdse = new zzdse();
        f10030 = zzdse;
        zzdse.m12535(zzffu.zzg.f10405, (Object) null, (Object) null);
        zzdse.f10394.m12718();
    }

    private zzdse() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdse m11910(zzfes zzfes) throws zzfge {
        return (zzdse) zzffu.m12526(f10030, zzfes);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m11911() {
        int i = this.f10395;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.f10031 != null) {
            i2 = zzffg.m5262(1, (zzfhe) this.f10031 == null ? zzdsg.m11916() : this.f10031) + 0;
        }
        int r0 = i2 + this.f10394.m12716();
        this.f10395 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzdsg m11912() {
        return this.f10031 == null ? zzdsg.m11916() : this.f10031;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Object m11913(int i, Object obj, Object obj2) {
        zzdsg.zza zza2;
        switch (zzdsf.f10032[i - 1]) {
            case 1:
                return new zzdse();
            case 2:
                return f10030;
            case 3:
                return null;
            case 4:
                return new zza((zzdsf) null);
            case 5:
                this.f10031 = (zzdsg) ((zzffu.zzh) obj).m12572(this.f10031, ((zzdse) obj2).f10031);
                return this;
            case 6:
                zzffb zzffb = (zzffb) obj;
                zzffm zzffm = (zzffm) obj2;
                if (zzffm != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int r0 = zzffb.m12415();
                            switch (r0) {
                                case 0:
                                    z = true;
                                    break;
                                case 10:
                                    if (this.f10031 != null) {
                                        zzdsg zzdsg = this.f10031;
                                        zzffu.zza zza3 = (zzffu.zza) zzdsg.m12535(zzffu.zzg.f10407, (Object) null, (Object) null);
                                        zza3.m12545(zzdsg);
                                        zza2 = (zzdsg.zza) zza3;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.f10031 = (zzdsg) zzffb.m12416(zzdsg.m11916(), zzffm);
                                    if (zza2 == null) {
                                        break;
                                    } else {
                                        zza2.m12545(this.f10031);
                                        this.f10031 = (zzdsg) zza2.m12543();
                                        break;
                                    }
                                default:
                                    if (m12537(r0, zzffb)) {
                                        break;
                                    } else {
                                        z = true;
                                        break;
                                    }
                            }
                        } catch (zzfge e) {
                            throw new RuntimeException(e.zzi(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfge(e2.getMessage()).zzi(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (f10029 == null) {
                    synchronized (zzdse.class) {
                        if (f10029 == null) {
                            f10029 = new zzffu.zzb(f10030);
                        }
                    }
                }
                return f10029;
            case 9:
                return (byte) 1;
            case 10:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
        return f10030;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11914(zzffg zzffg) throws IOException {
        if (this.f10031 != null) {
            zzffg.m5289(1, (zzfhe) this.f10031 == null ? zzdsg.m11916() : this.f10031);
        }
        this.f10394.m12719(zzffg);
    }
}
