package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.webkit.WebSettings;

@TargetApi(16)
public class zzaib extends zzahy {
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9627(Activity activity, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        Window window = activity.getWindow();
        if (window != null && window.getDecorView() != null && window.getDecorView().getViewTreeObserver() != null) {
            m4660(window.getDecorView().getViewTreeObserver(), onGlobalLayoutListener);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9628(View view, Drawable drawable) {
        view.setBackground(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9629(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m9630(Context context, WebSettings webSettings) {
        super.m9616(context, webSettings);
        webSettings.setAllowFileAccessFromFileURLs(false);
        webSettings.setAllowUniversalAccessFromFileURLs(false);
        return true;
    }
}
