package com.google.android.gms.internal;

import android.location.Location;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.zzt;

final class zzcfi extends zzt {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzci<LocationListener> f9079;

    zzcfi(zzci<LocationListener> zzci) {
        this.f9079 = zzci;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m10397() {
        this.f9079.m8854();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m10398(Location location) {
        this.f9079.m8855(new zzcfj(this, location));
    }
}
