package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.SettingsApi;

public final class zzcfv implements SettingsApi {
    /* renamed from: 龘  reason: contains not printable characters */
    public final PendingResult<LocationSettingsResult> m10421(GoogleApiClient googleApiClient, LocationSettingsRequest locationSettingsRequest) {
        return googleApiClient.zzd(new zzcfw(this, googleApiClient, locationSettingsRequest, (String) null));
    }
}
