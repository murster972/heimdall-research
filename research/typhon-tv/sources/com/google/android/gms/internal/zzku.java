package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzku extends zzeu implements zzks {
    zzku(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdManager");
    }

    public final void destroy() throws RemoteException {
        m12298(2, v_());
    }

    public final String getAdUnitId() throws RemoteException {
        Parcel r0 = m12300(31, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        Parcel r0 = m12300(18, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }

    public final zzll getVideoController() throws RemoteException {
        zzll zzln;
        Parcel r1 = m12300(26, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzln = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
            zzln = queryLocalInterface instanceof zzll ? (zzll) queryLocalInterface : new zzln(readStrongBinder);
        }
        r1.recycle();
        return zzln;
    }

    public final boolean isLoading() throws RemoteException {
        Parcel r0 = m12300(23, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    public final boolean isReady() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    public final void pause() throws RemoteException {
        m12298(5, v_());
    }

    public final void resume() throws RemoteException {
        m12298(6, v_());
    }

    public final void setImmersiveMode(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(34, v_);
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(22, v_);
    }

    public final void setUserId(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12298(25, v_);
    }

    public final void showInterstitial() throws RemoteException {
        m12298(9, v_());
    }

    public final void stopLoading() throws RemoteException {
        m12298(10, v_());
    }

    public final void zza(zzadp zzadp) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzadp);
        m12298(24, v_);
    }

    public final void zza(zzjn zzjn) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzjn);
        m12298(13, v_);
    }

    public final void zza(zzke zzke) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzke);
        m12298(20, v_);
    }

    public final void zza(zzkh zzkh) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzkh);
        m12298(7, v_);
    }

    public final void zza(zzkx zzkx) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzkx);
        m12298(8, v_);
    }

    public final void zza(zzld zzld) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzld);
        m12298(21, v_);
    }

    public final void zza(zzlr zzlr) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzlr);
        m12298(30, v_);
    }

    public final void zza(zzmr zzmr) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzmr);
        m12298(29, v_);
    }

    public final void zza(zzoa zzoa) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzoa);
        m12298(19, v_);
    }

    public final void zza(zzxl zzxl) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzxl);
        m12298(14, v_);
    }

    public final void zza(zzxr zzxr, String str) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzxr);
        v_.writeString(str);
        m12298(15, v_);
    }

    public final boolean zzb(zzjj zzjj) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzjj);
        Parcel r0 = m12300(4, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    public final IObjectWrapper zzbr() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    public final zzjn zzbs() throws RemoteException {
        Parcel r1 = m12300(12, v_());
        zzjn zzjn = (zzjn) zzew.m12304(r1, zzjn.CREATOR);
        r1.recycle();
        return zzjn;
    }

    public final void zzbu() throws RemoteException {
        m12298(11, v_());
    }

    public final zzkx zzcd() throws RemoteException {
        zzkx zzkz;
        Parcel r1 = m12300(32, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzkz = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
            zzkz = queryLocalInterface instanceof zzkx ? (zzkx) queryLocalInterface : new zzkz(readStrongBinder);
        }
        r1.recycle();
        return zzkz;
    }

    public final zzkh zzce() throws RemoteException {
        zzkh zzkj;
        Parcel r1 = m12300(33, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzkj = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
            zzkj = queryLocalInterface instanceof zzkh ? (zzkh) queryLocalInterface : new zzkj(readStrongBinder);
        }
        r1.recycle();
        return zzkj;
    }

    public final String zzcp() throws RemoteException {
        Parcel r0 = m12300(35, v_());
        String readString = r0.readString();
        r0.recycle();
        return readString;
    }
}
