package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzchb implements Parcelable.Creator<zzcha> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r6 = zzbfn.m10169(parcel);
        long j = 0;
        String str = null;
        zzcgx zzcgx = null;
        String str2 = null;
        while (parcel.dataPosition() < r6) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    zzcgx = (zzcgx) zzbfn.m10171(parcel, readInt, zzcgx.CREATOR);
                    break;
                case 4:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r6);
        return new zzcha(str2, zzcgx, str, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcha[i];
    }
}
