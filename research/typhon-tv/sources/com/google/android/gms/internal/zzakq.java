package com.google.android.gms.internal;

import java.util.concurrent.Future;

final /* synthetic */ class zzakq implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Future f8278;

    zzakq(Future future) {
        this.f8278 = future;
    }

    public final void run() {
        Future future = this.f8278;
        if (!future.isDone()) {
            future.cancel(true);
        }
    }
}
