package com.google.android.gms.internal;

import java.util.PriorityQueue;

@zzzv
public final class zzhx {
    /* renamed from: 靐  reason: contains not printable characters */
    private static long m5402(String[] strArr, int i, int i2) {
        long r2 = (((long) zzhu.m5399(strArr[0])) + 2147483647L) % 1073807359;
        for (int i3 = 1; i3 < i2; i3++) {
            r2 = (((r2 * 16785407) % 1073807359) + ((((long) zzhu.m5399(strArr[i3])) + 2147483647L) % 1073807359)) % 1073807359;
        }
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m5403(long j, int i) {
        if (i == 0) {
            return 1;
        }
        return i != 1 ? i % 2 == 0 ? m5403((j * j) % 1073807359, i / 2) % 1073807359 : ((m5403((j * j) % 1073807359, i / 2) % 1073807359) * j) % 1073807359 : j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m5404(String[] strArr, int i, int i2) {
        if (strArr.length < i + i2) {
            zzagf.m4795("Unable to construct shingle");
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i3 = i; i3 < (i + i2) - 1; i3++) {
            stringBuffer.append(strArr[i3]);
            stringBuffer.append(' ');
        }
        stringBuffer.append(strArr[(i + i2) - 1]);
        return stringBuffer.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m5405(int i, long j, String str, int i2, PriorityQueue<zzhy> priorityQueue) {
        zzhy zzhy = new zzhy(j, str, i2);
        if ((priorityQueue.size() != i || (priorityQueue.peek().f10706 <= zzhy.f10706 && priorityQueue.peek().f10707 <= zzhy.f10707)) && !priorityQueue.contains(zzhy)) {
            priorityQueue.add(zzhy);
            if (priorityQueue.size() > i) {
                priorityQueue.poll();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m5406(String[] strArr, int i, int i2, PriorityQueue<zzhy> priorityQueue) {
        if (strArr.length < i2) {
            m5405(i, m5402(strArr, 0, strArr.length), m5404(strArr, 0, strArr.length), strArr.length, priorityQueue);
            return;
        }
        long r2 = m5402(strArr, 0, i2);
        m5405(i, r2, m5404(strArr, 0, i2), i2, priorityQueue);
        long r8 = m5403(16785407, i2 - 1);
        for (int i3 = 1; i3 < (strArr.length - i2) + 1; i3++) {
            long j = r2 + 1073807359;
            r2 = (((((j - ((((((long) zzhu.m5399(strArr[i3 - 1])) + 2147483647L) % 1073807359) * r8) % 1073807359)) % 1073807359) * 16785407) % 1073807359) + ((((long) zzhu.m5399(strArr[(i3 + i2) - 1])) + 2147483647L) % 1073807359)) % 1073807359;
            m5405(i, r2, m5404(strArr, i3, i2), strArr.length, priorityQueue);
        }
    }
}
