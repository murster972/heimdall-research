package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzli extends zzeu implements zzlg {
    zzli(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }

    public final void initialize() throws RemoteException {
        m12298(1, v_());
    }

    public final void setAppMuted(boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        m12298(4, v_);
    }

    public final void setAppVolume(float f) throws RemoteException {
        Parcel v_ = v_();
        v_.writeFloat(f);
        m12298(2, v_);
    }

    public final void zza(String str, IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        m12298(6, v_);
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeString(str);
        m12298(5, v_);
    }

    public final float zzdn() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        float readFloat = r0.readFloat();
        r0.recycle();
        return readFloat;
    }

    public final boolean zzdo() throws RemoteException {
        Parcel r0 = m12300(8, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    public final void zzu(String str) throws RemoteException {
        Parcel v_ = v_();
        v_.writeString(str);
        m12298(3, v_);
    }
}
