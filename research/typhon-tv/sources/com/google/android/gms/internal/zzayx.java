package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.zzd;
import com.google.android.gms.cast.framework.zzab;
import com.google.android.gms.cast.framework.zzh;
import com.google.android.gms.cast.framework.zzj;
import com.google.android.gms.cast.framework.zzl;
import com.google.android.gms.cast.framework.zzr;
import com.google.android.gms.cast.framework.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.Map;

public interface zzayx extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    zzd m9769(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3, CastMediaOptions castMediaOptions) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    zzj m9770(IObjectWrapper iObjectWrapper, CastOptions castOptions, zzayz zzayz, Map map) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    zzl m9771(CastOptions castOptions, IObjectWrapper iObjectWrapper, zzh zzh) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    zzr m9772(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    zzt m9773(String str, String str2, zzab zzab) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    zzazu m9774(IObjectWrapper iObjectWrapper, zzazw zzazw, int i, int i2, boolean z, long j, int i3, int i4, int i5) throws RemoteException;
}
