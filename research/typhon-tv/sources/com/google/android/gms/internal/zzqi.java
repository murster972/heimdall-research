package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzqi extends IInterface {
    /* renamed from: ʻ  reason: contains not printable characters */
    zzpq m13286() throws RemoteException;

    /* renamed from: ʼ  reason: contains not printable characters */
    String m13287() throws RemoteException;

    /* renamed from: ʽ  reason: contains not printable characters */
    String m13288() throws RemoteException;

    /* renamed from: ʿ  reason: contains not printable characters */
    Bundle m13289() throws RemoteException;

    /* renamed from: ˊ  reason: contains not printable characters */
    void m13290() throws RemoteException;

    /* renamed from: ˑ  reason: contains not printable characters */
    zzll m13291() throws RemoteException;

    /* renamed from: ٴ  reason: contains not printable characters */
    IObjectWrapper m13292() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    String m13293() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    List m13294() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m13295(Bundle bundle) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    String m13296() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    IObjectWrapper m13297() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m13298(Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m13299() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m13300(Bundle bundle) throws RemoteException;

    /* renamed from: ﾞ  reason: contains not printable characters */
    zzpm m13301() throws RemoteException;
}
