package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdsy;
import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

final class zzdqy implements zzdpw<zzdqa> {
    zzdqy() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final zzdqa m11757(zzfes zzfes) throws GeneralSecurityException {
        zzdvg zzdvg;
        try {
            zzdss r0 = zzdss.m11976(zzfes);
            if (!(r0 instanceof zzdss)) {
                throw new GeneralSecurityException("expected HmacKey proto");
            }
            zzdss zzdss = r0;
            zzdvk.m12238(zzdss.m11985(), 0);
            if (zzdss.m11984().size() < 16) {
                throw new GeneralSecurityException("key too short");
            }
            m11753(zzdss.m11982());
            zzdsq r1 = zzdss.m11982().m12003();
            SecretKeySpec secretKeySpec = new SecretKeySpec(zzdss.m11984().toByteArray(), "HMAC");
            int r2 = zzdss.m11982().m12001();
            switch (zzdqz.f9957[r1.ordinal()]) {
                case 1:
                    zzdvg = new zzdvg("HMACSHA1", secretKeySpec, r2);
                    break;
                case 2:
                    zzdvg = new zzdvg("HMACSHA256", secretKeySpec, r2);
                    break;
                case 3:
                    zzdvg = new zzdvg("HMACSHA512", secretKeySpec, r2);
                    break;
                default:
                    throw new GeneralSecurityException("unknown hash");
            }
            return zzdvg;
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized HmacKey proto", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m11753(zzdsw zzdsw) throws GeneralSecurityException {
        if (zzdsw.m12001() < 10) {
            throw new GeneralSecurityException("tag size too small");
        }
        switch (zzdqz.f9957[zzdsw.m12003().ordinal()]) {
            case 1:
                if (zzdsw.m12001() > 20) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            case 2:
                if (zzdsw.m12001() > 32) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            case 3:
                if (zzdsw.m12001() > 64) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            default:
                throw new GeneralSecurityException("unknown hash type");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11754(zzfes zzfes) throws GeneralSecurityException {
        try {
            return m11755((zzfhe) zzdsu.m11993(zzfes));
        } catch (zzfge e) {
            throw new GeneralSecurityException("expected serialized HmacKeyFormat proto", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzfhe m11755(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdsu)) {
            throw new GeneralSecurityException("expected HmacKeyFormat proto");
        }
        zzdsu zzdsu = (zzdsu) zzfhe;
        if (zzdsu.m11994() < 16) {
            throw new GeneralSecurityException("key too short");
        }
        m11753(zzdsu.m11996());
        return zzdss.m11974().m11988(0).m11989(zzdsu.m11996()).m11990(zzfes.zzaz(zzdvi.m12235(zzdsu.m11994()))).m12542();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzdsy m11756(zzfes zzfes) throws GeneralSecurityException {
        return (zzdsy) zzdsy.m12008().m12023("type.googleapis.com/google.crypto.tink.HmacKey").m12022(((zzdss) m11754(zzfes)).m12361()).m12021(zzdsy.zzb.SYMMETRIC).m12542();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m11758(zzfhe zzfhe) throws GeneralSecurityException {
        if (!(zzfhe instanceof zzdss)) {
            throw new GeneralSecurityException("expected HmacKey proto");
        }
        zzdss zzdss = (zzdss) zzfhe;
        zzdvk.m12238(zzdss.m11985(), 0);
        if (zzdss.m11984().size() < 16) {
            throw new GeneralSecurityException("key too short");
        }
        m11753(zzdss.m11982());
        zzdsq r0 = zzdss.m11982().m12003();
        SecretKeySpec secretKeySpec = new SecretKeySpec(zzdss.m11984().toByteArray(), "HMAC");
        int r1 = zzdss.m11982().m12001();
        switch (zzdqz.f9957[r0.ordinal()]) {
            case 1:
                return new zzdvg("HMACSHA1", secretKeySpec, r1);
            case 2:
                return new zzdvg("HMACSHA256", secretKeySpec, r1);
            case 3:
                return new zzdvg("HMACSHA512", secretKeySpec, r1);
            default:
                throw new GeneralSecurityException("unknown hash");
        }
    }
}
