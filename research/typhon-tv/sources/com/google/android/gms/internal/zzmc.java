package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;

public final class zzmc extends zzko {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public zzkh f10796;

    public final void zza(PublisherAdViewOptions publisherAdViewOptions) throws RemoteException {
    }

    public final void zza(zzpe zzpe) throws RemoteException {
    }

    public final void zza(zzqq zzqq) throws RemoteException {
    }

    public final void zza(zzqt zzqt) throws RemoteException {
    }

    public final void zza(zzrc zzrc) throws RemoteException {
    }

    public final void zza(zzrf zzrf, zzjn zzjn) throws RemoteException {
    }

    public final void zza(String str, zzqz zzqz, zzqw zzqw) throws RemoteException {
    }

    public final void zzb(zzkh zzkh) throws RemoteException {
        this.f10796 = zzkh;
    }

    public final void zzb(zzld zzld) throws RemoteException {
    }

    public final zzkk zzdi() throws RemoteException {
        return new zzme(this);
    }
}
