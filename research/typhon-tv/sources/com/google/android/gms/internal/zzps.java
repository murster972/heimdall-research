package com.google.android.gms.internal;

import android.net.Uri;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzps extends zzeu implements zzpq {
    zzps(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdImage");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Uri m13227() throws RemoteException {
        Parcel r1 = m12300(2, v_());
        Uri uri = (Uri) zzew.m12304(r1, Uri.CREATOR);
        r1.recycle();
        return uri;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final double m13228() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        double readDouble = r0.readDouble();
        r0.recycle();
        return readDouble;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m13229() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
