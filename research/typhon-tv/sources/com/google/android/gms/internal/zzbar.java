package com.google.android.gms.internal;

import android.widget.ProgressBar;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbar extends UIController implements RemoteMediaClient.ProgressListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f8586;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ProgressBar f8587;

    public zzbar(ProgressBar progressBar, long j) {
        this.f8587 = progressBar;
        this.f8586 = j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9914() {
        if (m8226() != null) {
            m8226().m4166((RemoteMediaClient.ProgressListener) this);
        }
        this.f8587.setMax(1);
        this.f8587.setProgress(0);
        super.m8223();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9915() {
        RemoteMediaClient r0 = m8226();
        if (r0 == null || !r0.m4143()) {
            this.f8587.setMax(1);
            this.f8587.setProgress(0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9916(long j, long j2) {
        this.f8587.setMax((int) j2);
        this.f8587.setProgress((int) j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9917(CastSession castSession) {
        super.m8227(castSession);
        RemoteMediaClient r0 = m8226();
        if (r0 != null) {
            r0.m4168((RemoteMediaClient.ProgressListener) this, this.f8586);
            if (r0.m4143()) {
                this.f8587.setMax((int) r0.m4135());
                this.f8587.setProgress((int) r0.m4148());
                return;
            }
            this.f8587.setMax(1);
            this.f8587.setProgress(0);
        }
    }
}
