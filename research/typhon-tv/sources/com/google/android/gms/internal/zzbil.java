package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public final class zzbil extends zzbfm {
    public static final Parcelable.Creator<zzbil> CREATOR = new zzbim();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Bundle f8803;

    public zzbil(Bundle bundle) {
        this.f8803 = bundle;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10187(parcel, 2, this.f8803, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m10291() {
        return this.f8803;
    }
}
