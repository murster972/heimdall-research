package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffu;
import java.io.IOException;

final class zzffi implements zzfji {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzffg f10377;

    private zzffi(zzffg zzffg) {
        this.f10377 = (zzffg) zzffz.m12580(zzffg, "output");
        this.f10377.f4602 = this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzffi m12493(zzffg zzffg) {
        return zzffg.f4602 != null ? zzffg.f4602 : new zzffi(zzffg);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m12494() {
        return zzffu.zzg.f10413;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12495(int i, Object obj) {
        try {
            if (obj instanceof zzfes) {
                this.f10377.m5276(i, (zzfes) obj);
            } else {
                this.f10377.m5277(i, (zzfhe) obj);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
