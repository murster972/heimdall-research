package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbav extends UIController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f8594;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f8595;

    public zzbav(View view, int i) {
        this.f8595 = view;
        this.f8594 = i;
        this.f8595.setEnabled(false);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m9933() {
        boolean z;
        RemoteMediaClient r3 = m8226();
        if (r3 == null || !r3.m4143()) {
            this.f8595.setEnabled(false);
            return;
        }
        MediaStatus r0 = r3.m4136();
        if (r0.m7905() == 0) {
            Integer r02 = r0.m7918(r0.m7911());
            z = r02 != null && r02.intValue() > 0;
        } else {
            z = true;
        }
        if (!z || r3.m4144()) {
            this.f8595.setVisibility(this.f8594);
            this.f8595.setEnabled(false);
            return;
        }
        this.f8595.setVisibility(0);
        this.f8595.setEnabled(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9934() {
        this.f8595.setEnabled(false);
        super.m8223();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9935() {
        this.f8595.setEnabled(false);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9936() {
        m9933();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9937(CastSession castSession) {
        super.m8227(castSession);
        m9933();
    }
}
