package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class zzgu implements Application.ActivityLifecycleCallbacks {

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakReference<Application.ActivityLifecycleCallbacks> f10668;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f10669 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Application f10670;

    public zzgu(Application application, Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        this.f10668 = new WeakReference<>(activityLifecycleCallbacks);
        this.f10670 = application;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m12972(zzhc zzhc) {
        try {
            Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = (Application.ActivityLifecycleCallbacks) this.f10668.get();
            if (activityLifecycleCallbacks != null) {
                zzhc.m12983(activityLifecycleCallbacks);
            } else if (!this.f10669) {
                this.f10670.unregisterActivityLifecycleCallbacks(this);
                this.f10669 = true;
            }
        } catch (Exception e) {
            zzagf.m4793("Error while dispatching lifecycle callback.", e);
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        m12972(new zzgv(this, activity, bundle));
    }

    public final void onActivityDestroyed(Activity activity) {
        m12972(new zzhb(this, activity));
    }

    public final void onActivityPaused(Activity activity) {
        m12972(new zzgy(this, activity));
    }

    public final void onActivityResumed(Activity activity) {
        m12972(new zzgx(this, activity));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        m12972(new zzha(this, activity, bundle));
    }

    public final void onActivityStarted(Activity activity) {
        m12972(new zzgw(this, activity));
    }

    public final void onActivityStopped(Activity activity) {
        m12972(new zzgz(this, activity));
    }
}
