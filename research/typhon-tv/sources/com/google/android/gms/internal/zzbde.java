package com.google.android.gms.internal;

import com.google.android.gms.common.util.zzd;

public final class zzbde {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final zzbcy f8713 = new zzbcy("RequestTracker");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Object f8714 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzd f8715;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzbdd f8716;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f8717;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f8718 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f8719 = -1;

    public zzbde(zzd zzd, long j) {
        this.f8715 = zzd;
        this.f8717 = j;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m10127() {
        this.f8719 = -1;
        this.f8716 = null;
        this.f8718 = 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m10128() {
        boolean z;
        synchronized (f8714) {
            z = this.f8719 != -1;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10129() {
        synchronized (f8714) {
            if (this.f8719 != -1) {
                m10127();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10130(long j, zzbdd zzbdd) {
        zzbdd zzbdd2;
        long j2;
        synchronized (f8714) {
            zzbdd2 = this.f8716;
            j2 = this.f8719;
            this.f8719 = j;
            this.f8716 = zzbdd;
            this.f8718 = this.f8715.m9241();
        }
        if (zzbdd2 != null) {
            zzbdd2.m10125(j2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10131(long j) {
        boolean z;
        synchronized (f8714) {
            z = this.f8719 != -1 && this.f8719 == j;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10132(long j, int i) {
        zzbdd zzbdd;
        boolean z = true;
        long j2 = 0;
        synchronized (f8714) {
            if (this.f8719 == -1 || j - this.f8718 < this.f8717) {
                zzbdd = null;
                z = false;
            } else {
                f8713.m10090("request %d timed out", Long.valueOf(this.f8719));
                j2 = this.f8719;
                zzbdd = this.f8716;
                m10127();
            }
        }
        if (zzbdd != null) {
            zzbdd.m10126(j2, i, (Object) null);
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m10133(long j, int i, Object obj) {
        boolean z = true;
        zzbdd zzbdd = null;
        synchronized (f8714) {
            if (this.f8719 == -1 || this.f8719 != j) {
                z = false;
            } else {
                f8713.m10090("request %d completed", Long.valueOf(this.f8719));
                zzbdd = this.f8716;
                m10127();
            }
        }
        if (zzbdd != null) {
            zzbdd.m10126(j, i, obj);
        }
        return z;
    }
}
