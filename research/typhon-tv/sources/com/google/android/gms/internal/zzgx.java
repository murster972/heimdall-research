package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;

final class zzgx implements zzhc {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f10674;

    zzgx(zzgu zzgu, Activity activity) {
        this.f10674 = activity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12975(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityResumed(this.f10674);
    }
}
