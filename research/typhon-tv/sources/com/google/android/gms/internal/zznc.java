package com.google.android.gms.internal;

import android.content.SharedPreferences;
import org.json.JSONObject;

final class zznc extends zzmx<String> {
    zznc(int i, String str, String str2) {
        super(i, str, str2, (zzmy) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13160(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(m5586(), (String) m5582());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m13161(JSONObject jSONObject) {
        return jSONObject.optString(m5586(), (String) m5582());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m13162(SharedPreferences.Editor editor, Object obj) {
        editor.putString(m5586(), (String) obj);
    }
}
