package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

final class zzdb implements zzdi {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Bundle f9857;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Activity f9858;

    zzdb(zzda zzda, Activity activity, Bundle bundle) {
        this.f9858 = activity;
        this.f9857 = bundle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m11585(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityCreated(this.f9858, this.f9857);
    }
}
