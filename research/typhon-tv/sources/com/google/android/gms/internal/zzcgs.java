package com.google.android.gms.internal;

import android.os.Handler;
import com.google.android.gms.common.internal.zzbq;

abstract class zzcgs {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile Handler f9172;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f9173 = true;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public volatile long f9174;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Runnable f9175 = new zzcgt(this);
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcim f9176;

    zzcgs(zzcim zzcim) {
        zzbq.m9120(zzcim);
        this.f9176 = zzcim;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private final Handler m10620() {
        Handler handler;
        if (f9172 != null) {
            return f9172;
        }
        synchronized (zzcgs.class) {
            if (f9172 == null) {
                f9172 = new Handler(this.f9176.m11021().getMainLooper());
            }
            handler = f9172;
        }
        return handler;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m10623() {
        return this.f9174 != 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m10624() {
        this.f9174 = 0;
        m10620().removeCallbacks(this.f9175);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m10625();

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10626(long j) {
        m10624();
        if (j >= 0) {
            this.f9174 = this.f9176.m11023().m9243();
            if (!m10620().postDelayed(this.f9175, j)) {
                this.f9176.m11016().m10832().m10850("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }
}
