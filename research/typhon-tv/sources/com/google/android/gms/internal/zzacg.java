package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.mobileads.VastIconXmlManager;
import io.presage.ads.NewAd;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzacg {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final SimpleDateFormat f3923 = new SimpleDateFormat("yyyyMMdd", Locale.US);

    /* JADX WARNING: Removed duplicated region for block: B:57:0x0168 A[Catch:{ JSONException -> 0x0283 }] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.zzaax m4285(android.content.Context r50, com.google.android.gms.internal.zzaat r51, java.lang.String r52) {
        /*
            org.json.JSONObject r28 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0283 }
            r0 = r28
            r1 = r52
            r0.<init>((java.lang.String) r1)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "ad_base_url"
            r5 = 0
            r0 = r28
            java.lang.String r6 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "ad_url"
            r5 = 0
            r0 = r28
            java.lang.String r7 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "ad_size"
            r5 = 0
            r0 = r28
            java.lang.String r19 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "ad_slot_size"
            r0 = r28
            r1 = r19
            java.lang.String r43 = r0.optString(r4, r1)     // Catch:{ JSONException -> 0x0283 }
            if (r51 == 0) goto L_0x00ed
            r0 = r51
            int r4 = r0.f3722     // Catch:{ JSONException -> 0x0283 }
            if (r4 == 0) goto L_0x00ed
            r27 = 1
        L_0x003c:
            java.lang.String r4 = "ad_json"
            r5 = 0
            r0 = r28
            java.lang.String r5 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            if (r5 != 0) goto L_0x0052
            java.lang.String r4 = "ad_html"
            r5 = 0
            r0 = r28
            java.lang.String r5 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
        L_0x0052:
            if (r5 != 0) goto L_0x005e
            java.lang.String r4 = "body"
            r5 = 0
            r0 = r28
            java.lang.String r5 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
        L_0x005e:
            if (r5 != 0) goto L_0x006f
            java.lang.String r4 = "ads"
            r0 = r28
            boolean r4 = r0.has(r4)     // Catch:{ JSONException -> 0x0283 }
            if (r4 == 0) goto L_0x006f
            java.lang.String r5 = r28.toString()     // Catch:{ JSONException -> 0x0283 }
        L_0x006f:
            r20 = -1
            java.lang.String r4 = "debug_dialog"
            r8 = 0
            r0 = r28
            java.lang.String r22 = r0.optString(r4, r8)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "debug_signals"
            r8 = 0
            r0 = r28
            java.lang.String r45 = r0.optString(r4, r8)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "interstitial_timeout"
            r0 = r28
            boolean r4 = r0.has(r4)     // Catch:{ JSONException -> 0x0283 }
            if (r4 == 0) goto L_0x00f1
            java.lang.String r4 = "interstitial_timeout"
            r0 = r28
            double r8 = r0.getDouble(r4)     // Catch:{ JSONException -> 0x0283 }
            r10 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r8 = r8 * r10
            long r12 = (long) r8     // Catch:{ JSONException -> 0x0283 }
        L_0x00a0:
            java.lang.String r4 = "orientation"
            r8 = 0
            r0 = r28
            java.lang.String r4 = r0.optString(r4, r8)     // Catch:{ JSONException -> 0x0283 }
            r18 = -1
            java.lang.String r8 = "portrait"
            boolean r8 = r8.equals(r4)     // Catch:{ JSONException -> 0x0283 }
            if (r8 == 0) goto L_0x00f4
            com.google.android.gms.internal.zzaht r4 = com.google.android.gms.ads.internal.zzbs.zzek()     // Catch:{ JSONException -> 0x0283 }
            int r18 = r4.m4644()     // Catch:{ JSONException -> 0x0283 }
        L_0x00bd:
            r4 = 0
            boolean r8 = android.text.TextUtils.isEmpty(r5)     // Catch:{ JSONException -> 0x0283 }
            if (r8 == 0) goto L_0x02b9
            boolean r8 = android.text.TextUtils.isEmpty(r7)     // Catch:{ JSONException -> 0x0283 }
            if (r8 != 0) goto L_0x02b9
            r0 = r51
            com.google.android.gms.internal.zzakd r4 = r0.f3748     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r6 = r4.f4297     // Catch:{ JSONException -> 0x0283 }
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r4 = r51
            r5 = r50
            com.google.android.gms.internal.zzaax r4 = com.google.android.gms.internal.zzacb.m4278(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r6 = r4.f3861     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r7 = r4.f3858     // Catch:{ JSONException -> 0x0283 }
            long r0 = r4.f3826     // Catch:{ JSONException -> 0x0283 }
            r20 = r0
        L_0x00e4:
            if (r7 != 0) goto L_0x0106
            com.google.android.gms.internal.zzaax r4 = new com.google.android.gms.internal.zzaax     // Catch:{ JSONException -> 0x0283 }
            r5 = 0
            r4.<init>(r5)     // Catch:{ JSONException -> 0x0283 }
        L_0x00ec:
            return r4
        L_0x00ed:
            r27 = 0
            goto L_0x003c
        L_0x00f1:
            r12 = -1
            goto L_0x00a0
        L_0x00f4:
            java.lang.String r8 = "landscape"
            boolean r4 = r8.equals(r4)     // Catch:{ JSONException -> 0x0283 }
            if (r4 == 0) goto L_0x00bd
            com.google.android.gms.internal.zzaht r4 = com.google.android.gms.ads.internal.zzbs.zzek()     // Catch:{ JSONException -> 0x0283 }
            int r18 = r4.m4652()     // Catch:{ JSONException -> 0x0283 }
            goto L_0x00bd
        L_0x0106:
            java.lang.String r5 = "click_urls"
            r0 = r28
            org.json.JSONArray r5 = r0.optJSONArray(r5)     // Catch:{ JSONException -> 0x0283 }
            if (r4 != 0) goto L_0x02a4
            r8 = 0
        L_0x0112:
            if (r5 == 0) goto L_0x0118
            java.util.List r8 = m4287((org.json.JSONArray) r5, (java.util.List<java.lang.String>) r8)     // Catch:{ JSONException -> 0x0283 }
        L_0x0118:
            java.lang.String r5 = "impression_urls"
            r0 = r28
            org.json.JSONArray r5 = r0.optJSONArray(r5)     // Catch:{ JSONException -> 0x0283 }
            if (r4 != 0) goto L_0x02a8
            r9 = 0
        L_0x0124:
            if (r5 == 0) goto L_0x012a
            java.util.List r9 = m4287((org.json.JSONArray) r5, (java.util.List<java.lang.String>) r9)     // Catch:{ JSONException -> 0x0283 }
        L_0x012a:
            java.lang.String r5 = "manual_impression_urls"
            r0 = r28
            org.json.JSONArray r5 = r0.optJSONArray(r5)     // Catch:{ JSONException -> 0x0283 }
            if (r4 != 0) goto L_0x02ac
            r15 = 0
        L_0x0136:
            if (r5 == 0) goto L_0x013c
            java.util.List r15 = m4287((org.json.JSONArray) r5, (java.util.List<java.lang.String>) r15)     // Catch:{ JSONException -> 0x0283 }
        L_0x013c:
            if (r4 == 0) goto L_0x02b6
            int r5 = r4.f3849     // Catch:{ JSONException -> 0x0283 }
            r10 = -1
            if (r5 == r10) goto L_0x0147
            int r0 = r4.f3849     // Catch:{ JSONException -> 0x0283 }
            r18 = r0
        L_0x0147:
            long r10 = r4.f3820     // Catch:{ JSONException -> 0x0283 }
            r16 = 0
            int r5 = (r10 > r16 ? 1 : (r10 == r16 ? 0 : -1))
            if (r5 <= 0) goto L_0x02b6
            long r10 = r4.f3820     // Catch:{ JSONException -> 0x0283 }
        L_0x0151:
            java.lang.String r4 = "active_view"
            r0 = r28
            java.lang.String r25 = r0.optString(r4)     // Catch:{ JSONException -> 0x0283 }
            r24 = 0
            java.lang.String r4 = "ad_is_javascript"
            r5 = 0
            r0 = r28
            boolean r23 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            if (r23 == 0) goto L_0x0172
            java.lang.String r4 = "ad_passback_url"
            r5 = 0
            r0 = r28
            java.lang.String r24 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
        L_0x0172:
            java.lang.String r4 = "mediation"
            r5 = 0
            r0 = r28
            boolean r12 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "custom_render_allowed"
            r5 = 0
            r0 = r28
            boolean r26 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "content_url_opted_out"
            r5 = 1
            r0 = r28
            boolean r29 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "content_vertical_opted_out"
            r5 = 1
            r0 = r28
            boolean r46 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "prefetch"
            r5 = 0
            r0 = r28
            boolean r30 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "refresh_interval_milliseconds"
            r16 = -1
            r0 = r28
            r1 = r16
            long r16 = r0.optLong(r4, r1)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "mediation_config_cache_time_milliseconds"
            r32 = -1
            r0 = r28
            r1 = r32
            long r13 = r0.optLong(r4, r1)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "gws_query_id"
            java.lang.String r5 = ""
            r0 = r28
            java.lang.String r31 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "height"
            java.lang.String r5 = "fluid"
            java.lang.String r32 = ""
            r0 = r28
            r1 = r32
            java.lang.String r5 = r0.optString(r5, r1)     // Catch:{ JSONException -> 0x0283 }
            boolean r32 = r4.equals(r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "native_express"
            r5 = 0
            r0 = r28
            boolean r33 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "video_start_urls"
            r0 = r28
            org.json.JSONArray r4 = r0.optJSONArray(r4)     // Catch:{ JSONException -> 0x0283 }
            r5 = 0
            java.util.List r35 = m4287((org.json.JSONArray) r4, (java.util.List<java.lang.String>) r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "video_complete_urls"
            r0 = r28
            org.json.JSONArray r4 = r0.optJSONArray(r4)     // Catch:{ JSONException -> 0x0283 }
            r5 = 0
            java.util.List r36 = m4287((org.json.JSONArray) r4, (java.util.List<java.lang.String>) r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "rewards"
            r0 = r28
            org.json.JSONArray r4 = r0.optJSONArray(r4)     // Catch:{ JSONException -> 0x0283 }
            com.google.android.gms.internal.zzaeq r34 = com.google.android.gms.internal.zzaeq.m4388((org.json.JSONArray) r4)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "use_displayed_impression"
            r5 = 0
            r0 = r28
            boolean r37 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "auto_protection_configuration"
            r0 = r28
            org.json.JSONObject r4 = r0.optJSONObject(r4)     // Catch:{ JSONException -> 0x0283 }
            com.google.android.gms.internal.zzaaz r38 = com.google.android.gms.internal.zzaaz.m4261(r4)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "set_cookie"
            java.lang.String r5 = ""
            r0 = r28
            java.lang.String r40 = r0.optString(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "remote_ping_urls"
            r0 = r28
            org.json.JSONArray r4 = r0.optJSONArray(r4)     // Catch:{ JSONException -> 0x0283 }
            r5 = 0
            java.util.List r41 = m4287((org.json.JSONArray) r4, (java.util.List<java.lang.String>) r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "safe_browsing"
            r0 = r28
            org.json.JSONObject r4 = r0.optJSONObject(r4)     // Catch:{ JSONException -> 0x0283 }
            com.google.android.gms.internal.zzaey r44 = com.google.android.gms.internal.zzaey.m4403(r4)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "render_in_browser"
            r0 = r51
            boolean r5 = r0.f3725     // Catch:{ JSONException -> 0x0283 }
            r0 = r28
            boolean r42 = r0.optBoolean(r4, r5)     // Catch:{ JSONException -> 0x0283 }
            java.lang.String r4 = "custom_close_blocked"
            r0 = r28
            boolean r48 = r0.optBoolean(r4)     // Catch:{ JSONException -> 0x0283 }
            com.google.android.gms.internal.zzaax r4 = new com.google.android.gms.internal.zzaax     // Catch:{ JSONException -> 0x0283 }
            r0 = r51
            boolean r0 = r0.f3768     // Catch:{ JSONException -> 0x0283 }
            r28 = r0
            r0 = r51
            boolean r0 = r0.f3751     // Catch:{ JSONException -> 0x0283 }
            r39 = r0
            r0 = r51
            boolean r0 = r0.f3741     // Catch:{ JSONException -> 0x0283 }
            r47 = r0
            r49 = 0
            r5 = r51
            r4.<init>(r5, r6, r7, r8, r9, r10, r12, r13, r15, r16, r18, r19, r20, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49)     // Catch:{ JSONException -> 0x0283 }
            goto L_0x00ec
        L_0x0283:
            r4 = move-exception
            java.lang.String r5 = "Could not parse the inline ad response: "
            java.lang.String r4 = r4.getMessage()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r6 = r4.length()
            if (r6 == 0) goto L_0x02b0
            java.lang.String r4 = r5.concat(r4)
        L_0x0299:
            com.google.android.gms.internal.zzagf.m4791(r4)
            com.google.android.gms.internal.zzaax r4 = new com.google.android.gms.internal.zzaax
            r5 = 0
            r4.<init>(r5)
            goto L_0x00ec
        L_0x02a4:
            java.util.List<java.lang.String> r8 = r4.f3860     // Catch:{ JSONException -> 0x0283 }
            goto L_0x0112
        L_0x02a8:
            java.util.List<java.lang.String> r9 = r4.f3857     // Catch:{ JSONException -> 0x0283 }
            goto L_0x0124
        L_0x02ac:
            java.util.List<java.lang.String> r15 = r4.f3844     // Catch:{ JSONException -> 0x0283 }
            goto L_0x0136
        L_0x02b0:
            java.lang.String r4 = new java.lang.String
            r4.<init>(r5)
            goto L_0x0299
        L_0x02b6:
            r10 = r12
            goto L_0x0151
        L_0x02b9:
            r7 = r5
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzacg.m4285(android.content.Context, com.google.android.gms.internal.zzaat, java.lang.String):com.google.android.gms.internal.zzaax");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Integer m4286(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<String> m4287(JSONArray jSONArray, List<String> list) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        if (list == null) {
            list = new LinkedList<>();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            list.add(jSONArray.getString(i));
        }
        return list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static JSONArray m4288(List<String> list) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put((Object) put);
        }
        return jSONArray;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONObject m4289(Context context, zzabz zzabz) {
        String str;
        zzaat zzaat = zzabz.f3898;
        Location location = zzabz.f3903;
        zzaco zzaco = zzabz.f3899;
        Bundle bundle = zzabz.f3905;
        JSONObject jSONObject = zzabz.f3900;
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("extra_caps", zzkb.m5481().m5595(zznh.f4981));
            if (zzabz.f3904.size() > 0) {
                hashMap.put("eid", TextUtils.join(",", zzabz.f3904));
            }
            if (zzaat.f3761 != null) {
                hashMap.put("ad_pos", zzaat.f3761);
            }
            zzjj zzjj = zzaat.f3763;
            String r2 = zzaga.m4518();
            if (r2 != null) {
                hashMap.put("abf", r2);
            }
            if (zzjj.f4765 != -1) {
                hashMap.put("cust_age", f3923.format(new Date(zzjj.f4765)));
            }
            if (zzjj.f4767 != null) {
                hashMap.put("extras", zzjj.f4767);
            }
            if (zzjj.f4766 != -1) {
                hashMap.put("cust_gender", Integer.valueOf(zzjj.f4766));
            }
            if (zzjj.f4764 != null) {
                hashMap.put("kw", zzjj.f4764);
            }
            if (zzjj.f4754 != -1) {
                hashMap.put("tag_for_child_directed_treatment", Integer.valueOf(zzjj.f4754));
            }
            if (zzjj.f4753) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f5045)).booleanValue()) {
                    hashMap.put("test_request", true);
                } else {
                    hashMap.put("adtest", "on");
                }
            }
            if (zzjj.f4768 >= 2) {
                if (zzjj.f4755) {
                    hashMap.put("d_imp_hdr", 1);
                }
                if (!TextUtils.isEmpty(zzjj.f4761)) {
                    hashMap.put("ppid", zzjj.f4761);
                }
            }
            if (zzjj.f4768 >= 3 && zzjj.f4758 != null) {
                hashMap.put("url", zzjj.f4758);
            }
            if (zzjj.f4768 >= 5) {
                if (zzjj.f4757 != null) {
                    hashMap.put("custom_targeting", zzjj.f4757);
                }
                if (zzjj.f4769 != null) {
                    hashMap.put("category_exclusions", zzjj.f4769);
                }
                if (zzjj.f4770 != null) {
                    hashMap.put("request_agent", zzjj.f4770);
                }
            }
            if (zzjj.f4768 >= 6 && zzjj.f4759 != null) {
                hashMap.put("request_pkg", zzjj.f4759);
            }
            if (zzjj.f4768 >= 7) {
                hashMap.put("is_designed_for_families", Boolean.valueOf(zzjj.f4760));
            }
            if (zzaat.f3762.f4790 != null) {
                boolean z = false;
                boolean z2 = false;
                for (zzjn zzjn : zzaat.f3762.f4790) {
                    if (!zzjn.f4792 && !z) {
                        hashMap.put("format", zzjn.f4798);
                        z = true;
                    }
                    if (zzjn.f4792 && !z2) {
                        hashMap.put("fluid", VastIconXmlManager.HEIGHT);
                        z2 = true;
                    }
                    if (z && z2) {
                        break;
                    }
                }
            } else {
                hashMap.put("format", zzaat.f3762.f4798);
                if (zzaat.f3762.f4792) {
                    hashMap.put("fluid", VastIconXmlManager.HEIGHT);
                }
            }
            if (zzaat.f3762.f4794 == -1) {
                hashMap.put("smart_w", "full");
            }
            if (zzaat.f3762.f4795 == -2) {
                hashMap.put("smart_h", "auto");
            }
            if (zzaat.f3762.f4790 != null) {
                StringBuilder sb = new StringBuilder();
                boolean z3 = false;
                for (zzjn zzjn2 : zzaat.f3762.f4790) {
                    if (zzjn2.f4792) {
                        z3 = true;
                    } else {
                        if (sb.length() != 0) {
                            sb.append("|");
                        }
                        sb.append(zzjn2.f4794 == -1 ? (int) (((float) zzjn2.f4789) / zzaco.f3989) : zzjn2.f4794);
                        sb.append("x");
                        sb.append(zzjn2.f4795 == -2 ? (int) (((float) zzjn2.f4797) / zzaco.f3989) : zzjn2.f4795);
                    }
                }
                if (z3) {
                    if (sb.length() != 0) {
                        sb.insert(0, "|");
                    }
                    sb.insert(0, "320x50");
                }
                hashMap.put("sz", sb);
            }
            if (zzaat.f3722 != 0) {
                hashMap.put("native_version", Integer.valueOf(zzaat.f3722));
                hashMap.put("native_templates", zzaat.f3724);
                zzpe zzpe = zzaat.f3750;
                if (zzpe != null) {
                    switch (zzpe.f5286) {
                        case 0:
                            str = "any";
                            break;
                        case 1:
                            str = "portrait";
                            break;
                        case 2:
                            str = "landscape";
                            break;
                        default:
                            str = "not_set";
                            break;
                    }
                } else {
                    str = "any";
                }
                hashMap.put("native_image_orientation", str);
                if (!zzaat.f3752.isEmpty()) {
                    hashMap.put("native_custom_templates", zzaat.f3752);
                }
                if (zzaat.f3764 >= 24) {
                    hashMap.put("max_num_ads", Integer.valueOf(zzaat.f3757));
                }
                if (!TextUtils.isEmpty(zzaat.f3743)) {
                    try {
                        hashMap.put("native_advanced_settings", new JSONArray(zzaat.f3743));
                    } catch (JSONException e) {
                        zzagf.m4796("Problem creating json from native advanced settings", e);
                    }
                }
            }
            if (zzaat.f3755 != null && zzaat.f3755.size() > 0) {
                for (Integer next : zzaat.f3755) {
                    if (next.intValue() == 2) {
                        hashMap.put("iba", true);
                    } else if (next.intValue() == 1) {
                        hashMap.put("ina", true);
                    }
                }
            }
            if (zzaat.f3762.f4793) {
                hashMap.put("ene", true);
            }
            if (zzaat.f3731 != null) {
                hashMap.put("is_icon_ad", true);
                hashMap.put("icon_ad_expansion_behavior", Integer.valueOf(zzaat.f3731.f4824));
            }
            hashMap.put("slotname", zzaat.f3760);
            hashMap.put("pn", zzaat.f3716.packageName);
            if (zzaat.f3718 != null) {
                hashMap.put("vc", Integer.valueOf(zzaat.f3718.versionCode));
            }
            hashMap.put("ms", zzabz.f3895);
            hashMap.put("seq_num", zzaat.f3740);
            hashMap.put("session_id", zzaat.f3746);
            hashMap.put("js", zzaat.f3748.f4297);
            zzacy zzacy = zzabz.f3901;
            Bundle bundle2 = zzaat.f3745;
            Bundle bundle3 = zzabz.f3902;
            hashMap.put("am", Integer.valueOf(zzaco.f4004));
            hashMap.put("cog", m4286(zzaco.f4001));
            hashMap.put("coh", m4286(zzaco.f4003));
            if (!TextUtils.isEmpty(zzaco.f4002)) {
                hashMap.put("carrier", zzaco.f4002);
            }
            hashMap.put("gl", zzaco.f4000);
            if (zzaco.f3979) {
                hashMap.put("simulator", 1);
            }
            if (zzaco.f3980) {
                hashMap.put("is_sidewinder", 1);
            }
            hashMap.put("ma", m4286(zzaco.f3981));
            hashMap.put("sp", m4286(zzaco.f3991));
            hashMap.put("hl", zzaco.f3994);
            if (!TextUtils.isEmpty(zzaco.f3995)) {
                hashMap.put("mv", zzaco.f3995);
            }
            hashMap.put("muv", Integer.valueOf(zzaco.f3982));
            if (zzaco.f3983 != -2) {
                hashMap.put("cnt", Integer.valueOf(zzaco.f3983));
            }
            hashMap.put("gnt", Integer.valueOf(zzaco.f4005));
            hashMap.put("pt", Integer.valueOf(zzaco.f4006));
            hashMap.put("rm", Integer.valueOf(zzaco.f3987));
            hashMap.put("riv", Integer.valueOf(zzaco.f3988));
            Bundle bundle4 = new Bundle();
            bundle4.putString("build_build", zzaco.f3997);
            bundle4.putString("build_device", zzaco.f3998);
            Bundle bundle5 = new Bundle();
            bundle5.putBoolean("is_charging", zzaco.f3992);
            bundle5.putDouble("battery_level", zzaco.f3990);
            bundle4.putBundle("battery", bundle5);
            Bundle bundle6 = new Bundle();
            bundle6.putInt("active_network_state", zzaco.f3996);
            bundle6.putBoolean("active_network_metered", zzaco.f3993);
            if (zzacy != null) {
                Bundle bundle7 = new Bundle();
                bundle7.putInt("predicted_latency_micros", zzacy.f8109);
                bundle7.putLong("predicted_down_throughput_bps", zzacy.f8107);
                bundle7.putLong("predicted_up_throughput_bps", zzacy.f8108);
                bundle6.putBundle("predictions", bundle7);
            }
            bundle4.putBundle("network", bundle6);
            Bundle bundle8 = new Bundle();
            bundle8.putBoolean("is_browser_custom_tabs_capable", zzaco.f3999);
            bundle4.putBundle("browser", bundle8);
            if (bundle2 != null) {
                Bundle bundle9 = new Bundle();
                bundle9.putString("runtime_free", Long.toString(bundle2.getLong("runtime_free_memory", -1)));
                bundle9.putString("runtime_max", Long.toString(bundle2.getLong("runtime_max_memory", -1)));
                bundle9.putString("runtime_total", Long.toString(bundle2.getLong("runtime_total_memory", -1)));
                bundle9.putString("web_view_count", Integer.toString(bundle2.getInt("web_view_count", 0)));
                Debug.MemoryInfo memoryInfo = (Debug.MemoryInfo) bundle2.getParcelable("debug_memory_info");
                if (memoryInfo != null) {
                    bundle9.putString("debug_info_dalvik_private_dirty", Integer.toString(memoryInfo.dalvikPrivateDirty));
                    bundle9.putString("debug_info_dalvik_pss", Integer.toString(memoryInfo.dalvikPss));
                    bundle9.putString("debug_info_dalvik_shared_dirty", Integer.toString(memoryInfo.dalvikSharedDirty));
                    bundle9.putString("debug_info_native_private_dirty", Integer.toString(memoryInfo.nativePrivateDirty));
                    bundle9.putString("debug_info_native_pss", Integer.toString(memoryInfo.nativePss));
                    bundle9.putString("debug_info_native_shared_dirty", Integer.toString(memoryInfo.nativeSharedDirty));
                    bundle9.putString("debug_info_other_private_dirty", Integer.toString(memoryInfo.otherPrivateDirty));
                    bundle9.putString("debug_info_other_pss", Integer.toString(memoryInfo.otherPss));
                    bundle9.putString("debug_info_other_shared_dirty", Integer.toString(memoryInfo.otherSharedDirty));
                }
                bundle4.putBundle("android_mem_info", bundle9);
            }
            Bundle bundle10 = new Bundle();
            bundle10.putBundle("parental_controls", bundle3);
            if (!TextUtils.isEmpty(zzaco.f3985)) {
                bundle10.putString("package_version", zzaco.f3985);
            }
            bundle4.putBundle("play_store", bundle10);
            hashMap.put("device", bundle4);
            Bundle bundle11 = new Bundle();
            bundle11.putString("doritos", zzabz.f3894);
            if (((Boolean) zzkb.m5481().m5595(zznh.f4914)).booleanValue()) {
                String str2 = null;
                boolean z4 = false;
                if (zzabz.f3896 != null) {
                    str2 = zzabz.f3896.getId();
                    z4 = zzabz.f3896.isLimitAdTrackingEnabled();
                }
                if (!TextUtils.isEmpty(str2)) {
                    bundle11.putString("rdid", str2);
                    bundle11.putBoolean("is_lat", z4);
                    bundle11.putString("idtype", "adid");
                } else {
                    zzkb.m5487();
                    bundle11.putString("pdid", zzajr.m4751(context));
                    bundle11.putString("pdidtype", "ssaid");
                }
            }
            hashMap.put("pii", bundle11);
            hashMap.put("platform", Build.MANUFACTURER);
            hashMap.put("submodel", Build.MODEL);
            if (location != null) {
                m4291((HashMap<String, Object>) hashMap, location);
            } else if (zzaat.f3763.f4768 >= 2 && zzaat.f3763.f4763 != null) {
                m4291((HashMap<String, Object>) hashMap, zzaat.f3763.f4763);
            }
            if (zzaat.f3764 >= 2) {
                hashMap.put("quality_signals", zzaat.f3728);
            }
            if (zzaat.f3764 >= 4 && zzaat.f3768) {
                hashMap.put("forceHttps", Boolean.valueOf(zzaat.f3768));
            }
            if (bundle != null) {
                hashMap.put("content_info", bundle);
            }
            if (zzaat.f3764 >= 5) {
                hashMap.put("u_sd", Float.valueOf(zzaat.f3736));
                hashMap.put("sh", Integer.valueOf(zzaat.f3734));
                hashMap.put("sw", Integer.valueOf(zzaat.f3732));
            } else {
                hashMap.put("u_sd", Float.valueOf(zzaco.f3989));
                hashMap.put("sh", Integer.valueOf(zzaco.f3986));
                hashMap.put("sw", Integer.valueOf(zzaco.f3984));
            }
            if (zzaat.f3764 >= 6) {
                if (!TextUtils.isEmpty(zzaat.f3726)) {
                    try {
                        hashMap.put("view_hierarchy", new JSONObject(zzaat.f3726));
                    } catch (JSONException e2) {
                        zzagf.m4796("Problem serializing view hierarchy to JSON", e2);
                    }
                }
                hashMap.put("correlation_id", Long.valueOf(zzaat.f3730));
            }
            if (zzaat.f3764 >= 7) {
                hashMap.put("request_id", zzaat.f3738);
            }
            if (zzaat.f3764 >= 12 && !TextUtils.isEmpty(zzaat.f3756)) {
                hashMap.put("anchor", zzaat.f3756);
            }
            if (zzaat.f3764 >= 13) {
                hashMap.put("android_app_volume", Float.valueOf(zzaat.f3758));
            }
            if (zzaat.f3764 >= 18) {
                hashMap.put("android_app_muted", Boolean.valueOf(zzaat.f3721));
            }
            if (zzaat.f3764 >= 14 && zzaat.f3765 > 0) {
                hashMap.put("target_api", Integer.valueOf(zzaat.f3765));
            }
            if (zzaat.f3764 >= 15) {
                hashMap.put("scroll_index", Integer.valueOf(zzaat.f3769 == -1 ? -1 : zzaat.f3769));
            }
            if (zzaat.f3764 >= 16) {
                hashMap.put("_activity_context", Boolean.valueOf(zzaat.f3749));
            }
            if (zzaat.f3764 >= 18) {
                if (!TextUtils.isEmpty(zzaat.f3719)) {
                    try {
                        hashMap.put("app_settings", new JSONObject(zzaat.f3719));
                    } catch (JSONException e3) {
                        zzagf.m4796("Problem creating json from app settings", e3);
                    }
                }
                hashMap.put("render_in_browser", Boolean.valueOf(zzaat.f3725));
            }
            if (zzaat.f3764 >= 18) {
                hashMap.put("android_num_video_cache_tasks", Integer.valueOf(zzaat.f3723));
            }
            zzakd zzakd = zzaat.f3748;
            boolean z5 = zzaat.f3759;
            boolean z6 = zzabz.f3897;
            boolean z7 = zzaat.f3747;
            Bundle bundle12 = new Bundle();
            Bundle bundle13 = new Bundle();
            bundle13.putString("cl", "179146524");
            bundle13.putString("rapid_rc", "dev");
            bundle13.putString("rapid_rollup", "HEAD");
            bundle12.putBundle("build_meta", bundle13);
            bundle12.putString(PubnativeRequest.Parameters.META_FIELDS, Boolean.toString(((Boolean) zzkb.m5481().m5595(zznh.f4983)).booleanValue()));
            bundle12.putBoolean("instant_app", z5);
            bundle12.putBoolean("lite", zzakd.f4293);
            bundle12.putBoolean("local_service", z6);
            bundle12.putBoolean("is_privileged_process", z7);
            hashMap.put("sdk_env", bundle12);
            hashMap.put("cache_state", jSONObject);
            if (zzaat.f3764 >= 19) {
                hashMap.put("gct", zzaat.f3727);
            }
            if (zzaat.f3764 >= 21 && zzaat.f3729) {
                hashMap.put("de", PubnativeRequest.LEGACY_ZONE_ID);
            }
            if (((Boolean) zzkb.m5481().m5595(zznh.f4924)).booleanValue()) {
                String str3 = zzaat.f3762.f4798;
                boolean z8 = str3.equals("interstitial_mb") || str3.equals("reward_mb");
                Bundle bundle14 = zzaat.f3735;
                boolean z9 = bundle14 != null;
                if (z8 && z9) {
                    Bundle bundle15 = new Bundle();
                    bundle15.putBundle("interstitial_pool", bundle14);
                    hashMap.put("counters", bundle15);
                }
            }
            if (zzaat.f3733 != null) {
                hashMap.put("gmp_app_id", zzaat.f3733);
            }
            if (zzaat.f3739 == null) {
                hashMap.put("fbs_aiid", "");
            } else if ("TIME_OUT".equals(zzaat.f3739)) {
                hashMap.put("sai_timeout", zzkb.m5481().m5595(zznh.f4906));
            } else {
                hashMap.put("fbs_aiid", zzaat.f3739);
            }
            if (zzaat.f3737 != null) {
                hashMap.put("fbs_aeid", zzaat.f3737);
            }
            if (zzaat.f3764 >= 24) {
                hashMap.put("disable_ml", Boolean.valueOf(zzaat.f3766));
            }
            String str4 = (String) zzkb.m5481().m5595(zznh.f5149);
            if (str4 != null && !str4.isEmpty()) {
                if (Build.VERSION.SDK_INT >= ((Integer) zzkb.m5481().m5595(zznh.f5151)).intValue()) {
                    HashMap hashMap2 = new HashMap();
                    for (String str5 : str4.split(",")) {
                        hashMap2.put(str5, zzajp.m4735(str5));
                    }
                    hashMap.put("video_decoders", hashMap2);
                }
            }
            if (zzagf.m4798(2)) {
                String valueOf = String.valueOf(zzbs.zzei().m4634((Map<String, ?>) hashMap).toString(2));
                zzagf.m4527(valueOf.length() != 0 ? "Ad Request JSON: ".concat(valueOf) : new String("Ad Request JSON: "));
            }
            return zzbs.zzei().m4634((Map<String, ?>) hashMap);
        } catch (JSONException e4) {
            String valueOf2 = String.valueOf(e4.getMessage());
            zzagf.m4791(valueOf2.length() != 0 ? "Problem serializing ad request to JSON: ".concat(valueOf2) : new String("Problem serializing ad request to JSON: "));
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONObject m4290(zzaax zzaax) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (zzaax.f3861 != null) {
            jSONObject.put("ad_base_url", (Object) zzaax.f3861);
        }
        if (zzaax.f3832 != null) {
            jSONObject.put("ad_size", (Object) zzaax.f3832);
        }
        jSONObject.put("native", zzaax.f3840);
        if (zzaax.f3840) {
            jSONObject.put("ad_json", (Object) zzaax.f3858);
        } else {
            jSONObject.put("ad_html", (Object) zzaax.f3858);
        }
        if (zzaax.f3828 != null) {
            jSONObject.put("debug_dialog", (Object) zzaax.f3828);
        }
        if (zzaax.f3827 != null) {
            jSONObject.put("debug_signals", (Object) zzaax.f3827);
        }
        if (zzaax.f3820 != -1) {
            jSONObject.put("interstitial_timeout", ((double) zzaax.f3820) / 1000.0d);
        }
        if (zzaax.f3849 == zzbs.zzek().m4644()) {
            jSONObject.put("orientation", (Object) "portrait");
        } else if (zzaax.f3849 == zzbs.zzek().m4652()) {
            jSONObject.put("orientation", (Object) "landscape");
        }
        if (zzaax.f3860 != null) {
            jSONObject.put("click_urls", (Object) m4288(zzaax.f3860));
        }
        if (zzaax.f3857 != null) {
            jSONObject.put("impression_urls", (Object) m4288(zzaax.f3857));
        }
        if (zzaax.f3844 != null) {
            jSONObject.put("manual_impression_urls", (Object) m4288(zzaax.f3844));
        }
        if (zzaax.f3836 != null) {
            jSONObject.put("active_view", (Object) zzaax.f3836);
        }
        jSONObject.put("ad_is_javascript", zzaax.f3863);
        if (zzaax.f3864 != null) {
            jSONObject.put("ad_passback_url", (Object) zzaax.f3864);
        }
        jSONObject.put("mediation", zzaax.f3822);
        jSONObject.put("custom_render_allowed", zzaax.f3838);
        jSONObject.put("content_url_opted_out", zzaax.f3834);
        jSONObject.put("content_vertical_opted_out", zzaax.f3847);
        jSONObject.put("prefetch", zzaax.f3842);
        if (zzaax.f3848 != -1) {
            jSONObject.put("refresh_interval_milliseconds", zzaax.f3848);
        }
        if (zzaax.f3824 != -1) {
            jSONObject.put("mediation_config_cache_time_milliseconds", zzaax.f3824);
        }
        if (!TextUtils.isEmpty(zzaax.f3846)) {
            jSONObject.put("gws_query_id", (Object) zzaax.f3846);
        }
        jSONObject.put("fluid", (Object) zzaax.f3851 ? VastIconXmlManager.HEIGHT : "");
        jSONObject.put("native_express", zzaax.f3853);
        if (zzaax.f3855 != null) {
            jSONObject.put("video_start_urls", (Object) m4288(zzaax.f3855));
        }
        if (zzaax.f3856 != null) {
            jSONObject.put("video_complete_urls", (Object) m4288(zzaax.f3856));
        }
        if (zzaax.f3854 != null) {
            zzaeq zzaeq = zzaax.f3854;
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("rb_type", (Object) zzaeq.f4056);
            jSONObject2.put("rb_amount", zzaeq.f4055);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put((Object) jSONObject2);
            jSONObject.put(NewAd.EVENT_REWARD, (Object) jSONArray);
        }
        jSONObject.put("use_displayed_impression", zzaax.f3862);
        jSONObject.put("auto_protection_configuration", (Object) zzaax.f3865);
        jSONObject.put("render_in_browser", zzaax.f3825);
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4291(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put(PubnativeRequest.Parameters.LAT, valueOf3);
        hashMap2.put(PubnativeRequest.Parameters.LONG, valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }
}
