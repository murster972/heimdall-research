package com.google.android.gms.internal;

final /* synthetic */ class zzffr {

    /* renamed from: 靐  reason: contains not printable characters */
    static final /* synthetic */ int[] f10389 = new int[zzfiy.values().length];

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ int[] f10390 = new int[zzfjd.values().length];

    static {
        try {
            f10389[zzfiy.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f10389[zzfiy.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f10389[zzfiy.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f10389[zzfiy.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f10389[zzfiy.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f10389[zzfiy.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f10389[zzfiy.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f10389[zzfiy.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f10389[zzfiy.GROUP.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f10389[zzfiy.MESSAGE.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f10389[zzfiy.STRING.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f10389[zzfiy.BYTES.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f10389[zzfiy.UINT32.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f10389[zzfiy.SFIXED32.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f10389[zzfiy.SFIXED64.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f10389[zzfiy.SINT32.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f10389[zzfiy.SINT64.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
        try {
            f10389[zzfiy.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError e18) {
        }
        try {
            f10390[zzfjd.INT.ordinal()] = 1;
        } catch (NoSuchFieldError e19) {
        }
        try {
            f10390[zzfjd.LONG.ordinal()] = 2;
        } catch (NoSuchFieldError e20) {
        }
        try {
            f10390[zzfjd.FLOAT.ordinal()] = 3;
        } catch (NoSuchFieldError e21) {
        }
        try {
            f10390[zzfjd.DOUBLE.ordinal()] = 4;
        } catch (NoSuchFieldError e22) {
        }
        try {
            f10390[zzfjd.BOOLEAN.ordinal()] = 5;
        } catch (NoSuchFieldError e23) {
        }
        try {
            f10390[zzfjd.STRING.ordinal()] = 6;
        } catch (NoSuchFieldError e24) {
        }
        try {
            f10390[zzfjd.BYTE_STRING.ordinal()] = 7;
        } catch (NoSuchFieldError e25) {
        }
        try {
            f10390[zzfjd.ENUM.ordinal()] = 8;
        } catch (NoSuchFieldError e26) {
        }
        try {
            f10390[zzfjd.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError e27) {
        }
    }
}
