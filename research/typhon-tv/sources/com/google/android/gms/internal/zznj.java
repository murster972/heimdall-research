package com.google.android.gms.internal;

import android.content.Context;
import android.os.Build;
import com.google.android.gms.ads.internal.zzbs;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Future;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zznj {

    /* renamed from: 连任  reason: contains not printable characters */
    private String f5166 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f5167;

    /* renamed from: 麤  reason: contains not printable characters */
    private Context f5168 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Map<String, String> f5169;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f5170;

    public zznj(Context context, String str) {
        this.f5168 = context;
        this.f5166 = str;
        this.f5170 = ((Boolean) zzkb.m5481().m5595(zznh.f4893)).booleanValue();
        this.f5167 = (String) zzkb.m5481().m5595(zznh.f4947);
        this.f5169 = new LinkedHashMap();
        this.f5169.put("s", "gmob_sdk");
        this.f5169.put("v", "3");
        this.f5169.put(PubnativeRequest.Parameters.OS, Build.VERSION.RELEASE);
        this.f5169.put("sdk", Build.VERSION.SDK);
        Map<String, String> map = this.f5169;
        zzbs.zzei();
        map.put("device", zzahn.m4578());
        this.f5169.put("app", context.getApplicationContext() != null ? context.getApplicationContext().getPackageName() : context.getPackageName());
        Map<String, String> map2 = this.f5169;
        zzbs.zzei();
        map2.put("is_lite_sdk", zzahn.m4566(context) ? PubnativeRequest.LEGACY_ZONE_ID : "0");
        Future<zzaco> r1 = zzbs.zzes().m4318(this.f5168);
        try {
            r1.get();
            this.f5169.put("network_coarse", Integer.toString(r1.get().f3983));
            this.f5169.put("network_fine", Integer.toString(r1.get().f4005));
        } catch (Exception e) {
            zzbs.zzem().m4505((Throwable) e, "CsiConfiguration.CsiConfiguration");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final Map<String, String> m5601() {
        return this.f5169;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m5602() {
        return this.f5167;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final String m5603() {
        return this.f5166;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final Context m5604() {
        return this.f5168;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m5605() {
        return this.f5170;
    }
}
