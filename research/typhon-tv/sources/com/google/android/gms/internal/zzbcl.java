package com.google.android.gms.internal;

import com.google.android.gms.cast.Cast;

final class zzbcl implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f8667;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f8668;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbcf f8669;

    zzbcl(zzbch zzbch, zzbcf zzbcf, String str, String str2) {
        this.f8669 = zzbcf;
        this.f8667 = str;
        this.f8668 = str2;
    }

    public final void run() {
        Cast.MessageReceivedCallback messageReceivedCallback;
        synchronized (this.f8669.f8634) {
            messageReceivedCallback = (Cast.MessageReceivedCallback) this.f8669.f8634.get(this.f8667);
        }
        if (messageReceivedCallback != null) {
            messageReceivedCallback.m7816(this.f8669.f8632, this.f8667, this.f8668);
            return;
        }
        zzbcf.f8631.m10090("Discarded message for unknown namespace '%s'", this.f8667);
    }
}
