package com.google.android.gms.internal;

@zzzv
public final class zzale<T> extends zzalf<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final T f4308;

    private zzale(T t) {
        this.f4308 = t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> zzale<T> m4819(T t) {
        return new zzale<>(t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4820() {
        m4822(this.f4308);
    }
}
