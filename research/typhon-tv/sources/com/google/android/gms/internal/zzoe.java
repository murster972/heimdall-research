package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsServiceConnection;
import java.util.List;

@zzzv
public final class zzoe implements zzftt {

    /* renamed from: 靐  reason: contains not printable characters */
    private CustomTabsServiceConnection f5199;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzof f5200;

    /* renamed from: 龘  reason: contains not printable characters */
    private CustomTabsClient f5201;

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m5638(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return false;
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.example.com"));
        ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        if (queryIntentActivities == null || resolveActivity == null) {
            return false;
        }
        for (int i = 0; i < queryIntentActivities.size(); i++) {
            if (resolveActivity.activityInfo.name.equals(queryIntentActivities.get(i).activityInfo.name)) {
                return resolveActivity.activityInfo.packageName.equals(zzftr.m12949(context));
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5639(Activity activity) {
        String r0;
        if (this.f5201 == null && (r0 = zzftr.m12949(activity)) != null) {
            this.f5199 = new zzfts(this);
            CustomTabsClient.bindCustomTabsService(activity, r0, this.f5199);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m5640(zzof zzof) {
        this.f5200 = zzof;
    }
}
