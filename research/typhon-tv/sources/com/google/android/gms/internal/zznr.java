package com.google.android.gms.internal;

import android.text.TextUtils;

final class zznr extends zzno {
    zznr() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m13165(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        int i = 0;
        int length = str.length();
        while (i < str.length() && str.charAt(i) == ',') {
            i++;
        }
        while (length > 0 && str.charAt(length - 1) == ',') {
            length--;
        }
        return (i == 0 && length == str.length()) ? str : str.substring(i, length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m13166(String str, String str2) {
        String r1 = m13165(str);
        String r0 = m13165(str2);
        return TextUtils.isEmpty(r1) ? r0 : TextUtils.isEmpty(r0) ? r1 : new StringBuilder(String.valueOf(r1).length() + 1 + String.valueOf(r0).length()).append(r1).append(",").append(r0).toString();
    }
}
