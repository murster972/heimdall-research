package com.google.android.gms.internal;

@zzzv
public final class zzuo {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final zzvg f5443;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final long f5444;

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzuk f5445;

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzuh f5446;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f5447;

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzva f5448;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f5449;

    public zzuo(int i) {
        this((zzuh) null, (zzva) null, (String) null, (zzuk) null, i, (zzvg) null, 0);
    }

    public zzuo(zzuh zzuh, zzva zzva, String str, zzuk zzuk, int i, zzvg zzvg, long j) {
        this.f5446 = zzuh;
        this.f5448 = zzva;
        this.f5447 = str;
        this.f5445 = zzuk;
        this.f5449 = i;
        this.f5443 = zzvg;
        this.f5444 = j;
    }
}
