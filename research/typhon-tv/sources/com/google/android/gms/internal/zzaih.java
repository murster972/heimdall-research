package com.google.android.gms.internal;

import android.content.DialogInterface;

final class zzaih implements DialogInterface.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f8219;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzaig f8220;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f8221;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ int f8222;

    zzaih(zzaig zzaig, int i, int i2, int i3) {
        this.f8220 = zzaig;
        this.f8222 = i;
        this.f8219 = i2;
        this.f8221 = i3;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == this.f8222) {
            this.f8220.m4679();
            return;
        }
        if (i == this.f8219) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5029)).booleanValue()) {
                this.f8220.m4683();
                return;
            }
        }
        if (i == this.f8221) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5030)).booleanValue()) {
                this.f8220.m4682();
            }
        }
    }
}
