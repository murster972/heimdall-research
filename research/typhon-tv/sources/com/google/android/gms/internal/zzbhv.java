package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import java.util.Map;
import java.util.TreeMap;

final class zzbhv extends zzbhs {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbhu f8780;

    zzbhv(zzbhu zzbhu) {
        this.f8780 = zzbhu;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m10271(Status status, zzbif zzbif) {
        if (zzbif.m10284() == 6502 || zzbif.m10284() == 6507) {
            this.f8780.m4198(new zzbhw(zzbhq.m10256(zzbif.m10284()), zzbhq.m10258(zzbif), zzbif.m10283(), zzbhq.m10260(zzbif)));
        } else {
            this.f8780.m4198(new zzbhw(zzbhq.m10256(zzbif.m10284()), (Map<String, TreeMap<String, byte[]>>) zzbhq.m10258(zzbif), zzbhq.m10260(zzbif)));
        }
    }
}
