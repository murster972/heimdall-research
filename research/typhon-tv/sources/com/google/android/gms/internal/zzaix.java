package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;

final class zzaix implements zzakg<Throwable, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzajb f8240;

    zzaix(zzaiv zzaiv, zzajb zzajb) {
        this.f8240 = zzajb;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzakv m9643(Object obj) throws Exception {
        Throwable th = (Throwable) obj;
        zzagf.m4793("Error occurred while dispatching http response in getter.", th);
        zzbs.zzem().m4505(th, "HttpGetter.deliverResponse.1");
        return zzakl.m4802(this.f8240.m4715());
    }
}
