package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class zzpn extends zzev implements zzpm {
    public zzpn() {
        attachInterface(this, "com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzpm m13220(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
        return queryLocalInterface instanceof zzpm ? (zzpm) queryLocalInterface : new zzpo(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 2:
                String r1 = m13219();
                parcel2.writeNoException();
                parcel2.writeString(r1);
                return true;
            case 3:
                List<zzpq> r12 = m13218();
                parcel2.writeNoException();
                parcel2.writeList(r12);
                return true;
            default:
                return false;
        }
    }
}
