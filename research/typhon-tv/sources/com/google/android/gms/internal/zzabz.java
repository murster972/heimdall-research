package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

@zzzv
public final class zzabz {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f3894;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f3895;

    /* renamed from: ʽ  reason: contains not printable characters */
    public AdvertisingIdClient.Info f3896;

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f3897;

    /* renamed from: ˑ  reason: contains not printable characters */
    public zzaat f3898;

    /* renamed from: ٴ  reason: contains not printable characters */
    public zzaco f3899;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public JSONObject f3900 = new JSONObject();

    /* renamed from: 连任  reason: contains not printable characters */
    public zzacy f3901;

    /* renamed from: 靐  reason: contains not printable characters */
    public Bundle f3902;

    /* renamed from: 麤  reason: contains not printable characters */
    public Location f3903;

    /* renamed from: 齉  reason: contains not printable characters */
    public List<String> f3904 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    public Bundle f3905;
}
