package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbia implements Parcelable.Creator<zzbhz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        byte[] bArr = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    bArr = zzbfn.m10154(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzbhz(bArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbhz[i];
    }
}
