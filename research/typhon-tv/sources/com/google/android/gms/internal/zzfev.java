package com.google.android.gms.internal;

final class zzfev extends zzfez {
    private final int zzpfj;
    private final int zzpfk;

    zzfev(byte[] bArr, int i, int i2) {
        super(bArr);
        m12370(i, i + i2, bArr.length);
        this.zzpfj = i;
        this.zzpfk = i2;
    }

    public final int size() {
        return this.zzpfk;
    }

    public final byte zzkn(int i) {
        m12374(i, size());
        return this.zzjng[this.zzpfj + i];
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final int m12383() {
        return this.zzpfj;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m12384(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.zzjng, m12391() + i, bArr, i2, i3);
    }
}
