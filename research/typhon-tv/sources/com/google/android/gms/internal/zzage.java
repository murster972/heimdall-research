package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;

final class zzage extends zzagb {

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f8154;

    zzage(Context context) {
        this.f8154 = context;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9579() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9580() {
        boolean z;
        try {
            z = AdvertisingIdClient.getIsAdIdFakeForDebugLogging(this.f8154);
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e) {
            zzagf.m4793("Fail to get isAdIdFakeForDebugLogging", e);
            z = false;
        }
        zzajv.m4782(z);
        zzagf.m4791(new StringBuilder(43).append("Update ad debug logging enablement as ").append(z).toString());
    }
}
