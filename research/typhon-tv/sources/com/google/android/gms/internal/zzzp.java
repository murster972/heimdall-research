package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import com.mopub.common.TyphoonApp;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;

@zzzv
public final class zzzp implements zzzt {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzzt f5630 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f5631 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ExecutorService f5632 = Executors.newCachedThreadPool();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzakd f5633;

    /* renamed from: 连任  reason: contains not printable characters */
    private final WeakHashMap<Thread, Boolean> f5634 = new WeakHashMap<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f5635;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Object f5636 = new Object();

    private zzzp(Context context) {
        this.f5635 = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.f5633 = zzakd.m4800();
        Thread thread = Looper.getMainLooper().getThread();
        if (thread != null) {
            synchronized (this.f5636) {
                this.f5634.put(thread, true);
            }
            thread.setUncaughtExceptionHandler(new zzzr(this, thread.getUncaughtExceptionHandler()));
        }
        Thread.setDefaultUncaughtExceptionHandler(new zzzq(this, Thread.getDefaultUncaughtExceptionHandler()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Uri.Builder m6094(String str, String str2, String str3, int i) {
        boolean z = false;
        try {
            z = zzbhf.m10231(this.f5635).m10227();
        } catch (Throwable th) {
            zzakb.m4793("Error fetching instant app info", th);
        }
        String str4 = EnvironmentCompat.MEDIA_UNKNOWN;
        try {
            str4 = this.f5635.getPackageName();
        } catch (Throwable th2) {
            zzakb.m4791("Cannot obtain package name, proceeding.");
        }
        Uri.Builder appendQueryParameter = new Uri.Builder().scheme(TyphoonApp.HTTPS).path("=").appendQueryParameter("is_aia", Boolean.toString(z)).appendQueryParameter("id", "gmob-apps-report-exception").appendQueryParameter(PubnativeRequest.Parameters.OS, Build.VERSION.RELEASE).appendQueryParameter("api", String.valueOf(Build.VERSION.SDK_INT));
        String str5 = Build.MANUFACTURER;
        String str6 = Build.MODEL;
        if (!str6.startsWith(str5)) {
            str6 = new StringBuilder(String.valueOf(str5).length() + 1 + String.valueOf(str6).length()).append(str5).append(StringUtils.SPACE).append(str6).toString();
        }
        return appendQueryParameter.appendQueryParameter("device", str6).appendQueryParameter("js", this.f5633.f4297).appendQueryParameter("appid", str4).appendQueryParameter("exceptiontype", str).appendQueryParameter("stacktrace", str2).appendQueryParameter("eids", TextUtils.join(",", zznh.m5598())).appendQueryParameter("exceptionkey", str3).appendQueryParameter("cl", "179146524").appendQueryParameter("rc", "dev").appendQueryParameter("session_id", zzkb.m5486()).appendQueryParameter("sampling_rate", Integer.toString(1)).appendQueryParameter("pb_tm", String.valueOf(zzkb.m5481().m5595(zznh.f5044)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzzt m6095(Context context) {
        synchronized (f5631) {
            if (f5630 == null) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f5156)).booleanValue()) {
                    f5630 = new zzzp(context);
                } else {
                    f5630 = new zzzu();
                }
            }
        }
        return f5630;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m6096(List<String> list) {
        for (String zzzs : list) {
            this.f5632.submit(new zzzs(this, new zzakc(), zzzs));
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m6097(java.lang.Thread r11, java.lang.Throwable r12) {
        /*
            r10 = this;
            r1 = 1
            r3 = 0
            if (r12 == 0) goto L_0x0084
            r2 = r3
            r0 = r3
            r5 = r12
        L_0x0007:
            if (r5 == 0) goto L_0x003a
            java.lang.StackTraceElement[] r6 = r5.getStackTrace()
            int r7 = r6.length
            r4 = r3
        L_0x000f:
            if (r4 >= r7) goto L_0x0034
            r8 = r6[r4]
            java.lang.String r9 = r8.getClassName()
            boolean r9 = com.google.android.gms.internal.zzajr.m4753((java.lang.String) r9)
            if (r9 == 0) goto L_0x001e
            r0 = r1
        L_0x001e:
            java.lang.Class r9 = r10.getClass()
            java.lang.String r9 = r9.getName()
            java.lang.String r8 = r8.getClassName()
            boolean r8 = r9.equals(r8)
            if (r8 == 0) goto L_0x0031
            r2 = r1
        L_0x0031:
            int r4 = r4 + 1
            goto L_0x000f
        L_0x0034:
            java.lang.Throwable r4 = r5.getCause()
            r5 = r4
            goto L_0x0007
        L_0x003a:
            if (r0 == 0) goto L_0x0084
            if (r2 != 0) goto L_0x0084
            r0 = r1
        L_0x003f:
            if (r0 == 0) goto L_0x0083
            java.lang.String r2 = ""
            java.lang.Throwable r0 = com.google.android.gms.internal.zzajr.m4762((java.lang.Throwable) r12)
            if (r0 == 0) goto L_0x0083
            java.lang.Class r0 = r12.getClass()
            java.lang.String r4 = r0.getName()
            java.io.StringWriter r0 = new java.io.StringWriter
            r0.<init>()
            java.io.PrintWriter r5 = new java.io.PrintWriter
            r5.<init>(r0)
            com.google.android.gms.internal.zzdvl.m12240(r12, r5)
            java.lang.String r5 = r0.toString()
            double r6 = java.lang.Math.random()
            r8 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r0 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r0 >= 0) goto L_0x0086
            r0 = r1
        L_0x006e:
            if (r0 == 0) goto L_0x0083
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            android.net.Uri$Builder r1 = r10.m6094(r4, r5, r2, r1)
            java.lang.String r1 = r1.toString()
            r0.add(r1)
            r10.m6096((java.util.List<java.lang.String>) r0)
        L_0x0083:
            return
        L_0x0084:
            r0 = r3
            goto L_0x003f
        L_0x0086:
            r0 = r3
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzzp.m6097(java.lang.Thread, java.lang.Throwable):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6098(Throwable th, String str) {
        if (zzajr.m4762(th) != null) {
            String name = th.getClass().getName();
            StringWriter stringWriter = new StringWriter();
            zzdvl.m12240(th, new PrintWriter(stringWriter));
            String stringWriter2 = stringWriter.toString();
            if (Math.random() < 1.0d) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(m6094(name, stringWriter2, str, 1).toString());
                m6096((List<String>) arrayList);
            }
        }
    }
}
