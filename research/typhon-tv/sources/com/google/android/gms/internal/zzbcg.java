package com.google.android.gms.internal;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Status;

final class zzbcg implements Cast.ApplicationConnectionResult {

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f8654;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ApplicationMetadata f8655;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f8656;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f8657;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Status f8658;

    public zzbcg(Status status) {
        this(status, (ApplicationMetadata) null, (String) null, (String) null, false);
    }

    public zzbcg(Status status, ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
        this.f8658 = status;
        this.f8655 = applicationMetadata;
        this.f8657 = str;
        this.f8656 = str2;
        this.f8654 = z;
    }

    public final Status s_() {
        return this.f8658;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m10018() {
        return this.f8657;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m10019() {
        return this.f8654;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m10020() {
        return this.f8656;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ApplicationMetadata m10021() {
        return this.f8655;
    }
}
