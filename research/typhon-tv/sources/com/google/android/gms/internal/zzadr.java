package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzadr extends zzeu implements zzadp {
    zzadr(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m9506() throws RemoteException {
        m12298(6, v_());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9507() throws RemoteException {
        m12298(2, v_());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9508() throws RemoteException {
        m12298(4, v_());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9509() throws RemoteException {
        m12298(3, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9510() throws RemoteException {
        m12298(1, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9511(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(7, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9512(zzadh zzadh) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzadh);
        m12298(5, v_);
    }
}
