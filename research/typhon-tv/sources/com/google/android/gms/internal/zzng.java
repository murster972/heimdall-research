package com.google.android.gms.internal;

import java.util.concurrent.Callable;

final class zzng implements Callable<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zznf f10809;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzmx f10810;

    zzng(zznf zznf, zzmx zzmx) {
        this.f10809 = zznf;
        this.f10810 = zzmx;
    }

    public final T call() {
        return this.f10810.m5584(this.f10809.f4889);
    }
}
