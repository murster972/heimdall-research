package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzmo implements Parcelable.Creator<zzmn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        String str = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 15:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new zzmn(str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzmn[i];
    }
}
