package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.js.zzaj;

final class zzabr implements zzalk<zzaj> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzabq f8063;

    zzabr(zzabq zzabq) {
        this.f8063 = zzabq;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m9450(Object obj) {
        try {
            ((zzaj) obj).zzb("AFMA_getAdapterLessMediationAd", this.f8063.f8062);
        } catch (Exception e) {
            zzagf.m4793("Error requesting an ad url", e);
            zzabo.f3880.zzat(this.f8063.f8060);
        }
    }
}
