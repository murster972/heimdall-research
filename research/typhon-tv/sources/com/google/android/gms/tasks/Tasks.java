package com.google.android.gms.tasks;

import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class Tasks {

    static final class zza implements zzb {

        /* renamed from: 龘  reason: contains not printable characters */
        private final CountDownLatch f11099;

        private zza() {
            this.f11099 = new CountDownLatch(1);
        }

        /* synthetic */ zza(zzo zzo) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m13730() throws InterruptedException {
            this.f11099.await();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m13731(Exception exc) {
            this.f11099.countDown();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m13732(Object obj) {
            this.f11099.countDown();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m13733(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.f11099.await(j, timeUnit);
        }
    }

    interface zzb extends OnFailureListener, OnSuccessListener<Object> {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static <TResult> TResult m13723(Task<TResult> task) throws ExecutionException {
        if (task.m6101()) {
            return task.m6103();
        }
        throw new ExecutionException(task.m6102());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TResult> Task<TResult> m13724(Exception exc) {
        zzn zzn = new zzn();
        zzn.m13758(exc);
        return zzn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TResult> Task<TResult> m13725(TResult tresult) {
        zzn zzn = new zzn();
        zzn.m13759(tresult);
        return zzn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TResult> Task<TResult> m13726(Executor executor, Callable<TResult> callable) {
        zzbq.m9121(executor, (Object) "Executor must not be null");
        zzbq.m9121(callable, (Object) "Callback must not be null");
        zzn zzn = new zzn();
        executor.execute(new zzo(zzn, callable));
        return zzn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TResult> TResult m13727(Task<TResult> task) throws ExecutionException, InterruptedException {
        zzbq.m9119("Must not be called on the main application thread");
        zzbq.m9121(task, (Object) "Task must not be null");
        if (task.m6108()) {
            return m13723(task);
        }
        zza zza2 = new zza((zzo) null);
        m13729((Task<?>) task, (zzb) zza2);
        zza2.m13730();
        return m13723(task);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TResult> TResult m13728(Task<TResult> task, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        zzbq.m9119("Must not be called on the main application thread");
        zzbq.m9121(task, (Object) "Task must not be null");
        zzbq.m9121(timeUnit, (Object) "TimeUnit must not be null");
        if (task.m6108()) {
            return m13723(task);
        }
        zza zza2 = new zza((zzo) null);
        m13729((Task<?>) task, (zzb) zza2);
        if (zza2.m13733(j, timeUnit)) {
            return m13723(task);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m13729(Task<?> task, zzb zzb2) {
        task.m6107(TaskExecutors.f11096, (OnSuccessListener<? super Object>) zzb2);
        task.m6106(TaskExecutors.f11096, (OnFailureListener) zzb2);
    }
}
