package com.google.android.gms.tasks;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

public final class TaskExecutors {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Executor f11096 = new zzm();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Executor f11097 = new zza();

    static final class zza implements Executor {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Handler f11098 = new Handler(Looper.getMainLooper());

        public final void execute(Runnable runnable) {
            this.f11098.post(runnable);
        }
    }
}
