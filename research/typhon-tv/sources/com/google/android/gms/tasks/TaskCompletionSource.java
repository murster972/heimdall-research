package com.google.android.gms.tasks;

public class TaskCompletionSource<TResult> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzn<TResult> f11095 = new zzn<>();

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m13718(Exception exc) {
        return this.f11095.m13750(exc);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m13719(TResult tresult) {
        return this.f11095.m13751(tresult);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Task<TResult> m13720() {
        return this.f11095;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m13721(Exception exc) {
        this.f11095.m13758(exc);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m13722(TResult tresult) {
        this.f11095.m13759(tresult);
    }
}
