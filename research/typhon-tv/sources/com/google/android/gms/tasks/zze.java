package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zze<TResult> implements zzk<TResult> {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f11100 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public OnCompleteListener<TResult> f11101;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Executor f11102;

    public zze(Executor executor, OnCompleteListener<TResult> onCompleteListener) {
        this.f11102 = executor;
        this.f11101 = onCompleteListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13736(Task<TResult> task) {
        synchronized (this.f11100) {
            if (this.f11101 != null) {
                this.f11102.execute(new zzf(this, task));
            }
        }
    }
}
