package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zzi<TResult> implements zzk<TResult> {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f11110 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public OnSuccessListener<? super TResult> f11111;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Executor f11112;

    public zzi(Executor executor, OnSuccessListener<? super TResult> onSuccessListener) {
        this.f11112 = executor;
        this.f11111 = onSuccessListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13742(Task<TResult> task) {
        if (task.m6101()) {
            synchronized (this.f11110) {
                if (this.f11111 != null) {
                    this.f11112.execute(new zzj(this, task));
                }
            }
        }
    }
}
