package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

public abstract class Task<TResult> {
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m6101();

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract Exception m6102();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract TResult m6103();

    /* renamed from: 龘  reason: contains not printable characters */
    public Task<TResult> m6104(OnCompleteListener<TResult> onCompleteListener) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Task<TResult> m6105(Executor executor, OnCompleteListener<TResult> onCompleteListener) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Task<TResult> m6106(Executor executor, OnFailureListener onFailureListener);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Task<TResult> m6107(Executor executor, OnSuccessListener<? super TResult> onSuccessListener);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m6108();
}
