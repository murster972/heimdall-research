package com.google.android.gms.tasks;

import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.Executor;

final class zzn<TResult> extends Task<TResult> {

    /* renamed from: 连任  reason: contains not printable characters */
    private Exception f11118;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzl<TResult> f11119 = new zzl<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private TResult f11120;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f11121;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f11122 = new Object();

    zzn() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m13746() {
        zzbq.m9126(!this.f11121, (Object) "Task is already complete");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final void m13747() {
        synchronized (this.f11122) {
            if (this.f11121) {
                this.f11119.m13744(this);
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m13748() {
        zzbq.m9126(this.f11121, (Object) "Task is not yet complete");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13749() {
        boolean z;
        synchronized (this.f11122) {
            z = this.f11121 && this.f11118 == null;
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13750(Exception exc) {
        boolean z = true;
        zzbq.m9121(exc, (Object) "Exception must not be null");
        synchronized (this.f11122) {
            if (this.f11121) {
                z = false;
            } else {
                this.f11121 = true;
                this.f11118 = exc;
                this.f11119.m13744(this);
            }
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m13751(TResult tresult) {
        boolean z = true;
        synchronized (this.f11122) {
            if (this.f11121) {
                z = false;
            } else {
                this.f11121 = true;
                this.f11120 = tresult;
                this.f11119.m13744(this);
            }
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Exception m13752() {
        Exception exc;
        synchronized (this.f11122) {
            exc = this.f11118;
        }
        return exc;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final TResult m13753() {
        TResult tresult;
        synchronized (this.f11122) {
            m13748();
            if (this.f11118 != null) {
                throw new RuntimeExecutionException(this.f11118);
            }
            tresult = this.f11120;
        }
        return tresult;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Task<TResult> m13754(OnCompleteListener<TResult> onCompleteListener) {
        return m6105(TaskExecutors.f11097, onCompleteListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Task<TResult> m13755(Executor executor, OnCompleteListener<TResult> onCompleteListener) {
        this.f11119.m13745(new zze(executor, onCompleteListener));
        m13747();
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Task<TResult> m13756(Executor executor, OnFailureListener onFailureListener) {
        this.f11119.m13745(new zzg(executor, onFailureListener));
        m13747();
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Task<TResult> m13757(Executor executor, OnSuccessListener<? super TResult> onSuccessListener) {
        this.f11119.m13745(new zzi(executor, onSuccessListener));
        m13747();
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13758(Exception exc) {
        zzbq.m9121(exc, (Object) "Exception must not be null");
        synchronized (this.f11122) {
            m13746();
            this.f11121 = true;
            this.f11118 = exc;
        }
        this.f11119.m13744(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13759(TResult tresult) {
        synchronized (this.f11122) {
            m13746();
            this.f11121 = true;
            this.f11120 = tresult;
        }
        this.f11119.m13744(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13760() {
        boolean z;
        synchronized (this.f11122) {
            z = this.f11121;
        }
        return z;
    }
}
