package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zzg<TResult> implements zzk<TResult> {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f11105 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public OnFailureListener f11106;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Executor f11107;

    public zzg(Executor executor, OnFailureListener onFailureListener) {
        this.f11107 = executor;
        this.f11106 = onFailureListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13739(Task<TResult> task) {
        if (!task.m6101()) {
            synchronized (this.f11105) {
                if (this.f11106 != null) {
                    this.f11107.execute(new zzh(this, task));
                }
            }
        }
    }
}
