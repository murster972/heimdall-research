package com.google.android.gms.ads.formats;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzky;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class PublisherAdViewOptions extends zzbfm {
    public static final Parcelable.Creator<PublisherAdViewOptions> CREATOR = new zzc();

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzkx f3388;

    /* renamed from: 齉  reason: contains not printable characters */
    private AppEventListener f3389;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f3390;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public AppEventListener f6647;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean f6648 = false;

        public final PublisherAdViewOptions build() {
            return new PublisherAdViewOptions(this);
        }

        public final Builder setAppEventListener(AppEventListener appEventListener) {
            this.f6647 = appEventListener;
            return this;
        }

        public final Builder setManualImpressionsEnabled(boolean z) {
            this.f6648 = z;
            return this;
        }
    }

    private PublisherAdViewOptions(Builder builder) {
        this.f3390 = builder.f6648;
        this.f3389 = builder.f6647;
        this.f3388 = this.f3389 != null ? new zzjp(this.f3389) : null;
    }

    PublisherAdViewOptions(boolean z, IBinder iBinder) {
        this.f3390 = z;
        this.f3388 = iBinder != null ? zzky.m13074(iBinder) : null;
    }

    public final AppEventListener getAppEventListener() {
        return this.f3389;
    }

    public final boolean getManualImpressionsEnabled() {
        return this.f3390;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r1 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 1, getManualImpressionsEnabled());
        zzbfp.m10188(parcel, 2, this.f3388 == null ? null : this.f3388.asBinder(), false);
        zzbfp.m10182(parcel, r1);
    }

    public final zzkx zzbn() {
        return this.f3388;
    }
}
