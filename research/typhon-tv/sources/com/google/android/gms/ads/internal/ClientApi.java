package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Keep;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzadd;
import com.google.android.gms.internal.zzadk;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkn;
import com.google.android.gms.internal.zzks;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlg;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpu;
import com.google.android.gms.internal.zzpz;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzxe;
import com.google.android.gms.internal.zzxo;
import com.google.android.gms.internal.zzzv;
import java.util.HashMap;

@Keep
@zzzv
@DynamiteApi
public class ClientApi extends zzlb {
    public zzkn createAdLoaderBuilder(IObjectWrapper iObjectWrapper, String str, zzux zzux, int i) {
        Context context = (Context) zzn.m9307(iObjectWrapper);
        zzbs.zzei();
        return new zzaj(context, str, zzux, new zzakd(11910000, i, true, zzahn.m4566(context)), zzv.zzc(context));
    }

    public zzxe createAdOverlay(IObjectWrapper iObjectWrapper) {
        return new zzd((Activity) zzn.m9307(iObjectWrapper));
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [com.google.android.gms.ads.internal.zzx, com.google.android.gms.internal.zzks] */
    public zzks createBannerAdManager(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, zzux zzux, int i) throws RemoteException {
        Context context = (Context) zzn.m9307(iObjectWrapper);
        zzbs.zzei();
        return new zzx(context, zzjn, str, zzux, new zzakd(11910000, i, true, zzahn.m4566(context)), zzv.zzc(context));
    }

    public zzxo createInAppPurchaseManager(IObjectWrapper iObjectWrapper) {
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0036, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4924)).booleanValue() == false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x004a, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4925)).booleanValue() != false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x004c, code lost:
        r1 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.gms.internal.zzks createInterstitialAdManager(com.google.android.gms.dynamic.IObjectWrapper r14, com.google.android.gms.internal.zzjn r15, java.lang.String r16, com.google.android.gms.internal.zzux r17, int r18) throws android.os.RemoteException {
        /*
            r13 = this;
            java.lang.Object r2 = com.google.android.gms.dynamic.zzn.m9307((com.google.android.gms.dynamic.IObjectWrapper) r14)
            android.content.Context r2 = (android.content.Context) r2
            com.google.android.gms.internal.zznh.m5599(r2)
            com.google.android.gms.internal.zzakd r5 = new com.google.android.gms.internal.zzakd
            r1 = 11910000(0xb5bb70, float:1.6689465E-38)
            r3 = 1
            com.google.android.gms.ads.internal.zzbs.zzei()
            boolean r4 = com.google.android.gms.internal.zzahn.m4566(r2)
            r0 = r18
            r5.<init>(r1, r0, r3, r4)
            java.lang.String r1 = "reward_mb"
            java.lang.String r3 = r15.f4798
            boolean r3 = r1.equals(r3)
            if (r3 != 0) goto L_0x0038
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r1 = com.google.android.gms.internal.zznh.f4924
            com.google.android.gms.internal.zznf r4 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r1 = r4.m5595(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 != 0) goto L_0x004c
        L_0x0038:
            if (r3 == 0) goto L_0x005d
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r1 = com.google.android.gms.internal.zznh.f4925
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r1 = r3.m5595(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x005d
        L_0x004c:
            r1 = 1
        L_0x004d:
            if (r1 == 0) goto L_0x005f
            com.google.android.gms.internal.zztl r1 = new com.google.android.gms.internal.zztl
            com.google.android.gms.ads.internal.zzv r6 = com.google.android.gms.ads.internal.zzv.zzc(r2)
            r3 = r16
            r4 = r17
            r1.<init>(r2, r3, r4, r5, r6)
        L_0x005c:
            return r1
        L_0x005d:
            r1 = 0
            goto L_0x004d
        L_0x005f:
            com.google.android.gms.ads.internal.zzak r6 = new com.google.android.gms.ads.internal.zzak
            com.google.android.gms.ads.internal.zzv r12 = com.google.android.gms.ads.internal.zzv.zzc(r2)
            r7 = r2
            r8 = r15
            r9 = r16
            r10 = r17
            r11 = r5
            r6.<init>(r7, r8, r9, r10, r11, r12)
            r1 = r6
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.ClientApi.createInterstitialAdManager(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.zzjn, java.lang.String, com.google.android.gms.internal.zzux, int):com.google.android.gms.internal.zzks");
    }

    public zzpu createNativeAdViewDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) {
        return new zzpg((FrameLayout) zzn.m9307(iObjectWrapper), (FrameLayout) zzn.m9307(iObjectWrapper2));
    }

    public zzpz createNativeAdViewHolderDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) {
        return new zzpj((View) zzn.m9307(iObjectWrapper), (HashMap) zzn.m9307(iObjectWrapper2), (HashMap) zzn.m9307(iObjectWrapper3));
    }

    public zzadk createRewardedVideoAd(IObjectWrapper iObjectWrapper, zzux zzux, int i) {
        Context context = (Context) zzn.m9307(iObjectWrapper);
        zzbs.zzei();
        return new zzadd(context, zzv.zzc(context), zzux, new zzakd(11910000, i, true, zzahn.m4566(context)));
    }

    public zzks createSearchAdManager(IObjectWrapper iObjectWrapper, zzjn zzjn, String str, int i) throws RemoteException {
        Context context = (Context) zzn.m9307(iObjectWrapper);
        zzbs.zzei();
        return new zzbm(context, zzjn, str, new zzakd(11910000, i, true, zzahn.m4566(context)));
    }

    public zzlg getMobileAdsSettingsManager(IObjectWrapper iObjectWrapper) {
        return null;
    }

    public zzlg getMobileAdsSettingsManagerWithClientJarVersion(IObjectWrapper iObjectWrapper, int i) {
        Context context = (Context) zzn.m9307(iObjectWrapper);
        zzbs.zzei();
        return zzaw.zza(context, new zzakd(11910000, i, true, zzahn.m4566(context)));
    }
}
