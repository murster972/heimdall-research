package com.google.android.gms.ads.mediation;

import android.os.Bundle;

public interface MediationAdapter {

    public static class zza {

        /* renamed from: 龘  reason: contains not printable characters */
        private int f6809;

        public final zza zzah(int i) {
            this.f6809 = 1;
            return this;
        }

        public final Bundle zzuk() {
            Bundle bundle = new Bundle();
            bundle.putInt("capabilities", this.f6809);
            return bundle;
        }
    }

    void onDestroy();

    void onPause();

    void onResume();
}
