package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ViewSwitcher;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzaig;
import com.google.android.gms.internal.zzajq;
import com.google.android.gms.internal.zzanh;
import java.util.ArrayList;

public final class zzbu extends ViewSwitcher {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzajq f6779;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f6780 = true;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaig f6781;

    public zzbu(Context context, String str, String str2, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener onScrollChangedListener) {
        super(context);
        this.f6781 = new zzaig(context);
        this.f6781.m4693(str);
        this.f6781.m4688(str2);
        if (context instanceof Activity) {
            this.f6779 = new zzajq((Activity) context, this, onGlobalLayoutListener, onScrollChangedListener);
        } else {
            this.f6779 = new zzajq((Activity) null, this, onGlobalLayoutListener, onScrollChangedListener);
        }
        this.f6779.m4742();
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f6779 != null) {
            this.f6779.m4741();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f6779 != null) {
            this.f6779.m4740();
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.f6780) {
            return false;
        }
        this.f6781.m4692(motionEvent);
        return false;
    }

    public final void removeAllViews() {
        int i = 0;
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt != null && (childAt instanceof zzanh)) {
                arrayList.add((zzanh) childAt);
            }
        }
        super.removeAllViews();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            ((zzanh) obj).destroy();
        }
    }

    public final zzaig zzfn() {
        return this.f6781;
    }

    public final void zzfo() {
        zzagf.m4527("Disable position monitoring on adFrame.");
        if (this.f6779 != null) {
            this.f6779.m4739();
        }
    }

    public final void zzfp() {
        zzagf.m4527("Enable debug gesture detector on adFrame.");
        this.f6780 = true;
    }

    public final void zzfq() {
        zzagf.m4527("Disable debug gesture detector on adFrame.");
        this.f6780 = false;
    }
}
