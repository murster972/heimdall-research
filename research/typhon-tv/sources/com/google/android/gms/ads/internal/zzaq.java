package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import com.Pinkamena;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzafo;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzanm;
import com.google.android.gms.internal.zzll;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzoj;
import com.google.android.gms.internal.zzol;
import com.google.android.gms.internal.zzpq;
import com.google.android.gms.internal.zzpr;
import com.google.android.gms.internal.zzuo;
import com.google.android.gms.internal.zzvj;
import com.google.android.gms.internal.zzvm;
import com.google.android.gms.internal.zzzv;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzaq {
    public static boolean zza(zzanh zzanh, zzuo zzuo, CountDownLatch countDownLatch) {
        boolean z;
        if (zzanh == null) {
            try {
                throw null;
            } catch (RemoteException e) {
                zzagf.m4796("Unable to invoke load assets", e);
                z = false;
            } catch (RuntimeException e2) {
                countDownLatch.countDown();
                throw e2;
            }
        } else {
            View view = (View) zzanh;
            if (view == null) {
                zzagf.m4791("AdWebView is null");
                z = false;
            } else {
                view.setVisibility(4);
                List<String> list = zzuo.f5446.f5420;
                if (list == null || list.isEmpty()) {
                    zzagf.m4791("No template ids present in mediation response");
                    z = false;
                } else {
                    zzanh.m4980().m5053("/nativeExpressAssetsLoaded", (zzt<? super zzanh>) new zzat(countDownLatch));
                    zzanh.m4980().m5053("/nativeExpressAssetsLoadingFailed", (zzt<? super zzanh>) new zzau(countDownLatch));
                    zzvj r16 = zzuo.f5448.m13457();
                    zzvm r13 = zzuo.f5448.m13461();
                    if (list.contains("2") && r16 != null) {
                        zzanh.m4980().m5048((zzanm) new zzar(new zzoj(r16.m13547(), r16.m13542(), r16.m13545(), r16.m13544(), r16.m13541(), r16.m13531(), r16.m13532(), r16.m13533(), (zzog) null, r16.m13536(), (zzll) null, r16.m13550() != null ? (View) zzn.m9307(r16.m13550()) : null, r16.m13537(), (String) null), zzuo.f5446.f5419, zzanh));
                    } else if (!list.contains(PubnativeRequest.LEGACY_ZONE_ID) || r13 == null) {
                        zzagf.m4791("No matching template id and mapper");
                        z = false;
                    } else {
                        zzanh.m4980().m5048((zzanm) new zzas(new zzol(r13.m13586(), r13.m13581(), r13.m13584(), r13.m13583(), r13.m13580(), r13.m13571(), (zzog) null, r13.m13578(), (zzll) null, r13.m13575() != null ? (View) zzn.m9307(r13.m13575()) : null, r13.m13588(), (String) null), zzuo.f5446.f5419, zzanh));
                    }
                    String str = zzuo.f5446.f5405;
                    if (zzuo.f5446.f5406 != null) {
                        zzanh zzanh2 = zzanh;
                        Pinkamena.DianePie();
                    } else {
                        zzanh zzanh3 = zzanh;
                        Pinkamena.DianePie();
                    }
                    z = true;
                }
            }
            if (!z) {
                countDownLatch.countDown();
            }
            return z;
        }
    }

    public static View zze(zzafo zzafo) {
        if (zzafo == null) {
            zzagf.m4795("AdState is null");
            return null;
        } else if (!zzf(zzafo) || zzafo.f4119 == null) {
            try {
                IObjectWrapper r0 = zzafo.f4124 != null ? zzafo.f4124.m13468() : null;
                if (r0 != null) {
                    return (View) zzn.m9307(r0);
                }
                zzagf.m4791("View in mediation adapter is null.");
                return null;
            } catch (RemoteException e) {
                zzagf.m4796("Could not get View from mediation adapter.", e);
                return null;
            }
        } else {
            zzanh zzanh = zzafo.f4119;
            if (zzanh != null) {
                return (View) zzanh;
            }
            throw null;
        }
    }

    public static boolean zzf(zzafo zzafo) {
        return (zzafo == null || !zzafo.f4094 || zzafo.f4096 == null || zzafo.f4096.f5405 == null) ? false : true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static zzpq m4053(Object obj) {
        if (obj instanceof IBinder) {
            return zzpr.m13226((IBinder) obj);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m4054(zzpq zzpq) {
        try {
            IObjectWrapper r0 = zzpq.m13225();
            if (r0 == null) {
                zzagf.m4791("Drawable is null. Returning empty string");
                return "";
            }
            Drawable drawable = (Drawable) zzn.m9307(r0);
            if (drawable instanceof BitmapDrawable) {
                return m4059(((BitmapDrawable) drawable).getBitmap());
            }
            zzagf.m4791("Drawable is not an instance of BitmapDrawable. Returning empty string");
            return "";
        } catch (RemoteException e) {
            zzagf.m4791("Unable to get drawable. Returning empty string");
            return "";
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static JSONObject m4055(Bundle bundle, String str) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (bundle == null || TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str2 = (String) keys.next();
            if (bundle.containsKey(str2)) {
                if ("image".equals(jSONObject2.getString(str2))) {
                    Object obj = bundle.get(str2);
                    if (obj instanceof Bitmap) {
                        jSONObject.put(str2, (Object) m4059((Bitmap) obj));
                    } else {
                        zzagf.m4791("Invalid type. An image type extra should return a bitmap");
                    }
                } else if (bundle.get(str2) instanceof Bitmap) {
                    zzagf.m4791("Invalid asset type. Bitmap should be returned only for image type");
                } else {
                    jSONObject.put(str2, (Object) String.valueOf(bundle.get(str2)));
                }
            }
        }
        return jSONObject;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4056(zzanh zzanh) {
        View.OnClickListener r0 = zzanh.m4990();
        if (r0 == null) {
            return;
        }
        if (zzanh == null) {
            throw null;
        }
        r0.onClick((View) zzanh);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzt<zzanh> m4057(zzvj zzvj, zzvm zzvm, zzab zzab) {
        return new zzav(zzvj, zzab, zzvm);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m4059(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap == null) {
            zzagf.m4791("Bitmap is null. Returning empty string");
            return "";
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        String valueOf = String.valueOf("data:image/png;base64,");
        String valueOf2 = String.valueOf(encodeToString);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m4060(zzpq zzpq) {
        if (zzpq == null) {
            zzagf.m4791("Image is null. Returning empty string");
            return "";
        }
        try {
            Uri r0 = zzpq.m13223();
            if (r0 != null) {
                return r0.toString();
            }
        } catch (RemoteException e) {
            zzagf.m4791("Unable to get image uri. Trying data uri next");
        }
        return m4054(zzpq);
    }
}
