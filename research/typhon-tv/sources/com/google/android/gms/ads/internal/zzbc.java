package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.android.gms.internal.zzaat;
import com.google.android.gms.internal.zzaau;
import com.google.android.gms.internal.zzafp;
import com.google.android.gms.internal.zzakl;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zzou;
import java.util.concurrent.Callable;
import org.json.JSONArray;
import org.json.JSONObject;

final class zzbc implements Callable<zzou> {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ zzba f6750;

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ JSONArray f6751;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzafp f6752;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f6753;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ int f6754;

    zzbc(zzba zzba, int i, JSONArray jSONArray, int i2, zzafp zzafp) {
        this.f6750 = zzba;
        this.f6754 = i;
        this.f6751 = jSONArray;
        this.f6753 = i2;
        this.f6752 = zzafp;
    }

    public final /* synthetic */ Object call() throws Exception {
        if (this.f6754 >= this.f6751.length()) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.f6751.get(this.f6754));
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("ads", (Object) jSONArray);
        zzba zzba = new zzba(this.f6750.连任.zzair, this.f6750.ʽ, this.f6750.连任.zzauc, this.f6750.连任.zzatw, this.f6750.ˑ, this.f6750.连任.zzaty, true);
        zzba.龘(this.f6750, this.f6750.连任, zzba.连任);
        zzba.ʻ();
        zzba.zza(this.f6750.靐);
        zznu zznu = zzba.龘;
        int i = this.f6754;
        zznu.m5629("num_ads_requested", String.valueOf(this.f6753));
        zznu.m5629("ad_index", String.valueOf(i));
        zzaat zzaat = this.f6752.f4136;
        String jSONObject2 = jSONObject.toString();
        Bundle bundle = zzaat.f3763.f4767 != null ? new Bundle(zzaat.f3763.f4767) : new Bundle();
        bundle.putString("_ad", jSONObject2);
        zzba.zza(new zzaau(zzaat.f3761, new zzjj(zzaat.f3763.f4768, zzaat.f3763.f4765, bundle, zzaat.f3763.f4766, zzaat.f3763.f4764, zzaat.f3763.f4753, zzaat.f3763.f4754, zzaat.f3763.f4755, zzaat.f3763.f4761, zzaat.f3763.f4762, zzaat.f3763.f4763, zzaat.f3763.f4758, zzaat.f3763.f4756, zzaat.f3763.f4757, zzaat.f3763.f4769, zzaat.f3763.f4770, zzaat.f3763.f4759, zzaat.f3763.f4760), zzaat.f3762, zzaat.f3760, zzaat.f3716, zzaat.f3718, zzaat.f3740, zzaat.f3746, zzaat.f3748, zzaat.f3728, zzaat.f3724, zzaat.f3752, zzaat.f3767, zzaat.f3768, zzaat.f3732, zzaat.f3734, zzaat.f3736, zzaat.f3726, zzaat.f3730, zzaat.f3738, zzaat.f3742, zzaat.f3744, zzaat.f3750, zzaat.f3756, zzaat.f3758, zzaat.f3721, zzaat.f3765, zzaat.f3769, zzaat.f3749, zzaat.f3751, zzakl.m4802(zzaat.f3717), zzaat.f3719, zzaat.f3725, zzaat.f3723, zzaat.f3745, zzaat.f3727, zzaat.f3731, zzaat.f3729, zzaat.f3735, zzaat.f3741, zzakl.m4802(zzaat.f3720), zzaat.f3755, zzaat.f3743, zzaat.f3753, 1, zzaat.f3759, zzaat.f3766, zzaat.f3747), zzba.龘);
        return (zzou) zzba.ʼ().get();
    }
}
