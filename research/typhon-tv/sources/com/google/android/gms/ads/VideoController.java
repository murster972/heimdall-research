package com.google.android.gms.ads;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzll;
import com.google.android.gms.internal.zzlo;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class VideoController {
    public static final int PLAYBACK_STATE_ENDED = 3;
    public static final int PLAYBACK_STATE_PAUSED = 2;
    public static final int PLAYBACK_STATE_PLAYING = 1;
    public static final int PLAYBACK_STATE_READY = 5;
    public static final int PLAYBACK_STATE_UNKNOWN = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private zzll f3370;

    /* renamed from: 齉  reason: contains not printable characters */
    private VideoLifecycleCallbacks f3371;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f3372 = new Object();

    public static abstract class VideoLifecycleCallbacks {
        public void onVideoEnd() {
        }

        public void onVideoMute(boolean z) {
        }

        public void onVideoPause() {
        }

        public void onVideoPlay() {
        }

        public void onVideoStart() {
        }
    }

    public final float getAspectRatio() {
        float f = 0.0f;
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    f = this.f3370.m13087();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call getAspectRatio on video controller.", e);
                }
            }
        }
        return f;
    }

    public final int getPlaybackState() {
        int i = 0;
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    i = this.f3370.m13089();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call getPlaybackState on video controller.", e);
                }
            }
        }
        return i;
    }

    public final VideoLifecycleCallbacks getVideoLifecycleCallbacks() {
        VideoLifecycleCallbacks videoLifecycleCallbacks;
        synchronized (this.f3372) {
            videoLifecycleCallbacks = this.f3371;
        }
        return videoLifecycleCallbacks;
    }

    public final boolean hasVideoContent() {
        boolean z;
        synchronized (this.f3372) {
            z = this.f3370 != null;
        }
        return z;
    }

    public final boolean isClickToExpandEnabled() {
        boolean z = false;
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    z = this.f3370.m13086();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call isClickToExpandEnabled.", e);
                }
            }
        }
        return z;
    }

    public final boolean isCustomControlsEnabled() {
        boolean z = false;
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    z = this.f3370.m13085();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call isUsingCustomPlayerControls.", e);
                }
            }
        }
        return z;
    }

    public final boolean isMuted() {
        boolean z = true;
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    z = this.f3370.m13090();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call isMuted on video controller.", e);
                }
            }
        }
        return z;
    }

    public final void mute(boolean z) {
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    this.f3370.m13093(z);
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call mute on video controller.", e);
                }
                return;
            }
            return;
        }
    }

    public final void pause() {
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    this.f3370.m13088();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call pause on video controller.", e);
                }
                return;
            }
            return;
        }
    }

    public final void play() {
        synchronized (this.f3372) {
            if (this.f3370 != null) {
                try {
                    this.f3370.m13091();
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call play on video controller.", e);
                }
                return;
            }
            return;
        }
    }

    public final void setVideoLifecycleCallbacks(VideoLifecycleCallbacks videoLifecycleCallbacks) {
        zzbq.m9121(videoLifecycleCallbacks, (Object) "VideoLifecycleCallbacks may not be null.");
        synchronized (this.f3372) {
            this.f3371 = videoLifecycleCallbacks;
            if (this.f3370 != null) {
                try {
                    this.f3370.m13092((zzlo) new zzmq(videoLifecycleCallbacks));
                } catch (RemoteException e) {
                    zzakb.m4793("Unable to call setVideoLifecycleCallbacks on video controller.", e);
                }
                return;
            }
            return;
        }
    }

    public final void zza(zzll zzll) {
        synchronized (this.f3372) {
            this.f3370 = zzll;
            if (this.f3371 != null) {
                setVideoLifecycleCallbacks(this.f3371);
            }
        }
    }

    public final zzll zzbj() {
        zzll zzll;
        synchronized (this.f3372) {
            zzll = this.f3370;
        }
        return zzll;
    }
}
