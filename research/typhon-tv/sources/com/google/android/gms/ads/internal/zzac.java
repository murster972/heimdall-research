package com.google.android.gms.ads.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.internal.zzafs;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzakj;
import com.google.android.gms.internal.zzakl;
import com.google.android.gms.internal.zzakv;
import com.google.android.gms.internal.zzala;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zztp;
import com.google.android.gms.internal.zztu;
import com.google.android.gms.internal.zzzv;
import org.json.JSONObject;

@zzzv
public final class zzac {

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f3466;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f3467 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f3468 = new Object();

    public final void zza(Context context, zzakd zzakd, String str, Runnable runnable) {
        m4037(context, zzakd, true, (zzafs) null, str, (String) null, runnable);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ zzakv m4036(JSONObject jSONObject) throws Exception {
        if (!jSONObject.optBoolean("isSuccessful", false)) {
            return zzakl.m4802(null);
        }
        return zzbs.zzem().m4493(this.f3466, jSONObject.getString("appSettingsJson"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4037(Context context, zzakd zzakd, boolean z, zzafs zzafs, String str, String str2, Runnable runnable) {
        boolean z2;
        if (zzbs.zzeo().m9241() - this.f3467 < DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) {
            zzagf.m4791("Not retrying to fetch app settings");
            return;
        }
        this.f3467 = zzbs.zzeo().m9241();
        if (zzafs == null) {
            z2 = true;
        } else {
            z2 = (((zzbs.zzeo().m9243() - zzafs.m4462()) > ((Long) zzkb.m5481().m5595(zznh.f5006)).longValue() ? 1 : ((zzbs.zzeo().m9243() - zzafs.m4462()) == ((Long) zzkb.m5481().m5595(zznh.f5006)).longValue() ? 0 : -1)) > 0) || !zzafs.m4459();
        }
        if (!z2) {
            return;
        }
        if (context == null) {
            zzagf.m4791("Context not provided to fetch application settings");
        } else if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext == null) {
                applicationContext = context;
            }
            this.f3466 = applicationContext;
            zztp<I, O> r0 = zzbs.zzev().m5869(this.f3466, zzakd).m5871("google.afma.config.fetchAppSettings", zztu.f5394, zztu.f5394);
            try {
                JSONObject jSONObject = new JSONObject();
                if (!TextUtils.isEmpty(str)) {
                    jSONObject.put("app_id", (Object) str);
                } else if (!TextUtils.isEmpty(str2)) {
                    jSONObject.put("ad_unit_id", (Object) str2);
                }
                jSONObject.put("is_init", z);
                jSONObject.put("pn", (Object) context.getPackageName());
                zzakv<O> r02 = r0.m5868(jSONObject);
                zzakv<B> r1 = zzakl.m4804(r02, new zzad(this), zzala.f4304);
                if (runnable != null) {
                    r02.m9670(runnable, zzala.f4304);
                }
                zzakj.m4801(r1, "ConfigLoader.maybeFetchNewAppSettings");
            } catch (Exception e) {
                zzagf.m4793("Error requesting application settings", e);
            }
        } else {
            zzagf.m4791("App settings could not be fetched. Required parameters missing");
        }
    }
}
