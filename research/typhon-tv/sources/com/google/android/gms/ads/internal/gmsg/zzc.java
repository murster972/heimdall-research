package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zzzv;
import java.util.Map;

@zzzv
public final class zzc implements zzt<zzanh> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        String str = (String) map.get("action");
        if ("tick".equals(str)) {
            String str2 = (String) map.get("label");
            String str3 = (String) map.get("start_label");
            String str4 = (String) map.get("timestamp");
            if (TextUtils.isEmpty(str2)) {
                zzagf.m4791("No label given for CSI tick.");
            } else if (TextUtils.isEmpty(str4)) {
                zzagf.m4791("No timestamp given for CSI tick.");
            } else {
                try {
                    long parseLong = (Long.parseLong(str4) - zzbs.zzeo().m9243()) + zzbs.zzeo().m9241();
                    if (TextUtils.isEmpty(str3)) {
                        str3 = "native:view_load";
                    }
                    zzanh.m4974().m5621(str2, str3, parseLong);
                } catch (NumberFormatException e) {
                    zzagf.m4796("Malformed timestamp for CSI tick.", e);
                }
            }
        } else if ("experiment".equals(str)) {
            String str5 = (String) map.get("value");
            if (TextUtils.isEmpty(str5)) {
                zzagf.m4791("No value given for CSI experiment.");
                return;
            }
            zznu r1 = zzanh.m4974().m5619();
            if (r1 == null) {
                zzagf.m4791("No ticker for WebView, dropping experiment ID.");
            } else {
                r1.m5629("e", str5);
            }
        } else if ("extra".equals(str)) {
            String str6 = (String) map.get("name");
            String str7 = (String) map.get("value");
            if (TextUtils.isEmpty(str7)) {
                zzagf.m4791("No value given for CSI extra.");
            } else if (TextUtils.isEmpty(str6)) {
                zzagf.m4791("No name given for CSI extra.");
            } else {
                zznu r2 = zzanh.m4974().m5619();
                if (r2 == null) {
                    zzagf.m4791("No ticker for WebView, dropping extra parameter.");
                } else {
                    r2.m5629(str6, str7);
                }
            }
        }
    }
}
