package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzqm;

final class zzu implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzq f6804;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzqm f6805;

    zzu(zzq zzq, zzqm zzqm) {
        this.f6804 = zzq;
        this.f6805 = zzqm;
    }

    public final void run() {
        try {
            this.f6804.连任.f3583.get(this.f6805.m13319()).m13350(this.f6805);
        } catch (RemoteException e) {
            zzagf.m4796("Could not call onCustomTemplateAdLoadedListener.onCustomTemplateAdLoaded().", e);
        }
    }
}
