package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzali;

final class zzag implements zzali {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzaa f6672;

    zzag(zzae zzae, zzaa zzaa) {
        this.f6672 = zzaa;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7639() {
        zzagf.m4527("Rejecting reference for JS Engine.");
        this.f6672.reject();
    }
}
