package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzalf;
import com.google.android.gms.internal.zzzv;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzzv
public final class zzy implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private HashMap<String, zzalf<JSONObject>> f3423 = new HashMap<>();

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get("request_id");
        String str2 = map.get("fetched_ad");
        zzagf.m4792("Received ad from the cache.");
        zzalf zzalf = this.f3423.get(str);
        if (zzalf == null) {
            zzagf.m4795("Could not find the ad request for the corresponding ad response.");
            return;
        }
        try {
            zzalf.m4822(new JSONObject(str2));
        } catch (JSONException e) {
            zzagf.m4793("Failed constructing JSON object from value passed from javascript", e);
            zzalf.m4822(null);
        } finally {
            this.f3423.remove(str);
        }
    }

    public final Future<JSONObject> zzas(String str) {
        zzalf zzalf = new zzalf();
        this.f3423.put(str, zzalf);
        return zzalf;
    }

    public final void zzat(String str) {
        zzalf zzalf = this.f3423.get(str);
        if (zzalf == null) {
            zzagf.m4795("Could not find the ad request for the corresponding ad response.");
            return;
        }
        if (!zzalf.isDone()) {
            zzalf.cancel(true);
        }
        this.f3423.remove(str);
    }
}
