package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;

final class zzn implements View.OnTouchListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzl f6795;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzw f6796;

    zzn(zzl zzl, zzw zzw) {
        this.f6795 = zzl;
        this.f6796 = zzw;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.f6796.recordClick();
        if (this.f6795.f6790 == null) {
            return false;
        }
        this.f6795.f6790.m9562();
        return false;
    }
}
