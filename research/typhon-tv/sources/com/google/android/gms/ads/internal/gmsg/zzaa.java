package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.ads.internal.zzw;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzwo;
import com.google.android.gms.internal.zzwr;
import com.google.android.gms.internal.zzwt;
import com.google.android.gms.internal.zzwu;
import com.google.android.gms.internal.zzzv;
import java.util.Map;

@zzzv
public final class zzaa implements zzt<zzanh> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static Map<String, Integer> f3405 = zze.m9246("resize", 1, "playVideo", 2, "storePicture", 3, "createCalendarEvent", 4, "setOrientationProperties", 5, "closeResizedAd", 6);

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzwr f3406;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzw f3407;

    public zzaa(zzw zzw, zzwr zzwr) {
        this.f3407 = zzw;
        this.f3406 = zzwr;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        int intValue = f3405.get((String) map.get("a")).intValue();
        if (intValue == 5 || this.f3407 == null || this.f3407.zzda()) {
            switch (intValue) {
                case 1:
                    this.f3406.m5999((Map<String, String>) map);
                    return;
                case 3:
                    new zzwu(zzanh, map).m6004();
                    return;
                case 4:
                    new zzwo(zzanh, map).m5994();
                    return;
                case 5:
                    new zzwt(zzanh, map).m6002();
                    return;
                case 6:
                    this.f3406.m6000(true);
                    return;
                default:
                    zzagf.m4794("Unknown MRAID command called.");
                    return;
            }
        } else {
            this.f3407.zzt((String) null);
        }
    }
}
