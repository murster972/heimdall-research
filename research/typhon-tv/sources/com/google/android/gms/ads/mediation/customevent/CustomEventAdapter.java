package com.google.android.gms.ads.mediation.customevent;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.Pinkamena;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.zzakb;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter {

    /* renamed from: 靐  reason: contains not printable characters */
    private CustomEventBanner f6824;

    /* renamed from: 麤  reason: contains not printable characters */
    private CustomEventNative f6825;

    /* renamed from: 齉  reason: contains not printable characters */
    private CustomEventInterstitial f6826;

    /* renamed from: 龘  reason: contains not printable characters */
    private View f6827;

    static final class zza implements CustomEventBannerListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MediationBannerListener f6828;

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventAdapter f6829;

        public zza(CustomEventAdapter customEventAdapter, MediationBannerListener mediationBannerListener) {
            this.f6829 = customEventAdapter;
            this.f6828 = mediationBannerListener;
        }

        public final void onAdClicked() {
            zzakb.m4792("Custom event adapter called onAdClicked.");
            this.f6828.onAdClicked(this.f6829);
        }

        public final void onAdClosed() {
            zzakb.m4792("Custom event adapter called onAdClosed.");
            this.f6828.onAdClosed(this.f6829);
        }

        public final void onAdFailedToLoad(int i) {
            zzakb.m4792("Custom event adapter called onAdFailedToLoad.");
            this.f6828.onAdFailedToLoad(this.f6829, i);
        }

        public final void onAdLeftApplication() {
            zzakb.m4792("Custom event adapter called onAdLeftApplication.");
            this.f6828.onAdLeftApplication(this.f6829);
        }

        public final void onAdLoaded(View view) {
            zzakb.m4792("Custom event adapter called onAdLoaded.");
            this.f6829.m7661(view);
            this.f6828.onAdLoaded(this.f6829);
        }

        public final void onAdOpened() {
            zzakb.m4792("Custom event adapter called onAdOpened.");
            this.f6828.onAdOpened(this.f6829);
        }
    }

    class zzb implements CustomEventInterstitialListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MediationInterstitialListener f6830;

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventAdapter f6832;

        public zzb(CustomEventAdapter customEventAdapter, MediationInterstitialListener mediationInterstitialListener) {
            this.f6832 = customEventAdapter;
            this.f6830 = mediationInterstitialListener;
        }

        public final void onAdClicked() {
            zzakb.m4792("Custom event adapter called onAdClicked.");
            this.f6830.onAdClicked(this.f6832);
        }

        public final void onAdClosed() {
            zzakb.m4792("Custom event adapter called onAdClosed.");
            this.f6830.onAdClosed(this.f6832);
        }

        public final void onAdFailedToLoad(int i) {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6830.onAdFailedToLoad(this.f6832, i);
        }

        public final void onAdLeftApplication() {
            zzakb.m4792("Custom event adapter called onAdLeftApplication.");
            this.f6830.onAdLeftApplication(this.f6832);
        }

        public final void onAdLoaded() {
            zzakb.m4792("Custom event adapter called onReceivedAd.");
            this.f6830.onAdLoaded(CustomEventAdapter.this);
        }

        public final void onAdOpened() {
            zzakb.m4792("Custom event adapter called onAdOpened.");
            this.f6830.onAdOpened(this.f6832);
        }
    }

    static class zzc implements CustomEventNativeListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MediationNativeListener f6833;

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventAdapter f6834;

        public zzc(CustomEventAdapter customEventAdapter, MediationNativeListener mediationNativeListener) {
            this.f6834 = customEventAdapter;
            this.f6833 = mediationNativeListener;
        }

        public final void onAdClicked() {
            zzakb.m4792("Custom event adapter called onAdClicked.");
            this.f6833.onAdClicked(this.f6834);
        }

        public final void onAdClosed() {
            zzakb.m4792("Custom event adapter called onAdClosed.");
            this.f6833.onAdClosed(this.f6834);
        }

        public final void onAdFailedToLoad(int i) {
            zzakb.m4792("Custom event adapter called onAdFailedToLoad.");
            this.f6833.onAdFailedToLoad(this.f6834, i);
        }

        public final void onAdImpression() {
            zzakb.m4792("Custom event adapter called onAdImpression.");
            this.f6833.onAdImpression(this.f6834);
        }

        public final void onAdLeftApplication() {
            zzakb.m4792("Custom event adapter called onAdLeftApplication.");
            this.f6833.onAdLeftApplication(this.f6834);
        }

        public final void onAdLoaded(NativeAdMapper nativeAdMapper) {
            zzakb.m4792("Custom event adapter called onAdLoaded.");
            this.f6833.onAdLoaded(this.f6834, nativeAdMapper);
        }

        public final void onAdOpened() {
            zzakb.m4792("Custom event adapter called onAdOpened.");
            this.f6833.onAdOpened(this.f6834);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> T m7660(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            zzakb.m4791(new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length()).append("Could not instantiate custom event adapter: ").append(str).append(". ").append(message).toString());
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7661(View view) {
        this.f6827 = view;
    }

    public final View getBannerView() {
        return this.f6827;
    }

    public final void onDestroy() {
        if (this.f6824 != null) {
            this.f6824.onDestroy();
        }
        if (this.f6826 != null) {
            this.f6826.onDestroy();
        }
        if (this.f6825 != null) {
            this.f6825.onDestroy();
        }
    }

    public final void onPause() {
        if (this.f6824 != null) {
            this.f6824.onPause();
        }
        if (this.f6826 != null) {
            this.f6826.onPause();
        }
        if (this.f6825 != null) {
            this.f6825.onPause();
        }
    }

    public final void onResume() {
        if (this.f6824 != null) {
            this.f6824.onResume();
        }
        if (this.f6826 != null) {
            this.f6826.onResume();
        }
        if (this.f6825 != null) {
            this.f6825.onResume();
        }
    }

    public final void requestBannerAd(Context context, MediationBannerListener mediationBannerListener, Bundle bundle, AdSize adSize, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.f6824 = (CustomEventBanner) m7660(bundle.getString("class_name"));
        if (this.f6824 == null) {
            mediationBannerListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 != null) {
            Bundle bundle3 = bundle2.getBundle(bundle.getString("class_name"));
        }
        CustomEventBanner customEventBanner = this.f6824;
        new zza(this, mediationBannerListener);
        String string = bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD);
        Context context2 = context;
        AdSize adSize2 = adSize;
        MediationAdRequest mediationAdRequest2 = mediationAdRequest;
        Pinkamena.DianePie();
    }

    public final void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.f6826 = (CustomEventInterstitial) m7660(bundle.getString("class_name"));
        if (this.f6826 == null) {
            mediationInterstitialListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 != null) {
            Bundle bundle3 = bundle2.getBundle(bundle.getString("class_name"));
        }
        CustomEventInterstitial customEventInterstitial = this.f6826;
        new zzb(this, mediationInterstitialListener);
        String string = bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD);
        Context context2 = context;
        MediationAdRequest mediationAdRequest2 = mediationAdRequest;
        Pinkamena.DianePie();
    }

    public final void requestNativeAd(Context context, MediationNativeListener mediationNativeListener, Bundle bundle, NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle2) {
        this.f6825 = (CustomEventNative) m7660(bundle.getString("class_name"));
        if (this.f6825 == null) {
            mediationNativeListener.onAdFailedToLoad(this, 0);
        } else {
            this.f6825.requestNativeAd(context, new zzc(this, mediationNativeListener), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), nativeMediationAdRequest, bundle2 == null ? null : bundle2.getBundle(bundle.getString("class_name")));
        }
    }

    public final void showInterstitial() {
        CustomEventInterstitial customEventInterstitial = this.f6826;
        Pinkamena.DianePie();
    }
}
