package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import java.util.Map;

final class zzp implements zzt<zzanh> {
    zzp() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        if (((Boolean) zzkb.m5481().m5595(zznh.f4952)).booleanValue()) {
            zzanh.m5004(!Boolean.parseBoolean((String) map.get("disabled")));
        }
    }
}
