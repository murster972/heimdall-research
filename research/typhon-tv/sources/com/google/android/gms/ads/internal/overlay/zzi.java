package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzi {
    public final int index;
    public final ViewGroup parent;
    public final Context zzair;
    public final ViewGroup.LayoutParams zzcis;

    public zzi(zzanh zzanh) throws zzg {
        this.zzcis = zzanh.getLayoutParams();
        ViewParent parent2 = zzanh.getParent();
        this.zzair = zzanh.m5017();
        if (parent2 == null || !(parent2 instanceof ViewGroup)) {
            throw new zzg("Could not get the parent of the WebView for an overlay.");
        }
        this.parent = (ViewGroup) parent2;
        ViewGroup viewGroup = this.parent;
        if (zzanh == null) {
            throw null;
        }
        this.index = viewGroup.indexOfChild((View) zzanh);
        ViewGroup viewGroup2 = this.parent;
        if (zzanh == null) {
            throw null;
        }
        viewGroup2.removeView((View) zzanh);
        zzanh.m5002(true);
    }
}
