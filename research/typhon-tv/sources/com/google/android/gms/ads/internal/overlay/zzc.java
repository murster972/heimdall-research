package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzc extends zzbfm {
    public static final Parcelable.Creator<zzc> CREATOR = new zzb();
    public final Intent intent;
    public final String mimeType;
    public final String packageName;
    public final String url;
    public final String zzchu;
    public final String zzchv;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f3438;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f3439;

    public zzc(Intent intent2) {
        this((String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, intent2);
    }

    public zzc(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this(str, str2, str3, str4, str5, str6, str7, (Intent) null);
    }

    public zzc(String str, String str2, String str3, String str4, String str5, String str6, String str7, Intent intent2) {
        this.f3439 = str;
        this.url = str2;
        this.mimeType = str3;
        this.packageName = str4;
        this.zzchu = str5;
        this.zzchv = str6;
        this.f3438 = str7;
        this.intent = intent2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f3439, false);
        zzbfp.m10193(parcel, 3, this.url, false);
        zzbfp.m10193(parcel, 4, this.mimeType, false);
        zzbfp.m10193(parcel, 5, this.packageName, false);
        zzbfp.m10193(parcel, 6, this.zzchu, false);
        zzbfp.m10193(parcel, 7, this.zzchv, false);
        zzbfp.m10193(parcel, 8, this.f3438, false);
        zzbfp.m10189(parcel, 9, (Parcelable) this.intent, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
