package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzali;

final class zzx implements zzali {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzn f6706;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzae f6707;

    zzx(zzn zzn, zzae zzae) {
        this.f6706 = zzn;
        this.f6707 = zzae;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7645() {
        synchronized (this.f6706.f3437) {
            int unused = this.f6706.f3432 = 1;
            zzagf.m4527("Failed loading new engine. Marking new engine destroyable.");
            this.f6707.zzlr();
        }
    }
}
