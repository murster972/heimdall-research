package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzol;

final class zzbf implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzba f6761;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzol f6762;

    zzbf(zzba zzba, zzol zzol) {
        this.f6761 = zzba;
        this.f6762 = zzol;
    }

    public final void run() {
        try {
            if (this.f6761.连任.f3579 != null) {
                this.f6761.连任.f3579.m13344(this.f6762);
            }
        } catch (RemoteException e) {
            zzagf.m4796("Could not call OnContentAdLoadedListener.onContentAdLoaded().", e);
        }
    }
}
