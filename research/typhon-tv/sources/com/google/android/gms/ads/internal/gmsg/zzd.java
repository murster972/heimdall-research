package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.js.zza;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzajh;
import com.google.android.gms.internal.zzamp;
import com.google.android.gms.internal.zzamv;
import com.google.android.gms.internal.zzamw;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzaog;
import com.google.android.gms.internal.zzaow;
import com.google.android.gms.internal.zzaox;
import com.google.android.gms.internal.zzaoy;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzcw;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzzv;
import com.mopub.common.TyphoonApp;
import java.util.HashMap;
import java.util.Map;

@zzzv
public final class zzd {
    public static final zzt<zzanh> zzbwx = zze.f6652;
    public static final zzt<zzanh> zzbwy = zzf.f6653;
    public static final zzt<zzanh> zzbwz = zzg.f6654;
    public static final zzt<zzanh> zzbxa = new zzl();
    public static final zzt<zzanh> zzbxb = new zzm();
    public static final zzt<zzanh> zzbxc = zzh.f6655;
    public static final zzt<Object> zzbxd = new zzn();
    public static final zzt<zzanh> zzbxe = new zzo();
    public static final zzt<zzanh> zzbxf = zzi.f6656;
    public static final zzt<zzanh> zzbxg = new zzp();
    public static final zzt<zzanh> zzbxh = new zzq();
    public static final zzt<zzamp> zzbxi = new zzamv();
    public static final zzt<zzamp> zzbxj = new zzamw();
    public static final zzt<zzanh> zzbxk = new zzc();
    public static final zzad zzbxl = new zzad();
    public static final zzt<zzanh> zzbxm = new zzr();
    public static final zzt<zzanh> zzbxn = new zzs();
    public static final zzt<zzanh> zzbxo = new zzk();

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzt<Object> f3421 = new zzj();

    /* JADX WARNING: Removed duplicated region for block: B:17:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011f  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final /* synthetic */ void m4010(com.google.android.gms.internal.zzaog r16, java.util.Map r17) {
        /*
            android.content.Context r1 = r16.getContext()
            android.content.pm.PackageManager r4 = r1.getPackageManager()
            java.lang.String r1 = "data"
            r0 = r17
            java.lang.Object r1 = r0.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00d7 }
            r2.<init>((java.lang.String) r1)     // Catch:{ JSONException -> 0x00d7 }
            java.lang.String r1 = "intents"
            org.json.JSONArray r5 = r2.getJSONArray(r1)     // Catch:{ JSONException -> 0x00e8 }
            org.json.JSONObject r6 = new org.json.JSONObject
            r6.<init>()
            r1 = 0
        L_0x0025:
            int r2 = r5.length()
            if (r1 >= r2) goto L_0x0129
            org.json.JSONObject r2 = r5.getJSONObject(r1)     // Catch:{ JSONException -> 0x00f9 }
            java.lang.String r3 = "id"
            java.lang.String r7 = r2.optString(r3)
            java.lang.String r3 = "u"
            java.lang.String r8 = r2.optString(r3)
            java.lang.String r3 = "i"
            java.lang.String r9 = r2.optString(r3)
            java.lang.String r3 = "m"
            java.lang.String r10 = r2.optString(r3)
            java.lang.String r3 = "p"
            java.lang.String r11 = r2.optString(r3)
            java.lang.String r3 = "c"
            java.lang.String r12 = r2.optString(r3)
            java.lang.String r3 = "f"
            r2.optString(r3)
            java.lang.String r3 = "e"
            r2.optString(r3)
            java.lang.String r3 = "intent_url"
            java.lang.String r2 = r2.optString(r3)
            r3 = 0
            boolean r13 = android.text.TextUtils.isEmpty(r2)
            if (r13 != 0) goto L_0x0116
            r13 = 0
            android.content.Intent r2 = android.content.Intent.parseUri(r2, r13)     // Catch:{ URISyntaxException -> 0x0101 }
        L_0x0078:
            if (r2 != 0) goto L_0x00c7
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            boolean r3 = android.text.TextUtils.isEmpty(r8)
            if (r3 != 0) goto L_0x008c
            android.net.Uri r3 = android.net.Uri.parse(r8)
            r2.setData(r3)
        L_0x008c:
            boolean r3 = android.text.TextUtils.isEmpty(r9)
            if (r3 != 0) goto L_0x0095
            r2.setAction(r9)
        L_0x0095:
            boolean r3 = android.text.TextUtils.isEmpty(r10)
            if (r3 != 0) goto L_0x009e
            r2.setType(r10)
        L_0x009e:
            boolean r3 = android.text.TextUtils.isEmpty(r11)
            if (r3 != 0) goto L_0x00a7
            r2.setPackage(r11)
        L_0x00a7:
            boolean r3 = android.text.TextUtils.isEmpty(r12)
            if (r3 != 0) goto L_0x00c7
            java.lang.String r3 = "/"
            r8 = 2
            java.lang.String[] r3 = r12.split(r3, r8)
            int r8 = r3.length
            r9 = 2
            if (r8 != r9) goto L_0x00c7
            android.content.ComponentName r8 = new android.content.ComponentName
            r9 = 0
            r9 = r3[r9]
            r10 = 1
            r3 = r3[r10]
            r8.<init>(r9, r3)
            r2.setComponent(r8)
        L_0x00c7:
            r3 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r2 = r4.resolveActivity(r2, r3)
            if (r2 == 0) goto L_0x011f
            r2 = 1
        L_0x00d0:
            r6.put((java.lang.String) r7, (boolean) r2)     // Catch:{ JSONException -> 0x0121 }
        L_0x00d3:
            int r1 = r1 + 1
            goto L_0x0025
        L_0x00d7:
            r1 = move-exception
            com.google.android.gms.ads.internal.js.zza r16 = (com.google.android.gms.ads.internal.js.zza) r16
            java.lang.String r1 = "openableIntents"
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>()
            r0 = r16
            r0.zza((java.lang.String) r1, (org.json.JSONObject) r2)
        L_0x00e7:
            return
        L_0x00e8:
            r1 = move-exception
            com.google.android.gms.ads.internal.js.zza r16 = (com.google.android.gms.ads.internal.js.zza) r16
            java.lang.String r1 = "openableIntents"
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>()
            r0 = r16
            r0.zza((java.lang.String) r1, (org.json.JSONObject) r2)
            goto L_0x00e7
        L_0x00f9:
            r2 = move-exception
            java.lang.String r3 = "Error parsing the intent data."
            com.google.android.gms.internal.zzagf.m4793(r3, r2)
            goto L_0x00d3
        L_0x0101:
            r13 = move-exception
            java.lang.String r14 = "Error parsing the url: "
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r15 = r2.length()
            if (r15 == 0) goto L_0x0119
            java.lang.String r2 = r14.concat(r2)
        L_0x0113:
            com.google.android.gms.internal.zzagf.m4793(r2, r13)
        L_0x0116:
            r2 = r3
            goto L_0x0078
        L_0x0119:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r14)
            goto L_0x0113
        L_0x011f:
            r2 = 0
            goto L_0x00d0
        L_0x0121:
            r2 = move-exception
            java.lang.String r3 = "Error constructing openable urls response."
            com.google.android.gms.internal.zzagf.m4793(r3, r2)
            goto L_0x00d3
        L_0x0129:
            com.google.android.gms.ads.internal.js.zza r16 = (com.google.android.gms.ads.internal.js.zza) r16
            java.lang.String r1 = "openableIntents"
            r0 = r16
            r0.zza((java.lang.String) r1, (org.json.JSONObject) r6)
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.zzd.m4010(com.google.android.gms.internal.zzaog, java.util.Map):void");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static final /* synthetic */ void m4011(zzaog zzaog, Map map) {
        String str = (String) map.get(TyphoonApp.VIDEO_TRACKING_URLS_KEY);
        if (TextUtils.isEmpty(str)) {
            zzagf.m4791("URLs missing in canOpenURLs GMSG.");
            return;
        }
        String[] split = str.split(",");
        HashMap hashMap = new HashMap();
        PackageManager packageManager = zzaog.getContext().getPackageManager();
        for (String str2 : split) {
            String[] split2 = str2.split(";", 2);
            hashMap.put(str2, Boolean.valueOf(packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), 65536) != null));
        }
        ((zza) zzaog).zza("openableURLs", (Map<String, ?>) hashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4012(zza zza, Map map) {
        Uri uri;
        String str = (String) map.get("u");
        if (str == null) {
            zzagf.m4791("URL missing from click GMSG.");
            return;
        }
        Uri parse = Uri.parse(str);
        try {
            zzcv r5 = ((zzaow) zza).m9726();
            if (r5 == null || !r5.m11541(parse)) {
                uri = parse;
            } else {
                Context context = ((zzaog) zza).getContext();
                zzaoy zzaoy = (zzaoy) zza;
                if (zzaoy == null) {
                    throw null;
                }
                uri = r5.m11538(parse, context, (View) zzaoy, ((zzaog) zza).m9720());
            }
            parse = uri;
        } catch (zzcw e) {
            String valueOf = String.valueOf(str);
            zzagf.m4791(valueOf.length() != 0 ? "Unable to append parameter to URL: ".concat(valueOf) : new String("Unable to append parameter to URL: "));
        }
        if ((((Boolean) zzkb.m5481().m5595(zznh.f4903)).booleanValue() && zzbs.zzfd().m4436(((zzaog) zza).getContext())) && TextUtils.isEmpty(parse.getQueryParameter("fbs_aeid"))) {
            String r2 = zzbs.zzfd().m4423(((zzaog) zza).getContext());
            zzbs.zzei();
            parse = zzahn.m4592(parse.toString(), "fbs_aeid", r2);
            zzbs.zzfd().m4429(((zzaog) zza).getContext(), r2);
        }
        new zzajh(((zzaog) zza).getContext(), ((zzaox) zza).m9727().f4297, parse.toString()).m4521();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4013(zzaog zzaog, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            zzagf.m4791("URL missing from httpTrack GMSG.");
        } else {
            new zzajh(zzaog.getContext(), ((zzaox) zzaog).m9727().f4297, str).m4521();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ void m4014(zzaow zzaow, Map map) {
        String str = (String) map.get("tx");
        String str2 = (String) map.get("ty");
        String str3 = (String) map.get("td");
        try {
            int parseInt = Integer.parseInt(str);
            int parseInt2 = Integer.parseInt(str2);
            int parseInt3 = Integer.parseInt(str3);
            zzcv r3 = zzaow.m9726();
            if (r3 != null) {
                r3.m11539().zza(parseInt, parseInt2, parseInt3);
            }
        } catch (NumberFormatException e) {
            zzagf.m4791("Could not parse touch parameters from gmsg.");
        }
    }
}
