package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzafo;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzon;

final class zzbg implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzafo f6763;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzba f6764;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f6765;

    zzbg(zzba zzba, String str, zzafo zzafo) {
        this.f6764 = zzba;
        this.f6765 = str;
        this.f6763 = zzafo;
    }

    public final void run() {
        try {
            this.f6764.连任.f3583.get(this.f6765).m13350((zzon) this.f6763.f4116);
        } catch (RemoteException e) {
            zzagf.m4796("Could not call onCustomTemplateAdLoadedListener.onCustomTemplateAdLoaded().", e);
        }
    }
}
