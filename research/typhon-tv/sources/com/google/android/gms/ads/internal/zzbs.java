package com.google.android.gms.ads.internal;

import android.os.Build;
import com.google.android.gms.ads.internal.js.zzb;
import com.google.android.gms.ads.internal.overlay.zza;
import com.google.android.gms.ads.internal.overlay.zzl;
import com.google.android.gms.ads.internal.overlay.zzr;
import com.google.android.gms.ads.internal.overlay.zzs;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzacq;
import com.google.android.gms.internal.zzaff;
import com.google.android.gms.internal.zzaft;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.internal.zzahy;
import com.google.android.gms.internal.zzahz;
import com.google.android.gms.internal.zzaia;
import com.google.android.gms.internal.zzaib;
import com.google.android.gms.internal.zzaic;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzaie;
import com.google.android.gms.internal.zzaim;
import com.google.android.gms.internal.zzajf;
import com.google.android.gms.internal.zzajg;
import com.google.android.gms.internal.zzajn;
import com.google.android.gms.internal.zzalg;
import com.google.android.gms.internal.zzaln;
import com.google.android.gms.internal.zzamz;
import com.google.android.gms.internal.zzanr;
import com.google.android.gms.internal.zzhg;
import com.google.android.gms.internal.zzic;
import com.google.android.gms.internal.zzid;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.internal.zztg;
import com.google.android.gms.internal.zztq;
import com.google.android.gms.internal.zzuq;
import com.google.android.gms.internal.zzya;
import com.google.android.gms.internal.zzzv;
import com.google.android.gms.internal.zzzw;

@zzzv
public final class zzbs {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzbs f3533;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f3534 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzya f3535 = new zzya();

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private final zzajn f3536;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzahn f3537 = new zzahn();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzanr f3538 = new zzanr();

    /* renamed from: ʾ  reason: contains not printable characters */
    private final zzid f3539;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final zzd f3540;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final zztg f3541;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzic f3542;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final zztq f3543;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final zzaim f3544;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final zzacq f3545;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final zzalg f3546;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final zzajf f3547;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzaht f3548;

    /* renamed from: י  reason: contains not printable characters */
    private final zzr f3549;

    /* renamed from: ـ  reason: contains not printable characters */
    private final zzs f3550;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzhg f3551;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final zzaft f3552;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private final zzb f3553;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final zzuq f3554;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private final zzaie f3555;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final zzajg f3556;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final zzaz f3557;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final zzir f3558;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final zzaff f3559;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzl f3560 = new zzl();

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzzw f3561 = new zzzw();

    /* renamed from: 齉  reason: contains not printable characters */
    private final zza f3562 = new zza();

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final zzamz f3563;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final zzac f3564;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zznm f3565;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final zzaln f3566;

    static {
        zzbs zzbs = new zzbs();
        synchronized (f3534) {
            f3533 = zzbs;
        }
    }

    protected zzbs() {
        int i = Build.VERSION.SDK_INT;
        this.f3548 = i >= 21 ? new zzaid() : i >= 19 ? new zzaic() : i >= 18 ? new zzaia() : i >= 17 ? new zzahz() : i >= 16 ? new zzaib() : new zzahy();
        this.f3551 = new zzhg();
        this.f3552 = new zzaft(this.f3537);
        this.f3542 = new zzic();
        this.f3539 = new zzid();
        this.f3540 = zzh.m9250();
        this.f3564 = new zzac();
        this.f3565 = new zznm();
        this.f3544 = new zzaim();
        this.f3545 = new zzacq();
        this.f3553 = new zzb();
        this.f3546 = new zzalg();
        this.f3541 = new zztg();
        this.f3543 = new zztq();
        this.f3547 = new zzajf();
        this.f3549 = new zzr();
        this.f3550 = new zzs();
        this.f3554 = new zzuq();
        this.f3556 = new zzajg();
        this.f3557 = new zzaz();
        this.f3558 = new zzir();
        this.f3559 = new zzaff();
        this.f3563 = new zzamz();
        this.f3566 = new zzaln();
        this.f3555 = new zzaie();
        this.f3536 = new zzajn();
    }

    public static zzzw zzee() {
        return m4081().f3561;
    }

    public static zza zzef() {
        return m4081().f3562;
    }

    public static zzl zzeg() {
        return m4081().f3560;
    }

    public static zzya zzeh() {
        return m4081().f3535;
    }

    public static zzahn zzei() {
        return m4081().f3537;
    }

    public static zzanr zzej() {
        return m4081().f3538;
    }

    public static zzaht zzek() {
        return m4081().f3548;
    }

    public static zzhg zzel() {
        return m4081().f3551;
    }

    public static zzaft zzem() {
        return m4081().f3552;
    }

    public static zzid zzen() {
        return m4081().f3539;
    }

    public static zzd zzeo() {
        return m4081().f3540;
    }

    public static zzac zzep() {
        return m4081().f3564;
    }

    public static zznm zzeq() {
        return m4081().f3565;
    }

    public static zzaim zzer() {
        return m4081().f3544;
    }

    public static zzacq zzes() {
        return m4081().f3545;
    }

    public static zzalg zzet() {
        return m4081().f3546;
    }

    public static zztg zzeu() {
        return m4081().f3541;
    }

    public static zztq zzev() {
        return m4081().f3543;
    }

    public static zzajf zzew() {
        return m4081().f3547;
    }

    public static zzr zzex() {
        return m4081().f3549;
    }

    public static zzs zzey() {
        return m4081().f3550;
    }

    public static zzuq zzez() {
        return m4081().f3554;
    }

    public static zzajg zzfa() {
        return m4081().f3556;
    }

    public static zzamz zzfb() {
        return m4081().f3563;
    }

    public static zzaln zzfc() {
        return m4081().f3566;
    }

    public static zzaff zzfd() {
        return m4081().f3559;
    }

    public static zzb zzfe() {
        return m4081().f3553;
    }

    public static zzaie zzff() {
        return m4081().f3555;
    }

    public static zzajn zzfg() {
        return m4081().f3536;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzbs m4081() {
        zzbs zzbs;
        synchronized (f3534) {
            zzbs = f3533;
        }
        return zzbs;
    }
}
