package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import java.util.Map;
import java.util.TreeMap;

final class zzbr {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, String> f6775 = new TreeMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private String f6776;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f6777;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f6778;

    public zzbr(String str) {
        this.f6778 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m7653() {
        return this.f6777;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Map<String, String> m7654() {
        return this.f6775;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m7655() {
        return this.f6778;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m7656() {
        return this.f6776;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7657(zzjj zzjj, zzakd zzakd) {
        this.f6777 = zzjj.f4762.f4876;
        Bundle bundle = zzjj.f4756 != null ? zzjj.f4756.getBundle(AdMobAdapter.class.getName()) : null;
        if (bundle != null) {
            String str = (String) zzkb.m5481().m5595(zznh.f5011);
            for (String str2 : bundle.keySet()) {
                if (str.equals(str2)) {
                    this.f6776 = bundle.getString(str2);
                } else if (str2.startsWith("csa_")) {
                    this.f6775.put(str2.substring(4), bundle.getString(str2));
                }
            }
            this.f6775.put("SDKVersion", zzakd.f4297);
        }
    }
}
