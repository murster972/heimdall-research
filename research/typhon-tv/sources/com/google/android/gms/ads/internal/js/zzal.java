package com.google.android.gms.ads.internal.js;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzzv;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

@zzzv
public final class zzal implements zzak {

    /* renamed from: 靐  reason: contains not printable characters */
    private final HashSet<AbstractMap.SimpleEntry<String, zzt<? super zzaj>>> f3425 = new HashSet<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaj f3426;

    public zzal(zzaj zzaj) {
        this.f3426 = zzaj;
    }

    public final void zza(String str, zzt<? super zzaj> zzt) {
        this.f3426.zza(str, zzt);
        this.f3425.add(new AbstractMap.SimpleEntry(str, zzt));
    }

    public final void zza(String str, Map<String, ?> map) {
        this.f3426.zza(str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        this.f3426.zza(str, jSONObject);
    }

    public final void zzb(String str, zzt<? super zzaj> zzt) {
        this.f3426.zzb(str, zzt);
        this.f3425.remove(new AbstractMap.SimpleEntry(str, zzt));
    }

    public final void zzb(String str, JSONObject jSONObject) {
        this.f3426.zzb(str, jSONObject);
    }

    public final void zzlt() {
        Iterator<AbstractMap.SimpleEntry<String, zzt<? super zzaj>>> it2 = this.f3425.iterator();
        while (it2.hasNext()) {
            AbstractMap.SimpleEntry next = it2.next();
            String valueOf = String.valueOf(((zzt) next.getValue()).toString());
            zzagf.m4527(valueOf.length() != 0 ? "Unregistering eventhandler: ".concat(valueOf) : new String("Unregistering eventhandler: "));
            this.f3426.zzb((String) next.getKey(), (zzt) next.getValue());
        }
        this.f3425.clear();
    }
}
