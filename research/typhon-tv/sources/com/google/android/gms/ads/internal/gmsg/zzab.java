package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzq;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzw;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzaog;
import com.google.android.gms.internal.zzaoh;
import com.google.android.gms.internal.zzaou;
import com.google.android.gms.internal.zzaow;
import com.google.android.gms.internal.zzaoy;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzje;
import com.google.android.gms.internal.zzwr;
import com.google.android.gms.internal.zzzv;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzab<T extends zzaog & zzaoh & zzaou & zzaow & zzaoy> implements zzt<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzn f3408;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzb f3409;

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzw f3410;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzwr f3411;

    /* renamed from: ٴ  reason: contains not printable characters */
    private zzanh f3412 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzje f3413;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzcv f3414;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzq f3415;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzakd f3416;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f3417;

    public zzab(Context context, zzakd zzakd, zzcv zzcv, zzq zzq, zzje zzje, zzb zzb, zzn zzn, zzw zzw, zzwr zzwr) {
        this.f3417 = context;
        this.f3416 = zzakd;
        this.f3414 = zzcv;
        this.f3415 = zzq;
        this.f3413 = zzje;
        this.f3409 = zzb;
        this.f3410 = zzw;
        this.f3411 = zzwr;
        this.f3408 = zzn;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m4007(Map<String, String> map) {
        String str = map.get("o");
        if (str != null) {
            if (TtmlNode.TAG_P.equalsIgnoreCase(str)) {
                return zzbs.zzek().m4644();
            }
            if ("l".equalsIgnoreCase(str)) {
                return zzbs.zzek().m4652();
            }
            if ("c".equalsIgnoreCase(str)) {
                return zzbs.zzek().m4648();
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4008(boolean z) {
        if (this.f3411 != null) {
            this.f3411.m6000(z);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m4009(Map<String, String> map) {
        return PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("custom_close"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x012f A[SYNTHETIC, Splitter:B:46:0x012f] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(java.lang.Object r11, java.util.Map r12) {
        /*
            r10 = this;
            r5 = 1
            r4 = 0
            r6 = 0
            com.google.android.gms.internal.zzaog r11 = (com.google.android.gms.internal.zzaog) r11
            java.lang.String r1 = "u"
            java.lang.Object r1 = r12.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            android.content.Context r2 = r11.getContext()
            java.lang.String r3 = com.google.android.gms.internal.zzafi.m4437(r1, r2)
            java.lang.String r1 = "a"
            java.lang.Object r1 = r12.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0028
            java.lang.String r1 = "Action missing from an open GMSG."
            com.google.android.gms.internal.zzagf.m4791(r1)
        L_0x0027:
            return
        L_0x0028:
            com.google.android.gms.ads.internal.zzw r2 = r10.f3410
            if (r2 == 0) goto L_0x003a
            com.google.android.gms.ads.internal.zzw r2 = r10.f3410
            boolean r2 = r2.zzda()
            if (r2 != 0) goto L_0x003a
            com.google.android.gms.ads.internal.zzw r1 = r10.f3410
            r1.zzt(r3)
            goto L_0x0027
        L_0x003a:
            java.lang.String r2 = "expand"
            boolean r2 = r2.equalsIgnoreCase(r1)
            if (r2 == 0) goto L_0x0064
            r1 = r11
            com.google.android.gms.internal.zzaoh r1 = (com.google.android.gms.internal.zzaoh) r1
            boolean r1 = r1.m9721()
            if (r1 == 0) goto L_0x0053
            java.lang.String r1 = "Cannot expand WebView that is already expanded."
            com.google.android.gms.internal.zzagf.m4791(r1)
            goto L_0x0027
        L_0x0053:
            r10.m4008((boolean) r4)
            com.google.android.gms.internal.zzaou r11 = (com.google.android.gms.internal.zzaou) r11
            boolean r1 = m4009((java.util.Map<java.lang.String, java.lang.String>) r12)
            int r2 = m4007(r12)
            r11.m9723(r1, r2)
            goto L_0x0027
        L_0x0064:
            java.lang.String r2 = "webapp"
            boolean r2 = r2.equalsIgnoreCase(r1)
            if (r2 == 0) goto L_0x00a0
            r10.m4008((boolean) r4)
            if (r3 == 0) goto L_0x0080
            com.google.android.gms.internal.zzaou r11 = (com.google.android.gms.internal.zzaou) r11
            boolean r1 = m4009((java.util.Map<java.lang.String, java.lang.String>) r12)
            int r2 = m4007(r12)
            r11.m9724(r1, r2, r3)
            goto L_0x0027
        L_0x0080:
            com.google.android.gms.internal.zzaou r11 = (com.google.android.gms.internal.zzaou) r11
            boolean r3 = m4009((java.util.Map<java.lang.String, java.lang.String>) r12)
            int r4 = m4007(r12)
            java.lang.String r1 = "html"
            java.lang.Object r1 = r12.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "baseurl"
            java.lang.Object r2 = r12.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            r11.m9725(r3, r4, r1, r2)
            goto L_0x0027
        L_0x00a0:
            java.lang.String r2 = "app"
            boolean r1 = r2.equalsIgnoreCase(r1)
            if (r1 == 0) goto L_0x0101
            java.lang.String r2 = "true"
            java.lang.String r1 = "system_browser"
            java.lang.Object r1 = r12.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = r2.equalsIgnoreCase(r1)
            if (r1 == 0) goto L_0x0101
            r10.m4008((boolean) r5)
            r11.getContext()
            boolean r1 = android.text.TextUtils.isEmpty(r3)
            if (r1 == 0) goto L_0x00cf
            java.lang.String r1 = "Destination url cannot be empty."
            com.google.android.gms.internal.zzagf.m4791(r1)
            goto L_0x0027
        L_0x00cf:
            com.google.android.gms.ads.internal.gmsg.zzac r2 = new com.google.android.gms.ads.internal.gmsg.zzac
            android.content.Context r3 = r11.getContext()
            r1 = r11
            com.google.android.gms.internal.zzaow r1 = (com.google.android.gms.internal.zzaow) r1
            com.google.android.gms.internal.zzcv r4 = r1.m9726()
            r1 = r11
            com.google.android.gms.internal.zzaoy r1 = (com.google.android.gms.internal.zzaoy) r1
            if (r1 != 0) goto L_0x00e2
            throw r6
        L_0x00e2:
            android.view.View r1 = (android.view.View) r1
            r2.<init>(r3, r4, r1)
            android.content.Intent r1 = r2.zzm(r12)
            com.google.android.gms.internal.zzaou r11 = (com.google.android.gms.internal.zzaou) r11     // Catch:{ ActivityNotFoundException -> 0x00f7 }
            com.google.android.gms.ads.internal.overlay.zzc r2 = new com.google.android.gms.ads.internal.overlay.zzc     // Catch:{ ActivityNotFoundException -> 0x00f7 }
            r2.<init>(r1)     // Catch:{ ActivityNotFoundException -> 0x00f7 }
            r11.m9722(r2)     // Catch:{ ActivityNotFoundException -> 0x00f7 }
            goto L_0x0027
        L_0x00f7:
            r1 = move-exception
            java.lang.String r1 = r1.getMessage()
            com.google.android.gms.internal.zzagf.m4791(r1)
            goto L_0x0027
        L_0x0101:
            r10.m4008((boolean) r5)
            java.lang.String r1 = "intent_url"
            java.lang.Object r1 = r12.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0181
            r2 = 0
            android.content.Intent r1 = android.content.Intent.parseUri(r1, r2)     // Catch:{ URISyntaxException -> 0x016c }
            r5 = r1
        L_0x0119:
            if (r5 == 0) goto L_0x015e
            android.net.Uri r1 = r5.getData()
            if (r1 == 0) goto L_0x015e
            android.net.Uri r2 = r5.getData()
            java.lang.String r4 = r2.toString()
            boolean r1 = android.text.TextUtils.isEmpty(r4)
            if (r1 != 0) goto L_0x01b3
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ Exception -> 0x0145 }
            android.content.Context r7 = r11.getContext()     // Catch:{ Exception -> 0x0145 }
            r0 = r11
            com.google.android.gms.internal.zzaow r0 = (com.google.android.gms.internal.zzaow) r0     // Catch:{ Exception -> 0x0145 }
            r1 = r0
            com.google.android.gms.internal.zzcv r8 = r1.m9726()     // Catch:{ Exception -> 0x0145 }
            r0 = r11
            com.google.android.gms.internal.zzaoy r0 = (com.google.android.gms.internal.zzaoy) r0     // Catch:{ Exception -> 0x0145 }
            r1 = r0
            if (r1 != 0) goto L_0x0189
            throw r6     // Catch:{ Exception -> 0x0145 }
        L_0x0145:
            r1 = move-exception
            java.lang.String r7 = "Error occurred while adding signals."
            com.google.android.gms.internal.zzagf.m4793(r7, r1)
            com.google.android.gms.internal.zzaft r7 = com.google.android.gms.ads.internal.zzbs.zzem()
            java.lang.String r8 = "OpenGmsgHandler.onGmsg"
            r7.m4505((java.lang.Throwable) r1, (java.lang.String) r8)
            r1 = r4
        L_0x0157:
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0194 }
        L_0x015b:
            r5.setData(r1)
        L_0x015e:
            if (r5 == 0) goto L_0x01bb
            com.google.android.gms.internal.zzaou r11 = (com.google.android.gms.internal.zzaou) r11
            com.google.android.gms.ads.internal.overlay.zzc r1 = new com.google.android.gms.ads.internal.overlay.zzc
            r1.<init>(r5)
            r11.m9722(r1)
            goto L_0x0027
        L_0x016c:
            r2 = move-exception
            java.lang.String r4 = "Error parsing the url: "
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r5 = r1.length()
            if (r5 == 0) goto L_0x0183
            java.lang.String r1 = r4.concat(r1)
        L_0x017e:
            com.google.android.gms.internal.zzagf.m4793(r1, r2)
        L_0x0181:
            r5 = r6
            goto L_0x0119
        L_0x0183:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r4)
            goto L_0x017e
        L_0x0189:
            android.view.View r1 = (android.view.View) r1     // Catch:{ Exception -> 0x0145 }
            android.app.Activity r9 = r11.m9720()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r1 = com.google.android.gms.internal.zzahn.m4599((android.content.Context) r7, (com.google.android.gms.internal.zzcv) r8, (java.lang.String) r4, (android.view.View) r1, (android.app.Activity) r9)     // Catch:{ Exception -> 0x0145 }
            goto L_0x0157
        L_0x0194:
            r4 = move-exception
            java.lang.String r7 = "Error parsing the uri: "
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r8 = r1.length()
            if (r8 == 0) goto L_0x01b5
            java.lang.String r1 = r7.concat(r1)
        L_0x01a6:
            com.google.android.gms.internal.zzagf.m4793(r1, r4)
            com.google.android.gms.internal.zzaft r1 = com.google.android.gms.ads.internal.zzbs.zzem()
            java.lang.String r7 = "OpenGmsgHandler.onGmsg"
            r1.m4505((java.lang.Throwable) r4, (java.lang.String) r7)
        L_0x01b3:
            r1 = r2
            goto L_0x015b
        L_0x01b5:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r7)
            goto L_0x01a6
        L_0x01bb:
            boolean r1 = android.text.TextUtils.isEmpty(r3)
            if (r1 != 0) goto L_0x01df
            com.google.android.gms.ads.internal.zzbs.zzei()
            android.content.Context r2 = r11.getContext()
            r1 = r11
            com.google.android.gms.internal.zzaow r1 = (com.google.android.gms.internal.zzaow) r1
            com.google.android.gms.internal.zzcv r4 = r1.m9726()
            r1 = r11
            com.google.android.gms.internal.zzaoy r1 = (com.google.android.gms.internal.zzaoy) r1
            if (r1 != 0) goto L_0x01d5
            throw r6
        L_0x01d5:
            android.view.View r1 = (android.view.View) r1
            android.app.Activity r5 = r11.m9720()
            java.lang.String r3 = com.google.android.gms.internal.zzahn.m4599((android.content.Context) r2, (com.google.android.gms.internal.zzcv) r4, (java.lang.String) r3, (android.view.View) r1, (android.app.Activity) r5)
        L_0x01df:
            com.google.android.gms.internal.zzaou r11 = (com.google.android.gms.internal.zzaou) r11
            com.google.android.gms.ads.internal.overlay.zzc r1 = new com.google.android.gms.ads.internal.overlay.zzc
            java.lang.String r2 = "i"
            java.lang.Object r2 = r12.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r4 = "m"
            java.lang.Object r4 = r12.get(r4)
            java.lang.String r4 = (java.lang.String) r4
            java.lang.String r5 = "p"
            java.lang.Object r5 = r12.get(r5)
            java.lang.String r5 = (java.lang.String) r5
            java.lang.String r6 = "c"
            java.lang.Object r6 = r12.get(r6)
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r7 = "f"
            java.lang.Object r7 = r12.get(r7)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r8 = "e"
            java.lang.Object r8 = r12.get(r8)
            java.lang.String r8 = (java.lang.String) r8
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            r11.m9722(r1)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.zzab.zza(java.lang.Object, java.util.Map):void");
    }
}
