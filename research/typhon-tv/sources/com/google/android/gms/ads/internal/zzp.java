package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzzv;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzp {
    public static Object[] zza(String str, zzjj zzjj, String str2, int i, zzjn zzjn) {
        HashSet hashSet = new HashSet(Arrays.asList(str.split(",")));
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        arrayList.add(str2);
        if (hashSet.contains("formatString")) {
            arrayList.add((Object) null);
        }
        if (hashSet.contains("networkType")) {
            arrayList.add(Integer.valueOf(i));
        }
        if (hashSet.contains("birthday")) {
            arrayList.add(Long.valueOf(zzjj.f4765));
        }
        if (hashSet.contains("extras")) {
            arrayList.add(m4088(zzjj.f4767));
        }
        if (hashSet.contains(PubnativeRequest.Parameters.GENDER)) {
            arrayList.add(Integer.valueOf(zzjj.f4766));
        }
        if (hashSet.contains(PubnativeRequest.Parameters.KEYWORDS)) {
            if (zzjj.f4764 != null) {
                arrayList.add(zzjj.f4764.toString());
            } else {
                arrayList.add((Object) null);
            }
        }
        if (hashSet.contains("isTestDevice")) {
            arrayList.add(Boolean.valueOf(zzjj.f4753));
        }
        if (hashSet.contains("tagForChildDirectedTreatment")) {
            arrayList.add(Integer.valueOf(zzjj.f4754));
        }
        if (hashSet.contains("manualImpressionsEnabled")) {
            arrayList.add(Boolean.valueOf(zzjj.f4755));
        }
        if (hashSet.contains("publisherProvidedId")) {
            arrayList.add(zzjj.f4761);
        }
        if (hashSet.contains("location")) {
            if (zzjj.f4763 != null) {
                arrayList.add(zzjj.f4763.toString());
            } else {
                arrayList.add((Object) null);
            }
        }
        if (hashSet.contains("contentUrl")) {
            arrayList.add(zzjj.f4758);
        }
        if (hashSet.contains("networkExtras")) {
            arrayList.add(m4088(zzjj.f4756));
        }
        if (hashSet.contains("customTargeting")) {
            arrayList.add(m4088(zzjj.f4757));
        }
        if (hashSet.contains("categoryExclusions")) {
            if (zzjj.f4769 != null) {
                arrayList.add(zzjj.f4769.toString());
            } else {
                arrayList.add((Object) null);
            }
        }
        if (hashSet.contains("requestAgent")) {
            arrayList.add(zzjj.f4770);
        }
        if (hashSet.contains("requestPackage")) {
            arrayList.add(zzjj.f4759);
        }
        if (hashSet.contains("isDesignedForFamilies")) {
            arrayList.add(Boolean.valueOf(zzjj.f4760));
        }
        return arrayList.toArray();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m4088(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        Iterator it2 = new TreeSet(bundle.keySet()).iterator();
        while (it2.hasNext()) {
            Object obj = bundle.get((String) it2.next());
            sb.append(obj == null ? "null" : obj instanceof Bundle ? m4088((Bundle) obj) : obj.toString());
        }
        return sb.toString();
    }
}
