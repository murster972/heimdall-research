package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class NativeAdOptions {
    public static final int ADCHOICES_BOTTOM_LEFT = 3;
    public static final int ADCHOICES_BOTTOM_RIGHT = 2;
    public static final int ADCHOICES_TOP_LEFT = 0;
    public static final int ADCHOICES_TOP_RIGHT = 1;
    public static final int ORIENTATION_ANY = 0;
    public static final int ORIENTATION_LANDSCAPE = 2;
    public static final int ORIENTATION_PORTRAIT = 1;

    /* renamed from: 连任  reason: contains not printable characters */
    private final VideoOptions f3378;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f3379;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f3380;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f3381;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f3382;

    public @interface AdChoicesPlacement {
    }

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public int f3383 = 1;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public int f3384 = -1;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public VideoOptions f3385;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean f3386 = false;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean f3387 = false;

        public final NativeAdOptions build() {
            return new NativeAdOptions(this);
        }

        public final Builder setAdChoicesPlacement(@AdChoicesPlacement int i) {
            this.f3383 = i;
            return this;
        }

        public final Builder setImageOrientation(int i) {
            this.f3384 = i;
            return this;
        }

        public final Builder setRequestMultipleImages(boolean z) {
            this.f3386 = z;
            return this;
        }

        public final Builder setReturnUrlsForImageAssets(boolean z) {
            this.f3387 = z;
            return this;
        }

        public final Builder setVideoOptions(VideoOptions videoOptions) {
            this.f3385 = videoOptions;
            return this;
        }
    }

    private NativeAdOptions(Builder builder) {
        this.f3382 = builder.f3387;
        this.f3379 = builder.f3384;
        this.f3381 = builder.f3386;
        this.f3380 = builder.f3383;
        this.f3378 = builder.f3385;
    }

    public final int getAdChoicesPlacement() {
        return this.f3380;
    }

    public final int getImageOrientation() {
        return this.f3379;
    }

    public final VideoOptions getVideoOptions() {
        return this.f3378;
    }

    public final boolean shouldRequestMultipleImages() {
        return this.f3381;
    }

    public final boolean shouldReturnUrlsForImageAssets() {
        return this.f3382;
    }
}
