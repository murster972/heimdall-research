package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzzv;
import java.lang.ref.WeakReference;

@zzzv
public final class zzbi {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f3518;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f3519;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Runnable f3520;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f3521;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public zzjj f3522;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzbk f3523;

    public zzbi(zza zza) {
        this(zza, new zzbk(zzahn.f4212));
    }

    private zzbi(zza zza, zzbk zzbk) {
        this.f3521 = false;
        this.f3519 = false;
        this.f3518 = 0;
        this.f3523 = zzbk;
        this.f3520 = new zzbj(this, new WeakReference(zza));
    }

    public final void cancel() {
        this.f3521 = false;
        this.f3523.removeCallbacks(this.f3520);
    }

    public final void pause() {
        this.f3519 = true;
        if (this.f3521) {
            this.f3523.removeCallbacks(this.f3520);
        }
    }

    public final void resume() {
        this.f3519 = false;
        if (this.f3521) {
            this.f3521 = false;
            zza(this.f3522, this.f3518);
        }
    }

    public final void zza(zzjj zzjj, long j) {
        if (this.f3521) {
            zzagf.m4791("An ad refresh is already scheduled.");
            return;
        }
        this.f3522 = zzjj;
        this.f3521 = true;
        this.f3518 = j;
        if (!this.f3519) {
            zzagf.m4794(new StringBuilder(65).append("Scheduling ad refresh ").append(j).append(" milliseconds from now.").toString());
            this.f3523.postDelayed(this.f3520, j);
        }
    }

    public final boolean zzdx() {
        return this.f3521;
    }

    public final void zzf(zzjj zzjj) {
        this.f3522 = zzjj;
    }

    public final void zzg(zzjj zzjj) {
        zza(zzjj, 60000);
    }
}
