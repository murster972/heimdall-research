package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzlt;
import com.google.android.gms.internal.zzlu;
import java.util.Date;
import java.util.List;
import java.util.Set;

public final class PublisherAdRequest {
    public static final String DEVICE_ID_EMULATOR = "B3EEABB8EE11C2BE770B684D95219ECB";
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_INVALID_REQUEST = 1;
    public static final int ERROR_CODE_NETWORK_ERROR = 2;
    public static final int ERROR_CODE_NO_FILL = 3;
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_UNKNOWN = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzlt f3376;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzlu f3377 = new zzlu();

        public final Builder addCategoryExclusion(String str) {
            this.f3377.ʼ(str);
            return this;
        }

        public final Builder addCustomEventExtrasBundle(Class<? extends CustomEvent> cls, Bundle bundle) {
            this.f3377.靐(cls, bundle);
            return this;
        }

        public final Builder addCustomTargeting(String str, String str2) {
            this.f3377.龘(str, str2);
            return this;
        }

        public final Builder addCustomTargeting(String str, List<String> list) {
            if (list != null) {
                this.f3377.龘(str, TextUtils.join(",", list));
            }
            return this;
        }

        public final Builder addKeyword(String str) {
            this.f3377.龘(str);
            return this;
        }

        public final Builder addNetworkExtras(NetworkExtras networkExtras) {
            this.f3377.龘(networkExtras);
            return this;
        }

        public final Builder addNetworkExtrasBundle(Class<? extends MediationAdapter> cls, Bundle bundle) {
            this.f3377.龘(cls, bundle);
            return this;
        }

        public final Builder addTestDevice(String str) {
            this.f3377.靐(str);
            return this;
        }

        public final PublisherAdRequest build() {
            return new PublisherAdRequest(this);
        }

        public final Builder setBirthday(Date date) {
            this.f3377.龘(date);
            return this;
        }

        public final Builder setContentUrl(String str) {
            zzbq.m9121(str, (Object) "Content URL must be non-null.");
            zzbq.m9123(str, (Object) "Content URL must be non-empty.");
            zzbq.m9118(str.length() <= 512, "Content URL must not exceed %d in length.  Provided length was %d.", 512, Integer.valueOf(str.length()));
            this.f3377.麤(str);
            return this;
        }

        public final Builder setGender(int i) {
            this.f3377.龘(i);
            return this;
        }

        public final Builder setIsDesignedForFamilies(boolean z) {
            this.f3377.齉(z);
            return this;
        }

        public final Builder setLocation(Location location) {
            this.f3377.龘(location);
            return this;
        }

        @Deprecated
        public final Builder setManualImpressionsEnabled(boolean z) {
            this.f3377.龘(z);
            return this;
        }

        public final Builder setPublisherProvidedId(String str) {
            this.f3377.连任(str);
            return this;
        }

        public final Builder setRequestAgent(String str) {
            this.f3377.ʻ(str);
            return this;
        }

        public final Builder tagForChildDirectedTreatment(boolean z) {
            this.f3377.靐(z);
            return this;
        }
    }

    private PublisherAdRequest(Builder builder) {
        this.f3376 = new zzlt(builder.f3377);
    }

    public static void updateCorrelator() {
    }

    public final Date getBirthday() {
        return this.f3376.m5507();
    }

    public final String getContentUrl() {
        return this.f3376.m5502();
    }

    public final <T extends CustomEvent> Bundle getCustomEventExtrasBundle(Class<T> cls) {
        return this.f3376.m5505(cls);
    }

    public final Bundle getCustomTargeting() {
        return this.f3376.m5494();
    }

    public final int getGender() {
        return this.f3376.m5504();
    }

    public final Set<String> getKeywords() {
        return this.f3376.m5503();
    }

    public final Location getLocation() {
        return this.f3376.m5500();
    }

    public final boolean getManualImpressionsEnabled() {
        return this.f3376.m5491();
    }

    @Deprecated
    public final <T extends NetworkExtras> T getNetworkExtras(Class<T> cls) {
        return this.f3376.m5506(cls);
    }

    public final <T extends MediationAdapter> Bundle getNetworkExtrasBundle(Class<T> cls) {
        return this.f3376.m5501(cls);
    }

    public final String getPublisherProvidedId() {
        return this.f3376.m5492();
    }

    public final boolean isTestDevice(Context context) {
        return this.f3376.m5508(context);
    }

    public final zzlt zzbg() {
        return this.f3376;
    }
}
