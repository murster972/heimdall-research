package com.google.android.gms.ads.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzaig;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzaw extends zzlh {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f3512 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private static zzaw f3513;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzakd f3514;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f3515;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Object f3516 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context f3517;

    private zzaw(Context context, zzakd zzakd) {
        this.f3517 = context;
        this.f3514 = zzakd;
        this.f3515 = false;
    }

    public static zzaw zza(Context context, zzakd zzakd) {
        zzaw zzaw;
        synchronized (f3512) {
            if (f3513 == null) {
                f3513 = new zzaw(context.getApplicationContext(), zzakd);
            }
            zzaw = f3513;
        }
        return zzaw;
    }

    public final void initialize() {
        synchronized (f3512) {
            if (this.f3515) {
                zzagf.m4791("Mobile ads is initialized already.");
                return;
            }
            this.f3515 = true;
            zznh.m5599(this.f3517);
            zzbs.zzem().m4500(this.f3517, this.f3514);
            zzbs.zzen().m5420(this.f3517);
        }
    }

    public final void setAppMuted(boolean z) {
        zzbs.zzff().m4673(z);
    }

    public final void setAppVolume(float f) {
        zzbs.zzff().m4672(f);
    }

    public final void zza(String str, IObjectWrapper iObjectWrapper) {
        zzax zzax;
        if (!TextUtils.isEmpty(str)) {
            zznh.m5599(this.f3517);
            boolean booleanValue = ((Boolean) zzkb.m5481().m5595(zznh.f5005)).booleanValue() | ((Boolean) zzkb.m5481().m5595(zznh.f4909)).booleanValue();
            if (((Boolean) zzkb.m5481().m5595(zznh.f4909)).booleanValue()) {
                booleanValue = true;
                zzax = new zzax(this, (Runnable) zzn.m9307(iObjectWrapper));
            } else {
                zzax = null;
            }
            if (booleanValue) {
                zzbs.zzep().zza(this.f3517, this.f3514, str, zzax);
            }
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str) {
        if (iObjectWrapper == null) {
            zzagf.m4795("Wrapped context is null. Failed to open debug menu.");
            return;
        }
        Context context = (Context) zzn.m9307(iObjectWrapper);
        if (context == null) {
            zzagf.m4795("Context is null. Failed to open debug menu.");
            return;
        }
        zzaig zzaig = new zzaig(context);
        zzaig.m4693(str);
        zzaig.m4688(this.f3514.f4297);
        zzaig.m4691();
    }

    public final float zzdn() {
        return zzbs.zzff().m4671();
    }

    public final boolean zzdo() {
        return zzbs.zzff().m4670();
    }

    public final void zzu(String str) {
        zznh.m5599(this.f3517);
        if (!TextUtils.isEmpty(str)) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5005)).booleanValue()) {
                zzbs.zzep().zza(this.f3517, this.f3514, str, (Runnable) null);
            }
        }
    }
}
