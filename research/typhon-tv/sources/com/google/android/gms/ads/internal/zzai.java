package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzjj;

final class zzai implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f6724;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzag f6725;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzjj f6726;

    zzai(zzag zzag, zzjj zzjj, int i) {
        this.f6725 = zzag;
        this.f6726 = zzjj;
        this.f6724 = i;
    }

    public final void run() {
        synchronized (this.f6725.f3482) {
            this.f6725.m4048(this.f6726, this.f6724);
        }
    }
}
