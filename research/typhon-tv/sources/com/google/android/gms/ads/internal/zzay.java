package com.google.android.gms.ads.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzade;
import com.google.android.gms.internal.zzael;
import com.google.android.gms.internal.zzaem;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzui;
import com.google.android.gms.internal.zzva;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class zzay implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzax f6744;

    zzay(zzax zzax) {
        this.f6744 = zzax;
    }

    public final void run() {
        Context r0 = this.f6744.f6742.f3517;
        Runnable runnable = this.f6744.f6743;
        zzbq.m9115("Adapters must be initialized on the main thread.");
        Map<String, zzui> r2 = zzbs.zzem().m4474().m4458();
        if (r2 != null && !r2.isEmpty()) {
            if (runnable != null) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    zzagf.m4796("Could not initialize rewarded ads.", th);
                    return;
                }
            }
            zzade r3 = zzade.m4340();
            if (r3 != null) {
                Collection<zzui> values = r2.values();
                HashMap hashMap = new HashMap();
                IObjectWrapper r4 = zzn.m9306(r0);
                for (zzui zzui : values) {
                    for (zzuh next : zzui.f5437) {
                        String str = next.f5412;
                        for (String next2 : next.f5417) {
                            if (!hashMap.containsKey(next2)) {
                                hashMap.put(next2, new ArrayList());
                            }
                            if (str != null) {
                                ((Collection) hashMap.get(next2)).add(str);
                            }
                        }
                    }
                }
                for (Map.Entry entry : hashMap.entrySet()) {
                    String str2 = (String) entry.getKey();
                    try {
                        zzael r5 = r3.m4351(str2);
                        if (r5 != null) {
                            zzva r6 = r5.m4386();
                            if (!r6.m13456() && r6.m13458()) {
                                r6.m13470(r4, (zzaem) r5.m4385(), (List<String>) (List) entry.getValue());
                                String valueOf = String.valueOf(str2);
                                zzagf.m4792(valueOf.length() != 0 ? "Initialized rewarded video mediation adapter ".concat(valueOf) : new String("Initialized rewarded video mediation adapter "));
                            }
                        }
                    } catch (Throwable th2) {
                        zzagf.m4796(new StringBuilder(String.valueOf(str2).length() + 56).append("Failed to initialize rewarded video mediation adapter \"").append(str2).append("\"").toString(), th2);
                    }
                }
            }
        }
    }
}
