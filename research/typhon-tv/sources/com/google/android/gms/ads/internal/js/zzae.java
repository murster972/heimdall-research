package com.google.android.gms.ads.internal.js;

import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzaiq;
import com.google.android.gms.internal.zzalj;
import com.google.android.gms.internal.zzall;

public final class zzae extends zzall<zzc> {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public zzaiq<zzc> f6667;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f6668;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f6669;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f6670 = new Object();

    public zzae(zzaiq<zzc> zzaiq) {
        this.f6667 = zzaiq;
        this.f6669 = false;
        this.f6668 = 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m7635() {
        synchronized (this.f6670) {
            zzbq.m9125(this.f6668 >= 0);
            if (!this.f6669 || this.f6668 != 0) {
                zzagf.m4527("There are still references to the engine. Not destroying.");
            } else {
                zzagf.m4527("No reference is left (including root). Cleaning up engine.");
                zza(new zzah(this), new zzalj());
            }
        }
    }

    public final zzaa zzlp() {
        zzaa zzaa = new zzaa(this);
        synchronized (this.f6670) {
            zza(new zzaf(this, zzaa), new zzag(this, zzaa));
            zzbq.m9125(this.f6668 >= 0);
            this.f6668++;
        }
        return zzaa;
    }

    public final void zzlr() {
        boolean z = true;
        synchronized (this.f6670) {
            if (this.f6668 < 0) {
                z = false;
            }
            zzbq.m9125(z);
            zzagf.m4527("Releasing root reference. JS Engine will be destroyed once other references are released.");
            this.f6669 = true;
            m7635();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7637() {
        synchronized (this.f6670) {
            zzbq.m9125(this.f6668 > 0);
            zzagf.m4527("Releasing 1 reference for JS Engine");
            this.f6668--;
            m7635();
        }
    }
}
