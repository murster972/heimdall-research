package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzkl;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzqq;
import com.google.android.gms.internal.zzqt;
import com.google.android.gms.internal.zzqw;
import com.google.android.gms.internal.zzqz;
import com.google.android.gms.internal.zzrc;
import com.google.android.gms.internal.zzrf;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzzv;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzag extends zzkl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzqt f3474;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzrf f3475;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzjn f3476;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final List<String> f3477;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final zzld f3478;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzpe f3479;

    /* renamed from: ˊ  reason: contains not printable characters */
    private WeakReference<zzd> f3480;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final zzv f3481;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public final Object f3482 = new Object();

    /* renamed from: ˑ  reason: contains not printable characters */
    private final PublisherAdViewOptions f3483;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final SimpleArrayMap<String, zzqz> f3484;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final SimpleArrayMap<String, zzqw> f3485;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzrc f3486;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzkh f3487;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzqq f3488;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzux f3489;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f3490;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final String f3491;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zzakd f3492;

    zzag(Context context, String str, zzux zzux, zzakd zzakd, zzkh zzkh, zzqq zzqq, zzrc zzrc, zzqt zzqt, SimpleArrayMap<String, zzqz> simpleArrayMap, SimpleArrayMap<String, zzqw> simpleArrayMap2, zzpe zzpe, zzld zzld, zzv zzv, zzrf zzrf, zzjn zzjn, PublisherAdViewOptions publisherAdViewOptions) {
        this.f3490 = context;
        this.f3491 = str;
        this.f3489 = zzux;
        this.f3492 = zzakd;
        this.f3487 = zzkh;
        this.f3474 = zzqt;
        this.f3488 = zzqq;
        this.f3486 = zzrc;
        this.f3484 = simpleArrayMap;
        this.f3485 = simpleArrayMap2;
        this.f3479 = zzpe;
        this.f3477 = m4043();
        this.f3478 = zzld;
        this.f3481 = zzv;
        this.f3475 = zzrf;
        this.f3476 = zzjn;
        this.f3483 = publisherAdViewOptions;
        zznh.m5599(this.f3490);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean m4041() {
        return (this.f3488 == null && this.f3474 == null && (this.f3484 == null || this.f3484.size() <= 0)) ? false : true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<String> m4043() {
        ArrayList arrayList = new ArrayList();
        if (this.f3474 != null) {
            arrayList.add(PubnativeRequest.LEGACY_ZONE_ID);
        }
        if (this.f3488 != null) {
            arrayList.add("2");
        }
        if (this.f3484.size() > 0) {
            arrayList.add("3");
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4047(zzjj zzjj) {
        zzq zzq = new zzq(this.f3490, this.f3481, this.f3476, this.f3491, this.f3489, this.f3492);
        this.f3480 = new WeakReference<>(zzq);
        zzrf zzrf = this.f3475;
        zzbq.m9115("setOnPublisherAdViewLoadedListener must be called on the main UI thread.");
        zzq.连任.f3593 = zzrf;
        if (this.f3483 != null) {
            if (this.f3483.zzbn() != null) {
                zzq.zza(this.f3483.zzbn());
            }
            zzq.setManualImpressionsEnabled(this.f3483.getManualImpressionsEnabled());
        }
        zzqq zzqq = this.f3488;
        zzbq.m9115("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
        zzq.连任.f3569 = zzqq;
        zzqt zzqt = this.f3474;
        zzbq.m9115("setOnContentAdLoadedListener must be called on the main UI thread.");
        zzq.连任.f3579 = zzqt;
        SimpleArrayMap<String, zzqz> simpleArrayMap = this.f3484;
        zzbq.m9115("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        zzq.连任.f3583 = simpleArrayMap;
        SimpleArrayMap<String, zzqw> simpleArrayMap2 = this.f3485;
        zzbq.m9115("setOnCustomClickListener must be called on the main UI thread.");
        zzq.连任.f3582 = simpleArrayMap2;
        zzpe zzpe = this.f3479;
        zzbq.m9115("setNativeAdOptions must be called on the main UI thread.");
        zzq.连任.f3573 = zzpe;
        zzq.zzd(m4043());
        zzq.zza(this.f3487);
        zzq.zza(this.f3478);
        ArrayList arrayList = new ArrayList();
        if (m4041()) {
            arrayList.add(1);
        }
        if (this.f3475 != null) {
            arrayList.add(2);
        }
        zzq.zze(arrayList);
        if (m4041()) {
            zzjj.f4767.putBoolean("ina", true);
        }
        if (this.f3475 != null) {
            zzjj.f4767.putBoolean("iba", true);
        }
        zzq.zzb(zzjj);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4048(zzjj zzjj, int i) {
        zzba zzba = new zzba(this.f3490, this.f3481, zzjn.m5457(this.f3490), this.f3491, this.f3489, this.f3492);
        this.f3480 = new WeakReference<>(zzba);
        zzqq zzqq = this.f3488;
        zzbq.m9115("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
        zzba.连任.f3569 = zzqq;
        zzqt zzqt = this.f3474;
        zzbq.m9115("setOnContentAdLoadedListener must be called on the main UI thread.");
        zzba.连任.f3579 = zzqt;
        SimpleArrayMap<String, zzqz> simpleArrayMap = this.f3484;
        zzbq.m9115("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        zzba.连任.f3583 = simpleArrayMap;
        zzba.zza(this.f3487);
        SimpleArrayMap<String, zzqw> simpleArrayMap2 = this.f3485;
        zzbq.m9115("setOnCustomClickListener must be called on the main UI thread.");
        zzba.连任.f3582 = simpleArrayMap2;
        zzba.zzd(m4043());
        zzpe zzpe = this.f3479;
        zzbq.m9115("setNativeAdOptions must be called on the main UI thread.");
        zzba.连任.f3573 = zzpe;
        zzba.zza(this.f3478);
        zzba.zzj(i);
        zzba.zzb(zzjj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4049(Runnable runnable) {
        zzahn.f4212.post(runnable);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4050() {
        return ((Boolean) zzkb.m5481().m5595(zznh.f4917)).booleanValue() && this.f3475 != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String getMediationAdapterClassName() {
        /*
            r3 = this;
            r1 = 0
            java.lang.Object r2 = r3.f3482
            monitor-enter(r2)
            java.lang.ref.WeakReference<com.google.android.gms.ads.internal.zzd> r0 = r3.f3480     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001a
            java.lang.ref.WeakReference<com.google.android.gms.ads.internal.zzd> r0 = r3.f3480     // Catch:{ all -> 0x001d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x001d }
            com.google.android.gms.ads.internal.zzd r0 = (com.google.android.gms.ads.internal.zzd) r0     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = r0.getMediationAdapterClassName()     // Catch:{ all -> 0x001d }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
        L_0x0017:
            return r0
        L_0x0018:
            r0 = r1
            goto L_0x0016
        L_0x001a:
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
            r0 = r1
            goto L_0x0017
        L_0x001d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzag.getMediationAdapterClassName():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isLoading() {
        /*
            r3 = this;
            r1 = 0
            java.lang.Object r2 = r3.f3482
            monitor-enter(r2)
            java.lang.ref.WeakReference<com.google.android.gms.ads.internal.zzd> r0 = r3.f3480     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001a
            java.lang.ref.WeakReference<com.google.android.gms.ads.internal.zzd> r0 = r3.f3480     // Catch:{ all -> 0x001d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x001d }
            com.google.android.gms.ads.internal.zzd r0 = (com.google.android.gms.ads.internal.zzd) r0     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0018
            boolean r0 = r0.isLoading()     // Catch:{ all -> 0x001d }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
        L_0x0017:
            return r0
        L_0x0018:
            r0 = r1
            goto L_0x0016
        L_0x001a:
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
            r0 = r1
            goto L_0x0017
        L_0x001d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzag.isLoading():boolean");
    }

    public final void zza(zzjj zzjj, int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("Number of ads has to be more than 0");
        }
        m4049((Runnable) new zzai(this, zzjj, i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String zzcp() {
        /*
            r3 = this;
            r1 = 0
            java.lang.Object r2 = r3.f3482
            monitor-enter(r2)
            java.lang.ref.WeakReference<com.google.android.gms.ads.internal.zzd> r0 = r3.f3480     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001a
            java.lang.ref.WeakReference<com.google.android.gms.ads.internal.zzd> r0 = r3.f3480     // Catch:{ all -> 0x001d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x001d }
            com.google.android.gms.ads.internal.zzd r0 = (com.google.android.gms.ads.internal.zzd) r0     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = r0.zzcp()     // Catch:{ all -> 0x001d }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
        L_0x0017:
            return r0
        L_0x0018:
            r0 = r1
            goto L_0x0016
        L_0x001a:
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
            r0 = r1
            goto L_0x0017
        L_0x001d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzag.zzcp():java.lang.String");
    }

    public final void zzd(zzjj zzjj) {
        m4049((Runnable) new zzah(this, zzjj));
    }
}
