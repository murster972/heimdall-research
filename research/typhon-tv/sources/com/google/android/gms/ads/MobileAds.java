package com.google.android.gms.ads;

import android.content.Context;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.internal.zzly;
import com.google.android.gms.internal.zzma;

public class MobileAds {

    public static final class Settings {

        /* renamed from: 龘  reason: contains not printable characters */
        private final zzma f3369 = new zzma();

        @Deprecated
        public final String getTrackingId() {
            return null;
        }

        @Deprecated
        public final boolean isGoogleAnalyticsEnabled() {
            return false;
        }

        @Deprecated
        public final Settings setGoogleAnalyticsEnabled(boolean z) {
            return this;
        }

        @Deprecated
        public final Settings setTrackingId(String str) {
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final zzma m3987() {
            return this.f3369;
        }
    }

    private MobileAds() {
    }

    public static RewardedVideoAd getRewardedVideoAdInstance(Context context) {
        return zzly.m5559().m5562(context);
    }

    public static void initialize(Context context) {
        initialize(context, (String) null, (Settings) null);
    }

    public static void initialize(Context context, String str) {
        initialize(context, str, (Settings) null);
    }

    @Deprecated
    public static void initialize(Context context, String str, Settings settings) {
        zzly.m5559().m5565(context, str, settings == null ? null : settings.m3987());
    }

    public static void openDebugMenu(Context context, String str) {
        zzly.m5559().m5564(context, str);
    }

    public static void setAppMuted(boolean z) {
        zzly.m5559().m5566(z);
    }

    public static void setAppVolume(float f) {
        zzly.m5559().m5563(f);
    }
}
