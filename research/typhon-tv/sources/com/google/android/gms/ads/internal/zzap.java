package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzap implements Parcelable.Creator<zzao> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r9 = zzbfn.m10169(parcel);
        String str = null;
        float f = 0.0f;
        boolean z = false;
        boolean z2 = false;
        int i = 0;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        while (parcel.dataPosition() < r9) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    z5 = zzbfn.m10168(parcel, readInt);
                    break;
                case 3:
                    z4 = zzbfn.m10168(parcel, readInt);
                    break;
                case 4:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    f = zzbfn.m10151(parcel, readInt);
                    break;
                case 7:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 8:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r9);
        return new zzao(z5, z4, str, z3, f, i, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzao[i];
    }
}
