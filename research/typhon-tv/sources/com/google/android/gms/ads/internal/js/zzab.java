package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzalk;

final class zzab implements zzalk<zzaj> {
    zzab(zzaa zzaa) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m7632(Object obj) {
        zzagf.m4527("Ending javascript session.");
        ((zzak) ((zzaj) obj)).zzlt();
    }
}
