package com.google.android.gms.ads.internal;

import android.content.Context;
import android.view.View;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzafo;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzfs;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzoa;
import com.google.android.gms.internal.zzou;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzuk;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzva;
import com.google.android.gms.internal.zzxc;
import com.google.android.gms.internal.zzzv;

@zzzv
public class zzi extends zzd implements zzae, zzxc {

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f3595;

    public zzi(Context context, zzjn zzjn, String str, zzux zzux, zzakd zzakd, zzv zzv) {
        super(context, zzjn, str, zzux, zzakd, zzv);
    }

    public final void zza(int i, int i2, int i3, int i4) {
        齉();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: com.google.android.gms.internal.zzyb} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zza(com.google.android.gms.internal.zzafp r10, com.google.android.gms.internal.zznu r11) {
        /*
            r9 = this;
            int r0 = r10.f4132
            r1 = -2
            if (r0 == r1) goto L_0x0010
            android.os.Handler r0 = com.google.android.gms.internal.zzahn.f4212
            com.google.android.gms.ads.internal.zzk r1 = new com.google.android.gms.ads.internal.zzk
            r1.<init>(r9, r10)
            r0.post(r1)
        L_0x000f:
            return
        L_0x0010:
            com.google.android.gms.internal.zzjn r0 = r10.f4134
            if (r0 == 0) goto L_0x001a
            com.google.android.gms.ads.internal.zzbt r0 = r9.连任
            com.google.android.gms.internal.zzjn r1 = r10.f4134
            r0.zzauc = r1
        L_0x001a:
            com.google.android.gms.internal.zzaax r0 = r10.f4133
            boolean r0 = r0.f3822
            if (r0 == 0) goto L_0x0046
            com.google.android.gms.internal.zzaax r0 = r10.f4133
            boolean r0 = r0.f3853
            if (r0 != 0) goto L_0x0046
            com.google.android.gms.ads.internal.zzbt r0 = r9.连任
            r1 = 0
            r0.zzauz = r1
            com.google.android.gms.ads.internal.zzbt r8 = r9.连任
            com.google.android.gms.ads.internal.zzbs.zzeh()
            com.google.android.gms.ads.internal.zzbt r0 = r9.连任
            android.content.Context r0 = r0.zzair
            com.google.android.gms.ads.internal.zzbt r1 = r9.连任
            com.google.android.gms.internal.zzcv r3 = r1.f3589
            r4 = 0
            com.google.android.gms.internal.zzux r5 = r9.ˑ
            r1 = r9
            r2 = r10
            r6 = r9
            r7 = r11
            com.google.android.gms.internal.zzaif r0 = com.google.android.gms.internal.zzya.m6026(r0, r1, r2, r3, r4, r5, r6, r7)
            r8.zzaub = r0
            goto L_0x000f
        L_0x0046:
            com.google.android.gms.ads.internal.zzv r0 = r9.ʽ
            com.google.android.gms.internal.zzafc r0 = r0.zzaon
            com.google.android.gms.ads.internal.zzbt r1 = r9.连任
            android.content.Context r1 = r1.zzair
            com.google.android.gms.ads.internal.zzbt r2 = r9.连任
            com.google.android.gms.internal.zzakd r2 = r2.zzaty
            com.google.android.gms.internal.zzaax r3 = r10.f4133
            com.google.android.gms.internal.zzafb r0 = r0.m9567(r1, r2, r3)
            android.os.Handler r1 = com.google.android.gms.internal.zzahn.f4212
            com.google.android.gms.ads.internal.zzl r2 = new com.google.android.gms.ads.internal.zzl
            r2.<init>(r9, r10, r0, r11)
            r1.post(r2)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzi.zza(com.google.android.gms.internal.zzafp, com.google.android.gms.internal.zznu):void");
    }

    public final void zza(zzoa zzoa) {
        zzbq.m9115("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
        this.连任.f3575 = zzoa;
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzafo zzafo, zzafo zzafo2) {
        if (this.连任.zzfk() && this.连任.f3591 != null) {
            this.连任.f3591.zzfn().m4690(zzafo2.f4114);
        }
        try {
            if (zzafo2.f4119 != null && !zzafo2.f4094 && zzafo2.f4091) {
                if (((Boolean) zzkb.m5481().m5595(zznh.f5046)).booleanValue()) {
                    String str = zzafo2.f4122.f4759;
                    if ((str == null || "com.google.ads.mediation.AbstractAdViewAdapter".equals(str)) && !zzafo2.f4122.f4767.containsKey("sdk_less_server_data")) {
                        try {
                            zzafo2.f4119.m4973();
                        } catch (Throwable th) {
                            zzagf.m4527("Could not render test Ad label.");
                        }
                    }
                }
            }
        } catch (RuntimeException e) {
            zzagf.m4527("Could not render test AdLabel.");
        }
        return zzi.super.zza(zzafo, zzafo2);
    }

    public final void zzcs() {
        onAdClicked();
    }

    public final void zzct() {
        recordImpression();
        zzbu();
    }

    public final void zzcu() {
        龘();
    }

    public final void zzh(View view) {
        this.连任.f3572 = view;
        zzb(new zzafo(this.连任.zzaue, (zzanh) null, (zzuh) null, (zzva) null, (String) null, (zzuk) null, (zzou) null, (String) null));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4084(zzanh zzanh) {
        if (this.连任.zzaud != null) {
            zzfs zzfs = this.ʼ;
            zzjn zzjn = this.连任.zzauc;
            zzafo zzafo = this.连任.zzaud;
            if (zzanh == null) {
                throw null;
            }
            zzfs.m5311(zzjn, zzafo, (View) zzanh, zzanh);
            this.f3595 = false;
            return;
        }
        this.f3595 = true;
        zzagf.m4791("Request to enable ActiveView before adState is available.");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m4085() {
        zzi.super.麤();
        if (this.f3595) {
            if (((Boolean) zzkb.m5481().m5595(zznh.f4993)).booleanValue()) {
                m4084(this.连任.zzaud.f4119);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: com.google.android.gms.ads.internal.gmsg.zzb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: com.google.android.gms.ads.internal.overlay.zzq} */
    /* JADX WARNING: type inference failed for: r9v0, types: [com.google.android.gms.ads.internal.zzbl] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.google.android.gms.internal.zzje] */
    /* JADX WARNING: type inference failed for: r2v3, types: [com.google.android.gms.ads.internal.overlay.zzn] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.gms.internal.zzanh m4086(com.google.android.gms.internal.zzafp r13, com.google.android.gms.ads.internal.zzw r14, com.google.android.gms.internal.zzafb r15) throws com.google.android.gms.internal.zzanv {
        /*
            r12 = this;
            com.google.android.gms.ads.internal.zzbt r0 = r12.连任
            com.google.android.gms.ads.internal.zzbu r0 = r0.f3591
            android.view.View r1 = r0.getNextView()
            boolean r0 = r1 instanceof com.google.android.gms.internal.zzanh
            if (r0 == 0) goto L_0x0012
            r0 = r1
            com.google.android.gms.internal.zzanh r0 = (com.google.android.gms.internal.zzanh) r0
            r0.destroy()
        L_0x0012:
            if (r1 == 0) goto L_0x001b
            com.google.android.gms.ads.internal.zzbt r0 = r12.连任
            com.google.android.gms.ads.internal.zzbu r0 = r0.f3591
            r0.removeView(r1)
        L_0x001b:
            com.google.android.gms.internal.zzanr r0 = com.google.android.gms.ads.internal.zzbs.zzej()
            com.google.android.gms.ads.internal.zzbt r1 = r12.连任
            android.content.Context r1 = r1.zzair
            com.google.android.gms.ads.internal.zzbt r2 = r12.连任
            com.google.android.gms.internal.zzjn r2 = r2.zzauc
            com.google.android.gms.internal.zzapa r2 = com.google.android.gms.internal.zzapa.m5224(r2)
            com.google.android.gms.ads.internal.zzbt r3 = r12.连任
            com.google.android.gms.internal.zzjn r3 = r3.zzauc
            java.lang.String r3 = r3.f4798
            r4 = 0
            r5 = 0
            com.google.android.gms.ads.internal.zzbt r6 = r12.连任
            com.google.android.gms.internal.zzcv r6 = r6.f3589
            com.google.android.gms.ads.internal.zzbt r7 = r12.连任
            com.google.android.gms.internal.zzakd r7 = r7.zzaty
            com.google.android.gms.internal.zznu r8 = r12.龘
            com.google.android.gms.ads.internal.zzv r10 = r12.ʽ
            com.google.android.gms.internal.zzis r11 = r13.f4130
            r9 = r12
            com.google.android.gms.internal.zzanh r10 = r0.m5060(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            com.google.android.gms.ads.internal.zzbt r0 = r12.连任
            com.google.android.gms.internal.zzjn r0 = r0.zzauc
            com.google.android.gms.internal.zzjn[] r0 = r0.f4790
            if (r0 != 0) goto L_0x0058
            if (r10 != 0) goto L_0x0052
            r0 = 0
            throw r0
        L_0x0052:
            r0 = r10
            android.view.View r0 = (android.view.View) r0
            r12.龘(r0)
        L_0x0058:
            com.google.android.gms.internal.zzani r0 = r10.m4980()
            r5 = 0
            r6 = 0
            r1 = r12
            r2 = r12
            r3 = r12
            r4 = r12
            r7 = r14
            r8 = r12
            r9 = r15
            r0.m5052(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            r12.m4087(r10)
            com.google.android.gms.internal.zzaat r0 = r13.f4136
            java.lang.String r0 = r0.f3738
            r10.m5000((java.lang.String) r0)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzi.m4086(com.google.android.gms.internal.zzafp, com.google.android.gms.ads.internal.zzw, com.google.android.gms.internal.zzafb):com.google.android.gms.internal.zzanh");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4087(zzanh zzanh) {
        zzanh.m5014("/trackActiveViewUnit", new zzj(this));
    }
}
