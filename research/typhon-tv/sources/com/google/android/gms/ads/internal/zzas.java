package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzanm;
import com.google.android.gms.internal.zzol;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class zzas implements zzanm {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f6734;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzanh f6735;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzol f6736;

    zzas(zzol zzol, String str, zzanh zzanh) {
        this.f6736 = zzol;
        this.f6734 = str;
        this.f6735 = zzanh;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7651(zzanh zzanh, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("headline", (Object) this.f6736.m5696());
            jSONObject.put(TtmlNode.TAG_BODY, (Object) this.f6736.m5690());
            jSONObject.put("call_to_action", (Object) this.f6736.m5681());
            jSONObject.put("advertiser", (Object) this.f6736.m5682());
            jSONObject.put("logo", (Object) zzaq.m4060(this.f6736.m5680()));
            JSONArray jSONArray = new JSONArray();
            List<Object> r2 = this.f6736.m5691();
            if (r2 != null) {
                for (Object r3 : r2) {
                    jSONArray.put((Object) zzaq.m4060(zzaq.m4053(r3)));
                }
            }
            jSONObject.put("images", (Object) jSONArray);
            jSONObject.put("extras", (Object) zzaq.m4055(this.f6736.m5684(), this.f6734));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("assets", (Object) jSONObject);
            jSONObject2.put("template_id", (Object) PubnativeRequest.LEGACY_ZONE_ID);
            this.f6735.zzb("google.afma.nativeExpressAds.loadAssets", jSONObject2);
        } catch (JSONException e) {
            zzagf.m4796("Exception occurred when loading assets", e);
        }
    }
}
