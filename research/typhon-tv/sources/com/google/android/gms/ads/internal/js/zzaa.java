package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzalj;
import com.google.android.gms.internal.zzall;

public final class zzaa extends zzall<zzaj> {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzae f6662;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f6663;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f6664 = new Object();

    public zzaa(zzae zzae) {
        this.f6662 = zzae;
    }

    public final void release() {
        synchronized (this.f6664) {
            if (!this.f6663) {
                this.f6663 = true;
                zza(new zzab(this), new zzalj());
                zza(new zzac(this), new zzad(this));
            }
        }
    }
}
