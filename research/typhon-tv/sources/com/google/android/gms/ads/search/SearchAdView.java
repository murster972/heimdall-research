package com.google.android.gms.ads.search;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzlv;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class SearchAdView extends ViewGroup {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzlv f3619;

    public SearchAdView(Context context) {
        super(context);
        this.f3619 = new zzlv(this);
    }

    public SearchAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3619 = new zzlv(this, attributeSet, false);
    }

    public SearchAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3619 = new zzlv(this, attributeSet, false);
    }

    public final void destroy() {
        this.f3619.m5527();
    }

    public final AdListener getAdListener() {
        return this.f3619.m5523();
    }

    public final AdSize getAdSize() {
        return this.f3619.m5526();
    }

    public final String getAdUnitId() {
        return this.f3619.m5522();
    }

    public final void loadAd(DynamicHeightSearchAdRequest dynamicHeightSearchAdRequest) {
        if (!AdSize.SEARCH.equals(getAdSize())) {
            throw new IllegalStateException("You must use AdSize.SEARCH for a DynamicHeightSearchAdRequest");
        }
        this.f3619.m5534(dynamicHeightSearchAdRequest.m4102());
    }

    public final void loadAd(SearchAdRequest searchAdRequest) {
        this.f3619.m5534(searchAdRequest.m4103());
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3;
        int i4;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzakb.m4793("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
                i4 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            int measuredWidth = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
            i4 = measuredWidth;
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }

    public final void pause() {
        this.f3619.m5515();
    }

    public final void resume() {
        this.f3619.m5520();
    }

    public final void setAdListener(AdListener adListener) {
        this.f3619.m5528(adListener);
    }

    public final void setAdSize(AdSize adSize) {
        this.f3619.m5537(adSize);
    }

    public final void setAdUnitId(String str) {
        this.f3619.m5535(str);
    }
}
