package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.support.annotation.Keep;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahh;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzzv;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Keep
@KeepName
@zzzv
public class HttpClient implements zzt<com.google.android.gms.ads.internal.js.zza> {
    private final Context mContext;
    private final zzakd zzapr;

    @zzzv
    static class zza {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f3391;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f3392;

        public zza(String str, String str2) {
            this.f3392 = str;
            this.f3391 = str2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final String m3994() {
            return this.f3391;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m3995() {
            return this.f3392;
        }
    }

    @zzzv
    static class zzb {

        /* renamed from: 靐  reason: contains not printable characters */
        private final URL f3393;

        /* renamed from: 麤  reason: contains not printable characters */
        private final String f3394;

        /* renamed from: 齉  reason: contains not printable characters */
        private final ArrayList<zza> f3395;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f3396;

        zzb(String str, URL url, ArrayList<zza> arrayList, String str2) {
            this.f3396 = str;
            this.f3393 = url;
            this.f3395 = arrayList;
            this.f3394 = str2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final URL m3996() {
            return this.f3393;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final String m3997() {
            return this.f3394;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final ArrayList<zza> m3998() {
            return this.f3395;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m3999() {
            return this.f3396;
        }
    }

    @zzzv
    class zzc {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f3397;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f3398;

        /* renamed from: 龘  reason: contains not printable characters */
        private final zzd f3399;

        public zzc(HttpClient httpClient, boolean z, zzd zzd, String str) {
            this.f3397 = z;
            this.f3399 = zzd;
            this.f3398 = str;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final zzd m4000() {
            return this.f3399;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final boolean m4001() {
            return this.f3397;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m4002() {
            return this.f3398;
        }
    }

    @zzzv
    static class zzd {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f3400;

        /* renamed from: 麤  reason: contains not printable characters */
        private final String f3401;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<zza> f3402;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f3403;

        zzd(String str, int i, List<zza> list, String str2) {
            this.f3403 = str;
            this.f3400 = i;
            this.f3402 = list;
            this.f3401 = str2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final int m4003() {
            return this.f3400;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final String m4004() {
            return this.f3401;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final Iterable<zza> m4005() {
            return this.f3402;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m4006() {
            return this.f3403;
        }
    }

    public HttpClient(Context context, zzakd zzakd) {
        this.mContext = context;
        this.zzapr = zzakd;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: byte[]} */
    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.ads.internal.gmsg.HttpClient.zzc zza(com.google.android.gms.ads.internal.gmsg.HttpClient.zzb r10) {
        /*
            r9 = this;
            r2 = 0
            r3 = 0
            java.net.URL r0 = r10.m3996()     // Catch:{ Exception -> 0x0110, all -> 0x0109 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0110, all -> 0x0109 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0110, all -> 0x0109 }
            com.google.android.gms.internal.zzahn r1 = com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            android.content.Context r4 = r9.mContext     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            com.google.android.gms.internal.zzakd r5 = r9.zzapr     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r5 = r5.f4297     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r6 = 0
            r1.m4636((android.content.Context) r4, (java.lang.String) r5, (boolean) r6, (java.net.HttpURLConnection) r0)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.ArrayList r1 = r10.m3998()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.ArrayList r1 = (java.util.ArrayList) r1     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            int r5 = r1.size()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r4 = r2
        L_0x0025:
            if (r4 >= r5) goto L_0x004e
            java.lang.Object r2 = r1.get(r4)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            int r4 = r4 + 1
            com.google.android.gms.ads.internal.gmsg.HttpClient$zza r2 = (com.google.android.gms.ads.internal.gmsg.HttpClient.zza) r2     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r6 = r2.m3995()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r2 = r2.m3994()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r0.addRequestProperty(r6, r2)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            goto L_0x0025
        L_0x003b:
            r1 = move-exception
            r2 = r0
        L_0x003d:
            com.google.android.gms.ads.internal.gmsg.HttpClient$zzc r0 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zzc     // Catch:{ all -> 0x010c }
            r3 = 0
            r4 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x010c }
            r0.<init>(r9, r3, r4, r1)     // Catch:{ all -> 0x010c }
            if (r2 == 0) goto L_0x004d
            r2.disconnect()
        L_0x004d:
            return r0
        L_0x004e:
            java.lang.String r1 = r10.m3997()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            if (r1 != 0) goto L_0x0077
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r1 = r10.m3997()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            byte[] r3 = r1.getBytes()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            int r1 = r3.length     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r0.setFixedLengthStreamingMode(r1)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r1.write(r3)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r1.close()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
        L_0x0077:
            com.google.android.gms.internal.zzajv r4 = new com.google.android.gms.internal.zzajv     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r4.<init>()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r4.m4788((java.net.HttpURLConnection) r0, (byte[]) r3)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r5.<init>()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.Map r1 = r0.getHeaderFields()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            if (r1 == 0) goto L_0x00cf
            java.util.Map r1 = r0.getHeaderFields()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.Set r1 = r1.entrySet()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
        L_0x0096:
            boolean r1 = r6.hasNext()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            if (r1 == 0) goto L_0x00cf
            java.lang.Object r1 = r6.next()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.Object r2 = r1.getValue()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.List r2 = (java.util.List) r2     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.util.Iterator r7 = r2.iterator()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
        L_0x00ac:
            boolean r2 = r7.hasNext()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            if (r2 == 0) goto L_0x0096
            java.lang.Object r2 = r7.next()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            com.google.android.gms.ads.internal.gmsg.HttpClient$zza r8 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zza     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.Object r3 = r1.getKey()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r8.<init>(r3, r2)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r5.add(r8)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            goto L_0x00ac
        L_0x00c7:
            r1 = move-exception
            r3 = r0
        L_0x00c9:
            if (r3 == 0) goto L_0x00ce
            r3.disconnect()
        L_0x00ce:
            throw r1
        L_0x00cf:
            com.google.android.gms.ads.internal.gmsg.HttpClient$zzd r2 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zzd     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r1 = r10.m3999()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            int r3 = r0.getResponseCode()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.io.InputStream r7 = r0.getInputStream()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r6 = com.google.android.gms.internal.zzahn.m4601((java.io.InputStreamReader) r6)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r2.<init>(r1, r3, r5, r6)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            int r1 = r2.m4003()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r4.m4787((java.net.HttpURLConnection) r0, (int) r1)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            java.lang.String r1 = r2.m4004()     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r4.m4785((java.lang.String) r1)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            com.google.android.gms.ads.internal.gmsg.HttpClient$zzc r1 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zzc     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            r3 = 1
            r4 = 0
            r1.<init>(r9, r3, r2, r4)     // Catch:{ Exception -> 0x003b, all -> 0x00c7 }
            if (r0 == 0) goto L_0x0106
            r0.disconnect()
        L_0x0106:
            r0 = r1
            goto L_0x004d
        L_0x0109:
            r0 = move-exception
            r1 = r0
            goto L_0x00c9
        L_0x010c:
            r0 = move-exception
            r1 = r0
            r3 = r2
            goto L_0x00c9
        L_0x0110:
            r0 = move-exception
            r1 = r0
            r2 = r3
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.HttpClient.zza(com.google.android.gms.ads.internal.gmsg.HttpClient$zzb):com.google.android.gms.ads.internal.gmsg.HttpClient$zzc");
    }

    private static JSONObject zza(zzd zzd2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("http_request_id", (Object) zzd2.m4006());
            if (zzd2.m4004() != null) {
                jSONObject.put(TtmlNode.TAG_BODY, (Object) zzd2.m4004());
            }
            JSONArray jSONArray = new JSONArray();
            for (zza next : zzd2.m4005()) {
                jSONArray.put((Object) new JSONObject().put("key", (Object) next.m3995()).put("value", (Object) next.m3994()));
            }
            jSONObject.put("headers", (Object) jSONArray);
            jSONObject.put("response_code", zzd2.m4003());
        } catch (JSONException e) {
            zzagf.m4793("Error constructing JSON for http response.", e);
        }
        return jSONObject;
    }

    private static zzb zzd(JSONObject jSONObject) {
        URL url;
        String optString = jSONObject.optString("http_request_id");
        String optString2 = jSONObject.optString("url");
        String optString3 = jSONObject.optString("post_body", (String) null);
        try {
            url = new URL(optString2);
        } catch (MalformedURLException e) {
            zzagf.m4793("Error constructing http request.", e);
            url = null;
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(new zza(optJSONObject.optString("key"), optJSONObject.optString("value")));
            }
        }
        return new zzb(optString, url, arrayList, optString3);
    }

    @Keep
    @KeepName
    public JSONObject send(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            String optString = jSONObject.optString("http_request_id");
            zzc zza2 = zza(zzd(jSONObject));
            if (zza2.m4001()) {
                jSONObject2.put("response", (Object) zza(zza2.m4000()));
                jSONObject2.put("success", true);
            } else {
                jSONObject2.put("response", (Object) new JSONObject().put("http_request_id", (Object) optString));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", (Object) zza2.m4002());
            }
        } catch (Exception e) {
            zzagf.m4793("Error executing http request.", e);
            try {
                jSONObject2.put("response", (Object) new JSONObject().put("http_request_id", (Object) ""));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", (Object) e.toString());
            } catch (JSONException e2) {
                zzagf.m4793("Error executing http request.", e2);
            }
        }
        return jSONObject2;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzahh.m4555((Runnable) new zzu(this, map, (com.google.android.gms.ads.internal.js.zza) obj));
    }
}
