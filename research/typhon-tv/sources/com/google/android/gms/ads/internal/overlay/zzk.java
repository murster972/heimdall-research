package com.google.android.gms.ads.internal.overlay;

import android.graphics.drawable.Drawable;

final class zzk implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzj f6711;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Drawable f6712;

    zzk(zzj zzj, Drawable drawable) {
        this.f6711 = zzj;
        this.f6712 = drawable;
    }

    public final void run() {
        this.f6711.f3463.f3457.getWindow().setBackgroundDrawable(this.f6712);
    }
}
