package com.google.android.gms.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzlt;
import com.google.android.gms.internal.zzlu;
import java.util.Date;
import java.util.Set;

public final class AdRequest {
    public static final String DEVICE_ID_EMULATOR = "B3EEABB8EE11C2BE770B684D95219ECB";
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_INVALID_REQUEST = 1;
    public static final int ERROR_CODE_NETWORK_ERROR = 2;
    public static final int ERROR_CODE_NO_FILL = 3;
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_UNKNOWN = 0;
    public static final int MAX_CONTENT_URL_LENGTH = 512;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzlt f3367;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final zzlu f6632 = new zzlu();

        public Builder() {
            this.f6632.靐("B3EEABB8EE11C2BE770B684D95219ECB");
        }

        public final Builder addCustomEventExtrasBundle(Class<? extends CustomEvent> cls, Bundle bundle) {
            this.f6632.靐(cls, bundle);
            return this;
        }

        public final Builder addKeyword(String str) {
            this.f6632.龘(str);
            return this;
        }

        public final Builder addNetworkExtras(NetworkExtras networkExtras) {
            this.f6632.龘(networkExtras);
            return this;
        }

        public final Builder addNetworkExtrasBundle(Class<? extends MediationAdapter> cls, Bundle bundle) {
            this.f6632.龘(cls, bundle);
            if (cls.equals(AdMobAdapter.class) && bundle.getBoolean("_emulatorLiveAds")) {
                this.f6632.齉("B3EEABB8EE11C2BE770B684D95219ECB");
            }
            return this;
        }

        public final Builder addTestDevice(String str) {
            this.f6632.靐(str);
            return this;
        }

        public final AdRequest build() {
            return new AdRequest(this);
        }

        public final Builder setBirthday(Date date) {
            this.f6632.龘(date);
            return this;
        }

        public final Builder setContentUrl(String str) {
            zzbq.m9121(str, (Object) "Content URL must be non-null.");
            zzbq.m9123(str, (Object) "Content URL must be non-empty.");
            zzbq.m9118(str.length() <= 512, "Content URL must not exceed %d in length.  Provided length was %d.", 512, Integer.valueOf(str.length()));
            this.f6632.麤(str);
            return this;
        }

        public final Builder setGender(int i) {
            this.f6632.龘(i);
            return this;
        }

        public final Builder setIsDesignedForFamilies(boolean z) {
            this.f6632.齉(z);
            return this;
        }

        public final Builder setLocation(Location location) {
            this.f6632.龘(location);
            return this;
        }

        public final Builder setRequestAgent(String str) {
            this.f6632.ʻ(str);
            return this;
        }

        public final Builder tagForChildDirectedTreatment(boolean z) {
            this.f6632.靐(z);
            return this;
        }
    }

    private AdRequest(Builder builder) {
        this.f3367 = new zzlt(builder.f6632);
    }

    public final Date getBirthday() {
        return this.f3367.m5507();
    }

    public final String getContentUrl() {
        return this.f3367.m5502();
    }

    public final <T extends CustomEvent> Bundle getCustomEventExtrasBundle(Class<T> cls) {
        return this.f3367.m5505(cls);
    }

    public final int getGender() {
        return this.f3367.m5504();
    }

    public final Set<String> getKeywords() {
        return this.f3367.m5503();
    }

    public final Location getLocation() {
        return this.f3367.m5500();
    }

    @Deprecated
    public final <T extends NetworkExtras> T getNetworkExtras(Class<T> cls) {
        return this.f3367.m5506(cls);
    }

    public final <T extends MediationAdapter> Bundle getNetworkExtrasBundle(Class<T> cls) {
        return this.f3367.m5501(cls);
    }

    public final boolean isTestDevice(Context context) {
        return this.f3367.m5508(context);
    }

    public final zzlt zzbg() {
        return this.f3367;
    }
}
