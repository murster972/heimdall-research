package com.google.android.gms.ads.mediation.customevent;

import com.google.ads.mediation.NetworkExtras;
import java.util.HashMap;

@Deprecated
public final class CustomEventExtras implements NetworkExtras {

    /* renamed from: 龘  reason: contains not printable characters */
    private final HashMap<String, Object> f3613 = new HashMap<>();

    public final Object getExtra(String str) {
        return this.f3613.get(str);
    }

    public final void setExtra(String str, Object obj) {
        this.f3613.put(str, obj);
    }
}
