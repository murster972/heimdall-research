package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzoj;

final class zzbe implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzba f6759;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzoj f6760;

    zzbe(zzba zzba, zzoj zzoj) {
        this.f6759 = zzba;
        this.f6760 = zzoj;
    }

    public final void run() {
        try {
            if (this.f6759.连任.f3569 != null) {
                this.f6759.连任.f3569.m13341(this.f6760);
            }
        } catch (RemoteException e) {
            zzagf.m4796("Could not call OnAppInstallAdLoadedListener.onAppInstallAdLoaded().", e);
        }
    }
}
