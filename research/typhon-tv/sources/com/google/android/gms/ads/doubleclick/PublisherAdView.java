package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzks;
import com.google.android.gms.internal.zzlv;

public final class PublisherAdView extends ViewGroup {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzlv f6641;

    public PublisherAdView(Context context) {
        super(context);
        this.f6641 = new zzlv(this);
    }

    public PublisherAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f6641 = new zzlv(this, attributeSet, true);
        zzbq.m9121(context, (Object) "Context cannot be null");
    }

    public PublisherAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f6641 = new zzlv(this, attributeSet, true);
    }

    public final void destroy() {
        this.f6641.m5527();
    }

    public final AdListener getAdListener() {
        return this.f6641.m5523();
    }

    public final AdSize getAdSize() {
        return this.f6641.m5526();
    }

    public final AdSize[] getAdSizes() {
        return this.f6641.m5525();
    }

    public final String getAdUnitId() {
        return this.f6641.m5522();
    }

    public final AppEventListener getAppEventListener() {
        return this.f6641.m5513();
    }

    public final String getMediationAdapterClassName() {
        return this.f6641.m5521();
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.f6641.m5514();
    }

    public final VideoController getVideoController() {
        return this.f6641.m5516();
    }

    public final VideoOptions getVideoOptions() {
        return this.f6641.m5539();
    }

    public final boolean isLoading() {
        return this.f6641.m5518();
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest) {
        this.f6641.m5534(publisherAdRequest.zzbg());
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3;
        int i4;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzakb.m4793("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
                i4 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            int measuredWidth = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
            i4 = measuredWidth;
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }

    public final void pause() {
        this.f6641.m5515();
    }

    public final void recordManualImpression() {
        this.f6641.m5519();
    }

    public final void resume() {
        this.f6641.m5520();
    }

    public final void setAdListener(AdListener adListener) {
        this.f6641.m5528(adListener);
    }

    public final void setAdSizes(AdSize... adSizeArr) {
        if (adSizeArr == null || adSizeArr.length <= 0) {
            throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
        }
        this.f6641.m5524(adSizeArr);
    }

    public final void setAdUnitId(String str) {
        this.f6641.m5535(str);
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        this.f6641.m5531(appEventListener);
    }

    public final void setCorrelator(Correlator correlator) {
        this.f6641.m5529(correlator);
    }

    public final void setManualImpressionsEnabled(boolean z) {
        this.f6641.m5536(z);
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.f6641.m5532(onCustomRenderedAdLoadedListener);
    }

    public final void setVideoOptions(VideoOptions videoOptions) {
        this.f6641.m5530(videoOptions);
    }

    public final boolean zza(zzks zzks) {
        return this.f6641.m5538(zzks);
    }
}
