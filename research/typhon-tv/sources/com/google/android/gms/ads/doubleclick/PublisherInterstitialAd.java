package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzlx;

public final class PublisherInterstitialAd {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzlx f6642;

    public PublisherInterstitialAd(Context context) {
        this.f6642 = new zzlx(context, this);
        zzbq.m9121(context, (Object) "Context cannot be null");
    }

    public final AdListener getAdListener() {
        return this.f6642.m5549();
    }

    public final String getAdUnitId() {
        return this.f6642.m5545();
    }

    public final AppEventListener getAppEventListener() {
        return this.f6642.m5548();
    }

    public final String getMediationAdapterClassName() {
        return this.f6642.m5542();
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.f6642.m5547();
    }

    public final boolean isLoaded() {
        return this.f6642.m5544();
    }

    public final boolean isLoading() {
        return this.f6642.m5541();
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest) {
        this.f6642.m5556(publisherAdRequest.zzbg());
    }

    public final void setAdListener(AdListener adListener) {
        this.f6642.m5550(adListener);
    }

    public final void setAdUnitId(String str) {
        this.f6642.m5557(str);
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        this.f6642.m5552(appEventListener);
    }

    public final void setCorrelator(Correlator correlator) {
        this.f6642.m5551(correlator);
    }

    public final void setImmersiveMode(boolean z) {
        this.f6642.m5546(z);
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.f6642.m5553(onCustomRenderedAdLoadedListener);
    }

    public final void show() {
        this.f6642.m5543();
    }
}
