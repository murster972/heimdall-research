package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzafo;
import com.google.android.gms.internal.zzafp;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzll;
import com.google.android.gms.internal.zzoa;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzoj;
import com.google.android.gms.internal.zzol;
import com.google.android.gms.internal.zzoq;
import com.google.android.gms.internal.zzor;
import com.google.android.gms.internal.zzos;
import com.google.android.gms.internal.zzot;
import com.google.android.gms.internal.zzou;
import com.google.android.gms.internal.zzqm;
import com.google.android.gms.internal.zzqw;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzui;
import com.google.android.gms.internal.zzuk;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzva;
import com.google.android.gms.internal.zzvj;
import com.google.android.gms.internal.zzvm;
import com.google.android.gms.internal.zzzv;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@zzzv
public final class zzq extends zzd implements zzot {

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f3596 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f3597;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public zzafo f3598;

    public zzq(Context context, zzv zzv, zzjn zzjn, String str, zzux zzux, zzakd zzakd) {
        super(context, zzjn, str, zzux, zzakd, zzv);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzui m4089() {
        if (this.连任.zzaud == null || !this.连任.zzaud.f4094) {
            return null;
        }
        return this.连任.zzaud.f4101;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean m4090(zzafo zzafo, zzafo zzafo2) {
        View zze = zzaq.zze(zzafo2);
        if (zze == null) {
            return false;
        }
        View nextView = this.连任.f3591.getNextView();
        if (nextView != null) {
            if (nextView instanceof zzanh) {
                ((zzanh) nextView).destroy();
            }
            this.连任.f3591.removeView(nextView);
        }
        if (!zzaq.zzf(zzafo2)) {
            try {
                龘(zze);
            } catch (Throwable th) {
                zzbs.zzem().m4505(th, "AdLoaderManager.swapBannerViews");
                zzagf.m4796("Could not add mediation view to view hierarchy.", th);
                return false;
            }
        }
        if (this.连任.f3591.getChildCount() > 1) {
            this.连任.f3591.showNext();
        }
        if (zzafo != null) {
            View nextView2 = this.连任.f3591.getNextView();
            if (nextView2 != null) {
                this.连任.f3591.removeView(nextView2);
            }
            this.连任.zzfj();
        }
        this.连任.f3591.setMinimumWidth(zzbs().f4789);
        this.连任.f3591.setMinimumHeight(zzbs().f4797);
        this.连任.f3591.requestLayout();
        this.连任.f3591.setVisibility(0);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzafo m4092(zzafp zzafp, int i) {
        return new zzafo(zzafp.f4136.f3763, (zzanh) null, zzafp.f4133.f3860, i, zzafp.f4133.f3857, zzafp.f4133.f3844, zzafp.f4133.f3849, zzafp.f4133.f3848, zzafp.f4136.f3740, zzafp.f4133.f3822, (zzuh) null, (zzva) null, (String) null, zzafp.f4135, (zzuk) null, zzafp.f4133.f3824, zzafp.f4134, zzafp.f4133.f3820, zzafp.f4127, zzafp.f4128, zzafp.f4133.f3828, zzafp.f4129, (zzou) null, zzafp.f4133.f3854, zzafp.f4133.f3855, zzafp.f4133.f3855, zzafp.f4133.f3862, zzafp.f4133.f3865, (String) null, zzafp.f4133.f3821, zzafp.f4133.f3827, zzafp.f4130, zzafp.f4133.f3835, zzafp.f4131);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m4093(zzafo zzafo, zzafo zzafo2) {
        zzd((List<String>) null);
        if (!this.连任.zzfk()) {
            zzagf.m4791("Native ad does not have custom rendering mode.");
            龘(0);
            return false;
        }
        try {
            zzvj r18 = zzafo2.f4124 != null ? zzafo2.f4124.m13457() : null;
            zzvm r3 = zzafo2.f4124 != null ? zzafo2.f4124.m13461() : null;
            zzqm r2 = zzafo2.f4124 != null ? zzafo2.f4124.m13459() : null;
            String r17 = 靐(zzafo2);
            if (r18 == null || this.连任.f3569 == null) {
                if (r3 != null) {
                    if (this.连任.f3579 != null) {
                        zzol zzol = new zzol(r3.m13586(), r3.m13581(), r3.m13584(), r3.m13583() != null ? r3.m13583() : null, r3.m13580(), r3.m13571(), (zzog) null, r3.m13578(), r3.m13576(), r3.m13575() != null ? (View) zzn.m9307(r3.m13575()) : null, r3.m13588(), r17);
                        zzol.m5698((zzos) new zzor(this.连任.zzair, (zzot) this, this.连任.f3589, r3, (zzou) zzol));
                        zzahn.f4212.post(new zzt(this, zzol));
                    }
                }
                if (r2 != null) {
                    if (!(this.连任.f3583 == null || this.连任.f3583.get(r2.m13319()) == null)) {
                        zzahn.f4212.post(new zzu(this, r2));
                    }
                }
                zzagf.m4791("No matching mapper/listener for retrieved native ad template.");
                龘(0);
                return false;
            }
            zzoj zzoj = new zzoj(r18.m13547(), r18.m13542(), r18.m13545(), r18.m13544() != null ? r18.m13544() : null, r18.m13541(), r18.m13531(), r18.m13532(), r18.m13533(), (zzog) null, r18.m13536(), r18.m13534(), r18.m13550() != null ? (View) zzn.m9307(r18.m13550()) : null, r18.m13537(), r17);
            zzoj.m5675((zzos) new zzor(this.连任.zzair, (zzot) this, this.连任.f3589, r18, (zzou) zzoj));
            zzahn.f4212.post(new zzs(this, zzoj));
            return zzq.super.zza(zzafo, zzafo2);
        } catch (RemoteException e) {
            zzagf.m4796("Failed to get native ad mapper", e);
            龘(0);
            return false;
        }
    }

    public final zzll getVideoController() {
        return null;
    }

    public final void pause() {
        if (!this.f3596) {
            throw new IllegalStateException("Native Ad does not support pause().");
        }
        zzq.super.pause();
    }

    public final void resume() {
        if (!this.f3596) {
            throw new IllegalStateException("Native Ad does not support resume().");
        }
        zzq.super.resume();
    }

    public final void setManualImpressionsEnabled(boolean z) {
        zzbq.m9115("setManualImpressionsEnabled must be called from the main thread.");
        this.f3597 = z;
    }

    public final void showInterstitial() {
        throw new IllegalStateException("Interstitial is not supported by AdLoaderManager.");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: com.google.android.gms.internal.zzyb} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.internal.zzafp r10, com.google.android.gms.internal.zznu r11) {
        /*
            r9 = this;
            r4 = 0
            r2 = 0
            r9.f3598 = r4
            int r0 = r10.f4132
            r1 = -2
            if (r0 == r1) goto L_0x0020
            int r0 = r10.f4132
            com.google.android.gms.internal.zzafo r0 = m4092((com.google.android.gms.internal.zzafp) r10, (int) r0)
            r9.f3598 = r0
        L_0x0011:
            com.google.android.gms.internal.zzafo r0 = r9.f3598
            if (r0 == 0) goto L_0x0033
            android.os.Handler r0 = com.google.android.gms.internal.zzahn.f4212
            com.google.android.gms.ads.internal.zzr r1 = new com.google.android.gms.ads.internal.zzr
            r1.<init>(r9)
            r0.post(r1)
        L_0x001f:
            return
        L_0x0020:
            com.google.android.gms.internal.zzaax r0 = r10.f4133
            boolean r0 = r0.f3822
            if (r0 != 0) goto L_0x0011
            java.lang.String r0 = "partialAdState is not mediation"
            com.google.android.gms.internal.zzagf.m4791(r0)
            com.google.android.gms.internal.zzafo r0 = m4092((com.google.android.gms.internal.zzafp) r10, (int) r2)
            r9.f3598 = r0
            goto L_0x0011
        L_0x0033:
            com.google.android.gms.internal.zzjn r0 = r10.f4134
            if (r0 == 0) goto L_0x003d
            com.google.android.gms.ads.internal.zzbt r0 = r9.连任
            com.google.android.gms.internal.zzjn r1 = r10.f4134
            r0.zzauc = r1
        L_0x003d:
            com.google.android.gms.ads.internal.zzbt r0 = r9.连任
            r0.zzauz = r2
            com.google.android.gms.ads.internal.zzbt r8 = r9.连任
            com.google.android.gms.ads.internal.zzbs.zzeh()
            com.google.android.gms.ads.internal.zzbt r0 = r9.连任
            android.content.Context r0 = r0.zzair
            com.google.android.gms.ads.internal.zzbt r1 = r9.连任
            com.google.android.gms.internal.zzcv r3 = r1.f3589
            com.google.android.gms.internal.zzux r5 = r9.ˑ
            r1 = r9
            r2 = r10
            r6 = r9
            r7 = r11
            com.google.android.gms.internal.zzaif r0 = com.google.android.gms.internal.zzya.m6026(r0, r1, r2, r3, r4, r5, r6, r7)
            r8.zzaub = r0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzq.zza(com.google.android.gms.internal.zzafp, com.google.android.gms.internal.zznu):void");
    }

    public final void zza(zzoa zzoa) {
        throw new IllegalStateException("CustomRendering is not supported by AdLoaderManager.");
    }

    public final void zza(zzoq zzoq) {
        zzagf.m4791("Unexpected call to AdLoaderManager method");
    }

    public final void zza(zzos zzos) {
        zzagf.m4791("Unexpected call to AdLoaderManager method");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzafo zzafo, zzafo zzafo2) {
        boolean z;
        if (!this.连任.zzfk()) {
            throw new IllegalStateException("AdLoader API does not support custom rendering.");
        } else if (!zzafo2.f4094) {
            龘(0);
            zzagf.m4791("newState is not mediation.");
            return false;
        } else {
            if (zzafo2.f4096 != null && zzafo2.f4096.m5879()) {
                if (this.连任.zzfk() && this.连任.f3591 != null) {
                    this.连任.f3591.zzfn().m4690(zzafo2.f4114);
                }
                if (!zzq.super.zza(zzafo, zzafo2)) {
                    z = false;
                } else if (!this.连任.zzfk() || m4090(zzafo, zzafo2)) {
                    if (!this.连任.zzfl()) {
                        zzq.super.龘(zzafo2, false);
                    }
                    z = true;
                } else {
                    龘(0);
                    z = false;
                }
                if (!z) {
                    return false;
                }
                this.f3596 = true;
            } else if (zzafo2.f4096 == null || !zzafo2.f4096.m5878()) {
                龘(0);
                zzagf.m4791("Response is neither banner nor native.");
                return false;
            } else if (!m4093(zzafo, zzafo2)) {
                return false;
            }
            zze(new ArrayList(Arrays.asList(new Integer[]{2})));
            return true;
        }
    }

    public final boolean zzb(zzjj zzjj) {
        if (this.连任.f3594 != null && this.连任.f3594.size() == 1 && this.连任.f3594.get(0).intValue() == 2) {
            zzagf.m4795("Requesting only banner Ad from AdLoader or calling loadAd on returned banner is not yet supported");
            龘(0);
            return false;
        } else if (this.连任.f3593 == null) {
            return zzq.super.zzb(zzjj);
        } else {
            if (zzjj.f4755 != this.f3597) {
                zzjj = new zzjj(zzjj.f4768, zzjj.f4765, zzjj.f4767, zzjj.f4766, zzjj.f4764, zzjj.f4753, zzjj.f4754, zzjj.f4755 || this.f3597, zzjj.f4761, zzjj.f4762, zzjj.f4763, zzjj.f4758, zzjj.f4756, zzjj.f4757, zzjj.f4769, zzjj.f4770, zzjj.f4759, zzjj.f4760);
            }
            return zzq.super.zzb(zzjj);
        }
    }

    public final void zzcj() {
        if (this.连任.zzaud == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.连任.zzaud.f4125) || this.连任.zzaud.f4096 == null || !this.连任.zzaud.f4096.m5878()) {
            zzq.super.zzcj();
        } else {
            zzca();
        }
    }

    public final void zzco() {
        if (this.连任.zzaud == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.连任.zzaud.f4125) || this.连任.zzaud.f4096 == null || !this.连任.zzaud.f4096.m5878()) {
            zzq.super.zzco();
        } else {
            zzbz();
        }
    }

    public final void zzcv() {
        zzagf.m4791("Unexpected call to AdLoaderManager method");
    }

    public final boolean zzcw() {
        if (m4089() != null) {
            return m4089().f5438;
        }
        return false;
    }

    public final boolean zzcx() {
        if (m4089() != null) {
            return m4089().f5439;
        }
        return false;
    }

    public final void zzd(List<String> list) {
        zzbq.m9115("setNativeTemplates must be called on the main UI thread.");
        this.连任.f3577 = list;
    }

    public final void zze(List<Integer> list) {
        zzbq.m9115("setAllowedAdTypes must be called on the main UI thread.");
        this.连任.f3594 = list;
    }

    public final zzqw zzs(String str) {
        zzbq.m9115("getOnCustomClickListener must be called on the main UI thread.");
        return this.连任.f3582.get(str);
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.google.android.gms.ads.internal.zzd, com.google.android.gms.internal.zzks, com.google.android.gms.ads.internal.zzq] */
    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m4094() {
        zzq.super.麤();
        zzafo zzafo = this.连任.zzaud;
        if (zzafo != null && zzafo.f4096 != null && zzafo.f4096.m5879() && this.连任.f3593 != null) {
            try {
                this.连任.f3593.m13377(this, zzn.m9306(this.连任.zzair));
            } catch (RemoteException e) {
                zzagf.m4796("Could not call PublisherAdViewLoadedListener.onPublisherAdViewLoaded().", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m4095(zzjj zzjj, zzafo zzafo, boolean z) {
        return false;
    }
}
