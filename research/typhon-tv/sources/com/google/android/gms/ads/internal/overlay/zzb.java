package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzb implements Parcelable.Creator<zzc> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r9 = zzbfn.m10169(parcel);
        Intent intent = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        while (parcel.dataPosition() < r9) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str7 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str6 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 9:
                    intent = (Intent) zzbfn.m10171(parcel, readInt, Intent.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r9);
        return new zzc(str7, str6, str5, str4, str3, str2, str, intent);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzc[i];
    }
}
