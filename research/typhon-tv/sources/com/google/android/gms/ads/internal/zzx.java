package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzafb;
import com.google.android.gms.internal.zzafe;
import com.google.android.gms.internal.zzafo;
import com.google.android.gms.internal.zzafp;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzanq;
import com.google.android.gms.internal.zzanv;
import com.google.android.gms.internal.zzapa;
import com.google.android.gms.internal.zzgp;
import com.google.android.gms.internal.zzgt;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzll;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzvj;
import com.google.android.gms.internal.zzvm;
import com.google.android.gms.internal.zzzv;
import java.lang.ref.WeakReference;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzx extends zzi implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f3603;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private WeakReference<Object> f3604 = new WeakReference<>((Object) null);

    public zzx(Context context, zzjn zzjn, String str, zzux zzux, zzakd zzakd, zzv zzv) {
        super(context, zzjn, str, zzux, zzakd, zzv);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m4097(zzafo zzafo, zzafo zzafo2) {
        if (zzafo2.f4094) {
            View zze = zzaq.zze(zzafo2);
            if (zze == null) {
                zzagf.m4791("Could not get mediation view");
                return false;
            }
            View nextView = this.连任.f3591.getNextView();
            if (nextView != null) {
                if (nextView instanceof zzanh) {
                    ((zzanh) nextView).destroy();
                }
                this.连任.f3591.removeView(nextView);
            }
            if (!zzaq.zzf(zzafo2)) {
                try {
                    if (zzbs.zzfd().m4428(this.连任.zzair)) {
                        new zzgp(this.连任.zzair, zze).m5357((zzgt) new zzafe(this.连任.zzair, this.连任.zzatw));
                    }
                    if (zzafo2.f4098 != null) {
                        this.连任.f3591.setMinimumWidth(zzafo2.f4098.f4789);
                        this.连任.f3591.setMinimumHeight(zzafo2.f4098.f4797);
                    }
                    龘(zze);
                } catch (Exception e) {
                    zzbs.zzem().m4505((Throwable) e, "BannerAdManager.swapViews");
                    zzagf.m4796("Could not add mediation view to view hierarchy.", e);
                    return false;
                }
            }
        } else if (!(zzafo2.f4098 == null || zzafo2.f4119 == null)) {
            zzafo2.f4119.m5011(zzapa.m5224(zzafo2.f4098));
            this.连任.f3591.removeAllViews();
            this.连任.f3591.setMinimumWidth(zzafo2.f4098.f4789);
            this.连任.f3591.setMinimumHeight(zzafo2.f4098.f4797);
            zzanh zzanh = zzafo2.f4119;
            if (zzanh == null) {
                throw null;
            }
            龘((View) zzanh);
        }
        if (this.连任.f3591.getChildCount() > 1) {
            this.连任.f3591.showNext();
        }
        if (zzafo != null) {
            View nextView2 = this.连任.f3591.getNextView();
            if (nextView2 instanceof zzanh) {
                ((zzanh) nextView2).destroy();
            } else if (nextView2 != null) {
                this.连任.f3591.removeView(nextView2);
            }
            this.连任.zzfj();
        }
        this.连任.f3591.setVisibility(0);
        return true;
    }

    public final zzll getVideoController() {
        zzbq.m9115("getVideoController must be called from the main thread.");
        if (this.连任.zzaud == null || this.连任.zzaud.f4119 == null) {
            return null;
        }
        return this.连任.zzaud.f4119.m4997();
    }

    public final void onGlobalLayout() {
        m4099(this.连任.zzaud);
    }

    public final void onScrollChanged() {
        m4099(this.连任.zzaud);
    }

    public final void setManualImpressionsEnabled(boolean z) {
        zzbq.m9115("setManualImpressionsEnabled must be called from the main thread.");
        this.f3603 = z;
    }

    public final void showInterstitial() {
        throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bc, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.zzkb.m5481().m5595(com.google.android.gms.internal.zznh.f4984)).booleanValue() != false) goto L_0x00be;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(com.google.android.gms.internal.zzafo r6, com.google.android.gms.internal.zzafo r7) {
        /*
            r5 = this;
            r1 = 0
            r2 = 0
            boolean r0 = super.zza((com.google.android.gms.internal.zzafo) r6, (com.google.android.gms.internal.zzafo) r7)
            if (r0 != 0) goto L_0x000a
            r0 = r2
        L_0x0009:
            return r0
        L_0x000a:
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            boolean r0 = r0.zzfk()
            if (r0 == 0) goto L_0x0028
            boolean r0 = r5.m4097((com.google.android.gms.internal.zzafo) r6, (com.google.android.gms.internal.zzafo) r7)
            if (r0 != 0) goto L_0x0028
            com.google.android.gms.internal.zzis r0 = r7.f4089
            if (r0 == 0) goto L_0x0023
            com.google.android.gms.internal.zzis r0 = r7.f4089
            com.google.android.gms.internal.zziu$zza$zzb r1 = com.google.android.gms.internal.zziu.zza.zzb.AD_FAILED_TO_LOAD
            r0.m5434((com.google.android.gms.internal.zziu.zza.zzb) r1)
        L_0x0023:
            r5.龘(r2)
            r0 = r2
            goto L_0x0009
        L_0x0028:
            boolean r0 = r7.f4110
            if (r0 == 0) goto L_0x00a4
            r5.m4099(r7)
            com.google.android.gms.ads.internal.zzbs.zzfc()
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            com.google.android.gms.ads.internal.zzbu r0 = r0.f3591
            com.google.android.gms.internal.zzaln.m4826((android.view.View) r0, (android.view.ViewTreeObserver.OnGlobalLayoutListener) r5)
            com.google.android.gms.ads.internal.zzbs.zzfc()
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            com.google.android.gms.ads.internal.zzbu r0 = r0.f3591
            com.google.android.gms.internal.zzaln.m4827((android.view.View) r0, (android.view.ViewTreeObserver.OnScrollChangedListener) r5)
            boolean r0 = r7.f4099
            if (r0 != 0) goto L_0x0060
            com.google.android.gms.ads.internal.zzy r2 = new com.google.android.gms.ads.internal.zzy
            r2.<init>(r5)
            com.google.android.gms.internal.zzanh r0 = r7.f4119
            if (r0 == 0) goto L_0x00a2
            com.google.android.gms.internal.zzanh r0 = r7.f4119
            com.google.android.gms.internal.zzani r0 = r0.m4980()
        L_0x0056:
            if (r0 == 0) goto L_0x0060
            com.google.android.gms.ads.internal.zzz r3 = new com.google.android.gms.ads.internal.zzz
            r3.<init>(r5, r7, r2)
            r0.m5051((com.google.android.gms.internal.zzanq) r3)
        L_0x0060:
            com.google.android.gms.internal.zzanh r0 = r7.f4119
            if (r0 == 0) goto L_0x0084
            com.google.android.gms.internal.zzanh r0 = r7.f4119
            com.google.android.gms.internal.zzaoa r0 = r0.m4997()
            com.google.android.gms.internal.zzanh r2 = r7.f4119
            com.google.android.gms.internal.zzani r2 = r2.m4980()
            if (r2 == 0) goto L_0x0075
            r2.m5031()
        L_0x0075:
            com.google.android.gms.ads.internal.zzbt r2 = r5.连任
            com.google.android.gms.internal.zzmr r2 = r2.f3570
            if (r2 == 0) goto L_0x0084
            if (r0 == 0) goto L_0x0084
            com.google.android.gms.ads.internal.zzbt r2 = r5.连任
            com.google.android.gms.internal.zzmr r2 = r2.f3570
            r0.m5212((com.google.android.gms.internal.zzmr) r2)
        L_0x0084:
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            boolean r0 = r0.zzfk()
            if (r0 == 0) goto L_0x0123
            com.google.android.gms.internal.zzanh r0 = r7.f4119
            if (r0 == 0) goto L_0x013f
            org.json.JSONObject r0 = r7.f4109
            if (r0 == 0) goto L_0x009d
            com.google.android.gms.internal.zzfs r0 = r5.ʼ
            com.google.android.gms.ads.internal.zzbt r2 = r5.连任
            com.google.android.gms.internal.zzjn r2 = r2.zzauc
            r0.m5309(r2, r7)
        L_0x009d:
            com.google.android.gms.internal.zzanh r0 = r7.f4119
            if (r0 != 0) goto L_0x00c2
            throw r1
        L_0x00a2:
            r0 = r1
            goto L_0x0056
        L_0x00a4:
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            boolean r0 = r0.zzfl()
            if (r0 == 0) goto L_0x00be
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r0 = com.google.android.gms.internal.zznh.f4984
            com.google.android.gms.internal.zznf r3 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r0 = r3.m5595(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0060
        L_0x00be:
            r5.龘(r7, r2)
            goto L_0x0060
        L_0x00c2:
            android.view.View r0 = (android.view.View) r0
            com.google.android.gms.internal.zzgp r1 = new com.google.android.gms.internal.zzgp
            com.google.android.gms.ads.internal.zzbt r2 = r5.连任
            android.content.Context r2 = r2.zzair
            r1.<init>(r2, r0)
            com.google.android.gms.internal.zzaff r2 = com.google.android.gms.ads.internal.zzbs.zzfd()
            com.google.android.gms.ads.internal.zzbt r3 = r5.连任
            android.content.Context r3 = r3.zzair
            boolean r2 = r2.m4428(r3)
            if (r2 == 0) goto L_0x00fd
            com.google.android.gms.internal.zzjj r2 = r7.f4122
            boolean r2 = 龘(r2)
            if (r2 == 0) goto L_0x00fd
            com.google.android.gms.ads.internal.zzbt r2 = r5.连任
            java.lang.String r2 = r2.zzatw
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x00fd
            com.google.android.gms.internal.zzafe r2 = new com.google.android.gms.internal.zzafe
            com.google.android.gms.ads.internal.zzbt r3 = r5.连任
            android.content.Context r3 = r3.zzair
            com.google.android.gms.ads.internal.zzbt r4 = r5.连任
            java.lang.String r4 = r4.zzatw
            r2.<init>(r3, r4)
            r1.m5357((com.google.android.gms.internal.zzgt) r2)
        L_0x00fd:
            boolean r2 = r7.m4442()
            if (r2 == 0) goto L_0x0114
            com.google.android.gms.internal.zzanh r2 = r7.f4119
            r1.m5357((com.google.android.gms.internal.zzgt) r2)
        L_0x0108:
            boolean r1 = r7.f4094
            if (r1 != 0) goto L_0x0111
            com.google.android.gms.ads.internal.zzbt r1 = r5.连任
            r1.m4083((android.view.View) r0)
        L_0x0111:
            r0 = 1
            goto L_0x0009
        L_0x0114:
            com.google.android.gms.internal.zzanh r2 = r7.f4119
            com.google.android.gms.internal.zzani r2 = r2.m4980()
            com.google.android.gms.ads.internal.zzaa r3 = new com.google.android.gms.ads.internal.zzaa
            r3.<init>(r5, r1, r7)
            r2.m5050((com.google.android.gms.internal.zzano) r3)
            goto L_0x0108
        L_0x0123:
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            android.view.View r0 = r0.f3572
            if (r0 == 0) goto L_0x013f
            org.json.JSONObject r0 = r7.f4109
            if (r0 == 0) goto L_0x013f
            com.google.android.gms.internal.zzfs r0 = r5.ʼ
            com.google.android.gms.ads.internal.zzbt r1 = r5.连任
            com.google.android.gms.internal.zzjn r1 = r1.zzauc
            com.google.android.gms.ads.internal.zzbt r2 = r5.连任
            android.view.View r2 = r2.f3572
            r0.m5310(r1, r7, r2)
            com.google.android.gms.ads.internal.zzbt r0 = r5.连任
            android.view.View r0 = r0.f3572
            goto L_0x0108
        L_0x013f:
            r0 = r1
            goto L_0x0108
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzx.zza(com.google.android.gms.internal.zzafo, com.google.android.gms.internal.zzafo):boolean");
    }

    public final boolean zzb(zzjj zzjj) {
        if (zzjj.f4755 != this.f3603) {
            zzjj = new zzjj(zzjj.f4768, zzjj.f4765, zzjj.f4767, zzjj.f4766, zzjj.f4764, zzjj.f4753, zzjj.f4754, zzjj.f4755 || this.f3603, zzjj.f4761, zzjj.f4762, zzjj.f4763, zzjj.f4758, zzjj.f4756, zzjj.f4757, zzjj.f4769, zzjj.f4770, zzjj.f4759, zzjj.f4760);
        }
        return super.zzb(zzjj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m4098() {
        boolean z = true;
        zzbs.zzei();
        if (!zzahn.m4618(this.连任.zzair, this.连任.zzair.getPackageName(), "android.permission.INTERNET")) {
            zzkb.m5487().m4769(this.连任.f3591, this.连任.zzauc, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
            z = false;
        }
        zzbs.zzei();
        if (!zzahn.m4617(this.连任.zzair)) {
            zzkb.m5487().m4769(this.连任.f3591, this.连任.zzauc, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
            z = false;
        }
        if (!z && this.连任.f3591 != null) {
            this.连任.f3591.setVisibility(0);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4099(zzafo zzafo) {
        if (zzafo != null && !zzafo.f4099 && this.连任.f3591 != null && zzbs.zzei().m4638((View) this.连任.f3591, this.连任.zzair) && this.连任.f3591.getGlobalVisibleRect(new Rect(), (Point) null)) {
            if (!(zzafo == null || zzafo.f4119 == null || zzafo.f4119.m4980() == null)) {
                zzafo.f4119.m4980().m5051((zzanq) null);
            }
            龘(zzafo, false);
            zzafo.f4099 = true;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzanh m4100(zzafp zzafp, zzw zzw, zzafb zzafb) throws zzanv {
        AdSize r0;
        zzjn zzjn;
        if (this.连任.zzauc.f4790 == null && this.连任.zzauc.f4792) {
            zzbt zzbt = this.连任;
            if (zzafp.f4133.f3851) {
                zzjn = this.连任.zzauc;
            } else {
                String str = zzafp.f4133.f3832;
                if (str != null) {
                    String[] split = str.split("[xX]");
                    split[0] = split[0].trim();
                    split[1] = split[1].trim();
                    r0 = new AdSize(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
                } else {
                    r0 = this.连任.zzauc.m5458();
                }
                zzjn = new zzjn(this.连任.zzair, r0);
            }
            zzbt.zzauc = zzjn;
        }
        return super.m4086(zzafp, zzw, zzafb);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4101(zzafo zzafo, boolean z) {
        View view;
        super.龘(zzafo, z);
        if (zzaq.zzf(zzafo)) {
            zzab zzab = new zzab(this);
            if (zzafo != null && zzaq.zzf(zzafo)) {
                zzanh zzanh = zzafo.f4119;
                if (zzanh == null) {
                    view = null;
                } else if (zzanh == null) {
                    throw null;
                } else {
                    view = (View) zzanh;
                }
                if (view == null) {
                    zzagf.m4791("AdWebView is null");
                    return;
                }
                try {
                    List<String> list = zzafo.f4096 != null ? zzafo.f4096.f5420 : null;
                    if (list == null || list.isEmpty()) {
                        zzagf.m4791("No template ids present in mediation response");
                        return;
                    }
                    zzvj r3 = zzafo.f4124 != null ? zzafo.f4124.m13457() : null;
                    zzvm r0 = zzafo.f4124 != null ? zzafo.f4124.m13461() : null;
                    if (list.contains("2") && r3 != null) {
                        r3.m13543(zzn.m9306(view));
                        if (!r3.m13539()) {
                            r3.m13538();
                        }
                        zzanh.m4980().m5053("/nativeExpressViewClicked", (zzt<? super zzanh>) zzaq.m4057(r3, (zzvm) null, zzab));
                    } else if (!list.contains(PubnativeRequest.LEGACY_ZONE_ID) || r0 == null) {
                        zzagf.m4791("No matching template id and mapper");
                    } else {
                        r0.m13582(zzn.m9306(view));
                        if (!r0.m13573()) {
                            r0.m13572();
                        }
                        zzanh.m4980().m5053("/nativeExpressViewClicked", (zzt<? super zzanh>) zzaq.m4057((zzvj) null, r0, zzab));
                    }
                } catch (RemoteException e) {
                    zzagf.m4796("Error occurred while recording impression and registering for clicks", e);
                }
            }
        }
    }
}
