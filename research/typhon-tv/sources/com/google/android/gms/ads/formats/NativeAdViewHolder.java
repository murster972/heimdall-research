package com.google.android.gms.ads.formats;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzpz;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class NativeAdViewHolder {
    public static WeakHashMap<View, NativeAdViewHolder> zzamp = new WeakHashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private WeakReference<View> f6645;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzpz f6646;

    public NativeAdViewHolder(View view, Map<String, View> map, Map<String, View> map2) {
        zzbq.m9121(view, (Object) "ContainerView must not be null");
        if (view instanceof NativeAdView) {
            zzakb.m4795("The provided containerView is of type NativeAdView. NativeAdView objects should not be used with NativeAdViewHolder.");
        } else if (zzamp.get(view) != null) {
            zzakb.m4795("The provided containerView is already in use with another NativeAdViewHolder.");
        } else {
            zzamp.put(view, this);
            this.f6645 = new WeakReference<>(view);
            this.f6646 = zzkb.m5484().m5479(view, m7624(map), m7624(map2));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static HashMap<String, View> m7624(Map<String, View> map) {
        return map == null ? new HashMap<>() : new HashMap<>(map);
    }

    public final void setNativeAd(NativeAd nativeAd) {
        View view = this.f6645 != null ? (View) this.f6645.get() : null;
        if (view == null) {
            zzakb.m4791("NativeAdViewHolder.setNativeAd containerView doesn't exist, returning");
            return;
        }
        if (!zzamp.containsKey(view)) {
            zzamp.put(view, this);
        }
        if (this.f6646 != null) {
            try {
                this.f6646.m13244((IObjectWrapper) nativeAd.m7619());
            } catch (RemoteException e) {
                zzakb.m4793("Unable to call setNativeAd on delegate", e);
            }
        }
    }

    public final void unregisterNativeAd() {
        if (this.f6646 != null) {
            try {
                this.f6646.m13243();
            } catch (RemoteException e) {
                zzakb.m4793("Unable to call unregisterNativeAd on delegate", e);
            }
        }
        View view = this.f6645 != null ? (View) this.f6645.get() : null;
        if (view != null) {
            zzamp.remove(view);
        }
    }
}
