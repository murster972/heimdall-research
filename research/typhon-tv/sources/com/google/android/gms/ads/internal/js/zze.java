package com.google.android.gms.ads.internal.js;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.util.zzr;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzajr;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzanm;
import com.google.android.gms.internal.zzanv;
import com.google.android.gms.internal.zzapa;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zzzv;
import java.util.Map;
import org.json.JSONObject;

@zzzv
public final class zze implements zzc {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f3428;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzanh f3429;

    public zze(Context context, zzakd zzakd, zzcv zzcv, zzv zzv) throws zzanv {
        this.f3428 = context;
        this.f3429 = zzbs.zzej().m5060(context, zzapa.m5222(), "", false, false, zzcv, zzakd, (zznu) null, (zzbl) null, (zzv) null, zzis.m5432());
        zzanh zzanh = this.f3429;
        if (zzanh == null) {
            throw null;
        }
        ((View) zzanh).setWillNotDraw(true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m4016(Runnable runnable) {
        zzkb.m5487();
        if (zzajr.m4752()) {
            runnable.run();
        } else {
            zzahn.f4212.post(runnable);
        }
    }

    public final void destroy() {
        this.f3429.destroy();
    }

    public final void zza(zzd zzd) {
        this.f3429.m4980().m5048((zzanm) new zzk(this, zzd));
    }

    public final void zza(String str, zzt<? super zzaj> zzt) {
        this.f3429.m4980().m5053(str, (zzt<? super zzanh>) new zzl(this, zzt));
    }

    public final void zza(String str, Map<String, ?> map) {
        this.f3429.zza(str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        this.f3429.zza(str, jSONObject);
    }

    public final void zzb(String str, zzt<? super zzaj> zzt) {
        this.f3429.m4980().m5054(str, (zzr<zzt<? super zzanh>>) new zzf(zzt));
    }

    public final void zzb(String str, JSONObject jSONObject) {
        m4016((Runnable) new zzg(this, str, jSONObject));
    }

    public final void zzbb(String str) {
        m4016((Runnable) new zzh(this, String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", new Object[]{str})));
    }

    public final void zzbc(String str) {
        m4016((Runnable) new zzj(this, str));
    }

    public final void zzbd(String str) {
        m4016((Runnable) new zzi(this, str));
    }

    public final zzak zzln() {
        return new zzal(this);
    }
}
