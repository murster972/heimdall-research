package com.google.android.gms.ads.internal.gmsg;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzanh;
import java.util.HashMap;
import java.util.Map;

final class zzk implements zzt<zzanh> {
    zzk() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        zzbs.zzei();
        DisplayMetrics r0 = zzahn.m4594((WindowManager) zzanh.getContext().getSystemService("window"));
        int i = r0.widthPixels;
        int i2 = r0.heightPixels;
        int[] iArr = new int[2];
        HashMap hashMap = new HashMap();
        ((View) zzanh).getLocationInWindow(iArr);
        hashMap.put("xInPixels", Integer.valueOf(iArr[0]));
        hashMap.put("yInPixels", Integer.valueOf(iArr[1]));
        hashMap.put("windowWidthInPixels", Integer.valueOf(i));
        hashMap.put("windowHeightInPixels", Integer.valueOf(i2));
        zzanh.zza("locationReady", (Map<String, ?>) hashMap);
        zzagf.m4791("GET LOCATION COMPILED");
    }
}
