package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzanh;
import java.util.Map;

final class zzq implements zzt<zzanh> {
    zzq() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        String str = (String) map.get("action");
        if ("pause".equals(str)) {
            zzanh.zzcq();
        } else if ("resume".equals(str)) {
            zzanh.zzcr();
        }
    }
}
