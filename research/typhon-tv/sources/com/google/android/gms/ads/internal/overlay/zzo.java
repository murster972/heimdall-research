package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import com.Pinkamena;
import com.google.android.gms.internal.zzajr;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzo extends FrameLayout implements View.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzt f3464;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ImageButton f3465;

    public zzo(Context context, zzp zzp, zzt zzt) {
        super(context);
        this.f3464 = zzt;
        setOnClickListener(this);
        this.f3465 = new ImageButton(context);
        this.f3465.setImageResource(17301527);
        this.f3465.setBackgroundColor(0);
        this.f3465.setOnClickListener(this);
        ImageButton imageButton = this.f3465;
        zzkb.m5487();
        int r1 = zzajr.m4757(context, zzp.f6717);
        zzkb.m5487();
        int r2 = zzajr.m4757(context, 0);
        zzkb.m5487();
        int r3 = zzajr.m4757(context, zzp.f6714);
        zzkb.m5487();
        imageButton.setPadding(r1, r2, r3, zzajr.m4757(context, zzp.f6715));
        this.f3465.setContentDescription("Interstitial close button");
        zzkb.m5487();
        zzajr.m4757(context, zzp.f6713);
        ImageButton imageButton2 = this.f3465;
        zzkb.m5487();
        int r22 = zzajr.m4757(context, zzp.f6713 + zzp.f6717 + zzp.f6714);
        zzkb.m5487();
        new FrameLayout.LayoutParams(r22, zzajr.m4757(context, zzp.f6713 + zzp.f6715), 17);
        Pinkamena.DianePie();
    }

    public final void onClick(View view) {
        if (this.f3464 != null) {
            this.f3464.zzmt();
        }
    }

    public final void zza(boolean z, boolean z2) {
        if (!z2) {
            this.f3465.setVisibility(0);
        } else if (z) {
            this.f3465.setVisibility(4);
        } else {
            this.f3465.setVisibility(8);
        }
    }
}
