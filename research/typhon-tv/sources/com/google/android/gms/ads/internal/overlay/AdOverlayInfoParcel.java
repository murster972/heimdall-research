package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.gmsg.zzb;
import com.google.android.gms.ads.internal.zzao;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.google.android.gms.internal.zzje;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class AdOverlayInfoParcel extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<AdOverlayInfoParcel> CREATOR = new zzm();
    public final int orientation;
    public final String url;
    public final zzakd zzaty;
    public final zzc zzciv;
    public final zzje zzciw;
    public final zzn zzcix;
    public final zzanh zzciy;
    public final zzb zzciz;
    public final String zzcja;
    public final boolean zzcjb;
    public final String zzcjc;
    public final zzq zzcjd;
    public final int zzcje;
    public final String zzcjf;
    public final zzao zzcjg;

    AdOverlayInfoParcel(zzc zzc, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, IBinder iBinder4, String str, boolean z, String str2, IBinder iBinder5, int i, int i2, String str3, zzakd zzakd, String str4, zzao zzao) {
        this.zzciv = zzc;
        this.zzciw = (zzje) zzn.m9307(IObjectWrapper.zza.m9305(iBinder));
        this.zzcix = (zzn) zzn.m9307(IObjectWrapper.zza.m9305(iBinder2));
        this.zzciy = (zzanh) zzn.m9307(IObjectWrapper.zza.m9305(iBinder3));
        this.zzciz = (zzb) zzn.m9307(IObjectWrapper.zza.m9305(iBinder4));
        this.zzcja = str;
        this.zzcjb = z;
        this.zzcjc = str2;
        this.zzcjd = (zzq) zzn.m9307(IObjectWrapper.zza.m9305(iBinder5));
        this.orientation = i;
        this.zzcje = i2;
        this.url = str3;
        this.zzaty = zzakd;
        this.zzcjf = str4;
        this.zzcjg = zzao;
    }

    public AdOverlayInfoParcel(zzc zzc, zzje zzje, zzn zzn, zzq zzq, zzakd zzakd) {
        this.zzciv = zzc;
        this.zzciw = zzje;
        this.zzcix = zzn;
        this.zzciy = null;
        this.zzciz = null;
        this.zzcja = null;
        this.zzcjb = false;
        this.zzcjc = null;
        this.zzcjd = zzq;
        this.orientation = -1;
        this.zzcje = 4;
        this.url = null;
        this.zzaty = zzakd;
        this.zzcjf = null;
        this.zzcjg = null;
    }

    public AdOverlayInfoParcel(zzje zzje, zzn zzn, zzb zzb, zzq zzq, zzanh zzanh, boolean z, int i, String str, zzakd zzakd) {
        this.zzciv = null;
        this.zzciw = zzje;
        this.zzcix = zzn;
        this.zzciy = zzanh;
        this.zzciz = zzb;
        this.zzcja = null;
        this.zzcjb = z;
        this.zzcjc = null;
        this.zzcjd = zzq;
        this.orientation = i;
        this.zzcje = 3;
        this.url = str;
        this.zzaty = zzakd;
        this.zzcjf = null;
        this.zzcjg = null;
    }

    public AdOverlayInfoParcel(zzje zzje, zzn zzn, zzb zzb, zzq zzq, zzanh zzanh, boolean z, int i, String str, String str2, zzakd zzakd) {
        this.zzciv = null;
        this.zzciw = zzje;
        this.zzcix = zzn;
        this.zzciy = zzanh;
        this.zzciz = zzb;
        this.zzcja = str2;
        this.zzcjb = z;
        this.zzcjc = str;
        this.zzcjd = zzq;
        this.orientation = i;
        this.zzcje = 3;
        this.url = null;
        this.zzaty = zzakd;
        this.zzcjf = null;
        this.zzcjg = null;
    }

    public AdOverlayInfoParcel(zzje zzje, zzn zzn, zzq zzq, zzanh zzanh, int i, zzakd zzakd, String str, zzao zzao) {
        this.zzciv = null;
        this.zzciw = zzje;
        this.zzcix = zzn;
        this.zzciy = zzanh;
        this.zzciz = null;
        this.zzcja = null;
        this.zzcjb = false;
        this.zzcjc = null;
        this.zzcjd = zzq;
        this.orientation = i;
        this.zzcje = 1;
        this.url = null;
        this.zzaty = zzakd;
        this.zzcjf = str;
        this.zzcjg = zzao;
    }

    public AdOverlayInfoParcel(zzje zzje, zzn zzn, zzq zzq, zzanh zzanh, boolean z, int i, zzakd zzakd) {
        this.zzciv = null;
        this.zzciw = zzje;
        this.zzcix = zzn;
        this.zzciy = zzanh;
        this.zzciz = null;
        this.zzcja = null;
        this.zzcjb = z;
        this.zzcjc = null;
        this.zzcjd = zzq;
        this.orientation = i;
        this.zzcje = 2;
        this.url = null;
        this.zzaty = zzakd;
        this.zzcjf = null;
        this.zzcjg = null;
    }

    public static void zza(Intent intent, AdOverlayInfoParcel adOverlayInfoParcel) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", adOverlayInfoParcel);
        intent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", bundle);
    }

    public static AdOverlayInfoParcel zzc(Intent intent) {
        try {
            Bundle bundleExtra = intent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
            bundleExtra.setClassLoader(AdOverlayInfoParcel.class.getClassLoader());
            return (AdOverlayInfoParcel) bundleExtra.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
        } catch (Exception e) {
            return null;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 2, (Parcelable) this.zzciv, i, false);
        zzbfp.m10188(parcel, 3, zzn.m9306(this.zzciw).asBinder(), false);
        zzbfp.m10188(parcel, 4, zzn.m9306(this.zzcix).asBinder(), false);
        zzbfp.m10188(parcel, 5, zzn.m9306(this.zzciy).asBinder(), false);
        zzbfp.m10188(parcel, 6, zzn.m9306(this.zzciz).asBinder(), false);
        zzbfp.m10193(parcel, 7, this.zzcja, false);
        zzbfp.m10195(parcel, 8, this.zzcjb);
        zzbfp.m10193(parcel, 9, this.zzcjc, false);
        zzbfp.m10188(parcel, 10, zzn.m9306(this.zzcjd).asBinder(), false);
        zzbfp.m10185(parcel, 11, this.orientation);
        zzbfp.m10185(parcel, 12, this.zzcje);
        zzbfp.m10193(parcel, 13, this.url, false);
        zzbfp.m10189(parcel, 14, (Parcelable) this.zzaty, i, false);
        zzbfp.m10193(parcel, 16, this.zzcjf, false);
        zzbfp.m10189(parcel, 17, (Parcelable) this.zzcjg, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
