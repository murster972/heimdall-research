package com.google.android.gms.ads.internal.js;

import com.google.android.gms.ads.internal.gmsg.zzt;

final class zzs implements zzt<zzaj> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzo f6696;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzc f6697;

    zzs(zzo zzo, zzc zzc) {
        this.f6696 = zzo;
        this.f6697 = zzc;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(java.lang.Object r4, java.util.Map r5) {
        /*
            r3 = this;
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696
            com.google.android.gms.ads.internal.js.zzn r0 = r0.f6690
            java.lang.Object r1 = r0.f3437
            monitor-enter(r1)
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x0052 }
            int r0 = r0.getStatus()     // Catch:{ all -> 0x0052 }
            r2 = -1
            if (r0 == r2) goto L_0x001f
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x0052 }
            int r0 = r0.getStatus()     // Catch:{ all -> 0x0052 }
            r2 = 1
            if (r0 != r2) goto L_0x0021
        L_0x001f:
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
        L_0x0020:
            return
        L_0x0021:
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzn r0 = r0.f6690     // Catch:{ all -> 0x0052 }
            r2 = 0
            int unused = r0.f3432 = r2     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzn r0 = r0.f6690     // Catch:{ all -> 0x0052 }
            com.google.android.gms.internal.zzaiq r0 = r0.f3433     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzc r2 = r3.f6697     // Catch:{ all -> 0x0052 }
            r0.zzf(r2)     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzc r2 = r3.f6697     // Catch:{ all -> 0x0052 }
            r0.zzk(r2)     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzn r0 = r0.f6690     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzo r2 = r3.f6696     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzae r2 = r2.f6689     // Catch:{ all -> 0x0052 }
            com.google.android.gms.ads.internal.js.zzae unused = r0.f3431 = r2     // Catch:{ all -> 0x0052 }
            java.lang.String r0 = "Successfully loaded JS Engine."
            com.google.android.gms.internal.zzagf.m4527(r0)     // Catch:{ all -> 0x0052 }
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            goto L_0x0020
        L_0x0052:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.js.zzs.zza(java.lang.Object, java.util.Map):void");
    }
}
