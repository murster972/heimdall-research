package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;

final class zzbo implements View.OnTouchListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbm f6772;

    zzbo(zzbm zzbm) {
        this.f6772 = zzbm;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f6772.f3526 == null) {
            return false;
        }
        this.f6772.f3526.m11540(motionEvent);
        return false;
    }
}
