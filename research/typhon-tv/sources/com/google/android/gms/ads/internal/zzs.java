package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzoj;

final class zzs implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzq f6800;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzoj f6801;

    zzs(zzq zzq, zzoj zzoj) {
        this.f6800 = zzq;
        this.f6801 = zzoj;
    }

    public final void run() {
        try {
            if (this.f6800.连任.f3569 != null) {
                this.f6800.连任.f3569.m13341(this.f6801);
            }
        } catch (RemoteException e) {
            zzagf.m4796("Could not call OnAppInstallAdLoadedListener.onAppInstallAdLoaded().", e);
        }
    }
}
