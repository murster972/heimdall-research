package com.google.android.gms.ads.internal;

import android.os.Debug;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

final class zzb extends TimerTask {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Timer f6745;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zza f6746;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ CountDownLatch f6747;

    zzb(zza zza, CountDownLatch countDownLatch, Timer timer) {
        this.f6746 = zza;
        this.f6747 = countDownLatch;
        this.f6745 = timer;
    }

    public final void run() {
        if (((long) ((Integer) zzkb.m5481().m5595(zznh.f5002)).intValue()) != this.f6747.getCount()) {
            zzagf.m4792("Stopping method tracing");
            Debug.stopMethodTracing();
            if (this.f6747.getCount() == 0) {
                this.f6745.cancel();
                return;
            }
        }
        String concat = String.valueOf(this.f6746.连任.zzair.getPackageName()).concat("_adsTrace_");
        try {
            zzagf.m4792("Starting method tracing");
            this.f6747.countDown();
            Debug.startMethodTracing(new StringBuilder(String.valueOf(concat).length() + 20).append(concat).append(zzbs.zzeo().m9243()).toString(), ((Integer) zzkb.m5481().m5595(zznh.f5003)).intValue());
        } catch (Exception e) {
            zzagf.m4796("Exception occurred while starting method tracing.", e);
        }
    }
}
