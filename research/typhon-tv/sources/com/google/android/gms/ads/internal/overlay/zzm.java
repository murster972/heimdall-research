package com.google.android.gms.ads.internal.overlay;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.zzao;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzbfn;

public final class zzm implements Parcelable.Creator<AdOverlayInfoParcel> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r18 = zzbfn.m10169(parcel);
        zzc zzc = null;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        IBinder iBinder4 = null;
        String str = null;
        boolean z = false;
        String str2 = null;
        IBinder iBinder5 = null;
        int i = 0;
        int i2 = 0;
        String str3 = null;
        zzakd zzakd = null;
        String str4 = null;
        zzao zzao = null;
        while (parcel.dataPosition() < r18) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    zzc = (zzc) zzbfn.m10171(parcel, readInt, zzc.CREATOR);
                    break;
                case 3:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                case 4:
                    iBinder2 = zzbfn.m10156(parcel, readInt);
                    break;
                case 5:
                    iBinder3 = zzbfn.m10156(parcel, readInt);
                    break;
                case 6:
                    iBinder4 = zzbfn.m10156(parcel, readInt);
                    break;
                case 7:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 10:
                    iBinder5 = zzbfn.m10156(parcel, readInt);
                    break;
                case 11:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 12:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 13:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 14:
                    zzakd = (zzakd) zzbfn.m10171(parcel, readInt, zzakd.CREATOR);
                    break;
                case 16:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 17:
                    zzao = (zzao) zzbfn.m10171(parcel, readInt, zzao.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r18);
        return new AdOverlayInfoParcel(zzc, iBinder, iBinder2, iBinder3, iBinder4, str, z, str2, iBinder5, i, i2, str3, zzakd, str4, zzao);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdOverlayInfoParcel[i];
    }
}
