package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import java.util.Map;

final class zzl implements zzt<zzanh> {
    zzl() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        zzd r0 = zzanh.m4981();
        if (r0 != null) {
            r0.close();
            return;
        }
        zzd r02 = zzanh.m4982();
        if (r02 != null) {
            r02.close();
        } else {
            zzagf.m4791("A GMSG tried to close something that wasn't an overlay.");
        }
    }
}
