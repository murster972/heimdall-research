package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzadp;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahh;
import com.google.android.gms.internal.zzajr;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzcw;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzke;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzkt;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzll;
import com.google.android.gms.internal.zzlr;
import com.google.android.gms.internal.zzmr;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzoa;
import com.google.android.gms.internal.zzxl;
import com.google.android.gms.internal.zzxr;
import com.google.android.gms.internal.zzzv;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@zzzv
public final class zzbm extends zzkt {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public WebView f3524 = new WebView(this.f3530);
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public zzkh f3525;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public zzcv f3526;

    /* renamed from: ˑ  reason: contains not printable characters */
    private AsyncTask<Void, Void, String> f3527;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzbr f3528;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzjn f3529;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final Context f3530;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Future<zzcv> f3531 = zzahh.m4556((ExecutorService) zzahh.f4211, new zzbp(this));
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzakd f3532;

    public zzbm(Context context, zzjn zzjn, String str, zzakd zzakd) {
        this.f3530 = context;
        this.f3532 = zzakd;
        this.f3529 = zzjn;
        this.f3528 = new zzbr(str);
        m4080(0);
        this.f3524.setVerticalScrollBarEnabled(false);
        this.f3524.getSettings().setJavaScriptEnabled(true);
        this.f3524.setWebViewClient(new zzbn(this));
        this.f3524.setOnTouchListener(new zzbo(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m4069(String str) {
        if (this.f3526 == null) {
            return str;
        }
        Uri parse = Uri.parse(str);
        try {
            parse = this.f3526.m11538(parse, this.f3530, (View) null, (Activity) null);
        } catch (zzcw e) {
            zzagf.m4796("Unable to process ad data", e);
        }
        return parse.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4073(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        this.f3530.startActivity(intent);
    }

    public final void destroy() throws RemoteException {
        zzbq.m9115("destroy must be called on the main UI thread.");
        this.f3527.cancel(true);
        this.f3531.cancel(true);
        this.f3524.destroy();
        this.f3524 = null;
    }

    public final String getAdUnitId() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return null;
    }

    public final zzll getVideoController() {
        return null;
    }

    public final boolean isLoading() throws RemoteException {
        return false;
    }

    public final boolean isReady() throws RemoteException {
        return false;
    }

    public final void pause() throws RemoteException {
        zzbq.m9115("pause must be called on the main UI thread.");
    }

    public final void resume() throws RemoteException {
        zzbq.m9115("resume must be called on the main UI thread.");
    }

    public final void setImmersiveMode(boolean z) {
        throw new IllegalStateException("Unused method");
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
    }

    public final void setUserId(String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void showInterstitial() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void stopLoading() throws RemoteException {
    }

    public final void zza(zzadp zzadp) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzjn zzjn) throws RemoteException {
        throw new IllegalStateException("AdSize must be set before initialization");
    }

    public final void zza(zzke zzke) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzkh zzkh) throws RemoteException {
        this.f3525 = zzkh;
    }

    public final void zza(zzkx zzkx) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzld zzld) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzlr zzlr) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzmr zzmr) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzoa zzoa) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzxl zzxl) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzxr zzxr, String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final boolean zzb(zzjj zzjj) throws RemoteException {
        zzbq.m9121(this.f3524, (Object) "This Search Ad has already been torn down");
        this.f3528.m7657(zzjj, this.f3532);
        this.f3527 = new zzbq(this, (zzbn) null).execute(new Void[0]);
        return true;
    }

    public final IObjectWrapper zzbr() throws RemoteException {
        zzbq.m9115("getAdFrame must be called on the main UI thread.");
        return zzn.m9306(this.f3524);
    }

    public final zzjn zzbs() throws RemoteException {
        return this.f3529;
    }

    public final void zzbu() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final zzkx zzcd() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }

    public final zzkh zzce() {
        throw new IllegalStateException("getIAdListener not implemented");
    }

    public final String zzcp() throws RemoteException {
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m4077() {
        String r0 = this.f3528.m7656();
        String str = TextUtils.isEmpty(r0) ? "www.google.com" : r0;
        String str2 = (String) zzkb.m5481().m5595(zznh.f5010);
        return new StringBuilder(String.valueOf("https://").length() + String.valueOf(str).length() + String.valueOf(str2).length()).append("https://").append(str).append(str2).toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m4078(String str) {
        String queryParameter = Uri.parse(str).getQueryParameter(VastIconXmlManager.HEIGHT);
        if (TextUtils.isEmpty(queryParameter)) {
            return 0;
        }
        try {
            zzkb.m5487();
            return zzajr.m4757(this.f3530, Integer.parseInt(queryParameter));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m4079() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https://").appendEncodedPath((String) zzkb.m5481().m5595(zznh.f5010));
        builder.appendQueryParameter("query", this.f3528.m7653());
        builder.appendQueryParameter("pubId", this.f3528.m7655());
        Map<String, String> r3 = this.f3528.m7654();
        for (String next : r3.keySet()) {
            builder.appendQueryParameter(next, r3.get(next));
        }
        Uri build = builder.build();
        if (this.f3526 != null) {
            try {
                build = this.f3526.m11537(build, this.f3530);
            } catch (zzcw e) {
                zzagf.m4796("Unable to process ad data", e);
            }
        }
        String r1 = m4077();
        String encodedQuery = build.getEncodedQuery();
        return new StringBuilder(String.valueOf(r1).length() + 1 + String.valueOf(encodedQuery).length()).append(r1).append("#").append(encodedQuery).toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4080(int i) {
        if (this.f3524 != null) {
            this.f3524.setLayoutParams(new ViewGroup.LayoutParams(-1, i));
        }
    }
}
