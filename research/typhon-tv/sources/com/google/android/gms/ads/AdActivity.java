package com.google.android.gms.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzxe;

public class AdActivity extends Activity {
    public static final String CLASS_NAME = "com.google.android.gms.ads.AdActivity";
    public static final String SIMPLE_CLASS_NAME = "AdActivity";

    /* renamed from: 龘  reason: contains not printable characters */
    private zzxe f6629;

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m7614() {
        if (this.f6629 != null) {
            try {
                this.f6629.zzbf();
            } catch (RemoteException e) {
                zzakb.m4796("Could not forward setContentViewSet to ad overlay:", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        try {
            this.f6629.onActivityResult(i, i2, intent);
        } catch (Exception e) {
            zzakb.m4796("Could not forward onActivityResult to ad overlay:", e);
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onBackPressed() {
        boolean z = true;
        try {
            if (this.f6629 != null) {
                z = this.f6629.zzmu();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onBackPressed to ad overlay:", e);
        }
        if (z) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            this.f6629.zzk(zzn.m9306(configuration));
        } catch (RemoteException e) {
            zzakb.m4796("Failed to wrap configuration.", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6629 = zzkb.m5484().m5480((Activity) this);
        if (this.f6629 == null) {
            zzakb.m4791("Could not create ad overlay.");
            finish();
            return;
        }
        try {
            this.f6629.onCreate(bundle);
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onCreate to ad overlay:", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.f6629 != null) {
                this.f6629.onDestroy();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onDestroy to ad overlay:", e);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            if (this.f6629 != null) {
                this.f6629.onPause();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onPause to ad overlay:", e);
            finish();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        try {
            if (this.f6629 != null) {
                this.f6629.onRestart();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onRestart to ad overlay:", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            if (this.f6629 != null) {
                this.f6629.onResume();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onResume to ad overlay:", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        try {
            if (this.f6629 != null) {
                this.f6629.onSaveInstanceState(bundle);
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onSaveInstanceState to ad overlay:", e);
            finish();
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        try {
            if (this.f6629 != null) {
                this.f6629.onStart();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onStart to ad overlay:", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            if (this.f6629 != null) {
                this.f6629.onStop();
            }
        } catch (RemoteException e) {
            zzakb.m4796("Could not forward onStop to ad overlay:", e);
            finish();
        }
        super.onStop();
    }

    public void setContentView(int i) {
        super.setContentView(i);
        m7614();
    }

    public void setContentView(View view) {
        super.setContentView(view);
        m7614();
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(view, layoutParams);
        m7614();
    }
}
