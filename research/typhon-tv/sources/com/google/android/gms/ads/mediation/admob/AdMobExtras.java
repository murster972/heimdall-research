package com.google.android.gms.ads.mediation.admob;

import android.os.Bundle;
import com.google.ads.mediation.NetworkExtras;

@Deprecated
public final class AdMobExtras implements NetworkExtras {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Bundle f3612;

    public AdMobExtras(Bundle bundle) {
        this.f3612 = bundle != null ? new Bundle(bundle) : null;
    }

    public final Bundle getExtras() {
        return this.f3612;
    }
}
