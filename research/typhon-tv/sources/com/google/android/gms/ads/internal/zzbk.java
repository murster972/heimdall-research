package com.google.android.gms.ads.internal;

import android.os.Handler;

public final class zzbk {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Handler f6770;

    public zzbk(Handler handler) {
        this.f6770 = handler;
    }

    public final boolean postDelayed(Runnable runnable, long j) {
        return this.f6770.postDelayed(runnable, j);
    }

    public final void removeCallbacks(Runnable runnable) {
        this.f6770.removeCallbacks(runnable);
    }
}
