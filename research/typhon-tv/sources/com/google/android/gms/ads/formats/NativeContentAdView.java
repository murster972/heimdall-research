package com.google.android.gms.ads.formats;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.gms.internal.zzakb;

public final class NativeContentAdView extends NativeAdView {
    public NativeContentAdView(Context context) {
        super(context);
    }

    public NativeContentAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeContentAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public NativeContentAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    public final View getAdvertiserView() {
        return super.m7622(NativeContentAd.ASSET_ADVERTISER);
    }

    public final View getBodyView() {
        return super.m7622(NativeContentAd.ASSET_BODY);
    }

    public final View getCallToActionView() {
        return super.m7622(NativeContentAd.ASSET_CALL_TO_ACTION);
    }

    public final View getHeadlineView() {
        return super.m7622(NativeContentAd.ASSET_HEADLINE);
    }

    public final View getImageView() {
        return super.m7622(NativeContentAd.ASSET_IMAGE);
    }

    public final View getLogoView() {
        return super.m7622(NativeContentAd.ASSET_LOGO);
    }

    public final MediaView getMediaView() {
        View r0 = super.m7622(NativeContentAd.ASSET_MEDIA_VIDEO);
        if (r0 instanceof MediaView) {
            return (MediaView) r0;
        }
        if (r0 != null) {
            zzakb.m4792("View is not an instance of MediaView");
        }
        return null;
    }

    public final void setAdvertiserView(View view) {
        super.m7623(NativeContentAd.ASSET_ADVERTISER, view);
    }

    public final void setBodyView(View view) {
        super.m7623(NativeContentAd.ASSET_BODY, view);
    }

    public final void setCallToActionView(View view) {
        super.m7623(NativeContentAd.ASSET_CALL_TO_ACTION, view);
    }

    public final void setHeadlineView(View view) {
        super.m7623(NativeContentAd.ASSET_HEADLINE, view);
    }

    public final void setImageView(View view) {
        super.m7623(NativeContentAd.ASSET_IMAGE, view);
    }

    public final void setLogoView(View view) {
        super.m7623(NativeContentAd.ASSET_LOGO, view);
    }

    public final void setMediaView(MediaView mediaView) {
        super.m7623(NativeContentAd.ASSET_MEDIA_VIDEO, mediaView);
    }
}
