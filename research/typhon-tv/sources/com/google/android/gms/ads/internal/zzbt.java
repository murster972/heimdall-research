package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.v4.util.SimpleArrayMap;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.internal.zzadp;
import com.google.android.gms.internal.zzafo;
import com.google.android.gms.internal.zzafp;
import com.google.android.gms.internal.zzafq;
import com.google.android.gms.internal.zzafz;
import com.google.android.gms.internal.zzagb;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzaif;
import com.google.android.gms.internal.zzaji;
import com.google.android.gms.internal.zzajr;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzani;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzke;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzlr;
import com.google.android.gms.internal.zzmr;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzoa;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzqq;
import com.google.android.gms.internal.zzqt;
import com.google.android.gms.internal.zzqw;
import com.google.android.gms.internal.zzqz;
import com.google.android.gms.internal.zzrf;
import com.google.android.gms.internal.zzzv;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@zzzv
public final class zzbt implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {
    public final Context zzair;
    public String zzatw;
    public final zzakd zzaty;
    public zzagb zzaua;
    public zzaif zzaub;
    public zzjn zzauc;
    public zzafo zzaud;
    public zzafp zzaue;
    public zzafq zzauf;
    public String zzauv;
    public zzafz zzaux;
    public int zzauz;

    /* renamed from: ʻ  reason: contains not printable characters */
    zzkx f3567;

    /* renamed from: ʼ  reason: contains not printable characters */
    zzld f3568;

    /* renamed from: ʽ  reason: contains not printable characters */
    zzqq f3569;

    /* renamed from: ʾ  reason: contains not printable characters */
    zzmr f3570;

    /* renamed from: ʿ  reason: contains not printable characters */
    zzlr f3571;

    /* renamed from: ˆ  reason: contains not printable characters */
    View f3572;

    /* renamed from: ˈ  reason: contains not printable characters */
    zzpe f3573;

    /* renamed from: ˉ  reason: contains not printable characters */
    boolean f3574;

    /* renamed from: ˊ  reason: contains not printable characters */
    zzoa f3575;

    /* renamed from: ˋ  reason: contains not printable characters */
    zzadp f3576;

    /* renamed from: ˎ  reason: contains not printable characters */
    List<String> f3577;

    /* renamed from: ˏ  reason: contains not printable characters */
    private HashSet<zzafq> f3578;

    /* renamed from: ˑ  reason: contains not printable characters */
    zzqt f3579;

    /* renamed from: י  reason: contains not printable characters */
    private int f3580;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f3581;

    /* renamed from: ٴ  reason: contains not printable characters */
    SimpleArrayMap<String, zzqw> f3582;

    /* renamed from: ᐧ  reason: contains not printable characters */
    SimpleArrayMap<String, zzqz> f3583;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private zzaji f3584;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f3585;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f3586;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f3587;

    /* renamed from: 连任  reason: contains not printable characters */
    zzkh f3588;

    /* renamed from: 靐  reason: contains not printable characters */
    final zzcv f3589;

    /* renamed from: 麤  reason: contains not printable characters */
    zzke f3590;

    /* renamed from: 齉  reason: contains not printable characters */
    zzbu f3591;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f3592;

    /* renamed from: ﹶ  reason: contains not printable characters */
    zzrf f3593;

    /* renamed from: ﾞ  reason: contains not printable characters */
    List<Integer> f3594;

    public zzbt(Context context, zzjn zzjn, String str, zzakd zzakd) {
        this(context, zzjn, str, zzakd, (zzcv) null);
    }

    private zzbt(Context context, zzjn zzjn, String str, zzakd zzakd, zzcv zzcv) {
        this.zzaux = null;
        this.f3572 = null;
        this.zzauz = 0;
        this.f3574 = false;
        this.f3578 = null;
        this.f3580 = -1;
        this.f3581 = -1;
        this.f3585 = true;
        this.f3586 = true;
        this.f3587 = false;
        zznh.m5599(context);
        if (zzbs.zzem().m4483() != null) {
            List<String> r0 = zznh.m5597();
            if (zzakd.f4294 != 0) {
                r0.add(Integer.toString(zzakd.f4294));
            }
            zzbs.zzem().m4483().m5610(r0);
        }
        this.f3592 = UUID.randomUUID().toString();
        if (zzjn.f4796 || zzjn.f4791) {
            this.f3591 = null;
        } else {
            this.f3591 = new zzbu(context, str, zzakd.f4297, this, this);
            this.f3591.setMinimumWidth(zzjn.f4789);
            this.f3591.setMinimumHeight(zzjn.f4797);
            this.f3591.setVisibility(8);
        }
        this.zzauc = zzjn;
        this.zzatw = str;
        this.zzair = context;
        this.zzaty = zzakd;
        this.f3589 = new zzcv(new zzaf(this));
        this.f3584 = new zzaji(200);
        this.f3583 = new SimpleArrayMap<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4082(boolean z) {
        View findViewById;
        boolean z2 = true;
        if (this.f3591 != null && this.zzaud != null && this.zzaud.f4119 != null && this.zzaud.f4119.m4980() != null) {
            if (!z || this.f3584.m4724()) {
                if (this.zzaud.f4119.m4980().m5040()) {
                    int[] iArr = new int[2];
                    this.f3591.getLocationOnScreen(iArr);
                    zzkb.m5487();
                    int r3 = zzajr.m4749(this.zzair, iArr[0]);
                    zzkb.m5487();
                    int r2 = zzajr.m4749(this.zzair, iArr[1]);
                    if (!(r3 == this.f3580 && r2 == this.f3581)) {
                        this.f3580 = r3;
                        this.f3581 = r2;
                        zzani r22 = this.zzaud.f4119.m4980();
                        int i = this.f3580;
                        int i2 = this.f3581;
                        if (z) {
                            z2 = false;
                        }
                        r22.m5045(i, i2, z2);
                    }
                }
                if (this.f3591 != null && (findViewById = this.f3591.getRootView().findViewById(16908290)) != null) {
                    Rect rect = new Rect();
                    Rect rect2 = new Rect();
                    this.f3591.getGlobalVisibleRect(rect);
                    findViewById.getGlobalVisibleRect(rect2);
                    if (rect.top != rect2.top) {
                        this.f3585 = false;
                    }
                    if (rect.bottom != rect2.bottom) {
                        this.f3586 = false;
                    }
                }
            }
        }
    }

    public final void onGlobalLayout() {
        m4082(false);
    }

    public final void onScrollChanged() {
        m4082(true);
        this.f3587 = true;
    }

    public final void zza(HashSet<zzafq> hashSet) {
        this.f3578 = hashSet;
    }

    public final void zzf(boolean z) {
        if (!(this.zzauz != 0 || this.zzaud == null || this.zzaud.f4119 == null)) {
            this.zzaud.f4119.stopLoading();
        }
        if (this.zzaua != null) {
            this.zzaua.m4524();
        }
        if (this.zzaub != null) {
            this.zzaub.m4675();
        }
        if (z) {
            this.zzaud = null;
        }
    }

    public final HashSet<zzafq> zzfh() {
        return this.f3578;
    }

    public final void zzfi() {
        if (this.zzaud != null && this.zzaud.f4119 != null) {
            this.zzaud.f4119.destroy();
        }
    }

    public final void zzfj() {
        if (this.zzaud != null && this.zzaud.f4124 != null) {
            try {
                this.zzaud.f4124.m13467();
            } catch (RemoteException e) {
                zzagf.m4791("Could not destroy mediation adapter.");
            }
        }
    }

    public final boolean zzfk() {
        return this.zzauz == 0;
    }

    public final boolean zzfl() {
        return this.zzauz == 1;
    }

    public final String zzfm() {
        return (!this.f3585 || !this.f3586) ? this.f3585 ? this.f3587 ? "top-scrollable" : "top-locked" : this.f3586 ? this.f3587 ? "bottom-scrollable" : "bottom-locked" : "" : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4083(View view) {
        zzcr r0;
        if (((Boolean) zzkb.m5481().m5595(zznh.f4966)).booleanValue() && (r0 = this.f3589.m11539()) != null) {
            r0.zzb(view);
        }
    }
}
