package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.Pinkamena;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzaht;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzxb;
import com.google.android.gms.internal.zzxf;
import com.google.android.gms.internal.zzzv;
import java.util.Collections;
import java.util.Map;

@zzzv
public final class zzd extends zzxf implements zzt {

    /* renamed from: 靐  reason: contains not printable characters */
    private static int f3440 = Color.argb(0, 0, 0, 0);

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzo f3441;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f3442 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private FrameLayout f3443;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f3444 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f3445 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f3446 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private zzh f3447;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f3448 = true;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f3449;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f3450;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f3451 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private WebChromeClient.CustomViewCallback f3452;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f3453 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f3454 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private zzi f3455;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzanh f3456;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Activity f3457;

    /* renamed from: 龘  reason: contains not printable characters */
    AdOverlayInfoParcel f3458;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Object f3459 = new Object();

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Runnable f3460;

    public zzd(Activity activity) {
        this.f3457 = activity;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4028() {
        if (this.f3457.isFinishing() && !this.f3451) {
            this.f3451 = true;
            if (this.f3456 != null) {
                this.f3456.m5007(this.f3445);
                synchronized (this.f3459) {
                    if (!this.f3449 && this.f3456.m5015()) {
                        this.f3460 = new zzf(this);
                        zzahn.f4212.postDelayed(this.f3460, ((Long) zzkb.m5481().m5595(zznh.f4921)).longValue());
                        return;
                    }
                }
            }
            m4033();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x02b3  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02ca  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0146 A[SYNTHETIC, Splitter:B:51:0x0146] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0227  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x022c  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x025f  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m4029(boolean r18) throws com.google.android.gms.ads.internal.overlay.zzg {
        /*
            r17 = this;
            r0 = r17
            boolean r1 = r0.f3450
            if (r1 != 0) goto L_0x000e
            r0 = r17
            android.app.Activity r1 = r0.f3457
            r2 = 1
            r1.requestWindowFeature(r2)
        L_0x000e:
            r0 = r17
            android.app.Activity r1 = r0.f3457
            android.view.Window r3 = r1.getWindow()
            if (r3 != 0) goto L_0x0021
            com.google.android.gms.ads.internal.overlay.zzg r1 = new com.google.android.gms.ads.internal.overlay.zzg
            java.lang.String r2 = "Invalid activity, no window available."
            r1.<init>(r2)
            throw r1
        L_0x0021:
            r2 = 1
            boolean r1 = com.google.android.gms.common.util.zzq.m9266()
            if (r1 == 0) goto L_0x0310
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r1 = com.google.android.gms.internal.zznh.f5037
            com.google.android.gms.internal.zznf r4 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r1 = r4.m5595(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0310
            com.google.android.gms.ads.internal.zzbs.zzei()
            r0 = r17
            android.app.Activity r1 = r0.f3457
            r0 = r17
            android.app.Activity r2 = r0.f3457
            android.content.res.Resources r2 = r2.getResources()
            android.content.res.Configuration r2 = r2.getConfiguration()
            boolean r1 = com.google.android.gms.internal.zzahn.m4616((android.app.Activity) r1, (android.content.res.Configuration) r2)
        L_0x0051:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r2 = r0.f3458
            com.google.android.gms.ads.internal.zzao r2 = r2.zzcjg
            if (r2 == 0) goto L_0x0229
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r2 = r0.f3458
            com.google.android.gms.ads.internal.zzao r2 = r2.zzcjg
            boolean r2 = r2.zzaqp
            if (r2 == 0) goto L_0x0229
            r2 = 1
        L_0x0064:
            r0 = r17
            boolean r4 = r0.f3454
            if (r4 == 0) goto L_0x006c
            if (r2 == 0) goto L_0x00a8
        L_0x006c:
            if (r1 == 0) goto L_0x00a8
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1024(0x400, float:1.435E-42)
            r3.setFlags(r1, r2)
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r1 = com.google.android.gms.internal.zznh.f4922
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r1 = r2.m5595(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x00a8
            boolean r1 = com.google.android.gms.common.util.zzq.m9268()
            if (r1 == 0) goto L_0x00a8
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.ads.internal.zzao r1 = r1.zzcjg
            if (r1 == 0) goto L_0x00a8
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.ads.internal.zzao r1 = r1.zzcjg
            boolean r1 = r1.zzaqu
            if (r1 == 0) goto L_0x00a8
            android.view.View r1 = r3.getDecorView()
            r2 = 4098(0x1002, float:5.743E-42)
            r1.setSystemUiVisibility(r2)
        L_0x00a8:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            if (r1 == 0) goto L_0x022c
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            com.google.android.gms.internal.zzani r1 = r1.m4980()
        L_0x00ba:
            if (r1 == 0) goto L_0x022f
            boolean r6 = r1.m5040()
        L_0x00c0:
            r1 = 0
            r0 = r17
            r0.f3444 = r1
            if (r6 == 0) goto L_0x00ed
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            int r1 = r1.orientation
            com.google.android.gms.internal.zzaht r2 = com.google.android.gms.ads.internal.zzbs.zzek()
            int r2 = r2.m4652()
            if (r1 != r2) goto L_0x0235
            r0 = r17
            android.app.Activity r1 = r0.f3457
            android.content.res.Resources r1 = r1.getResources()
            android.content.res.Configuration r1 = r1.getConfiguration()
            int r1 = r1.orientation
            r2 = 1
            if (r1 != r2) goto L_0x0232
            r1 = 1
        L_0x00e9:
            r0 = r17
            r0.f3444 = r1
        L_0x00ed:
            r0 = r17
            boolean r1 = r0.f3444
            r2 = 46
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r2)
            java.lang.String r2 = "Delay onShow to next orientation change: "
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            com.google.android.gms.internal.zzagf.m4792(r1)
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            int r1 = r1.orientation
            r0 = r17
            r0.setRequestedOrientation(r1)
            com.google.android.gms.internal.zzaht r1 = com.google.android.gms.ads.internal.zzbs.zzek()
            boolean r1 = r1.m4664((android.view.Window) r3)
            if (r1 == 0) goto L_0x0125
            java.lang.String r1 = "Hardware acceleration on the AdActivity window enabled."
            com.google.android.gms.internal.zzagf.m4792(r1)
        L_0x0125:
            r0 = r17
            boolean r1 = r0.f3454
            if (r1 != 0) goto L_0x025f
            r0 = r17
            com.google.android.gms.ads.internal.overlay.zzh r1 = r0.f3447
            r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r1.setBackgroundColor(r2)
        L_0x0134:
            r0 = r17
            android.app.Activity r1 = r0.f3457
            r0 = r17
            com.google.android.gms.ads.internal.overlay.zzh r2 = r0.f3447
            r1.setContentView(r2)
            r1 = 1
            r0 = r17
            r0.f3450 = r1
            if (r18 == 0) goto L_0x02b3
            com.google.android.gms.internal.zzanr r1 = com.google.android.gms.ads.internal.zzbs.zzej()     // Catch:{ Exception -> 0x0273 }
            r0 = r17
            android.app.Activity r2 = r0.f3457     // Catch:{ Exception -> 0x0273 }
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r3 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r3 = r3.zzciy     // Catch:{ Exception -> 0x0273 }
            if (r3 == 0) goto L_0x026a
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r3 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r3 = r3.zzciy     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzapa r3 = r3.m4983()     // Catch:{ Exception -> 0x0273 }
        L_0x0160:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r4 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r4 = r4.zzciy     // Catch:{ Exception -> 0x0273 }
            if (r4 == 0) goto L_0x026d
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r4 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r4 = r4.zzciy     // Catch:{ Exception -> 0x0273 }
            java.lang.String r4 = r4.m4978()     // Catch:{ Exception -> 0x0273 }
        L_0x0172:
            r5 = 1
            r7 = 0
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r8 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzakd r8 = r8.zzaty     // Catch:{ Exception -> 0x0273 }
            r9 = 0
            r10 = 0
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r11 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r11 = r11.zzciy     // Catch:{ Exception -> 0x0273 }
            if (r11 == 0) goto L_0x0270
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r11 = r0.f3458     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r11 = r11.zzciy     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.ads.internal.zzv r11 = r11.m4995()     // Catch:{ Exception -> 0x0273 }
        L_0x018e:
            com.google.android.gms.internal.zzis r12 = com.google.android.gms.internal.zzis.m5432()     // Catch:{ Exception -> 0x0273 }
            com.google.android.gms.internal.zzanh r1 = r1.m5060(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x0273 }
            r0 = r17
            r0.f3456 = r1     // Catch:{ Exception -> 0x0273 }
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            com.google.android.gms.internal.zzani r7 = r1.m4980()
            r8 = 0
            r9 = 0
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.ads.internal.gmsg.zzb r10 = r1.zzciz
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.ads.internal.overlay.zzq r11 = r1.zzcjd
            r12 = 1
            r13 = 0
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            if (r1 == 0) goto L_0x0283
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            com.google.android.gms.internal.zzani r1 = r1.m4980()
            com.google.android.gms.ads.internal.zzw r14 = r1.m5043()
        L_0x01c8:
            r15 = 0
            r16 = 0
            r7.m5052(r8, r9, r10, r11, r12, r13, r14, r15, r16)
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            com.google.android.gms.internal.zzani r1 = r1.m4980()
            com.google.android.gms.ads.internal.overlay.zze r2 = new com.google.android.gms.ads.internal.overlay.zze
            r0 = r17
            r2.<init>(r0)
            r1.m5048((com.google.android.gms.internal.zzanm) r2)
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            java.lang.String r1 = r1.url
            if (r1 == 0) goto L_0x0286
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r2 = r0.f3458
            java.lang.String r2 = r2.url
            com.Pinkamena.DianePie()
        L_0x01f5:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            if (r1 == 0) goto L_0x0208
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            r0 = r17
            r1.m4999((com.google.android.gms.ads.internal.overlay.zzd) r0)
        L_0x0208:
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            r0 = r17
            r1.m5009((com.google.android.gms.ads.internal.overlay.zzd) r0)
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            android.view.ViewParent r1 = r1.getParent()
            if (r1 == 0) goto L_0x02cf
            boolean r2 = r1 instanceof android.view.ViewGroup
            if (r2 == 0) goto L_0x02cf
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            r0 = r17
            com.google.android.gms.internal.zzanh r2 = r0.f3456
            if (r2 != 0) goto L_0x02ca
            r1 = 0
            throw r1
        L_0x0229:
            r2 = 0
            goto L_0x0064
        L_0x022c:
            r1 = 0
            goto L_0x00ba
        L_0x022f:
            r6 = 0
            goto L_0x00c0
        L_0x0232:
            r1 = 0
            goto L_0x00e9
        L_0x0235:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            int r1 = r1.orientation
            com.google.android.gms.internal.zzaht r2 = com.google.android.gms.ads.internal.zzbs.zzek()
            int r2 = r2.m4644()
            if (r1 != r2) goto L_0x00ed
            r0 = r17
            android.app.Activity r1 = r0.f3457
            android.content.res.Resources r1 = r1.getResources()
            android.content.res.Configuration r1 = r1.getConfiguration()
            int r1 = r1.orientation
            r2 = 2
            if (r1 != r2) goto L_0x025d
            r1 = 1
        L_0x0257:
            r0 = r17
            r0.f3444 = r1
            goto L_0x00ed
        L_0x025d:
            r1 = 0
            goto L_0x0257
        L_0x025f:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.zzh r1 = r0.f3447
            int r2 = f3440
            r1.setBackgroundColor(r2)
            goto L_0x0134
        L_0x026a:
            r3 = 0
            goto L_0x0160
        L_0x026d:
            r4 = 0
            goto L_0x0172
        L_0x0270:
            r11 = 0
            goto L_0x018e
        L_0x0273:
            r1 = move-exception
            java.lang.String r2 = "Error obtaining webview."
            com.google.android.gms.internal.zzagf.m4793(r2, r1)
            com.google.android.gms.ads.internal.overlay.zzg r1 = new com.google.android.gms.ads.internal.overlay.zzg
            java.lang.String r2 = "Could not obtain webview for the overlay."
            r1.<init>(r2)
            throw r1
        L_0x0283:
            r14 = 0
            goto L_0x01c8
        L_0x0286:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            java.lang.String r1 = r1.zzcjc
            if (r1 == 0) goto L_0x02aa
            r0 = r17
            com.google.android.gms.internal.zzanh r7 = r0.f3456
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            java.lang.String r8 = r1.zzcja
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            java.lang.String r9 = r1.zzcjc
            java.lang.String r10 = "text/html"
            java.lang.String r11 = "UTF-8"
            r12 = 0
            com.Pinkamena.DianePie()
            goto L_0x01f5
        L_0x02aa:
            com.google.android.gms.ads.internal.overlay.zzg r1 = new com.google.android.gms.ads.internal.overlay.zzg
            java.lang.String r2 = "No URL or HTML to display in ad overlay."
            r1.<init>(r2)
            throw r1
        L_0x02b3:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r1 = r0.f3458
            com.google.android.gms.internal.zzanh r1 = r1.zzciy
            r0 = r17
            r0.f3456 = r1
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            r0 = r17
            android.app.Activity r2 = r0.f3457
            r1.m5008((android.content.Context) r2)
            goto L_0x0208
        L_0x02ca:
            android.view.View r2 = (android.view.View) r2
            r1.removeView(r2)
        L_0x02cf:
            r0 = r17
            boolean r1 = r0.f3454
            if (r1 == 0) goto L_0x02dc
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            r1.m4975()
        L_0x02dc:
            r0 = r17
            com.google.android.gms.ads.internal.overlay.zzh r2 = r0.f3447
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            if (r1 != 0) goto L_0x02e8
            r1 = 0
            throw r1
        L_0x02e8:
            android.view.View r1 = (android.view.View) r1
            r3 = -1
            r4 = -1
            com.Pinkamena.DianePie()
            if (r18 != 0) goto L_0x02fa
            r0 = r17
            boolean r1 = r0.f3444
            if (r1 != 0) goto L_0x02fa
            r17.m4030()
        L_0x02fa:
            r0 = r17
            r0.m4032((boolean) r6)
            r0 = r17
            com.google.android.gms.internal.zzanh r1 = r0.f3456
            boolean r1 = r1.m4984()
            if (r1 == 0) goto L_0x030f
            r1 = 1
            r0 = r17
            r0.zza((boolean) r6, (boolean) r1)
        L_0x030f:
            return
        L_0x0310:
            r1 = r2
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.overlay.zzd.m4029(boolean):void");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m4030() {
        this.f3456.m4977();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m4032(boolean z) {
        int intValue = ((Integer) zzkb.m5481().m5595(zznh.f5039)).intValue();
        zzp zzp = new zzp();
        zzp.f6713 = 50;
        zzp.f6717 = z ? intValue : 0;
        zzp.f6714 = z ? 0 : intValue;
        zzp.f6716 = 0;
        zzp.f6715 = intValue;
        this.f3441 = new zzo(this.f3457, zzp, this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(z ? 11 : 9);
        zza(z, this.f3458.zzcjb);
        zzh zzh = this.f3447;
        zzo zzo = this.f3441;
        Pinkamena.DianePie();
    }

    public final void close() {
        this.f3445 = 2;
        this.f3457.finish();
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
    }

    public final void onBackPressed() {
        this.f3445 = 0;
    }

    public final void onCreate(Bundle bundle) {
        boolean z = false;
        this.f3457.requestWindowFeature(1);
        if (bundle != null) {
            z = bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
        }
        this.f3453 = z;
        try {
            this.f3458 = AdOverlayInfoParcel.zzc(this.f3457.getIntent());
            if (this.f3458 == null) {
                throw new zzg("Could not get info for ad overlay.");
            }
            if (this.f3458.zzaty.f4296 > 7500000) {
                this.f3445 = 3;
            }
            if (this.f3457.getIntent() != null) {
                this.f3448 = this.f3457.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
            }
            if (this.f3458.zzcjg != null) {
                this.f3454 = this.f3458.zzcjg.zzaqo;
            } else {
                this.f3454 = false;
            }
            if (((Boolean) zzkb.m5481().m5595(zznh.f4979)).booleanValue() && this.f3454 && this.f3458.zzcjg.zzaqt != -1) {
                new zzj(this, (zze) null).m4521();
            }
            if (bundle == null) {
                if (this.f3458.zzcix != null && this.f3448) {
                    this.f3458.zzcix.zzch();
                }
                if (!(this.f3458.zzcje == 1 || this.f3458.zzciw == null)) {
                    this.f3458.zzciw.onAdClicked();
                }
            }
            this.f3447 = new zzh(this.f3457, this.f3458.zzcjf, this.f3458.zzaty.f4297);
            this.f3447.setId(1000);
            switch (this.f3458.zzcje) {
                case 1:
                    m4029(false);
                    return;
                case 2:
                    this.f3455 = new zzi(this.f3458.zzciy);
                    m4029(false);
                    return;
                case 3:
                    m4029(true);
                    return;
                case 4:
                    if (this.f3453) {
                        this.f3445 = 3;
                        this.f3457.finish();
                        return;
                    }
                    zzbs.zzef();
                    if (!zza.zza(this.f3457, this.f3458.zzciv, this.f3458.zzcjd)) {
                        this.f3445 = 3;
                        this.f3457.finish();
                        return;
                    }
                    return;
                default:
                    throw new zzg("Could not determine ad overlay type.");
            }
        } catch (zzg e) {
            zzagf.m4791(e.getMessage());
            this.f3445 = 3;
            this.f3457.finish();
        }
    }

    public final void onDestroy() {
        if (this.f3456 != null) {
            zzh zzh = this.f3447;
            zzanh zzanh = this.f3456;
            if (zzanh == null) {
                throw null;
            }
            zzh.removeView((View) zzanh);
        }
        m4028();
    }

    public final void onPause() {
        zzms();
        if (this.f3458.zzcix != null) {
            this.f3458.zzcix.onPause();
        }
        if (!((Boolean) zzkb.m5481().m5595(zznh.f5038)).booleanValue() && this.f3456 != null && (!this.f3457.isFinishing() || this.f3455 == null)) {
            zzbs.zzek();
            zzaht.m4642(this.f3456);
        }
        m4028();
    }

    public final void onRestart() {
    }

    public final void onResume() {
        if (this.f3458 != null && this.f3458.zzcje == 4) {
            if (this.f3453) {
                this.f3445 = 3;
                this.f3457.finish();
            } else {
                this.f3453 = true;
            }
        }
        if (this.f3458.zzcix != null) {
            this.f3458.zzcix.onResume();
        }
        if (((Boolean) zzkb.m5481().m5595(zznh.f5038)).booleanValue()) {
            return;
        }
        if (this.f3456 == null || this.f3456.m4991()) {
            zzagf.m4791("The webview does not exist. Ignoring action.");
            return;
        }
        zzbs.zzek();
        zzaht.m4641(this.f3456);
    }

    public final void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.f3453);
    }

    public final void onStart() {
        if (!((Boolean) zzkb.m5481().m5595(zznh.f5038)).booleanValue()) {
            return;
        }
        if (this.f3456 == null || this.f3456.m4991()) {
            zzagf.m4791("The webview does not exist. Ignoring action.");
            return;
        }
        zzbs.zzek();
        zzaht.m4641(this.f3456);
    }

    public final void onStop() {
        if (((Boolean) zzkb.m5481().m5595(zznh.f5038)).booleanValue() && this.f3456 != null && (!this.f3457.isFinishing() || this.f3455 == null)) {
            zzbs.zzek();
            zzaht.m4642(this.f3456);
        }
        m4028();
    }

    public final void setRequestedOrientation(int i) {
        this.f3457.setRequestedOrientation(i);
    }

    public final void zza(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        this.f3443 = new FrameLayout(this.f3457);
        this.f3443.setBackgroundColor(-16777216);
        FrameLayout frameLayout = this.f3443;
        this.f3457.setContentView(this.f3443);
        this.f3450 = true;
        this.f3452 = customViewCallback;
        this.f3442 = true;
    }

    public final void zza(boolean z, boolean z2) {
        boolean z3 = ((Boolean) zzkb.m5481().m5595(zznh.f4923)).booleanValue() && this.f3458 != null && this.f3458.zzcjg != null && this.f3458.zzcjg.zzaqv;
        if (z && z2 && z3) {
            new zzxb(this.f3456, "useCustomClose").m6012("Custom close has been disabled for interstitial ads in this ad slot.");
        }
        if (this.f3441 != null) {
            this.f3441.zza(z, z2 && !z3);
        }
    }

    public final void zzbf() {
        this.f3450 = true;
    }

    public final void zzk(IObjectWrapper iObjectWrapper) {
        if (((Boolean) zzkb.m5481().m5595(zznh.f5037)).booleanValue() && zzq.m9266()) {
            zzbs.zzei();
            if (zzahn.m4616(this.f3457, (Configuration) zzn.m9307(iObjectWrapper))) {
                this.f3457.getWindow().addFlags(1024);
                this.f3457.getWindow().clearFlags(2048);
                return;
            }
            this.f3457.getWindow().addFlags(2048);
            this.f3457.getWindow().clearFlags(1024);
        }
    }

    public final void zzms() {
        if (this.f3458 != null && this.f3442) {
            setRequestedOrientation(this.f3458.orientation);
        }
        if (this.f3443 != null) {
            this.f3457.setContentView(this.f3447);
            this.f3450 = true;
            this.f3443.removeAllViews();
            this.f3443 = null;
        }
        if (this.f3452 != null) {
            this.f3452.onCustomViewHidden();
            this.f3452 = null;
        }
        this.f3442 = false;
    }

    public final void zzmt() {
        this.f3445 = 1;
        this.f3457.finish();
    }

    public final boolean zzmu() {
        this.f3445 = 0;
        if (this.f3456 == null) {
            return true;
        }
        boolean r0 = this.f3456.m4993();
        if (r0) {
            return r0;
        }
        this.f3456.zza("onbackblocked", (Map<String, ?>) Collections.emptyMap());
        return r0;
    }

    public final void zzmv() {
        this.f3447.removeView(this.f3441);
        m4032(true);
    }

    public final void zzmy() {
        if (this.f3444) {
            this.f3444 = false;
            m4030();
        }
    }

    public final void zzna() {
        this.f3447.f3462 = true;
    }

    public final void zznb() {
        synchronized (this.f3459) {
            this.f3449 = true;
            if (this.f3460 != null) {
                zzahn.f4212.removeCallbacks(this.f3460);
                zzahn.f4212.post(this.f3460);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4033() {
        if (!this.f3446) {
            this.f3446 = true;
            if (this.f3456 != null) {
                zzh zzh = this.f3447;
                zzanh zzanh = this.f3456;
                if (zzanh == null) {
                    throw null;
                }
                zzh.removeView((View) zzanh);
                if (this.f3455 != null) {
                    this.f3456.m5008(this.f3455.zzair);
                    this.f3456.m5002(false);
                    ViewGroup viewGroup = this.f3455.parent;
                    zzanh zzanh2 = this.f3456;
                    if (zzanh2 == null) {
                        throw null;
                    }
                    View view = (View) zzanh2;
                    int i = this.f3455.index;
                    ViewGroup.LayoutParams layoutParams = this.f3455.zzcis;
                    this.f3455 = null;
                } else if (this.f3457.getApplicationContext() != null) {
                    this.f3456.m5008(this.f3457.getApplicationContext());
                }
                this.f3456 = null;
            }
            if (this.f3458 != null && this.f3458.zzcix != null) {
                this.f3458.zzcix.zzcg();
            }
        }
    }
}
