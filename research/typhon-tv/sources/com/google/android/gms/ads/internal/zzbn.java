package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;

final class zzbn extends WebViewClient {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbm f6771;

    zzbn(zzbm zzbm) {
        this.f6771 = zzbm;
    }

    public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        if (this.f6771.f3525 != null) {
            try {
                this.f6771.f3525.m13061(0);
            } catch (RemoteException e) {
                zzagf.m4796("Could not call AdListener.onAdFailedToLoad().", e);
            }
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith(this.f6771.m4077())) {
            return false;
        }
        if (str.startsWith((String) zzkb.m5481().m5595(zznh.f5007))) {
            if (this.f6771.f3525 != null) {
                try {
                    this.f6771.f3525.m13061(3);
                } catch (RemoteException e) {
                    zzagf.m4796("Could not call AdListener.onAdFailedToLoad().", e);
                }
            }
            this.f6771.m4080(0);
            return true;
        }
        if (str.startsWith((String) zzkb.m5481().m5595(zznh.f5008))) {
            if (this.f6771.f3525 != null) {
                try {
                    this.f6771.f3525.m13061(0);
                } catch (RemoteException e2) {
                    zzagf.m4796("Could not call AdListener.onAdFailedToLoad().", e2);
                }
            }
            this.f6771.m4080(0);
            return true;
        }
        if (str.startsWith((String) zzkb.m5481().m5595(zznh.f5009))) {
            if (this.f6771.f3525 != null) {
                try {
                    this.f6771.f3525.m13059();
                } catch (RemoteException e3) {
                    zzagf.m4796("Could not call AdListener.onAdLoaded().", e3);
                }
            }
            this.f6771.m4080(this.f6771.m4078(str));
            return true;
        } else if (str.startsWith("gmsg://")) {
            return true;
        } else {
            if (this.f6771.f3525 != null) {
                try {
                    this.f6771.f3525.m13057();
                } catch (RemoteException e4) {
                    zzagf.m4796("Could not call AdListener.onAdLeftApplication().", e4);
                }
            }
            this.f6771.m4073(this.f6771.m4069(str));
            return true;
        }
    }
}
