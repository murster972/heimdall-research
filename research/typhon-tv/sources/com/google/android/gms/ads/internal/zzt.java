package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzol;

final class zzt implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzq f6802;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzol f6803;

    zzt(zzq zzq, zzol zzol) {
        this.f6802 = zzq;
        this.f6803 = zzol;
    }

    public final void run() {
        try {
            if (this.f6802.连任.f3579 != null) {
                this.f6802.连任.f3579.m13344(this.f6803);
            }
        } catch (RemoteException e) {
            zzagf.m4796("Could not call OnContentAdLoadedListener.onContentAdLoaded().", e);
        }
    }
}
