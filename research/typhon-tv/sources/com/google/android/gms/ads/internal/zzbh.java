package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzqm;

final class zzbh implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzba f6766;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzqm f6767;

    zzbh(zzba zzba, zzqm zzqm) {
        this.f6766 = zzba;
        this.f6767 = zzqm;
    }

    public final void run() {
        try {
            this.f6766.连任.f3583.get(this.f6767.m13319()).m13350(this.f6767);
        } catch (RemoteException e) {
            zzagf.m4796("Could not call onCustomTemplateAdLoadedListener.onCustomTemplateAdLoaded().", e);
        }
    }
}
