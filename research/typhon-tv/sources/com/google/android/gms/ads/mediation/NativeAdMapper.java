package com.google.android.gms.ads.mediation;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.internal.zzzv;

@zzzv
public class NativeAdMapper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private VideoController f3605;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f3606;

    /* renamed from: 连任  reason: contains not printable characters */
    private View f3607;

    /* renamed from: 靐  reason: contains not printable characters */
    protected boolean f3608;

    /* renamed from: 麤  reason: contains not printable characters */
    protected View f3609;

    /* renamed from: 齉  reason: contains not printable characters */
    protected Bundle f3610 = new Bundle();

    /* renamed from: 龘  reason: contains not printable characters */
    protected boolean f3611;

    public View getAdChoicesContent() {
        return this.f3609;
    }

    public final Bundle getExtras() {
        return this.f3610;
    }

    public final boolean getOverrideClickHandling() {
        return this.f3608;
    }

    public final boolean getOverrideImpressionRecording() {
        return this.f3611;
    }

    public final VideoController getVideoController() {
        return this.f3605;
    }

    public void handleClick(View view) {
    }

    public boolean hasVideoContent() {
        return this.f3606;
    }

    public void recordImpression() {
    }

    public void setAdChoicesContent(View view) {
        this.f3609 = view;
    }

    public final void setExtras(Bundle bundle) {
        this.f3610 = bundle;
    }

    public void setHasVideoContent(boolean z) {
        this.f3606 = z;
    }

    public void setMediaView(View view) {
        this.f3607 = view;
    }

    public final void setOverrideClickHandling(boolean z) {
        this.f3608 = z;
    }

    public final void setOverrideImpressionRecording(boolean z) {
        this.f3611 = z;
    }

    public void trackView(View view) {
    }

    public void untrackView(View view) {
    }

    public final void zza(VideoController videoController) {
        this.f3605 = videoController;
    }

    public final View zzul() {
        return this.f3607;
    }
}
