package com.google.android.gms.ads.internal.js;

final class zzu implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzo f6701;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzc f6702;

    zzu(zzo zzo, zzc zzc) {
        this.f6701 = zzo;
        this.f6702 = zzc;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6701
            com.google.android.gms.ads.internal.js.zzn r0 = r0.f6690
            java.lang.Object r1 = r0.f3437
            monitor-enter(r1)
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6701     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x003b }
            int r0 = r0.getStatus()     // Catch:{ all -> 0x003b }
            r2 = -1
            if (r0 == r2) goto L_0x001f
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6701     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x003b }
            int r0 = r0.getStatus()     // Catch:{ all -> 0x003b }
            r2 = 1
            if (r0 != r2) goto L_0x0021
        L_0x001f:
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
        L_0x0020:
            return
        L_0x0021:
            com.google.android.gms.ads.internal.js.zzo r0 = r3.f6701     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x003b }
            r0.reject()     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzv r0 = new com.google.android.gms.ads.internal.js.zzv     // Catch:{ all -> 0x003b }
            r0.<init>(r3)     // Catch:{ all -> 0x003b }
            com.google.android.gms.internal.zzahn.m4612((java.lang.Runnable) r0)     // Catch:{ all -> 0x003b }
            java.lang.String r0 = "Could not receive loaded message in a timely manner. Rejecting."
            com.google.android.gms.internal.zzagf.m4527(r0)     // Catch:{ all -> 0x003b }
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            goto L_0x0020
        L_0x003b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.js.zzu.run():void");
    }
}
