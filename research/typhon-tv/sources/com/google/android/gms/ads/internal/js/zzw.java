package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzalk;

final class zzw implements zzalk<zzc> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzn f6704;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzae f6705;

    zzw(zzn zzn, zzae zzae) {
        this.f6704 = zzn;
        this.f6705 = zzae;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m7644(Object obj) {
        synchronized (this.f6704.f3437) {
            int unused = this.f6704.f3432 = 0;
            if (!(this.f6704.f3431 == null || this.f6705 == this.f6704.f3431)) {
                zzagf.m4527("New JS engine is loaded, marking previous one as destroyable.");
                this.f6704.f3431.zzlr();
            }
            zzae unused2 = this.f6704.f3431 = this.f6705;
        }
    }
}
