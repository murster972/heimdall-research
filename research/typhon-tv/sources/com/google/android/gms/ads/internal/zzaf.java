package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahh;
import com.google.android.gms.internal.zzajr;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzzv;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@zzzv
public final class zzaf implements zzcr, Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private CountDownLatch f3469;

    /* renamed from: 靐  reason: contains not printable characters */
    private final AtomicReference<zzcr> f3470;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzakd f3471;

    /* renamed from: 齉  reason: contains not printable characters */
    private Context f3472;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<Object[]> f3473;

    private zzaf(Context context, zzakd zzakd) {
        this.f3473 = new Vector();
        this.f3470 = new AtomicReference<>();
        this.f3469 = new CountDownLatch(1);
        this.f3472 = context;
        this.f3471 = zzakd;
        zzkb.m5487();
        if (zzajr.m4752()) {
            zzahh.m4555((Runnable) this);
        } else {
            run();
        }
    }

    public zzaf(zzbt zzbt) {
        this(zzbt.zzair, zzbt.zzaty);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m4038() {
        if (!this.f3473.isEmpty()) {
            for (Object[] next : this.f3473) {
                if (next.length == 1) {
                    this.f3470.get().zza((MotionEvent) next[0]);
                } else if (next.length == 3) {
                    this.f3470.get().zza(((Integer) next[0]).intValue(), ((Integer) next[1]).intValue(), ((Integer) next[2]).intValue());
                }
            }
            this.f3473.clear();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Context m4039(Context context) {
        Context applicationContext = context.getApplicationContext();
        return applicationContext == null ? context : applicationContext;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m4040() {
        try {
            this.f3469.await();
            return true;
        } catch (InterruptedException e) {
            zzagf.m4796("Interrupted during GADSignals creation.", e);
            return false;
        }
    }

    public final void run() {
        try {
            this.f3470.set(zzcu.m11532(this.f3471.f4297, m4039(this.f3472), !((Boolean) zzkb.m5481().m5595(zznh.f4916)).booleanValue() && (this.f3471.f4295)));
        } finally {
            this.f3469.countDown();
            this.f3472 = null;
            this.f3471 = null;
        }
    }

    public final String zza(Context context) {
        zzcr zzcr;
        if (!m4040() || (zzcr = this.f3470.get()) == null) {
            return "";
        }
        m4038();
        return zzcr.zza(m4039(context));
    }

    public final String zza(Context context, String str, View view) {
        return zza(context, str, view, (Activity) null);
    }

    public final String zza(Context context, String str, View view, Activity activity) {
        zzcr zzcr;
        if (!m4040() || (zzcr = this.f3470.get()) == null) {
            return "";
        }
        m4038();
        return zzcr.zza(m4039(context), str, view, activity);
    }

    public final void zza(int i, int i2, int i3) {
        zzcr zzcr = this.f3470.get();
        if (zzcr != null) {
            m4038();
            zzcr.zza(i, i2, i3);
            return;
        }
        this.f3473.add(new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)});
    }

    public final void zza(MotionEvent motionEvent) {
        zzcr zzcr = this.f3470.get();
        if (zzcr != null) {
            m4038();
            zzcr.zza(motionEvent);
            return;
        }
        this.f3473.add(new Object[]{motionEvent});
    }

    public final void zzb(View view) {
        zzcr zzcr = this.f3470.get();
        if (zzcr != null) {
            zzcr.zzb(view);
        }
    }
}
