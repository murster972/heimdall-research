package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzanh;
import io.presage.ads.NewAd;
import java.util.Map;

final class zzr implements zzt<zzanh> {
    zzr() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        if (map.keySet().contains(TtmlNode.START)) {
            zzanh.m4980().m5035();
        } else if (map.keySet().contains("stop")) {
            zzanh.m4980().m5036();
        } else if (map.keySet().contains(NewAd.EVENT_CANCEL)) {
            zzanh.m4980().m5037();
        }
    }
}
