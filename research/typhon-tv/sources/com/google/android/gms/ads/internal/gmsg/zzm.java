package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzanh;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

final class zzm implements zzt<zzanh> {
    zzm() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzanh) obj).m5006(PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("custom_close")));
    }
}
