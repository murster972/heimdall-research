package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import com.google.android.gms.internal.zzaig;
import com.google.android.gms.internal.zzzv;

@zzzv
final class zzh extends RelativeLayout {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzaig f3461;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f3462;

    public zzh(Context context, String str, String str2) {
        super(context);
        this.f3461 = new zzaig(context, str);
        this.f3461.m4688(str2);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.f3462) {
            return false;
        }
        this.f3461.m4692(motionEvent);
        return false;
    }
}
