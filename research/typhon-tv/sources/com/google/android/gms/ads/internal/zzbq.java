package com.google.android.gms.ads.internal;

import android.os.AsyncTask;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class zzbq extends AsyncTask<Void, Void, String> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbm f6774;

    private zzbq(zzbm zzbm) {
        this.f6774 = zzbm;
    }

    /* synthetic */ zzbq(zzbm zzbm, zzbn zzbn) {
        this(zzbm);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String doInBackground(Void... voidArr) {
        try {
            zzcv unused = this.f6774.f3526 = (zzcv) this.f6774.f3531.get(((Long) zzkb.m5481().m5595(zznh.f5012)).longValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException e) {
            zzagf.m4796("Failed to load ad data", e);
        } catch (TimeoutException e2) {
            zzagf.m4791("Timed out waiting for ad data");
        }
        return this.f6774.m4079();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.f6774.f3524 != null && str != null) {
            this.f6774.f3524.loadUrl(str);
        }
    }
}
