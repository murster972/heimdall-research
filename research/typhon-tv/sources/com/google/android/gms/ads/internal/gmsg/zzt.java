package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzzv;
import java.util.Map;

@zzzv
public interface zzt<ContextT> {
    void zza(ContextT contextt, Map<String, String> map);
}
