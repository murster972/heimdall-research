package com.google.android.gms.ads.internal;

import android.view.View;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.internal.zzanh;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

final class zzat implements zzt<zzanh> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ CountDownLatch f6737;

    zzat(CountDownLatch countDownLatch) {
        this.f6737 = countDownLatch;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        this.f6737.countDown();
        if (zzanh == null) {
            throw null;
        }
        ((View) zzanh).setVisibility(0);
    }
}
