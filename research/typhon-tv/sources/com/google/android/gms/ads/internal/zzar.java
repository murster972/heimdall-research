package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzanm;
import com.google.android.gms.internal.zzoj;
import java.util.List;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class zzar implements zzanm {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f6731;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzanh f6732;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzoj f6733;

    zzar(zzoj zzoj, String str, zzanh zzanh) {
        this.f6733 = zzoj;
        this.f6731 = str;
        this.f6732 = zzanh;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7650(zzanh zzanh, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("headline", (Object) this.f6733.m5673());
            jSONObject.put(TtmlNode.TAG_BODY, (Object) this.f6733.m5671());
            jSONObject.put("call_to_action", (Object) this.f6733.m5667());
            jSONObject.put("price", (Object) this.f6733.m5657());
            jSONObject.put("star_rating", (Object) String.valueOf(this.f6733.m5655()));
            jSONObject.put("store", (Object) this.f6733.m5656());
            jSONObject.put(PubnativeAsset.ICON, (Object) zzaq.m4060(this.f6733.m5670()));
            JSONArray jSONArray = new JSONArray();
            List<Object> r2 = this.f6733.m5668();
            if (r2 != null) {
                for (Object r3 : r2) {
                    jSONArray.put((Object) zzaq.m4060(zzaq.m4053(r3)));
                }
            }
            jSONObject.put("images", (Object) jSONArray);
            jSONObject.put("extras", (Object) zzaq.m4055(this.f6733.m5659(), this.f6731));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("assets", (Object) jSONObject);
            jSONObject2.put("template_id", (Object) "2");
            this.f6732.zzb("google.afma.nativeExpressAds.loadAssets", jSONObject2);
        } catch (JSONException e) {
            zzagf.m4796("Exception occurred when loading assets", e);
        }
    }
}
