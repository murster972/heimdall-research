package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzcv;
import java.util.concurrent.Callable;

final class zzbp implements Callable<zzcv> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzbm f6773;

    zzbp(zzbm zzbm) {
        this.f6773 = zzbm;
    }

    public final /* synthetic */ Object call() throws Exception {
        return new zzcv(zzcu.m11532(this.f6773.f3532.f4297, this.f6773.f3530, false));
    }
}
