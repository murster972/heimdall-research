package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzoj;
import com.google.android.gms.internal.zzol;
import com.google.android.gms.internal.zzou;
import java.util.List;

final class zzbd implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ int f6755;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzba f6756;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ List f6757;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzou f6758;

    zzbd(zzba zzba, zzou zzou, int i, List list) {
        this.f6756 = zzba;
        this.f6758 = zzou;
        this.f6755 = i;
        this.f6757 = list;
    }

    public final void run() {
        boolean z = true;
        try {
            if ((this.f6758 instanceof zzol) && this.f6756.连任.f3579 != null) {
                zzba zzba = this.f6756;
                if (this.f6755 == this.f6757.size() - 1) {
                    z = false;
                }
                zzba.齉 = z;
                this.f6756.连任.f3579.m13344((zzol) this.f6758);
            } else if (!(this.f6758 instanceof zzoj) || this.f6756.连任.f3569 == null) {
                zzba zzba2 = this.f6756;
                if (this.f6755 == this.f6757.size() - 1) {
                    z = false;
                }
                zzba2.龘(3, z);
            } else {
                zzba zzba3 = this.f6756;
                if (this.f6755 == this.f6757.size() - 1) {
                    z = false;
                }
                zzba3.齉 = z;
                this.f6756.连任.f3569.m13341((zzoj) this.f6758);
            }
        } catch (RemoteException e) {
            zzagf.m4796("Could not call onAppInstallAdLoaded or onContentAdLoaded method", e);
        }
    }
}
