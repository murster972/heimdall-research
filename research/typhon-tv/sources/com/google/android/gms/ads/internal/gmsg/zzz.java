package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzzv;
import java.util.Map;

@zzzv
public final class zzz implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f3424;

    public zzz(Context context) {
        this.f3424 = context;
    }

    public final void zza(Object obj, Map<String, String> map) {
        if (zzbs.zzfd().m4436(this.f3424)) {
            zzbs.zzfd().m4434(this.f3424, map.get("eventName"), map.get("eventId"));
        }
    }
}
