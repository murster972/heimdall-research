package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzje;
import com.google.android.gms.internal.zzlv;

class BaseAdView extends ViewGroup {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzlv f6636;

    public BaseAdView(Context context, int i) {
        super(context);
        this.f6636 = new zzlv(this, i);
    }

    public BaseAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.f6636 = new zzlv(this, attributeSet, false, i);
    }

    public BaseAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        this.f6636 = new zzlv(this, attributeSet, false, i2);
    }

    public void destroy() {
        this.f6636.m5527();
    }

    public AdListener getAdListener() {
        return this.f6636.m5523();
    }

    public AdSize getAdSize() {
        return this.f6636.m5526();
    }

    public String getAdUnitId() {
        return this.f6636.m5522();
    }

    public String getMediationAdapterClassName() {
        return this.f6636.m5521();
    }

    public boolean isLoading() {
        return this.f6636.m5518();
    }

    public void loadAd(AdRequest adRequest) {
        this.f6636.m5534(adRequest.zzbg());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzakb.m4793("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
                i4 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            int measuredWidth = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
            i4 = measuredWidth;
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }

    public void pause() {
        this.f6636.m5515();
    }

    public void resume() {
        this.f6636.m5520();
    }

    public void setAdListener(AdListener adListener) {
        this.f6636.m5528(adListener);
        if (adListener == null) {
            this.f6636.m5533((zzje) null);
            this.f6636.m5531((AppEventListener) null);
            return;
        }
        if (adListener instanceof zzje) {
            this.f6636.m5533((zzje) adListener);
        }
        if (adListener instanceof AppEventListener) {
            this.f6636.m5531((AppEventListener) adListener);
        }
    }

    public void setAdSize(AdSize adSize) {
        this.f6636.m5537(adSize);
    }

    public void setAdUnitId(String str) {
        this.f6636.m5535(str);
    }
}
