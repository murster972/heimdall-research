package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzvj;
import com.google.android.gms.internal.zzvm;
import java.util.Map;

final class zzav implements zzt<zzanh> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzab f6739;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzvm f6740;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzvj f6741;

    zzav(zzvj zzvj, zzab zzab, zzvm zzvm) {
        this.f6741 = zzvj;
        this.f6739 = zzab;
        this.f6740 = zzvm;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        if (zzanh == null) {
            throw null;
        }
        View view = (View) zzanh;
        if (view != null) {
            try {
                if (this.f6741 != null) {
                    if (!this.f6741.m13540()) {
                        this.f6741.m13548(zzn.m9306(view));
                        this.f6739.f6720.onAdClicked();
                        return;
                    }
                    zzaq.m4056(zzanh);
                } else if (this.f6740 == null) {
                } else {
                    if (!this.f6740.m13577()) {
                        this.f6740.m13587(zzn.m9306(view));
                        this.f6739.f6720.onAdClicked();
                        return;
                    }
                    zzaq.m4056(zzanh);
                }
            } catch (RemoteException e) {
                zzagf.m4796("Unable to call handleClick on mapper", e);
            }
        }
    }
}
