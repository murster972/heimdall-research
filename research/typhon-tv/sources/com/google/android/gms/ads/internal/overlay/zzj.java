package com.google.android.gms.ads.internal.overlay;

import android.graphics.Bitmap;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzagb;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzzv;

@zzzv
final class zzj extends zzagb {

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzd f3463;

    private zzj(zzd zzd) {
        this.f3463 = zzd;
    }

    /* synthetic */ zzj(zzd zzd, zze zze) {
        this(zzd);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m4034() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4035() {
        Bitmap r0 = zzbs.zzfa().m4721(Integer.valueOf(this.f3463.f3458.zzcjg.zzaqt));
        if (r0 != null) {
            zzahn.f4212.post(new zzk(this, zzbs.zzek().m4653(this.f3463.f3457, r0, this.f3463.f3458.zzcjg.zzaqr, this.f3463.f3458.zzcjg.zzaqs)));
        }
    }
}
