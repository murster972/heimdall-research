package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzagf;
import org.json.JSONObject;

final class zzv implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzu f6660;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ JSONObject f6661;

    zzv(zzu zzu, JSONObject jSONObject) {
        this.f6660 = zzu;
        this.f6661 = jSONObject;
    }

    public final void run() {
        this.f6660.f6659.zza("fetchHttpRequestCompleted", this.f6661);
        zzagf.m4792("Dispatched http response.");
    }
}
