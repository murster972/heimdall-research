package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzalk;

final class zzac implements zzalk<zzaj> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzaa f6665;

    zzac(zzaa zzaa) {
        this.f6665 = zzaa;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m7633(Object obj) {
        zzagf.m4527("Releasing engine reference.");
        this.f6665.f6662.m7637();
    }
}
