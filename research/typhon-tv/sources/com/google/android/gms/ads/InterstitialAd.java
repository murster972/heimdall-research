package com.google.android.gms.ads;

import android.content.Context;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzje;
import com.google.android.gms.internal.zzlx;

public final class InterstitialAd {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzlx f6637;

    public InterstitialAd(Context context) {
        this.f6637 = new zzlx(context);
        zzbq.m9121(context, (Object) "Context cannot be null");
    }

    public final AdListener getAdListener() {
        return this.f6637.m5549();
    }

    public final String getAdUnitId() {
        return this.f6637.m5545();
    }

    public final String getMediationAdapterClassName() {
        return this.f6637.m5542();
    }

    public final boolean isLoaded() {
        return this.f6637.m5544();
    }

    public final boolean isLoading() {
        return this.f6637.m5541();
    }

    public final void loadAd(AdRequest adRequest) {
        this.f6637.m5556(adRequest.zzbg());
    }

    public final void setAdListener(AdListener adListener) {
        this.f6637.m5550(adListener);
        if (adListener != null && (adListener instanceof zzje)) {
            this.f6637.m5555((zzje) adListener);
        } else if (adListener == null) {
            this.f6637.m5555((zzje) null);
        }
    }

    public final void setAdUnitId(String str) {
        this.f6637.m5557(str);
    }

    public final void setImmersiveMode(boolean z) {
        this.f6637.m5546(z);
    }

    public final void setRewardedVideoAdListener(RewardedVideoAdListener rewardedVideoAdListener) {
        this.f6637.m5554(rewardedVideoAdListener);
    }

    public final void show() {
        this.f6637.m5543();
    }

    public final void zza(boolean z) {
        this.f6637.m5558(true);
    }
}
