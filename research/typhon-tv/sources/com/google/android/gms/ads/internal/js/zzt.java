package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzajj;
import java.util.Map;

final class zzt implements com.google.android.gms.ads.internal.gmsg.zzt<zzaj> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzajj f6698;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzo f6699;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzc f6700;

    zzt(zzo zzo, zzc zzc, zzajj zzajj) {
        this.f6699 = zzo;
        this.f6700 = zzc;
        this.f6698 = zzajj;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        synchronized (this.f6699.f6690.f3437) {
            zzagf.m4794("JS Engine is requesting an update");
            if (this.f6699.f6690.f3432 == 0) {
                zzagf.m4794("Starting reload.");
                int unused = this.f6699.f6690.f3432 = 2;
                this.f6699.f6690.m4026(this.f6699.f6691);
            }
            this.f6700.zzb("/requestReload", (com.google.android.gms.ads.internal.gmsg.zzt) this.f6698.m4725());
        }
    }
}
