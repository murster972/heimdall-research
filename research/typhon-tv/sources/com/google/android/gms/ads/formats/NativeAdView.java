package com.google.android.gms.ads.formats;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.Pinkamena;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzpu;

public class NativeAdView extends FrameLayout {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzpu f6643 = m7621();

    /* renamed from: 龘  reason: contains not printable characters */
    private final FrameLayout f6644;

    public NativeAdView(Context context) {
        super(context);
        this.f6644 = m7620(context);
    }

    public NativeAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f6644 = m7620(context);
    }

    public NativeAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f6644 = m7620(context);
    }

    @TargetApi(21)
    public NativeAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f6644 = m7620(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final FrameLayout m7620(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        Pinkamena.DianePie();
        return frameLayout;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzpu m7621() {
        zzbq.m9121(this.f6644, (Object) "createDelegate must be called after mOverlayFrame has been created");
        if (isInEditMode()) {
            return null;
        }
        return zzkb.m5484().m5478(this.f6644.getContext(), (FrameLayout) this, this.f6644);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        super.bringChildToFront(this.f6644);
    }

    public void bringChildToFront(View view) {
        super.bringChildToFront(view);
        if (this.f6644 != view) {
            super.bringChildToFront(this.f6644);
        }
    }

    public void destroy() {
        try {
            this.f6643.m13231();
        } catch (RemoteException e) {
            zzakb.m4793("Unable to destroy native ad view", e);
        }
    }

    public AdChoicesView getAdChoicesView() {
        View r0 = m7622(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW);
        if (r0 instanceof AdChoicesView) {
            return (AdChoicesView) r0;
        }
        return null;
    }

    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (this.f6643 != null) {
            try {
                this.f6643.m13233(zzn.m9306(view), i);
            } catch (RemoteException e) {
                zzakb.m4793("Unable to call onVisibilityChanged on delegate", e);
            }
        }
    }

    public void removeAllViews() {
        super.removeAllViews();
        FrameLayout frameLayout = this.f6644;
    }

    public void removeView(View view) {
        if (this.f6644 != view) {
            super.removeView(view);
        }
    }

    public void setAdChoicesView(AdChoicesView adChoicesView) {
        m7623(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW, adChoicesView);
    }

    public void setNativeAd(NativeAd nativeAd) {
        try {
            this.f6643.m13232((IObjectWrapper) nativeAd.m7619());
        } catch (RemoteException e) {
            zzakb.m4793("Unable to call setNativeAd on delegate", e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final View m7622(String str) {
        try {
            IObjectWrapper r0 = this.f6643.m13230(str);
            if (r0 != null) {
                return (View) zzn.m9307(r0);
            }
        } catch (RemoteException e) {
            zzakb.m4793("Unable to call getAssetView on delegate", e);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7623(String str, View view) {
        try {
            this.f6643.m13234(str, zzn.m9306(view));
        } catch (RemoteException e) {
            zzakb.m4793("Unable to call setAssetView on delegate", e);
        }
    }
}
