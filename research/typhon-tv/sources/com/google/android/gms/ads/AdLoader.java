package com.google.android.gms.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzjg;
import com.google.android.gms.internal.zzjm;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzkk;
import com.google.android.gms.internal.zzkn;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzlt;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzqq;
import com.google.android.gms.internal.zzqt;
import com.google.android.gms.internal.zzrk;
import com.google.android.gms.internal.zzrl;
import com.google.android.gms.internal.zzrm;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;
import com.google.android.gms.internal.zzuw;
import com.google.android.gms.internal.zzux;

public class AdLoader {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f3364;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzkk f3365;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzjm f3366;

    public static class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private final zzkn f6630;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Context f6631;

        private Builder(Context context, zzkn zzkn) {
            this.f6631 = context;
            this.f6630 = zzkn;
        }

        public Builder(Context context, String str) {
            this((Context) zzbq.m9121(context, (Object) "context cannot be null"), zzkb.m5484().m5477(context, str, (zzux) new zzuw()));
        }

        public AdLoader build() {
            try {
                return new AdLoader(this.f6631, this.f6630.zzdi());
            } catch (RemoteException e) {
                zzakb.m4793("Failed to build AdLoader.", e);
                return null;
            }
        }

        public Builder forAppInstallAd(NativeAppInstallAd.OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
            try {
                this.f6630.zza((zzqq) new zzrk(onAppInstallAdLoadedListener));
            } catch (RemoteException e) {
                zzakb.m4796("Failed to add app install ad listener", e);
            }
            return this;
        }

        public Builder forContentAd(NativeContentAd.OnContentAdLoadedListener onContentAdLoadedListener) {
            try {
                this.f6630.zza((zzqt) new zzrl(onContentAdLoadedListener));
            } catch (RemoteException e) {
                zzakb.m4796("Failed to add content ad listener", e);
            }
            return this;
        }

        public Builder forCustomTemplateAd(String str, NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener, NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
            try {
                this.f6630.zza(str, new zzrn(onCustomTemplateAdLoadedListener), onCustomClickListener == null ? null : new zzrm(onCustomClickListener));
            } catch (RemoteException e) {
                zzakb.m4796("Failed to add custom template ad listener", e);
            }
            return this;
        }

        public Builder forPublisherAdView(OnPublisherAdViewLoadedListener onPublisherAdViewLoadedListener, AdSize... adSizeArr) {
            if (adSizeArr == null || adSizeArr.length <= 0) {
                throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
            }
            try {
                this.f6630.zza(new zzro(onPublisherAdViewLoadedListener), new zzjn(this.f6631, adSizeArr));
            } catch (RemoteException e) {
                zzakb.m4796("Failed to add publisher banner ad listener", e);
            }
            return this;
        }

        public Builder withAdListener(AdListener adListener) {
            try {
                this.f6630.zzb((zzkh) new zzjg(adListener));
            } catch (RemoteException e) {
                zzakb.m4796("Failed to set AdListener.", e);
            }
            return this;
        }

        public Builder withCorrelator(Correlator correlator) {
            zzbq.m9120(correlator);
            try {
                this.f6630.zzb((zzld) correlator.zzbh());
            } catch (RemoteException e) {
                zzakb.m4796("Failed to set correlator.", e);
            }
            return this;
        }

        public Builder withNativeAdOptions(NativeAdOptions nativeAdOptions) {
            try {
                this.f6630.zza(new zzpe(nativeAdOptions));
            } catch (RemoteException e) {
                zzakb.m4796("Failed to specify native ad options", e);
            }
            return this;
        }

        public Builder withPublisherAdViewOptions(PublisherAdViewOptions publisherAdViewOptions) {
            try {
                this.f6630.zza(publisherAdViewOptions);
            } catch (RemoteException e) {
                zzakb.m4796("Failed to specify DFP banner ad options", e);
            }
            return this;
        }
    }

    AdLoader(Context context, zzkk zzkk) {
        this(context, zzkk, zzjm.f4788);
    }

    private AdLoader(Context context, zzkk zzkk, zzjm zzjm) {
        this.f3364 = context;
        this.f3365 = zzkk;
        this.f3366 = zzjm;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m3986(zzlt zzlt) {
        try {
            this.f3365.zzd(zzjm.m5452(this.f3364, zzlt));
        } catch (RemoteException e) {
            zzakb.m4793("Failed to load ad.", e);
        }
    }

    @Deprecated
    public String getMediationAdapterClassName() {
        try {
            return this.f3365.zzcp();
        } catch (RemoteException e) {
            zzakb.m4796("Failed to get the mediation adapter class name.", e);
            return null;
        }
    }

    public boolean isLoading() {
        try {
            return this.f3365.isLoading();
        } catch (RemoteException e) {
            zzakb.m4796("Failed to check if ad is loading.", e);
            return false;
        }
    }

    public void loadAd(AdRequest adRequest) {
        m3986(adRequest.zzbg());
    }

    public void loadAd(PublisherAdRequest publisherAdRequest) {
        m3986(publisherAdRequest.zzbg());
    }

    public void loadAds(AdRequest adRequest, int i) {
        try {
            this.f3365.zza(zzjm.m5452(this.f3364, adRequest.zzbg()), i);
        } catch (RemoteException e) {
            zzakb.m4793("Failed to load ads.", e);
        }
    }
}
