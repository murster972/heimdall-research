package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.zzafb;
import com.google.android.gms.internal.zzafp;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzanh;
import com.google.android.gms.internal.zzanv;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zznv;
import com.google.android.gms.internal.zzya;

final class zzl implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    final /* synthetic */ zzafb f6790;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zznu f6791;

    /* renamed from: 齉  reason: contains not printable characters */
    final /* synthetic */ zzi f6792;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzafp f6793;

    zzl(zzi zzi, zzafp zzafp, zzafb zzafb, zznu zznu) {
        this.f6792 = zzi;
        this.f6793 = zzafp;
        this.f6790 = zzafb;
        this.f6791 = zznu;
    }

    /* JADX WARNING: type inference failed for: r6v0, types: [com.google.android.gms.ads.internal.zzi, com.google.android.gms.internal.zzyb] */
    public final void run() {
        if (this.f6793.f4133.f3838 && this.f6792.连任.f3575 != null) {
            String str = null;
            if (this.f6793.f4133.f3861 != null) {
                zzbs.zzei();
                str = zzahn.m4602(this.f6793.f4133.f3861);
            }
            zznv zznv = new zznv(this.f6792, str, this.f6793.f4133.f3858);
            this.f6792.连任.zzauz = 1;
            try {
                this.f6792.齉 = false;
                this.f6792.连任.f3575.m13177(zznv);
                return;
            } catch (RemoteException e) {
                zzagf.m4796("Could not call the onCustomRenderedAdLoadedListener.", e);
                this.f6792.齉 = true;
            }
        }
        zzw zzw = new zzw(this.f6792.连任.zzair, this.f6790, this.f6793.f4133.f3865);
        try {
            zzanh r4 = this.f6792.m4086(this.f6793, zzw, this.f6790);
            r4.setOnTouchListener(new zzn(this, zzw));
            r4.setOnClickListener(new zzo(this, zzw));
            this.f6792.连任.zzauz = 0;
            zzbt zzbt = this.f6792.连任;
            zzbs.zzeh();
            zzbt.zzaub = zzya.m6026(this.f6792.连任.zzair, this.f6792, this.f6793, this.f6792.连任.f3589, r4, this.f6792.ˑ, this.f6792, this.f6791);
        } catch (zzanv e2) {
            zzagf.m4793("Could not obtain webview.", e2);
            zzahn.f4212.post(new zzm(this));
        }
    }
}
