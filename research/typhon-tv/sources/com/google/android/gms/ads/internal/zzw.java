package com.google.android.gms.ads.internal;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.zzaaz;
import com.google.android.gms.internal.zzafb;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzzv;
import java.util.Map;

@zzzv
public final class zzw {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f3599;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzaaz f3600;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzafb f3601;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f3602;

    public zzw(Context context, zzafb zzafb, zzaaz zzaaz) {
        this.f3602 = context;
        this.f3601 = zzafb;
        this.f3600 = zzaaz;
        if (this.f3600 == null) {
            this.f3600 = new zzaaz();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m4096() {
        return (this.f3601 != null && this.f3601.m9563().f4069) || this.f3600.f3867;
    }

    public final void recordClick() {
        this.f3599 = true;
    }

    public final boolean zzda() {
        return !m4096() || this.f3599;
    }

    public final void zzt(String str) {
        if (m4096()) {
            if (str == null) {
                str = "";
            }
            if (this.f3601 != null) {
                this.f3601.m9566(str, (Map<String, String>) null, 3);
            } else if (this.f3600.f3867 && this.f3600.f3866 != null) {
                for (String next : this.f3600.f3866) {
                    if (!TextUtils.isEmpty(next)) {
                        String replace = next.replace("{NAVIGATION_URL}", Uri.encode(str));
                        zzbs.zzei();
                        zzahn.m4582(this.f3602, "", replace);
                    }
                }
            }
        }
    }
}
