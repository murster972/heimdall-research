package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzzv;
import java.util.Map;

@zzzv
public final class zza implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzb f3404;

    public zza(zzb zzb) {
        this.f3404 = zzb;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get("name");
        if (str == null) {
            zzagf.m4791("App event with no name parameter.");
        } else {
            this.f3404.onAppEvent(str, map.get("info"));
        }
    }
}
