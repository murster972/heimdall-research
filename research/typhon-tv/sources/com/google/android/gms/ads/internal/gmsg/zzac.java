package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.View;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzcv;
import java.util.ArrayList;
import java.util.List;

public final class zzac {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzcv f6649;

    /* renamed from: 齉  reason: contains not printable characters */
    private final View f6650;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f6651;

    public zzac(Context context, zzcv zzcv, View view) {
        this.f6651 = context;
        this.f6649 = zzcv;
        this.f6650 = view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Intent m7627(Intent intent, ResolveInfo resolveInfo) {
        Intent intent2 = new Intent(intent);
        intent2.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        return intent2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Intent m7628(Uri uri) {
        if (uri == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setData(uri);
        intent.setAction("android.intent.action.VIEW");
        return intent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResolveInfo m7629(Intent intent) {
        return m7630(intent, (ArrayList<ResolveInfo>) new ArrayList());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResolveInfo m7630(Intent intent, ArrayList<ResolveInfo> arrayList) {
        ResolveInfo resolveInfo;
        try {
            PackageManager packageManager = this.f6651.getPackageManager();
            if (packageManager == null) {
                return null;
            }
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 65536);
            if (queryIntentActivities != null && resolveActivity != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= queryIntentActivities.size()) {
                        break;
                    }
                    ResolveInfo resolveInfo2 = queryIntentActivities.get(i2);
                    if (resolveActivity != null && resolveActivity.activityInfo.name.equals(resolveInfo2.activityInfo.name)) {
                        resolveInfo = resolveActivity;
                        break;
                    }
                    i = i2 + 1;
                }
                arrayList.addAll(queryIntentActivities);
                return resolveInfo;
            }
            resolveInfo = null;
            try {
                arrayList.addAll(queryIntentActivities);
                return resolveInfo;
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            resolveInfo = null;
            zzbs.zzem().m4505(th, "OpenSystemBrowserHandler.getDefaultBrowserResolverForIntent");
            return resolveInfo;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00d3 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.content.Intent zzm(java.util.Map<java.lang.String, java.lang.String> r13) {
        /*
            r12 = this;
            r3 = 0
            r4 = 0
            android.content.Context r0 = r12.f6651
            java.lang.String r1 = "activity"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            java.lang.String r1 = "u"
            java.lang.Object r1 = r13.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x001d
        L_0x001c:
            return r3
        L_0x001d:
            com.google.android.gms.ads.internal.zzbs.zzei()
            android.content.Context r2 = r12.f6651
            com.google.android.gms.internal.zzcv r5 = r12.f6649
            android.view.View r6 = r12.f6650
            java.lang.String r1 = com.google.android.gms.internal.zzahn.m4599((android.content.Context) r2, (com.google.android.gms.internal.zzcv) r5, (java.lang.String) r1, (android.view.View) r6, (android.app.Activity) r3)
            android.net.Uri r5 = android.net.Uri.parse(r1)
            java.lang.String r1 = "use_first_package"
            java.lang.Object r1 = r13.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r7 = java.lang.Boolean.parseBoolean(r1)
            java.lang.String r1 = "use_running_process"
            java.lang.Object r1 = r13.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r6 = java.lang.Boolean.parseBoolean(r1)
            java.lang.String r1 = "use_custom_tabs"
            java.lang.Object r1 = r13.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = java.lang.Boolean.parseBoolean(r1)
            if (r1 != 0) goto L_0x0069
            com.google.android.gms.internal.zzmx<java.lang.Boolean> r1 = com.google.android.gms.internal.zznh.f5026
            com.google.android.gms.internal.zznf r2 = com.google.android.gms.internal.zzkb.m5481()
            java.lang.Object r1 = r2.m5595(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x00b3
        L_0x0069:
            r1 = 1
            r2 = r1
        L_0x006b:
            java.lang.String r1 = "http"
            java.lang.String r8 = r5.getScheme()
            boolean r1 = r1.equalsIgnoreCase(r8)
            if (r1 == 0) goto L_0x00b5
            android.net.Uri$Builder r1 = r5.buildUpon()
            java.lang.String r3 = "https"
            android.net.Uri$Builder r1 = r1.scheme(r3)
            android.net.Uri r1 = r1.build()
            r3 = r1
        L_0x0088:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            android.content.Intent r5 = m7628((android.net.Uri) r5)
            android.content.Intent r3 = m7628((android.net.Uri) r3)
            if (r2 == 0) goto L_0x00a7
            com.google.android.gms.ads.internal.zzbs.zzei()
            android.content.Context r2 = r12.f6651
            com.google.android.gms.internal.zzahn.m4581((android.content.Context) r2, (android.content.Intent) r5)
            com.google.android.gms.ads.internal.zzbs.zzei()
            android.content.Context r2 = r12.f6651
            com.google.android.gms.internal.zzahn.m4581((android.content.Context) r2, (android.content.Intent) r3)
        L_0x00a7:
            android.content.pm.ResolveInfo r2 = r12.m7630((android.content.Intent) r5, (java.util.ArrayList<android.content.pm.ResolveInfo>) r1)
            if (r2 == 0) goto L_0x00d3
            android.content.Intent r3 = m7627((android.content.Intent) r5, (android.content.pm.ResolveInfo) r2)
            goto L_0x001c
        L_0x00b3:
            r2 = r4
            goto L_0x006b
        L_0x00b5:
            java.lang.String r1 = "https"
            java.lang.String r8 = r5.getScheme()
            boolean r1 = r1.equalsIgnoreCase(r8)
            if (r1 == 0) goto L_0x0088
            android.net.Uri$Builder r1 = r5.buildUpon()
            java.lang.String r3 = "http"
            android.net.Uri$Builder r1 = r1.scheme(r3)
            android.net.Uri r1 = r1.build()
            r3 = r1
            goto L_0x0088
        L_0x00d3:
            if (r3 == 0) goto L_0x00e5
            android.content.pm.ResolveInfo r2 = r12.m7629((android.content.Intent) r3)
            if (r2 == 0) goto L_0x00e5
            android.content.Intent r3 = m7627((android.content.Intent) r5, (android.content.pm.ResolveInfo) r2)
            android.content.pm.ResolveInfo r2 = r12.m7629((android.content.Intent) r3)
            if (r2 != 0) goto L_0x001c
        L_0x00e5:
            int r2 = r1.size()
            if (r2 != 0) goto L_0x00ee
            r3 = r5
            goto L_0x001c
        L_0x00ee:
            if (r6 == 0) goto L_0x012e
            if (r0 == 0) goto L_0x012e
            java.util.List r8 = r0.getRunningAppProcesses()
            if (r8 == 0) goto L_0x012e
            r0 = r1
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            int r9 = r0.size()
            r3 = r4
        L_0x0100:
            if (r3 >= r9) goto L_0x012e
            java.lang.Object r2 = r0.get(r3)
            int r6 = r3 + 1
            android.content.pm.ResolveInfo r2 = (android.content.pm.ResolveInfo) r2
            java.util.Iterator r10 = r8.iterator()
        L_0x010e:
            boolean r3 = r10.hasNext()
            if (r3 == 0) goto L_0x012c
            java.lang.Object r3 = r10.next()
            android.app.ActivityManager$RunningAppProcessInfo r3 = (android.app.ActivityManager.RunningAppProcessInfo) r3
            java.lang.String r3 = r3.processName
            android.content.pm.ActivityInfo r11 = r2.activityInfo
            java.lang.String r11 = r11.packageName
            boolean r3 = r3.equals(r11)
            if (r3 == 0) goto L_0x010e
            android.content.Intent r3 = m7627((android.content.Intent) r5, (android.content.pm.ResolveInfo) r2)
            goto L_0x001c
        L_0x012c:
            r3 = r6
            goto L_0x0100
        L_0x012e:
            if (r7 == 0) goto L_0x013c
            java.lang.Object r0 = r1.get(r4)
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0
            android.content.Intent r3 = m7627((android.content.Intent) r5, (android.content.pm.ResolveInfo) r0)
            goto L_0x001c
        L_0x013c:
            r3 = r5
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.zzac.zzm(java.util.Map):android.content.Intent");
    }
}
