package com.google.android.gms.ads;

import com.google.android.gms.internal.zzmr;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class VideoOptions {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f3373;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f3374;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f3375;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean f6638 = false;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean f6639 = false;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean f6640 = true;

        public final VideoOptions build() {
            return new VideoOptions(this);
        }

        public final Builder setClickToExpandRequested(boolean z) {
            this.f6639 = z;
            return this;
        }

        public final Builder setCustomControlsRequested(boolean z) {
            this.f6638 = z;
            return this;
        }

        public final Builder setStartMuted(boolean z) {
            this.f6640 = z;
            return this;
        }
    }

    private VideoOptions(Builder builder) {
        this.f3375 = builder.f6640;
        this.f3373 = builder.f6638;
        this.f3374 = builder.f6639;
    }

    public VideoOptions(zzmr zzmr) {
        this.f3375 = zzmr.f4879;
        this.f3373 = zzmr.f4877;
        this.f3374 = zzmr.f4878;
    }

    public final boolean getClickToExpandRequested() {
        return this.f3374;
    }

    public final boolean getCustomControlsRequested() {
        return this.f3373;
    }

    public final boolean getStartMuted() {
        return this.f3375;
    }
}
