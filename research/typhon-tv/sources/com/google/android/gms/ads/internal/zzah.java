package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzjj;

final class zzah implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzag f6722;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzjj f6723;

    zzah(zzag zzag, zzjj zzjj) {
        this.f6722 = zzag;
        this.f6723 = zzjj;
    }

    public final void run() {
        synchronized (this.f6722.f3482) {
            if (this.f6722.m4050()) {
                this.f6722.m4047(this.f6723);
            } else {
                this.f6722.m4048(this.f6723, 1);
            }
        }
    }
}
