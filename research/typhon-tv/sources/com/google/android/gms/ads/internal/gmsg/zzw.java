package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzzv;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

@zzzv
public final class zzw implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzx f3422;

    public zzw(zzx zzx) {
        this.f3422 = zzx;
    }

    public final void zza(Object obj, Map<String, String> map) {
        float f;
        boolean equals = PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("transparentBackground"));
        boolean equals2 = PubnativeRequest.LEGACY_ZONE_ID.equals(map.get("blur"));
        try {
            if (map.get("blurRadius") != null) {
                f = Float.parseFloat(map.get("blurRadius"));
                this.f3422.zzd(equals);
                this.f3422.zza(equals2, f);
            }
        } catch (NumberFormatException e) {
            zzagf.m4793("Fail to parse float", e);
        }
        f = 0.0f;
        this.f3422.zzd(equals);
        this.f3422.zza(equals2, f);
    }
}
