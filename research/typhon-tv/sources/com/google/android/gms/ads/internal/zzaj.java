package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzjn;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzkk;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzqq;
import com.google.android.gms.internal.zzqt;
import com.google.android.gms.internal.zzqw;
import com.google.android.gms.internal.zzqz;
import com.google.android.gms.internal.zzrc;
import com.google.android.gms.internal.zzrf;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzaj extends zzko {

    /* renamed from: ʻ  reason: contains not printable characters */
    private SimpleArrayMap<String, zzqz> f3493 = new SimpleArrayMap<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzrf f3494;

    /* renamed from: ʽ  reason: contains not printable characters */
    private zzjn f3495;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final zzux f3496;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final String f3497;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Context f3498;

    /* renamed from: ˑ  reason: contains not printable characters */
    private PublisherAdViewOptions f3499;

    /* renamed from: ٴ  reason: contains not printable characters */
    private zzpe f3500;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzld f3501;

    /* renamed from: 连任  reason: contains not printable characters */
    private SimpleArrayMap<String, zzqw> f3502 = new SimpleArrayMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzqq f3503;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzqt f3504;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzrc f3505;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzkh f3506;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final zzakd f3507;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zzv f3508;

    public zzaj(Context context, String str, zzux zzux, zzakd zzakd, zzv zzv) {
        this.f3498 = context;
        this.f3497 = str;
        this.f3496 = zzux;
        this.f3507 = zzakd;
        this.f3508 = zzv;
    }

    public final void zza(PublisherAdViewOptions publisherAdViewOptions) {
        this.f3499 = publisherAdViewOptions;
    }

    public final void zza(zzpe zzpe) {
        this.f3500 = zzpe;
    }

    public final void zza(zzqq zzqq) {
        this.f3503 = zzqq;
    }

    public final void zza(zzqt zzqt) {
        this.f3504 = zzqt;
    }

    public final void zza(zzrc zzrc) {
        this.f3505 = zzrc;
    }

    public final void zza(zzrf zzrf, zzjn zzjn) {
        this.f3494 = zzrf;
        this.f3495 = zzjn;
    }

    public final void zza(String str, zzqz zzqz, zzqw zzqw) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Custom template ID for native custom template ad is empty. Please provide a valid template id.");
        }
        this.f3493.put(str, zzqz);
        this.f3502.put(str, zzqw);
    }

    public final void zzb(zzkh zzkh) {
        this.f3506 = zzkh;
    }

    public final void zzb(zzld zzld) {
        this.f3501 = zzld;
    }

    public final zzkk zzdi() {
        return new zzag(this.f3498, this.f3497, this.f3496, this.f3507, this.f3506, this.f3503, this.f3505, this.f3504, this.f3493, this.f3502, this.f3500, this.f3501, this.f3508, this.f3494, this.f3495, this.f3499);
    }
}
