package com.google.android.gms.ads;

import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class Correlator {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzkc f3368 = new zzkc();

    public final void reset() {
        this.f3368.m5489();
    }

    public final zzkc zzbh() {
        return this.f3368;
    }
}
