package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;

public class NativeAppInstallAdMapper extends NativeAdMapper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private List<NativeAd.Image> f6810;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f6811;

    /* renamed from: ʽ  reason: contains not printable characters */
    private NativeAd.Image f6812;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f6813;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f6814;

    /* renamed from: ٴ  reason: contains not printable characters */
    private double f6815;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f6816;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f6817;

    public final String getBody() {
        return this.f6811;
    }

    public final String getCallToAction() {
        return this.f6814;
    }

    public final String getHeadline() {
        return this.f6817;
    }

    public final NativeAd.Image getIcon() {
        return this.f6812;
    }

    public final List<NativeAd.Image> getImages() {
        return this.f6810;
    }

    public final String getPrice() {
        return this.f6813;
    }

    public final double getStarRating() {
        return this.f6815;
    }

    public final String getStore() {
        return this.f6816;
    }

    public final void setBody(String str) {
        this.f6811 = str;
    }

    public final void setCallToAction(String str) {
        this.f6814 = str;
    }

    public final void setHeadline(String str) {
        this.f6817 = str;
    }

    public final void setIcon(NativeAd.Image image) {
        this.f6812 = image;
    }

    public final void setImages(List<NativeAd.Image> list) {
        this.f6810 = list;
    }

    public final void setPrice(String str) {
        this.f6813 = str;
    }

    public final void setStarRating(double d) {
        this.f6815 = d;
    }

    public final void setStore(String str) {
        this.f6816 = str;
    }
}
