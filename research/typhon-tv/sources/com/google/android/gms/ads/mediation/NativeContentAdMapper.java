package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;

public class NativeContentAdMapper extends NativeAdMapper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private List<NativeAd.Image> f6818;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f6819;

    /* renamed from: ʽ  reason: contains not printable characters */
    private NativeAd.Image f6820;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f6821;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f6822;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f6823;

    public final String getAdvertiser() {
        return this.f6822;
    }

    public final String getBody() {
        return this.f6819;
    }

    public final String getCallToAction() {
        return this.f6821;
    }

    public final String getHeadline() {
        return this.f6823;
    }

    public final List<NativeAd.Image> getImages() {
        return this.f6818;
    }

    public final NativeAd.Image getLogo() {
        return this.f6820;
    }

    public final void setAdvertiser(String str) {
        this.f6822 = str;
    }

    public final void setBody(String str) {
        this.f6819 = str;
    }

    public final void setCallToAction(String str) {
        this.f6821 = str;
    }

    public final void setHeadline(String str) {
        this.f6823 = str;
    }

    public final void setImages(List<NativeAd.Image> list) {
        this.f6818 = list;
    }

    public final void setLogo(NativeAd.Image image) {
        this.f6820 = image;
    }
}
