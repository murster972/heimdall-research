package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.ads.internal.js.zza;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import java.util.Map;
import org.json.JSONObject;

final class zzu implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Map f6657;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ HttpClient f6658;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zza f6659;

    zzu(HttpClient httpClient, Map map, zza zza) {
        this.f6658 = httpClient;
        this.f6657 = map;
        this.f6659 = zza;
    }

    public final void run() {
        zzagf.m4792("Received Http request.");
        try {
            JSONObject send = this.f6658.send(new JSONObject((String) this.f6657.get("http_request")));
            if (send == null) {
                zzagf.m4795("Response should not be null.");
            } else {
                zzahn.f4212.post(new zzv(this, send));
            }
        } catch (Exception e) {
            zzagf.m4793("Error converting request to json.", e);
        }
    }
}
