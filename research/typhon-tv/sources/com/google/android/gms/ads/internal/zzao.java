package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzao extends zzbfm {
    public static final Parcelable.Creator<zzao> CREATOR = new zzap();
    public final boolean zzaqo;
    public final boolean zzaqp;
    public final boolean zzaqr;
    public final float zzaqs;
    public final int zzaqt;
    public final boolean zzaqu;
    public final boolean zzaqv;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f3511;

    zzao(boolean z, boolean z2, String str, boolean z3, float f, int i, boolean z4, boolean z5) {
        this.zzaqo = z;
        this.zzaqp = z2;
        this.f3511 = str;
        this.zzaqr = z3;
        this.zzaqs = f;
        this.zzaqt = i;
        this.zzaqu = z4;
        this.zzaqv = z5;
    }

    public zzao(boolean z, boolean z2, boolean z3, float f, int i, boolean z4, boolean z5) {
        this(z, z2, (String) null, z3, f, i, z4, z5);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 2, this.zzaqo);
        zzbfp.m10195(parcel, 3, this.zzaqp);
        zzbfp.m10193(parcel, 4, this.f3511, false);
        zzbfp.m10195(parcel, 5, this.zzaqr);
        zzbfp.m10184(parcel, 6, this.zzaqs);
        zzbfp.m10185(parcel, 7, this.zzaqt);
        zzbfp.m10195(parcel, 8, this.zzaqu);
        zzbfp.m10195(parcel, 9, this.zzaqv);
        zzbfp.m10182(parcel, r0);
    }
}
