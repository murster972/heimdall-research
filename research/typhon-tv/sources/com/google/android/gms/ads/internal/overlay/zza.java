package com.google.android.gms.ads.internal.overlay;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzzv;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

@zzzv
public final class zza {
    public static boolean zza(Context context, zzc zzc, zzq zzq) {
        int i;
        if (zzc == null) {
            zzagf.m4791("No intent data for launcher overlay.");
            return false;
        }
        zznh.m5599(context);
        if (zzc.intent != null) {
            return m4027(context, zzc.intent, zzq);
        }
        Intent intent = new Intent();
        if (TextUtils.isEmpty(zzc.url)) {
            zzagf.m4791("Open GMSG did not contain a URL.");
            return false;
        }
        if (!TextUtils.isEmpty(zzc.mimeType)) {
            intent.setDataAndType(Uri.parse(zzc.url), zzc.mimeType);
        } else {
            intent.setData(Uri.parse(zzc.url));
        }
        intent.setAction("android.intent.action.VIEW");
        if (!TextUtils.isEmpty(zzc.packageName)) {
            intent.setPackage(zzc.packageName);
        }
        if (!TextUtils.isEmpty(zzc.zzchu)) {
            String[] split = zzc.zzchu.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, 2);
            if (split.length < 2) {
                String valueOf = String.valueOf(zzc.zzchu);
                zzagf.m4791(valueOf.length() != 0 ? "Could not parse component name from open GMSG: ".concat(valueOf) : new String("Could not parse component name from open GMSG: "));
                return false;
            }
            intent.setClassName(split[0], split[1]);
        }
        String str = zzc.zzchv;
        if (!TextUtils.isEmpty(str)) {
            try {
                i = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                zzagf.m4791("Could not parse intent flags.");
                i = 0;
            }
            intent.addFlags(i);
        }
        if (((Boolean) zzkb.m5481().m5595(zznh.f5027)).booleanValue()) {
            intent.addFlags(268435456);
            intent.putExtra("android.support.customtabs.extra.user_opt_out", true);
        } else {
            if (((Boolean) zzkb.m5481().m5595(zznh.f5026)).booleanValue()) {
                zzbs.zzei();
                zzahn.m4581(context, intent);
            }
        }
        return m4027(context, intent, zzq);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m4027(Context context, Intent intent, zzq zzq) {
        try {
            String valueOf = String.valueOf(intent.toURI());
            zzagf.m4527(valueOf.length() != 0 ? "Launching an intent: ".concat(valueOf) : new String("Launching an intent: "));
            zzbs.zzei();
            zzahn.m4608(context, intent);
            if (zzq != null) {
                zzq.zzbt();
            }
            return true;
        } catch (ActivityNotFoundException e) {
            zzagf.m4791(e.getMessage());
            return false;
        }
    }
}
