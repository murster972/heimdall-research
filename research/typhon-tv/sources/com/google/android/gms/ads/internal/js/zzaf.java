package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzalk;

final class zzaf implements zzalk<zzc> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzaa f6671;

    zzaf(zzae zzae, zzaa zzaa) {
        this.f6671 = zzaa;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m7638(Object obj) {
        zzagf.m4527("Getting a new session for JS Engine.");
        this.f6671.zzk(((zzc) obj).zzln());
    }
}
