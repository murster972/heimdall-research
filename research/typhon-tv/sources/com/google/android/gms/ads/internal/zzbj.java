package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzjj;
import java.lang.ref.WeakReference;

final class zzbj implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzbi f6768;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ WeakReference f6769;

    zzbj(zzbi zzbi, WeakReference weakReference) {
        this.f6768 = zzbi;
        this.f6769 = weakReference;
    }

    public final void run() {
        boolean unused = this.f6768.f3521 = false;
        zza zza = (zza) this.f6769.get();
        if (zza != null) {
            zzjj r1 = this.f6768.f3522;
            if (zza.靐(r1)) {
                zza.zzb(r1);
                return;
            }
            zzagf.m4794("Ad is not visible. Not refreshing ad.");
            zza.麤.zzg(r1);
        }
    }
}
