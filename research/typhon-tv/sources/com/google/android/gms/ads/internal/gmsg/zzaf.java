package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.internal.zzaeq;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzzv;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.Map;

@zzzv
public final class zzaf implements zzt<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzag f3420;

    public zzaf(zzag zzag) {
        this.f3420 = zzag;
    }

    public final void zza(Object obj, Map<String, String> map) {
        zzaeq zzaeq;
        String str = map.get("action");
        if ("grant".equals(str)) {
            try {
                int parseInt = Integer.parseInt(map.get("amount"));
                String str2 = map.get(VastExtensionXmlManager.TYPE);
                if (!TextUtils.isEmpty(str2)) {
                    zzaeq = new zzaeq(str2, parseInt);
                    this.f3420.zzb(zzaeq);
                }
            } catch (NumberFormatException e) {
                zzagf.m4796("Unable to parse reward amount.", e);
            }
            zzaeq = null;
            this.f3420.zzb(zzaeq);
        } else if ("video_start".equals(str)) {
            this.f3420.zzdl();
        }
    }
}
