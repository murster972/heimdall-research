package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzanh;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

final class zzau implements zzt<zzanh> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ CountDownLatch f6738;

    zzau(CountDownLatch countDownLatch) {
        this.f6738 = countDownLatch;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzagf.m4791("Adapter returned an ad, but assets substitution failed");
        this.f6738.countDown();
        ((zzanh) obj).destroy();
    }
}
