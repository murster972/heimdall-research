package com.google.android.gms.ads.internal.js;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzaiq;
import com.google.android.gms.internal.zzakd;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzzv;

@zzzv
public final class zzn {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzaiq<zzc> f3430;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public zzae f3431;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f3432;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public zzaiq<zzc> f3433;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context f3434;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzakd f3435;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f3436;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Object f3437;

    public zzn(Context context, zzakd zzakd, String str) {
        this.f3437 = new Object();
        this.f3432 = 1;
        this.f3436 = str;
        this.f3434 = context.getApplicationContext();
        this.f3435 = zzakd;
        this.f3433 = new zzz();
        this.f3430 = new zzz();
    }

    public zzn(Context context, zzakd zzakd, String str, zzaiq<zzc> zzaiq, zzaiq<zzc> zzaiq2) {
        this(context, zzakd, str);
        this.f3433 = zzaiq;
        this.f3430 = zzaiq2;
    }

    public final zzaa zzb(zzcv zzcv) {
        zzaa zzaa;
        synchronized (this.f3437) {
            if (this.f3431 == null || this.f3431.getStatus() == -1) {
                this.f3432 = 2;
                this.f3431 = m4026((zzcv) null);
                zzaa = this.f3431.zzlp();
            } else if (this.f3432 == 0) {
                zzaa = this.f3431.zzlp();
            } else if (this.f3432 == 1) {
                this.f3432 = 2;
                m4026((zzcv) null);
                zzaa = this.f3431.zzlp();
            } else {
                zzaa = this.f3432 == 2 ? this.f3431.zzlp() : this.f3431.zzlp();
            }
        }
        return zzaa;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final zzae m4026(zzcv zzcv) {
        zzae zzae = new zzae(this.f3430);
        zzbs.zzei();
        zzahn.m4612((Runnable) new zzo(this, zzcv, zzae));
        zzae.zza(new zzw(this, zzae), new zzx(this, zzae));
        return zzae;
    }
}
