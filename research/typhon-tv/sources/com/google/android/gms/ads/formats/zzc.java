package com.google.android.gms.ads.formats;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzc implements Parcelable.Creator<PublisherAdViewOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r2 = zzbfn.m10169(parcel);
        boolean z = false;
        IBinder iBinder = null;
        while (parcel.dataPosition() < r2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 2:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r2);
        return new PublisherAdViewOptions(z, iBinder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new PublisherAdViewOptions[i];
    }
}
