package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzanh;
import java.util.Map;

final class zzs implements zzt<zzanh> {
    zzs() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzanh zzanh = (zzanh) obj;
        if (map.keySet().contains(TtmlNode.START)) {
            zzanh.m4996(true);
        }
        if (map.keySet().contains("stop")) {
            zzanh.m4996(false);
        }
    }
}
