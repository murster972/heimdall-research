package com.google.android.gms.ads.internal.js;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.common.util.zzr;

final /* synthetic */ class zzf implements zzr {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzt f6676;

    zzf(zzt zzt) {
        this.f6676 = zzt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m7641(Object obj) {
        zzt zzt = (zzt) obj;
        return (zzt instanceof zzl) && ((zzl) zzt).f6688.equals(this.f6676);
    }
}
