package com.google.android.gms.ads.internal.js;

import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.internal.zzagf;
import com.google.android.gms.internal.zzahn;
import com.google.android.gms.internal.zzajj;
import com.google.android.gms.internal.zzcv;

final class zzo implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    final /* synthetic */ zzae f6689;

    /* renamed from: 齉  reason: contains not printable characters */
    final /* synthetic */ zzn f6690;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzcv f6691;

    zzo(zzn zzn, zzcv zzcv, zzae zzae) {
        this.f6690 = zzn;
        this.f6691 = zzcv;
        this.f6689 = zzae;
    }

    public final void run() {
        try {
            zze zze = new zze(this.f6690.f3434, this.f6690.f3435, this.f6691, (zzv) null);
            zze.zza(new zzp(this, zze));
            zze.zza("/jsLoaded", new zzs(this, zze));
            zzajj zzajj = new zzajj();
            zzt zzt = new zzt(this, zze, zzajj);
            zzajj.m4726(zzt);
            zze.zza("/requestReload", zzt);
            if (this.f6690.f3436.endsWith(".js")) {
                zze.zzbb(this.f6690.f3436);
            } else if (this.f6690.f3436.startsWith("<html>")) {
                zze.zzbd(this.f6690.f3436);
            } else {
                zze.zzbc(this.f6690.f3436);
            }
            zzahn.f4212.postDelayed(new zzu(this, zze), (long) zzy.f6709);
        } catch (Throwable th) {
            zzagf.m4793("Error creating webview.", th);
            zzbs.zzem().m4505(th, "SdkJavascriptFactory.loadJavascriptEngine");
            this.f6689.reject();
        }
    }
}
