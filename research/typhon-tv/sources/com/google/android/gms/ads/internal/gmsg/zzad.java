package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzzv;
import java.util.HashMap;
import java.util.Map;

@zzzv
public final class zzad implements zzt<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, zzae> f3418 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f3419 = new Object();

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.Object r7, java.util.Map<java.lang.String, java.lang.String> r8) {
        /*
            r6 = this;
            java.lang.String r0 = "id"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "fail"
            java.lang.Object r1 = r8.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "fail_reason"
            java.lang.Object r2 = r8.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "result"
            java.lang.Object r3 = r8.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r5 = r6.f3419
            monitor-enter(r5)
            java.util.Map<java.lang.String, com.google.android.gms.ads.internal.gmsg.zzae> r4 = r6.f3418     // Catch:{ all -> 0x004d }
            java.lang.Object r4 = r4.remove(r0)     // Catch:{ all -> 0x004d }
            com.google.android.gms.ads.internal.gmsg.zzae r4 = (com.google.android.gms.ads.internal.gmsg.zzae) r4     // Catch:{ all -> 0x004d }
            if (r4 != 0) goto L_0x0050
            java.lang.String r1 = "Received result for unexpected method invocation: "
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x004d }
            int r2 = r0.length()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x0047
            java.lang.String r0 = r1.concat(r0)     // Catch:{ all -> 0x004d }
        L_0x0042:
            com.google.android.gms.internal.zzagf.m4791(r0)     // Catch:{ all -> 0x004d }
            monitor-exit(r5)     // Catch:{ all -> 0x004d }
        L_0x0046:
            return
        L_0x0047:
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x004d }
            r0.<init>(r1)     // Catch:{ all -> 0x004d }
            goto L_0x0042
        L_0x004d:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x004d }
            throw r0
        L_0x0050:
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x004d }
            if (r0 != 0) goto L_0x005b
            r4.zzau(r2)     // Catch:{ all -> 0x004d }
            monitor-exit(r5)     // Catch:{ all -> 0x004d }
            goto L_0x0046
        L_0x005b:
            if (r3 != 0) goto L_0x0063
            r0 = 0
            r4.zze(r0)     // Catch:{ all -> 0x004d }
            monitor-exit(r5)     // Catch:{ all -> 0x004d }
            goto L_0x0046
        L_0x0063:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0092 }
            r1.<init>((java.lang.String) r3)     // Catch:{ JSONException -> 0x0092 }
            boolean r0 = com.google.android.gms.internal.zzagf.m4528()     // Catch:{ JSONException -> 0x0092 }
            if (r0 == 0) goto L_0x0087
            java.lang.String r2 = "Result GMSG: "
            r0 = 2
            java.lang.String r0 = r1.toString(r0)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ JSONException -> 0x0092 }
            int r3 = r0.length()     // Catch:{ JSONException -> 0x0092 }
            if (r3 == 0) goto L_0x008c
            java.lang.String r0 = r2.concat(r0)     // Catch:{ JSONException -> 0x0092 }
        L_0x0084:
            com.google.android.gms.internal.zzagf.m4527(r0)     // Catch:{ JSONException -> 0x0092 }
        L_0x0087:
            r4.zze(r1)     // Catch:{ JSONException -> 0x0092 }
        L_0x008a:
            monitor-exit(r5)     // Catch:{ all -> 0x004d }
            goto L_0x0046
        L_0x008c:
            java.lang.String r0 = new java.lang.String     // Catch:{ JSONException -> 0x0092 }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x0092 }
            goto L_0x0084
        L_0x0092:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x004d }
            r4.zzau(r0)     // Catch:{ all -> 0x004d }
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.zzad.zza(java.lang.Object, java.util.Map):void");
    }

    public final void zza(String str, zzae zzae) {
        synchronized (this.f3419) {
            this.f3418.put(str, zzae);
        }
    }
}
