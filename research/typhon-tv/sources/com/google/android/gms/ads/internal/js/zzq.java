package com.google.android.gms.ads.internal.js;

final class zzq implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzp f6694;

    zzq(zzp zzp) {
        this.f6694 = zzp;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            com.google.android.gms.ads.internal.js.zzp r0 = r3.f6694
            com.google.android.gms.ads.internal.js.zzo r0 = r0.f6692
            com.google.android.gms.ads.internal.js.zzn r0 = r0.f6690
            java.lang.Object r1 = r0.f3437
            monitor-enter(r1)
            com.google.android.gms.ads.internal.js.zzp r0 = r3.f6694     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzo r0 = r0.f6692     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x0043 }
            int r0 = r0.getStatus()     // Catch:{ all -> 0x0043 }
            r2 = -1
            if (r0 == r2) goto L_0x0025
            com.google.android.gms.ads.internal.js.zzp r0 = r3.f6694     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzo r0 = r0.f6692     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x0043 }
            int r0 = r0.getStatus()     // Catch:{ all -> 0x0043 }
            r2 = 1
            if (r0 != r2) goto L_0x0027
        L_0x0025:
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
        L_0x0026:
            return
        L_0x0027:
            com.google.android.gms.ads.internal.js.zzp r0 = r3.f6694     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzo r0 = r0.f6692     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzae r0 = r0.f6689     // Catch:{ all -> 0x0043 }
            r0.reject()     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzr r0 = new com.google.android.gms.ads.internal.js.zzr     // Catch:{ all -> 0x0043 }
            r0.<init>(r3)     // Catch:{ all -> 0x0043 }
            com.google.android.gms.internal.zzahn.m4612((java.lang.Runnable) r0)     // Catch:{ all -> 0x0043 }
            java.lang.String r0 = "Could not receive loaded message in a timely manner. Rejecting."
            com.google.android.gms.internal.zzagf.m4527(r0)     // Catch:{ all -> 0x0043 }
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            goto L_0x0026
        L_0x0043:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.js.zzq.run():void");
    }
}
