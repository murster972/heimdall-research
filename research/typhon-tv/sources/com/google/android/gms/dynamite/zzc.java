package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class zzc implements DynamiteModule.zzd {
    zzc() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzj m9331(Context context, String str, zzi zzi) throws DynamiteModule.zzc {
        zzj zzj = new zzj();
        zzj.f7983 = zzi.m9336(context, str);
        if (zzj.f7983 != 0) {
            zzj.f7982 = -1;
        } else {
            zzj.f7981 = zzi.m9337(context, str, true);
            if (zzj.f7981 != 0) {
                zzj.f7982 = 1;
            }
        }
        return zzj;
    }
}
