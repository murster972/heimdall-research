package com.google.android.gms.dynamite;

import com.evernote.android.state.StateSaver;
import dalvik.system.PathClassLoader;

final class zzh extends PathClassLoader {
    zzh(String str, ClassLoader classLoader) {
        super(str, classLoader);
    }

    /* access modifiers changed from: protected */
    public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
        if (!str.startsWith(StateSaver.JAVA_PREFIX) && !str.startsWith(StateSaver.ANDROID_PREFIX)) {
            try {
                return findClass(str);
            } catch (ClassNotFoundException e) {
            }
        }
        return super.loadClass(str, z);
    }
}
