package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class zza implements zzi {
    zza() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9328(Context context, String str) {
        return DynamiteModule.m9316(context, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9329(Context context, String str, boolean z) throws DynamiteModule.zzc {
        return DynamiteModule.m9317(context, str, z);
    }
}
