package com.google.android.gms.dynamite;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzk extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    int m9338(IObjectWrapper iObjectWrapper, String str, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m9339(IObjectWrapper iObjectWrapper, String str, int i) throws RemoteException;
}
