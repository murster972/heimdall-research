package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzl extends zzeu implements zzk {
    zzl(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9340(IObjectWrapper iObjectWrapper, String str, boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeString(str);
        zzew.m12307(v_, z);
        Parcel r0 = m12300(3, v_);
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m9341(IObjectWrapper iObjectWrapper, String str, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        v_.writeString(str);
        v_.writeInt(i);
        Parcel r0 = m12300(2, v_);
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
