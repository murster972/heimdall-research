package com.google.android.gms;

public final class R {

    public static final class array {
        public static final int cast_expanded_controller_default_control_buttons = 2130903041;
        public static final int cast_mini_controller_default_control_buttons = 2130903043;
    }

    public static final class attr {
        public static final int adSize = 2130968610;
        public static final int adSizes = 2130968611;
        public static final int adUnitId = 2130968612;
        public static final int buttonSize = 2130968696;
        public static final int castBackground = 2130968707;
        public static final int castBackgroundColor = 2130968708;
        public static final int castButtonBackgroundColor = 2130968709;
        public static final int castButtonColor = 2130968710;
        public static final int castButtonText = 2130968711;
        public static final int castButtonTextAppearance = 2130968712;
        public static final int castClosedCaptionsButtonDrawable = 2130968713;
        public static final int castControlButtons = 2130968714;
        public static final int castExpandedControllerStyle = 2130968715;
        public static final int castExpandedControllerToolbarStyle = 2130968716;
        public static final int castFocusRadius = 2130968717;
        public static final int castForward30ButtonDrawable = 2130968718;
        public static final int castIntroOverlayStyle = 2130968719;
        public static final int castLargePauseButtonDrawable = 2130968720;
        public static final int castLargePlayButtonDrawable = 2130968721;
        public static final int castLargeStopButtonDrawable = 2130968722;
        public static final int castMiniControllerStyle = 2130968723;
        public static final int castMuteToggleButtonDrawable = 2130968724;
        public static final int castPauseButtonDrawable = 2130968725;
        public static final int castPlayButtonDrawable = 2130968726;
        public static final int castProgressBarColor = 2130968727;
        public static final int castRewind30ButtonDrawable = 2130968728;
        public static final int castSeekBarProgressDrawable = 2130968729;
        public static final int castSeekBarThumbDrawable = 2130968730;
        public static final int castShowImageThumbnail = 2130968731;
        public static final int castSkipNextButtonDrawable = 2130968732;
        public static final int castSkipPreviousButtonDrawable = 2130968733;
        public static final int castStopButtonDrawable = 2130968734;
        public static final int castSubtitleTextAppearance = 2130968735;
        public static final int castTitleTextAppearance = 2130968736;
        public static final int circleCrop = 2130968741;
        public static final int colorScheme = 2130968758;
        public static final int imageAspectRatio = 2130968846;
        public static final int imageAspectRatioAdjust = 2130968847;
        public static final int scopeUris = 2130969018;
    }

    public static final class color {
        public static final int cast_expanded_controller_ad_container_white_stripe_color = 2131099700;
        public static final int cast_expanded_controller_ad_label_background_color = 2131099701;
        public static final int cast_expanded_controller_background_color = 2131099702;
        public static final int cast_expanded_controller_progress_text_color = 2131099703;
        public static final int cast_expanded_controller_seek_bar_progress_background_tint_color = 2131099704;
        public static final int cast_expanded_controller_text_color = 2131099705;
        public static final int cast_intro_overlay_background_color = 2131099706;
        public static final int cast_intro_overlay_button_background_color = 2131099707;
        public static final int cast_libraries_material_featurehighlight_outer_highlight_default_color = 2131099708;
        public static final int cast_libraries_material_featurehighlight_text_body_color = 2131099709;
        public static final int cast_libraries_material_featurehighlight_text_header_color = 2131099710;
        public static final int common_google_signin_btn_text_dark = 2131099717;
        public static final int common_google_signin_btn_text_dark_default = 2131099718;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099719;
        public static final int common_google_signin_btn_text_dark_focused = 2131099720;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099721;
        public static final int common_google_signin_btn_text_light = 2131099722;
        public static final int common_google_signin_btn_text_light_default = 2131099723;
        public static final int common_google_signin_btn_text_light_disabled = 2131099724;
        public static final int common_google_signin_btn_text_light_focused = 2131099725;
        public static final int common_google_signin_btn_text_light_pressed = 2131099726;
        public static final int common_google_signin_btn_tint = 2131099727;
    }

    public static final class dimen {
        public static final int cast_expanded_controller_ad_background_layout_height = 2131165273;
        public static final int cast_expanded_controller_ad_background_layout_width = 2131165274;
        public static final int cast_expanded_controller_ad_layout_height = 2131165275;
        public static final int cast_expanded_controller_ad_layout_width = 2131165276;
        public static final int cast_expanded_controller_control_button_margin = 2131165277;
        public static final int cast_expanded_controller_control_toolbar_min_height = 2131165278;
        public static final int cast_expanded_controller_margin_between_seek_bar_and_control_buttons = 2131165279;
        public static final int cast_expanded_controller_margin_between_status_text_and_seek_bar = 2131165280;
        public static final int cast_expanded_controller_seekbar_disabled_alpha = 2131165281;
        public static final int cast_intro_overlay_button_margin_bottom = 2131165282;
        public static final int cast_intro_overlay_focus_radius = 2131165283;
        public static final int cast_intro_overlay_title_margin_top = 2131165284;
        public static final int cast_libraries_material_featurehighlight_center_horizontal_offset = 2131165285;
        public static final int cast_libraries_material_featurehighlight_center_threshold = 2131165286;
        public static final int cast_libraries_material_featurehighlight_inner_margin = 2131165287;
        public static final int cast_libraries_material_featurehighlight_inner_radius = 2131165288;
        public static final int cast_libraries_material_featurehighlight_outer_padding = 2131165289;
        public static final int cast_libraries_material_featurehighlight_text_body_size = 2131165290;
        public static final int cast_libraries_material_featurehighlight_text_header_size = 2131165291;
        public static final int cast_libraries_material_featurehighlight_text_horizontal_margin = 2131165292;
        public static final int cast_libraries_material_featurehighlight_text_horizontal_offset = 2131165293;
        public static final int cast_libraries_material_featurehighlight_text_max_width = 2131165294;
        public static final int cast_libraries_material_featurehighlight_text_vertical_space = 2131165295;
        public static final int cast_mini_controller_control_button_margin = 2131165296;
        public static final int cast_mini_controller_icon_height = 2131165297;
        public static final int cast_mini_controller_icon_width = 2131165298;
        public static final int cast_notification_image_size = 2131165299;
        public static final int cast_tracks_chooser_dialog_no_message_text_size = 2131165300;
        public static final int cast_tracks_chooser_dialog_row_text_size = 2131165301;
    }

    public static final class drawable {
        public static final int cast_abc_scrubber_control_off_mtrl_alpha = 2131230861;
        public static final int cast_abc_scrubber_control_to_pressed_mtrl_000 = 2131230862;
        public static final int cast_abc_scrubber_control_to_pressed_mtrl_005 = 2131230863;
        public static final int cast_abc_scrubber_primary_mtrl_alpha = 2131230864;
        public static final int cast_album_art_placeholder = 2131230865;
        public static final int cast_album_art_placeholder_large = 2131230866;
        public static final int cast_expanded_controller_actionbar_bg_gradient_light = 2131230867;
        public static final int cast_expanded_controller_bg_gradient_light = 2131230868;
        public static final int cast_expanded_controller_seekbar_thumb = 2131230869;
        public static final int cast_expanded_controller_seekbar_track = 2131230870;
        public static final int cast_ic_expanded_controller_closed_caption = 2131230871;
        public static final int cast_ic_expanded_controller_forward30 = 2131230872;
        public static final int cast_ic_expanded_controller_mute = 2131230873;
        public static final int cast_ic_expanded_controller_pause = 2131230874;
        public static final int cast_ic_expanded_controller_play = 2131230875;
        public static final int cast_ic_expanded_controller_rewind30 = 2131230876;
        public static final int cast_ic_expanded_controller_skip_next = 2131230877;
        public static final int cast_ic_expanded_controller_skip_previous = 2131230878;
        public static final int cast_ic_expanded_controller_stop = 2131230879;
        public static final int cast_ic_mini_controller_closed_caption = 2131230880;
        public static final int cast_ic_mini_controller_forward30 = 2131230881;
        public static final int cast_ic_mini_controller_mute = 2131230882;
        public static final int cast_ic_mini_controller_pause = 2131230883;
        public static final int cast_ic_mini_controller_pause_large = 2131230884;
        public static final int cast_ic_mini_controller_play = 2131230885;
        public static final int cast_ic_mini_controller_play_large = 2131230886;
        public static final int cast_ic_mini_controller_rewind30 = 2131230887;
        public static final int cast_ic_mini_controller_skip_next = 2131230888;
        public static final int cast_ic_mini_controller_skip_prev = 2131230889;
        public static final int cast_ic_mini_controller_stop = 2131230890;
        public static final int cast_ic_mini_controller_stop_large = 2131230891;
        public static final int cast_ic_notification_0 = 2131230892;
        public static final int cast_ic_notification_1 = 2131230893;
        public static final int cast_ic_notification_2 = 2131230894;
        public static final int cast_ic_notification_connecting = 2131230895;
        public static final int cast_ic_notification_disconnect = 2131230896;
        public static final int cast_ic_notification_forward = 2131230897;
        public static final int cast_ic_notification_forward10 = 2131230898;
        public static final int cast_ic_notification_forward30 = 2131230899;
        public static final int cast_ic_notification_on = 2131230900;
        public static final int cast_ic_notification_pause = 2131230901;
        public static final int cast_ic_notification_play = 2131230902;
        public static final int cast_ic_notification_rewind = 2131230903;
        public static final int cast_ic_notification_rewind10 = 2131230904;
        public static final int cast_ic_notification_rewind30 = 2131230905;
        public static final int cast_ic_notification_skip_next = 2131230906;
        public static final int cast_ic_notification_skip_prev = 2131230907;
        public static final int cast_ic_notification_small_icon = 2131230908;
        public static final int cast_ic_notification_stop_live_stream = 2131230909;
        public static final int cast_ic_stop_circle_filled_grey600 = 2131230910;
        public static final int cast_ic_stop_circle_filled_white = 2131230911;
        public static final int cast_mini_controller_gradient_light = 2131230912;
        public static final int cast_mini_controller_progress_drawable = 2131230913;
        public static final int cast_skip_ad_label_border = 2131230914;
        public static final int common_full_open_on_phone = 2131230915;
        public static final int common_google_signin_btn_icon_dark = 2131230916;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230917;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230918;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230919;
        public static final int common_google_signin_btn_icon_disabled = 2131230920;
        public static final int common_google_signin_btn_icon_light = 2131230921;
        public static final int common_google_signin_btn_icon_light_focused = 2131230922;
        public static final int common_google_signin_btn_icon_light_normal = 2131230923;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230924;
        public static final int common_google_signin_btn_text_dark = 2131230925;
        public static final int common_google_signin_btn_text_dark_focused = 2131230926;
        public static final int common_google_signin_btn_text_dark_normal = 2131230927;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230928;
        public static final int common_google_signin_btn_text_disabled = 2131230929;
        public static final int common_google_signin_btn_text_light = 2131230930;
        public static final int common_google_signin_btn_text_light_focused = 2131230931;
        public static final int common_google_signin_btn_text_light_normal = 2131230932;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230933;
        public static final int googleg_disabled_color_18 = 2131230951;
        public static final int googleg_standard_color_18 = 2131230952;
        public static final int quantum_ic_art_track_grey600_48 = 2131231293;
        public static final int quantum_ic_bigtop_updates_white_24 = 2131231294;
        public static final int quantum_ic_cast_connected_white_24 = 2131231295;
        public static final int quantum_ic_cast_white_36 = 2131231296;
        public static final int quantum_ic_clear_white_24 = 2131231297;
        public static final int quantum_ic_closed_caption_grey600_36 = 2131231298;
        public static final int quantum_ic_closed_caption_white_36 = 2131231299;
        public static final int quantum_ic_forward_10_white_24 = 2131231300;
        public static final int quantum_ic_forward_30_grey600_36 = 2131231301;
        public static final int quantum_ic_forward_30_white_24 = 2131231302;
        public static final int quantum_ic_forward_30_white_36 = 2131231303;
        public static final int quantum_ic_keyboard_arrow_down_white_36 = 2131231304;
        public static final int quantum_ic_pause_circle_filled_grey600_36 = 2131231305;
        public static final int quantum_ic_pause_circle_filled_white_36 = 2131231306;
        public static final int quantum_ic_pause_grey600_36 = 2131231307;
        public static final int quantum_ic_pause_grey600_48 = 2131231308;
        public static final int quantum_ic_pause_white_24 = 2131231309;
        public static final int quantum_ic_play_arrow_grey600_36 = 2131231310;
        public static final int quantum_ic_play_arrow_grey600_48 = 2131231311;
        public static final int quantum_ic_play_arrow_white_24 = 2131231312;
        public static final int quantum_ic_play_circle_filled_grey600_36 = 2131231313;
        public static final int quantum_ic_play_circle_filled_white_36 = 2131231314;
        public static final int quantum_ic_refresh_white_24 = 2131231315;
        public static final int quantum_ic_replay_10_white_24 = 2131231316;
        public static final int quantum_ic_replay_30_grey600_36 = 2131231317;
        public static final int quantum_ic_replay_30_white_24 = 2131231318;
        public static final int quantum_ic_replay_30_white_36 = 2131231319;
        public static final int quantum_ic_replay_white_24 = 2131231320;
        public static final int quantum_ic_skip_next_grey600_36 = 2131231321;
        public static final int quantum_ic_skip_next_white_24 = 2131231322;
        public static final int quantum_ic_skip_next_white_36 = 2131231323;
        public static final int quantum_ic_skip_previous_grey600_36 = 2131231324;
        public static final int quantum_ic_skip_previous_white_24 = 2131231325;
        public static final int quantum_ic_skip_previous_white_36 = 2131231326;
        public static final int quantum_ic_stop_grey600_36 = 2131231327;
        public static final int quantum_ic_stop_grey600_48 = 2131231328;
        public static final int quantum_ic_stop_white_24 = 2131231329;
        public static final int quantum_ic_volume_off_grey600_36 = 2131231330;
        public static final int quantum_ic_volume_off_white_36 = 2131231331;
        public static final int quantum_ic_volume_up_grey600_36 = 2131231332;
        public static final int quantum_ic_volume_up_white_36 = 2131231333;
    }

    public static final class id {
        public static final int ad_container = 2131296328;
        public static final int ad_image_view = 2131296329;
        public static final int ad_in_progress_label = 2131296330;
        public static final int ad_label = 2131296331;
        public static final int adjust_height = 2131296333;
        public static final int adjust_width = 2131296334;
        public static final int audio_list_view = 2131296341;
        public static final int auto = 2131296342;
        public static final int background_image_view = 2131296365;
        public static final int background_place_holder_image_view = 2131296366;
        public static final int blurred_background_image_view = 2131296370;
        public static final int button = 2131296375;
        public static final int button_0 = 2131296377;
        public static final int button_1 = 2131296378;
        public static final int button_2 = 2131296379;
        public static final int button_3 = 2131296380;
        public static final int button_play_pause_toggle = 2131296381;
        public static final int cast_button_type_closed_caption = 2131296386;
        public static final int cast_button_type_custom = 2131296387;
        public static final int cast_button_type_empty = 2131296388;
        public static final int cast_button_type_forward_30_seconds = 2131296389;
        public static final int cast_button_type_mute_toggle = 2131296390;
        public static final int cast_button_type_play_pause_toggle = 2131296391;
        public static final int cast_button_type_rewind_30_seconds = 2131296392;
        public static final int cast_button_type_skip_next = 2131296393;
        public static final int cast_button_type_skip_previous = 2131296394;
        public static final int cast_featurehighlight_help_text_body_view = 2131296395;
        public static final int cast_featurehighlight_help_text_header_view = 2131296396;
        public static final int cast_featurehighlight_view = 2131296397;
        public static final int cast_notification_id = 2131296398;
        public static final int center = 2131296399;
        public static final int container_all = 2131296423;
        public static final int container_current = 2131296424;
        public static final int controllers = 2131296426;
        public static final int dark = 2131296438;
        public static final int end_text = 2131296460;
        public static final int expanded_controller_layout = 2131296485;
        public static final int icon_only = 2131296507;
        public static final int icon_view = 2131296508;
        public static final int light = 2131296556;
        public static final int live_stream_indicator = 2131296563;
        public static final int live_stream_seek_bar = 2131296564;
        public static final int loading_indicator = 2131296579;
        public static final int none = 2131296761;
        public static final int normal = 2131296762;
        public static final int progressBar = 2131296789;
        public static final int radio = 2131296794;
        public static final int seek_bar = 2131296864;
        public static final int seek_bar_controls = 2131296865;
        public static final int standard = 2131296882;
        public static final int start_text = 2131296884;
        public static final int status_text = 2131296886;
        public static final int subtitle_view = 2131296889;
        public static final int tab_host = 2131296893;
        public static final int text = 2131296900;
        public static final int text2 = 2131296901;
        public static final int textTitle = 2131296904;
        public static final int text_list_view = 2131296906;
        public static final int title_view = 2131296916;
        public static final int toolbar = 2131296917;
        public static final int wide = 2131296984;
        public static final int wrap_content = 2131296987;
    }

    public static final class integer {
        public static final int cast_libraries_material_featurehighlight_pulse_base_alpha = 2131361798;
        public static final int google_play_services_version = 2131361802;
    }

    public static final class layout {
        public static final int cast_expanded_controller_activity = 2131492921;
        public static final int cast_help_text = 2131492923;
        public static final int cast_intro_overlay = 2131492924;
        public static final int cast_mini_controller = 2131492925;
        public static final int cast_tracks_chooser_dialog_layout = 2131492927;
        public static final int cast_tracks_chooser_dialog_row_layout = 2131492928;
    }

    public static final class string {
        public static final int cast_ad_label = 2131820639;
        public static final int cast_casting_to_device = 2131820640;
        public static final int cast_closed_captions = 2131820641;
        public static final int cast_closed_captions_unavailable = 2131820642;
        public static final int cast_connecting_to_device = 2131820643;
        public static final int cast_disconnect = 2131820644;
        public static final int cast_expanded_controller_ad_image_description = 2131820645;
        public static final int cast_expanded_controller_ad_in_progress = 2131820646;
        public static final int cast_expanded_controller_background_image = 2131820647;
        public static final int cast_expanded_controller_live_stream_indicator = 2131820648;
        public static final int cast_expanded_controller_loading = 2131820649;
        public static final int cast_expanded_controller_skip_ad_label = 2131820650;
        public static final int cast_forward = 2131820651;
        public static final int cast_forward_10 = 2131820652;
        public static final int cast_forward_30 = 2131820653;
        public static final int cast_intro_overlay_button_text = 2131820654;
        public static final int cast_invalid_stream_duration_text = 2131820655;
        public static final int cast_invalid_stream_position_text = 2131820656;
        public static final int cast_mute = 2131820657;
        public static final int cast_notification_connected_message = 2131820658;
        public static final int cast_notification_connecting_message = 2131820659;
        public static final int cast_notification_default_channel_name = 2131820660;
        public static final int cast_notification_disconnect = 2131820661;
        public static final int cast_pause = 2131820662;
        public static final int cast_play = 2131820663;
        public static final int cast_rewind = 2131820664;
        public static final int cast_rewind_10 = 2131820665;
        public static final int cast_rewind_30 = 2131820666;
        public static final int cast_seek_bar = 2131820667;
        public static final int cast_skip_next = 2131820668;
        public static final int cast_skip_prev = 2131820669;
        public static final int cast_stop = 2131820670;
        public static final int cast_stop_live_stream = 2131820671;
        public static final int cast_tracks_chooser_dialog_audio = 2131820672;
        public static final int cast_tracks_chooser_dialog_cancel = 2131820673;
        public static final int cast_tracks_chooser_dialog_closed_captions = 2131820674;
        public static final int cast_tracks_chooser_dialog_default_track_name = 2131820675;
        public static final int cast_tracks_chooser_dialog_none = 2131820676;
        public static final int cast_tracks_chooser_dialog_ok = 2131820677;
        public static final int cast_tracks_chooser_dialog_subtitles = 2131820678;
        public static final int cast_unmute = 2131820679;
        public static final int common_google_play_services_enable_button = 2131820702;
        public static final int common_google_play_services_enable_text = 2131820703;
        public static final int common_google_play_services_enable_title = 2131820704;
        public static final int common_google_play_services_install_button = 2131820705;
        public static final int common_google_play_services_install_text = 2131820706;
        public static final int common_google_play_services_install_title = 2131820707;
        public static final int common_google_play_services_notification_channel_name = 2131820708;
        public static final int common_google_play_services_notification_ticker = 2131820709;
        public static final int common_google_play_services_unknown_issue = 2131820710;
        public static final int common_google_play_services_unsupported_text = 2131820711;
        public static final int common_google_play_services_update_button = 2131820712;
        public static final int common_google_play_services_update_text = 2131820713;
        public static final int common_google_play_services_update_title = 2131820714;
        public static final int common_google_play_services_updating_text = 2131820715;
        public static final int common_google_play_services_wear_update_text = 2131820716;
        public static final int common_open_on_phone = 2131820717;
        public static final int common_signin_button_text = 2131820718;
        public static final int common_signin_button_text_long = 2131820719;
        public static final int fcm_fallback_notification_channel_label = 2131820790;
        public static final int gcm_fallback_notification_channel_label = 2131820799;
        public static final int s1 = 2131821073;
        public static final int s2 = 2131821074;
        public static final int s3 = 2131821075;
        public static final int s4 = 2131821076;
        public static final int s5 = 2131821077;
        public static final int s6 = 2131821078;
        public static final int s7 = 2131821079;
    }

    public static final class style {
        public static final int CastExpandedController = 2131886253;
        public static final int CastIntroOverlay = 2131886254;
        public static final int CastMiniController = 2131886255;
        public static final int CustomCastTheme = 2131886257;
        public static final int TextAppearance_CastIntroOverlay_Button = 2131886357;
        public static final int TextAppearance_CastIntroOverlay_Title = 2131886358;
        public static final int TextAppearance_CastMiniController_Subtitle = 2131886359;
        public static final int TextAppearance_CastMiniController_Title = 2131886360;
        public static final int Theme_IAPTheme = 2131886411;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.typhoon.tv.R.attr.adSize, com.typhoon.tv.R.attr.adSizes, com.typhoon.tv.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] CastExpandedController = {com.typhoon.tv.R.attr.castButtonColor, com.typhoon.tv.R.attr.castClosedCaptionsButtonDrawable, com.typhoon.tv.R.attr.castControlButtons, com.typhoon.tv.R.attr.castForward30ButtonDrawable, com.typhoon.tv.R.attr.castMuteToggleButtonDrawable, com.typhoon.tv.R.attr.castPauseButtonDrawable, com.typhoon.tv.R.attr.castPlayButtonDrawable, com.typhoon.tv.R.attr.castRewind30ButtonDrawable, com.typhoon.tv.R.attr.castSeekBarProgressDrawable, com.typhoon.tv.R.attr.castSeekBarThumbDrawable, com.typhoon.tv.R.attr.castSkipNextButtonDrawable, com.typhoon.tv.R.attr.castSkipPreviousButtonDrawable, com.typhoon.tv.R.attr.castStopButtonDrawable};
        public static final int CastExpandedController_castButtonColor = 0;
        public static final int CastExpandedController_castClosedCaptionsButtonDrawable = 1;
        public static final int CastExpandedController_castControlButtons = 2;
        public static final int CastExpandedController_castForward30ButtonDrawable = 3;
        public static final int CastExpandedController_castMuteToggleButtonDrawable = 4;
        public static final int CastExpandedController_castPauseButtonDrawable = 5;
        public static final int CastExpandedController_castPlayButtonDrawable = 6;
        public static final int CastExpandedController_castRewind30ButtonDrawable = 7;
        public static final int CastExpandedController_castSeekBarProgressDrawable = 8;
        public static final int CastExpandedController_castSeekBarThumbDrawable = 9;
        public static final int CastExpandedController_castSkipNextButtonDrawable = 10;
        public static final int CastExpandedController_castSkipPreviousButtonDrawable = 11;
        public static final int CastExpandedController_castStopButtonDrawable = 12;
        public static final int[] CastIntroOverlay = {com.typhoon.tv.R.attr.castBackgroundColor, com.typhoon.tv.R.attr.castButtonBackgroundColor, com.typhoon.tv.R.attr.castButtonText, com.typhoon.tv.R.attr.castButtonTextAppearance, com.typhoon.tv.R.attr.castFocusRadius, com.typhoon.tv.R.attr.castTitleTextAppearance};
        public static final int CastIntroOverlay_castBackgroundColor = 0;
        public static final int CastIntroOverlay_castButtonBackgroundColor = 1;
        public static final int CastIntroOverlay_castButtonText = 2;
        public static final int CastIntroOverlay_castButtonTextAppearance = 3;
        public static final int CastIntroOverlay_castFocusRadius = 4;
        public static final int CastIntroOverlay_castTitleTextAppearance = 5;
        public static final int[] CastMiniController = {com.typhoon.tv.R.attr.castBackground, com.typhoon.tv.R.attr.castButtonColor, com.typhoon.tv.R.attr.castClosedCaptionsButtonDrawable, com.typhoon.tv.R.attr.castControlButtons, com.typhoon.tv.R.attr.castForward30ButtonDrawable, com.typhoon.tv.R.attr.castLargePauseButtonDrawable, com.typhoon.tv.R.attr.castLargePlayButtonDrawable, com.typhoon.tv.R.attr.castLargeStopButtonDrawable, com.typhoon.tv.R.attr.castMuteToggleButtonDrawable, com.typhoon.tv.R.attr.castPauseButtonDrawable, com.typhoon.tv.R.attr.castPlayButtonDrawable, com.typhoon.tv.R.attr.castProgressBarColor, com.typhoon.tv.R.attr.castRewind30ButtonDrawable, com.typhoon.tv.R.attr.castShowImageThumbnail, com.typhoon.tv.R.attr.castSkipNextButtonDrawable, com.typhoon.tv.R.attr.castSkipPreviousButtonDrawable, com.typhoon.tv.R.attr.castStopButtonDrawable, com.typhoon.tv.R.attr.castSubtitleTextAppearance, com.typhoon.tv.R.attr.castTitleTextAppearance};
        public static final int CastMiniController_castBackground = 0;
        public static final int CastMiniController_castButtonColor = 1;
        public static final int CastMiniController_castClosedCaptionsButtonDrawable = 2;
        public static final int CastMiniController_castControlButtons = 3;
        public static final int CastMiniController_castForward30ButtonDrawable = 4;
        public static final int CastMiniController_castLargePauseButtonDrawable = 5;
        public static final int CastMiniController_castLargePlayButtonDrawable = 6;
        public static final int CastMiniController_castLargeStopButtonDrawable = 7;
        public static final int CastMiniController_castMuteToggleButtonDrawable = 8;
        public static final int CastMiniController_castPauseButtonDrawable = 9;
        public static final int CastMiniController_castPlayButtonDrawable = 10;
        public static final int CastMiniController_castProgressBarColor = 11;
        public static final int CastMiniController_castRewind30ButtonDrawable = 12;
        public static final int CastMiniController_castShowImageThumbnail = 13;
        public static final int CastMiniController_castSkipNextButtonDrawable = 14;
        public static final int CastMiniController_castSkipPreviousButtonDrawable = 15;
        public static final int CastMiniController_castStopButtonDrawable = 16;
        public static final int CastMiniController_castSubtitleTextAppearance = 17;
        public static final int CastMiniController_castTitleTextAppearance = 18;
        public static final int[] CustomCastTheme = {com.typhoon.tv.R.attr.castExpandedControllerStyle, com.typhoon.tv.R.attr.castIntroOverlayStyle, com.typhoon.tv.R.attr.castMiniControllerStyle};
        public static final int CustomCastTheme_castExpandedControllerStyle = 0;
        public static final int CustomCastTheme_castIntroOverlayStyle = 1;
        public static final int CustomCastTheme_castMiniControllerStyle = 2;
        public static final int[] LoadingImageView = {com.typhoon.tv.R.attr.circleCrop, com.typhoon.tv.R.attr.imageAspectRatio, com.typhoon.tv.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.typhoon.tv.R.attr.buttonSize, com.typhoon.tv.R.attr.colorScheme, com.typhoon.tv.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
