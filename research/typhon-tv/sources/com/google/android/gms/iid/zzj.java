package com.google.android.gms.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzj extends zzeu implements zzi {
    zzj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.iid.IMessengerCompat");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9423(Message message) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) message);
        m12299(1, v_);
    }
}
