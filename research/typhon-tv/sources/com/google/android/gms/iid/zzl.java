package com.google.android.gms.iid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Process;
import android.util.Log;
import com.google.android.gms.common.util.zzq;
import java.util.Iterator;

public final class zzl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static BroadcastReceiver f8037 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private static int f8038 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f8039 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private static int f8040 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private static int f8041 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f8042 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9424(Context context) {
        boolean z;
        if (f8042 != null) {
            return f8042;
        }
        f8041 = Process.myUid();
        PackageManager packageManager = context.getPackageManager();
        if (!zzq.m9267()) {
            Iterator<ResolveInfo> it2 = packageManager.queryIntentServices(new Intent("com.google.android.c2dm.intent.REGISTER"), 0).iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (m9427(packageManager, it2.next().serviceInfo.packageName, "com.google.android.c2dm.intent.REGISTER")) {
                        f8039 = false;
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z) {
                return f8042;
            }
        }
        if (m9425(packageManager)) {
            return f8042;
        }
        Log.w("InstanceID/Rpc", "Failed to resolve IID implementation package, falling back");
        if (m9426(packageManager, "com.google.android.gms")) {
            f8039 = zzq.m9267();
            return f8042;
        } else if (zzq.m9265() || !m9426(packageManager, "com.google.android.gsf")) {
            Log.w("InstanceID/Rpc", "Google Play services is missing, unable to get tokens");
            return null;
        } else {
            f8039 = false;
            return f8042;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m9425(PackageManager packageManager) {
        for (ResolveInfo resolveInfo : packageManager.queryBroadcastReceivers(new Intent("com.google.iid.TOKEN_REQUEST"), 0)) {
            if (m9427(packageManager, resolveInfo.activityInfo.packageName, "com.google.iid.TOKEN_REQUEST")) {
                f8039 = true;
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m9426(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
            f8042 = applicationInfo.packageName;
            f8040 = applicationInfo.uid;
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m9427(PackageManager packageManager, String str, String str2) {
        if (packageManager.checkPermission("com.google.android.c2dm.permission.SEND", str) == 0) {
            return m9426(packageManager, str);
        }
        Log.w("InstanceID/Rpc", new StringBuilder(String.valueOf(str).length() + 56 + String.valueOf(str2).length()).append("Possible malicious package ").append(str).append(" declares ").append(str2).append(" without permission").toString());
        return false;
    }
}
