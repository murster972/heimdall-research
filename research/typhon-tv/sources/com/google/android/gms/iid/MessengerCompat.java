package com.google.android.gms.iid;

import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.internal.ReflectedParcelable;

public class MessengerCompat implements ReflectedParcelable {
    public static final Parcelable.Creator<MessengerCompat> CREATOR = new zzk();

    /* renamed from: 靐  reason: contains not printable characters */
    private zzi f8035;

    /* renamed from: 龘  reason: contains not printable characters */
    private Messenger f8036;

    public MessengerCompat(IBinder iBinder) {
        zzi zzj;
        if (Build.VERSION.SDK_INT >= 21) {
            this.f8036 = new Messenger(iBinder);
            return;
        }
        if (iBinder == null) {
            zzj = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.iid.IMessengerCompat");
            zzj = queryLocalInterface instanceof zzi ? (zzi) queryLocalInterface : new zzj(iBinder);
        }
        this.f8035 = zzj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final IBinder m9420() {
        return this.f8036 != null ? this.f8036.getBinder() : this.f8035.asBinder();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return m9420().equals(((MessengerCompat) obj).m9420());
        } catch (ClassCastException e) {
            return false;
        }
    }

    public int hashCode() {
        return m9420().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.f8036 != null) {
            parcel.writeStrongBinder(this.f8036.getBinder());
        } else {
            parcel.writeStrongBinder(this.f8035.asBinder());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9421(Message message) throws RemoteException {
        if (this.f8036 != null) {
            this.f8036.send(message);
        } else {
            this.f8035.m9422(message);
        }
    }
}
