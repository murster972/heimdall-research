package com.google.android.gms.measurement;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import com.google.android.gms.internal.zzcla;
import com.google.android.gms.internal.zzcle;

@TargetApi(24)
public final class AppMeasurementJobService extends JobService implements zzcle {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcla<AppMeasurementJobService> f11092;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcla<AppMeasurementJobService> m13706() {
        if (this.f11092 == null) {
            this.f11092 = new zzcla<>(this);
        }
        return this.f11092;
    }

    public final void onCreate() {
        super.onCreate();
        m13706().m11292();
    }

    public final void onDestroy() {
        m13706().m11287();
        super.onDestroy();
    }

    public final void onRebind(Intent intent) {
        m13706().m11289(intent);
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        return m13706().m11295(jobParameters);
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    public final boolean onUnbind(Intent intent) {
        return m13706().m11288(intent);
    }

    @TargetApi(24)
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13707(JobParameters jobParameters, boolean z) {
        jobFinished(jobParameters, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13708(Intent intent) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13709(int i) {
        throw new UnsupportedOperationException();
    }
}
