package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.google.android.gms.internal.zzcid;
import com.google.android.gms.internal.zzcif;

public final class AppMeasurementReceiver extends WakefulBroadcastReceiver implements zzcif {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcid f11093;

    public final void onReceive(Context context, Intent intent) {
        if (this.f11093 == null) {
            this.f11093 = new zzcid(this);
        }
        this.f11093.m10911(context, intent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final BroadcastReceiver.PendingResult m13710() {
        return goAsync();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13711(Context context, Intent intent) {
        startWakefulService(context, intent);
    }
}
