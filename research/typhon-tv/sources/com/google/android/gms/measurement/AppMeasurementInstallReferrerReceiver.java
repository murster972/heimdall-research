package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.internal.zzcid;
import com.google.android.gms.internal.zzcif;

public final class AppMeasurementInstallReferrerReceiver extends BroadcastReceiver implements zzcif {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcid f11091;

    public final void onReceive(Context context, Intent intent) {
        if (this.f11091 == null) {
            this.f11091 = new zzcid(this);
        }
        this.f11091.m10911(context, intent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final BroadcastReceiver.PendingResult m13704() {
        return goAsync();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13705(Context context, Intent intent) {
    }
}
