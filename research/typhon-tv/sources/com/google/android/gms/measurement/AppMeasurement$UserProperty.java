package com.google.android.gms.measurement;

import com.google.android.gms.internal.zzclq;
import com.google.firebase.analytics.FirebaseAnalytics;

public final class AppMeasurement$UserProperty extends FirebaseAnalytics.UserProperty {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String[] f11086 = {"_ln", "_fot", "_fvt", "_ldl", "_id", "_fi"};

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String[] f11087 = {"firebase_last_notification", "first_open_time", "first_visit_time", "last_deep_link_referrer", "user_id", "first_open_after_install"};

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m13702(String str) {
        return zzclq.m11379(str, f11087, f11086);
    }
}
