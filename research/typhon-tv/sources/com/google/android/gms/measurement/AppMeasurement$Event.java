package com.google.android.gms.measurement;

import com.google.android.gms.internal.zzclq;
import com.google.firebase.analytics.FirebaseAnalytics;

public final class AppMeasurement$Event extends FirebaseAnalytics.Event {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String[] f11082 = {"_cd", "_ae", "_ui", "_ug", "_in", "_au", "_cmp", "_err", "_f", "_v", "_iap", "_nd", "_nf", "_no", "_nr", "_ou", "_s", "_e", "_xa", "_xu", "_aq", "_aa", "_ai", "_ac", "_vs", "_ep"};

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String[] f11083 = {"app_clear_data", "app_exception", "app_remove", "app_upgrade", "app_install", "app_update", "firebase_campaign", "error", "first_open", "first_visit", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement", "ad_exposure", "adunit_exposure", "ad_query", "ad_activeview", "ad_impression", "ad_click", "screen_view", "firebase_extra_parameter"};

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m13698(String str) {
        return zzclq.m11379(str, f11083, f11082);
    }
}
