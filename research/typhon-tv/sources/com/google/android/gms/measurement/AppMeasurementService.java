package com.google.android.gms.measurement;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Intent;
import android.os.IBinder;
import com.google.android.gms.internal.zzcla;
import com.google.android.gms.internal.zzcle;

public final class AppMeasurementService extends Service implements zzcle {

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcla<AppMeasurementService> f11094;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcla<AppMeasurementService> m13712() {
        if (this.f11094 == null) {
            this.f11094 = new zzcla<>(this);
        }
        return this.f11094;
    }

    public final IBinder onBind(Intent intent) {
        return m13712().m11291(intent);
    }

    public final void onCreate() {
        super.onCreate();
        m13712().m11292();
    }

    public final void onDestroy() {
        m13712().m11287();
        super.onDestroy();
    }

    public final void onRebind(Intent intent) {
        m13712().m11289(intent);
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        return m13712().m11290(intent, i, i2);
    }

    public final boolean onUnbind(Intent intent) {
        return m13712().m11288(intent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13713(JobParameters jobParameters, boolean z) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m13714(Intent intent) {
        AppMeasurementReceiver.completeWakefulIntent(intent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m13715(int i) {
        return stopSelfResult(i);
    }
}
