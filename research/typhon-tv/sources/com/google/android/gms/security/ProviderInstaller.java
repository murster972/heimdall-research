package com.google.android.gms.security;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.zzf;
import com.google.android.gms.common.zzp;
import java.lang.reflect.Method;

public class ProviderInstaller {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f5637 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private static Method f5638 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzf f5639 = zzf.m4219();

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m6099(Context context) throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException {
        zzbq.m9121(context, (Object) "Context must not be null");
        zzf.m4222(context);
        Context remoteContext = zzp.getRemoteContext(context);
        if (remoteContext == null) {
            Log.e("ProviderInstaller", "Failed to get remote context");
            throw new GooglePlayServicesNotAvailableException(8);
        }
        synchronized (f5637) {
            try {
                if (f5638 == null) {
                    f5638 = remoteContext.getClassLoader().loadClass("com.google.android.gms.common.security.ProviderInstallerImpl").getMethod("insertProvider", new Class[]{Context.class});
                }
                f5638.invoke((Object) null, new Object[]{remoteContext});
            } catch (Exception e) {
                String valueOf = String.valueOf(e.getMessage());
                Log.e("ProviderInstaller", valueOf.length() != 0 ? "Failed to install provider: ".concat(valueOf) : new String("Failed to install provider: "));
                throw new GooglePlayServicesNotAvailableException(8);
            }
        }
    }
}
