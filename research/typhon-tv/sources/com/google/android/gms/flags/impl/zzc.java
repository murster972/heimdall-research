package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzc implements Callable<Boolean> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f7986;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ Boolean f7987;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ SharedPreferences f7988;

    zzc(SharedPreferences sharedPreferences, String str, Boolean bool) {
        this.f7988 = sharedPreferences;
        this.f7986 = str;
        this.f7987 = bool;
    }

    public final /* synthetic */ Object call() throws Exception {
        return Boolean.valueOf(this.f7988.getBoolean(this.f7986, this.f7987.booleanValue()));
    }
}
