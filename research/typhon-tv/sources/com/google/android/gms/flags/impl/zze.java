package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zze implements Callable<Integer> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f7989;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ Integer f7990;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ SharedPreferences f7991;

    zze(SharedPreferences sharedPreferences, String str, Integer num) {
        this.f7991 = sharedPreferences;
        this.f7989 = str;
        this.f7990 = num;
    }

    public final /* synthetic */ Object call() throws Exception {
        return Integer.valueOf(this.f7991.getInt(this.f7989, this.f7990.intValue()));
    }
}
