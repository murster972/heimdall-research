package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzi implements Callable<String> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f7995;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f7996;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ SharedPreferences f7997;

    zzi(SharedPreferences sharedPreferences, String str, String str2) {
        this.f7997 = sharedPreferences;
        this.f7995 = str;
        this.f7996 = str2;
    }

    public final /* synthetic */ Object call() throws Exception {
        return this.f7997.getString(this.f7995, this.f7996);
    }
}
