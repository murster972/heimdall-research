package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzg implements Callable<Long> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f7992;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ Long f7993;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ SharedPreferences f7994;

    zzg(SharedPreferences sharedPreferences, String str, Long l) {
        this.f7994 = sharedPreferences;
        this.f7992 = str;
        this.f7993 = l;
    }

    public final /* synthetic */ Object call() throws Exception {
        return Long.valueOf(this.f7994.getLong(this.f7992, this.f7993.longValue()));
    }
}
