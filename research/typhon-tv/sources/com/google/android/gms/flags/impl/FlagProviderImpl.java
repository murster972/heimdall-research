package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzcaz;

@DynamiteApi
public class FlagProviderImpl extends zzcaz {

    /* renamed from: 靐  reason: contains not printable characters */
    private SharedPreferences f7984;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f7985 = false;

    public boolean getBooleanFlagValue(String str, boolean z, int i) {
        return !this.f7985 ? z : zzb.m9344(this.f7984, str, Boolean.valueOf(z)).booleanValue();
    }

    public int getIntFlagValue(String str, int i, int i2) {
        return !this.f7985 ? i : zzd.m9345(this.f7984, str, Integer.valueOf(i)).intValue();
    }

    public long getLongFlagValue(String str, long j, int i) {
        return !this.f7985 ? j : zzf.m9346(this.f7984, str, Long.valueOf(j)).longValue();
    }

    public String getStringFlagValue(String str, String str2, int i) {
        return !this.f7985 ? str2 : zzh.m9347(this.f7984, str, str2);
    }

    public void init(IObjectWrapper iObjectWrapper) {
        Context context = (Context) zzn.m9307(iObjectWrapper);
        if (!this.f7985) {
            try {
                this.f7984 = zzj.m9348(context.createPackageContext("com.google.android.gms", 0));
                this.f7985 = true;
            } catch (PackageManager.NameNotFoundException e) {
            } catch (Exception e2) {
                String valueOf = String.valueOf(e2.getMessage());
                Log.w("FlagProviderImpl", valueOf.length() != 0 ? "Could not retrieve sdk flags, continuing with defaults: ".concat(valueOf) : new String("Could not retrieve sdk flags, continuing with defaults: "));
            }
        }
    }
}
