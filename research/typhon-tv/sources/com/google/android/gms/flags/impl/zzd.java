package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gms.internal.zzcbc;

public final class zzd extends zza<Integer> {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Integer m9345(SharedPreferences sharedPreferences, String str, Integer num) {
        try {
            return (Integer) zzcbc.m10311(new zze(sharedPreferences, str, num));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FlagDataUtils", valueOf.length() != 0 ? "Flag value not available, returning default: ".concat(valueOf) : new String("Flag value not available, returning default: "));
            return num;
        }
    }
}
