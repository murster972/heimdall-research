package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzk implements Callable<SharedPreferences> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Context f7999;

    zzk(Context context) {
        this.f7999 = context;
    }

    public final /* synthetic */ Object call() throws Exception {
        return this.f7999.getSharedPreferences("google_sdk_flags", 0);
    }
}
