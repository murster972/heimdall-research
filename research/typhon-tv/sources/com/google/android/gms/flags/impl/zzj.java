package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.internal.zzcbc;

public final class zzj {

    /* renamed from: 龘  reason: contains not printable characters */
    private static SharedPreferences f7998 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static SharedPreferences m9348(Context context) throws Exception {
        SharedPreferences sharedPreferences;
        synchronized (SharedPreferences.class) {
            if (f7998 == null) {
                f7998 = (SharedPreferences) zzcbc.m10311(new zzk(context));
            }
            sharedPreferences = f7998;
        }
        return sharedPreferences;
    }
}
