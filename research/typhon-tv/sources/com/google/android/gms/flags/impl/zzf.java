package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gms.internal.zzcbc;

public final class zzf extends zza<Long> {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Long m9346(SharedPreferences sharedPreferences, String str, Long l) {
        try {
            return (Long) zzcbc.m10311(new zzg(sharedPreferences, str, l));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FlagDataUtils", valueOf.length() != 0 ? "Flag value not available, returning default: ".concat(valueOf) : new String("Flag value not available, returning default: "));
            return l;
        }
    }
}
