package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gms.internal.zzcbc;

public final class zzh extends zza<String> {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9347(SharedPreferences sharedPreferences, String str, String str2) {
        try {
            return (String) zzcbc.m10311(new zzi(sharedPreferences, str, str2));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FlagDataUtils", valueOf.length() != 0 ? "Flag value not available, returning default: ".concat(valueOf) : new String("Flag value not available, returning default: "));
            return str2;
        }
    }
}
