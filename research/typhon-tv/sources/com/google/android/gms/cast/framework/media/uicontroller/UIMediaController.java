package com.google.android.gms.cast.framework.media.uicontroller;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.gms.R;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.TracksChooserDialogFragment;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbad;
import com.google.android.gms.internal.zzbag;
import com.google.android.gms.internal.zzbai;
import com.google.android.gms.internal.zzbal;
import com.google.android.gms.internal.zzban;
import com.google.android.gms.internal.zzbao;
import com.google.android.gms.internal.zzbaq;
import com.google.android.gms.internal.zzbar;
import com.google.android.gms.internal.zzbas;
import com.google.android.gms.internal.zzbat;
import com.google.android.gms.internal.zzbau;
import com.google.android.gms.internal.zzbav;
import com.google.android.gms.internal.zzbaw;
import com.google.android.gms.internal.zzbax;
import com.google.android.gms.internal.zzbay;
import com.google.android.gms.internal.zzbba;
import com.google.android.gms.internal.zzbcy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class UIMediaController implements SessionManagerListener<CastSession>, RemoteMediaClient.Listener {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7285 = new zzbcy("UIMediaController");

    /* renamed from: ʻ  reason: contains not printable characters */
    private RemoteMediaClient.Listener f7286;

    /* renamed from: ʼ  reason: contains not printable characters */
    private RemoteMediaClient f7287;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Set<zzbay> f7288 = new HashSet();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Activity f7289;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<View, List<UIController>> f7290 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private final SessionManager f7291;

    public UIMediaController(Activity activity) {
        this.f7289 = activity;
        this.f7291 = CastContext.m7977((Context) activity).m7981();
        this.f7291.m8082(this, CastSession.class);
        m8231((Session) this.f7291.m8075());
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private final void m8228() {
        if (m8235()) {
            for (List<UIController> it2 : this.f7290.values()) {
                for (UIController r0 : it2) {
                    r0.m8223();
                }
            }
            this.f7287.m4151((RemoteMediaClient.Listener) this);
            this.f7287 = null;
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final void m8229() {
        for (List<UIController> it2 : this.f7290.values()) {
            for (UIController r0 : it2) {
                r0.m8225();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8230(View view, UIController uIController) {
        List list = this.f7290.get(view);
        if (list == null) {
            list = new ArrayList();
            this.f7290.put(view, list);
        }
        list.add(uIController);
        if (m8235()) {
            uIController.m8227(this.f7291.m8075());
            m8229();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m8231(Session session) {
        if (!m8235() && (session instanceof CastSession) && session.m8053()) {
            CastSession castSession = (CastSession) session;
            this.f7287 = castSession.m8022();
            if (this.f7287 != null) {
                this.f7287.m4165((RemoteMediaClient.Listener) this);
                for (List<UIController> it2 : this.f7290.values()) {
                    for (UIController r0 : it2) {
                        r0.m8227(castSession);
                    }
                }
                m8229();
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m8232() {
        m8229();
        if (this.f7286 != null) {
            this.f7286.m8189();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m8233(View view) {
        RemoteMediaClient r0 = m8236();
        if (r0 != null && r0.m4143()) {
            r0.m4153((JSONObject) null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m8234(View view) {
        RemoteMediaClient r0 = m8236();
        if (r0 != null && r0.m4143()) {
            r0.m4155((JSONObject) null);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m8235() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7287 != null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public RemoteMediaClient m8236() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7287;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m8237() {
        zzbq.m9115("Must be called from the main thread.");
        m8228();
        this.f7290.clear();
        this.f7291.m8077(this, CastSession.class);
        this.f7286 = null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m8238() {
        for (List<UIController> it2 : this.f7290.values()) {
            for (UIController r0 : it2) {
                r0.m8224();
            }
        }
        if (this.f7286 != null) {
            this.f7286.m8190();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m8239(View view) {
        ComponentName componentName = new ComponentName(this.f7289.getApplicationContext(), CastContext.m7977((Context) this.f7289).m7984().m7986().m8137());
        Intent intent = new Intent();
        intent.setComponent(componentName);
        this.f7289.startActivity(intent);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8240() {
        m8229();
        if (this.f7286 != null) {
            this.f7286.m8191();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8241(View view) {
        zzbq.m9115("Must be called from the main thread.");
        view.setOnClickListener(new zzg(this));
        m8230(view, (UIController) new zzbai(view));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8242(View view, int i) {
        zzbq.m9115("Must be called from the main thread.");
        view.setOnClickListener(new zzc(this));
        m8230(view, (UIController) new zzbav(view, i));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8243(View view, long j) {
        zzbq.m9115("Must be called from the main thread.");
        view.setOnClickListener(new zze(this, j));
        m8230(view, (UIController) new zzbat(view));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m8244(ImageView imageView) {
        CastSession r3 = CastContext.m7977(this.f7289.getApplicationContext()).m7981().m8075();
        if (r3 != null && r3.m8053()) {
            try {
                r3.m8017(!r3.m8019());
            } catch (IOException | IllegalArgumentException e) {
                f7285.m10087("Unable to call CastSession.setMute(boolean).", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m8245(SeekBar seekBar) {
        if (this.f7290.containsKey(seekBar)) {
            for (UIController uIController : this.f7290.get(seekBar)) {
                if (uIController instanceof zzbas) {
                    ((zzbas) uIController).m9922(true);
                }
            }
        }
        for (zzbay r0 : this.f7288) {
            r0.m9948(true);
        }
        RemoteMediaClient r02 = m8236();
        if (r02 != null && r02.m4143()) {
            r02.m4156((long) seekBar.getProgress());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8249(CastSession castSession) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8250(CastSession castSession, int i) {
        m8228();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8251(CastSession castSession, String str) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m8252() {
        m8229();
        if (this.f7286 != null) {
            this.f7286.m8192();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m8253(View view) {
        RemoteMediaClient r1 = m8236();
        if (r1 != null && r1.m4143() && (this.f7289 instanceof FragmentActivity)) {
            FragmentActivity fragmentActivity = (FragmentActivity) this.f7289;
            FragmentTransaction beginTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
            Fragment findFragmentByTag = fragmentActivity.getSupportFragmentManager().findFragmentByTag("TRACKS_CHOOSER_DIALOG_TAG");
            if (findFragmentByTag != null) {
                beginTransaction.remove(findFragmentByTag);
            }
            beginTransaction.addToBackStack((String) null);
            TracksChooserDialogFragment r0 = TracksChooserDialogFragment.m8219(r1.m4137(), r1.m4136().m7910());
            if (r0 != null) {
                r0.show(beginTransaction, "TRACKS_CHOOSER_DIALOG_TAG");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m8254(View view, long j) {
        RemoteMediaClient r0 = m8236();
        if (r0 != null && r0.m4143()) {
            r0.m4156(r0.m4148() - j);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m8256(CastSession castSession, int i) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m8257() {
        m8229();
        if (this.f7286 != null) {
            this.f7286.m8193();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m8258(View view) {
        zzbq.m9115("Must be called from the main thread.");
        view.setOnClickListener(new zzh(this));
        m8230(view, (UIController) new zzbad(view, this.f7289));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m8259(View view, int i) {
        zzbq.m9115("Must be called from the main thread.");
        m8230(view, (UIController) new zzbba(view, i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m8260(View view, long j) {
        RemoteMediaClient r0 = m8236();
        if (r0 != null && r0.m4143()) {
            r0.m4156(r0.m4148() + j);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m8261(ImageView imageView) {
        RemoteMediaClient r0 = m8236();
        if (r0 != null && r0.m4143()) {
            r0.m4142();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m8262(SeekBar seekBar) {
        if (this.f7290.containsKey(seekBar)) {
            for (UIController uIController : this.f7290.get(seekBar)) {
                if (uIController instanceof zzbas) {
                    ((zzbas) uIController).m9922(false);
                }
            }
        }
        for (zzbay r0 : this.f7288) {
            r0.m9948(false);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m8264(CastSession castSession, int i) {
        m8228();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8265() {
        m8229();
        if (this.f7286 != null) {
            this.f7286.m8194();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8266(View view) {
        zzbq.m9115("Must be called from the main thread.");
        m8230(view, (UIController) new zzbal(view));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8267(View view, int i) {
        zzbq.m9115("Must be called from the main thread.");
        view.setOnClickListener(new zzb(this));
        m8230(view, (UIController) new zzbau(view, i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8268(View view, long j) {
        zzbq.m9115("Must be called from the main thread.");
        view.setOnClickListener(new zzd(this, j));
        m8230(view, (UIController) new zzbat(view));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8269(View view, UIController uIController) {
        zzbq.m9115("Must be called from the main thread.");
        m8230(view, uIController);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8270(ImageView imageView) {
        zzbq.m9115("Must be called from the main thread.");
        imageView.setOnClickListener(new zzi(this));
        m8230((View) imageView, (UIController) new zzbao(imageView, this.f7289));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8271(ImageView imageView, Drawable drawable, Drawable drawable2, Drawable drawable3, View view, boolean z) {
        zzbq.m9115("Must be called from the main thread.");
        imageView.setOnClickListener(new zza(this));
        m8230((View) imageView, (UIController) new zzbaq(imageView, this.f7289, drawable, drawable2, drawable3, view, z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8272(ImageView imageView, ImageHints imageHints, int i) {
        zzbq.m9115("Must be called from the main thread.");
        m8230((View) imageView, (UIController) new zzbag(imageView, this.f7289, imageHints, i, (View) null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8273(ImageView imageView, ImageHints imageHints, View view) {
        zzbq.m9115("Must be called from the main thread.");
        m8230((View) imageView, (UIController) new zzbag(imageView, this.f7289, imageHints, 0, view));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8274(ProgressBar progressBar) {
        m8275(progressBar, 1000);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8275(ProgressBar progressBar, long j) {
        zzbq.m9115("Must be called from the main thread.");
        m8230((View) progressBar, (UIController) new zzbar(progressBar, j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8276(SeekBar seekBar) {
        m8278(seekBar, 1000);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8277(SeekBar seekBar, int i, boolean z) {
        if (z) {
            for (zzbay r0 : this.f7288) {
                r0.m9945((long) i);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8278(SeekBar seekBar, long j) {
        zzbq.m9115("Must be called from the main thread.");
        seekBar.setOnSeekBarChangeListener(new zzf(this));
        m8230((View) seekBar, (UIController) new zzbas(seekBar, j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8279(TextView textView) {
        zzbq.m9115("Must be called from the main thread.");
        m8230((View) textView, (UIController) new zzbaw(textView));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8280(TextView textView, View view) {
        zzbq.m9115("Must be called from the main thread.");
        m8230((View) textView, (UIController) new zzbax(textView, this.f7289.getString(R.string.cast_invalid_stream_duration_text), view));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8281(TextView textView, String str) {
        zzbq.m9115("Must be called from the main thread.");
        m8282(textView, (List<String>) Collections.singletonList(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8282(TextView textView, List<String> list) {
        zzbq.m9115("Must be called from the main thread.");
        m8230((View) textView, (UIController) new zzban(textView, list));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8283(TextView textView, boolean z) {
        m8284(textView, z, 1000);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8284(TextView textView, boolean z, long j) {
        zzbq.m9115("Must be called from the main thread.");
        zzbay zzbay = new zzbay(textView, j, this.f7289.getString(R.string.cast_invalid_stream_position_text));
        if (z) {
            this.f7288.add(zzbay);
        }
        m8230((View) textView, (UIController) zzbay);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8289(CastSession castSession) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8290(CastSession castSession, int i) {
        m8228();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8291(CastSession castSession, String str) {
        m8231((Session) castSession);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8292(CastSession castSession, boolean z) {
        m8231((Session) castSession);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8293(RemoteMediaClient.Listener listener) {
        zzbq.m9115("Must be called from the main thread.");
        this.f7286 = listener;
    }
}
