package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzx extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    void m8450(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m8451(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m8452(IObjectWrapper iObjectWrapper, String str) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m8453(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m8454(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m8455() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8456(IObjectWrapper iObjectWrapper) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8457(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8458(IObjectWrapper iObjectWrapper, String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8459(IObjectWrapper iObjectWrapper, boolean z) throws RemoteException;
}
