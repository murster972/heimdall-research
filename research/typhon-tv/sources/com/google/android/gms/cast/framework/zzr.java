package com.google.android.gms.cast.framework;

import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.zzev;

public interface zzr extends IInterface {

    public static abstract class zza extends zzev implements zzr {
        /* renamed from: 龘  reason: contains not printable characters */
        public static zzr m8414(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.IReconnectionService");
            return queryLocalInterface instanceof zzr ? (zzr) queryLocalInterface : new zzs(iBinder);
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    void m8410() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    int m8411(Intent intent, int i, int i2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m8412(Intent intent) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8413() throws RemoteException;
}
