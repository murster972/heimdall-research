package com.google.android.gms.cast.framework.media;

import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.zzev;

public interface zzd extends IInterface {

    public static abstract class zza extends zzev implements zzd {
        /* renamed from: 龘  reason: contains not printable characters */
        public static zzd m8348(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.IMediaNotificationService");
            return queryLocalInterface instanceof zzd ? (zzd) queryLocalInterface : new zze(iBinder);
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    void m8344() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    int m8345(Intent intent, int i, int i2) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IBinder m8346(Intent intent) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8347() throws RemoteException;
}
