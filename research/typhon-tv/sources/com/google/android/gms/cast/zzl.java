package com.google.android.gms.cast;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbcq;

final class zzl extends zzbcq {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f7409;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzl(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.f7409 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m8470(Api.zzb zzb) throws RemoteException {
        m10050((zzbcf) zzb);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8471(zzbcf zzbcf) throws RemoteException {
        if (TextUtils.isEmpty(this.f7409)) {
            m4198(m4195(new Status(2001, "IllegalArgument: sessionId cannot be null or empty", (PendingIntent) null)));
            return;
        }
        try {
            zzbcf.m10014(this.f7409, (zzn<Status>) this);
        } catch (IllegalStateException e) {
            m9957(2001);
        }
    }
}
