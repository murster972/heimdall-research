package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzag implements Parcelable.Creator<MediaMetadata> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        int i = 0;
        Bundle bundle = null;
        ArrayList<WebImage> arrayList = null;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    arrayList = zzbfn.m10167(parcel, readInt, WebImage.CREATOR);
                    break;
                case 3:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 4:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new MediaMetadata(arrayList, bundle, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaMetadata[i];
    }
}
