package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import com.google.android.gms.internal.zzeu;

public final class zzh extends zzeu implements zzf {
    zzh(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.INotificationActionsProvider");
    }
}
