package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Arrays;
import java.util.Locale;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONException;
import org.json.JSONObject;

public class AdBreakClipInfo extends zzbfm {
    public static final Parcelable.Creator<AdBreakClipInfo> CREATOR = new zza();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f6937;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f6938;

    /* renamed from: ʽ  reason: contains not printable characters */
    private JSONObject f6939;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f6940;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f6941;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f6942;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f6943;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f6944;

    AdBreakClipInfo(String str, String str2, long j, String str3, String str4, String str5, String str6) {
        this.f6944 = str;
        this.f6941 = str2;
        this.f6943 = j;
        this.f6942 = str3;
        this.f6940 = str4;
        this.f6937 = str5;
        this.f6938 = str6;
        if (!TextUtils.isEmpty(this.f6938)) {
            try {
                this.f6939 = new JSONObject(str6);
            } catch (JSONException e) {
                Log.w("AdBreakClipInfo", String.format(Locale.ROOT, "Error creating AdBreakClipInfo: %s", new Object[]{e.getMessage()}));
                this.f6938 = null;
                this.f6939 = new JSONObject();
            }
        } else {
            this.f6939 = new JSONObject();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static AdBreakClipInfo m7766(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        if (!jSONObject.has("id")) {
            return null;
        }
        try {
            String string = jSONObject.getString("id");
            long optLong = (long) (((double) jSONObject.optLong(VastIconXmlManager.DURATION)) * 1000.0d);
            String optString = jSONObject.optString("clickThroughUrl", (String) null);
            String optString2 = jSONObject.optString("contentUrl", (String) null);
            String optString3 = jSONObject.optString("mimeType", (String) null);
            String optString4 = jSONObject.optString(PubnativeAsset.TITLE, (String) null);
            JSONObject optJSONObject = jSONObject.optJSONObject("customData");
            return new AdBreakClipInfo(string, optString4, optLong, optString2, optString3, optString, optJSONObject == null ? null : optJSONObject.toString());
        } catch (JSONException e) {
            Log.d("AdBreakClipInfo", String.format(Locale.ROOT, "Error while creating an AdBreakClipInfo from JSON: %s", new Object[]{e.getMessage()}));
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdBreakClipInfo)) {
            return false;
        }
        AdBreakClipInfo adBreakClipInfo = (AdBreakClipInfo) obj;
        return zzbcm.m10043(this.f6944, adBreakClipInfo.f6944) && zzbcm.m10043(this.f6941, adBreakClipInfo.f6941) && this.f6943 == adBreakClipInfo.f6943 && zzbcm.m10043(this.f6942, adBreakClipInfo.f6942) && zzbcm.m10043(this.f6940, adBreakClipInfo.f6940) && zzbcm.m10043(this.f6937, adBreakClipInfo.f6937) && zzbcm.m10043(this.f6938, adBreakClipInfo.f6938);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f6944, this.f6941, Long.valueOf(this.f6943), this.f6942, this.f6940, this.f6937, this.f6938});
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, m7772(), false);
        zzbfp.m10193(parcel, 3, m7769(), false);
        zzbfp.m10186(parcel, 4, m7771());
        zzbfp.m10193(parcel, 5, m7770(), false);
        zzbfp.m10193(parcel, 6, m7768(), false);
        zzbfp.m10193(parcel, 7, m7767(), false);
        zzbfp.m10193(parcel, 8, this.f6938, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7767() {
        return this.f6937;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m7768() {
        return this.f6940;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7769() {
        return this.f6941;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m7770() {
        return this.f6942;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public long m7771() {
        return this.f6943;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7772() {
        return this.f6944;
    }
}
