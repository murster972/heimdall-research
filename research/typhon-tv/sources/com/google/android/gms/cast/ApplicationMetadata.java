package com.google.android.gms.cast;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ApplicationMetadata extends zzbfm {
    public static final Parcelable.Creator<ApplicationMetadata> CREATOR = new zzd();

    /* renamed from: ʻ  reason: contains not printable characters */
    private Uri f6955;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f6956;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f6957;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<String> f6958;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<WebImage> f6959;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f6960;

    private ApplicationMetadata() {
        this.f6959 = new ArrayList();
        this.f6958 = new ArrayList();
    }

    ApplicationMetadata(String str, String str2, List<WebImage> list, List<String> list2, String str3, Uri uri) {
        this.f6960 = str;
        this.f6957 = str2;
        this.f6959 = list;
        this.f6958 = list2;
        this.f6956 = str3;
        this.f6955 = uri;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ApplicationMetadata)) {
            return false;
        }
        ApplicationMetadata applicationMetadata = (ApplicationMetadata) obj;
        return zzbcm.m10043(this.f6960, applicationMetadata.f6960) && zzbcm.m10043(this.f6959, applicationMetadata.f6959) && zzbcm.m10043(this.f6957, applicationMetadata.f6957) && zzbcm.m10043(this.f6958, applicationMetadata.f6958) && zzbcm.m10043(this.f6956, applicationMetadata.f6956) && zzbcm.m10043(this.f6955, applicationMetadata.f6955);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f6960, this.f6957, this.f6959, this.f6958, this.f6956, this.f6955});
    }

    public String toString() {
        int i = 0;
        StringBuilder append = new StringBuilder("applicationId: ").append(this.f6960).append(", name: ").append(this.f6957).append(", images.count: ").append(this.f6959 == null ? 0 : this.f6959.size()).append(", namespaces.count: ");
        if (this.f6958 != null) {
            i = this.f6958.size();
        }
        return append.append(i).append(", senderAppIdentifier: ").append(this.f6956).append(", senderAppLaunchUrl: ").append(this.f6955).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, m7789(), false);
        zzbfp.m10193(parcel, 3, m7786(), false);
        zzbfp.m10180(parcel, 4, m7785(), false);
        zzbfp.m10178(parcel, 5, m7788(), false);
        zzbfp.m10193(parcel, 6, m7787(), false);
        zzbfp.m10189(parcel, 7, (Parcelable) this.f6955, i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public List<WebImage> m7785() {
        return this.f6959;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7786() {
        return this.f6957;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m7787() {
        return this.f6956;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public List<String> m7788() {
        return Collections.unmodifiableList(this.f6958);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7789() {
        return this.f6960;
    }
}
