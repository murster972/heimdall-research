package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zza implements Parcelable.Creator<CastMediaOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r5 = zzbfn.m10169(parcel);
        NotificationOptions notificationOptions = null;
        IBinder iBinder = null;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < r5) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                case 5:
                    notificationOptions = (NotificationOptions) zzbfn.m10171(parcel, readInt, NotificationOptions.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r5);
        return new CastMediaOptions(str2, str, iBinder, notificationOptions);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CastMediaOptions[i];
    }
}
