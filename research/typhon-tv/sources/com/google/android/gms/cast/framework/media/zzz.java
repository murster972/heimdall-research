package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbdb;
import java.io.IOException;
import org.json.JSONObject;

final class zzz extends RemoteMediaClient.zzb {

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7395;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ JSONObject f7396;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzz(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, JSONObject jSONObject) {
        super(remoteMediaClient, googleApiClient);
        this.f7395 = remoteMediaClient;
        this.f7396 = jSONObject;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8363(zzbcf zzbcf) {
        synchronized (this.f7395.f3638) {
            try {
                this.f7395.f3639.m10106(this.f7272, 0, -1, (MediaQueueItem[]) null, 1, (Integer) null, this.f7396);
            } catch (zzbdb | IOException e) {
                m4198((RemoteMediaClient.MediaChannelResult) m4195(new Status(2100)));
            }
        }
    }
}
