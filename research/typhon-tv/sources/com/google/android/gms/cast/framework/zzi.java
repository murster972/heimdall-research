package com.google.android.gms.cast.framework;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.internal.zzev;
import com.google.android.gms.internal.zzew;

public abstract class zzi extends zzev implements zzh {
    public zzi() {
        attachInterface(this, "com.google.android.gms.cast.framework.ICastConnectionController");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                m8385(parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 2:
                m8384(parcel.readString(), (LaunchOptions) zzew.m12304(parcel, LaunchOptions.CREATOR));
                parcel2.writeNoException();
                break;
            case 3:
                m8383(parcel.readString());
                parcel2.writeNoException();
                break;
            case 4:
                m8382(parcel.readInt());
                parcel2.writeNoException();
                break;
            case 5:
                parcel2.writeNoException();
                parcel2.writeInt(11910208);
                break;
            default:
                return false;
        }
        return true;
    }
}
