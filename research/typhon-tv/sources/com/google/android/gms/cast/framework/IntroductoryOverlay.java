package com.google.android.gms.cast.framework;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.internal.zzazd;
import com.google.android.gms.internal.zzazh;

public interface IntroductoryOverlay {

    public static class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f7118;

        /* renamed from: ʼ  reason: contains not printable characters */
        private float f7119;

        /* renamed from: ʽ  reason: contains not printable characters */
        private String f7120;

        /* renamed from: 连任  reason: contains not printable characters */
        private OnOverlayDismissedListener f7121;

        /* renamed from: 靐  reason: contains not printable characters */
        private final View f7122;

        /* renamed from: 麤  reason: contains not printable characters */
        private String f7123;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f7124;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Activity f7125;

        public Builder(Activity activity, MenuItem menuItem) {
            this.f7125 = (Activity) zzbq.m9120(activity);
            this.f7122 = ((MenuItem) zzbq.m9120(menuItem)).getActionView();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public final boolean m8038() {
            return this.f7118;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public final String m8039() {
            return this.f7123;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public final String m8040() {
            return this.f7120;
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public final float m8041() {
            return this.f7119;
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        public IntroductoryOverlay m8042() {
            return zzq.m9269() ? new zzazd(this) : new zzazh(this);
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final int m8043() {
            return this.f7124;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final Activity m8044() {
            return this.f7125;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m8045(String str) {
            this.f7120 = str;
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final OnOverlayDismissedListener m8046() {
            return this.f7121;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final View m8047() {
            return this.f7122;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m8048() {
            this.f7118 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m8049(String str) {
            this.f7123 = str;
            return this;
        }
    }

    public interface OnOverlayDismissedListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m8050();
    }

    public static class zza {
        /* renamed from: 靐  reason: contains not printable characters */
        public static boolean m8051(Context context) {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("googlecast-introOverlayShown", false);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m8052(Context context) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("googlecast-introOverlayShown", true).apply();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    void m8037();
}
