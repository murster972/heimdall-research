package com.google.android.gms.cast;

import android.os.RemoteException;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbcq;

final class zzf extends zzbcq {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f7402;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f7403;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzf(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient);
        this.f7403 = str;
        this.f7402 = str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m8464(Api.zzb zzb) throws RemoteException {
        m10050((zzbcf) zzb);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8465(zzbcf zzbcf) throws RemoteException {
        try {
            zzbcf.m10016(this.f7403, this.f7402, (zzn<Status>) this);
        } catch (IllegalArgumentException | IllegalStateException e) {
            m9957(2001);
        }
    }
}
