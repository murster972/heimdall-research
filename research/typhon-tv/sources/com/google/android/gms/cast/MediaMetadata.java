package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.internal.zzbdf;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaMetadata extends zzbfm {
    public static final Parcelable.Creator<MediaMetadata> CREATOR = new zzag();

    /* renamed from: 靐  reason: contains not printable characters */
    private static final zza f7010 = new zza().m7888("com.google.android.gms.cast.metadata.CREATION_DATE", "creationDateTime", 4).m7888("com.google.android.gms.cast.metadata.RELEASE_DATE", "releaseDate", 4).m7888("com.google.android.gms.cast.metadata.BROADCAST_DATE", "originalAirdate", 4).m7888("com.google.android.gms.cast.metadata.TITLE", PubnativeAsset.TITLE, 1).m7888("com.google.android.gms.cast.metadata.SUBTITLE", "subtitle", 1).m7888("com.google.android.gms.cast.metadata.ARTIST", "artist", 1).m7888("com.google.android.gms.cast.metadata.ALBUM_ARTIST", "albumArtist", 1).m7888("com.google.android.gms.cast.metadata.ALBUM_TITLE", "albumName", 1).m7888("com.google.android.gms.cast.metadata.COMPOSER", "composer", 1).m7888("com.google.android.gms.cast.metadata.DISC_NUMBER", "discNumber", 2).m7888("com.google.android.gms.cast.metadata.TRACK_NUMBER", "trackNumber", 2).m7888("com.google.android.gms.cast.metadata.SEASON_NUMBER", "season", 2).m7888("com.google.android.gms.cast.metadata.EPISODE_NUMBER", "episode", 2).m7888("com.google.android.gms.cast.metadata.SERIES_TITLE", "seriesTitle", 1).m7888("com.google.android.gms.cast.metadata.STUDIO", "studio", 1).m7888("com.google.android.gms.cast.metadata.WIDTH", VastIconXmlManager.WIDTH, 2).m7888("com.google.android.gms.cast.metadata.HEIGHT", VastIconXmlManager.HEIGHT, 2).m7888("com.google.android.gms.cast.metadata.LOCATION_NAME", "location", 1).m7888("com.google.android.gms.cast.metadata.LOCATION_LATITUDE", "latitude", 3).m7888("com.google.android.gms.cast.metadata.LOCATION_LONGITUDE", "longitude", 3);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f7011 = {null, "String", "int", "double", "ISO-8601 date String"};

    /* renamed from: 连任  reason: contains not printable characters */
    private int f7012;

    /* renamed from: 麤  reason: contains not printable characters */
    private Bundle f7013;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<WebImage> f7014;

    static class zza {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Map<String, String> f7015 = new HashMap();

        /* renamed from: 齉  reason: contains not printable characters */
        private final Map<String, Integer> f7016 = new HashMap();

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, String> f7017 = new HashMap();

        /* renamed from: 靐  reason: contains not printable characters */
        public final String m7886(String str) {
            return this.f7015.get(str);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final int m7887(String str) {
            Integer num = this.f7016.get(str);
            if (num != null) {
                return num.intValue();
            }
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final zza m7888(String str, String str2, int i) {
            this.f7017.put(str, str2);
            this.f7015.put(str2, str);
            this.f7016.put(str, Integer.valueOf(i));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m7889(String str) {
            return this.f7017.get(str);
        }
    }

    public MediaMetadata() {
        this(0);
    }

    public MediaMetadata(int i) {
        this(new ArrayList(), new Bundle(), i);
    }

    MediaMetadata(List<WebImage> list, Bundle bundle, int i) {
        this.f7014 = list;
        this.f7013 = bundle;
        this.f7012 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m7871(String str, int i) throws IllegalArgumentException {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("null and empty keys are not allowed");
        }
        int r0 = f7010.m7887(str);
        if (r0 != i && r0 != 0) {
            String str2 = f7011[i];
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length()).append("Value for ").append(str).append(" must be a ").append(str2).toString());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m7872(JSONObject jSONObject, String... strArr) {
        HashSet hashSet = new HashSet(Arrays.asList(strArr));
        try {
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                if (!"metadataType".equals(str)) {
                    String r6 = f7010.m7886(str);
                    if (r6 == null) {
                        Object obj = jSONObject.get(str);
                        if (obj instanceof String) {
                            this.f7013.putString(str, (String) obj);
                        } else if (obj instanceof Integer) {
                            this.f7013.putInt(str, ((Integer) obj).intValue());
                        } else if (obj instanceof Double) {
                            this.f7013.putDouble(str, ((Double) obj).doubleValue());
                        }
                    } else if (hashSet.contains(r6)) {
                        try {
                            Object obj2 = jSONObject.get(str);
                            if (obj2 != null) {
                                switch (f7010.m7887(r6)) {
                                    case 1:
                                        if (!(obj2 instanceof String)) {
                                            break;
                                        } else {
                                            this.f7013.putString(r6, (String) obj2);
                                            break;
                                        }
                                    case 2:
                                        if (!(obj2 instanceof Integer)) {
                                            break;
                                        } else {
                                            this.f7013.putInt(r6, ((Integer) obj2).intValue());
                                            break;
                                        }
                                    case 3:
                                        if (!(obj2 instanceof Double)) {
                                            break;
                                        } else {
                                            this.f7013.putDouble(r6, ((Double) obj2).doubleValue());
                                            break;
                                        }
                                    case 4:
                                        if ((obj2 instanceof String) && zzbdf.m10136((String) obj2) != null) {
                                            this.f7013.putString(r6, (String) obj2);
                                            break;
                                        }
                                }
                            }
                        } catch (JSONException e) {
                        }
                    }
                }
            }
        } catch (JSONException e2) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m7873(JSONObject jSONObject, String... strArr) {
        try {
            for (String str : strArr) {
                if (this.f7013.containsKey(str)) {
                    switch (f7010.m7887(str)) {
                        case 1:
                        case 4:
                            jSONObject.put(f7010.m7889(str), (Object) this.f7013.getString(str));
                            break;
                        case 2:
                            jSONObject.put(f7010.m7889(str), this.f7013.getInt(str));
                            break;
                        case 3:
                            jSONObject.put(f7010.m7889(str), this.f7013.getDouble(str));
                            break;
                    }
                }
            }
            for (String str2 : this.f7013.keySet()) {
                if (!str2.startsWith("com.google.")) {
                    Object obj = this.f7013.get(str2);
                    if (obj instanceof String) {
                        jSONObject.put(str2, obj);
                    } else if (obj instanceof Integer) {
                        jSONObject.put(str2, obj);
                    } else if (obj instanceof Double) {
                        jSONObject.put(str2, obj);
                    }
                }
            }
        } catch (JSONException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m7874(Bundle bundle, Bundle bundle2) {
        if (bundle.size() != bundle2.size()) {
            return false;
        }
        for (String str : bundle.keySet()) {
            Object obj = bundle.get(str);
            Object obj2 = bundle2.get(str);
            if ((obj instanceof Bundle) && (obj2 instanceof Bundle) && !m7874((Bundle) obj, (Bundle) obj2)) {
                return false;
            }
            if (obj == null) {
                if (obj2 != null || !bundle2.containsKey(str)) {
                    return false;
                }
            } else if (!obj.equals(obj2)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaMetadata)) {
            return false;
        }
        MediaMetadata mediaMetadata = (MediaMetadata) obj;
        return m7874(this.f7013, mediaMetadata.f7013) && this.f7014.equals(mediaMetadata.f7014);
    }

    public int hashCode() {
        int i = 17;
        Iterator it2 = this.f7013.keySet().iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return (i2 * 31) + this.f7014.hashCode();
            }
            i = this.f7013.get((String) it2.next()).hashCode() + (i2 * 31);
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10180(parcel, 2, m7878(), false);
        zzbfp.m10187(parcel, 3, this.f7013, false);
        zzbfp.m10185(parcel, 4, m7880());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m7875() {
        return this.f7014 != null && !this.f7014.isEmpty();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7876(String str) {
        m7871(str, 1);
        return this.f7013.getString(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m7877() {
        this.f7013.clear();
        this.f7014.clear();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public List<WebImage> m7878() {
        return this.f7014;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final JSONObject m7879() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("metadataType", this.f7012);
        } catch (JSONException e) {
        }
        zzbdf.m10138(jSONObject, this.f7014);
        switch (this.f7012) {
            case 0:
                m7873(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.ARTIST", "com.google.android.gms.cast.metadata.SUBTITLE", "com.google.android.gms.cast.metadata.RELEASE_DATE");
                break;
            case 1:
                m7873(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.STUDIO", "com.google.android.gms.cast.metadata.SUBTITLE", "com.google.android.gms.cast.metadata.RELEASE_DATE");
                break;
            case 2:
                m7873(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.SERIES_TITLE", "com.google.android.gms.cast.metadata.SEASON_NUMBER", "com.google.android.gms.cast.metadata.EPISODE_NUMBER", "com.google.android.gms.cast.metadata.BROADCAST_DATE");
                break;
            case 3:
                m7873(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.ARTIST", "com.google.android.gms.cast.metadata.ALBUM_TITLE", "com.google.android.gms.cast.metadata.ALBUM_ARTIST", "com.google.android.gms.cast.metadata.COMPOSER", "com.google.android.gms.cast.metadata.TRACK_NUMBER", "com.google.android.gms.cast.metadata.DISC_NUMBER", "com.google.android.gms.cast.metadata.RELEASE_DATE");
                break;
            case 4:
                m7873(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.ARTIST", "com.google.android.gms.cast.metadata.LOCATION_NAME", "com.google.android.gms.cast.metadata.LOCATION_LATITUDE", "com.google.android.gms.cast.metadata.LOCATION_LONGITUDE", "com.google.android.gms.cast.metadata.WIDTH", "com.google.android.gms.cast.metadata.HEIGHT", "com.google.android.gms.cast.metadata.CREATION_DATE");
                break;
            default:
                m7873(jSONObject, new String[0]);
                break;
        }
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m7880() {
        return this.f7012;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7881(WebImage webImage) {
        this.f7014.add(webImage);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7882(String str, int i) {
        m7871(str, 2);
        this.f7013.putInt(str, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7883(String str, String str2) {
        m7871(str, 1);
        this.f7013.putString(str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7884(JSONObject jSONObject) {
        m7877();
        this.f7012 = 0;
        try {
            this.f7012 = jSONObject.getInt("metadataType");
        } catch (JSONException e) {
        }
        zzbdf.m10137(this.f7014, jSONObject);
        switch (this.f7012) {
            case 0:
                m7872(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.ARTIST", "com.google.android.gms.cast.metadata.SUBTITLE", "com.google.android.gms.cast.metadata.RELEASE_DATE");
                return;
            case 1:
                m7872(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.STUDIO", "com.google.android.gms.cast.metadata.SUBTITLE", "com.google.android.gms.cast.metadata.RELEASE_DATE");
                return;
            case 2:
                m7872(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.SERIES_TITLE", "com.google.android.gms.cast.metadata.SEASON_NUMBER", "com.google.android.gms.cast.metadata.EPISODE_NUMBER", "com.google.android.gms.cast.metadata.BROADCAST_DATE");
                return;
            case 3:
                m7872(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.ALBUM_TITLE", "com.google.android.gms.cast.metadata.ARTIST", "com.google.android.gms.cast.metadata.ALBUM_ARTIST", "com.google.android.gms.cast.metadata.COMPOSER", "com.google.android.gms.cast.metadata.TRACK_NUMBER", "com.google.android.gms.cast.metadata.DISC_NUMBER", "com.google.android.gms.cast.metadata.RELEASE_DATE");
                return;
            case 4:
                m7872(jSONObject, "com.google.android.gms.cast.metadata.TITLE", "com.google.android.gms.cast.metadata.ARTIST", "com.google.android.gms.cast.metadata.LOCATION_NAME", "com.google.android.gms.cast.metadata.LOCATION_LATITUDE", "com.google.android.gms.cast.metadata.LOCATION_LONGITUDE", "com.google.android.gms.cast.metadata.WIDTH", "com.google.android.gms.cast.metadata.HEIGHT", "com.google.android.gms.cast.metadata.CREATION_DATE");
                return;
            default:
                m7872(jSONObject, new String[0]);
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7885(String str) {
        return this.f7013.containsKey(str);
    }
}
