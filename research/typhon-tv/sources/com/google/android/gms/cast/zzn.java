package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzn implements Parcelable.Creator<CastDevice> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        ArrayList<WebImage> arrayList = null;
        int i2 = 0;
        int i3 = -1;
        String str6 = null;
        String str7 = null;
        int i4 = 0;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 8:
                    arrayList = zzbfn.m10167(parcel, readInt, WebImage.CREATOR);
                    break;
                case 9:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 10:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 11:
                    str6 = zzbfn.m10162(parcel, readInt);
                    break;
                case 12:
                    str7 = zzbfn.m10162(parcel, readInt);
                    break;
                case 13:
                    i4 = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new CastDevice(str, str2, str3, str4, str5, i, arrayList, i2, i3, str6, str7, i4);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CastDevice[i];
    }
}
