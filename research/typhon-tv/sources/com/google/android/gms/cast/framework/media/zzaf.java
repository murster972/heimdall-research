package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbdb;
import java.io.IOException;
import org.json.JSONObject;

final class zzaf extends RemoteMediaClient.zzb {

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7362;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ JSONObject f7363;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaf(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, JSONObject jSONObject) {
        super(remoteMediaClient, googleApiClient);
        this.f7362 = remoteMediaClient;
        this.f7363 = jSONObject;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8330(zzbcf zzbcf) {
        synchronized (this.f7362.f3638) {
            try {
                this.f7362.f3639.m10109(this.f7272, this.f7363);
            } catch (zzbdb | IOException e) {
                m4198((RemoteMediaClient.MediaChannelResult) m4195(new Status(2100)));
            }
        }
    }
}
