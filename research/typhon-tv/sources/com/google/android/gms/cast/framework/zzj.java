package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzev;

public interface zzj extends IInterface {

    public static abstract class zza extends zzev implements zzj {
        /* renamed from: 龘  reason: contains not printable characters */
        public static zzj m8390(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.ICastContext");
            return queryLocalInterface instanceof zzj ? (zzj) queryLocalInterface : new zzk(iBinder);
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    zzv m8386() throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    IObjectWrapper m8387() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    zzp m8388() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    Bundle m8389() throws RemoteException;
}
