package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zza implements Parcelable.Creator<AdBreakClipInfo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        long j = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str6 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 5:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new AdBreakClipInfo(str6, str5, j, str4, str3, str2, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdBreakClipInfo[i];
    }
}
