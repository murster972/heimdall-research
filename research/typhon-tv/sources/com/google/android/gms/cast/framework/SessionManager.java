package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzbcy;

public class SessionManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7137 = new zzbcy("SessionManager");

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzv f7138;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f7139;

    public SessionManager(zzv zzv, Context context) {
        this.f7138 = zzv;
        this.f7139 = context;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public CastSession m8075() {
        zzbq.m9115("Must be called from the main thread.");
        Session r0 = m8080();
        if (r0 == null || !(r0 instanceof CastSession)) {
            return null;
        }
        return (CastSession) r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8076(SessionManagerListener<Session> sessionManagerListener) {
        zzbq.m9115("Must be called from the main thread.");
        m8077(sessionManagerListener, Session.class);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public <T extends Session> void m8077(SessionManagerListener<T> sessionManagerListener, Class cls) {
        zzbq.m9120(cls);
        zzbq.m9115("Must be called from the main thread.");
        if (sessionManagerListener != null) {
            try {
                this.f7138.m8439(new zzae(sessionManagerListener, cls));
            } catch (RemoteException e) {
                f7137.m10091(e, "Unable to call %s on %s.", "removeSessionManagerListener", zzv.class.getSimpleName());
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final IObjectWrapper m8078() {
        try {
            return this.f7138.m8438();
        } catch (RemoteException e) {
            f7137.m10091(e, "Unable to call %s on %s.", "getWrappedThis", zzv.class.getSimpleName());
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final int m8079() {
        try {
            return this.f7138.m8440();
        } catch (RemoteException e) {
            f7137.m10091(e, "Unable to call %s on %s.", "addCastStateListener", zzv.class.getSimpleName());
            return 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Session m8080() {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return (Session) zzn.m9307(this.f7138.m8441());
        } catch (RemoteException e) {
            f7137.m10091(e, "Unable to call %s on %s.", "getWrappedCurrentSession", zzv.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8081(SessionManagerListener<Session> sessionManagerListener) throws NullPointerException {
        zzbq.m9115("Must be called from the main thread.");
        m8082(sessionManagerListener, Session.class);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T extends Session> void m8082(SessionManagerListener<T> sessionManagerListener, Class<T> cls) throws NullPointerException {
        zzbq.m9120(sessionManagerListener);
        zzbq.m9120(cls);
        zzbq.m9115("Must be called from the main thread.");
        try {
            this.f7138.m8442(new zzae(sessionManagerListener, cls));
        } catch (RemoteException e) {
            f7137.m10091(e, "Unable to call %s on %s.", "addSessionManagerListener", zzv.class.getSimpleName());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8083(boolean z) {
        zzbq.m9115("Must be called from the main thread.");
        try {
            this.f7138.m8443(true, z);
        } catch (RemoteException e) {
            f7137.m10091(e, "Unable to call %s on %s.", "endCurrentSession", zzv.class.getSimpleName());
        }
    }
}
