package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.Keep;
import com.google.android.gms.R;
import com.google.android.gms.internal.zzdmr;

class InnerZoneDrawable extends Drawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f7144;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f7145 = 1.0f;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f7146;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f7147;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f7148;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f7149;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f7150;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Paint f7151 = new Paint();

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f7152;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Rect f7153 = new Rect();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f7154 = new Paint();

    public InnerZoneDrawable(Context context) {
        Resources resources = context.getResources();
        this.f7152 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_inner_radius);
        this.f7150 = resources.getInteger(R.integer.cast_libraries_material_featurehighlight_pulse_base_alpha);
        this.f7154.setAntiAlias(true);
        this.f7154.setStyle(Paint.Style.FILL);
        this.f7151.setAntiAlias(true);
        this.f7151.setStyle(Paint.Style.FILL);
        this.f7154.setColor(-1);
        this.f7151.setColor(-1);
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        if (this.f7149 > 0.0f) {
            float f = this.f7144 * this.f7148;
            this.f7151.setAlpha((int) (((float) this.f7150) * this.f7149));
            canvas.drawCircle(this.f7146, this.f7147, f, this.f7151);
        }
        canvas.drawCircle(this.f7146, this.f7147, this.f7144 * this.f7145, this.f7154);
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i) {
        this.f7154.setAlpha(i);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f7154.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @Keep
    public void setPulseAlpha(float f) {
        this.f7149 = f;
        invalidateSelf();
    }

    @Keep
    public void setPulseScale(float f) {
        this.f7148 = f;
        invalidateSelf();
    }

    @Keep
    public void setScale(float f) {
        this.f7145 = f;
        invalidateSelf();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Animator m8101() {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{0.0f}), PropertyValuesHolder.ofInt("alpha", new int[]{0}), PropertyValuesHolder.ofFloat("pulseScale", new float[]{0.0f}), PropertyValuesHolder.ofFloat("pulseAlpha", new float[]{0.0f})});
        ofPropertyValuesHolder.setInterpolator(zzdmr.m11641());
        return ofPropertyValuesHolder.setDuration(200);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8102(Rect rect) {
        this.f7153.set(rect);
        this.f7146 = this.f7153.exactCenterX();
        this.f7147 = this.f7153.exactCenterY();
        this.f7144 = Math.max((float) this.f7152, Math.max(((float) this.f7153.width()) / 2.0f, ((float) this.f7153.height()) / 2.0f));
        invalidateSelf();
    }
}
