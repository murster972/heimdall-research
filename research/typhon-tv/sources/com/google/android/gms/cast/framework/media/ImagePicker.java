package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.zzb;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

public class ImagePicker {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzb f3629 = new zza();

    class zza extends zzb.zza {
        private zza() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final IObjectWrapper m8147() {
            return zzn.m9306(ImagePicker.this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final int m8148() {
            return 11910208;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final WebImage m8149(MediaMetadata mediaMetadata, int i) {
            return ImagePicker.this.m4120(mediaMetadata, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final WebImage m8150(MediaMetadata mediaMetadata, ImageHints imageHints) {
            return ImagePicker.this.m4121(mediaMetadata, imageHints);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzb m4119() {
        return this.f3629;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public WebImage m4120(MediaMetadata mediaMetadata, int i) {
        if (mediaMetadata == null || !mediaMetadata.m7875()) {
            return null;
        }
        return mediaMetadata.m7878().get(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public WebImage m4121(MediaMetadata mediaMetadata, ImageHints imageHints) {
        return m4120(mediaMetadata, imageHints.m8146());
    }
}
