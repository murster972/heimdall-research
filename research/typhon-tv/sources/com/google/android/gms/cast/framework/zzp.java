package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzp extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m8408() throws RemoteException;
}
