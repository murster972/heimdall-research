package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzc implements Parcelable.Creator<AdBreakStatus> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        long j = 0;
        String str = null;
        String str2 = null;
        long j2 = 0;
        long j3 = 0;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    j3 = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new AdBreakStatus(j3, j2, str2, str, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdBreakStatus[i];
    }
}
