package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbcf;
import java.io.IOException;

final class zzy extends RemoteMediaClient.zzb {

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7392;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ MediaLoadOptions f7393;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ MediaInfo f7394;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzy(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, MediaInfo mediaInfo, MediaLoadOptions mediaLoadOptions) {
        super(remoteMediaClient, googleApiClient);
        this.f7392 = remoteMediaClient;
        this.f7394 = mediaInfo;
        this.f7393 = mediaLoadOptions;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8362(zzbcf zzbcf) {
        synchronized (this.f7392.f3638) {
            try {
                this.f7392.f3639.m10108(this.f7272, this.f7394, this.f7393);
            } catch (IOException | IllegalStateException e) {
                m4198((RemoteMediaClient.MediaChannelResult) m4195(new Status(2100)));
            }
        }
    }
}
