package com.google.android.gms.cast;

import org.json.JSONObject;

public class MediaLoadOptions {

    /* renamed from: 连任  reason: contains not printable characters */
    private JSONObject f7000;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f7001;

    /* renamed from: 麤  reason: contains not printable characters */
    private long[] f7002;

    /* renamed from: 齉  reason: contains not printable characters */
    private double f7003;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f7004;

    public static class Builder {

        /* renamed from: 连任  reason: contains not printable characters */
        private JSONObject f7005 = null;

        /* renamed from: 靐  reason: contains not printable characters */
        private long f7006 = 0;

        /* renamed from: 麤  reason: contains not printable characters */
        private long[] f7007 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        private double f7008 = 1.0d;

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean f7009 = true;

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7866(long j) {
            this.f7006 = j;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7867(JSONObject jSONObject) {
            this.f7005 = jSONObject;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7868(boolean z) {
            this.f7009 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7869(long[] jArr) {
            this.f7007 = jArr;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaLoadOptions m7870() {
            return new MediaLoadOptions(this.f7009, this.f7006, this.f7008, this.f7007, this.f7005);
        }
    }

    private MediaLoadOptions(boolean z, long j, double d, long[] jArr, JSONObject jSONObject) {
        this.f7004 = z;
        this.f7001 = j;
        this.f7003 = d;
        this.f7002 = jArr;
        this.f7000 = jSONObject;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public JSONObject m7861() {
        return this.f7000;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m7862() {
        return this.f7001;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long[] m7863() {
        return this.f7002;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public double m7864() {
        return this.f7003;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7865() {
        return this.f7004;
    }
}
