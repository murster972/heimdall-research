package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzu extends zzeu implements zzt {
    zzu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ISession");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m8429() throws RemoteException {
        Parcel r0 = m12300(9, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m8430() throws RemoteException {
        Parcel r0 = m12300(8, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8431(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(13, v_);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8432() throws RemoteException {
        Parcel r0 = m12300(5, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m8433() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8434(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(15, v_);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m8435() throws RemoteException {
        Parcel r0 = m12300(6, v_());
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m8436() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8437(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(12, v_);
    }
}
