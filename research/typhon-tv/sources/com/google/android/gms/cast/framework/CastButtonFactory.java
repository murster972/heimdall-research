package com.google.android.gms.cast.framework;

import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.app.MediaRouteDialogFactory;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.common.internal.zzbq;
import java.util.Locale;

public final class CastButtonFactory {
    /* renamed from: 龘  reason: contains not printable characters */
    public static MenuItem m7974(Context context, Menu menu, int i) {
        return m7975(context, menu, i, (MediaRouteDialogFactory) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MenuItem m7975(Context context, Menu menu, int i, MediaRouteDialogFactory mediaRouteDialogFactory) {
        zzbq.m9115("Must be called from the main thread.");
        zzbq.m9120(menu);
        CastContext r1 = CastContext.m7977(context);
        MenuItem findItem = menu.findItem(i);
        if (findItem == null) {
            throw new IllegalArgumentException(String.format(Locale.ROOT, "menu doesn't contain a menu item whose ID is %d.", new Object[]{Integer.valueOf(i)}));
        }
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat.getActionProvider(findItem);
        if (mediaRouteActionProvider == null) {
            throw new IllegalArgumentException(String.format(Locale.ROOT, "menu item with ID %d doesn't have a MediaRouteActionProvider.", new Object[]{Integer.valueOf(i)}));
        }
        mediaRouteActionProvider.m699(r1.m7983());
        if (mediaRouteDialogFactory != null) {
            mediaRouteActionProvider.m698(mediaRouteDialogFactory);
        }
        return findItem;
    }
}
