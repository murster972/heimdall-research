package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.View;

final class zzd implements View.OnLayoutChangeListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zza f7186;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Runnable f7187 = null;

    zzd(zza zza, Runnable runnable) {
        this.f7186 = zza;
    }

    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        if (this.f7187 != null) {
            this.f7187.run();
        }
        this.f7186.m8125();
        this.f7186.removeOnLayoutChangeListener(this);
    }
}
