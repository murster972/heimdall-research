package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import java.util.Set;
import java.util.TimerTask;

final class zzaq extends TimerTask {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient.zze f7376;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7377;

    zzaq(RemoteMediaClient.zze zze, RemoteMediaClient remoteMediaClient) {
        this.f7376 = zze;
        this.f7377 = remoteMediaClient;
    }

    public final void run() {
        RemoteMediaClient.this.m4134((Set<RemoteMediaClient.ProgressListener>) this.f7376.f7276);
        RemoteMediaClient.this.f3640.postDelayed(this, this.f7376.f7278);
    }
}
