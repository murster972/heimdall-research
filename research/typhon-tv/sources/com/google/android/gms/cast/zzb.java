package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzb implements Parcelable.Creator<AdBreakInfo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r10 = zzbfn.m10169(parcel);
        boolean z = false;
        String[] strArr = null;
        long j = 0;
        String str = null;
        long j2 = 0;
        while (parcel.dataPosition() < r10) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 5:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 6:
                    strArr = zzbfn.m10157(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r10);
        return new AdBreakInfo(j2, str, j, z, strArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdBreakInfo[i];
    }
}
