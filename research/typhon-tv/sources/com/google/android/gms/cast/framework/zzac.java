package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzev;
import com.google.android.gms.internal.zzew;

public abstract class zzac extends zzev implements zzab {
    public zzac() {
        attachInterface(this, "com.google.android.gms.cast.framework.ISessionProxy");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                IObjectWrapper r0 = m8368();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r0);
                break;
            case 2:
                m8365((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 3:
                m8366((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 4:
                m8370(zzew.m12308(parcel));
                parcel2.writeNoException();
                break;
            case 5:
                long r2 = m8364();
                parcel2.writeNoException();
                parcel2.writeLong(r2);
                break;
            case 6:
                parcel2.writeNoException();
                parcel2.writeInt(11910208);
                break;
            case 7:
                m8369((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 8:
                m8367((Bundle) zzew.m12304(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
