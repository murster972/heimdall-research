package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.cast.LaunchOptions;

public interface zzh extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m8382(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8383(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8384(String str, LaunchOptions launchOptions) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8385(String str, String str2) throws RemoteException;
}
