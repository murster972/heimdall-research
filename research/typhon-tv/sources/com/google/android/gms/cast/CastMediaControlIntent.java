package com.google.android.gms.cast;

import com.google.android.gms.internal.zzbcm;
import java.util.Collection;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public final class CastMediaControlIntent {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7828(String str) throws IllegalArgumentException {
        if (str != null) {
            return m7829("com.google.android.gms.cast.CATEGORY_CAST", str, (Collection<String>) null, false);
        }
        throw new IllegalArgumentException("applicationId cannot be null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m7829(String str, String str2, Collection<String> collection, boolean z) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder(str);
        if (str2 != null) {
            String upperCase = str2.toUpperCase();
            if (!upperCase.matches("[A-F0-9]+")) {
                String valueOf = String.valueOf(str2);
                throw new IllegalArgumentException(valueOf.length() != 0 ? "Invalid application ID: ".concat(valueOf) : new String("Invalid application ID: "));
            }
            sb.append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).append(upperCase);
        }
        if (collection != null) {
            if (collection.isEmpty()) {
                throw new IllegalArgumentException("Must specify at least one namespace");
            }
            if (str2 == null) {
                sb.append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
            }
            sb.append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
            boolean z2 = true;
            for (String next : collection) {
                zzbcm.m10042(next);
                if (z2) {
                    z2 = false;
                } else {
                    sb.append(",");
                }
                sb.append(zzbcm.m10040(next));
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7830(String str, Collection<String> collection) {
        if (str == null) {
            throw new IllegalArgumentException("applicationId cannot be null");
        } else if (collection != null) {
            return m7829("com.google.android.gms.cast.CATEGORY_CAST", str, collection, false);
        } else {
            throw new IllegalArgumentException("namespaces cannot be null");
        }
    }
}
