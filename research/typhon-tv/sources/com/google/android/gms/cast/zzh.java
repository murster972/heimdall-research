package com.google.android.gms.cast;

import android.os.RemoteException;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.internal.zzbcf;

final class zzh extends Cast.zza {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ LaunchOptions f7404;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f7405;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzh(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str, LaunchOptions launchOptions) {
        super(googleApiClient);
        this.f7405 = str;
        this.f7404 = launchOptions;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m8466(Api.zzb zzb) throws RemoteException {
        m7818((zzbcf) zzb);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8467(zzbcf zzbcf) throws RemoteException {
        try {
            zzbcf.m10013(this.f7405, this.f7404, (zzn<Cast.ApplicationConnectionResult>) this);
        } catch (IllegalStateException e) {
            m9957(2001);
        }
    }
}
