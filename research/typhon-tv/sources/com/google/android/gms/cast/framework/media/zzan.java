package com.google.android.gms.cast.framework.media;

import android.util.Log;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbdd;
import org.json.JSONObject;

final class zzan implements zzbdd {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient.zzb f7372;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7373;

    zzan(RemoteMediaClient.zzb zzb, RemoteMediaClient remoteMediaClient) {
        this.f7372 = zzb;
        this.f7373 = remoteMediaClient;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8333(long j) {
        try {
            this.f7372.m4198((RemoteMediaClient.MediaChannelResult) this.f7372.m8201(new Status(2103)));
        } catch (IllegalStateException e) {
            Log.e("RemoteMediaClient", "Result already set when calling onRequestReplaced", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8334(long j, int i, Object obj) {
        try {
            this.f7372.m4198(new RemoteMediaClient.zzc(new Status(i), obj instanceof JSONObject ? (JSONObject) obj : null));
        } catch (IllegalStateException e) {
            Log.e("RemoteMediaClient", "Result already set when calling onRequestCompleted", e);
        }
    }
}
