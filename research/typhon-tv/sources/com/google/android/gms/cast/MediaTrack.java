package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaTrack extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<MediaTrack> CREATOR = new zzak();

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f7049;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7050;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f7051;

    /* renamed from: ˑ  reason: contains not printable characters */
    private JSONObject f7052;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f7053;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7054;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f7055;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f7056;

    /* renamed from: 龘  reason: contains not printable characters */
    private long f7057;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final MediaTrack f7058;

        public Builder(long j, int i) throws IllegalArgumentException {
            this.f7058 = new MediaTrack(j, i);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7938(String str) {
            this.f7058.m7930(str);
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m7939(String str) {
            this.f7058.m7932(str);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m7940(String str) {
            this.f7058.m7934(str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7941(int i) throws IllegalArgumentException {
            this.f7058.m7936(i);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7942(String str) {
            this.f7058.m7937(str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaTrack m7943() {
            return this.f7058;
        }
    }

    MediaTrack(long j, int i) throws IllegalArgumentException {
        this(0, 0, (String) null, (String) null, (String) null, (String) null, -1, (String) null);
        this.f7057 = j;
        if (i <= 0 || i > 3) {
            throw new IllegalArgumentException(new StringBuilder(24).append("invalid type ").append(i).toString());
        }
        this.f7054 = i;
    }

    MediaTrack(long j, int i, String str, String str2, String str3, String str4, int i2, String str5) {
        this.f7057 = j;
        this.f7054 = i;
        this.f7056 = str;
        this.f7055 = str2;
        this.f7053 = str3;
        this.f7049 = str4;
        this.f7050 = i2;
        this.f7051 = str5;
        if (this.f7051 != null) {
            try {
                this.f7052 = new JSONObject(this.f7051);
            } catch (JSONException e) {
                this.f7052 = null;
                this.f7051 = null;
            }
        } else {
            this.f7052 = null;
        }
    }

    MediaTrack(JSONObject jSONObject) throws JSONException {
        this(0, 0, (String) null, (String) null, (String) null, (String) null, -1, (String) null);
        this.f7057 = jSONObject.getLong("trackId");
        String string = jSONObject.getString(VastExtensionXmlManager.TYPE);
        if ("TEXT".equals(string)) {
            this.f7054 = 1;
        } else if ("AUDIO".equals(string)) {
            this.f7054 = 2;
        } else if ("VIDEO".equals(string)) {
            this.f7054 = 3;
        } else {
            String valueOf = String.valueOf(string);
            throw new JSONException(valueOf.length() != 0 ? "invalid type: ".concat(valueOf) : new String("invalid type: "));
        }
        this.f7056 = jSONObject.optString("trackContentId", (String) null);
        this.f7055 = jSONObject.optString("trackContentType", (String) null);
        this.f7053 = jSONObject.optString("name", (String) null);
        this.f7049 = jSONObject.optString("language", (String) null);
        if (jSONObject.has("subtype")) {
            String string2 = jSONObject.getString("subtype");
            if ("SUBTITLES".equals(string2)) {
                this.f7050 = 1;
            } else if ("CAPTIONS".equals(string2)) {
                this.f7050 = 2;
            } else if ("DESCRIPTIONS".equals(string2)) {
                this.f7050 = 3;
            } else if ("CHAPTERS".equals(string2)) {
                this.f7050 = 4;
            } else if ("METADATA".equals(string2)) {
                this.f7050 = 5;
            } else {
                String valueOf2 = String.valueOf(string2);
                throw new JSONException(valueOf2.length() != 0 ? "invalid subtype: ".concat(valueOf2) : new String("invalid subtype: "));
            }
        } else {
            this.f7050 = 0;
        }
        this.f7052 = jSONObject.optJSONObject("customData");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaTrack)) {
            return false;
        }
        MediaTrack mediaTrack = (MediaTrack) obj;
        if ((this.f7052 == null) == (mediaTrack.f7052 == null)) {
            return (this.f7052 == null || mediaTrack.f7052 == null || zzo.m9263(this.f7052, mediaTrack.f7052)) && this.f7057 == mediaTrack.f7057 && this.f7054 == mediaTrack.f7054 && zzbcm.m10043(this.f7056, mediaTrack.f7056) && zzbcm.m10043(this.f7055, mediaTrack.f7055) && zzbcm.m10043(this.f7053, mediaTrack.f7053) && zzbcm.m10043(this.f7049, mediaTrack.f7049) && this.f7050 == mediaTrack.f7050;
        }
        return false;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.f7057), Integer.valueOf(this.f7054), this.f7056, this.f7055, this.f7053, this.f7049, Integer.valueOf(this.f7050), String.valueOf(this.f7052)});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        this.f7051 = this.f7052 == null ? null : this.f7052.toString();
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10186(parcel, 2, m7935());
        zzbfp.m10185(parcel, 3, m7929());
        zzbfp.m10193(parcel, 4, m7933(), false);
        zzbfp.m10193(parcel, 5, m7931(), false);
        zzbfp.m10193(parcel, 6, m7928(), false);
        zzbfp.m10193(parcel, 7, m7925(), false);
        zzbfp.m10185(parcel, 8, m7926());
        zzbfp.m10193(parcel, 9, this.f7051, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m7925() {
        return this.f7049;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int m7926() {
        return this.f7050;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final JSONObject m7927() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("trackId", this.f7057);
            switch (this.f7054) {
                case 1:
                    jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "TEXT");
                    break;
                case 2:
                    jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "AUDIO");
                    break;
                case 3:
                    jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "VIDEO");
                    break;
            }
            if (this.f7056 != null) {
                jSONObject.put("trackContentId", (Object) this.f7056);
            }
            if (this.f7055 != null) {
                jSONObject.put("trackContentType", (Object) this.f7055);
            }
            if (this.f7053 != null) {
                jSONObject.put("name", (Object) this.f7053);
            }
            if (!TextUtils.isEmpty(this.f7049)) {
                jSONObject.put("language", (Object) this.f7049);
            }
            switch (this.f7050) {
                case 1:
                    jSONObject.put("subtype", (Object) "SUBTITLES");
                    break;
                case 2:
                    jSONObject.put("subtype", (Object) "CAPTIONS");
                    break;
                case 3:
                    jSONObject.put("subtype", (Object) "DESCRIPTIONS");
                    break;
                case 4:
                    jSONObject.put("subtype", (Object) "CHAPTERS");
                    break;
                case 5:
                    jSONObject.put("subtype", (Object) "METADATA");
                    break;
            }
            if (this.f7052 != null) {
                jSONObject.put("customData", (Object) this.f7052);
            }
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m7928() {
        return this.f7053;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m7929() {
        return this.f7054;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m7930(String str) {
        this.f7055 = str;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m7931() {
        return this.f7055;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m7932(String str) {
        this.f7049 = str;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final String m7933() {
        return this.f7056;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m7934(String str) {
        this.f7053 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m7935() {
        return this.f7057;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7936(int i) throws IllegalArgumentException {
        if (i < 0 || i > 5) {
            throw new IllegalArgumentException(new StringBuilder(27).append("invalid subtype ").append(i).toString());
        } else if (i == 0 || this.f7054 == 1) {
            this.f7050 = i;
        } else {
            throw new IllegalArgumentException("subtypes are only valid for text tracks");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7937(String str) {
        this.f7056 = str;
    }
}
