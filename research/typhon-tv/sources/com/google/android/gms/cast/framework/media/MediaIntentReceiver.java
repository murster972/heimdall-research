package com.google.android.gms.cast.framework.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import org.json.JSONObject;

public class MediaIntentReceiver extends BroadcastReceiver {
    /* renamed from: 靐  reason: contains not printable characters */
    private static RemoteMediaClient m8151(CastSession castSession) {
        if (castSession == null || !castSession.m8053()) {
            return null;
        }
        return castSession.m8022();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m8152(CastSession castSession) {
        RemoteMediaClient r0 = m8151(castSession);
        if (r0 != null) {
            r0.m4142();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m8153(CastSession castSession, long j) {
        RemoteMediaClient r0;
        if (j != 0 && (r0 = m8151(castSession)) != null && !r0.m4147() && !r0.m4144()) {
            r0.m4156(r0.m4148() + j);
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            SessionManager r4 = CastContext.m7977(context).m7981();
            char c = 65535;
            switch (action.hashCode()) {
                case -1699820260:
                    if (action.equals("com.google.android.gms.cast.framework.action.REWIND")) {
                        c = 4;
                        break;
                    }
                    break;
                case -945151566:
                    if (action.equals("com.google.android.gms.cast.framework.action.SKIP_NEXT")) {
                        c = 1;
                        break;
                    }
                    break;
                case -945080078:
                    if (action.equals("com.google.android.gms.cast.framework.action.SKIP_PREV")) {
                        c = 2;
                        break;
                    }
                    break;
                case -668151673:
                    if (action.equals("com.google.android.gms.cast.framework.action.STOP_CASTING")) {
                        c = 5;
                        break;
                    }
                    break;
                case -124479363:
                    if (action.equals("com.google.android.gms.cast.framework.action.DISCONNECT")) {
                        c = 6;
                        break;
                    }
                    break;
                case 235550565:
                    if (action.equals("com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1362116196:
                    if (action.equals("com.google.android.gms.cast.framework.action.FORWARD")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1997055314:
                    if (action.equals("android.intent.action.MEDIA_BUTTON")) {
                        c = 7;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    m8158(r4.m8080());
                    return;
                case 1:
                    m8154(r4.m8080());
                    return;
                case 2:
                    m8156(r4.m8080());
                    return;
                case 3:
                    m8159(r4.m8080(), intent.getLongExtra("googlecast-extra_skip_step_ms", 0));
                    return;
                case 4:
                    m8155(r4.m8080(), intent.getLongExtra("googlecast-extra_skip_step_ms", 0));
                    return;
                case 5:
                    r4.m8083(true);
                    return;
                case 6:
                    r4.m8083(false);
                    return;
                case 7:
                    m8160(r4.m8080(), intent);
                    return;
                default:
                    m8157(context, action, intent);
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m8154(Session session) {
        RemoteMediaClient r0;
        if ((session instanceof CastSession) && (r0 = m8151((CastSession) session)) != null && !r0.m4144()) {
            r0.m4153((JSONObject) null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m8155(Session session, long j) {
        if (session instanceof CastSession) {
            m8153((CastSession) session, -j);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m8156(Session session) {
        RemoteMediaClient r0;
        if ((session instanceof CastSession) && (r0 = m8151((CastSession) session)) != null && !r0.m4144()) {
            r0.m4155((JSONObject) null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8157(Context context, String str, Intent intent) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8158(Session session) {
        if (session instanceof CastSession) {
            m8152((CastSession) session);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8159(Session session, long j) {
        if (session instanceof CastSession) {
            m8153((CastSession) session, j);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8160(Session session, Intent intent) {
        KeyEvent keyEvent;
        if ((session instanceof CastSession) && intent.hasExtra("android.intent.extra.KEY_EVENT") && (keyEvent = (KeyEvent) intent.getExtras().get("android.intent.extra.KEY_EVENT")) != null && keyEvent.getAction() == 0 && keyEvent.getKeyCode() == 85) {
            m8152((CastSession) session);
        }
    }
}
