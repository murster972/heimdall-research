package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzak implements Parcelable.Creator<MediaTrack> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        long j = 0;
        String str = null;
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i2 = 0;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 3:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    str5 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    str4 = zzbfn.m10162(parcel, readInt);
                    break;
                case 6:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 8:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 9:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new MediaTrack(j, i2, str5, str4, str3, str2, i, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaTrack[i];
    }
}
