package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.internal.zzev;

public interface zzl extends IInterface {

    public static abstract class zza extends zzev implements zzl {
        /* renamed from: 龘  reason: contains not printable characters */
        public static zzl m8401(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.ICastSession");
            return queryLocalInterface instanceof zzl ? (zzl) queryLocalInterface : new zzm(iBinder);
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    void m8395(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8396(int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8397(Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8398(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8399(ConnectionResult connectionResult) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8400(boolean z, int i) throws RemoteException;
}
