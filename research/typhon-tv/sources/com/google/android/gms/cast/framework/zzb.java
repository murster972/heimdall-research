package com.google.android.gms.cast.framework;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzb implements Parcelable.Creator<CastOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        double d = 0.0d;
        boolean z = false;
        CastMediaOptions castMediaOptions = null;
        boolean z2 = false;
        LaunchOptions launchOptions = null;
        boolean z3 = false;
        ArrayList<String> arrayList = null;
        String str = null;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 4:
                    z3 = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    launchOptions = (LaunchOptions) zzbfn.m10171(parcel, readInt, LaunchOptions.CREATOR);
                    break;
                case 6:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 7:
                    castMediaOptions = (CastMediaOptions) zzbfn.m10171(parcel, readInt, CastMediaOptions.CREATOR);
                    break;
                case 8:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 9:
                    d = zzbfn.m10160(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new CastOptions(str, arrayList, z3, launchOptions, z2, castMediaOptions, z, d);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CastOptions[i];
    }
}
