package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzbn implements Parcelable.Creator<TextTrackStyle> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        float f = 0.0f;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        String str = null;
        int i8 = 0;
        int i9 = 0;
        String str2 = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    f = zzbfn.m10151(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 6:
                    i4 = zzbfn.m10166(parcel, readInt);
                    break;
                case 7:
                    i5 = zzbfn.m10166(parcel, readInt);
                    break;
                case 8:
                    i6 = zzbfn.m10166(parcel, readInt);
                    break;
                case 9:
                    i7 = zzbfn.m10166(parcel, readInt);
                    break;
                case 10:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 11:
                    i8 = zzbfn.m10166(parcel, readInt);
                    break;
                case 12:
                    i9 = zzbfn.m10166(parcel, readInt);
                    break;
                case 13:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new TextTrackStyle(f, i, i2, i3, i4, i5, i6, i7, str, i8, i9, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new TextTrackStyle[i];
    }
}
