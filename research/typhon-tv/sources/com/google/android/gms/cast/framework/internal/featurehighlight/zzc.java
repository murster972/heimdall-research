package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

final class zzc extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f7183 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzh f7184;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ View f7185;

    zzc(zza zza, View view, boolean z, zzh zzh) {
        this.f7185 = view;
        this.f7184 = zzh;
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        if (this.f7185.getParent() != null) {
            this.f7185.performClick();
        }
        if (!this.f7183) {
            return true;
        }
        this.f7184.m8131();
        return true;
    }
}
