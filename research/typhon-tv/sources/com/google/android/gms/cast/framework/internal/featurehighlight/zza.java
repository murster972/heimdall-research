package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.R;
import com.google.android.gms.internal.zzdmi;
import com.google.android.gms.internal.zzdmr;
import com.google.android.gms.internal.zzdmy;

public final class zza extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzi f7168;

    /* renamed from: ʼ  reason: contains not printable characters */
    private View f7169;

    /* renamed from: ʽ  reason: contains not printable characters */
    private View f7170;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public zzh f7171;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f7172;

    /* renamed from: ˈ  reason: contains not printable characters */
    private GestureDetectorCompat f7173;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public Animator f7174;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzj f7175;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final GestureDetectorCompat f7176;

    /* renamed from: 连任  reason: contains not printable characters */
    private final InnerZoneDrawable f7177;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Rect f7178 = new Rect();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final OuterHighlightDrawable f7179;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Rect f7180 = new Rect();

    /* renamed from: 龘  reason: contains not printable characters */
    private final int[] f7181 = new int[2];

    public zza(Context context) {
        super(context);
        setId(R.id.cast_featurehighlight_view);
        setWillNotDraw(false);
        this.f7177 = new InnerZoneDrawable(context);
        this.f7177.setCallback(this);
        this.f7179 = new OuterHighlightDrawable(context);
        this.f7179.setCallback(this);
        this.f7175 = new zzj(this);
        this.f7176 = new GestureDetectorCompat(context, new zzb(this));
        this.f7176.setIsLongpressEnabled(false);
        setVisibility(8);
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final Animator m8111() {
        InnerZoneDrawable innerZoneDrawable = this.f7177;
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator duration = ObjectAnimator.ofFloat(innerZoneDrawable, "scale", new float[]{1.0f, 1.1f}).setDuration(500);
        ObjectAnimator duration2 = ObjectAnimator.ofFloat(innerZoneDrawable, "scale", new float[]{1.1f, 1.0f}).setDuration(500);
        ObjectAnimator duration3 = ObjectAnimator.ofPropertyValuesHolder(innerZoneDrawable, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("pulseScale", new float[]{1.1f, 2.0f}), PropertyValuesHolder.ofFloat("pulseAlpha", new float[]{1.0f, 0.0f})}).setDuration(500);
        animatorSet.play(duration);
        animatorSet.play(duration2).with(duration3).after(duration);
        animatorSet.setInterpolator(zzdmr.m11642());
        animatorSet.setStartDelay(500);
        zzdmi.m11631(animatorSet, -1, (Runnable) null);
        return animatorSet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8117(Animator animator) {
        if (this.f7174 != null) {
            this.f7174.cancel();
        }
        this.f7174 = animator;
        this.f7174.start();
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8118(float f, float f2) {
        return this.f7180.contains(Math.round(f), Math.round(f2));
    }

    /* access modifiers changed from: protected */
    public final boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams;
    }

    /* access modifiers changed from: protected */
    public final ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-2, -2);
    }

    public final ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public final ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        canvas.save();
        this.f7179.draw(canvas);
        this.f7177.draw(canvas);
        if (this.f7169 != null) {
            if (this.f7169.getParent() != null) {
                Bitmap createBitmap = Bitmap.createBitmap(this.f7169.getWidth(), this.f7169.getHeight(), Bitmap.Config.ARGB_8888);
                this.f7169.draw(new Canvas(createBitmap));
                int r0 = this.f7179.m8107();
                int red = Color.red(r0);
                int green = Color.green(r0);
                int blue = Color.blue(r0);
                for (int i = 0; i < createBitmap.getHeight(); i++) {
                    for (int i2 = 0; i2 < createBitmap.getWidth(); i2++) {
                        int pixel = createBitmap.getPixel(i2, i);
                        if (Color.alpha(pixel) != 0) {
                            createBitmap.setPixel(i2, i, Color.argb(Color.alpha(pixel), red, green, blue));
                        }
                    }
                }
                canvas.drawBitmap(createBitmap, (float) this.f7178.left, (float) this.f7178.top, (Paint) null);
            }
            canvas.restore();
            return;
        }
        throw new IllegalStateException("Neither target view nor drawable was set");
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.f7169 == null) {
            throw new IllegalStateException("Target view must be set before layout");
        }
        if (this.f7169.getParent() != null) {
            int[] iArr = this.f7181;
            View view = this.f7169;
            getLocationInWindow(iArr);
            int i5 = iArr[0];
            int i6 = iArr[1];
            view.getLocationInWindow(iArr);
            iArr[0] = iArr[0] - i5;
            iArr[1] = iArr[1] - i6;
        }
        this.f7178.set(this.f7181[0], this.f7181[1], this.f7181[0] + this.f7169.getWidth(), this.f7181[1] + this.f7169.getHeight());
        this.f7180.set(i, i2, i3, i4);
        this.f7179.setBounds(this.f7180);
        this.f7177.setBounds(this.f7180);
        this.f7175.m8134(this.f7178, this.f7180);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        setMeasuredDimension(resolveSize(View.MeasureSpec.getSize(i), i), resolveSize(View.MeasureSpec.getSize(i2), i2));
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f7172 = this.f7178.contains((int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (this.f7172) {
            if (this.f7173 != null) {
                this.f7173.onTouchEvent(motionEvent);
                if (actionMasked == 1) {
                    motionEvent = MotionEvent.obtain(motionEvent);
                    motionEvent.setAction(3);
                }
            }
            if (this.f7169.getParent() != null) {
                this.f7169.onTouchEvent(motionEvent);
            }
        } else {
            this.f7176.onTouchEvent(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f7179 || drawable == this.f7177 || drawable == null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final View m8120() {
        return this.f7168.asView();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8121(Runnable runnable) {
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.f7168.asView(), "alpha", new float[]{0.0f}).setDuration(200);
        duration.setInterpolator(zzdmr.m11641());
        float exactCenterX = this.f7178.exactCenterX() - this.f7179.m8104();
        float exactCenterY = this.f7178.exactCenterY() - this.f7179.m8106();
        OuterHighlightDrawable outerHighlightDrawable = this.f7179;
        PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat("scale", new float[]{0.0f});
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt("alpha", new int[]{0});
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(outerHighlightDrawable, new PropertyValuesHolder[]{ofFloat, PropertyValuesHolder.ofFloat("translationX", new float[]{0.0f, exactCenterX}), PropertyValuesHolder.ofFloat("translationY", new float[]{0.0f, exactCenterY}), ofInt});
        ofPropertyValuesHolder.setInterpolator(zzdmr.m11641());
        Animator duration2 = ofPropertyValuesHolder.setDuration(200);
        Animator r2 = this.f7177.m8101();
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(new Animator[]{duration, duration2, r2});
        animatorSet.addListener(new zzg(this, runnable));
        m8117((Animator) animatorSet);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final InnerZoneDrawable m8122() {
        return this.f7177;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final OuterHighlightDrawable m8123() {
        return this.f7179;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8124(Runnable runnable) {
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.f7168.asView(), "alpha", new float[]{0.0f}).setDuration(200);
        duration.setInterpolator(zzdmr.m11641());
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.f7179, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{1.125f}), PropertyValuesHolder.ofInt("alpha", new int[]{0})});
        ofPropertyValuesHolder.setInterpolator(zzdmr.m11641());
        Animator duration2 = ofPropertyValuesHolder.setDuration(200);
        Animator r2 = this.f7177.m8101();
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(new Animator[]{duration, duration2, r2});
        animatorSet.addListener(new zzf(this, runnable));
        m8117((Animator) animatorSet);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8125() {
        if (this.f7169 == null) {
            throw new IllegalStateException("Target view must be set before animation");
        }
        setVisibility(0);
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.f7168.asView(), "alpha", new float[]{0.0f, 1.0f}).setDuration(350);
        duration.setInterpolator(zzdmr.m11643());
        Animator r1 = this.f7179.m8105(this.f7178.exactCenterX() - this.f7179.m8104(), this.f7178.exactCenterY() - this.f7179.m8106());
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.f7177, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{0.0f, 1.0f}), PropertyValuesHolder.ofInt("alpha", new int[]{0, 255})});
        ofPropertyValuesHolder.setInterpolator(zzdmr.m11643());
        Animator duration2 = ofPropertyValuesHolder.setDuration(350);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(new Animator[]{duration, r1, duration2});
        animatorSet.addListener(new zze(this));
        m8117((Animator) animatorSet);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8126(int i) {
        this.f7179.m8108(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8127(View view, View view2, boolean z, zzh zzh) {
        this.f7169 = (View) zzdmy.m11648(view);
        this.f7170 = null;
        this.f7171 = (zzh) zzdmy.m11648(zzh);
        this.f7173 = new GestureDetectorCompat(getContext(), new zzc(this, view, true, zzh));
        this.f7173.setIsLongpressEnabled(false);
        setVisibility(4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8128(zzi zzi) {
        this.f7168 = (zzi) zzdmy.m11648(zzi);
        addView(zzi.asView(), 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8129(Runnable runnable) {
        addOnLayoutChangeListener(new zzd(this, (Runnable) null));
    }
}
