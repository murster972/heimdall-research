package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.internal.zzbda;
import java.util.List;

final class zzn implements zzbda {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7386;

    zzn(RemoteMediaClient remoteMediaClient) {
        this.f7386 = remoteMediaClient;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m8353() {
        MediaStatus r0;
        if (this.f7386.f3636 != null && (r0 = this.f7386.m4136()) != null) {
            r0.m7922(this.f7386.f3636.m8196(r0));
            List<AdBreakInfo> r02 = this.f7386.f3636.m8195(r0);
            MediaInfo r1 = this.f7386.m4137();
            if (r1 != null) {
                r1.m7845(r02);
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m8354() {
        for (RemoteMediaClient.Listener r0 : this.f7386.f3633) {
            r0.m8189();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8355() {
        m8353();
        for (RemoteMediaClient.Listener r0 : this.f7386.f3633) {
            r0.m8191();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8356() {
        for (RemoteMediaClient.Listener r0 : this.f7386.f3633) {
            r0.m8192();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8357() {
        for (RemoteMediaClient.Listener r0 : this.f7386.f3633) {
            r0.m8193();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8358() {
        m8353();
        this.f7386.m4126();
        for (RemoteMediaClient.Listener r0 : this.f7386.f3633) {
            r0.m8194();
        }
    }
}
