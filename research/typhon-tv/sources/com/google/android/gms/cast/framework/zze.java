package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzbcy;

public final class zze {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7399 = new zzbcy("DiscoveryManager");

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzp f7400;

    zze(zzp zzp) {
        this.f7400 = zzp;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m8381() {
        try {
            return this.f7400.m8408();
        } catch (RemoteException e) {
            f7399.m10091(e, "Unable to call %s on %s.", "getWrappedThis", zzp.class.getSimpleName());
            return null;
        }
    }
}
