package com.google.android.gms.cast.framework;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.internal.zzayu;
import com.google.android.gms.internal.zzbcy;

public class ReconnectionService extends Service {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7131 = new zzbcy("ReconnectionService");

    /* renamed from: 靐  reason: contains not printable characters */
    private zzr f7132;

    public IBinder onBind(Intent intent) {
        try {
            return this.f7132.m8412(intent);
        } catch (RemoteException e) {
            f7131.m10091(e, "Unable to call %s on %s.", "onBind", zzr.class.getSimpleName());
            return null;
        }
    }

    public void onCreate() {
        CastContext r0 = CastContext.m7977((Context) this);
        this.f7132 = zzayu.m9763(this, r0.m7981().m8078(), r0.m7980().m8381());
        try {
            this.f7132.m8413();
        } catch (RemoteException e) {
            f7131.m10091(e, "Unable to call %s on %s.", "onCreate", zzr.class.getSimpleName());
        }
        super.onCreate();
    }

    public void onDestroy() {
        try {
            this.f7132.m8410();
        } catch (RemoteException e) {
            f7131.m10091(e, "Unable to call %s on %s.", "onDestroy", zzr.class.getSimpleName());
        }
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        try {
            return this.f7132.m8411(intent, i, i2);
        } catch (RemoteException e) {
            f7131.m10091(e, "Unable to call %s on %s.", "onStartCommand", zzr.class.getSimpleName());
            return 1;
        }
    }
}
