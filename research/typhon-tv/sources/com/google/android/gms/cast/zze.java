package com.google.android.gms.cast;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.internal.zzbcf;

final class zze extends Api.zza<zzbcf, Cast.CastOptions> {
    zze() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Api.zze m8463(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Cast.CastOptions castOptions = (Cast.CastOptions) obj;
        zzbq.m9121(castOptions, (Object) "Setting the API options is required.");
        return new zzbcf(context, looper, zzr, castOptions.f6967, (long) castOptions.f6965, castOptions.f6964, castOptions.f6966, connectionCallbacks, onConnectionFailedListener);
    }
}
