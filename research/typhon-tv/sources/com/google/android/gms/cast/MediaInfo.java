package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaInfo extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<MediaInfo> CREATOR = new zzae();

    /* renamed from: ʻ  reason: contains not printable characters */
    private List<MediaTrack> f6988;

    /* renamed from: ʼ  reason: contains not printable characters */
    private TextTrackStyle f6989;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f6990;

    /* renamed from: ˑ  reason: contains not printable characters */
    private List<AdBreakInfo> f6991;

    /* renamed from: ٴ  reason: contains not printable characters */
    private List<AdBreakClipInfo> f6992;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private JSONObject f6993;

    /* renamed from: 连任  reason: contains not printable characters */
    private long f6994;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f6995;

    /* renamed from: 麤  reason: contains not printable characters */
    private MediaMetadata f6996;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f6997;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f6998;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final MediaInfo f6999;

        public Builder(String str) throws IllegalArgumentException {
            if (TextUtils.isEmpty(str)) {
                throw new IllegalArgumentException("Content ID cannot be empty");
            }
            this.f6999 = new MediaInfo(str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7855(int i) throws IllegalArgumentException {
            this.f6999.m7849(i);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7856(MediaMetadata mediaMetadata) {
            this.f6999.m7850(mediaMetadata);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7857(TextTrackStyle textTrackStyle) {
            this.f6999.m7851(textTrackStyle);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7858(String str) throws IllegalArgumentException {
            this.f6999.m7852(str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7859(List<MediaTrack> list) {
            this.f6999.m7853(list);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaInfo m7860() throws IllegalArgumentException {
            this.f6999.m7836();
            return this.f6999;
        }
    }

    MediaInfo(String str) throws IllegalArgumentException {
        this(str, -1, (String) null, (MediaMetadata) null, -1, (List<MediaTrack>) null, (TextTrackStyle) null, (String) null, (List<AdBreakInfo>) null, (List<AdBreakClipInfo>) null);
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("content ID cannot be null or empty");
        }
    }

    MediaInfo(String str, int i, String str2, MediaMetadata mediaMetadata, long j, List<MediaTrack> list, TextTrackStyle textTrackStyle, String str3, List<AdBreakInfo> list2, List<AdBreakClipInfo> list3) {
        this.f6998 = str;
        this.f6995 = i;
        this.f6997 = str2;
        this.f6996 = mediaMetadata;
        this.f6994 = j;
        this.f6988 = list;
        this.f6989 = textTrackStyle;
        this.f6990 = str3;
        if (this.f6990 != null) {
            try {
                this.f6993 = new JSONObject(this.f6990);
            } catch (JSONException e) {
                this.f6993 = null;
                this.f6990 = null;
            }
        } else {
            this.f6993 = null;
        }
        this.f6991 = list2;
        this.f6992 = list3;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    MediaInfo(JSONObject jSONObject) throws JSONException {
        this(jSONObject.getString("contentId"), -1, (String) null, (MediaMetadata) null, -1, (List<MediaTrack>) null, (TextTrackStyle) null, (String) null, (List<AdBreakInfo>) null, (List<AdBreakClipInfo>) null);
        String string = jSONObject.getString("streamType");
        if ("NONE".equals(string)) {
            this.f6995 = 0;
        } else if ("BUFFERED".equals(string)) {
            this.f6995 = 1;
        } else if ("LIVE".equals(string)) {
            this.f6995 = 2;
        } else {
            this.f6995 = -1;
        }
        this.f6997 = jSONObject.getString("contentType");
        if (jSONObject.has(TtmlNode.TAG_METADATA)) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(TtmlNode.TAG_METADATA);
            this.f6996 = new MediaMetadata(jSONObject2.getInt("metadataType"));
            this.f6996.m7884(jSONObject2);
        }
        this.f6994 = -1;
        if (jSONObject.has(VastIconXmlManager.DURATION) && !jSONObject.isNull(VastIconXmlManager.DURATION)) {
            double optDouble = jSONObject.optDouble(VastIconXmlManager.DURATION, 0.0d);
            if (!Double.isNaN(optDouble) && !Double.isInfinite(optDouble)) {
                this.f6994 = (long) (optDouble * 1000.0d);
            }
        }
        if (jSONObject.has("tracks")) {
            this.f6988 = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("tracks");
            for (int i = 0; i < jSONArray.length(); i++) {
                this.f6988.add(new MediaTrack(jSONArray.getJSONObject(i)));
            }
        } else {
            this.f6988 = null;
        }
        if (jSONObject.has("textTrackStyle")) {
            JSONObject jSONObject3 = jSONObject.getJSONObject("textTrackStyle");
            TextTrackStyle textTrackStyle = new TextTrackStyle();
            textTrackStyle.m7969(jSONObject3);
            this.f6989 = textTrackStyle;
        } else {
            this.f6989 = null;
        }
        m7854(jSONObject);
        this.f6993 = jSONObject.optJSONObject("customData");
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m7836() throws IllegalArgumentException {
        if (TextUtils.isEmpty(this.f6998)) {
            throw new IllegalArgumentException("content ID cannot be null or empty");
        } else if (TextUtils.isEmpty(this.f6997)) {
            throw new IllegalArgumentException("content type cannot be null or empty");
        } else if (this.f6995 == -1) {
            throw new IllegalArgumentException("a valid stream type must be specified");
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaInfo)) {
            return false;
        }
        MediaInfo mediaInfo = (MediaInfo) obj;
        if ((this.f6993 == null) == (mediaInfo.f6993 == null)) {
            return (this.f6993 == null || mediaInfo.f6993 == null || zzo.m9263(this.f6993, mediaInfo.f6993)) && zzbcm.m10043(this.f6998, mediaInfo.f6998) && this.f6995 == mediaInfo.f6995 && zzbcm.m10043(this.f6997, mediaInfo.f6997) && zzbcm.m10043(this.f6996, mediaInfo.f6996) && this.f6994 == mediaInfo.f6994 && zzbcm.m10043(this.f6988, mediaInfo.f6988) && zzbcm.m10043(this.f6989, mediaInfo.f6989) && zzbcm.m10043(this.f6991, mediaInfo.f6991) && zzbcm.m10043(this.f6992, mediaInfo.f6992);
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f6998, Integer.valueOf(this.f6995), this.f6997, this.f6996, Long.valueOf(this.f6994), String.valueOf(this.f6993), this.f6988, this.f6989, this.f6991, this.f6992});
    }

    public void writeToParcel(Parcel parcel, int i) {
        this.f6990 = this.f6993 == null ? null : this.f6993.toString();
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, m7848(), false);
        zzbfp.m10185(parcel, 3, m7844());
        zzbfp.m10193(parcel, 4, m7847(), false);
        zzbfp.m10189(parcel, 5, (Parcelable) m7846(), i, false);
        zzbfp.m10186(parcel, 6, m7843());
        zzbfp.m10180(parcel, 7, m7838(), false);
        zzbfp.m10189(parcel, 8, (Parcelable) m7839(), i, false);
        zzbfp.m10193(parcel, 9, this.f6990, false);
        zzbfp.m10180(parcel, 10, m7840(), false);
        zzbfp.m10180(parcel, 11, m7841(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<MediaTrack> m7838() {
        return this.f6988;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public TextTrackStyle m7839() {
        return this.f6989;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public List<AdBreakInfo> m7840() {
        if (this.f6991 == null) {
            return null;
        }
        return Collections.unmodifiableList(this.f6991);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public List<AdBreakClipInfo> m7841() {
        if (this.f6992 == null) {
            return null;
        }
        return Collections.unmodifiableList(this.f6992);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final JSONObject m7842() {
        String str;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("contentId", (Object) this.f6998);
            switch (this.f6995) {
                case 1:
                    str = "BUFFERED";
                    break;
                case 2:
                    str = "LIVE";
                    break;
                default:
                    str = "NONE";
                    break;
            }
            jSONObject.put("streamType", (Object) str);
            if (this.f6997 != null) {
                jSONObject.put("contentType", (Object) this.f6997);
            }
            if (this.f6996 != null) {
                jSONObject.put(TtmlNode.TAG_METADATA, (Object) this.f6996.m7879());
            }
            if (this.f6994 <= -1) {
                jSONObject.put(VastIconXmlManager.DURATION, JSONObject.NULL);
            } else {
                jSONObject.put(VastIconXmlManager.DURATION, ((double) this.f6994) / 1000.0d);
            }
            if (this.f6988 != null) {
                JSONArray jSONArray = new JSONArray();
                for (MediaTrack r0 : this.f6988) {
                    jSONArray.put((Object) r0.m7927());
                }
                jSONObject.put("tracks", (Object) jSONArray);
            }
            if (this.f6989 != null) {
                jSONObject.put("textTrackStyle", (Object) this.f6989.m7952());
            }
            if (this.f6993 != null) {
                jSONObject.put("customData", (Object) this.f6993);
            }
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m7843() {
        return this.f6994;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m7844() {
        return this.f6995;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m7845(List<AdBreakInfo> list) {
        this.f6991 = list;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public MediaMetadata m7846() {
        return this.f6996;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m7847() {
        return this.f6997;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7848() {
        return this.f6998;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7849(int i) throws IllegalArgumentException {
        if (i < -1 || i > 2) {
            throw new IllegalArgumentException("invalid stream type");
        }
        this.f6995 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7850(MediaMetadata mediaMetadata) {
        this.f6996 = mediaMetadata;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7851(TextTrackStyle textTrackStyle) {
        this.f6989 = textTrackStyle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7852(String str) throws IllegalArgumentException {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("content type cannot be null or empty");
        }
        this.f6997 = str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7853(List<MediaTrack> list) {
        this.f6988 = list;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7854(JSONObject jSONObject) throws JSONException {
        int i = 0;
        if (jSONObject.has("breaks")) {
            JSONArray jSONArray = jSONObject.getJSONArray("breaks");
            this.f6991 = new ArrayList(jSONArray.length());
            int i2 = 0;
            while (true) {
                if (i2 < jSONArray.length()) {
                    AdBreakInfo r3 = AdBreakInfo.m7773(jSONArray.getJSONObject(i2));
                    if (r3 == null) {
                        this.f6991.clear();
                        break;
                    } else {
                        this.f6991.add(r3);
                        i2++;
                    }
                } else {
                    break;
                }
            }
        }
        if (jSONObject.has("breakClips")) {
            JSONArray jSONArray2 = jSONObject.getJSONArray("breakClips");
            this.f6992 = new ArrayList(jSONArray2.length());
            while (i < jSONArray2.length()) {
                AdBreakClipInfo r2 = AdBreakClipInfo.m7766(jSONArray2.getJSONObject(i));
                if (r2 != null) {
                    this.f6992.add(r2);
                    i++;
                } else {
                    this.f6992.clear();
                    return;
                }
            }
        }
    }
}
