package com.google.android.gms.cast.framework.media.widget;

import android.graphics.Bitmap;
import com.google.android.gms.internal.zzazo;

final class zza implements zzazo {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ ExpandedControllerActivity f7360;

    zza(ExpandedControllerActivity expandedControllerActivity) {
        this.f7360 = expandedControllerActivity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8325(Bitmap bitmap) {
        if (bitmap != null) {
            if (this.f7360.f7321 != null) {
                this.f7360.f7321.setVisibility(8);
            }
            if (this.f7360.f7320 != null) {
                this.f7360.f7320.setVisibility(0);
                this.f7360.f7320.setImageBitmap(bitmap);
            }
        }
    }
}
