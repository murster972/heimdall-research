package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbdb;
import java.io.IOException;

final class zzp extends RemoteMediaClient.zzb {

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7388;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ long[] f7389;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzp(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, long[] jArr) {
        super(remoteMediaClient, googleApiClient);
        this.f7388 = remoteMediaClient;
        this.f7389 = jArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8360(zzbcf zzbcf) {
        synchronized (this.f7388.f3638) {
            try {
                this.f7388.f3639.m10110(this.f7272, this.f7389);
            } catch (zzbdb | IOException e) {
                m4198((RemoteMediaClient.MediaChannelResult) m4195(new Status(2100)));
            }
        }
    }
}
