package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class SessionProvider {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7140;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zza f7141 = new zza();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f7142;

    class zza extends zzaa {
        private zza() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final String m8098() {
            return SessionProvider.this.m8093();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final IObjectWrapper m8099(String str) {
            Session r0 = SessionProvider.this.m8097(str);
            if (r0 == null) {
                return null;
            }
            return r0.m8055();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean m8100() {
            return SessionProvider.this.m8095();
        }
    }

    protected SessionProvider(Context context, String str) {
        this.f7142 = ((Context) zzbq.m9120(context)).getApplicationContext();
        this.f7140 = zzbq.m9122(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m8093() {
        return this.f7140;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final IBinder m8094() {
        return this.f7141;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract boolean m8095();

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context m8096() {
        return this.f7142;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Session m8097(String str);
}
