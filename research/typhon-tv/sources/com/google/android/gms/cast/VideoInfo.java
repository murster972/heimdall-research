package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class VideoInfo extends zzbfm {
    public static final Parcelable.Creator<VideoInfo> CREATOR = new zzbo();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7072;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f7073;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7074;

    VideoInfo(int i, int i2, int i3) {
        this.f7074 = i;
        this.f7072 = i2;
        this.f7073 = i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static VideoInfo m7970(JSONObject jSONObject) {
        int i = 2;
        if (jSONObject == null) {
            return null;
        }
        try {
            String string = jSONObject.getString("hdrType");
            char c = 65535;
            switch (string.hashCode()) {
                case 3218:
                    if (string.equals("dv")) {
                        c = 0;
                        break;
                    }
                    break;
                case 103158:
                    if (string.equals("hdr")) {
                        c = 2;
                        break;
                    }
                    break;
                case 113729:
                    if (string.equals("sdr")) {
                        c = 3;
                        break;
                    }
                    break;
                case 99136405:
                    if (string.equals("hdr10")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    i = 3;
                    break;
                case 1:
                    break;
                case 2:
                    i = 4;
                    break;
                case 3:
                    i = 1;
                    break;
                default:
                    Log.d("VideoInfo", String.format(Locale.ROOT, "Unknown HDR type: %s", new Object[]{string}));
                    i = 0;
                    break;
            }
            return new VideoInfo(jSONObject.getInt(VastIconXmlManager.WIDTH), jSONObject.getInt(VastIconXmlManager.HEIGHT), i);
        } catch (JSONException e) {
            Log.d("VideoInfo", String.format(Locale.ROOT, "Error while creating a VideoInfo instance from JSON: %s", new Object[]{e.getMessage()}));
            return null;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VideoInfo)) {
            return false;
        }
        VideoInfo videoInfo = (VideoInfo) obj;
        return this.f7072 == videoInfo.m7971() && this.f7074 == videoInfo.m7973() && this.f7073 == videoInfo.m7972();
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f7072), Integer.valueOf(this.f7074), Integer.valueOf(this.f7073)});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 2, m7973());
        zzbfp.m10185(parcel, 3, m7971());
        zzbfp.m10185(parcel, 4, m7972());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m7971() {
        return this.f7072;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m7972() {
        return this.f7073;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m7973() {
        return this.f7074;
    }
}
