package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzbo implements Parcelable.Creator<VideoInfo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r4 = zzbfn.m10169(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < r4) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r4);
        return new VideoInfo(i3, i2, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new VideoInfo[i];
    }
}
