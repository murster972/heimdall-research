package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbcf;
import java.io.IOException;

final class zzo extends RemoteMediaClient.zzb {

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7387;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzo(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient) {
        super(remoteMediaClient, googleApiClient);
        this.f7387 = remoteMediaClient;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8359(zzbcf zzbcf) {
        synchronized (this.f7387.f3638) {
            try {
                this.f7387.f3639.m10105(this.f7272);
            } catch (IOException e) {
                m4198((RemoteMediaClient.MediaChannelResult) m4195(new Status(2100)));
            }
        }
    }
}
