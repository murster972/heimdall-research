package com.google.android.gms.cast;

import android.os.RemoteException;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.internal.zzbcf;

final class zzi extends Cast.zza {

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzab f7406 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ String f7407;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ String f7408;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzi(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str, String str2, zzab zzab) {
        super(googleApiClient);
        this.f7408 = str;
        this.f7407 = str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m8468(Api.zzb zzb) throws RemoteException {
        m7818((zzbcf) zzb);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8469(zzbcf zzbcf) throws RemoteException {
        try {
            zzbcf.m10015(this.f7408, this.f7407, this.f7406, (zzn<Cast.ApplicationConnectionResult>) this);
        } catch (IllegalStateException e) {
            m9957(2001);
        }
    }
}
