package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.R;
import com.google.android.gms.internal.zzdmy;

final class zzj {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zza f7193;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f7194;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f7195;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f7196;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7197;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Rect f7198 = new Rect();

    zzj(zza zza) {
        this.f7193 = (zza) zzdmy.m11648(zza);
        Resources resources = zza.getResources();
        this.f7195 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_inner_radius);
        this.f7197 = resources.getDimensionPixelOffset(R.dimen.cast_libraries_material_featurehighlight_inner_margin);
        this.f7196 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_text_max_width);
        this.f7194 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_text_horizontal_offset);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final int m8132(View view, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i5 = i3 / 2;
        int i6 = i4 - i <= i2 - i4 ? (i4 - i5) + this.f7194 : (i4 - i5) - this.f7194;
        return i6 - marginLayoutParams.leftMargin < i ? i + marginLayoutParams.leftMargin : (i6 + i3) + marginLayoutParams.rightMargin > i2 ? (i2 - i3) - marginLayoutParams.rightMargin : i6;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8133(View view, int i, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(View.MeasureSpec.makeMeasureSpec(Math.min((i - marginLayoutParams.leftMargin) - marginLayoutParams.rightMargin, this.f7196), 1073741824), View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8134(Rect rect, Rect rect2) {
        boolean z = false;
        View r1 = this.f7193.m8120();
        if (rect.isEmpty() || rect2.isEmpty()) {
            r1.layout(0, 0, 0, 0);
        } else {
            int centerY = rect.centerY();
            int centerX = rect.centerX();
            if (centerY < rect2.centerY()) {
                z = true;
            }
            int max = Math.max(this.f7195 * 2, rect.height());
            int i = this.f7197 + (max / 2) + centerY;
            if (z) {
                m8133(r1, rect2.width(), rect2.bottom - i);
                int r0 = m8132(r1, rect2.left, rect2.right, r1.getMeasuredWidth(), centerX);
                r1.layout(r0, i, r1.getMeasuredWidth() + r0, r1.getMeasuredHeight() + i);
            } else {
                int i2 = (centerY - (max / 2)) - this.f7197;
                m8133(r1, rect2.width(), i2 - rect2.top);
                int r02 = m8132(r1, rect2.left, rect2.right, r1.getMeasuredWidth(), centerX);
                r1.layout(r02, i2 - r1.getMeasuredHeight(), r1.getMeasuredWidth() + r02, i2);
            }
        }
        this.f7198.set(r1.getLeft(), r1.getTop(), r1.getRight(), r1.getBottom());
        this.f7193.m8123().m8109(rect, this.f7198);
        this.f7193.m8122().m8102(rect);
    }
}
