package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

final class zzam implements ResultCallback<Status> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient.zza f7370;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f7371;

    zzam(RemoteMediaClient.zza zza, long j) {
        this.f7370 = zza;
        this.f7371 = j;
    }

    public final /* synthetic */ void onResult(Result result) {
        Status status = (Status) result;
        if (!status.m8549()) {
            RemoteMediaClient.this.f3639.m9972(this.f7371, status.m8547());
        }
    }
}
