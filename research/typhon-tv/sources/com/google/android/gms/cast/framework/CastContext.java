package com.google.android.gms.cast.framework;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.view.KeyEvent;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzayu;
import com.google.android.gms.internal.zzayv;
import com.google.android.gms.internal.zzayz;
import com.google.android.gms.internal.zzazm;
import com.google.android.gms.internal.zzbbz;
import com.google.android.gms.internal.zzbcy;
import com.google.android.gms.internal.zzbhf;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class CastContext {

    /* renamed from: 靐  reason: contains not printable characters */
    private static CastContext f7075;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7076 = new zzbcy("CastContext");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zze f7077;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final PrecacheManager f7078;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final MediaNotificationManager f7079;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final CastOptions f7080;

    /* renamed from: ٴ  reason: contains not printable characters */
    private zzazm f7081 = new zzazm(MediaRouter.m1109(this.f7084));

    /* renamed from: 连任  reason: contains not printable characters */
    private final SessionManager f7082;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzj f7083;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f7084;

    private CastContext(Context context, CastOptions castOptions, List<SessionProvider> list) {
        zzp zzp;
        zzv zzv;
        PrecacheManager precacheManager = null;
        this.f7084 = context.getApplicationContext();
        this.f7080 = castOptions;
        HashMap hashMap = new HashMap();
        zzayv zzayv = new zzayv(this.f7084, castOptions, this.f7081);
        hashMap.put(zzayv.m8093(), zzayv.m8094());
        if (list != null) {
            for (SessionProvider next : list) {
                zzbq.m9121(next, (Object) "Additional SessionProvider must not be null.");
                String r7 = zzbq.m9123(next.m8093(), (Object) "Category for SessionProvider must not be null or empty string.");
                zzbq.m9117(!hashMap.containsKey(r7), String.format("SessionProvider for category %s already added", new Object[]{r7}));
                hashMap.put(r7, next.m8094());
            }
        }
        this.f7083 = zzayu.m9761(this.f7084, castOptions, (zzayz) this.f7081, (Map<String, IBinder>) hashMap);
        try {
            zzp = this.f7083.m8388();
        } catch (RemoteException e) {
            f7076.m10091(e, "Unable to call %s on %s.", "getDiscoveryManagerImpl", zzj.class.getSimpleName());
            zzp = null;
        }
        this.f7077 = zzp == null ? null : new zze(zzp);
        try {
            zzv = this.f7083.m8386();
        } catch (RemoteException e2) {
            f7076.m10091(e2, "Unable to call %s on %s.", "getSessionManagerImpl", zzj.class.getSimpleName());
            zzv = null;
        }
        this.f7082 = zzv == null ? null : new SessionManager(zzv, this.f7084);
        this.f7079 = new MediaNotificationManager(this.f7082);
        this.f7078 = this.f7082 != null ? new PrecacheManager(this.f7080, this.f7082, new zzbbz(this.f7084)) : precacheManager;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static OptionsProvider m7976(Context context) throws IllegalStateException {
        try {
            Bundle bundle = zzbhf.m10231(context).m10226(context.getPackageName(), 128).metaData;
            if (bundle == null) {
                f7076.m10087("Bundle is null", new Object[0]);
            }
            String string = bundle.getString("com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME");
            if (string != null) {
                return (OptionsProvider) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            }
            throw new IllegalStateException("The fully qualified name of the implementation of OptionsProvider must be provided as a metadata in the AndroidManifest.xml with key com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME.");
        } catch (PackageManager.NameNotFoundException | ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | NullPointerException | InvocationTargetException e) {
            throw new IllegalStateException("Failed to initialize CastContext.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static CastContext m7977(Context context) throws IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        if (f7075 == null) {
            OptionsProvider r0 = m7976(context.getApplicationContext());
            f7075 = new CastContext(context, r0.getCastOptions(context.getApplicationContext()), r0.getAdditionalSessionProviders(context.getApplicationContext()));
        }
        return f7075;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m7978(CastSession castSession, double d, boolean z) {
        double d2 = 1.0d;
        if (z) {
            try {
                double r2 = castSession.m8020() + d;
                if (r2 <= 1.0d) {
                    d2 = r2;
                }
                castSession.m8023(d2);
            } catch (IOException | IllegalStateException e) {
                f7076.m10087("Unable to call CastSession.setVolume(double).", e);
            }
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final IObjectWrapper m7979() {
        try {
            return this.f7083.m8387();
        } catch (RemoteException e) {
            f7076.m10091(e, "Unable to call %s on %s.", "getWrappedThis", zzj.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final zze m7980() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7077;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final SessionManager m7981() throws IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7082;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m7982() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7082.m8079();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final MediaRouteSelector m7983() throws IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return MediaRouteSelector.m1095(this.f7083.m8389());
        } catch (RemoteException e) {
            f7076.m10091(e, "Unable to call %s on %s.", "getMergedSelectorAsBundle", zzj.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final CastOptions m7984() throws IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7080;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m7985(KeyEvent keyEvent) {
        CastSession r3;
        zzbq.m9115("Must be called from the main thread.");
        if (zzq.m9269() || (r3 = this.f7082.m8075()) == null || !r3.m8053()) {
            return false;
        }
        double r4 = m7984().m7988();
        boolean z = keyEvent.getAction() == 0;
        switch (keyEvent.getKeyCode()) {
            case 24:
                m7978(r3, r4, z);
                return true;
            case 25:
                m7978(r3, -r4, z);
                return true;
            default:
                return false;
        }
    }
}
