package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class AdBreakStatus extends zzbfm {
    public static final Parcelable.Creator<AdBreakStatus> CREATOR = new zzc();

    /* renamed from: 连任  reason: contains not printable characters */
    private final long f6950;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f6951;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f6952;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f6953;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f6954;

    AdBreakStatus(long j, long j2, String str, String str2, long j3) {
        this.f6954 = j;
        this.f6951 = j2;
        this.f6953 = str;
        this.f6952 = str2;
        this.f6950 = j3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static AdBreakStatus m7779(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        if (!jSONObject.has("currentBreakTime") || !jSONObject.has("currentBreakClipTime")) {
            return null;
        }
        try {
            long j = (long) (((double) jSONObject.getLong("currentBreakTime")) * 1000.0d);
            long j2 = (long) (((double) jSONObject.getLong("currentBreakClipTime")) * 1000.0d);
            String optString = jSONObject.optString("breakId", (String) null);
            String optString2 = jSONObject.optString("breakClipId", (String) null);
            long optLong = jSONObject.optLong("whenSkippable", -1);
            if (optLong != -1) {
                optLong = (long) (((double) optLong) * 1000.0d);
            }
            return new AdBreakStatus(j, j2, optString, optString2, optLong);
        } catch (JSONException e) {
            Log.d("AdBreakInfo", String.format(Locale.ROOT, "Error while creating an AdBreakClipInfo from JSON: %s", new Object[]{e.getMessage()}));
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdBreakStatus)) {
            return false;
        }
        AdBreakStatus adBreakStatus = (AdBreakStatus) obj;
        return this.f6954 == adBreakStatus.f6954 && this.f6951 == adBreakStatus.f6951 && zzbcm.m10043(this.f6953, adBreakStatus.f6953) && zzbcm.m10043(this.f6952, adBreakStatus.f6952) && this.f6950 == adBreakStatus.f6950;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.f6954), Long.valueOf(this.f6951), this.f6953, this.f6952, Long.valueOf(this.f6950)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10186(parcel, 2, m7783());
        zzbfp.m10186(parcel, 3, m7782());
        zzbfp.m10193(parcel, 4, m7784(), false);
        zzbfp.m10193(parcel, 5, m7781(), false);
        zzbfp.m10186(parcel, 6, m7780());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m7780() {
        return this.f6950;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7781() {
        return this.f6952;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long m7782() {
        return this.f6951;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public long m7783() {
        return this.f6954;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7784() {
        return this.f6953;
    }
}
