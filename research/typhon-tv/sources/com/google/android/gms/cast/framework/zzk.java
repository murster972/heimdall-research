package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzk extends zzeu implements zzj {
    zzk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ICastContext");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzv m8391() throws RemoteException {
        zzv zzw;
        Parcel r1 = m12300(5, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzw = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.cast.framework.ISessionManager");
            zzw = queryLocalInterface instanceof zzv ? (zzv) queryLocalInterface : new zzw(readStrongBinder);
        }
        r1.recycle();
        return zzw;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final IObjectWrapper m8392() throws RemoteException {
        Parcel r0 = m12300(10, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzp m8393() throws RemoteException {
        zzp zzq;
        Parcel r1 = m12300(6, v_());
        IBinder readStrongBinder = r1.readStrongBinder();
        if (readStrongBinder == null) {
            zzq = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.cast.framework.IDiscoveryManager");
            zzq = queryLocalInterface instanceof zzp ? (zzp) queryLocalInterface : new zzq(readStrongBinder);
        }
        r1.recycle();
        return zzq;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m8394() throws RemoteException {
        Parcel r1 = m12300(1, v_());
        Bundle bundle = (Bundle) zzew.m12304(r1, Bundle.CREATOR);
        r1.recycle();
        return bundle;
    }
}
