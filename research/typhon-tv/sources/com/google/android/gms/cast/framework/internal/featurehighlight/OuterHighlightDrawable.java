package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.Keep;
import android.support.v4.graphics.ColorUtils;
import android.util.TypedValue;
import com.google.android.gms.R;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.internal.zzdmr;
import com.google.android.gms.internal.zzdmt;

class OuterHighlightDrawable extends Drawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Paint f7155 = new Paint();

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f7156;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f7157 = 1.0f;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f7158 = 244;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f7159 = 0.0f;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f7160;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f7161;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f7162 = 0.0f;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Rect f7163 = new Rect();

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f7164;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Rect f7165 = new Rect();

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7166;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f7167;

    public OuterHighlightDrawable(Context context) {
        if (zzq.m9265()) {
            TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(16843827, typedValue, true);
            m8108(ColorUtils.setAlphaComponent(typedValue.data, 244));
        } else {
            m8108(context.getResources().getColor(R.color.cast_libraries_material_featurehighlight_outer_highlight_default_color));
        }
        this.f7155.setAntiAlias(true);
        this.f7155.setStyle(Paint.Style.FILL);
        Resources resources = context.getResources();
        this.f7167 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_center_threshold);
        this.f7164 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_center_horizontal_offset);
        this.f7166 = resources.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_outer_padding);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static float m8103(float f, float f2, Rect rect) {
        float f3 = (float) rect.left;
        float f4 = (float) rect.top;
        float f5 = (float) rect.right;
        float f6 = (float) rect.bottom;
        float r0 = zzdmt.m11647(f, f2, f3, f4);
        float r1 = zzdmt.m11647(f, f2, f5, f4);
        float r2 = zzdmt.m11647(f, f2, f5, f6);
        float r3 = zzdmt.m11647(f, f2, f3, f6);
        if (r0 <= r1 || r0 <= r2 || r0 <= r3) {
            r0 = (r1 <= r2 || r1 <= r3) ? r2 > r3 ? r2 : r3 : r1;
        }
        return (float) Math.ceil((double) r0);
    }

    public void draw(Canvas canvas) {
        canvas.drawCircle(this.f7160 + this.f7162, this.f7161 + this.f7159, this.f7156 * this.f7157, this.f7155);
    }

    public int getAlpha() {
        return this.f7155.getAlpha();
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i) {
        this.f7155.setAlpha(i);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f7155.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @Keep
    public void setScale(float f) {
        this.f7157 = f;
        invalidateSelf();
    }

    @Keep
    public void setTranslationX(float f) {
        this.f7162 = f;
        invalidateSelf();
    }

    @Keep
    public void setTranslationY(float f) {
        this.f7159 = f;
        invalidateSelf();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final float m8104() {
        return this.f7160;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Animator m8105(float f, float f2) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{0.0f, 1.0f}), PropertyValuesHolder.ofFloat("translationX", new float[]{f, 0.0f}), PropertyValuesHolder.ofFloat("translationY", new float[]{f2, 0.0f}), PropertyValuesHolder.ofInt("alpha", new int[]{0, this.f7158})});
        ofPropertyValuesHolder.setInterpolator(zzdmr.m11643());
        return ofPropertyValuesHolder.setDuration(350);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final float m8106() {
        return this.f7161;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m8107() {
        return this.f7155.getColor();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8108(int i) {
        this.f7155.setColor(i);
        this.f7158 = this.f7155.getAlpha();
        invalidateSelf();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8109(Rect rect, Rect rect2) {
        this.f7165.set(rect);
        this.f7163.set(rect2);
        float exactCenterX = rect.exactCenterX();
        float exactCenterY = rect.exactCenterY();
        Rect bounds = getBounds();
        if (Math.min(exactCenterY - ((float) bounds.top), ((float) bounds.bottom) - exactCenterY) < ((float) this.f7167)) {
            this.f7160 = exactCenterX;
            this.f7161 = exactCenterY;
        } else {
            this.f7160 = (exactCenterX > bounds.exactCenterX() ? 1 : (exactCenterX == bounds.exactCenterX() ? 0 : -1)) <= 0 ? rect2.exactCenterX() + ((float) this.f7164) : rect2.exactCenterX() - ((float) this.f7164);
            this.f7161 = rect2.exactCenterY();
        }
        this.f7156 = ((float) this.f7166) + Math.max(m8103(this.f7160, this.f7161, rect), m8103(this.f7160, this.f7161, rect2));
        invalidateSelf();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8110(float f, float f2) {
        return zzdmt.m11647(f, f2, this.f7160, this.f7161) < this.f7156;
    }
}
