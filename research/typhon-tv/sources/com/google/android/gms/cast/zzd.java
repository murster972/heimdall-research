package com.google.android.gms.cast;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzd implements Parcelable.Creator<ApplicationMetadata> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        Uri uri = null;
        String str = null;
        ArrayList<String> arrayList = null;
        ArrayList<WebImage> arrayList2 = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 4:
                    arrayList2 = zzbfn.m10167(parcel, readInt, WebImage.CREATOR);
                    break;
                case 5:
                    arrayList = zzbfn.m10159(parcel, readInt);
                    break;
                case 6:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 7:
                    uri = (Uri) zzbfn.m10171(parcel, readInt, Uri.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new ApplicationMetadata(str3, str2, arrayList2, arrayList, str, uri);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ApplicationMetadata[i];
    }
}
