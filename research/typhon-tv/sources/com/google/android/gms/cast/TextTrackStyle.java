package com.google.android.gms.cast;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;

public final class TextTrackStyle extends zzbfm {
    public static final Parcelable.Creator<TextTrackStyle> CREATOR = new zzbn();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7059;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7060;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7061;

    /* renamed from: ʾ  reason: contains not printable characters */
    private JSONObject f7062;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f7063;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f7064;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f7065;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f7066;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f7067;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7068;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f7069;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f7070;

    /* renamed from: 龘  reason: contains not printable characters */
    private float f7071;

    public TextTrackStyle() {
        this(1.0f, 0, 0, -1, 0, -1, 0, 0, (String) null, -1, -1, (String) null);
    }

    TextTrackStyle(float f, int i, int i2, int i3, int i4, int i5, int i6, int i7, String str, int i8, int i9, String str2) {
        this.f7071 = f;
        this.f7068 = i;
        this.f7070 = i2;
        this.f7069 = i3;
        this.f7067 = i4;
        this.f7059 = i5;
        this.f7060 = i6;
        this.f7061 = i7;
        this.f7064 = str;
        this.f7065 = i8;
        this.f7066 = i9;
        this.f7063 = str2;
        if (this.f7063 != null) {
            try {
                this.f7062 = new JSONObject(this.f7063);
            } catch (JSONException e) {
                this.f7062 = null;
                this.f7063 = null;
            }
        } else {
            this.f7062 = null;
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private static String m7944(int i) {
        return String.format("#%02X%02X%02X%02X", new Object[]{Integer.valueOf(Color.red(i)), Integer.valueOf(Color.green(i)), Integer.valueOf(Color.blue(i)), Integer.valueOf(Color.alpha(i))});
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m7945(String str) {
        if (str == null || str.length() != 9 || str.charAt(0) != '#') {
            return 0;
        }
        try {
            return Color.argb(Integer.parseInt(str.substring(7, 9), 16), Integer.parseInt(str.substring(1, 3), 16), Integer.parseInt(str.substring(3, 5), 16), Integer.parseInt(str.substring(5, 7), 16));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TextTrackStyle)) {
            return false;
        }
        TextTrackStyle textTrackStyle = (TextTrackStyle) obj;
        if ((this.f7062 == null) == (textTrackStyle.f7062 == null)) {
            return (this.f7062 == null || textTrackStyle.f7062 == null || zzo.m9263(this.f7062, textTrackStyle.f7062)) && this.f7071 == textTrackStyle.f7071 && this.f7068 == textTrackStyle.f7068 && this.f7070 == textTrackStyle.f7070 && this.f7069 == textTrackStyle.f7069 && this.f7067 == textTrackStyle.f7067 && this.f7059 == textTrackStyle.f7059 && this.f7061 == textTrackStyle.f7061 && zzbcm.m10043(this.f7064, textTrackStyle.f7064) && this.f7065 == textTrackStyle.f7065 && this.f7066 == textTrackStyle.f7066;
        }
        return false;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.f7071), Integer.valueOf(this.f7068), Integer.valueOf(this.f7070), Integer.valueOf(this.f7069), Integer.valueOf(this.f7067), Integer.valueOf(this.f7059), Integer.valueOf(this.f7060), Integer.valueOf(this.f7061), this.f7064, Integer.valueOf(this.f7065), Integer.valueOf(this.f7066), String.valueOf(this.f7062)});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        this.f7063 = this.f7062 == null ? null : this.f7062.toString();
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10184(parcel, 2, m7965());
        zzbfp.m10185(parcel, 3, m7959());
        zzbfp.m10185(parcel, 4, m7963());
        zzbfp.m10185(parcel, 5, m7961());
        zzbfp.m10185(parcel, 6, m7957());
        zzbfp.m10185(parcel, 7, m7946());
        zzbfp.m10185(parcel, 8, m7948());
        zzbfp.m10185(parcel, 9, m7950());
        zzbfp.m10193(parcel, 10, m7953(), false);
        zzbfp.m10185(parcel, 11, m7955());
        zzbfp.m10185(parcel, 12, m7956());
        zzbfp.m10193(parcel, 13, this.f7063, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final int m7946() {
        return this.f7059;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m7947(int i) {
        this.f7060 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int m7948() {
        return this.f7060;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m7949(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("invalid windowCornerRadius");
        }
        this.f7061 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int m7950() {
        return this.f7061;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m7951(int i) {
        if (i < 0 || i > 6) {
            throw new IllegalArgumentException("invalid fontGenericFamily");
        }
        this.f7065 = i;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final JSONObject m7952() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("fontScale", (double) this.f7071);
            if (this.f7068 != 0) {
                jSONObject.put("foregroundColor", (Object) m7944(this.f7068));
            }
            if (this.f7070 != 0) {
                jSONObject.put(TtmlNode.ATTR_TTS_BACKGROUND_COLOR, (Object) m7944(this.f7070));
            }
            switch (this.f7069) {
                case 0:
                    jSONObject.put("edgeType", (Object) "NONE");
                    break;
                case 1:
                    jSONObject.put("edgeType", (Object) "OUTLINE");
                    break;
                case 2:
                    jSONObject.put("edgeType", (Object) "DROP_SHADOW");
                    break;
                case 3:
                    jSONObject.put("edgeType", (Object) "RAISED");
                    break;
                case 4:
                    jSONObject.put("edgeType", (Object) "DEPRESSED");
                    break;
            }
            if (this.f7067 != 0) {
                jSONObject.put("edgeColor", (Object) m7944(this.f7067));
            }
            switch (this.f7059) {
                case 0:
                    jSONObject.put("windowType", (Object) "NONE");
                    break;
                case 1:
                    jSONObject.put("windowType", (Object) "NORMAL");
                    break;
                case 2:
                    jSONObject.put("windowType", (Object) "ROUNDED_CORNERS");
                    break;
            }
            if (this.f7060 != 0) {
                jSONObject.put("windowColor", (Object) m7944(this.f7060));
            }
            if (this.f7059 == 2) {
                jSONObject.put("windowRoundedCornerRadius", this.f7061);
            }
            if (this.f7064 != null) {
                jSONObject.put(TtmlNode.ATTR_TTS_FONT_FAMILY, (Object) this.f7064);
            }
            switch (this.f7065) {
                case 0:
                    jSONObject.put("fontGenericFamily", (Object) "SANS_SERIF");
                    break;
                case 1:
                    jSONObject.put("fontGenericFamily", (Object) "MONOSPACED_SANS_SERIF");
                    break;
                case 2:
                    jSONObject.put("fontGenericFamily", (Object) "SERIF");
                    break;
                case 3:
                    jSONObject.put("fontGenericFamily", (Object) "MONOSPACED_SERIF");
                    break;
                case 4:
                    jSONObject.put("fontGenericFamily", (Object) "CASUAL");
                    break;
                case 5:
                    jSONObject.put("fontGenericFamily", (Object) "CURSIVE");
                    break;
                case 6:
                    jSONObject.put("fontGenericFamily", (Object) "SMALL_CAPITALS");
                    break;
            }
            switch (this.f7066) {
                case 0:
                    jSONObject.put(TtmlNode.ATTR_TTS_FONT_STYLE, (Object) "NORMAL");
                    break;
                case 1:
                    jSONObject.put(TtmlNode.ATTR_TTS_FONT_STYLE, (Object) "BOLD");
                    break;
                case 2:
                    jSONObject.put(TtmlNode.ATTR_TTS_FONT_STYLE, (Object) "ITALIC");
                    break;
                case 3:
                    jSONObject.put(TtmlNode.ATTR_TTS_FONT_STYLE, (Object) "BOLD_ITALIC");
                    break;
            }
            if (this.f7062 != null) {
                jSONObject.put("customData", (Object) this.f7062);
            }
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m7953() {
        return this.f7064;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m7954(int i) {
        if (i < 0 || i > 3) {
            throw new IllegalArgumentException("invalid fontStyle");
        }
        this.f7066 = i;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m7955() {
        return this.f7065;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int m7956() {
        return this.f7066;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m7957() {
        return this.f7067;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m7958(int i) {
        if (i < 0 || i > 2) {
            throw new IllegalArgumentException("invalid windowType");
        }
        this.f7059 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m7959() {
        return this.f7068;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m7960(int i) {
        this.f7070 = i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final int m7961() {
        return this.f7069;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m7962(int i) {
        this.f7067 = i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m7963() {
        return this.f7070;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m7964(int i) {
        if (i < 0 || i > 4) {
            throw new IllegalArgumentException("invalid edgeType");
        }
        this.f7069 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final float m7965() {
        return this.f7071;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7966(float f) {
        this.f7071 = f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7967(int i) {
        this.f7068 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7968(String str) {
        this.f7064 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7969(JSONObject jSONObject) throws JSONException {
        this.f7071 = (float) jSONObject.optDouble("fontScale", 1.0d);
        this.f7068 = m7945(jSONObject.optString("foregroundColor"));
        this.f7070 = m7945(jSONObject.optString(TtmlNode.ATTR_TTS_BACKGROUND_COLOR));
        if (jSONObject.has("edgeType")) {
            String string = jSONObject.getString("edgeType");
            if ("NONE".equals(string)) {
                this.f7069 = 0;
            } else if ("OUTLINE".equals(string)) {
                this.f7069 = 1;
            } else if ("DROP_SHADOW".equals(string)) {
                this.f7069 = 2;
            } else if ("RAISED".equals(string)) {
                this.f7069 = 3;
            } else if ("DEPRESSED".equals(string)) {
                this.f7069 = 4;
            }
        }
        this.f7067 = m7945(jSONObject.optString("edgeColor"));
        if (jSONObject.has("windowType")) {
            String string2 = jSONObject.getString("windowType");
            if ("NONE".equals(string2)) {
                this.f7059 = 0;
            } else if ("NORMAL".equals(string2)) {
                this.f7059 = 1;
            } else if ("ROUNDED_CORNERS".equals(string2)) {
                this.f7059 = 2;
            }
        }
        this.f7060 = m7945(jSONObject.optString("windowColor"));
        if (this.f7059 == 2) {
            this.f7061 = jSONObject.optInt("windowRoundedCornerRadius", 0);
        }
        this.f7064 = jSONObject.optString(TtmlNode.ATTR_TTS_FONT_FAMILY, (String) null);
        if (jSONObject.has("fontGenericFamily")) {
            String string3 = jSONObject.getString("fontGenericFamily");
            if ("SANS_SERIF".equals(string3)) {
                this.f7065 = 0;
            } else if ("MONOSPACED_SANS_SERIF".equals(string3)) {
                this.f7065 = 1;
            } else if ("SERIF".equals(string3)) {
                this.f7065 = 2;
            } else if ("MONOSPACED_SERIF".equals(string3)) {
                this.f7065 = 3;
            } else if ("CASUAL".equals(string3)) {
                this.f7065 = 4;
            } else if ("CURSIVE".equals(string3)) {
                this.f7065 = 5;
            } else if ("SMALL_CAPITALS".equals(string3)) {
                this.f7065 = 6;
            }
        }
        if (jSONObject.has(TtmlNode.ATTR_TTS_FONT_STYLE)) {
            String string4 = jSONObject.getString(TtmlNode.ATTR_TTS_FONT_STYLE);
            if ("NORMAL".equals(string4)) {
                this.f7066 = 0;
            } else if ("BOLD".equals(string4)) {
                this.f7066 = 1;
            } else if ("ITALIC".equals(string4)) {
                this.f7066 = 2;
            } else if ("BOLD_ITALIC".equals(string4)) {
                this.f7066 = 3;
            }
        }
        this.f7062 = jSONObject.optJSONObject("customData");
    }
}
