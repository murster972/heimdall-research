package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.media.NotificationOptions;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzbcy;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public class CastMediaOptions extends zzbfm {
    public static final Parcelable.Creator<CastMediaOptions> CREATOR = new zza();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7199 = new zzbcy("CastMediaOptions");

    /* renamed from: 连任  reason: contains not printable characters */
    private final NotificationOptions f7200;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7201;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzb f7202;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f7203;

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private String f7204;

        /* renamed from: 麤  reason: contains not printable characters */
        private NotificationOptions f7205 = new NotificationOptions.Builder().m8188();

        /* renamed from: 齉  reason: contains not printable characters */
        private ImagePicker f7206;

        /* renamed from: 龘  reason: contains not printable characters */
        private String f7207 = MediaIntentReceiver.class.getName();

        /* renamed from: 靐  reason: contains not printable characters */
        public final Builder m8139(String str) {
            this.f7204 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m8140(ImagePicker imagePicker) {
            this.f7206 = imagePicker;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m8141(NotificationOptions notificationOptions) {
            this.f7205 = notificationOptions;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m8142(String str) {
            this.f7207 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final CastMediaOptions m8143() {
            return new CastMediaOptions(this.f7207, this.f7204, this.f7206 == null ? null : this.f7206.m4119().asBinder(), this.f7205);
        }
    }

    CastMediaOptions(String str, String str2, IBinder iBinder, NotificationOptions notificationOptions) {
        zzb zzc;
        this.f7201 = str;
        this.f7203 = str2;
        if (iBinder == null) {
            zzc = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.IImagePicker");
            zzc = queryLocalInterface instanceof zzb ? (zzb) queryLocalInterface : new zzc(iBinder);
        }
        this.f7202 = zzc;
        this.f7200 = notificationOptions;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r1 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, m8138(), false);
        zzbfp.m10193(parcel, 3, m8137(), false);
        zzbfp.m10188(parcel, 4, this.f7202 == null ? null : this.f7202.asBinder(), false);
        zzbfp.m10189(parcel, 5, (Parcelable) m8135(), i, false);
        zzbfp.m10182(parcel, r1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public NotificationOptions m8135() {
        return this.f7200;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ImagePicker m8136() {
        if (this.f7202 != null) {
            try {
                return (ImagePicker) zzn.m9307(this.f7202.m8336());
            } catch (RemoteException e) {
                f7199.m10091(e, "Unable to call %s on %s.", "getWrappedClientObject", zzb.class.getSimpleName());
            }
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m8137() {
        return this.f7203;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m8138() {
        return this.f7201;
    }
}
