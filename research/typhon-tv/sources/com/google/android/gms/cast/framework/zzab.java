package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzab extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    long m8364() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m8365(Bundle bundle) throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m8366(Bundle bundle) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m8367(Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m8368() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8369(Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8370(boolean z) throws RemoteException;
}
