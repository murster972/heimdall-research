package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.View;

public interface zzi {
    View asView();

    void setText(CharSequence charSequence, CharSequence charSequence2);
}
