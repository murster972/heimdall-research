package com.google.android.gms.cast.framework.media;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzev;
import com.google.android.gms.internal.zzew;

public interface zzb extends IInterface {

    public static abstract class zza extends zzev implements zzb {
        public zza() {
            attachInterface(this, "com.google.android.gms.cast.framework.media.IImagePicker");
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (zza(i, parcel, parcel2, i2)) {
                return true;
            }
            switch (i) {
                case 1:
                    WebImage r0 = m8338((MediaMetadata) zzew.m12304(parcel, MediaMetadata.CREATOR), parcel.readInt());
                    parcel2.writeNoException();
                    zzew.m12302(parcel2, r0);
                    break;
                case 2:
                    IObjectWrapper r02 = m8336();
                    parcel2.writeNoException();
                    zzew.m12305(parcel2, (IInterface) r02);
                    break;
                case 3:
                    int r03 = m8337();
                    parcel2.writeNoException();
                    parcel2.writeInt(r03);
                    break;
                case 4:
                    WebImage r04 = m8339((MediaMetadata) zzew.m12304(parcel, MediaMetadata.CREATOR), (ImageHints) zzew.m12304(parcel, ImageHints.CREATOR));
                    parcel2.writeNoException();
                    zzew.m12302(parcel2, r04);
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    IObjectWrapper m8336() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    int m8337() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    WebImage m8338(MediaMetadata mediaMetadata, int i) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    WebImage m8339(MediaMetadata mediaMetadata, ImageHints imageHints) throws RemoteException;
}
