package com.google.android.gms.cast.framework;

import com.google.android.gms.internal.zzbbz;
import com.google.android.gms.internal.zzbcy;

public class PrecacheManager {

    /* renamed from: 靐  reason: contains not printable characters */
    private final CastOptions f7127;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzbbz f7128;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SessionManager f7129;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzbcy f7130 = new zzbcy("PrecacheManager");

    public PrecacheManager(CastOptions castOptions, SessionManager sessionManager, zzbbz zzbbz) {
        this.f7127 = castOptions;
        this.f7129 = sessionManager;
        this.f7128 = zzbbz;
    }
}
