package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Status;

final class zzap implements RemoteMediaClient.MediaChannelResult {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Status f7375;

    zzap(RemoteMediaClient.zzd zzd, Status status) {
        this.f7375 = status;
    }

    public final Status s_() {
        return this.f7375;
    }
}
