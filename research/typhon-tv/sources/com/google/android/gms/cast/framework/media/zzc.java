package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzc extends zzeu implements zzb {
    zzc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.IImagePicker");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final IObjectWrapper m8340() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m8341() throws RemoteException {
        Parcel r0 = m12300(3, v_());
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final WebImage m8342(MediaMetadata mediaMetadata, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) mediaMetadata);
        v_.writeInt(i);
        Parcel r1 = m12300(1, v_);
        WebImage webImage = (WebImage) zzew.m12304(r1, WebImage.CREATOR);
        r1.recycle();
        return webImage;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final WebImage m8343(MediaMetadata mediaMetadata, ImageHints imageHints) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) mediaMetadata);
        zzew.m12306(v_, (Parcelable) imageHints);
        Parcel r1 = m12300(4, v_);
        WebImage webImage = (WebImage) zzew.m12304(r1, WebImage.CREATOR);
        r1.recycle();
        return webImage;
    }
}
