package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;

public final class zzq extends zzeu implements zzp {
    zzq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.IDiscoveryManager");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m8409() throws RemoteException {
        Parcel r0 = m12300(5, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
