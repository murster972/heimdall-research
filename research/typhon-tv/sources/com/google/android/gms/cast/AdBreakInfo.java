package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdBreakInfo extends zzbfm {
    public static final Parcelable.Creator<AdBreakInfo> CREATOR = new zzb();

    /* renamed from: 连任  reason: contains not printable characters */
    private String[] f6945;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f6946;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f6947;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f6948;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f6949;

    public AdBreakInfo(long j, String str, long j2, boolean z, String[] strArr) {
        this.f6949 = j;
        this.f6946 = str;
        this.f6948 = j2;
        this.f6947 = z;
        this.f6945 = strArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static AdBreakInfo m7773(JSONObject jSONObject) {
        String[] strArr;
        if (jSONObject == null) {
            return null;
        }
        if (!jSONObject.has("id") || !jSONObject.has("position")) {
            return null;
        }
        try {
            String string = jSONObject.getString("id");
            long j = (long) (((double) jSONObject.getLong("position")) * 1000.0d);
            boolean optBoolean = jSONObject.optBoolean("isWatched");
            long optLong = (long) (((double) jSONObject.optLong(VastIconXmlManager.DURATION)) * 1000.0d);
            JSONArray optJSONArray = jSONObject.optJSONArray("breakClipIds");
            if (optJSONArray != null) {
                strArr = new String[optJSONArray.length()];
                for (int i = 0; i < optJSONArray.length(); i++) {
                    strArr[i] = optJSONArray.getString(i);
                }
            } else {
                strArr = null;
            }
            return new AdBreakInfo(j, string, optLong, optBoolean, strArr);
        } catch (JSONException e) {
            Log.d("AdBreakInfo", String.format(Locale.ROOT, "Error while creating an AdBreakInfo from JSON: %s", new Object[]{e.getMessage()}));
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdBreakInfo)) {
            return false;
        }
        AdBreakInfo adBreakInfo = (AdBreakInfo) obj;
        return zzbcm.m10043(this.f6946, adBreakInfo.f6946) && this.f6949 == adBreakInfo.f6949 && this.f6948 == adBreakInfo.f6948 && this.f6947 == adBreakInfo.f6947 && Arrays.equals(this.f6945, adBreakInfo.f6945);
    }

    public int hashCode() {
        return this.f6946.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10186(parcel, 2, m7778());
        zzbfp.m10193(parcel, 3, m7775(), false);
        zzbfp.m10186(parcel, 4, m7777());
        zzbfp.m10195(parcel, 5, m7776());
        zzbfp.m10200(parcel, 6, m7774(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String[] m7774() {
        return this.f6945;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7775() {
        return this.f6946;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m7776() {
        return this.f6947;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public long m7777() {
        return this.f6948;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m7778() {
        return this.f6949;
    }
}
