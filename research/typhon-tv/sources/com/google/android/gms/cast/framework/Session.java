package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzayu;
import com.google.android.gms.internal.zzbcy;

public abstract class Session {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7133 = new zzbcy("Session");

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzt f7134;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zza f7135 = new zza();

    class zza extends zzac {
        private zza() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final long m8068() {
            return Session.this.m8059();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m8069(Bundle bundle) {
            Session.this.m8064(bundle);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final void m8070(Bundle bundle) {
            Session.this.m8062(bundle);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m8071(Bundle bundle) {
            Session.this.m8061(bundle);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final IObjectWrapper m8072() {
            return zzn.m9306(Session.this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8073(Bundle bundle) {
            Session.this.m8066(bundle);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8074(boolean z) {
            Session.this.m8067(z);
        }
    }

    protected Session(Context context, String str, String str2) {
        this.f7134 = zzayu.m9764(context, str, str2, (zzab) this.f7135);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m8053() {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return this.f7134.m8422();
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "isConnected", zzt.class.getSimpleName());
            return false;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m8054() {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return this.f7134.m8425();
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "isConnecting", zzt.class.getSimpleName());
            return false;
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final IObjectWrapper m8055() {
        try {
            return this.f7134.m8426();
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "getWrappedObject", zzt.class.getSimpleName());
            return null;
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m8056() {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return this.f7134.m8423();
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "isDisconnecting", zzt.class.getSimpleName());
            return false;
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m8057() {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return this.f7134.m8420();
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "isDisconnected", zzt.class.getSimpleName());
            return true;
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m8058() {
        zzbq.m9115("Must be called from the main thread.");
        try {
            return this.f7134.m8419();
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "isResuming", zzt.class.getSimpleName());
            return false;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m8059() {
        zzbq.m9115("Must be called from the main thread.");
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8060(int i) {
        try {
            this.f7134.m8421(i);
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "notifySessionEnded", zzt.class.getSimpleName());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m8061(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m8062(Bundle bundle);

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8063(int i) {
        try {
            this.f7134.m8424(i);
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "notifyFailedToResumeSession", zzt.class.getSimpleName());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m8064(Bundle bundle);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8065(int i) {
        try {
            this.f7134.m8427(i);
        } catch (RemoteException e) {
            f7133.m10091(e, "Unable to call %s on %s.", "notifyFailedToStartSession", zzt.class.getSimpleName());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8066(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8067(boolean z);
}
