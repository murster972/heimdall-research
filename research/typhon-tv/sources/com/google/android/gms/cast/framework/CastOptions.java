package com.google.android.gms.cast.framework;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastOptions extends zzbfm {
    public static final Parcelable.Creator<CastOptions> CREATOR = new zzb();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CastMediaOptions f7085;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f7086;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final double f7087;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f7088;

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<String> f7089;

    /* renamed from: 麤  reason: contains not printable characters */
    private final LaunchOptions f7090;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f7091;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f7092;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private CastMediaOptions f7093 = new CastMediaOptions.Builder().m8143();

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f7094 = true;

        /* renamed from: ʽ  reason: contains not printable characters */
        private double f7095 = 0.05000000074505806d;

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f7096 = true;

        /* renamed from: 靐  reason: contains not printable characters */
        private List<String> f7097 = new ArrayList();

        /* renamed from: 麤  reason: contains not printable characters */
        private LaunchOptions f7098 = new LaunchOptions();

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f7099;

        /* renamed from: 龘  reason: contains not printable characters */
        private String f7100;

        /* renamed from: 靐  reason: contains not printable characters */
        public final Builder m7994(boolean z) {
            this.f7094 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7995(double d) throws IllegalArgumentException {
            if (d <= 0.0d || d > 0.5d) {
                throw new IllegalArgumentException("volumeDelta must be greater than 0 and less or equal to 0.5");
            }
            this.f7095 = d;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7996(LaunchOptions launchOptions) {
            this.f7098 = launchOptions;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7997(CastMediaOptions castMediaOptions) {
            this.f7093 = castMediaOptions;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7998(String str) {
            this.f7100 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7999(boolean z) {
            this.f7096 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final CastOptions m8000() {
            return new CastOptions(this.f7100, this.f7097, this.f7099, this.f7098, this.f7096, this.f7093, this.f7094, this.f7095);
        }
    }

    CastOptions(String str, List<String> list, boolean z, LaunchOptions launchOptions, boolean z2, CastMediaOptions castMediaOptions, boolean z3, double d) {
        this.f7092 = TextUtils.isEmpty(str) ? "" : str;
        int size = list == null ? 0 : list.size();
        this.f7089 = new ArrayList(size);
        if (size > 0) {
            this.f7089.addAll(list);
        }
        this.f7091 = z;
        this.f7090 = launchOptions == null ? new LaunchOptions() : launchOptions;
        this.f7088 = z2;
        this.f7085 = castMediaOptions;
        this.f7086 = z3;
        this.f7087 = d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, m7993(), false);
        zzbfp.m10178(parcel, 3, m7990(), false);
        zzbfp.m10195(parcel, 4, m7992());
        zzbfp.m10189(parcel, 5, (Parcelable) m7991(), i, false);
        zzbfp.m10195(parcel, 6, m7989());
        zzbfp.m10189(parcel, 7, (Parcelable) m7986(), i, false);
        zzbfp.m10195(parcel, 8, m7987());
        zzbfp.m10183(parcel, 9, m7988());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public CastMediaOptions m7986() {
        return this.f7085;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m7987() {
        return this.f7086;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public double m7988() {
        return this.f7087;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m7989() {
        return this.f7088;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<String> m7990() {
        return Collections.unmodifiableList(this.f7089);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public LaunchOptions m7991() {
        return this.f7090;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m7992() {
        return this.f7091;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7993() {
        return this.f7092;
    }
}
