package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;

public final class zzae<T extends Session> extends zzy {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Class<T> f7397;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SessionManagerListener<T> f7398;

    public zzae(SessionManagerListener<T> sessionManagerListener, Class<T> cls) {
        this.f7398 = sessionManagerListener;
        this.f7397 = cls;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8371(IObjectWrapper iObjectWrapper) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8084((Session) this.f7397.cast(session));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8372(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8085((Session) this.f7397.cast(session), i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8373(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8086((Session) this.f7397.cast(session), str);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8374(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8087((Session) this.f7397.cast(session), i);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8375(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8088((Session) this.f7397.cast(session), i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m8376() {
        return zzn.m9306(this.f7398);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8377(IObjectWrapper iObjectWrapper) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8089((Session) this.f7397.cast(session));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8378(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8090((Session) this.f7397.cast(session), i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8379(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8091((Session) this.f7397.cast(session), str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8380(IObjectWrapper iObjectWrapper, boolean z) throws RemoteException {
        Session session = (Session) zzn.m9307(iObjectWrapper);
        if (this.f7397.isInstance(session)) {
            this.f7398.m8092((Session) this.f7397.cast(session), z);
        }
    }
}
