package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.R;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class NotificationOptions extends zzbfm {
    public static final Parcelable.Creator<NotificationOptions> CREATOR = new zzm();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final int[] f7215 = {0, 1};
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final List<String> f7216 = Arrays.asList(new String[]{"com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK", "com.google.android.gms.cast.framework.action.STOP_CASTING"});

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f7217;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private final zzf f7218;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f7219;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f7220;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f7221;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f7222;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final int f7223;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f7224;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final int f7225;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f7226;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final int f7227;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final int f7228;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final int f7229;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f7230;

    /* renamed from: י  reason: contains not printable characters */
    private final int f7231;

    /* renamed from: ـ  reason: contains not printable characters */
    private final int f7232;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f7233;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f7234;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private final int f7235;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final int f7236;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private final int f7237;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final int f7238;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final int f7239;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final int f7240;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final int f7241;

    /* renamed from: 连任  reason: contains not printable characters */
    private final long f7242;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int[] f7243;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<String> f7244;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final int f7245;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int f7246;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final int f7247;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final int f7248;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f7249 = R.drawable.cast_ic_notification_stop_live_stream;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f7250 = R.drawable.cast_ic_notification_pause;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f7251 = R.drawable.cast_ic_notification_play;

        /* renamed from: ʾ  reason: contains not printable characters */
        private int f7252 = R.drawable.cast_ic_notification_forward30;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f7253 = R.drawable.cast_ic_notification_rewind;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f7254 = R.drawable.cast_ic_notification_forward10;

        /* renamed from: ˊ  reason: contains not printable characters */
        private int f7255 = R.drawable.cast_ic_notification_disconnect;

        /* renamed from: ˋ  reason: contains not printable characters */
        private long f7256 = 10000;

        /* renamed from: ˑ  reason: contains not printable characters */
        private int f7257 = R.drawable.cast_ic_notification_skip_next;

        /* renamed from: ٴ  reason: contains not printable characters */
        private int f7258 = R.drawable.cast_ic_notification_skip_prev;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private int f7259 = R.drawable.cast_ic_notification_forward;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f7260 = R.drawable.cast_ic_notification_small_icon;

        /* renamed from: 靐  reason: contains not printable characters */
        private List<String> f7261 = NotificationOptions.f7216;

        /* renamed from: 麤  reason: contains not printable characters */
        private int[] f7262 = NotificationOptions.f7215;

        /* renamed from: 齉  reason: contains not printable characters */
        private NotificationActionsProvider f7263;

        /* renamed from: 龘  reason: contains not printable characters */
        private String f7264;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private int f7265 = R.drawable.cast_ic_notification_rewind10;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private int f7266 = R.drawable.cast_ic_notification_rewind30;

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m8185(long j) {
            zzbq.m9117(j > 0, "skipStepMs must be positive.");
            this.f7256 = j;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m8186(String str) {
            this.f7264 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m8187(List<String> list, int[] iArr) {
            if (list == null && iArr != null) {
                throw new IllegalArgumentException("When setting actions to null, you must also set compatActionIndices to null.");
            } else if (list == null || iArr != null) {
                if (list == null || iArr == null) {
                    this.f7261 = NotificationOptions.f7216;
                    this.f7262 = NotificationOptions.f7215;
                } else {
                    int size = list.size();
                    if (iArr.length > size) {
                        throw new IllegalArgumentException(String.format(Locale.ROOT, "Invalid number of compat actions: %d > %d.", new Object[]{Integer.valueOf(iArr.length), Integer.valueOf(size)}));
                    }
                    for (int i : iArr) {
                        if (i < 0 || i >= size) {
                            throw new IllegalArgumentException(String.format(Locale.ROOT, "Index %d in compatActionIndices out of range: [0, %d]", new Object[]{Integer.valueOf(i), Integer.valueOf(size - 1)}));
                        }
                    }
                    this.f7261 = new ArrayList(list);
                    this.f7262 = Arrays.copyOf(iArr, iArr.length);
                }
                return this;
            } else {
                throw new IllegalArgumentException("When setting compatActionIndices to null, you must also set actions to null.");
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final NotificationOptions m8188() {
            return new NotificationOptions(this.f7261, this.f7262, this.f7256, this.f7264, this.f7260, this.f7249, this.f7250, this.f7251, this.f7257, this.f7258, this.f7259, this.f7254, this.f7252, this.f7253, this.f7265, this.f7266, this.f7255, R.dimen.cast_notification_image_size, R.string.cast_casting_to_device, R.string.cast_stop_live_stream, R.string.cast_pause, R.string.cast_play, R.string.cast_skip_next, R.string.cast_skip_prev, R.string.cast_forward, R.string.cast_forward_10, R.string.cast_forward_30, R.string.cast_rewind, R.string.cast_rewind_10, R.string.cast_rewind_30, R.string.cast_disconnect, this.f7263 == null ? null : this.f7263.m8163().asBinder());
        }
    }

    public NotificationOptions(List<String> list, int[] iArr, long j, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15, int i16, int i17, int i18, int i19, int i20, int i21, int i22, int i23, int i24, int i25, int i26, int i27, IBinder iBinder) {
        zzf zzh;
        if (list != null) {
            this.f7244 = new ArrayList(list);
        } else {
            this.f7244 = null;
        }
        if (iArr != null) {
            this.f7243 = Arrays.copyOf(iArr, iArr.length);
        } else {
            this.f7243 = null;
        }
        this.f7242 = j;
        this.f7217 = str;
        this.f7219 = i;
        this.f7220 = i2;
        this.f7230 = i3;
        this.f7233 = i4;
        this.f7234 = i5;
        this.f7224 = i6;
        this.f7221 = i7;
        this.f7222 = i8;
        this.f7246 = i9;
        this.f7247 = i10;
        this.f7226 = i11;
        this.f7227 = i12;
        this.f7228 = i13;
        this.f7223 = i14;
        this.f7225 = i15;
        this.f7229 = i16;
        this.f7231 = i17;
        this.f7232 = i18;
        this.f7236 = i19;
        this.f7238 = i20;
        this.f7239 = i21;
        this.f7240 = i22;
        this.f7241 = i23;
        this.f7245 = i24;
        this.f7248 = i25;
        this.f7235 = i26;
        this.f7237 = i27;
        if (iBinder == null) {
            zzh = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.INotificationActionsProvider");
            zzh = queryLocalInterface instanceof zzf ? (zzf) queryLocalInterface : new zzh(iBinder);
        }
        this.f7218 = zzh;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r1 = zzbfp.m10181(parcel);
        zzbfp.m10178(parcel, 2, m8182(), false);
        zzbfp.m10197(parcel, 3, m8179(), false);
        zzbfp.m10186(parcel, 4, m8181());
        zzbfp.m10193(parcel, 5, m8180(), false);
        zzbfp.m10185(parcel, 6, m8178());
        zzbfp.m10185(parcel, 7, m8166());
        zzbfp.m10185(parcel, 8, m8167());
        zzbfp.m10185(parcel, 9, m8168());
        zzbfp.m10185(parcel, 10, m8175());
        zzbfp.m10185(parcel, 11, m8176());
        zzbfp.m10185(parcel, 12, m8177());
        zzbfp.m10185(parcel, 13, m8171());
        zzbfp.m10185(parcel, 14, m8169());
        zzbfp.m10185(parcel, 15, m8170());
        zzbfp.m10185(parcel, 16, m8183());
        zzbfp.m10185(parcel, 17, m8184());
        zzbfp.m10185(parcel, 18, m8172());
        zzbfp.m10185(parcel, 19, this.f7223);
        zzbfp.m10185(parcel, 20, m8173());
        zzbfp.m10185(parcel, 21, m8174());
        zzbfp.m10185(parcel, 22, this.f7231);
        zzbfp.m10185(parcel, 23, this.f7232);
        zzbfp.m10185(parcel, 24, this.f7236);
        zzbfp.m10185(parcel, 25, this.f7238);
        zzbfp.m10185(parcel, 26, this.f7239);
        zzbfp.m10185(parcel, 27, this.f7240);
        zzbfp.m10185(parcel, 28, this.f7241);
        zzbfp.m10185(parcel, 29, this.f7245);
        zzbfp.m10185(parcel, 30, this.f7248);
        zzbfp.m10185(parcel, 31, this.f7235);
        zzbfp.m10185(parcel, 32, this.f7237);
        zzbfp.m10188(parcel, 33, this.f7218 == null ? null : this.f7218.asBinder(), false);
        zzbfp.m10182(parcel, r1);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m8166() {
        return this.f7220;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m8167() {
        return this.f7230;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m8168() {
        return this.f7233;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m8169() {
        return this.f7246;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m8170() {
        return this.f7247;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m8171() {
        return this.f7222;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public int m8172() {
        return this.f7228;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m8173() {
        return this.f7225;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m8174() {
        return this.f7229;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public int m8175() {
        return this.f7234;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public int m8176() {
        return this.f7224;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int m8177() {
        return this.f7221;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m8178() {
        return this.f7219;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int[] m8179() {
        return Arrays.copyOf(this.f7243, this.f7243.length);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m8180() {
        return this.f7217;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public long m8181() {
        return this.f7242;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<String> m8182() {
        return this.f7244;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public int m8183() {
        return this.f7226;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public int m8184() {
        return this.f7227;
    }
}
