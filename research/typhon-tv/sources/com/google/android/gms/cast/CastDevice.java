package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastDevice extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<CastDevice> CREATOR = new zzn();

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f6972;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f6973;

    /* renamed from: ʽ  reason: contains not printable characters */
    private List<WebImage> f6974;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f6975;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f6976;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f6977;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f6978;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f6979;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f6980;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f6981;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f6982;

    /* renamed from: 齉  reason: contains not printable characters */
    private Inet4Address f6983;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f6984;

    CastDevice(String str, String str2, String str3, String str4, String str5, int i, List<WebImage> list, int i2, int i3, String str6, String str7, int i4) {
        this.f6984 = m7821(str);
        this.f6981 = m7821(str2);
        if (!TextUtils.isEmpty(this.f6981)) {
            try {
                InetAddress byName = InetAddress.getByName(this.f6981);
                if (byName instanceof Inet4Address) {
                    this.f6983 = (Inet4Address) byName;
                }
            } catch (UnknownHostException e) {
                String str8 = this.f6981;
                String message = e.getMessage();
                Log.i("CastDevice", new StringBuilder(String.valueOf(str8).length() + 48 + String.valueOf(message).length()).append("Unable to convert host address (").append(str8).append(") to ipaddress: ").append(message).toString());
            }
        }
        this.f6982 = m7821(str3);
        this.f6980 = m7821(str4);
        this.f6972 = m7821(str5);
        this.f6973 = i;
        this.f6974 = list == null ? new ArrayList<>() : list;
        this.f6977 = i2;
        this.f6978 = i3;
        this.f6979 = m7821(str6);
        this.f6976 = str7;
        this.f6975 = i4;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static CastDevice m7820(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        bundle.setClassLoader(CastDevice.class.getClassLoader());
        return (CastDevice) bundle.getParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m7821(String str) {
        return str == null ? "" : str;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CastDevice)) {
            return false;
        }
        CastDevice castDevice = (CastDevice) obj;
        return this.f6984 == null ? castDevice.f6984 == null : zzbcm.m10043(this.f6984, castDevice.f6984) && zzbcm.m10043(this.f6983, castDevice.f6983) && zzbcm.m10043(this.f6980, castDevice.f6980) && zzbcm.m10043(this.f6982, castDevice.f6982) && zzbcm.m10043(this.f6972, castDevice.f6972) && this.f6973 == castDevice.f6973 && zzbcm.m10043(this.f6974, castDevice.f6974) && this.f6977 == castDevice.f6977 && this.f6978 == castDevice.f6978 && zzbcm.m10043(this.f6979, castDevice.f6979) && zzbcm.m10043(Integer.valueOf(this.f6975), Integer.valueOf(castDevice.f6975));
    }

    public int hashCode() {
        if (this.f6984 == null) {
            return 0;
        }
        return this.f6984.hashCode();
    }

    public String toString() {
        return String.format("\"%s\" (%s)", new Object[]{this.f6982, this.f6984});
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 2, this.f6984, false);
        zzbfp.m10193(parcel, 3, this.f6981, false);
        zzbfp.m10193(parcel, 4, m7826(), false);
        zzbfp.m10193(parcel, 5, m7823(), false);
        zzbfp.m10193(parcel, 6, m7825(), false);
        zzbfp.m10185(parcel, 7, m7824());
        zzbfp.m10180(parcel, 8, m7822(), false);
        zzbfp.m10185(parcel, 9, this.f6977);
        zzbfp.m10185(parcel, 10, this.f6978);
        zzbfp.m10193(parcel, 11, this.f6979, false);
        zzbfp.m10193(parcel, 12, this.f6976, false);
        zzbfp.m10185(parcel, 13, this.f6975);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public List<WebImage> m7822() {
        return Collections.unmodifiableList(this.f6974);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7823() {
        return this.f6980;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m7824() {
        return this.f6973;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m7825() {
        return this.f6972;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7826() {
        return this.f6982;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7827(Bundle bundle) {
        if (bundle != null) {
            bundle.putParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE", this);
        }
    }
}
