package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;

public final class zzab extends zzbfm {
    public static final Parcelable.Creator<zzab> CREATOR = new zzac();

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7401;

    public zzab() {
        this(0);
    }

    zzab(int i) {
        this.f7401 = i;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzab)) {
            return false;
        }
        return this.f7401 == ((zzab) obj).f7401;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f7401)});
    }

    public final String toString() {
        String str;
        switch (this.f7401) {
            case 0:
                str = "STRONG";
                break;
            case 2:
                str = "INVISIBLE";
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        return String.format("joinOptions(connectionType=%s)", new Object[]{str});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 2, this.f7401);
        zzbfp.m10182(parcel, r0);
    }
}
