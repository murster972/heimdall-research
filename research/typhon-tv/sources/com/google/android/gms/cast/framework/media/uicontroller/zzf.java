package com.google.android.gms.cast.framework.media.uicontroller;

import android.widget.SeekBar;

final class zzf implements SeekBar.OnSeekBarChangeListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ UIMediaController f7299;

    zzf(UIMediaController uIMediaController) {
        this.f7299 = uIMediaController;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.f7299.m8277(seekBar, i, z);
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
        this.f7299.m8262(seekBar);
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
        this.f7299.m8245(seekBar);
    }
}
