package com.google.android.gms.cast.framework.media;

import android.content.DialogInterface;

final class zzas implements DialogInterface.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzat f7379;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ TracksChooserDialogFragment f7380;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzat f7381;

    zzas(TracksChooserDialogFragment tracksChooserDialogFragment, zzat zzat, zzat zzat2) {
        this.f7380 = tracksChooserDialogFragment;
        this.f7381 = zzat;
        this.f7379 = zzat2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f7380.m8222(this.f7381, this.f7379);
    }
}
