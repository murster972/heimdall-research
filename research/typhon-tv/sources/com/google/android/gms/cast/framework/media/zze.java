package com.google.android.gms.cast.framework.media;

import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zze extends zzeu implements zzd {
    zze(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.IMediaNotificationService");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8349() throws RemoteException {
        m12298(4, v_());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m8350(Intent intent, int i, int i2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) intent);
        v_.writeInt(i);
        v_.writeInt(i2);
        Parcel r0 = m12300(2, v_);
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m8351(Intent intent) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) intent);
        Parcel r0 = m12300(3, v_);
        IBinder readStrongBinder = r0.readStrongBinder();
        r0.recycle();
        return readStrongBinder;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8352() throws RemoteException {
        m12298(1, v_());
    }
}
