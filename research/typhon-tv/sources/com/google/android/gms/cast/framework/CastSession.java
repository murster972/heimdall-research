package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzayu;
import com.google.android.gms.internal.zzayw;
import com.google.android.gms.internal.zzazy;
import com.google.android.gms.internal.zzbcy;
import com.google.android.gms.internal.zzbcz;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class CastSession extends Session {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzbcy f7101 = new zzbcy("CastSession");
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Cast.CastApi f7102;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzayw f7103;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzazy f7104;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public Cast.ApplicationConnectionResult f7105;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public GoogleApiClient f7106;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public RemoteMediaClient f7107;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private CastDevice f7108;

    /* renamed from: 连任  reason: contains not printable characters */
    private final CastOptions f7109;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f7110;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzl f7111;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Set<Cast.Listener> f7112 = new HashSet();

    class zza implements ResultCallback<Cast.ApplicationConnectionResult> {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f7114;

        zza(String str) {
            this.f7114 = str;
        }

        public final /* synthetic */ void onResult(Result result) {
            Cast.ApplicationConnectionResult applicationConnectionResult = (Cast.ApplicationConnectionResult) result;
            Cast.ApplicationConnectionResult unused = CastSession.this.f7105 = applicationConnectionResult;
            try {
                if (applicationConnectionResult.s_().m8549()) {
                    CastSession.f7101.m10090("%s() -> success result", this.f7114);
                    RemoteMediaClient unused2 = CastSession.this.f7107 = new RemoteMediaClient(new zzbcz((String) null, zzh.m9250()), CastSession.this.f7102);
                    try {
                        CastSession.this.f7107.m4167(CastSession.this.f7106);
                        CastSession.this.f7107.m4163();
                        CastSession.this.f7107.m4152();
                        CastSession.this.f7104.m9862(CastSession.this.f7107, CastSession.this.m8014());
                    } catch (IOException e) {
                        CastSession.f7101.m10086(e, "Exception when setting GoogleApiClient.", new Object[0]);
                        RemoteMediaClient unused3 = CastSession.this.f7107 = null;
                    }
                    CastSession.this.f7111.m8398(applicationConnectionResult.m7793(), applicationConnectionResult.m7790(), applicationConnectionResult.m7792(), applicationConnectionResult.m7791());
                    return;
                }
                CastSession.f7101.m10090("%s() -> failure result", this.f7114);
                CastSession.this.f7111.m8395(applicationConnectionResult.s_().m8547());
            } catch (RemoteException e2) {
                CastSession.f7101.m10091(e2, "Unable to call %s on %s.", "methods", zzl.class.getSimpleName());
            }
        }
    }

    class zzb extends zzi {
        private zzb() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8027(int i) {
            CastSession.this.m8007(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8028(String str) {
            if (CastSession.this.f7106 != null) {
                CastSession.this.f7102.m4113(CastSession.this.f7106, str);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8029(String str, LaunchOptions launchOptions) {
            if (CastSession.this.f7106 != null) {
                CastSession.this.f7102.m4114(CastSession.this.f7106, str, launchOptions).m8535(new zza("launchApplication"));
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8030(String str, String str2) {
            if (CastSession.this.f7106 != null) {
                CastSession.this.f7102.m4109(CastSession.this.f7106, str, str2).m8535(new zza("joinApplication"));
            }
        }
    }

    class zzc extends Cast.Listener {
        private zzc() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m8031() {
            for (Cast.Listener r0 : new HashSet(CastSession.this.f7112)) {
                r0.m7810();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m8032(int i) {
            for (Cast.Listener r0 : new HashSet(CastSession.this.f7112)) {
                r0.m7811(i);
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m8033(int i) {
            for (Cast.Listener r0 : new HashSet(CastSession.this.f7112)) {
                r0.m7812(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8034() {
            for (Cast.Listener r0 : new HashSet(CastSession.this.f7112)) {
                r0.m7813();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8035(int i) {
            CastSession.this.m8007(i);
            CastSession.this.m8060(i);
            for (Cast.Listener r0 : new HashSet(CastSession.this.f7112)) {
                r0.m7814(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8036(ApplicationMetadata applicationMetadata) {
            for (Cast.Listener r0 : new HashSet(CastSession.this.f7112)) {
                r0.m7815(applicationMetadata);
            }
        }
    }

    class zzd implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private zzd() {
        }

        public final void onConnected(Bundle bundle) {
            try {
                if (CastSession.this.f7107 != null) {
                    try {
                        CastSession.this.f7107.m4163();
                        CastSession.this.f7107.m4152();
                    } catch (IOException e) {
                        CastSession.f7101.m10086(e, "Exception when setting GoogleApiClient.", new Object[0]);
                        RemoteMediaClient unused = CastSession.this.f7107 = null;
                    }
                }
                CastSession.this.f7111.m8397(bundle);
            } catch (RemoteException e2) {
                CastSession.f7101.m10091(e2, "Unable to call %s on %s.", "onConnected", zzl.class.getSimpleName());
            }
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            try {
                CastSession.this.f7111.m8399(connectionResult);
            } catch (RemoteException e) {
                CastSession.f7101.m10091(e, "Unable to call %s on %s.", "onConnectionFailed", zzl.class.getSimpleName());
            }
        }

        public final void onConnectionSuspended(int i) {
            try {
                CastSession.this.f7111.m8396(i);
            } catch (RemoteException e) {
                CastSession.f7101.m10091(e, "Unable to call %s on %s.", "onConnectionSuspended", zzl.class.getSimpleName());
            }
        }
    }

    public CastSession(Context context, String str, String str2, CastOptions castOptions, Cast.CastApi castApi, zzayw zzayw, zzazy zzazy) {
        super(context, str, str2);
        this.f7110 = context.getApplicationContext();
        this.f7109 = castOptions;
        this.f7102 = castApi;
        this.f7103 = zzayw;
        this.f7104 = zzazy;
        this.f7111 = zzayu.m9762(context, castOptions, m8055(), (zzh) new zzb());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m8004(Bundle bundle) {
        boolean z = true;
        this.f7108 = CastDevice.m7820(bundle);
        if (this.f7108 != null) {
            if (this.f7106 != null) {
                this.f7106.disconnect();
                this.f7106 = null;
            }
            f7101.m10090("Acquiring a connection to Google Play Services for %s", this.f7108);
            zzd zzd2 = new zzd();
            Context context = this.f7110;
            CastDevice castDevice = this.f7108;
            CastOptions castOptions = this.f7109;
            zzc zzc2 = new zzc();
            Bundle bundle2 = new Bundle();
            if (castOptions == null || castOptions.m7986() == null || castOptions.m7986().m8135() == null) {
                z = false;
            }
            bundle2.putBoolean("com.google.android.gms.cast.EXTRA_CAST_FRAMEWORK_NOTIFICATION_ENABLED", z);
            this.f7106 = new GoogleApiClient.Builder(context).addApi(Cast.f6963, new Cast.CastOptions.Builder(castDevice, zzc2).m7808(bundle2).m7809()).addConnectionCallbacks(zzd2).addOnConnectionFailedListener(zzd2).build();
            this.f7106.connect();
        } else if (m8058()) {
            m8063(8);
        } else {
            m8065(8);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8007(int i) {
        this.f7104.m9861(i);
        if (this.f7106 != null) {
            this.f7106.disconnect();
            this.f7106 = null;
        }
        this.f7108 = null;
        if (this.f7107 != null) {
            this.f7107.m4167((GoogleApiClient) null);
            this.f7107 = null;
        }
        this.f7105 = null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m8013() {
        zzbq.m9115("Must be called from the main thread.");
        if (this.f7107 == null) {
            return 0;
        }
        return this.f7107.m4135() - this.f7107.m4148();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public CastDevice m8014() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7108;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m8015(Bundle bundle) {
        this.f7108 = CastDevice.m7820(bundle);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8016(Cast.Listener listener) {
        zzbq.m9115("Must be called from the main thread.");
        if (listener != null) {
            this.f7112.remove(listener);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8017(boolean z) throws IOException, IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        if (this.f7106 != null) {
            this.f7102.m4118(this.f7106, z);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m8018(Bundle bundle) {
        m8004(bundle);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m8019() throws IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        if (this.f7106 != null) {
            return this.f7102.m4111(this.f7106);
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public double m8020() throws IllegalStateException {
        zzbq.m9115("Must be called from the main thread.");
        if (this.f7106 != null) {
            return this.f7102.m4112(this.f7106);
        }
        return 0.0d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m8021(Bundle bundle) {
        m8004(bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RemoteMediaClient m8022() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f7107;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8023(double d) throws IOException {
        zzbq.m9115("Must be called from the main thread.");
        if (this.f7106 != null) {
            this.f7102.m4116(this.f7106, d);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8024(Bundle bundle) {
        this.f7108 = CastDevice.m7820(bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8025(Cast.Listener listener) {
        zzbq.m9115("Must be called from the main thread.");
        if (listener != null) {
            this.f7112.add(listener);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m8026(boolean z) {
        try {
            this.f7111.m8400(z, 0);
        } catch (RemoteException e) {
            f7101.m10091(e, "Unable to call %s on %s.", "disconnectFromDevice", zzl.class.getSimpleName());
        }
        m8060(0);
    }
}
