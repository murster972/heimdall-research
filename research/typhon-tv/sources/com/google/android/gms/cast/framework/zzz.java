package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzz extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    String m8460() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m8461(String str) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m8462() throws RemoteException;
}
