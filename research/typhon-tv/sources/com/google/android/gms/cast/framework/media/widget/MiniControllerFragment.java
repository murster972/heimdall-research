package com.google.android.gms.cast.framework.media.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.R;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbcy;

public class MiniControllerFragment extends Fragment {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7336 = new zzbcy("MiniControllerFragment");

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7337;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7338;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7339;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f7340;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f7341;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f7342;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f7343;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f7344;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f7345;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f7346;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f7347;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f7348;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int[] f7349;

    /* renamed from: י  reason: contains not printable characters */
    private int f7350;

    /* renamed from: ـ  reason: contains not printable characters */
    private UIMediaController f7351;

    /* renamed from: ٴ  reason: contains not printable characters */
    private ImageView[] f7352 = new ImageView[3];

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f7353;

    /* renamed from: 连任  reason: contains not printable characters */
    private TextView f7354;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f7355;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f7356;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f7357;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f7358;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f7359;

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8324(RelativeLayout relativeLayout, int i, int i2) {
        int i3;
        ImageView imageView = (ImageView) relativeLayout.findViewById(i);
        int i4 = this.f7349[i2];
        if (i4 == R.id.cast_button_type_empty) {
            imageView.setVisibility(4);
        } else if (i4 == R.id.cast_button_type_custom) {
        } else {
            if (i4 == R.id.cast_button_type_play_pause_toggle) {
                int i5 = this.f7343;
                int i6 = this.f7340;
                int i7 = this.f7341;
                if (this.f7353 == 1) {
                    i5 = this.f7358;
                    int i8 = this.f7359;
                    i7 = this.f7345;
                    i3 = i8;
                } else {
                    i3 = i6;
                }
                Drawable r2 = zzb.m8328(getContext(), this.f7339, i5);
                Drawable r3 = zzb.m8328(getContext(), this.f7339, i3);
                Drawable r4 = zzb.m8328(getContext(), this.f7339, i7);
                imageView.setImageDrawable(r3);
                ProgressBar progressBar = new ProgressBar(getContext());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(8, i);
                layoutParams.addRule(6, i);
                layoutParams.addRule(5, i);
                layoutParams.addRule(7, i);
                layoutParams.addRule(15);
                progressBar.setLayoutParams(layoutParams);
                progressBar.setVisibility(8);
                Drawable indeterminateDrawable = progressBar.getIndeterminateDrawable();
                if (!(this.f7338 == 0 || indeterminateDrawable == null)) {
                    indeterminateDrawable.setColorFilter(this.f7338, PorterDuff.Mode.SRC_IN);
                }
                relativeLayout.addView(progressBar);
                this.f7351.m8271(imageView, r2, r3, r4, progressBar, true);
            } else if (i4 == R.id.cast_button_type_skip_previous) {
                imageView.setImageDrawable(zzb.m8328(getContext(), this.f7339, this.f7346));
                imageView.setContentDescription(getResources().getString(R.string.cast_skip_prev));
                this.f7351.m8242((View) imageView, 0);
            } else if (i4 == R.id.cast_button_type_skip_next) {
                imageView.setImageDrawable(zzb.m8328(getContext(), this.f7339, this.f7347));
                imageView.setContentDescription(getResources().getString(R.string.cast_skip_next));
                this.f7351.m8267((View) imageView, 0);
            } else if (i4 == R.id.cast_button_type_rewind_30_seconds) {
                imageView.setImageDrawable(zzb.m8328(getContext(), this.f7339, this.f7342));
                imageView.setContentDescription(getResources().getString(R.string.cast_rewind_30));
                this.f7351.m8243((View) imageView, 30000);
            } else if (i4 == R.id.cast_button_type_forward_30_seconds) {
                imageView.setImageDrawable(zzb.m8328(getContext(), this.f7339, this.f7344));
                imageView.setContentDescription(getResources().getString(R.string.cast_forward_30));
                this.f7351.m8268((View) imageView, 30000);
            } else if (i4 == R.id.cast_button_type_mute_toggle) {
                imageView.setImageDrawable(zzb.m8328(getContext(), this.f7339, this.f7348));
                this.f7351.m8270(imageView);
            } else if (i4 == R.id.cast_button_type_closed_caption) {
                imageView.setImageDrawable(zzb.m8328(getContext(), this.f7339, this.f7350));
                this.f7351.m8258((View) imageView);
            }
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f7351 = new UIMediaController(getActivity());
        View inflate = layoutInflater.inflate(R.layout.cast_mini_controller, viewGroup);
        inflate.setVisibility(8);
        this.f7351.m8259(inflate, 8);
        RelativeLayout relativeLayout = (RelativeLayout) inflate.findViewById(R.id.container_current);
        if (this.f7337 != 0) {
            relativeLayout.setBackgroundResource(this.f7337);
        }
        ImageView imageView = (ImageView) inflate.findViewById(R.id.icon_view);
        TextView textView = (TextView) inflate.findViewById(R.id.title_view);
        if (this.f7357 != 0) {
            textView.setTextAppearance(getActivity(), this.f7357);
        }
        this.f7354 = (TextView) inflate.findViewById(R.id.subtitle_view);
        if (this.f7356 != 0) {
            this.f7354.setTextAppearance(getActivity(), this.f7356);
        }
        ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.progressBar);
        if (this.f7338 != 0) {
            ((LayerDrawable) progressBar.getProgressDrawable()).setColorFilter(this.f7338, PorterDuff.Mode.SRC_IN);
        }
        this.f7351.m8281(textView, "com.google.android.gms.cast.metadata.TITLE");
        this.f7351.m8279(this.f7354);
        this.f7351.m8274(progressBar);
        this.f7351.m8241((View) relativeLayout);
        if (this.f7355) {
            this.f7351.m8272(imageView, new ImageHints(2, getResources().getDimensionPixelSize(R.dimen.cast_mini_controller_icon_width), getResources().getDimensionPixelSize(R.dimen.cast_mini_controller_icon_height)), R.drawable.cast_album_art_placeholder);
        } else {
            imageView.setVisibility(8);
        }
        this.f7352[0] = (ImageView) relativeLayout.findViewById(R.id.button_0);
        this.f7352[1] = (ImageView) relativeLayout.findViewById(R.id.button_1);
        this.f7352[2] = (ImageView) relativeLayout.findViewById(R.id.button_2);
        m8324(relativeLayout, R.id.button_0, 0);
        m8324(relativeLayout, R.id.button_1, 1);
        m8324(relativeLayout, R.id.button_2, 2);
        return inflate;
    }

    public void onDestroy() {
        if (this.f7351 != null) {
            this.f7351.m8237();
            this.f7351 = null;
        }
        super.onDestroy();
    }

    public void onInflate(Context context, AttributeSet attributeSet, Bundle bundle) {
        boolean z = true;
        super.onInflate(context, attributeSet, bundle);
        if (this.f7349 == null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CastMiniController, R.attr.castMiniControllerStyle, R.style.CastMiniController);
            this.f7355 = obtainStyledAttributes.getBoolean(R.styleable.CastMiniController_castShowImageThumbnail, true);
            this.f7357 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castTitleTextAppearance, 0);
            this.f7356 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castSubtitleTextAppearance, 0);
            this.f7337 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castBackground, 0);
            this.f7338 = obtainStyledAttributes.getColor(R.styleable.CastMiniController_castProgressBarColor, 0);
            this.f7339 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castButtonColor, 0);
            this.f7343 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castPlayButtonDrawable, 0);
            this.f7340 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castPauseButtonDrawable, 0);
            this.f7341 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castStopButtonDrawable, 0);
            this.f7358 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castPlayButtonDrawable, 0);
            this.f7359 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castPauseButtonDrawable, 0);
            this.f7345 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castStopButtonDrawable, 0);
            this.f7346 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castSkipPreviousButtonDrawable, 0);
            this.f7347 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castSkipNextButtonDrawable, 0);
            this.f7342 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castRewind30ButtonDrawable, 0);
            this.f7344 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castForward30ButtonDrawable, 0);
            this.f7348 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castMuteToggleButtonDrawable, 0);
            this.f7350 = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castClosedCaptionsButtonDrawable, 0);
            int resourceId = obtainStyledAttributes.getResourceId(R.styleable.CastMiniController_castControlButtons, 0);
            if (resourceId != 0) {
                TypedArray obtainTypedArray = context.getResources().obtainTypedArray(resourceId);
                if (obtainTypedArray.length() != 3) {
                    z = false;
                }
                zzbq.m9116(z);
                this.f7349 = new int[obtainTypedArray.length()];
                for (int i = 0; i < obtainTypedArray.length(); i++) {
                    this.f7349[i] = obtainTypedArray.getResourceId(i, 0);
                }
                obtainTypedArray.recycle();
                if (this.f7355) {
                    this.f7349[0] = R.id.cast_button_type_empty;
                }
                this.f7353 = 0;
                for (int i2 : this.f7349) {
                    if (i2 != R.id.cast_button_type_empty) {
                        this.f7353++;
                    }
                }
            } else {
                f7336.m10088("Unable to read attribute castControlButtons.", new Object[0]);
                this.f7349 = new int[]{R.id.cast_button_type_empty, R.id.cast_button_type_empty, R.id.cast_button_type_empty};
            }
            obtainStyledAttributes.recycle();
        }
    }
}
