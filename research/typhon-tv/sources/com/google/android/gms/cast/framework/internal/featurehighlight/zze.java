package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zze extends AnimatorListenerAdapter {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zza f7188;

    zze(zza zza) {
        this.f7188 = zza;
    }

    public final void onAnimationEnd(Animator animator) {
        Animator unused = this.f7188.f7174 = this.f7188.m8111();
        this.f7188.f7174.start();
    }
}
