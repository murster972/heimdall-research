package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zzg extends AnimatorListenerAdapter {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zza f7191;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Runnable f7192;

    zzg(zza zza, Runnable runnable) {
        this.f7191 = zza;
        this.f7192 = runnable;
    }

    public final void onAnimationEnd(Animator animator) {
        this.f7191.setVisibility(8);
        Animator unused = this.f7191.f7174 = null;
        if (this.f7192 != null) {
            this.f7192.run();
        }
    }
}
