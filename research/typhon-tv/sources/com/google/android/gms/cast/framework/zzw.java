package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzw extends zzeu implements zzv {
    zzw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ISessionManager");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final IObjectWrapper m8444() throws RemoteException {
        Parcel r0 = m12300(7, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8445(zzx zzx) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzx);
        m12298(3, v_);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m8446() throws RemoteException {
        Parcel r0 = m12300(8, v_());
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m8447() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8448(zzx zzx) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12305(v_, (IInterface) zzx);
        m12298(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8449(boolean z, boolean z2) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, true);
        zzew.m12307(v_, z2);
        m12298(6, v_);
    }
}
