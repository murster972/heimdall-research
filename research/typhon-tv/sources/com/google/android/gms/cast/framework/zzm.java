package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzm extends zzeu implements zzl {
    zzm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ICastSession");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8402(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(5, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8403(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(2, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8404(Bundle bundle) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) bundle);
        m12298(1, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8405(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) applicationMetadata);
        v_.writeString(str);
        v_.writeString(str2);
        zzew.m12307(v_, z);
        m12298(4, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8406(ConnectionResult connectionResult) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) connectionResult);
        m12298(3, v_);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8407(boolean z, int i) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12307(v_, z);
        v_.writeInt(0);
        m12298(6, v_);
    }
}
