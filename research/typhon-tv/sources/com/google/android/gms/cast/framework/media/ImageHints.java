package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public class ImageHints extends zzbfm {
    public static final Parcelable.Creator<ImageHints> CREATOR = new zzi();

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f7208;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7209;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f7210;

    public ImageHints(int i, int i2, int i3) {
        this.f7210 = i;
        this.f7208 = i2;
        this.f7209 = i3;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 2, m8146());
        zzbfp.m10185(parcel, 3, m8144());
        zzbfp.m10185(parcel, 4, m8145());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m8144() {
        return this.f7208;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m8145() {
        return this.f7209;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m8146() {
        return this.f7210;
    }
}
