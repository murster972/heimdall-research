package com.google.android.gms.cast.framework.media;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbbv;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbcz;
import com.google.android.gms.internal.zzbda;
import com.google.android.gms.internal.zzbdc;
import com.google.android.gms.internal.zzbdd;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

public class RemoteMediaClient implements Cast.MessageReceivedCallback {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f3630 = zzbcz.f8689;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Cast.CastApi f3631;

    /* renamed from: ʼ  reason: contains not printable characters */
    private GoogleApiClient f3632;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final List<Listener> f3633 = new CopyOnWriteArrayList();

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Map<ProgressListener, zze> f3634 = new ConcurrentHashMap();

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Map<Long, zze> f3635 = new ConcurrentHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public ParseAdsInfoCallback f3636;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zza f3637 = new zza();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f3638 = new Object();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzbcz f3639;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Handler f3640 = new Handler(Looper.getMainLooper());

    public interface Listener {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m8189();

        /* renamed from: 连任  reason: contains not printable characters */
        void m8190();

        /* renamed from: 靐  reason: contains not printable characters */
        void m8191();

        /* renamed from: 麤  reason: contains not printable characters */
        void m8192();

        /* renamed from: 齉  reason: contains not printable characters */
        void m8193();

        /* renamed from: 龘  reason: contains not printable characters */
        void m8194();
    }

    public interface MediaChannelResult extends Result {
    }

    public interface ParseAdsInfoCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        List<AdBreakInfo> m8195(MediaStatus mediaStatus);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m8196(MediaStatus mediaStatus);
    }

    public interface ProgressListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m8197(long j, long j2);
    }

    class zza implements zzbdc {

        /* renamed from: 靐  reason: contains not printable characters */
        private GoogleApiClient f7267;

        /* renamed from: 齉  reason: contains not printable characters */
        private long f7268 = 0;

        public zza() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final long m8198() {
            long j = this.f7268 + 1;
            this.f7268 = j;
            return j;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8199(GoogleApiClient googleApiClient) {
            this.f7267 = googleApiClient;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8200(String str, String str2, long j, String str3) throws IOException {
            if (this.f7267 == null) {
                throw new IOException("No GoogleApiClient available");
            }
            RemoteMediaClient.this.f3631.m4115(this.f7267, str, str2).m8535(new zzam(this, j));
        }
    }

    abstract class zzb extends zzbbv<MediaChannelResult> {

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f7271;

        /* renamed from: 龘  reason: contains not printable characters */
        zzbdd f7272;

        zzb(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient) {
            this(googleApiClient, false);
        }

        zzb(GoogleApiClient googleApiClient, boolean z) {
            super(googleApiClient);
            this.f7271 = z;
            this.f7272 = new zzan(this, RemoteMediaClient.this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* synthetic */ Result m8201(Status status) {
            return new zzao(this, status);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final /* synthetic */ void m8202(Api.zzb zzb) throws RemoteException {
            zzbcf zzbcf = (zzbcf) zzb;
            if (!this.f7271) {
                for (Listener r0 : RemoteMediaClient.this.f3633) {
                    r0.m8190();
                }
            }
            m8203(zzbcf);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m8203(zzbcf zzbcf);

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8204(Object obj) {
            super.m4198((MediaChannelResult) obj);
        }
    }

    static final class zzc implements MediaChannelResult {

        /* renamed from: 靐  reason: contains not printable characters */
        private final JSONObject f7273;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Status f7274;

        zzc(Status status, JSONObject jSONObject) {
            this.f7274 = status;
            this.f7273 = jSONObject;
        }

        public final Status s_() {
            return this.f7274;
        }
    }

    static class zzd extends BasePendingResult<MediaChannelResult> {
        zzd() {
            super((GoogleApiClient) null);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public final MediaChannelResult m8206(Status status) {
            return new zzap(this, status);
        }
    }

    class zze {

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f7275;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final Set<ProgressListener> f7276 = new HashSet();

        /* renamed from: 麤  reason: contains not printable characters */
        private final Runnable f7277;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final long f7278;

        public zze(long j) {
            this.f7278 = j;
            this.f7277 = new zzaq(this, RemoteMediaClient.this);
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final boolean m8209() {
            return this.f7275;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m8210(ProgressListener progressListener) {
            this.f7276.remove(progressListener);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final boolean m8211() {
            return !this.f7276.isEmpty();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final void m8212() {
            RemoteMediaClient.this.f3640.removeCallbacks(this.f7277);
            this.f7275 = false;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m8213() {
            RemoteMediaClient.this.f3640.removeCallbacks(this.f7277);
            this.f7275 = true;
            RemoteMediaClient.this.f3640.postDelayed(this.f7277, this.f7278);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final long m8214() {
            return this.f7278;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8215(ProgressListener progressListener) {
            this.f7276.add(progressListener);
        }
    }

    public RemoteMediaClient(zzbcz zzbcz, Cast.CastApi castApi) {
        this.f3631 = castApi;
        this.f3639 = (zzbcz) zzbq.m9120(zzbcz);
        this.f3639.m10113((zzbda) new zzn(this));
        this.f3639.m9973((zzbdc) this.f3637);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private final boolean m4124() {
        return this.f3632 != null;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static PendingResult<MediaChannelResult> m4125() {
        zzd zzd2 = new zzd();
        zzd2.m4198(zzd2.m8206(new Status(17)));
        return zzd2;
    }

    /* access modifiers changed from: private */
    /* renamed from: י  reason: contains not printable characters */
    public final void m4126() {
        for (zze next : this.f3635.values()) {
            if (m4143() && !next.m8209()) {
                next.m8213();
            } else if (!m4143() && next.m8209()) {
                next.m8212();
            }
            if (next.m8209() && (m4139() || m4138() || m4169())) {
                m4134((Set<ProgressListener>) next.f7276);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzb m4131(zzb zzb2) {
        try {
            this.f3632.zze(zzb2);
        } catch (IllegalStateException e) {
            zzb2.m4198((MediaChannelResult) zzb2.m8201(new Status(2100)));
        } catch (Throwable th) {
        }
        return zzb2;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4134(Set<ProgressListener> set) {
        if (!m4139() && !m4138()) {
            HashSet<ProgressListener> hashSet = new HashSet<>(set);
            if (m4141()) {
                for (ProgressListener r0 : hashSet) {
                    r0.m8197(m4148(), m4135());
                }
            } else if (m4169()) {
                MediaQueueItem r1 = m4170();
                if (r1 != null && r1.m7898() != null) {
                    for (ProgressListener r02 : hashSet) {
                        r02.m8197(0, r1.m7898().m7843());
                    }
                }
            } else {
                for (ProgressListener r03 : hashSet) {
                    r03.m8197(0, 0);
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public long m4135() {
        long r2;
        synchronized (this.f3638) {
            zzbq.m9115("Must be called from the main thread.");
            r2 = this.f3639.m10101();
        }
        return r2;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public MediaStatus m4136() {
        MediaStatus r0;
        synchronized (this.f3638) {
            zzbq.m9115("Must be called from the main thread.");
            r0 = this.f3639.m10099();
        }
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public MediaInfo m4137() {
        MediaInfo r0;
        synchronized (this.f3638) {
            zzbq.m9115("Must be called from the main thread.");
            r0 = this.f3639.m10100();
        }
        return r0;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m4138() {
        zzbq.m9115("Must be called from the main thread.");
        MediaStatus r0 = m4136();
        return r0 != null && (r0.m7914() == 3 || (m4147() && m4146() == 2));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m4139() {
        zzbq.m9115("Must be called from the main thread.");
        MediaStatus r0 = m4136();
        return r0 != null && r0.m7914() == 4;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public String m4140() {
        zzbq.m9115("Must be called from the main thread.");
        return this.f3639.m9968();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m4141() {
        zzbq.m9115("Must be called from the main thread.");
        MediaStatus r0 = m4136();
        return r0 != null && r0.m7914() == 2;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m4142() {
        zzbq.m9115("Must be called from the main thread.");
        int r0 = m4145();
        if (r0 == 4 || r0 == 2) {
            m4149();
        } else {
            m4154();
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m4143() {
        zzbq.m9115("Must be called from the main thread.");
        return m4139() || m4141() || m4138() || m4169();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m4144() {
        zzbq.m9115("Must be called from the main thread.");
        MediaStatus r0 = m4136();
        return r0 != null && r0.m7923();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public int m4145() {
        int r0;
        synchronized (this.f3638) {
            zzbq.m9115("Must be called from the main thread.");
            MediaStatus r02 = m4136();
            r0 = r02 != null ? r02.m7914() : 1;
        }
        return r0;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public int m4146() {
        int r0;
        synchronized (this.f3638) {
            zzbq.m9115("Must be called from the main thread.");
            MediaStatus r02 = m4136();
            r0 = r02 != null ? r02.m7917() : 0;
        }
        return r0;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m4147() {
        zzbq.m9115("Must be called from the main thread.");
        MediaInfo r0 = m4137();
        return r0 != null && r0.m7844() == 2;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m4148() {
        long r2;
        synchronized (this.f3638) {
            zzbq.m9115("Must be called from the main thread.");
            r2 = this.f3639.m10104();
        }
        return r2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4149() {
        return m4161((JSONObject) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4150(JSONObject jSONObject) {
        zzbq.m9115("Must be called from the main thread.");
        return !m4124() ? m4125() : m4131((zzb) new zzah(this, this.f3632, jSONObject));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m4151(Listener listener) {
        zzbq.m9115("Must be called from the main thread.");
        if (listener != null) {
            this.f3633.remove(listener);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4152() {
        zzbq.m9115("Must be called from the main thread.");
        return !m4124() ? m4125() : m4131((zzb) new zzo(this, this.f3632));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4153(JSONObject jSONObject) {
        zzbq.m9115("Must be called from the main thread.");
        return !m4124() ? m4125() : m4131((zzb) new zzz(this, this.f3632, jSONObject));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4154() {
        return m4150((JSONObject) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4155(JSONObject jSONObject) {
        zzbq.m9115("Must be called from the main thread.");
        return !m4124() ? m4125() : m4131((zzb) new zzx(this, this.f3632, jSONObject));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4156(long j) {
        return m4157(j, 0, (JSONObject) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4157(long j, int i, JSONObject jSONObject) {
        zzbq.m9115("Must be called from the main thread.");
        if (!m4124()) {
            return m4125();
        }
        return m4131((zzb) new zzai(this, this.f3632, j, i, jSONObject));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4158(MediaInfo mediaInfo, MediaLoadOptions mediaLoadOptions) {
        zzbq.m9115("Must be called from the main thread.");
        return !m4124() ? m4125() : m4131((zzb) new zzy(this, this.f3632, mediaInfo, mediaLoadOptions));
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4159(MediaInfo mediaInfo, boolean z, long j) {
        return m4158(mediaInfo, new MediaLoadOptions.Builder().m7868(z).m7866(j).m7870());
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4160(MediaInfo mediaInfo, boolean z, long j, long[] jArr, JSONObject jSONObject) {
        return m4158(mediaInfo, new MediaLoadOptions.Builder().m7868(z).m7866(j).m7869(jArr).m7867(jSONObject).m7870());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4161(JSONObject jSONObject) {
        zzbq.m9115("Must be called from the main thread.");
        return !m4124() ? m4125() : m4131((zzb) new zzaf(this, this.f3632, jSONObject));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingResult<MediaChannelResult> m4162(long[] jArr) {
        zzbq.m9115("Must be called from the main thread.");
        if (!m4124()) {
            return m4125();
        }
        if (jArr != null) {
            return m4131((zzb) new zzp(this, this.f3632, jArr));
        }
        throw new IllegalArgumentException("trackIds cannot be null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4163() throws IOException {
        if (this.f3632 != null) {
            this.f3631.m4117(this.f3632, m4140(), (Cast.MessageReceivedCallback) this);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4164(CastDevice castDevice, String str, String str2) {
        this.f3639.m9969(str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4165(Listener listener) {
        zzbq.m9115("Must be called from the main thread.");
        if (listener != null) {
            this.f3633.add(listener);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4166(ProgressListener progressListener) {
        zzbq.m9115("Must be called from the main thread.");
        zze remove = this.f3634.remove(progressListener);
        if (remove != null) {
            remove.m8210(progressListener);
            if (!remove.m8211()) {
                this.f3635.remove(Long.valueOf(remove.m8214()));
                remove.m8212();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4167(GoogleApiClient googleApiClient) {
        if (this.f3632 != googleApiClient) {
            if (this.f3632 != null) {
                this.f3639.m9971();
                try {
                    this.f3631.m4110(this.f3632, m4140());
                } catch (IOException e) {
                }
                this.f3637.m8199((GoogleApiClient) null);
                this.f3640.removeCallbacksAndMessages((Object) null);
            }
            this.f3632 = googleApiClient;
            if (this.f3632 != null) {
                this.f3637.m8199(this.f3632);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m4168(ProgressListener progressListener, long j) {
        zzbq.m9115("Must be called from the main thread.");
        if (progressListener == null || this.f3634.containsKey(progressListener)) {
            return false;
        }
        zze zze2 = this.f3635.get(Long.valueOf(j));
        if (zze2 == null) {
            zze2 = new zze(j);
            this.f3635.put(Long.valueOf(j), zze2);
        }
        zze2.m8215(progressListener);
        this.f3634.put(progressListener, zze2);
        if (m4143()) {
            zze2.m8213();
        }
        return true;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public boolean m4169() {
        zzbq.m9115("Must be called from the main thread.");
        MediaStatus r0 = m4136();
        return (r0 == null || r0.m7912() == 0) ? false : true;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public MediaQueueItem m4170() {
        zzbq.m9115("Must be called from the main thread.");
        MediaStatus r0 = m4136();
        if (r0 == null) {
            return null;
        }
        return r0.m7921(r0.m7912());
    }
}
