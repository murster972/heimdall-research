package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaQueueItem extends zzbfm {
    public static final Parcelable.Creator<MediaQueueItem> CREATOR = new zzai();

    /* renamed from: ʻ  reason: contains not printable characters */
    private double f7018;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long[] f7019;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f7020;

    /* renamed from: ˑ  reason: contains not printable characters */
    private JSONObject f7021;

    /* renamed from: 连任  reason: contains not printable characters */
    private double f7022;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7023;

    /* renamed from: 麤  reason: contains not printable characters */
    private double f7024;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f7025;

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo f7026;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final MediaQueueItem f7027;

        public Builder(MediaInfo mediaInfo) throws IllegalArgumentException {
            this.f7027 = new MediaQueueItem(mediaInfo);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaQueueItem m7900() {
            this.f7027.m7892();
            return this.f7027;
        }
    }

    private MediaQueueItem(MediaInfo mediaInfo) throws IllegalArgumentException {
        this(mediaInfo, 0, true, 0.0d, Double.POSITIVE_INFINITY, 0.0d, (long[]) null, (String) null);
        if (mediaInfo == null) {
            throw new IllegalArgumentException("media cannot be null.");
        }
    }

    MediaQueueItem(MediaInfo mediaInfo, int i, boolean z, double d, double d2, double d3, long[] jArr, String str) {
        this.f7026 = mediaInfo;
        this.f7023 = i;
        this.f7025 = z;
        this.f7024 = d;
        this.f7022 = d2;
        this.f7018 = d3;
        this.f7019 = jArr;
        this.f7020 = str;
        if (this.f7020 != null) {
            try {
                this.f7021 = new JSONObject(this.f7020);
            } catch (JSONException e) {
                this.f7021 = null;
                this.f7020 = null;
            }
        } else {
            this.f7021 = null;
        }
    }

    MediaQueueItem(JSONObject jSONObject) throws JSONException {
        this((MediaInfo) null, 0, true, 0.0d, Double.POSITIVE_INFINITY, 0.0d, (long[]) null, (String) null);
        m7899(jSONObject);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaQueueItem)) {
            return false;
        }
        MediaQueueItem mediaQueueItem = (MediaQueueItem) obj;
        if ((this.f7021 == null) == (mediaQueueItem.f7021 == null)) {
            return (this.f7021 == null || mediaQueueItem.f7021 == null || zzo.m9263(this.f7021, mediaQueueItem.f7021)) && zzbcm.m10043(this.f7026, mediaQueueItem.f7026) && this.f7023 == mediaQueueItem.f7023 && this.f7025 == mediaQueueItem.f7025 && this.f7024 == mediaQueueItem.f7024 && this.f7022 == mediaQueueItem.f7022 && this.f7018 == mediaQueueItem.f7018 && Arrays.equals(this.f7019, mediaQueueItem.f7019);
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f7026, Integer.valueOf(this.f7023), Boolean.valueOf(this.f7025), Double.valueOf(this.f7024), Double.valueOf(this.f7022), Double.valueOf(this.f7018), Integer.valueOf(Arrays.hashCode(this.f7019)), String.valueOf(this.f7021)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        this.f7020 = this.f7021 == null ? null : this.f7021.toString();
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 2, (Parcelable) m7898(), i, false);
        zzbfp.m10185(parcel, 3, m7895());
        zzbfp.m10195(parcel, 4, m7897());
        zzbfp.m10183(parcel, 5, m7896());
        zzbfp.m10183(parcel, 6, m7894());
        zzbfp.m10183(parcel, 7, m7890());
        zzbfp.m10198(parcel, 8, m7891(), false);
        zzbfp.m10193(parcel, 9, this.f7020, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public double m7890() {
        return this.f7018;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public long[] m7891() {
        return this.f7019;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m7892() throws IllegalArgumentException {
        if (this.f7026 == null) {
            throw new IllegalArgumentException("media cannot be null.");
        } else if (Double.isNaN(this.f7024) || this.f7024 < 0.0d) {
            throw new IllegalArgumentException("startTime cannot be negative or NaN.");
        } else if (Double.isNaN(this.f7022)) {
            throw new IllegalArgumentException("playbackDuration cannot be NaN.");
        } else if (Double.isNaN(this.f7018) || this.f7018 < 0.0d) {
            throw new IllegalArgumentException("preloadTime cannot be negative or Nan.");
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final JSONObject m7893() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("media", (Object) this.f7026.m7842());
            if (this.f7023 != 0) {
                jSONObject.put("itemId", this.f7023);
            }
            jSONObject.put("autoplay", this.f7025);
            jSONObject.put("startTime", this.f7024);
            if (this.f7022 != Double.POSITIVE_INFINITY) {
                jSONObject.put("playbackDuration", this.f7022);
            }
            jSONObject.put("preloadTime", this.f7018);
            if (this.f7019 != null) {
                JSONArray jSONArray = new JSONArray();
                for (long put : this.f7019) {
                    jSONArray.put(put);
                }
                jSONObject.put("activeTrackIds", (Object) jSONArray);
            }
            if (this.f7021 != null) {
                jSONObject.put("customData", (Object) this.f7021);
            }
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public double m7894() {
        return this.f7022;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m7895() {
        return this.f7023;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public double m7896() {
        return this.f7024;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m7897() {
        return this.f7025;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaInfo m7898() {
        return this.f7026;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m7899(JSONObject jSONObject) throws JSONException {
        boolean z;
        boolean z2;
        int i;
        boolean z3 = false;
        if (jSONObject.has("media")) {
            this.f7026 = new MediaInfo(jSONObject.getJSONObject("media"));
            z = true;
        } else {
            z = false;
        }
        if (jSONObject.has("itemId") && this.f7023 != (i = jSONObject.getInt("itemId"))) {
            this.f7023 = i;
            z = true;
        }
        if (jSONObject.has("autoplay") && this.f7025 != (z2 = jSONObject.getBoolean("autoplay"))) {
            this.f7025 = z2;
            z = true;
        }
        if (jSONObject.has("startTime")) {
            double d = jSONObject.getDouble("startTime");
            if (Math.abs(d - this.f7024) > 1.0E-7d) {
                this.f7024 = d;
                z = true;
            }
        }
        if (jSONObject.has("playbackDuration")) {
            double d2 = jSONObject.getDouble("playbackDuration");
            if (Math.abs(d2 - this.f7022) > 1.0E-7d) {
                this.f7022 = d2;
                z = true;
            }
        }
        if (jSONObject.has("preloadTime")) {
            double d3 = jSONObject.getDouble("preloadTime");
            if (Math.abs(d3 - this.f7018) > 1.0E-7d) {
                this.f7018 = d3;
                z = true;
            }
        }
        long[] jArr = null;
        if (jSONObject.has("activeTrackIds")) {
            JSONArray jSONArray = jSONObject.getJSONArray("activeTrackIds");
            int length = jSONArray.length();
            jArr = new long[length];
            for (int i2 = 0; i2 < length; i2++) {
                jArr[i2] = jSONArray.getLong(i2);
            }
            if (this.f7019 == null) {
                z3 = true;
            } else if (this.f7019.length != length) {
                z3 = true;
            } else {
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (this.f7019[i3] != jArr[i3]) {
                        z3 = true;
                        break;
                    } else {
                        i3++;
                    }
                }
            }
        }
        if (z3) {
            this.f7019 = jArr;
            z = true;
        }
        if (!jSONObject.has("customData")) {
            return z;
        }
        this.f7021 = jSONObject.getJSONObject("customData");
        return true;
    }
}
