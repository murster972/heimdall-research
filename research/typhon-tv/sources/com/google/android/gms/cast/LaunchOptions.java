package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;
import java.util.Locale;

public class LaunchOptions extends zzbfm {
    public static final Parcelable.Creator<LaunchOptions> CREATOR = new zzad();

    /* renamed from: 靐  reason: contains not printable characters */
    private String f6985;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f6986;

    public static final class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private LaunchOptions f6987 = new LaunchOptions();

        /* renamed from: 龘  reason: contains not printable characters */
        public final Builder m7834(Locale locale) {
            this.f6987.m7832(zzbcm.m10041(locale));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final LaunchOptions m7835() {
            return this.f6987;
        }
    }

    public LaunchOptions() {
        this(false, zzbcm.m10041(Locale.getDefault()));
    }

    LaunchOptions(boolean z, String str) {
        this.f6986 = z;
        this.f6985 = str;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LaunchOptions)) {
            return false;
        }
        LaunchOptions launchOptions = (LaunchOptions) obj;
        return this.f6986 == launchOptions.f6986 && zzbcm.m10043(this.f6985, launchOptions.f6985);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Boolean.valueOf(this.f6986), this.f6985});
    }

    public String toString() {
        return String.format("LaunchOptions(relaunchIfRunning=%b, language=%s)", new Object[]{Boolean.valueOf(this.f6986), this.f6985});
    }

    public void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10195(parcel, 2, m7833());
        zzbfp.m10193(parcel, 3, m7831(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7831() {
        return this.f6985;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7832(String str) {
        this.f6985 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7833() {
        return this.f6986;
    }
}
