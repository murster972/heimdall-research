package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Status;

final class zzao implements RemoteMediaClient.MediaChannelResult {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Status f7374;

    zzao(RemoteMediaClient.zzb zzb, Status status) {
        this.f7374 = status;
    }

    public final Status s_() {
        return this.f7374;
    }
}
