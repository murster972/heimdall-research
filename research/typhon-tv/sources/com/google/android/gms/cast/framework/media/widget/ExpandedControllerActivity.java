package com.google.android.gms.cast.framework.media.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.gms.R;
import com.google.android.gms.cast.AdBreakClipInfo;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.internal.zzazn;
import com.google.android.gms.internal.zzazo;
import com.google.android.gms.internal.zzbac;
import com.google.android.gms.internal.zzbaj;

public class ExpandedControllerActivity extends AppCompatActivity {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7303;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7304;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7305;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f7306;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f7307;

    /* renamed from: ˆ  reason: contains not printable characters */
    private ImageView f7308;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f7309;

    /* renamed from: ˉ  reason: contains not printable characters */
    private zzbac f7310;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public TextView f7311;

    /* renamed from: ˋ  reason: contains not printable characters */
    private SeekBar f7312;

    /* renamed from: ˎ  reason: contains not printable characters */
    private ImageView f7313;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int[] f7314;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f7315;

    /* renamed from: י  reason: contains not printable characters */
    private ImageView[] f7316 = new ImageView[4];

    /* renamed from: ـ  reason: contains not printable characters */
    private View f7317;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f7318;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f7319;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public ImageView f7320;
    /* access modifiers changed from: private */

    /* renamed from: ᵎ  reason: contains not printable characters */
    public TextView f7321;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private TextView f7322;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private zzazn f7323;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private UIMediaController f7324;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f7325;

    /* renamed from: 靐  reason: contains not printable characters */
    private final RemoteMediaClient.Listener f7326 = new zza(this, (zza) null);

    /* renamed from: 麤  reason: contains not printable characters */
    private int f7327;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f7328;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SessionManagerListener<CastSession> f7329 = new zzb(this, (zza) null);

    /* renamed from: ﹳ  reason: contains not printable characters */
    private SessionManager f7330;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f7331;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f7332;
    /* access modifiers changed from: private */

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public boolean f7333;

    class zza implements RemoteMediaClient.Listener {
        private zza() {
        }

        /* synthetic */ zza(ExpandedControllerActivity expandedControllerActivity, zza zza) {
            this();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public final void m8309() {
            ExpandedControllerActivity.this.m8297();
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final void m8310() {
            ExpandedControllerActivity.this.f7311.setText(ExpandedControllerActivity.this.getResources().getString(R.string.cast_expanded_controller_loading));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m8311() {
            ExpandedControllerActivity.this.m8304();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final void m8312() {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m8313() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8314() {
            RemoteMediaClient r0 = ExpandedControllerActivity.this.m8300();
            if (r0 != null && r0.m4143()) {
                boolean unused = ExpandedControllerActivity.this.f7333 = false;
                ExpandedControllerActivity.this.m8301();
                ExpandedControllerActivity.this.m8297();
            } else if (!ExpandedControllerActivity.this.f7333) {
                ExpandedControllerActivity.this.finish();
            }
        }
    }

    class zzb implements SessionManagerListener<CastSession> {
        private zzb() {
        }

        /* synthetic */ zzb(ExpandedControllerActivity expandedControllerActivity, zza zza) {
            this();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8315(Session session) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final /* synthetic */ void m8316(Session session, int i) {
            ExpandedControllerActivity.this.finish();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8317(Session session, String str) {
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8318(Session session, int i) {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8319(Session session, int i) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8320(Session session) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8321(Session session, int i) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8322(Session session, String str) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* bridge */ /* synthetic */ void m8323(Session session, boolean z) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m8297() {
        String str;
        String str2;
        Drawable drawable;
        Bitmap bitmap;
        Bitmap r0;
        RemoteMediaClient r4 = m8300();
        MediaInfo r5 = r4 == null ? null : r4.m4137();
        MediaStatus r42 = r4 == null ? null : r4.m4136();
        if (r42 != null && r42.m7923()) {
            if (zzq.m9271() && this.f7308.getVisibility() == 8 && (drawable = this.f7313.getDrawable()) != null && (drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) drawable).getBitmap()) != null && (r0 = zzb.m8327(this, bitmap, 0.25f, 7.5f)) != null) {
                this.f7308.setImageBitmap(r0);
                this.f7308.setVisibility(0);
            }
            AdBreakClipInfo r02 = r42.m7909();
            if (r02 != null) {
                String r1 = r02.m7769();
                str = r02.m7770();
                str2 = r1;
            } else {
                str = null;
                str2 = null;
            }
            this.f7321.setVisibility(0);
            if (!TextUtils.isEmpty(str)) {
                this.f7323.m9837(Uri.parse(str));
            } else {
                this.f7320.setVisibility(8);
            }
            TextView textView = this.f7322;
            if (TextUtils.isEmpty(str2)) {
                str2 = getResources().getString(R.string.cast_ad_label);
            }
            textView.setText(str2);
            this.f7312.setEnabled(false);
            this.f7317.setVisibility(0);
        } else {
            this.f7312.setEnabled(true);
            this.f7317.setVisibility(8);
            if (zzq.m9271()) {
                this.f7308.setVisibility(8);
                this.f7308.setImageBitmap((Bitmap) null);
            }
        }
        if (r5 != null) {
            this.f7310.m9874(this.f7312.getMax());
            this.f7310.m9875(r5.m7840(), -1);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final RemoteMediaClient m8300() {
        CastSession r0 = this.f7330.m8075();
        if (r0 == null || !r0.m8053()) {
            return null;
        }
        return r0.m8022();
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8301() {
        CastDevice r0;
        CastSession r02 = this.f7330.m8075();
        if (!(r02 == null || (r0 = r02.m8014()) == null)) {
            String r03 = r0.m7826();
            if (!TextUtils.isEmpty(r03)) {
                this.f7311.setText(getResources().getString(R.string.cast_casting_to_device, new Object[]{r03}));
                return;
            }
        }
        this.f7311.setText("");
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8304() {
        MediaInfo r0;
        MediaMetadata r02;
        ActionBar supportActionBar;
        RemoteMediaClient r03 = m8300();
        if (r03 != null && r03.m4143() && (r0 = r03.m4137()) != null && (r02 = r0.m7846()) != null && (supportActionBar = getSupportActionBar()) != null) {
            supportActionBar.m440((CharSequence) r02.m7876("com.google.android.gms.cast.metadata.TITLE"));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final ColorStateList m8305() {
        int color = getResources().getColor(this.f7327);
        TypedValue typedValue = new TypedValue();
        getResources().getValue(R.dimen.cast_expanded_controller_seekbar_disabled_alpha, typedValue, true);
        int[] iArr = {color, Color.argb((int) (typedValue.getFloat() * ((float) Color.alpha(color))), Color.red(color), Color.green(color), Color.blue(color))};
        return new ColorStateList(new int[][]{new int[]{16842910}, new int[]{-16842910}}, iArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8307(View view, int i, int i2, UIMediaController uIMediaController) {
        ImageView imageView = (ImageView) view.findViewById(i);
        if (i2 == R.id.cast_button_type_empty) {
            imageView.setVisibility(4);
        } else if (i2 == R.id.cast_button_type_custom) {
        } else {
            if (i2 == R.id.cast_button_type_play_pause_toggle) {
                imageView.setBackgroundResource(this.f7328);
                Drawable r3 = zzb.m8326(this, this.f7332, this.f7305);
                Drawable r2 = zzb.m8326(this, this.f7332, this.f7304);
                Drawable r4 = zzb.m8326(this, this.f7332, this.f7315);
                imageView.setImageDrawable(r2);
                uIMediaController.m8271(imageView, r2, r3, r4, (View) null, false);
            } else if (i2 == R.id.cast_button_type_skip_previous) {
                imageView.setBackgroundResource(this.f7328);
                imageView.setImageDrawable(zzb.m8326(this, this.f7332, this.f7318));
                imageView.setContentDescription(getResources().getString(R.string.cast_skip_prev));
                uIMediaController.m8242((View) imageView, 0);
            } else if (i2 == R.id.cast_button_type_skip_next) {
                imageView.setBackgroundResource(this.f7328);
                imageView.setImageDrawable(zzb.m8326(this, this.f7332, this.f7319));
                imageView.setContentDescription(getResources().getString(R.string.cast_skip_next));
                uIMediaController.m8267((View) imageView, 0);
            } else if (i2 == R.id.cast_button_type_rewind_30_seconds) {
                imageView.setBackgroundResource(this.f7328);
                imageView.setImageDrawable(zzb.m8326(this, this.f7332, this.f7309));
                imageView.setContentDescription(getResources().getString(R.string.cast_rewind_30));
                uIMediaController.m8243((View) imageView, 30000);
            } else if (i2 == R.id.cast_button_type_forward_30_seconds) {
                imageView.setBackgroundResource(this.f7328);
                imageView.setImageDrawable(zzb.m8326(this, this.f7332, this.f7306));
                imageView.setContentDescription(getResources().getString(R.string.cast_forward_30));
                uIMediaController.m8268((View) imageView, 30000);
            } else if (i2 == R.id.cast_button_type_mute_toggle) {
                imageView.setBackgroundResource(this.f7328);
                imageView.setImageDrawable(zzb.m8326(this, this.f7332, this.f7307));
                uIMediaController.m8270(imageView);
            } else if (i2 == R.id.cast_button_type_closed_caption) {
                imageView.setBackgroundResource(this.f7328);
                imageView.setImageDrawable(zzb.m8326(this, this.f7332, this.f7331));
                uIMediaController.m8258((View) imageView);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f7330 = CastContext.m7977((Context) this).m7981();
        if (this.f7330.m8075() == null) {
            finish();
        }
        this.f7324 = new UIMediaController(this);
        this.f7324.m8293(this.f7326);
        setContentView(R.layout.cast_expanded_controller_activity);
        TypedArray obtainStyledAttributes = obtainStyledAttributes(new int[]{android.support.v7.appcompat.R.attr.selectableItemBackgroundBorderless, android.support.v7.appcompat.R.attr.colorControlActivated});
        this.f7328 = obtainStyledAttributes.getResourceId(0, 0);
        this.f7327 = obtainStyledAttributes.getResourceId(1, 0);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = obtainStyledAttributes((AttributeSet) null, R.styleable.CastExpandedController, R.attr.castExpandedControllerStyle, R.style.CastExpandedController);
        this.f7332 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castButtonColor, 0);
        this.f7325 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castSeekBarProgressDrawable, 0);
        this.f7303 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castSeekBarThumbDrawable, 0);
        this.f7304 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castPlayButtonDrawable, 0);
        this.f7305 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castPauseButtonDrawable, 0);
        this.f7315 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castStopButtonDrawable, 0);
        this.f7318 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castSkipPreviousButtonDrawable, 0);
        this.f7319 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castSkipNextButtonDrawable, 0);
        this.f7309 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castRewind30ButtonDrawable, 0);
        this.f7306 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castForward30ButtonDrawable, 0);
        this.f7307 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castMuteToggleButtonDrawable, 0);
        this.f7331 = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castClosedCaptionsButtonDrawable, 0);
        int resourceId = obtainStyledAttributes2.getResourceId(R.styleable.CastExpandedController_castControlButtons, 0);
        if (resourceId != 0) {
            TypedArray obtainTypedArray = getResources().obtainTypedArray(resourceId);
            zzbq.m9116(obtainTypedArray.length() == 4);
            this.f7314 = new int[obtainTypedArray.length()];
            for (int i = 0; i < obtainTypedArray.length(); i++) {
                this.f7314[i] = obtainTypedArray.getResourceId(i, 0);
            }
            obtainTypedArray.recycle();
        } else {
            this.f7314 = new int[]{R.id.cast_button_type_empty, R.id.cast_button_type_empty, R.id.cast_button_type_empty, R.id.cast_button_type_empty};
        }
        obtainStyledAttributes2.recycle();
        View findViewById = findViewById(R.id.expanded_controller_layout);
        UIMediaController uIMediaController = this.f7324;
        this.f7313 = (ImageView) findViewById.findViewById(R.id.background_image_view);
        this.f7308 = (ImageView) findViewById.findViewById(R.id.blurred_background_image_view);
        View findViewById2 = findViewById.findViewById(R.id.background_place_holder_image_view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        uIMediaController.m8273(this.f7313, new ImageHints(4, displayMetrics.widthPixels, displayMetrics.heightPixels), findViewById2);
        this.f7311 = (TextView) findViewById.findViewById(R.id.status_text);
        uIMediaController.m8266((View) (ProgressBar) findViewById.findViewById(R.id.loading_indicator));
        TextView textView = (TextView) findViewById.findViewById(R.id.start_text);
        TextView textView2 = (TextView) findViewById.findViewById(R.id.end_text);
        ImageView imageView = (ImageView) findViewById.findViewById(R.id.live_stream_indicator);
        this.f7312 = (SeekBar) findViewById.findViewById(R.id.seek_bar);
        Drawable drawable = getResources().getDrawable(this.f7325);
        ColorStateList colorStateList = null;
        if (drawable != null) {
            if (this.f7325 == R.drawable.cast_expanded_controller_seekbar_track) {
                ColorStateList r7 = m8305();
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                Drawable wrap = DrawableCompat.wrap(layerDrawable.findDrawableByLayerId(16908301));
                DrawableCompat.setTintList(wrap, r7);
                layerDrawable.setDrawableByLayerId(16908301, wrap);
                layerDrawable.findDrawableByLayerId(16908288).setColorFilter(getResources().getColor(R.color.cast_expanded_controller_seek_bar_progress_background_tint_color), PorterDuff.Mode.SRC_IN);
                colorStateList = r7;
            }
            this.f7312.setProgressDrawable(drawable);
        }
        Drawable drawable2 = getResources().getDrawable(this.f7303);
        if (drawable2 != null) {
            if (this.f7303 == R.drawable.cast_expanded_controller_seekbar_thumb) {
                if (colorStateList == null) {
                    colorStateList = m8305();
                }
                drawable2 = DrawableCompat.wrap(drawable2);
                DrawableCompat.setTintList(drawable2, colorStateList);
            }
            this.f7312.setThumb(drawable2);
        }
        if (zzq.m9265()) {
            this.f7312.setSplitTrack(false);
        }
        SeekBar seekBar = (SeekBar) findViewById.findViewById(R.id.live_stream_seek_bar);
        uIMediaController.m8283(textView, true);
        uIMediaController.m8280(textView2, (View) imageView);
        uIMediaController.m8276(this.f7312);
        uIMediaController.m8269((View) seekBar, (UIController) new zzbaj(seekBar, this.f7312));
        this.f7316[0] = (ImageView) findViewById.findViewById(R.id.button_0);
        this.f7316[1] = (ImageView) findViewById.findViewById(R.id.button_1);
        this.f7316[2] = (ImageView) findViewById.findViewById(R.id.button_2);
        this.f7316[3] = (ImageView) findViewById.findViewById(R.id.button_3);
        m8307(findViewById, R.id.button_0, this.f7314[0], uIMediaController);
        m8307(findViewById, R.id.button_1, this.f7314[1], uIMediaController);
        m8307(findViewById, R.id.button_play_pause_toggle, R.id.cast_button_type_play_pause_toggle, uIMediaController);
        m8307(findViewById, R.id.button_2, this.f7314[2], uIMediaController);
        m8307(findViewById, R.id.button_3, this.f7314[3], uIMediaController);
        this.f7317 = findViewById(R.id.ad_container);
        this.f7320 = (ImageView) this.f7317.findViewById(R.id.ad_image_view);
        this.f7322 = (TextView) this.f7317.findViewById(R.id.ad_label);
        this.f7321 = (TextView) this.f7317.findViewById(R.id.ad_in_progress_label);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById.findViewById(R.id.seek_bar_controls);
        zzbac zzbac = new zzbac(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(0, R.id.end_text);
        layoutParams.addRule(1, R.id.start_text);
        layoutParams.addRule(6, R.id.seek_bar);
        layoutParams.addRule(7, R.id.seek_bar);
        layoutParams.addRule(5, R.id.seek_bar);
        layoutParams.addRule(8, R.id.seek_bar);
        zzbac.setLayoutParams(layoutParams);
        if (zzq.m9271()) {
            zzbac.setPaddingRelative(this.f7312.getPaddingStart(), this.f7312.getPaddingTop(), this.f7312.getPaddingEnd(), this.f7312.getPaddingBottom());
        } else {
            zzbac.setPadding(this.f7312.getPaddingLeft(), this.f7312.getPaddingTop(), this.f7312.getPaddingRight(), this.f7312.getPaddingBottom());
        }
        zzbac.setContentDescription(getResources().getString(R.string.cast_seek_bar));
        zzbac.setBackgroundColor(0);
        relativeLayout.addView(zzbac);
        this.f7310 = zzbac;
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().m427(true);
            getSupportActionBar().m425(R.drawable.quantum_ic_keyboard_arrow_down_white_36);
        }
        m8301();
        m8304();
        this.f7323 = new zzazn(getApplicationContext(), new ImageHints(-1, this.f7320.getWidth(), this.f7320.getHeight()));
        this.f7323.m9836((zzazo) new zza(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f7323.m9834();
        if (this.f7324 != null) {
            this.f7324.m8293((RemoteMediaClient.Listener) null);
            this.f7324.m8237();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return true;
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        CastContext.m7977((Context) this).m7981().m8077(this.f7329, CastSession.class);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        CastContext.m7977((Context) this).m7981().m8082(this.f7329, CastSession.class);
        CastSession r0 = CastContext.m7977((Context) this).m7981().m8075();
        if (r0 == null || (!r0.m8053() && !r0.m8054())) {
            finish();
        }
        RemoteMediaClient r02 = m8300();
        this.f7333 = r02 == null || !r02.m4143();
        m8301();
        m8297();
        super.onResume();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            int systemUiVisibility = getWindow().getDecorView().getSystemUiVisibility() ^ 2;
            if (zzq.m9269()) {
                systemUiVisibility ^= 4;
            }
            if (zzq.m9268()) {
                systemUiVisibility ^= 4096;
            }
            getWindow().getDecorView().setSystemUiVisibility(systemUiVisibility);
            if (zzq.m9270()) {
                setImmersive(true);
            }
        }
    }
}
