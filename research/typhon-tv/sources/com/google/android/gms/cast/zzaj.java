package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzaj implements Parcelable.Creator<MediaStatus> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r3 = zzbfn.m10169(parcel);
        MediaInfo mediaInfo = null;
        long j = 0;
        int i = 0;
        double d = 0.0d;
        int i2 = 0;
        int i3 = 0;
        long j2 = 0;
        long j3 = 0;
        double d2 = 0.0d;
        boolean z = false;
        long[] jArr = null;
        int i4 = 0;
        int i5 = 0;
        String str = null;
        int i6 = 0;
        ArrayList<MediaQueueItem> arrayList = null;
        boolean z2 = false;
        AdBreakStatus adBreakStatus = null;
        VideoInfo videoInfo = null;
        while (parcel.dataPosition() < r3) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    mediaInfo = (MediaInfo) zzbfn.m10171(parcel, readInt, MediaInfo.CREATOR);
                    break;
                case 3:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 4:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 5:
                    d = zzbfn.m10160(parcel, readInt);
                    break;
                case 6:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 7:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 8:
                    j2 = zzbfn.m10163(parcel, readInt);
                    break;
                case 9:
                    j3 = zzbfn.m10163(parcel, readInt);
                    break;
                case 10:
                    d2 = zzbfn.m10160(parcel, readInt);
                    break;
                case 11:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 12:
                    jArr = zzbfn.m10175(parcel, readInt);
                    break;
                case 13:
                    i4 = zzbfn.m10166(parcel, readInt);
                    break;
                case 14:
                    i5 = zzbfn.m10166(parcel, readInt);
                    break;
                case 15:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 16:
                    i6 = zzbfn.m10166(parcel, readInt);
                    break;
                case 17:
                    arrayList = zzbfn.m10167(parcel, readInt, MediaQueueItem.CREATOR);
                    break;
                case 18:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 19:
                    adBreakStatus = (AdBreakStatus) zzbfn.m10171(parcel, readInt, AdBreakStatus.CREATOR);
                    break;
                case 20:
                    videoInfo = (VideoInfo) zzbfn.m10171(parcel, readInt, VideoInfo.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r3);
        return new MediaStatus(mediaInfo, j, i, d, i2, i3, j2, j3, d2, z, jArr, i4, i5, str, i6, arrayList, z2, adBreakStatus, videoInfo);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaStatus[i];
    }
}
