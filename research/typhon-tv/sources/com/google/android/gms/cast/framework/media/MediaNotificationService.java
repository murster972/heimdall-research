package com.google.android.gms.cast.framework.media;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzayu;
import com.google.android.gms.internal.zzbcy;

public class MediaNotificationService extends Service {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbcy f7212 = new zzbcy("MediaNotificationService");

    /* renamed from: 靐  reason: contains not printable characters */
    private zzd f7213;

    public IBinder onBind(Intent intent) {
        try {
            return this.f7213.m8346(intent);
        } catch (RemoteException e) {
            f7212.m10091(e, "Unable to call %s on %s.", "onBind", zzd.class.getSimpleName());
            return null;
        }
    }

    public void onCreate() {
        this.f7213 = zzayu.m9760((Service) this, CastContext.m7977((Context) this).m7979(), zzn.m9306(null), CastContext.m7977((Context) this).m7984().m7986());
        try {
            this.f7213.m8347();
        } catch (RemoteException e) {
            f7212.m10091(e, "Unable to call %s on %s.", "onCreate", zzd.class.getSimpleName());
        }
        super.onCreate();
    }

    public void onDestroy() {
        try {
            this.f7213.m8344();
        } catch (RemoteException e) {
            f7212.m10091(e, "Unable to call %s on %s.", "onDestroy", zzd.class.getSimpleName());
        }
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        try {
            return this.f7213.m8345(intent, i, i2);
        } catch (RemoteException e) {
            f7212.m10091(e, "Unable to call %s on %s.", "onStartCommand", zzd.class.getSimpleName());
            return 1;
        }
    }
}
