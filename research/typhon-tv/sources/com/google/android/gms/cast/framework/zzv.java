package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzv extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    IObjectWrapper m8438() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m8439(zzx zzx) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    int m8440() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m8441() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8442(zzx zzx) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8443(boolean z, boolean z2) throws RemoteException;
}
