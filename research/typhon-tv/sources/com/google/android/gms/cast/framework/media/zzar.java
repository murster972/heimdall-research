package com.google.android.gms.cast.framework.media;

import android.app.Dialog;
import android.content.DialogInterface;

final class zzar implements DialogInterface.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ TracksChooserDialogFragment f7378;

    zzar(TracksChooserDialogFragment tracksChooserDialogFragment) {
        this.f7378 = tracksChooserDialogFragment;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.f7378.f7281 != null) {
            this.f7378.f7281.cancel();
            Dialog unused = this.f7378.f7281 = null;
        }
    }
}
