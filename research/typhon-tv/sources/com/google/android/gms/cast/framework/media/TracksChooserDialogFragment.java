package com.google.android.gms.cast.framework.media;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TabHost;
import com.google.android.gms.R;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class TracksChooserDialogFragment extends DialogFragment {

    /* renamed from: 靐  reason: contains not printable characters */
    private List<MediaTrack> f7280;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Dialog f7281;

    /* renamed from: 齉  reason: contains not printable characters */
    private long[] f7282;

    /* renamed from: 龘  reason: contains not printable characters */
    private List<MediaTrack> f7283;

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m8216(List<MediaTrack> list, long[] jArr, int i) {
        if (jArr == null || list == null) {
            return i;
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            for (long j : jArr) {
                if (j == list.get(i2).m7935()) {
                    return i2;
                }
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TracksChooserDialogFragment m8219(MediaInfo mediaInfo, long[] jArr) {
        List<MediaTrack> r1;
        if (mediaInfo == null || (r1 = mediaInfo.m7838()) == null) {
            return null;
        }
        ArrayList<MediaTrack> r2 = m8220(r1, 2);
        ArrayList<MediaTrack> r12 = m8220(r1, 1);
        if (r2.size() <= 1 && r12.isEmpty()) {
            return null;
        }
        TracksChooserDialogFragment tracksChooserDialogFragment = new TracksChooserDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("extra_tracks_type_audio", r2);
        bundle.putParcelableArrayList("extra_tracks_type_text", r12);
        bundle.putLongArray("extra_active_track_ids", jArr);
        tracksChooserDialogFragment.setArguments(bundle);
        return tracksChooserDialogFragment;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ArrayList<MediaTrack> m8220(List<MediaTrack> list, int i) {
        ArrayList<MediaTrack> arrayList = new ArrayList<>();
        if (list != null) {
            for (MediaTrack next : list) {
                if (next.m7929() == i) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8222(zzat zzat, zzat zzat2) {
        RemoteMediaClient r2;
        CastSession r0 = CastContext.m7977(getContext()).m7981().m8075();
        if (r0 != null && r0.m8053() && (r2 = r0.m8022()) != null && r2.m4143()) {
            ArrayList arrayList = new ArrayList();
            MediaTrack r02 = zzat.m8335();
            if (!(r02 == null || r02.m7935() == -1)) {
                arrayList.add(Long.valueOf(r02.m7935()));
            }
            MediaTrack r03 = zzat2.m8335();
            if (r03 != null) {
                arrayList.add(Long.valueOf(r03.m7935()));
            }
            long[] r4 = r2.m4136().m7910();
            if (r4 != null && r4.length > 0) {
                HashSet hashSet = new HashSet();
                for (MediaTrack r04 : this.f7280) {
                    hashSet.add(Long.valueOf(r04.m7935()));
                }
                for (MediaTrack r05 : this.f7283) {
                    hashSet.add(Long.valueOf(r05.m7935()));
                }
                for (long j : r4) {
                    if (!hashSet.contains(Long.valueOf(j))) {
                        arrayList.add(Long.valueOf(j));
                    }
                }
            }
            long[] jArr = new long[arrayList.size()];
            for (int i = 0; i < arrayList.size(); i++) {
                jArr[i] = ((Long) arrayList.get(i)).longValue();
            }
            Arrays.sort(jArr);
            r2.m4162(jArr);
            if (this.f7281 != null) {
                this.f7281.cancel();
                this.f7281 = null;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("extra_tracks_type_text");
        if (parcelableArrayList != null && !parcelableArrayList.isEmpty()) {
            this.f7283 = new ArrayList(parcelableArrayList);
            this.f7283.add(0, new MediaTrack.Builder(-1, 1).m7940(getActivity().getString(R.string.cast_tracks_chooser_dialog_none)).m7941(2).m7942("").m7943());
        }
        this.f7280 = getArguments().getParcelableArrayList("extra_tracks_type_audio");
        this.f7282 = getArguments().getLongArray("extra_active_track_ids");
    }

    public Dialog onCreateDialog(Bundle bundle) {
        int r0 = m8216(this.f7283, this.f7282, 0);
        int r1 = m8216(this.f7280, this.f7282, -1);
        zzat zzat = new zzat(getActivity(), this.f7283, r0);
        zzat zzat2 = new zzat(getActivity(), this.f7280, r1);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View inflate = getActivity().getLayoutInflater().inflate(R.layout.cast_tracks_chooser_dialog_layout, (ViewGroup) null);
        ListView listView = (ListView) inflate.findViewById(R.id.text_list_view);
        ListView listView2 = (ListView) inflate.findViewById(R.id.audio_list_view);
        TabHost tabHost = (TabHost) inflate.findViewById(R.id.tab_host);
        tabHost.setup();
        if (zzat.getCount() == 0) {
            listView.setVisibility(4);
        } else {
            listView.setAdapter(zzat);
            TabHost.TabSpec newTabSpec = tabHost.newTabSpec("textTab");
            newTabSpec.setContent(R.id.text_list_view);
            newTabSpec.setIndicator(getActivity().getString(R.string.cast_tracks_chooser_dialog_subtitles));
            tabHost.addTab(newTabSpec);
        }
        if (zzat2.getCount() <= 1) {
            listView2.setVisibility(4);
        } else {
            listView2.setAdapter(zzat2);
            TabHost.TabSpec newTabSpec2 = tabHost.newTabSpec("audioTab");
            newTabSpec2.setContent(R.id.audio_list_view);
            newTabSpec2.setIndicator(getActivity().getString(R.string.cast_tracks_chooser_dialog_audio));
            tabHost.addTab(newTabSpec2);
        }
        builder.setView(inflate).setPositiveButton(getActivity().getString(R.string.cast_tracks_chooser_dialog_ok), new zzas(this, zzat, zzat2)).setNegativeButton(R.string.cast_tracks_chooser_dialog_cancel, new zzar(this));
        if (this.f7281 != null) {
            this.f7281.cancel();
            this.f7281 = null;
        }
        this.f7281 = builder.create();
        return this.f7281;
    }

    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage((Message) null);
        }
        super.onDestroyView();
    }
}
