package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class zzb extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zza f7182;

    zzb(zza zza) {
        this.f7182 = zza;
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        if (this.f7182.m8118(x, y) && this.f7182.f7179.m8110(x, y)) {
            return true;
        }
        this.f7182.f7171.m8130();
        return true;
    }
}
