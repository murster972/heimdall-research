package com.google.android.gms.cast.framework;

import com.google.android.gms.cast.framework.Session;

public interface SessionManagerListener<T extends Session> {
    /* renamed from: 靐  reason: contains not printable characters */
    void m8084(T t);

    /* renamed from: 靐  reason: contains not printable characters */
    void m8085(T t, int i);

    /* renamed from: 靐  reason: contains not printable characters */
    void m8086(T t, String str);

    /* renamed from: 麤  reason: contains not printable characters */
    void m8087(T t, int i);

    /* renamed from: 齉  reason: contains not printable characters */
    void m8088(T t, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8089(T t);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8090(T t, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8091(T t, String str);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8092(T t, boolean z);
}
