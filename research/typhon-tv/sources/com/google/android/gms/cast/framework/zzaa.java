package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzev;
import com.google.android.gms.internal.zzew;

public abstract class zzaa extends zzev implements zzz {
    public zzaa() {
        attachInterface(this, "com.google.android.gms.cast.framework.ISessionProvider");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                IObjectWrapper r1 = m8461(parcel.readString());
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r1);
                return true;
            case 2:
                boolean r12 = m8462();
                parcel2.writeNoException();
                zzew.m12307(parcel2, r12);
                return true;
            case 3:
                String r13 = m8460();
                parcel2.writeNoException();
                parcel2.writeString(r13);
                return true;
            case 4:
                parcel2.writeNoException();
                parcel2.writeInt(11910208);
                return true;
            default:
                return false;
        }
    }
}
