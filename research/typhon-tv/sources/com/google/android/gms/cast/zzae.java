package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;
import java.util.ArrayList;

public final class zzae implements Parcelable.Creator<MediaInfo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r1 = zzbfn.m10169(parcel);
        int i = 0;
        long j = 0;
        ArrayList<AdBreakClipInfo> arrayList = null;
        ArrayList<AdBreakInfo> arrayList2 = null;
        String str = null;
        TextTrackStyle textTrackStyle = null;
        ArrayList<MediaTrack> arrayList3 = null;
        MediaMetadata mediaMetadata = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < r1) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str3 = zzbfn.m10162(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    str2 = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    mediaMetadata = (MediaMetadata) zzbfn.m10171(parcel, readInt, MediaMetadata.CREATOR);
                    break;
                case 6:
                    j = zzbfn.m10163(parcel, readInt);
                    break;
                case 7:
                    arrayList3 = zzbfn.m10167(parcel, readInt, MediaTrack.CREATOR);
                    break;
                case 8:
                    textTrackStyle = (TextTrackStyle) zzbfn.m10171(parcel, readInt, TextTrackStyle.CREATOR);
                    break;
                case 9:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 10:
                    arrayList2 = zzbfn.m10167(parcel, readInt, AdBreakInfo.CREATOR);
                    break;
                case 11:
                    arrayList = zzbfn.m10167(parcel, readInt, AdBreakClipInfo.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r1);
        return new MediaInfo(str3, i, str2, mediaMetadata, j, arrayList3, textTrackStyle, str, arrayList2, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaInfo[i];
    }
}
