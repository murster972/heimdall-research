package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbbv;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbcx;
import java.io.IOException;

public final class Cast {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final CastApi f6961 = new CastApi.zza();

    /* renamed from: 齉  reason: contains not printable characters */
    private static Api.zza<zzbcf, CastOptions> f6962 = new zze();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Api<CastOptions> f6963 = new Api<>("Cast.API", f6962, zzbcx.f8682);

    @Deprecated
    public interface CastApi {

        public static final class zza implements CastApi {
            /* renamed from: 龘  reason: contains not printable characters */
            private final PendingResult<ApplicationConnectionResult> m7794(GoogleApiClient googleApiClient, String str, String str2, zzab zzab) {
                return googleApiClient.zze(new zzi(this, googleApiClient, str, str2, (zzab) null));
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public final PendingResult<ApplicationConnectionResult> m7795(GoogleApiClient googleApiClient, String str, String str2) {
                return m7794(googleApiClient, str, str2, (zzab) null);
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public final void m7796(GoogleApiClient googleApiClient, String str) throws IOException, IllegalArgumentException {
                try {
                    ((zzbcf) googleApiClient.zza(zzbcx.f8682)).m10011(str);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public final boolean m7797(GoogleApiClient googleApiClient) throws IllegalStateException {
                return ((zzbcf) googleApiClient.zza(zzbcx.f8682)).m10003();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final double m7798(GoogleApiClient googleApiClient) throws IllegalStateException {
                return ((zzbcf) googleApiClient.zza(zzbcx.f8682)).m10004();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final PendingResult<Status> m7799(GoogleApiClient googleApiClient, String str) {
                return googleApiClient.zze(new zzl(this, googleApiClient, str));
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final PendingResult<ApplicationConnectionResult> m7800(GoogleApiClient googleApiClient, String str, LaunchOptions launchOptions) {
                return googleApiClient.zze(new zzh(this, googleApiClient, str, launchOptions));
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final PendingResult<Status> m7801(GoogleApiClient googleApiClient, String str, String str2) {
                return googleApiClient.zze(new zzf(this, googleApiClient, str, str2));
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m7802(GoogleApiClient googleApiClient, double d) throws IOException, IllegalArgumentException, IllegalStateException {
                try {
                    ((zzbcf) googleApiClient.zza(zzbcx.f8682)).m10008(d);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m7803(GoogleApiClient googleApiClient, String str, MessageReceivedCallback messageReceivedCallback) throws IOException, IllegalStateException {
                try {
                    ((zzbcf) googleApiClient.zza(zzbcx.f8682)).m10012(str, messageReceivedCallback);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m7804(GoogleApiClient googleApiClient, boolean z) throws IOException, IllegalStateException {
                try {
                    ((zzbcf) googleApiClient.zza(zzbcx.f8682)).m10017(z);
                } catch (RemoteException e) {
                    throw new IOException("service error");
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        PendingResult<ApplicationConnectionResult> m4109(GoogleApiClient googleApiClient, String str, String str2);

        /* renamed from: 靐  reason: contains not printable characters */
        void m4110(GoogleApiClient googleApiClient, String str) throws IOException, IllegalArgumentException;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean m4111(GoogleApiClient googleApiClient) throws IllegalStateException;

        /* renamed from: 龘  reason: contains not printable characters */
        double m4112(GoogleApiClient googleApiClient) throws IllegalStateException;

        /* renamed from: 龘  reason: contains not printable characters */
        PendingResult<Status> m4113(GoogleApiClient googleApiClient, String str);

        /* renamed from: 龘  reason: contains not printable characters */
        PendingResult<ApplicationConnectionResult> m4114(GoogleApiClient googleApiClient, String str, LaunchOptions launchOptions);

        /* renamed from: 龘  reason: contains not printable characters */
        PendingResult<Status> m4115(GoogleApiClient googleApiClient, String str, String str2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m4116(GoogleApiClient googleApiClient, double d) throws IOException, IllegalArgumentException, IllegalStateException;

        /* renamed from: 龘  reason: contains not printable characters */
        void m4117(GoogleApiClient googleApiClient, String str, MessageReceivedCallback messageReceivedCallback) throws IOException, IllegalStateException;

        /* renamed from: 龘  reason: contains not printable characters */
        void m4118(GoogleApiClient googleApiClient, boolean z) throws IOException, IllegalStateException;
    }

    public interface ApplicationConnectionResult extends Result {
        /* renamed from: 靐  reason: contains not printable characters */
        String m7790();

        /* renamed from: 麤  reason: contains not printable characters */
        boolean m7791();

        /* renamed from: 齉  reason: contains not printable characters */
        String m7792();

        /* renamed from: 龘  reason: contains not printable characters */
        ApplicationMetadata m7793();
    }

    public static final class CastOptions implements Api.ApiOptions.HasOptions {

        /* renamed from: 靐  reason: contains not printable characters */
        final Listener f6964;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public final int f6965;

        /* renamed from: 齉  reason: contains not printable characters */
        final Bundle f6966;

        /* renamed from: 龘  reason: contains not printable characters */
        final CastDevice f6967;

        public static final class Builder {

            /* renamed from: 靐  reason: contains not printable characters */
            Listener f6968;
            /* access modifiers changed from: private */

            /* renamed from: 麤  reason: contains not printable characters */
            public Bundle f6969;
            /* access modifiers changed from: private */

            /* renamed from: 齉  reason: contains not printable characters */
            public int f6970 = 0;

            /* renamed from: 龘  reason: contains not printable characters */
            CastDevice f6971;

            public Builder(CastDevice castDevice, Listener listener) {
                zzbq.m9121(castDevice, (Object) "CastDevice parameter cannot be null");
                zzbq.m9121(listener, (Object) "CastListener parameter cannot be null");
                this.f6971 = castDevice;
                this.f6968 = listener;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final Builder m7808(Bundle bundle) {
                this.f6969 = bundle;
                return this;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final CastOptions m7809() {
                return new CastOptions(this, (zze) null);
            }
        }

        private CastOptions(Builder builder) {
            this.f6967 = builder.f6971;
            this.f6964 = builder.f6968;
            this.f6965 = builder.f6970;
            this.f6966 = builder.f6969;
        }

        /* synthetic */ CastOptions(Builder builder, zze zze) {
            this(builder);
        }
    }

    public static class Listener {
        /* renamed from: 靐  reason: contains not printable characters */
        public void m7810() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m7811(int i) {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m7812(int i) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m7813() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m7814(int i) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m7815(ApplicationMetadata applicationMetadata) {
        }
    }

    public interface MessageReceivedCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        void m7816(CastDevice castDevice, String str, String str2);
    }

    static abstract class zza extends zzbbv<ApplicationConnectionResult> {
        public zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final /* synthetic */ Result m7817(Status status) {
            return new zzm(this, status);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m7818(zzbcf zzbcf) throws RemoteException {
        }
    }
}
