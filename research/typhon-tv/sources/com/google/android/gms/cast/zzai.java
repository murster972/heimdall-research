package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfn;

public final class zzai implements Parcelable.Creator<MediaQueueItem> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r12 = zzbfn.m10169(parcel);
        MediaInfo mediaInfo = null;
        int i = 0;
        boolean z = false;
        double d = 0.0d;
        double d2 = 0.0d;
        double d3 = 0.0d;
        long[] jArr = null;
        String str = null;
        while (parcel.dataPosition() < r12) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    mediaInfo = (MediaInfo) zzbfn.m10171(parcel, readInt, MediaInfo.CREATOR);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    d = zzbfn.m10160(parcel, readInt);
                    break;
                case 6:
                    d2 = zzbfn.m10160(parcel, readInt);
                    break;
                case 7:
                    d3 = zzbfn.m10160(parcel, readInt);
                    break;
                case 8:
                    jArr = zzbfn.m10175(parcel, readInt);
                    break;
                case 9:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r12);
        return new MediaQueueItem(mediaInfo, i, z, d, d2, d3, jArr, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaQueueItem[i];
    }
}
