package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzev;

public interface zzt extends IInterface {

    public static abstract class zza extends zzev implements zzt {
        /* renamed from: 龘  reason: contains not printable characters */
        public static zzt m8428(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.ISession");
            return queryLocalInterface instanceof zzt ? (zzt) queryLocalInterface : new zzu(iBinder);
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m8419() throws RemoteException;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean m8420() throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    void m8421(int i) throws RemoteException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m8422() throws RemoteException;

    /* renamed from: 麤  reason: contains not printable characters */
    boolean m8423() throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m8424(int i) throws RemoteException;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m8425() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m8426() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m8427(int i) throws RemoteException;
}
