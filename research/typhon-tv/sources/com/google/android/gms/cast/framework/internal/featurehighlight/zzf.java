package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zzf extends AnimatorListenerAdapter {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zza f7189;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Runnable f7190;

    zzf(zza zza, Runnable runnable) {
        this.f7189 = zza;
        this.f7190 = runnable;
    }

    public final void onAnimationEnd(Animator animator) {
        this.f7189.setVisibility(8);
        Animator unused = this.f7189.f7174 = null;
        if (this.f7190 != null) {
            this.f7190.run();
        }
    }
}
