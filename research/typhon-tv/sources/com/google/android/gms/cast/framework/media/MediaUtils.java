package com.google.android.gms.cast.framework.media;

import android.annotation.TargetApi;
import android.net.Uri;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.common.util.zzq;
import java.util.Locale;

public class MediaUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Uri m8161(MediaInfo mediaInfo, int i) {
        MediaMetadata r1;
        if (mediaInfo == null || (r1 = mediaInfo.m7846()) == null || r1.m7878() == null || r1.m7878().size() <= i) {
            return null;
        }
        return r1.m7878().get(i).m9037();
    }

    @TargetApi(21)
    /* renamed from: 龘  reason: contains not printable characters */
    public static Locale m8162(MediaTrack mediaTrack) {
        if (mediaTrack.m7925() == null) {
            return null;
        }
        if (zzq.m9265()) {
            return Locale.forLanguageTag(mediaTrack.m7925());
        }
        String[] split = mediaTrack.m7925().split("-");
        return split.length == 1 ? new Locale(split[0]) : new Locale(split[0], split[1]);
    }
}
