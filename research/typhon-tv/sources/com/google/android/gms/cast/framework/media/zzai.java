package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzbcf;
import com.google.android.gms.internal.zzbdb;
import java.io.IOException;
import org.json.JSONObject;

final class zzai extends RemoteMediaClient.zzb {

    /* renamed from: ʻ  reason: contains not printable characters */
    private /* synthetic */ RemoteMediaClient f7366;

    /* renamed from: 连任  reason: contains not printable characters */
    private /* synthetic */ JSONObject f7367;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ int f7368;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ long f7369;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzai(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, long j, int i, JSONObject jSONObject) {
        super(remoteMediaClient, googleApiClient);
        this.f7366 = remoteMediaClient;
        this.f7369 = j;
        this.f7368 = i;
        this.f7367 = jSONObject;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8332(zzbcf zzbcf) {
        synchronized (this.f7366.f3638) {
            try {
                this.f7366.f3639.m10107(this.f7272, this.f7369, this.f7368, this.f7367);
            } catch (zzbdb | IOException e) {
                m4198((RemoteMediaClient.MediaChannelResult) m4195(new Status(2100)));
            }
        }
    }
}
