package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzev;
import com.google.android.gms.internal.zzew;

public abstract class zzy extends zzev implements zzx {
    public zzy() {
        attachInterface(this, "com.google.android.gms.cast.framework.ISessionManagerListener");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                IObjectWrapper r1 = m8455();
                parcel2.writeNoException();
                zzew.m12305(parcel2, (IInterface) r1);
                return true;
            case 2:
                m8456(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 3:
                m8458(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 4:
                m8457(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 5:
                m8450(IObjectWrapper.zza.m9305(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 6:
                m8451(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 7:
                m8452(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 8:
                m8459(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), zzew.m12308(parcel));
                parcel2.writeNoException();
                return true;
            case 9:
                m8454(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 10:
                m8453(IObjectWrapper.zza.m9305(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 11:
                parcel2.writeNoException();
                parcel2.writeInt(11910208);
                return true;
            default:
                return false;
        }
    }
}
