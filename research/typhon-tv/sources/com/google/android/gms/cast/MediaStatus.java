package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseArray;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzbcm;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaStatus extends zzbfm {
    public static final Parcelable.Creator<MediaStatus> CREATOR = new zzaj();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7028;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f7029;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f7030;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f7031;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f7032;

    /* renamed from: ˆ  reason: contains not printable characters */
    private VideoInfo f7033;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f7034;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final SparseArray<Integer> f7035;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ArrayList<MediaQueueItem> f7036;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f7037;

    /* renamed from: ˎ  reason: contains not printable characters */
    private AdBreakStatus f7038;

    /* renamed from: ˑ  reason: contains not printable characters */
    private double f7039;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f7040;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long[] f7041;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f7042;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f7043;

    /* renamed from: 麤  reason: contains not printable characters */
    private double f7044;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f7045;

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo f7046;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private JSONObject f7047;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f7048;

    MediaStatus(MediaInfo mediaInfo, long j, int i, double d, int i2, int i3, long j2, long j3, double d2, boolean z, long[] jArr, int i4, int i5, String str, int i6, List<MediaQueueItem> list, boolean z2, AdBreakStatus adBreakStatus, VideoInfo videoInfo) {
        this.f7036 = new ArrayList<>();
        this.f7035 = new SparseArray<>();
        this.f7046 = mediaInfo;
        this.f7043 = j;
        this.f7045 = i;
        this.f7044 = d;
        this.f7042 = i2;
        this.f7028 = i3;
        this.f7029 = j2;
        this.f7030 = j3;
        this.f7039 = d2;
        this.f7040 = z;
        this.f7041 = jArr;
        this.f7034 = i4;
        this.f7031 = i5;
        this.f7032 = str;
        if (this.f7032 != null) {
            try {
                this.f7047 = new JSONObject(this.f7032);
            } catch (JSONException e) {
                this.f7047 = null;
                this.f7032 = null;
            }
        } else {
            this.f7047 = null;
        }
        this.f7048 = i6;
        if (list != null && !list.isEmpty()) {
            m7901((MediaQueueItem[]) list.toArray(new MediaQueueItem[list.size()]));
        }
        this.f7037 = z2;
        this.f7038 = adBreakStatus;
        this.f7033 = videoInfo;
    }

    public MediaStatus(JSONObject jSONObject) throws JSONException {
        this((MediaInfo) null, 0, 0, 0.0d, 0, 0, 0, 0, 0.0d, false, (long[]) null, 0, 0, (String) null, 0, (List<MediaQueueItem>) null, false, (AdBreakStatus) null, (VideoInfo) null);
        m7919(jSONObject, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m7901(MediaQueueItem[] mediaQueueItemArr) {
        this.f7036.clear();
        this.f7035.clear();
        for (int i = 0; i < mediaQueueItemArr.length; i++) {
            MediaQueueItem mediaQueueItem = mediaQueueItemArr[i];
            this.f7036.add(mediaQueueItem);
            this.f7035.put(mediaQueueItem.m7895(), Integer.valueOf(i));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaStatus)) {
            return false;
        }
        MediaStatus mediaStatus = (MediaStatus) obj;
        if ((this.f7047 == null) != (mediaStatus.f7047 == null) || this.f7043 != mediaStatus.f7043 || this.f7045 != mediaStatus.f7045 || this.f7044 != mediaStatus.f7044 || this.f7042 != mediaStatus.f7042 || this.f7028 != mediaStatus.f7028 || this.f7029 != mediaStatus.f7029 || this.f7039 != mediaStatus.f7039 || this.f7040 != mediaStatus.f7040 || this.f7034 != mediaStatus.f7034 || this.f7031 != mediaStatus.f7031 || this.f7048 != mediaStatus.f7048 || !Arrays.equals(this.f7041, mediaStatus.f7041) || !zzbcm.m10043(Long.valueOf(this.f7030), Long.valueOf(mediaStatus.f7030)) || !zzbcm.m10043(this.f7036, mediaStatus.f7036) || !zzbcm.m10043(this.f7046, mediaStatus.f7046)) {
            return false;
        }
        return (this.f7047 == null || mediaStatus.f7047 == null || zzo.m9263(this.f7047, mediaStatus.f7047)) && this.f7037 == mediaStatus.m7923();
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f7046, Long.valueOf(this.f7043), Integer.valueOf(this.f7045), Double.valueOf(this.f7044), Integer.valueOf(this.f7042), Integer.valueOf(this.f7028), Long.valueOf(this.f7029), Long.valueOf(this.f7030), Double.valueOf(this.f7039), Boolean.valueOf(this.f7040), Integer.valueOf(Arrays.hashCode(this.f7041)), Integer.valueOf(this.f7034), Integer.valueOf(this.f7031), String.valueOf(this.f7047), Integer.valueOf(this.f7048), this.f7036, Boolean.valueOf(this.f7037)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        this.f7032 = this.f7047 == null ? null : this.f7047.toString();
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10189(parcel, 2, (Parcelable) m7913(), i, false);
        zzbfp.m10186(parcel, 3, this.f7043);
        zzbfp.m10185(parcel, 4, m7911());
        zzbfp.m10183(parcel, 5, m7916());
        zzbfp.m10185(parcel, 6, m7914());
        zzbfp.m10185(parcel, 7, m7917());
        zzbfp.m10186(parcel, 8, m7902());
        zzbfp.m10186(parcel, 9, this.f7030);
        zzbfp.m10183(parcel, 10, m7903());
        zzbfp.m10195(parcel, 11, m7904());
        zzbfp.m10198(parcel, 12, m7910(), false);
        zzbfp.m10185(parcel, 13, m7912());
        zzbfp.m10185(parcel, 14, m7907());
        zzbfp.m10193(parcel, 15, this.f7032, false);
        zzbfp.m10185(parcel, 16, this.f7048);
        zzbfp.m10180(parcel, 17, this.f7036, false);
        zzbfp.m10195(parcel, 18, m7923());
        zzbfp.m10189(parcel, 19, (Parcelable) m7924(), i, false);
        zzbfp.m10189(parcel, 20, (Parcelable) m7908(), i, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public long m7902() {
        return this.f7029;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public double m7903() {
        return this.f7039;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m7904() {
        return this.f7040;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m7905() {
        return this.f7048;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m7906() {
        return this.f7036.size();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int m7907() {
        return this.f7031;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public VideoInfo m7908() {
        return this.f7033;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public AdBreakClipInfo m7909() {
        if (this.f7038 == null || this.f7046 == null) {
            return null;
        }
        String r2 = this.f7038.m7781();
        if (TextUtils.isEmpty(r2)) {
            return null;
        }
        List<AdBreakClipInfo> r0 = this.f7046.m7841();
        if (r0 == null || r0.isEmpty()) {
            return null;
        }
        for (AdBreakClipInfo next : r0) {
            if (r2.equals(next.m7772())) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public long[] m7910() {
        return this.f7041;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public int m7911() {
        return this.f7045;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int m7912() {
        return this.f7034;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public MediaInfo m7913() {
        return this.f7046;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m7914() {
        return this.f7042;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaQueueItem m7915(int i) {
        Integer num = this.f7035.get(i);
        if (num == null) {
            return null;
        }
        return this.f7036.get(num.intValue());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public double m7916() {
        return this.f7044;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m7917() {
        return this.f7028;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Integer m7918(int i) {
        return this.f7035.get(i);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x032f  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int m7919(org.json.JSONObject r11, int r12) throws org.json.JSONException {
        /*
            r10 = this;
            r0 = 0
            java.lang.String r1 = "mediaSessionId"
            long r2 = r11.getLong(r1)
            long r4 = r10.f7043
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x0011
            r10.f7043 = r2
            r0 = 1
        L_0x0011:
            java.lang.String r1 = "playerState"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x005a
            r1 = 0
            java.lang.String r2 = "playerState"
            java.lang.String r2 = r11.getString(r2)
            java.lang.String r3 = "IDLE"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x010b
            r1 = 1
        L_0x002c:
            int r2 = r10.f7042
            if (r1 == r2) goto L_0x03c4
            r10.f7042 = r1
            r2 = r0 | 2
        L_0x0034:
            r0 = 1
            if (r1 != r0) goto L_0x03c1
            java.lang.String r0 = "idleReason"
            boolean r0 = r11.has(r0)
            if (r0 == 0) goto L_0x03c1
            r0 = 0
            java.lang.String r1 = "idleReason"
            java.lang.String r1 = r11.getString(r1)
            java.lang.String r3 = "CANCELLED"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x012f
            r0 = 2
        L_0x0052:
            int r1 = r10.f7028
            if (r0 == r1) goto L_0x03c1
            r10.f7028 = r0
            r0 = r2 | 2
        L_0x005a:
            java.lang.String r1 = "playbackRate"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x0074
            java.lang.String r1 = "playbackRate"
            double r2 = r11.getDouble(r1)
            double r4 = r10.f7044
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x0074
            r10.f7044 = r2
            r0 = r0 | 2
        L_0x0074:
            java.lang.String r1 = "currentTime"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x0099
            r1 = r12 & 2
            if (r1 != 0) goto L_0x0099
            java.lang.String r1 = "currentTime"
            double r2 = r11.getDouble(r1)
            r4 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r2 = r2 * r4
            long r2 = (long) r2
            long r4 = r10.f7029
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x0099
            r10.f7029 = r2
            r0 = r0 | 2
        L_0x0099:
            java.lang.String r1 = "supportedMediaCommands"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x00b3
            java.lang.String r1 = "supportedMediaCommands"
            long r2 = r11.getLong(r1)
            long r4 = r10.f7030
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x00b3
            r10.f7030 = r2
            r0 = r0 | 2
        L_0x00b3:
            java.lang.String r1 = "volume"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x00e7
            r1 = r12 & 1
            if (r1 != 0) goto L_0x00e7
            java.lang.String r1 = "volume"
            org.json.JSONObject r1 = r11.getJSONObject(r1)
            java.lang.String r2 = "level"
            double r2 = r1.getDouble(r2)
            double r4 = r10.f7039
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x00d8
            r10.f7039 = r2
            r0 = r0 | 2
        L_0x00d8:
            java.lang.String r2 = "muted"
            boolean r1 = r1.getBoolean(r2)
            boolean r2 = r10.f7040
            if (r1 == r2) goto L_0x00e7
            r10.f7040 = r1
            r0 = r0 | 2
        L_0x00e7:
            r2 = 0
            r1 = 0
            java.lang.String r3 = "activeTrackIds"
            boolean r3 = r11.has(r3)
            if (r3 == 0) goto L_0x0268
            java.lang.String r1 = "activeTrackIds"
            org.json.JSONArray r4 = r11.getJSONArray(r1)
            int r5 = r4.length()
            long[] r1 = new long[r5]
            r3 = 0
        L_0x0100:
            if (r3 >= r5) goto L_0x0153
            long r6 = r4.getLong(r3)
            r1[r3] = r6
            int r3 = r3 + 1
            goto L_0x0100
        L_0x010b:
            java.lang.String r3 = "PLAYING"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0117
            r1 = 2
            goto L_0x002c
        L_0x0117:
            java.lang.String r3 = "PAUSED"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0123
            r1 = 3
            goto L_0x002c
        L_0x0123:
            java.lang.String r3 = "BUFFERING"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x002c
            r1 = 4
            goto L_0x002c
        L_0x012f:
            java.lang.String r3 = "INTERRUPTED"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x013b
            r0 = 3
            goto L_0x0052
        L_0x013b:
            java.lang.String r3 = "FINISHED"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0147
            r0 = 1
            goto L_0x0052
        L_0x0147:
            java.lang.String r3 = "ERROR"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x0052
            r0 = 4
            goto L_0x0052
        L_0x0153:
            long[] r3 = r10.f7041
            if (r3 != 0) goto L_0x024d
            r2 = 1
        L_0x0158:
            if (r2 == 0) goto L_0x015c
            r10.f7041 = r1
        L_0x015c:
            if (r2 == 0) goto L_0x0162
            r10.f7041 = r1
            r0 = r0 | 2
        L_0x0162:
            java.lang.String r1 = "customData"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x0179
            java.lang.String r1 = "customData"
            org.json.JSONObject r1 = r11.getJSONObject(r1)
            r10.f7047 = r1
            r1 = 0
            r10.f7032 = r1
            r0 = r0 | 2
        L_0x0179:
            java.lang.String r1 = "media"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x01ad
            java.lang.String r1 = "media"
            org.json.JSONObject r1 = r11.getJSONObject(r1)
            com.google.android.gms.cast.MediaInfo r2 = new com.google.android.gms.cast.MediaInfo
            r2.<init>((org.json.JSONObject) r1)
            com.google.android.gms.cast.MediaInfo r3 = r10.f7046
            if (r3 == 0) goto L_0x019e
            com.google.android.gms.cast.MediaInfo r3 = r10.f7046
            if (r3 == 0) goto L_0x01a2
            com.google.android.gms.cast.MediaInfo r3 = r10.f7046
            boolean r3 = r3.equals(r2)
            if (r3 != 0) goto L_0x01a2
        L_0x019e:
            r10.f7046 = r2
            r0 = r0 | 2
        L_0x01a2:
            java.lang.String r2 = "metadata"
            boolean r1 = r1.has(r2)
            if (r1 == 0) goto L_0x01ad
            r0 = r0 | 4
        L_0x01ad:
            java.lang.String r1 = "currentItemId"
            boolean r1 = r11.has(r1)
            if (r1 == 0) goto L_0x01c5
            java.lang.String r1 = "currentItemId"
            int r1 = r11.getInt(r1)
            int r2 = r10.f7045
            if (r2 == r1) goto L_0x01c5
            r10.f7045 = r1
            r0 = r0 | 2
        L_0x01c5:
            java.lang.String r1 = "preloadedItemId"
            r2 = 0
            int r1 = r11.optInt(r1, r2)
            int r2 = r10.f7031
            if (r2 == r1) goto L_0x01d5
            r10.f7031 = r1
            r0 = r0 | 16
        L_0x01d5:
            java.lang.String r1 = "loadingItemId"
            r2 = 0
            int r1 = r11.optInt(r1, r2)
            int r2 = r10.f7034
            if (r2 == r1) goto L_0x03be
            r10.f7034 = r1
            r0 = r0 | 2
            r1 = r0
        L_0x01e6:
            com.google.android.gms.cast.MediaInfo r0 = r10.f7046
            if (r0 != 0) goto L_0x026f
            r0 = -1
        L_0x01eb:
            int r2 = r10.f7042
            int r3 = r10.f7028
            int r4 = r10.f7034
            r5 = 1
            if (r2 == r5) goto L_0x0277
            r0 = 0
        L_0x01f5:
            if (r0 != 0) goto L_0x0394
            r2 = 0
            java.lang.String r0 = "repeatMode"
            boolean r0 = r11.has(r0)
            if (r0 == 0) goto L_0x03bb
            int r0 = r10.f7048
            java.lang.String r3 = "repeatMode"
            java.lang.String r4 = r11.getString(r3)
            r3 = -1
            int r5 = r4.hashCode()
            switch(r5) {
                case -1118317585: goto L_0x02ab;
                case -962896020: goto L_0x029f;
                case 1645938909: goto L_0x0293;
                case 1645952171: goto L_0x0288;
                default: goto L_0x0212;
            }
        L_0x0212:
            switch(r3) {
                case 0: goto L_0x02b7;
                case 1: goto L_0x02ba;
                case 2: goto L_0x02bd;
                case 3: goto L_0x02c0;
                default: goto L_0x0215;
            }
        L_0x0215:
            int r3 = r10.f7048
            if (r3 == r0) goto L_0x03bb
            r10.f7048 = r0
            r0 = 1
        L_0x021c:
            java.lang.String r2 = "items"
            boolean r2 = r11.has(r2)
            if (r2 == 0) goto L_0x032d
            java.lang.String r2 = "items"
            org.json.JSONArray r4 = r11.getJSONArray(r2)
            int r5 = r4.length()
            android.util.SparseArray r6 = new android.util.SparseArray
            r6.<init>()
            r2 = 0
        L_0x0236:
            if (r2 >= r5) goto L_0x02c3
            org.json.JSONObject r3 = r4.getJSONObject(r2)
            java.lang.String r7 = "itemId"
            int r3 = r3.getInt(r7)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r6.put(r2, r3)
            int r2 = r2 + 1
            goto L_0x0236
        L_0x024d:
            long[] r3 = r10.f7041
            int r3 = r3.length
            if (r3 == r5) goto L_0x0255
            r2 = 1
            goto L_0x0158
        L_0x0255:
            r3 = 0
        L_0x0256:
            if (r3 >= r5) goto L_0x0158
            long[] r4 = r10.f7041
            r6 = r4[r3]
            r8 = r1[r3]
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 == 0) goto L_0x0265
            r2 = 1
            goto L_0x0158
        L_0x0265:
            int r3 = r3 + 1
            goto L_0x0256
        L_0x0268:
            long[] r3 = r10.f7041
            if (r3 == 0) goto L_0x015c
            r2 = 1
            goto L_0x015c
        L_0x026f:
            com.google.android.gms.cast.MediaInfo r0 = r10.f7046
            int r0 = r0.m7844()
            goto L_0x01eb
        L_0x0277:
            switch(r3) {
                case 1: goto L_0x027d;
                case 2: goto L_0x0282;
                case 3: goto L_0x027d;
                default: goto L_0x027a;
            }
        L_0x027a:
            r0 = 1
            goto L_0x01f5
        L_0x027d:
            if (r4 == 0) goto L_0x027a
            r0 = 0
            goto L_0x01f5
        L_0x0282:
            r2 = 2
            if (r0 != r2) goto L_0x027a
            r0 = 0
            goto L_0x01f5
        L_0x0288:
            java.lang.String r5 = "REPEAT_OFF"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0212
            r3 = 0
            goto L_0x0212
        L_0x0293:
            java.lang.String r5 = "REPEAT_ALL"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0212
            r3 = 1
            goto L_0x0212
        L_0x029f:
            java.lang.String r5 = "REPEAT_SINGLE"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0212
            r3 = 2
            goto L_0x0212
        L_0x02ab:
            java.lang.String r5 = "REPEAT_ALL_AND_SHUFFLE"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0212
            r3 = 3
            goto L_0x0212
        L_0x02b7:
            r0 = 0
            goto L_0x0215
        L_0x02ba:
            r0 = 1
            goto L_0x0215
        L_0x02bd:
            r0 = 2
            goto L_0x0215
        L_0x02c0:
            r0 = 3
            goto L_0x0215
        L_0x02c3:
            com.google.android.gms.cast.MediaQueueItem[] r7 = new com.google.android.gms.cast.MediaQueueItem[r5]
            r3 = 0
            r2 = r0
        L_0x02c7:
            if (r3 >= r5) goto L_0x0321
            java.lang.Object r0 = r6.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            org.json.JSONObject r8 = r4.getJSONObject(r3)
            int r9 = r0.intValue()
            com.google.android.gms.cast.MediaQueueItem r9 = r10.m7915(r9)
            if (r9 == 0) goto L_0x02f7
            boolean r8 = r9.m7899(r8)
            r2 = r2 | r8
            r7[r3] = r9
            int r0 = r0.intValue()
            java.lang.Integer r0 = r10.m7918(r0)
            int r0 = r0.intValue()
            if (r3 == r0) goto L_0x031f
            r0 = 1
        L_0x02f3:
            int r3 = r3 + 1
            r2 = r0
            goto L_0x02c7
        L_0x02f7:
            r2 = 1
            int r0 = r0.intValue()
            int r9 = r10.f7045
            if (r0 != r9) goto L_0x0318
            com.google.android.gms.cast.MediaInfo r0 = r10.f7046
            if (r0 == 0) goto L_0x0318
            com.google.android.gms.cast.MediaQueueItem$Builder r0 = new com.google.android.gms.cast.MediaQueueItem$Builder
            com.google.android.gms.cast.MediaInfo r9 = r10.f7046
            r0.<init>(r9)
            com.google.android.gms.cast.MediaQueueItem r0 = r0.m7900()
            r7[r3] = r0
            r0 = r7[r3]
            r0.m7899(r8)
            r0 = r2
            goto L_0x02f3
        L_0x0318:
            com.google.android.gms.cast.MediaQueueItem r0 = new com.google.android.gms.cast.MediaQueueItem
            r0.<init>((org.json.JSONObject) r8)
            r7[r3] = r0
        L_0x031f:
            r0 = r2
            goto L_0x02f3
        L_0x0321:
            java.util.ArrayList<com.google.android.gms.cast.MediaQueueItem> r0 = r10.f7036
            int r0 = r0.size()
            if (r0 == r5) goto L_0x03b8
            r0 = 1
        L_0x032a:
            r10.m7901((com.google.android.gms.cast.MediaQueueItem[]) r7)
        L_0x032d:
            if (r0 == 0) goto L_0x0331
            r1 = r1 | 8
        L_0x0331:
            java.lang.String r0 = "breakStatus"
            org.json.JSONObject r0 = r11.optJSONObject(r0)
            com.google.android.gms.cast.AdBreakStatus r2 = com.google.android.gms.cast.AdBreakStatus.m7779(r0)
            com.google.android.gms.cast.AdBreakStatus r0 = r10.f7038
            if (r0 != 0) goto L_0x0342
            if (r2 != 0) goto L_0x034e
        L_0x0342:
            com.google.android.gms.cast.AdBreakStatus r0 = r10.f7038
            if (r0 == 0) goto L_0x0357
            com.google.android.gms.cast.AdBreakStatus r0 = r10.f7038
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0357
        L_0x034e:
            if (r2 == 0) goto L_0x03b6
            r0 = 1
        L_0x0351:
            r10.f7037 = r0
            r10.f7038 = r2
            r1 = r1 | 32
        L_0x0357:
            java.lang.String r0 = "videoInfo"
            org.json.JSONObject r0 = r11.optJSONObject(r0)
            com.google.android.gms.cast.VideoInfo r0 = com.google.android.gms.cast.VideoInfo.m7970(r0)
            com.google.android.gms.cast.VideoInfo r2 = r10.f7033
            if (r2 != 0) goto L_0x0368
            if (r0 != 0) goto L_0x0374
        L_0x0368:
            com.google.android.gms.cast.VideoInfo r2 = r10.f7033
            if (r2 == 0) goto L_0x0378
            com.google.android.gms.cast.VideoInfo r2 = r10.f7033
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0378
        L_0x0374:
            r10.f7033 = r0
            r1 = r1 | 64
        L_0x0378:
            java.lang.String r0 = "breakInfo"
            boolean r0 = r11.has(r0)
            if (r0 == 0) goto L_0x0393
            com.google.android.gms.cast.MediaInfo r0 = r10.f7046
            if (r0 == 0) goto L_0x0393
            com.google.android.gms.cast.MediaInfo r0 = r10.f7046
            java.lang.String r2 = "breakInfo"
            org.json.JSONObject r2 = r11.getJSONObject(r2)
            r0.m7854((org.json.JSONObject) r2)
            r1 = r1 | 2
        L_0x0393:
            return r1
        L_0x0394:
            r0 = 0
            r10.f7045 = r0
            r0 = 0
            r10.f7034 = r0
            r0 = 0
            r10.f7031 = r0
            java.util.ArrayList<com.google.android.gms.cast.MediaQueueItem> r0 = r10.f7036
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0331
            r0 = 0
            r10.f7048 = r0
            java.util.ArrayList<com.google.android.gms.cast.MediaQueueItem> r0 = r10.f7036
            r0.clear()
            android.util.SparseArray<java.lang.Integer> r0 = r10.f7035
            r0.clear()
            r1 = r1 | 8
            goto L_0x0331
        L_0x03b6:
            r0 = 0
            goto L_0x0351
        L_0x03b8:
            r0 = r2
            goto L_0x032a
        L_0x03bb:
            r0 = r2
            goto L_0x021c
        L_0x03be:
            r1 = r0
            goto L_0x01e6
        L_0x03c1:
            r0 = r2
            goto L_0x005a
        L_0x03c4:
            r2 = r0
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.MediaStatus.m7919(org.json.JSONObject, int):int");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m7920() {
        return this.f7043;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaQueueItem m7921(int i) {
        return m7915(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7922(boolean z) {
        this.f7037 = z;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public boolean m7923() {
        return this.f7037;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public AdBreakStatus m7924() {
        return this.f7038;
    }
}
