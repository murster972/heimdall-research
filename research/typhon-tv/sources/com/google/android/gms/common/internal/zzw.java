package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class zzw extends zzv {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Activity f7911;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f7912;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Intent f7913;

    zzw(Intent intent, Activity activity, int i) {
        this.f7913 = intent;
        this.f7911 = activity;
        this.f7912 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9232() {
        if (this.f7913 != null) {
            this.f7911.startActivityForResult(this.f7913, this.f7912);
        }
    }
}
