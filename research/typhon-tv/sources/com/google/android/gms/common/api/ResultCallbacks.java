package com.google.android.gms.common.api;

import com.google.android.gms.common.api.Result;

public abstract class ResultCallbacks<R extends Result> implements ResultCallback<R> {
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8540(R r);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8541(Status status);
}
