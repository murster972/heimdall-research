package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;

final class zzdh implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzdg f7697;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Result f7698;

    zzdh(zzdg zzdg, Result result) {
        this.f7697 = zzdg;
        this.f7698 = result;
    }

    public final void run() {
        try {
            BasePendingResult.f3654.set(true);
            this.f7697.f7690.sendMessage(this.f7697.f7690.obtainMessage(0, this.f7697.f7696.m8542(this.f7698)));
            BasePendingResult.f3654.set(false);
            zzdg.m8898(this.f7698);
            GoogleApiClient googleApiClient = (GoogleApiClient) this.f7697.f7689.get();
            if (googleApiClient != null) {
                googleApiClient.zzb(this.f7697);
            }
        } catch (RuntimeException e) {
            this.f7697.f7690.sendMessage(this.f7697.f7690.obtainMessage(1, e));
            BasePendingResult.f3654.set(false);
            zzdg.m8898(this.f7698);
            GoogleApiClient googleApiClient2 = (GoogleApiClient) this.f7697.f7689.get();
            if (googleApiClient2 != null) {
                googleApiClient2.zzb(this.f7697);
            }
        } catch (Throwable th) {
            Throwable th2 = th;
            BasePendingResult.f3654.set(false);
            zzdg.m8898(this.f7698);
            GoogleApiClient googleApiClient3 = (GoogleApiClient) this.f7697.f7689.get();
            if (googleApiClient3 != null) {
                googleApiClient3.zzb(this.f7697);
            }
            throw th2;
        }
    }
}
