package com.google.android.gms.common.api;

import com.google.android.gms.common.api.Result;

public abstract class PendingResult<R extends Result> {

    public interface zza {
        /* renamed from: 龘  reason: contains not printable characters */
        void m8536(Status status);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m8531();

    /* renamed from: 齉  reason: contains not printable characters */
    public Integer m8532() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8533();

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8534(zza zza2) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8535(ResultCallback<? super R> resultCallback);
}
