package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.concurrent.atomic.AtomicReference;

public abstract class zzo extends LifecycleCallback implements DialogInterface.OnCancelListener {

    /* renamed from: 连任  reason: contains not printable characters */
    private final Handler f7734;

    /* renamed from: 靐  reason: contains not printable characters */
    protected volatile boolean f7735;

    /* renamed from: 麤  reason: contains not printable characters */
    protected final GoogleApiAvailability f7736;

    /* renamed from: 齉  reason: contains not printable characters */
    protected final AtomicReference<zzp> f7737;

    protected zzo(zzcf zzcf) {
        this(zzcf, GoogleApiAvailability.m8486());
    }

    private zzo(zzcf zzcf, GoogleApiAvailability googleApiAvailability) {
        super(zzcf);
        this.f7737 = new AtomicReference<>((Object) null);
        this.f7734 = new Handler(Looper.getMainLooper());
        this.f7736 = googleApiAvailability;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m8948(zzp zzp) {
        if (zzp == null) {
            return -1;
        }
        return zzp.m8959();
    }

    public void onCancel(DialogInterface dialogInterface) {
        m8957(new ConnectionResult(13, (PendingIntent) null), m8948(this.f7737.get()));
        m8950();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m8949();

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m8950() {
        this.f7737.set((Object) null);
        m8949();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8951() {
        super.m8556();
        this.f7735 = true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8952(Bundle bundle) {
        super.m8557(bundle);
        zzp zzp = this.f7737.get();
        if (zzp != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", zzp.m8959());
            bundle.putInt("failed_status", zzp.m8958().m8480());
            bundle.putParcelable("failed_resolution", zzp.m8958().m8479());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8953(ConnectionResult connectionResult, int i) {
        zzp zzp = new zzp(connectionResult, i);
        if (this.f7737.compareAndSet((Object) null, zzp)) {
            this.f7734.post(new zzq(this, zzp));
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m8954() {
        super.m8558();
        this.f7735 = false;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m8955(int r7, int r8, android.content.Intent r9) {
        /*
            r6 = this;
            r5 = 18
            r1 = 13
            r2 = 1
            r3 = 0
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.common.api.internal.zzp> r0 = r6.f7737
            java.lang.Object r0 = r0.get()
            com.google.android.gms.common.api.internal.zzp r0 = (com.google.android.gms.common.api.internal.zzp) r0
            switch(r7) {
                case 1: goto L_0x0034;
                case 2: goto L_0x0018;
                default: goto L_0x0011;
            }
        L_0x0011:
            r1 = r3
        L_0x0012:
            if (r1 == 0) goto L_0x005b
            r6.m8950()
        L_0x0017:
            return
        L_0x0018:
            com.google.android.gms.common.GoogleApiAvailability r1 = r6.f7736
            android.app.Activity r4 = r6.m8560()
            int r4 = r1.m4227((android.content.Context) r4)
            if (r4 != 0) goto L_0x0069
            r1 = r2
        L_0x0025:
            if (r0 == 0) goto L_0x0017
            com.google.android.gms.common.ConnectionResult r2 = r0.m8958()
            int r2 = r2.m8480()
            if (r2 != r5) goto L_0x0012
            if (r4 != r5) goto L_0x0012
            goto L_0x0017
        L_0x0034:
            r4 = -1
            if (r8 != r4) goto L_0x0039
            r1 = r2
            goto L_0x0012
        L_0x0039:
            if (r8 != 0) goto L_0x0011
            if (r9 == 0) goto L_0x0044
            java.lang.String r2 = "<<ResolutionFailureErrorDetail>>"
            int r1 = r9.getIntExtra(r2, r1)
        L_0x0044:
            com.google.android.gms.common.api.internal.zzp r2 = new com.google.android.gms.common.api.internal.zzp
            com.google.android.gms.common.ConnectionResult r4 = new com.google.android.gms.common.ConnectionResult
            r5 = 0
            r4.<init>(r1, r5)
            int r0 = m8948((com.google.android.gms.common.api.internal.zzp) r0)
            r2.<init>(r4, r0)
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.common.api.internal.zzp> r0 = r6.f7737
            r0.set(r2)
            r0 = r2
            r1 = r3
            goto L_0x0012
        L_0x005b:
            if (r0 == 0) goto L_0x0017
            com.google.android.gms.common.ConnectionResult r1 = r0.m8958()
            int r0 = r0.m8959()
            r6.m8957(r1, r0)
            goto L_0x0017
        L_0x0069:
            r1 = r3
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzo.m8955(int, int, android.content.Intent):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8956(Bundle bundle) {
        super.m8562(bundle);
        if (bundle != null) {
            this.f7737.set(bundle.getBoolean("resolving_error", false) ? new zzp(new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8957(ConnectionResult connectionResult, int i);
}
