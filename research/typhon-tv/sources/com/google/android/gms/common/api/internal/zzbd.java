package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

final class zzbd implements GoogleApiClient.OnConnectionFailedListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzda f7574;

    zzbd(zzba zzba, zzda zzda) {
        this.f7574 = zzda;
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.f7574.m4198(new Status(8));
    }
}
