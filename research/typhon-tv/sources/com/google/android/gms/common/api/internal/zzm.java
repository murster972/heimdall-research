package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbz;

public abstract class zzm<R extends Result, A extends Api.zzb> extends BasePendingResult<R> implements zzn<R> {

    /* renamed from: 齉  reason: contains not printable characters */
    private final Api<?> f7732;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Api.zzc<A> f7733;

    protected zzm(Api<?> api, GoogleApiClient googleApiClient) {
        super((GoogleApiClient) zzbq.m9121(googleApiClient, (Object) "GoogleApiClient must not be null"));
        zzbq.m9121(api, (Object) "Api must not be null");
        this.f7733 = api.m8506();
        this.f7732 = api;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8940(RemoteException remoteException) {
        m8944(new Status(8, remoteException.getLocalizedMessage(), (PendingIntent) null));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Api.zzc<A> m8941() {
        return this.f7733;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Api<?> m8942() {
        return this.f7732;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8943(A a) throws DeadObjectException {
        if (a instanceof zzbz) {
            a = zzbz.m9137();
        }
        try {
            m8945(a);
        } catch (DeadObjectException e) {
            m8940((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            m8940(e2);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8944(Status status) {
        zzbq.m9117(!status.m8549(), "Failed result must not be success");
        m4198(m4195(status));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8945(A a) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ void m8946(Object obj) {
        super.m4198((Result) obj);
    }
}
