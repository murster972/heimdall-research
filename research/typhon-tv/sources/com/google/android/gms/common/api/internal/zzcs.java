package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzbhb;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class zzcs {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ExecutorService f7668 = new ThreadPoolExecutor(0, 4, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new zzbhb("GAC_Transform"));

    /* renamed from: 龘  reason: contains not printable characters */
    public static ExecutorService m8864() {
        return f7668;
    }
}
