package com.google.android.gms.common.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;

public final class Status extends zzbfm implements Result, ReflectedParcelable {
    public static final Parcelable.Creator<Status> CREATOR = new zzg();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Status f7458 = new Status(17);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Status f7459 = new Status(18);

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Status f7460 = new Status(16);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Status f7461 = new Status(14);

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Status f7462 = new Status(15);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Status f7463 = new Status(8);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Status f7464 = new Status(0);

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7465;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f7466;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final String f7467;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final PendingIntent f7468;

    public Status(int i) {
        this(i, (String) null);
    }

    Status(int i, int i2, String str, PendingIntent pendingIntent) {
        this.f7465 = i;
        this.f7466 = i2;
        this.f7467 = str;
        this.f7468 = pendingIntent;
    }

    public Status(int i, String str) {
        this(1, i, str, (PendingIntent) null);
    }

    public Status(int i, String str, PendingIntent pendingIntent) {
        this(1, i, str, pendingIntent);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.f7465 == status.f7465 && this.f7466 == status.f7466 && zzbg.m9113(this.f7467, status.f7467) && zzbg.m9113(this.f7468, status.f7468);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f7465), Integer.valueOf(this.f7466), this.f7467, this.f7468});
    }

    public final Status s_() {
        return this;
    }

    public final String toString() {
        return zzbg.m9112(this).m9114("statusCode", m8546()).m9114("resolution", this.f7468).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, m8547());
        zzbfp.m10193(parcel, 2, m8548(), false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f7468, i, false);
        zzbfp.m10185(parcel, 1000, this.f7465);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final PendingIntent m8545() {
        return this.f7468;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m8546() {
        return this.f7467 != null ? this.f7467 : CommonStatusCodes.m8526(this.f7466);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m8547() {
        return this.f7466;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m8548() {
        return this.f7467;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m8549() {
        return this.f7466 <= 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m8550() {
        return this.f7468 != null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8551(Activity activity, int i) throws IntentSender.SendIntentException {
        if (m8550()) {
            activity.startIntentSenderForResult(this.f7468.getIntentSender(), i, (Intent) null, 0, 0, 0);
        }
    }
}
