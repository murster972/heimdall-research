package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzan;
import java.util.Set;

public interface zzcy {
    /* renamed from: 靐  reason: contains not printable characters */
    void m8879(ConnectionResult connectionResult);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8880(zzan zzan, Set<Scope> set);
}
