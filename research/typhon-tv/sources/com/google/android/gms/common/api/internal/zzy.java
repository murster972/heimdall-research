package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class zzy implements zzcd {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzv f7763;

    private zzy(zzv zzv) {
        this.f7763 = zzv;
    }

    /* synthetic */ zzy(zzv zzv, zzw zzw) {
        this(zzv);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9001(int i, boolean z) {
        this.f7763.f7750.lock();
        try {
            if (this.f7763.f7752) {
                boolean unused = this.f7763.f7752 = false;
                this.f7763.m8979(i, z);
                return;
            }
            boolean unused2 = this.f7763.f7752 = true;
            this.f7763.f7758.onConnectionSuspended(i);
            this.f7763.f7750.unlock();
        } finally {
            this.f7763.f7750.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9002(Bundle bundle) {
        this.f7763.f7750.lock();
        try {
            ConnectionResult unused = this.f7763.f7755 = ConnectionResult.f7411;
            this.f7763.m8965();
        } finally {
            this.f7763.f7750.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9003(ConnectionResult connectionResult) {
        this.f7763.f7750.lock();
        try {
            ConnectionResult unused = this.f7763.f7755 = connectionResult;
            this.f7763.m8965();
        } finally {
            this.f7763.f7750.unlock();
        }
    }
}
