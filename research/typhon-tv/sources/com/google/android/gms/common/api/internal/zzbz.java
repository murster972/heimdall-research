package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbf;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzca;

@Deprecated
public final class zzbz {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzbz f3670;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f3671 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f3672;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f3673;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Status f3674;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f3675;

    private zzbz(Context context) {
        boolean z = true;
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(R.string.common_google_play_services_unknown_issue));
        if (identifier != 0) {
            boolean z2 = resources.getInteger(identifier) != 0;
            this.f3672 = z2 ? false : z;
            z = z2;
        } else {
            this.f3672 = false;
        }
        this.f3673 = z;
        String r0 = zzbf.m9111(context);
        r0 = r0 == null ? new zzca(context).m9141("google_app_id") : r0;
        if (TextUtils.isEmpty(r0)) {
            this.f3674 = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.f3675 = null;
            return;
        }
        this.f3675 = r0;
        this.f3674 = Status.f7464;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4201() {
        return m4203("isMeasurementExplicitlyDisabled").f3672;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Status m4202(Context context) {
        Status status;
        zzbq.m9121(context, (Object) "Context must not be null.");
        synchronized (f3671) {
            if (f3670 == null) {
                f3670 = new zzbz(context);
            }
            status = f3670.f3674;
        }
        return status;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzbz m4203(String str) {
        zzbz zzbz;
        synchronized (f3671) {
            if (f3670 == null) {
                throw new IllegalStateException(new StringBuilder(String.valueOf(str).length() + 34).append("Initialize must be called before ").append(str).append(".").toString());
            }
            zzbz = f3670;
        }
        return zzbz;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m4204() {
        return m4203("getGoogleAppId").f3675;
    }
}
