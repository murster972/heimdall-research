package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

final class zzv implements zzcc {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<Api.zzc<?>, zzbi> f7747;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Set<zzcu> f7748 = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Api.zze f7749;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public final Lock f7750;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f7751 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f7752 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Bundle f7753;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public ConnectionResult f7754 = null;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public ConnectionResult f7755 = null;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzbi f7756;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzba f7757;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzbi f7758;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Looper f7759;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f7760;

    private zzv(Context context, zzba zzba, Lock lock, Looper looper, zzf zzf, Map<Api.zzc<?>, Api.zze> map, Map<Api.zzc<?>, Api.zze> map2, zzr zzr, Api.zza<? extends zzcxd, zzcxe> zza, Api.zze zze, ArrayList<zzt> arrayList, ArrayList<zzt> arrayList2, Map<Api<?>, Boolean> map3, Map<Api<?>, Boolean> map4) {
        this.f7760 = context;
        this.f7757 = zzba;
        this.f7750 = lock;
        this.f7759 = looper;
        this.f7749 = zze;
        this.f7758 = new zzbi(context, this.f7757, lock, looper, zzf, map2, (zzr) null, map4, (Api.zza<? extends zzcxd, zzcxe>) null, arrayList2, new zzx(this, (zzw) null));
        this.f7756 = new zzbi(context, this.f7757, lock, looper, zzf, map, zzr, map3, zza, arrayList, new zzy(this, (zzw) null));
        ArrayMap arrayMap = new ArrayMap();
        for (Api.zzc<?> put : map2.keySet()) {
            arrayMap.put(put, this.f7758);
        }
        for (Api.zzc<?> put2 : map.keySet()) {
            arrayMap.put(put2, this.f7756);
        }
        this.f7747 = Collections.unmodifiableMap(arrayMap);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m8965() {
        if (m8972(this.f7754)) {
            if (m8972(this.f7755) || m8967()) {
                switch (this.f7751) {
                    case 1:
                        break;
                    case 2:
                        this.f7757.m8713(this.f7753);
                        break;
                    default:
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        break;
                }
                m8966();
                this.f7751 = 0;
            } else if (this.f7755 == null) {
            } else {
                if (this.f7751 == 1) {
                    m8966();
                    return;
                }
                m8981(this.f7755);
                this.f7758.m8736();
            }
        } else if (this.f7754 != null && m8972(this.f7755)) {
            this.f7756.m8736();
            m8981(this.f7754);
        } else if (this.f7754 != null && this.f7755 != null) {
            ConnectionResult connectionResult = this.f7754;
            if (this.f7756.f7593 < this.f7758.f7593) {
                connectionResult = this.f7755;
            }
            m8981(connectionResult);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final void m8966() {
        for (zzcu r0 : this.f7748) {
            r0.m8871();
        }
        this.f7748.clear();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean m8967() {
        return this.f7755 != null && this.f7755.m8480() == 4;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final PendingIntent m8968() {
        if (this.f7749 == null) {
            return null;
        }
        return PendingIntent.getActivity(this.f7760, System.identityHashCode(this.f7757), this.f7749.m8520(), 134217728);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m8972(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.m8478();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m8974(zzm<? extends Result, ? extends Api.zzb> zzm) {
        Api.zzc<? extends Api.zzb> r0 = zzm.m8941();
        zzbq.m9117(this.f7747.containsKey(r0), "GoogleApiClient is not configured to use the API required for this call.");
        return this.f7747.get(r0).equals(this.f7756);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzv m8977(Context context, zzba zzba, Lock lock, Looper looper, zzf zzf, Map<Api.zzc<?>, Api.zze> map, zzr zzr, Map<Api<?>, Boolean> map2, Api.zza<? extends zzcxd, zzcxe> zza, ArrayList<zzt> arrayList) {
        Api.zze zze = null;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        for (Map.Entry next : map.entrySet()) {
            Api.zze zze2 = (Api.zze) next.getValue();
            if (zze2.m8521()) {
                zze = zze2;
            }
            if (zze2.m8517()) {
                arrayMap.put((Api.zzc) next.getKey(), zze2);
            } else {
                arrayMap2.put((Api.zzc) next.getKey(), zze2);
            }
        }
        zzbq.m9126(!arrayMap.isEmpty(), (Object) "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (Api next2 : map2.keySet()) {
            Api.zzc<?> r2 = next2.m8506();
            if (arrayMap.containsKey(r2)) {
                arrayMap3.put(next2, map2.get(next2));
            } else if (arrayMap2.containsKey(r2)) {
                arrayMap4.put(next2, map2.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = arrayList;
        int size = arrayList4.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList4.get(i);
            i++;
            zzt zzt = (zzt) obj;
            if (arrayMap3.containsKey(zzt.f7746)) {
                arrayList2.add(zzt);
            } else if (arrayMap4.containsKey(zzt.f7746)) {
                arrayList3.add(zzt);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new zzv(context, zzba, lock, looper, zzf, arrayMap, arrayMap2, zzr, zza, zze, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8979(int i, boolean z) {
        this.f7757.m8712(i, z);
        this.f7755 = null;
        this.f7754 = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8980(Bundle bundle) {
        if (this.f7753 == null) {
            this.f7753 = bundle;
        } else if (bundle != null) {
            this.f7753.putAll(bundle);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8981(ConnectionResult connectionResult) {
        switch (this.f7751) {
            case 1:
                break;
            case 2:
                this.f7757.m8714(connectionResult);
                break;
            default:
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                break;
        }
        m8966();
        this.f7751 = 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8985() {
        this.f7750.lock();
        try {
            boolean r0 = m8987();
            this.f7756.m8736();
            this.f7755 = new ConnectionResult(4);
            if (r0) {
                new Handler(this.f7759).post(new zzw(this));
            } else {
                m8966();
            }
        } finally {
            this.f7750.unlock();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m8986() {
        this.f7758.m8729();
        this.f7756.m8729();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m8987() {
        this.f7750.lock();
        try {
            return this.f7751 == 2;
        } finally {
            this.f7750.unlock();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectionResult m8988() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8989(T t) {
        if (!m8974((zzm<? extends Result, ? extends Api.zzb>) t)) {
            return this.f7758.m8734(t);
        }
        if (!m8967()) {
            return this.f7756.m8734(t);
        }
        t.m8944(new Status(4, (String) null, m8968()));
        return t;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m8990() {
        boolean z = true;
        this.f7750.lock();
        try {
            if (!this.f7758.m8735() || (!this.f7756.m8735() && !m8967() && this.f7751 != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.f7750.unlock();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8991() {
        this.f7755 = null;
        this.f7754 = null;
        this.f7751 = 0;
        this.f7758.m8736();
        this.f7756.m8736();
        m8966();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m8992(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m8993(Api<?> api) {
        return this.f7747.get(api.m8506()).equals(this.f7756) ? m8967() ? new ConnectionResult(4, m8968()) : this.f7756.m8738(api) : this.f7758.m8738(api);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8994(T t) {
        if (!m8974((zzm<? extends Result, ? extends Api.zzb>) t)) {
            return this.f7758.m8739(t);
        }
        if (!m8967()) {
            return this.f7756.m8739(t);
        }
        t.m8944(new Status(4, (String) null, m8968()));
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8995() {
        this.f7751 = 2;
        this.f7752 = false;
        this.f7755 = null;
        this.f7754 = null;
        this.f7758.m8740();
        this.f7756.m8740();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8996(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("authClient").println(":");
        this.f7756.m8745(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append(str).append("anonClient").println(":");
        this.f7758.m8745(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8997(zzcu zzcu) {
        this.f7750.lock();
        try {
            if ((m8987() || m8990()) && !this.f7756.m8735()) {
                this.f7748.add(zzcu);
                if (this.f7751 == 0) {
                    this.f7751 = 1;
                }
                this.f7755 = null;
                this.f7756.m8740();
                return true;
            }
            this.f7750.unlock();
            return false;
        } finally {
            this.f7750.unlock();
        }
    }
}
