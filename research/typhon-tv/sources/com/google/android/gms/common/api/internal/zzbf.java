package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

final class zzbf extends Handler {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzba f7579;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbf(zzba zzba, Looper looper) {
        super(looper);
        this.f7579 = zzba;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f7579.m8699();
                return;
            case 2:
                this.f7579.m8698();
                return;
            default:
                Log.w("GoogleApiClientImpl", new StringBuilder(31).append("Unknown message id: ").append(message.what).toString());
                return;
        }
    }
}
