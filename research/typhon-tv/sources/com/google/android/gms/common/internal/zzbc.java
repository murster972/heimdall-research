package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.zzn;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzbc extends zzeu implements zzba {
    zzbc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9106(zzn zzn, IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel v_ = v_();
        zzew.m12306(v_, (Parcelable) zzn);
        zzew.m12305(v_, (IInterface) iObjectWrapper);
        Parcel r0 = m12300(5, v_);
        boolean r1 = zzew.m12308(r0);
        r0.recycle();
        return r1;
    }
}
