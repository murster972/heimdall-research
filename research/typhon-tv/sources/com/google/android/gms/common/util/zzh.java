package com.google.android.gms.common.util;

import android.os.SystemClock;

public final class zzh implements zzd {

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzh f7936 = new zzh();

    private zzh() {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static zzd m9250() {
        return f7936;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m9251() {
        return SystemClock.elapsedRealtime();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m9252() {
        return System.nanoTime();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m9253() {
        return System.currentTimeMillis();
    }
}
