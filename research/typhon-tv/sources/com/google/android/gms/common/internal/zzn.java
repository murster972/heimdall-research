package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;

public final class zzn extends zze {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzd f7900;

    /* renamed from: 龘  reason: contains not printable characters */
    private IBinder f7901;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzn(zzd zzd, int i, IBinder iBinder, Bundle bundle) {
        super(zzd, i, bundle);
        this.f7900 = zzd;
        this.f7901 = iBinder;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9209(ConnectionResult connectionResult) {
        if (this.f7900.f7873 != null) {
            this.f7900.f7873.m9198(connectionResult);
        }
        this.f7900.m9185(connectionResult);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9210() {
        try {
            String interfaceDescriptor = this.f7901.getInterfaceDescriptor();
            if (!this.f7900.m9175().equals(interfaceDescriptor)) {
                String r3 = this.f7900.m9175();
                Log.e("GmsClient", new StringBuilder(String.valueOf(r3).length() + 34 + String.valueOf(interfaceDescriptor).length()).append("service descriptor mismatch: ").append(r3).append(" vs. ").append(interfaceDescriptor).toString());
                return false;
            }
            IInterface r1 = this.f7900.m9180(this.f7901);
            if (r1 == null) {
                return false;
            }
            if (!this.f7900.m9158(2, 4, r1) && !this.f7900.m9158(3, 4, r1)) {
                return false;
            }
            ConnectionResult unused = this.f7900.f7879 = null;
            Bundle r0 = this.f7900.m9179();
            if (this.f7900.f7869 != null) {
                this.f7900.f7869.m9197(r0);
            }
            return true;
        } catch (RemoteException e) {
            Log.w("GmsClient", "service probably died");
            return false;
        }
    }
}
