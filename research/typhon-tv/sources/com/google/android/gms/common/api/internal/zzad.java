package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collections;
import java.util.Map;

final class zzad implements OnCompleteListener<Map<zzh<?>, String>> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzaa f7491;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcu f7492;

    zzad(zzaa zzaa, zzcu zzcu) {
        this.f7491 = zzaa;
        this.f7492 = zzcu;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8608() {
        this.f7492.m8871();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8609(Task<Map<zzh<?>, String>> task) {
        this.f7491.f7472.lock();
        try {
            if (!this.f7491.f7476) {
                this.f7492.m8871();
                return;
            }
            if (task.m6101()) {
                Map unused = this.f7491.f7489 = new ArrayMap(this.f7491.f7484.size());
                for (zzz r0 : this.f7491.f7484.values()) {
                    this.f7491.f7489.put(r0.m4176(), ConnectionResult.f7411);
                }
            } else if (task.m6102() instanceof AvailabilityException) {
                AvailabilityException availabilityException = (AvailabilityException) task.m6102();
                if (this.f7491.f7477) {
                    Map unused2 = this.f7491.f7489 = new ArrayMap(this.f7491.f7484.size());
                    for (zzz zzz : this.f7491.f7484.values()) {
                        zzh r3 = zzz.m4176();
                        ConnectionResult connectionResult = availabilityException.getConnectionResult(zzz);
                        if (this.f7491.m8593((zzz<?>) zzz, connectionResult)) {
                            this.f7491.f7489.put(r3, new ConnectionResult(16));
                        } else {
                            this.f7491.f7489.put(r3, connectionResult);
                        }
                    }
                } else {
                    Map unused3 = this.f7491.f7489 = availabilityException.zzagj();
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", task.m6102());
                Map unused4 = this.f7491.f7489 = Collections.emptyMap();
            }
            if (this.f7491.m8599()) {
                this.f7491.f7488.putAll(this.f7491.f7489);
                if (this.f7491.m8579() == null) {
                    this.f7491.m8575();
                    this.f7491.m8577();
                    this.f7491.f7480.signalAll();
                }
            }
            this.f7492.m8871();
            this.f7491.f7472.unlock();
        } finally {
            this.f7491.f7472.unlock();
        }
    }
}
