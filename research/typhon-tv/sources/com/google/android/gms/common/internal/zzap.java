package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzew;

public final class zzap extends zzeu implements zzan {
    zzap(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Account m9092() throws RemoteException {
        Parcel r1 = m12300(2, v_());
        Account account = (Account) zzew.m12304(r1, Account.CREATOR);
        r1.recycle();
        return account;
    }
}
