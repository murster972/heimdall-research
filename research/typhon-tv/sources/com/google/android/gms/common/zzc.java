package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzc extends zzbfm {
    public static final Parcelable.Creator<zzc> CREATOR = new zzd();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7947;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f7948;

    public zzc(String str, int i) {
        this.f7948 = str;
        this.f7947 = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 1, this.f7948, false);
        zzbfp.m10185(parcel, 2, this.f7947);
        zzbfp.m10182(parcel, r0);
    }
}
