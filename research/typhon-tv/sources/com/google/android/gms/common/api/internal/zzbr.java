package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

final class zzbr implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzbo f7631;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ ConnectionResult f7632;

    zzbr(zzbo zzbo, ConnectionResult connectionResult) {
        this.f7631 = zzbo;
        this.f7632 = connectionResult;
    }

    public final void run() {
        this.f7631.onConnectionFailed(this.f7632);
    }
}
