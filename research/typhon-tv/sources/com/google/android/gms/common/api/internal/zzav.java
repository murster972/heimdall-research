package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzcxi;
import com.google.android.gms.internal.zzcxq;
import java.lang.ref.WeakReference;

final class zzav extends zzcxi {

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<zzao> f7538;

    zzav(zzao zzao) {
        this.f7538 = new WeakReference<>(zzao);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8683(zzcxq zzcxq) {
        zzao zzao = (zzao) this.f7538.get();
        if (zzao != null) {
            zzao.f7524.m8743((zzbj) new zzaw(this, zzao, zzao, zzcxq));
        }
    }
}
