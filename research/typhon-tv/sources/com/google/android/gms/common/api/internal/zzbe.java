package com.google.android.gms.common.api.internal;

import com.google.android.gms.auth.api.signin.internal.zzz;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

final class zzbe implements ResultCallback<Status> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ boolean f7575;

    /* renamed from: 麤  reason: contains not printable characters */
    private /* synthetic */ zzba f7576;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ GoogleApiClient f7577;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzda f7578;

    zzbe(zzba zzba, zzda zzda, boolean z, GoogleApiClient googleApiClient) {
        this.f7576 = zzba;
        this.f7578 = zzda;
        this.f7575 = z;
        this.f7577 = googleApiClient;
    }

    public final /* synthetic */ void onResult(Result result) {
        Status status = (Status) result;
        zzz.m7754(this.f7576.f7561).m7757();
        if (status.m8549() && this.f7576.isConnected()) {
            this.f7576.reconnect();
        }
        this.f7578.m4198(status);
        if (this.f7575) {
            this.f7577.disconnect();
        }
    }
}
