package com.google.android.gms.common.internal;

import android.content.Intent;
import android.support.v4.app.Fragment;

final class zzx extends zzv {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ Fragment f7914;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f7915;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Intent f7916;

    zzx(Intent intent, Fragment fragment, int i) {
        this.f7916 = intent;
        this.f7914 = fragment;
        this.f7915 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9233() {
        if (this.f7916 != null) {
            this.f7914.startActivityForResult(this.f7916, this.f7915);
        }
    }
}
