package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbz;

public final class zzal implements zzbh {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f7502 = false;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbi f7503;

    public zzal(zzbi zzbi) {
        this.f7503 = zzbi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8629(T t) {
        try {
            this.f7503.f7592.f7563.m8906(t);
            zzba zzba = this.f7503.f7592;
            Api.zze zze = zzba.f7564.get(t.m8941());
            zzbq.m9121(zze, (Object) "Appropriate Api was not requested.");
            if (zze.m8514() || !this.f7503.f7591.containsKey(t.m8941())) {
                boolean z = zze instanceof zzbz;
                Api.zzb zzb = zze;
                if (z) {
                    zzb = zzbz.m9137();
                }
                t.m8943(zzb);
                return t;
            }
            t.m8944(new Status(17));
            return t;
        } catch (DeadObjectException e) {
            this.f7503.m8743((zzbj) new zzam(this, this));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8630() {
        if (this.f7502) {
            return false;
        }
        if (this.f7503.f7592.m8711()) {
            this.f7502 = true;
            for (zzdg r0 : this.f7503.f7592.f7565) {
                r0.m8902();
            }
            return false;
        }
        this.f7503.m8741((ConnectionResult) null);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8631() {
        if (this.f7502) {
            this.f7502 = false;
            this.f7503.f7592.f7563.m8905();
            m8630();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8632() {
        if (this.f7502) {
            this.f7502 = false;
            this.f7503.m8743((zzbj) new zzan(this, this));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8633(T t) {
        return m8629(t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8634() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8635(int i) {
        this.f7503.m8741((ConnectionResult) null);
        this.f7503.f7590.m8836(i, this.f7502);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8636(Bundle bundle) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8637(ConnectionResult connectionResult, Api<?> api, boolean z) {
    }
}
