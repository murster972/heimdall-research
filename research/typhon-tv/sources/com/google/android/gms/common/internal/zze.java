package com.google.android.gms.common.internal;

import android.app.PendingIntent;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

abstract class zze extends zzi<Boolean> {

    /* renamed from: 靐  reason: contains not printable characters */
    private Bundle f7888;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzd f7889;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7890;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected zze(zzd zzd, int i, Bundle bundle) {
        super(zzd, true);
        this.f7889 = zzd;
        this.f7890 = i;
        this.f7888 = bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m9193(ConnectionResult connectionResult);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m9194(Object obj) {
        PendingIntent pendingIntent = null;
        if (((Boolean) obj) == null) {
            this.f7889.m9155(1, null);
            return;
        }
        switch (this.f7890) {
            case 0:
                if (!m9195()) {
                    this.f7889.m9155(1, null);
                    m9193(new ConnectionResult(8, (PendingIntent) null));
                    return;
                }
                return;
            case 10:
                this.f7889.m9155(1, null);
                throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
            default:
                this.f7889.m9155(1, null);
                if (this.f7888 != null) {
                    pendingIntent = (PendingIntent) this.f7888.getParcelable("pendingIntent");
                }
                m9193(new ConnectionResult(this.f7890, pendingIntent));
                return;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m9195();
}
