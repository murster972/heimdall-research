package com.google.android.gms.common;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzn extends zzbfm {
    public static final Parcelable.Creator<zzn> CREATOR = new zzo();

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzh f7957;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f7958;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f7959;

    zzn(String str, IBinder iBinder, boolean z) {
        this.f7959 = str;
        this.f7957 = m9297(iBinder);
        this.f7958 = z;
    }

    zzn(String str, zzh zzh, boolean z) {
        this.f7959 = str;
        this.f7957 = zzh;
        this.f7958 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzh m9297(IBinder iBinder) {
        zzi zzi;
        if (iBinder == null) {
            return null;
        }
        try {
            IObjectWrapper r0 = zzau.m9096(iBinder).m9095();
            byte[] bArr = r0 == null ? null : (byte[]) com.google.android.gms.dynamic.zzn.m9307(r0);
            if (bArr != null) {
                zzi = new zzi(bArr);
            } else {
                Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
                zzi = null;
            }
            return zzi;
        } catch (RemoteException e) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e);
            return null;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        IBinder asBinder;
        int r1 = zzbfp.m10181(parcel);
        zzbfp.m10193(parcel, 1, this.f7959, false);
        if (this.f7957 == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            asBinder = null;
        } else {
            asBinder = this.f7957.asBinder();
        }
        zzbfp.m10188(parcel, 2, asBinder, false);
        zzbfp.m10195(parcel, 3, this.f7958);
        zzbfp.m10182(parcel, r1);
    }
}
