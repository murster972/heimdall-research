package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.concurrent.atomic.AtomicReference;

final class zzbc implements GoogleApiClient.ConnectionCallbacks {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzda f7571;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzba f7572;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AtomicReference f7573;

    zzbc(zzba zzba, AtomicReference atomicReference, zzda zzda) {
        this.f7572 = zzba;
        this.f7573 = atomicReference;
        this.f7571 = zzda;
    }

    public final void onConnected(Bundle bundle) {
        this.f7572.m8706((GoogleApiClient) this.f7573.get(), this.f7571, true);
    }

    public final void onConnectionSuspended(int i) {
    }
}
