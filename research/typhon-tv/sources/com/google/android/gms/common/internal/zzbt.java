package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzbt extends zzbfm {
    public static final Parcelable.Creator<zzbt> CREATOR = new zzbu();

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f7853;

    /* renamed from: 靐  reason: contains not printable characters */
    private IBinder f7854;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f7855;

    /* renamed from: 齉  reason: contains not printable characters */
    private ConnectionResult f7856;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7857;

    zzbt(int i, IBinder iBinder, ConnectionResult connectionResult, boolean z, boolean z2) {
        this.f7857 = i;
        this.f7854 = iBinder;
        this.f7856 = connectionResult;
        this.f7855 = z;
        this.f7853 = z2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzbt)) {
            return false;
        }
        zzbt zzbt = (zzbt) obj;
        return this.f7856.equals(zzbt.f7856) && m9131().equals(zzbt.m9131());
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f7857);
        zzbfp.m10188(parcel, 2, this.f7854, false);
        zzbfp.m10189(parcel, 3, (Parcelable) this.f7856, i, false);
        zzbfp.m10195(parcel, 4, this.f7855);
        zzbfp.m10195(parcel, 5, this.f7853);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectionResult m9128() {
        return this.f7856;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m9129() {
        return this.f7853;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m9130() {
        return this.f7855;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzan m9131() {
        IBinder iBinder = this.f7854;
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
        return queryLocalInterface instanceof zzan ? (zzan) queryLocalInterface : new zzap(iBinder);
    }
}
