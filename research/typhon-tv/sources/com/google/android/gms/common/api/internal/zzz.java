package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;

public final class zzz<O extends Api.ApiOptions> extends GoogleApi<O> {

    /* renamed from: 连任  reason: contains not printable characters */
    private final Api.zza<? extends zzcxd, zzcxe> f7764;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Api.zze f7765;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzr f7766;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzt f7767;

    public zzz(Context context, Api<O> api, Looper looper, Api.zze zze, zzt zzt, zzr zzr, Api.zza<? extends zzcxd, zzcxe> zza) {
        super(context, api, looper);
        this.f7765 = zze;
        this.f7767 = zzt;
        this.f7766 = zzr;
        this.f7764 = zza;
        this.f3653.m8775((GoogleApi<?>) this);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Api.zze m9004() {
        return this.f7765;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Api.zze m9005(Looper looper, zzbo<O> zzbo) {
        this.f7767.m8962(zzbo);
        return this.f7765;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcv m9006(Context context, Handler handler) {
        return new zzcv(context, handler, this.f7766, this.f7764);
    }
}
