package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzbfn;

public final class zzbw implements Parcelable.Creator<zzbv> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r5 = zzbfn.m10169(parcel);
        Scope[] scopeArr = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < r5) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    scopeArr = (Scope[]) zzbfn.m10165(parcel, readInt, Scope.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r5);
        return new zzbv(i3, i2, i, scopeArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbv[i];
    }
}
