package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzba;
import com.google.android.gms.common.internal.zzbb;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamite.DynamiteModule;

final class zzg {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f7949 = new Object();

    /* renamed from: 齉  reason: contains not printable characters */
    private static Context f7950;

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzba f7951;

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m9283(String str, zzh zzh) {
        return m9287(str, zzh, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static synchronized void m9284(Context context) {
        synchronized (zzg.class) {
            if (f7950 != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            } else if (context != null) {
                f7950 = context.getApplicationContext();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m9285() {
        boolean z = true;
        if (f7951 == null) {
            zzbq.m9120(f7950);
            synchronized (f7949) {
                if (f7951 == null) {
                    try {
                        f7951 = zzbb.m9105(DynamiteModule.m9319(f7950, DynamiteModule.f7975, "com.google.android.gms.googlecertificates").m9324("com.google.android.gms.common.GoogleCertificatesImpl"));
                    } catch (DynamiteModule.zzc e) {
                        Log.e("GoogleCertificates", "Failed to load com.google.android.gms.googlecertificates", e);
                        z = false;
                    }
                }
            }
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m9286(String str, zzh zzh) {
        return m9287(str, zzh, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m9287(String str, zzh zzh, boolean z) {
        if (!m9285()) {
            return false;
        }
        zzbq.m9120(f7950);
        try {
            return f7951.m9104(new zzn(str, zzh, z), zzn.m9306(f7950.getPackageManager()));
        } catch (RemoteException e) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            return false;
        }
    }
}
