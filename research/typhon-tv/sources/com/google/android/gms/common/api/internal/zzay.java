package com.google.android.gms.common.api.internal;

abstract class zzay implements Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzao f7542;

    private zzay(zzao zzao) {
        this.f7542 = zzao;
    }

    /* synthetic */ zzay(zzao zzao, zzap zzap) {
        this(zzao);
    }

    public void run() {
        this.f7542.f7521.lock();
        try {
            if (!Thread.interrupted()) {
                m8685();
                this.f7542.f7521.unlock();
            }
        } catch (RuntimeException e) {
            this.f7542.f7524.m8744(e);
        } finally {
            this.f7542.f7521.unlock();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8685();
}
