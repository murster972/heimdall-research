package com.google.android.gms.common.data;

import com.google.android.gms.common.api.Releasable;
import java.util.Iterator;

public interface DataBuffer<T> extends Releasable, Iterable<T> {
    Iterator<T> iterator();

    /* renamed from: 靐  reason: contains not printable characters */
    int m9013();

    /* renamed from: 龘  reason: contains not printable characters */
    T m9014(int i);
}
