package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzbr extends zzbfm {
    public static final Parcelable.Creator<zzbr> CREATOR = new zzbs();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Account f7849;

    /* renamed from: 麤  reason: contains not printable characters */
    private final GoogleSignInAccount f7850;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7851;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7852;

    zzbr(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.f7852 = i;
        this.f7849 = account;
        this.f7851 = i2;
        this.f7850 = googleSignInAccount;
    }

    public zzbr(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f7852);
        zzbfp.m10189(parcel, 2, (Parcelable) this.f7849, i, false);
        zzbfp.m10185(parcel, 3, this.f7851);
        zzbfp.m10189(parcel, 4, (Parcelable) this.f7850, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
