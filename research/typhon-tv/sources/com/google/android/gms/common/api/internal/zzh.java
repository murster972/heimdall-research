package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.internal.zzbg;
import java.util.Arrays;

public final class zzh<O extends Api.ApiOptions> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f7713;

    /* renamed from: 麤  reason: contains not printable characters */
    private final O f7714;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Api<O> f7715;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f7716 = true;

    private zzh(Api<O> api) {
        this.f7715 = api;
        this.f7714 = null;
        this.f7713 = System.identityHashCode(this);
    }

    private zzh(Api<O> api, O o) {
        this.f7715 = api;
        this.f7714 = o;
        this.f7713 = Arrays.hashCode(new Object[]{this.f7715, this.f7714});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <O extends Api.ApiOptions> zzh<O> m8919(Api<O> api) {
        return new zzh<>(api);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <O extends Api.ApiOptions> zzh<O> m8920(Api<O> api, O o) {
        return new zzh<>(api, o);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzh)) {
            return false;
        }
        zzh zzh = (zzh) obj;
        return !this.f7716 && !zzh.f7716 && zzbg.m9113(this.f7715, zzh.f7715) && zzbg.m9113(this.f7714, zzh.f7714);
    }

    public final int hashCode() {
        return this.f7713;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m8921() {
        return this.f7715.m8505();
    }
}
