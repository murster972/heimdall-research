package com.google.android.gms.common.internal;

import android.util.Log;

public abstract class zzi<TListener> {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f7892 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzd f7893;

    /* renamed from: 龘  reason: contains not printable characters */
    private TListener f7894;

    public zzi(zzd zzd, TListener tlistener) {
        this.f7893 = zzd;
        this.f7894 = tlistener;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9201() {
        TListener tlistener;
        synchronized (this) {
            tlistener = this.f7894;
            if (this.f7892) {
                String valueOf = String.valueOf(this);
                Log.w("GmsClient", new StringBuilder(String.valueOf(valueOf).length() + 47).append("Callback proxy ").append(valueOf).append(" being reused. This is not safe.").toString());
            }
        }
        if (tlistener != null) {
            try {
                m9204(tlistener);
            } catch (RuntimeException e) {
                throw e;
            }
        }
        synchronized (this) {
            this.f7892 = true;
        }
        m9203();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m9202() {
        synchronized (this) {
            this.f7894 = null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9203() {
        m9202();
        synchronized (this.f7893.f7871) {
            this.f7893.f7871.remove(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m9204(TListener tlistener);
}
