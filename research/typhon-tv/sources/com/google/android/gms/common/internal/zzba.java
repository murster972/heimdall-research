package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.zzn;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzba extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    boolean m9104(zzn zzn, IObjectWrapper iObjectWrapper) throws RemoteException;
}
