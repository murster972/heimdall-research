package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.internal.zzbq;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

public final class zzcm {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Set<zzci<?>> f7661 = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: 靐  reason: contains not printable characters */
    public static <L> zzci<L> m8858(L l, Looper looper, String str) {
        zzbq.m9121(l, (Object) "Listener must not be null");
        zzbq.m9121(looper, (Object) "Looper must not be null");
        zzbq.m9121(str, (Object) "Listener type must not be null");
        return new zzci<>(looper, l, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <L> zzck<L> m8859(L l, String str) {
        zzbq.m9121(l, (Object) "Listener must not be null");
        zzbq.m9121(str, (Object) "Listener type must not be null");
        zzbq.m9123(str, (Object) "Listener type must not be empty");
        return new zzck<>(l, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <L> zzci<L> m8860(L l, Looper looper, String str) {
        zzci<L> r0 = m8858(l, looper, str);
        this.f7661.add(r0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8861() {
        for (zzci<?> r0 : this.f7661) {
            r0.m8854();
        }
        this.f7661.clear();
    }
}
