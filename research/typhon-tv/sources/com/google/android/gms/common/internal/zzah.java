package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Intent;
import java.util.Arrays;

public final class zzah {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7815;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f7816;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ComponentName f7817 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f7818;

    public zzah(String str, String str2, int i) {
        this.f7818 = zzbq.m9122(str);
        this.f7815 = zzbq.m9122(str2);
        this.f7816 = i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzah)) {
            return false;
        }
        zzah zzah = (zzah) obj;
        return zzbg.m9113(this.f7818, zzah.f7818) && zzbg.m9113(this.f7815, zzah.f7815) && zzbg.m9113(this.f7817, zzah.f7817) && this.f7816 == zzah.f7816;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f7818, this.f7815, this.f7817, Integer.valueOf(this.f7816)});
    }

    public final String toString() {
        return this.f7818 == null ? this.f7817.flattenToString() : this.f7818;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final ComponentName m9064() {
        return this.f7817;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Intent m9065() {
        return this.f7818 != null ? new Intent(this.f7818).setPackage(this.f7815) : new Intent().setComponent(this.f7817);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m9066() {
        return this.f7816;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m9067() {
        return this.f7815;
    }
}
