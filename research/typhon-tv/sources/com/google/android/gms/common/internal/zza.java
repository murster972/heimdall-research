package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;

public final class zza extends zzao {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Account m9038(zzan zzan) {
        Account account = null;
        if (zzan != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                account = zzan.m9091();
            } catch (RemoteException e) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return account;
    }

    public final boolean equals(Object obj) {
        throw new NoSuchMethodError();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Account m9039() {
        throw new NoSuchMethodError();
    }
}
