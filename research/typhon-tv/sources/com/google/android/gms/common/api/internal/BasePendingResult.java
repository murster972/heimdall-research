package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaq;
import com.google.android.gms.common.internal.zzbq;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@KeepName
public abstract class BasePendingResult<R extends Result> extends PendingResult<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    static final ThreadLocal<Boolean> f3654 = new zzs();
    @KeepName
    private zzb mResultGuardian;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ArrayList<PendingResult.zza> f3655;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ResultCallback<? super R> f3656;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final AtomicReference<zzdm> f3657;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f3658;

    /* renamed from: ʿ  reason: contains not printable characters */
    private zzaq f3659;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f3660;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public R f3661;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Status f3662;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private volatile boolean f3663;

    /* renamed from: 连任  reason: contains not printable characters */
    private final CountDownLatch f3664;

    /* renamed from: 麤  reason: contains not printable characters */
    private WeakReference<GoogleApiClient> f3665;

    /* renamed from: 齉  reason: contains not printable characters */
    private zza<R> f3666;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f3667;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private volatile zzdg<R> f3668;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f3669;

    public static class zza<R extends Result> extends Handler {
        public zza() {
            this(Looper.getMainLooper());
        }

        public zza(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Pair pair = (Pair) message.obj;
                    ResultCallback resultCallback = (ResultCallback) pair.first;
                    Result result = (Result) pair.second;
                    try {
                        resultCallback.onResult(result);
                        return;
                    } catch (RuntimeException e) {
                        BasePendingResult.m4186(result);
                        throw e;
                    }
                case 2:
                    ((BasePendingResult) message.obj).m4194(Status.f7462);
                    return;
                default:
                    Log.wtf("BasePendingResult", new StringBuilder(45).append("Don't know how to handle message: ").append(message.what).toString(), new Exception());
                    return;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m8552(ResultCallback<? super R> resultCallback, R r) {
            sendMessage(obtainMessage(1, new Pair(resultCallback, r)));
        }
    }

    final class zzb {
        private zzb() {
        }

        /* synthetic */ zzb(BasePendingResult basePendingResult, zzs zzs) {
            this();
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            BasePendingResult.m4186(BasePendingResult.this.f3661);
            super.finalize();
        }
    }

    @Deprecated
    BasePendingResult() {
        this.f3667 = new Object();
        this.f3664 = new CountDownLatch(1);
        this.f3655 = new ArrayList<>();
        this.f3657 = new AtomicReference<>();
        this.f3669 = false;
        this.f3666 = new zza<>(Looper.getMainLooper());
        this.f3665 = new WeakReference<>((Object) null);
    }

    protected BasePendingResult(GoogleApiClient googleApiClient) {
        this.f3667 = new Object();
        this.f3664 = new CountDownLatch(1);
        this.f3655 = new ArrayList<>();
        this.f3657 = new AtomicReference<>();
        this.f3669 = false;
        this.f3666 = new zza<>(googleApiClient != null ? googleApiClient.getLooper() : Looper.getMainLooper());
        this.f3665 = new WeakReference<>(googleApiClient);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final R m4185() {
        R r;
        boolean z = true;
        synchronized (this.f3667) {
            if (this.f3663) {
                z = false;
            }
            zzbq.m9126(z, (Object) "Result has already been consumed.");
            zzbq.m9126(m4192(), (Object) "Result is not ready.");
            r = this.f3661;
            this.f3661 = null;
            this.f3656 = null;
            this.f3663 = true;
        }
        zzdm andSet = this.f3657.getAndSet((Object) null);
        if (andSet != null) {
            andSet.m8910(this);
        }
        return r;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4186(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).m8539();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(result);
                Log.w("BasePendingResult", new StringBuilder(String.valueOf(valueOf).length() + 18).append("Unable to release ").append(valueOf).toString(), e);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final void m4187(R r) {
        this.f3661 = r;
        this.f3659 = null;
        this.f3664.countDown();
        this.f3662 = this.f3661.s_();
        if (this.f3660) {
            this.f3656 = null;
        } else if (this.f3656 != null) {
            this.f3666.removeMessages(2);
            this.f3666.m8552(this.f3656, m4185());
        } else if (this.f3661 instanceof Releasable) {
            this.mResultGuardian = new zzb(this, (zzs) null);
        }
        ArrayList arrayList = this.f3655;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((PendingResult.zza) obj).m8536(this.f3662);
        }
        this.f3655.clear();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m4189() {
        this.f3669 = this.f3669 || f3654.get().booleanValue();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m4190() {
        boolean r0;
        synchronized (this.f3667) {
            if (((GoogleApiClient) this.f3665.get()) == null || !this.f3669) {
                m8533();
            }
            r0 = m8531();
        }
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m4191() {
        boolean z;
        synchronized (this.f3667) {
            z = this.f3660;
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m4192() {
        return this.f3664.getCount() == 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Integer m4193() {
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m4194(Status status) {
        synchronized (this.f3667) {
            if (!m4192()) {
                m4198(m4195(status));
                this.f3658 = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract R m4195(Status status);

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m4196() {
        /*
            r2 = this;
            java.lang.Object r1 = r2.f3667
            monitor-enter(r1)
            boolean r0 = r2.f3660     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x000b
            boolean r0 = r2.f3663     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
        L_0x000c:
            return
        L_0x000d:
            com.google.android.gms.common.internal.zzaq r0 = r2.f3659     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0016
            com.google.android.gms.common.internal.zzaq r0 = r2.f3659     // Catch:{ RemoteException -> 0x002c }
            r0.m9093()     // Catch:{ RemoteException -> 0x002c }
        L_0x0016:
            R r0 = r2.f3661     // Catch:{ all -> 0x0029 }
            m4186(r0)     // Catch:{ all -> 0x0029 }
            r0 = 1
            r2.f3660 = r0     // Catch:{ all -> 0x0029 }
            com.google.android.gms.common.api.Status r0 = com.google.android.gms.common.api.Status.f7460     // Catch:{ all -> 0x0029 }
            com.google.android.gms.common.api.Result r0 = r2.m4195((com.google.android.gms.common.api.Status) r0)     // Catch:{ all -> 0x0029 }
            r2.m4187(r0)     // Catch:{ all -> 0x0029 }
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            goto L_0x000c
        L_0x0029:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            throw r0
        L_0x002c:
            r0 = move-exception
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.BasePendingResult.m4196():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4197(PendingResult.zza zza2) {
        zzbq.m9117(zza2 != null, "Callback cannot be null.");
        synchronized (this.f3667) {
            if (m4192()) {
                zza2.m8536(this.f3662);
            } else {
                this.f3655.add(zza2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4198(R r) {
        boolean z = true;
        synchronized (this.f3667) {
            if (this.f3658 || this.f3660) {
                m4186(r);
                return;
            }
            if (m4192()) {
            }
            zzbq.m9126(!m4192(), (Object) "Results have already been set");
            if (this.f3663) {
                z = false;
            }
            zzbq.m9126(z, (Object) "Result has already been consumed");
            m4187(r);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m4199(com.google.android.gms.common.api.ResultCallback<? super R> r6) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            java.lang.Object r3 = r5.f3667
            monitor-enter(r3)
            if (r6 != 0) goto L_0x000c
            r0 = 0
            r5.f3656 = r0     // Catch:{ all -> 0x0029 }
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
        L_0x000b:
            return
        L_0x000c:
            boolean r2 = r5.f3663     // Catch:{ all -> 0x0029 }
            if (r2 != 0) goto L_0x002c
            r2 = r0
        L_0x0011:
            java.lang.String r4 = "Result has already been consumed."
            com.google.android.gms.common.internal.zzbq.m9126((boolean) r2, (java.lang.Object) r4)     // Catch:{ all -> 0x0029 }
            com.google.android.gms.common.api.internal.zzdg<R> r2 = r5.f3668     // Catch:{ all -> 0x0029 }
            if (r2 != 0) goto L_0x002e
        L_0x001b:
            java.lang.String r1 = "Cannot set callbacks if then() has been called."
            com.google.android.gms.common.internal.zzbq.m9126((boolean) r0, (java.lang.Object) r1)     // Catch:{ all -> 0x0029 }
            boolean r0 = r5.m8531()     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0030
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
            goto L_0x000b
        L_0x0029:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
            throw r0
        L_0x002c:
            r2 = r1
            goto L_0x0011
        L_0x002e:
            r0 = r1
            goto L_0x001b
        L_0x0030:
            boolean r0 = r5.m4192()     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0041
            com.google.android.gms.common.api.internal.BasePendingResult$zza<R> r0 = r5.f3666     // Catch:{ all -> 0x0029 }
            com.google.android.gms.common.api.Result r1 = r5.m4185()     // Catch:{ all -> 0x0029 }
            r0.m8552(r6, r1)     // Catch:{ all -> 0x0029 }
        L_0x003f:
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
            goto L_0x000b
        L_0x0041:
            r5.f3656 = r6     // Catch:{ all -> 0x0029 }
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.BasePendingResult.m4199(com.google.android.gms.common.api.ResultCallback):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4200(zzdm zzdm) {
        this.f3657.set(zzdm);
    }
}
