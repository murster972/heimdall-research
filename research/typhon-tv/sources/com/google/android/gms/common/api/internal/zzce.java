package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import com.google.android.gms.common.internal.zzbq;

public final class zzce {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f7647;

    public zzce(Activity activity) {
        zzbq.m9121(activity, (Object) "Activity must not be null");
        this.f7647 = activity;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8839() {
        return this.f7647 instanceof Activity;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final FragmentActivity m8840() {
        return (FragmentActivity) this.f7647;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Activity m8841() {
        return (Activity) this.f7647;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8842() {
        return this.f7647 instanceof FragmentActivity;
    }
}
