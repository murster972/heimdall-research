package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class zzae {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Map<TaskCompletionSource<?>, Boolean> f7493 = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<BasePendingResult<?>, Boolean> f7494 = Collections.synchronizedMap(new WeakHashMap());

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8612(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.f7494) {
            hashMap = new HashMap(this.f7494);
        }
        synchronized (this.f7493) {
            hashMap2 = new HashMap(this.f7493);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).m4194(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((TaskCompletionSource) entry2.getKey()).m13718((Exception) new ApiException(status));
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8613() {
        m8612(false, zzbm.f7602);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8614() {
        m8612(true, zzdj.f7701);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8615(BasePendingResult<? extends Result> basePendingResult, boolean z) {
        this.f7494.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.m8534((PendingResult.zza) new zzaf(this, basePendingResult));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final <TResult> void m8616(TaskCompletionSource<TResult> taskCompletionSource, boolean z) {
        this.f7493.put(taskCompletionSource, Boolean.valueOf(z));
        taskCompletionSource.m13720().m6104(new zzag(this, taskCompletionSource));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8617() {
        return !this.f7494.isEmpty() || !this.f7493.isEmpty();
    }
}
