package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;

public interface zzcf {
    void startActivityForResult(Intent intent, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    Activity m8843();

    /* renamed from: 龘  reason: contains not printable characters */
    <T extends LifecycleCallback> T m8844(String str, Class<T> cls);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8845(String str, LifecycleCallback lifecycleCallback);
}
