package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.internal.zzbq;

final class zzcj extends Handler {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzci f7658;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzcj(zzci zzci, Looper looper) {
        super(looper);
        this.f7658 = zzci;
    }

    public final void handleMessage(Message message) {
        boolean z = true;
        if (message.what != 1) {
            z = false;
        }
        zzbq.m9116(z);
        this.f7658.m8853((zzcl) message.obj);
    }
}
