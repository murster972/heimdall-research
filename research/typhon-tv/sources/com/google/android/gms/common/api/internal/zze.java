package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zze<TResult> extends zza {

    /* renamed from: 靐  reason: contains not printable characters */
    private final TaskCompletionSource<TResult> f7709;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzcz f7710;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzdd<Api.zzb, TResult> f7711;

    public zze(int i, zzdd<Api.zzb, TResult> zzdd, TaskCompletionSource<TResult> taskCompletionSource, zzcz zzcz) {
        super(i);
        this.f7709 = taskCompletionSource;
        this.f7711 = zzdd;
        this.f7710 = zzcz;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8912(Status status) {
        this.f7709.m13718(this.f7710.m8881(status));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8913(zzae zzae, boolean z) {
        zzae.m8616(this.f7709, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8914(zzbo<?> zzbo) throws DeadObjectException {
        try {
            this.f7711.m8889(zzbo.m8799(), this.f7709);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            m8566(zza.m8564(e2));
        }
    }
}
