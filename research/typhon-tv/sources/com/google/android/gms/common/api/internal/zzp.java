package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbq;

final class zzp {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ConnectionResult f7738;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f7739;

    zzp(ConnectionResult connectionResult, int i) {
        zzbq.m9120(connectionResult);
        this.f7738 = connectionResult;
        this.f7739 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectionResult m8958() {
        return this.f7738;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m8959() {
        return this.f7739;
    }
}
