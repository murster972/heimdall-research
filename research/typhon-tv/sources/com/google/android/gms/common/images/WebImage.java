package com.google.android.gms.common.images;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class WebImage extends zzbfm {
    public static final Parcelable.Creator<WebImage> CREATOR = new zze();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Uri f7794;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f7795;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7796;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7797;

    WebImage(int i, Uri uri, int i2, int i3) {
        this.f7797 = i;
        this.f7794 = uri;
        this.f7796 = i2;
        this.f7795 = i3;
    }

    public WebImage(Uri uri) throws IllegalArgumentException {
        this(uri, 0, 0);
    }

    public WebImage(Uri uri, int i, int i2) throws IllegalArgumentException {
        this(1, uri, i, i2);
        if (uri == null) {
            throw new IllegalArgumentException("url cannot be null");
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("width and height must not be negative");
        }
    }

    public WebImage(JSONObject jSONObject) throws IllegalArgumentException {
        this(m9033(jSONObject), jSONObject.optInt(VastIconXmlManager.WIDTH, 0), jSONObject.optInt(VastIconXmlManager.HEIGHT, 0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Uri m9033(JSONObject jSONObject) {
        if (!jSONObject.has("url")) {
            return null;
        }
        try {
            return Uri.parse(jSONObject.getString("url"));
        } catch (JSONException e) {
            return null;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof WebImage)) {
            return false;
        }
        WebImage webImage = (WebImage) obj;
        return zzbg.m9113(this.f7794, webImage.f7794) && this.f7796 == webImage.f7796 && this.f7795 == webImage.f7795;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f7794, Integer.valueOf(this.f7796), Integer.valueOf(this.f7795)});
    }

    public final String toString() {
        return String.format(Locale.US, "Image %dx%d %s", new Object[]{Integer.valueOf(this.f7796), Integer.valueOf(this.f7795), this.f7794.toString()});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f7797);
        zzbfp.m10189(parcel, 2, (Parcelable) m9037(), i, false);
        zzbfp.m10185(parcel, 3, m9034());
        zzbfp.m10185(parcel, 4, m9036());
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9034() {
        return this.f7796;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final JSONObject m9035() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("url", (Object) this.f7794.toString());
            jSONObject.put(VastIconXmlManager.WIDTH, this.f7796);
            jSONObject.put(VastIconXmlManager.HEIGHT, this.f7795);
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m9036() {
        return this.f7795;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Uri m9037() {
        return this.f7794;
    }
}
