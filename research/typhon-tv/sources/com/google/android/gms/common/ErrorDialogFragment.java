package com.google.android.gms.common;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzbq;

public class ErrorDialogFragment extends DialogFragment {

    /* renamed from: 靐  reason: contains not printable characters */
    private DialogInterface.OnCancelListener f7416 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Dialog f7417 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static ErrorDialogFragment m8482(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        Dialog dialog2 = (Dialog) zzbq.m9121(dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener((DialogInterface.OnCancelListener) null);
        dialog2.setOnDismissListener((DialogInterface.OnDismissListener) null);
        errorDialogFragment.f7417 = dialog2;
        if (onCancelListener != null) {
            errorDialogFragment.f7416 = onCancelListener;
        }
        return errorDialogFragment;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.f7416 != null) {
            this.f7416.onCancel(dialogInterface);
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f7417 == null) {
            setShowsDialog(false);
        }
        return this.f7417;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
