package com.google.android.gms.common;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzat;
import com.google.android.gms.common.internal.zzau;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzl;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

abstract class zzh extends zzau {

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7952;

    protected zzh(byte[] bArr) {
        boolean z = false;
        if (bArr.length != 25) {
            int length = bArr.length;
            String r3 = zzl.m9258(bArr, 0, bArr.length, false);
            Log.wtf("GoogleCertificates", new StringBuilder(String.valueOf(r3).length() + 51).append("Cert hash data has incorrect length (").append(length).append("):\n").append(r3).toString(), new Exception());
            bArr = Arrays.copyOfRange(bArr, 0, 25);
            zzbq.m9117(bArr.length == 25 ? true : z, new StringBuilder(55).append("cert hash data has incorrect length. length=").append(bArr.length).toString());
        }
        this.f7952 = Arrays.hashCode(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static byte[] m9288(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof zzat)) {
            return false;
        }
        try {
            zzat zzat = (zzat) obj;
            if (zzat.m9094() != hashCode()) {
                return false;
            }
            IObjectWrapper r0 = zzat.m9095();
            if (r0 == null) {
                return false;
            }
            return Arrays.equals(m9290(), (byte[]) zzn.m9307(r0));
        } catch (RemoteException e) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            return false;
        }
    }

    public int hashCode() {
        return this.f7952;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9289() {
        return hashCode();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract byte[] m9290();

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m9291() {
        return zzn.m9306(m9290());
    }
}
