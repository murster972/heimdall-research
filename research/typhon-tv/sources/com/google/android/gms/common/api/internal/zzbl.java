package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzbhb;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class zzbl {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ExecutorService f7598 = Executors.newFixedThreadPool(2, new zzbhb("GAC_Executor"));

    /* renamed from: 龘  reason: contains not printable characters */
    public static ExecutorService m8749() {
        return f7598;
    }
}
