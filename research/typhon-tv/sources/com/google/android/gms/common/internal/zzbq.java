package com.google.android.gms.common.internal;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.common.util.zzw;

public final class zzbq {
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m9115(String str) {
        if (!zzw.m9279()) {
            throw new IllegalStateException(str);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m9116(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m9117(boolean z, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m9118(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalArgumentException(String.format(str, objArr));
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m9119(String str) {
        if (zzw.m9279()) {
            throw new IllegalStateException(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m9120(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException("null reference");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m9121(T t, Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9122(String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9123(String str, Object obj) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9124(Handler handler) {
        if (Looper.myLooper() != handler.getLooper()) {
            throw new IllegalStateException("Must be called on the handler thread");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9125(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9126(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9127(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalStateException(String.format(str, objArr));
        }
    }
}
