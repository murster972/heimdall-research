package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzat extends IInterface {
    /* renamed from: 靐  reason: contains not printable characters */
    int m9094() throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m9095() throws RemoteException;
}
