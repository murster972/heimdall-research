package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class Scope extends zzbfm implements ReflectedParcelable {
    public static final Parcelable.Creator<Scope> CREATOR = new zzf();

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7456;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7457;

    Scope(int i, String str) {
        zzbq.m9123(str, (Object) "scopeUri must not be null or empty");
        this.f7457 = i;
        this.f7456 = str;
    }

    public Scope(String str) {
        this(1, str);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Scope)) {
            return false;
        }
        return this.f7456.equals(((Scope) obj).f7456);
    }

    public final int hashCode() {
        return this.f7456.hashCode();
    }

    public final String toString() {
        return this.f7456;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f7457);
        zzbfp.m10193(parcel, 2, this.f7456, false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m8544() {
        return this.f7456;
    }
}
