package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Map;
import java.util.Set;

public final class zzj {

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f7722 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ArrayMap<zzh<?>, String> f7723 = new ArrayMap<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private int f7724;

    /* renamed from: 齉  reason: contains not printable characters */
    private final TaskCompletionSource<Map<zzh<?>, String>> f7725 = new TaskCompletionSource<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final ArrayMap<zzh<?>, ConnectionResult> f7726 = new ArrayMap<>();

    public zzj(Iterable<? extends GoogleApi<?>> iterable) {
        for (GoogleApi r0 : iterable) {
            this.f7726.put(r0.m4176(), null);
        }
        this.f7724 = this.f7726.keySet().size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Task<Map<zzh<?>, String>> m8931() {
        return this.f7725.m13720();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Set<zzh<?>> m8932() {
        return this.f7726.keySet();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8933(zzh<?> zzh, ConnectionResult connectionResult, String str) {
        this.f7726.put(zzh, connectionResult);
        this.f7723.put(zzh, str);
        this.f7724--;
        if (!connectionResult.m8478()) {
            this.f7722 = true;
        }
        if (this.f7724 != 0) {
            return;
        }
        if (this.f7722) {
            this.f7725.m13721((Exception) new AvailabilityException(this.f7726));
            return;
        }
        this.f7725.m13722(this.f7723);
    }
}
