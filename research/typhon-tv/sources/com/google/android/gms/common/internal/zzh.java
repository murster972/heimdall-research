package com.google.android.gms.common.internal;

import android.app.PendingIntent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;

final class zzh extends Handler {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzd f7891;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzh(zzd zzd, Looper looper) {
        super(looper);
        this.f7891 = zzd;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m9199(Message message) {
        return message.what == 2 || message.what == 1 || message.what == 7;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m9200(Message message) {
        ((zzi) message.obj).m9203();
    }

    public final void handleMessage(Message message) {
        PendingIntent pendingIntent = null;
        if (this.f7891.f7884.get() != message.arg1) {
            if (m9199(message)) {
                m9200(message);
            }
        } else if ((message.what == 1 || message.what == 7 || message.what == 4 || message.what == 5) && !this.f7891.m9162()) {
            m9200(message);
        } else if (message.what == 4) {
            ConnectionResult unused = this.f7891.f7879 = new ConnectionResult(message.arg2);
            if (!this.f7891.m9145() || this.f7891.f7880) {
                ConnectionResult r0 = this.f7891.f7879 != null ? this.f7891.f7879 : new ConnectionResult(8);
                this.f7891.f7882.m9205(r0);
                this.f7891.m9185(r0);
                return;
            }
            this.f7891.m9155(3, null);
        } else if (message.what == 5) {
            ConnectionResult r02 = this.f7891.f7879 != null ? this.f7891.f7879 : new ConnectionResult(8);
            this.f7891.f7882.m9205(r02);
            this.f7891.m9185(r02);
        } else if (message.what == 3) {
            if (message.obj instanceof PendingIntent) {
                pendingIntent = (PendingIntent) message.obj;
            }
            ConnectionResult connectionResult = new ConnectionResult(message.arg2, pendingIntent);
            this.f7891.f7882.m9205(connectionResult);
            this.f7891.m9185(connectionResult);
        } else if (message.what == 6) {
            this.f7891.m9155(5, null);
            if (this.f7891.f7869 != null) {
                this.f7891.f7869.m9196(message.arg2);
            }
            this.f7891.m9181(message.arg2);
            boolean unused2 = this.f7891.m9158(5, 1, null);
        } else if (message.what == 2 && !this.f7891.m9161()) {
            m9200(message);
        } else if (m9199(message)) {
            ((zzi) message.obj).m9201();
        } else {
            Log.wtf("GmsClient", new StringBuilder(45).append("Don't know how to handle message: ").append(message.what).toString(), new Exception());
        }
    }
}
