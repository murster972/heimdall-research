package com.google.android.gms.common.api.internal;

abstract class zzbj {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzbh f7596;

    protected zzbj(zzbh zzbh) {
        this.f7596 = zzbh;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8747();

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8748(zzbi zzbi) {
        zzbi.f7581.lock();
        try {
            if (zzbi.f7585 == this.f7596) {
                m8747();
                zzbi.f7581.unlock();
            }
        } finally {
            zzbi.f7581.unlock();
        }
    }
}
