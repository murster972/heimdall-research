package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Result;

public final class zzbw<O extends Api.ApiOptions> extends zzak {

    /* renamed from: 龘  reason: contains not printable characters */
    private final GoogleApi<O> f7643;

    public zzbw(GoogleApi<O> googleApi) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.f7643 = googleApi;
    }

    public final Context getContext() {
        return this.f7643.m4174();
    }

    public final Looper getLooper() {
        return this.f7643.m4175();
    }

    public final void zza(zzdg zzdg) {
    }

    public final void zzb(zzdg zzdg) {
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(T t) {
        return this.f7643.m4183(t);
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(T t) {
        return this.f7643.m4177(t);
    }
}
