package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.zzbq;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class zzb<T> implements Iterator<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    protected int f7790 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final DataBuffer<T> f7791;

    public zzb(DataBuffer<T> dataBuffer) {
        this.f7791 = (DataBuffer) zzbq.m9120(dataBuffer);
    }

    public boolean hasNext() {
        return this.f7790 < this.f7791.m9013() + -1;
    }

    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException(new StringBuilder(46).append("Cannot advance the iterator beyond ").append(this.f7790).toString());
        }
        DataBuffer<T> dataBuffer = this.f7791;
        int i = this.f7790 + 1;
        this.f7790 = i;
        return dataBuffer.m9014(i);
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
