package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;

public final class zzl implements ServiceConnection {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzd f7897;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f7898;

    public zzl(zzd zzd, int i) {
        this.f7897 = zzd;
        this.f7898 = i;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        zzay zzaz;
        if (iBinder == null) {
            this.f7897.m9150(16);
            return;
        }
        synchronized (this.f7897.f7886) {
            zzd zzd = this.f7897;
            if (iBinder == null) {
                zzaz = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                zzaz = (queryLocalInterface == null || !(queryLocalInterface instanceof zzay)) ? new zzaz(iBinder) : (zzay) queryLocalInterface;
            }
            zzay unused = zzd.f7887 = zzaz;
        }
        this.f7897.m9182(0, (Bundle) null, this.f7898);
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.f7897.f7886) {
            zzay unused = this.f7897.f7887 = null;
        }
        this.f7897.f7885.sendMessage(this.f7897.f7885.obtainMessage(6, this.f7898, 1));
    }
}
