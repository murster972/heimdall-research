package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.ServiceConnection;

public abstract class zzag {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzag f7813;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f7814 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzag m9060(Context context) {
        synchronized (f7814) {
            if (f7813 == null) {
                f7813 = new zzai(context.getApplicationContext());
            }
        }
        return f7813;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m9061(zzah zzah, ServiceConnection serviceConnection, String str);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9062(String str, String str2, int i, ServiceConnection serviceConnection, String str3) {
        m9061(new zzah(str, str2, i), serviceConnection, str3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m9063(zzah zzah, ServiceConnection serviceConnection, String str);
}
