package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collections;
import java.util.Map;

final class zzac implements OnCompleteListener<Map<zzh<?>, String>> {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzaa f7490;

    private zzac(zzaa zzaa) {
        this.f7490 = zzaa;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8607(Task<Map<zzh<?>, String>> task) {
        this.f7490.f7472.lock();
        try {
            if (this.f7490.f7476) {
                if (task.m6101()) {
                    Map unused = this.f7490.f7488 = new ArrayMap(this.f7490.f7487.size());
                    for (zzz r0 : this.f7490.f7487.values()) {
                        this.f7490.f7488.put(r0.m4176(), ConnectionResult.f7411);
                    }
                } else if (task.m6102() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) task.m6102();
                    if (this.f7490.f7477) {
                        Map unused2 = this.f7490.f7488 = new ArrayMap(this.f7490.f7487.size());
                        for (zzz zzz : this.f7490.f7487.values()) {
                            zzh r3 = zzz.m4176();
                            ConnectionResult connectionResult = availabilityException.getConnectionResult(zzz);
                            if (this.f7490.m8593((zzz<?>) zzz, connectionResult)) {
                                this.f7490.f7488.put(r3, new ConnectionResult(16));
                            } else {
                                this.f7490.f7488.put(r3, connectionResult);
                            }
                        }
                    } else {
                        Map unused3 = this.f7490.f7488 = availabilityException.zzagj();
                    }
                    ConnectionResult unused4 = this.f7490.f7479 = this.f7490.m8579();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", task.m6102());
                    Map unused5 = this.f7490.f7488 = Collections.emptyMap();
                    ConnectionResult unused6 = this.f7490.f7479 = new ConnectionResult(8);
                }
                if (this.f7490.f7489 != null) {
                    this.f7490.f7488.putAll(this.f7490.f7489);
                    ConnectionResult unused7 = this.f7490.f7479 = this.f7490.m8579();
                }
                if (this.f7490.f7479 == null) {
                    this.f7490.m8575();
                    this.f7490.m8577();
                } else {
                    boolean unused8 = this.f7490.f7476 = false;
                    this.f7490.f7483.m8714(this.f7490.f7479);
                }
                this.f7490.f7480.signalAll();
                this.f7490.f7472.unlock();
            }
        } finally {
            this.f7490.f7472.unlock();
        }
    }
}
