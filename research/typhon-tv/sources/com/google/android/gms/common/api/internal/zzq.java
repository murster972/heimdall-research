package com.google.android.gms.common.api.internal;

import android.content.DialogInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;

final class zzq implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzp f7740;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzo f7741;

    zzq(zzo zzo, zzp zzp) {
        this.f7741 = zzo;
        this.f7740 = zzp;
    }

    public final void run() {
        if (this.f7741.f7735) {
            ConnectionResult r3 = this.f7740.m8958();
            if (r3.m8481()) {
                this.f7741.f7470.startActivityForResult(GoogleApiActivity.m8528(this.f7741.m8560(), r3.m8479(), this.f7740.m8959(), false), 1);
            } else if (this.f7741.f7736.m4230(r3.m8480())) {
                this.f7741.f7736.m8501(this.f7741.m8560(), this.f7741.f7470, r3.m8480(), 2, this.f7741);
            } else if (r3.m8480() == 18) {
                GoogleApiAvailability.m8487(this.f7741.m8560().getApplicationContext(), (zzby) new zzr(this, GoogleApiAvailability.m8484(this.f7741.m8560(), (DialogInterface.OnCancelListener) this.f7741)));
            } else {
                this.f7741.m8957(r3, this.f7740.m8959());
            }
        }
    }
}
