package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzan;
import java.util.Collections;

final class zzbv implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzbu f7641;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ ConnectionResult f7642;

    zzbv(zzbu zzbu, ConnectionResult connectionResult) {
        this.f7641 = zzbu;
        this.f7642 = connectionResult;
    }

    public final void run() {
        if (this.f7642.m8478()) {
            boolean unused = this.f7641.f7635 = true;
            if (this.f7641.f7637.m8517()) {
                this.f7641.m8812();
            } else {
                this.f7641.f7637.m8522((zzan) null, Collections.emptySet());
            }
        } else {
            ((zzbo) this.f7641.f7640.f7604.get(this.f7641.f7639)).onConnectionFailed(this.f7642);
        }
    }
}
