package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.support.v4.util.ArraySet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbq;

public class zzah extends zzo {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzbm f7499;

    /* renamed from: 连任  reason: contains not printable characters */
    private final ArraySet<zzh<?>> f7500 = new ArraySet<>();

    private zzah(zzcf zzcf) {
        super(zzcf);
        this.f7470.m8845("ConnectionlessLifecycleHelper", (LifecycleCallback) this);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private final void m8620() {
        if (!this.f7500.isEmpty()) {
            this.f7499.m8778(this);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m8621(Activity activity, zzbm zzbm, zzh<?> zzh) {
        zzcf r1 = m8553(activity);
        zzah zzah = (zzah) r1.m8844("ConnectionlessLifecycleHelper", zzah.class);
        if (zzah == null) {
            zzah = new zzah(r1);
        }
        zzah.f7499 = zzbm;
        zzbq.m9121(zzh, (Object) "ApiKey cannot be null");
        zzah.f7500.add(zzh);
        zzbm.m8778(zzah);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8622() {
        this.f7499.m8771();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final ArraySet<zzh<?>> m8623() {
        return this.f7500;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8624() {
        super.m8951();
        m8620();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8625() {
        super.m8954();
        this.f7499.m8770(this);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8626() {
        super.m8559();
        m8620();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8627(ConnectionResult connectionResult, int i) {
        this.f7499.m8769(connectionResult, i);
    }
}
