package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zzb<T> extends zza {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final TaskCompletionSource<T> f7544;

    public zzb(int i, TaskCompletionSource<T> taskCompletionSource) {
        super(i);
        this.f7544 = taskCompletionSource;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m8694(zzbo<?> zzbo) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8695(Status status) {
        this.f7544.m13718((Exception) new ApiException(status));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8696(zzae zzae, boolean z) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8697(zzbo<?> zzbo) throws DeadObjectException {
        try {
            m8694(zzbo);
        } catch (DeadObjectException e) {
            m8566(zza.m8564(e));
            throw e;
        } catch (RemoteException e2) {
            m8566(zza.m8564(e2));
        }
    }
}
