package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import com.google.android.gms.common.api.zze;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;

final class zzdl implements IBinder.DeathRecipient, zzdm {

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakReference<zze> f7706;

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<IBinder> f7707;

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<BasePendingResult<?>> f7708;

    private zzdl(BasePendingResult<?> basePendingResult, zze zze, IBinder iBinder) {
        this.f7706 = new WeakReference<>(zze);
        this.f7708 = new WeakReference<>(basePendingResult);
        this.f7707 = new WeakReference<>(iBinder);
    }

    /* synthetic */ zzdl(BasePendingResult basePendingResult, zze zze, IBinder iBinder, zzdk zzdk) {
        this(basePendingResult, (zze) null, iBinder);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8908() {
        BasePendingResult basePendingResult = (BasePendingResult) this.f7708.get();
        zze zze = (zze) this.f7706.get();
        if (!(zze == null || basePendingResult == null)) {
            zze.m9010(basePendingResult.m8532().intValue());
        }
        IBinder iBinder = (IBinder) this.f7707.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException e) {
            }
        }
    }

    public final void binderDied() {
        m8908();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8909(BasePendingResult<?> basePendingResult) {
        m8908();
    }
}
