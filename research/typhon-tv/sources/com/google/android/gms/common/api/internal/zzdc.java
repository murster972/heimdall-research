package com.google.android.gms.common.api.internal;

final class zzdc implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f7685;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzdb f7686;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LifecycleCallback f7687;

    zzdc(zzdb zzdb, LifecycleCallback lifecycleCallback, String str) {
        this.f7686 = zzdb;
        this.f7687 = lifecycleCallback;
        this.f7685 = str;
    }

    public final void run() {
        if (this.f7686.f7684 > 0) {
            this.f7687.m8562(this.f7686.f7683 != null ? this.f7686.f7683.getBundle(this.f7685) : null);
        }
        if (this.f7686.f7684 >= 2) {
            this.f7687.m8556();
        }
        if (this.f7686.f7684 >= 3) {
            this.f7687.m8559();
        }
        if (this.f7686.f7684 >= 4) {
            this.f7687.m8558();
        }
        if (this.f7686.f7684 >= 5) {
            this.f7687.m8555();
        }
    }
}
