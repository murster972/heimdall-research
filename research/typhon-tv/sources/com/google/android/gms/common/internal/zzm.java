package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;

public final class zzm implements zzj {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzd f7899;

    public zzm(zzd zzd) {
        this.f7899 = zzd;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9208(ConnectionResult connectionResult) {
        if (connectionResult.m8478()) {
            this.f7899.m9186((zzan) null, this.f7899.m9191());
        } else if (this.f7899.f7873 != null) {
            this.f7899.f7873.m9198(connectionResult);
        }
    }
}
