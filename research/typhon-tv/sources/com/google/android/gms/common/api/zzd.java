package com.google.android.gms.common.api;

import android.os.Looper;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.zzcz;
import com.google.android.gms.common.api.internal.zzg;
import com.google.android.gms.common.internal.zzbq;

public final class zzd {

    /* renamed from: 靐  reason: contains not printable characters */
    private Looper f7768;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzcz f7769;

    /* renamed from: 龘  reason: contains not printable characters */
    public final GoogleApi.zza m9007() {
        if (this.f7769 == null) {
            this.f7769 = new zzg();
        }
        if (this.f7768 == null) {
            this.f7768 = Looper.getMainLooper();
        }
        return new GoogleApi.zza(this.f7769, this.f7768);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzd m9008(Looper looper) {
        zzbq.m9121(looper, (Object) "Looper must not be null.");
        this.f7768 = looper;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzd m9009(zzcz zzcz) {
        zzbq.m9121(zzcz, (Object) "StatusExceptionMapper must not be null.");
        this.f7769 = zzcz;
        return this;
    }
}
