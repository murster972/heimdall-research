package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public final class zzk extends zzax {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f7895;

    /* renamed from: 龘  reason: contains not printable characters */
    private zzd f7896;

    public zzk(zzd zzd, int i) {
        this.f7896 = zzd;
        this.f7895 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9206(int i, Bundle bundle) {
        Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9207(int i, IBinder iBinder, Bundle bundle) {
        zzbq.m9121(this.f7896, (Object) "onPostInitComplete can be called only once per call to getRemoteService");
        this.f7896.m9183(i, iBinder, bundle, this.f7895);
        this.f7896 = null;
    }
}
