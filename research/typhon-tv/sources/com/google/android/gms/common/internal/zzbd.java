package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzbd extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    IObjectWrapper m9107(IObjectWrapper iObjectWrapper, zzbv zzbv) throws RemoteException;
}
