package com.google.android.gms.common;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class zza implements ServiceConnection {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BlockingQueue<IBinder> f7945 = new LinkedBlockingQueue();

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f7946 = false;

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f7945.add(iBinder);
    }

    public final void onServiceDisconnected(ComponentName componentName) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IBinder m9282(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        zzbq.m9119("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (this.f7946) {
            throw new IllegalStateException("Cannot call get on this connection more than once");
        }
        this.f7946 = true;
        IBinder poll = this.f7945.poll(10000, timeUnit);
        if (poll != null) {
            return poll;
        }
        throw new TimeoutException("Timed out waiting for the service connection");
    }
}
