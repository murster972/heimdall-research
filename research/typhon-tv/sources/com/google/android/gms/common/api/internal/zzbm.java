package com.google.android.gms.common.api.internal;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.util.ArraySet;
import android.util.Log;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzbm implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Object f7599 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static zzbm f7600;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Status f7601 = new Status(4, "The user must be signed in to make this API call.");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Status f7602 = new Status(4, "Sign-out occurred while this API call was in progress.");
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Context f7603;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public final Map<zzh<?>, zzbo<?>> f7604 = new ConcurrentHashMap(5, 0.75f, 1);
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public zzah f7605 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final AtomicInteger f7606 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Handler f7607;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final GoogleApiAvailability f7608;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public int f7609 = -1;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final AtomicInteger f7610 = new AtomicInteger(1);
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public long f7611 = 10000;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public long f7612 = 120000;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public long f7613 = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Set<zzh<?>> f7614 = new ArraySet();

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final Set<zzh<?>> f7615 = new ArraySet();

    private zzbm(Context context, Looper looper, GoogleApiAvailability googleApiAvailability) {
        this.f7603 = context;
        this.f7607 = new Handler(looper, this);
        this.f7608 = googleApiAvailability;
        this.f7607.sendMessage(this.f7607.obtainMessage(6));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final void m8755() {
        for (zzh<?> remove : this.f7615) {
            this.f7604.remove(remove).m8802();
        }
        this.f7615.clear();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m8760() {
        synchronized (f7599) {
            if (f7600 != null) {
                zzbm zzbm = f7600;
                zzbm.f7606.incrementAndGet();
                zzbm.f7607.sendMessageAtFrontOfQueue(zzbm.f7607.obtainMessage(10));
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8761(GoogleApi<?> googleApi) {
        zzh<?> r1 = googleApi.m4176();
        zzbo zzbo = this.f7604.get(r1);
        if (zzbo == null) {
            zzbo = new zzbo(this, googleApi);
            this.f7604.put(r1, zzbo);
        }
        if (zzbo.m8797()) {
            this.f7615.add(r1);
        }
        zzbo.m8795();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbm m8766() {
        zzbm zzbm;
        synchronized (f7599) {
            zzbq.m9121(f7600, (Object) "Must guarantee manager is non-null before using getInstance");
            zzbm = f7600;
        }
        return zzbm;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbm m8767(Context context) {
        zzbm zzbm;
        synchronized (f7599) {
            if (f7600 == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                f7600 = new zzbm(context.getApplicationContext(), handlerThread.getLooper(), GoogleApiAvailability.m8486());
            }
            zzbm = f7600;
        }
        return zzbm;
    }

    public final boolean handleMessage(Message message) {
        zzbo zzbo;
        switch (message.what) {
            case 1:
                this.f7611 = ((Boolean) message.obj).booleanValue() ? 10000 : 300000;
                this.f7607.removeMessages(12);
                for (zzh<?> obtainMessage : this.f7604.keySet()) {
                    this.f7607.sendMessageDelayed(this.f7607.obtainMessage(12, obtainMessage), this.f7611);
                }
                break;
            case 2:
                zzj zzj = (zzj) message.obj;
                Iterator<zzh<?>> it2 = zzj.m8932().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    } else {
                        zzh next = it2.next();
                        zzbo zzbo2 = this.f7604.get(next);
                        if (zzbo2 == null) {
                            zzj.m8933(next, new ConnectionResult(13), (String) null);
                            break;
                        } else if (zzbo2.m8796()) {
                            zzj.m8933(next, ConnectionResult.f7411, zzbo2.m8799().m8516());
                        } else if (zzbo2.m8798() != null) {
                            zzj.m8933(next, zzbo2.m8798(), (String) null);
                        } else {
                            zzbo2.m8807(zzj);
                        }
                    }
                }
            case 3:
                for (zzbo next2 : this.f7604.values()) {
                    next2.m8800();
                    next2.m8795();
                }
                break;
            case 4:
            case 8:
            case 13:
                zzcp zzcp = (zzcp) message.obj;
                zzbo zzbo3 = this.f7604.get(zzcp.f7663.m4176());
                if (zzbo3 == null) {
                    m8761(zzcp.f7663);
                    zzbo3 = this.f7604.get(zzcp.f7663.m4176());
                }
                if (zzbo3.m8797() && this.f7606.get() != zzcp.f7662) {
                    zzcp.f7664.m8566(f7602);
                    zzbo3.m8802();
                    break;
                } else {
                    zzbo3.m8806(zzcp.f7664);
                    break;
                }
                break;
            case 5:
                int i = message.arg1;
                ConnectionResult connectionResult = (ConnectionResult) message.obj;
                Iterator<zzbo<?>> it3 = this.f7604.values().iterator();
                while (true) {
                    if (it3.hasNext()) {
                        zzbo = it3.next();
                        if (zzbo.m8794() == i) {
                        }
                    } else {
                        zzbo = null;
                    }
                }
                if (zzbo == null) {
                    Log.wtf("GoogleApiManager", new StringBuilder(76).append("Could not find API instance ").append(i).append(" while trying to fail enqueued calls.").toString(), new Exception());
                    break;
                } else {
                    String r5 = this.f7608.m4225(connectionResult.m8480());
                    String r0 = connectionResult.m8477();
                    zzbo.m8805(new Status(17, new StringBuilder(String.valueOf(r5).length() + 69 + String.valueOf(r0).length()).append("Error resolution was canceled by the user, original error message: ").append(r5).append(": ").append(r0).toString()));
                    break;
                }
            case 6:
                if (this.f7603.getApplicationContext() instanceof Application) {
                    zzk.m8936((Application) this.f7603.getApplicationContext());
                    zzk.m8935().m8937((zzl) new zzbn(this));
                    if (!zzk.m8935().m8938(true)) {
                        this.f7611 = 300000;
                        break;
                    }
                }
                break;
            case 7:
                m8761((GoogleApi<?>) (GoogleApi) message.obj);
                break;
            case 9:
                if (this.f7604.containsKey(message.obj)) {
                    this.f7604.get(message.obj).m8790();
                    break;
                }
                break;
            case 10:
                m8755();
                break;
            case 11:
                if (this.f7604.containsKey(message.obj)) {
                    this.f7604.get(message.obj).m8791();
                    break;
                }
                break;
            case 12:
                if (this.f7604.containsKey(message.obj)) {
                    this.f7604.get(message.obj).m8792();
                    break;
                }
                break;
            default:
                Log.w("GoogleApiManager", new StringBuilder(31).append("Unknown message id: ").append(message.what).toString());
                return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m8768() {
        this.f7606.incrementAndGet();
        this.f7607.sendMessage(this.f7607.obtainMessage(10));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8769(ConnectionResult connectionResult, int i) {
        if (!m8779(connectionResult, i)) {
            this.f7607.sendMessage(this.f7607.obtainMessage(5, i, 0, connectionResult));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8770(zzah zzah) {
        synchronized (f7599) {
            if (this.f7605 == zzah) {
                this.f7605 = null;
                this.f7614.clear();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8771() {
        this.f7607.sendMessage(this.f7607.obtainMessage(3));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m8772() {
        return this.f7610.getAndIncrement();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final PendingIntent m8773(zzh<?> zzh, int i) {
        zzbo zzbo = this.f7604.get(zzh);
        if (zzbo == null) {
            return null;
        }
        zzcxd r0 = zzbo.m8793();
        if (r0 == null) {
            return null;
        }
        return PendingIntent.getActivity(this.f7603, i, r0.m8520(), 134217728);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Task<Map<zzh<?>, String>> m8774(Iterable<? extends GoogleApi<?>> iterable) {
        zzj zzj = new zzj(iterable);
        for (GoogleApi googleApi : iterable) {
            zzbo zzbo = this.f7604.get(googleApi.m4176());
            if (zzbo == null || !zzbo.m8796()) {
                this.f7607.sendMessage(this.f7607.obtainMessage(2, zzj));
                return zzj.m8931();
            }
            zzj.m8933(googleApi.m4176(), ConnectionResult.f7411, zzbo.m8799().m8516());
        }
        return zzj.m8931();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8775(GoogleApi<?> googleApi) {
        this.f7607.sendMessage(this.f7607.obtainMessage(7, googleApi));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <O extends Api.ApiOptions, TResult> void m8776(GoogleApi<O> googleApi, int i, zzdd<Api.zzb, TResult> zzdd, TaskCompletionSource<TResult> taskCompletionSource, zzcz zzcz) {
        this.f7607.sendMessage(this.f7607.obtainMessage(4, new zzcp(new zze(i, zzdd, taskCompletionSource, zzcz), this.f7606.get(), googleApi)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <O extends Api.ApiOptions> void m8777(GoogleApi<O> googleApi, int i, zzm<? extends Result, Api.zzb> zzm) {
        this.f7607.sendMessage(this.f7607.obtainMessage(4, new zzcp(new zzc(i, zzm), this.f7606.get(), googleApi)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8778(zzah zzah) {
        synchronized (f7599) {
            if (this.f7605 != zzah) {
                this.f7605 = zzah;
                this.f7614.clear();
                this.f7614.addAll(zzah.m8623());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8779(ConnectionResult connectionResult, int i) {
        return this.f7608.m8502(this.f7603, connectionResult, i);
    }
}
