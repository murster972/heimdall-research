package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.internal.zzbq;

public final class zzci<L> {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile L f7655;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzck<L> f7656;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzcj f7657;

    zzci(Looper looper, L l, String str) {
        this.f7657 = new zzcj(this, looper);
        this.f7655 = zzbq.m9121(l, (Object) "Listener must not be null");
        this.f7656 = new zzck<>(l, zzbq.m9122(str));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzck<L> m8852() {
        return this.f7656;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8853(zzcl<? super L> zzcl) {
        L l = this.f7655;
        if (l == null) {
            zzcl.m8856();
            return;
        }
        try {
            zzcl.m8857(l);
        } catch (RuntimeException e) {
            zzcl.m8856();
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8854() {
        this.f7655 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8855(zzcl<? super L> zzcl) {
        zzbq.m9121(zzcl, (Object) "Notifier must not be null");
        this.f7657.sendMessage(this.f7657.obtainMessage(1, zzcl));
    }
}
