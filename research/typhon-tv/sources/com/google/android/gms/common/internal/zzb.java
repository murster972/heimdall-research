package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;

public final class zzb {
    /* renamed from: 龘  reason: contains not printable characters */
    public static ApiException m9103(Status status) {
        return status.m8550() ? new ResolvableApiException(status) : new ApiException(status);
    }
}
