package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzag implements OnCompleteListener<TResult> {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzae f7497;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ TaskCompletionSource f7498;

    zzag(zzae zzae, TaskCompletionSource taskCompletionSource) {
        this.f7497 = zzae;
        this.f7498 = taskCompletionSource;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8619(Task<TResult> task) {
        this.f7497.f7493.remove(this.f7498);
    }
}
