package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzba;
import com.google.android.gms.common.api.internal.zzce;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.api.internal.zzcu;
import com.google.android.gms.common.api.internal.zzdg;
import com.google.android.gms.common.api.internal.zzi;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.internal.zzt;
import com.google.android.gms.internal.zzcxa;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GoogleApiClient {
    public static final int SIGN_IN_MODE_OPTIONAL = 2;
    public static final int SIGN_IN_MODE_REQUIRED = 1;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Set<GoogleApiClient> f7435 = Collections.newSetFromMap(new WeakHashMap());

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f7436;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f7437;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final Map<Api<?>, zzt> f7438;

        /* renamed from: ʾ  reason: contains not printable characters */
        private OnConnectionFailedListener f7439;

        /* renamed from: ʿ  reason: contains not printable characters */
        private Looper f7440;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f7441;

        /* renamed from: ˊ  reason: contains not printable characters */
        private final ArrayList<ConnectionCallbacks> f7442;

        /* renamed from: ˋ  reason: contains not printable characters */
        private final ArrayList<OnConnectionFailedListener> f7443;

        /* renamed from: ˎ  reason: contains not printable characters */
        private boolean f7444;

        /* renamed from: ˑ  reason: contains not printable characters */
        private final Context f7445;

        /* renamed from: ٴ  reason: contains not printable characters */
        private final Map<Api<?>, Api.ApiOptions> f7446;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private zzce f7447;

        /* renamed from: 连任  reason: contains not printable characters */
        private View f7448;

        /* renamed from: 靐  reason: contains not printable characters */
        private final Set<Scope> f7449;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f7450;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Set<Scope> f7451;

        /* renamed from: 龘  reason: contains not printable characters */
        private Account f7452;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private GoogleApiAvailability f7453;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private Api.zza<? extends zzcxd, zzcxe> f7454;

        public Builder(Context context) {
            this.f7449 = new HashSet();
            this.f7451 = new HashSet();
            this.f7438 = new ArrayMap();
            this.f7446 = new ArrayMap();
            this.f7441 = -1;
            this.f7453 = GoogleApiAvailability.m8486();
            this.f7454 = zzcxa.f9823;
            this.f7442 = new ArrayList<>();
            this.f7443 = new ArrayList<>();
            this.f7444 = false;
            this.f7445 = context;
            this.f7440 = context.getMainLooper();
            this.f7436 = context.getPackageName();
            this.f7437 = context.getClass().getName();
        }

        public Builder(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            this(context);
            zzbq.m9121(connectionCallbacks, (Object) "Must provide a connected listener");
            this.f7442.add(connectionCallbacks);
            zzbq.m9121(onConnectionFailedListener, (Object) "Must provide a connection failed listener");
            this.f7443.add(onConnectionFailedListener);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private final <O extends Api.ApiOptions> void m8530(Api<O> api, O o, Scope... scopeArr) {
            HashSet hashSet = new HashSet(api.m8507().m8512(o));
            for (Scope add : scopeArr) {
                hashSet.add(add);
            }
            this.f7438.put(api, new zzt(hashSet));
        }

        public final Builder addApi(Api<? extends Api.ApiOptions.NotRequiredOptions> api) {
            zzbq.m9121(api, (Object) "Api must not be null");
            this.f7446.put(api, (Object) null);
            List<Scope> r0 = api.m8507().m8512(null);
            this.f7451.addAll(r0);
            this.f7449.addAll(r0);
            return this;
        }

        public final <O extends Api.ApiOptions.HasOptions> Builder addApi(Api<O> api, O o) {
            zzbq.m9121(api, (Object) "Api must not be null");
            zzbq.m9121(o, (Object) "Null options are not permitted for this Api");
            this.f7446.put(api, o);
            List<Scope> r0 = api.m8507().m8512(o);
            this.f7451.addAll(r0);
            this.f7449.addAll(r0);
            return this;
        }

        public final <O extends Api.ApiOptions.HasOptions> Builder addApiIfAvailable(Api<O> api, O o, Scope... scopeArr) {
            zzbq.m9121(api, (Object) "Api must not be null");
            zzbq.m9121(o, (Object) "Null options are not permitted for this Api");
            this.f7446.put(api, o);
            m8530(api, o, scopeArr);
            return this;
        }

        public final Builder addApiIfAvailable(Api<? extends Api.ApiOptions.NotRequiredOptions> api, Scope... scopeArr) {
            zzbq.m9121(api, (Object) "Api must not be null");
            this.f7446.put(api, (Object) null);
            m8530(api, (Api.ApiOptions) null, scopeArr);
            return this;
        }

        public final Builder addConnectionCallbacks(ConnectionCallbacks connectionCallbacks) {
            zzbq.m9121(connectionCallbacks, (Object) "Listener must not be null");
            this.f7442.add(connectionCallbacks);
            return this;
        }

        public final Builder addOnConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener) {
            zzbq.m9121(onConnectionFailedListener, (Object) "Listener must not be null");
            this.f7443.add(onConnectionFailedListener);
            return this;
        }

        public final Builder addScope(Scope scope) {
            zzbq.m9121(scope, (Object) "Scope must not be null");
            this.f7449.add(scope);
            return this;
        }

        public final GoogleApiClient build() {
            zzbq.m9117(!this.f7446.isEmpty(), "must call addApi() to add at least one API");
            zzr zzagu = zzagu();
            Api api = null;
            Map<Api<?>, zzt> r11 = zzagu.m4206();
            ArrayMap arrayMap = new ArrayMap();
            ArrayMap arrayMap2 = new ArrayMap();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (Api next : this.f7446.keySet()) {
                Api.ApiOptions apiOptions = this.f7446.get(next);
                boolean z2 = r11.get(next) != null;
                arrayMap.put(next, Boolean.valueOf(z2));
                com.google.android.gms.common.api.internal.zzt zzt = new com.google.android.gms.common.api.internal.zzt(next, z2);
                arrayList.add(zzt);
                Api.zza r1 = next.m8504();
                Api.zze r2 = r1.m8510(this.f7445, this.f7440, zzagu, apiOptions, zzt, zzt);
                arrayMap2.put(next.m8506(), r2);
                boolean z3 = r1.m8511() == 1 ? apiOptions != null : z;
                if (!r2.m8521()) {
                    next = api;
                } else if (api != null) {
                    String r22 = next.m8505();
                    String r3 = api.m8505();
                    throw new IllegalStateException(new StringBuilder(String.valueOf(r22).length() + 21 + String.valueOf(r3).length()).append(r22).append(" cannot be used with ").append(r3).toString());
                }
                z = z3;
                api = next;
            }
            if (api != null) {
                if (z) {
                    String r23 = api.m8505();
                    throw new IllegalStateException(new StringBuilder(String.valueOf(r23).length() + 82).append("With using ").append(r23).append(", GamesOptions can only be specified within GoogleSignInOptions.Builder").toString());
                }
                zzbq.m9127(this.f7452 == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", api.m8505());
                zzbq.m9127(this.f7449.equals(this.f7451), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", api.m8505());
            }
            zzba zzba = new zzba(this.f7445, new ReentrantLock(), this.f7440, zzagu, this.f7453, this.f7454, arrayMap, this.f7442, this.f7443, arrayMap2, this.f7441, zzba.m8704((Iterable<Api.zze>) arrayMap2.values(), true), arrayList, false);
            synchronized (GoogleApiClient.f7435) {
                GoogleApiClient.f7435.add(zzba);
            }
            if (this.f7441 >= 0) {
                zzi.m8923(this.f7447).m8928(this.f7441, zzba, this.f7439);
            }
            return zzba;
        }

        public final Builder enableAutoManage(FragmentActivity fragmentActivity, int i, OnConnectionFailedListener onConnectionFailedListener) {
            zzce zzce = new zzce(fragmentActivity);
            zzbq.m9117(i >= 0, "clientId must be non-negative");
            this.f7441 = i;
            this.f7439 = onConnectionFailedListener;
            this.f7447 = zzce;
            return this;
        }

        public final Builder enableAutoManage(FragmentActivity fragmentActivity, OnConnectionFailedListener onConnectionFailedListener) {
            return enableAutoManage(fragmentActivity, 0, onConnectionFailedListener);
        }

        public final Builder setAccountName(String str) {
            this.f7452 = str == null ? null : new Account(str, "com.google");
            return this;
        }

        public final Builder setGravityForPopups(int i) {
            this.f7450 = i;
            return this;
        }

        public final Builder setHandler(Handler handler) {
            zzbq.m9121(handler, (Object) "Handler must not be null");
            this.f7440 = handler.getLooper();
            return this;
        }

        public final Builder setViewForPopups(View view) {
            zzbq.m9121(view, (Object) "View must not be null");
            this.f7448 = view;
            return this;
        }

        public final Builder useDefaultAccount() {
            return setAccountName("<<default account>>");
        }

        public final zzr zzagu() {
            zzcxe zzcxe = zzcxe.f9824;
            if (this.f7446.containsKey(zzcxa.f9820)) {
                zzcxe = (zzcxe) this.f7446.get(zzcxa.f9820);
            }
            return new zzr(this.f7452, this.f7449, this.f7438, this.f7450, this.f7448, this.f7436, this.f7437, zzcxe);
        }
    }

    public interface ConnectionCallbacks {
        public static final int CAUSE_NETWORK_LOST = 2;
        public static final int CAUSE_SERVICE_DISCONNECTED = 1;

        void onConnected(Bundle bundle);

        void onConnectionSuspended(int i);
    }

    public interface OnConnectionFailedListener {
        void onConnectionFailed(ConnectionResult connectionResult);
    }

    public static void dumpAll(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        synchronized (f7435) {
            String concat = String.valueOf(str).concat("  ");
            int i = 0;
            for (GoogleApiClient dump : f7435) {
                printWriter.append(str).append("GoogleApiClient#").println(i);
                dump.dump(concat, fileDescriptor, printWriter, strArr);
                i++;
            }
        }
    }

    public static Set<GoogleApiClient> zzagr() {
        Set<GoogleApiClient> set;
        synchronized (f7435) {
            set = f7435;
        }
        return set;
    }

    public abstract ConnectionResult blockingConnect();

    public abstract ConnectionResult blockingConnect(long j, TimeUnit timeUnit);

    public abstract PendingResult<Status> clearDefaultAccountAndReconnect();

    public abstract void connect();

    public void connect(int i) {
        throw new UnsupportedOperationException();
    }

    public abstract void disconnect();

    public abstract void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract ConnectionResult getConnectionResult(Api<?> api);

    public Context getContext() {
        throw new UnsupportedOperationException();
    }

    public Looper getLooper() {
        throw new UnsupportedOperationException();
    }

    public abstract boolean hasConnectedApi(Api<?> api);

    public abstract boolean isConnected();

    public abstract boolean isConnecting();

    public abstract boolean isConnectionCallbacksRegistered(ConnectionCallbacks connectionCallbacks);

    public abstract boolean isConnectionFailedListenerRegistered(OnConnectionFailedListener onConnectionFailedListener);

    public abstract void reconnect();

    public abstract void registerConnectionCallbacks(ConnectionCallbacks connectionCallbacks);

    public abstract void registerConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener);

    public abstract void stopAutoManage(FragmentActivity fragmentActivity);

    public abstract void unregisterConnectionCallbacks(ConnectionCallbacks connectionCallbacks);

    public abstract void unregisterConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener);

    public <C extends Api.zze> C zza(Api.zzc<C> zzc) {
        throw new UnsupportedOperationException();
    }

    public void zza(zzdg zzdg) {
        throw new UnsupportedOperationException();
    }

    public boolean zza(Api<?> api) {
        throw new UnsupportedOperationException();
    }

    public boolean zza(zzcu zzcu) {
        throw new UnsupportedOperationException();
    }

    public void zzags() {
        throw new UnsupportedOperationException();
    }

    public void zzb(zzdg zzdg) {
        throw new UnsupportedOperationException();
    }

    public <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(T t) {
        throw new UnsupportedOperationException();
    }

    public <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(T t) {
        throw new UnsupportedOperationException();
    }

    public <L> zzci<L> zzt(L l) {
        throw new UnsupportedOperationException();
    }
}
