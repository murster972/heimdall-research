package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import com.google.android.gms.common.internal.zzbq;

public class SupportErrorDialogFragment extends DialogFragment {

    /* renamed from: 靐  reason: contains not printable characters */
    private DialogInterface.OnCancelListener f7424 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Dialog f7425 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static SupportErrorDialogFragment m8503(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        SupportErrorDialogFragment supportErrorDialogFragment = new SupportErrorDialogFragment();
        Dialog dialog2 = (Dialog) zzbq.m9121(dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener((DialogInterface.OnCancelListener) null);
        dialog2.setOnDismissListener((DialogInterface.OnDismissListener) null);
        supportErrorDialogFragment.f7425 = dialog2;
        if (onCancelListener != null) {
            supportErrorDialogFragment.f7424 = onCancelListener;
        }
        return supportErrorDialogFragment;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.f7424 != null) {
            this.f7424.onCancel(dialogInterface);
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f7425 == null) {
            setShowsDialog(false);
        }
        return this.f7425;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
