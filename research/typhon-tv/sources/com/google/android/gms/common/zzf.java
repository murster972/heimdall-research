package com.google.android.gms.common;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzak;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.internal.zzbhf;

public class zzf {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final int f3690 = zzp.GOOGLE_PLAY_SERVICES_VERSION_CODE;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzf f3691 = new zzf();

    zzf() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static int m4218(Context context) {
        return zzp.zzcf(context);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzf m4219() {
        return f3691;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m4220(Context context, int i) {
        return zzp.zze(context, i);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static void m4221(Context context) {
        zzp.zzce(context);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m4222(Context context) throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException {
        zzp.zzbp(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Intent m4223(Context context, int i, String str) {
        switch (i) {
            case 1:
            case 2:
                return (context == null || !zzi.m9254(context)) ? zzak.m9087("com.google.android.gms", m4224(context, str)) : zzak.m9085();
            case 3:
                return zzak.m9086("com.google.android.gms");
            default:
                return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m4224(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(f3690);
        sb.append("-");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(zzbhf.m10231(context).m10222(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m4225(int i) {
        return zzp.getErrorString(i);
    }

    @Deprecated
    /* renamed from: 齉  reason: contains not printable characters */
    public final Intent m4226(int i) {
        return m4223((Context) null, i, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m4227(Context context) {
        int isGooglePlayServicesAvailable = zzp.isGooglePlayServicesAvailable(context);
        if (zzp.zze(context, isGooglePlayServicesAvailable)) {
            return 18;
        }
        return isGooglePlayServicesAvailable;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingIntent m4228(Context context, int i, int i2) {
        return m4229(context, i, i2, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final PendingIntent m4229(Context context, int i, int i2, String str) {
        Intent r0 = m4223(context, i, str);
        if (r0 == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, r0, 268435456);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m4230(int i) {
        return zzp.isUserRecoverableError(i);
    }
}
