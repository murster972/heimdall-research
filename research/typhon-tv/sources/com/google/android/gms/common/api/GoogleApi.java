package com.google.android.gms.common.api;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.internal.zzah;
import com.google.android.gms.common.api.internal.zzbm;
import com.google.android.gms.common.api.internal.zzbo;
import com.google.android.gms.common.api.internal.zzbw;
import com.google.android.gms.common.api.internal.zzcv;
import com.google.android.gms.common.api.internal.zzcz;
import com.google.android.gms.common.api.internal.zzdd;
import com.google.android.gms.common.api.internal.zzg;
import com.google.android.gms.common.api.internal.zzh;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzs;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collection;
import java.util.Collections;

public class GoogleApi<O extends Api.ApiOptions> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Looper f3645;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f3646;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final GoogleApiClient f3647;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzcz f3648;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzh<O> f3649;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f3650;

    /* renamed from: 麤  reason: contains not printable characters */
    private final O f3651;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Api<O> f3652;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzbm f3653;

    public static class zza {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final zza f7431 = new zzd().m9007();

        /* renamed from: 靐  reason: contains not printable characters */
        public final zzcz f7432;

        /* renamed from: 齉  reason: contains not printable characters */
        public final Looper f7433;

        private zza(zzcz zzcz, Account account, Looper looper) {
            this.f7432 = zzcz;
            this.f7433 = looper;
        }
    }

    public GoogleApi(Activity activity, Api<O> api, O o, zza zza2) {
        zzbq.m9121(activity, (Object) "Null activity is not permitted.");
        zzbq.m9121(api, (Object) "Api must not be null.");
        zzbq.m9121(zza2, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.f3650 = activity.getApplicationContext();
        this.f3652 = api;
        this.f3651 = o;
        this.f3645 = zza2.f7433;
        this.f3649 = zzh.m8920(this.f3652, this.f3651);
        this.f3647 = new zzbw(this);
        this.f3653 = zzbm.m8767(this.f3650);
        this.f3646 = this.f3653.m8772();
        this.f3648 = zza2.f7432;
        zzah.m8621(activity, this.f3653, this.f3649);
        this.f3653.m8775((GoogleApi<?>) this);
    }

    @Deprecated
    public GoogleApi(Activity activity, Api<O> api, O o, zzcz zzcz) {
        this(activity, api, o, new zzd().m9009(zzcz).m9008(activity.getMainLooper()).m9007());
    }

    protected GoogleApi(Context context, Api<O> api, Looper looper) {
        zzbq.m9121(context, (Object) "Null context is not permitted.");
        zzbq.m9121(api, (Object) "Api must not be null.");
        zzbq.m9121(looper, (Object) "Looper must not be null.");
        this.f3650 = context.getApplicationContext();
        this.f3652 = api;
        this.f3651 = null;
        this.f3645 = looper;
        this.f3649 = zzh.m8919(api);
        this.f3647 = new zzbw(this);
        this.f3653 = zzbm.m8767(this.f3650);
        this.f3646 = this.f3653.m8772();
        this.f3648 = new zzg();
    }

    public GoogleApi(Context context, Api<O> api, O o, zza zza2) {
        zzbq.m9121(context, (Object) "Null context is not permitted.");
        zzbq.m9121(api, (Object) "Api must not be null.");
        zzbq.m9121(zza2, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.f3650 = context.getApplicationContext();
        this.f3652 = api;
        this.f3651 = o;
        this.f3645 = zza2.f7433;
        this.f3649 = zzh.m8920(this.f3652, this.f3651);
        this.f3647 = new zzbw(this);
        this.f3653 = zzbm.m8767(this.f3650);
        this.f3646 = this.f3653.m8772();
        this.f3648 = zza2.f7432;
        this.f3653.m8775((GoogleApi<?>) this);
    }

    @Deprecated
    public GoogleApi(Context context, Api<O> api, O o, zzcz zzcz) {
        this(context, api, o, new zzd().m9009(zzcz).m9007());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private final zzs m4171() {
        GoogleSignInAccount r0;
        GoogleSignInAccount r02;
        return new zzs().m9216((!(this.f3651 instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) || (r02 = ((Api.ApiOptions.HasGoogleSignInAccountOptions) this.f3651).m8509()) == null) ? this.f3651 instanceof Api.ApiOptions.HasAccountOptions ? ((Api.ApiOptions.HasAccountOptions) this.f3651).m8508() : null : r02.m7686()).m9218((Collection<Scope>) (!(this.f3651 instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) || (r0 = ((Api.ApiOptions.HasGoogleSignInAccountOptions) this.f3651).m8509()) == null) ? Collections.emptySet() : r0.m7683());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m4172(int i, T t) {
        t.m4189();
        this.f3653.m8777(this, i, t);
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final <TResult, A extends Api.zzb> Task<TResult> m4173(int i, zzdd<A, TResult> zzdd) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.f3653.m8776(this, i, zzdd, taskCompletionSource, this.f3648);
        return taskCompletionSource.m13720();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Context m4174() {
        return this.f3650;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Looper m4175() {
        return this.f3645;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzh<O> m4176() {
        return this.f3649;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m4177(T t) {
        return m4172(1, t);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final GoogleApiClient m4178() {
        return this.f3647;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m4179() {
        return this.f3646;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Api.zze m4180(Looper looper, zzbo<O> zzbo) {
        return this.f3652.m8504().m8510(this.f3650, looper, m4171().m9217(this.f3650.getPackageName()).m9214(this.f3650.getClass().getName()).m9215(), this.f3651, zzbo, zzbo);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Api<O> m4181() {
        return this.f3652;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public zzcv m4182(Context context, Handler handler) {
        return new zzcv(context, handler, m4171().m9215());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m4183(T t) {
        return m4172(0, t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <TResult, A extends Api.zzb> Task<TResult> m4184(zzdd<A, TResult> zzdd) {
        return m4173(0, zzdd);
    }
}
