package com.google.android.gms.common.api.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.gms.common.util.zzq;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public final class zzk implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzk f7727 = new zzk();

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f7728 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final AtomicBoolean f7729 = new AtomicBoolean();

    /* renamed from: 麤  reason: contains not printable characters */
    private final ArrayList<zzl> f7730 = new ArrayList<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private final AtomicBoolean f7731 = new AtomicBoolean();

    private zzk() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8934(boolean z) {
        synchronized (f7727) {
            ArrayList arrayList = this.f7730;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                ((zzl) obj).m8939(z);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzk m8935() {
        return f7727;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m8936(Application application) {
        synchronized (f7727) {
            if (!f7727.f7728) {
                application.registerActivityLifecycleCallbacks(f7727);
                application.registerComponentCallbacks(f7727);
                f7727.f7728 = true;
            }
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.f7729.compareAndSet(true, false);
        this.f7731.set(true);
        if (compareAndSet) {
            m8934(false);
        }
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.f7729.compareAndSet(true, false);
        this.f7731.set(true);
        if (compareAndSet) {
            m8934(false);
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public final void onTrimMemory(int i) {
        if (i == 20 && this.f7729.compareAndSet(false, true)) {
            this.f7731.set(true);
            m8934(true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8937(zzl zzl) {
        synchronized (f7727) {
            this.f7730.add(zzl);
        }
    }

    @TargetApi(16)
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8938(boolean z) {
        if (!this.f7731.get()) {
            if (!zzq.m9269()) {
                return true;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.f7731.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.f7729.set(true);
            }
        }
        return this.f7729.get();
    }
}
