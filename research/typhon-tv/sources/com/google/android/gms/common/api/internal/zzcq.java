package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzcq<A extends Api.zzb, L> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzci<L> f7665;

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8862() {
        this.f7665.m8854();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8863(A a, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException;
}
