package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzbt;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.internal.zzt;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import com.google.android.gms.internal.zzcxq;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public final class zzao implements zzbh {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f7506;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7507 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7508;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f7509;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f7510;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final Api.zza<? extends zzcxd, zzcxe> f7511;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f7512;

    /* renamed from: ˉ  reason: contains not printable characters */
    private ArrayList<Future<?>> f7513 = new ArrayList<>();

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f7514;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final zzr f7515;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final Map<Api<?>, Boolean> f7516;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Bundle f7517 = new Bundle();

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Set<Api.zzc> f7518 = new HashSet();
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public zzcxd f7519;

    /* renamed from: 连任  reason: contains not printable characters */
    private ConnectionResult f7520;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Lock f7521;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zzf f7522;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Context f7523;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbi f7524;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public zzan f7525;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f7526;

    public zzao(zzbi zzbi, zzr zzr, Map<Api<?>, Boolean> map, zzf zzf, Api.zza<? extends zzcxd, zzcxe> zza, Lock lock, Context context) {
        this.f7524 = zzbi;
        this.f7515 = zzr;
        this.f7516 = map;
        this.f7522 = zzf;
        this.f7511 = zza;
        this.f7521 = lock;
        this.f7523 = context;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private final void m8641() {
        this.f7524.m8731();
        zzbl.m8749().execute(new zzap(this));
        if (this.f7519 != null) {
            if (this.f7526) {
                this.f7519.m11550(this.f7525, this.f7514);
            }
            m8666(false);
        }
        for (Api.zzc<?> zzc : this.f7524.f7591.keySet()) {
            this.f7524.f7594.get(zzc).m8513();
        }
        this.f7524.f7590.m8837(this.f7517.isEmpty() ? null : this.f7517);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m8643() {
        this.f7509 = false;
        this.f7524.f7592.f7566 = Collections.emptySet();
        for (Api.zzc next : this.f7518) {
            if (!this.f7524.f7591.containsKey(next)) {
                this.f7524.f7591.put(next, new ConnectionResult(17, (PendingIntent) null));
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private final void m8645() {
        ArrayList arrayList = this.f7513;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((Future) obj).cancel(true);
        }
        this.f7513.clear();
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final Set<Scope> m8646() {
        if (this.f7515 == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.f7515.m4213());
        Map<Api<?>, zzt> r2 = this.f7515.m4206();
        for (Api next : r2.keySet()) {
            if (!this.f7524.f7591.containsKey(next.m8506())) {
                hashSet.addAll(r2.get(next).f7909);
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m8650() {
        if (this.f7508 == 0) {
            if (!this.f7509 || this.f7510) {
                ArrayList arrayList = new ArrayList();
                this.f7507 = 1;
                this.f7508 = this.f7524.f7594.size();
                for (Api.zzc next : this.f7524.f7594.keySet()) {
                    if (!this.f7524.f7591.containsKey(next)) {
                        arrayList.add(this.f7524.f7594.get(next));
                    } else if (m8658()) {
                        m8641();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.f7513.add(zzbl.m8749().submit(new zzau(this, arrayList)));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8653(ConnectionResult connectionResult) {
        m8645();
        m8666(!connectionResult.m8481());
        this.f7524.m8741(connectionResult);
        this.f7524.f7590.m8838(connectionResult);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if ((r6.m8481() ? true : r5.f7522.m4226(r6.m8480()) != null) != false) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        if (r3 >= r5.f7506) goto L_0x003f;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m8654(com.google.android.gms.common.ConnectionResult r6, com.google.android.gms.common.api.Api<?> r7, boolean r8) {
        /*
            r5 = this;
            r1 = 0
            r0 = 1
            com.google.android.gms.common.api.Api$zzd r2 = r7.m8507()
            int r3 = r2.m8511()
            if (r8 == 0) goto L_0x0015
            boolean r2 = r6.m8481()
            if (r2 == 0) goto L_0x002f
            r2 = r0
        L_0x0013:
            if (r2 == 0) goto L_0x003f
        L_0x0015:
            com.google.android.gms.common.ConnectionResult r2 = r5.f7520
            if (r2 == 0) goto L_0x001d
            int r2 = r5.f7506
            if (r3 >= r2) goto L_0x003f
        L_0x001d:
            if (r0 == 0) goto L_0x0023
            r5.f7520 = r6
            r5.f7506 = r3
        L_0x0023:
            com.google.android.gms.common.api.internal.zzbi r0 = r5.f7524
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.ConnectionResult> r0 = r0.f7591
            com.google.android.gms.common.api.Api$zzc r1 = r7.m8506()
            r0.put(r1, r6)
            return
        L_0x002f:
            com.google.android.gms.common.zzf r2 = r5.f7522
            int r4 = r6.m8480()
            android.content.Intent r2 = r2.m4226((int) r4)
            if (r2 == 0) goto L_0x003d
            r2 = r0
            goto L_0x0013
        L_0x003d:
            r2 = r1
            goto L_0x0013
        L_0x003f:
            r0 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzao.m8654(com.google.android.gms.common.ConnectionResult, com.google.android.gms.common.api.Api, boolean):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8655(int i) {
        if (this.f7507 == i) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.f7524.f7592.m8710());
        String valueOf = String.valueOf(this);
        Log.w("GoogleApiClientConnecting", new StringBuilder(String.valueOf(valueOf).length() + 23).append("Unexpected callback in ").append(valueOf).toString());
        Log.w("GoogleApiClientConnecting", new StringBuilder(33).append("mRemainingConnections=").append(this.f7508).toString());
        String r1 = m8659(this.f7507);
        String r2 = m8659(i);
        Log.wtf("GoogleApiClientConnecting", new StringBuilder(String.valueOf(r1).length() + 70 + String.valueOf(r2).length()).append("GoogleApiClient connecting is in step ").append(r1).append(" but received callback for step ").append(r2).toString(), new Exception());
        m8653(new ConnectionResult(8, (PendingIntent) null));
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m8658() {
        this.f7508--;
        if (this.f7508 > 0) {
            return false;
        }
        if (this.f7508 < 0) {
            Log.w("GoogleApiClientConnecting", this.f7524.f7592.m8710());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            m8653(new ConnectionResult(8, (PendingIntent) null));
            return false;
        } else if (this.f7520 == null) {
            return true;
        } else {
            this.f7524.f7593 = this.f7506;
            m8653(this.f7520);
            return false;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m8659(int i) {
        switch (i) {
            case 0:
                return "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
            case 1:
                return "STEP_GETTING_REMOTE_SERVICE";
            default:
                return "UNKNOWN";
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8665(zzcxq zzcxq) {
        if (m8655(0)) {
            ConnectionResult r0 = zzcxq.m11578();
            if (r0.m8478()) {
                zzbt r02 = zzcxq.m11577();
                ConnectionResult r1 = r02.m9128();
                if (!r1.m8478()) {
                    String valueOf = String.valueOf(r1);
                    Log.wtf("GoogleApiClientConnecting", new StringBuilder(String.valueOf(valueOf).length() + 48).append("Sign-in succeeded with resolve account failure: ").append(valueOf).toString(), new Exception());
                    m8653(r1);
                    return;
                }
                this.f7510 = true;
                this.f7525 = r02.m9131();
                this.f7526 = r02.m9130();
                this.f7514 = r02.m9129();
                m8650();
            } else if (m8667(r0)) {
                m8643();
                m8650();
            } else {
                m8653(r0);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8666(boolean z) {
        if (this.f7519 != null) {
            if (this.f7519.m8514() && z) {
                this.f7519.m11549();
            }
            this.f7519.m8513();
            this.f7525 = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8667(ConnectionResult connectionResult) {
        return this.f7512 && !connectionResult.m8481();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8669(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8670() {
        m8645();
        m8666(true);
        this.f7524.m8741((ConnectionResult) null);
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8671() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8672(T t) {
        this.f7524.f7592.f7567.add(t);
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8673() {
        this.f7524.f7591.clear();
        this.f7509 = false;
        this.f7520 = null;
        this.f7507 = 0;
        this.f7512 = true;
        this.f7510 = false;
        this.f7526 = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (Api next : this.f7516.keySet()) {
            Api.zze zze = this.f7524.f7594.get(next.m8506());
            boolean z2 = (next.m8507().m8511() == 1) | z;
            boolean booleanValue = this.f7516.get(next).booleanValue();
            if (zze.m8517()) {
                this.f7509 = true;
                if (booleanValue) {
                    this.f7518.add(next.m8506());
                } else {
                    this.f7512 = false;
                }
            }
            hashMap.put(zze, new zzaq(this, next, booleanValue));
            z = z2;
        }
        if (z) {
            this.f7509 = false;
        }
        if (this.f7509) {
            this.f7515.m4217(Integer.valueOf(System.identityHashCode(this.f7524.f7592)));
            zzax zzax = new zzax(this, (zzap) null);
            this.f7519 = (zzcxd) this.f7511.m8510(this.f7523, this.f7524.f7592.getLooper(), this.f7515, this.f7515.m4209(), zzax, zzax);
        }
        this.f7508 = this.f7524.f7594.size();
        this.f7513.add(zzbl.m8749().submit(new zzar(this, hashMap)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8674(int i) {
        m8653(new ConnectionResult(8, (PendingIntent) null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8675(Bundle bundle) {
        if (m8655(1)) {
            if (bundle != null) {
                this.f7517.putAll(bundle);
            }
            if (m8658()) {
                m8641();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8676(ConnectionResult connectionResult, Api<?> api, boolean z) {
        if (m8655(1)) {
            m8654(connectionResult, api, z);
            if (m8658()) {
                m8641();
            }
        }
    }
}
