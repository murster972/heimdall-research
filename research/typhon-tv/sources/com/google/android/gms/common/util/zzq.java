package com.google.android.gms.common.util;

import android.os.Build;

public final class zzq {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m9264() {
        return Build.VERSION.SDK_INT >= 20;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m9265() {
        return Build.VERSION.SDK_INT >= 21;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static boolean m9266() {
        return Build.VERSION.SDK_INT >= 24;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static boolean m9267() {
        return Build.VERSION.SDK_INT >= 26 || "O".equals(Build.VERSION.CODENAME) || Build.VERSION.CODENAME.startsWith("OMR") || Build.VERSION.CODENAME.startsWith("ODR");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m9268() {
        return Build.VERSION.SDK_INT >= 19;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m9269() {
        return Build.VERSION.SDK_INT >= 16;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m9270() {
        return Build.VERSION.SDK_INT >= 18;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m9271() {
        return Build.VERSION.SDK_INT >= 17;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m9272() {
        return Build.VERSION.SDK_INT >= 15;
    }
}
