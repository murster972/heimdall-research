package com.google.android.gms.common.util;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.gms.internal.zzbhf;

public final class zzc {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m9240(Context context, String str) {
        "com.google.android.gms".equals(str);
        try {
            return (zzbhf.m10231(context).m10226(str, 0).flags & 2097152) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
