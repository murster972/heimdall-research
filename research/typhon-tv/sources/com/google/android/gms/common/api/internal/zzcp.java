package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApi;

public final class zzcp {

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f7662;

    /* renamed from: 齉  reason: contains not printable characters */
    public final GoogleApi<?> f7663;

    /* renamed from: 龘  reason: contains not printable characters */
    public final zza f7664;

    public zzcp(zza zza, int i, GoogleApi<?> googleApi) {
        this.f7664 = zza;
        this.f7662 = i;
        this.f7663 = googleApi;
    }
}
