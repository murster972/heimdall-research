package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.Log;
import java.io.File;

public final class zzv {
    @TargetApi(21)
    /* renamed from: 龘  reason: contains not printable characters */
    public static File m9277(Context context) {
        return zzq.m9265() ? context.getNoBackupFilesDir() : m9278(new File(context.getApplicationInfo().dataDir, "no_backup"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized File m9278(File file) {
        synchronized (zzv.class) {
            if (!file.exists() && !file.mkdirs() && !file.exists()) {
                String valueOf = String.valueOf(file.getPath());
                Log.w("SupportV4Utils", valueOf.length() != 0 ? "Unable to create no-backup dir ".concat(valueOf) : new String("Unable to create no-backup dir "));
                file = null;
            }
        }
        return file;
    }
}
