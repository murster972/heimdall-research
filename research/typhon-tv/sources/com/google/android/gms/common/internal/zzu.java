package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.R;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.internal.zzbhf;

public final class zzu {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final SimpleArrayMap<String, String> f7910 = new SimpleArrayMap<>();

    /* renamed from: 连任  reason: contains not printable characters */
    public static String m9219(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                return resources.getString(R.string.common_google_play_services_install_button);
            case 2:
                return resources.getString(R.string.common_google_play_services_update_button);
            case 3:
                return resources.getString(R.string.common_google_play_services_enable_button);
            default:
                return resources.getString(17039370);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m9220(Context context) {
        String packageName = context.getPackageName();
        try {
            return zzbhf.m10231(context).m10223(packageName).toString();
        } catch (PackageManager.NameNotFoundException | NullPointerException e) {
            String str = context.getApplicationInfo().name;
            return !TextUtils.isEmpty(str) ? str : packageName;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m9221(Context context, int i) {
        String r0 = i == 6 ? m9226(context, "common_google_play_services_resolution_required_title") : m9225(context, i);
        return r0 == null ? context.getResources().getString(R.string.common_google_play_services_notification_ticker) : r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m9222(Context context, int i) {
        return i == 6 ? m9227(context, "common_google_play_services_resolution_required_text", m9220(context)) : m9223(context, i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m9223(Context context, int i) {
        Resources resources = context.getResources();
        String r1 = m9220(context);
        switch (i) {
            case 1:
                return resources.getString(R.string.common_google_play_services_install_text, new Object[]{r1});
            case 2:
                if (zzi.m9254(context)) {
                    return resources.getString(R.string.common_google_play_services_wear_update_text);
                }
                return resources.getString(R.string.common_google_play_services_update_text, new Object[]{r1});
            case 3:
                return resources.getString(R.string.common_google_play_services_enable_text, new Object[]{r1});
            case 5:
                return m9227(context, "common_google_play_services_invalid_account_text", r1);
            case 7:
                return m9227(context, "common_google_play_services_network_error_text", r1);
            case 9:
                return resources.getString(R.string.common_google_play_services_unsupported_text, new Object[]{r1});
            case 16:
                return m9227(context, "common_google_play_services_api_unavailable_text", r1);
            case 17:
                return m9227(context, "common_google_play_services_sign_in_failed_text", r1);
            case 18:
                return resources.getString(R.string.common_google_play_services_updating_text, new Object[]{r1});
            case 20:
                return m9227(context, "common_google_play_services_restricted_profile_text", r1);
            default:
                return resources.getString(R.string.common_google_play_services_unknown_issue, new Object[]{r1});
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9224(Context context) {
        return context.getResources().getString(R.string.common_google_play_services_notification_channel_name);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9225(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                return resources.getString(R.string.common_google_play_services_install_title);
            case 2:
                return resources.getString(R.string.common_google_play_services_update_title);
            case 3:
                return resources.getString(R.string.common_google_play_services_enable_title);
            case 4:
            case 6:
            case 18:
                return null;
            case 5:
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                return m9226(context, "common_google_play_services_invalid_account_title");
            case 7:
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                return m9226(context, "common_google_play_services_network_error_title");
            case 8:
                Log.e("GoogleApiAvailability", "Internal error occurred. Please see logs for detailed information");
                return null;
            case 9:
                Log.e("GoogleApiAvailability", "Google Play services is invalid. Cannot recover.");
                return null;
            case 10:
                Log.e("GoogleApiAvailability", "Developer error occurred. Please see logs for detailed information");
                return null;
            case 11:
                Log.e("GoogleApiAvailability", "The application is not licensed to the user.");
                return null;
            case 16:
                Log.e("GoogleApiAvailability", "One of the API components you attempted to connect to is not available.");
                return null;
            case 17:
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                return m9226(context, "common_google_play_services_sign_in_failed_title");
            case 20:
                Log.e("GoogleApiAvailability", "The current user profile is restricted and could not use authenticated features.");
                return m9226(context, "common_google_play_services_restricted_profile_title");
            default:
                Log.e("GoogleApiAvailability", new StringBuilder(33).append("Unexpected error code ").append(i).toString());
                return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m9226(Context context, String str) {
        synchronized (f7910) {
            String str2 = f7910.get(str);
            if (str2 != null) {
                return str2;
            }
            Resources remoteResource = GooglePlayServicesUtil.getRemoteResource(context);
            if (remoteResource == null) {
                return null;
            }
            int identifier = remoteResource.getIdentifier(str, "string", "com.google.android.gms");
            if (identifier == 0) {
                String valueOf = String.valueOf(str);
                Log.w("GoogleApiAvailability", valueOf.length() != 0 ? "Missing resource: ".concat(valueOf) : new String("Missing resource: "));
                return null;
            }
            String string = remoteResource.getString(identifier);
            if (TextUtils.isEmpty(string)) {
                String valueOf2 = String.valueOf(str);
                Log.w("GoogleApiAvailability", valueOf2.length() != 0 ? "Got empty resource: ".concat(valueOf2) : new String("Got empty resource: "));
                return null;
            }
            f7910.put(str, string);
            return string;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m9227(Context context, String str, String str2) {
        Resources resources = context.getResources();
        String r0 = m9226(context, str);
        if (r0 == null) {
            r0 = resources.getString(R.string.common_google_play_services_unknown_issue);
        }
        return String.format(resources.getConfiguration().locale, r0, new Object[]{str2});
    }
}
