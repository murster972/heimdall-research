package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.internal.zzbq;
import java.lang.ref.WeakReference;

public final class zzdg<R extends Result> extends TransformedResult<R> implements ResultCallback<R> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Status f7688;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final WeakReference<GoogleApiClient> f7689;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final zzdi f7690;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f7691;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final Object f7692;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public zzdg<? extends Result> f7693;

    /* renamed from: 麤  reason: contains not printable characters */
    private PendingResult<R> f7694;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile ResultCallbacks<? super R> f7695;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public ResultTransform<? super R, ? extends Result> f7696;

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8892() {
        if (this.f7696 != null || this.f7695 != null) {
            GoogleApiClient googleApiClient = (GoogleApiClient) this.f7689.get();
            if (!(this.f7691 || this.f7696 == null || googleApiClient == null)) {
                googleApiClient.zza(this);
                this.f7691 = true;
            }
            if (this.f7688 != null) {
                m8893(this.f7688);
            } else if (this.f7694 != null) {
                this.f7694.m8535(this);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8893(Status status) {
        synchronized (this.f7692) {
            if (this.f7696 != null) {
                Status r0 = this.f7696.m8543(status);
                zzbq.m9121(r0, (Object) "onFailure must not return null");
                this.f7693.m8899(r0);
            } else if (m8896()) {
                this.f7695.m8541(status);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m8896() {
        return (this.f7695 == null || ((GoogleApiClient) this.f7689.get()) == null) ? false : true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m8898(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).m8539();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(result);
                Log.w("TransformedResultImpl", new StringBuilder(String.valueOf(valueOf).length() + 18).append("Unable to release ").append(valueOf).toString(), e);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8899(Status status) {
        synchronized (this.f7692) {
            this.f7688 = status;
            m8893(this.f7688);
        }
    }

    public final void onResult(R r) {
        synchronized (this.f7692) {
            if (!r.s_().m8549()) {
                m8899(r.s_());
                m8898((Result) r);
            } else if (this.f7696 != null) {
                zzcs.m8864().submit(new zzdh(this, r));
            } else if (m8896()) {
                this.f7695.m8540(r);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8902() {
        this.f7695 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8903(PendingResult<?> pendingResult) {
        synchronized (this.f7692) {
            this.f7694 = pendingResult;
            m8892();
        }
    }
}
