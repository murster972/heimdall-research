package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.util.Arrays;

public final class ConnectionResult extends zzbfm {
    public static final Parcelable.Creator<ConnectionResult> CREATOR = new zzb();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ConnectionResult f7411 = new ConnectionResult(0);

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f7412;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7413;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PendingIntent f7414;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7415;

    public ConnectionResult(int i) {
        this(i, (PendingIntent) null, (String) null);
    }

    ConnectionResult(int i, int i2, PendingIntent pendingIntent, String str) {
        this.f7413 = i;
        this.f7415 = i2;
        this.f7414 = pendingIntent;
        this.f7412 = str;
    }

    public ConnectionResult(int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, (String) null);
    }

    public ConnectionResult(int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m8476(int i) {
        switch (i) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            case 13:
                return "CANCELED";
            case 14:
                return "TIMEOUT";
            case 15:
                return "INTERRUPTED";
            case 16:
                return "API_UNAVAILABLE";
            case 17:
                return "SIGN_IN_FAILED";
            case 18:
                return "SERVICE_UPDATING";
            case 19:
                return "SERVICE_MISSING_PERMISSION";
            case 20:
                return "RESTRICTED_PROFILE";
            case 21:
                return "API_VERSION_UPDATE_REQUIRED";
            case 99:
                return "UNFINISHED";
            case 1500:
                return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
            default:
                return new StringBuilder(31).append("UNKNOWN_ERROR_CODE(").append(i).append(")").toString();
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ConnectionResult)) {
            return false;
        }
        ConnectionResult connectionResult = (ConnectionResult) obj;
        return this.f7415 == connectionResult.f7415 && zzbg.m9113(this.f7414, connectionResult.f7414) && zzbg.m9113(this.f7412, connectionResult.f7412);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f7415), this.f7414, this.f7412});
    }

    public final String toString() {
        return zzbg.m9112(this).m9114("statusCode", m8476(this.f7415)).m9114("resolution", this.f7414).m9114("message", this.f7412).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f7413);
        zzbfp.m10185(parcel, 2, m8480());
        zzbfp.m10189(parcel, 3, (Parcelable) m8479(), i, false);
        zzbfp.m10193(parcel, 4, m8477(), false);
        zzbfp.m10182(parcel, r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m8477() {
        return this.f7412;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8478() {
        return this.f7415 == 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final PendingIntent m8479() {
        return this.f7414;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m8480() {
        return this.f7415;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8481() {
        return (this.f7415 == 0 || this.f7414 == null) ? false : true;
    }
}
