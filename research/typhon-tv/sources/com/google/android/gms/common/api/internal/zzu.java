package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

public interface zzu extends GoogleApiClient.ConnectionCallbacks {
    /* renamed from: 龘  reason: contains not printable characters */
    void m8963(ConnectionResult connectionResult, Api<?> api, boolean z);
}
