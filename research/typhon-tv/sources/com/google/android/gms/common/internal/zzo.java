package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

public final class zzo extends zze {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzd f7902;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzo(zzd zzd, int i, Bundle bundle) {
        super(zzd, i, (Bundle) null);
        this.f7902 = zzd;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9211(ConnectionResult connectionResult) {
        this.f7902.f7882.m9205(connectionResult);
        this.f7902.m9185(connectionResult);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9212() {
        this.f7902.f7882.m9205(ConnectionResult.f7411);
        return true;
    }
}
