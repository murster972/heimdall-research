package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.support.v4.os.EnvironmentCompat;
import android.util.Log;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.stats.zza;
import java.util.HashMap;

final class zzai extends zzag implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final long f7819;

    /* renamed from: 连任  reason: contains not printable characters */
    private final long f7820;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Context f7821;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final zza f7822;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Handler f7823;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final HashMap<zzah, zzaj> f7824 = new HashMap<>();

    zzai(Context context) {
        this.f7821 = context.getApplicationContext();
        this.f7823 = new Handler(context.getMainLooper(), this);
        this.f7822 = zza.m9235();
        this.f7820 = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
        this.f7819 = 300000;
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                synchronized (this.f7824) {
                    zzah zzah = (zzah) message.obj;
                    zzaj zzaj = this.f7824.get(zzah);
                    if (zzaj != null && zzaj.m9080()) {
                        if (zzaj.m9083()) {
                            zzaj.m9078("GmsClientSupervisor");
                        }
                        this.f7824.remove(zzah);
                    }
                }
                return true;
            case 1:
                synchronized (this.f7824) {
                    zzah zzah2 = (zzah) message.obj;
                    zzaj zzaj2 = this.f7824.get(zzah2);
                    if (zzaj2 != null && zzaj2.m9076() == 3) {
                        String valueOf = String.valueOf(zzah2);
                        Log.wtf("GmsClientSupervisor", new StringBuilder(String.valueOf(valueOf).length() + 47).append("Timeout waiting for ServiceConnection callback ").append(valueOf).toString(), new Exception());
                        ComponentName r3 = zzaj2.m9075();
                        if (r3 == null) {
                            r3 = zzah2.m9064();
                        }
                        zzaj2.onServiceDisconnected(r3 == null ? new ComponentName(zzah2.m9067(), EnvironmentCompat.MEDIA_UNKNOWN) : r3);
                    }
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9073(zzah zzah, ServiceConnection serviceConnection, String str) {
        zzbq.m9121(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.f7824) {
            zzaj zzaj = this.f7824.get(zzah);
            if (zzaj == null) {
                String valueOf = String.valueOf(zzah);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 50).append("Nonexistent connection status for service config: ").append(valueOf).toString());
            } else if (!zzaj.m9084(serviceConnection)) {
                String valueOf2 = String.valueOf(zzah);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf2).length() + 76).append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=").append(valueOf2).toString());
            } else {
                zzaj.m9077(serviceConnection, str);
                if (zzaj.m9080()) {
                    this.f7823.sendMessageDelayed(this.f7823.obtainMessage(0, zzah), this.f7820);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9074(zzah zzah, ServiceConnection serviceConnection, String str) {
        boolean r0;
        zzbq.m9121(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.f7824) {
            zzaj zzaj = this.f7824.get(zzah);
            if (zzaj != null) {
                this.f7823.removeMessages(0, zzah);
                if (!zzaj.m9084(serviceConnection)) {
                    zzaj.m9081(serviceConnection, str);
                    switch (zzaj.m9076()) {
                        case 1:
                            serviceConnection.onServiceConnected(zzaj.m9075(), zzaj.m9079());
                            break;
                        case 2:
                            zzaj.m9082(str);
                            break;
                    }
                } else {
                    String valueOf = String.valueOf(zzah);
                    throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 81).append("Trying to bind a GmsServiceConnection that was already connected before.  config=").append(valueOf).toString());
                }
            } else {
                zzaj = new zzaj(this, zzah);
                zzaj.m9081(serviceConnection, str);
                zzaj.m9082(str);
                this.f7824.put(zzah, zzaj);
            }
            r0 = zzaj.m9083();
        }
        return r0;
    }
}
