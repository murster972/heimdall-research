package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.zzeu;

public final class zzav extends zzeu implements zzat {
    zzav(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9097() throws RemoteException {
        Parcel r0 = m12300(2, v_());
        int readInt = r0.readInt();
        r0.recycle();
        return readInt;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final IObjectWrapper m9098() throws RemoteException {
        Parcel r0 = m12300(1, v_());
        IObjectWrapper r1 = IObjectWrapper.zza.m9305(r0.readStrongBinder());
        r0.recycle();
        return r1;
    }
}
