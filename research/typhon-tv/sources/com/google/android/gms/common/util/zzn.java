package com.google.android.gms.common.util;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class zzn {
    /* renamed from: 龘  reason: contains not printable characters */
    public static long m9259(InputStream inputStream, OutputStream outputStream, boolean z) throws IOException {
        return m9260(inputStream, outputStream, z, 1024);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m9260(InputStream inputStream, OutputStream outputStream, boolean z, int i) throws IOException {
        byte[] bArr = new byte[i];
        long j = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, bArr.length);
                if (read == -1) {
                    break;
                }
                j += (long) read;
                outputStream.write(bArr, 0, read);
            } finally {
                if (z) {
                    m9261(inputStream);
                    m9261(outputStream);
                }
            }
        }
        return j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m9261(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m9262(InputStream inputStream, boolean z) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        m9259(inputStream, byteArrayOutputStream, z);
        return byteArrayOutputStream.toByteArray();
    }
}
