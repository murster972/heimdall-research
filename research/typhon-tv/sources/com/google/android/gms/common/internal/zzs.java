package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.support.v4.util.ArraySet;
import android.view.View;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzcxe;
import java.util.Collection;
import java.util.Map;

public final class zzs {

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzcxe f7903 = zzcxe.f9824;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f7904;

    /* renamed from: 靐  reason: contains not printable characters */
    private ArraySet<Scope> f7905;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f7906;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f7907 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private Account f7908;

    /* renamed from: 靐  reason: contains not printable characters */
    public final zzs m9214(String str) {
        this.f7904 = str;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzr m9215() {
        return new zzr(this.f7908, this.f7905, (Map<Api<?>, zzt>) null, 0, (View) null, this.f7906, this.f7904, this.f7903);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzs m9216(Account account) {
        this.f7908 = account;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzs m9217(String str) {
        this.f7906 = str;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzs m9218(Collection<Scope> collection) {
        if (this.f7905 == null) {
            this.f7905 = new ArraySet<>();
        }
        this.f7905.addAll(collection);
        return this;
    }
}
