package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzbv extends zzbfm {
    public static final Parcelable.Creator<zzbv> CREATOR = new zzbw();

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f3676;
    @Deprecated

    /* renamed from: 麤  reason: contains not printable characters */
    private final Scope[] f3677;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f3678;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f3679;

    zzbv(int i, int i2, int i3, Scope[] scopeArr) {
        this.f3679 = i;
        this.f3676 = i2;
        this.f3678 = i3;
        this.f3677 = scopeArr;
    }

    public zzbv(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, (Scope[]) null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f3679);
        zzbfp.m10185(parcel, 2, this.f3676);
        zzbfp.m10185(parcel, 3, this.f3678);
        zzbfp.m10199(parcel, 4, (T[]) this.f3677, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
