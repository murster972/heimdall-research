package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import com.google.android.gms.internal.zzbfn;

public final class zzaa implements Parcelable.Creator<zzz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r11 = zzbfn.m10169(parcel);
        zzc[] zzcArr = null;
        Account account = null;
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        String str = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < r11) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i3 = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    i2 = zzbfn.m10166(parcel, readInt);
                    break;
                case 3:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 4:
                    str = zzbfn.m10162(parcel, readInt);
                    break;
                case 5:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                case 6:
                    scopeArr = (Scope[]) zzbfn.m10165(parcel, readInt, Scope.CREATOR);
                    break;
                case 7:
                    bundle = zzbfn.m10153(parcel, readInt);
                    break;
                case 8:
                    account = (Account) zzbfn.m10171(parcel, readInt, Account.CREATOR);
                    break;
                case 10:
                    zzcArr = (zzc[]) zzbfn.m10165(parcel, readInt, zzc.CREATOR);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r11);
        return new zzz(i3, i2, i, str, iBinder, scopeArr, bundle, account, zzcArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzz[i];
    }
}
