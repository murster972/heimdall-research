package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzj;
import java.util.Set;

final class zzbu implements zzcy, zzj {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f7635 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private Set<Scope> f7636 = null;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Api.zze f7637;

    /* renamed from: 麤  reason: contains not printable characters */
    private zzan f7638 = null;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzh<?> f7639;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzbm f7640;

    public zzbu(zzbm zzbm, Api.zze zze, zzh<?> zzh) {
        this.f7640 = zzbm;
        this.f7637 = zze;
        this.f7639 = zzh;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8812() {
        if (this.f7635 && this.f7638 != null) {
            this.f7637.m8522(this.f7638, this.f7636);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8814(ConnectionResult connectionResult) {
        ((zzbo) this.f7640.f7604.get(this.f7639)).m8803(connectionResult);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8815(ConnectionResult connectionResult) {
        this.f7640.f7607.post(new zzbv(this, connectionResult));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8816(zzan zzan, Set<Scope> set) {
        if (zzan == null || set == null) {
            Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
            m8814(new ConnectionResult(4));
            return;
        }
        this.f7638 = zzan;
        this.f7636 = set;
        m8812();
    }
}
