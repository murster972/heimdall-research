package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.google.android.gms.common.api.internal.zzcf;

public abstract class zzv implements DialogInterface.OnClickListener {
    /* renamed from: 龘  reason: contains not printable characters */
    public static zzv m9228(Activity activity, Intent intent, int i) {
        return new zzw(intent, activity, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzv m9229(Fragment fragment, Intent intent, int i) {
        return new zzx(intent, fragment, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzv m9230(zzcf zzcf, Intent intent, int i) {
        return new zzy(intent, zzcf, 2);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            m9231();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m9231();
}
