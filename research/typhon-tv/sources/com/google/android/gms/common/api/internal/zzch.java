package com.google.android.gms.common.api.internal;

final class zzch implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ String f7652;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ zzcg f7653;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ LifecycleCallback f7654;

    zzch(zzcg zzcg, LifecycleCallback lifecycleCallback, String str) {
        this.f7653 = zzcg;
        this.f7654 = lifecycleCallback;
        this.f7652 = str;
    }

    public final void run() {
        if (this.f7653.f7651 > 0) {
            this.f7654.m8562(this.f7653.f7650 != null ? this.f7653.f7650.getBundle(this.f7652) : null);
        }
        if (this.f7653.f7651 >= 2) {
            this.f7654.m8556();
        }
        if (this.f7653.f7651 >= 3) {
            this.f7654.m8559();
        }
        if (this.f7653.f7651 >= 4) {
            this.f7654.m8558();
        }
        if (this.f7653.f7651 >= 5) {
            this.f7654.m8555();
        }
    }
}
