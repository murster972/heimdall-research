package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import com.google.android.gms.common.zzf;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.lang3.StringUtils;

public abstract class zzd<T extends IInterface> {

    /* renamed from: ᵔ  reason: contains not printable characters */
    private static String[] f7861 = {"service_esmobile", "service_googleme"};

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f7862;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7863;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f7864;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final zzf f7865;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Object f7866;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f7867;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final zzag f7868;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public final zzf f7869;

    /* renamed from: ˊ  reason: contains not printable characters */
    private T f7870;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final ArrayList<zzi<?>> f7871;

    /* renamed from: ˎ  reason: contains not printable characters */
    private zzl f7872;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public final zzg f7873;

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzam f7874;

    /* renamed from: י  reason: contains not printable characters */
    private final int f7875;

    /* renamed from: ـ  reason: contains not printable characters */
    private final String f7876;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Context f7877;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Looper f7878;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public ConnectionResult f7879;
    /* access modifiers changed from: private */

    /* renamed from: ᵎ  reason: contains not printable characters */
    public boolean f7880;

    /* renamed from: 连任  reason: contains not printable characters */
    private long f7881;

    /* renamed from: 靐  reason: contains not printable characters */
    protected zzj f7882;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f7883;

    /* renamed from: 齉  reason: contains not printable characters */
    protected AtomicInteger f7884;

    /* renamed from: 龘  reason: contains not printable characters */
    final Handler f7885;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Object f7886;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public zzay f7887;

    protected zzd(Context context, Looper looper, int i, zzf zzf, zzg zzg, String str) {
        this(context, looper, zzag.m9060(context), zzf.m4219(), i, (zzf) zzbq.m9120(zzf), (zzg) zzbq.m9120(zzg), (String) null);
    }

    protected zzd(Context context, Looper looper, zzag zzag, zzf zzf, int i, zzf zzf2, zzg zzg, String str) {
        this.f7866 = new Object();
        this.f7886 = new Object();
        this.f7871 = new ArrayList<>();
        this.f7867 = 1;
        this.f7879 = null;
        this.f7880 = false;
        this.f7884 = new AtomicInteger(0);
        this.f7877 = (Context) zzbq.m9121(context, (Object) "Context must not be null");
        this.f7878 = (Looper) zzbq.m9121(looper, (Object) "Looper must not be null");
        this.f7868 = (zzag) zzbq.m9121(zzag, (Object) "Supervisor must not be null");
        this.f7865 = (zzf) zzbq.m9121(zzf, (Object) "API availability must not be null");
        this.f7885 = new zzh(this, looper);
        this.f7875 = i;
        this.f7869 = zzf2;
        this.f7873 = zzg;
        this.f7876 = str;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private final boolean m9144() {
        boolean z;
        synchronized (this.f7866) {
            z = this.f7867 == 3;
        }
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public final boolean m9145() {
        if (this.f7880 || TextUtils.isEmpty(m9175()) || TextUtils.isEmpty((CharSequence) null)) {
            return false;
        }
        try {
            Class.forName(m9175());
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final String m9147() {
        return this.f7876 == null ? this.f7877.getClass().getName() : this.f7876;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9150(int i) {
        int i2;
        if (m9144()) {
            i2 = 5;
            this.f7880 = true;
        } else {
            i2 = 4;
        }
        this.f7885.sendMessage(this.f7885.obtainMessage(i2, this.f7884.get(), 16));
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9155(int i, T t) {
        boolean z = true;
        if ((i == 4) != (t != null)) {
            z = false;
        }
        zzbq.m9116(z);
        synchronized (this.f7866) {
            this.f7867 = i;
            this.f7870 = t;
            switch (i) {
                case 1:
                    if (this.f7872 != null) {
                        this.f7868.m9062(r_(), m9166(), 129, this.f7872, m9147());
                        this.f7872 = null;
                        break;
                    }
                    break;
                case 2:
                case 3:
                    if (!(this.f7872 == null || this.f7874 == null)) {
                        String r1 = this.f7874.m9090();
                        String r2 = this.f7874.m9088();
                        Log.e("GmsClient", new StringBuilder(String.valueOf(r1).length() + 70 + String.valueOf(r2).length()).append("Calling connect() while still connected, missing disconnect() for ").append(r1).append(" on ").append(r2).toString());
                        this.f7868.m9062(this.f7874.m9090(), this.f7874.m9088(), this.f7874.m9089(), this.f7872, m9147());
                        this.f7884.incrementAndGet();
                    }
                    this.f7872 = new zzl(this, this.f7884.get());
                    this.f7874 = new zzam(m9166(), r_(), false, 129);
                    if (!this.f7868.m9063(new zzah(this.f7874.m9090(), this.f7874.m9088(), this.f7874.m9089()), this.f7872, m9147())) {
                        String r12 = this.f7874.m9090();
                        String r22 = this.f7874.m9088();
                        Log.e("GmsClient", new StringBuilder(String.valueOf(r12).length() + 34 + String.valueOf(r22).length()).append("unable to connect to service: ").append(r12).append(" on ").append(r22).toString());
                        m9182(16, (Bundle) null, this.f7884.get());
                        break;
                    }
                    break;
                case 4:
                    m9184(t);
                    break;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9158(int i, int i2, T t) {
        boolean z;
        synchronized (this.f7866) {
            if (this.f7867 != i) {
                z = false;
            } else {
                m9155(i2, t);
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public abstract String r_();

    public Account u_() {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m9160() {
        this.f7884.incrementAndGet();
        synchronized (this.f7871) {
            int size = this.f7871.size();
            for (int i = 0; i < size; i++) {
                this.f7871.get(i).m9202();
            }
            this.f7871.clear();
        }
        synchronized (this.f7886) {
            this.f7887 = null;
        }
        m9155(1, (IInterface) null);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m9161() {
        boolean z;
        synchronized (this.f7866) {
            z = this.f7867 == 4;
        }
        return z;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m9162() {
        boolean z;
        synchronized (this.f7866) {
            z = this.f7867 == 2 || this.f7867 == 3;
        }
        return z;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final Looper m9163() {
        return this.f7878;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m9164() {
        if (m9161() && this.f7874 != null) {
            return this.f7874.m9088();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public Bundle m9165() {
        return new Bundle();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˊ  reason: contains not printable characters */
    public String m9166() {
        return "com.google.android.gms";
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m9167() {
        int r0 = this.f7865.m4227(this.f7877);
        if (r0 != 0) {
            m9155(1, (IInterface) null);
            m9188((zzj) new zzm(this), r0, (PendingIntent) null);
            return;
        }
        m9187((zzj) new zzm(this));
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final Context m9168() {
        return this.f7877;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m9169() {
        if (!m9161()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m9170() {
        return false;
    }

    /* renamed from: י  reason: contains not printable characters */
    public final T m9171() throws DeadObjectException {
        T t;
        synchronized (this.f7866) {
            if (this.f7867 == 5) {
                throw new DeadObjectException();
            }
            m9169();
            zzbq.m9126(this.f7870 != null, (Object) "Client is connected but service is null");
            t = this.f7870;
        }
        return t;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean m9172() {
        return false;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m9173() {
        return true;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final IBinder m9174() {
        IBinder asBinder;
        synchronized (this.f7886) {
            asBinder = this.f7887 == null ? null : this.f7887.asBinder();
        }
        return asBinder;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract String m9175();

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9176(int i) {
        this.f7885.sendMessage(this.f7885.obtainMessage(6, this.f7884.get(), i));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Intent m9177() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m9178() {
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bundle m9179() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m9180(IBinder iBinder);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m9181(int i) {
        this.f7883 = i;
        this.f7881 = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9182(int i, Bundle bundle, int i2) {
        this.f7885.sendMessage(this.f7885.obtainMessage(7, i2, -1, new zzo(this, i, (Bundle) null)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m9183(int i, IBinder iBinder, Bundle bundle, int i2) {
        this.f7885.sendMessage(this.f7885.obtainMessage(1, i2, -1, new zzn(this, i, iBinder, bundle)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m9184(T t) {
        this.f7862 = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m9185(ConnectionResult connectionResult) {
        this.f7863 = connectionResult.m8480();
        this.f7864 = System.currentTimeMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9186(zzan zzan, Set<Scope> set) {
        Bundle r0 = m9165();
        zzz zzz = new zzz(this.f7875);
        zzz.f7928 = this.f7877.getPackageName();
        zzz.f7926 = r0;
        if (set != null) {
            zzz.f7927 = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (m9170()) {
            zzz.f7924 = u_() != null ? u_() : new Account("<<default account>>", "com.google");
            if (zzan != null) {
                zzz.f7925 = zzan.asBinder();
            }
        } else if (m9172()) {
            zzz.f7924 = u_();
        }
        zzz.f7920 = m9192();
        try {
            synchronized (this.f7886) {
                if (this.f7887 != null) {
                    this.f7887.m9101(new zzk(this, this.f7884.get()), zzz);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e);
            m9176(1);
        } catch (SecurityException e2) {
            throw e2;
        } catch (RemoteException | RuntimeException e3) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e3);
            m9183(8, (IBinder) null, (Bundle) null, this.f7884.get());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9187(zzj zzj) {
        this.f7882 = (zzj) zzbq.m9121(zzj, (Object) "Connection progress callbacks cannot be null.");
        m9155(2, (IInterface) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9188(zzj zzj, int i, PendingIntent pendingIntent) {
        this.f7882 = (zzj) zzbq.m9121(zzj, (Object) "Connection progress callbacks cannot be null.");
        this.f7885.sendMessage(this.f7885.obtainMessage(3, this.f7884.get(), i, pendingIntent));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9189(zzp zzp) {
        zzp.m9213();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9190(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i;
        T t;
        zzay zzay;
        synchronized (this.f7866) {
            i = this.f7867;
            t = this.f7870;
        }
        synchronized (this.f7886) {
            zzay = this.f7887;
        }
        printWriter.append(str).append("mConnectState=");
        switch (i) {
            case 1:
                printWriter.print("DISCONNECTED");
                break;
            case 2:
                printWriter.print("REMOTE_CONNECTING");
                break;
            case 3:
                printWriter.print("LOCAL_CONNECTING");
                break;
            case 4:
                printWriter.print("CONNECTED");
                break;
            case 5:
                printWriter.print("DISCONNECTING");
                break;
            default:
                printWriter.print("UNKNOWN");
                break;
        }
        printWriter.append(" mService=");
        if (t == null) {
            printWriter.append("null");
        } else {
            printWriter.append(m9175()).append("@").append(Integer.toHexString(System.identityHashCode(t.asBinder())));
        }
        printWriter.append(" mServiceBroker=");
        if (zzay == null) {
            printWriter.println("null");
        } else {
            printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(zzay.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.f7862 > 0) {
            PrintWriter append = printWriter.append(str).append("lastConnectedTime=");
            long j = this.f7862;
            String format = simpleDateFormat.format(new Date(this.f7862));
            append.println(new StringBuilder(String.valueOf(format).length() + 21).append(j).append(StringUtils.SPACE).append(format).toString());
        }
        if (this.f7881 > 0) {
            printWriter.append(str).append("lastSuspendedCause=");
            switch (this.f7883) {
                case 1:
                    printWriter.append("CAUSE_SERVICE_DISCONNECTED");
                    break;
                case 2:
                    printWriter.append("CAUSE_NETWORK_LOST");
                    break;
                default:
                    printWriter.append(String.valueOf(this.f7883));
                    break;
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j2 = this.f7881;
            String format2 = simpleDateFormat.format(new Date(this.f7881));
            append2.println(new StringBuilder(String.valueOf(format2).length() + 21).append(j2).append(StringUtils.SPACE).append(format2).toString());
        }
        if (this.f7864 > 0) {
            printWriter.append(str).append("lastFailedStatus=").append(CommonStatusCodes.m8526(this.f7863));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j3 = this.f7864;
            String format3 = simpleDateFormat.format(new Date(this.f7864));
            append3.println(new StringBuilder(String.valueOf(format3).length() + 21).append(j3).append(StringUtils.SPACE).append(format3).toString());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public Set<Scope> m9191() {
        return Collections.EMPTY_SET;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public zzc[] m9192() {
        return new zzc[0];
    }
}
