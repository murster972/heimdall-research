package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class zzx implements zzcd {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzv f7762;

    private zzx(zzv zzv) {
        this.f7762 = zzv;
    }

    /* synthetic */ zzx(zzv zzv, zzw zzw) {
        this(zzv);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8998(int i, boolean z) {
        this.f7762.f7750.lock();
        try {
            if (this.f7762.f7752 || this.f7762.f7755 == null || !this.f7762.f7755.m8478()) {
                boolean unused = this.f7762.f7752 = false;
                this.f7762.m8979(i, z);
                return;
            }
            boolean unused2 = this.f7762.f7752 = true;
            this.f7762.f7756.onConnectionSuspended(i);
            this.f7762.f7750.unlock();
        } finally {
            this.f7762.f7750.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8999(Bundle bundle) {
        this.f7762.f7750.lock();
        try {
            this.f7762.m8980(bundle);
            ConnectionResult unused = this.f7762.f7754 = ConnectionResult.f7411;
            this.f7762.m8965();
        } finally {
            this.f7762.f7750.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9000(ConnectionResult connectionResult) {
        this.f7762.f7750.lock();
        try {
            ConnectionResult unused = this.f7762.f7754 = connectionResult;
            this.f7762.m8965();
        } finally {
            this.f7762.f7750.unlock();
        }
    }
}
