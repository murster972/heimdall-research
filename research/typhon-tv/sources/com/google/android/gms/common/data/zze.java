package com.google.android.gms.common.data;

import android.content.ContentValues;
import com.google.android.gms.common.data.DataHolder;
import java.util.HashMap;

final class zze extends DataHolder.zza {
    zze(String[] strArr, String str) {
        super(strArr, (String) null, (zze) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final DataHolder.zza m9031(ContentValues contentValues) {
        throw new UnsupportedOperationException("Cannot add data to empty builder");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final DataHolder.zza m9032(HashMap<String, Object> hashMap) {
        throw new UnsupportedOperationException("Cannot add data to empty builder");
    }
}
