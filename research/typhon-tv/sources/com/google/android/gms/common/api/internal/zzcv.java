package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbt;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.internal.zzcxa;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import com.google.android.gms.internal.zzcxi;
import com.google.android.gms.internal.zzcxq;
import java.util.Set;

public final class zzcv extends zzcxi implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private static Api.zza<? extends zzcxd, zzcxe> f7670 = zzcxa.f9823;

    /* renamed from: ʻ  reason: contains not printable characters */
    private zzr f7671;

    /* renamed from: ʼ  reason: contains not printable characters */
    private zzcxd f7672;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public zzcy f7673;

    /* renamed from: 连任  reason: contains not printable characters */
    private Set<Scope> f7674;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f7675;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Api.zza<? extends zzcxd, zzcxe> f7676;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Handler f7677;

    public zzcv(Context context, Handler handler, zzr zzr) {
        this(context, handler, zzr, f7670);
    }

    public zzcv(Context context, Handler handler, zzr zzr, Api.zza<? extends zzcxd, zzcxe> zza) {
        this.f7675 = context;
        this.f7677 = handler;
        this.f7671 = (zzr) zzbq.m9121(zzr, (Object) "ClientSettings must not be null");
        this.f7674 = zzr.m4213();
        this.f7676 = zza;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8872(zzcxq zzcxq) {
        ConnectionResult r0 = zzcxq.m11578();
        if (r0.m8478()) {
            zzbt r02 = zzcxq.m11577();
            ConnectionResult r1 = r02.m9128();
            if (!r1.m8478()) {
                String valueOf = String.valueOf(r1);
                Log.wtf("SignInCoordinator", new StringBuilder(String.valueOf(valueOf).length() + 48).append("Sign-in succeeded with resolve account failure: ").append(valueOf).toString(), new Exception());
                this.f7673.m8879(r1);
                this.f7672.m8513();
                return;
            }
            this.f7673.m8880(r02.m9131(), this.f7674);
        } else {
            this.f7673.m8879(r0);
        }
        this.f7672.m8513();
    }

    public final void onConnected(Bundle bundle) {
        this.f7672.m11551(this);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.f7673.m8879(connectionResult);
    }

    public final void onConnectionSuspended(int i) {
        this.f7672.m8513();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8875() {
        if (this.f7672 != null) {
            this.f7672.m8513();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzcxd m8876() {
        return this.f7672;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8877(zzcy zzcy) {
        if (this.f7672 != null) {
            this.f7672.m8513();
        }
        this.f7671.m4217(Integer.valueOf(System.identityHashCode(this)));
        this.f7672 = (zzcxd) this.f7676.m8510(this.f7675, this.f7677.getLooper(), this.f7671, this.f7671.m4209(), this, this);
        this.f7673 = zzcy;
        if (this.f7674 == null || this.f7674.isEmpty()) {
            this.f7677.post(new zzcw(this));
        } else {
            this.f7672.m11548();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8878(zzcxq zzcxq) {
        this.f7677.post(new zzcx(this, zzcxq));
    }
}
