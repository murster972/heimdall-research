package com.google.android.gms.common.data;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.zzbfq;

public class zzd<T extends zzbfq> extends AbstractDataBuffer<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f7792 = {"data"};

    /* renamed from: 齉  reason: contains not printable characters */
    private final Parcelable.Creator<T> f7793;

    public zzd(DataHolder dataHolder, Parcelable.Creator<T> creator) {
        super(dataHolder);
        this.f7793 = creator;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static DataHolder.zza m9027() {
        return DataHolder.m9015(f7792);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends zzbfq> void m9028(DataHolder.zza zza, T t) {
        Parcel obtain = Parcel.obtain();
        t.writeToParcel(obtain, 0);
        ContentValues contentValues = new ContentValues();
        contentValues.put("data", obtain.marshall());
        zza.m9024(contentValues);
        obtain.recycle();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public T m9030(int i) {
        byte[] r0 = this.f7772.m9021("data", i, this.f7772.m9019(i));
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(r0, 0, r0.length);
        obtain.setDataPosition(0);
        T t = (zzbfq) this.f7793.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }
}
