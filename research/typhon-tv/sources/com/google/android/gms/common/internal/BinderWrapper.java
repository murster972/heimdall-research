package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public final class BinderWrapper implements Parcelable {
    public static final Parcelable.Creator<BinderWrapper> CREATOR = new zzq();

    /* renamed from: 龘  reason: contains not printable characters */
    private IBinder f7798;

    public BinderWrapper() {
        this.f7798 = null;
    }

    public BinderWrapper(IBinder iBinder) {
        this.f7798 = null;
        this.f7798 = iBinder;
    }

    private BinderWrapper(Parcel parcel) {
        this.f7798 = null;
        this.f7798 = parcel.readStrongBinder();
    }

    /* synthetic */ BinderWrapper(Parcel parcel, zzq zzq) {
        this(parcel);
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeStrongBinder(this.f7798);
    }
}
