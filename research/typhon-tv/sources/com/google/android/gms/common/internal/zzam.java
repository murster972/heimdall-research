package com.google.android.gms.common.internal;

public final class zzam {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7838;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f7839 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f7840 = 129;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f7841;

    public zzam(String str, String str2, boolean z, int i) {
        this.f7838 = str;
        this.f7841 = str2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m9088() {
        return this.f7838;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final int m9089() {
        return this.f7840;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m9090() {
        return this.f7841;
    }
}
