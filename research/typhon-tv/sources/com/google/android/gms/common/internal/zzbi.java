package com.google.android.gms.common.internal;

import java.util.ArrayList;
import java.util.List;

public final class zzbi {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f7847;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<String> f7848;

    private zzbi(Object obj) {
        this.f7847 = zzbq.m9120(obj);
        this.f7848 = new ArrayList();
    }

    public final String toString() {
        StringBuilder append = new StringBuilder(100).append(this.f7847.getClass().getSimpleName()).append('{');
        int size = this.f7848.size();
        for (int i = 0; i < size; i++) {
            append.append(this.f7848.get(i));
            if (i < size - 1) {
                append.append(", ");
            }
        }
        return append.append('}').toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzbi m9114(String str, Object obj) {
        List<String> list = this.f7848;
        String str2 = (String) zzbq.m9120(str);
        String valueOf = String.valueOf(obj);
        list.add(new StringBuilder(String.valueOf(str2).length() + 1 + String.valueOf(valueOf).length()).append(str2).append("=").append(valueOf).toString());
        return this;
    }
}
