package com.google.android.gms.common.internal;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

public final class zzak {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Uri f7832;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Uri f7833;

    static {
        Uri parse = Uri.parse("https://plus.google.com/");
        f7833 = parse;
        f7832 = parse.buildUpon().appendPath("circles").appendPath("find").build();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Intent m9085() {
        Intent intent = new Intent("com.google.android.clockwork.home.UPDATE_ANDROID_WEAR_ACTION");
        intent.setPackage("com.google.android.wearable.app");
        return intent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Intent m9086(String str) {
        Uri fromParts = Uri.fromParts("package", str, (String) null);
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(fromParts);
        return intent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Intent m9087(String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri.Builder appendQueryParameter = Uri.parse("market://details").buildUpon().appendQueryParameter("id", str);
        if (!TextUtils.isEmpty(str2)) {
            appendQueryParameter.appendQueryParameter("pcampaignid", str2);
        }
        intent.setData(appendQueryParameter.build());
        intent.setPackage("com.android.vending");
        intent.addFlags(524288);
        return intent;
    }
}
