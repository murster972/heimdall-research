package com.google.android.gms.common.api.internal;

public final class zzck<L> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7659;

    /* renamed from: 龘  reason: contains not printable characters */
    private final L f7660;

    zzck(L l, String str) {
        this.f7660 = l;
        this.f7659 = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzck)) {
            return false;
        }
        zzck zzck = (zzck) obj;
        return this.f7660 == zzck.f7660 && this.f7659.equals(zzck.f7659);
    }

    public final int hashCode() {
        return (System.identityHashCode(this.f7660) * 31) + this.f7659.hashCode();
    }
}
