package com.google.android.gms.common.stats;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import com.google.android.gms.common.util.zzc;
import java.util.Collections;
import java.util.List;

public final class zza {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile zza f7929;

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean f7930 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f7931 = new Object();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<String> f7932 = Collections.EMPTY_LIST;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<String> f7933 = Collections.EMPTY_LIST;

    /* renamed from: 连任  reason: contains not printable characters */
    private final List<String> f7934 = Collections.EMPTY_LIST;

    /* renamed from: 麤  reason: contains not printable characters */
    private final List<String> f7935 = Collections.EMPTY_LIST;

    private zza() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zza m9235() {
        if (f7929 == null) {
            synchronized (f7931) {
                if (f7929 == null) {
                    f7929 = new zza();
                }
            }
        }
        return f7929;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9236(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        return m9237(context, context.getClass().getName(), intent, serviceConnection, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9237(Context context, String str, Intent intent, ServiceConnection serviceConnection, int i) {
        ComponentName component = intent.getComponent();
        if (!(component == null ? false : zzc.m9240(context, component.getPackageName()))) {
            return context.bindService(intent, serviceConnection, i);
        }
        Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
        return false;
    }
}
