package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.zzq;
import com.google.android.gms.internal.zzbhf;

public final class zzx {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m9280(Context context, int i) {
        if (!m9281(context, i, "com.google.android.gms")) {
            return false;
        }
        try {
            return zzq.m9300(context).m9304(context.getPackageManager().getPackageInfo("com.google.android.gms", 64));
        } catch (PackageManager.NameNotFoundException e) {
            if (!Log.isLoggable("UidVerifier", 3)) {
                return false;
            }
            Log.d("UidVerifier", "Package manager can't find google play services package, defaulting to false");
            return false;
        }
    }

    @TargetApi(19)
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m9281(Context context, int i, String str) {
        return zzbhf.m10231(context).m10228(i, str);
    }
}
