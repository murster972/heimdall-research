package com.google.android.gms.common.data;

import java.util.Iterator;

public abstract class AbstractDataBuffer<T> implements DataBuffer<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final DataHolder f7772;

    protected AbstractDataBuffer(DataHolder dataHolder) {
        this.f7772 = dataHolder;
    }

    public Iterator<T> iterator() {
        return new zzb(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m9011() {
        if (this.f7772 == null) {
            return 0;
        }
        return this.f7772.f7783;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9012() {
        if (this.f7772 != null) {
            this.f7772.close();
        }
    }
}
