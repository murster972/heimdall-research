package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzae;
import com.google.android.gms.common.internal.zzaf;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzbft;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;

public final class zzba extends GoogleApiClient implements zzcd {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Lock f7545;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f7546;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzae f7547;

    /* renamed from: ʾ  reason: contains not printable characters */
    private volatile boolean f7548;

    /* renamed from: ʿ  reason: contains not printable characters */
    private long f7549 = 120000;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Map<Api<?>, Boolean> f7550;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Looper f7551;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Api.zza<? extends zzcxd, zzcxe> f7552;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final GoogleApiAvailability f7553;

    /* renamed from: ˋ  reason: contains not printable characters */
    private zzbx f7554;

    /* renamed from: ˎ  reason: contains not printable characters */
    private zzr f7555;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final zzcm f7556 = new zzcm();

    /* renamed from: ˑ  reason: contains not printable characters */
    private zzcc f7557 = null;

    /* renamed from: י  reason: contains not printable characters */
    private final ArrayList<zzt> f7558;

    /* renamed from: ـ  reason: contains not printable characters */
    private Integer f7559 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f7560;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final Context f7561;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final zzaf f7562 = new zzbb(this);

    /* renamed from: 连任  reason: contains not printable characters */
    final zzdj f7563;

    /* renamed from: 靐  reason: contains not printable characters */
    final Map<Api.zzc<?>, Api.zze> f7564;

    /* renamed from: 麤  reason: contains not printable characters */
    Set<zzdg> f7565 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    Set<Scope> f7566 = new HashSet();

    /* renamed from: 龘  reason: contains not printable characters */
    final Queue<zzm<?, ?>> f7567 = new LinkedList();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private long f7568 = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final zzbf f7569;

    public zzba(Context context, Lock lock, Looper looper, zzr zzr, GoogleApiAvailability googleApiAvailability, Api.zza<? extends zzcxd, zzcxe> zza, Map<Api<?>, Boolean> map, List<GoogleApiClient.ConnectionCallbacks> list, List<GoogleApiClient.OnConnectionFailedListener> list2, Map<Api.zzc<?>, Api.zze> map2, int i, int i2, ArrayList<zzt> arrayList, boolean z) {
        this.f7561 = context;
        this.f7545 = lock;
        this.f7546 = false;
        this.f7547 = new zzae(looper, this.f7562);
        this.f7551 = looper;
        this.f7569 = new zzbf(this, looper);
        this.f7553 = googleApiAvailability;
        this.f7560 = i;
        if (this.f7560 >= 0) {
            this.f7559 = Integer.valueOf(i2);
        }
        this.f7550 = map;
        this.f7564 = map2;
        this.f7558 = arrayList;
        this.f7563 = new zzdj(this.f7564);
        for (GoogleApiClient.ConnectionCallbacks r2 : list) {
            this.f7547.m9056(r2);
        }
        for (GoogleApiClient.OnConnectionFailedListener r22 : list2) {
            this.f7547.m9057(r22);
        }
        this.f7555 = zzr;
        this.f7552 = zza;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8698() {
        this.f7545.lock();
        try {
            if (this.f7548) {
                m8700();
            }
        } finally {
            this.f7545.unlock();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m8699() {
        this.f7545.lock();
        try {
            if (m8709()) {
                m8700();
            }
        } finally {
            this.f7545.unlock();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private final void m8700() {
        this.f7547.m9047();
        this.f7557.m8833();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m8701(int i) {
        switch (i) {
            case 1:
                return "SIGN_IN_MODE_REQUIRED";
            case 2:
                return "SIGN_IN_MODE_OPTIONAL";
            case 3:
                return "SIGN_IN_MODE_NONE";
            default:
                return "UNKNOWN";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m8704(Iterable<Api.zze> iterable, boolean z) {
        boolean z2 = false;
        boolean z3 = false;
        for (Api.zze next : iterable) {
            if (next.m8517()) {
                z3 = true;
            }
            z2 = next.m8521() ? true : z2;
        }
        if (z3) {
            return (!z2 || !z) ? 1 : 2;
        }
        return 3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8705(int i) {
        if (this.f7559 == null) {
            this.f7559 = Integer.valueOf(i);
        } else if (this.f7559.intValue() != i) {
            String r1 = m8701(i);
            String r2 = m8701(this.f7559.intValue());
            throw new IllegalStateException(new StringBuilder(String.valueOf(r1).length() + 51 + String.valueOf(r2).length()).append("Cannot use sign-in mode: ").append(r1).append(". Mode was already set to ").append(r2).toString());
        }
        if (this.f7557 == null) {
            boolean z = false;
            boolean z2 = false;
            for (Api.zze next : this.f7564.values()) {
                if (next.m8517()) {
                    z2 = true;
                }
                z = next.m8521() ? true : z;
            }
            switch (this.f7559.intValue()) {
                case 1:
                    if (!z2) {
                        throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
                    } else if (z) {
                        throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
                    }
                    break;
                case 2:
                    if (z2) {
                        if (this.f7546) {
                            this.f7557 = new zzaa(this.f7561, this.f7545, this.f7551, this.f7553, this.f7564, this.f7555, this.f7550, this.f7552, this.f7558, this, true);
                            return;
                        } else {
                            this.f7557 = zzv.m8977(this.f7561, this, this.f7545, this.f7551, this.f7553, this.f7564, this.f7555, this.f7550, this.f7552, this.f7558);
                            return;
                        }
                    }
                    break;
            }
            if (!this.f7546 || z) {
                this.f7557 = new zzbi(this.f7561, this, this.f7545, this.f7551, this.f7553, this.f7564, this.f7555, this.f7550, this.f7552, this.f7558, this);
            } else {
                this.f7557 = new zzaa(this.f7561, this.f7545, this.f7551, this.f7553, this.f7564, this.f7555, this.f7550, this.f7552, this.f7558, this, false);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8706(GoogleApiClient googleApiClient, zzda zzda, boolean z) {
        zzbft.f8739.m10206(googleApiClient).m8535(new zzbe(this, zzda, z, googleApiClient));
    }

    public final ConnectionResult blockingConnect() {
        boolean z = true;
        zzbq.m9126(Looper.myLooper() != Looper.getMainLooper(), (Object) "blockingConnect must not be called on the UI thread");
        this.f7545.lock();
        try {
            if (this.f7560 >= 0) {
                if (this.f7559 == null) {
                    z = false;
                }
                zzbq.m9126(z, (Object) "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.f7559 == null) {
                this.f7559 = Integer.valueOf(m8704((Iterable<Api.zze>) this.f7564.values(), false));
            } else if (this.f7559.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            m8705(this.f7559.intValue());
            this.f7547.m9047();
            return this.f7557.m8826();
        } finally {
            this.f7545.unlock();
        }
    }

    public final ConnectionResult blockingConnect(long j, TimeUnit timeUnit) {
        boolean z = false;
        if (Looper.myLooper() != Looper.getMainLooper()) {
            z = true;
        }
        zzbq.m9126(z, (Object) "blockingConnect must not be called on the UI thread");
        zzbq.m9121(timeUnit, (Object) "TimeUnit must not be null");
        this.f7545.lock();
        try {
            if (this.f7559 == null) {
                this.f7559 = Integer.valueOf(m8704((Iterable<Api.zze>) this.f7564.values(), false));
            } else if (this.f7559.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            m8705(this.f7559.intValue());
            this.f7547.m9047();
            return this.f7557.m8830(j, timeUnit);
        } finally {
            this.f7545.unlock();
        }
    }

    public final PendingResult<Status> clearDefaultAccountAndReconnect() {
        zzbq.m9126(isConnected(), (Object) "GoogleApiClient is not connected yet.");
        zzbq.m9126(this.f7559.intValue() != 2, (Object) "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        zzda zzda = new zzda(this);
        if (this.f7564.containsKey(zzbft.f8740)) {
            m8706(this, zzda, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            GoogleApiClient build = new GoogleApiClient.Builder(this.f7561).addApi(zzbft.f8737).addConnectionCallbacks(new zzbc(this, atomicReference, zzda)).addOnConnectionFailedListener(new zzbd(this, zzda)).setHandler(this.f7569).build();
            atomicReference.set(build);
            build.connect();
        }
        return zzda;
    }

    public final void connect() {
        boolean z = false;
        this.f7545.lock();
        try {
            if (this.f7560 >= 0) {
                if (this.f7559 != null) {
                    z = true;
                }
                zzbq.m9126(z, (Object) "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.f7559 == null) {
                this.f7559 = Integer.valueOf(m8704((Iterable<Api.zze>) this.f7564.values(), false));
            } else if (this.f7559.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            connect(this.f7559.intValue());
        } finally {
            this.f7545.unlock();
        }
    }

    public final void connect(int i) {
        boolean z = true;
        this.f7545.lock();
        if (!(i == 3 || i == 1 || i == 2)) {
            z = false;
        }
        try {
            zzbq.m9117(z, new StringBuilder(33).append("Illegal sign-in mode: ").append(i).toString());
            m8705(i);
            m8700();
        } finally {
            this.f7545.unlock();
        }
    }

    public final void disconnect() {
        this.f7545.lock();
        try {
            this.f7563.m8905();
            if (this.f7557 != null) {
                this.f7557.m8829();
            }
            this.f7556.m8861();
            for (zzm zzm : this.f7567) {
                zzm.m4200((zzdm) null);
                zzm.m8533();
            }
            this.f7567.clear();
            if (this.f7557 != null) {
                m8709();
                this.f7547.m9052();
                this.f7545.unlock();
            }
        } finally {
            this.f7545.unlock();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("mContext=").println(this.f7561);
        printWriter.append(str).append("mResuming=").print(this.f7548);
        printWriter.append(" mWorkQueue.size()=").print(this.f7567.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.f7563.f7703.size());
        if (this.f7557 != null) {
            this.f7557.m8834(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final ConnectionResult getConnectionResult(Api<?> api) {
        this.f7545.lock();
        try {
            if (!isConnected() && !this.f7548) {
                throw new IllegalStateException("Cannot invoke getConnectionResult unless GoogleApiClient is connected");
            } else if (this.f7564.containsKey(api.m8506())) {
                ConnectionResult r0 = this.f7557.m8831(api);
                if (r0 != null) {
                    this.f7545.unlock();
                } else if (this.f7548) {
                    r0 = ConnectionResult.f7411;
                } else {
                    Log.w("GoogleApiClientImpl", m8710());
                    Log.wtf("GoogleApiClientImpl", String.valueOf(api.m8505()).concat(" requested in getConnectionResult is not connected but is not present in the failed  connections map"), new Exception());
                    r0 = new ConnectionResult(8, (PendingIntent) null);
                    this.f7545.unlock();
                }
                return r0;
            } else {
                throw new IllegalArgumentException(String.valueOf(api.m8505()).concat(" was never registered with GoogleApiClient"));
            }
        } finally {
            this.f7545.unlock();
        }
    }

    public final Context getContext() {
        return this.f7561;
    }

    public final Looper getLooper() {
        return this.f7551;
    }

    public final boolean hasConnectedApi(Api<?> api) {
        if (!isConnected()) {
            return false;
        }
        Api.zze zze = this.f7564.get(api.m8506());
        return zze != null && zze.m8514();
    }

    public final boolean isConnected() {
        return this.f7557 != null && this.f7557.m8828();
    }

    public final boolean isConnecting() {
        return this.f7557 != null && this.f7557.m8825();
    }

    public final boolean isConnectionCallbacksRegistered(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        return this.f7547.m9048(connectionCallbacks);
    }

    public final boolean isConnectionFailedListenerRegistered(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return this.f7547.m9049(onConnectionFailedListener);
    }

    public final void reconnect() {
        disconnect();
        connect();
    }

    public final void registerConnectionCallbacks(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.f7547.m9056(connectionCallbacks);
    }

    public final void registerConnectionFailedListener(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.f7547.m9057(onConnectionFailedListener);
    }

    public final void stopAutoManage(FragmentActivity fragmentActivity) {
        zzce zzce = new zzce(fragmentActivity);
        if (this.f7560 >= 0) {
            zzi.m8923(zzce).m8927(this.f7560);
            return;
        }
        throw new IllegalStateException("Called stopAutoManage but automatic lifecycle management is not enabled.");
    }

    public final void unregisterConnectionCallbacks(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.f7547.m9050(connectionCallbacks);
    }

    public final void unregisterConnectionFailedListener(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.f7547.m9051(onConnectionFailedListener);
    }

    public final <C extends Api.zze> C zza(Api.zzc<C> zzc) {
        C c = (Api.zze) this.f7564.get(zzc);
        zzbq.m9121(c, (Object) "Appropriate Api was not requested.");
        return c;
    }

    public final void zza(zzdg zzdg) {
        this.f7545.lock();
        try {
            if (this.f7565 == null) {
                this.f7565 = new HashSet();
            }
            this.f7565.add(zzdg);
        } finally {
            this.f7545.unlock();
        }
    }

    public final boolean zza(Api<?> api) {
        return this.f7564.containsKey(api.m8506());
    }

    public final boolean zza(zzcu zzcu) {
        return this.f7557 != null && this.f7557.m8835(zzcu);
    }

    public final void zzags() {
        if (this.f7557 != null) {
            this.f7557.m8823();
        }
    }

    public final void zzb(zzdg zzdg) {
        this.f7545.lock();
        try {
            if (this.f7565 == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.f7565.remove(zzdg)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!m8711()) {
                this.f7557.m8824();
            }
        } finally {
            this.f7545.unlock();
        }
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(T t) {
        zzbq.m9117(t.m8941() != null, "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.f7564.containsKey(t.m8941());
        String r0 = t.m8942() != null ? t.m8942().m8505() : "the API";
        zzbq.m9117(containsKey, new StringBuilder(String.valueOf(r0).length() + 65).append("GoogleApiClient is not configured to use ").append(r0).append(" required for this call.").toString());
        this.f7545.lock();
        try {
            if (this.f7557 == null) {
                this.f7567.add(t);
            } else {
                t = this.f7557.m8832(t);
                this.f7545.unlock();
            }
            return t;
        } finally {
            this.f7545.unlock();
        }
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(T t) {
        zzbq.m9117(t.m8941() != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.f7564.containsKey(t.m8941());
        String r0 = t.m8942() != null ? t.m8942().m8505() : "the API";
        zzbq.m9117(containsKey, new StringBuilder(String.valueOf(r0).length() + 65).append("GoogleApiClient is not configured to use ").append(r0).append(" required for this call.").toString());
        this.f7545.lock();
        try {
            if (this.f7557 == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            }
            if (this.f7548) {
                this.f7567.add(t);
                while (!this.f7567.isEmpty()) {
                    zzm remove = this.f7567.remove();
                    this.f7563.m8906(remove);
                    remove.m8944(Status.f7463);
                }
            } else {
                t = this.f7557.m8827(t);
                this.f7545.unlock();
            }
            return t;
        } finally {
            this.f7545.unlock();
        }
    }

    public final <L> zzci<L> zzt(L l) {
        this.f7545.lock();
        try {
            return this.f7556.m8860(l, this.f7551, "NO_TYPE");
        } finally {
            this.f7545.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8709() {
        if (!this.f7548) {
            return false;
        }
        this.f7548 = false;
        this.f7569.removeMessages(2);
        this.f7569.removeMessages(1);
        if (this.f7554 != null) {
            this.f7554.m8817();
            this.f7554 = null;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final String m8710() {
        StringWriter stringWriter = new StringWriter();
        dump("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m8711() {
        boolean z = false;
        this.f7545.lock();
        try {
            if (this.f7565 != null) {
                if (!this.f7565.isEmpty()) {
                    z = true;
                }
                this.f7545.unlock();
            }
            return z;
        } finally {
            this.f7545.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8712(int i, boolean z) {
        if (i == 1 && !z && !this.f7548) {
            this.f7548 = true;
            if (this.f7554 == null) {
                this.f7554 = GoogleApiAvailability.m8487(this.f7561.getApplicationContext(), (zzby) new zzbg(this));
            }
            this.f7569.sendMessageDelayed(this.f7569.obtainMessage(1), this.f7549);
            this.f7569.sendMessageDelayed(this.f7569.obtainMessage(2), this.f7568);
        }
        this.f7563.m8904();
        this.f7547.m9053(i);
        this.f7547.m9052();
        if (i == 2) {
            m8700();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8713(Bundle bundle) {
        while (!this.f7567.isEmpty()) {
            zze(this.f7567.remove());
        }
        this.f7547.m9054(bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8714(ConnectionResult connectionResult) {
        if (!zzf.m4220(this.f7561, connectionResult.m8480())) {
            m8709();
        }
        if (!this.f7548) {
            this.f7547.m9055(connectionResult);
            this.f7547.m9052();
        }
    }
}
