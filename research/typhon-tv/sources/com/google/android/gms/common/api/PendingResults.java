package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.internal.zzbq;

public final class PendingResults {

    static final class zzb<R extends Result> extends BasePendingResult<R> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final R f7455;

        public zzb(GoogleApiClient googleApiClient, R r) {
            super(googleApiClient);
            this.f7455 = r;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final R m8538(Status status) {
            return this.f7455;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <R extends Result> PendingResult<R> m8537(R r, GoogleApiClient googleApiClient) {
        zzbq.m9121(r, (Object) "Result must not be null");
        zzbq.m9117(!r.s_().m8549(), "Status code must not be SUCCESS");
        zzb zzb2 = new zzb(googleApiClient, r);
        zzb2.m4198(r);
        return zzb2;
    }
}
