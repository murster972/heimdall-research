package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

public final class zzcg extends Fragment implements zzcf {

    /* renamed from: 龘  reason: contains not printable characters */
    private static WeakHashMap<Activity, WeakReference<zzcg>> f7648 = new WeakHashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, LifecycleCallback> f7649 = new ArrayMap();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Bundle f7650;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public int f7651 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzcg m8848(Activity activity) {
        zzcg zzcg;
        WeakReference weakReference = f7648.get(activity);
        if (weakReference == null || (zzcg = (zzcg) weakReference.get()) == null) {
            try {
                zzcg = (zzcg) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
                if (zzcg == null || zzcg.isRemoving()) {
                    zzcg = new zzcg();
                    activity.getFragmentManager().beginTransaction().add(zzcg, "LifecycleFragmentImpl").commitAllowingStateLoss();
                }
                f7648.put(activity, new WeakReference(zzcg));
            } catch (ClassCastException e) {
                throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
            }
        }
        return zzcg;
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback r0 : this.f7649.values()) {
            r0.m8563(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback r0 : this.f7649.values()) {
            r0.m8561(i, i2, intent);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f7651 = 1;
        this.f7650 = bundle;
        for (Map.Entry next : this.f7649.entrySet()) {
            ((LifecycleCallback) next.getValue()).m8562(bundle != null ? bundle.getBundle((String) next.getKey()) : null);
        }
    }

    public final void onDestroy() {
        super.onDestroy();
        this.f7651 = 5;
        for (LifecycleCallback r0 : this.f7649.values()) {
            r0.m8555();
        }
    }

    public final void onResume() {
        super.onResume();
        this.f7651 = 3;
        for (LifecycleCallback r0 : this.f7649.values()) {
            r0.m8559();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry next : this.f7649.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) next.getValue()).m8557(bundle2);
                bundle.putBundle((String) next.getKey(), bundle2);
            }
        }
    }

    public final void onStart() {
        super.onStart();
        this.f7651 = 2;
        for (LifecycleCallback r0 : this.f7649.values()) {
            r0.m8556();
        }
    }

    public final void onStop() {
        super.onStop();
        this.f7651 = 4;
        for (LifecycleCallback r0 : this.f7649.values()) {
            r0.m8558();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Activity m8849() {
        return getActivity();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends LifecycleCallback> T m8850(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.f7649.get(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8851(String str, LifecycleCallback lifecycleCallback) {
        if (!this.f7649.containsKey(str)) {
            this.f7649.put(str, lifecycleCallback);
            if (this.f7651 > 0) {
                new Handler(Looper.getMainLooper()).post(new zzch(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 59).append("LifecycleCallback with tag ").append(str).append(" already added to this fragment.").toString());
    }
}
