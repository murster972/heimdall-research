package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import java.util.Collections;

public final class zzaz implements zzbh {

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzbi f7543;

    public zzaz(zzbi zzbi) {
        this.f7543 = zzbi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8686(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m8687() {
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8688() {
        this.f7543.m8730();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8689(T t) {
        this.f7543.f7592.f7567.add(t);
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8690() {
        for (Api.zze r0 : this.f7543.f7594.values()) {
            r0.m8513();
        }
        this.f7543.f7592.f7566 = Collections.emptySet();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8691(int i) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8692(Bundle bundle) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8693(ConnectionResult connectionResult, Api<?> api, boolean z) {
    }
}
