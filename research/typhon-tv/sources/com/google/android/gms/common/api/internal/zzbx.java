package com.google.android.gms.common.api.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class zzbx extends BroadcastReceiver {

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzby f7644;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f7645;

    public zzbx(zzby zzby) {
        this.f7644 = zzby;
    }

    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        String str = null;
        if (data != null) {
            str = data.getSchemeSpecificPart();
        }
        if ("com.google.android.gms".equals(str)) {
            this.f7644.m8819();
            m8817();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m8817() {
        if (this.f7645 != null) {
            this.f7645.unregisterReceiver(this);
        }
        this.f7645 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8818(Context context) {
        this.f7645 = context;
    }
}
