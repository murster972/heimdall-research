package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

public final class zzdb extends Fragment implements zzcf {

    /* renamed from: 龘  reason: contains not printable characters */
    private static WeakHashMap<FragmentActivity, WeakReference<zzdb>> f7681 = new WeakHashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, LifecycleCallback> f7682 = new ArrayMap();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Bundle f7683;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public int f7684 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzdb m8885(FragmentActivity fragmentActivity) {
        zzdb zzdb;
        WeakReference weakReference = f7681.get(fragmentActivity);
        if (weakReference == null || (zzdb = (zzdb) weakReference.get()) == null) {
            try {
                zzdb = (zzdb) fragmentActivity.getSupportFragmentManager().findFragmentByTag("SupportLifecycleFragmentImpl");
                if (zzdb == null || zzdb.isRemoving()) {
                    zzdb = new zzdb();
                    fragmentActivity.getSupportFragmentManager().beginTransaction().add((Fragment) zzdb, "SupportLifecycleFragmentImpl").commitAllowingStateLoss();
                }
                f7681.put(fragmentActivity, new WeakReference(zzdb));
            } catch (ClassCastException e) {
                throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e);
            }
        }
        return zzdb;
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback r0 : this.f7682.values()) {
            r0.m8563(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback r0 : this.f7682.values()) {
            r0.m8561(i, i2, intent);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f7684 = 1;
        this.f7683 = bundle;
        for (Map.Entry next : this.f7682.entrySet()) {
            ((LifecycleCallback) next.getValue()).m8562(bundle != null ? bundle.getBundle((String) next.getKey()) : null);
        }
    }

    public final void onDestroy() {
        super.onDestroy();
        this.f7684 = 5;
        for (LifecycleCallback r0 : this.f7682.values()) {
            r0.m8555();
        }
    }

    public final void onResume() {
        super.onResume();
        this.f7684 = 3;
        for (LifecycleCallback r0 : this.f7682.values()) {
            r0.m8559();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry next : this.f7682.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) next.getValue()).m8557(bundle2);
                bundle.putBundle((String) next.getKey(), bundle2);
            }
        }
    }

    public final void onStart() {
        super.onStart();
        this.f7684 = 2;
        for (LifecycleCallback r0 : this.f7682.values()) {
            r0.m8556();
        }
    }

    public final void onStop() {
        super.onStop();
        this.f7684 = 4;
        for (LifecycleCallback r0 : this.f7682.values()) {
            r0.m8558();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Activity m8886() {
        return getActivity();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T extends LifecycleCallback> T m8887(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.f7682.get(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8888(String str, LifecycleCallback lifecycleCallback) {
        if (!this.f7682.containsKey(str)) {
            this.f7682.put(str, lifecycleCallback);
            if (this.f7684 > 0) {
                new Handler(Looper.getMainLooper()).post(new zzdc(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 59).append("LifecycleCallback with tag ").append(str).append(" already added to this fragment.").toString());
    }
}
