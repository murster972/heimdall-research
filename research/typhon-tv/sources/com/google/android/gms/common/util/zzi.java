package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;

public final class zzi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Boolean f7937;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Boolean f7938;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Boolean f7939;

    @TargetApi(24)
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m9254(Context context) {
        return (!zzq.m9266() || m9256(context)) && m9257(context);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m9255(Context context) {
        if (f7938 == null) {
            f7938 = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return f7938.booleanValue();
    }

    @TargetApi(21)
    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m9256(Context context) {
        if (f7937 == null) {
            f7937 = Boolean.valueOf(zzq.m9265() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return f7937.booleanValue();
    }

    @TargetApi(20)
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m9257(Context context) {
        if (f7939 == null) {
            f7939 = Boolean.valueOf(zzq.m9264() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return f7939.booleanValue();
    }
}
