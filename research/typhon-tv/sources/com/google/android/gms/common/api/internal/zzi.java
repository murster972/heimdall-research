package com.google.android.gms.common.api.internal;

import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbq;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import org.apache.commons.lang3.StringUtils;

public class zzi extends zzo {

    /* renamed from: 连任  reason: contains not printable characters */
    private final SparseArray<zza> f7717 = new SparseArray<>();

    class zza implements GoogleApiClient.OnConnectionFailedListener {

        /* renamed from: 靐  reason: contains not printable characters */
        public final GoogleApiClient f7718;

        /* renamed from: 齉  reason: contains not printable characters */
        public final GoogleApiClient.OnConnectionFailedListener f7720;

        /* renamed from: 龘  reason: contains not printable characters */
        public final int f7721;

        public zza(int i, GoogleApiClient googleApiClient, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            this.f7721 = i;
            this.f7718 = googleApiClient;
            this.f7720 = onConnectionFailedListener;
            googleApiClient.registerConnectionFailedListener(this);
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 27).append("beginFailureResolution for ").append(valueOf).toString());
            zzi.this.m8953(connectionResult, this.f7721);
        }
    }

    private zzi(zzcf zzcf) {
        super(zzcf);
        this.f7470.m8845("AutoManageHelper", (LifecycleCallback) this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final zza m8922(int i) {
        if (this.f7717.size() <= i) {
            return null;
        }
        return this.f7717.get(this.f7717.keyAt(i));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static zzi m8923(zzce zzce) {
        zzcf r1 = m8554(zzce);
        zzi zzi = (zzi) r1.m8844("AutoManageHelper", zzi.class);
        return zzi != null ? zzi : new zzi(r1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8924() {
        for (int i = 0; i < this.f7717.size(); i++) {
            zza r1 = m8922(i);
            if (r1 != null) {
                r1.f7718.connect();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8925() {
        super.m8951();
        boolean z = this.f7735;
        String valueOf = String.valueOf(this.f7717);
        Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 14).append("onStart ").append(z).append(StringUtils.SPACE).append(valueOf).toString());
        if (this.f7737.get() == null) {
            for (int i = 0; i < this.f7717.size(); i++) {
                zza r1 = m8922(i);
                if (r1 != null) {
                    r1.f7718.connect();
                }
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8926() {
        super.m8954();
        for (int i = 0; i < this.f7717.size(); i++) {
            zza r1 = m8922(i);
            if (r1 != null) {
                r1.f7718.disconnect();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8927(int i) {
        zza zza2 = this.f7717.get(i);
        this.f7717.remove(i);
        if (zza2 != null) {
            zza2.f7718.unregisterConnectionFailedListener(zza2);
            zza2.f7718.disconnect();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8928(int i, GoogleApiClient googleApiClient, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        zzbq.m9121(googleApiClient, (Object) "GoogleApiClient instance cannot be null");
        zzbq.m9126(this.f7717.indexOfKey(i) < 0, (Object) new StringBuilder(54).append("Already managing a GoogleApiClient with id ").append(i).toString());
        zzp zzp = (zzp) this.f7737.get();
        boolean z = this.f7735;
        String valueOf = String.valueOf(zzp);
        Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 49).append("starting AutoManage for client ").append(i).append(StringUtils.SPACE).append(z).append(StringUtils.SPACE).append(valueOf).toString());
        this.f7717.put(i, new zza(i, googleApiClient, onConnectionFailedListener));
        if (this.f7735 && zzp == null) {
            String valueOf2 = String.valueOf(googleApiClient);
            Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf2).length() + 11).append("connecting ").append(valueOf2).toString());
            googleApiClient.connect();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8929(ConnectionResult connectionResult, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        zza zza2 = this.f7717.get(i);
        if (zza2 != null) {
            m8927(i);
            GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener = zza2.f7720;
            if (onConnectionFailedListener != null) {
                onConnectionFailedListener.onConnectionFailed(connectionResult);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8930(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.f7717.size(); i++) {
            zza r1 = m8922(i);
            if (r1 != null) {
                printWriter.append(str).append("GoogleApiClient #").print(r1.f7721);
                printWriter.println(":");
                r1.f7718.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }
}
