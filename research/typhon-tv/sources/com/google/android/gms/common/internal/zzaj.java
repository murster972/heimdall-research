package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.stats.zza;
import java.util.HashSet;
import java.util.Set;

final class zzaj implements ServiceConnection {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ComponentName f7825;

    /* renamed from: ʼ  reason: contains not printable characters */
    private /* synthetic */ zzai f7826;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzah f7827;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7828 = 2;

    /* renamed from: 麤  reason: contains not printable characters */
    private IBinder f7829;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f7830;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Set<ServiceConnection> f7831 = new HashSet();

    public zzaj(zzai zzai, zzah zzah) {
        this.f7826 = zzai;
        this.f7827 = zzah;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.f7826.f7824) {
            this.f7826.f7823.removeMessages(1, this.f7827);
            this.f7829 = iBinder;
            this.f7825 = componentName;
            for (ServiceConnection onServiceConnected : this.f7831) {
                onServiceConnected.onServiceConnected(componentName, iBinder);
            }
            this.f7828 = 1;
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.f7826.f7824) {
            this.f7826.f7823.removeMessages(1, this.f7827);
            this.f7829 = null;
            this.f7825 = componentName;
            for (ServiceConnection onServiceDisconnected : this.f7831) {
                onServiceDisconnected.onServiceDisconnected(componentName);
            }
            this.f7828 = 2;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final ComponentName m9075() {
        return this.f7825;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9076() {
        return this.f7828;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9077(ServiceConnection serviceConnection, String str) {
        zza unused = this.f7826.f7822;
        Context unused2 = this.f7826.f7821;
        this.f7831.remove(serviceConnection);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9078(String str) {
        this.f7826.f7823.removeMessages(1, this.f7827);
        zza unused = this.f7826.f7822;
        this.f7826.f7821.unbindService(this);
        this.f7830 = false;
        this.f7828 = 2;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final IBinder m9079() {
        return this.f7829;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m9080() {
        return this.f7831.isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9081(ServiceConnection serviceConnection, String str) {
        zza unused = this.f7826.f7822;
        Context unused2 = this.f7826.f7821;
        this.f7827.m9065();
        this.f7831.add(serviceConnection);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9082(String str) {
        this.f7828 = 3;
        this.f7830 = this.f7826.f7822.m9237(this.f7826.f7821, str, this.f7827.m9065(), this, this.f7827.m9066());
        if (this.f7830) {
            this.f7826.f7823.sendMessageDelayed(this.f7826.f7823.obtainMessage(1, this.f7827), this.f7826.f7819);
            return;
        }
        this.f7828 = 2;
        try {
            zza unused = this.f7826.f7822;
            this.f7826.f7821.unbindService(this);
        } catch (IllegalArgumentException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9083() {
        return this.f7830;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9084(ServiceConnection serviceConnection) {
        return this.f7831.contains(serviceConnection);
    }
}
