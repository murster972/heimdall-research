package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzax implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzao f7541;

    private zzax(zzao zzao) {
        this.f7541 = zzao;
    }

    /* synthetic */ zzax(zzao zzao, zzap zzap) {
        this(zzao);
    }

    public final void onConnected(Bundle bundle) {
        this.f7541.f7519.m11551(new zzav(this.f7541));
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.f7541.f7521.lock();
        try {
            if (this.f7541.m8667(connectionResult)) {
                this.f7541.m8643();
                this.f7541.m8650();
            } else {
                this.f7541.m8653(connectionResult);
            }
        } finally {
            this.f7541.f7521.unlock();
        }
    }

    public final void onConnectionSuspended(int i) {
    }
}
