package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzj;
import java.lang.ref.WeakReference;

final class zzaq implements zzj {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Api<?> f7528;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean f7529;

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<zzao> f7530;

    public zzaq(zzao zzao, Api<?> api, boolean z) {
        this.f7530 = new WeakReference<>(zzao);
        this.f7528 = api;
        this.f7529 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8678(ConnectionResult connectionResult) {
        boolean z = false;
        zzao zzao = (zzao) this.f7530.get();
        if (zzao != null) {
            if (Looper.myLooper() == zzao.f7524.f7592.getLooper()) {
                z = true;
            }
            zzbq.m9126(z, (Object) "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            zzao.f7521.lock();
            try {
                if (zzao.m8655(0)) {
                    if (!connectionResult.m8478()) {
                        zzao.m8654(connectionResult, this.f7528, this.f7529);
                    }
                    if (zzao.m8658()) {
                        zzao.m8650();
                    }
                    zzao.f7521.unlock();
                }
            } finally {
                zzao.f7521.unlock();
            }
        }
    }
}
