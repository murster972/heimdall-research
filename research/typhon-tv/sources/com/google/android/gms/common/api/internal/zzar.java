package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzj;
import java.util.Iterator;
import java.util.Map;

final class zzar extends zzay {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<Api.zze, zzaq> f7531;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzao f7532;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzar(zzao zzao, Map<Api.zze, zzaq> map) {
        super(zzao, (zzap) null);
        this.f7532 = zzao;
        this.f7531 = map;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8679() {
        boolean z;
        boolean z2;
        int i = 0;
        Iterator<Api.zze> it2 = this.f7531.keySet().iterator();
        boolean z3 = true;
        boolean z4 = false;
        while (true) {
            if (!it2.hasNext()) {
                z = false;
                break;
            }
            Api.zze next = it2.next();
            if (!next.m8518()) {
                z2 = false;
            } else if (!this.f7531.get(next).f7529) {
                z4 = true;
                z = true;
                break;
            } else {
                z2 = z3;
                z4 = true;
            }
            z3 = z2;
        }
        if (z4) {
            i = this.f7532.f7522.m4227(this.f7532.f7523);
        }
        if (i == 0 || (!z && !z3)) {
            if (this.f7532.f7509) {
                this.f7532.f7519.m11548();
            }
            for (Api.zze next2 : this.f7531.keySet()) {
                zzj zzj = this.f7531.get(next2);
                if (!next2.m8518() || i == 0) {
                    next2.m8523(zzj);
                } else {
                    this.f7532.f7524.m8743((zzbj) new zzat(this, this.f7532, zzj));
                }
            }
            return;
        }
        this.f7532.f7524.m8743((zzbj) new zzas(this, this.f7532, new ConnectionResult(i, (PendingIntent) null)));
    }
}
