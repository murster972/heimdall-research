package com.google.android.gms.common.data;

import android.content.ContentValues;
import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@KeepName
public final class DataHolder extends zzbfm implements Closeable {
    public static final Parcelable.Creator<DataHolder> CREATOR = new zzf();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final zza f7773 = new zze(new String[0], (String) null);

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f7774;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Bundle f7775;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int[] f7776;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f7777;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f7778;

    /* renamed from: 连任  reason: contains not printable characters */
    private final CursorWindow[] f7779;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f7780;

    /* renamed from: 麤  reason: contains not printable characters */
    private Bundle f7781;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String[] f7782;

    /* renamed from: 龘  reason: contains not printable characters */
    int f7783;

    public static class zza {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f7784;

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f7785;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final ArrayList<HashMap<String, Object>> f7786;

        /* renamed from: 麤  reason: contains not printable characters */
        private final HashMap<Object, Integer> f7787;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f7788;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final String[] f7789;

        private zza(String[] strArr, String str) {
            this.f7789 = (String[]) zzbq.m9120(strArr);
            this.f7786 = new ArrayList<>();
            this.f7788 = str;
            this.f7787 = new HashMap<>();
            this.f7785 = false;
            this.f7784 = null;
        }

        /* synthetic */ zza(String[] strArr, String str, zze zze) {
            this(strArr, (String) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public zza m9024(ContentValues contentValues) {
            zzc.m9140(contentValues);
            HashMap hashMap = new HashMap(contentValues.size());
            for (Map.Entry next : contentValues.valueSet()) {
                hashMap.put((String) next.getKey(), next.getValue());
            }
            return m9025((HashMap<String, Object>) hashMap);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public zza m9025(HashMap<String, Object> hashMap) {
            int intValue;
            zzc.m9140(hashMap);
            if (this.f7788 == null) {
                intValue = -1;
            } else {
                Object obj = hashMap.get(this.f7788);
                if (obj == null) {
                    intValue = -1;
                } else {
                    Integer num = this.f7787.get(obj);
                    if (num == null) {
                        this.f7787.put(obj, Integer.valueOf(this.f7786.size()));
                        intValue = -1;
                    } else {
                        intValue = num.intValue();
                    }
                }
            }
            if (intValue == -1) {
                this.f7786.add(hashMap);
            } else {
                this.f7786.remove(intValue);
                this.f7786.add(intValue, hashMap);
            }
            this.f7785 = false;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final DataHolder m9026(int i) {
            return new DataHolder(this, 0, (Bundle) null, (zze) null);
        }
    }

    public static class zzb extends RuntimeException {
        public zzb(String str) {
            super(str);
        }
    }

    DataHolder(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.f7777 = false;
        this.f7778 = true;
        this.f7780 = i;
        this.f7782 = strArr;
        this.f7779 = cursorWindowArr;
        this.f7774 = i2;
        this.f7775 = bundle;
    }

    private DataHolder(zza zza2, int i, Bundle bundle) {
        this(zza2.f7789, m9017(zza2, -1), i, (Bundle) null);
    }

    /* synthetic */ DataHolder(zza zza2, int i, Bundle bundle, zze zze) {
        this(zza2, 0, (Bundle) null);
    }

    private DataHolder(String[] strArr, CursorWindow[] cursorWindowArr, int i, Bundle bundle) {
        this.f7777 = false;
        this.f7778 = true;
        this.f7780 = 1;
        this.f7782 = (String[]) zzbq.m9120(strArr);
        this.f7779 = (CursorWindow[]) zzbq.m9120(cursorWindowArr);
        this.f7774 = i;
        this.f7775 = bundle;
        m9020();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zza m9015(String[] strArr) {
        return new zza(strArr, (String) null, (zze) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m9016(String str, int i) {
        if (this.f7781 == null || !this.f7781.containsKey(str)) {
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? "No such column: ".concat(valueOf) : new String("No such column: "));
        } else if (m9018()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i < 0 || i >= this.f7783) {
            throw new CursorIndexOutOfBoundsException(i, this.f7783);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static CursorWindow[] m9017(zza zza2, int i) {
        int i2;
        boolean z;
        if (zza2.f7789.length == 0) {
            return new CursorWindow[0];
        }
        ArrayList r10 = zza2.f7786;
        int size = r10.size();
        CursorWindow cursorWindow = new CursorWindow(false);
        ArrayList arrayList = new ArrayList();
        arrayList.add(cursorWindow);
        cursorWindow.setNumColumns(zza2.f7789.length);
        int i3 = 0;
        boolean z2 = false;
        while (i3 < size) {
            try {
                if (!cursorWindow.allocRow()) {
                    Log.d("DataHolder", new StringBuilder(72).append("Allocating additional cursor window for large data set (row ").append(i3).append(")").toString());
                    cursorWindow = new CursorWindow(false);
                    cursorWindow.setStartPosition(i3);
                    cursorWindow.setNumColumns(zza2.f7789.length);
                    arrayList.add(cursorWindow);
                    if (!cursorWindow.allocRow()) {
                        Log.e("DataHolder", "Unable to allocate row to hold data.");
                        arrayList.remove(cursorWindow);
                        return (CursorWindow[]) arrayList.toArray(new CursorWindow[arrayList.size()]);
                    }
                }
                Map map = (Map) r10.get(i3);
                boolean z3 = true;
                for (int i4 = 0; i4 < zza2.f7789.length && z3; i4++) {
                    String str = zza2.f7789[i4];
                    Object obj = map.get(str);
                    if (obj == null) {
                        z3 = cursorWindow.putNull(i3, i4);
                    } else if (obj instanceof String) {
                        z3 = cursorWindow.putString((String) obj, i3, i4);
                    } else if (obj instanceof Long) {
                        z3 = cursorWindow.putLong(((Long) obj).longValue(), i3, i4);
                    } else if (obj instanceof Integer) {
                        z3 = cursorWindow.putLong((long) ((Integer) obj).intValue(), i3, i4);
                    } else if (obj instanceof Boolean) {
                        z3 = cursorWindow.putLong(((Boolean) obj).booleanValue() ? 1 : 0, i3, i4);
                    } else if (obj instanceof byte[]) {
                        z3 = cursorWindow.putBlob((byte[]) obj, i3, i4);
                    } else if (obj instanceof Double) {
                        z3 = cursorWindow.putDouble(((Double) obj).doubleValue(), i3, i4);
                    } else if (obj instanceof Float) {
                        z3 = cursorWindow.putDouble((double) ((Float) obj).floatValue(), i3, i4);
                    } else {
                        String valueOf = String.valueOf(obj);
                        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 32 + String.valueOf(valueOf).length()).append("Unsupported object for column ").append(str).append(": ").append(valueOf).toString());
                    }
                }
                if (z3) {
                    i2 = i3;
                    z = false;
                } else if (z2) {
                    throw new zzb("Could not add the value to a new CursorWindow. The size of value may be larger than what a CursorWindow can handle.");
                } else {
                    Log.d("DataHolder", new StringBuilder(74).append("Couldn't populate window data for row ").append(i3).append(" - allocating new window.").toString());
                    cursorWindow.freeLastRow();
                    cursorWindow = new CursorWindow(false);
                    cursorWindow.setStartPosition(i3);
                    cursorWindow.setNumColumns(zza2.f7789.length);
                    arrayList.add(cursorWindow);
                    i2 = i3 - 1;
                    z = true;
                }
                i3 = i2 + 1;
                z2 = z;
            } catch (RuntimeException e) {
                RuntimeException runtimeException = e;
                int size2 = arrayList.size();
                for (int i5 = 0; i5 < size2; i5++) {
                    ((CursorWindow) arrayList.get(i5)).close();
                }
                throw runtimeException;
            }
        }
        return (CursorWindow[]) arrayList.toArray(new CursorWindow[arrayList.size()]);
    }

    public final void close() {
        synchronized (this) {
            if (!this.f7777) {
                this.f7777 = true;
                for (CursorWindow close : this.f7779) {
                    close.close();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            if (this.f7778 && this.f7779.length > 0 && !m9018()) {
                close();
                String obj = toString();
                Log.e("DataBuffer", new StringBuilder(String.valueOf(obj).length() + 178).append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ").append(obj).append(")").toString());
            }
        } finally {
            super.finalize();
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10200(parcel, 1, this.f7782, false);
        zzbfp.m10199(parcel, 2, (T[]) this.f7779, i, false);
        zzbfp.m10185(parcel, 3, this.f7774);
        zzbfp.m10187(parcel, 4, this.f7775, false);
        zzbfp.m10185(parcel, 1000, this.f7780);
        zzbfp.m10182(parcel, r0);
        if ((i & 1) != 0) {
            close();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9018() {
        boolean z;
        synchronized (this) {
            z = this.f7777;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9019(int i) {
        int i2 = 0;
        zzbq.m9125(i >= 0 && i < this.f7783);
        while (true) {
            if (i2 >= this.f7776.length) {
                break;
            } else if (i < this.f7776[i2]) {
                i2--;
                break;
            } else {
                i2++;
            }
        }
        return i2 == this.f7776.length ? i2 - 1 : i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9020() {
        this.f7781 = new Bundle();
        for (int i = 0; i < this.f7782.length; i++) {
            this.f7781.putInt(this.f7782[i], i);
        }
        this.f7776 = new int[this.f7779.length];
        int i2 = 0;
        for (int i3 = 0; i3 < this.f7779.length; i3++) {
            this.f7776[i3] = i2;
            i2 += this.f7779[i3].getNumRows() - (i2 - this.f7779[i3].getStartPosition());
        }
        this.f7783 = i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final byte[] m9021(String str, int i, int i2) {
        m9016(str, i);
        return this.f7779[i2].getBlob(i, this.f7781.getInt(str));
    }
}
