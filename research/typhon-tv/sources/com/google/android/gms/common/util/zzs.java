package com.google.android.gms.common.util;

import android.os.Process;
import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public final class zzs {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int f7942 = Process.myPid();

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f7943 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9274() {
        if (f7943 == null) {
            f7943 = m9275(f7942);
        }
        return f7943;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m9275(int i) {
        BufferedReader bufferedReader;
        Throwable th;
        BufferedReader bufferedReader2;
        StrictMode.ThreadPolicy allowThreadDiskReads;
        String str = null;
        if (i > 0) {
            try {
                allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                bufferedReader2 = new BufferedReader(new FileReader(new StringBuilder(25).append("/proc/").append(i).append("/cmdline").toString()));
                try {
                    StrictMode.setThreadPolicy(allowThreadDiskReads);
                    str = bufferedReader2.readLine().trim();
                    zzn.m9261(bufferedReader2);
                } catch (IOException e) {
                    zzn.m9261(bufferedReader2);
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = bufferedReader2;
                    zzn.m9261(bufferedReader);
                    throw th;
                }
            } catch (IOException e2) {
                bufferedReader2 = null;
            } catch (Throwable th3) {
                th = th3;
                bufferedReader = null;
                zzn.m9261(bufferedReader);
                throw th;
            }
        }
        return str;
    }
}
