package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.zzq;

public abstract class zza {

    /* renamed from: 龘  reason: contains not printable characters */
    private int f7471;

    public zza(int i) {
        this.f7471 = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static Status m8564(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (zzq.m9272() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8566(Status status);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8567(zzae zzae, boolean z);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m8568(zzbo<?> zzbo) throws DeadObjectException;
}
