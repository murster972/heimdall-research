package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbz;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public final class zzbo<O extends Api.ApiOptions> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, zzu {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final zzae f7617;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Set<zzj> f7618 = new HashSet();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Map<zzck<?>, zzcr> f7619 = new HashMap();

    /* renamed from: ˈ  reason: contains not printable characters */
    private ConnectionResult f7620 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f7621;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzcv f7622;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f7623;

    /* renamed from: 连任  reason: contains not printable characters */
    private final zzh<O> f7624;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Queue<zza> f7625 = new LinkedList();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Api.zzb f7626;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Api.zze f7627;

    /* renamed from: 龘  reason: contains not printable characters */
    final /* synthetic */ zzbm f7628;

    public zzbo(zzbm zzbm, GoogleApi<O> googleApi) {
        this.f7628 = zzbm;
        this.f7627 = googleApi.m4180(zzbm.f7607.getLooper(), (zzbo<O>) this);
        if (this.f7627 instanceof zzbz) {
            this.f7626 = zzbz.m9137();
        } else {
            this.f7626 = this.f7627;
        }
        this.f7624 = googleApi.m4176();
        this.f7617 = new zzae();
        this.f7621 = googleApi.m4179();
        if (this.f7627.m8517()) {
            this.f7622 = googleApi.m4182(zzbm.f7603, zzbm.f7607);
        } else {
            this.f7622 = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m8781() {
        m8800();
        m8783(ConnectionResult.f7411);
        m8789();
        for (zzcr zzcr : this.f7619.values()) {
            try {
                zzcr.f7667.m8863(this.f7626, new TaskCompletionSource());
            } catch (DeadObjectException e) {
                onConnectionSuspended(1);
                this.f7627.m8513();
            } catch (RemoteException e2) {
            }
        }
        while (this.f7627.m8514() && !this.f7625.isEmpty()) {
            m8784(this.f7625.remove());
        }
        m8782();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private final void m8782() {
        this.f7628.f7607.removeMessages(12, this.f7624);
        this.f7628.f7607.sendMessageDelayed(this.f7628.f7607.obtainMessage(12, this.f7624), this.f7628.f7611);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8783(ConnectionResult connectionResult) {
        for (zzj next : this.f7618) {
            String str = null;
            if (connectionResult == ConnectionResult.f7411) {
                str = this.f7627.m8516();
            }
            next.m8933(this.f7624, connectionResult, str);
        }
        this.f7618.clear();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final void m8784(zza zza) {
        zza.m8567(this.f7617, m8797());
        try {
            zza.m8568((zzbo<?>) this);
        } catch (DeadObjectException e) {
            onConnectionSuspended(1);
            this.f7627.m8513();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m8788() {
        m8800();
        this.f7623 = true;
        this.f7617.m8614();
        this.f7628.f7607.sendMessageDelayed(Message.obtain(this.f7628.f7607, 9, this.f7624), this.f7628.f7613);
        this.f7628.f7607.sendMessageDelayed(Message.obtain(this.f7628.f7607, 11, this.f7624), this.f7628.f7612);
        int unused = this.f7628.f7609 = -1;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final void m8789() {
        if (this.f7623) {
            this.f7628.f7607.removeMessages(11, this.f7624);
            this.f7628.f7607.removeMessages(9, this.f7624);
            this.f7623 = false;
        }
    }

    public final void onConnected(Bundle bundle) {
        if (Looper.myLooper() == this.f7628.f7607.getLooper()) {
            m8781();
        } else {
            this.f7628.f7607.post(new zzbp(this));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        if (r5.f7628.m8779(r6, r5.f7621) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0074, code lost:
        if (r6.m8480() != 18) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0076, code lost:
        r5.f7623 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007b, code lost:
        if (r5.f7623 == false) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007d, code lost:
        r5.f7628.f7607.sendMessageDelayed(android.os.Message.obtain(r5.f7628.f7607, 9, r5.f7624), r5.f7628.f7613);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009b, code lost:
        r2 = r5.f7624.m8921();
        m8805(new com.google.android.gms.common.api.Status(17, new java.lang.StringBuilder(java.lang.String.valueOf(r2).length() + 38).append("API: ").append(r2).append(" is not available on this device.").toString()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onConnectionFailed(com.google.android.gms.common.ConnectionResult r6) {
        /*
            r5 = this;
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628
            android.os.Handler r0 = r0.f7607
            com.google.android.gms.common.internal.zzbq.m9124((android.os.Handler) r0)
            com.google.android.gms.common.api.internal.zzcv r0 = r5.f7622
            if (r0 == 0) goto L_0x0012
            com.google.android.gms.common.api.internal.zzcv r0 = r5.f7622
            r0.m8875()
        L_0x0012:
            r5.m8800()
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628
            r1 = -1
            int unused = r0.f7609 = r1
            r5.m8783((com.google.android.gms.common.ConnectionResult) r6)
            int r0 = r6.m8480()
            r1 = 4
            if (r0 != r1) goto L_0x002d
            com.google.android.gms.common.api.Status r0 = com.google.android.gms.common.api.internal.zzbm.f7601
            r5.m8805((com.google.android.gms.common.api.Status) r0)
        L_0x002c:
            return
        L_0x002d:
            java.util.Queue<com.google.android.gms.common.api.internal.zza> r0 = r5.f7625
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0038
            r5.f7620 = r6
            goto L_0x002c
        L_0x0038:
            java.lang.Object r1 = com.google.android.gms.common.api.internal.zzbm.f7599
            monitor-enter(r1)
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628     // Catch:{ all -> 0x0060 }
            com.google.android.gms.common.api.internal.zzah r0 = r0.f7605     // Catch:{ all -> 0x0060 }
            if (r0 == 0) goto L_0x0063
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628     // Catch:{ all -> 0x0060 }
            java.util.Set r0 = r0.f7614     // Catch:{ all -> 0x0060 }
            com.google.android.gms.common.api.internal.zzh<O> r2 = r5.f7624     // Catch:{ all -> 0x0060 }
            boolean r0 = r0.contains(r2)     // Catch:{ all -> 0x0060 }
            if (r0 == 0) goto L_0x0063
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628     // Catch:{ all -> 0x0060 }
            com.google.android.gms.common.api.internal.zzah r0 = r0.f7605     // Catch:{ all -> 0x0060 }
            int r2 = r5.f7621     // Catch:{ all -> 0x0060 }
            r0.m8953(r6, r2)     // Catch:{ all -> 0x0060 }
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            goto L_0x002c
        L_0x0060:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            throw r0
        L_0x0063:
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628
            int r1 = r5.f7621
            boolean r0 = r0.m8779((com.google.android.gms.common.ConnectionResult) r6, (int) r1)
            if (r0 != 0) goto L_0x002c
            int r0 = r6.m8480()
            r1 = 18
            if (r0 != r1) goto L_0x0079
            r0 = 1
            r5.f7623 = r0
        L_0x0079:
            boolean r0 = r5.f7623
            if (r0 == 0) goto L_0x009b
            com.google.android.gms.common.api.internal.zzbm r0 = r5.f7628
            android.os.Handler r0 = r0.f7607
            com.google.android.gms.common.api.internal.zzbm r1 = r5.f7628
            android.os.Handler r1 = r1.f7607
            r2 = 9
            com.google.android.gms.common.api.internal.zzh<O> r3 = r5.f7624
            android.os.Message r1 = android.os.Message.obtain(r1, r2, r3)
            com.google.android.gms.common.api.internal.zzbm r2 = r5.f7628
            long r2 = r2.f7613
            r0.sendMessageDelayed(r1, r2)
            goto L_0x002c
        L_0x009b:
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status
            r1 = 17
            com.google.android.gms.common.api.internal.zzh<O> r2 = r5.f7624
            java.lang.String r2 = r2.m8921()
            java.lang.String r3 = java.lang.String.valueOf(r2)
            int r3 = r3.length()
            int r3 = r3 + 38
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r3)
            java.lang.String r3 = "API: "
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " is not available on this device."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r1, r2)
            r5.m8805((com.google.android.gms.common.api.Status) r0)
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzbo.onConnectionFailed(com.google.android.gms.common.ConnectionResult):void");
    }

    public final void onConnectionSuspended(int i) {
        if (Looper.myLooper() == this.f7628.f7607.getLooper()) {
            m8788();
        } else {
            this.f7628.f7607.post(new zzbq(this));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8790() {
        zzbq.m9124(this.f7628.f7607);
        if (this.f7623) {
            m8795();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m8791() {
        zzbq.m9124(this.f7628.f7607);
        if (this.f7623) {
            m8789();
            m8805(this.f7628.f7608.m4227(this.f7628.f7603) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
            this.f7627.m8513();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m8792() {
        zzbq.m9124(this.f7628.f7607);
        if (this.f7627.m8514() && this.f7619.size() == 0) {
            if (this.f7617.m8617()) {
                m8782();
            } else {
                this.f7627.m8513();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final zzcxd m8793() {
        if (this.f7622 == null) {
            return null;
        }
        return this.f7622.m8876();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int m8794() {
        return this.f7621;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m8795() {
        zzbq.m9124(this.f7628.f7607);
        if (!this.f7627.m8514() && !this.f7627.m8515()) {
            if (this.f7627.m8518() && this.f7628.f7609 != 0) {
                int unused = this.f7628.f7609 = this.f7628.f7608.m4227(this.f7628.f7603);
                if (this.f7628.f7609 != 0) {
                    onConnectionFailed(new ConnectionResult(this.f7628.f7609, (PendingIntent) null));
                    return;
                }
            }
            zzbu zzbu = new zzbu(this.f7628, this.f7627, this.f7624);
            if (this.f7627.m8517()) {
                this.f7622.m8877((zzcy) zzbu);
            }
            this.f7627.m8523((zzj) zzbu);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m8796() {
        return this.f7627.m8514();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m8797() {
        return this.f7627.m8517();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final ConnectionResult m8798() {
        zzbq.m9124(this.f7628.f7607);
        return this.f7620;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Api.zze m8799() {
        return this.f7627;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m8800() {
        zzbq.m9124(this.f7628.f7607);
        this.f7620 = null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Map<zzck<?>, zzcr> m8801() {
        return this.f7619;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8802() {
        zzbq.m9124(this.f7628.f7607);
        m8805(zzbm.f7602);
        this.f7617.m8613();
        for (zzck zzf : (zzck[]) this.f7619.keySet().toArray(new zzck[this.f7619.size()])) {
            m8806((zza) new zzf(zzf, new TaskCompletionSource()));
        }
        m8783(new ConnectionResult(4));
        if (this.f7627.m8514()) {
            this.f7627.m8524((zzp) new zzbs(this));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8803(ConnectionResult connectionResult) {
        zzbq.m9124(this.f7628.f7607);
        this.f7627.m8513();
        onConnectionFailed(connectionResult);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8804(ConnectionResult connectionResult, Api<?> api, boolean z) {
        if (Looper.myLooper() == this.f7628.f7607.getLooper()) {
            onConnectionFailed(connectionResult);
        } else {
            this.f7628.f7607.post(new zzbr(this, connectionResult));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8805(Status status) {
        zzbq.m9124(this.f7628.f7607);
        for (zza r0 : this.f7625) {
            r0.m8566(status);
        }
        this.f7625.clear();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8806(zza zza) {
        zzbq.m9124(this.f7628.f7607);
        if (this.f7627.m8514()) {
            m8784(zza);
            m8782();
            return;
        }
        this.f7625.add(zza);
        if (this.f7620 == null || !this.f7620.m8481()) {
            m8795();
        } else {
            onConnectionFailed(this.f7620);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8807(zzj zzj) {
        zzbq.m9124(this.f7628.f7607);
        this.f7618.add(zzj);
    }
}
