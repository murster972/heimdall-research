package com.google.android.gms.common.internal;

import android.content.Context;

public final class zzbf {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f7843;

    /* renamed from: 麤  reason: contains not printable characters */
    private static int f7844;

    /* renamed from: 齉  reason: contains not printable characters */
    private static String f7845;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object f7846 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m9109(Context context) {
        m9110(context);
        return f7844;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m9110(android.content.Context r4) {
        /*
            java.lang.Object r1 = f7846
            monitor-enter(r1)
            boolean r0 = f7843     // Catch:{ all -> 0x0020 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0020 }
        L_0x0008:
            return
        L_0x0009:
            r0 = 1
            f7843 = r0     // Catch:{ all -> 0x0020 }
            java.lang.String r0 = r4.getPackageName()     // Catch:{ all -> 0x0020 }
            com.google.android.gms.internal.zzbhe r2 = com.google.android.gms.internal.zzbhf.m10231(r4)     // Catch:{ all -> 0x0020 }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r0 = r2.m10226((java.lang.String) r0, (int) r3)     // Catch:{ NameNotFoundException -> 0x0037 }
            android.os.Bundle r0 = r0.metaData     // Catch:{ NameNotFoundException -> 0x0037 }
            if (r0 != 0) goto L_0x0023
            monitor-exit(r1)     // Catch:{ all -> 0x0020 }
            goto L_0x0008
        L_0x0020:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0020 }
            throw r0
        L_0x0023:
            java.lang.String r2 = "com.google.app.id"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ NameNotFoundException -> 0x0037 }
            f7845 = r2     // Catch:{ NameNotFoundException -> 0x0037 }
            java.lang.String r2 = "com.google.android.gms.version"
            int r0 = r0.getInt(r2)     // Catch:{ NameNotFoundException -> 0x0037 }
            f7844 = r0     // Catch:{ NameNotFoundException -> 0x0037 }
        L_0x0035:
            monitor-exit(r1)     // Catch:{ all -> 0x0020 }
            goto L_0x0008
        L_0x0037:
            r0 = move-exception
            java.lang.String r2 = "MetadataValueReader"
            java.lang.String r3 = "This should never happen."
            android.util.Log.wtf(r2, r3, r0)     // Catch:{ all -> 0x0020 }
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.zzbf.m9110(android.content.Context):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9111(Context context) {
        m9110(context);
        return f7845;
    }
}
