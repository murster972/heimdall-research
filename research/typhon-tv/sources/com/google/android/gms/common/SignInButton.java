package com.google.android.gms.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.R;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzbx;
import com.google.android.gms.common.internal.zzby;
import com.google.android.gms.dynamic.zzq;

public final class SignInButton extends FrameLayout implements View.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f3641;

    /* renamed from: 麤  reason: contains not printable characters */
    private View.OnClickListener f3642;

    /* renamed from: 齉  reason: contains not printable characters */
    private View f3643;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f3644;

    public SignInButton(Context context) {
        this(context, (AttributeSet) null);
    }

    public SignInButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public SignInButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3642 = null;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.SignInButton, 0, 0);
        try {
            this.f3644 = obtainStyledAttributes.getInt(R.styleable.SignInButton_buttonSize, 0);
            this.f3641 = obtainStyledAttributes.getInt(R.styleable.SignInButton_colorScheme, 2);
            obtainStyledAttributes.recycle();
            setStyle(this.f3644, this.f3641);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public final void onClick(View view) {
        if (this.f3642 != null && view == this.f3643) {
            this.f3642.onClick(this);
        }
    }

    public final void setColorScheme(int i) {
        setStyle(this.f3644, i);
    }

    public final void setEnabled(boolean z) {
        super.setEnabled(z);
        this.f3643.setEnabled(z);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.f3642 = onClickListener;
        if (this.f3643 != null) {
            this.f3643.setOnClickListener(this);
        }
    }

    @Deprecated
    public final void setScopes(Scope[] scopeArr) {
        setStyle(this.f3644, this.f3641);
    }

    public final void setSize(int i) {
        setStyle(i, this.f3641);
    }

    public final void setStyle(int i, int i2) {
        this.f3644 = i;
        this.f3641 = i2;
        Context context = getContext();
        if (this.f3643 != null) {
            removeView(this.f3643);
        }
        try {
            this.f3643 = zzbx.m9133(context, this.f3644, this.f3641);
        } catch (zzq e) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            int i3 = this.f3644;
            int i4 = this.f3641;
            zzby zzby = new zzby(context);
            zzby.m9136(context.getResources(), i3, i4);
            this.f3643 = zzby;
        }
        addView(this.f3643);
        this.f3643.setEnabled(isEnabled());
        this.f3643.setOnClickListener(this);
    }

    @Deprecated
    public final void setStyle(int i, int i2, Scope[] scopeArr) {
        setStyle(i, i2);
    }
}
