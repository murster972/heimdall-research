package com.google.android.gms.common.util;

import android.util.Base64;

public final class zzb {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m9239(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return Base64.encodeToString(bArr, 0);
    }
}
