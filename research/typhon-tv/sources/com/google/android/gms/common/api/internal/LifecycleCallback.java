package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Keep;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class LifecycleCallback {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final zzcf f7470;

    protected LifecycleCallback(zzcf zzcf) {
        this.f7470 = zzcf;
    }

    @Keep
    private static zzcf getChimeraLifecycleFragmentImpl(zzce zzce) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzcf m8553(Activity activity) {
        return m8554(new zzce(activity));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static zzcf m8554(zzce zzce) {
        if (zzce.m8842()) {
            return zzdb.m8885(zzce.m8840());
        }
        if (zzce.m8839()) {
            return zzcg.m8848(zzce.m8841());
        }
        throw new IllegalArgumentException("Can't get fragment for unexpected activity.");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m8555() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8556() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m8557(Bundle bundle) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m8558() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m8559() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Activity m8560() {
        return this.f7470.m8843();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8561(int i, int i2, Intent intent) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8562(Bundle bundle) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8563(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }
}
