package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;

public interface zzbh {
    /* renamed from: 靐  reason: contains not printable characters */
    <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8718(T t);

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m8719();

    /* renamed from: 齉  reason: contains not printable characters */
    void m8720();

    /* renamed from: 龘  reason: contains not printable characters */
    <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8721(T t);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8722();

    /* renamed from: 龘  reason: contains not printable characters */
    void m8723(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8724(Bundle bundle);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8725(ConnectionResult connectionResult, Api<?> api, boolean z);
}
