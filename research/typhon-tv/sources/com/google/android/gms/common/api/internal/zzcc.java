package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public interface zzcc {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m8823();

    /* renamed from: ʼ  reason: contains not printable characters */
    void m8824();

    /* renamed from: 连任  reason: contains not printable characters */
    boolean m8825();

    /* renamed from: 靐  reason: contains not printable characters */
    ConnectionResult m8826();

    /* renamed from: 靐  reason: contains not printable characters */
    <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8827(T t);

    /* renamed from: 麤  reason: contains not printable characters */
    boolean m8828();

    /* renamed from: 齉  reason: contains not printable characters */
    void m8829();

    /* renamed from: 龘  reason: contains not printable characters */
    ConnectionResult m8830(long j, TimeUnit timeUnit);

    /* renamed from: 龘  reason: contains not printable characters */
    ConnectionResult m8831(Api<?> api);

    /* renamed from: 龘  reason: contains not printable characters */
    <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8832(T t);

    /* renamed from: 龘  reason: contains not printable characters */
    void m8833();

    /* renamed from: 龘  reason: contains not printable characters */
    void m8834(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m8835(zzcu zzcu);
}
