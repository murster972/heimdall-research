package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

final class zzaz implements zzay {

    /* renamed from: 龘  reason: contains not printable characters */
    private final IBinder f7842;

    zzaz(IBinder iBinder) {
        this.f7842 = iBinder;
    }

    public final IBinder asBinder() {
        return this.f7842;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9102(zzaw zzaw, zzz zzz) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
            obtain.writeStrongBinder(zzaw.asBinder());
            obtain.writeInt(1);
            zzz.writeToParcel(obtain, 0);
            this.f7842.transact(46, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }
}
