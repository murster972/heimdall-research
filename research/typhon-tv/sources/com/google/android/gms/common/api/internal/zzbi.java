package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class zzbi implements zzcc, zzu {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Lock f7581;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Condition f7582;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Context f7583;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Api.zza<? extends zzcxd, zzcxe> f7584;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public volatile zzbh f7585;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Map<Api<?>, Boolean> f7586;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzf f7587;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzbk f7588;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private zzr f7589;

    /* renamed from: 连任  reason: contains not printable characters */
    final zzcd f7590;

    /* renamed from: 靐  reason: contains not printable characters */
    final Map<Api.zzc<?>, ConnectionResult> f7591 = new HashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    final zzba f7592;

    /* renamed from: 齉  reason: contains not printable characters */
    int f7593;

    /* renamed from: 龘  reason: contains not printable characters */
    final Map<Api.zzc<?>, Api.zze> f7594;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private ConnectionResult f7595 = null;

    public zzbi(Context context, zzba zzba, Lock lock, Looper looper, zzf zzf, Map<Api.zzc<?>, Api.zze> map, zzr zzr, Map<Api<?>, Boolean> map2, Api.zza<? extends zzcxd, zzcxe> zza, ArrayList<zzt> arrayList, zzcd zzcd) {
        this.f7583 = context;
        this.f7581 = lock;
        this.f7587 = zzf;
        this.f7594 = map;
        this.f7589 = zzr;
        this.f7586 = map2;
        this.f7584 = zza;
        this.f7592 = zzba;
        this.f7590 = zzcd;
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            ((zzt) obj).m8962(this);
        }
        this.f7588 = new zzbk(this, looper);
        this.f7582 = lock.newCondition();
        this.f7585 = new zzaz(this);
    }

    public final void onConnected(Bundle bundle) {
        this.f7581.lock();
        try {
            this.f7585.m8724(bundle);
        } finally {
            this.f7581.unlock();
        }
    }

    public final void onConnectionSuspended(int i) {
        this.f7581.lock();
        try {
            this.f7585.m8723(i);
        } finally {
            this.f7581.unlock();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8728() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m8729() {
        if (m8735()) {
            ((zzal) this.f7585).m8631();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m8730() {
        this.f7581.lock();
        try {
            this.f7585 = new zzao(this, this.f7589, this.f7586, this.f7587, this.f7584, this.f7581, this.f7583);
            this.f7585.m8722();
            this.f7582.signalAll();
        } finally {
            this.f7581.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m8731() {
        this.f7581.lock();
        try {
            this.f7592.m8709();
            this.f7585 = new zzal(this);
            this.f7585.m8722();
            this.f7582.signalAll();
        } finally {
            this.f7581.unlock();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m8732() {
        return this.f7585 instanceof zzao;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectionResult m8733() {
        m8740();
        while (m8732()) {
            try {
                this.f7582.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, (PendingIntent) null);
            }
        }
        return m8735() ? ConnectionResult.f7411 : this.f7595 != null ? this.f7595 : new ConnectionResult(13, (PendingIntent) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8734(T t) {
        t.m4189();
        return this.f7585.m8718(t);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m8735() {
        return this.f7585 instanceof zzal;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8736() {
        if (this.f7585.m8719()) {
            this.f7591.clear();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m8737(long j, TimeUnit timeUnit) {
        m8740();
        long nanos = timeUnit.toNanos(j);
        while (m8732()) {
            if (nanos <= 0) {
                try {
                    m8736();
                    return new ConnectionResult(14, (PendingIntent) null);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return new ConnectionResult(15, (PendingIntent) null);
                }
            } else {
                nanos = this.f7582.awaitNanos(nanos);
            }
        }
        return m8735() ? ConnectionResult.f7411 : this.f7595 != null ? this.f7595 : new ConnectionResult(13, (PendingIntent) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m8738(Api<?> api) {
        Api.zzc<?> r1 = api.m8506();
        if (this.f7594.containsKey(r1)) {
            if (this.f7594.get(r1).m8514()) {
                return ConnectionResult.f7411;
            }
            if (this.f7591.containsKey(r1)) {
                return this.f7591.get(r1);
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8739(T t) {
        t.m4189();
        return this.f7585.m8721(t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8740() {
        this.f7585.m8720();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8741(ConnectionResult connectionResult) {
        this.f7581.lock();
        try {
            this.f7595 = connectionResult;
            this.f7585 = new zzaz(this);
            this.f7585.m8722();
            this.f7582.signalAll();
        } finally {
            this.f7581.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8742(ConnectionResult connectionResult, Api<?> api, boolean z) {
        this.f7581.lock();
        try {
            this.f7585.m8725(connectionResult, api, z);
        } finally {
            this.f7581.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8743(zzbj zzbj) {
        this.f7588.sendMessage(this.f7588.obtainMessage(1, zzbj));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8744(RuntimeException runtimeException) {
        this.f7588.sendMessage(this.f7588.obtainMessage(2, runtimeException));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8745(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append(str).append("mState=").println(this.f7585);
        for (Api next : this.f7586.keySet()) {
            printWriter.append(str).append(next.m8505()).println(":");
            this.f7594.get(next.m8506()).m8525(concat, fileDescriptor, printWriter, strArr);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8746(zzcu zzcu) {
        return false;
    }
}
