package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.view.View;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzcxe;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class zzr {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final View f3680;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f3681;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f3682;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final zzcxe f3683;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Integer f3684;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f3685;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Set<Scope> f3686;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<Api<?>, zzt> f3687;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Set<Scope> f3688;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Account f3689;

    public zzr(Account account, Set<Scope> set, Map<Api<?>, zzt> map, int i, View view, String str, String str2, zzcxe zzcxe) {
        this.f3689 = account;
        this.f3686 = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        this.f3687 = map == null ? Collections.EMPTY_MAP : map;
        this.f3680 = view;
        this.f3685 = i;
        this.f3681 = str;
        this.f3682 = str2;
        this.f3683 = zzcxe;
        HashSet hashSet = new HashSet(this.f3686);
        for (zzt zzt : this.f3687.values()) {
            hashSet.addAll(zzt.f7909);
        }
        this.f3688 = Collections.unmodifiableSet(hashSet);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzr m4205(Context context) {
        return new GoogleApiClient.Builder(context).zzagu();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Map<Api<?>, zzt> m4206() {
        return this.f3687;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m4207() {
        return this.f3681;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m4208() {
        return this.f3682;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final zzcxe m4209() {
        return this.f3683;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final Integer m4210() {
        return this.f3684;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Set<Scope> m4211() {
        return this.f3688;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Account m4212() {
        return this.f3689;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Set<Scope> m4213() {
        return this.f3686;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Account m4214() {
        return this.f3689 != null ? this.f3689 : new Account("<<default account>>", "com.google");
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m4215() {
        if (this.f3689 != null) {
            return this.f3689.name;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Set<Scope> m4216(Api<?> api) {
        zzt zzt = this.f3687.get(api);
        if (zzt == null || zzt.f7909.isEmpty()) {
            return this.f3686;
        }
        HashSet hashSet = new HashSet(this.f3686);
        hashSet.addAll(zzt.f7909);
        return hashSet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m4217(Integer num) {
        this.f3684 = num;
    }
}
