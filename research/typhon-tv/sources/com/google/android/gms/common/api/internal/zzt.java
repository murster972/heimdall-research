package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbq;

public final class zzt implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f7744;

    /* renamed from: 齉  reason: contains not printable characters */
    private zzu f7745;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Api<?> f7746;

    public zzt(Api<?> api, boolean z) {
        this.f7746 = api;
        this.f7744 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8961() {
        zzbq.m9121(this.f7745, (Object) "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }

    public final void onConnected(Bundle bundle) {
        m8961();
        this.f7745.onConnected(bundle);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        m8961();
        this.f7745.m8963(connectionResult, this.f7746, this.f7744);
    }

    public final void onConnectionSuspended(int i) {
        m8961();
        this.f7745.onConnectionSuspended(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8962(zzu zzu) {
        this.f7745 = zzu;
    }
}
