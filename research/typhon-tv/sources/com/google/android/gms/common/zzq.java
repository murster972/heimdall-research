package com.google.android.gms.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbhf;

public class zzq {

    /* renamed from: 龘  reason: contains not printable characters */
    private static zzq f7960;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f7961;

    private zzq(Context context) {
        this.f7961 = context.getApplicationContext();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m9298(PackageInfo packageInfo, boolean z) {
        boolean z2 = false;
        if (packageInfo.signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
        } else {
            zzi zzi = new zzi(packageInfo.signatures[0].toByteArray());
            String str = packageInfo.packageName;
            z2 = z ? zzg.m9283(str, zzi) : zzg.m9286(str, zzi);
            if (!z2) {
                Log.d("GoogleSignatureVerifier", new StringBuilder(27).append("Cert not in list. atk=").append(z).toString());
            }
        }
        return z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static zzh m9299(PackageInfo packageInfo, zzh... zzhArr) {
        if (packageInfo.signatures == null) {
            return null;
        }
        if (packageInfo.signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        zzi zzi = new zzi(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < zzhArr.length; i++) {
            if (zzhArr[i].equals(zzi)) {
                return zzhArr[i];
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzq m9300(Context context) {
        zzbq.m9120(context);
        synchronized (zzq.class) {
            if (f7960 == null) {
                zzg.m9284(context);
                f7960 = new zzq(context);
            }
        }
        return f7960;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m9301(PackageInfo packageInfo, boolean z) {
        zzh r2;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                r2 = m9299(packageInfo, zzk.f7956);
            } else {
                r2 = m9299(packageInfo, zzk.f7956[0]);
            }
            if (r2 != null) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean m9302(String str) {
        try {
            PackageInfo r2 = zzbhf.m10231(this.f7961).m10222(str, 64);
            if (r2 == null) {
                return false;
            }
            if (zzp.zzch(this.f7961)) {
                return m9298(r2, true);
            }
            boolean r1 = m9298(r2, false);
            if (!r1 && m9298(r2, true)) {
                Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
            }
            return r1;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9303(int i) {
        String[] r2 = zzbhf.m10231(this.f7961).m10229(i);
        if (r2 == null || r2.length == 0) {
            return false;
        }
        for (String r4 : r2) {
            if (m9302(r4)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m9304(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (m9301(packageInfo, false)) {
            return true;
        }
        if (!m9301(packageInfo, true)) {
            return false;
        }
        if (zzp.zzch(this.f7961)) {
            return true;
        }
        Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        return false;
    }
}
