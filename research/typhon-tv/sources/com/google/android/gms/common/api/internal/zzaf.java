package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

final class zzaf implements PendingResult.zza {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzae f7495;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ BasePendingResult f7496;

    zzaf(zzae zzae, BasePendingResult basePendingResult) {
        this.f7495 = zzae;
        this.f7496 = basePendingResult;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8618(Status status) {
        this.f7495.f7494.remove(this.f7496);
    }
}
