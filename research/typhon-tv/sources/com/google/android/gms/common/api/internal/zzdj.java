package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class zzdj {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final BasePendingResult<?>[] f7700 = new BasePendingResult[0];

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Status f7701 = new Status(8, "The connection to Google Play services was lost");

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<Api.zzc<?>, Api.zze> f7702;

    /* renamed from: 靐  reason: contains not printable characters */
    final Set<BasePendingResult<?>> f7703 = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzdm f7704 = new zzdk(this);

    public zzdj(Map<Api.zzc<?>, Api.zze> map) {
        this.f7702 = map;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8904() {
        for (BasePendingResult r3 : (BasePendingResult[]) this.f7703.toArray(f7700)) {
            r3.m4194(f7701);
        }
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [com.google.android.gms.common.api.ResultCallback, com.google.android.gms.common.api.zze, com.google.android.gms.common.api.internal.zzdk, com.google.android.gms.common.api.internal.zzdm] */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8905() {
        ? r7 = 0;
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.f7703.toArray(f7700)) {
            basePendingResult.m4200((zzdm) r7);
            if (basePendingResult.m8532() != null) {
                basePendingResult.m8535(r7);
                IBinder r1 = this.f7702.get(((zzm) basePendingResult).m8941()).m8519();
                if (basePendingResult.m4192()) {
                    basePendingResult.m4200((zzdm) new zzdl(basePendingResult, r7, r1, r7));
                } else if (r1 == null || !r1.isBinderAlive()) {
                    basePendingResult.m4200((zzdm) r7);
                    basePendingResult.m8533();
                    r7.m9010(basePendingResult.m8532().intValue());
                } else {
                    zzdl zzdl = new zzdl(basePendingResult, r7, r1, r7);
                    basePendingResult.m4200((zzdm) zzdl);
                    try {
                        r1.linkToDeath(zzdl, 0);
                    } catch (RemoteException e) {
                        basePendingResult.m8533();
                        r7.m9010(basePendingResult.m8532().intValue());
                    }
                }
                this.f7703.remove(basePendingResult);
            } else if (basePendingResult.m4190()) {
                this.f7703.remove(basePendingResult);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8906(BasePendingResult<? extends Result> basePendingResult) {
        this.f7703.add(basePendingResult);
        basePendingResult.m4200(this.f7704);
    }
}
