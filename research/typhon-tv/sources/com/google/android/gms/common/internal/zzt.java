package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.Set;

public final class zzt {

    /* renamed from: 龘  reason: contains not printable characters */
    public final Set<Scope> f7909;

    public zzt(Set<Scope> set) {
        zzbq.m9120(set);
        this.f7909 = Collections.unmodifiableSet(set);
    }
}
