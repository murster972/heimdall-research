package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

final class zzdi extends Handler {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ zzdg f7699;

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                PendingResult pendingResult = (PendingResult) message.obj;
                synchronized (this.f7699.f7692) {
                    if (pendingResult == null) {
                        this.f7699.f7693.m8899(new Status(13, "Transform returned null"));
                    } else if (pendingResult instanceof zzct) {
                        this.f7699.f7693.m8899(((zzct) pendingResult).m8866());
                    } else {
                        this.f7699.f7693.m8903((PendingResult<?>) pendingResult);
                    }
                }
                return;
            case 1:
                RuntimeException runtimeException = (RuntimeException) message.obj;
                String valueOf = String.valueOf(runtimeException.getMessage());
                Log.e("TransformedResultImpl", valueOf.length() != 0 ? "Runtime exception on the transformation worker thread: ".concat(valueOf) : new String("Runtime exception on the transformation worker thread: "));
                throw runtimeException;
            default:
                Log.e("TransformedResultImpl", new StringBuilder(70).append("TransformationResultHandler received unknown message type: ").append(message.what).toString());
                return;
        }
    }
}
