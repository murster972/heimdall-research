package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzaw extends IInterface {
    /* renamed from: 龘  reason: contains not printable characters */
    void m9099(int i, Bundle bundle) throws RemoteException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m9100(int i, IBinder iBinder, Bundle bundle) throws RemoteException;
}
