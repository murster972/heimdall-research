package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class zzj extends zzh {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final WeakReference<byte[]> f7954 = new WeakReference<>((Object) null);

    /* renamed from: 龘  reason: contains not printable characters */
    private WeakReference<byte[]> f7955 = f7954;

    zzj(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public abstract byte[] m9293();

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final byte[] m9294() {
        byte[] bArr;
        synchronized (this) {
            bArr = (byte[]) this.f7955.get();
            if (bArr == null) {
                bArr = m9293();
                this.f7955 = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }
}
