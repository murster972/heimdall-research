package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;

public final class zzg implements zzcz {
    /* renamed from: 龘  reason: contains not printable characters */
    public final Exception m8918(Status status) {
        return zzb.m9103(status);
    }
}
