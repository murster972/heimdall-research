package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzad implements zzg {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ GoogleApiClient.OnConnectionFailedListener f7803;

    zzad(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.f7803 = onConnectionFailedListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9046(ConnectionResult connectionResult) {
        this.f7803.onConnectionFailed(connectionResult);
    }
}
