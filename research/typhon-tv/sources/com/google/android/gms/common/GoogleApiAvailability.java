package com.google.android.gms.common;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import com.google.android.gms.R;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.zzbx;
import com.google.android.gms.common.api.internal.zzby;
import com.google.android.gms.common.api.internal.zzcf;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzu;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.common.util.zzq;

public class GoogleApiAvailability extends zzf {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final GoogleApiAvailability f7418 = new GoogleApiAvailability();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Object f7419 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int f7420 = zzf.f3690;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f7421;

    @SuppressLint({"HandlerLeak"})
    class zza extends Handler {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Context f7423;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public zza(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f7423 = context.getApplicationContext();
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    int r0 = GoogleApiAvailability.this.m4227(this.f7423);
                    if (GoogleApiAvailability.this.m4230(r0)) {
                        GoogleApiAvailability.this.m8499(this.f7423, r0);
                        return;
                    }
                    return;
                default:
                    Log.w("GoogleApiAvailability", new StringBuilder(50).append("Don't know how to handle this message: ").append(message.what).toString());
                    return;
            }
        }
    }

    GoogleApiAvailability() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final String m8483() {
        String str;
        synchronized (f7419) {
            str = this.f7421;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Dialog m8484(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, (AttributeSet) null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(zzu.m9223(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        m8489(activity, (Dialog) create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Dialog m8485(Context context, int i, zzv zzv, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(zzu.m9223(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String r1 = zzu.m9219(context, i);
        if (r1 != null) {
            builder.setPositiveButton(r1, zzv);
        }
        String r12 = zzu.m9225(context, i);
        if (r12 != null) {
            builder.setTitle(r12);
        }
        return builder.create();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GoogleApiAvailability m8486() {
        return f7418;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static zzbx m8487(Context context, zzby zzby) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        zzbx zzbx = new zzbx(zzby);
        context.registerReceiver(zzbx, intentFilter);
        zzbx.m8818(context);
        if (zzp.zzv(context, "com.google.android.gms")) {
            return zzbx;
        }
        zzby.m8819();
        zzbx.m8817();
        return null;
    }

    @TargetApi(26)
    /* renamed from: 龘  reason: contains not printable characters */
    private final String m8488(Context context, NotificationManager notificationManager) {
        zzbq.m9125(zzq.m9267());
        String r0 = m8483();
        if (r0 == null) {
            r0 = "com.google.android.gms.availability";
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(r0);
            String r2 = zzu.m9224(context);
            if (notificationChannel == null) {
                notificationManager.createNotificationChannel(new NotificationChannel(r0, r2, 4));
            } else if (!r2.equals(notificationChannel.getName())) {
                notificationChannel.setName(r2);
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m8489(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            SupportErrorDialogFragment.m8503(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        ErrorDialogFragment.m8482(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @TargetApi(20)
    /* renamed from: 龘  reason: contains not printable characters */
    private final void m8490(Context context, int i, String str, PendingIntent pendingIntent) {
        Notification build;
        int i2;
        if (i == 18) {
            m8492(context);
        } else if (pendingIntent != null) {
            String r1 = zzu.m9221(context, i);
            String r2 = zzu.m9222(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            if (zzi.m9254(context)) {
                zzbq.m9125(zzq.m9264());
                Notification.Builder addAction = new Notification.Builder(context).setSmallIcon(context.getApplicationInfo().icon).setPriority(2).setAutoCancel(true).setContentTitle(r1).setStyle(new Notification.BigTextStyle().bigText(r2)).addAction(R.drawable.common_full_open_on_phone, resources.getString(R.string.common_open_on_phone), pendingIntent);
                if (zzq.m9267() && zzq.m9267()) {
                    addAction.setChannelId(m8488(context, notificationManager));
                }
                build = addAction.build();
            } else {
                NotificationCompat.Builder style = new NotificationCompat.Builder(context).setSmallIcon(17301642).setTicker(resources.getString(R.string.common_google_play_services_notification_ticker)).setWhen(System.currentTimeMillis()).setAutoCancel(true).setContentIntent(pendingIntent).setContentTitle(r1).setContentText(r2).setLocalOnly(true).setStyle(new NotificationCompat.BigTextStyle().bigText(r2));
                if (zzq.m9267() && zzq.m9267()) {
                    style.setChannelId(m8488(context, notificationManager));
                }
                build = style.build();
            }
            switch (i) {
                case 1:
                case 2:
                case 3:
                    i2 = 10436;
                    zzp.zzfln.set(false);
                    break;
                default:
                    i2 = 39789;
                    break;
            }
            notificationManager.notify(i2, build);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m8491(int i) {
        return super.m4225(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8492(Context context) {
        new zza(context).sendEmptyMessageDelayed(1, 120000);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m8493(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog r0 = m8496(activity, i, i2, onCancelListener);
        if (r0 == null) {
            return false;
        }
        m8489(activity, r0, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m8494(Context context) {
        return super.m4227(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Dialog m8495(Activity activity, int i, int i2) {
        return m8496(activity, i, i2, (DialogInterface.OnCancelListener) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Dialog m8496(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return m8485((Context) activity, i, zzv.m9228(activity, zzf.m4223((Context) activity, i, "d"), i2), onCancelListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingIntent m8497(Context context, int i, int i2) {
        return super.m4228(context, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PendingIntent m8498(Context context, ConnectionResult connectionResult) {
        return connectionResult.m8481() ? connectionResult.m8479() : m4228(context, connectionResult.m8480(), 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m8499(Context context, int i) {
        m8490(context, i, (String) null, m4229(context, i, 0, "n"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8500(int i) {
        return super.m4230(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8501(Activity activity, zzcf zzcf, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog r0 = m8485((Context) activity, i, zzv.m9230(zzcf, zzf.m4223((Context) activity, i, "d"), 2), onCancelListener);
        if (r0 == null) {
            return false;
        }
        m8489(activity, r0, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8502(Context context, ConnectionResult connectionResult, int i) {
        PendingIntent r0 = m8498(context, connectionResult);
        if (r0 == null) {
            return false;
        }
        m8490(context, connectionResult.m8480(), (String) null, GoogleApiActivity.m8527(context, r0, i));
        return true;
    }
}
