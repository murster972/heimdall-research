package com.google.android.gms.common.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamic.zzp;
import com.google.android.gms.dynamic.zzq;

public final class zzbx extends zzp<zzbd> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final zzbx f7858 = new zzbx();

    private zzbx() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final View m9132(Context context, int i, int i2) throws zzq {
        try {
            zzbv zzbv = new zzbv(i, i2, (Scope[]) null);
            return (View) zzn.m9307(((zzbd) m9308(context)).m9107(zzn.m9306(context), zzbv));
        } catch (Exception e) {
            throw new zzq(new StringBuilder(64).append("Could not get button with size ").append(i).append(" and color ").append(i2).toString(), e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static View m9133(Context context, int i, int i2) throws zzq {
        return f7858.m9132(context, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ Object m9134(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        return queryLocalInterface instanceof zzbd ? (zzbd) queryLocalInterface : new zzbe(iBinder);
    }
}
