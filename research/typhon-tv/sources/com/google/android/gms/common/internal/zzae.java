package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzae implements Handler.Callback {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final AtomicInteger f7804 = new AtomicInteger(0);

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f7805 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Handler f7806;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Object f7807 = new Object();

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile boolean f7808 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ArrayList<GoogleApiClient.ConnectionCallbacks> f7809 = new ArrayList<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private final ArrayList<GoogleApiClient.OnConnectionFailedListener> f7810 = new ArrayList<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private ArrayList<GoogleApiClient.ConnectionCallbacks> f7811 = new ArrayList<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final zzaf f7812;

    public zzae(Looper looper, zzaf zzaf) {
        this.f7812 = zzaf;
        this.f7806 = new Handler(looper, this);
    }

    public final boolean handleMessage(Message message) {
        if (message.what == 1) {
            GoogleApiClient.ConnectionCallbacks connectionCallbacks = (GoogleApiClient.ConnectionCallbacks) message.obj;
            synchronized (this.f7807) {
                if (this.f7808 && this.f7812.m9058() && this.f7809.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnected(this.f7812.m9059());
                }
            }
            return true;
        }
        Log.wtf("GmsClientEvents", new StringBuilder(45).append("Don't know how to handle message: ").append(message.what).toString(), new Exception());
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m9047() {
        this.f7808 = true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9048(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        boolean contains;
        zzbq.m9120(connectionCallbacks);
        synchronized (this.f7807) {
            contains = this.f7809.contains(connectionCallbacks);
        }
        return contains;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m9049(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        boolean contains;
        zzbq.m9120(onConnectionFailedListener);
        synchronized (this.f7807) {
            contains = this.f7810.contains(onConnectionFailedListener);
        }
        return contains;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9050(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        zzbq.m9120(connectionCallbacks);
        synchronized (this.f7807) {
            if (!this.f7809.remove(connectionCallbacks)) {
                String valueOf = String.valueOf(connectionCallbacks);
                Log.w("GmsClientEvents", new StringBuilder(String.valueOf(valueOf).length() + 52).append("unregisterConnectionCallbacks(): listener ").append(valueOf).append(" not found").toString());
            } else if (this.f7805) {
                this.f7811.add(connectionCallbacks);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m9051(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        zzbq.m9120(onConnectionFailedListener);
        synchronized (this.f7807) {
            if (!this.f7810.remove(onConnectionFailedListener)) {
                String valueOf = String.valueOf(onConnectionFailedListener);
                Log.w("GmsClientEvents", new StringBuilder(String.valueOf(valueOf).length() + 57).append("unregisterConnectionFailedListener(): listener ").append(valueOf).append(" not found").toString());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9052() {
        this.f7808 = false;
        this.f7804.incrementAndGet();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9053(int i) {
        int i2 = 0;
        zzbq.m9126(Looper.myLooper() == this.f7806.getLooper(), (Object) "onUnintentionalDisconnection must only be called on the Handler thread");
        this.f7806.removeMessages(1);
        synchronized (this.f7807) {
            this.f7805 = true;
            ArrayList arrayList = new ArrayList(this.f7809);
            int i3 = this.f7804.get();
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            while (i2 < size) {
                Object obj = arrayList2.get(i2);
                i2++;
                GoogleApiClient.ConnectionCallbacks connectionCallbacks = (GoogleApiClient.ConnectionCallbacks) obj;
                if (this.f7808 && this.f7804.get() == i3) {
                    if (this.f7809.contains(connectionCallbacks)) {
                        connectionCallbacks.onConnectionSuspended(i);
                    }
                }
            }
            this.f7811.clear();
            this.f7805 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9054(Bundle bundle) {
        boolean z = true;
        int i = 0;
        zzbq.m9126(Looper.myLooper() == this.f7806.getLooper(), (Object) "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.f7807) {
            zzbq.m9125(!this.f7805);
            this.f7806.removeMessages(1);
            this.f7805 = true;
            if (this.f7811.size() != 0) {
                z = false;
            }
            zzbq.m9125(z);
            ArrayList arrayList = new ArrayList(this.f7809);
            int i2 = this.f7804.get();
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            while (i < size) {
                Object obj = arrayList2.get(i);
                i++;
                GoogleApiClient.ConnectionCallbacks connectionCallbacks = (GoogleApiClient.ConnectionCallbacks) obj;
                if (this.f7808 && this.f7812.m9058() && this.f7804.get() == i2) {
                    if (!this.f7811.contains(connectionCallbacks)) {
                        connectionCallbacks.onConnected(bundle);
                    }
                }
            }
            this.f7811.clear();
            this.f7805 = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m9055(com.google.android.gms.common.ConnectionResult r8) {
        /*
            r7 = this;
            r1 = 1
            r2 = 0
            android.os.Looper r0 = android.os.Looper.myLooper()
            android.os.Handler r3 = r7.f7806
            android.os.Looper r3 = r3.getLooper()
            if (r0 != r3) goto L_0x0048
            r0 = r1
        L_0x000f:
            java.lang.String r3 = "onConnectionFailure must only be called on the Handler thread"
            com.google.android.gms.common.internal.zzbq.m9126((boolean) r0, (java.lang.Object) r3)
            android.os.Handler r0 = r7.f7806
            r0.removeMessages(r1)
            java.lang.Object r3 = r7.f7807
            monitor-enter(r3)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0056 }
            java.util.ArrayList<com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener> r1 = r7.f7810     // Catch:{ all -> 0x0056 }
            r0.<init>(r1)     // Catch:{ all -> 0x0056 }
            java.util.concurrent.atomic.AtomicInteger r1 = r7.f7804     // Catch:{ all -> 0x0056 }
            int r4 = r1.get()     // Catch:{ all -> 0x0056 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x0056 }
            int r5 = r0.size()     // Catch:{ all -> 0x0056 }
        L_0x0030:
            if (r2 >= r5) goto L_0x0059
            java.lang.Object r1 = r0.get(r2)     // Catch:{ all -> 0x0056 }
            int r2 = r2 + 1
            com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener r1 = (com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener) r1     // Catch:{ all -> 0x0056 }
            boolean r6 = r7.f7808     // Catch:{ all -> 0x0056 }
            if (r6 == 0) goto L_0x0046
            java.util.concurrent.atomic.AtomicInteger r6 = r7.f7804     // Catch:{ all -> 0x0056 }
            int r6 = r6.get()     // Catch:{ all -> 0x0056 }
            if (r6 == r4) goto L_0x004a
        L_0x0046:
            monitor-exit(r3)     // Catch:{ all -> 0x0056 }
        L_0x0047:
            return
        L_0x0048:
            r0 = r2
            goto L_0x000f
        L_0x004a:
            java.util.ArrayList<com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener> r6 = r7.f7810     // Catch:{ all -> 0x0056 }
            boolean r6 = r6.contains(r1)     // Catch:{ all -> 0x0056 }
            if (r6 == 0) goto L_0x0030
            r1.onConnectionFailed(r8)     // Catch:{ all -> 0x0056 }
            goto L_0x0030
        L_0x0056:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0056 }
            throw r0
        L_0x0059:
            monitor-exit(r3)     // Catch:{ all -> 0x0056 }
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.zzae.m9055(com.google.android.gms.common.ConnectionResult):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9056(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        zzbq.m9120(connectionCallbacks);
        synchronized (this.f7807) {
            if (this.f7809.contains(connectionCallbacks)) {
                String valueOf = String.valueOf(connectionCallbacks);
                Log.w("GmsClientEvents", new StringBuilder(String.valueOf(valueOf).length() + 62).append("registerConnectionCallbacks(): listener ").append(valueOf).append(" is already registered").toString());
            } else {
                this.f7809.add(connectionCallbacks);
            }
        }
        if (this.f7812.m9058()) {
            this.f7806.sendMessage(this.f7806.obtainMessage(1, connectionCallbacks));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9057(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        zzbq.m9120(onConnectionFailedListener);
        synchronized (this.f7807) {
            if (this.f7810.contains(onConnectionFailedListener)) {
                String valueOf = String.valueOf(onConnectionFailedListener);
                Log.w("GmsClientEvents", new StringBuilder(String.valueOf(valueOf).length() + 67).append("registerConnectionFailedListener(): listener ").append(valueOf).append(" is already registered").toString());
            } else {
                this.f7810.add(onConnectionFailedListener);
            }
        }
    }
}
