package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzf extends zzb<Boolean> {

    /* renamed from: 靐  reason: contains not printable characters */
    private zzck<?> f7712;

    public zzf(zzck<?> zzck, TaskCompletionSource<Boolean> taskCompletionSource) {
        super(4, taskCompletionSource);
        this.f7712 = zzck;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m8915(zzbo<?> zzbo) throws RemoteException {
        zzcr remove = zzbo.m8801().remove(this.f7712);
        if (remove != null) {
            remove.f7666.m8911(zzbo.m8799(), this.f7544);
            remove.f7667.m8862();
            return;
        }
        this.f7544.m13719(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m8916(Status status) {
        super.m8695(status);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m8917(zzae zzae, boolean z) {
    }
}
