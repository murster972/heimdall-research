package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

final class zzbg extends zzby {

    /* renamed from: 龘  reason: contains not printable characters */
    private WeakReference<zzba> f7580;

    zzbg(zzba zzba) {
        this.f7580 = new WeakReference<>(zzba);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8717() {
        zzba zzba = (zzba) this.f7580.get();
        if (zzba != null) {
            zzba.m8698();
        }
    }
}
