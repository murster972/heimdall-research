package com.google.android.gms.common.util;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.ArraySet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class zze {
    /* renamed from: 靐  reason: contains not printable characters */
    private static <K, V> Map<K, V> m9244(int i, boolean z) {
        return i <= 256 ? new ArrayMap(i) : new HashMap(i, 1.0f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <K, V> Map<K, V> m9245(K k, V v, K k2, V v2, K k3, V v3) {
        Map r0 = m9244(3, false);
        r0.put(k, v);
        r0.put(k2, v2);
        r0.put(k3, v3);
        return Collections.unmodifiableMap(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <K, V> Map<K, V> m9246(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6) {
        Map r0 = m9244(6, false);
        r0.put(k, v);
        r0.put(k2, v2);
        r0.put(k3, v3);
        r0.put(k4, v4);
        r0.put(k5, v5);
        r0.put(k6, v6);
        return Collections.unmodifiableMap(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> Set<T> m9247(int i, boolean z) {
        return i <= 256 ? new ArraySet(i) : new HashSet(i, 1.0f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Set<T> m9248(T t, T t2, T t3) {
        Set r0 = m9247(3, false);
        r0.add(t);
        r0.add(t2);
        r0.add(t3);
        return Collections.unmodifiableSet(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Set<T> m9249(T... tArr) {
        switch (tArr.length) {
            case 0:
                return Collections.emptySet();
            case 1:
                return Collections.singleton(tArr[0]);
            case 2:
                T t = tArr[0];
                T t2 = tArr[1];
                Set r2 = m9247(2, false);
                r2.add(t);
                r2.add(t2);
                return Collections.unmodifiableSet(r2);
            case 3:
                return m9248(tArr[0], tArr[1], tArr[2]);
            case 4:
                T t3 = tArr[0];
                T t4 = tArr[1];
                T t5 = tArr[2];
                T t6 = tArr[3];
                Set r4 = m9247(4, false);
                r4.add(t3);
                r4.add(t4);
                r4.add(t5);
                r4.add(t6);
                return Collections.unmodifiableSet(r4);
            default:
                Set r0 = m9247(tArr.length, false);
                Collections.addAll(r0, tArr);
                return Collections.unmodifiableSet(r0);
        }
    }
}
