package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import java.util.ArrayList;

final class zzau extends zzay {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzao f7536;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ArrayList<Api.zze> f7537;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzau(zzao zzao, ArrayList<Api.zze> arrayList) {
        super(zzao, (zzap) null);
        this.f7536 = zzao;
        this.f7537 = arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8682() {
        this.f7536.f7524.f7592.f7566 = this.f7536.m8646();
        ArrayList arrayList = this.f7537;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((Api.zze) obj).m8522(this.f7536.f7525, this.f7536.f7524.f7592.f7566);
        }
    }
}
