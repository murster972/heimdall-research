package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzbfm;
import com.google.android.gms.internal.zzbfp;

public final class zzz extends zzbfm {
    public static final Parcelable.Creator<zzz> CREATOR = new zzaa();

    /* renamed from: ʻ  reason: contains not printable characters */
    zzc[] f7920;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f7921;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f7922;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f7923;

    /* renamed from: 连任  reason: contains not printable characters */
    Account f7924;

    /* renamed from: 靐  reason: contains not printable characters */
    IBinder f7925;

    /* renamed from: 麤  reason: contains not printable characters */
    Bundle f7926;

    /* renamed from: 齉  reason: contains not printable characters */
    Scope[] f7927;

    /* renamed from: 龘  reason: contains not printable characters */
    String f7928;

    public zzz(int i) {
        this.f7921 = 3;
        this.f7923 = zzf.f3690;
        this.f7922 = i;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: android.accounts.Account} */
    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.common.internal.zzan] */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: type inference failed for: r0v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzz(int r3, int r4, int r5, java.lang.String r6, android.os.IBinder r7, com.google.android.gms.common.api.Scope[] r8, android.os.Bundle r9, android.accounts.Account r10, com.google.android.gms.common.zzc[] r11) {
        /*
            r2 = this;
            r0 = 0
            r2.<init>()
            r2.f7921 = r3
            r2.f7922 = r4
            r2.f7923 = r5
            java.lang.String r1 = "com.google.android.gms"
            boolean r1 = r1.equals(r6)
            if (r1 == 0) goto L_0x002c
            java.lang.String r1 = "com.google.android.gms"
            r2.f7928 = r1
        L_0x0018:
            r1 = 2
            if (r3 >= r1) goto L_0x0043
            if (r7 == 0) goto L_0x0023
            if (r7 != 0) goto L_0x002f
        L_0x001f:
            android.accounts.Account r0 = com.google.android.gms.common.internal.zza.m9038(r0)
        L_0x0023:
            r2.f7924 = r0
        L_0x0025:
            r2.f7927 = r8
            r2.f7926 = r9
            r2.f7920 = r11
            return
        L_0x002c:
            r2.f7928 = r6
            goto L_0x0018
        L_0x002f:
            java.lang.String r0 = "com.google.android.gms.common.internal.IAccountAccessor"
            android.os.IInterface r0 = r7.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.common.internal.zzan
            if (r1 == 0) goto L_0x003d
            com.google.android.gms.common.internal.zzan r0 = (com.google.android.gms.common.internal.zzan) r0
            goto L_0x001f
        L_0x003d:
            com.google.android.gms.common.internal.zzap r0 = new com.google.android.gms.common.internal.zzap
            r0.<init>(r7)
            goto L_0x001f
        L_0x0043:
            r2.f7925 = r7
            r2.f7924 = r10
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.zzz.<init>(int, int, int, java.lang.String, android.os.IBinder, com.google.android.gms.common.api.Scope[], android.os.Bundle, android.accounts.Account, com.google.android.gms.common.zzc[]):void");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int r0 = zzbfp.m10181(parcel);
        zzbfp.m10185(parcel, 1, this.f7921);
        zzbfp.m10185(parcel, 2, this.f7922);
        zzbfp.m10185(parcel, 3, this.f7923);
        zzbfp.m10193(parcel, 4, this.f7928, false);
        zzbfp.m10188(parcel, 5, this.f7925, false);
        zzbfp.m10199(parcel, 6, (T[]) this.f7927, i, false);
        zzbfp.m10187(parcel, 7, this.f7926, false);
        zzbfp.m10189(parcel, 8, (Parcelable) this.f7924, i, false);
        zzbfp.m10199(parcel, 10, (T[]) this.f7920, i, false);
        zzbfp.m10182(parcel, r0);
    }
}
