package com.google.android.gms.common.internal;

import android.content.Intent;
import com.google.android.gms.common.api.internal.zzcf;

final class zzy extends zzv {

    /* renamed from: 靐  reason: contains not printable characters */
    private /* synthetic */ zzcf f7917;

    /* renamed from: 齉  reason: contains not printable characters */
    private /* synthetic */ int f7918;

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ Intent f7919;

    zzy(Intent intent, zzcf zzcf, int i) {
        this.f7919 = intent;
        this.f7917 = zzcf;
        this.f7918 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9234() {
        if (this.f7919 != null) {
            this.f7917.startActivityForResult(this.f7919, this.f7918);
        }
    }
}
