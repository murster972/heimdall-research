package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzac implements zzf {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ GoogleApiClient.ConnectionCallbacks f7802;

    zzac(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.f7802 = connectionCallbacks;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9044(int i) {
        this.f7802.onConnectionSuspended(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9045(Bundle bundle) {
        this.f7802.onConnected(bundle);
    }
}
