package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.common.internal.zzr;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class Api<O extends ApiOptions> {

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f7426;

    /* renamed from: 靐  reason: contains not printable characters */
    private final zzh<?, O> f7427 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzi<?> f7428;

    /* renamed from: 齉  reason: contains not printable characters */
    private final zzf<?> f7429;

    /* renamed from: 龘  reason: contains not printable characters */
    private final zza<?, O> f7430;

    public interface ApiOptions {

        public interface HasAccountOptions extends HasOptions, NotRequiredOptions {
            /* renamed from: 龘  reason: contains not printable characters */
            Account m8508();
        }

        public interface HasGoogleSignInAccountOptions extends HasOptions {
            /* renamed from: 龘  reason: contains not printable characters */
            GoogleSignInAccount m8509();
        }

        public interface HasOptions extends ApiOptions {
        }

        public interface NotRequiredOptions extends ApiOptions {
        }

        public interface Optional extends HasOptions, NotRequiredOptions {
        }
    }

    public static abstract class zza<T extends zze, O> extends zzd<T, O> {
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract T m8510(Context context, Looper looper, zzr zzr, O o, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener);
    }

    public interface zzb {
    }

    public static class zzc<C extends zzb> {
    }

    public static abstract class zzd<T extends zzb, O> {
        /* renamed from: 龘  reason: contains not printable characters */
        public int m8511() {
            return MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public List<Scope> m8512(O o) {
            return Collections.emptyList();
        }
    }

    public interface zze extends zzb {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m8513();

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean m8514();

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean m8515();

        /* renamed from: ˈ  reason: contains not printable characters */
        String m8516();

        /* renamed from: ˑ  reason: contains not printable characters */
        boolean m8517();

        /* renamed from: ٴ  reason: contains not printable characters */
        boolean m8518();

        /* renamed from: ᐧ  reason: contains not printable characters */
        IBinder m8519();

        /* renamed from: 麤  reason: contains not printable characters */
        Intent m8520();

        /* renamed from: 齉  reason: contains not printable characters */
        boolean m8521();

        /* renamed from: 龘  reason: contains not printable characters */
        void m8522(zzan zzan, Set<Scope> set);

        /* renamed from: 龘  reason: contains not printable characters */
        void m8523(zzj zzj);

        /* renamed from: 龘  reason: contains not printable characters */
        void m8524(zzp zzp);

        /* renamed from: 龘  reason: contains not printable characters */
        void m8525(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);
    }

    public static final class zzf<C extends zze> extends zzc<C> {
    }

    public interface zzg<T extends IInterface> extends zzb {
    }

    public static abstract class zzh<T extends zzg, O> extends zzd<T, O> {
    }

    public static final class zzi<C extends zzg> extends zzc<C> {
    }

    public <C extends zze> Api(String str, zza<C, O> zza2, zzf<C> zzf2) {
        zzbq.m9121(zza2, (Object) "Cannot construct an Api with a null ClientBuilder");
        zzbq.m9121(zzf2, (Object) "Cannot construct an Api with a null ClientKey");
        this.f7426 = str;
        this.f7430 = zza2;
        this.f7429 = zzf2;
        this.f7428 = null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final zza<?, O> m8504() {
        zzbq.m9126(this.f7430 != null, (Object) "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.f7430;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m8505() {
        return this.f7426;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final zzc<?> m8506() {
        if (this.f7429 != null) {
            return this.f7429;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final zzd<?, O> m8507() {
        return this.f7430;
    }
}
