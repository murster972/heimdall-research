package com.google.android.gms.common;

import java.util.Arrays;

final class zzi extends zzh {

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f7953;

    zzi(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.f7953 = bArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final byte[] m9292() {
        return this.f7953;
    }
}
