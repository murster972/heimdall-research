package com.google.android.gms.common.internal;

public final class zzal {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f7834 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private static int f7835 = 15;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f7836;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f7837;

    public zzal(String str) {
        this(str, (String) null);
    }

    public zzal(String str, String str2) {
        zzbq.m9121(str, (Object) "log tag cannot be null");
        zzbq.m9118(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        this.f7837 = str;
        if (str2 == null || str2.length() <= 0) {
            this.f7836 = null;
        } else {
            this.f7836 = str2;
        }
    }
}
