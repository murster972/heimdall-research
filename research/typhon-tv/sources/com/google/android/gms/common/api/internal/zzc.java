package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzm;

public final class zzc<A extends zzm<? extends Result, Api.zzb>> extends zza {

    /* renamed from: 龘  reason: contains not printable characters */
    private A f7646;

    public zzc(int i, A a) {
        super(i);
        this.f7646 = a;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8820(Status status) {
        this.f7646.m8944(status);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8821(zzae zzae, boolean z) {
        zzae.m8615((BasePendingResult<? extends Result>) this.f7646, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8822(zzbo<?> zzbo) throws DeadObjectException {
        this.f7646.m8943(zzbo.m8799());
    }
}
