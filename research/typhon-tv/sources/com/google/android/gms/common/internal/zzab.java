package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import java.util.Set;

public abstract class zzab<T extends IInterface> extends zzd<T> implements Api.zze, zzaf {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Account f7799;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Set<Scope> f7800;

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzr f7801;

    protected zzab(Context context, Looper looper, int i, zzr zzr, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, zzag.m9060(context), GoogleApiAvailability.m8486(), i, zzr, (GoogleApiClient.ConnectionCallbacks) zzbq.m9120(connectionCallbacks), (GoogleApiClient.OnConnectionFailedListener) zzbq.m9120(onConnectionFailedListener));
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzab(Context context, Looper looper, zzag zzag, GoogleApiAvailability googleApiAvailability, int i, zzr zzr, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, zzag, googleApiAvailability, i, connectionCallbacks == null ? null : new zzac(connectionCallbacks), onConnectionFailedListener == null ? null : new zzad(onConnectionFailedListener), zzr.m4208());
        this.f7801 = zzr;
        this.f7799 = zzr.m4212();
        Set<Scope> r2 = zzr.m4211();
        Set<Scope> r3 = m9041(r2);
        for (Scope contains : r3) {
            if (!r2.contains(contains)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        this.f7800 = r3;
    }

    public final Account u_() {
        return this.f7799;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final zzr m9040() {
        return this.f7801;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Set<Scope> m9041(Set<Scope> set) {
        return set;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Set<Scope> m9042() {
        return this.f7800;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public zzc[] m9043() {
        return new zzc[0];
    }
}
