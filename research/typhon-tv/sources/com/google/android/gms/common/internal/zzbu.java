package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.internal.zzbfn;

public final class zzbu implements Parcelable.Creator<zzbt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int r7 = zzbfn.m10169(parcel);
        boolean z = false;
        boolean z2 = false;
        ConnectionResult connectionResult = null;
        IBinder iBinder = null;
        int i = 0;
        while (parcel.dataPosition() < r7) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbfn.m10166(parcel, readInt);
                    break;
                case 2:
                    iBinder = zzbfn.m10156(parcel, readInt);
                    break;
                case 3:
                    connectionResult = (ConnectionResult) zzbfn.m10171(parcel, readInt, ConnectionResult.CREATOR);
                    break;
                case 4:
                    z2 = zzbfn.m10168(parcel, readInt);
                    break;
                case 5:
                    z = zzbfn.m10168(parcel, readInt);
                    break;
                default:
                    zzbfn.m10164(parcel, readInt);
                    break;
            }
        }
        zzbfn.m10155(parcel, r7);
        return new zzbt(i, iBinder, connectionResult, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbt[i];
    }
}
