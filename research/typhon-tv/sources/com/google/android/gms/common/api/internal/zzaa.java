package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.internal.zzt;
import com.google.android.gms.common.zzf;
import com.google.android.gms.internal.zzbha;
import com.google.android.gms.internal.zzcxd;
import com.google.android.gms.internal.zzcxe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class zzaa implements zzcc {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Lock f7472;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Looper f7473;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzf f7474;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Queue<zzm<?, ?>> f7475 = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean f7476;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean f7477;

    /* renamed from: ˊ  reason: contains not printable characters */
    private zzad f7478;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public ConnectionResult f7479;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final Condition f7480;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final zzr f7481;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final boolean f7482;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final zzba f7483;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Map<Api.zzc<?>, zzz<?>> f7484 = new HashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private final zzbm f7485;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Map<Api<?>, Boolean> f7486;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<Api.zzc<?>, zzz<?>> f7487 = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Map<zzh<?>, ConnectionResult> f7488;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Map<zzh<?>, ConnectionResult> f7489;

    public zzaa(Context context, Lock lock, Looper looper, zzf zzf, Map<Api.zzc<?>, Api.zze> map, zzr zzr, Map<Api<?>, Boolean> map2, Api.zza<? extends zzcxd, zzcxe> zza, ArrayList<zzt> arrayList, zzba zzba, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.f7472 = lock;
        this.f7473 = looper;
        this.f7480 = lock.newCondition();
        this.f7474 = zzf;
        this.f7483 = zzba;
        this.f7486 = map2;
        this.f7481 = zzr;
        this.f7482 = z;
        HashMap hashMap = new HashMap();
        for (Api next : map2.keySet()) {
            hashMap.put(next.m8506(), next);
        }
        HashMap hashMap2 = new HashMap();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            zzt zzt = (zzt) obj;
            hashMap2.put(zzt.f7746, zzt);
        }
        boolean z5 = false;
        boolean z6 = true;
        boolean z7 = false;
        for (Map.Entry next2 : map.entrySet()) {
            Api api = (Api) hashMap.get(next2.getKey());
            Api.zze zze = (Api.zze) next2.getValue();
            if (zze.m8518()) {
                z2 = true;
                if (!this.f7486.get(api).booleanValue()) {
                    z3 = z6;
                    z4 = true;
                } else {
                    z3 = z6;
                    z4 = z7;
                }
            } else {
                z2 = z5;
                z3 = false;
                z4 = z7;
            }
            zzz zzz = new zzz(context, api, looper, zze, (zzt) hashMap2.get(api), zzr, zza);
            this.f7487.put((Api.zzc) next2.getKey(), zzz);
            if (zze.m8517()) {
                this.f7484.put((Api.zzc) next2.getKey(), zzz);
            }
            z5 = z2;
            z6 = z3;
            z7 = z4;
        }
        this.f7477 = z5 && !z6 && !z7;
        this.f7485 = zzbm.m8766();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0025 A[Catch:{ all -> 0x0045 }] */
    /* renamed from: ʽ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m8572() {
        /*
            r3 = this;
            r1 = 0
            java.util.concurrent.locks.Lock r0 = r3.f7472
            r0.lock()
            boolean r0 = r3.f7476     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x000e
            boolean r0 = r3.f7482     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0015
        L_0x000e:
            java.util.concurrent.locks.Lock r0 = r3.f7472
            r0.unlock()
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.api.internal.zzz<?>> r0 = r3.f7484     // Catch:{ all -> 0x0045 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x0045 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0045 }
        L_0x001f:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0045 }
            com.google.android.gms.common.api.Api$zzc r0 = (com.google.android.gms.common.api.Api.zzc) r0     // Catch:{ all -> 0x0045 }
            com.google.android.gms.common.ConnectionResult r0 = r3.m8587((com.google.android.gms.common.api.Api.zzc<?>) r0)     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0037
            boolean r0 = r0.m8478()     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x001f
        L_0x0037:
            java.util.concurrent.locks.Lock r0 = r3.f7472
            r0.unlock()
            r0 = r1
            goto L_0x0014
        L_0x003e:
            java.util.concurrent.locks.Lock r0 = r3.f7472
            r0.unlock()
            r0 = 1
            goto L_0x0014
        L_0x0045:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r3.f7472
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzaa.m8572():boolean");
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m8575() {
        if (this.f7481 == null) {
            this.f7483.f7566 = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(this.f7481.m4213());
        Map<Api<?>, zzt> r2 = this.f7481.m4206();
        for (Api next : r2.keySet()) {
            ConnectionResult r4 = m8602((Api<?>) next);
            if (r4 != null && r4.m8478()) {
                hashSet.addAll(r2.get(next).f7909);
            }
        }
        this.f7483.f7566 = hashSet;
    }

    /* access modifiers changed from: private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m8577() {
        while (!this.f7475.isEmpty()) {
            m8598(this.f7475.remove());
        }
        this.f7483.m8713((Bundle) null);
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final ConnectionResult m8579() {
        int i = 0;
        ConnectionResult connectionResult = null;
        int i2 = 0;
        ConnectionResult connectionResult2 = null;
        for (zzz next : this.f7487.values()) {
            Api r7 = next.m4181();
            ConnectionResult connectionResult3 = this.f7488.get(next.m4176());
            if (!connectionResult3.m8478() && (!this.f7486.get(r7).booleanValue() || connectionResult3.m8481() || this.f7474.m4230(connectionResult3.m8480()))) {
                if (connectionResult3.m8480() != 4 || !this.f7482) {
                    int r1 = r7.m8507().m8511();
                    if (connectionResult2 != null && i2 <= r1) {
                        r1 = i2;
                        connectionResult3 = connectionResult2;
                    }
                    i2 = r1;
                    connectionResult2 = connectionResult3;
                } else {
                    int r12 = r7.m8507().m8511();
                    if (connectionResult == null || i > r12) {
                        i = r12;
                        connectionResult = connectionResult3;
                    }
                }
            }
        }
        return (connectionResult2 == null || connectionResult == null || i2 <= i) ? connectionResult2 : connectionResult;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final <T extends zzm<? extends Result, ? extends Api.zzb>> boolean m8586(T t) {
        Api.zzc r0 = t.m8941();
        ConnectionResult r1 = m8587((Api.zzc<?>) r0);
        if (r1 == null || r1.m8480() != 4) {
            return false;
        }
        t.m8944(new Status(4, (String) null, this.f7485.m8773((zzh<?>) this.f7487.get(r0).m4176(), System.identityHashCode(this.f7483))));
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final ConnectionResult m8587(Api.zzc<?> zzc) {
        this.f7472.lock();
        try {
            zzz zzz = this.f7487.get(zzc);
            if (this.f7488 != null && zzz != null) {
                return this.f7488.get(zzz.m4176());
            }
            this.f7472.unlock();
            return null;
        } finally {
            this.f7472.unlock();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8593(zzz<?> zzz, ConnectionResult connectionResult) {
        return !connectionResult.m8478() && !connectionResult.m8481() && this.f7486.get(zzz.m4181()).booleanValue() && zzz.m9004().m8518() && this.f7474.m4230(connectionResult.m8480());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m8594() {
        this.f7472.lock();
        try {
            this.f7485.m8768();
            if (this.f7478 != null) {
                this.f7478.m8608();
                this.f7478 = null;
            }
            if (this.f7489 == null) {
                this.f7489 = new ArrayMap(this.f7484.size());
            }
            ConnectionResult connectionResult = new ConnectionResult(4);
            for (zzz<?> r0 : this.f7484.values()) {
                this.f7489.put(r0.m4176(), connectionResult);
            }
            if (this.f7488 != null) {
                this.f7488.putAll(this.f7489);
            }
        } finally {
            this.f7472.unlock();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m8595() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m8596() {
        this.f7472.lock();
        try {
            return this.f7488 == null && this.f7476;
        } finally {
            this.f7472.unlock();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectionResult m8597() {
        m8604();
        while (m8596()) {
            try {
                this.f7480.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, (PendingIntent) null);
            }
        }
        return m8599() ? ConnectionResult.f7411 : this.f7479 != null ? this.f7479 : new ConnectionResult(13, (PendingIntent) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T m8598(T t) {
        Api.zzc r0 = t.m8941();
        if (this.f7482 && m8586(t)) {
            return t;
        }
        this.f7483.f7563.m8906(t);
        return this.f7487.get(r0).m4177(t);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m8599() {
        this.f7472.lock();
        try {
            return this.f7488 != null && this.f7479 == null;
        } finally {
            this.f7472.unlock();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m8600() {
        this.f7472.lock();
        try {
            this.f7476 = false;
            this.f7488 = null;
            this.f7489 = null;
            if (this.f7478 != null) {
                this.f7478.m8608();
                this.f7478 = null;
            }
            this.f7479 = null;
            while (!this.f7475.isEmpty()) {
                zzm remove = this.f7475.remove();
                remove.m4200((zzdm) null);
                remove.m8533();
            }
            this.f7480.signalAll();
        } finally {
            this.f7472.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m8601(long j, TimeUnit timeUnit) {
        m8604();
        long nanos = timeUnit.toNanos(j);
        while (m8596()) {
            if (nanos <= 0) {
                try {
                    m8600();
                    return new ConnectionResult(14, (PendingIntent) null);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return new ConnectionResult(15, (PendingIntent) null);
                }
            } else {
                nanos = this.f7480.awaitNanos(nanos);
            }
        }
        return m8599() ? ConnectionResult.f7411 : this.f7479 != null ? this.f7479 : new ConnectionResult(13, (PendingIntent) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectionResult m8602(Api<?> api) {
        return m8587(api.m8506());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T m8603(T t) {
        if (this.f7482 && m8586(t)) {
            return t;
        }
        if (!m8599()) {
            this.f7475.add(t);
            return t;
        }
        this.f7483.f7563.m8906(t);
        return this.f7487.get(t.m8941()).m4183(t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8604() {
        this.f7472.lock();
        try {
            if (!this.f7476) {
                this.f7476 = true;
                this.f7488 = null;
                this.f7489 = null;
                this.f7478 = null;
                this.f7479 = null;
                this.f7485.m8771();
                this.f7485.m8774((Iterable<? extends GoogleApi<?>>) this.f7487.values()).m6105((Executor) new zzbha(this.f7473), new zzac(this));
                this.f7472.unlock();
            }
        } finally {
            this.f7472.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m8605(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m8606(zzcu zzcu) {
        this.f7472.lock();
        try {
            if (!this.f7476 || m8572()) {
                this.f7472.unlock();
                return false;
            }
            this.f7485.m8771();
            this.f7478 = new zzad(this, zzcu);
            this.f7485.m8774((Iterable<? extends GoogleApi<?>>) this.f7484.values()).m6105((Executor) new zzbha(this.f7473), this.f7478);
            this.f7472.unlock();
            return true;
        } catch (Throwable th) {
            this.f7472.unlock();
            throw th;
        }
    }
}
