package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.R;

public final class zzca {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f7859 = this.f7860.getResourcePackageName(R.string.common_google_play_services_unknown_issue);

    /* renamed from: 龘  reason: contains not printable characters */
    private final Resources f7860;

    public zzca(Context context) {
        zzbq.m9120(context);
        this.f7860 = context.getResources();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m9141(String str) {
        int identifier = this.f7860.getIdentifier(str, "string", this.f7859);
        if (identifier == 0) {
            return null;
        }
        return this.f7860.getString(identifier);
    }
}
