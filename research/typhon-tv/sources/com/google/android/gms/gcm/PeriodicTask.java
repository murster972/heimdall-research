package com.google.android.gms.gcm;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.gcm.Task;

public class PeriodicTask extends Task {
    public static final Parcelable.Creator<PeriodicTask> CREATOR = new zzh();

    /* renamed from: 靐  reason: contains not printable characters */
    protected long f3694;

    /* renamed from: 龘  reason: contains not printable characters */
    protected long f3695;

    public static class Builder extends Task.Builder {
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public long f8016 = -1;
        /* access modifiers changed from: private */

        /* renamed from: ٴ  reason: contains not printable characters */
        public long f8017 = -1;

        public Builder() {
            this.f8021 = true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m9388(long j) {
            this.f8017 = j;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m9395(boolean z) {
            this.f8021 = z;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public PeriodicTask m9390() {
            m9411();
            return new PeriodicTask(this, (zzh) null);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m9387(boolean z) {
            this.f8023 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9391(int i) {
            this.f8025 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9398(long j) {
            this.f8016 = j;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9392(Bundle bundle) {
            this.f8020 = bundle;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9393(Class<? extends GcmTaskService> cls) {
            this.f8022 = cls.getName();
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9394(String str) {
            this.f8024 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9386(boolean z) {
            this.f8018 = z;
            return this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m9403() {
            super.m9411();
            if (this.f8016 == -1) {
                throw new IllegalArgumentException("Must call setPeriod(long) to establish an execution interval for this periodic task.");
            } else if (this.f8016 <= 0) {
                throw new IllegalArgumentException(new StringBuilder(66).append("Period set cannot be less than or equal to 0: ").append(this.f8016).toString());
            } else if (this.f8017 == -1) {
                this.f8017 = (long) (((float) this.f8016) * 0.1f);
            } else if (this.f8017 > this.f8016) {
                this.f8017 = this.f8016;
            }
        }
    }

    @Deprecated
    private PeriodicTask(Parcel parcel) {
        super(parcel);
        this.f3695 = -1;
        this.f3694 = -1;
        this.f3695 = parcel.readLong();
        this.f3694 = Math.min(parcel.readLong(), this.f3695);
    }

    /* synthetic */ PeriodicTask(Parcel parcel, zzh zzh) {
        this(parcel);
    }

    private PeriodicTask(Builder builder) {
        super((Task.Builder) builder);
        this.f3695 = -1;
        this.f3694 = -1;
        this.f3695 = builder.f8016;
        this.f3694 = Math.min(builder.f8017, this.f3695);
    }

    /* synthetic */ PeriodicTask(Builder builder, zzh zzh) {
        this(builder);
    }

    public String toString() {
        String obj = super.toString();
        long r2 = m4235();
        return new StringBuilder(String.valueOf(obj).length() + 54).append(obj).append(" period=").append(r2).append(" flex=").append(m4234()).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(this.f3695);
        parcel.writeLong(this.f3694);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m4234() {
        return this.f3694;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m4235() {
        return this.f3695;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4236(Bundle bundle) {
        super.m4240(bundle);
        bundle.putLong("period", this.f3695);
        bundle.putLong("period_flex", this.f3694);
    }
}
