package com.google.android.gms.gcm;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.gcm.Task;

public class OneoffTask extends Task {
    public static final Parcelable.Creator<OneoffTask> CREATOR = new zzf();

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f3692;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f3693;

    public static class Builder extends Task.Builder {
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public long f8013 = -1;
        /* access modifiers changed from: private */

        /* renamed from: ٴ  reason: contains not printable characters */
        public long f8014 = -1;

        public Builder() {
            this.f8021 = false;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m9375(boolean z) {
            this.f8021 = z;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public OneoffTask m9370() {
            m9411();
            return new OneoffTask(this, (zzf) null);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m9368(boolean z) {
            this.f8023 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9371(int i) {
            this.f8025 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9378(long j, long j2) {
            this.f8013 = j;
            this.f8014 = j2;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9372(Bundle bundle) {
            this.f8020 = bundle;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9373(Class<? extends GcmTaskService> cls) {
            this.f8022 = cls.getName();
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9374(String str) {
            this.f8024 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m9367(boolean z) {
            this.f8018 = z;
            return this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m9383() {
            super.m9411();
            if (this.f8013 == -1 || this.f8014 == -1) {
                throw new IllegalArgumentException("Must specify an execution window using setExecutionWindow.");
            } else if (this.f8013 >= this.f8014) {
                throw new IllegalArgumentException("Window start must be shorter than window end.");
            }
        }
    }

    @Deprecated
    private OneoffTask(Parcel parcel) {
        super(parcel);
        this.f3693 = parcel.readLong();
        this.f3692 = parcel.readLong();
    }

    /* synthetic */ OneoffTask(Parcel parcel, zzf zzf) {
        this(parcel);
    }

    private OneoffTask(Builder builder) {
        super((Task.Builder) builder);
        this.f3693 = builder.f8013;
        this.f3692 = builder.f8014;
    }

    /* synthetic */ OneoffTask(Builder builder, zzf zzf) {
        this(builder);
    }

    public String toString() {
        String obj = super.toString();
        long r2 = m4232();
        return new StringBuilder(String.valueOf(obj).length() + 64).append(obj).append(" windowStart=").append(r2).append(" windowEnd=").append(m4231()).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(this.f3693);
        parcel.writeLong(this.f3692);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m4231() {
        return this.f3692;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m4232() {
        return this.f3693;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4233(Bundle bundle) {
        super.m4240(bundle);
        bundle.putLong("window_start", this.f3693);
        bundle.putLong("window_end", this.f3692);
    }
}
