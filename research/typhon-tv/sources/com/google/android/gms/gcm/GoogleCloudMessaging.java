package com.google.android.gms.gcm;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.android.gms.iid.zzl;
import java.util.concurrent.atomic.AtomicInteger;

public class GoogleCloudMessaging {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final AtomicInteger f8012 = new AtomicInteger(1);

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m9364(Context context) {
        String r0 = zzl.m9424(context);
        if (r0 != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(r0, 0);
                if (packageInfo != null) {
                    return packageInfo.versionCode;
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        return -1;
    }
}
