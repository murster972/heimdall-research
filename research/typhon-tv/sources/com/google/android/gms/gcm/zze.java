package com.google.android.gms.gcm;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.zzeu;

public final class zze extends zzeu implements zzd {
    zze(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gcm.INetworkTaskCallback");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m9415(int i) throws RemoteException {
        Parcel v_ = v_();
        v_.writeInt(i);
        m12298(2, v_);
    }
}
