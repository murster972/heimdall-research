package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.os.UserManager;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.iid.zzl;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class GcmNetworkManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static GcmNetworkManager f8000;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<String, Map<String, Boolean>> f8001 = new ArrayMap();

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f8002;

    /* renamed from: 麤  reason: contains not printable characters */
    private Boolean f8003;

    /* renamed from: 齉  reason: contains not printable characters */
    private final PendingIntent f8004;

    private GcmNetworkManager(Context context) {
        this.f8002 = context;
        this.f8004 = PendingIntent.getBroadcast(this.f8002, 0, new Intent().setPackage("com.google.example.invalidpackage"), 0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private final synchronized boolean m9349() {
        if (this.f8003 == null) {
            this.f8003 = Boolean.valueOf(this.f8002.checkPermission("android.permission.INTERACT_ACROSS_USERS", Process.myPid(), Process.myUid()) == 0);
        }
        return this.f8003.booleanValue();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean m9350(String str) {
        boolean z;
        List<ResolveInfo> r0;
        zzbq.m9121(str, (Object) "GcmTaskService must not be null.");
        PackageManager packageManager = this.f8002.getPackageManager();
        if (packageManager == null) {
            r0 = null;
        } else {
            boolean z2 = Build.VERSION.SDK_INT < 17;
            boolean z3 = Build.VERSION.SDK_INT < 24;
            if (z2 || (z3 && !m9349())) {
                z = true;
            } else {
                UserManager userManager = (UserManager) this.f8002.getSystemService("user");
                z = userManager == null || userManager.isUserRunning(Process.myUserHandle());
            }
            if (!z) {
                String str2 = str == null ? "unknown service" : str;
                Log.w("GcmNetworkManager", new StringBuilder(String.valueOf(str2).length() + 51).append("Dropping request for ").append(str2).append(" because user is shutting down").toString());
                r0 = null;
            } else {
                r0 = m9353(packageManager, str != null ? new Intent(GcmTaskService.SERVICE_ACTION_EXECUTE_TASK).setClassName(this.f8002, str) : new Intent(GcmTaskService.SERVICE_ACTION_EXECUTE_TASK).setPackage(this.f8002.getPackageName()), 0);
                if (r0 == null) {
                    r0 = Collections.emptyList();
                }
            }
        }
        if (r0 == null) {
            return false;
        }
        for (ResolveInfo resolveInfo : r0) {
            if (resolveInfo.serviceInfo.name.equals(str)) {
                return true;
            }
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 118).append("The GcmTaskService class you provided ").append(str).append(" does not seem to support receiving com.google.android.gms.gcm.ACTION_TASK_READY").toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final Intent m9351() {
        String r1 = zzl.m9424(this.f8002);
        int i = -1;
        if (r1 != null) {
            i = GoogleCloudMessaging.m9364(this.f8002);
        }
        if (r1 == null || i < 5000000) {
            Log.e("GcmNetworkManager", new StringBuilder(91).append("Google Play Services is not available, dropping GcmNetworkManager request. code=").append(i).toString());
            return null;
        }
        Intent intent = new Intent("com.google.android.gms.gcm.ACTION_SCHEDULE");
        intent.setPackage(r1);
        intent.putExtra("app", this.f8004);
        intent.putExtra("source", 4);
        intent.putExtra("source_version", 11910000);
        return intent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GcmNetworkManager m9352(Context context) {
        GcmNetworkManager gcmNetworkManager;
        synchronized (GcmNetworkManager.class) {
            if (f8000 == null) {
                f8000 = new GcmNetworkManager(context.getApplicationContext());
            }
            gcmNetworkManager = f8000;
        }
        return gcmNetworkManager;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<ResolveInfo> m9353(PackageManager packageManager, Intent intent, int i) {
        List<ResolveInfo> list = null;
        int i2 = 0;
        while (true) {
            if (i2 >= 2) {
                break;
            }
            list = packageManager.queryIntentServices(intent, 0);
            if (list == null ? true : list.isEmpty()) {
                i2++;
            } else if (i2 > 0) {
                Log.i("GcmNetworkManager", new StringBuilder(46).append("validation query succeeded on try #2").toString());
            }
        }
        return list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m9354(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Must provide a valid tag.");
        } else if (100 < str.length()) {
            throw new IllegalArgumentException("Tag is larger than max permissible tag length (100)");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m9355(String str, String str2) {
        Map map = this.f8001.get(str2);
        if (map != null) {
            if ((map.remove(str) != null) && map.isEmpty()) {
                this.f8001.remove(str2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized boolean m9356(String str) {
        return this.f8001.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final synchronized boolean m9357(String str, String str2) {
        boolean z;
        Map map = this.f8001.get(str2);
        if (map != null) {
            Boolean bool = (Boolean) map.get(str);
            z = bool == null ? false : bool.booleanValue();
        } else {
            z = false;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m9358(Task task) {
        if (m9350(task.m4239())) {
            Intent r0 = m9351();
            if (r0 != null) {
                Bundle extras = r0.getExtras();
                extras.putString("scheduler_action", "SCHEDULE_TASK");
                task.m4240(extras);
                r0.putExtras(extras);
                this.f8002.sendBroadcast(r0);
                Map map = this.f8001.get(task.m4239());
                if (map != null && map.containsKey(task.m4238())) {
                    map.put(task.m4238(), true);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m9359(String str, Class<? extends GcmTaskService> cls) {
        Intent r1;
        ComponentName componentName = new ComponentName(this.f8002, cls);
        m9354(str);
        if (m9350(componentName.getClassName()) && (r1 = m9351()) != null) {
            r1.putExtra("scheduler_action", "CANCEL_TASK");
            r1.putExtra("tag", str);
            r1.putExtra("component", componentName);
            this.f8002.sendBroadcast(r1);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized boolean m9360(String str, String str2) {
        Map map;
        map = this.f8001.get(str2);
        if (map == null) {
            map = new ArrayMap();
            this.f8001.put(str2, map);
        }
        return map.put(str, false) == null;
    }
}
