package com.google.android.gms.gcm;

import android.net.Uri;
import android.os.Bundle;
import java.util.List;

public class TaskParams {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Bundle f8026;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<Uri> f8027;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f8028;

    public TaskParams(String str, Bundle bundle, List<Uri> list) {
        this.f8028 = str;
        this.f8026 = bundle;
        this.f8027 = list;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Bundle m9412() {
        return this.f8026;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m9413() {
        return this.f8028;
    }
}
