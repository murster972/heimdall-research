package com.google.android.gms.gcm;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzb implements ThreadFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicInteger f8029 = new AtomicInteger(1);

    zzb(GcmTaskService gcmTaskService) {
    }

    public final Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, new StringBuilder(20).append("gcm-task#").append(this.f8029.getAndIncrement()).toString());
        thread.setPriority(4);
        return thread;
    }
}
