package com.google.android.gms.gcm;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.common.util.zzx;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.apache.commons.lang3.StringUtils;

public abstract class GcmTaskService extends Service {
    public static final String SERVICE_ACTION_EXECUTE_TASK = "com.google.android.gms.gcm.ACTION_TASK_READY";
    public static final String SERVICE_ACTION_INITIALIZE = "com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE";
    public static final String SERVICE_PERMISSION = "com.google.android.gms.permission.BIND_NETWORK_TASK_SERVICE";
    /* access modifiers changed from: private */
    public ComponentName componentName;
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    private ExecutorService zzais;
    /* access modifiers changed from: private */
    public int zzicf;
    private Messenger zzicg;
    /* access modifiers changed from: private */
    public GcmNetworkManager zzich;

    @TargetApi(21)
    class zza extends Handler {
        zza(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            Messenger messenger;
            if (!zzx.m9281(GcmTaskService.this, message.sendingUid, "com.google.android.gms")) {
                Log.e("GcmTaskService", "unable to verify presence of Google Play Services");
                return;
            }
            switch (message.what) {
                case 1:
                    Bundle data = message.getData();
                    if (!data.isEmpty() && (messenger = message.replyTo) != null) {
                        String string = data.getString("tag");
                        ArrayList parcelableArrayList = data.getParcelableArrayList("triggered_uris");
                        if (!GcmTaskService.this.zzhy(string)) {
                            GcmTaskService.this.zza(new zzb(string, messenger, data.getBundle("extras"), (List<Uri>) parcelableArrayList));
                            return;
                        }
                        return;
                    }
                    return;
                case 2:
                    if (Log.isLoggable("GcmTaskService", 3)) {
                        String valueOf = String.valueOf(message);
                        Log.d("GcmTaskService", new StringBuilder(String.valueOf(valueOf).length() + 45).append("ignoring unimplemented stop message for now: ").append(valueOf).toString());
                        return;
                    }
                    return;
                case 4:
                    GcmTaskService.this.onInitializeTasks();
                    return;
                default:
                    String valueOf2 = String.valueOf(message);
                    Log.e("GcmTaskService", new StringBuilder(String.valueOf(valueOf2).length() + 31).append("Unrecognized message received: ").append(valueOf2).toString());
                    return;
            }
        }
    }

    class zzb implements Runnable {

        /* renamed from: 连任  reason: contains not printable characters */
        private final Messenger f8007;

        /* renamed from: 靐  reason: contains not printable characters */
        private final Bundle f8008;

        /* renamed from: 麤  reason: contains not printable characters */
        private final zzd f8009;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<Uri> f8010;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f8011;

        zzb(String str, IBinder iBinder, Bundle bundle, List<Uri> list) {
            zzd zze;
            this.f8011 = str;
            if (iBinder == null) {
                zze = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gcm.INetworkTaskCallback");
                zze = queryLocalInterface instanceof zzd ? (zzd) queryLocalInterface : new zze(iBinder);
            }
            this.f8009 = zze;
            this.f8008 = bundle;
            this.f8010 = list;
            this.f8007 = null;
        }

        zzb(String str, Messenger messenger, Bundle bundle, List<Uri> list) {
            this.f8011 = str;
            this.f8007 = messenger;
            this.f8008 = bundle;
            this.f8010 = list;
            this.f8009 = null;
        }

        /* access modifiers changed from: private */
        /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
            return;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void m9361(int r7) {
            /*
                r6 = this;
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this
                java.lang.Object r1 = r0.lock
                monitor-enter(r1)
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ RemoteException -> 0x00d4 }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ RemoteException -> 0x00d4 }
                java.lang.String r2 = r6.f8011     // Catch:{ RemoteException -> 0x00d4 }
                com.google.android.gms.gcm.GcmTaskService r3 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ RemoteException -> 0x00d4 }
                android.content.ComponentName r3 = r3.componentName     // Catch:{ RemoteException -> 0x00d4 }
                java.lang.String r3 = r3.getClassName()     // Catch:{ RemoteException -> 0x00d4 }
                boolean r0 = r0.m9357(r2, r3)     // Catch:{ RemoteException -> 0x00d4 }
                if (r0 == 0) goto L_0x005d
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ all -> 0x00cb }
                java.lang.String r2 = r6.f8011     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r3 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r3 = r3.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r3 = r3.getClassName()     // Catch:{ all -> 0x00cb }
                r0.m9355(r2, r3)     // Catch:{ all -> 0x00cb }
                boolean r0 = r6.m9363()     // Catch:{ all -> 0x00cb }
                if (r0 != 0) goto L_0x005b
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r2 = r2.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r2 = r2.getClassName()     // Catch:{ all -> 0x00cb }
                boolean r0 = r0.m9356(r2)     // Catch:{ all -> 0x00cb }
                if (r0 != 0) goto L_0x005b
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                int r2 = r2.zzicf     // Catch:{ all -> 0x00cb }
                r0.stopSelf(r2)     // Catch:{ all -> 0x00cb }
            L_0x005b:
                monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            L_0x005c:
                return
            L_0x005d:
                boolean r0 = r6.m9363()     // Catch:{ RemoteException -> 0x00d4 }
                if (r0 == 0) goto L_0x00ce
                android.os.Messenger r0 = r6.f8007     // Catch:{ RemoteException -> 0x00d4 }
                android.os.Message r2 = android.os.Message.obtain()     // Catch:{ RemoteException -> 0x00d4 }
                r3 = 3
                r2.what = r3     // Catch:{ RemoteException -> 0x00d4 }
                r2.arg1 = r7     // Catch:{ RemoteException -> 0x00d4 }
                android.os.Bundle r3 = new android.os.Bundle     // Catch:{ RemoteException -> 0x00d4 }
                r3.<init>()     // Catch:{ RemoteException -> 0x00d4 }
                java.lang.String r4 = "component"
                com.google.android.gms.gcm.GcmTaskService r5 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ RemoteException -> 0x00d4 }
                android.content.ComponentName r5 = r5.componentName     // Catch:{ RemoteException -> 0x00d4 }
                r3.putParcelable(r4, r5)     // Catch:{ RemoteException -> 0x00d4 }
                java.lang.String r4 = "tag"
                java.lang.String r5 = r6.f8011     // Catch:{ RemoteException -> 0x00d4 }
                r3.putString(r4, r5)     // Catch:{ RemoteException -> 0x00d4 }
                r2.setData(r3)     // Catch:{ RemoteException -> 0x00d4 }
                r0.send(r2)     // Catch:{ RemoteException -> 0x00d4 }
            L_0x008d:
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ all -> 0x00cb }
                java.lang.String r2 = r6.f8011     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r3 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r3 = r3.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r3 = r3.getClassName()     // Catch:{ all -> 0x00cb }
                r0.m9355(r2, r3)     // Catch:{ all -> 0x00cb }
                boolean r0 = r6.m9363()     // Catch:{ all -> 0x00cb }
                if (r0 != 0) goto L_0x00c9
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r2 = r2.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r2 = r2.getClassName()     // Catch:{ all -> 0x00cb }
                boolean r0 = r0.m9356(r2)     // Catch:{ all -> 0x00cb }
                if (r0 != 0) goto L_0x00c9
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                int r2 = r2.zzicf     // Catch:{ all -> 0x00cb }
                r0.stopSelf(r2)     // Catch:{ all -> 0x00cb }
            L_0x00c9:
                monitor-exit(r1)     // Catch:{ all -> 0x00cb }
                goto L_0x005c
            L_0x00cb:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00cb }
                throw r0
            L_0x00ce:
                com.google.android.gms.gcm.zzd r0 = r6.f8009     // Catch:{ RemoteException -> 0x00d4 }
                r0.m9414(r7)     // Catch:{ RemoteException -> 0x00d4 }
                goto L_0x008d
            L_0x00d4:
                r0 = move-exception
                java.lang.String r2 = "GcmTaskService"
                java.lang.String r3 = "Error reporting result of operation to scheduler for "
                java.lang.String r0 = r6.f8011     // Catch:{ all -> 0x0131 }
                java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0131 }
                int r4 = r0.length()     // Catch:{ all -> 0x0131 }
                if (r4 == 0) goto L_0x012b
                java.lang.String r0 = r3.concat(r0)     // Catch:{ all -> 0x0131 }
            L_0x00eb:
                android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0131 }
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ all -> 0x00cb }
                java.lang.String r2 = r6.f8011     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r3 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r3 = r3.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r3 = r3.getClassName()     // Catch:{ all -> 0x00cb }
                r0.m9355(r2, r3)     // Catch:{ all -> 0x00cb }
                boolean r0 = r6.m9363()     // Catch:{ all -> 0x00cb }
                if (r0 != 0) goto L_0x00c9
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r0 = r0.zzich     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r2 = r2.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r2 = r2.getClassName()     // Catch:{ all -> 0x00cb }
                boolean r0 = r0.m9356(r2)     // Catch:{ all -> 0x00cb }
                if (r0 != 0) goto L_0x00c9
                com.google.android.gms.gcm.GcmTaskService r0 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                int r2 = r2.zzicf     // Catch:{ all -> 0x00cb }
                r0.stopSelf(r2)     // Catch:{ all -> 0x00cb }
                goto L_0x00c9
            L_0x012b:
                java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0131 }
                r0.<init>(r3)     // Catch:{ all -> 0x0131 }
                goto L_0x00eb
            L_0x0131:
                r0 = move-exception
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r2 = r2.zzich     // Catch:{ all -> 0x00cb }
                java.lang.String r3 = r6.f8011     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r4 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r4 = r4.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r4 = r4.getClassName()     // Catch:{ all -> 0x00cb }
                r2.m9355(r3, r4)     // Catch:{ all -> 0x00cb }
                boolean r2 = r6.m9363()     // Catch:{ all -> 0x00cb }
                if (r2 != 0) goto L_0x016e
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmNetworkManager r2 = r2.zzich     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r3 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                android.content.ComponentName r3 = r3.componentName     // Catch:{ all -> 0x00cb }
                java.lang.String r3 = r3.getClassName()     // Catch:{ all -> 0x00cb }
                boolean r2 = r2.m9356(r3)     // Catch:{ all -> 0x00cb }
                if (r2 != 0) goto L_0x016e
                com.google.android.gms.gcm.GcmTaskService r2 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                com.google.android.gms.gcm.GcmTaskService r3 = com.google.android.gms.gcm.GcmTaskService.this     // Catch:{ all -> 0x00cb }
                int r3 = r3.zzicf     // Catch:{ all -> 0x00cb }
                r2.stopSelf(r3)     // Catch:{ all -> 0x00cb }
            L_0x016e:
                throw r0     // Catch:{ all -> 0x00cb }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.gcm.GcmTaskService.zzb.m9361(int):void");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private final boolean m9363() {
            return this.f8007 != null;
        }

        public final void run() {
            m9361(GcmTaskService.this.onRunTask(new TaskParams(this.f8011, this.f8008, this.f8010)));
        }
    }

    /* access modifiers changed from: private */
    public final void zza(zzb zzb2) {
        try {
            this.zzais.execute(zzb2);
        } catch (RejectedExecutionException e) {
            Log.e("GcmTaskService", "Executor is shutdown. onDestroy was called but main looper had an unprocessed start task message. The task will be retried with backoff delay.", e);
            zzb2.m9361(1);
        }
    }

    private final void zzdp(int i) {
        synchronized (this.lock) {
            this.zzicf = i;
            if (!this.zzich.m9356(this.componentName.getClassName())) {
                stopSelf(this.zzicf);
            }
        }
    }

    /* access modifiers changed from: private */
    public final boolean zzhy(String str) {
        boolean z;
        synchronized (this.lock) {
            z = !this.zzich.m9360(str, this.componentName.getClassName());
            if (z) {
                String packageName = getPackageName();
                Log.w("GcmTaskService", new StringBuilder(String.valueOf(packageName).length() + 44 + String.valueOf(str).length()).append(packageName).append(StringUtils.SPACE).append(str).append(": Task already running, won't start another").toString());
            }
        }
        return z;
    }

    public IBinder onBind(Intent intent) {
        if (intent == null || !zzq.m9265() || !SERVICE_ACTION_EXECUTE_TASK.equals(intent.getAction())) {
            return null;
        }
        return this.zzicg.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        this.zzich = GcmNetworkManager.m9352((Context) this);
        this.zzais = Executors.newFixedThreadPool(2, new zzb(this));
        this.zzicg = new Messenger(new zza(Looper.getMainLooper()));
        this.componentName = new ComponentName(this, getClass());
    }

    public void onDestroy() {
        super.onDestroy();
        List<Runnable> shutdownNow = this.zzais.shutdownNow();
        if (!shutdownNow.isEmpty()) {
            Log.e("GcmTaskService", new StringBuilder(79).append("Shutting down, but not all tasks are finished executing. Remaining: ").append(shutdownNow.size()).toString());
        }
    }

    public void onInitializeTasks() {
    }

    public abstract int onRunTask(TaskParams taskParams);

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            zzdp(i2);
        } else {
            try {
                intent.setExtrasClassLoader(PendingCallback.class.getClassLoader());
                String action = intent.getAction();
                if (SERVICE_ACTION_EXECUTE_TASK.equals(action)) {
                    String stringExtra = intent.getStringExtra("tag");
                    Parcelable parcelableExtra = intent.getParcelableExtra("callback");
                    Bundle bundleExtra = intent.getBundleExtra("extras");
                    ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("triggered_uris");
                    if (!(parcelableExtra instanceof PendingCallback)) {
                        String packageName = getPackageName();
                        Log.e("GcmTaskService", new StringBuilder(String.valueOf(packageName).length() + 47 + String.valueOf(stringExtra).length()).append(packageName).append(StringUtils.SPACE).append(stringExtra).append(": Could not process request, invalid callback.").toString());
                    } else if (zzhy(stringExtra)) {
                        zzdp(i2);
                    } else {
                        zza(new zzb(stringExtra, ((PendingCallback) parcelableExtra).f8015, bundleExtra, (List<Uri>) parcelableArrayListExtra));
                    }
                } else if (SERVICE_ACTION_INITIALIZE.equals(action)) {
                    onInitializeTasks();
                } else {
                    Log.e("GcmTaskService", new StringBuilder(String.valueOf(action).length() + 37).append("Unknown action received ").append(action).append(", terminating").toString());
                }
                zzdp(i2);
            } finally {
                zzdp(i2);
            }
        }
        return 2;
    }
}
