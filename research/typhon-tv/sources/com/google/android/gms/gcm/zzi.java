package com.google.android.gms.gcm;

import android.os.Bundle;

public final class zzi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static zzi f8030 = new zzi(1, 30, 3600);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final zzi f8031 = new zzi(0, 30, 3600);

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f8032 = 3600;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f8033 = 30;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f8034;

    private zzi(int i, int i2, int i3) {
        this.f8034 = i;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzi)) {
            return false;
        }
        zzi zzi = (zzi) obj;
        return zzi.f8034 == this.f8034 && zzi.f8033 == this.f8033 && zzi.f8032 == this.f8032;
    }

    public final int hashCode() {
        return (((((this.f8034 + 1) ^ 1000003) * 1000003) ^ this.f8033) * 1000003) ^ this.f8032;
    }

    public final String toString() {
        int i = this.f8034;
        int i2 = this.f8033;
        return new StringBuilder(74).append("policy=").append(i).append(" initial_backoff=").append(i2).append(" maximum_backoff=").append(this.f8032).toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m9416() {
        return this.f8033;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m9417() {
        return this.f8032;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m9418() {
        return this.f8034;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Bundle m9419(Bundle bundle) {
        bundle.putInt("retry_policy", this.f8034);
        bundle.putInt("initial_backoff_seconds", this.f8033);
        bundle.putInt("maximum_backoff_seconds", this.f8032);
        return bundle;
    }
}
