package com.google.android.gms.gcm;

import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;

public class Task implements ReflectedParcelable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f3696;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f3697;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final zzi f3698;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Bundle f3699;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f3700;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f3701;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f3702;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f3703;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f3704;

    public static abstract class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        protected boolean f8018;

        /* renamed from: ʼ  reason: contains not printable characters */
        protected zzi f8019 = zzi.f8031;

        /* renamed from: ʽ  reason: contains not printable characters */
        protected Bundle f8020;

        /* renamed from: 连任  reason: contains not printable characters */
        protected boolean f8021;

        /* renamed from: 靐  reason: contains not printable characters */
        protected String f8022;

        /* renamed from: 麤  reason: contains not printable characters */
        protected boolean f8023;

        /* renamed from: 齉  reason: contains not printable characters */
        protected String f8024;

        /* renamed from: 龘  reason: contains not printable characters */
        protected int f8025;

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract Builder m9404(boolean z);

        /* renamed from: 连任  reason: contains not printable characters */
        public abstract Builder m9405(boolean z);

        /* renamed from: 靐  reason: contains not printable characters */
        public abstract Builder m9406(int i);

        /* renamed from: 靐  reason: contains not printable characters */
        public abstract Builder m9407(Bundle bundle);

        /* renamed from: 靐  reason: contains not printable characters */
        public abstract Builder m9408(Class<? extends GcmTaskService> cls);

        /* renamed from: 靐  reason: contains not printable characters */
        public abstract Builder m9409(String str);

        /* renamed from: 麤  reason: contains not printable characters */
        public abstract Builder m9410(boolean z);

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m9411() {
            zzbq.m9117(this.f8022 != null, "Must provide an endpoint for this task by calling setService(ComponentName).");
            GcmNetworkManager.m9354(this.f8024);
            zzi zzi = this.f8019;
            if (zzi != null) {
                int r2 = zzi.m9418();
                if (r2 == 1 || r2 == 0) {
                    int r3 = zzi.m9416();
                    int r4 = zzi.m9417();
                    if (r2 == 0 && r3 < 0) {
                        throw new IllegalArgumentException(new StringBuilder(52).append("InitialBackoffSeconds can't be negative: ").append(r3).toString());
                    } else if (r2 == 1 && r3 < 10) {
                        throw new IllegalArgumentException("RETRY_POLICY_LINEAR must have an initial backoff at least 10 seconds.");
                    } else if (r4 < r3) {
                        throw new IllegalArgumentException(new StringBuilder(77).append("MaximumBackoffSeconds must be greater than InitialBackoffSeconds: ").append(zzi.m9417()).toString());
                    }
                } else {
                    throw new IllegalArgumentException(new StringBuilder(45).append("Must provide a valid RetryPolicy: ").append(r2).toString());
                }
            }
            if (this.f8021) {
                Task.m4237(this.f8020);
            }
        }
    }

    @Deprecated
    Task(Parcel parcel) {
        boolean z = true;
        Log.e("Task", "Constructing a Task object using a parcel.");
        this.f3704 = parcel.readString();
        this.f3701 = parcel.readString();
        this.f3703 = parcel.readInt() == 1;
        this.f3702 = parcel.readInt() != 1 ? false : z;
        this.f3700 = 2;
        this.f3696 = false;
        this.f3697 = false;
        this.f3698 = zzi.f8031;
        this.f3699 = null;
    }

    Task(Builder builder) {
        this.f3704 = builder.f8022;
        this.f3701 = builder.f8024;
        this.f3703 = builder.f8023;
        this.f3702 = builder.f8021;
        this.f3700 = builder.f8025;
        this.f3696 = builder.f8018;
        this.f3697 = false;
        this.f3699 = builder.f8020;
        this.f3698 = builder.f8019 != null ? builder.f8019 : zzi.f8031;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m4237(Bundle bundle) {
        if (bundle != null) {
            Parcel obtain = Parcel.obtain();
            bundle.writeToParcel(obtain, 0);
            int dataSize = obtain.dataSize();
            obtain.recycle();
            if (dataSize > 10240) {
                throw new IllegalArgumentException(new StringBuilder(String.valueOf("Extras exceeding maximum size(10240 bytes): ").length() + 11).append("Extras exceeding maximum size(10240 bytes): ").append(dataSize).toString());
            }
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (!((obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Double) || (obj instanceof String) || (obj instanceof Boolean))) {
                    if (obj instanceof Bundle) {
                        m4237((Bundle) obj);
                    } else {
                        throw new IllegalArgumentException("Only the following extra parameter types are supported: Integer, Long, Double, String, Boolean, and nested Bundles with the same restrictions.");
                    }
                }
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.f3704);
        parcel.writeString(this.f3701);
        parcel.writeInt(this.f3703 ? 1 : 0);
        if (!this.f3702) {
            i2 = 0;
        }
        parcel.writeInt(i2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m4238() {
        return this.f3701;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m4239() {
        return this.f3704;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m4240(Bundle bundle) {
        bundle.putString("tag", this.f3701);
        bundle.putBoolean("update_current", this.f3703);
        bundle.putBoolean("persisted", this.f3702);
        bundle.putString(NotificationCompat.CATEGORY_SERVICE, this.f3704);
        bundle.putInt("requiredNetwork", this.f3700);
        bundle.putBoolean("requiresCharging", this.f3696);
        bundle.putBoolean("requiresIdle", false);
        bundle.putBundle("retryStrategy", this.f3698.m9419(new Bundle()));
        bundle.putBundle("extras", this.f3699);
    }
}
