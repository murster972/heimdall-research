package com.google.android.exoplayer2.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextRenderer;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.util.Assertions;
import java.util.List;

@TargetApi(16)
public final class SimpleExoPlayerView extends FrameLayout {
    private static final int SURFACE_TYPE_NONE = 0;
    private static final int SURFACE_TYPE_SURFACE_VIEW = 1;
    private static final int SURFACE_TYPE_TEXTURE_VIEW = 2;
    private final ImageView artworkView;
    private final ComponentListener componentListener;
    /* access modifiers changed from: private */
    public final AspectRatioFrameLayout contentFrame;
    private final PlaybackControlView controller;
    private boolean controllerAutoShow;
    private boolean controllerHideOnTouch;
    private int controllerShowTimeoutMs;
    private Bitmap defaultArtwork;
    private final FrameLayout overlayFrameLayout;
    private SimpleExoPlayer player;
    /* access modifiers changed from: private */
    public final View shutterView;
    /* access modifiers changed from: private */
    public final SubtitleView subtitleView;
    private final View surfaceView;
    private boolean useArtwork;
    private boolean useController;

    private final class ComponentListener implements Player.EventListener, SimpleExoPlayer.VideoListener, TextRenderer.Output {
        private ComponentListener() {
        }

        public void onCues(List<Cue> list) {
            if (SimpleExoPlayerView.this.subtitleView != null) {
                SimpleExoPlayerView.this.subtitleView.onCues(list);
            }
        }

        public void onLoadingChanged(boolean z) {
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }

        public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        }

        public void onPlayerStateChanged(boolean z, int i) {
            SimpleExoPlayerView.this.maybeShowController(false);
        }

        public void onPositionDiscontinuity() {
        }

        public void onRenderedFirstFrame() {
            if (SimpleExoPlayerView.this.shutterView != null) {
                SimpleExoPlayerView.this.shutterView.setVisibility(4);
            }
        }

        public void onRepeatModeChanged(int i) {
        }

        public void onTimelineChanged(Timeline timeline, Object obj) {
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            SimpleExoPlayerView.this.updateForCurrentTrackSelections();
        }

        public void onVideoSizeChanged(int i, int i2, int i3, float f) {
            if (SimpleExoPlayerView.this.contentFrame != null) {
                SimpleExoPlayerView.this.contentFrame.setAspectRatio(i2 == 0 ? 1.0f : (((float) i) * f) / ((float) i2));
            }
        }
    }

    public SimpleExoPlayerView(Context context) {
        this(context, (AttributeSet) null);
    }

    public SimpleExoPlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: type inference failed for: r21v44 */
    /* JADX WARNING: type inference failed for: r0v87, types: [android.view.SurfaceView] */
    /* JADX WARNING: type inference failed for: r0v88, types: [android.view.TextureView] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SimpleExoPlayerView(android.content.Context r26, android.util.AttributeSet r27, int r28) {
        /*
            r25 = this;
            r25.<init>(r26, r27, r28)
            boolean r21 = r25.isInEditMode()
            if (r21 == 0) goto L_0x0073
            r21 = 0
            r0 = r21
            r1 = r25
            r1.contentFrame = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.shutterView = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.surfaceView = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.artworkView = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.subtitleView = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.controller = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.componentListener = r0
            r21 = 0
            r0 = r21
            r1 = r25
            r1.overlayFrameLayout = r0
            android.widget.ImageView r13 = new android.widget.ImageView
            r0 = r26
            r13.<init>(r0)
            int r21 = com.google.android.exoplayer2.util.Util.SDK_INT
            r22 = 23
            r0 = r21
            r1 = r22
            if (r0 < r1) goto L_0x0069
            android.content.res.Resources r21 = r25.getResources()
            r0 = r21
            configureEditModeLogoV23(r0, r13)
        L_0x0063:
            r0 = r25
            r0.addView(r13)
        L_0x0068:
            return
        L_0x0069:
            android.content.res.Resources r21 = r25.getResources()
            r0 = r21
            configureEditModeLogo(r0, r13)
            goto L_0x0063
        L_0x0073:
            int r16 = com.google.android.exoplayer2.ui.R.layout.exo_simple_player_view
            r19 = 1
            r12 = 0
            r20 = 1
            r18 = 1
            r17 = 0
            r10 = 5000(0x1388, float:7.006E-42)
            r7 = 1
            r6 = 1
            if (r27 == 0) goto L_0x00f1
            android.content.res.Resources$Theme r21 = r26.getTheme()
            int[] r22 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView
            r23 = 0
            r24 = 0
            r0 = r21
            r1 = r27
            r2 = r22
            r3 = r23
            r4 = r24
            android.content.res.TypedArray r5 = r0.obtainStyledAttributes(r1, r2, r3, r4)
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_player_layout_id     // Catch:{ all -> 0x0256 }
            r0 = r21
            r1 = r16
            int r16 = r5.getResourceId(r0, r1)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_use_artwork     // Catch:{ all -> 0x0256 }
            r0 = r21
            r1 = r19
            boolean r19 = r5.getBoolean(r0, r1)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_default_artwork     // Catch:{ all -> 0x0256 }
            r0 = r21
            int r12 = r5.getResourceId(r0, r12)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_use_controller     // Catch:{ all -> 0x0256 }
            r0 = r21
            r1 = r20
            boolean r20 = r5.getBoolean(r0, r1)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_surface_type     // Catch:{ all -> 0x0256 }
            r0 = r21
            r1 = r18
            int r18 = r5.getInt(r0, r1)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_resize_mode     // Catch:{ all -> 0x0256 }
            r0 = r21
            r1 = r17
            int r17 = r5.getInt(r0, r1)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_show_timeout     // Catch:{ all -> 0x0256 }
            r0 = r21
            int r10 = r5.getInt(r0, r10)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_hide_on_touch     // Catch:{ all -> 0x0256 }
            r0 = r21
            boolean r7 = r5.getBoolean(r0, r7)     // Catch:{ all -> 0x0256 }
            int r21 = com.google.android.exoplayer2.ui.R.styleable.SimpleExoPlayerView_auto_show     // Catch:{ all -> 0x0256 }
            r0 = r21
            boolean r6 = r5.getBoolean(r0, r6)     // Catch:{ all -> 0x0256 }
            r5.recycle()
        L_0x00f1:
            android.view.LayoutInflater r21 = android.view.LayoutInflater.from(r26)
            r0 = r21
            r1 = r16
            r2 = r25
            r0.inflate(r1, r2)
            com.google.android.exoplayer2.ui.SimpleExoPlayerView$ComponentListener r21 = new com.google.android.exoplayer2.ui.SimpleExoPlayerView$ComponentListener
            r22 = 0
            r0 = r21
            r1 = r25
            r2 = r22
            r0.<init>()
            r0 = r21
            r1 = r25
            r1.componentListener = r0
            r21 = 262144(0x40000, float:3.67342E-40)
            r0 = r25
            r1 = r21
            r0.setDescendantFocusability(r1)
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_content_frame
            r0 = r25
            r1 = r21
            android.view.View r21 = r0.findViewById(r1)
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r21 = (com.google.android.exoplayer2.ui.AspectRatioFrameLayout) r21
            r0 = r21
            r1 = r25
            r1.contentFrame = r0
            r0 = r25
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.contentFrame
            r21 = r0
            if (r21 == 0) goto L_0x0141
            r0 = r25
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.contentFrame
            r21 = r0
            r0 = r21
            r1 = r17
            setResizeModeRaw(r0, r1)
        L_0x0141:
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_shutter
            r0 = r25
            r1 = r21
            android.view.View r21 = r0.findViewById(r1)
            r0 = r21
            r1 = r25
            r1.shutterView = r0
            r0 = r25
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.contentFrame
            r21 = r0
            if (r21 == 0) goto L_0x0266
            if (r18 == 0) goto L_0x0266
            android.view.ViewGroup$LayoutParams r14 = new android.view.ViewGroup$LayoutParams
            r21 = -1
            r22 = -1
            r0 = r21
            r1 = r22
            r14.<init>(r0, r1)
            r21 = 2
            r0 = r18
            r1 = r21
            if (r0 != r1) goto L_0x025b
            android.view.TextureView r21 = new android.view.TextureView
            r0 = r21
            r1 = r26
            r0.<init>(r1)
        L_0x0179:
            r0 = r21
            r1 = r25
            r1.surfaceView = r0
            r0 = r25
            android.view.View r0 = r0.surfaceView
            r21 = r0
            r0 = r21
            r0.setLayoutParams(r14)
            r0 = r25
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.contentFrame
            r21 = r0
            r0 = r25
            android.view.View r0 = r0.surfaceView
            r22 = r0
            r23 = 0
            r21.addView(r22, r23)
        L_0x019b:
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_overlay
            r0 = r25
            r1 = r21
            android.view.View r21 = r0.findViewById(r1)
            android.widget.FrameLayout r21 = (android.widget.FrameLayout) r21
            r0 = r21
            r1 = r25
            r1.overlayFrameLayout = r0
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_artwork
            r0 = r25
            r1 = r21
            android.view.View r21 = r0.findViewById(r1)
            android.widget.ImageView r21 = (android.widget.ImageView) r21
            r0 = r21
            r1 = r25
            r1.artworkView = r0
            if (r19 == 0) goto L_0x0270
            r0 = r25
            android.widget.ImageView r0 = r0.artworkView
            r21 = r0
            if (r21 == 0) goto L_0x0270
            r21 = 1
        L_0x01cb:
            r0 = r21
            r1 = r25
            r1.useArtwork = r0
            if (r12 == 0) goto L_0x01e3
            android.content.res.Resources r21 = r26.getResources()
            r0 = r21
            android.graphics.Bitmap r21 = android.graphics.BitmapFactory.decodeResource(r0, r12)
            r0 = r21
            r1 = r25
            r1.defaultArtwork = r0
        L_0x01e3:
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_subtitles
            r0 = r25
            r1 = r21
            android.view.View r21 = r0.findViewById(r1)
            com.google.android.exoplayer2.ui.SubtitleView r21 = (com.google.android.exoplayer2.ui.SubtitleView) r21
            r0 = r21
            r1 = r25
            r1.subtitleView = r0
            r0 = r25
            com.google.android.exoplayer2.ui.SubtitleView r0 = r0.subtitleView
            r21 = r0
            if (r21 == 0) goto L_0x020f
            r0 = r25
            com.google.android.exoplayer2.ui.SubtitleView r0 = r0.subtitleView
            r21 = r0
            r21.setUserDefaultStyle()
            r0 = r25
            com.google.android.exoplayer2.ui.SubtitleView r0 = r0.subtitleView
            r21 = r0
            r21.setUserDefaultTextSize()
        L_0x020f:
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_controller
            r0 = r25
            r1 = r21
            android.view.View r11 = r0.findViewById(r1)
            com.google.android.exoplayer2.ui.PlaybackControlView r11 = (com.google.android.exoplayer2.ui.PlaybackControlView) r11
            int r21 = com.google.android.exoplayer2.ui.R.id.exo_controller_placeholder
            r0 = r25
            r1 = r21
            android.view.View r9 = r0.findViewById(r1)
            if (r11 == 0) goto L_0x0274
            r0 = r25
            r0.controller = r11
        L_0x022b:
            r0 = r25
            com.google.android.exoplayer2.ui.PlaybackControlView r0 = r0.controller
            r21 = r0
            if (r21 == 0) goto L_0x02c0
        L_0x0233:
            r0 = r25
            r0.controllerShowTimeoutMs = r10
            r0 = r25
            r0.controllerHideOnTouch = r7
            r0 = r25
            r0.controllerAutoShow = r6
            if (r20 == 0) goto L_0x02c3
            r0 = r25
            com.google.android.exoplayer2.ui.PlaybackControlView r0 = r0.controller
            r21 = r0
            if (r21 == 0) goto L_0x02c3
            r21 = 1
        L_0x024b:
            r0 = r21
            r1 = r25
            r1.useController = r0
            r25.hideController()
            goto L_0x0068
        L_0x0256:
            r21 = move-exception
            r5.recycle()
            throw r21
        L_0x025b:
            android.view.SurfaceView r21 = new android.view.SurfaceView
            r0 = r21
            r1 = r26
            r0.<init>(r1)
            goto L_0x0179
        L_0x0266:
            r21 = 0
            r0 = r21
            r1 = r25
            r1.surfaceView = r0
            goto L_0x019b
        L_0x0270:
            r21 = 0
            goto L_0x01cb
        L_0x0274:
            if (r9 == 0) goto L_0x02b6
            com.google.android.exoplayer2.ui.PlaybackControlView r21 = new com.google.android.exoplayer2.ui.PlaybackControlView
            r22 = 0
            r23 = 0
            r0 = r21
            r1 = r26
            r2 = r22
            r3 = r23
            r4 = r27
            r0.<init>(r1, r2, r3, r4)
            r0 = r21
            r1 = r25
            r1.controller = r0
            r0 = r25
            com.google.android.exoplayer2.ui.PlaybackControlView r0 = r0.controller
            r21 = r0
            android.view.ViewGroup$LayoutParams r22 = r9.getLayoutParams()
            r21.setLayoutParams(r22)
            android.view.ViewParent r15 = r9.getParent()
            android.view.ViewGroup r15 = (android.view.ViewGroup) r15
            int r8 = r15.indexOfChild(r9)
            r15.removeView(r9)
            r0 = r25
            com.google.android.exoplayer2.ui.PlaybackControlView r0 = r0.controller
            r21 = r0
            r0 = r21
            r15.addView(r0, r8)
            goto L_0x022b
        L_0x02b6:
            r21 = 0
            r0 = r21
            r1 = r25
            r1.controller = r0
            goto L_0x022b
        L_0x02c0:
            r10 = 0
            goto L_0x0233
        L_0x02c3:
            r21 = 0
            goto L_0x024b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.SimpleExoPlayerView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    private static void configureEditModeLogo(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(R.drawable.exo_edit_mode_logo));
        imageView.setBackgroundColor(resources.getColor(R.color.exo_edit_mode_background_color));
    }

    @TargetApi(23)
    private static void configureEditModeLogoV23(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(R.drawable.exo_edit_mode_logo, (Resources.Theme) null));
        imageView.setBackgroundColor(resources.getColor(R.color.exo_edit_mode_background_color, (Resources.Theme) null));
    }

    private void hideArtwork() {
        if (this.artworkView != null) {
            this.artworkView.setImageResource(17170445);
            this.artworkView.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void maybeShowController(boolean z) {
        if (this.useController) {
            boolean z2 = this.controller.isVisible() && this.controller.getShowTimeoutMs() <= 0;
            boolean shouldShowControllerIndefinitely = shouldShowControllerIndefinitely();
            if (z || z2 || shouldShowControllerIndefinitely) {
                showController(shouldShowControllerIndefinitely);
            }
        }
    }

    private boolean setArtworkFromBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return false;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= 0 || height <= 0) {
            return false;
        }
        if (this.contentFrame != null) {
            this.contentFrame.setAspectRatio(((float) width) / ((float) height));
        }
        this.artworkView.setImageBitmap(bitmap);
        this.artworkView.setVisibility(0);
        return true;
    }

    private boolean setArtworkFromMetadata(Metadata metadata) {
        for (int i = 0; i < metadata.length(); i++) {
            Metadata.Entry entry = metadata.get(i);
            if (entry instanceof ApicFrame) {
                byte[] bArr = ((ApicFrame) entry).pictureData;
                return setArtworkFromBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
            }
        }
        return false;
    }

    private static void setResizeModeRaw(AspectRatioFrameLayout aspectRatioFrameLayout, int i) {
        aspectRatioFrameLayout.setResizeMode(i);
    }

    private boolean shouldShowControllerIndefinitely() {
        if (this.player == null) {
            return true;
        }
        int playbackState = this.player.getPlaybackState();
        return this.controllerAutoShow && (playbackState == 1 || playbackState == 4 || !this.player.getPlayWhenReady());
    }

    private void showController(boolean z) {
        if (this.useController) {
            this.controller.setShowTimeoutMs(z ? 0 : this.controllerShowTimeoutMs);
            this.controller.show();
        }
    }

    public static void switchTargetView(SimpleExoPlayer simpleExoPlayer, SimpleExoPlayerView simpleExoPlayerView, SimpleExoPlayerView simpleExoPlayerView2) {
        if (simpleExoPlayerView != simpleExoPlayerView2) {
            if (simpleExoPlayerView2 != null) {
                simpleExoPlayerView2.setPlayer(simpleExoPlayer);
            }
            if (simpleExoPlayerView != null) {
                simpleExoPlayerView.setPlayer((SimpleExoPlayer) null);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateForCurrentTrackSelections() {
        if (this.player != null) {
            TrackSelectionArray currentTrackSelections = this.player.getCurrentTrackSelections();
            int i = 0;
            while (i < currentTrackSelections.length) {
                if (this.player.getRendererType(i) != 2 || currentTrackSelections.get(i) == null) {
                    i++;
                } else {
                    hideArtwork();
                    return;
                }
            }
            if (this.shutterView != null) {
                this.shutterView.setVisibility(0);
            }
            if (this.useArtwork) {
                for (int i2 = 0; i2 < currentTrackSelections.length; i2++) {
                    TrackSelection trackSelection = currentTrackSelections.get(i2);
                    if (trackSelection != null) {
                        int i3 = 0;
                        while (i3 < trackSelection.length()) {
                            Metadata metadata = trackSelection.getFormat(i3).metadata;
                            if (metadata == null || !setArtworkFromMetadata(metadata)) {
                                i3++;
                            } else {
                                return;
                            }
                        }
                        continue;
                    }
                }
                if (setArtworkFromBitmap(this.defaultArtwork)) {
                    return;
                }
            }
            hideArtwork();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.player == null || !this.player.isPlayingAd()) {
            maybeShowController(true);
            return dispatchMediaKeyEvent(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }
        this.overlayFrameLayout.requestFocus();
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchMediaKeyEvent(KeyEvent keyEvent) {
        return this.useController && this.controller.dispatchMediaKeyEvent(keyEvent);
    }

    public boolean getControllerAutoShow() {
        return this.controllerAutoShow;
    }

    public boolean getControllerHideOnTouch() {
        return this.controllerHideOnTouch;
    }

    public int getControllerShowTimeoutMs() {
        return this.controllerShowTimeoutMs;
    }

    public Bitmap getDefaultArtwork() {
        return this.defaultArtwork;
    }

    public FrameLayout getOverlayFrameLayout() {
        return this.overlayFrameLayout;
    }

    public SimpleExoPlayer getPlayer() {
        return this.player;
    }

    public SubtitleView getSubtitleView() {
        return this.subtitleView;
    }

    public boolean getUseArtwork() {
        return this.useArtwork;
    }

    public boolean getUseController() {
        return this.useController;
    }

    public View getVideoSurfaceView() {
        return this.surfaceView;
    }

    public void hideController() {
        if (this.controller != null) {
            this.controller.hide();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.useController || this.player == null || motionEvent.getActionMasked() != 0) {
            return false;
        }
        if (!this.controller.isVisible()) {
            maybeShowController(true);
            return true;
        } else if (!this.controllerHideOnTouch) {
            return true;
        } else {
            this.controller.hide();
            return true;
        }
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!this.useController || this.player == null) {
            return false;
        }
        maybeShowController(true);
        return true;
    }

    public void setControlDispatcher(PlaybackControlView.ControlDispatcher controlDispatcher) {
        Assertions.checkState(this.controller != null);
        this.controller.setControlDispatcher(controlDispatcher);
    }

    public void setControllerAutoShow(boolean z) {
        this.controllerAutoShow = z;
    }

    public void setControllerHideOnTouch(boolean z) {
        Assertions.checkState(this.controller != null);
        this.controllerHideOnTouch = z;
    }

    public void setControllerShowTimeoutMs(int i) {
        Assertions.checkState(this.controller != null);
        this.controllerShowTimeoutMs = i;
    }

    public void setControllerVisibilityListener(PlaybackControlView.VisibilityListener visibilityListener) {
        Assertions.checkState(this.controller != null);
        this.controller.setVisibilityListener(visibilityListener);
    }

    public void setDefaultArtwork(Bitmap bitmap) {
        if (this.defaultArtwork != bitmap) {
            this.defaultArtwork = bitmap;
            updateForCurrentTrackSelections();
        }
    }

    public void setFastForwardIncrementMs(int i) {
        Assertions.checkState(this.controller != null);
        this.controller.setFastForwardIncrementMs(i);
    }

    public void setPlayer(SimpleExoPlayer simpleExoPlayer) {
        if (this.player != simpleExoPlayer) {
            if (this.player != null) {
                this.player.removeListener(this.componentListener);
                this.player.removeTextOutput(this.componentListener);
                this.player.removeVideoListener(this.componentListener);
                if (this.surfaceView instanceof TextureView) {
                    this.player.clearVideoTextureView((TextureView) this.surfaceView);
                } else if (this.surfaceView instanceof SurfaceView) {
                    this.player.clearVideoSurfaceView((SurfaceView) this.surfaceView);
                }
            }
            this.player = simpleExoPlayer;
            if (this.useController) {
                this.controller.setPlayer(simpleExoPlayer);
            }
            if (this.shutterView != null) {
                this.shutterView.setVisibility(0);
            }
            if (simpleExoPlayer != null) {
                if (this.surfaceView instanceof TextureView) {
                    simpleExoPlayer.setVideoTextureView((TextureView) this.surfaceView);
                } else if (this.surfaceView instanceof SurfaceView) {
                    simpleExoPlayer.setVideoSurfaceView((SurfaceView) this.surfaceView);
                }
                simpleExoPlayer.addVideoListener(this.componentListener);
                simpleExoPlayer.addTextOutput(this.componentListener);
                simpleExoPlayer.addListener(this.componentListener);
                maybeShowController(false);
                updateForCurrentTrackSelections();
                return;
            }
            hideController();
            hideArtwork();
        }
    }

    public void setRepeatToggleModes(int i) {
        Assertions.checkState(this.controller != null);
        this.controller.setRepeatToggleModes(i);
    }

    public void setResizeMode(int i) {
        Assertions.checkState(this.contentFrame != null);
        this.contentFrame.setResizeMode(i);
    }

    public void setRewindIncrementMs(int i) {
        Assertions.checkState(this.controller != null);
        this.controller.setRewindIncrementMs(i);
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        Assertions.checkState(this.controller != null);
        this.controller.setShowMultiWindowTimeBar(z);
    }

    public void setUseArtwork(boolean z) {
        Assertions.checkState(!z || this.artworkView != null);
        if (this.useArtwork != z) {
            this.useArtwork = z;
            updateForCurrentTrackSelections();
        }
    }

    public void setUseController(boolean z) {
        Assertions.checkState(!z || this.controller != null);
        if (this.useController != z) {
            this.useController = z;
            if (z) {
                this.controller.setPlayer(this.player);
            } else if (this.controller != null) {
                this.controller.hide();
                this.controller.setPlayer((Player) null);
            }
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        if (this.surfaceView instanceof SurfaceView) {
            this.surfaceView.setVisibility(i);
        }
    }

    public void showController() {
        showController(shouldShowControllerIndefinitely());
    }
}
