package com.google.android.exoplayer2.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.Log;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.util.Util;

final class SubtitlePainter {
    private static final float INNER_PADDING_RATIO = 0.125f;
    private static final String TAG = "SubtitlePainter";
    private boolean applyEmbeddedFontSizes;
    private boolean applyEmbeddedStyles;
    private int backgroundColor;
    private Rect bitmapRect;
    private float bottomPaddingFraction;
    private final float cornerRadius;
    private Bitmap cueBitmap;
    private float cueBitmapHeight;
    private float cueLine;
    private int cueLineAnchor;
    private int cueLineType;
    private float cuePosition;
    private int cuePositionAnchor;
    private float cueSize;
    private CharSequence cueText;
    private Layout.Alignment cueTextAlignment;
    private int edgeColor;
    private int edgeType;
    private int foregroundColor;
    private final RectF lineBounds = new RectF();
    private final float outlineWidth;
    private final Paint paint;
    private int parentBottom;
    private int parentLeft;
    private int parentRight;
    private int parentTop;
    private final float shadowOffset;
    private final float shadowRadius;
    private final float spacingAdd;
    private final float spacingMult;
    private StaticLayout textLayout;
    private int textLeft;
    private int textPaddingX;
    private final TextPaint textPaint;
    private float textSizePx;
    private int textTop;
    private int windowColor;

    public SubtitlePainter(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, new int[]{16843287, 16843288}, 0, 0);
        this.spacingAdd = (float) obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.spacingMult = obtainStyledAttributes.getFloat(1, 1.0f);
        obtainStyledAttributes.recycle();
        int round = Math.round((2.0f * ((float) context.getResources().getDisplayMetrics().densityDpi)) / 160.0f);
        this.cornerRadius = (float) round;
        this.outlineWidth = (float) round;
        this.shadowRadius = (float) round;
        this.shadowOffset = (float) round;
        this.textPaint = new TextPaint();
        this.textPaint.setAntiAlias(true);
        this.textPaint.setSubpixelText(true);
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Paint.Style.FILL);
    }

    private static boolean areCharSequencesEqual(CharSequence charSequence, CharSequence charSequence2) {
        return charSequence == charSequence2 || (charSequence != null && charSequence.equals(charSequence2));
    }

    private void drawBitmapLayout(Canvas canvas) {
        canvas.drawBitmap(this.cueBitmap, (Rect) null, this.bitmapRect, (Paint) null);
    }

    private void drawLayout(Canvas canvas, boolean z) {
        if (z) {
            drawTextLayout(canvas);
        } else {
            drawBitmapLayout(canvas);
        }
    }

    private void drawTextLayout(Canvas canvas) {
        StaticLayout staticLayout = this.textLayout;
        if (staticLayout != null) {
            int save = canvas.save();
            canvas.translate((float) this.textLeft, (float) this.textTop);
            if (Color.alpha(this.windowColor) > 0) {
                this.paint.setColor(this.windowColor);
                canvas.drawRect((float) (-this.textPaddingX), 0.0f, (float) (staticLayout.getWidth() + this.textPaddingX), (float) staticLayout.getHeight(), this.paint);
            }
            if (Color.alpha(this.backgroundColor) > 0) {
                this.paint.setColor(this.backgroundColor);
                float lineTop = (float) staticLayout.getLineTop(0);
                int lineCount = staticLayout.getLineCount();
                for (int i = 0; i < lineCount; i++) {
                    this.lineBounds.left = staticLayout.getLineLeft(i) - ((float) this.textPaddingX);
                    this.lineBounds.right = staticLayout.getLineRight(i) + ((float) this.textPaddingX);
                    this.lineBounds.top = lineTop;
                    this.lineBounds.bottom = (float) staticLayout.getLineBottom(i);
                    lineTop = this.lineBounds.bottom;
                    canvas.drawRoundRect(this.lineBounds, this.cornerRadius, this.cornerRadius, this.paint);
                }
            }
            if (this.edgeType == 1) {
                this.textPaint.setStrokeJoin(Paint.Join.ROUND);
                this.textPaint.setStrokeWidth(this.outlineWidth);
                this.textPaint.setColor(this.edgeColor);
                this.textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                staticLayout.draw(canvas);
            } else if (this.edgeType == 2) {
                this.textPaint.setShadowLayer(this.shadowRadius, this.shadowOffset, this.shadowOffset, this.edgeColor);
            } else if (this.edgeType == 3 || this.edgeType == 4) {
                boolean z = this.edgeType == 3;
                int i2 = z ? -1 : this.edgeColor;
                int i3 = z ? this.edgeColor : -1;
                float f = this.shadowRadius / 2.0f;
                this.textPaint.setColor(this.foregroundColor);
                this.textPaint.setStyle(Paint.Style.FILL);
                this.textPaint.setShadowLayer(this.shadowRadius, -f, -f, i2);
                staticLayout.draw(canvas);
                this.textPaint.setShadowLayer(this.shadowRadius, f, f, i3);
            }
            this.textPaint.setColor(this.foregroundColor);
            this.textPaint.setStyle(Paint.Style.FILL);
            staticLayout.draw(canvas);
            this.textPaint.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
            canvas.restoreToCount(save);
        }
    }

    private void setupBitmapLayout() {
        int i = this.parentRight - this.parentLeft;
        int i2 = this.parentBottom - this.parentTop;
        float f = ((float) this.parentLeft) + (((float) i) * this.cuePosition);
        float f2 = ((float) this.parentTop) + (((float) i2) * this.cueLine);
        int round = Math.round(((float) i) * this.cueSize);
        int round2 = this.cueBitmapHeight != Float.MIN_VALUE ? Math.round(((float) i2) * this.cueBitmapHeight) : Math.round(((float) round) * (((float) this.cueBitmap.getHeight()) / ((float) this.cueBitmap.getWidth())));
        if (this.cueLineAnchor == 2) {
            f -= (float) round;
        } else if (this.cueLineAnchor == 1) {
            f -= (float) (round / 2);
        }
        int round3 = Math.round(f);
        if (this.cuePositionAnchor == 2) {
            f2 -= (float) round2;
        } else if (this.cuePositionAnchor == 1) {
            f2 -= (float) (round2 / 2);
        }
        int round4 = Math.round(f2);
        this.bitmapRect = new Rect(round3, round4, round3 + round, round4 + round2);
    }

    private void setupTextLayout() {
        CharSequence charSequence;
        int i;
        int i2;
        int i3;
        int round;
        int i4 = this.parentRight - this.parentLeft;
        int i5 = this.parentBottom - this.parentTop;
        this.textPaint.setTextSize(this.textSizePx);
        int i6 = (int) ((this.textSizePx * INNER_PADDING_RATIO) + 0.5f);
        int i7 = i4 - (i6 * 2);
        if (this.cueSize != Float.MIN_VALUE) {
            i7 = (int) (((float) i7) * this.cueSize);
        }
        if (i7 <= 0) {
            Log.w(TAG, "Skipped drawing subtitle cue (insufficient space)");
            return;
        }
        if (this.applyEmbeddedFontSizes && this.applyEmbeddedStyles) {
            charSequence = this.cueText;
        } else if (!this.applyEmbeddedStyles) {
            charSequence = this.cueText.toString();
        } else {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.cueText);
            int length = spannableStringBuilder.length();
            AbsoluteSizeSpan[] absoluteSizeSpanArr = (AbsoluteSizeSpan[]) spannableStringBuilder.getSpans(0, length, AbsoluteSizeSpan.class);
            RelativeSizeSpan[] relativeSizeSpanArr = (RelativeSizeSpan[]) spannableStringBuilder.getSpans(0, length, RelativeSizeSpan.class);
            for (AbsoluteSizeSpan removeSpan : absoluteSizeSpanArr) {
                spannableStringBuilder.removeSpan(removeSpan);
            }
            for (RelativeSizeSpan removeSpan2 : relativeSizeSpanArr) {
                spannableStringBuilder.removeSpan(removeSpan2);
            }
            charSequence = spannableStringBuilder;
        }
        Layout.Alignment alignment = this.cueTextAlignment == null ? Layout.Alignment.ALIGN_CENTER : this.cueTextAlignment;
        this.textLayout = new StaticLayout(charSequence, this.textPaint, i7, alignment, this.spacingMult, this.spacingAdd, true);
        int height = this.textLayout.getHeight();
        int i8 = 0;
        int lineCount = this.textLayout.getLineCount();
        for (int i9 = 0; i9 < lineCount; i9++) {
            i8 = Math.max((int) Math.ceil((double) this.textLayout.getLineWidth(i9)), i8);
        }
        if (this.cueSize != Float.MIN_VALUE && i8 < i7) {
            i8 = i7;
        }
        int i10 = i8 + (i6 * 2);
        if (this.cuePosition != Float.MIN_VALUE) {
            int round2 = Math.round(((float) i4) * this.cuePosition) + this.parentLeft;
            i = Math.max(this.cuePositionAnchor == 2 ? round2 - i10 : this.cuePositionAnchor == 1 ? ((round2 * 2) - i10) / 2 : round2, this.parentLeft);
            i2 = Math.min(i + i10, this.parentRight);
        } else {
            i = (i4 - i10) / 2;
            i2 = i + i10;
        }
        int i11 = i2 - i;
        if (i11 <= 0) {
            Log.w(TAG, "Skipped drawing subtitle cue (invalid horizontal positioning)");
            return;
        }
        if (this.cueLine != Float.MIN_VALUE) {
            if (this.cueLineType == 0) {
                round = Math.round(((float) i5) * this.cueLine) + this.parentTop;
            } else {
                int lineBottom = this.textLayout.getLineBottom(0) - this.textLayout.getLineTop(0);
                round = this.cueLine >= 0.0f ? Math.round(this.cueLine * ((float) lineBottom)) + this.parentTop : Math.round((this.cueLine + 1.0f) * ((float) lineBottom)) + this.parentBottom;
            }
            i3 = this.cueLineAnchor == 2 ? round - height : this.cueLineAnchor == 1 ? ((round * 2) - height) / 2 : round;
            if (i3 + height > this.parentBottom) {
                i3 = this.parentBottom - height;
            } else if (i3 < this.parentTop) {
                i3 = this.parentTop;
            }
        } else {
            i3 = (this.parentBottom - height) - ((int) (((float) i5) * this.bottomPaddingFraction));
        }
        this.textLayout = new StaticLayout(charSequence, this.textPaint, i11, alignment, this.spacingMult, this.spacingAdd, true);
        this.textLeft = i;
        this.textTop = i3;
        this.textPaddingX = i6;
    }

    public void draw(Cue cue, boolean z, boolean z2, CaptionStyleCompat captionStyleCompat, float f, float f2, Canvas canvas, int i, int i2, int i3, int i4) {
        boolean z3 = cue.bitmap == null;
        int i5 = -16777216;
        if (z3) {
            if (!TextUtils.isEmpty(cue.text)) {
                i5 = (!cue.windowColorSet || !z) ? captionStyleCompat.windowColor : cue.windowColor;
            } else {
                return;
            }
        }
        if (areCharSequencesEqual(this.cueText, cue.text) && Util.areEqual(this.cueTextAlignment, cue.textAlignment) && this.cueBitmap == cue.bitmap && this.cueLine == cue.line && this.cueLineType == cue.lineType && Util.areEqual(Integer.valueOf(this.cueLineAnchor), Integer.valueOf(cue.lineAnchor)) && this.cuePosition == cue.position && Util.areEqual(Integer.valueOf(this.cuePositionAnchor), Integer.valueOf(cue.positionAnchor)) && this.cueSize == cue.size && this.cueBitmapHeight == cue.bitmapHeight && this.applyEmbeddedStyles == z && this.applyEmbeddedFontSizes == z2 && this.foregroundColor == captionStyleCompat.foregroundColor && this.backgroundColor == captionStyleCompat.backgroundColor && this.windowColor == i5 && this.edgeType == captionStyleCompat.edgeType && this.edgeColor == captionStyleCompat.edgeColor && Util.areEqual(this.textPaint.getTypeface(), captionStyleCompat.typeface) && this.textSizePx == f && this.bottomPaddingFraction == f2 && this.parentLeft == i && this.parentTop == i2 && this.parentRight == i3 && this.parentBottom == i4) {
            drawLayout(canvas, z3);
            return;
        }
        this.cueText = cue.text;
        this.cueTextAlignment = cue.textAlignment;
        this.cueBitmap = cue.bitmap;
        this.cueLine = cue.line;
        this.cueLineType = cue.lineType;
        this.cueLineAnchor = cue.lineAnchor;
        this.cuePosition = cue.position;
        this.cuePositionAnchor = cue.positionAnchor;
        this.cueSize = cue.size;
        this.cueBitmapHeight = cue.bitmapHeight;
        this.applyEmbeddedStyles = z;
        this.applyEmbeddedFontSizes = z2;
        this.foregroundColor = captionStyleCompat.foregroundColor;
        this.backgroundColor = captionStyleCompat.backgroundColor;
        this.windowColor = i5;
        this.edgeType = captionStyleCompat.edgeType;
        this.edgeColor = captionStyleCompat.edgeColor;
        this.textPaint.setTypeface(captionStyleCompat.typeface);
        this.textSizePx = f;
        this.bottomPaddingFraction = f2;
        this.parentLeft = i;
        this.parentTop = i2;
        this.parentRight = i3;
        this.parentBottom = i4;
        if (z3) {
            setupTextLayout();
        } else {
            setupBitmapLayout();
        }
        drawLayout(canvas, z3);
    }
}
