package com.google.android.exoplayer2.ui;

import android.annotation.SuppressLint;
import android.support.v4.os.EnvironmentCompat;
import android.widget.TextView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

public final class DebugTextViewHelper implements Player.EventListener, Runnable {
    private static final int REFRESH_INTERVAL_MS = 1000;
    private final SimpleExoPlayer player;
    private boolean started;
    private final TextView textView;

    public DebugTextViewHelper(SimpleExoPlayer simpleExoPlayer, TextView textView2) {
        this.player = simpleExoPlayer;
        this.textView = textView2;
    }

    private String getAudioString() {
        Format audioFormat = this.player.getAudioFormat();
        return audioFormat == null ? "" : StringUtils.LF + audioFormat.sampleMimeType + "(id:" + audioFormat.id + " hz:" + audioFormat.sampleRate + " ch:" + audioFormat.channelCount + getDecoderCountersBufferCountString(this.player.getAudioDecoderCounters()) + ")";
    }

    private static String getDecoderCountersBufferCountString(DecoderCounters decoderCounters) {
        if (decoderCounters == null) {
            return "";
        }
        decoderCounters.ensureUpdated();
        return " rb:" + decoderCounters.renderedOutputBufferCount + " sb:" + decoderCounters.skippedOutputBufferCount + " db:" + decoderCounters.droppedOutputBufferCount + " mcdb:" + decoderCounters.maxConsecutiveDroppedOutputBufferCount;
    }

    private static String getPixelAspectRatioString(float f) {
        if (f == -1.0f || f == 1.0f) {
            return "";
        }
        return " par:" + String.format(Locale.US, "%.02f", new Object[]{Float.valueOf(f)});
    }

    private String getPlayerStateString() {
        String str = "playWhenReady:" + this.player.getPlayWhenReady() + " playbackState:";
        switch (this.player.getPlaybackState()) {
            case 1:
                return str + "idle";
            case 2:
                return str + "buffering";
            case 3:
                return str + "ready";
            case 4:
                return str + "ended";
            default:
                return str + EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    private String getPlayerWindowIndexString() {
        return " window:" + this.player.getCurrentWindowIndex();
    }

    private String getVideoString() {
        Format videoFormat = this.player.getVideoFormat();
        return videoFormat == null ? "" : StringUtils.LF + videoFormat.sampleMimeType + "(id:" + videoFormat.id + " r:" + videoFormat.width + "x" + videoFormat.height + getPixelAspectRatioString(videoFormat.pixelWidthHeightRatio) + getDecoderCountersBufferCountString(this.player.getVideoDecoderCounters()) + ")";
    }

    @SuppressLint({"SetTextI18n"})
    private void updateAndPost() {
        this.textView.setText(getPlayerStateString() + getPlayerWindowIndexString() + getVideoString() + getAudioString());
        this.textView.removeCallbacks(this);
        this.textView.postDelayed(this, 1000);
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
    }

    public void onPlayerStateChanged(boolean z, int i) {
        updateAndPost();
    }

    public void onPositionDiscontinuity() {
        updateAndPost();
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onTimelineChanged(Timeline timeline, Object obj) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public void run() {
        updateAndPost();
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            this.player.addListener(this);
            updateAndPost();
        }
    }

    public void stop() {
        if (this.started) {
            this.started = false;
            this.player.removeListener(this);
            this.textView.removeCallbacks(this);
        }
    }
}
