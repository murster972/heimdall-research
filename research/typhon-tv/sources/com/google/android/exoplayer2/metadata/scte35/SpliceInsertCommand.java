package com.google.android.exoplayer2.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SpliceInsertCommand extends SpliceCommand {
    public static final Parcelable.Creator<SpliceInsertCommand> CREATOR = new Parcelable.Creator<SpliceInsertCommand>() {
        public SpliceInsertCommand createFromParcel(Parcel parcel) {
            return new SpliceInsertCommand(parcel);
        }

        public SpliceInsertCommand[] newArray(int i) {
            return new SpliceInsertCommand[i];
        }
    };
    public final boolean autoReturn;
    public final int availNum;
    public final int availsExpected;
    public final long breakDuration;
    public final List<ComponentSplice> componentSpliceList;
    public final boolean outOfNetworkIndicator;
    public final boolean programSpliceFlag;
    public final long programSplicePlaybackPositionUs;
    public final long programSplicePts;
    public final boolean spliceEventCancelIndicator;
    public final long spliceEventId;
    public final boolean spliceImmediateFlag;
    public final int uniqueProgramId;

    public static final class ComponentSplice {
        public final long componentSplicePlaybackPositionUs;
        public final long componentSplicePts;
        public final int componentTag;

        private ComponentSplice(int i, long j, long j2) {
            this.componentTag = i;
            this.componentSplicePts = j;
            this.componentSplicePlaybackPositionUs = j2;
        }

        public static ComponentSplice createFromParcel(Parcel parcel) {
            return new ComponentSplice(parcel.readInt(), parcel.readLong(), parcel.readLong());
        }

        public void writeToParcel(Parcel parcel) {
            parcel.writeInt(this.componentTag);
            parcel.writeLong(this.componentSplicePts);
            parcel.writeLong(this.componentSplicePlaybackPositionUs);
        }
    }

    private SpliceInsertCommand(long j, boolean z, boolean z2, boolean z3, boolean z4, long j2, long j3, List<ComponentSplice> list, boolean z5, long j4, int i, int i2, int i3) {
        this.spliceEventId = j;
        this.spliceEventCancelIndicator = z;
        this.outOfNetworkIndicator = z2;
        this.programSpliceFlag = z3;
        this.spliceImmediateFlag = z4;
        this.programSplicePts = j2;
        this.programSplicePlaybackPositionUs = j3;
        this.componentSpliceList = Collections.unmodifiableList(list);
        this.autoReturn = z5;
        this.breakDuration = j4;
        this.uniqueProgramId = i;
        this.availNum = i2;
        this.availsExpected = i3;
    }

    private SpliceInsertCommand(Parcel parcel) {
        boolean z = true;
        this.spliceEventId = parcel.readLong();
        this.spliceEventCancelIndicator = parcel.readByte() == 1;
        this.outOfNetworkIndicator = parcel.readByte() == 1;
        this.programSpliceFlag = parcel.readByte() == 1;
        this.spliceImmediateFlag = parcel.readByte() == 1;
        this.programSplicePts = parcel.readLong();
        this.programSplicePlaybackPositionUs = parcel.readLong();
        int readInt = parcel.readInt();
        ArrayList arrayList = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            arrayList.add(ComponentSplice.createFromParcel(parcel));
        }
        this.componentSpliceList = Collections.unmodifiableList(arrayList);
        this.autoReturn = parcel.readByte() != 1 ? false : z;
        this.breakDuration = parcel.readLong();
        this.uniqueProgramId = parcel.readInt();
        this.availNum = parcel.readInt();
        this.availsExpected = parcel.readInt();
    }

    /* JADX WARNING: type inference failed for: r18v0, types: [java.util.List] */
    /* JADX WARNING: type inference failed for: r18v2 */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.util.ArrayList] */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand parseFromSection(com.google.android.exoplayer2.util.ParsableByteArray r35, long r36, com.google.android.exoplayer2.util.TimestampAdjuster r38) {
        /*
            long r32 = r35.readUnsignedInt()
            int r2 = r35.readUnsignedByte()
            r2 = r2 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L_0x0085
            r10 = 1
        L_0x000d:
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            java.util.List r18 = java.util.Collections.emptyList()
            r22 = 0
            r23 = 0
            r24 = 0
            r19 = 0
            r20 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r10 != 0) goto L_0x00be
            int r27 = r35.readUnsignedByte()
            r0 = r27
            r2 = r0 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L_0x0087
            r11 = 1
        L_0x0033:
            r2 = r27 & 64
            if (r2 == 0) goto L_0x0089
            r12 = 1
        L_0x0038:
            r2 = r27 & 32
            if (r2 == 0) goto L_0x008b
            r26 = 1
        L_0x003e:
            r2 = r27 & 16
            if (r2 == 0) goto L_0x008e
            r13 = 1
        L_0x0043:
            if (r12 == 0) goto L_0x004b
            if (r13 != 0) goto L_0x004b
            long r14 = com.google.android.exoplayer2.metadata.scte35.TimeSignalCommand.parseSpliceTime(r35, r36)
        L_0x004b:
            if (r12 != 0) goto L_0x0090
            int r25 = r35.readUnsignedByte()
            java.util.ArrayList r18 = new java.util.ArrayList
            r0 = r18
            r1 = r25
            r0.<init>(r1)
            r30 = 0
        L_0x005c:
            r0 = r30
            r1 = r25
            if (r0 >= r1) goto L_0x0090
            int r3 = r35.readUnsignedByte()
            r4 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r13 != 0) goto L_0x0071
            long r4 = com.google.android.exoplayer2.metadata.scte35.TimeSignalCommand.parseSpliceTime(r35, r36)
        L_0x0071:
            com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand$ComponentSplice r2 = new com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand$ComponentSplice
            r0 = r38
            long r6 = r0.adjustTsTimestamp(r4)
            r8 = 0
            r2.<init>(r3, r4, r6)
            r0 = r18
            r0.add(r2)
            int r30 = r30 + 1
            goto L_0x005c
        L_0x0085:
            r10 = 0
            goto L_0x000d
        L_0x0087:
            r11 = 0
            goto L_0x0033
        L_0x0089:
            r12 = 0
            goto L_0x0038
        L_0x008b:
            r26 = 0
            goto L_0x003e
        L_0x008e:
            r13 = 0
            goto L_0x0043
        L_0x0090:
            if (r26 == 0) goto L_0x00b2
            int r2 = r35.readUnsignedByte()
            long r0 = (long) r2
            r28 = r0
            r6 = 128(0x80, double:6.32E-322)
            long r6 = r6 & r28
            r8 = 0
            int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r2 == 0) goto L_0x00cc
            r19 = 1
        L_0x00a5:
            r6 = 1
            long r6 = r6 & r28
            r2 = 32
            long r6 = r6 << r2
            long r8 = r35.readUnsignedInt()
            long r20 = r6 | r8
        L_0x00b2:
            int r22 = r35.readUnsignedShort()
            int r23 = r35.readUnsignedByte()
            int r24 = r35.readUnsignedByte()
        L_0x00be:
            com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand r7 = new com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand
            r0 = r38
            long r16 = r0.adjustTsTimestamp(r14)
            r8 = r32
            r7.<init>(r8, r10, r11, r12, r13, r14, r16, r18, r19, r20, r22, r23, r24)
            return r7
        L_0x00cc:
            r19 = 0
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand.parseFromSection(com.google.android.exoplayer2.util.ParsableByteArray, long, com.google.android.exoplayer2.util.TimestampAdjuster):com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand");
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeLong(this.spliceEventId);
        parcel.writeByte((byte) (this.spliceEventCancelIndicator ? 1 : 0));
        parcel.writeByte((byte) (this.outOfNetworkIndicator ? 1 : 0));
        parcel.writeByte((byte) (this.programSpliceFlag ? 1 : 0));
        parcel.writeByte((byte) (this.spliceImmediateFlag ? 1 : 0));
        parcel.writeLong(this.programSplicePts);
        parcel.writeLong(this.programSplicePlaybackPositionUs);
        int size = this.componentSpliceList.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            this.componentSpliceList.get(i3).writeToParcel(parcel);
        }
        if (!this.autoReturn) {
            i2 = 0;
        }
        parcel.writeByte((byte) i2);
        parcel.writeLong(this.breakDuration);
        parcel.writeInt(this.uniqueProgramId);
        parcel.writeInt(this.availNum);
        parcel.writeInt(this.availsExpected);
    }
}
