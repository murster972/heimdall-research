package com.google.android.exoplayer2;

public class TTVWorkaroundUtils {
    private static boolean enableAllWorkaround = false;
    private static final Object lock = new Object();

    public static void disableAllWorkaround() {
        synchronized (lock) {
            enableAllWorkaround = false;
        }
    }

    public static void enableAllWorkaround() {
        synchronized (lock) {
            enableAllWorkaround = true;
        }
    }

    public static boolean isAllWorkaroundEnabled() {
        boolean z;
        synchronized (lock) {
            z = enableAllWorkaround;
        }
        return z;
    }
}
