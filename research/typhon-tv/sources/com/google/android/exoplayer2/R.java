package com.google.android.exoplayer2;

public final class R {

    public static final class attr {
        public static final int ad_marker_color = 2130968613;
        public static final int ad_marker_width = 2130968614;
        public static final int auto_show = 2130968632;
        public static final int bar_height = 2130968678;
        public static final int buffered_color = 2130968688;
        public static final int controller_layout_id = 2130968775;
        public static final int default_artwork = 2130968782;
        public static final int fastforward_increment = 2130968818;
        public static final int hide_on_touch = 2130968835;
        public static final int played_ad_marker_color = 2130968989;
        public static final int played_color = 2130968990;
        public static final int player_layout_id = 2130968991;
        public static final int repeat_toggle_modes = 2130969011;
        public static final int resize_mode = 2130969012;
        public static final int rewind_increment = 2130969014;
        public static final int scrubber_color = 2130969022;
        public static final int scrubber_disabled_size = 2130969023;
        public static final int scrubber_dragged_size = 2130969024;
        public static final int scrubber_enabled_size = 2130969025;
        public static final int show_timeout = 2130969037;
        public static final int surface_type = 2130969058;
        public static final int touch_target_height = 2130969118;
        public static final int unplayed_color = 2130969122;
        public static final int use_artwork = 2130969124;
        public static final int use_controller = 2130969125;
    }

    public static final class color {
        public static final int exo_edit_mode_background_color = 2131099745;
    }

    public static final class dimen {
        public static final int exo_media_button_height = 2131165374;
        public static final int exo_media_button_width = 2131165375;
    }

    public static final class drawable {
        public static final int exo_controls_fastforward = 2131230941;
        public static final int exo_controls_next = 2131230942;
        public static final int exo_controls_pause = 2131230943;
        public static final int exo_controls_play = 2131230944;
        public static final int exo_controls_previous = 2131230945;
        public static final int exo_controls_repeat_all = 2131230946;
        public static final int exo_controls_repeat_off = 2131230947;
        public static final int exo_controls_repeat_one = 2131230948;
        public static final int exo_controls_rewind = 2131230949;
        public static final int exo_edit_mode_logo = 2131230950;
    }

    public static final class id {
        public static final int exo_artwork = 2131296465;
        public static final int exo_content_frame = 2131296467;
        public static final int exo_controller = 2131296468;
        public static final int exo_controller_placeholder = 2131296469;
        public static final int exo_duration = 2131296470;
        public static final int exo_ffwd = 2131296471;
        public static final int exo_next = 2131296472;
        public static final int exo_overlay = 2131296473;
        public static final int exo_pause = 2131296474;
        public static final int exo_play = 2131296475;
        public static final int exo_position = 2131296476;
        public static final int exo_prev = 2131296477;
        public static final int exo_progress = 2131296478;
        public static final int exo_repeat_toggle = 2131296479;
        public static final int exo_rew = 2131296481;
        public static final int exo_shutter = 2131296482;
        public static final int exo_subtitles = 2131296483;
        public static final int fill = 2131296490;
        public static final int fit = 2131296493;
        public static final int fixed_height = 2131296495;
        public static final int fixed_width = 2131296496;
        public static final int none = 2131296761;
        public static final int surface_view = 2131296891;
        public static final int texture_view = 2131296909;
    }

    public static final class layout {
        public static final int exo_playback_control_view = 2131492961;
        public static final int exo_simple_player_view = 2131492962;
    }

    public static final class string {
        public static final int exo_controls_fastforward_description = 2131820752;
        public static final int exo_controls_next_description = 2131820753;
        public static final int exo_controls_pause_description = 2131820754;
        public static final int exo_controls_play_description = 2131820755;
        public static final int exo_controls_previous_description = 2131820756;
        public static final int exo_controls_repeat_all_description = 2131820757;
        public static final int exo_controls_repeat_off_description = 2131820758;
        public static final int exo_controls_repeat_one_description = 2131820759;
        public static final int exo_controls_rewind_description = 2131820760;
        public static final int exo_controls_stop_description = 2131820761;
    }

    public static final class style {
        public static final int ExoMediaButton = 2131886259;
        public static final int ExoMediaButton_FastForward = 2131886260;
        public static final int ExoMediaButton_Next = 2131886261;
        public static final int ExoMediaButton_Pause = 2131886262;
        public static final int ExoMediaButton_Play = 2131886263;
        public static final int ExoMediaButton_Previous = 2131886264;
        public static final int ExoMediaButton_Rewind = 2131886265;
    }

    public static final class styleable {
        public static final int[] AspectRatioFrameLayout = {com.typhoon.tv.R.attr.resize_mode};
        public static final int AspectRatioFrameLayout_resize_mode = 0;
        public static final int[] DefaultTimeBar = {com.typhoon.tv.R.attr.ad_marker_color, com.typhoon.tv.R.attr.ad_marker_width, com.typhoon.tv.R.attr.bar_height, com.typhoon.tv.R.attr.buffered_color, com.typhoon.tv.R.attr.played_ad_marker_color, com.typhoon.tv.R.attr.played_color, com.typhoon.tv.R.attr.scrubber_color, com.typhoon.tv.R.attr.scrubber_disabled_size, com.typhoon.tv.R.attr.scrubber_dragged_size, com.typhoon.tv.R.attr.scrubber_enabled_size, com.typhoon.tv.R.attr.touch_target_height, com.typhoon.tv.R.attr.unplayed_color};
        public static final int DefaultTimeBar_ad_marker_color = 0;
        public static final int DefaultTimeBar_ad_marker_width = 1;
        public static final int DefaultTimeBar_bar_height = 2;
        public static final int DefaultTimeBar_buffered_color = 3;
        public static final int DefaultTimeBar_played_ad_marker_color = 4;
        public static final int DefaultTimeBar_played_color = 5;
        public static final int DefaultTimeBar_scrubber_color = 6;
        public static final int DefaultTimeBar_scrubber_disabled_size = 7;
        public static final int DefaultTimeBar_scrubber_dragged_size = 8;
        public static final int DefaultTimeBar_scrubber_enabled_size = 9;
        public static final int DefaultTimeBar_touch_target_height = 10;
        public static final int DefaultTimeBar_unplayed_color = 11;
        public static final int[] PlaybackControlView = {com.typhoon.tv.R.attr.controller_layout_id, com.typhoon.tv.R.attr.fastforward_increment, com.typhoon.tv.R.attr.repeat_toggle_modes, com.typhoon.tv.R.attr.rewind_increment, com.typhoon.tv.R.attr.show_timeout};
        public static final int PlaybackControlView_controller_layout_id = 0;
        public static final int PlaybackControlView_fastforward_increment = 1;
        public static final int PlaybackControlView_repeat_toggle_modes = 2;
        public static final int PlaybackControlView_rewind_increment = 3;
        public static final int PlaybackControlView_show_timeout = 4;
        public static final int[] SimpleExoPlayerView = {com.typhoon.tv.R.attr.auto_show, com.typhoon.tv.R.attr.controller_layout_id, com.typhoon.tv.R.attr.default_artwork, com.typhoon.tv.R.attr.fastforward_increment, com.typhoon.tv.R.attr.hide_on_touch, com.typhoon.tv.R.attr.player_layout_id, com.typhoon.tv.R.attr.resize_mode, com.typhoon.tv.R.attr.rewind_increment, com.typhoon.tv.R.attr.show_timeout, com.typhoon.tv.R.attr.surface_type, com.typhoon.tv.R.attr.use_artwork, com.typhoon.tv.R.attr.use_controller};
        public static final int SimpleExoPlayerView_auto_show = 0;
        public static final int SimpleExoPlayerView_controller_layout_id = 1;
        public static final int SimpleExoPlayerView_default_artwork = 2;
        public static final int SimpleExoPlayerView_fastforward_increment = 3;
        public static final int SimpleExoPlayerView_hide_on_touch = 4;
        public static final int SimpleExoPlayerView_player_layout_id = 5;
        public static final int SimpleExoPlayerView_resize_mode = 6;
        public static final int SimpleExoPlayerView_rewind_increment = 7;
        public static final int SimpleExoPlayerView_show_timeout = 8;
        public static final int SimpleExoPlayerView_surface_type = 9;
        public static final int SimpleExoPlayerView_use_artwork = 10;
        public static final int SimpleExoPlayerView_use_controller = 11;
    }
}
