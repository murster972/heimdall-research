package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.util.Util;

final class ConstantBitrateSeeker implements Mp3Extractor.Seeker {
    private static final int BITS_PER_BYTE = 8;
    private final int bitrate;
    private final long durationUs;
    private final long firstFramePosition;

    public ConstantBitrateSeeker(long j, int i, long j2) {
        this.firstFramePosition = j;
        this.bitrate = i;
        this.durationUs = j2 == -1 ? C.TIME_UNSET : getTimeUs(j2);
    }

    public long getDurationUs() {
        return this.durationUs;
    }

    public long getPosition(long j) {
        if (this.durationUs == C.TIME_UNSET) {
            return 0;
        }
        long constrainValue = Util.constrainValue(j, 0, this.durationUs);
        return ((((long) this.bitrate) * constrainValue) / 8000000) + this.firstFramePosition;
    }

    public long getTimeUs(long j) {
        return ((Math.max(0, j - this.firstFramePosition) * C.MICROS_PER_SECOND) * 8) / ((long) this.bitrate);
    }

    public boolean isSeekable() {
        return this.durationUs != C.TIME_UNSET;
    }
}
