package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.MpegAudioHeader;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;

final class VbriSeeker implements Mp3Extractor.Seeker {
    private final long durationUs;
    private final long[] positions;
    private final long[] timesUs;

    private VbriSeeker(long[] jArr, long[] jArr2, long j) {
        this.timesUs = jArr;
        this.positions = jArr2;
        this.durationUs = j;
    }

    public static VbriSeeker create(MpegAudioHeader mpegAudioHeader, ParsableByteArray parsableByteArray, long j, long j2) {
        int readUnsignedIntToInt;
        parsableByteArray.skipBytes(10);
        int readInt = parsableByteArray.readInt();
        if (readInt <= 0) {
            return null;
        }
        int i = mpegAudioHeader.sampleRate;
        long scaleLargeTimestamp = Util.scaleLargeTimestamp((long) readInt, ((long) (i >= 32000 ? 1152 : 576)) * C.MICROS_PER_SECOND, (long) i);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        int readUnsignedShort2 = parsableByteArray.readUnsignedShort();
        int readUnsignedShort3 = parsableByteArray.readUnsignedShort();
        parsableByteArray.skipBytes(2);
        long j3 = j + ((long) mpegAudioHeader.frameSize);
        long[] jArr = new long[(readUnsignedShort + 1)];
        long[] jArr2 = new long[(readUnsignedShort + 1)];
        jArr[0] = 0;
        jArr2[0] = j3;
        for (int i2 = 1; i2 < jArr.length; i2++) {
            switch (readUnsignedShort3) {
                case 1:
                    readUnsignedIntToInt = parsableByteArray.readUnsignedByte();
                    break;
                case 2:
                    readUnsignedIntToInt = parsableByteArray.readUnsignedShort();
                    break;
                case 3:
                    readUnsignedIntToInt = parsableByteArray.readUnsignedInt24();
                    break;
                case 4:
                    readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
                    break;
                default:
                    return null;
            }
            j3 += (long) (readUnsignedIntToInt * readUnsignedShort2);
            jArr[i2] = (((long) i2) * scaleLargeTimestamp) / ((long) readUnsignedShort);
            jArr2[i2] = j2 == -1 ? j3 : Math.min(j2, j3);
        }
        return new VbriSeeker(jArr, jArr2, scaleLargeTimestamp);
    }

    public long getDurationUs() {
        return this.durationUs;
    }

    public long getPosition(long j) {
        return this.positions[Util.binarySearchFloor(this.timesUs, j, true, true)];
    }

    public long getTimeUs(long j) {
        return this.timesUs[Util.binarySearchFloor(this.positions, j, true, true)];
    }

    public boolean isSeekable() {
        return true;
    }
}
