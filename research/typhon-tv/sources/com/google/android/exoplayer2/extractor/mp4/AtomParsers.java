package com.google.android.exoplayer2.extractor.mp4;

import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.mp4.Atom;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class AtomParsers {
    private static final String TAG = "AtomParsers";
    private static final int TYPE_cenc = Util.getIntegerCodeForString(C.CENC_TYPE_cenc);
    private static final int TYPE_clcp = Util.getIntegerCodeForString("clcp");
    private static final int TYPE_meta = Util.getIntegerCodeForString("meta");
    private static final int TYPE_sbtl = Util.getIntegerCodeForString("sbtl");
    private static final int TYPE_soun = Util.getIntegerCodeForString("soun");
    private static final int TYPE_subt = Util.getIntegerCodeForString("subt");
    private static final int TYPE_text = Util.getIntegerCodeForString(MimeTypes.BASE_TYPE_TEXT);
    private static final int TYPE_vide = Util.getIntegerCodeForString("vide");

    private static final class ChunkIterator {
        private final ParsableByteArray chunkOffsets;
        private final boolean chunkOffsetsAreLongs;
        public int index;
        public final int length;
        private int nextSamplesPerChunkChangeIndex;
        public int numSamples;
        public long offset;
        private int remainingSamplesPerChunkChanges;
        private final ParsableByteArray stsc;

        public ChunkIterator(ParsableByteArray parsableByteArray, ParsableByteArray parsableByteArray2, boolean z) {
            boolean z2 = true;
            this.stsc = parsableByteArray;
            this.chunkOffsets = parsableByteArray2;
            this.chunkOffsetsAreLongs = z;
            parsableByteArray2.setPosition(12);
            this.length = parsableByteArray2.readUnsignedIntToInt();
            parsableByteArray.setPosition(12);
            this.remainingSamplesPerChunkChanges = parsableByteArray.readUnsignedIntToInt();
            Assertions.checkState(parsableByteArray.readInt() != 1 ? false : z2, "first_chunk must be 1");
            this.index = -1;
        }

        public boolean moveNext() {
            int i = this.index + 1;
            this.index = i;
            if (i == this.length) {
                return false;
            }
            this.offset = this.chunkOffsetsAreLongs ? this.chunkOffsets.readUnsignedLongToLong() : this.chunkOffsets.readUnsignedInt();
            if (this.index == this.nextSamplesPerChunkChangeIndex) {
                this.numSamples = this.stsc.readUnsignedIntToInt();
                this.stsc.skipBytes(4);
                int i2 = this.remainingSamplesPerChunkChanges - 1;
                this.remainingSamplesPerChunkChanges = i2;
                this.nextSamplesPerChunkChangeIndex = i2 > 0 ? this.stsc.readUnsignedIntToInt() - 1 : -1;
            }
            return true;
        }
    }

    private interface SampleSizeBox {
        int getSampleCount();

        boolean isFixedSampleSize();

        int readNextSampleSize();
    }

    private static final class StsdData {
        public static final int STSD_HEADER_SIZE = 8;
        public Format format;
        public int nalUnitLengthFieldLength;
        public int requiredSampleTransformation = 0;
        public final TrackEncryptionBox[] trackEncryptionBoxes;

        public StsdData(int i) {
            this.trackEncryptionBoxes = new TrackEncryptionBox[i];
        }
    }

    static final class StszSampleSizeBox implements SampleSizeBox {
        private final ParsableByteArray data;
        private final int fixedSampleSize = this.data.readUnsignedIntToInt();
        private final int sampleCount = this.data.readUnsignedIntToInt();

        public StszSampleSizeBox(Atom.LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public boolean isFixedSampleSize() {
            return this.fixedSampleSize != 0;
        }

        public int readNextSampleSize() {
            return this.fixedSampleSize == 0 ? this.data.readUnsignedIntToInt() : this.fixedSampleSize;
        }
    }

    static final class Stz2SampleSizeBox implements SampleSizeBox {
        private int currentByte;
        private final ParsableByteArray data;
        private final int fieldSize = (this.data.readUnsignedIntToInt() & 255);
        private final int sampleCount = this.data.readUnsignedIntToInt();
        private int sampleIndex;

        public Stz2SampleSizeBox(Atom.LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public boolean isFixedSampleSize() {
            return false;
        }

        public int readNextSampleSize() {
            if (this.fieldSize == 8) {
                return this.data.readUnsignedByte();
            }
            if (this.fieldSize == 16) {
                return this.data.readUnsignedShort();
            }
            int i = this.sampleIndex;
            this.sampleIndex = i + 1;
            if (i % 2 != 0) {
                return this.currentByte & 15;
            }
            this.currentByte = this.data.readUnsignedByte();
            return (this.currentByte & 240) >> 4;
        }
    }

    private static final class TkhdData {
        /* access modifiers changed from: private */
        public final long duration;
        /* access modifiers changed from: private */
        public final int id;
        /* access modifiers changed from: private */
        public final int rotationDegrees;

        public TkhdData(int i, long j, int i2) {
            this.id = i;
            this.duration = j;
            this.rotationDegrees = i2;
        }
    }

    private AtomParsers() {
    }

    private static int findEsdsPosition(ParsableByteArray parsableByteArray, int i, int i2) {
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_esds) {
                return position;
            }
            position += readInt;
        }
        return -1;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v26, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v3, resolved type: byte[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void parseAudioSampleEntry(com.google.android.exoplayer2.util.ParsableByteArray r25, int r26, int r27, int r28, int r29, java.lang.String r30, boolean r31, com.google.android.exoplayer2.drm.DrmInitData r32, com.google.android.exoplayer2.extractor.mp4.AtomParsers.StsdData r33, int r34) {
        /*
            int r4 = r27 + 8
            int r4 = r4 + 8
            r0 = r25
            r0.setPosition(r4)
            r23 = 0
            if (r31 == 0) goto L_0x00ee
            int r23 = r25.readUnsignedShort()
            r4 = 6
            r0 = r25
            r0.skipBytes(r4)
        L_0x0017:
            if (r23 == 0) goto L_0x001e
            r4 = 1
            r0 = r23
            if (r0 != r4) goto L_0x00f7
        L_0x001e:
            int r9 = r25.readUnsignedShort()
            r4 = 6
            r0 = r25
            r0.skipBytes(r4)
            int r10 = r25.readUnsignedFixedPoint1616()
            r4 = 1
            r0 = r23
            if (r0 != r4) goto L_0x0038
            r4 = 16
            r0 = r25
            r0.skipBytes(r4)
        L_0x0038:
            int r19 = r25.getPosition()
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_enca
            r0 = r26
            if (r0 != r4) goto L_0x006f
            r0 = r25
            r1 = r27
            r2 = r28
            android.util.Pair r24 = parseSampleEntryEncryptionData(r0, r1, r2)
            if (r24 == 0) goto L_0x0068
            r0 = r24
            java.lang.Object r4 = r0.first
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r26 = r4.intValue()
            if (r32 != 0) goto L_0x0119
            r32 = 0
        L_0x005c:
            r0 = r33
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox[] r6 = r0.trackEncryptionBoxes
            r0 = r24
            java.lang.Object r4 = r0.second
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox r4 = (com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox) r4
            r6[r34] = r4
        L_0x0068:
            r0 = r25
            r1 = r19
            r0.setPosition(r1)
        L_0x006f:
            r5 = 0
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_ac_3
            r0 = r26
            if (r0 != r4) goto L_0x0129
            java.lang.String r5 = "audio/ac3"
        L_0x0079:
            r21 = 0
        L_0x007b:
            int r4 = r19 - r27
            r0 = r28
            if (r4 >= r0) goto L_0x0225
            r0 = r25
            r1 = r19
            r0.setPosition(r1)
            int r17 = r25.readInt()
            if (r17 <= 0) goto L_0x0198
            r4 = 1
        L_0x008f:
            java.lang.String r6 = "childAtomSize should be positive"
            com.google.android.exoplayer2.util.Assertions.checkArgument(r4, r6)
            int r18 = r25.readInt()
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_esds
            r0 = r18
            if (r0 == r4) goto L_0x00a7
            if (r31 == 0) goto L_0x01a7
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_wave
            r0 = r18
            if (r0 != r4) goto L_0x01a7
        L_0x00a7:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_esds
            r0 = r18
            if (r0 != r4) goto L_0x019b
            r20 = r19
        L_0x00af:
            r4 = -1
            r0 = r20
            if (r0 == r4) goto L_0x00eb
            r0 = r25
            r1 = r20
            android.util.Pair r22 = parseEsdsFromParent(r0, r1)
            r0 = r22
            java.lang.Object r5 = r0.first
            java.lang.String r5 = (java.lang.String) r5
            r0 = r22
            java.lang.Object r0 = r0.second
            r21 = r0
            byte[] r21 = (byte[]) r21
            java.lang.String r4 = "audio/mp4a-latm"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x00eb
            android.util.Pair r16 = com.google.android.exoplayer2.util.CodecSpecificDataUtil.parseAacAudioSpecificConfig(r21)
            r0 = r16
            java.lang.Object r4 = r0.first
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r10 = r4.intValue()
            r0 = r16
            java.lang.Object r4 = r0.second
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r9 = r4.intValue()
        L_0x00eb:
            int r19 = r19 + r17
            goto L_0x007b
        L_0x00ee:
            r4 = 8
            r0 = r25
            r0.skipBytes(r4)
            goto L_0x0017
        L_0x00f7:
            r4 = 2
            r0 = r23
            if (r0 != r4) goto L_0x024e
            r4 = 16
            r0 = r25
            r0.skipBytes(r4)
            double r6 = r25.readDouble()
            long r6 = java.lang.Math.round(r6)
            int r10 = (int) r6
            int r9 = r25.readUnsignedIntToInt()
            r4 = 20
            r0 = r25
            r0.skipBytes(r4)
            goto L_0x0038
        L_0x0119:
            r0 = r24
            java.lang.Object r4 = r0.second
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox r4 = (com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox) r4
            java.lang.String r4 = r4.schemeType
            r0 = r32
            com.google.android.exoplayer2.drm.DrmInitData r32 = r0.copyWithSchemeType(r4)
            goto L_0x005c
        L_0x0129:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_ec_3
            r0 = r26
            if (r0 != r4) goto L_0x0134
            java.lang.String r5 = "audio/eac3"
            goto L_0x0079
        L_0x0134:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_dtsc
            r0 = r26
            if (r0 != r4) goto L_0x013f
            java.lang.String r5 = "audio/vnd.dts"
            goto L_0x0079
        L_0x013f:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_dtsh
            r0 = r26
            if (r0 == r4) goto L_0x014b
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_dtsl
            r0 = r26
            if (r0 != r4) goto L_0x0150
        L_0x014b:
            java.lang.String r5 = "audio/vnd.dts.hd"
            goto L_0x0079
        L_0x0150:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_dtse
            r0 = r26
            if (r0 != r4) goto L_0x015b
            java.lang.String r5 = "audio/vnd.dts.hd;profile=lbr"
            goto L_0x0079
        L_0x015b:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_samr
            r0 = r26
            if (r0 != r4) goto L_0x0166
            java.lang.String r5 = "audio/3gpp"
            goto L_0x0079
        L_0x0166:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_sawb
            r0 = r26
            if (r0 != r4) goto L_0x0171
            java.lang.String r5 = "audio/amr-wb"
            goto L_0x0079
        L_0x0171:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_lpcm
            r0 = r26
            if (r0 == r4) goto L_0x017d
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_sowt
            r0 = r26
            if (r0 != r4) goto L_0x0182
        L_0x017d:
            java.lang.String r5 = "audio/raw"
            goto L_0x0079
        L_0x0182:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE__mp3
            r0 = r26
            if (r0 != r4) goto L_0x018d
            java.lang.String r5 = "audio/mpeg"
            goto L_0x0079
        L_0x018d:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_alac
            r0 = r26
            if (r0 != r4) goto L_0x0079
            java.lang.String r5 = "audio/alac"
            goto L_0x0079
        L_0x0198:
            r4 = 0
            goto L_0x008f
        L_0x019b:
            r0 = r25
            r1 = r19
            r2 = r17
            int r20 = findEsdsPosition(r0, r1, r2)
            goto L_0x00af
        L_0x01a7:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_dac3
            r0 = r18
            if (r0 != r4) goto L_0x01c8
            int r4 = r19 + 8
            r0 = r25
            r0.setPosition(r4)
            java.lang.String r4 = java.lang.Integer.toString(r29)
            r0 = r25
            r1 = r30
            r2 = r32
            com.google.android.exoplayer2.Format r4 = com.google.android.exoplayer2.audio.Ac3Util.parseAc3AnnexFFormat(r0, r4, r1, r2)
            r0 = r33
            r0.format = r4
            goto L_0x00eb
        L_0x01c8:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_dec3
            r0 = r18
            if (r0 != r4) goto L_0x01e9
            int r4 = r19 + 8
            r0 = r25
            r0.setPosition(r4)
            java.lang.String r4 = java.lang.Integer.toString(r29)
            r0 = r25
            r1 = r30
            r2 = r32
            com.google.android.exoplayer2.Format r4 = com.google.android.exoplayer2.audio.Ac3Util.parseEAc3AnnexFFormat(r0, r4, r1, r2)
            r0 = r33
            r0.format = r4
            goto L_0x00eb
        L_0x01e9:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_ddts
            r0 = r18
            if (r0 != r4) goto L_0x0206
            java.lang.String r4 = java.lang.Integer.toString(r29)
            r6 = 0
            r7 = -1
            r8 = -1
            r11 = 0
            r13 = 0
            r12 = r32
            r14 = r30
            com.google.android.exoplayer2.Format r4 = com.google.android.exoplayer2.Format.createAudioSampleFormat(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r33
            r0.format = r4
            goto L_0x00eb
        L_0x0206:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_alac
            r0 = r18
            if (r0 != r4) goto L_0x00eb
            r0 = r17
            byte[] r0 = new byte[r0]
            r21 = r0
            r0 = r25
            r1 = r19
            r0.setPosition(r1)
            r4 = 0
            r0 = r25
            r1 = r21
            r2 = r17
            r0.readBytes(r1, r4, r2)
            goto L_0x00eb
        L_0x0225:
            r0 = r33
            com.google.android.exoplayer2.Format r4 = r0.format
            if (r4 != 0) goto L_0x024e
            if (r5 == 0) goto L_0x024e
            java.lang.String r4 = "audio/raw"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x024f
            r11 = 2
        L_0x0237:
            java.lang.String r4 = java.lang.Integer.toString(r29)
            r6 = 0
            r7 = -1
            r8 = -1
            if (r21 != 0) goto L_0x0251
            r12 = 0
        L_0x0241:
            r14 = 0
            r13 = r32
            r15 = r30
            com.google.android.exoplayer2.Format r4 = com.google.android.exoplayer2.Format.createAudioSampleFormat(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            r0 = r33
            r0.format = r4
        L_0x024e:
            return
        L_0x024f:
            r11 = -1
            goto L_0x0237
        L_0x0251:
            java.util.List r12 = java.util.Collections.singletonList(r21)
            goto L_0x0241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.AtomParsers.parseAudioSampleEntry(com.google.android.exoplayer2.util.ParsableByteArray, int, int, int, int, java.lang.String, boolean, com.google.android.exoplayer2.drm.DrmInitData, com.google.android.exoplayer2.extractor.mp4.AtomParsers$StsdData, int):void");
    }

    static Pair<Integer, TrackEncryptionBox> parseCommonEncryptionSinfFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        boolean z = true;
        int i3 = i + 8;
        int i4 = -1;
        int i5 = 0;
        String str = null;
        Integer num = null;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_frma) {
                num = Integer.valueOf(parsableByteArray.readInt());
            } else if (readInt2 == Atom.TYPE_schm) {
                parsableByteArray.skipBytes(4);
                str = parsableByteArray.readString(4);
            } else if (readInt2 == Atom.TYPE_schi) {
                i4 = i3;
                i5 = readInt;
            }
            i3 += readInt;
        }
        if (!C.CENC_TYPE_cenc.equals(str) && !C.CENC_TYPE_cbc1.equals(str) && !C.CENC_TYPE_cens.equals(str) && !C.CENC_TYPE_cbcs.equals(str)) {
            return null;
        }
        Assertions.checkArgument(num != null, "frma atom is mandatory");
        Assertions.checkArgument(i4 != -1, "schi atom is mandatory");
        TrackEncryptionBox parseSchiFromParent = parseSchiFromParent(parsableByteArray, i4, i5, str);
        if (parseSchiFromParent == null) {
            z = false;
        }
        Assertions.checkArgument(z, "tenc atom is mandatory");
        return Pair.create(num, parseSchiFromParent);
    }

    private static Pair<long[], long[]> parseEdts(Atom.ContainerAtom containerAtom) {
        Atom.LeafAtom leafAtomOfType;
        if (containerAtom == null || (leafAtomOfType = containerAtom.getLeafAtomOfType(Atom.TYPE_elst)) == null) {
            return Pair.create((Object) null, (Object) null);
        }
        ParsableByteArray parsableByteArray = leafAtomOfType.data;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
        long[] jArr = new long[readUnsignedIntToInt];
        long[] jArr2 = new long[readUnsignedIntToInt];
        for (int i = 0; i < readUnsignedIntToInt; i++) {
            jArr[i] = parseFullAtomVersion == 1 ? parsableByteArray.readUnsignedLongToLong() : parsableByteArray.readUnsignedInt();
            jArr2[i] = parseFullAtomVersion == 1 ? parsableByteArray.readLong() : (long) parsableByteArray.readInt();
            if (parsableByteArray.readShort() != 1) {
                throw new IllegalArgumentException("Unsupported media rate.");
            }
            parsableByteArray.skipBytes(2);
        }
        return Pair.create(jArr, jArr2);
    }

    private static Pair<String, byte[]> parseEsdsFromParent(ParsableByteArray parsableByteArray, int i) {
        String str;
        parsableByteArray.setPosition(i + 8 + 4);
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        parsableByteArray.skipBytes(2);
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            parsableByteArray.skipBytes(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            parsableByteArray.skipBytes(parsableByteArray.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            parsableByteArray.skipBytes(2);
        }
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        switch (parsableByteArray.readUnsignedByte()) {
            case 32:
                str = MimeTypes.VIDEO_MP4V;
                break;
            case 33:
                str = MimeTypes.VIDEO_H264;
                break;
            case 35:
                str = MimeTypes.VIDEO_H265;
                break;
            case 64:
            case 102:
            case 103:
            case 104:
                str = MimeTypes.AUDIO_AAC;
                break;
            case 96:
            case 97:
                str = MimeTypes.VIDEO_MPEG2;
                break;
            case 107:
                return Pair.create(MimeTypes.AUDIO_MPEG, (Object) null);
            case 165:
                str = MimeTypes.AUDIO_AC3;
                break;
            case 166:
                str = MimeTypes.AUDIO_E_AC3;
                break;
            case 169:
            case 172:
                return Pair.create(MimeTypes.AUDIO_DTS, (Object) null);
            case 170:
            case 171:
                return Pair.create(MimeTypes.AUDIO_DTS_HD, (Object) null);
            default:
                str = null;
                break;
        }
        parsableByteArray.skipBytes(12);
        parsableByteArray.skipBytes(1);
        int parseExpandableClassSize = parseExpandableClassSize(parsableByteArray);
        byte[] bArr = new byte[parseExpandableClassSize];
        parsableByteArray.readBytes(bArr, 0, parseExpandableClassSize);
        return Pair.create(str, bArr);
    }

    private static int parseExpandableClassSize(ParsableByteArray parsableByteArray) {
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = parsableByteArray.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }

    private static int parseHdlr(ParsableByteArray parsableByteArray) {
        parsableByteArray.setPosition(16);
        int readInt = parsableByteArray.readInt();
        if (readInt == TYPE_soun) {
            return 1;
        }
        if (readInt == TYPE_vide) {
            return 2;
        }
        if (readInt == TYPE_text || readInt == TYPE_sbtl || readInt == TYPE_subt || readInt == TYPE_clcp) {
            return 3;
        }
        return readInt == TYPE_meta ? 4 : -1;
    }

    private static Metadata parseIlst(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.skipBytes(8);
        ArrayList arrayList = new ArrayList();
        while (parsableByteArray.getPosition() < i) {
            Metadata.Entry parseIlstElement = MetadataUtil.parseIlstElement(parsableByteArray);
            if (parseIlstElement != null) {
                arrayList.add(parseIlstElement);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata((List<? extends Metadata.Entry>) arrayList);
    }

    private static Pair<Long, String> parseMdhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        long readUnsignedInt = parsableByteArray.readUnsignedInt();
        if (parseFullAtomVersion == 0) {
            i = 4;
        }
        parsableByteArray.skipBytes(i);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        return Pair.create(Long.valueOf(readUnsignedInt), "" + ((char) (((readUnsignedShort >> 10) & 31) + 96)) + ((char) (((readUnsignedShort >> 5) & 31) + 96)) + ((char) ((readUnsignedShort & 31) + 96)));
    }

    private static Metadata parseMetaAtom(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.skipBytes(12);
        while (parsableByteArray.getPosition() < i) {
            int position = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_ilst) {
                parsableByteArray.setPosition(position);
                return parseIlst(parsableByteArray, position + readInt);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    private static long parseMvhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        if (Atom.parseFullAtomVersion(parsableByteArray.readInt()) != 0) {
            i = 16;
        }
        parsableByteArray.skipBytes(i);
        return parsableByteArray.readUnsignedInt();
    }

    private static float parsePaspFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8);
        return ((float) parsableByteArray.readUnsignedIntToInt()) / ((float) parsableByteArray.readUnsignedIntToInt());
    }

    private static byte[] parseProjFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_proj) {
                return Arrays.copyOfRange(parsableByteArray.data, i3, i3 + readInt);
            }
            i3 += readInt;
        }
        return null;
    }

    private static Pair<Integer, TrackEncryptionBox> parseSampleEntryEncryptionData(ParsableByteArray parsableByteArray, int i, int i2) {
        Pair<Integer, TrackEncryptionBox> parseCommonEncryptionSinfFromParent;
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_sinf && (parseCommonEncryptionSinfFromParent = parseCommonEncryptionSinfFromParent(parsableByteArray, position, readInt)) != null) {
                return parseCommonEncryptionSinfFromParent;
            }
            position += readInt;
        }
        return null;
    }

    private static TrackEncryptionBox parseSchiFromParent(ParsableByteArray parsableByteArray, int i, int i2, String str) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_tenc) {
                int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
                parsableByteArray.skipBytes(1);
                int i4 = 0;
                int i5 = 0;
                if (parseFullAtomVersion == 0) {
                    parsableByteArray.skipBytes(1);
                } else {
                    int readUnsignedByte = parsableByteArray.readUnsignedByte();
                    i4 = (readUnsignedByte & 240) >> 4;
                    i5 = readUnsignedByte & 15;
                }
                boolean z = parsableByteArray.readUnsignedByte() == 1;
                int readUnsignedByte2 = parsableByteArray.readUnsignedByte();
                byte[] bArr = new byte[16];
                parsableByteArray.readBytes(bArr, 0, bArr.length);
                byte[] bArr2 = null;
                if (z && readUnsignedByte2 == 0) {
                    int readUnsignedByte3 = parsableByteArray.readUnsignedByte();
                    bArr2 = new byte[readUnsignedByte3];
                    parsableByteArray.readBytes(bArr2, 0, readUnsignedByte3);
                }
                return new TrackEncryptionBox(z, str, readUnsignedByte2, bArr, i4, i5, bArr2);
            }
            i3 += readInt;
        }
        return null;
    }

    /* JADX WARNING: type inference failed for: r74v1, types: [com.google.android.exoplayer2.extractor.mp4.AtomParsers$SampleSizeBox] */
    /* JADX WARNING: type inference failed for: r0v116, types: [com.google.android.exoplayer2.extractor.mp4.AtomParsers$Stz2SampleSizeBox] */
    /* JADX WARNING: type inference failed for: r0v117, types: [com.google.android.exoplayer2.extractor.mp4.AtomParsers$StszSampleSizeBox] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.exoplayer2.extractor.mp4.TrackSampleTable parseStbl(com.google.android.exoplayer2.extractor.mp4.Track r88, com.google.android.exoplayer2.extractor.mp4.Atom.ContainerAtom r89, com.google.android.exoplayer2.extractor.GaplessInfoHolder r90) throws com.google.android.exoplayer2.ParserException {
        /*
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stsz
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r79 = r0.getLeafAtomOfType(r4)
            if (r79 == 0) goto L_0x002c
            com.google.android.exoplayer2.extractor.mp4.AtomParsers$StszSampleSizeBox r74 = new com.google.android.exoplayer2.extractor.mp4.AtomParsers$StszSampleSizeBox
            r0 = r74
            r1 = r79
            r0.<init>(r1)
        L_0x0013:
            int r72 = r74.getSampleCount()
            if (r72 != 0) goto L_0x0049
            com.google.android.exoplayer2.extractor.mp4.TrackSampleTable r4 = new com.google.android.exoplayer2.extractor.mp4.TrackSampleTable
            r12 = 0
            long[] r5 = new long[r12]
            r12 = 0
            int[] r6 = new int[r12]
            r7 = 0
            r12 = 0
            long[] r8 = new long[r12]
            r12 = 0
            int[] r9 = new int[r12]
            r4.<init>(r5, r6, r7, r8, r9)
        L_0x002b:
            return r4
        L_0x002c:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stz2
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r81 = r0.getLeafAtomOfType(r4)
            if (r81 != 0) goto L_0x003f
            com.google.android.exoplayer2.ParserException r4 = new com.google.android.exoplayer2.ParserException
            java.lang.String r12 = "Track has no sample table size information"
            r4.<init>((java.lang.String) r12)
            throw r4
        L_0x003f:
            com.google.android.exoplayer2.extractor.mp4.AtomParsers$Stz2SampleSizeBox r74 = new com.google.android.exoplayer2.extractor.mp4.AtomParsers$Stz2SampleSizeBox
            r0 = r74
            r1 = r81
            r0.<init>(r1)
            goto L_0x0013
        L_0x0049:
            r20 = 0
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stco
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r21 = r0.getLeafAtomOfType(r4)
            if (r21 != 0) goto L_0x005f
            r20 = 1
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_co64
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r21 = r0.getLeafAtomOfType(r4)
        L_0x005f:
            r0 = r21
            com.google.android.exoplayer2.util.ParsableByteArray r0 = r0.data
            r19 = r0
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stsc
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r4 = r0.getLeafAtomOfType(r4)
            com.google.android.exoplayer2.util.ParsableByteArray r0 = r4.data
            r76 = r0
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stts
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r4 = r0.getLeafAtomOfType(r4)
            com.google.android.exoplayer2.util.ParsableByteArray r0 = r4.data
            r80 = r0
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stss
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r78 = r0.getLeafAtomOfType(r4)
            if (r78 == 0) goto L_0x013f
            r0 = r78
            com.google.android.exoplayer2.util.ParsableByteArray r0 = r0.data
            r77 = r0
        L_0x008d:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_ctts
            r0 = r89
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r27 = r0.getLeafAtomOfType(r4)
            if (r27 == 0) goto L_0x0143
            r0 = r27
            com.google.android.exoplayer2.util.ParsableByteArray r0 = r0.data
            r26 = r0
        L_0x009d:
            com.google.android.exoplayer2.extractor.mp4.AtomParsers$ChunkIterator r18 = new com.google.android.exoplayer2.extractor.mp4.AtomParsers$ChunkIterator
            r0 = r18
            r1 = r76
            r2 = r19
            r3 = r20
            r0.<init>(r1, r2, r3)
            r4 = 12
            r0 = r80
            r0.setPosition(r4)
            int r4 = r80.readUnsignedIntToInt()
            int r70 = r4 + -1
            int r66 = r80.readUnsignedIntToInt()
            int r84 = r80.readUnsignedIntToInt()
            r67 = 0
            r71 = 0
            r85 = 0
            if (r26 == 0) goto L_0x00d2
            r4 = 12
            r0 = r26
            r0.setPosition(r4)
            int r71 = r26.readUnsignedIntToInt()
        L_0x00d2:
            r55 = -1
            r69 = 0
            if (r77 == 0) goto L_0x00eb
            r4 = 12
            r0 = r77
            r0.setPosition(r4)
            int r69 = r77.readUnsignedIntToInt()
            if (r69 <= 0) goto L_0x0147
            int r4 = r77.readUnsignedIntToInt()
            int r55 = r4 + -1
        L_0x00eb:
            boolean r4 = r74.isFixedSampleSize()
            if (r4 == 0) goto L_0x014a
            java.lang.String r4 = "audio/raw"
            r0 = r88
            com.google.android.exoplayer2.Format r12 = r0.format
            java.lang.String r12 = r12.sampleMimeType
            boolean r4 = r4.equals(r12)
            if (r4 == 0) goto L_0x014a
            if (r70 != 0) goto L_0x014a
            if (r71 != 0) goto L_0x014a
            if (r69 != 0) goto L_0x014a
            r48 = 1
        L_0x0108:
            r7 = 0
            r86 = 0
            if (r48 != 0) goto L_0x0242
            r0 = r72
            long[] r5 = new long[r0]
            r0 = r72
            int[] r6 = new int[r0]
            r0 = r72
            long[] r8 = new long[r0]
            r0 = r72
            int[] r9 = new int[r0]
            r56 = 0
            r68 = 0
            r47 = 0
        L_0x0123:
            r0 = r47
            r1 = r72
            if (r0 >= r1) goto L_0x01af
        L_0x0129:
            if (r68 != 0) goto L_0x014d
            boolean r4 = r18.moveNext()
            com.google.android.exoplayer2.util.Assertions.checkState(r4)
            r0 = r18
            long r0 = r0.offset
            r56 = r0
            r0 = r18
            int r0 = r0.numSamples
            r68 = r0
            goto L_0x0129
        L_0x013f:
            r77 = 0
            goto L_0x008d
        L_0x0143:
            r26 = 0
            goto L_0x009d
        L_0x0147:
            r77 = 0
            goto L_0x00eb
        L_0x014a:
            r48 = 0
            goto L_0x0108
        L_0x014d:
            if (r26 == 0) goto L_0x0160
        L_0x014f:
            if (r67 != 0) goto L_0x015e
            if (r71 <= 0) goto L_0x015e
            int r67 = r26.readUnsignedIntToInt()
            int r85 = r26.readInt()
            int r71 = r71 + -1
            goto L_0x014f
        L_0x015e:
            int r67 = r67 + -1
        L_0x0160:
            r5[r47] = r56
            int r4 = r74.readNextSampleSize()
            r6[r47] = r4
            r4 = r6[r47]
            if (r4 <= r7) goto L_0x016e
            r7 = r6[r47]
        L_0x016e:
            r0 = r85
            long r12 = (long) r0
            long r12 = r12 + r86
            r8[r47] = r12
            if (r77 != 0) goto L_0x01ad
            r4 = 1
        L_0x0178:
            r9[r47] = r4
            r0 = r47
            r1 = r55
            if (r0 != r1) goto L_0x018d
            r4 = 1
            r9[r47] = r4
            int r69 = r69 + -1
            if (r69 <= 0) goto L_0x018d
            int r4 = r77.readUnsignedIntToInt()
            int r55 = r4 + -1
        L_0x018d:
            r0 = r84
            long r12 = (long) r0
            long r86 = r86 + r12
            int r66 = r66 + -1
            if (r66 != 0) goto L_0x01a2
            if (r70 <= 0) goto L_0x01a2
            int r66 = r80.readUnsignedIntToInt()
            int r84 = r80.readInt()
            int r70 = r70 + -1
        L_0x01a2:
            r4 = r6[r47]
            long r12 = (long) r4
            long r56 = r56 + r12
            int r68 = r68 + -1
            int r47 = r47 + 1
            goto L_0x0123
        L_0x01ad:
            r4 = 0
            goto L_0x0178
        L_0x01af:
            if (r67 != 0) goto L_0x01c7
            r4 = 1
        L_0x01b2:
            com.google.android.exoplayer2.util.Assertions.checkArgument(r4)
        L_0x01b5:
            if (r71 <= 0) goto L_0x01cb
            int r4 = r26.readUnsignedIntToInt()
            if (r4 != 0) goto L_0x01c9
            r4 = 1
        L_0x01be:
            com.google.android.exoplayer2.util.Assertions.checkArgument(r4)
            r26.readInt()
            int r71 = r71 + -1
            goto L_0x01b5
        L_0x01c7:
            r4 = 0
            goto L_0x01b2
        L_0x01c9:
            r4 = 0
            goto L_0x01be
        L_0x01cb:
            if (r69 != 0) goto L_0x01d3
            if (r66 != 0) goto L_0x01d3
            if (r68 != 0) goto L_0x01d3
            if (r70 == 0) goto L_0x0225
        L_0x01d3:
            java.lang.String r4 = "AtomParsers"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "Inconsistent stbl box for track "
            java.lang.StringBuilder r12 = r12.append(r13)
            r0 = r88
            int r13 = r0.id
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = ": remainingSynchronizationSamples "
            java.lang.StringBuilder r12 = r12.append(r13)
            r0 = r69
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r13 = ", remainingSamplesAtTimestampDelta "
            java.lang.StringBuilder r12 = r12.append(r13)
            r0 = r66
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r13 = ", remainingSamplesInChunk "
            java.lang.StringBuilder r12 = r12.append(r13)
            r0 = r68
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r13 = ", remainingTimestampDeltaChanges "
            java.lang.StringBuilder r12 = r12.append(r13)
            r0 = r70
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r12 = r12.toString()
            android.util.Log.w(r4, r12)
        L_0x0225:
            r0 = r88
            long[] r4 = r0.editListDurations
            if (r4 == 0) goto L_0x0231
            boolean r4 = r90.hasGaplessInfo()
            if (r4 == 0) goto L_0x0293
        L_0x0231:
            r12 = 1000000(0xf4240, double:4.940656E-318)
            r0 = r88
            long r14 = r0.timescale
            com.google.android.exoplayer2.util.Util.scaleLargeTimestampsInPlace(r8, r12, r14)
            com.google.android.exoplayer2.extractor.mp4.TrackSampleTable r4 = new com.google.android.exoplayer2.extractor.mp4.TrackSampleTable
            r4.<init>(r5, r6, r7, r8, r9)
            goto L_0x002b
        L_0x0242:
            r0 = r18
            int r4 = r0.length
            long[] r0 = new long[r4]
            r22 = r0
            r0 = r18
            int r4 = r0.length
            int[] r0 = new int[r4]
            r23 = r0
        L_0x0252:
            boolean r4 = r18.moveNext()
            if (r4 == 0) goto L_0x026d
            r0 = r18
            int r4 = r0.index
            r0 = r18
            long r12 = r0.offset
            r22[r4] = r12
            r0 = r18
            int r4 = r0.index
            r0 = r18
            int r12 = r0.numSamples
            r23[r4] = r12
            goto L_0x0252
        L_0x026d:
            int r43 = r74.readNextSampleSize()
            r0 = r84
            long r12 = (long) r0
            r0 = r43
            r1 = r22
            r2 = r23
            com.google.android.exoplayer2.extractor.mp4.FixedSampleSizeRechunker$Results r59 = com.google.android.exoplayer2.extractor.mp4.FixedSampleSizeRechunker.rechunk(r0, r1, r2, r12)
            r0 = r59
            long[] r5 = r0.offsets
            r0 = r59
            int[] r6 = r0.sizes
            r0 = r59
            int r7 = r0.maximumSize
            r0 = r59
            long[] r8 = r0.timestamps
            r0 = r59
            int[] r9 = r0.flags
            goto L_0x0225
        L_0x0293:
            r0 = r88
            long[] r4 = r0.editListDurations
            int r4 = r4.length
            r12 = 1
            if (r4 != r12) goto L_0x033f
            r0 = r88
            int r4 = r0.type
            r12 = 1
            if (r4 != r12) goto L_0x033f
            int r4 = r8.length
            r12 = 2
            if (r4 < r12) goto L_0x033f
            r0 = r88
            long[] r4 = r0.editListMediaTimes
            r12 = 0
            r30 = r4[r12]
            r0 = r88
            long[] r4 = r0.editListDurations
            r12 = 0
            r10 = r4[r12]
            r0 = r88
            long r12 = r0.timescale
            r0 = r88
            long r14 = r0.movieTimescale
            long r12 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r10, r12, r14)
            long r28 = r30 + r12
            r50 = r86
            r4 = 0
            r12 = r8[r4]
            int r4 = (r12 > r30 ? 1 : (r12 == r30 ? 0 : -1))
            if (r4 > 0) goto L_0x033f
            r4 = 1
            r12 = r8[r4]
            int r4 = (r30 > r12 ? 1 : (r30 == r12 ? 0 : -1))
            if (r4 >= 0) goto L_0x033f
            int r4 = r8.length
            int r4 = r4 + -1
            r12 = r8[r4]
            int r4 = (r12 > r28 ? 1 : (r12 == r28 ? 0 : -1))
            if (r4 >= 0) goto L_0x033f
            int r4 = (r28 > r50 ? 1 : (r28 == r50 ? 0 : -1))
            if (r4 > 0) goto L_0x033f
            long r60 = r50 - r28
            r4 = 0
            r12 = r8[r4]
            long r10 = r30 - r12
            r0 = r88
            com.google.android.exoplayer2.Format r4 = r0.format
            int r4 = r4.sampleRate
            long r12 = (long) r4
            r0 = r88
            long r14 = r0.timescale
            long r38 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r10, r12, r14)
            r0 = r88
            com.google.android.exoplayer2.Format r4 = r0.format
            int r4 = r4.sampleRate
            long r12 = (long) r4
            r0 = r88
            long r14 = r0.timescale
            r10 = r60
            long r40 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r10, r12, r14)
            r12 = 0
            int r4 = (r38 > r12 ? 1 : (r38 == r12 ? 0 : -1))
            if (r4 != 0) goto L_0x0312
            r12 = 0
            int r4 = (r40 > r12 ? 1 : (r40 == r12 ? 0 : -1))
            if (r4 == 0) goto L_0x033f
        L_0x0312:
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r4 = (r38 > r12 ? 1 : (r38 == r12 ? 0 : -1))
            if (r4 > 0) goto L_0x033f
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r4 = (r40 > r12 ? 1 : (r40 == r12 ? 0 : -1))
            if (r4 > 0) goto L_0x033f
            r0 = r38
            int r4 = (int) r0
            r0 = r90
            r0.encoderDelay = r4
            r0 = r40
            int r4 = (int) r0
            r0 = r90
            r0.encoderPadding = r4
            r12 = 1000000(0xf4240, double:4.940656E-318)
            r0 = r88
            long r14 = r0.timescale
            com.google.android.exoplayer2.util.Util.scaleLargeTimestampsInPlace(r8, r12, r14)
            com.google.android.exoplayer2.extractor.mp4.TrackSampleTable r4 = new com.google.android.exoplayer2.extractor.mp4.TrackSampleTable
            r4.<init>(r5, r6, r7, r8, r9)
            goto L_0x002b
        L_0x033f:
            r0 = r88
            long[] r4 = r0.editListDurations
            int r4 = r4.length
            r12 = 1
            if (r4 != r12) goto L_0x037d
            r0 = r88
            long[] r4 = r0.editListDurations
            r12 = 0
            r12 = r4[r12]
            r14 = 0
            int r4 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r4 != 0) goto L_0x037d
            r47 = 0
        L_0x0356:
            int r4 = r8.length
            r0 = r47
            if (r0 >= r4) goto L_0x0376
            r12 = r8[r47]
            r0 = r88
            long[] r4 = r0.editListMediaTimes
            r14 = 0
            r14 = r4[r14]
            long r10 = r12 - r14
            r12 = 1000000(0xf4240, double:4.940656E-318)
            r0 = r88
            long r14 = r0.timescale
            long r12 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r10, r12, r14)
            r8[r47] = r12
            int r47 = r47 + 1
            goto L_0x0356
        L_0x0376:
            com.google.android.exoplayer2.extractor.mp4.TrackSampleTable r4 = new com.google.android.exoplayer2.extractor.mp4.TrackSampleTable
            r4.<init>(r5, r6, r7, r8, r9)
            goto L_0x002b
        L_0x037d:
            r0 = r88
            int r4 = r0.type
            r12 = 1
            if (r4 != r12) goto L_0x03d8
            r58 = 1
        L_0x0386:
            r35 = 0
            r54 = 0
            r24 = 0
            r47 = 0
        L_0x038e:
            r0 = r88
            long[] r4 = r0.editListDurations
            int r4 = r4.length
            r0 = r47
            if (r0 >= r4) goto L_0x03dd
            r0 = r88
            long[] r4 = r0.editListMediaTimes
            r52 = r4[r47]
            r12 = -1
            int r4 = (r52 > r12 ? 1 : (r52 == r12 ? 0 : -1))
            if (r4 == 0) goto L_0x03d5
            r0 = r88
            long[] r4 = r0.editListDurations
            r10 = r4[r47]
            r0 = r88
            long r12 = r0.timescale
            r0 = r88
            long r14 = r0.movieTimescale
            long r10 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r10, r12, r14)
            r4 = 1
            r12 = 1
            r0 = r52
            int r75 = com.google.android.exoplayer2.util.Util.binarySearchCeil((long[]) r8, (long) r0, (boolean) r4, (boolean) r12)
            long r12 = r52 + r10
            r4 = 0
            r0 = r58
            int r42 = com.google.android.exoplayer2.util.Util.binarySearchCeil((long[]) r8, (long) r12, (boolean) r0, (boolean) r4)
            int r4 = r42 - r75
            int r35 = r35 + r4
            r0 = r54
            r1 = r75
            if (r0 == r1) goto L_0x03db
            r4 = 1
        L_0x03d1:
            r24 = r24 | r4
            r54 = r42
        L_0x03d5:
            int r47 = r47 + 1
            goto L_0x038e
        L_0x03d8:
            r58 = 0
            goto L_0x0386
        L_0x03db:
            r4 = 0
            goto L_0x03d1
        L_0x03dd:
            r0 = r35
            r1 = r72
            if (r0 == r1) goto L_0x04a8
            r4 = 1
        L_0x03e4:
            r24 = r24 | r4
            if (r24 == 0) goto L_0x04ab
            r0 = r35
            long[] r0 = new long[r0]
            r34 = r0
        L_0x03ee:
            if (r24 == 0) goto L_0x04af
            r0 = r35
            int[] r0 = new int[r0]
            r36 = r0
        L_0x03f6:
            if (r24 == 0) goto L_0x04b3
            r33 = 0
        L_0x03fa:
            if (r24 == 0) goto L_0x04b7
            r0 = r35
            int[] r0 = new int[r0]
            r32 = r0
        L_0x0402:
            r0 = r35
            long[] r0 = new long[r0]
            r37 = r0
            r62 = 0
            r73 = 0
            r47 = 0
        L_0x040e:
            r0 = r88
            long[] r4 = r0.editListDurations
            int r4 = r4.length
            r0 = r47
            if (r0 >= r4) goto L_0x04c1
            r0 = r88
            long[] r4 = r0.editListMediaTimes
            r52 = r4[r47]
            r0 = r88
            long[] r4 = r0.editListDurations
            r10 = r4[r47]
            r12 = -1
            int r4 = (r52 > r12 ? 1 : (r52 == r12 ? 0 : -1))
            if (r4 == 0) goto L_0x04bb
            r0 = r88
            long r12 = r0.timescale
            r0 = r88
            long r14 = r0.movieTimescale
            long r12 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r10, r12, r14)
            long r44 = r52 + r12
            r4 = 1
            r12 = 1
            r0 = r52
            int r75 = com.google.android.exoplayer2.util.Util.binarySearchCeil((long[]) r8, (long) r0, (boolean) r4, (boolean) r12)
            r4 = 0
            r0 = r44
            r2 = r58
            int r42 = com.google.android.exoplayer2.util.Util.binarySearchCeil((long[]) r8, (long) r0, (boolean) r2, (boolean) r4)
            if (r24 == 0) goto L_0x046d
            int r25 = r42 - r75
            r0 = r75
            r1 = r34
            r2 = r73
            r3 = r25
            java.lang.System.arraycopy(r5, r0, r1, r2, r3)
            r0 = r75
            r1 = r36
            r2 = r73
            r3 = r25
            java.lang.System.arraycopy(r6, r0, r1, r2, r3)
            r0 = r75
            r1 = r32
            r2 = r73
            r3 = r25
            java.lang.System.arraycopy(r9, r0, r1, r2, r3)
        L_0x046d:
            r49 = r75
        L_0x046f:
            r0 = r49
            r1 = r42
            if (r0 >= r1) goto L_0x04bb
            r14 = 1000000(0xf4240, double:4.940656E-318)
            r0 = r88
            long r0 = r0.movieTimescale
            r16 = r0
            r12 = r62
            long r64 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r12, r14, r16)
            r12 = r8[r49]
            long r12 = r12 - r52
            r14 = 1000000(0xf4240, double:4.940656E-318)
            r0 = r88
            long r0 = r0.timescale
            r16 = r0
            long r82 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r12, r14, r16)
            long r12 = r64 + r82
            r37[r73] = r12
            if (r24 == 0) goto L_0x04a3
            r4 = r36[r73]
            r0 = r33
            if (r4 <= r0) goto L_0x04a3
            r33 = r6[r49]
        L_0x04a3:
            int r73 = r73 + 1
            int r49 = r49 + 1
            goto L_0x046f
        L_0x04a8:
            r4 = 0
            goto L_0x03e4
        L_0x04ab:
            r34 = r5
            goto L_0x03ee
        L_0x04af:
            r36 = r6
            goto L_0x03f6
        L_0x04b3:
            r33 = r7
            goto L_0x03fa
        L_0x04b7:
            r32 = r9
            goto L_0x0402
        L_0x04bb:
            long r62 = r62 + r10
            int r47 = r47 + 1
            goto L_0x040e
        L_0x04c1:
            r46 = 0
            r47 = 0
        L_0x04c5:
            r0 = r32
            int r4 = r0.length
            r0 = r47
            if (r0 >= r4) goto L_0x04dc
            if (r46 != 0) goto L_0x04dc
            r4 = r32[r47]
            r4 = r4 & 1
            if (r4 == 0) goto L_0x04da
            r4 = 1
        L_0x04d5:
            r46 = r46 | r4
            int r47 = r47 + 1
            goto L_0x04c5
        L_0x04da:
            r4 = 0
            goto L_0x04d5
        L_0x04dc:
            if (r46 != 0) goto L_0x04f8
            java.lang.String r4 = "AtomParsers"
            java.lang.String r12 = "Ignoring edit list: Edited sample sequence does not contain a sync sample."
            android.util.Log.w(r4, r12)
            r12 = 1000000(0xf4240, double:4.940656E-318)
            r0 = r88
            long r14 = r0.timescale
            com.google.android.exoplayer2.util.Util.scaleLargeTimestampsInPlace(r8, r12, r14)
            com.google.android.exoplayer2.extractor.mp4.TrackSampleTable r4 = new com.google.android.exoplayer2.extractor.mp4.TrackSampleTable
            r4.<init>(r5, r6, r7, r8, r9)
            goto L_0x002b
        L_0x04f8:
            com.google.android.exoplayer2.extractor.mp4.TrackSampleTable r12 = new com.google.android.exoplayer2.extractor.mp4.TrackSampleTable
            r13 = r34
            r14 = r36
            r15 = r33
            r16 = r37
            r17 = r32
            r12.<init>(r13, r14, r15, r16, r17)
            r4 = r12
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.AtomParsers.parseStbl(com.google.android.exoplayer2.extractor.mp4.Track, com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom, com.google.android.exoplayer2.extractor.GaplessInfoHolder):com.google.android.exoplayer2.extractor.mp4.TrackSampleTable");
    }

    private static StsdData parseStsd(ParsableByteArray parsableByteArray, int i, int i2, String str, DrmInitData drmInitData, boolean z) throws ParserException {
        parsableByteArray.setPosition(12);
        int readInt = parsableByteArray.readInt();
        StsdData stsdData = new StsdData(readInt);
        for (int i3 = 0; i3 < readInt; i3++) {
            int position = parsableByteArray.getPosition();
            int readInt2 = parsableByteArray.readInt();
            Assertions.checkArgument(readInt2 > 0, "childAtomSize should be positive");
            int readInt3 = parsableByteArray.readInt();
            if (readInt3 == Atom.TYPE_avc1 || readInt3 == Atom.TYPE_avc3 || readInt3 == Atom.TYPE_encv || readInt3 == Atom.TYPE_mp4v || readInt3 == Atom.TYPE_hvc1 || readInt3 == Atom.TYPE_hev1 || readInt3 == Atom.TYPE_s263 || readInt3 == Atom.TYPE_vp08 || readInt3 == Atom.TYPE_vp09) {
                parseVideoSampleEntry(parsableByteArray, readInt3, position, readInt2, i, i2, drmInitData, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_mp4a || readInt3 == Atom.TYPE_enca || readInt3 == Atom.TYPE_ac_3 || readInt3 == Atom.TYPE_ec_3 || readInt3 == Atom.TYPE_dtsc || readInt3 == Atom.TYPE_dtse || readInt3 == Atom.TYPE_dtsh || readInt3 == Atom.TYPE_dtsl || readInt3 == Atom.TYPE_samr || readInt3 == Atom.TYPE_sawb || readInt3 == Atom.TYPE_lpcm || readInt3 == Atom.TYPE_sowt || readInt3 == Atom.TYPE__mp3 || readInt3 == Atom.TYPE_alac) {
                parseAudioSampleEntry(parsableByteArray, readInt3, position, readInt2, i, str, z, drmInitData, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_TTML || readInt3 == Atom.TYPE_tx3g || readInt3 == Atom.TYPE_wvtt || readInt3 == Atom.TYPE_stpp || readInt3 == Atom.TYPE_c608) {
                parseTextSampleEntry(parsableByteArray, readInt3, position, readInt2, i, str, stsdData);
            } else if (readInt3 == Atom.TYPE_camm) {
                stsdData.format = Format.createSampleFormat(Integer.toString(i), MimeTypes.APPLICATION_CAMERA_MOTION, (String) null, -1, (DrmInitData) null);
            }
            parsableByteArray.setPosition(position + readInt2);
        }
        return stsdData;
    }

    private static void parseTextSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, String str, StsdData stsdData) throws ParserException {
        String str2;
        parsableByteArray.setPosition(i2 + 8 + 8);
        List list = null;
        long j = Long.MAX_VALUE;
        if (i == Atom.TYPE_TTML) {
            str2 = MimeTypes.APPLICATION_TTML;
        } else if (i == Atom.TYPE_tx3g) {
            str2 = MimeTypes.APPLICATION_TX3G;
            int i5 = (i3 - 8) - 8;
            byte[] bArr = new byte[i5];
            parsableByteArray.readBytes(bArr, 0, i5);
            list = Collections.singletonList(bArr);
        } else if (i == Atom.TYPE_wvtt) {
            str2 = MimeTypes.APPLICATION_MP4VTT;
        } else if (i == Atom.TYPE_stpp) {
            str2 = MimeTypes.APPLICATION_TTML;
            j = 0;
        } else if (i == Atom.TYPE_c608) {
            str2 = MimeTypes.APPLICATION_MP4CEA608;
            stsdData.requiredSampleTransformation = 1;
        } else {
            throw new IllegalStateException();
        }
        stsdData.format = Format.createTextSampleFormat(Integer.toString(i4), str2, (String) null, -1, 0, str, -1, (DrmInitData) null, j, list);
    }

    private static TkhdData parseTkhd(ParsableByteArray parsableByteArray) {
        long readUnsignedInt;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        int readInt = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        boolean z = true;
        int position = parsableByteArray.getPosition();
        int i = parseFullAtomVersion == 0 ? 4 : 8;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                break;
            } else if (parsableByteArray.data[position + i2] != -1) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            parsableByteArray.skipBytes(i);
            readUnsignedInt = C.TIME_UNSET;
        } else {
            readUnsignedInt = parseFullAtomVersion == 0 ? parsableByteArray.readUnsignedInt() : parsableByteArray.readUnsignedLongToLong();
            if (readUnsignedInt == 0) {
                readUnsignedInt = C.TIME_UNSET;
            }
        }
        parsableByteArray.skipBytes(16);
        int readInt2 = parsableByteArray.readInt();
        int readInt3 = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        int readInt4 = parsableByteArray.readInt();
        int readInt5 = parsableByteArray.readInt();
        return new TkhdData(readInt, readUnsignedInt, (readInt2 == 0 && readInt3 == 65536 && readInt4 == (-65536) && readInt5 == 0) ? 90 : (readInt2 == 0 && readInt3 == (-65536) && readInt4 == 65536 && readInt5 == 0) ? 270 : (readInt2 == (-65536) && readInt3 == 0 && readInt4 == 0 && readInt5 == (-65536)) ? 180 : 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v21, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v2, resolved type: long[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v2, resolved type: long[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.exoplayer2.extractor.mp4.Track parseTrak(com.google.android.exoplayer2.extractor.mp4.Atom.ContainerAtom r32, com.google.android.exoplayer2.extractor.mp4.Atom.LeafAtom r33, long r34, com.google.android.exoplayer2.drm.DrmInitData r36, boolean r37, boolean r38) throws com.google.android.exoplayer2.ParserException {
        /*
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_mdia
            r0 = r32
            com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom r26 = r0.getContainerAtomOfType(r2)
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_hdlr
            r0 = r26
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r2 = r0.getLeafAtomOfType(r2)
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r2.data
            int r30 = parseHdlr(r2)
            r2 = -1
            r0 = r30
            if (r0 != r2) goto L_0x001d
            r9 = 0
        L_0x001c:
            return r9
        L_0x001d:
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_tkhd
            r0 = r32
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r2 = r0.getLeafAtomOfType(r2)
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r2.data
            com.google.android.exoplayer2.extractor.mp4.AtomParsers$TkhdData r29 = parseTkhd(r2)
            r2 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r2 = (r34 > r2 ? 1 : (r34 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x0038
            long r34 = r29.duration
        L_0x0038:
            r0 = r33
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r0.data
            long r6 = parseMvhd(r2)
            r2 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r2 = (r34 > r2 ? 1 : (r34 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x00b5
            r16 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x004e:
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_minf
            r0 = r26
            com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom r2 = r0.getContainerAtomOfType(r2)
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stbl
            com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom r27 = r2.getContainerAtomOfType(r3)
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_mdhd
            r0 = r26
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r2 = r0.getLeafAtomOfType(r2)
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r2.data
            android.util.Pair r25 = parseMdhd(r2)
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_stsd
            r0 = r27
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r2 = r0.getLeafAtomOfType(r2)
            com.google.android.exoplayer2.util.ParsableByteArray r8 = r2.data
            int r9 = r29.id
            int r10 = r29.rotationDegrees
            r0 = r25
            java.lang.Object r11 = r0.second
            java.lang.String r11 = (java.lang.String) r11
            r12 = r36
            r13 = r38
            com.google.android.exoplayer2.extractor.mp4.AtomParsers$StsdData r28 = parseStsd(r8, r9, r10, r11, r12, r13)
            r22 = 0
            r23 = 0
            if (r37 != 0) goto L_0x00ac
            int r2 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_edts
            r0 = r32
            com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom r2 = r0.getContainerAtomOfType(r2)
            android.util.Pair r24 = parseEdts(r2)
            r0 = r24
            java.lang.Object r0 = r0.first
            r22 = r0
            long[] r22 = (long[]) r22
            r0 = r24
            java.lang.Object r0 = r0.second
            r23 = r0
            long[] r23 = (long[]) r23
        L_0x00ac:
            r0 = r28
            com.google.android.exoplayer2.Format r2 = r0.format
            if (r2 != 0) goto L_0x00bf
            r9 = 0
            goto L_0x001c
        L_0x00b5:
            r4 = 1000000(0xf4240, double:4.940656E-318)
            r2 = r34
            long r16 = com.google.android.exoplayer2.util.Util.scaleLargeTimestamp(r2, r4, r6)
            goto L_0x004e
        L_0x00bf:
            com.google.android.exoplayer2.extractor.mp4.Track r9 = new com.google.android.exoplayer2.extractor.mp4.Track
            int r10 = r29.id
            r0 = r25
            java.lang.Object r2 = r0.first
            java.lang.Long r2 = (java.lang.Long) r2
            long r12 = r2.longValue()
            r0 = r28
            com.google.android.exoplayer2.Format r0 = r0.format
            r18 = r0
            r0 = r28
            int r0 = r0.requiredSampleTransformation
            r19 = r0
            r0 = r28
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox[] r0 = r0.trackEncryptionBoxes
            r20 = r0
            r0 = r28
            int r0 = r0.nalUnitLengthFieldLength
            r21 = r0
            r11 = r30
            r14 = r6
            r9.<init>(r10, r11, r12, r14, r16, r18, r19, r20, r21, r22, r23)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.AtomParsers.parseTrak(com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom, com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom, long, com.google.android.exoplayer2.drm.DrmInitData, boolean, boolean):com.google.android.exoplayer2.extractor.mp4.Track");
    }

    public static Metadata parseUdta(Atom.LeafAtom leafAtom, boolean z) {
        if (z) {
            return null;
        }
        ParsableByteArray parsableByteArray = leafAtom.data;
        parsableByteArray.setPosition(8);
        while (parsableByteArray.bytesLeft() >= 8) {
            int position = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_meta) {
                parsableByteArray.setPosition(position);
                return parseMetaAtom(parsableByteArray, position + readInt);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:73:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void parseVideoSampleEntry(com.google.android.exoplayer2.util.ParsableByteArray r29, int r30, int r31, int r32, int r33, int r34, com.google.android.exoplayer2.drm.DrmInitData r35, com.google.android.exoplayer2.extractor.mp4.AtomParsers.StsdData r36, int r37) throws com.google.android.exoplayer2.ParserException {
        /*
            int r3 = r31 + 8
            int r3 = r3 + 8
            r0 = r29
            r0.setPosition(r3)
            r3 = 16
            r0 = r29
            r0.skipBytes(r3)
            int r8 = r29.readUnsignedShort()
            int r9 = r29.readUnsignedShort()
            r26 = 0
            r13 = 1065353216(0x3f800000, float:1.0)
            r3 = 50
            r0 = r29
            r0.skipBytes(r3)
            int r21 = r29.getPosition()
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_encv
            r0 = r30
            if (r0 != r3) goto L_0x005a
            r0 = r29
            r1 = r31
            r2 = r32
            android.util.Pair r27 = parseSampleEntryEncryptionData(r0, r1, r2)
            if (r27 == 0) goto L_0x0053
            r0 = r27
            java.lang.Object r3 = r0.first
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r30 = r3.intValue()
            if (r35 != 0) goto L_0x0082
            r35 = 0
        L_0x0047:
            r0 = r36
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox[] r5 = r0.trackEncryptionBoxes
            r0 = r27
            java.lang.Object r3 = r0.second
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox r3 = (com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox) r3
            r5[r37] = r3
        L_0x0053:
            r0 = r29
            r1 = r21
            r0.setPosition(r1)
        L_0x005a:
            r11 = 0
            r4 = 0
            r14 = 0
            r15 = -1
        L_0x005e:
            int r3 = r21 - r31
            r0 = r32
            if (r3 >= r0) goto L_0x007f
            r0 = r29
            r1 = r21
            r0.setPosition(r1)
            int r22 = r29.getPosition()
            int r19 = r29.readInt()
            if (r19 != 0) goto L_0x0091
            int r3 = r29.getPosition()
            int r3 = r3 - r31
            r0 = r32
            if (r3 != r0) goto L_0x0091
        L_0x007f:
            if (r4 != 0) goto L_0x0198
        L_0x0081:
            return
        L_0x0082:
            r0 = r27
            java.lang.Object r3 = r0.second
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox r3 = (com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox) r3
            java.lang.String r3 = r3.schemeType
            r0 = r35
            com.google.android.exoplayer2.drm.DrmInitData r35 = r0.copyWithSchemeType(r3)
            goto L_0x0047
        L_0x0091:
            if (r19 <= 0) goto L_0x00cd
            r3 = 1
        L_0x0094:
            java.lang.String r5 = "childAtomSize should be positive"
            com.google.android.exoplayer2.util.Assertions.checkArgument(r3, r5)
            int r20 = r29.readInt()
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_avcC
            r0 = r20
            if (r0 != r3) goto L_0x00d1
            if (r4 != 0) goto L_0x00cf
            r3 = 1
        L_0x00a7:
            com.google.android.exoplayer2.util.Assertions.checkState(r3)
            java.lang.String r4 = "video/avc"
            int r3 = r22 + 8
            r0 = r29
            r0.setPosition(r3)
            com.google.android.exoplayer2.video.AvcConfig r18 = com.google.android.exoplayer2.video.AvcConfig.parse(r29)
            r0 = r18
            java.util.List<byte[]> r11 = r0.initializationData
            r0 = r18
            int r3 = r0.nalUnitLengthFieldLength
            r0 = r36
            r0.nalUnitLengthFieldLength = r3
            if (r26 != 0) goto L_0x00ca
            r0 = r18
            float r13 = r0.pixelWidthAspectRatio
        L_0x00ca:
            int r21 = r21 + r19
            goto L_0x005e
        L_0x00cd:
            r3 = 0
            goto L_0x0094
        L_0x00cf:
            r3 = 0
            goto L_0x00a7
        L_0x00d1:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_hvcC
            r0 = r20
            if (r0 != r3) goto L_0x00fa
            if (r4 != 0) goto L_0x00f8
            r3 = 1
        L_0x00da:
            com.google.android.exoplayer2.util.Assertions.checkState(r3)
            java.lang.String r4 = "video/hevc"
            int r3 = r22 + 8
            r0 = r29
            r0.setPosition(r3)
            com.google.android.exoplayer2.video.HevcConfig r23 = com.google.android.exoplayer2.video.HevcConfig.parse(r29)
            r0 = r23
            java.util.List<byte[]> r11 = r0.initializationData
            r0 = r23
            int r3 = r0.nalUnitLengthFieldLength
            r0 = r36
            r0.nalUnitLengthFieldLength = r3
            goto L_0x00ca
        L_0x00f8:
            r3 = 0
            goto L_0x00da
        L_0x00fa:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_vpcC
            r0 = r20
            if (r0 != r3) goto L_0x0116
            if (r4 != 0) goto L_0x0110
            r3 = 1
        L_0x0103:
            com.google.android.exoplayer2.util.Assertions.checkState(r3)
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_vp08
            r0 = r30
            if (r0 != r3) goto L_0x0112
            java.lang.String r4 = "video/x-vnd.on2.vp8"
        L_0x010f:
            goto L_0x00ca
        L_0x0110:
            r3 = 0
            goto L_0x0103
        L_0x0112:
            java.lang.String r4 = "video/x-vnd.on2.vp9"
            goto L_0x010f
        L_0x0116:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_d263
            r0 = r20
            if (r0 != r3) goto L_0x0128
            if (r4 != 0) goto L_0x0126
            r3 = 1
        L_0x011f:
            com.google.android.exoplayer2.util.Assertions.checkState(r3)
            java.lang.String r4 = "video/3gpp"
            goto L_0x00ca
        L_0x0126:
            r3 = 0
            goto L_0x011f
        L_0x0128:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_esds
            r0 = r20
            if (r0 != r3) goto L_0x014d
            if (r4 != 0) goto L_0x014b
            r3 = 1
        L_0x0131:
            com.google.android.exoplayer2.util.Assertions.checkState(r3)
            r0 = r29
            r1 = r22
            android.util.Pair r25 = parseEsdsFromParent(r0, r1)
            r0 = r25
            java.lang.Object r4 = r0.first
            java.lang.String r4 = (java.lang.String) r4
            r0 = r25
            java.lang.Object r3 = r0.second
            java.util.List r11 = java.util.Collections.singletonList(r3)
            goto L_0x00ca
        L_0x014b:
            r3 = 0
            goto L_0x0131
        L_0x014d:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_pasp
            r0 = r20
            if (r0 != r3) goto L_0x015f
            r0 = r29
            r1 = r22
            float r13 = parsePaspFromParent(r0, r1)
            r26 = 1
            goto L_0x00ca
        L_0x015f:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_sv3d
            r0 = r20
            if (r0 != r3) goto L_0x0171
            r0 = r29
            r1 = r22
            r2 = r19
            byte[] r14 = parseProjFromParent(r0, r1, r2)
            goto L_0x00ca
        L_0x0171:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.TYPE_st3d
            r0 = r20
            if (r0 != r3) goto L_0x00ca
            int r28 = r29.readUnsignedByte()
            r3 = 3
            r0 = r29
            r0.skipBytes(r3)
            if (r28 != 0) goto L_0x00ca
            int r24 = r29.readUnsignedByte()
            switch(r24) {
                case 0: goto L_0x018c;
                case 1: goto L_0x018f;
                case 2: goto L_0x0192;
                case 3: goto L_0x0195;
                default: goto L_0x018a;
            }
        L_0x018a:
            goto L_0x00ca
        L_0x018c:
            r15 = 0
            goto L_0x00ca
        L_0x018f:
            r15 = 1
            goto L_0x00ca
        L_0x0192:
            r15 = 2
            goto L_0x00ca
        L_0x0195:
            r15 = 3
            goto L_0x00ca
        L_0x0198:
            java.lang.String r3 = java.lang.Integer.toString(r33)
            r5 = 0
            r6 = -1
            r7 = -1
            r10 = -1082130432(0xffffffffbf800000, float:-1.0)
            r16 = 0
            r12 = r34
            r17 = r35
            com.google.android.exoplayer2.Format r3 = com.google.android.exoplayer2.Format.createVideoSampleFormat(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r0 = r36
            r0.format = r3
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.AtomParsers.parseVideoSampleEntry(com.google.android.exoplayer2.util.ParsableByteArray, int, int, int, int, int, com.google.android.exoplayer2.drm.DrmInitData, com.google.android.exoplayer2.extractor.mp4.AtomParsers$StsdData, int):void");
    }
}
