package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.util.Util;

final class FixedSampleSizeRechunker {
    private static final int MAX_SAMPLE_SIZE = 8192;

    public static final class Results {
        public final int[] flags;
        public final int maximumSize;
        public final long[] offsets;
        public final int[] sizes;
        public final long[] timestamps;

        private Results(long[] jArr, int[] iArr, int i, long[] jArr2, int[] iArr2) {
            this.offsets = jArr;
            this.sizes = iArr;
            this.maximumSize = i;
            this.timestamps = jArr2;
            this.flags = iArr2;
        }
    }

    FixedSampleSizeRechunker() {
    }

    public static Results rechunk(int i, long[] jArr, int[] iArr, long j) {
        int i2 = 8192 / i;
        int i3 = 0;
        for (int ceilDivide : iArr) {
            i3 += Util.ceilDivide(ceilDivide, i2);
        }
        long[] jArr2 = new long[i3];
        int[] iArr2 = new int[i3];
        int i4 = 0;
        long[] jArr3 = new long[i3];
        int[] iArr3 = new int[i3];
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 0; i7 < iArr.length; i7++) {
            int i8 = iArr[i7];
            long j2 = jArr[i7];
            while (i8 > 0) {
                int min = Math.min(i2, i8);
                jArr2[i6] = j2;
                iArr2[i6] = i * min;
                i4 = Math.max(i4, iArr2[i6]);
                jArr3[i6] = ((long) i5) * j;
                iArr3[i6] = 1;
                j2 += (long) iArr2[i6];
                i5 += min;
                i8 -= min;
                i6++;
            }
        }
        return new Results(jArr2, iArr2, i4, jArr3, iArr3);
    }
}
