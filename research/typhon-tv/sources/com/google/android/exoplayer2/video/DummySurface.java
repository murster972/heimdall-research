package com.google.android.exoplayer2.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.Surface;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;

@TargetApi(17)
public final class DummySurface extends Surface {
    private static final int EGL_PROTECTED_CONTENT_EXT = 12992;
    private static final String TAG = "DummySurface";
    private static boolean secureSupported;
    private static boolean secureSupportedInitialized;
    public final boolean secure;
    private final DummySurfaceThread thread;
    private boolean threadReleased;

    private static class DummySurfaceThread extends HandlerThread implements SurfaceTexture.OnFrameAvailableListener, Handler.Callback {
        private static final int MSG_INIT = 1;
        private static final int MSG_RELEASE = 3;
        private static final int MSG_UPDATE_TEXTURE = 2;
        private EGLContext context;
        private EGLDisplay display;
        private Handler handler;
        private Error initError;
        private RuntimeException initException;
        private EGLSurface pbuffer;
        private DummySurface surface;
        private SurfaceTexture surfaceTexture;
        private final int[] textureIdHolder = new int[1];

        public DummySurfaceThread() {
            super("dummySurface");
        }

        private void initInternal(boolean z) {
            this.display = EGL14.eglGetDisplay(0);
            Assertions.checkState(this.display != null, "eglGetDisplay failed");
            int[] iArr = new int[2];
            Assertions.checkState(EGL14.eglInitialize(this.display, iArr, 0, iArr, 1), "eglInitialize failed");
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            int[] iArr2 = new int[1];
            Assertions.checkState(EGL14.eglChooseConfig(this.display, new int[]{12352, 4, 12324, 8, 12323, 8, 12322, 8, 12321, 8, 12325, 0, 12327, 12344, 12339, 4, 12344}, 0, eGLConfigArr, 0, 1, iArr2, 0) && iArr2[0] > 0 && eGLConfigArr[0] != null, "eglChooseConfig failed");
            EGLConfig eGLConfig = eGLConfigArr[0];
            this.context = EGL14.eglCreateContext(this.display, eGLConfig, EGL14.EGL_NO_CONTEXT, z ? new int[]{12440, 2, DummySurface.EGL_PROTECTED_CONTENT_EXT, 1, 12344} : new int[]{12440, 2, 12344}, 0);
            Assertions.checkState(this.context != null, "eglCreateContext failed");
            this.pbuffer = EGL14.eglCreatePbufferSurface(this.display, eGLConfig, z ? new int[]{12375, 1, 12374, 1, DummySurface.EGL_PROTECTED_CONTENT_EXT, 1, 12344} : new int[]{12375, 1, 12374, 1, 12344}, 0);
            Assertions.checkState(this.pbuffer != null, "eglCreatePbufferSurface failed");
            Assertions.checkState(EGL14.eglMakeCurrent(this.display, this.pbuffer, this.pbuffer, this.context), "eglMakeCurrent failed");
            GLES20.glGenTextures(1, this.textureIdHolder, 0);
            this.surfaceTexture = new SurfaceTexture(this.textureIdHolder[0]);
            this.surfaceTexture.setOnFrameAvailableListener(this);
            this.surface = new DummySurface(this, this.surfaceTexture, z);
        }

        private void releaseInternal() {
            try {
                if (this.surfaceTexture != null) {
                    this.surfaceTexture.release();
                    GLES20.glDeleteTextures(1, this.textureIdHolder, 0);
                }
            } finally {
                if (this.pbuffer != null) {
                    EGL14.eglDestroySurface(this.display, this.pbuffer);
                }
                if (this.context != null) {
                    EGL14.eglDestroyContext(this.display, this.context);
                }
                this.pbuffer = null;
                this.context = null;
                this.display = null;
                this.surface = null;
                this.surfaceTexture = null;
            }
        }

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    try {
                        initInternal(message.arg1 != 0);
                        synchronized (this) {
                            notify();
                        }
                        break;
                    } catch (RuntimeException e) {
                        Log.e(DummySurface.TAG, "Failed to initialize dummy surface", e);
                        this.initException = e;
                        synchronized (this) {
                            notify();
                            break;
                        }
                    } catch (Error e2) {
                        Log.e(DummySurface.TAG, "Failed to initialize dummy surface", e2);
                        this.initError = e2;
                        synchronized (this) {
                            notify();
                            break;
                        }
                    } catch (Throwable th) {
                        synchronized (this) {
                            notify();
                            throw th;
                        }
                    }
                case 2:
                    this.surfaceTexture.updateTexImage();
                    break;
                case 3:
                    try {
                        releaseInternal();
                        break;
                    } catch (Throwable th2) {
                        Log.e(DummySurface.TAG, "Failed to release dummy surface", th2);
                        break;
                    } finally {
                        quit();
                    }
            }
            return true;
        }

        public DummySurface init(boolean z) {
            int i = 1;
            start();
            this.handler = new Handler(getLooper(), this);
            boolean z2 = false;
            synchronized (this) {
                Handler handler2 = this.handler;
                if (!z) {
                    i = 0;
                }
                handler2.obtainMessage(1, i, 0).sendToTarget();
                while (this.surface == null && this.initException == null && this.initError == null) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        z2 = true;
                    }
                }
            }
            if (z2) {
                Thread.currentThread().interrupt();
            }
            if (this.initException != null) {
                throw this.initException;
            } else if (this.initError == null) {
                return this.surface;
            } else {
                throw this.initError;
            }
        }

        public void onFrameAvailable(SurfaceTexture surfaceTexture2) {
            this.handler.sendEmptyMessage(2);
        }

        public void release() {
            this.handler.sendEmptyMessage(3);
        }
    }

    private DummySurface(DummySurfaceThread dummySurfaceThread, SurfaceTexture surfaceTexture, boolean z) {
        super(surfaceTexture);
        this.thread = dummySurfaceThread;
        this.secure = z;
    }

    private static void assertApiLevel17OrHigher() {
        if (Util.SDK_INT < 17) {
            throw new UnsupportedOperationException("Unsupported prior to API level 17");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
        r1 = android.opengl.EGL14.eglQueryString(android.opengl.EGL14.eglGetDisplay(0), 12373);
     */
    @android.annotation.TargetApi(24)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean enableSecureDummySurfaceV24(android.content.Context r6) {
        /*
            r5 = 26
            r2 = 0
            int r3 = com.google.android.exoplayer2.util.Util.SDK_INT
            if (r3 >= r5) goto L_0x0013
            java.lang.String r3 = "samsung"
            java.lang.String r4 = com.google.android.exoplayer2.util.Util.MANUFACTURER
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0013
        L_0x0012:
            return r2
        L_0x0013:
            int r3 = com.google.android.exoplayer2.util.Util.SDK_INT
            if (r3 >= r5) goto L_0x0024
            android.content.pm.PackageManager r3 = r6.getPackageManager()
            java.lang.String r4 = "android.hardware.vr.high_performance"
            boolean r3 = r3.hasSystemFeature(r4)
            if (r3 == 0) goto L_0x0012
        L_0x0024:
            android.opengl.EGLDisplay r0 = android.opengl.EGL14.eglGetDisplay(r2)
            r3 = 12373(0x3055, float:1.7338E-41)
            java.lang.String r1 = android.opengl.EGL14.eglQueryString(r0, r3)
            if (r1 == 0) goto L_0x0012
            java.lang.String r3 = "EGL_EXT_protected_content"
            boolean r3 = r1.contains(r3)
            if (r3 == 0) goto L_0x0012
            r2 = 1
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.video.DummySurface.enableSecureDummySurfaceV24(android.content.Context):boolean");
    }

    public static synchronized boolean isSecureSupported(Context context) {
        boolean z;
        boolean z2 = true;
        synchronized (DummySurface.class) {
            if (!secureSupportedInitialized) {
                if (Util.SDK_INT < 24 || !enableSecureDummySurfaceV24(context)) {
                    z2 = false;
                }
                secureSupported = z2;
                secureSupportedInitialized = true;
            }
            z = secureSupported;
        }
        return z;
    }

    public static DummySurface newInstanceV17(Context context, boolean z) {
        assertApiLevel17OrHigher();
        Assertions.checkState(!z || isSecureSupported(context));
        return new DummySurfaceThread().init(z);
    }

    public void release() {
        super.release();
        synchronized (this.thread) {
            if (!this.threadReleased) {
                this.thread.release();
                this.threadReleased = true;
            }
        }
    }
}
