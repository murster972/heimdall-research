package com.google.android.exoplayer2.ext.vp9;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.atomic.AtomicReference;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

class VpxRenderer implements GLSurfaceView.Renderer {
    private static final String FRAGMENT_SHADER = "precision mediump float;\nvarying vec2 interp_tc;\nuniform sampler2D y_tex;\nuniform sampler2D u_tex;\nuniform sampler2D v_tex;\nuniform mat3 mColorConversion;\nvoid main() {\n  vec3 yuv;\n  yuv.x = texture2D(y_tex, interp_tc).r - 0.0625;\n  yuv.y = texture2D(u_tex, interp_tc).r - 0.5;\n  yuv.z = texture2D(v_tex, interp_tc).r - 0.5;\n  gl_FragColor = vec4(mColorConversion * yuv, 1.0);\n}\n";
    private static final String[] TEXTURE_UNIFORMS = {"y_tex", "u_tex", "v_tex"};
    private static final FloatBuffer TEXTURE_VERTICES = nativeFloatBuffer(-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f);
    private static final String VERTEX_SHADER = "varying vec2 interp_tc;\nattribute vec4 in_pos;\nattribute vec2 in_tc;\nvoid main() {\n  gl_Position = in_pos;\n  interp_tc = in_tc;\n}\n";
    private static final float[] kColorConversion2020 = {1.168f, 1.168f, 1.168f, 0.0f, -0.188f, 2.148f, 1.683f, -0.652f, 0.0f};
    private static final float[] kColorConversion601 = {1.164f, 1.164f, 1.164f, 0.0f, -0.392f, 2.017f, 1.596f, -0.813f, 0.0f};
    private static final float[] kColorConversion709 = {1.164f, 1.164f, 1.164f, 0.0f, -0.213f, 2.112f, 1.793f, -0.533f, 0.0f};
    private int colorMatrixLocation;
    private final AtomicReference<VpxOutputBuffer> pendingOutputBufferReference = new AtomicReference<>();
    private int previousStride = -1;
    private int previousWidth = -1;
    private int program;
    private VpxOutputBuffer renderedOutputBuffer;
    private int texLocation;
    private FloatBuffer textureCoords;
    private final int[] yuvTextures = new int[3];

    private void abortUnless(boolean z, String str) {
        if (!z) {
            throw new RuntimeException(str);
        }
    }

    private void addShader(int i, String str, int i2) {
        boolean z = true;
        int[] iArr = {0};
        int glCreateShader = GLES20.glCreateShader(i);
        GLES20.glShaderSource(glCreateShader, str);
        GLES20.glCompileShader(glCreateShader);
        GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
        if (iArr[0] != 1) {
            z = false;
        }
        abortUnless(z, GLES20.glGetShaderInfoLog(glCreateShader) + ", source: " + str);
        GLES20.glAttachShader(i2, glCreateShader);
        GLES20.glDeleteShader(glCreateShader);
        checkNoGLES2Error();
    }

    private void checkNoGLES2Error() {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            throw new RuntimeException("GLES20 error: " + glGetError);
        }
    }

    private static FloatBuffer nativeFloatBuffer(float... fArr) {
        FloatBuffer asFloatBuffer = ByteBuffer.allocateDirect(fArr.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        asFloatBuffer.put(fArr);
        asFloatBuffer.flip();
        return asFloatBuffer;
    }

    private void setupTextures() {
        GLES20.glGenTextures(3, this.yuvTextures, 0);
        for (int i = 0; i < 3; i++) {
            GLES20.glUniform1i(GLES20.glGetUniformLocation(this.program, TEXTURE_UNIFORMS[i]), i);
            GLES20.glActiveTexture(33984 + i);
            GLES20.glBindTexture(3553, this.yuvTextures[i]);
            GLES20.glTexParameterf(3553, 10241, 9729.0f);
            GLES20.glTexParameterf(3553, 10240, 9729.0f);
            GLES20.glTexParameterf(3553, 10242, 33071.0f);
            GLES20.glTexParameterf(3553, 10243, 33071.0f);
        }
        checkNoGLES2Error();
    }

    public void onDrawFrame(GL10 gl10) {
        VpxOutputBuffer andSet = this.pendingOutputBufferReference.getAndSet((Object) null);
        if (andSet != null || this.renderedOutputBuffer != null) {
            if (andSet != null) {
                if (this.renderedOutputBuffer != null) {
                    this.renderedOutputBuffer.release();
                }
                this.renderedOutputBuffer = andSet;
            }
            VpxOutputBuffer vpxOutputBuffer = this.renderedOutputBuffer;
            float[] fArr = kColorConversion709;
            switch (vpxOutputBuffer.colorspace) {
                case 1:
                    fArr = kColorConversion601;
                    break;
                case 3:
                    fArr = kColorConversion2020;
                    break;
            }
            GLES20.glUniformMatrix3fv(this.colorMatrixLocation, 1, false, fArr, 0);
            int i = 0;
            while (i < 3) {
                int i2 = i == 0 ? vpxOutputBuffer.height : (vpxOutputBuffer.height + 1) / 2;
                GLES20.glActiveTexture(33984 + i);
                GLES20.glBindTexture(3553, this.yuvTextures[i]);
                GLES20.glPixelStorei(3317, 1);
                GLES20.glTexImage2D(3553, 0, 6409, vpxOutputBuffer.yuvStrides[i], i2, 0, 6409, 5121, vpxOutputBuffer.yuvPlanes[i]);
                i++;
            }
            if (!(this.previousWidth == vpxOutputBuffer.width && this.previousStride == vpxOutputBuffer.yuvStrides[0])) {
                float f = ((float) vpxOutputBuffer.width) / ((float) vpxOutputBuffer.yuvStrides[0]);
                this.textureCoords = nativeFloatBuffer(0.0f, 0.0f, 0.0f, 1.0f, f, 0.0f, f, 1.0f);
                GLES20.glVertexAttribPointer(this.texLocation, 2, 5126, false, 0, this.textureCoords);
                this.previousWidth = vpxOutputBuffer.width;
                this.previousStride = vpxOutputBuffer.yuvStrides[0];
            }
            GLES20.glClear(16384);
            GLES20.glDrawArrays(5, 0, 4);
            checkNoGLES2Error();
        }
    }

    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
        GLES20.glViewport(0, 0, i, i2);
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        boolean z = true;
        this.program = GLES20.glCreateProgram();
        addShader(35633, VERTEX_SHADER, this.program);
        addShader(35632, FRAGMENT_SHADER, this.program);
        GLES20.glLinkProgram(this.program);
        int[] iArr = {0};
        iArr[0] = 0;
        GLES20.glGetProgramiv(this.program, 35714, iArr, 0);
        if (iArr[0] != 1) {
            z = false;
        }
        abortUnless(z, GLES20.glGetProgramInfoLog(this.program));
        GLES20.glUseProgram(this.program);
        int glGetAttribLocation = GLES20.glGetAttribLocation(this.program, "in_pos");
        GLES20.glEnableVertexAttribArray(glGetAttribLocation);
        GLES20.glVertexAttribPointer(glGetAttribLocation, 2, 5126, false, 0, TEXTURE_VERTICES);
        this.texLocation = GLES20.glGetAttribLocation(this.program, "in_tc");
        GLES20.glEnableVertexAttribArray(this.texLocation);
        checkNoGLES2Error();
        this.colorMatrixLocation = GLES20.glGetUniformLocation(this.program, "mColorConversion");
        checkNoGLES2Error();
        setupTextures();
        checkNoGLES2Error();
    }

    public void setFrame(VpxOutputBuffer vpxOutputBuffer) {
        VpxOutputBuffer andSet = this.pendingOutputBufferReference.getAndSet(vpxOutputBuffer);
        if (andSet != null) {
            andSet.release();
        }
    }
}
