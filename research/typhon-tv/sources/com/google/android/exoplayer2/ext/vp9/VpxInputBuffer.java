package com.google.android.exoplayer2.ext.vp9;

import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.video.ColorInfo;

final class VpxInputBuffer extends DecoderInputBuffer {
    public ColorInfo colorInfo;

    public VpxInputBuffer() {
        super(2);
    }
}
