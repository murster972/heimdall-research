package com.google.android.exoplayer2.ext.vp9;

import com.google.android.exoplayer2.decoder.CryptoInfo;
import com.google.android.exoplayer2.decoder.SimpleDecoder;
import com.google.android.exoplayer2.drm.DecryptionException;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import java.nio.ByteBuffer;

final class VpxDecoder extends SimpleDecoder<VpxInputBuffer, VpxOutputBuffer, VpxDecoderException> {
    private static final int DECODE_ERROR = 1;
    private static final int DRM_ERROR = 2;
    private static final int NO_ERROR = 0;
    public static final int OUTPUT_MODE_NONE = -1;
    public static final int OUTPUT_MODE_RGB = 1;
    public static final int OUTPUT_MODE_YUV = 0;
    private final ExoMediaCrypto exoMediaCrypto;
    private volatile int outputMode;
    private final long vpxDecContext;

    public VpxDecoder(int i, int i2, int i3, ExoMediaCrypto exoMediaCrypto2) throws VpxDecoderException {
        super(new VpxInputBuffer[i], new VpxOutputBuffer[i2]);
        if (!VpxLibrary.isAvailable()) {
            throw new VpxDecoderException("Failed to load decoder native libraries.");
        }
        this.exoMediaCrypto = exoMediaCrypto2;
        if (exoMediaCrypto2 == null || VpxLibrary.vpxIsSecureDecodeSupported()) {
            this.vpxDecContext = vpxInit();
            if (this.vpxDecContext == 0) {
                throw new VpxDecoderException("Failed to initialize decoder");
            }
            setInitialInputBufferSize(i3);
            return;
        }
        throw new VpxDecoderException("Vpx decoder does not support secure decode.");
    }

    private native long vpxClose(long j);

    private native long vpxDecode(long j, ByteBuffer byteBuffer, int i);

    private native int vpxGetErrorCode(long j);

    private native String vpxGetErrorMessage(long j);

    private native int vpxGetFrame(long j, VpxOutputBuffer vpxOutputBuffer);

    private native long vpxInit();

    private native long vpxSecureDecode(long j, ByteBuffer byteBuffer, int i, ExoMediaCrypto exoMediaCrypto2, int i2, byte[] bArr, byte[] bArr2, int i3, int[] iArr, int[] iArr2);

    /* access modifiers changed from: protected */
    public VpxInputBuffer createInputBuffer() {
        return new VpxInputBuffer();
    }

    /* access modifiers changed from: protected */
    public VpxOutputBuffer createOutputBuffer() {
        return new VpxOutputBuffer(this);
    }

    /* access modifiers changed from: protected */
    public VpxDecoderException decode(VpxInputBuffer vpxInputBuffer, VpxOutputBuffer vpxOutputBuffer, boolean z) {
        long vpxDecode;
        ByteBuffer byteBuffer = vpxInputBuffer.data;
        int limit = byteBuffer.limit();
        CryptoInfo cryptoInfo = vpxInputBuffer.cryptoInfo;
        if (vpxInputBuffer.isEncrypted()) {
            vpxDecode = vpxSecureDecode(this.vpxDecContext, byteBuffer, limit, this.exoMediaCrypto, cryptoInfo.mode, cryptoInfo.key, cryptoInfo.iv, cryptoInfo.numSubSamples, cryptoInfo.numBytesOfClearData, cryptoInfo.numBytesOfEncryptedData);
        } else {
            vpxDecode = vpxDecode(this.vpxDecContext, byteBuffer, limit);
        }
        if (vpxDecode == 0) {
            if (!vpxInputBuffer.isDecodeOnly()) {
                vpxOutputBuffer.init(vpxInputBuffer.timeUs, this.outputMode);
                int vpxGetFrame = vpxGetFrame(this.vpxDecContext, vpxOutputBuffer);
                if (vpxGetFrame == 1) {
                    vpxOutputBuffer.addFlag(Integer.MIN_VALUE);
                } else if (vpxGetFrame == -1) {
                    return new VpxDecoderException("Buffer initialization failed.");
                }
                vpxOutputBuffer.colorInfo = vpxInputBuffer.colorInfo;
            }
            return null;
        } else if (vpxDecode == 2) {
            String str = "Drm error: " + vpxGetErrorMessage(this.vpxDecContext);
            return new VpxDecoderException(str, new DecryptionException(vpxGetErrorCode(this.vpxDecContext), str));
        } else {
            return new VpxDecoderException("Decode error: " + vpxGetErrorMessage(this.vpxDecContext));
        }
    }

    public String getName() {
        return "libvpx" + VpxLibrary.getVersion();
    }

    public void release() {
        super.release();
        vpxClose(this.vpxDecContext);
    }

    /* access modifiers changed from: protected */
    public void releaseOutputBuffer(VpxOutputBuffer vpxOutputBuffer) {
        super.releaseOutputBuffer(vpxOutputBuffer);
    }

    public void setOutputMode(int i) {
        this.outputMode = i;
    }
}
