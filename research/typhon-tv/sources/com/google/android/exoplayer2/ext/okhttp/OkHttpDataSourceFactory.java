package com.google.android.exoplayer2.ext.okhttp;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Predicate;
import okhttp3.CacheControl;
import okhttp3.Call;

public final class OkHttpDataSourceFactory extends HttpDataSource.BaseFactory {
    private final CacheControl cacheControl;
    private final Call.Factory callFactory;
    private final TransferListener<? super DataSource> listener;
    private final String userAgent;

    public OkHttpDataSourceFactory(Call.Factory factory, String str, TransferListener<? super DataSource> transferListener) {
        this(factory, str, transferListener, (CacheControl) null);
    }

    public OkHttpDataSourceFactory(Call.Factory factory, String str, TransferListener<? super DataSource> transferListener, CacheControl cacheControl2) {
        this.callFactory = factory;
        this.userAgent = str;
        this.listener = transferListener;
        this.cacheControl = cacheControl2;
    }

    /* access modifiers changed from: protected */
    public OkHttpDataSource createDataSourceInternal(HttpDataSource.RequestProperties requestProperties) {
        return new OkHttpDataSource(this.callFactory, this.userAgent, (Predicate<String>) null, this.listener, this.cacheControl, requestProperties);
    }
}
