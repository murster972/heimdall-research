package com.google.android.exoplayer2.ext.vp9;

import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.util.LibraryLoader;

public final class VpxLibrary {
    private static final LibraryLoader LOADER = new LibraryLoader("vpx", "vpxJNI");

    static {
        ExoPlayerLibraryInfo.registerModule("goog.exo.vpx");
    }

    private VpxLibrary() {
    }

    public static String getBuildConfig() {
        if (isAvailable()) {
            return vpxGetBuildConfig();
        }
        return null;
    }

    public static String getVersion() {
        if (isAvailable()) {
            return vpxGetVersion();
        }
        return null;
    }

    public static boolean isAvailable() {
        return LOADER.isAvailable();
    }

    public static boolean isHighBitDepthSupported() {
        String buildConfig = getBuildConfig();
        return (buildConfig != null ? buildConfig.indexOf("--enable-vp9-highbitdepth") : -1) >= 0;
    }

    public static void setLibraries(String... strArr) {
        LOADER.setLibraries(strArr);
    }

    private static native String vpxGetBuildConfig();

    private static native String vpxGetVersion();

    public static native boolean vpxIsSecureDecodeSupported();
}
