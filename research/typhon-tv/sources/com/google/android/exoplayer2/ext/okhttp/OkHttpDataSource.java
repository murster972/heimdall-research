package com.google.android.exoplayer2.ext.okhttp;

import android.net.Uri;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.upstream.DataSourceException;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Predicate;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpDataSource implements HttpDataSource {
    private static final AtomicReference<byte[]> skipBufferReference = new AtomicReference<>();
    private long bytesRead;
    private long bytesSkipped;
    private long bytesToRead;
    private long bytesToSkip;
    private final CacheControl cacheControl;
    private final Call.Factory callFactory;
    private final Predicate<String> contentTypePredicate;
    private DataSpec dataSpec;
    private final HttpDataSource.RequestProperties defaultRequestProperties;
    private final TransferListener<? super OkHttpDataSource> listener;
    private boolean opened;
    private final HttpDataSource.RequestProperties requestProperties;
    private Response response;
    private InputStream responseByteStream;
    private final String userAgent;

    static {
        ExoPlayerLibraryInfo.registerModule("goog.exo.okhttp");
    }

    public OkHttpDataSource(Call.Factory factory, String str, Predicate<String> predicate) {
        this(factory, str, predicate, (TransferListener<? super OkHttpDataSource>) null);
    }

    public OkHttpDataSource(Call.Factory factory, String str, Predicate<String> predicate, TransferListener<? super OkHttpDataSource> transferListener) {
        this(factory, str, predicate, transferListener, (CacheControl) null, (HttpDataSource.RequestProperties) null);
    }

    public OkHttpDataSource(Call.Factory factory, String str, Predicate<String> predicate, TransferListener<? super OkHttpDataSource> transferListener, CacheControl cacheControl2, HttpDataSource.RequestProperties requestProperties2) {
        this.callFactory = (Call.Factory) Assertions.checkNotNull(factory);
        this.userAgent = str;
        this.contentTypePredicate = predicate;
        this.listener = transferListener;
        this.cacheControl = cacheControl2;
        this.defaultRequestProperties = requestProperties2;
        this.requestProperties = new HttpDataSource.RequestProperties();
    }

    private void closeConnectionQuietly() {
        this.response.m7056().close();
        this.response = null;
        this.responseByteStream = null;
    }

    private Request makeRequest(DataSpec dataSpec2) {
        long j = dataSpec2.position;
        long j2 = dataSpec2.length;
        boolean isFlagSet = dataSpec2.isFlagSet(1);
        HttpUrl r8 = HttpUrl.m6937(dataSpec2.uri.toString());
        Request.Builder r1 = r8 != null ? new Request.Builder().m19997(r8) : new Request.Builder().m19992(dataSpec2.uri.toString());
        if (this.cacheControl != null) {
            r1.m19995(this.cacheControl);
        }
        if (this.defaultRequestProperties != null) {
            for (Map.Entry next : this.defaultRequestProperties.getSnapshot().entrySet()) {
                r1.m19993((String) next.getKey(), (String) next.getValue());
            }
        }
        for (Map.Entry next2 : this.requestProperties.getSnapshot().entrySet()) {
            r1.m19993((String) next2.getKey(), (String) next2.getValue());
        }
        if (!(j == 0 && j2 == -1)) {
            String str = "bytes=" + j + "-";
            if (j2 != -1) {
                str = str + ((j + j2) - 1);
            }
            r1.m19993("Range", str);
        }
        if (this.userAgent != null) {
            r1.m19993(AbstractSpiCall.HEADER_USER_AGENT, this.userAgent);
        }
        if (!isFlagSet) {
            r1.m19988("Accept-Encoding", "identity");
        }
        if (dataSpec2.postBody != null) {
            r1.m19998(RequestBody.create((MediaType) null, dataSpec2.postBody));
        }
        return r1.m19989();
    }

    private int readInternal(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        if (this.bytesToRead != -1) {
            long j = this.bytesToRead - this.bytesRead;
            if (j == 0) {
                return -1;
            }
            i2 = (int) Math.min((long) i2, j);
        }
        int read = this.responseByteStream.read(bArr, i, i2);
        if (read != -1) {
            this.bytesRead += (long) read;
            if (this.listener == null) {
                return read;
            }
            this.listener.onBytesTransferred(this, read);
            return read;
        } else if (this.bytesToRead == -1) {
            return -1;
        } else {
            throw new EOFException();
        }
    }

    private void skipInternal() throws IOException {
        if (this.bytesSkipped != this.bytesToSkip) {
            byte[] andSet = skipBufferReference.getAndSet((Object) null);
            if (andSet == null) {
                andSet = new byte[4096];
            }
            while (this.bytesSkipped != this.bytesToSkip) {
                int read = this.responseByteStream.read(andSet, 0, (int) Math.min(this.bytesToSkip - this.bytesSkipped, (long) andSet.length));
                if (Thread.interrupted()) {
                    throw new InterruptedIOException();
                } else if (read == -1) {
                    throw new EOFException();
                } else {
                    this.bytesSkipped += (long) read;
                    if (this.listener != null) {
                        this.listener.onBytesTransferred(this, read);
                    }
                }
            }
            skipBufferReference.set(andSet);
        }
    }

    /* access modifiers changed from: protected */
    public final long bytesRead() {
        return this.bytesRead;
    }

    /* access modifiers changed from: protected */
    public final long bytesRemaining() {
        return this.bytesToRead == -1 ? this.bytesToRead : this.bytesToRead - this.bytesRead;
    }

    /* access modifiers changed from: protected */
    public final long bytesSkipped() {
        return this.bytesSkipped;
    }

    public void clearAllRequestProperties() {
        this.requestProperties.clear();
    }

    public void clearRequestProperty(String str) {
        Assertions.checkNotNull(str);
        this.requestProperties.remove(str);
    }

    public void close() throws HttpDataSource.HttpDataSourceException {
        if (this.opened) {
            this.opened = false;
            if (this.listener != null) {
                this.listener.onTransferEnd(this);
            }
            closeConnectionQuietly();
        }
    }

    public Map<String, List<String>> getResponseHeaders() {
        if (this.response == null) {
            return null;
        }
        return this.response.m7055().m6933();
    }

    public Uri getUri() {
        if (this.response == null) {
            return null;
        }
        return Uri.parse(this.response.m7069().m7053().toString());
    }

    public long open(DataSpec dataSpec2) throws HttpDataSource.HttpDataSourceException {
        long j = 0;
        this.dataSpec = dataSpec2;
        this.bytesRead = 0;
        this.bytesSkipped = 0;
        int request = request(false);
        MediaType r3 = this.response.m7056().m7096();
        String mediaType = r3 != null ? r3.toString() : null;
        if (this.contentTypePredicate == null || this.contentTypePredicate.evaluate(mediaType)) {
            if (request == 200 && dataSpec2.position != 0) {
                j = dataSpec2.position;
            }
            this.bytesToSkip = j;
            if (dataSpec2.length != -1) {
                this.bytesToRead = dataSpec2.length;
            } else {
                long r0 = this.response.m7056().m7093();
                this.bytesToRead = r0 != -1 ? r0 - this.bytesToSkip : -1;
            }
            this.opened = true;
            if (this.listener != null) {
                this.listener.onTransferStart(this, dataSpec2);
            }
            return this.bytesToRead;
        }
        closeConnectionQuietly();
        throw new HttpDataSource.InvalidContentTypeException(mediaType, dataSpec2);
    }

    public int read(byte[] bArr, int i, int i2) throws HttpDataSource.HttpDataSourceException {
        try {
            skipInternal();
            return readInternal(bArr, i, i2);
        } catch (IOException e) {
            throw new HttpDataSource.HttpDataSourceException(e, this.dataSpec, 2);
        }
    }

    public int request(boolean z) throws HttpDataSource.HttpDataSourceException {
        Request makeRequest = makeRequest(this.dataSpec);
        try {
            this.response = this.callFactory.m19888(makeRequest).m19883();
            this.responseByteStream = this.response.m7056().m7094();
            int r4 = this.response.m7066();
            if (this.response.m7065()) {
                return r4;
            }
            Map<String, List<String>> r2 = makeRequest.m7051().m6933();
            closeConnectionQuietly();
            HttpDataSource.InvalidResponseCodeException invalidResponseCodeException = new HttpDataSource.InvalidResponseCodeException(r4, r2, this.dataSpec);
            if (r4 == 416) {
                invalidResponseCodeException.initCause(new DataSourceException(0));
            } else if (r4 == 429 && !z) {
                return request(true);
            }
            throw invalidResponseCodeException;
        } catch (IOException e) {
            throw new HttpDataSource.HttpDataSourceException("Unable to connect to " + this.dataSpec.uri.toString(), e, this.dataSpec, 1);
        }
    }

    public void setRequestProperty(String str, String str2) {
        Assertions.checkNotNull(str);
        Assertions.checkNotNull(str2);
        this.requestProperties.set(str, str2);
    }
}
