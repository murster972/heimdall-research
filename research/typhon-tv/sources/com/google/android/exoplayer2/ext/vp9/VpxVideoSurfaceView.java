package com.google.android.exoplayer2.ext.vp9;

import android.annotation.TargetApi;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

@TargetApi(11)
public class VpxVideoSurfaceView extends GLSurfaceView implements VpxOutputBufferRenderer {
    private final VpxRenderer renderer;

    public VpxVideoSurfaceView(Context context) {
        this(context, (AttributeSet) null);
    }

    public VpxVideoSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.renderer = new VpxRenderer();
        setPreserveEGLContextOnPause(true);
        setEGLContextClientVersion(2);
        setRenderer(this.renderer);
        setRenderMode(0);
    }

    public void setOutputBuffer(VpxOutputBuffer vpxOutputBuffer) {
        this.renderer.setFrame(vpxOutputBuffer);
        requestRender();
    }
}
