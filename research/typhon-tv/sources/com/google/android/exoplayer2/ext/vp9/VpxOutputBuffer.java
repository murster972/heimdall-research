package com.google.android.exoplayer2.ext.vp9;

import com.google.android.exoplayer2.decoder.OutputBuffer;
import com.google.android.exoplayer2.video.ColorInfo;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.nio.ByteBuffer;

final class VpxOutputBuffer extends OutputBuffer {
    public static final int COLORSPACE_BT2020 = 3;
    public static final int COLORSPACE_BT601 = 1;
    public static final int COLORSPACE_BT709 = 2;
    public static final int COLORSPACE_UNKNOWN = 0;
    public ColorInfo colorInfo;
    public int colorspace;
    public ByteBuffer data;
    public int height;
    public int mode;
    private final VpxDecoder owner;
    public int width;
    public ByteBuffer[] yuvPlanes;
    public int[] yuvStrides;

    public VpxOutputBuffer(VpxDecoder vpxDecoder) {
        this.owner = vpxDecoder;
    }

    private void initData(int i) {
        if (this.data == null || this.data.capacity() < i) {
            this.data = ByteBuffer.allocateDirect(i);
            return;
        }
        this.data.position(0);
        this.data.limit(i);
    }

    private boolean isSafeToMultiply(int i, int i2) {
        return i >= 0 && i2 >= 0 && (i2 <= 0 || i < MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT / i2);
    }

    public void init(long j, int i) {
        this.timeUs = j;
        this.mode = i;
    }

    public boolean initForRgbFrame(int i, int i2) {
        this.width = i;
        this.height = i2;
        this.yuvPlanes = null;
        if (!isSafeToMultiply(i, i2) || !isSafeToMultiply(i * i2, 2)) {
            return false;
        }
        initData(i * i2 * 2);
        return true;
    }

    public boolean initForYuvFrame(int i, int i2, int i3, int i4, int i5) {
        this.width = i;
        this.height = i2;
        this.colorspace = i5;
        int i6 = (int) ((((long) i2) + 1) / 2);
        if (!isSafeToMultiply(i3, i2) || !isSafeToMultiply(i4, i6)) {
            return false;
        }
        int i7 = i3 * i2;
        int i8 = i4 * i6;
        int i9 = i7 + (i8 * 2);
        if (!isSafeToMultiply(i8, 2) || i9 < i7) {
            return false;
        }
        initData(i9);
        if (this.yuvPlanes == null) {
            this.yuvPlanes = new ByteBuffer[3];
        }
        this.yuvPlanes[0] = this.data.slice();
        this.yuvPlanes[0].limit(i7);
        this.data.position(i7);
        this.yuvPlanes[1] = this.data.slice();
        this.yuvPlanes[1].limit(i8);
        this.data.position(i7 + i8);
        this.yuvPlanes[2] = this.data.slice();
        this.yuvPlanes[2].limit(i8);
        if (this.yuvStrides == null) {
            this.yuvStrides = new int[3];
        }
        this.yuvStrides[0] = i3;
        this.yuvStrides[1] = i4;
        this.yuvStrides[2] = i4;
        return true;
    }

    public void release() {
        this.owner.releaseOutputBuffer(this);
    }
}
