package com.google.android.exoplayer2.ext.vp9;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.Surface;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class LibvpxVideoRenderer extends BaseRenderer {
    private static final int INITIAL_INPUT_BUFFER_SIZE = 786432;
    public static final int MSG_SET_OUTPUT_BUFFER_RENDERER = 10000;
    private static final int NUM_INPUT_BUFFERS = 8;
    private static final int NUM_OUTPUT_BUFFERS = 16;
    private static final int REINITIALIZATION_STATE_NONE = 0;
    private static final int REINITIALIZATION_STATE_SIGNAL_END_OF_STREAM = 1;
    private static final int REINITIALIZATION_STATE_WAIT_END_OF_STREAM = 2;
    private final long allowedJoiningTimeMs;
    private Bitmap bitmap;
    private int consecutiveDroppedFrameCount;
    private VpxDecoder decoder;
    private DecoderCounters decoderCounters;
    private boolean decoderReceivedBuffers;
    private int decoderReinitializationState;
    private DrmSession<ExoMediaCrypto> drmSession;
    private final DrmSessionManager<ExoMediaCrypto> drmSessionManager;
    private long droppedFrameAccumulationStartTimeMs;
    private int droppedFrames;
    private final VideoRendererEventListener.EventDispatcher eventDispatcher;
    private final DecoderInputBuffer flagsOnlyBuffer;
    private Format format;
    private final FormatHolder formatHolder;
    private VpxInputBuffer inputBuffer;
    private boolean inputStreamEnded;
    private long joiningDeadlineMs;
    private final int maxDroppedFramesToNotify;
    private VpxOutputBuffer nextOutputBuffer;
    private VpxOutputBuffer outputBuffer;
    private VpxOutputBufferRenderer outputBufferRenderer;
    private int outputMode;
    private boolean outputStreamEnded;
    private DrmSession<ExoMediaCrypto> pendingDrmSession;
    private final boolean playClearSamplesWithoutKeys;
    private boolean renderedFirstFrame;
    private int reportedHeight;
    private int reportedWidth;
    private final boolean scaleToFit;
    private Surface surface;
    private boolean waitingForKeys;

    @Retention(RetentionPolicy.SOURCE)
    private @interface ReinitializationState {
    }

    public LibvpxVideoRenderer(boolean z, long j) {
        this(z, j, (Handler) null, (VideoRendererEventListener) null, 0);
    }

    public LibvpxVideoRenderer(boolean z, long j, Handler handler, VideoRendererEventListener videoRendererEventListener, int i) {
        this(z, j, handler, videoRendererEventListener, i, (DrmSessionManager<ExoMediaCrypto>) null, false);
    }

    public LibvpxVideoRenderer(boolean z, long j, Handler handler, VideoRendererEventListener videoRendererEventListener, int i, DrmSessionManager<ExoMediaCrypto> drmSessionManager2, boolean z2) {
        super(2);
        this.scaleToFit = z;
        this.allowedJoiningTimeMs = j;
        this.maxDroppedFramesToNotify = i;
        this.drmSessionManager = drmSessionManager2;
        this.playClearSamplesWithoutKeys = z2;
        this.joiningDeadlineMs = C.TIME_UNSET;
        clearReportedVideoSize();
        this.formatHolder = new FormatHolder();
        this.flagsOnlyBuffer = DecoderInputBuffer.newFlagsOnlyInstance();
        this.eventDispatcher = new VideoRendererEventListener.EventDispatcher(handler, videoRendererEventListener);
        this.outputMode = -1;
        this.decoderReinitializationState = 0;
    }

    private void clearRenderedFirstFrame() {
        this.renderedFirstFrame = false;
    }

    private void clearReportedVideoSize() {
        this.reportedWidth = -1;
        this.reportedHeight = -1;
    }

    private boolean drainOutputBuffer(long j) throws ExoPlaybackException, VpxDecoderException {
        if (this.outputBuffer == null) {
            if (this.nextOutputBuffer != null) {
                this.outputBuffer = this.nextOutputBuffer;
                this.nextOutputBuffer = null;
            } else {
                this.outputBuffer = (VpxOutputBuffer) this.decoder.dequeueOutputBuffer();
            }
            if (this.outputBuffer == null) {
                return false;
            }
            this.decoderCounters.skippedOutputBufferCount += this.outputBuffer.skippedOutputBufferCount;
        }
        if (this.nextOutputBuffer == null) {
            this.nextOutputBuffer = (VpxOutputBuffer) this.decoder.dequeueOutputBuffer();
        }
        if (this.outputBuffer.isEndOfStream()) {
            if (this.decoderReinitializationState == 2) {
                releaseDecoder();
                maybeInitDecoder();
            } else {
                this.outputBuffer.release();
                this.outputBuffer = null;
                this.outputStreamEnded = true;
            }
            return false;
        } else if (this.outputMode != -1) {
            if (shouldDropOutputBuffer(this.outputBuffer.timeUs, (this.nextOutputBuffer == null || this.nextOutputBuffer.isEndOfStream()) ? C.TIME_UNSET : this.nextOutputBuffer.timeUs, j, this.joiningDeadlineMs)) {
                dropBuffer();
                return true;
            }
            if (!this.renderedFirstFrame || (getState() == 2 && this.outputBuffer.timeUs <= 30000 + j)) {
                renderBuffer();
            }
            return false;
        } else if (!isBufferLate(this.outputBuffer.timeUs - j)) {
            return false;
        } else {
            skipBuffer();
            return true;
        }
    }

    private void dropBuffer() {
        this.decoderCounters.droppedOutputBufferCount++;
        this.droppedFrames++;
        this.consecutiveDroppedFrameCount++;
        this.decoderCounters.maxConsecutiveDroppedOutputBufferCount = Math.max(this.consecutiveDroppedFrameCount, this.decoderCounters.maxConsecutiveDroppedOutputBufferCount);
        if (this.droppedFrames == this.maxDroppedFramesToNotify) {
            maybeNotifyDroppedFrames();
        }
        this.outputBuffer.release();
        this.outputBuffer = null;
    }

    private boolean feedInputBuffer() throws VpxDecoderException, ExoPlaybackException {
        if (this.decoder == null || this.decoderReinitializationState == 2 || this.inputStreamEnded) {
            return false;
        }
        if (this.inputBuffer == null) {
            this.inputBuffer = (VpxInputBuffer) this.decoder.dequeueInputBuffer();
            if (this.inputBuffer == null) {
                return false;
            }
        }
        if (this.decoderReinitializationState == 1) {
            this.inputBuffer.setFlags(4);
            this.decoder.queueInputBuffer(this.inputBuffer);
            this.inputBuffer = null;
            this.decoderReinitializationState = 2;
            return false;
        }
        int readSource = this.waitingForKeys ? -4 : readSource(this.formatHolder, this.inputBuffer, false);
        if (readSource == -3) {
            return false;
        }
        if (readSource == -5) {
            onInputFormatChanged(this.formatHolder.format);
            return true;
        } else if (this.inputBuffer.isEndOfStream()) {
            this.inputStreamEnded = true;
            this.decoder.queueInputBuffer(this.inputBuffer);
            this.inputBuffer = null;
            return false;
        } else {
            this.waitingForKeys = shouldWaitForKeys(this.inputBuffer.isEncrypted());
            if (this.waitingForKeys) {
                return false;
            }
            this.inputBuffer.flip();
            this.inputBuffer.colorInfo = this.formatHolder.format.colorInfo;
            this.decoder.queueInputBuffer(this.inputBuffer);
            this.decoderReceivedBuffers = true;
            this.decoderCounters.inputBufferCount++;
            this.inputBuffer = null;
            return true;
        }
    }

    private void flushDecoder() throws ExoPlaybackException {
        this.waitingForKeys = false;
        if (this.decoderReinitializationState != 0) {
            releaseDecoder();
            maybeInitDecoder();
            return;
        }
        this.inputBuffer = null;
        if (this.outputBuffer != null) {
            this.outputBuffer.release();
            this.outputBuffer = null;
        }
        if (this.nextOutputBuffer != null) {
            this.nextOutputBuffer.release();
            this.nextOutputBuffer = null;
        }
        this.decoder.flush();
        this.decoderReceivedBuffers = false;
    }

    private static boolean isBufferLate(long j) {
        return j < -30000;
    }

    private void maybeInitDecoder() throws ExoPlaybackException {
        if (this.decoder == null) {
            this.drmSession = this.pendingDrmSession;
            ExoMediaCrypto exoMediaCrypto = null;
            if (this.drmSession == null || (exoMediaCrypto = this.drmSession.getMediaCrypto()) != null) {
                try {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    TraceUtil.beginSection("createVpxDecoder");
                    this.decoder = new VpxDecoder(8, 16, INITIAL_INPUT_BUFFER_SIZE, exoMediaCrypto);
                    this.decoder.setOutputMode(this.outputMode);
                    TraceUtil.endSection();
                    long elapsedRealtime2 = SystemClock.elapsedRealtime();
                    this.eventDispatcher.decoderInitialized(this.decoder.getName(), elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
                    this.decoderCounters.decoderInitCount++;
                } catch (VpxDecoderException e) {
                    throw ExoPlaybackException.createForRenderer(e, getIndex());
                }
            } else {
                DrmSession.DrmSessionException error = this.drmSession.getError();
                if (error != null) {
                    throw ExoPlaybackException.createForRenderer(error, getIndex());
                }
            }
        }
    }

    private void maybeNotifyDroppedFrames() {
        if (this.droppedFrames > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.eventDispatcher.droppedFrames(this.droppedFrames, elapsedRealtime - this.droppedFrameAccumulationStartTimeMs);
            this.droppedFrames = 0;
            this.droppedFrameAccumulationStartTimeMs = elapsedRealtime;
        }
    }

    private void maybeNotifyRenderedFirstFrame() {
        if (!this.renderedFirstFrame) {
            this.renderedFirstFrame = true;
            this.eventDispatcher.renderedFirstFrame(this.surface);
        }
    }

    private void maybeNotifyVideoSizeChanged(int i, int i2) {
        if (this.reportedWidth != i || this.reportedHeight != i2) {
            this.reportedWidth = i;
            this.reportedHeight = i2;
            this.eventDispatcher.videoSizeChanged(i, i2, 0, 1.0f);
        }
    }

    private void maybeRenotifyRenderedFirstFrame() {
        if (this.renderedFirstFrame) {
            this.eventDispatcher.renderedFirstFrame(this.surface);
        }
    }

    private void maybeRenotifyVideoSizeChanged() {
        if (this.reportedWidth != -1 || this.reportedHeight != -1) {
            this.eventDispatcher.videoSizeChanged(this.reportedWidth, this.reportedHeight, 0, 1.0f);
        }
    }

    private void onInputFormatChanged(Format format2) throws ExoPlaybackException {
        Format format3 = this.format;
        this.format = format2;
        if (!Util.areEqual(this.format.drmInitData, format3 == null ? null : format3.drmInitData)) {
            if (this.format.drmInitData == null) {
                this.pendingDrmSession = null;
            } else if (this.drmSessionManager == null) {
                throw ExoPlaybackException.createForRenderer(new IllegalStateException("Media requires a DrmSessionManager"), getIndex());
            } else {
                this.pendingDrmSession = this.drmSessionManager.acquireSession(Looper.myLooper(), this.format.drmInitData);
                if (this.pendingDrmSession == this.drmSession) {
                    this.drmSessionManager.releaseSession(this.pendingDrmSession);
                }
            }
        }
        if (this.pendingDrmSession != this.drmSession) {
            if (this.decoderReceivedBuffers) {
                this.decoderReinitializationState = 1;
            } else {
                releaseDecoder();
                maybeInitDecoder();
            }
        }
        this.eventDispatcher.inputFormatChanged(this.format);
    }

    private void releaseDecoder() {
        if (this.decoder != null) {
            this.inputBuffer = null;
            this.outputBuffer = null;
            this.nextOutputBuffer = null;
            this.decoder.release();
            this.decoder = null;
            this.decoderCounters.decoderReleaseCount++;
            this.decoderReinitializationState = 0;
            this.decoderReceivedBuffers = false;
        }
    }

    private void renderBuffer() {
        boolean z = true;
        int i = this.outputBuffer.mode;
        boolean z2 = i == 1 && this.surface != null;
        if (i != 0 || this.outputBufferRenderer == null) {
            z = false;
        }
        if (z2 || z) {
            maybeNotifyVideoSizeChanged(this.outputBuffer.width, this.outputBuffer.height);
            if (z2) {
                renderRgbFrame(this.outputBuffer, this.scaleToFit);
                this.outputBuffer.release();
            } else {
                this.outputBufferRenderer.setOutputBuffer(this.outputBuffer);
            }
            this.outputBuffer = null;
            this.consecutiveDroppedFrameCount = 0;
            this.decoderCounters.renderedOutputBufferCount++;
            maybeNotifyRenderedFirstFrame();
            return;
        }
        dropBuffer();
    }

    private void renderRgbFrame(VpxOutputBuffer vpxOutputBuffer, boolean z) {
        if (!(this.bitmap != null && this.bitmap.getWidth() == vpxOutputBuffer.width && this.bitmap.getHeight() == vpxOutputBuffer.height)) {
            this.bitmap = Bitmap.createBitmap(vpxOutputBuffer.width, vpxOutputBuffer.height, Bitmap.Config.RGB_565);
        }
        this.bitmap.copyPixelsFromBuffer(vpxOutputBuffer.data);
        Canvas lockCanvas = this.surface.lockCanvas((Rect) null);
        if (z) {
            lockCanvas.scale(((float) lockCanvas.getWidth()) / ((float) vpxOutputBuffer.width), ((float) lockCanvas.getHeight()) / ((float) vpxOutputBuffer.height));
        }
        lockCanvas.drawBitmap(this.bitmap, 0.0f, 0.0f, (Paint) null);
        this.surface.unlockCanvasAndPost(lockCanvas);
    }

    private void setJoiningDeadlineMs() {
        this.joiningDeadlineMs = this.allowedJoiningTimeMs > 0 ? SystemClock.elapsedRealtime() + this.allowedJoiningTimeMs : C.TIME_UNSET;
    }

    private void setOutput(Surface surface2, VpxOutputBufferRenderer vpxOutputBufferRenderer) {
        int i = 0;
        Assertions.checkState(surface2 == null || vpxOutputBufferRenderer == null);
        if (this.surface != surface2 || this.outputBufferRenderer != vpxOutputBufferRenderer) {
            this.surface = surface2;
            this.outputBufferRenderer = vpxOutputBufferRenderer;
            if (vpxOutputBufferRenderer == null) {
                i = surface2 != null ? 1 : -1;
            }
            this.outputMode = i;
            if (this.outputMode != -1) {
                if (this.decoder != null) {
                    this.decoder.setOutputMode(this.outputMode);
                }
                maybeRenotifyVideoSizeChanged();
                clearRenderedFirstFrame();
                if (getState() == 2) {
                    setJoiningDeadlineMs();
                    return;
                }
                return;
            }
            clearReportedVideoSize();
            clearRenderedFirstFrame();
        } else if (this.outputMode != -1) {
            maybeRenotifyVideoSizeChanged();
            maybeRenotifyRenderedFirstFrame();
        }
    }

    private boolean shouldWaitForKeys(boolean z) throws ExoPlaybackException {
        if (this.drmSession == null || (!z && this.playClearSamplesWithoutKeys)) {
            return false;
        }
        int state = this.drmSession.getState();
        if (state != 1) {
            return state != 4;
        }
        throw ExoPlaybackException.createForRenderer(this.drmSession.getError(), getIndex());
    }

    private void skipBuffer() {
        this.decoderCounters.skippedOutputBufferCount++;
        this.outputBuffer.release();
        this.outputBuffer = null;
    }

    public void handleMessage(int i, Object obj) throws ExoPlaybackException {
        if (i == 1) {
            setOutput((Surface) obj, (VpxOutputBufferRenderer) null);
        } else if (i == 10000) {
            setOutput((Surface) null, (VpxOutputBufferRenderer) obj);
        } else {
            super.handleMessage(i, obj);
        }
    }

    public boolean isEnded() {
        return this.outputStreamEnded;
    }

    public boolean isReady() {
        if (this.waitingForKeys) {
            return false;
        }
        if (this.format != null && ((isSourceReady() || this.outputBuffer != null) && (this.renderedFirstFrame || this.outputMode == -1))) {
            this.joiningDeadlineMs = C.TIME_UNSET;
            return true;
        } else if (this.joiningDeadlineMs == C.TIME_UNSET) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.joiningDeadlineMs) {
                return true;
            }
            this.joiningDeadlineMs = C.TIME_UNSET;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDisabled() {
        this.format = null;
        this.waitingForKeys = false;
        clearReportedVideoSize();
        clearRenderedFirstFrame();
        try {
            releaseDecoder();
            try {
                if (this.drmSession != null) {
                    this.drmSessionManager.releaseSession(this.drmSession);
                }
                try {
                    if (!(this.pendingDrmSession == null || this.pendingDrmSession == this.drmSession)) {
                        this.drmSessionManager.releaseSession(this.pendingDrmSession);
                    }
                } finally {
                    this.drmSession = null;
                    this.pendingDrmSession = null;
                    this.decoderCounters.ensureUpdated();
                    this.eventDispatcher.disabled(this.decoderCounters);
                }
            } catch (Throwable th) {
                if (!(this.pendingDrmSession == null || this.pendingDrmSession == this.drmSession)) {
                    this.drmSessionManager.releaseSession(this.pendingDrmSession);
                }
                throw th;
            } finally {
                this.drmSession = null;
                this.pendingDrmSession = null;
                this.decoderCounters.ensureUpdated();
                this.eventDispatcher.disabled(this.decoderCounters);
            }
        } catch (Throwable th2) {
            try {
                if (!(this.pendingDrmSession == null || this.pendingDrmSession == this.drmSession)) {
                    this.drmSessionManager.releaseSession(this.pendingDrmSession);
                }
                throw th2;
            } finally {
                this.drmSession = null;
                this.pendingDrmSession = null;
                this.decoderCounters.ensureUpdated();
                this.eventDispatcher.disabled(this.decoderCounters);
            }
        } finally {
        }
    }

    /* access modifiers changed from: protected */
    public void onEnabled(boolean z) throws ExoPlaybackException {
        this.decoderCounters = new DecoderCounters();
        this.eventDispatcher.enabled(this.decoderCounters);
    }

    /* access modifiers changed from: protected */
    public void onPositionReset(long j, boolean z) throws ExoPlaybackException {
        this.inputStreamEnded = false;
        this.outputStreamEnded = false;
        clearRenderedFirstFrame();
        this.consecutiveDroppedFrameCount = 0;
        if (this.decoder != null) {
            flushDecoder();
        }
        if (z) {
            setJoiningDeadlineMs();
        } else {
            this.joiningDeadlineMs = C.TIME_UNSET;
        }
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
        this.droppedFrames = 0;
        this.droppedFrameAccumulationStartTimeMs = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: protected */
    public void onStopped() {
        this.joiningDeadlineMs = C.TIME_UNSET;
        maybeNotifyDroppedFrames();
    }

    public void render(long j, long j2) throws ExoPlaybackException {
        if (!this.outputStreamEnded) {
            if (this.format == null) {
                this.flagsOnlyBuffer.clear();
                int readSource = readSource(this.formatHolder, this.flagsOnlyBuffer, true);
                if (readSource == -5) {
                    onInputFormatChanged(this.formatHolder.format);
                } else if (readSource == -4) {
                    Assertions.checkState(this.flagsOnlyBuffer.isEndOfStream());
                    this.inputStreamEnded = true;
                    this.outputStreamEnded = true;
                    return;
                } else {
                    return;
                }
            }
            maybeInitDecoder();
            if (this.decoder != null) {
                try {
                    TraceUtil.beginSection("drainAndFeed");
                    do {
                    } while (drainOutputBuffer(j));
                    do {
                    } while (feedInputBuffer());
                    TraceUtil.endSection();
                    this.decoderCounters.ensureUpdated();
                } catch (VpxDecoderException e) {
                    throw ExoPlaybackException.createForRenderer(e, getIndex());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldDropOutputBuffer(long j, long j2, long j3, long j4) {
        return isBufferLate(j - j3) && !(j4 == C.TIME_UNSET && j2 == C.TIME_UNSET);
    }

    public int supportsFormat(Format format2) {
        return (!VpxLibrary.isAvailable() || !MimeTypes.VIDEO_VP9.equalsIgnoreCase(format2.sampleMimeType)) ? 0 : 20;
    }
}
