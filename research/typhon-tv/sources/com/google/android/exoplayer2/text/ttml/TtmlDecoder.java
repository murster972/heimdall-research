package com.google.android.exoplayer2.text.ttml;

import android.util.Log;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.util.XmlPullParserUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class TtmlDecoder extends SimpleSubtitleDecoder {
    private static final String ATTR_BEGIN = "begin";
    private static final String ATTR_DURATION = "dur";
    private static final String ATTR_END = "end";
    private static final String ATTR_REGION = "region";
    private static final String ATTR_STYLE = "style";
    private static final Pattern CLOCK_TIME = Pattern.compile("^([0-9][0-9]+):([0-9][0-9]):([0-9][0-9])(?:(\\.[0-9]+)|:([0-9][0-9])(?:\\.([0-9]+))?)?$");
    private static final FrameAndTickRate DEFAULT_FRAME_AND_TICK_RATE = new FrameAndTickRate(30.0f, 1, 1);
    private static final int DEFAULT_FRAME_RATE = 30;
    private static final Pattern FONT_SIZE = Pattern.compile("^(([0-9]*.)?[0-9]+)(px|em|%)$");
    private static final Pattern OFFSET_TIME = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?)(h|m|s|ms|f|t)$");
    private static final Pattern PERCENTAGE_COORDINATES = Pattern.compile("^(\\d+\\.?\\d*?)% (\\d+\\.?\\d*?)%$");
    private static final String TAG = "TtmlDecoder";
    private static final String TTP = "http://www.w3.org/ns/ttml#parameter";
    private final XmlPullParserFactory xmlParserFactory;

    private static final class FrameAndTickRate {
        final float effectiveFrameRate;
        final int subFrameRate;
        final int tickRate;

        FrameAndTickRate(float f, int i, int i2) {
            this.effectiveFrameRate = f;
            this.subFrameRate = i;
            this.tickRate = i2;
        }
    }

    public TtmlDecoder() {
        super(TAG);
        try {
            this.xmlParserFactory = XmlPullParserFactory.newInstance();
            this.xmlParserFactory.setNamespaceAware(true);
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    private TtmlStyle createIfNull(TtmlStyle ttmlStyle) {
        return ttmlStyle == null ? new TtmlStyle() : ttmlStyle;
    }

    private static boolean isSupportedTag(String str) {
        return str.equals(TtmlNode.TAG_TT) || str.equals(TtmlNode.TAG_HEAD) || str.equals(TtmlNode.TAG_BODY) || str.equals(TtmlNode.TAG_DIV) || str.equals(TtmlNode.TAG_P) || str.equals(TtmlNode.TAG_SPAN) || str.equals(TtmlNode.TAG_BR) || str.equals("style") || str.equals(TtmlNode.TAG_STYLING) || str.equals(TtmlNode.TAG_LAYOUT) || str.equals("region") || str.equals(TtmlNode.TAG_METADATA) || str.equals(TtmlNode.TAG_SMPTE_IMAGE) || str.equals(TtmlNode.TAG_SMPTE_DATA) || str.equals(TtmlNode.TAG_SMPTE_INFORMATION);
    }

    private static void parseFontSize(String str, TtmlStyle ttmlStyle) throws SubtitleDecoderException {
        Matcher matcher;
        String[] split = str.split("\\s+");
        if (split.length == 1) {
            matcher = FONT_SIZE.matcher(str);
        } else if (split.length == 2) {
            matcher = FONT_SIZE.matcher(split[1]);
            Log.w(TAG, "Multiple values in fontSize attribute. Picking the second value for vertical font size and ignoring the first.");
        } else {
            throw new SubtitleDecoderException("Invalid number of entries for fontSize: " + split.length + ".");
        }
        if (matcher.matches()) {
            String group = matcher.group(3);
            char c = 65535;
            switch (group.hashCode()) {
                case 37:
                    if (group.equals("%")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3240:
                    if (group.equals("em")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3592:
                    if (group.equals("px")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    ttmlStyle.setFontSizeUnit(1);
                    break;
                case 1:
                    ttmlStyle.setFontSizeUnit(2);
                    break;
                case 2:
                    ttmlStyle.setFontSizeUnit(3);
                    break;
                default:
                    throw new SubtitleDecoderException("Invalid unit for fontSize: '" + group + "'.");
            }
            ttmlStyle.setFontSize(Float.valueOf(matcher.group(1)).floatValue());
            return;
        }
        throw new SubtitleDecoderException("Invalid expression for fontSize: '" + str + "'.");
    }

    private FrameAndTickRate parseFrameAndTickRates(XmlPullParser xmlPullParser) throws SubtitleDecoderException {
        int i = 30;
        String attributeValue = xmlPullParser.getAttributeValue(TTP, "frameRate");
        if (attributeValue != null) {
            i = Integer.parseInt(attributeValue);
        }
        float f = 1.0f;
        String attributeValue2 = xmlPullParser.getAttributeValue(TTP, "frameRateMultiplier");
        if (attributeValue2 != null) {
            String[] split = attributeValue2.split(StringUtils.SPACE);
            if (split.length != 2) {
                throw new SubtitleDecoderException("frameRateMultiplier doesn't have 2 parts");
            }
            f = ((float) Integer.parseInt(split[0])) / ((float) Integer.parseInt(split[1]));
        }
        int i2 = DEFAULT_FRAME_AND_TICK_RATE.subFrameRate;
        String attributeValue3 = xmlPullParser.getAttributeValue(TTP, "subFrameRate");
        if (attributeValue3 != null) {
            i2 = Integer.parseInt(attributeValue3);
        }
        int i3 = DEFAULT_FRAME_AND_TICK_RATE.tickRate;
        String attributeValue4 = xmlPullParser.getAttributeValue(TTP, "tickRate");
        if (attributeValue4 != null) {
            i3 = Integer.parseInt(attributeValue4);
        }
        return new FrameAndTickRate(((float) i) * f, i2, i3);
    }

    private Map<String, TtmlStyle> parseHeader(XmlPullParser xmlPullParser, Map<String, TtmlStyle> map, Map<String, TtmlRegion> map2) throws IOException, XmlPullParserException {
        TtmlRegion parseRegionAttributes;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser, "style")) {
                String attributeValue = XmlPullParserUtil.getAttributeValue(xmlPullParser, "style");
                TtmlStyle parseStyleAttributes = parseStyleAttributes(xmlPullParser, new TtmlStyle());
                if (attributeValue != null) {
                    for (String str : parseStyleIds(attributeValue)) {
                        parseStyleAttributes.chain(map.get(str));
                    }
                }
                if (parseStyleAttributes.getId() != null) {
                    map.put(parseStyleAttributes.getId(), parseStyleAttributes);
                }
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser, "region") && (parseRegionAttributes = parseRegionAttributes(xmlPullParser)) != null) {
                map2.put(parseRegionAttributes.id, parseRegionAttributes);
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, TtmlNode.TAG_HEAD));
        return map;
    }

    private TtmlNode parseNode(XmlPullParser xmlPullParser, TtmlNode ttmlNode, Map<String, TtmlRegion> map, FrameAndTickRate frameAndTickRate) throws SubtitleDecoderException {
        long j = C.TIME_UNSET;
        long j2 = C.TIME_UNSET;
        long j3 = C.TIME_UNSET;
        String str = "";
        String[] strArr = null;
        int attributeCount = xmlPullParser.getAttributeCount();
        TtmlStyle parseStyleAttributes = parseStyleAttributes(xmlPullParser, (TtmlStyle) null);
        for (int i = 0; i < attributeCount; i++) {
            String attributeName = xmlPullParser.getAttributeName(i);
            String attributeValue = xmlPullParser.getAttributeValue(i);
            char c = 65535;
            switch (attributeName.hashCode()) {
                case -934795532:
                    if (attributeName.equals("region")) {
                        c = 4;
                        break;
                    }
                    break;
                case 99841:
                    if (attributeName.equals(ATTR_DURATION)) {
                        c = 2;
                        break;
                    }
                    break;
                case 100571:
                    if (attributeName.equals("end")) {
                        c = 1;
                        break;
                    }
                    break;
                case 93616297:
                    if (attributeName.equals(ATTR_BEGIN)) {
                        c = 0;
                        break;
                    }
                    break;
                case 109780401:
                    if (attributeName.equals("style")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    j2 = parseTimeExpression(attributeValue, frameAndTickRate);
                    break;
                case 1:
                    j3 = parseTimeExpression(attributeValue, frameAndTickRate);
                    break;
                case 2:
                    j = parseTimeExpression(attributeValue, frameAndTickRate);
                    break;
                case 3:
                    String[] parseStyleIds = parseStyleIds(attributeValue);
                    if (parseStyleIds.length <= 0) {
                        break;
                    } else {
                        strArr = parseStyleIds;
                        break;
                    }
                case 4:
                    if (!map.containsKey(attributeValue)) {
                        break;
                    } else {
                        str = attributeValue;
                        break;
                    }
            }
        }
        if (!(ttmlNode == null || ttmlNode.startTimeUs == C.TIME_UNSET)) {
            if (j2 != C.TIME_UNSET) {
                j2 += ttmlNode.startTimeUs;
            }
            if (j3 != C.TIME_UNSET) {
                j3 += ttmlNode.startTimeUs;
            }
        }
        if (j3 == C.TIME_UNSET) {
            if (j != C.TIME_UNSET) {
                j3 = j2 + j;
            } else if (!(ttmlNode == null || ttmlNode.endTimeUs == C.TIME_UNSET)) {
                j3 = ttmlNode.endTimeUs;
            }
        }
        return TtmlNode.buildNode(xmlPullParser.getName(), j2, j3, parseStyleAttributes, strArr, str);
    }

    private TtmlRegion parseRegionAttributes(XmlPullParser xmlPullParser) {
        String attributeValue = XmlPullParserUtil.getAttributeValue(xmlPullParser, "id");
        if (attributeValue == null) {
            return null;
        }
        String attributeValue2 = XmlPullParserUtil.getAttributeValue(xmlPullParser, TtmlNode.ATTR_TTS_ORIGIN);
        if (attributeValue2 != null) {
            Matcher matcher = PERCENTAGE_COORDINATES.matcher(attributeValue2);
            if (matcher.matches()) {
                try {
                    float parseFloat = Float.parseFloat(matcher.group(1)) / 100.0f;
                    float parseFloat2 = Float.parseFloat(matcher.group(2)) / 100.0f;
                    String attributeValue3 = XmlPullParserUtil.getAttributeValue(xmlPullParser, TtmlNode.ATTR_TTS_EXTENT);
                    if (attributeValue3 != null) {
                        Matcher matcher2 = PERCENTAGE_COORDINATES.matcher(attributeValue3);
                        if (matcher2.matches()) {
                            try {
                                float parseFloat3 = Float.parseFloat(matcher2.group(1)) / 100.0f;
                                float parseFloat4 = Float.parseFloat(matcher2.group(2)) / 100.0f;
                                int i = 0;
                                String attributeValue4 = XmlPullParserUtil.getAttributeValue(xmlPullParser, TtmlNode.ATTR_TTS_DISPLAY_ALIGN);
                                if (attributeValue4 != null) {
                                    String lowerInvariant = Util.toLowerInvariant(attributeValue4);
                                    char c = 65535;
                                    switch (lowerInvariant.hashCode()) {
                                        case -1364013995:
                                            if (lowerInvariant.equals(TtmlNode.CENTER)) {
                                                c = 0;
                                                break;
                                            }
                                            break;
                                        case 92734940:
                                            if (lowerInvariant.equals("after")) {
                                                c = 1;
                                                break;
                                            }
                                            break;
                                    }
                                    switch (c) {
                                        case 0:
                                            i = 1;
                                            parseFloat2 += parseFloat4 / 2.0f;
                                            break;
                                        case 1:
                                            i = 2;
                                            parseFloat2 += parseFloat4;
                                            break;
                                    }
                                }
                                return new TtmlRegion(attributeValue, parseFloat, parseFloat2, 0, i, parseFloat3);
                            } catch (NumberFormatException e) {
                                Log.w(TAG, "Ignoring region with malformed extent: " + attributeValue2);
                                return null;
                            }
                        } else {
                            Log.w(TAG, "Ignoring region with unsupported extent: " + attributeValue2);
                            return null;
                        }
                    } else {
                        Log.w(TAG, "Ignoring region without an extent");
                        return null;
                    }
                } catch (NumberFormatException e2) {
                    Log.w(TAG, "Ignoring region with malformed origin: " + attributeValue2);
                    return null;
                }
            } else {
                Log.w(TAG, "Ignoring region with unsupported origin: " + attributeValue2);
                return null;
            }
        } else {
            Log.w(TAG, "Ignoring region without an origin");
            return null;
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.exoplayer2.text.ttml.TtmlStyle parseStyleAttributes(org.xmlpull.v1.XmlPullParser r13, com.google.android.exoplayer2.text.ttml.TtmlStyle r14) {
        /*
            r12 = this;
            r9 = 3
            r8 = 2
            r6 = -1
            r7 = 1
            r5 = 0
            int r0 = r13.getAttributeCount()
            r3 = 0
        L_0x000a:
            if (r3 >= r0) goto L_0x022b
            java.lang.String r1 = r13.getAttributeValue(r3)
            java.lang.String r4 = r13.getAttributeName(r3)
            int r10 = r4.hashCode()
            switch(r10) {
                case -1550943582: goto L_0x0064;
                case -1224696685: goto L_0x0043;
                case -1065511464: goto L_0x006f;
                case -879295043: goto L_0x007a;
                case -734428249: goto L_0x0059;
                case 3355: goto L_0x0022;
                case 94842723: goto L_0x0038;
                case 365601008: goto L_0x004e;
                case 1287124693: goto L_0x002d;
                default: goto L_0x001b;
            }
        L_0x001b:
            r4 = r6
        L_0x001c:
            switch(r4) {
                case 0: goto L_0x0086;
                case 1: goto L_0x009c;
                case 2: goto L_0x00c6;
                case 3: goto L_0x00f0;
                case 4: goto L_0x00fa;
                case 5: goto L_0x0120;
                case 6: goto L_0x0131;
                case 7: goto L_0x0142;
                case 8: goto L_0x01c6;
                default: goto L_0x001f;
            }
        L_0x001f:
            int r3 = r3 + 1
            goto L_0x000a
        L_0x0022:
            java.lang.String r10 = "id"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = r5
            goto L_0x001c
        L_0x002d:
            java.lang.String r10 = "backgroundColor"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = r7
            goto L_0x001c
        L_0x0038:
            java.lang.String r10 = "color"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = r8
            goto L_0x001c
        L_0x0043:
            java.lang.String r10 = "fontFamily"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = r9
            goto L_0x001c
        L_0x004e:
            java.lang.String r10 = "fontSize"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = 4
            goto L_0x001c
        L_0x0059:
            java.lang.String r10 = "fontWeight"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = 5
            goto L_0x001c
        L_0x0064:
            java.lang.String r10 = "fontStyle"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = 6
            goto L_0x001c
        L_0x006f:
            java.lang.String r10 = "textAlign"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = 7
            goto L_0x001c
        L_0x007a:
            java.lang.String r10 = "textDecoration"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001b
            r4 = 8
            goto L_0x001c
        L_0x0086:
            java.lang.String r4 = "style"
            java.lang.String r10 = r13.getName()
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x001f
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setId(r1)
            goto L_0x001f
        L_0x009c:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r12.createIfNull(r14)
            int r4 = com.google.android.exoplayer2.util.ColorParser.parseTtmlColor(r1)     // Catch:{ IllegalArgumentException -> 0x00a9 }
            r14.setBackgroundColor(r4)     // Catch:{ IllegalArgumentException -> 0x00a9 }
            goto L_0x001f
        L_0x00a9:
            r2 = move-exception
            java.lang.String r4 = "TtmlDecoder"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Failed parsing background value: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r1)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r4, r10)
            goto L_0x001f
        L_0x00c6:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r12.createIfNull(r14)
            int r4 = com.google.android.exoplayer2.util.ColorParser.parseTtmlColor(r1)     // Catch:{ IllegalArgumentException -> 0x00d3 }
            r14.setFontColor(r4)     // Catch:{ IllegalArgumentException -> 0x00d3 }
            goto L_0x001f
        L_0x00d3:
            r2 = move-exception
            java.lang.String r4 = "TtmlDecoder"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Failed parsing color value: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r1)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r4, r10)
            goto L_0x001f
        L_0x00f0:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setFontFamily(r1)
            goto L_0x001f
        L_0x00fa:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r12.createIfNull(r14)     // Catch:{ SubtitleDecoderException -> 0x0103 }
            parseFontSize(r1, r14)     // Catch:{ SubtitleDecoderException -> 0x0103 }
            goto L_0x001f
        L_0x0103:
            r2 = move-exception
            java.lang.String r4 = "TtmlDecoder"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Failed parsing fontSize value: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r1)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r4, r10)
            goto L_0x001f
        L_0x0120:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            java.lang.String r10 = "bold"
            boolean r10 = r10.equalsIgnoreCase(r1)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setBold(r10)
            goto L_0x001f
        L_0x0131:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            java.lang.String r10 = "italic"
            boolean r10 = r10.equalsIgnoreCase(r1)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setItalic(r10)
            goto L_0x001f
        L_0x0142:
            java.lang.String r4 = com.google.android.exoplayer2.util.Util.toLowerInvariant(r1)
            int r10 = r4.hashCode()
            switch(r10) {
                case -1364013995: goto L_0x018b;
                case 100571: goto L_0x0180;
                case 3317767: goto L_0x015f;
                case 108511772: goto L_0x0175;
                case 109757538: goto L_0x016a;
                default: goto L_0x014d;
            }
        L_0x014d:
            r4 = r6
        L_0x014e:
            switch(r4) {
                case 0: goto L_0x0153;
                case 1: goto L_0x0196;
                case 2: goto L_0x01a2;
                case 3: goto L_0x01ae;
                case 4: goto L_0x01ba;
                default: goto L_0x0151;
            }
        L_0x0151:
            goto L_0x001f
        L_0x0153:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            android.text.Layout$Alignment r10 = android.text.Layout.Alignment.ALIGN_NORMAL
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setTextAlign(r10)
            goto L_0x001f
        L_0x015f:
            java.lang.String r10 = "left"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x014d
            r4 = r5
            goto L_0x014e
        L_0x016a:
            java.lang.String r10 = "start"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x014d
            r4 = r7
            goto L_0x014e
        L_0x0175:
            java.lang.String r10 = "right"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x014d
            r4 = r8
            goto L_0x014e
        L_0x0180:
            java.lang.String r10 = "end"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x014d
            r4 = r9
            goto L_0x014e
        L_0x018b:
            java.lang.String r10 = "center"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x014d
            r4 = 4
            goto L_0x014e
        L_0x0196:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            android.text.Layout$Alignment r10 = android.text.Layout.Alignment.ALIGN_NORMAL
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setTextAlign(r10)
            goto L_0x001f
        L_0x01a2:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            android.text.Layout$Alignment r10 = android.text.Layout.Alignment.ALIGN_OPPOSITE
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setTextAlign(r10)
            goto L_0x001f
        L_0x01ae:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            android.text.Layout$Alignment r10 = android.text.Layout.Alignment.ALIGN_OPPOSITE
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setTextAlign(r10)
            goto L_0x001f
        L_0x01ba:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            android.text.Layout$Alignment r10 = android.text.Layout.Alignment.ALIGN_CENTER
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setTextAlign(r10)
            goto L_0x001f
        L_0x01c6:
            java.lang.String r4 = com.google.android.exoplayer2.util.Util.toLowerInvariant(r1)
            int r10 = r4.hashCode()
            switch(r10) {
                case -1461280213: goto L_0x0202;
                case -1026963764: goto L_0x01f7;
                case 913457136: goto L_0x01ec;
                case 1679736913: goto L_0x01e1;
                default: goto L_0x01d1;
            }
        L_0x01d1:
            r4 = r6
        L_0x01d2:
            switch(r4) {
                case 0: goto L_0x01d7;
                case 1: goto L_0x020d;
                case 2: goto L_0x0217;
                case 3: goto L_0x0221;
                default: goto L_0x01d5;
            }
        L_0x01d5:
            goto L_0x001f
        L_0x01d7:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setLinethrough(r7)
            goto L_0x001f
        L_0x01e1:
            java.lang.String r10 = "linethrough"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x01d1
            r4 = r5
            goto L_0x01d2
        L_0x01ec:
            java.lang.String r10 = "nolinethrough"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x01d1
            r4 = r7
            goto L_0x01d2
        L_0x01f7:
            java.lang.String r10 = "underline"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x01d1
            r4 = r8
            goto L_0x01d2
        L_0x0202:
            java.lang.String r10 = "nounderline"
            boolean r4 = r4.equals(r10)
            if (r4 == 0) goto L_0x01d1
            r4 = r9
            goto L_0x01d2
        L_0x020d:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setLinethrough(r5)
            goto L_0x001f
        L_0x0217:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setUnderline(r7)
            goto L_0x001f
        L_0x0221:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r4 = r12.createIfNull(r14)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r14 = r4.setUnderline(r5)
            goto L_0x001f
        L_0x022b:
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ttml.TtmlDecoder.parseStyleAttributes(org.xmlpull.v1.XmlPullParser, com.google.android.exoplayer2.text.ttml.TtmlStyle):com.google.android.exoplayer2.text.ttml.TtmlStyle");
    }

    private String[] parseStyleIds(String str) {
        return str.split("\\s+");
    }

    private static long parseTimeExpression(String str, FrameAndTickRate frameAndTickRate) throws SubtitleDecoderException {
        Matcher matcher = CLOCK_TIME.matcher(str);
        if (matcher.matches()) {
            double parseLong = ((double) (Long.parseLong(matcher.group(1)) * 3600)) + ((double) (Long.parseLong(matcher.group(2)) * 60)) + ((double) Long.parseLong(matcher.group(3)));
            String group = matcher.group(4);
            double parseDouble = parseLong + (group != null ? Double.parseDouble(group) : 0.0d);
            String group2 = matcher.group(5);
            double parseLong2 = parseDouble + (group2 != null ? (double) (((float) Long.parseLong(group2)) / frameAndTickRate.effectiveFrameRate) : 0.0d);
            String group3 = matcher.group(6);
            return (long) (1000000.0d * (parseLong2 + (group3 != null ? (((double) Long.parseLong(group3)) / ((double) frameAndTickRate.subFrameRate)) / ((double) frameAndTickRate.effectiveFrameRate) : 0.0d)));
        }
        Matcher matcher2 = OFFSET_TIME.matcher(str);
        if (matcher2.matches()) {
            double parseDouble2 = Double.parseDouble(matcher2.group(1));
            String group4 = matcher2.group(2);
            char c = 65535;
            switch (group4.hashCode()) {
                case 102:
                    if (group4.equals("f")) {
                        c = 4;
                        break;
                    }
                    break;
                case 104:
                    if (group4.equals("h")) {
                        c = 0;
                        break;
                    }
                    break;
                case 109:
                    if (group4.equals("m")) {
                        c = 1;
                        break;
                    }
                    break;
                case 115:
                    if (group4.equals("s")) {
                        c = 2;
                        break;
                    }
                    break;
                case 116:
                    if (group4.equals("t")) {
                        c = 5;
                        break;
                    }
                    break;
                case 3494:
                    if (group4.equals("ms")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    parseDouble2 *= 3600.0d;
                    break;
                case 1:
                    parseDouble2 *= 60.0d;
                    break;
                case 3:
                    parseDouble2 /= 1000.0d;
                    break;
                case 4:
                    parseDouble2 /= (double) frameAndTickRate.effectiveFrameRate;
                    break;
                case 5:
                    parseDouble2 /= (double) frameAndTickRate.tickRate;
                    break;
            }
            return (long) (1000000.0d * parseDouble2);
        }
        throw new SubtitleDecoderException("Malformed time expression: " + str);
    }

    /* access modifiers changed from: protected */
    public TtmlSubtitle decode(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        try {
            XmlPullParser newPullParser = this.xmlParserFactory.newPullParser();
            HashMap hashMap = new HashMap();
            HashMap hashMap2 = new HashMap();
            hashMap2.put("", new TtmlRegion((String) null));
            newPullParser.setInput(new ByteArrayInputStream(bArr, 0, i), (String) null);
            TtmlSubtitle ttmlSubtitle = null;
            LinkedList linkedList = new LinkedList();
            int i2 = 0;
            FrameAndTickRate frameAndTickRate = DEFAULT_FRAME_AND_TICK_RATE;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.getEventType()) {
                TtmlNode ttmlNode = (TtmlNode) linkedList.peekLast();
                if (i2 == 0) {
                    String name = newPullParser.getName();
                    if (eventType == 2) {
                        if (TtmlNode.TAG_TT.equals(name)) {
                            frameAndTickRate = parseFrameAndTickRates(newPullParser);
                        }
                        if (!isSupportedTag(name)) {
                            Log.i(TAG, "Ignoring unsupported tag: " + newPullParser.getName());
                            i2++;
                        } else if (TtmlNode.TAG_HEAD.equals(name)) {
                            parseHeader(newPullParser, hashMap, hashMap2);
                        } else {
                            try {
                                TtmlNode parseNode = parseNode(newPullParser, ttmlNode, hashMap2, frameAndTickRate);
                                linkedList.addLast(parseNode);
                                if (ttmlNode != null) {
                                    ttmlNode.addChild(parseNode);
                                }
                            } catch (SubtitleDecoderException e) {
                                Log.w(TAG, "Suppressing parser error", e);
                                i2++;
                            }
                        }
                    } else if (eventType == 4) {
                        ttmlNode.addChild(TtmlNode.buildTextNode(newPullParser.getText()));
                    } else if (eventType == 3) {
                        if (newPullParser.getName().equals(TtmlNode.TAG_TT)) {
                            ttmlSubtitle = new TtmlSubtitle((TtmlNode) linkedList.getLast(), hashMap, hashMap2);
                        }
                        linkedList.removeLast();
                    }
                } else if (eventType == 2) {
                    i2++;
                } else if (eventType == 3) {
                    i2--;
                }
                newPullParser.next();
            }
            return ttmlSubtitle;
        } catch (XmlPullParserException e2) {
            throw new SubtitleDecoderException("Unable to decode source", e2);
        } catch (IOException e3) {
            throw new IllegalStateException("Unexpected error when reading input.", e3);
        }
    }
}
