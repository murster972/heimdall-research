package com.google.android.exoplayer2.text.dvb;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Region;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class DvbParser {
    private static final int DATA_TYPE_24_TABLE_DATA = 32;
    private static final int DATA_TYPE_28_TABLE_DATA = 33;
    private static final int DATA_TYPE_2BP_CODE_STRING = 16;
    private static final int DATA_TYPE_48_TABLE_DATA = 34;
    private static final int DATA_TYPE_4BP_CODE_STRING = 17;
    private static final int DATA_TYPE_8BP_CODE_STRING = 18;
    private static final int DATA_TYPE_END_LINE = 240;
    private static final int OBJECT_CODING_PIXELS = 0;
    private static final int OBJECT_CODING_STRING = 1;
    private static final int PAGE_STATE_NORMAL = 0;
    private static final int REGION_DEPTH_4_BIT = 2;
    private static final int REGION_DEPTH_8_BIT = 3;
    private static final int SEGMENT_TYPE_CLUT_DEFINITION = 18;
    private static final int SEGMENT_TYPE_DISPLAY_DEFINITION = 20;
    private static final int SEGMENT_TYPE_OBJECT_DATA = 19;
    private static final int SEGMENT_TYPE_PAGE_COMPOSITION = 16;
    private static final int SEGMENT_TYPE_REGION_COMPOSITION = 17;
    private static final String TAG = "DvbParser";
    private static final byte[] defaultMap2To4 = {0, 7, 8, 15};
    private static final byte[] defaultMap2To8 = {0, 119, -120, -1};
    private static final byte[] defaultMap4To8 = {0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1};
    private Bitmap bitmap;
    private final Canvas canvas;
    private final ClutDefinition defaultClutDefinition;
    private final DisplayDefinition defaultDisplayDefinition;
    private final Paint defaultPaint = new Paint();
    private final Paint fillRegionPaint;
    private final SubtitleService subtitleService;

    private static final class ClutDefinition {
        public final int[] clutEntries2Bit;
        public final int[] clutEntries4Bit;
        public final int[] clutEntries8Bit;
        public final int id;

        public ClutDefinition(int i, int[] iArr, int[] iArr2, int[] iArr3) {
            this.id = i;
            this.clutEntries2Bit = iArr;
            this.clutEntries4Bit = iArr2;
            this.clutEntries8Bit = iArr3;
        }
    }

    private static final class DisplayDefinition {
        public final int height;
        public final int horizontalPositionMaximum;
        public final int horizontalPositionMinimum;
        public final int verticalPositionMaximum;
        public final int verticalPositionMinimum;
        public final int width;

        public DisplayDefinition(int i, int i2, int i3, int i4, int i5, int i6) {
            this.width = i;
            this.height = i2;
            this.horizontalPositionMinimum = i3;
            this.horizontalPositionMaximum = i4;
            this.verticalPositionMinimum = i5;
            this.verticalPositionMaximum = i6;
        }
    }

    private static final class ObjectData {
        public final byte[] bottomFieldData;
        public final int id;
        public final boolean nonModifyingColorFlag;
        public final byte[] topFieldData;

        public ObjectData(int i, boolean z, byte[] bArr, byte[] bArr2) {
            this.id = i;
            this.nonModifyingColorFlag = z;
            this.topFieldData = bArr;
            this.bottomFieldData = bArr2;
        }
    }

    private static final class PageComposition {
        public final SparseArray<PageRegion> regions;
        public final int state;
        public final int timeOutSecs;
        public final int version;

        public PageComposition(int i, int i2, int i3, SparseArray<PageRegion> sparseArray) {
            this.timeOutSecs = i;
            this.version = i2;
            this.state = i3;
            this.regions = sparseArray;
        }
    }

    private static final class PageRegion {
        public final int horizontalAddress;
        public final int verticalAddress;

        public PageRegion(int i, int i2) {
            this.horizontalAddress = i;
            this.verticalAddress = i2;
        }
    }

    private static final class RegionComposition {
        public final int clutId;
        public final int depth;
        public final boolean fillFlag;
        public final int height;
        public final int id;
        public final int levelOfCompatibility;
        public final int pixelCode2Bit;
        public final int pixelCode4Bit;
        public final int pixelCode8Bit;
        public final SparseArray<RegionObject> regionObjects;
        public final int width;

        public RegionComposition(int i, boolean z, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, SparseArray<RegionObject> sparseArray) {
            this.id = i;
            this.fillFlag = z;
            this.width = i2;
            this.height = i3;
            this.levelOfCompatibility = i4;
            this.depth = i5;
            this.clutId = i6;
            this.pixelCode8Bit = i7;
            this.pixelCode4Bit = i8;
            this.pixelCode2Bit = i9;
            this.regionObjects = sparseArray;
        }

        public void mergeFrom(RegionComposition regionComposition) {
            if (regionComposition != null) {
                SparseArray<RegionObject> sparseArray = regionComposition.regionObjects;
                for (int i = 0; i < sparseArray.size(); i++) {
                    this.regionObjects.put(sparseArray.keyAt(i), sparseArray.valueAt(i));
                }
            }
        }
    }

    private static final class RegionObject {
        public final int backgroundPixelCode;
        public final int foregroundPixelCode;
        public final int horizontalPosition;
        public final int provider;
        public final int type;
        public final int verticalPosition;

        public RegionObject(int i, int i2, int i3, int i4, int i5, int i6) {
            this.type = i;
            this.provider = i2;
            this.horizontalPosition = i3;
            this.verticalPosition = i4;
            this.foregroundPixelCode = i5;
            this.backgroundPixelCode = i6;
        }
    }

    private static final class SubtitleService {
        public final SparseArray<ClutDefinition> ancillaryCluts = new SparseArray<>();
        public final SparseArray<ObjectData> ancillaryObjects = new SparseArray<>();
        public final int ancillaryPageId;
        public final SparseArray<ClutDefinition> cluts = new SparseArray<>();
        public DisplayDefinition displayDefinition;
        public final SparseArray<ObjectData> objects = new SparseArray<>();
        public PageComposition pageComposition;
        public final SparseArray<RegionComposition> regions = new SparseArray<>();
        public final int subtitlePageId;

        public SubtitleService(int i, int i2) {
            this.subtitlePageId = i;
            this.ancillaryPageId = i2;
        }

        public void reset() {
            this.regions.clear();
            this.cluts.clear();
            this.objects.clear();
            this.ancillaryCluts.clear();
            this.ancillaryObjects.clear();
            this.displayDefinition = null;
            this.pageComposition = null;
        }
    }

    public DvbParser(int i, int i2) {
        this.defaultPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.defaultPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        this.defaultPaint.setPathEffect((PathEffect) null);
        this.fillRegionPaint = new Paint();
        this.fillRegionPaint.setStyle(Paint.Style.FILL);
        this.fillRegionPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        this.fillRegionPaint.setPathEffect((PathEffect) null);
        this.canvas = new Canvas();
        this.defaultDisplayDefinition = new DisplayDefinition(719, 575, 0, 719, 0, 575);
        this.defaultClutDefinition = new ClutDefinition(0, generateDefault2BitClutEntries(), generateDefault4BitClutEntries(), generateDefault8BitClutEntries());
        this.subtitleService = new SubtitleService(i, i2);
    }

    private static byte[] buildClutMapTable(int i, int i2, ParsableBitArray parsableBitArray) {
        byte[] bArr = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr[i3] = (byte) parsableBitArray.readBits(i2);
        }
        return bArr;
    }

    private static int[] generateDefault2BitClutEntries() {
        return new int[]{0, -1, -16777216, -8421505};
    }

    private static int[] generateDefault4BitClutEntries() {
        int[] iArr = new int[16];
        iArr[0] = 0;
        for (int i = 1; i < iArr.length; i++) {
            if (i < 8) {
                iArr[i] = getColor(255, (i & 1) != 0 ? 255 : 0, (i & 2) != 0 ? 255 : 0, (i & 4) != 0 ? 255 : 0);
            } else {
                iArr[i] = getColor(255, (i & 1) != 0 ? 127 : 0, (i & 2) != 0 ? 127 : 0, (i & 4) != 0 ? 127 : 0);
            }
        }
        return iArr;
    }

    private static int[] generateDefault8BitClutEntries() {
        int[] iArr = new int[256];
        iArr[0] = 0;
        for (int i = 0; i < iArr.length; i++) {
            if (i >= 8) {
                switch (i & 136) {
                    case 0:
                        iArr[i] = getColor(255, ((i & 1) != 0 ? 85 : 0) + ((i & 16) != 0 ? 170 : 0), ((i & 2) != 0 ? 85 : 0) + ((i & 32) != 0 ? 170 : 0), ((i & 64) != 0 ? 170 : 0) + ((i & 4) != 0 ? 85 : 0));
                        break;
                    case 8:
                        iArr[i] = getColor(127, ((i & 1) != 0 ? 85 : 0) + ((i & 16) != 0 ? 170 : 0), ((i & 2) != 0 ? 85 : 0) + ((i & 32) != 0 ? 170 : 0), ((i & 64) != 0 ? 170 : 0) + ((i & 4) != 0 ? 85 : 0));
                        break;
                    case 128:
                        iArr[i] = getColor(255, ((i & 1) != 0 ? 43 : 0) + 127 + ((i & 16) != 0 ? 85 : 0), ((i & 2) != 0 ? 43 : 0) + 127 + ((i & 32) != 0 ? 85 : 0), ((i & 64) != 0 ? 85 : 0) + ((i & 4) != 0 ? 43 : 0) + 127);
                        break;
                    case 136:
                        iArr[i] = getColor(255, ((i & 1) != 0 ? 43 : 0) + ((i & 16) != 0 ? 85 : 0), ((i & 2) != 0 ? 43 : 0) + ((i & 32) != 0 ? 85 : 0), ((i & 64) != 0 ? 85 : 0) + ((i & 4) != 0 ? 43 : 0));
                        break;
                }
            } else {
                iArr[i] = getColor(63, (i & 1) != 0 ? 255 : 0, (i & 2) != 0 ? 255 : 0, (i & 4) != 0 ? 255 : 0);
            }
        }
        return iArr;
    }

    private static int getColor(int i, int i2, int i3, int i4) {
        return (i << 24) | (i2 << 16) | (i3 << 8) | i4;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int paint2BitPixelCodeString(com.google.android.exoplayer2.util.ParsableBitArray r10, int[] r11, byte[] r12, int r13, int r14, android.graphics.Paint r15, android.graphics.Canvas r16) {
        /*
            r7 = 0
        L_0x0001:
            r9 = 0
            r6 = 0
            r0 = 2
            int r8 = r10.readBits(r0)
            if (r8 == 0) goto L_0x002b
            r9 = 1
            r6 = r8
        L_0x000c:
            if (r9 == 0) goto L_0x0027
            if (r15 == 0) goto L_0x0027
            if (r12 == 0) goto L_0x0014
            byte r6 = r12[r6]
        L_0x0014:
            r0 = r11[r6]
            r15.setColor(r0)
            float r1 = (float) r13
            float r2 = (float) r14
            int r0 = r13 + r9
            float r3 = (float) r0
            int r0 = r14 + 1
            float r4 = (float) r0
            r0 = r16
            r5 = r15
            r0.drawRect(r1, r2, r3, r4, r5)
        L_0x0027:
            int r13 = r13 + r9
            if (r7 == 0) goto L_0x0001
            return r13
        L_0x002b:
            boolean r0 = r10.readBit()
            if (r0 == 0) goto L_0x003e
            r0 = 3
            int r0 = r10.readBits(r0)
            int r9 = r0 + 3
            r0 = 2
            int r6 = r10.readBits(r0)
            goto L_0x000c
        L_0x003e:
            boolean r0 = r10.readBit()
            if (r0 == 0) goto L_0x0046
            r9 = 1
            goto L_0x000c
        L_0x0046:
            r0 = 2
            int r0 = r10.readBits(r0)
            switch(r0) {
                case 0: goto L_0x004f;
                case 1: goto L_0x0051;
                case 2: goto L_0x0053;
                case 3: goto L_0x0060;
                default: goto L_0x004e;
            }
        L_0x004e:
            goto L_0x000c
        L_0x004f:
            r7 = 1
            goto L_0x000c
        L_0x0051:
            r9 = 2
            goto L_0x000c
        L_0x0053:
            r0 = 4
            int r0 = r10.readBits(r0)
            int r9 = r0 + 12
            r0 = 2
            int r6 = r10.readBits(r0)
            goto L_0x000c
        L_0x0060:
            r0 = 8
            int r0 = r10.readBits(r0)
            int r9 = r0 + 29
            r0 = 2
            int r6 = r10.readBits(r0)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.dvb.DvbParser.paint2BitPixelCodeString(com.google.android.exoplayer2.util.ParsableBitArray, int[], byte[], int, int, android.graphics.Paint, android.graphics.Canvas):int");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int paint4BitPixelCodeString(com.google.android.exoplayer2.util.ParsableBitArray r10, int[] r11, byte[] r12, int r13, int r14, android.graphics.Paint r15, android.graphics.Canvas r16) {
        /*
            r7 = 0
        L_0x0001:
            r9 = 0
            r6 = 0
            r0 = 4
            int r8 = r10.readBits(r0)
            if (r8 == 0) goto L_0x002b
            r9 = 1
            r6 = r8
        L_0x000c:
            if (r9 == 0) goto L_0x0027
            if (r15 == 0) goto L_0x0027
            if (r12 == 0) goto L_0x0014
            byte r6 = r12[r6]
        L_0x0014:
            r0 = r11[r6]
            r15.setColor(r0)
            float r1 = (float) r13
            float r2 = (float) r14
            int r0 = r13 + r9
            float r3 = (float) r0
            int r0 = r14 + 1
            float r4 = (float) r0
            r0 = r16
            r5 = r15
            r0.drawRect(r1, r2, r3, r4, r5)
        L_0x0027:
            int r13 = r13 + r9
            if (r7 == 0) goto L_0x0001
            return r13
        L_0x002b:
            boolean r0 = r10.readBit()
            if (r0 != 0) goto L_0x003e
            r0 = 3
            int r8 = r10.readBits(r0)
            if (r8 == 0) goto L_0x003c
            int r9 = r8 + 2
            r6 = 0
            goto L_0x000c
        L_0x003c:
            r7 = 1
            goto L_0x000c
        L_0x003e:
            boolean r0 = r10.readBit()
            if (r0 != 0) goto L_0x0051
            r0 = 2
            int r0 = r10.readBits(r0)
            int r9 = r0 + 4
            r0 = 4
            int r6 = r10.readBits(r0)
            goto L_0x000c
        L_0x0051:
            r0 = 2
            int r0 = r10.readBits(r0)
            switch(r0) {
                case 0: goto L_0x005a;
                case 1: goto L_0x005c;
                case 2: goto L_0x005e;
                case 3: goto L_0x006b;
                default: goto L_0x0059;
            }
        L_0x0059:
            goto L_0x000c
        L_0x005a:
            r9 = 1
            goto L_0x000c
        L_0x005c:
            r9 = 2
            goto L_0x000c
        L_0x005e:
            r0 = 4
            int r0 = r10.readBits(r0)
            int r9 = r0 + 9
            r0 = 4
            int r6 = r10.readBits(r0)
            goto L_0x000c
        L_0x006b:
            r0 = 8
            int r0 = r10.readBits(r0)
            int r9 = r0 + 25
            r0 = 4
            int r6 = r10.readBits(r0)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.dvb.DvbParser.paint4BitPixelCodeString(com.google.android.exoplayer2.util.ParsableBitArray, int[], byte[], int, int, android.graphics.Paint, android.graphics.Canvas):int");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int paint8BitPixelCodeString(com.google.android.exoplayer2.util.ParsableBitArray r10, int[] r11, byte[] r12, int r13, int r14, android.graphics.Paint r15, android.graphics.Canvas r16) {
        /*
            r7 = 0
        L_0x0001:
            r9 = 0
            r6 = 0
            r0 = 8
            int r8 = r10.readBits(r0)
            if (r8 == 0) goto L_0x002c
            r9 = 1
            r6 = r8
        L_0x000d:
            if (r9 == 0) goto L_0x0028
            if (r15 == 0) goto L_0x0028
            if (r12 == 0) goto L_0x0015
            byte r6 = r12[r6]
        L_0x0015:
            r0 = r11[r6]
            r15.setColor(r0)
            float r1 = (float) r13
            float r2 = (float) r14
            int r0 = r13 + r9
            float r3 = (float) r0
            int r0 = r14 + 1
            float r4 = (float) r0
            r0 = r16
            r5 = r15
            r0.drawRect(r1, r2, r3, r4, r5)
        L_0x0028:
            int r13 = r13 + r9
            if (r7 == 0) goto L_0x0001
            return r13
        L_0x002c:
            boolean r0 = r10.readBit()
            if (r0 != 0) goto L_0x003e
            r0 = 7
            int r8 = r10.readBits(r0)
            if (r8 == 0) goto L_0x003c
            r9 = r8
            r6 = 0
            goto L_0x000d
        L_0x003c:
            r7 = 1
            goto L_0x000d
        L_0x003e:
            r0 = 7
            int r9 = r10.readBits(r0)
            r0 = 8
            int r6 = r10.readBits(r0)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.dvb.DvbParser.paint8BitPixelCodeString(com.google.android.exoplayer2.util.ParsableBitArray, int[], byte[], int, int, android.graphics.Paint, android.graphics.Canvas):int");
    }

    private static void paintPixelDataSubBlock(byte[] bArr, int[] iArr, int i, int i2, int i3, Paint paint, Canvas canvas2) {
        ParsableBitArray parsableBitArray = new ParsableBitArray(bArr);
        int i4 = i2;
        int i5 = i3;
        byte[] bArr2 = null;
        byte[] bArr3 = null;
        while (parsableBitArray.bitsLeft() != 0) {
            switch (parsableBitArray.readBits(8)) {
                case 16:
                    i4 = paint2BitPixelCodeString(parsableBitArray, iArr, i == 3 ? bArr3 == null ? defaultMap2To8 : bArr3 : i == 2 ? bArr2 == null ? defaultMap2To4 : bArr2 : null, i4, i5, paint, canvas2);
                    parsableBitArray.byteAlign();
                    break;
                case 17:
                    i4 = paint4BitPixelCodeString(parsableBitArray, iArr, i == 3 ? 0 == 0 ? defaultMap4To8 : null : null, i4, i5, paint, canvas2);
                    parsableBitArray.byteAlign();
                    break;
                case 18:
                    i4 = paint8BitPixelCodeString(parsableBitArray, iArr, (byte[]) null, i4, i5, paint, canvas2);
                    break;
                case 32:
                    bArr2 = buildClutMapTable(4, 4, parsableBitArray);
                    break;
                case 33:
                    bArr3 = buildClutMapTable(4, 8, parsableBitArray);
                    break;
                case 34:
                    bArr3 = buildClutMapTable(16, 8, parsableBitArray);
                    break;
                case 240:
                    i4 = i2;
                    i5 += 2;
                    break;
            }
        }
    }

    private static void paintPixelDataSubBlocks(ObjectData objectData, ClutDefinition clutDefinition, int i, int i2, int i3, Paint paint, Canvas canvas2) {
        int[] iArr = i == 3 ? clutDefinition.clutEntries8Bit : i == 2 ? clutDefinition.clutEntries4Bit : clutDefinition.clutEntries2Bit;
        paintPixelDataSubBlock(objectData.topFieldData, iArr, i, i2, i3, paint, canvas2);
        paintPixelDataSubBlock(objectData.bottomFieldData, iArr, i, i2, i3 + 1, paint, canvas2);
    }

    private static ClutDefinition parseClutDefinition(ParsableBitArray parsableBitArray, int i) {
        int readBits;
        int readBits2;
        int readBits3;
        int readBits4;
        int readBits5 = parsableBitArray.readBits(8);
        parsableBitArray.skipBits(8);
        int i2 = i - 2;
        int[] generateDefault2BitClutEntries = generateDefault2BitClutEntries();
        int[] generateDefault4BitClutEntries = generateDefault4BitClutEntries();
        int[] generateDefault8BitClutEntries = generateDefault8BitClutEntries();
        while (i2 > 0) {
            int readBits6 = parsableBitArray.readBits(8);
            int readBits7 = parsableBitArray.readBits(8);
            int i3 = i2 - 2;
            int[] iArr = (readBits7 & 128) != 0 ? generateDefault2BitClutEntries : (readBits7 & 64) != 0 ? generateDefault4BitClutEntries : generateDefault8BitClutEntries;
            if ((readBits7 & 1) != 0) {
                readBits = parsableBitArray.readBits(8);
                readBits2 = parsableBitArray.readBits(8);
                readBits3 = parsableBitArray.readBits(8);
                readBits4 = parsableBitArray.readBits(8);
                i2 = i3 - 4;
            } else {
                readBits = parsableBitArray.readBits(6) << 2;
                readBits2 = parsableBitArray.readBits(4) << 4;
                readBits3 = parsableBitArray.readBits(4) << 4;
                readBits4 = parsableBitArray.readBits(2) << 6;
                i2 = i3 - 2;
            }
            if (readBits == 0) {
                readBits2 = 0;
                readBits3 = 0;
                readBits4 = 255;
            }
            iArr[readBits6] = getColor((byte) (255 - (readBits4 & 255)), Util.constrainValue((int) (((double) readBits) + (1.402d * ((double) (readBits2 - 128)))), 0, 255), Util.constrainValue((int) ((((double) readBits) - (0.34414d * ((double) (readBits3 - 128)))) - (0.71414d * ((double) (readBits2 - 128)))), 0, 255), Util.constrainValue((int) (((double) readBits) + (1.772d * ((double) (readBits3 - 128)))), 0, 255));
        }
        return new ClutDefinition(readBits5, generateDefault2BitClutEntries, generateDefault4BitClutEntries, generateDefault8BitClutEntries);
    }

    private static DisplayDefinition parseDisplayDefinition(ParsableBitArray parsableBitArray) {
        int i;
        int i2;
        int i3;
        int i4;
        parsableBitArray.skipBits(4);
        boolean readBit = parsableBitArray.readBit();
        parsableBitArray.skipBits(3);
        int readBits = parsableBitArray.readBits(16);
        int readBits2 = parsableBitArray.readBits(16);
        if (readBit) {
            i = parsableBitArray.readBits(16);
            i2 = parsableBitArray.readBits(16);
            i3 = parsableBitArray.readBits(16);
            i4 = parsableBitArray.readBits(16);
        } else {
            i = 0;
            i2 = readBits;
            i3 = 0;
            i4 = readBits2;
        }
        return new DisplayDefinition(readBits, readBits2, i, i2, i3, i4);
    }

    private static ObjectData parseObjectData(ParsableBitArray parsableBitArray) {
        int readBits = parsableBitArray.readBits(16);
        parsableBitArray.skipBits(4);
        int readBits2 = parsableBitArray.readBits(2);
        boolean readBit = parsableBitArray.readBit();
        parsableBitArray.skipBits(1);
        byte[] bArr = null;
        byte[] bArr2 = null;
        if (readBits2 == 1) {
            parsableBitArray.skipBits(parsableBitArray.readBits(8) * 16);
        } else if (readBits2 == 0) {
            int readBits3 = parsableBitArray.readBits(16);
            int readBits4 = parsableBitArray.readBits(16);
            if (readBits3 > 0) {
                bArr = new byte[readBits3];
                parsableBitArray.readBytes(bArr, 0, readBits3);
            }
            if (readBits4 > 0) {
                bArr2 = new byte[readBits4];
                parsableBitArray.readBytes(bArr2, 0, readBits4);
            } else {
                bArr2 = bArr;
            }
        }
        return new ObjectData(readBits, readBit, bArr, bArr2);
    }

    private static PageComposition parsePageComposition(ParsableBitArray parsableBitArray, int i) {
        int readBits = parsableBitArray.readBits(8);
        int readBits2 = parsableBitArray.readBits(4);
        int readBits3 = parsableBitArray.readBits(2);
        parsableBitArray.skipBits(2);
        int i2 = i - 2;
        SparseArray sparseArray = new SparseArray();
        while (i2 > 0) {
            int readBits4 = parsableBitArray.readBits(8);
            parsableBitArray.skipBits(8);
            i2 -= 6;
            sparseArray.put(readBits4, new PageRegion(parsableBitArray.readBits(16), parsableBitArray.readBits(16)));
        }
        return new PageComposition(readBits, readBits2, readBits3, sparseArray);
    }

    private static RegionComposition parseRegionComposition(ParsableBitArray parsableBitArray, int i) {
        int readBits = parsableBitArray.readBits(8);
        parsableBitArray.skipBits(4);
        boolean readBit = parsableBitArray.readBit();
        parsableBitArray.skipBits(3);
        int readBits2 = parsableBitArray.readBits(16);
        int readBits3 = parsableBitArray.readBits(16);
        int readBits4 = parsableBitArray.readBits(3);
        int readBits5 = parsableBitArray.readBits(3);
        parsableBitArray.skipBits(2);
        int readBits6 = parsableBitArray.readBits(8);
        int readBits7 = parsableBitArray.readBits(8);
        int readBits8 = parsableBitArray.readBits(4);
        int readBits9 = parsableBitArray.readBits(2);
        parsableBitArray.skipBits(2);
        int i2 = i - 10;
        SparseArray sparseArray = new SparseArray();
        while (i2 > 0) {
            int readBits10 = parsableBitArray.readBits(16);
            int readBits11 = parsableBitArray.readBits(2);
            int readBits12 = parsableBitArray.readBits(2);
            int readBits13 = parsableBitArray.readBits(12);
            parsableBitArray.skipBits(4);
            int readBits14 = parsableBitArray.readBits(12);
            i2 -= 6;
            int i3 = 0;
            int i4 = 0;
            if (readBits11 == 1 || readBits11 == 2) {
                i3 = parsableBitArray.readBits(8);
                i4 = parsableBitArray.readBits(8);
                i2 -= 2;
            }
            sparseArray.put(readBits10, new RegionObject(readBits11, readBits12, readBits13, readBits14, i3, i4));
        }
        return new RegionComposition(readBits, readBit, readBits2, readBits3, readBits4, readBits5, readBits6, readBits7, readBits8, readBits9, sparseArray);
    }

    private static void parseSubtitlingSegment(ParsableBitArray parsableBitArray, SubtitleService subtitleService2) {
        int readBits = parsableBitArray.readBits(8);
        int readBits2 = parsableBitArray.readBits(16);
        int readBits3 = parsableBitArray.readBits(16);
        int bytePosition = parsableBitArray.getBytePosition() + readBits3;
        if (readBits3 * 8 > parsableBitArray.bitsLeft()) {
            Log.w(TAG, "Data field length exceeds limit");
            parsableBitArray.skipBits(parsableBitArray.bitsLeft());
            return;
        }
        switch (readBits) {
            case 16:
                if (readBits2 == subtitleService2.subtitlePageId) {
                    PageComposition pageComposition = subtitleService2.pageComposition;
                    PageComposition parsePageComposition = parsePageComposition(parsableBitArray, readBits3);
                    if (parsePageComposition.state == 0) {
                        if (!(pageComposition == null || pageComposition.version == parsePageComposition.version)) {
                            subtitleService2.pageComposition = parsePageComposition;
                            break;
                        }
                    } else {
                        subtitleService2.pageComposition = parsePageComposition;
                        subtitleService2.regions.clear();
                        subtitleService2.cluts.clear();
                        subtitleService2.objects.clear();
                        break;
                    }
                }
                break;
            case 17:
                PageComposition pageComposition2 = subtitleService2.pageComposition;
                if (readBits2 == subtitleService2.subtitlePageId && pageComposition2 != null) {
                    RegionComposition parseRegionComposition = parseRegionComposition(parsableBitArray, readBits3);
                    if (pageComposition2.state == 0) {
                        parseRegionComposition.mergeFrom(subtitleService2.regions.get(parseRegionComposition.id));
                    }
                    subtitleService2.regions.put(parseRegionComposition.id, parseRegionComposition);
                    break;
                }
            case 18:
                if (readBits2 != subtitleService2.subtitlePageId) {
                    if (readBits2 == subtitleService2.ancillaryPageId) {
                        ClutDefinition parseClutDefinition = parseClutDefinition(parsableBitArray, readBits3);
                        subtitleService2.ancillaryCluts.put(parseClutDefinition.id, parseClutDefinition);
                        break;
                    }
                } else {
                    ClutDefinition parseClutDefinition2 = parseClutDefinition(parsableBitArray, readBits3);
                    subtitleService2.cluts.put(parseClutDefinition2.id, parseClutDefinition2);
                    break;
                }
                break;
            case 19:
                if (readBits2 != subtitleService2.subtitlePageId) {
                    if (readBits2 == subtitleService2.ancillaryPageId) {
                        ObjectData parseObjectData = parseObjectData(parsableBitArray);
                        subtitleService2.ancillaryObjects.put(parseObjectData.id, parseObjectData);
                        break;
                    }
                } else {
                    ObjectData parseObjectData2 = parseObjectData(parsableBitArray);
                    subtitleService2.objects.put(parseObjectData2.id, parseObjectData2);
                    break;
                }
                break;
            case 20:
                if (readBits2 == subtitleService2.subtitlePageId) {
                    subtitleService2.displayDefinition = parseDisplayDefinition(parsableBitArray);
                    break;
                }
                break;
        }
        parsableBitArray.skipBytes(bytePosition - parsableBitArray.getBytePosition());
    }

    public List<Cue> decode(byte[] bArr, int i) {
        ParsableBitArray parsableBitArray = new ParsableBitArray(bArr, i);
        while (parsableBitArray.bitsLeft() >= 48 && parsableBitArray.readBits(8) == 15) {
            parseSubtitlingSegment(parsableBitArray, this.subtitleService);
        }
        if (this.subtitleService.pageComposition == null) {
            return Collections.emptyList();
        }
        DisplayDefinition displayDefinition = this.subtitleService.displayDefinition != null ? this.subtitleService.displayDefinition : this.defaultDisplayDefinition;
        if (!(this.bitmap != null && displayDefinition.width + 1 == this.bitmap.getWidth() && displayDefinition.height + 1 == this.bitmap.getHeight())) {
            this.bitmap = Bitmap.createBitmap(displayDefinition.width + 1, displayDefinition.height + 1, Bitmap.Config.ARGB_8888);
            this.canvas.setBitmap(this.bitmap);
        }
        ArrayList arrayList = new ArrayList();
        SparseArray<PageRegion> sparseArray = this.subtitleService.pageComposition.regions;
        for (int i2 = 0; i2 < sparseArray.size(); i2++) {
            PageRegion valueAt = sparseArray.valueAt(i2);
            RegionComposition regionComposition = this.subtitleService.regions.get(sparseArray.keyAt(i2));
            int i3 = valueAt.horizontalAddress + displayDefinition.horizontalPositionMinimum;
            int i4 = valueAt.verticalAddress + displayDefinition.verticalPositionMinimum;
            this.canvas.clipRect((float) i3, (float) i4, (float) Math.min(regionComposition.width + i3, displayDefinition.horizontalPositionMaximum), (float) Math.min(regionComposition.height + i4, displayDefinition.verticalPositionMaximum), Region.Op.REPLACE);
            ClutDefinition clutDefinition = this.subtitleService.cluts.get(regionComposition.clutId);
            if (clutDefinition == null && (clutDefinition = this.subtitleService.ancillaryCluts.get(regionComposition.clutId)) == null) {
                clutDefinition = this.defaultClutDefinition;
            }
            SparseArray<RegionObject> sparseArray2 = regionComposition.regionObjects;
            for (int i5 = 0; i5 < sparseArray2.size(); i5++) {
                int keyAt = sparseArray2.keyAt(i5);
                RegionObject valueAt2 = sparseArray2.valueAt(i5);
                ObjectData objectData = this.subtitleService.objects.get(keyAt);
                if (objectData == null) {
                    objectData = this.subtitleService.ancillaryObjects.get(keyAt);
                }
                if (objectData != null) {
                    paintPixelDataSubBlocks(objectData, clutDefinition, regionComposition.depth, valueAt2.horizontalPosition + i3, valueAt2.verticalPosition + i4, objectData.nonModifyingColorFlag ? null : this.defaultPaint, this.canvas);
                }
            }
            if (regionComposition.fillFlag) {
                this.fillRegionPaint.setColor(regionComposition.depth == 3 ? clutDefinition.clutEntries8Bit[regionComposition.pixelCode8Bit] : regionComposition.depth == 2 ? clutDefinition.clutEntries4Bit[regionComposition.pixelCode4Bit] : clutDefinition.clutEntries2Bit[regionComposition.pixelCode2Bit]);
                this.canvas.drawRect((float) i3, (float) i4, (float) (regionComposition.width + i3), (float) (regionComposition.height + i4), this.fillRegionPaint);
            }
            arrayList.add(new Cue(Bitmap.createBitmap(this.bitmap, i3, i4, regionComposition.width, regionComposition.height), ((float) i3) / ((float) displayDefinition.width), 0, ((float) i4) / ((float) displayDefinition.height), 0, ((float) regionComposition.width) / ((float) displayDefinition.width), ((float) regionComposition.height) / ((float) displayDefinition.height)));
            this.canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        }
        return arrayList;
    }

    public void reset() {
        this.subtitleService.reset();
    }
}
