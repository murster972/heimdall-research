package com.google.android.exoplayer2.text.ssa;

import android.util.Log;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.LongArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public final class SsaDecoder extends SimpleSubtitleDecoder {
    private static final String DIALOGUE_LINE_PREFIX = "Dialogue: ";
    private static final String FORMAT_LINE_PREFIX = "Format: ";
    private static final Pattern SSA_TIMECODE_PATTERN = Pattern.compile("(?:(\\d+):)?(\\d+):(\\d+)(?::|\\.)(\\d+)");
    private static final String TAG = "SsaDecoder";
    private int formatEndIndex;
    private int formatKeyCount;
    private int formatStartIndex;
    private int formatTextIndex;
    private final boolean haveInitializationData;

    public SsaDecoder() {
        this((List<byte[]>) null);
    }

    public SsaDecoder(List<byte[]> list) {
        super(TAG);
        if (list != null) {
            this.haveInitializationData = true;
            String str = new String(list.get(0));
            Assertions.checkArgument(str.startsWith(FORMAT_LINE_PREFIX));
            parseFormatLine(str);
            parseHeader(new ParsableByteArray(list.get(1)));
            return;
        }
        this.haveInitializationData = false;
    }

    private void parseDialogueLine(String str, List<Cue> list, LongArray longArray) {
        if (this.formatKeyCount == 0) {
            Log.w(TAG, "Skipping dialogue line before format: " + str);
            return;
        }
        String[] split = str.substring(DIALOGUE_LINE_PREFIX.length()).split(",", this.formatKeyCount);
        long parseTimecodeUs = parseTimecodeUs(split[this.formatStartIndex]);
        if (parseTimecodeUs == C.TIME_UNSET) {
            Log.w(TAG, "Skipping invalid timing: " + str);
            return;
        }
        long j = C.TIME_UNSET;
        String str2 = split[this.formatEndIndex];
        if (!str2.trim().isEmpty()) {
            j = parseTimecodeUs(str2);
            if (j == C.TIME_UNSET) {
                Log.w(TAG, "Skipping invalid timing: " + str);
                return;
            }
        }
        list.add(new Cue(split[this.formatTextIndex].replaceAll("\\{.*?\\}", "").replaceAll("\\\\N", StringUtils.LF).replaceAll("\\\\n", StringUtils.LF)));
        longArray.add(parseTimecodeUs);
        if (j != C.TIME_UNSET) {
            list.add((Object) null);
            longArray.add(j);
        }
    }

    private void parseEventBody(ParsableByteArray parsableByteArray, List<Cue> list, LongArray longArray) {
        while (true) {
            String readLine = parsableByteArray.readLine();
            if (readLine == null) {
                return;
            }
            if (!this.haveInitializationData && readLine.startsWith(FORMAT_LINE_PREFIX)) {
                parseFormatLine(readLine);
            } else if (readLine.startsWith(DIALOGUE_LINE_PREFIX)) {
                parseDialogueLine(readLine, list, longArray);
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void parseFormatLine(java.lang.String r7) {
        /*
            r6 = this;
            r4 = -1
            java.lang.String r3 = "Format: "
            int r3 = r3.length()
            java.lang.String r3 = r7.substring(r3)
            java.lang.String r5 = ","
            java.lang.String[] r2 = android.text.TextUtils.split(r3, r5)
            int r3 = r2.length
            r6.formatKeyCount = r3
            r6.formatStartIndex = r4
            r6.formatEndIndex = r4
            r6.formatTextIndex = r4
            r0 = 0
        L_0x001d:
            int r3 = r6.formatKeyCount
            if (r0 >= r3) goto L_0x0063
            r3 = r2[r0]
            java.lang.String r3 = r3.trim()
            java.lang.String r1 = com.google.android.exoplayer2.util.Util.toLowerInvariant(r3)
            int r3 = r1.hashCode()
            switch(r3) {
                case 100571: goto L_0x0044;
                case 3556653: goto L_0x004f;
                case 109757538: goto L_0x0039;
                default: goto L_0x0032;
            }
        L_0x0032:
            r3 = r4
        L_0x0033:
            switch(r3) {
                case 0: goto L_0x005a;
                case 1: goto L_0x005d;
                case 2: goto L_0x0060;
                default: goto L_0x0036;
            }
        L_0x0036:
            int r0 = r0 + 1
            goto L_0x001d
        L_0x0039:
            java.lang.String r3 = "start"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0032
            r3 = 0
            goto L_0x0033
        L_0x0044:
            java.lang.String r3 = "end"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0032
            r3 = 1
            goto L_0x0033
        L_0x004f:
            java.lang.String r3 = "text"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0032
            r3 = 2
            goto L_0x0033
        L_0x005a:
            r6.formatStartIndex = r0
            goto L_0x0036
        L_0x005d:
            r6.formatEndIndex = r0
            goto L_0x0036
        L_0x0060:
            r6.formatTextIndex = r0
            goto L_0x0036
        L_0x0063:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ssa.SsaDecoder.parseFormatLine(java.lang.String):void");
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    private void parseHeader(com.google.android.exoplayer2.util.ParsableByteArray r3) {
        /*
            r2 = this;
        L_0x0000:
            java.lang.String r0 = r3.readLine()
            if (r0 == 0) goto L_0x000f
            java.lang.String r1 = "[Events]"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x0000
        L_0x000f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ssa.SsaDecoder.parseHeader(com.google.android.exoplayer2.util.ParsableByteArray):void");
    }

    public static long parseTimecodeUs(String str) {
        Matcher matcher = SSA_TIMECODE_PATTERN.matcher(str);
        return !matcher.matches() ? C.TIME_UNSET : (Long.parseLong(matcher.group(1)) * 60 * 60 * C.MICROS_PER_SECOND) + (Long.parseLong(matcher.group(2)) * 60 * C.MICROS_PER_SECOND) + (Long.parseLong(matcher.group(3)) * C.MICROS_PER_SECOND) + (Long.parseLong(matcher.group(4)) * 10000);
    }

    /* access modifiers changed from: protected */
    public SsaSubtitle decode(byte[] bArr, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        LongArray longArray = new LongArray();
        ParsableByteArray parsableByteArray = new ParsableByteArray(bArr, i);
        if (!this.haveInitializationData) {
            parseHeader(parsableByteArray);
        }
        parseEventBody(parsableByteArray, arrayList, longArray);
        Cue[] cueArr = new Cue[arrayList.size()];
        arrayList.toArray(cueArr);
        return new SsaSubtitle(cueArr, longArray.toArray());
    }
}
