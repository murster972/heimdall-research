package com.google.android.exoplayer2.source;

import android.net.Uri;
import android.os.Handler;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.SampleQueue;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ConditionVariable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

final class ExtractorMediaPeriod implements ExtractorOutput, MediaPeriod, SampleQueue.UpstreamFormatChangedListener, Loader.Callback<ExtractingLoadable>, Loader.ReleaseCallback {
    private static final long DEFAULT_LAST_SAMPLE_DURATION_US = 10000;
    private int actualMinLoadableRetryCount;
    private final Allocator allocator;
    /* access modifiers changed from: private */
    public MediaPeriod.Callback callback;
    /* access modifiers changed from: private */
    public final long continueLoadingCheckIntervalBytes;
    /* access modifiers changed from: private */
    public final String customCacheKey;
    private final DataSource dataSource;
    private long durationUs;
    private int enabledTrackCount;
    private final Handler eventHandler;
    /* access modifiers changed from: private */
    public final ExtractorMediaSource.EventListener eventListener;
    private int extractedSamplesCountAtStartOfLoad;
    private final ExtractorHolder extractorHolder;
    /* access modifiers changed from: private */
    public final Handler handler;
    private boolean haveAudioVideoTracks;
    private long lastSeekPositionUs;
    private long length;
    private final Listener listener;
    private final ConditionVariable loadCondition;
    private final Loader loader = new Loader("Loader:ExtractorMediaPeriod");
    private boolean loadingFinished;
    private final Runnable maybeFinishPrepareRunnable;
    private final int minLoadableRetryCount;
    private boolean notifyDiscontinuity;
    /* access modifiers changed from: private */
    public final Runnable onContinueLoadingRequestedRunnable;
    private long pendingResetPositionUs;
    private boolean prepared;
    /* access modifiers changed from: private */
    public boolean released;
    private int[] sampleQueueTrackIds;
    private SampleQueue[] sampleQueues;
    private boolean sampleQueuesBuilt;
    private SeekMap seekMap;
    private boolean seenFirstTrackSelection;
    private boolean[] trackEnabledStates;
    private boolean[] trackIsAudioVideoFlags;
    private TrackGroupArray tracks;
    private final Uri uri;

    final class ExtractingLoadable implements Loader.Loadable {
        private final DataSource dataSource;
        private final ExtractorHolder extractorHolder;
        /* access modifiers changed from: private */
        public long length = -1;
        private volatile boolean loadCanceled;
        private final ConditionVariable loadCondition;
        private boolean pendingExtractorSeek = true;
        private final PositionHolder positionHolder = new PositionHolder();
        private long seekTimeUs;
        private final Uri uri;

        public ExtractingLoadable(Uri uri2, DataSource dataSource2, ExtractorHolder extractorHolder2, ConditionVariable conditionVariable) {
            this.uri = (Uri) Assertions.checkNotNull(uri2);
            this.dataSource = (DataSource) Assertions.checkNotNull(dataSource2);
            this.extractorHolder = (ExtractorHolder) Assertions.checkNotNull(extractorHolder2);
            this.loadCondition = conditionVariable;
        }

        public void cancelLoad() {
            this.loadCanceled = true;
        }

        public boolean isLoadCanceled() {
            return this.loadCanceled;
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x008e  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00ab  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void load() throws java.io.IOException, java.lang.InterruptedException {
            /*
                r12 = this;
                r9 = 0
            L_0x0001:
                if (r9 != 0) goto L_0x00b6
                boolean r1 = r12.loadCanceled
                if (r1 != 0) goto L_0x00b6
                r8 = 0
                com.google.android.exoplayer2.extractor.PositionHolder r1 = r12.positionHolder     // Catch:{ all -> 0x00b7 }
                long r2 = r1.position     // Catch:{ all -> 0x00b7 }
                com.google.android.exoplayer2.upstream.DataSource r10 = r12.dataSource     // Catch:{ all -> 0x00b7 }
                com.google.android.exoplayer2.upstream.DataSpec r0 = new com.google.android.exoplayer2.upstream.DataSpec     // Catch:{ all -> 0x00b7 }
                android.net.Uri r1 = r12.uri     // Catch:{ all -> 0x00b7 }
                r4 = -1
                com.google.android.exoplayer2.source.ExtractorMediaPeriod r6 = com.google.android.exoplayer2.source.ExtractorMediaPeriod.this     // Catch:{ all -> 0x00b7 }
                java.lang.String r6 = r6.customCacheKey     // Catch:{ all -> 0x00b7 }
                r0.<init>(r1, r2, r4, r6)     // Catch:{ all -> 0x00b7 }
                long r4 = r10.open(r0)     // Catch:{ all -> 0x00b7 }
                r12.length = r4     // Catch:{ all -> 0x00b7 }
                long r4 = r12.length     // Catch:{ all -> 0x00b7 }
                r10 = -1
                int r1 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
                if (r1 == 0) goto L_0x0030
                long r4 = r12.length     // Catch:{ all -> 0x00b7 }
                long r4 = r4 + r2
                r12.length = r4     // Catch:{ all -> 0x00b7 }
            L_0x0030:
                com.google.android.exoplayer2.extractor.DefaultExtractorInput r0 = new com.google.android.exoplayer2.extractor.DefaultExtractorInput     // Catch:{ all -> 0x00b7 }
                com.google.android.exoplayer2.upstream.DataSource r1 = r12.dataSource     // Catch:{ all -> 0x00b7 }
                long r4 = r12.length     // Catch:{ all -> 0x00b7 }
                r0.<init>(r1, r2, r4)     // Catch:{ all -> 0x00b7 }
                com.google.android.exoplayer2.source.ExtractorMediaPeriod$ExtractorHolder r1 = r12.extractorHolder     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.upstream.DataSource r4 = r12.dataSource     // Catch:{ all -> 0x008a }
                android.net.Uri r4 = r4.getUri()     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.extractor.Extractor r7 = r1.selectExtractor(r0, r4)     // Catch:{ all -> 0x008a }
                boolean r1 = r12.pendingExtractorSeek     // Catch:{ all -> 0x008a }
                if (r1 == 0) goto L_0x0051
                long r4 = r12.seekTimeUs     // Catch:{ all -> 0x008a }
                r7.seek(r2, r4)     // Catch:{ all -> 0x008a }
                r1 = 0
                r12.pendingExtractorSeek = r1     // Catch:{ all -> 0x008a }
            L_0x0051:
                if (r9 != 0) goto L_0x0095
                boolean r1 = r12.loadCanceled     // Catch:{ all -> 0x008a }
                if (r1 != 0) goto L_0x0095
                com.google.android.exoplayer2.util.ConditionVariable r1 = r12.loadCondition     // Catch:{ all -> 0x008a }
                r1.block()     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.extractor.PositionHolder r1 = r12.positionHolder     // Catch:{ all -> 0x008a }
                int r9 = r7.read(r0, r1)     // Catch:{ all -> 0x008a }
                long r4 = r0.getPosition()     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.source.ExtractorMediaPeriod r1 = com.google.android.exoplayer2.source.ExtractorMediaPeriod.this     // Catch:{ all -> 0x008a }
                long r10 = r1.continueLoadingCheckIntervalBytes     // Catch:{ all -> 0x008a }
                long r10 = r10 + r2
                int r1 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
                if (r1 <= 0) goto L_0x0051
                long r2 = r0.getPosition()     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.util.ConditionVariable r1 = r12.loadCondition     // Catch:{ all -> 0x008a }
                r1.close()     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.source.ExtractorMediaPeriod r1 = com.google.android.exoplayer2.source.ExtractorMediaPeriod.this     // Catch:{ all -> 0x008a }
                android.os.Handler r1 = r1.handler     // Catch:{ all -> 0x008a }
                com.google.android.exoplayer2.source.ExtractorMediaPeriod r4 = com.google.android.exoplayer2.source.ExtractorMediaPeriod.this     // Catch:{ all -> 0x008a }
                java.lang.Runnable r4 = r4.onContinueLoadingRequestedRunnable     // Catch:{ all -> 0x008a }
                r1.post(r4)     // Catch:{ all -> 0x008a }
                goto L_0x0051
            L_0x008a:
                r1 = move-exception
            L_0x008b:
                r4 = 1
                if (r9 != r4) goto L_0x00ab
                r9 = 0
            L_0x008f:
                com.google.android.exoplayer2.upstream.DataSource r4 = r12.dataSource
                com.google.android.exoplayer2.util.Util.closeQuietly((com.google.android.exoplayer2.upstream.DataSource) r4)
                throw r1
            L_0x0095:
                r1 = 1
                if (r9 != r1) goto L_0x00a0
                r9 = 0
            L_0x0099:
                com.google.android.exoplayer2.upstream.DataSource r1 = r12.dataSource
                com.google.android.exoplayer2.util.Util.closeQuietly((com.google.android.exoplayer2.upstream.DataSource) r1)
                goto L_0x0001
            L_0x00a0:
                if (r0 == 0) goto L_0x0099
                com.google.android.exoplayer2.extractor.PositionHolder r1 = r12.positionHolder
                long r4 = r0.getPosition()
                r1.position = r4
                goto L_0x0099
            L_0x00ab:
                if (r0 == 0) goto L_0x008f
                com.google.android.exoplayer2.extractor.PositionHolder r4 = r12.positionHolder
                long r10 = r0.getPosition()
                r4.position = r10
                goto L_0x008f
            L_0x00b6:
                return
            L_0x00b7:
                r1 = move-exception
                r0 = r8
                goto L_0x008b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.ExtractorMediaPeriod.ExtractingLoadable.load():void");
        }

        public void setLoadPosition(long j, long j2) {
            this.positionHolder.position = j;
            this.seekTimeUs = j2;
            this.pendingExtractorSeek = true;
        }
    }

    private static final class ExtractorHolder {
        private Extractor extractor;
        private final ExtractorOutput extractorOutput;
        private final Extractor[] extractors;

        public ExtractorHolder(Extractor[] extractorArr, ExtractorOutput extractorOutput2) {
            this.extractors = extractorArr;
            this.extractorOutput = extractorOutput2;
        }

        public void release() {
            if (this.extractor != null) {
                this.extractor.release();
                this.extractor = null;
            }
        }

        public Extractor selectExtractor(ExtractorInput extractorInput, Uri uri) throws IOException, InterruptedException {
            if (this.extractor != null) {
                return this.extractor;
            }
            Extractor[] extractorArr = this.extractors;
            int length = extractorArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Extractor extractor2 = extractorArr[i];
                try {
                    if (extractor2.sniff(extractorInput)) {
                        this.extractor = extractor2;
                        extractorInput.resetPeekPosition();
                        break;
                    }
                    i++;
                } catch (EOFException e) {
                } finally {
                    extractorInput.resetPeekPosition();
                }
            }
            if (this.extractor == null) {
                throw new UnrecognizedInputFormatException("None of the available extractors (" + Util.getCommaDelimitedSimpleClassNames(this.extractors) + ") could read the stream.", uri);
            }
            this.extractor.init(this.extractorOutput);
            return this.extractor;
        }
    }

    interface Listener {
        void onSourceInfoRefreshed(long j, boolean z);
    }

    private final class SampleStreamImpl implements SampleStream {
        /* access modifiers changed from: private */
        public final int track;

        public SampleStreamImpl(int i) {
            this.track = i;
        }

        public boolean isReady() {
            return ExtractorMediaPeriod.this.isReady(this.track);
        }

        public void maybeThrowError() throws IOException {
            ExtractorMediaPeriod.this.maybeThrowError();
        }

        public int readData(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
            return ExtractorMediaPeriod.this.readData(this.track, formatHolder, decoderInputBuffer, z);
        }

        public void skipData(long j) {
            ExtractorMediaPeriod.this.skipData(this.track, j);
        }
    }

    public ExtractorMediaPeriod(Uri uri2, DataSource dataSource2, Extractor[] extractorArr, int i, Handler handler2, ExtractorMediaSource.EventListener eventListener2, Listener listener2, Allocator allocator2, String str, int i2) {
        this.uri = uri2;
        this.dataSource = dataSource2;
        this.minLoadableRetryCount = i;
        this.eventHandler = handler2;
        this.eventListener = eventListener2;
        this.listener = listener2;
        this.allocator = allocator2;
        this.customCacheKey = str;
        this.continueLoadingCheckIntervalBytes = (long) i2;
        this.extractorHolder = new ExtractorHolder(extractorArr, this);
        this.loadCondition = new ConditionVariable();
        this.maybeFinishPrepareRunnable = new Runnable() {
            public void run() {
                ExtractorMediaPeriod.this.maybeFinishPrepare();
            }
        };
        this.onContinueLoadingRequestedRunnable = new Runnable() {
            public void run() {
                if (!ExtractorMediaPeriod.this.released) {
                    ExtractorMediaPeriod.this.callback.onContinueLoadingRequested(ExtractorMediaPeriod.this);
                }
            }
        };
        this.handler = new Handler();
        this.sampleQueueTrackIds = new int[0];
        this.sampleQueues = new SampleQueue[0];
        this.pendingResetPositionUs = C.TIME_UNSET;
        this.length = -1;
        this.actualMinLoadableRetryCount = i == -1 ? 3 : i;
    }

    private void configureRetry(ExtractingLoadable extractingLoadable) {
        if (this.length != -1) {
            return;
        }
        if (this.seekMap == null || this.seekMap.getDurationUs() == C.TIME_UNSET) {
            this.lastSeekPositionUs = 0;
            this.notifyDiscontinuity = this.prepared;
            for (SampleQueue reset : this.sampleQueues) {
                reset.reset();
            }
            extractingLoadable.setLoadPosition(0, 0);
        }
    }

    private void copyLengthFromLoader(ExtractingLoadable extractingLoadable) {
        if (this.length == -1) {
            this.length = extractingLoadable.length;
        }
    }

    private int getExtractedSamplesCount() {
        int i = 0;
        for (SampleQueue writeIndex : this.sampleQueues) {
            i += writeIndex.getWriteIndex();
        }
        return i;
    }

    private long getLargestQueuedTimestampUs() {
        long j = Long.MIN_VALUE;
        for (SampleQueue largestQueuedTimestampUs : this.sampleQueues) {
            j = Math.max(j, largestQueuedTimestampUs.getLargestQueuedTimestampUs());
        }
        return j;
    }

    private boolean isLoadableExceptionFatal(IOException iOException) {
        return iOException instanceof UnrecognizedInputFormatException;
    }

    private boolean isPendingReset() {
        return this.pendingResetPositionUs != C.TIME_UNSET;
    }

    /* access modifiers changed from: private */
    public void maybeFinishPrepare() {
        if (!this.released && !this.prepared && this.seekMap != null && this.sampleQueuesBuilt) {
            SampleQueue[] sampleQueueArr = this.sampleQueues;
            int length2 = sampleQueueArr.length;
            int i = 0;
            while (i < length2) {
                if (sampleQueueArr[i].getUpstreamFormat() != null) {
                    i++;
                } else {
                    return;
                }
            }
            this.loadCondition.close();
            int length3 = this.sampleQueues.length;
            TrackGroup[] trackGroupArr = new TrackGroup[length3];
            this.trackIsAudioVideoFlags = new boolean[length3];
            this.trackEnabledStates = new boolean[length3];
            this.durationUs = this.seekMap.getDurationUs();
            for (int i2 = 0; i2 < length3; i2++) {
                Format upstreamFormat = this.sampleQueues[i2].getUpstreamFormat();
                trackGroupArr[i2] = new TrackGroup(upstreamFormat);
                String str = upstreamFormat.sampleMimeType;
                boolean z = MimeTypes.isVideo(str) || MimeTypes.isAudio(str);
                this.trackIsAudioVideoFlags[i2] = z;
                this.haveAudioVideoTracks |= z;
            }
            this.tracks = new TrackGroupArray(trackGroupArr);
            if (this.minLoadableRetryCount == -1 && this.length == -1 && this.seekMap.getDurationUs() == C.TIME_UNSET) {
                this.actualMinLoadableRetryCount = 6;
            }
            this.prepared = true;
            this.listener.onSourceInfoRefreshed(this.durationUs, this.seekMap.isSeekable());
            this.callback.onPrepared(this);
        }
    }

    private void notifyLoadError(final IOException iOException) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                public void run() {
                    ExtractorMediaPeriod.this.eventListener.onLoadError(iOException);
                }
            });
        }
    }

    private boolean seekInsideBufferUs(long j) {
        int length2 = this.sampleQueues.length;
        for (int i = 0; i < length2; i++) {
            SampleQueue sampleQueue = this.sampleQueues[i];
            sampleQueue.rewind();
            if (!sampleQueue.advanceTo(j, true, false) && (this.trackIsAudioVideoFlags[i] || !this.haveAudioVideoTracks)) {
                return false;
            }
            sampleQueue.discardToRead();
        }
        return true;
    }

    private void startLoading() {
        ExtractingLoadable extractingLoadable = new ExtractingLoadable(this.uri, this.dataSource, this.extractorHolder, this.loadCondition);
        if (this.prepared) {
            Assertions.checkState(isPendingReset());
            if (this.durationUs == C.TIME_UNSET || this.pendingResetPositionUs < this.durationUs) {
                extractingLoadable.setLoadPosition(this.seekMap.getPosition(this.pendingResetPositionUs), this.pendingResetPositionUs);
                this.pendingResetPositionUs = C.TIME_UNSET;
            } else {
                this.loadingFinished = true;
                this.pendingResetPositionUs = C.TIME_UNSET;
                return;
            }
        }
        this.extractedSamplesCountAtStartOfLoad = getExtractedSamplesCount();
        this.loader.startLoading(extractingLoadable, this, this.actualMinLoadableRetryCount);
    }

    public boolean continueLoading(long j) {
        if (this.loadingFinished || (this.prepared && this.enabledTrackCount == 0)) {
            return false;
        }
        boolean open = this.loadCondition.open();
        if (this.loader.isLoading()) {
            return open;
        }
        startLoading();
        return true;
    }

    public void discardBuffer(long j) {
        int length2 = this.sampleQueues.length;
        for (int i = 0; i < length2; i++) {
            this.sampleQueues[i].discardTo(j, false, this.trackEnabledStates[i]);
        }
    }

    public void endTracks() {
        this.sampleQueuesBuilt = true;
        this.handler.post(this.maybeFinishPrepareRunnable);
    }

    public long getBufferedPositionUs() {
        long j;
        if (this.loadingFinished) {
            return Long.MIN_VALUE;
        }
        if (isPendingReset()) {
            return this.pendingResetPositionUs;
        }
        if (this.haveAudioVideoTracks) {
            j = Long.MAX_VALUE;
            int length2 = this.sampleQueues.length;
            for (int i = 0; i < length2; i++) {
                if (this.trackIsAudioVideoFlags[i]) {
                    j = Math.min(j, this.sampleQueues[i].getLargestQueuedTimestampUs());
                }
            }
        } else {
            j = getLargestQueuedTimestampUs();
        }
        return j == Long.MIN_VALUE ? this.lastSeekPositionUs : j;
    }

    public long getNextLoadPositionUs() {
        if (this.enabledTrackCount == 0) {
            return Long.MIN_VALUE;
        }
        return getBufferedPositionUs();
    }

    public TrackGroupArray getTrackGroups() {
        return this.tracks;
    }

    /* access modifiers changed from: package-private */
    public boolean isReady(int i) {
        return this.loadingFinished || (!isPendingReset() && this.sampleQueues[i].hasNextSample());
    }

    /* access modifiers changed from: package-private */
    public void maybeThrowError() throws IOException {
        this.loader.maybeThrowError();
        this.loader.maybeThrowError(this.actualMinLoadableRetryCount);
    }

    public void maybeThrowPrepareError() throws IOException {
        maybeThrowError();
    }

    public void onLoadCanceled(ExtractingLoadable extractingLoadable, long j, long j2, boolean z) {
        if (!z) {
            copyLengthFromLoader(extractingLoadable);
            for (SampleQueue reset : this.sampleQueues) {
                reset.reset();
            }
            if (this.enabledTrackCount > 0) {
                this.callback.onContinueLoadingRequested(this);
            }
        }
    }

    public void onLoadCompleted(ExtractingLoadable extractingLoadable, long j, long j2) {
        copyLengthFromLoader(extractingLoadable);
        this.loadingFinished = true;
        if (this.durationUs == C.TIME_UNSET) {
            long largestQueuedTimestampUs = getLargestQueuedTimestampUs();
            this.durationUs = largestQueuedTimestampUs == Long.MIN_VALUE ? 0 : DEFAULT_LAST_SAMPLE_DURATION_US + largestQueuedTimestampUs;
            this.listener.onSourceInfoRefreshed(this.durationUs, this.seekMap.isSeekable());
        }
        this.callback.onContinueLoadingRequested(this);
    }

    public int onLoadError(ExtractingLoadable extractingLoadable, long j, long j2, IOException iOException) {
        copyLengthFromLoader(extractingLoadable);
        notifyLoadError(iOException);
        if (isLoadableExceptionFatal(iOException)) {
            return 3;
        }
        boolean z = getExtractedSamplesCount() > this.extractedSamplesCountAtStartOfLoad;
        configureRetry(extractingLoadable);
        this.extractedSamplesCountAtStartOfLoad = getExtractedSamplesCount();
        return !z ? 0 : 1;
    }

    public void onLoaderReleased() {
        this.extractorHolder.release();
        for (SampleQueue reset : this.sampleQueues) {
            reset.reset();
        }
    }

    public void onUpstreamFormatChanged(Format format) {
        this.handler.post(this.maybeFinishPrepareRunnable);
    }

    public void prepare(MediaPeriod.Callback callback2, long j) {
        this.callback = callback2;
        this.loadCondition.open();
        startLoading();
    }

    /* access modifiers changed from: package-private */
    public int readData(int i, FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
        if (this.notifyDiscontinuity || isPendingReset()) {
            return -3;
        }
        return this.sampleQueues[i].read(formatHolder, decoderInputBuffer, z, this.loadingFinished, this.lastSeekPositionUs);
    }

    public long readDiscontinuity() {
        if (!this.notifyDiscontinuity) {
            return C.TIME_UNSET;
        }
        this.notifyDiscontinuity = false;
        return this.lastSeekPositionUs;
    }

    public void release() {
        boolean release = this.loader.release(this);
        if (this.prepared && !release) {
            for (SampleQueue discardToEnd : this.sampleQueues) {
                discardToEnd.discardToEnd();
            }
        }
        this.handler.removeCallbacksAndMessages((Object) null);
        this.released = true;
    }

    public void seekMap(SeekMap seekMap2) {
        this.seekMap = seekMap2;
        this.handler.post(this.maybeFinishPrepareRunnable);
    }

    public long seekToUs(long j) {
        if (!this.seekMap.isSeekable()) {
            j = 0;
        }
        this.lastSeekPositionUs = j;
        this.notifyDiscontinuity = false;
        if (isPendingReset() || !seekInsideBufferUs(j)) {
            this.pendingResetPositionUs = j;
            this.loadingFinished = false;
            if (this.loader.isLoading()) {
                this.loader.cancelLoading();
            } else {
                for (SampleQueue reset : this.sampleQueues) {
                    reset.reset();
                }
            }
        }
        return j;
    }

    public long selectTracks(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j) {
        Assertions.checkState(this.prepared);
        int i = this.enabledTrackCount;
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if (sampleStreamArr[i2] != null && (trackSelectionArr[i2] == null || !zArr[i2])) {
                int access$300 = sampleStreamArr[i2].track;
                Assertions.checkState(this.trackEnabledStates[access$300]);
                this.enabledTrackCount--;
                this.trackEnabledStates[access$300] = false;
                sampleStreamArr[i2] = null;
            }
        }
        boolean z = this.seenFirstTrackSelection ? i == 0 : j != 0;
        for (int i3 = 0; i3 < trackSelectionArr.length; i3++) {
            if (sampleStreamArr[i3] == null && trackSelectionArr[i3] != null) {
                TrackSelection trackSelection = trackSelectionArr[i3];
                Assertions.checkState(trackSelection.length() == 1);
                Assertions.checkState(trackSelection.getIndexInTrackGroup(0) == 0);
                int indexOf = this.tracks.indexOf(trackSelection.getTrackGroup());
                Assertions.checkState(!this.trackEnabledStates[indexOf]);
                this.enabledTrackCount++;
                this.trackEnabledStates[indexOf] = true;
                sampleStreamArr[i3] = new SampleStreamImpl(indexOf);
                zArr2[i3] = true;
                if (!z) {
                    SampleQueue sampleQueue = this.sampleQueues[indexOf];
                    sampleQueue.rewind();
                    z = !sampleQueue.advanceTo(j, true, true) && sampleQueue.getReadIndex() != 0;
                }
            }
        }
        if (this.enabledTrackCount == 0) {
            this.notifyDiscontinuity = false;
            if (this.loader.isLoading()) {
                for (SampleQueue discardToEnd : this.sampleQueues) {
                    discardToEnd.discardToEnd();
                }
                this.loader.cancelLoading();
            } else {
                for (SampleQueue reset : this.sampleQueues) {
                    reset.reset();
                }
            }
        } else if (z) {
            j = seekToUs(j);
            for (int i4 = 0; i4 < sampleStreamArr.length; i4++) {
                if (sampleStreamArr[i4] != null) {
                    zArr2[i4] = true;
                }
            }
        }
        this.seenFirstTrackSelection = true;
        return j;
    }

    /* access modifiers changed from: package-private */
    public void skipData(int i, long j) {
        SampleQueue sampleQueue = this.sampleQueues[i];
        if (!this.loadingFinished || j <= sampleQueue.getLargestQueuedTimestampUs()) {
            sampleQueue.advanceTo(j, true, true);
        } else {
            sampleQueue.advanceToEnd();
        }
    }

    public TrackOutput track(int i, int i2) {
        int length2 = this.sampleQueues.length;
        for (int i3 = 0; i3 < length2; i3++) {
            if (this.sampleQueueTrackIds[i3] == i) {
                return this.sampleQueues[i3];
            }
        }
        SampleQueue sampleQueue = new SampleQueue(this.allocator);
        sampleQueue.setUpstreamFormatChangeListener(this);
        this.sampleQueueTrackIds = Arrays.copyOf(this.sampleQueueTrackIds, length2 + 1);
        this.sampleQueueTrackIds[length2] = i;
        this.sampleQueues = (SampleQueue[]) Arrays.copyOf(this.sampleQueues, length2 + 1);
        this.sampleQueues[length2] = sampleQueue;
        return sampleQueue;
    }
}
