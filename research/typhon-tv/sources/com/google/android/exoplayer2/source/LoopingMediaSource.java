package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.util.Assertions;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;

public final class LoopingMediaSource implements MediaSource {
    /* access modifiers changed from: private */
    public int childPeriodCount;
    private final MediaSource childSource;
    /* access modifiers changed from: private */
    public final int loopCount;

    private static final class InfinitelyLoopingTimeline extends Timeline {
        private final Timeline childTimeline;

        public InfinitelyLoopingTimeline(Timeline timeline) {
            this.childTimeline = timeline;
        }

        public int getIndexOfPeriod(Object obj) {
            return this.childTimeline.getIndexOfPeriod(obj);
        }

        public int getNextWindowIndex(int i, int i2) {
            int nextWindowIndex = this.childTimeline.getNextWindowIndex(i, i2);
            if (nextWindowIndex == -1) {
                return 0;
            }
            return nextWindowIndex;
        }

        public Timeline.Period getPeriod(int i, Timeline.Period period, boolean z) {
            return this.childTimeline.getPeriod(i, period, z);
        }

        public int getPeriodCount() {
            return this.childTimeline.getPeriodCount();
        }

        public int getPreviousWindowIndex(int i, int i2) {
            int previousWindowIndex = this.childTimeline.getPreviousWindowIndex(i, i2);
            return previousWindowIndex == -1 ? getWindowCount() - 1 : previousWindowIndex;
        }

        public Timeline.Window getWindow(int i, Timeline.Window window, boolean z, long j) {
            return this.childTimeline.getWindow(i, window, z, j);
        }

        public int getWindowCount() {
            return this.childTimeline.getWindowCount();
        }
    }

    private static final class LoopingTimeline extends AbstractConcatenatedTimeline {
        private final int childPeriodCount;
        private final Timeline childTimeline;
        private final int childWindowCount;
        private final int loopCount;

        public LoopingTimeline(Timeline timeline, int i) {
            super(i);
            this.childTimeline = timeline;
            this.childPeriodCount = timeline.getPeriodCount();
            this.childWindowCount = timeline.getWindowCount();
            this.loopCount = i;
            Assertions.checkState(i <= MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT / this.childPeriodCount, "LoopingMediaSource contains too many periods");
        }

        /* access modifiers changed from: protected */
        public int getChildIndexByChildUid(Object obj) {
            if (!(obj instanceof Integer)) {
                return -1;
            }
            return ((Integer) obj).intValue();
        }

        /* access modifiers changed from: protected */
        public int getChildIndexByPeriodIndex(int i) {
            return i / this.childPeriodCount;
        }

        /* access modifiers changed from: protected */
        public int getChildIndexByWindowIndex(int i) {
            return i / this.childWindowCount;
        }

        /* access modifiers changed from: protected */
        public Object getChildUidByChildIndex(int i) {
            return Integer.valueOf(i);
        }

        /* access modifiers changed from: protected */
        public int getFirstPeriodIndexByChildIndex(int i) {
            return this.childPeriodCount * i;
        }

        /* access modifiers changed from: protected */
        public int getFirstWindowIndexByChildIndex(int i) {
            return this.childWindowCount * i;
        }

        public int getPeriodCount() {
            return this.childPeriodCount * this.loopCount;
        }

        /* access modifiers changed from: protected */
        public Timeline getTimelineByChildIndex(int i) {
            return this.childTimeline;
        }

        public int getWindowCount() {
            return this.childWindowCount * this.loopCount;
        }
    }

    public LoopingMediaSource(MediaSource mediaSource) {
        this(mediaSource, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    public LoopingMediaSource(MediaSource mediaSource, int i) {
        Assertions.checkArgument(i > 0);
        this.childSource = mediaSource;
        this.loopCount = i;
    }

    public MediaPeriod createPeriod(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        return this.loopCount != Integer.MAX_VALUE ? this.childSource.createPeriod(new MediaSource.MediaPeriodId(mediaPeriodId.periodIndex % this.childPeriodCount), allocator) : this.childSource.createPeriod(mediaPeriodId, allocator);
    }

    public void maybeThrowSourceInfoRefreshError() throws IOException {
        this.childSource.maybeThrowSourceInfoRefreshError();
    }

    public void prepareSource(ExoPlayer exoPlayer, boolean z, final MediaSource.Listener listener) {
        this.childSource.prepareSource(exoPlayer, false, new MediaSource.Listener() {
            public void onSourceInfoRefreshed(Timeline timeline, Object obj) {
                int unused = LoopingMediaSource.this.childPeriodCount = timeline.getPeriodCount();
                listener.onSourceInfoRefreshed(LoopingMediaSource.this.loopCount != Integer.MAX_VALUE ? new LoopingTimeline(timeline, LoopingMediaSource.this.loopCount) : new InfinitelyLoopingTimeline(timeline), obj);
            }
        });
    }

    public void releasePeriod(MediaPeriod mediaPeriod) {
        this.childSource.releasePeriod(mediaPeriod);
    }

    public void releaseSource() {
        this.childSource.releaseSource();
    }
}
