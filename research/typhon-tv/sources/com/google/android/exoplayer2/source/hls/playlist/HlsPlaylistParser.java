package com.google.android.exoplayer2.source.hls.playlist;

import android.net.Uri;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.source.UnrecognizedInputFormatException;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class HlsPlaylistParser implements ParsingLoadable.Parser<HlsPlaylist> {
    private static final String ATTR_CLOSED_CAPTIONS_NONE = "CLOSED-CAPTIONS=NONE";
    private static final String BOOLEAN_FALSE = "NO";
    private static final String BOOLEAN_TRUE = "YES";
    private static final String METHOD_AES128 = "AES-128";
    private static final String METHOD_NONE = "NONE";
    private static final String PLAYLIST_HEADER = "#EXTM3U";
    private static final Pattern REGEX_ATTR_BYTERANGE = Pattern.compile("BYTERANGE=\"(\\d+(?:@\\d+)?)\\b\"");
    private static final Pattern REGEX_AUTOSELECT = compileBooleanAttrPattern("AUTOSELECT");
    private static final Pattern REGEX_AVERAGE_BANDWIDTH = Pattern.compile("AVERAGE-BANDWIDTH=(\\d+)\\b");
    private static final Pattern REGEX_BANDWIDTH = Pattern.compile("[^-]BANDWIDTH=(\\d+)\\b");
    private static final Pattern REGEX_BYTERANGE = Pattern.compile("#EXT-X-BYTERANGE:(\\d+(?:@\\d+)?)\\b");
    private static final Pattern REGEX_CODECS = Pattern.compile("CODECS=\"(.+?)\"");
    private static final Pattern REGEX_DEFAULT = compileBooleanAttrPattern("DEFAULT");
    private static final Pattern REGEX_FORCED = compileBooleanAttrPattern("FORCED");
    private static final Pattern REGEX_INSTREAM_ID = Pattern.compile("INSTREAM-ID=\"((?:CC|SERVICE)\\d+)\"");
    private static final Pattern REGEX_IV = Pattern.compile("IV=([^,.*]+)");
    private static final Pattern REGEX_LANGUAGE = Pattern.compile("LANGUAGE=\"(.+?)\"");
    private static final Pattern REGEX_MEDIA_DURATION = Pattern.compile("#EXTINF:([\\d\\.]+)\\b");
    private static final Pattern REGEX_MEDIA_SEQUENCE = Pattern.compile("#EXT-X-MEDIA-SEQUENCE:(\\d+)\\b");
    private static final Pattern REGEX_METHOD = Pattern.compile("METHOD=(NONE|AES-128)");
    private static final Pattern REGEX_NAME = Pattern.compile("NAME=\"(.+?)\"");
    private static final Pattern REGEX_PLAYLIST_TYPE = Pattern.compile("#EXT-X-PLAYLIST-TYPE:(.+)\\b");
    private static final Pattern REGEX_RESOLUTION = Pattern.compile("RESOLUTION=(\\d+x\\d+)");
    private static final Pattern REGEX_TARGET_DURATION = Pattern.compile("#EXT-X-TARGETDURATION:(\\d+)\\b");
    private static final Pattern REGEX_TIME_OFFSET = Pattern.compile("TIME-OFFSET=(-?[\\d\\.]+)\\b");
    private static final Pattern REGEX_TYPE = Pattern.compile("TYPE=(AUDIO|VIDEO|SUBTITLES|CLOSED-CAPTIONS)");
    private static final Pattern REGEX_URI = Pattern.compile("URI=\"(.+?)\"");
    private static final Pattern REGEX_VERSION = Pattern.compile("#EXT-X-VERSION:(\\d+)\\b");
    private static final String TAG_BYTERANGE = "#EXT-X-BYTERANGE";
    private static final String TAG_DISCONTINUITY = "#EXT-X-DISCONTINUITY";
    private static final String TAG_DISCONTINUITY_SEQUENCE = "#EXT-X-DISCONTINUITY-SEQUENCE";
    private static final String TAG_ENDLIST = "#EXT-X-ENDLIST";
    private static final String TAG_INDEPENDENT_SEGMENTS = "#EXT-X-INDEPENDENT-SEGMENTS";
    private static final String TAG_INIT_SEGMENT = "#EXT-X-MAP";
    private static final String TAG_KEY = "#EXT-X-KEY";
    private static final String TAG_MEDIA = "#EXT-X-MEDIA";
    private static final String TAG_MEDIA_DURATION = "#EXTINF";
    private static final String TAG_MEDIA_SEQUENCE = "#EXT-X-MEDIA-SEQUENCE";
    private static final String TAG_PLAYLIST_TYPE = "#EXT-X-PLAYLIST-TYPE";
    private static final String TAG_PREFIX = "#EXT";
    private static final String TAG_PROGRAM_DATE_TIME = "#EXT-X-PROGRAM-DATE-TIME";
    private static final String TAG_START = "#EXT-X-START";
    private static final String TAG_STREAM_INF = "#EXT-X-STREAM-INF";
    private static final String TAG_TARGET_DURATION = "#EXT-X-TARGETDURATION";
    private static final String TAG_VERSION = "#EXT-X-VERSION";
    private static final String TYPE_AUDIO = "AUDIO";
    private static final String TYPE_CLOSED_CAPTIONS = "CLOSED-CAPTIONS";
    private static final String TYPE_SUBTITLES = "SUBTITLES";
    private static final String TYPE_VIDEO = "VIDEO";

    private static class LineIterator {
        private final Queue<String> extraLines;
        private String next;
        private final BufferedReader reader;

        public LineIterator(Queue<String> queue, BufferedReader bufferedReader) {
            this.extraLines = queue;
            this.reader = bufferedReader;
        }

        public boolean hasNext() throws IOException {
            if (this.next != null) {
                return true;
            }
            if (!this.extraLines.isEmpty()) {
                this.next = this.extraLines.poll();
                return true;
            }
            do {
                String readLine = this.reader.readLine();
                this.next = readLine;
                if (readLine == null) {
                    return false;
                }
                this.next = this.next.trim();
            } while (this.next.isEmpty());
            return true;
        }

        public String next() throws IOException {
            if (!hasNext()) {
                return null;
            }
            String str = this.next;
            this.next = null;
            return str;
        }
    }

    private static boolean checkPlaylistHeader(BufferedReader bufferedReader) throws IOException {
        int read = bufferedReader.read();
        if (read == 239) {
            if (bufferedReader.read() != 187 || bufferedReader.read() != 191) {
                return false;
            }
            read = bufferedReader.read();
        }
        int skipIgnorableWhitespace = skipIgnorableWhitespace(bufferedReader, true, read);
        int length = PLAYLIST_HEADER.length();
        for (int i = 0; i < length; i++) {
            if (skipIgnorableWhitespace != PLAYLIST_HEADER.charAt(i)) {
                return false;
            }
            skipIgnorableWhitespace = bufferedReader.read();
        }
        return Util.isLinebreak(skipIgnorableWhitespace(bufferedReader, false, skipIgnorableWhitespace));
    }

    private static Pattern compileBooleanAttrPattern(String str) {
        return Pattern.compile(str + "=(" + BOOLEAN_FALSE + "|" + BOOLEAN_TRUE + ")");
    }

    private static boolean parseBooleanAttribute(String str, Pattern pattern, boolean z) {
        Matcher matcher = pattern.matcher(str);
        return matcher.find() ? matcher.group(1).equals(BOOLEAN_TRUE) : z;
    }

    private static double parseDoubleAttr(String str, Pattern pattern) throws ParserException {
        return Double.parseDouble(parseStringAttr(str, pattern));
    }

    private static int parseIntAttr(String str, Pattern pattern) throws ParserException {
        return Integer.parseInt(parseStringAttr(str, pattern));
    }

    private static HlsMasterPlaylist parseMasterPlaylist(LineIterator lineIterator, String str) throws IOException {
        int i;
        int i2;
        String str2;
        int parseInt;
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Format format = null;
        List list = null;
        boolean z = false;
        while (lineIterator.hasNext()) {
            String next = lineIterator.next();
            if (next.startsWith(TAG_PREFIX)) {
                arrayList4.add(next);
            }
            if (next.startsWith(TAG_MEDIA)) {
                int parseSelectionFlags = parseSelectionFlags(next);
                String parseOptionalStringAttr = parseOptionalStringAttr(next, REGEX_URI);
                String parseStringAttr = parseStringAttr(next, REGEX_NAME);
                String parseOptionalStringAttr2 = parseOptionalStringAttr(next, REGEX_LANGUAGE);
                String parseStringAttr2 = parseStringAttr(next, REGEX_TYPE);
                char c = 65535;
                switch (parseStringAttr2.hashCode()) {
                    case -959297733:
                        if (parseStringAttr2.equals(TYPE_SUBTITLES)) {
                            c = 1;
                            break;
                        }
                        break;
                    case -333210994:
                        if (parseStringAttr2.equals(TYPE_CLOSED_CAPTIONS)) {
                            c = 2;
                            break;
                        }
                        break;
                    case 62628790:
                        if (parseStringAttr2.equals(TYPE_AUDIO)) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        Format createAudioContainerFormat = Format.createAudioContainerFormat(parseStringAttr, MimeTypes.APPLICATION_M3U8, (String) null, (String) null, -1, -1, -1, (List<byte[]>) null, parseSelectionFlags, parseOptionalStringAttr2);
                        if (parseOptionalStringAttr != null) {
                            arrayList2.add(new HlsMasterPlaylist.HlsUrl(parseOptionalStringAttr, createAudioContainerFormat));
                            break;
                        } else {
                            format = createAudioContainerFormat;
                            break;
                        }
                    case 1:
                        arrayList3.add(new HlsMasterPlaylist.HlsUrl(parseOptionalStringAttr, Format.createTextContainerFormat(parseStringAttr, MimeTypes.APPLICATION_M3U8, MimeTypes.TEXT_VTT, (String) null, -1, parseSelectionFlags, parseOptionalStringAttr2)));
                        break;
                    case 2:
                        String parseStringAttr3 = parseStringAttr(next, REGEX_INSTREAM_ID);
                        if (parseStringAttr3.startsWith("CC")) {
                            str2 = MimeTypes.APPLICATION_CEA608;
                            parseInt = Integer.parseInt(parseStringAttr3.substring(2));
                        } else {
                            str2 = MimeTypes.APPLICATION_CEA708;
                            parseInt = Integer.parseInt(parseStringAttr3.substring(7));
                        }
                        if (list == null) {
                            list = new ArrayList();
                        }
                        list.add(Format.createTextContainerFormat(parseStringAttr, (String) null, str2, (String) null, -1, parseSelectionFlags, parseOptionalStringAttr2, parseInt));
                        break;
                }
            } else if (next.startsWith(TAG_STREAM_INF)) {
                int parseIntAttr = parseIntAttr(next, REGEX_BANDWIDTH);
                String parseOptionalStringAttr3 = parseOptionalStringAttr(next, REGEX_AVERAGE_BANDWIDTH);
                if (parseOptionalStringAttr3 != null) {
                    parseIntAttr = Integer.parseInt(parseOptionalStringAttr3);
                }
                String parseOptionalStringAttr4 = parseOptionalStringAttr(next, REGEX_CODECS);
                String parseOptionalStringAttr5 = parseOptionalStringAttr(next, REGEX_RESOLUTION);
                z |= next.contains(ATTR_CLOSED_CAPTIONS_NONE);
                if (parseOptionalStringAttr5 != null) {
                    String[] split = parseOptionalStringAttr5.split("x");
                    i = Integer.parseInt(split[0]);
                    i2 = Integer.parseInt(split[1]);
                    if (i <= 0 || i2 <= 0) {
                        i = -1;
                        i2 = -1;
                    }
                } else {
                    i = -1;
                    i2 = -1;
                }
                String next2 = lineIterator.next();
                if (hashSet.add(next2)) {
                    arrayList.add(new HlsMasterPlaylist.HlsUrl(next2, Format.createVideoContainerFormat(Integer.toString(arrayList.size()), MimeTypes.APPLICATION_M3U8, (String) null, parseOptionalStringAttr4, parseIntAttr, i, i2, -1.0f, (List<byte[]>) null, 0)));
                }
            }
        }
        if (z) {
            list = Collections.emptyList();
        }
        return new HlsMasterPlaylist(str, arrayList4, arrayList, arrayList2, arrayList3, format, list);
    }

    private static HlsMediaPlaylist parseMediaPlaylist(LineIterator lineIterator, String str) throws IOException {
        int i = 0;
        long j = C.TIME_UNSET;
        int i2 = 0;
        int i3 = 1;
        long j2 = C.TIME_UNSET;
        boolean z = false;
        boolean z2 = false;
        HlsMediaPlaylist.Segment segment = null;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        long j3 = 0;
        boolean z3 = false;
        int i4 = 0;
        int i5 = 0;
        long j4 = 0;
        long j5 = 0;
        long j6 = 0;
        long j7 = -1;
        int i6 = 0;
        boolean z4 = false;
        String str2 = null;
        String str3 = null;
        while (lineIterator.hasNext()) {
            String next = lineIterator.next();
            if (next.startsWith(TAG_PREFIX)) {
                arrayList2.add(next);
            }
            if (next.startsWith(TAG_PLAYLIST_TYPE)) {
                String parseStringAttr = parseStringAttr(next, REGEX_PLAYLIST_TYPE);
                if ("VOD".equals(parseStringAttr)) {
                    i = 1;
                } else if ("EVENT".equals(parseStringAttr)) {
                    i = 2;
                }
            } else if (next.startsWith(TAG_START)) {
                j = (long) (parseDoubleAttr(next, REGEX_TIME_OFFSET) * 1000000.0d);
            } else if (next.startsWith(TAG_INIT_SEGMENT)) {
                String parseStringAttr2 = parseStringAttr(next, REGEX_URI);
                String parseOptionalStringAttr = parseOptionalStringAttr(next, REGEX_ATTR_BYTERANGE);
                if (parseOptionalStringAttr != null) {
                    String[] split = parseOptionalStringAttr.split("@");
                    j7 = Long.parseLong(split[0]);
                    if (split.length > 1) {
                        j6 = Long.parseLong(split[1]);
                    }
                }
                segment = new HlsMediaPlaylist.Segment(parseStringAttr2, j6, j7);
                j6 = 0;
                j7 = -1;
            } else if (next.startsWith(TAG_TARGET_DURATION)) {
                j2 = ((long) parseIntAttr(next, REGEX_TARGET_DURATION)) * C.MICROS_PER_SECOND;
            } else if (next.startsWith(TAG_MEDIA_SEQUENCE)) {
                i2 = parseIntAttr(next, REGEX_MEDIA_SEQUENCE);
                i6 = i2;
            } else if (next.startsWith(TAG_VERSION)) {
                i3 = parseIntAttr(next, REGEX_VERSION);
            } else if (next.startsWith(TAG_MEDIA_DURATION)) {
                j3 = (long) (parseDoubleAttr(next, REGEX_MEDIA_DURATION) * 1000000.0d);
            } else if (next.startsWith(TAG_KEY)) {
                z4 = METHOD_AES128.equals(parseStringAttr(next, REGEX_METHOD));
                if (z4) {
                    str2 = parseStringAttr(next, REGEX_URI);
                    str3 = parseOptionalStringAttr(next, REGEX_IV);
                } else {
                    str2 = null;
                    str3 = null;
                }
            } else if (next.startsWith(TAG_BYTERANGE)) {
                String[] split2 = parseStringAttr(next, REGEX_BYTERANGE).split("@");
                j7 = Long.parseLong(split2[0]);
                if (split2.length > 1) {
                    j6 = Long.parseLong(split2[1]);
                }
            } else if (next.startsWith(TAG_DISCONTINUITY_SEQUENCE)) {
                z3 = true;
                i4 = Integer.parseInt(next.substring(next.indexOf(58) + 1));
            } else if (next.equals(TAG_DISCONTINUITY)) {
                i5++;
            } else if (next.startsWith(TAG_PROGRAM_DATE_TIME)) {
                if (j4 == 0) {
                    j4 = C.msToUs(Util.parseXsDateTime(next.substring(next.indexOf(58) + 1))) - j5;
                }
            } else if (!next.startsWith("#")) {
                String hexString = !z4 ? null : str3 != null ? str3 : Integer.toHexString(i6);
                i6++;
                if (j7 == -1) {
                    j6 = 0;
                }
                arrayList.add(new HlsMediaPlaylist.Segment(next, j3, i5, j5, z4, str2, hexString, j6, j7));
                j5 += j3;
                j3 = 0;
                if (j7 != -1) {
                    j6 += j7;
                }
                j7 = -1;
            } else if (next.equals(TAG_INDEPENDENT_SEGMENTS)) {
                z = true;
            } else if (next.equals(TAG_ENDLIST)) {
                z2 = true;
            }
        }
        return new HlsMediaPlaylist(i, str, arrayList2, j, j4, z3, i4, i2, i3, j2, z, z2, j4 != 0, segment, arrayList);
    }

    private static String parseOptionalStringAttr(String str, Pattern pattern) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private static int parseSelectionFlags(String str) {
        int i = 0;
        int i2 = (parseBooleanAttribute(str, REGEX_DEFAULT, false) ? 1 : 0) | (parseBooleanAttribute(str, REGEX_FORCED, false) ? 2 : 0);
        if (parseBooleanAttribute(str, REGEX_AUTOSELECT, false)) {
            i = 4;
        }
        return i2 | i;
    }

    private static String parseStringAttr(String str, Pattern pattern) throws ParserException {
        Matcher matcher = pattern.matcher(str);
        if (matcher.find() && matcher.groupCount() == 1) {
            return matcher.group(1);
        }
        throw new ParserException("Couldn't match " + pattern.pattern() + " in " + str);
    }

    private static int skipIgnorableWhitespace(BufferedReader bufferedReader, boolean z, int i) throws IOException {
        while (i != -1 && Character.isWhitespace(i) && (z || !Util.isLinebreak(i))) {
            i = bufferedReader.read();
        }
        return i;
    }

    public HlsPlaylist parse(Uri uri, InputStream inputStream) throws IOException {
        String trim;
        HlsPlaylist parseMasterPlaylist;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        LinkedList linkedList = new LinkedList();
        try {
            if (!checkPlaylistHeader(bufferedReader)) {
                throw new UnrecognizedInputFormatException("Input does not start with the #EXTM3U header.", uri);
            }
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    trim = readLine.trim();
                    if (!trim.isEmpty()) {
                        if (trim.startsWith(TAG_STREAM_INF)) {
                            linkedList.add(trim);
                            parseMasterPlaylist = parseMasterPlaylist(new LineIterator(linkedList, bufferedReader), uri.toString());
                            Util.closeQuietly((Closeable) bufferedReader);
                            break;
                        } else if (trim.startsWith(TAG_TARGET_DURATION) || trim.startsWith(TAG_MEDIA_SEQUENCE) || trim.startsWith(TAG_MEDIA_DURATION) || trim.startsWith(TAG_KEY) || trim.startsWith(TAG_BYTERANGE) || trim.equals(TAG_DISCONTINUITY) || trim.equals(TAG_DISCONTINUITY_SEQUENCE) || trim.equals(TAG_ENDLIST)) {
                            linkedList.add(trim);
                            parseMasterPlaylist = parseMediaPlaylist(new LineIterator(linkedList, bufferedReader), uri.toString());
                        } else {
                            linkedList.add(trim);
                        }
                    }
                } else {
                    Util.closeQuietly((Closeable) bufferedReader);
                    throw new ParserException("Failed to parse the playlist, could not identify any tags.");
                }
            }
            linkedList.add(trim);
            parseMasterPlaylist = parseMediaPlaylist(new LineIterator(linkedList, bufferedReader), uri.toString());
            return parseMasterPlaylist;
        } finally {
            Util.closeQuietly((Closeable) bufferedReader);
        }
    }
}
