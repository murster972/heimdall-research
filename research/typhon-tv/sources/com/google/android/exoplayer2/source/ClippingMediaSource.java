package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.ArrayList;

public final class ClippingMediaSource implements MediaSource, MediaSource.Listener {
    private ClippingTimeline clippingTimeline;
    private final boolean enableInitialDiscontinuity;
    private final long endUs;
    private final ArrayList<ClippingMediaPeriod> mediaPeriods;
    private final MediaSource mediaSource;
    private MediaSource.Listener sourceListener;
    private final long startUs;

    private static final class ClippingTimeline extends Timeline {
        /* access modifiers changed from: private */
        public final long endUs;
        /* access modifiers changed from: private */
        public final long startUs;
        private final Timeline timeline;

        public ClippingTimeline(Timeline timeline2, long j, long j2) {
            Assertions.checkArgument(timeline2.getWindowCount() == 1);
            Assertions.checkArgument(timeline2.getPeriodCount() == 1);
            Timeline.Window window = timeline2.getWindow(0, new Timeline.Window(), false);
            Assertions.checkArgument(!window.isDynamic);
            long j3 = j2 == Long.MIN_VALUE ? window.durationUs : j2;
            if (window.durationUs != C.TIME_UNSET) {
                j3 = j3 > window.durationUs ? window.durationUs : j3;
                Assertions.checkArgument(j == 0 || window.isSeekable);
                Assertions.checkArgument(j <= j3);
            }
            Assertions.checkArgument(timeline2.getPeriod(0, new Timeline.Period()).getPositionInWindowUs() == 0);
            this.timeline = timeline2;
            this.startUs = j;
            this.endUs = j3;
        }

        public int getIndexOfPeriod(Object obj) {
            return this.timeline.getIndexOfPeriod(obj);
        }

        public int getNextWindowIndex(int i, int i2) {
            return this.timeline.getNextWindowIndex(i, i2);
        }

        public Timeline.Period getPeriod(int i, Timeline.Period period, boolean z) {
            long j = C.TIME_UNSET;
            Timeline.Period period2 = this.timeline.getPeriod(0, period, z);
            if (this.endUs != C.TIME_UNSET) {
                j = this.endUs - this.startUs;
            }
            period2.durationUs = j;
            return period2;
        }

        public int getPeriodCount() {
            return 1;
        }

        public int getPreviousWindowIndex(int i, int i2) {
            return this.timeline.getPreviousWindowIndex(i, i2);
        }

        public Timeline.Window getWindow(int i, Timeline.Window window, boolean z, long j) {
            Timeline.Window window2 = this.timeline.getWindow(0, window, z, j);
            window2.durationUs = this.endUs != C.TIME_UNSET ? this.endUs - this.startUs : -9223372036854775807L;
            if (window2.defaultPositionUs != C.TIME_UNSET) {
                window2.defaultPositionUs = Math.max(window2.defaultPositionUs, this.startUs);
                window2.defaultPositionUs = this.endUs == C.TIME_UNSET ? window2.defaultPositionUs : Math.min(window2.defaultPositionUs, this.endUs);
                window2.defaultPositionUs -= this.startUs;
            }
            long usToMs = C.usToMs(this.startUs);
            if (window2.presentationStartTimeMs != C.TIME_UNSET) {
                window2.presentationStartTimeMs += usToMs;
            }
            if (window2.windowStartTimeMs != C.TIME_UNSET) {
                window2.windowStartTimeMs += usToMs;
            }
            return window2;
        }

        public int getWindowCount() {
            return 1;
        }
    }

    public ClippingMediaSource(MediaSource mediaSource2, long j, long j2) {
        this(mediaSource2, j, j2, true);
    }

    public ClippingMediaSource(MediaSource mediaSource2, long j, long j2, boolean z) {
        Assertions.checkArgument(j >= 0);
        this.mediaSource = (MediaSource) Assertions.checkNotNull(mediaSource2);
        this.startUs = j;
        this.endUs = j2;
        this.enableInitialDiscontinuity = z;
        this.mediaPeriods = new ArrayList<>();
    }

    public MediaPeriod createPeriod(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        ClippingMediaPeriod clippingMediaPeriod = new ClippingMediaPeriod(this.mediaSource.createPeriod(mediaPeriodId, allocator), this.enableInitialDiscontinuity);
        this.mediaPeriods.add(clippingMediaPeriod);
        clippingMediaPeriod.setClipping(this.clippingTimeline.startUs, this.clippingTimeline.endUs);
        return clippingMediaPeriod;
    }

    public void maybeThrowSourceInfoRefreshError() throws IOException {
        this.mediaSource.maybeThrowSourceInfoRefreshError();
    }

    public void onSourceInfoRefreshed(Timeline timeline, Object obj) {
        this.clippingTimeline = new ClippingTimeline(timeline, this.startUs, this.endUs);
        this.sourceListener.onSourceInfoRefreshed(this.clippingTimeline, obj);
        long access$000 = this.clippingTimeline.startUs;
        long access$100 = this.clippingTimeline.endUs == C.TIME_UNSET ? Long.MIN_VALUE : this.clippingTimeline.endUs;
        int size = this.mediaPeriods.size();
        for (int i = 0; i < size; i++) {
            this.mediaPeriods.get(i).setClipping(access$000, access$100);
        }
    }

    public void prepareSource(ExoPlayer exoPlayer, boolean z, MediaSource.Listener listener) {
        this.sourceListener = listener;
        this.mediaSource.prepareSource(exoPlayer, false, this);
    }

    public void releasePeriod(MediaPeriod mediaPeriod) {
        Assertions.checkState(this.mediaPeriods.remove(mediaPeriod));
        this.mediaSource.releasePeriod(((ClippingMediaPeriod) mediaPeriod).mediaPeriod);
    }

    public void releaseSource() {
        this.mediaSource.releaseSource();
    }
}
