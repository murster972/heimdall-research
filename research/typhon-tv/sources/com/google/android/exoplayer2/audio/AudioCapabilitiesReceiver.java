package com.google.android.exoplayer2.audio;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;

public final class AudioCapabilitiesReceiver {
    AudioCapabilities audioCapabilities;
    private final Context context;
    /* access modifiers changed from: private */
    public AudioCapabilities currentHDMIAudioCapabilities;
    private final Listener listener;
    private final SurroundSoundSettingObserver observer;
    private final BroadcastReceiver receiver;
    /* access modifiers changed from: private */
    public final ContentResolver resolver;

    private final class HdmiAudioPlugBroadcastReceiver extends BroadcastReceiver {
        private HdmiAudioPlugBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (!isInitialStickyBroadcast()) {
                AudioCapabilities unused = AudioCapabilitiesReceiver.this.currentHDMIAudioCapabilities = AudioCapabilities.getCapabilities(context, intent);
                AudioCapabilitiesReceiver.this.maybeNotifyAudioCapabilityChanged(AudioCapabilitiesReceiver.this.currentHDMIAudioCapabilities);
            }
        }
    }

    public interface Listener {
        void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities);
    }

    @TargetApi(17)
    private final class SurroundSoundSettingObserver extends ContentObserver {
        public SurroundSoundSettingObserver() {
            super((Handler) null);
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            super.onChange(z);
            AudioCapabilitiesReceiver.this.maybeNotifyAudioCapabilityChanged(AudioCapabilities.isSurroundSoundEnabledV17(AudioCapabilitiesReceiver.this.resolver) ? AudioCapabilities.SURROUND_AUDIO_CAPABILITIES : AudioCapabilitiesReceiver.this.currentHDMIAudioCapabilities);
        }
    }

    public AudioCapabilitiesReceiver(Context context2, Listener listener2) {
        this.context = (Context) Assertions.checkNotNull(context2);
        this.listener = (Listener) Assertions.checkNotNull(listener2);
        this.receiver = Util.SDK_INT >= 21 ? new HdmiAudioPlugBroadcastReceiver() : null;
        if (Util.SDK_INT >= 17) {
            this.resolver = context2.getContentResolver();
            this.observer = new SurroundSoundSettingObserver();
            return;
        }
        this.resolver = null;
        this.observer = null;
    }

    /* access modifiers changed from: private */
    public void maybeNotifyAudioCapabilityChanged(AudioCapabilities audioCapabilities2) {
        if (!audioCapabilities2.equals(this.audioCapabilities)) {
            this.audioCapabilities = audioCapabilities2;
            this.listener.onAudioCapabilitiesChanged(audioCapabilities2);
        }
    }

    public AudioCapabilities register() {
        AudioCapabilities capabilities = AudioCapabilities.getCapabilities(this.context, this.receiver == null ? null : this.context.registerReceiver(this.receiver, new IntentFilter("android.media.action.HDMI_AUDIO_PLUG")));
        this.audioCapabilities = capabilities;
        this.currentHDMIAudioCapabilities = capabilities;
        if (!(this.resolver == null || this.observer == null)) {
            this.resolver.registerContentObserver(Settings.Global.getUriFor(AudioCapabilities.EXTERNAL_SURROUND_SOUND_ENABLED), true, this.observer);
        }
        return this.audioCapabilities;
    }

    public void unregister() {
        if (this.receiver != null) {
            this.context.unregisterReceiver(this.receiver);
        }
        if (this.resolver != null && this.observer != null) {
            this.resolver.unregisterContentObserver(this.observer);
        }
    }
}
