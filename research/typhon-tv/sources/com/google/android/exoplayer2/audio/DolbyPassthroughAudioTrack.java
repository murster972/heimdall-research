package com.google.android.exoplayer2.audio;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import java.util.concurrent.Semaphore;

public final class DolbyPassthroughAudioTrack extends AudioTrack {
    private static final int BUFFER_COUNT = 2;
    private static final int MSG_FLUSH_TRACK = 4;
    private static final int MSG_PAUSE_TRACK = 2;
    private static final int MSG_PLAY_TRACK = 3;
    private static final int MSG_RELEASE_TRACK = 6;
    private static final int MSG_STOP_TRACK = 5;
    private static final int MSG_WRITE_TO_TRACK = 1;
    private static final String TRACK_HANDLER_THREAD_NAME = "dolbyTrackHandlerThread";
    /* access modifiers changed from: private */
    public final String TAG;
    /* access modifiers changed from: private */
    public byte[][] audioBuffer;
    private int nextBufferIndex;
    /* access modifiers changed from: private */
    public Semaphore pendingWriteSem;
    private Handler trackHandler;
    /* access modifiers changed from: private */
    public ConditionVariable trackHandlerGate;
    private HandlerThread trackHandlerThread;

    public DolbyPassthroughAudioTrack(int i, int i2, int i3, int i4, int i5, int i6) throws IllegalArgumentException {
        this(i, i2, i3, i4, i5, i6, 0);
    }

    public DolbyPassthroughAudioTrack(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        super(i, i2, i3, i4, i5, i6, i7);
        this.TAG = DolbyPassthroughAudioTrack.class.getSimpleName();
        this.trackHandlerThread = null;
        this.trackHandler = null;
        this.trackHandlerGate = null;
        this.pendingWriteSem = null;
        this.audioBuffer = null;
        this.nextBufferIndex = 0;
        initialize();
    }

    public DolbyPassthroughAudioTrack(AudioAttributes audioAttributes, AudioFormat audioFormat, int i, int i2, int i3) {
        super(audioAttributes, audioFormat, i, i2, i3);
        this.TAG = DolbyPassthroughAudioTrack.class.getSimpleName();
        this.trackHandlerThread = null;
        this.trackHandler = null;
        this.trackHandlerGate = null;
        this.pendingWriteSem = null;
        this.audioBuffer = null;
        this.nextBufferIndex = 0;
        initialize();
    }

    private void initialize() {
        Log.i(this.TAG, "initialize");
        this.trackHandlerGate = new ConditionVariable(true);
        this.trackHandlerThread = new HandlerThread(TRACK_HANDLER_THREAD_NAME);
        this.pendingWriteSem = new Semaphore(2);
        this.audioBuffer = new byte[2][];
        this.trackHandlerThread.start();
        this.trackHandler = new Handler(this.trackHandlerThread.getLooper()) {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        int unused = DolbyPassthroughAudioTrack.super.write(DolbyPassthroughAudioTrack.this.audioBuffer[message.arg2], 0, message.arg1);
                        DolbyPassthroughAudioTrack.this.pendingWriteSem.release();
                        return;
                    case 2:
                        Log.i(DolbyPassthroughAudioTrack.this.TAG, "pausing track");
                        DolbyPassthroughAudioTrack.super.pause();
                        DolbyPassthroughAudioTrack.this.trackHandlerGate.open();
                        return;
                    case 3:
                        Log.i(DolbyPassthroughAudioTrack.this.TAG, "playing track");
                        DolbyPassthroughAudioTrack.super.play();
                        DolbyPassthroughAudioTrack.this.trackHandlerGate.open();
                        return;
                    case 4:
                        Log.i(DolbyPassthroughAudioTrack.this.TAG, "flushing track");
                        DolbyPassthroughAudioTrack.super.flush();
                        DolbyPassthroughAudioTrack.this.trackHandlerGate.open();
                        return;
                    case 5:
                        Log.i(DolbyPassthroughAudioTrack.this.TAG, "stopping track");
                        DolbyPassthroughAudioTrack.super.stop();
                        DolbyPassthroughAudioTrack.this.trackHandlerGate.open();
                        return;
                    case 6:
                        Log.i(DolbyPassthroughAudioTrack.this.TAG, "releasing track");
                        if (DolbyPassthroughAudioTrack.super.getPlayState() != 1) {
                            Log.i(DolbyPassthroughAudioTrack.this.TAG, "not in stopped state...stopping");
                            DolbyPassthroughAudioTrack.super.stop();
                        }
                        DolbyPassthroughAudioTrack.super.release();
                        DolbyPassthroughAudioTrack.this.trackHandlerGate.open();
                        return;
                    default:
                        Log.w(DolbyPassthroughAudioTrack.this.TAG, "unknown message..ignoring!!!");
                        return;
                }
            }
        };
    }

    public void flush() throws IllegalStateException {
        Log.i(this.TAG, "flush");
        this.trackHandlerGate.close();
        this.trackHandler.sendMessage(this.trackHandler.obtainMessage(4));
        this.trackHandlerGate.block();
    }

    public void pause() throws IllegalStateException {
        Log.i(this.TAG, "pause");
        this.trackHandlerGate.close();
        this.trackHandler.sendMessage(this.trackHandler.obtainMessage(2));
        this.trackHandlerGate.block();
    }

    public void play() throws IllegalStateException {
        Log.i(this.TAG, "play");
        this.trackHandlerGate.close();
        this.trackHandler.sendMessage(this.trackHandler.obtainMessage(3));
        this.trackHandlerGate.block();
    }

    public void release() {
        Log.i(this.TAG, "release");
        this.trackHandlerGate.close();
        this.trackHandler.sendMessage(this.trackHandler.obtainMessage(6));
        this.trackHandlerGate.block();
        this.trackHandlerThread.quit();
        this.trackHandlerThread = null;
        this.trackHandler = null;
        this.trackHandlerGate = null;
        this.pendingWriteSem = null;
        this.audioBuffer = null;
    }

    public void stop() throws IllegalStateException {
        Log.i(this.TAG, "stop");
        if (getPlayState() == 1) {
            Log.i(this.TAG, "already in stopped state");
            return;
        }
        this.trackHandlerGate.close();
        this.trackHandler.sendMessage(this.trackHandler.obtainMessage(5));
        this.trackHandlerGate.block();
    }

    public int write(byte[] bArr, int i, int i2) {
        if (getPlayState() != 3) {
            Log.w(this.TAG, "not in play state..not writing buffer now...");
            return 0;
        } else if (!this.pendingWriteSem.tryAcquire()) {
            return 0;
        } else {
            if (this.audioBuffer[this.nextBufferIndex] == null || this.audioBuffer[this.nextBufferIndex].length < i2) {
                this.audioBuffer[this.nextBufferIndex] = new byte[i2];
            }
            System.arraycopy(bArr, i, this.audioBuffer[this.nextBufferIndex], 0, i2);
            this.trackHandler.sendMessage(this.trackHandler.obtainMessage(1, i2, this.nextBufferIndex));
            this.nextBufferIndex = (this.nextBufferIndex + 1) % 2;
            return i2;
        }
    }
}
