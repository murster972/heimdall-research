package com.google.android.exoplayer2.drm;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public final class DrmInitData implements Parcelable, Comparator<SchemeData> {
    public static final Parcelable.Creator<DrmInitData> CREATOR = new Parcelable.Creator<DrmInitData>() {
        public DrmInitData createFromParcel(Parcel parcel) {
            return new DrmInitData(parcel);
        }

        public DrmInitData[] newArray(int i) {
            return new DrmInitData[i];
        }
    };
    private int hashCode;
    public final int schemeDataCount;
    private final SchemeData[] schemeDatas;

    public static final class SchemeData implements Parcelable {
        public static final Parcelable.Creator<SchemeData> CREATOR = new Parcelable.Creator<SchemeData>() {
            public SchemeData createFromParcel(Parcel parcel) {
                return new SchemeData(parcel);
            }

            public SchemeData[] newArray(int i) {
                return new SchemeData[i];
            }
        };
        public final byte[] data;
        private int hashCode;
        public final String mimeType;
        public final boolean requiresSecureDecryption;
        public final String type;
        /* access modifiers changed from: private */
        public final UUID uuid;

        SchemeData(Parcel parcel) {
            this.uuid = new UUID(parcel.readLong(), parcel.readLong());
            this.type = parcel.readString();
            this.mimeType = parcel.readString();
            this.data = parcel.createByteArray();
            this.requiresSecureDecryption = parcel.readByte() != 0;
        }

        public SchemeData(UUID uuid2, String str, String str2, byte[] bArr) {
            this(uuid2, str, str2, bArr, false);
        }

        public SchemeData(UUID uuid2, String str, String str2, byte[] bArr, boolean z) {
            this.uuid = (UUID) Assertions.checkNotNull(uuid2);
            this.type = str;
            this.mimeType = (String) Assertions.checkNotNull(str2);
            this.data = (byte[]) Assertions.checkNotNull(bArr);
            this.requiresSecureDecryption = z;
        }

        public SchemeData copyWithSchemeType(String str) {
            if (Util.areEqual(this.type, str)) {
                return this;
            }
            return new SchemeData(this.uuid, str, this.mimeType, this.data, this.requiresSecureDecryption);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof SchemeData)) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            SchemeData schemeData = (SchemeData) obj;
            return this.mimeType.equals(schemeData.mimeType) && Util.areEqual(this.uuid, schemeData.uuid) && Util.areEqual(this.type, schemeData.type) && Arrays.equals(this.data, schemeData.data);
        }

        public int hashCode() {
            if (this.hashCode == 0) {
                this.hashCode = (((((this.uuid.hashCode() * 31) + (this.type == null ? 0 : this.type.hashCode())) * 31) + this.mimeType.hashCode()) * 31) + Arrays.hashCode(this.data);
            }
            return this.hashCode;
        }

        public boolean matches(UUID uuid2) {
            return C.UUID_NIL.equals(this.uuid) || uuid2.equals(this.uuid);
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeLong(this.uuid.getMostSignificantBits());
            parcel.writeLong(this.uuid.getLeastSignificantBits());
            parcel.writeString(this.type);
            parcel.writeString(this.mimeType);
            parcel.writeByteArray(this.data);
            parcel.writeByte((byte) (this.requiresSecureDecryption ? 1 : 0));
        }
    }

    DrmInitData(Parcel parcel) {
        this.schemeDatas = (SchemeData[]) parcel.createTypedArray(SchemeData.CREATOR);
        this.schemeDataCount = this.schemeDatas.length;
    }

    public DrmInitData(List<SchemeData> list) {
        this(false, (SchemeData[]) list.toArray(new SchemeData[list.size()]));
    }

    private DrmInitData(boolean z, SchemeData... schemeDataArr) {
        schemeDataArr = z ? (SchemeData[]) schemeDataArr.clone() : schemeDataArr;
        Arrays.sort(schemeDataArr, this);
        for (int i = 1; i < schemeDataArr.length; i++) {
            if (schemeDataArr[i - 1].uuid.equals(schemeDataArr[i].uuid)) {
                throw new IllegalArgumentException("Duplicate data for uuid: " + schemeDataArr[i].uuid);
            }
        }
        this.schemeDatas = schemeDataArr;
        this.schemeDataCount = schemeDataArr.length;
    }

    public DrmInitData(SchemeData... schemeDataArr) {
        this(true, schemeDataArr);
    }

    public int compare(SchemeData schemeData, SchemeData schemeData2) {
        return C.UUID_NIL.equals(schemeData.uuid) ? C.UUID_NIL.equals(schemeData2.uuid) ? 0 : 1 : schemeData.uuid.compareTo(schemeData2.uuid);
    }

    public DrmInitData copyWithSchemeType(String str) {
        boolean z = false;
        SchemeData[] schemeDataArr = this.schemeDatas;
        int length = schemeDataArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (!Util.areEqual(schemeDataArr[i].type, str)) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            return this;
        }
        SchemeData[] schemeDataArr2 = new SchemeData[this.schemeDatas.length];
        for (int i2 = 0; i2 < schemeDataArr2.length; i2++) {
            schemeDataArr2[i2] = this.schemeDatas[i2].copyWithSchemeType(str);
        }
        return new DrmInitData(schemeDataArr2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.schemeDatas, ((DrmInitData) obj).schemeDatas);
    }

    public SchemeData get(int i) {
        return this.schemeDatas[i];
    }

    public SchemeData get(UUID uuid) {
        for (SchemeData schemeData : this.schemeDatas) {
            if (schemeData.matches(uuid)) {
                return schemeData;
            }
        }
        return null;
    }

    public int hashCode() {
        if (this.hashCode == 0) {
            this.hashCode = Arrays.hashCode(this.schemeDatas);
        }
        return this.hashCode;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.schemeDatas, 0);
    }
}
