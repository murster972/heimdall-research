package com.google.android.exoplayer2.util;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

public final class AmazonQuirks {
    private static final String AMAZON = "Amazon";
    private static final int AUDIO_HARDWARE_LATENCY_FOR_TABLETS = 90000;
    private static final String DEVICEMODEL = Build.MODEL;
    private static final String FIRETV_GEN1_DEVICE_MODEL = "AFTB";
    private static final String FIRETV_GEN2_DEVICE_MODEL = "AFTS";
    private static final String FIRETV_STICK_DEVICE_MODEL = "AFTM";
    private static final String FIRETV_STICK_GEN2_DEVICE_MODEL = "AFTT";
    private static final String FIRE_PHONE_DEVICE_MODEL = "SD";
    private static final String KINDLE_TABLET_DEVICE_MODEL = "KF";
    private static final String MANUFACTURER = Build.MANUFACTURER;
    private static final int MAX_INPUT_SECURE_AVC_SIZE_FIRETV_GEN2 = 2936012;
    private static final String SOHO_DEVICE_MODEL = "KFSOWI";
    private static final String TAG = AmazonQuirks.class.getSimpleName();
    private static final boolean isAmazonDevice = MANUFACTURER.equalsIgnoreCase(AMAZON);
    private static final boolean isFirePhone = (isAmazonDevice && DEVICEMODEL.startsWith(FIRE_PHONE_DEVICE_MODEL));
    private static final boolean isFireTVGen1 = (isAmazonDevice && DEVICEMODEL.equalsIgnoreCase(FIRETV_GEN1_DEVICE_MODEL));
    private static final boolean isFireTVGen2 = (isAmazonDevice && DEVICEMODEL.equalsIgnoreCase(FIRETV_GEN2_DEVICE_MODEL));
    private static final boolean isFireTVStick = (isAmazonDevice && DEVICEMODEL.equalsIgnoreCase(FIRETV_STICK_DEVICE_MODEL));
    private static final boolean isKindleTablet = (isAmazonDevice && DEVICEMODEL.startsWith(KINDLE_TABLET_DEVICE_MODEL));
    private static final boolean isSOHOKindleTablet;

    static {
        boolean z = true;
        if (!isAmazonDevice || !DEVICEMODEL.equalsIgnoreCase(SOHO_DEVICE_MODEL)) {
            z = false;
        }
        isSOHOKindleTablet = z;
    }

    private AmazonQuirks() {
    }

    public static boolean codecNeedsEosPropagationWorkaround(String str) {
        boolean z = isFireTVGen2() && str.endsWith(".secure");
        if (z) {
            Log.i(TAG, "Codec Needs EOS Propagation Workaround " + str);
        }
        return z;
    }

    public static int getAudioHWLatency() {
        return AUDIO_HARDWARE_LATENCY_FOR_TABLETS;
    }

    public static boolean isAmazonDevice() {
        return isAmazonDevice;
    }

    public static boolean isDolbyPassthroughQuirkEnabled() {
        return isFireTVGen1Family();
    }

    public static boolean isFireTVGen1Family() {
        return isFireTVGen1 || isFireTVStick;
    }

    public static boolean isFireTVGen2() {
        return isFireTVGen2;
    }

    public static boolean isLatencyQuirkEnabled() {
        return Util.SDK_INT <= 19 && (isKindleTablet || isFirePhone);
    }

    public static boolean isMaxInputSizeSupported(String str, int i) {
        if (isFireTVGen2) {
            return TextUtils.isEmpty(str) || !str.endsWith("AVC.secure") || i <= MAX_INPUT_SECURE_AVC_SIZE_FIRETV_GEN2;
        }
        if (isSOHOKindleTablet) {
            return TextUtils.isEmpty(str) || !str.equalsIgnoreCase("OMX.TI.DUCATI1.VIDEO.DECODER");
        }
        return true;
    }

    public static boolean useDefaultPassthroughDecoder() {
        if (isFireTVGen1Family()) {
            Log.i(TAG, "Using platform Dolby decoder");
            return false;
        }
        Log.i(TAG, "Using default Dolby pass-through decoder");
        return true;
    }
}
