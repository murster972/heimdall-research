package com.google.android.exoplayer2.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.upstream.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.time.TimeZones;

public final class Util {
    private static final int[] CRC32_BYTES_MSBF = {0, 79764919, 159529838, 222504665, 319059676, 398814059, 445009330, 507990021, 638119352, 583659535, 797628118, 726387553, 890018660, 835552979, 1015980042, 944750013, 1276238704, 1221641927, 1167319070, 1095957929, 1595256236, 1540665371, 1452775106, 1381403509, 1780037320, 1859660671, 1671105958, 1733955601, 2031960084, 2111593891, 1889500026, 1952343757, -1742489888, -1662866601, -1851683442, -1788833735, -1960329156, -1880695413, -2103051438, -2040207643, -1104454824, -1159051537, -1213636554, -1284997759, -1389417084, -1444007885, -1532160278, -1603531939, -734892656, -789352409, -575645954, -646886583, -952755380, -1007220997, -827056094, -898286187, -231047128, -151282273, -71779514, -8804623, -515967244, -436212925, -390279782, -327299027, 881225847, 809987520, 1023691545, 969234094, 662832811, 591600412, 771767749, 717299826, 311336399, 374308984, 453813921, 533576470, 25881363, 88864420, 134795389, 214552010, 2023205639, 2086057648, 1897238633, 1976864222, 1804852699, 1867694188, 1645340341, 1724971778, 1587496639, 1516133128, 1461550545, 1406951526, 1302016099, 1230646740, 1142491917, 1087903418, -1398421865, -1469785312, -1524105735, -1578704818, -1079922613, -1151291908, -1239184603, -1293773166, -1968362705, -1905510760, -2094067647, -2014441994, -1716953613, -1654112188, -1876203875, -1796572374, -525066777, -462094256, -382327159, -302564546, -206542021, -143559028, -97365931, -17609246, -960696225, -1031934488, -817968335, -872425850, -709327229, -780559564, -600130067, -654598054, 1762451694, 1842216281, 1619975040, 1682949687, 2047383090, 2127137669, 1938468188, 2001449195, 1325665622, 1271206113, 1183200824, 1111960463, 1543535498, 1489069629, 1434599652, 1363369299, 622672798, 568075817, 748617968, 677256519, 907627842, 853037301, 1067152940, 995781531, 51762726, 131386257, 177728840, 240578815, 269590778, 349224269, 429104020, 491947555, -248556018, -168932423, -122852000, -60002089, -500490030, -420856475, -341238852, -278395381, -685261898, -739858943, -559578920, -630940305, -1004286614, -1058877219, -845023740, -916395085, -1119974018, -1174433591, -1262701040, -1333941337, -1371866206, -1426332139, -1481064244, -1552294533, -1690935098, -1611170447, -1833673816, -1770699233, -2009983462, -1930228819, -2119160460, -2056179517, 1569362073, 1498123566, 1409854455, 1355396672, 1317987909, 1246755826, 1192025387, 1137557660, 2072149281, 2135122070, 1912620623, 1992383480, 1753615357, 1816598090, 1627664531, 1707420964, 295390185, 358241886, 404320391, 483945776, 43990325, 106832002, 186451547, 266083308, 932423249, 861060070, 1041341759, 986742920, 613929101, 542559546, 756411363, 701822548, -978770311, -1050133554, -869589737, -924188512, -693284699, -764654318, -550540341, -605129092, -475935807, -413084042, -366743377, -287118056, -257573603, -194731862, -114850189, -35218492, -1984365303, -1921392450, -2143631769, -2063868976, -1698919467, -1635936670, -1824608069, -1744851700, -1347415887, -1418654458, -1506661409, -1561119128, -1129027987, -1200260134, -1254728445, -1309196108};
    public static final String DEVICE = Build.DEVICE;
    public static final String DEVICE_DEBUG_INFO = (DEVICE + ", " + MODEL + ", " + MANUFACTURER + ", " + SDK_INT);
    private static final Pattern ESCAPED_CHARACTER_PATTERN = Pattern.compile("%([A-Fa-f0-9]{2})");
    public static final String MANUFACTURER = Build.MANUFACTURER;
    public static final String MODEL = Build.MODEL;
    public static final int SDK_INT = ((Build.VERSION.SDK_INT == 25 && Build.VERSION.CODENAME.charAt(0) == 'O') ? 26 : Build.VERSION.SDK_INT);
    private static final String TAG = "Util";
    private static final Pattern XS_DATE_TIME_PATTERN = Pattern.compile("(\\d\\d\\d\\d)\\-(\\d\\d)\\-(\\d\\d)[Tt](\\d\\d):(\\d\\d):(\\d\\d)([\\.,](\\d+))?([Zz]|((\\+|\\-)(\\d?\\d):?(\\d\\d)))?");
    private static final Pattern XS_DURATION_PATTERN = Pattern.compile("^(-)?P(([0-9]*)Y)?(([0-9]*)M)?(([0-9]*)D)?(T(([0-9]*)H)?(([0-9]*)M)?(([0-9.]*)S)?)?$");

    private Util() {
    }

    public static boolean areEqual(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> int binarySearchCeil(java.util.List<? extends java.lang.Comparable<? super T>> r3, T r4, boolean r5, boolean r6) {
        /*
            int r0 = java.util.Collections.binarySearch(r3, r4)
            if (r0 >= 0) goto L_0x0015
            r0 = r0 ^ -1
        L_0x0008:
            if (r6 == 0) goto L_0x0014
            int r2 = r3.size()
            int r2 = r2 + -1
            int r0 = java.lang.Math.min(r2, r0)
        L_0x0014:
            return r0
        L_0x0015:
            int r1 = r3.size()
        L_0x0019:
            int r0 = r0 + 1
            if (r0 >= r1) goto L_0x0029
            java.lang.Object r2 = r3.get(r0)
            java.lang.Comparable r2 = (java.lang.Comparable) r2
            int r2 = r2.compareTo(r4)
            if (r2 == 0) goto L_0x0019
        L_0x0029:
            if (r5 == 0) goto L_0x0008
            int r0 = r0 + -1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.Util.binarySearchCeil(java.util.List, java.lang.Object, boolean, boolean):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int binarySearchCeil(long[] r5, long r6, boolean r8, boolean r9) {
        /*
            int r0 = java.util.Arrays.binarySearch(r5, r6)
            if (r0 >= 0) goto L_0x0012
            r0 = r0 ^ -1
        L_0x0008:
            if (r9 == 0) goto L_0x0011
            int r1 = r5.length
            int r1 = r1 + -1
            int r0 = java.lang.Math.min(r1, r0)
        L_0x0011:
            return r0
        L_0x0012:
            int r0 = r0 + 1
            int r1 = r5.length
            if (r0 >= r1) goto L_0x001d
            r2 = r5[r0]
            int r1 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r1 == 0) goto L_0x0012
        L_0x001d:
            if (r8 == 0) goto L_0x0008
            int r0 = r0 + -1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.Util.binarySearchCeil(long[], long, boolean, boolean):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> int binarySearchFloor(java.util.List<? extends java.lang.Comparable<? super T>> r2, T r3, boolean r4, boolean r5) {
        /*
            int r0 = java.util.Collections.binarySearch(r2, r3)
            if (r0 >= 0) goto L_0x0011
            int r1 = r0 + 2
            int r0 = -r1
        L_0x0009:
            if (r5 == 0) goto L_0x0010
            r1 = 0
            int r0 = java.lang.Math.max(r1, r0)
        L_0x0010:
            return r0
        L_0x0011:
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0021
            java.lang.Object r1 = r2.get(r0)
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r1.compareTo(r3)
            if (r1 == 0) goto L_0x0011
        L_0x0021:
            if (r4 == 0) goto L_0x0009
            int r0 = r0 + 1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.Util.binarySearchFloor(java.util.List, java.lang.Object, boolean, boolean):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int binarySearchFloor(int[] r2, int r3, boolean r4, boolean r5) {
        /*
            int r0 = java.util.Arrays.binarySearch(r2, r3)
            if (r0 >= 0) goto L_0x0011
            int r1 = r0 + 2
            int r0 = -r1
        L_0x0009:
            if (r5 == 0) goto L_0x0010
            r1 = 0
            int r0 = java.lang.Math.max(r1, r0)
        L_0x0010:
            return r0
        L_0x0011:
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0019
            r1 = r2[r0]
            if (r1 == r3) goto L_0x0011
        L_0x0019:
            if (r4 == 0) goto L_0x0009
            int r0 = r0 + 1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.Util.binarySearchFloor(int[], int, boolean, boolean):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int binarySearchFloor(long[] r5, long r6, boolean r8, boolean r9) {
        /*
            int r0 = java.util.Arrays.binarySearch(r5, r6)
            if (r0 >= 0) goto L_0x0011
            int r1 = r0 + 2
            int r0 = -r1
        L_0x0009:
            if (r9 == 0) goto L_0x0010
            r1 = 0
            int r0 = java.lang.Math.max(r1, r0)
        L_0x0010:
            return r0
        L_0x0011:
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x001b
            r2 = r5[r0]
            int r1 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r1 == 0) goto L_0x0011
        L_0x001b:
            if (r8 == 0) goto L_0x0009
            int r0 = r0 + 1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.Util.binarySearchFloor(long[], long, boolean, boolean):int");
    }

    public static int ceilDivide(int i, int i2) {
        return ((i + i2) - 1) / i2;
    }

    public static long ceilDivide(long j, long j2) {
        return ((j + j2) - 1) / j2;
    }

    public static void closeQuietly(DataSource dataSource) {
        if (dataSource != null) {
            try {
                dataSource.close();
            } catch (IOException e) {
            }
        }
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    public static float constrainValue(float f, float f2, float f3) {
        return Math.max(f2, Math.min(f, f3));
    }

    public static int constrainValue(int i, int i2, int i3) {
        return Math.max(i2, Math.min(i, i3));
    }

    public static long constrainValue(long j, long j2, long j3) {
        return Math.max(j2, Math.min(j, j3));
    }

    public static boolean contains(Object[] objArr, Object obj) {
        for (Object areEqual : objArr) {
            if (areEqual(areEqual, obj)) {
                return true;
            }
        }
        return false;
    }

    public static int crc(byte[] bArr, int i, int i2, int i3) {
        for (int i4 = i; i4 < i2; i4++) {
            i3 = (i3 << 8) ^ CRC32_BYTES_MSBF[((i3 >>> 24) ^ (bArr[i4] & 255)) & 255];
        }
        return i3;
    }

    public static File createTempDirectory(Context context, String str) throws IOException {
        File createTempFile = File.createTempFile(str, (String) null, context.getCacheDir());
        createTempFile.delete();
        createTempFile.mkdir();
        return createTempFile;
    }

    public static String escapeFileName(String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (shouldEscapeCharacter(str.charAt(i2))) {
                i++;
            }
        }
        if (i == 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder((i * 2) + length);
        int i3 = 0;
        while (i > 0) {
            int i4 = i3 + 1;
            char charAt = str.charAt(i3);
            if (shouldEscapeCharacter(charAt)) {
                sb.append('%').append(Integer.toHexString(charAt));
                i--;
            } else {
                sb.append(charAt);
            }
            i3 = i4;
        }
        if (i3 < length) {
            sb.append(str, i3, length);
        }
        int i5 = i3;
        return sb.toString();
    }

    public static int getAudioContentTypeForStreamType(int i) {
        switch (i) {
            case 0:
                return 1;
            case 1:
            case 2:
            case 4:
            case 5:
            case 8:
                return 4;
            default:
                return 2;
        }
    }

    public static int getAudioUsageForStreamType(int i) {
        switch (i) {
            case 0:
                return 2;
            case 1:
                return 13;
            case 2:
                return 6;
            case 4:
                return 4;
            case 5:
                return 5;
            case 8:
                return 3;
            default:
                return 1;
        }
    }

    public static byte[] getBytesFromHexString(String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) ((Character.digit(str.charAt(i2), 16) << 4) + Character.digit(str.charAt(i2 + 1), 16));
        }
        return bArr;
    }

    public static String getCommaDelimitedSimpleClassNames(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < objArr.length; i++) {
            sb.append(objArr[i].getClass().getSimpleName());
            if (i < objArr.length - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    public static int getDefaultBufferSize(int i) {
        switch (i) {
            case 0:
                return 16777216;
            case 1:
                return C.DEFAULT_AUDIO_BUFFER_SIZE;
            case 2:
                return C.DEFAULT_VIDEO_BUFFER_SIZE;
            case 3:
            case 4:
                return 131072;
            default:
                throw new IllegalStateException();
        }
    }

    @TargetApi(16)
    private static void getDisplaySizeV16(Display display, Point point) {
        display.getSize(point);
    }

    @TargetApi(17)
    private static void getDisplaySizeV17(Display display, Point point) {
        display.getRealSize(point);
    }

    @TargetApi(23)
    private static void getDisplaySizeV23(Display display, Point point) {
        Display.Mode mode = display.getMode();
        point.x = mode.getPhysicalWidth();
        point.y = mode.getPhysicalHeight();
    }

    private static void getDisplaySizeV9(Display display, Point point) {
        point.x = display.getWidth();
        point.y = display.getHeight();
    }

    public static int getIntegerCodeForString(String str) {
        int length = str.length();
        Assertions.checkArgument(length <= 4);
        char c = 0;
        for (int i = 0; i < length; i++) {
            c = (c << 8) | str.charAt(i);
        }
        return c;
    }

    public static int getPcmEncoding(int i) {
        switch (i) {
            case 8:
                return 3;
            case 16:
                return 2;
            case 24:
                return Integer.MIN_VALUE;
            case 32:
                return 1073741824;
            default:
                return 0;
        }
    }

    public static int getPcmFrameSize(int i, int i2) {
        switch (i) {
            case Integer.MIN_VALUE:
                return i2 * 3;
            case 2:
                return i2 * 2;
            case 3:
                return i2;
            case 1073741824:
                return i2 * 4;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static Point getPhysicalDisplaySize(Context context) {
        return getPhysicalDisplaySize(context, ((WindowManager) context.getSystemService("window")).getDefaultDisplay());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v25, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Point getPhysicalDisplaySize(android.content.Context r13, android.view.Display r14) {
        /*
            int r9 = SDK_INT
            r10 = 25
            if (r9 >= r10) goto L_0x00bf
            int r9 = r14.getDisplayId()
            if (r9 != 0) goto L_0x00bf
            java.lang.String r9 = "Sony"
            java.lang.String r10 = MANUFACTURER
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x0039
            java.lang.String r9 = MODEL
            java.lang.String r10 = "BRAVIA"
            boolean r9 = r9.startsWith(r10)
            if (r9 == 0) goto L_0x0039
            android.content.pm.PackageManager r9 = r13.getPackageManager()
            java.lang.String r10 = "com.sony.dtv.hardware.panel.qfhd"
            boolean r9 = r9.hasSystemFeature(r10)
            if (r9 == 0) goto L_0x0039
            android.graphics.Point r1 = new android.graphics.Point
            r9 = 3840(0xf00, float:5.381E-42)
            r10 = 2160(0x870, float:3.027E-42)
            r1.<init>(r9, r10)
        L_0x0038:
            return r1
        L_0x0039:
            java.lang.String r9 = "NVIDIA"
            java.lang.String r10 = MANUFACTURER
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x00bf
            java.lang.String r9 = MODEL
            java.lang.String r10 = "SHIELD"
            boolean r9 = r9.contains(r10)
            if (r9 == 0) goto L_0x00bf
            r5 = 0
            java.lang.String r9 = "android.os.SystemProperties"
            java.lang.Class r7 = java.lang.Class.forName(r9)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r9 = "get"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x00cf }
            r11 = 0
            java.lang.Class<java.lang.String> r12 = java.lang.String.class
            r10[r11] = r12     // Catch:{ Exception -> 0x00cf }
            java.lang.reflect.Method r3 = r7.getMethod(r9, r10)     // Catch:{ Exception -> 0x00cf }
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x00cf }
            r10 = 0
            java.lang.String r11 = "sys.display-size"
            r9[r10] = r11     // Catch:{ Exception -> 0x00cf }
            java.lang.Object r9 = r3.invoke(r7, r9)     // Catch:{ Exception -> 0x00cf }
            r0 = r9
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00cf }
            r5 = r0
        L_0x0077:
            boolean r9 = android.text.TextUtils.isEmpty(r5)
            if (r9 != 0) goto L_0x00bf
            java.lang.String r9 = r5.trim()     // Catch:{ NumberFormatException -> 0x00a4 }
            java.lang.String r10 = "x"
            java.lang.String[] r6 = r9.split(r10)     // Catch:{ NumberFormatException -> 0x00a4 }
            int r9 = r6.length     // Catch:{ NumberFormatException -> 0x00a4 }
            r10 = 2
            if (r9 != r10) goto L_0x00a5
            r9 = 0
            r9 = r6[r9]     // Catch:{ NumberFormatException -> 0x00a4 }
            int r8 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x00a4 }
            r9 = 1
            r9 = r6[r9]     // Catch:{ NumberFormatException -> 0x00a4 }
            int r4 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x00a4 }
            if (r8 <= 0) goto L_0x00a5
            if (r4 <= 0) goto L_0x00a5
            android.graphics.Point r1 = new android.graphics.Point     // Catch:{ NumberFormatException -> 0x00a4 }
            r1.<init>(r8, r4)     // Catch:{ NumberFormatException -> 0x00a4 }
            goto L_0x0038
        L_0x00a4:
            r9 = move-exception
        L_0x00a5:
            java.lang.String r9 = "Util"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Invalid sys.display-size: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r5)
            java.lang.String r10 = r10.toString()
            android.util.Log.e(r9, r10)
        L_0x00bf:
            android.graphics.Point r1 = new android.graphics.Point
            r1.<init>()
            int r9 = SDK_INT
            r10 = 23
            if (r9 < r10) goto L_0x00da
            getDisplaySizeV23(r14, r1)
            goto L_0x0038
        L_0x00cf:
            r2 = move-exception
            java.lang.String r9 = "Util"
            java.lang.String r10 = "Failed to read sys.display-size"
            android.util.Log.e(r9, r10, r2)
            goto L_0x0077
        L_0x00da:
            int r9 = SDK_INT
            r10 = 17
            if (r9 < r10) goto L_0x00e5
            getDisplaySizeV17(r14, r1)
            goto L_0x0038
        L_0x00e5:
            int r9 = SDK_INT
            r10 = 16
            if (r9 < r10) goto L_0x00f0
            getDisplaySizeV16(r14, r1)
            goto L_0x0038
        L_0x00f0:
            getDisplaySizeV9(r14, r1)
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.Util.getPhysicalDisplaySize(android.content.Context, android.view.Display):android.graphics.Point");
    }

    public static int getStreamTypeForAudioUsage(int i) {
        switch (i) {
            case 2:
                return 0;
            case 3:
                return 8;
            case 4:
                return 4;
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
                return 5;
            case 6:
                return 2;
            case 13:
                return 1;
            default:
                return 3;
        }
    }

    public static String getStringForTime(StringBuilder sb, Formatter formatter, long j) {
        if (j == C.TIME_UNSET) {
            j = 0;
        }
        long j2 = (500 + j) / 1000;
        long j3 = j2 % 60;
        long j4 = (j2 / 60) % 60;
        long j5 = j2 / 3600;
        sb.setLength(0);
        if (j5 > 0) {
            return formatter.format("%d:%02d:%02d", new Object[]{Long.valueOf(j5), Long.valueOf(j4), Long.valueOf(j3)}).toString();
        }
        return formatter.format("%02d:%02d", new Object[]{Long.valueOf(j4), Long.valueOf(j3)}).toString();
    }

    public static String getUserAgent(Context context, String str) {
        String str2;
        try {
            str2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            str2 = "?";
        }
        return str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str2 + " (Linux;Android " + Build.VERSION.RELEASE + ") " + ExoPlayerLibraryInfo.VERSION_SLASHY;
    }

    public static byte[] getUtf8Bytes(String str) {
        return str.getBytes(Charset.forName("UTF-8"));
    }

    public static int inferContentType(Uri uri) {
        String path = uri.getPath();
        if (path == null) {
            return 3;
        }
        return inferContentType(path);
    }

    public static int inferContentType(String str) {
        String lowerInvariant = toLowerInvariant(str);
        if (lowerInvariant.endsWith(".mpd")) {
            return 0;
        }
        if (lowerInvariant.endsWith(".m3u8")) {
            return 2;
        }
        return (lowerInvariant.endsWith(".ism") || lowerInvariant.endsWith(".isml") || lowerInvariant.endsWith(".ism/manifest") || lowerInvariant.endsWith(".isml/manifest")) ? 1 : 3;
    }

    public static boolean isLinebreak(int i) {
        return i == 10 || i == 13;
    }

    public static boolean isLocalFileUri(Uri uri) {
        String scheme = uri.getScheme();
        return TextUtils.isEmpty(scheme) || scheme.equals("file");
    }

    @TargetApi(23)
    public static boolean maybeRequestReadExternalStoragePermission(Activity activity, Uri... uriArr) {
        if (SDK_INT < 23) {
            return false;
        }
        int length = uriArr.length;
        int i = 0;
        while (i < length) {
            if (!isLocalFileUri(uriArr[i])) {
                i++;
            } else if (activity.checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == 0) {
                return false;
            } else {
                activity.requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 0);
                return true;
            }
        }
        return false;
    }

    public static ExecutorService newSingleThreadExecutor(final String str) {
        return Executors.newSingleThreadExecutor(new ThreadFactory() {
            public Thread newThread(Runnable runnable) {
                return new Thread(runnable, str);
            }
        });
    }

    public static String normalizeLanguageCode(String str) {
        if (str == null) {
            return null;
        }
        return new Locale(str).getLanguage();
    }

    public static long parseXsDateTime(String str) throws ParserException {
        int parseInt;
        Matcher matcher = XS_DATE_TIME_PATTERN.matcher(str);
        if (!matcher.matches()) {
            throw new ParserException("Invalid date/time format: " + str);
        }
        if (matcher.group(9) == null) {
            parseInt = 0;
        } else if (matcher.group(9).equalsIgnoreCase("Z")) {
            parseInt = 0;
        } else {
            parseInt = (Integer.parseInt(matcher.group(12)) * 60) + Integer.parseInt(matcher.group(13));
            if (matcher.group(11).equals("-")) {
                parseInt *= -1;
            }
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone(TimeZones.GMT_ID));
        gregorianCalendar.clear();
        gregorianCalendar.set(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)), Integer.parseInt(matcher.group(5)), Integer.parseInt(matcher.group(6)));
        if (!TextUtils.isEmpty(matcher.group(8))) {
            gregorianCalendar.set(14, new BigDecimal("0." + matcher.group(8)).movePointRight(3).intValue());
        }
        long timeInMillis = gregorianCalendar.getTimeInMillis();
        return parseInt != 0 ? timeInMillis - ((long) (60000 * parseInt)) : timeInMillis;
    }

    public static long parseXsDuration(String str) {
        Matcher matcher = XS_DURATION_PATTERN.matcher(str);
        if (!matcher.matches()) {
            return (long) (Double.parseDouble(str) * 3600.0d * 1000.0d);
        }
        boolean z = !TextUtils.isEmpty(matcher.group(1));
        String group = matcher.group(3);
        double parseDouble = group != null ? Double.parseDouble(group) * 3.1556908E7d : 0.0d;
        String group2 = matcher.group(5);
        double parseDouble2 = parseDouble + (group2 != null ? Double.parseDouble(group2) * 2629739.0d : 0.0d);
        String group3 = matcher.group(7);
        double parseDouble3 = parseDouble2 + (group3 != null ? Double.parseDouble(group3) * 86400.0d : 0.0d);
        String group4 = matcher.group(10);
        double parseDouble4 = parseDouble3 + (group4 != null ? Double.parseDouble(group4) * 3600.0d : 0.0d);
        String group5 = matcher.group(12);
        double parseDouble5 = parseDouble4 + (group5 != null ? Double.parseDouble(group5) * 60.0d : 0.0d);
        String group6 = matcher.group(14);
        long parseDouble6 = (long) (1000.0d * (parseDouble5 + (group6 != null ? Double.parseDouble(group6) : 0.0d)));
        return z ? -parseDouble6 : parseDouble6;
    }

    public static void recursiveDelete(File file) {
        if (file.isDirectory()) {
            for (File recursiveDelete : file.listFiles()) {
                recursiveDelete(recursiveDelete);
            }
        }
        file.delete();
    }

    public static long scaleLargeTimestamp(long j, long j2, long j3) {
        if (j3 >= j2 && j3 % j2 == 0) {
            return j / (j3 / j2);
        }
        if (j3 < j2 && j2 % j3 == 0) {
            return j * (j2 / j3);
        }
        return (long) (((double) j) * (((double) j2) / ((double) j3)));
    }

    public static long[] scaleLargeTimestamps(List<Long> list, long j, long j2) {
        long[] jArr = new long[list.size()];
        if (j2 >= j && j2 % j == 0) {
            long j3 = j2 / j;
            for (int i = 0; i < jArr.length; i++) {
                jArr[i] = list.get(i).longValue() / j3;
            }
        } else if (j2 >= j || j % j2 != 0) {
            double d = ((double) j) / ((double) j2);
            for (int i2 = 0; i2 < jArr.length; i2++) {
                jArr[i2] = (long) (((double) list.get(i2).longValue()) * d);
            }
        } else {
            long j4 = j / j2;
            for (int i3 = 0; i3 < jArr.length; i3++) {
                jArr[i3] = list.get(i3).longValue() * j4;
            }
        }
        return jArr;
    }

    public static void scaleLargeTimestampsInPlace(long[] jArr, long j, long j2) {
        if (j2 >= j && j2 % j == 0) {
            long j3 = j2 / j;
            for (int i = 0; i < jArr.length; i++) {
                jArr[i] = jArr[i] / j3;
            }
        } else if (j2 >= j || j % j2 != 0) {
            double d = ((double) j) / ((double) j2);
            for (int i2 = 0; i2 < jArr.length; i2++) {
                jArr[i2] = (long) (((double) jArr[i2]) * d);
            }
        } else {
            long j4 = j / j2;
            for (int i3 = 0; i3 < jArr.length; i3++) {
                jArr[i3] = jArr[i3] * j4;
            }
        }
    }

    private static boolean shouldEscapeCharacter(char c) {
        switch (c) {
            case '\"':
            case '%':
            case '*':
            case '/':
            case ':':
            case '<':
            case '>':
            case '?':
            case '\\':
            case '|':
                return true;
            default:
                return false;
        }
    }

    public static void sneakyThrow(Throwable th) {
        sneakyThrowInternal(th);
    }

    private static <T extends Throwable> void sneakyThrowInternal(Throwable th) throws Throwable {
        throw th;
    }

    public static int[] toArray(List<Integer> list) {
        if (list == null) {
            return null;
        }
        int size = list.size();
        int[] iArr = new int[size];
        for (int i = 0; i < size; i++) {
            iArr[i] = list.get(i).intValue();
        }
        return iArr;
    }

    public static byte[] toByteArray(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static String toLowerInvariant(String str) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase(Locale.US);
    }

    public static String unescapeFileName(String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (str.charAt(i2) == '%') {
                i++;
            }
        }
        if (i == 0) {
            return str;
        }
        int i3 = length - (i * 2);
        StringBuilder sb = new StringBuilder(i3);
        Matcher matcher = ESCAPED_CHARACTER_PATTERN.matcher(str);
        int i4 = 0;
        while (i > 0 && matcher.find()) {
            sb.append(str, i4, matcher.start()).append((char) Integer.parseInt(matcher.group(1), 16));
            i4 = matcher.end();
            i--;
        }
        if (i4 < length) {
            sb.append(str, i4, length);
        }
        if (sb.length() != i3) {
            return null;
        }
        return sb.toString();
    }
}
