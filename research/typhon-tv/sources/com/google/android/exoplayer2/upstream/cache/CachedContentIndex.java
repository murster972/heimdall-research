package com.google.android.exoplayer2.upstream.cache;

import android.util.SparseArray;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.AtomicFile;
import com.google.android.exoplayer2.util.ReusableBufferedOutputStream;
import com.google.android.exoplayer2.util.Util;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class CachedContentIndex {
    public static final String FILE_NAME = "cached_content_index.exi";
    private static final int FLAG_ENCRYPTED_INDEX = 1;
    private static final String TAG = "CachedContentIndex";
    private static final int VERSION = 1;
    private final AtomicFile atomicFile;
    private ReusableBufferedOutputStream bufferedOutputStream;
    private boolean changed;
    private final Cipher cipher;
    private final boolean encrypt;
    private final SparseArray<String> idToKey;
    private final HashMap<String, CachedContent> keyToContent;
    private final SecretKeySpec secretKeySpec;

    public CachedContentIndex(File file) {
        this(file, (byte[]) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CachedContentIndex(File file, byte[] bArr) {
        this(file, bArr, bArr != null);
    }

    public CachedContentIndex(File file, byte[] bArr, boolean z) {
        this.encrypt = z;
        if (bArr != null) {
            Assertions.checkArgument(bArr.length == 16);
            try {
                this.cipher = getCipher();
                this.secretKeySpec = new SecretKeySpec(bArr, "AES");
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                throw new IllegalStateException(e);
            }
        } else {
            this.cipher = null;
            this.secretKeySpec = null;
        }
        this.keyToContent = new HashMap<>();
        this.idToKey = new SparseArray<>();
        this.atomicFile = new AtomicFile(new File(file, FILE_NAME));
    }

    private void add(CachedContent cachedContent) {
        this.keyToContent.put(cachedContent.key, cachedContent);
        this.idToKey.put(cachedContent.id, cachedContent.key);
    }

    private CachedContent addNew(String str, long j) {
        CachedContent cachedContent = new CachedContent(getNewId(this.idToKey), str, j);
        addNew(cachedContent);
        return cachedContent;
    }

    private static Cipher getCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
        if (Util.SDK_INT == 18) {
            try {
                return Cipher.getInstance("AES/CBC/PKCS5PADDING", "BC");
            } catch (Throwable th) {
            }
        }
        return Cipher.getInstance("AES/CBC/PKCS5PADDING");
    }

    public static int getNewId(SparseArray<String> sparseArray) {
        int size = sparseArray.size();
        int keyAt = size == 0 ? 0 : sparseArray.keyAt(size - 1) + 1;
        if (keyAt < 0) {
            keyAt = 0;
            while (keyAt < size && keyAt == sparseArray.keyAt(keyAt)) {
                keyAt++;
            }
        }
        return keyAt;
    }

    /* JADX WARNING: Removed duplicated region for block: B:62:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean readFile() {
        /*
            r15 = this;
            r7 = 0
            java.io.BufferedInputStream r9 = new java.io.BufferedInputStream     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            com.google.android.exoplayer2.util.AtomicFile r12 = r15.atomicFile     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            java.io.InputStream r12 = r12.openRead()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            r9.<init>(r12)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            java.io.DataInputStream r8 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            r8.<init>(r9)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            int r11 = r8.readInt()     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r12 = 1
            if (r11 == r12) goto L_0x0020
            r12 = 0
            if (r8 == 0) goto L_0x001e
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r8)
        L_0x001e:
            r7 = r8
        L_0x001f:
            return r12
        L_0x0020:
            int r3 = r8.readInt()     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r12 = r3 & 1
            if (r12 == 0) goto L_0x007d
            javax.crypto.Cipher r12 = r15.cipher     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            if (r12 != 0) goto L_0x0034
            r12 = 0
            if (r8 == 0) goto L_0x0032
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r8)
        L_0x0032:
            r7 = r8
            goto L_0x001f
        L_0x0034:
            r12 = 16
            byte[] r6 = new byte[r12]     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r8.readFully(r6)     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            javax.crypto.spec.IvParameterSpec r10 = new javax.crypto.spec.IvParameterSpec     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r10.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            javax.crypto.Cipher r12 = r15.cipher     // Catch:{ InvalidKeyException -> 0x006c, InvalidAlgorithmParameterException -> 0x00b3 }
            r13 = 2
            javax.crypto.spec.SecretKeySpec r14 = r15.secretKeySpec     // Catch:{ InvalidKeyException -> 0x006c, InvalidAlgorithmParameterException -> 0x00b3 }
            r12.init(r13, r14, r10)     // Catch:{ InvalidKeyException -> 0x006c, InvalidAlgorithmParameterException -> 0x00b3 }
            java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            javax.crypto.CipherInputStream r12 = new javax.crypto.CipherInputStream     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            javax.crypto.Cipher r13 = r15.cipher     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r12.<init>(r9, r13)     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r7.<init>(r12)     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
        L_0x0054:
            int r1 = r7.readInt()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            r4 = 0
            r5 = 0
        L_0x005a:
            if (r5 >= r1) goto L_0x0086
            com.google.android.exoplayer2.upstream.cache.CachedContent r0 = new com.google.android.exoplayer2.upstream.cache.CachedContent     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            r0.<init>(r7)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            r15.add((com.google.android.exoplayer2.upstream.cache.CachedContent) r0)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            int r12 = r0.headerHashCode()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            int r4 = r4 + r12
            int r5 = r5 + 1
            goto L_0x005a
        L_0x006c:
            r12 = move-exception
            r2 = r12
        L_0x006e:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            r12.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            throw r12     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
        L_0x0074:
            r2 = move-exception
            r7 = r8
        L_0x0076:
            r12 = 0
            if (r7 == 0) goto L_0x001f
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r7)
            goto L_0x001f
        L_0x007d:
            javax.crypto.Cipher r12 = r15.cipher     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
            if (r12 == 0) goto L_0x0084
            r12 = 1
            r15.changed = r12     // Catch:{ FileNotFoundException -> 0x0074, IOException -> 0x00b9, all -> 0x00b6 }
        L_0x0084:
            r7 = r8
            goto L_0x0054
        L_0x0086:
            int r12 = r7.readInt()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x009a }
            if (r12 == r4) goto L_0x0093
            r12 = 0
            if (r7 == 0) goto L_0x001f
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r7)
            goto L_0x001f
        L_0x0093:
            if (r7 == 0) goto L_0x0098
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r7)
        L_0x0098:
            r12 = 1
            goto L_0x001f
        L_0x009a:
            r2 = move-exception
        L_0x009b:
            java.lang.String r12 = "CachedContentIndex"
            java.lang.String r13 = "Error reading cache content index file."
            android.util.Log.e(r12, r13, r2)     // Catch:{ all -> 0x00ac }
            r12 = 0
            if (r7 == 0) goto L_0x001f
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r7)
            goto L_0x001f
        L_0x00ac:
            r12 = move-exception
        L_0x00ad:
            if (r7 == 0) goto L_0x00b2
            com.google.android.exoplayer2.util.Util.closeQuietly((java.io.Closeable) r7)
        L_0x00b2:
            throw r12
        L_0x00b3:
            r12 = move-exception
            r2 = r12
            goto L_0x006e
        L_0x00b6:
            r12 = move-exception
            r7 = r8
            goto L_0x00ad
        L_0x00b9:
            r2 = move-exception
            r7 = r8
            goto L_0x009b
        L_0x00bc:
            r2 = move-exception
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.cache.CachedContentIndex.readFile():boolean");
    }

    private void writeFile() throws Cache.CacheException {
        Throwable th;
        int i = 1;
        DataOutputStream dataOutputStream = null;
        try {
            OutputStream startWrite = this.atomicFile.startWrite();
            if (this.bufferedOutputStream == null) {
                this.bufferedOutputStream = new ReusableBufferedOutputStream(startWrite);
            } else {
                this.bufferedOutputStream.reset(startWrite);
            }
            DataOutputStream dataOutputStream2 = new DataOutputStream(this.bufferedOutputStream);
            try {
                dataOutputStream2.writeInt(1);
                boolean z = this.encrypt && this.cipher != null;
                if (!z) {
                    i = 0;
                }
                dataOutputStream2.writeInt(i);
                if (z) {
                    byte[] bArr = new byte[16];
                    new Random().nextBytes(bArr);
                    dataOutputStream2.write(bArr);
                    try {
                        this.cipher.init(1, this.secretKeySpec, new IvParameterSpec(bArr));
                        dataOutputStream2.flush();
                        dataOutputStream = new DataOutputStream(new CipherOutputStream(this.bufferedOutputStream, this.cipher));
                    } catch (InvalidKeyException e) {
                        th = e;
                        throw new IllegalStateException(th);
                    } catch (InvalidAlgorithmParameterException e2) {
                        th = e2;
                        throw new IllegalStateException(th);
                    }
                } else {
                    dataOutputStream = dataOutputStream2;
                }
                dataOutputStream.writeInt(this.keyToContent.size());
                int i2 = 0;
                for (CachedContent next : this.keyToContent.values()) {
                    next.writeToStream(dataOutputStream);
                    i2 += next.headerHashCode();
                }
                dataOutputStream.writeInt(i2);
                this.atomicFile.endWrite(dataOutputStream);
                Util.closeQuietly((Closeable) null);
            } catch (IOException e3) {
                e = e3;
                dataOutputStream = dataOutputStream2;
                try {
                    throw new Cache.CacheException(e);
                } catch (Throwable th2) {
                    th = th2;
                    Util.closeQuietly((Closeable) dataOutputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                dataOutputStream = dataOutputStream2;
                Util.closeQuietly((Closeable) dataOutputStream);
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            throw new Cache.CacheException(e);
        }
    }

    public CachedContent add(String str) {
        CachedContent cachedContent = this.keyToContent.get(str);
        return cachedContent == null ? addNew(str, -1) : cachedContent;
    }

    /* access modifiers changed from: package-private */
    public void addNew(CachedContent cachedContent) {
        add(cachedContent);
        this.changed = true;
    }

    public int assignIdForKey(String str) {
        return add(str).id;
    }

    public CachedContent get(String str) {
        return this.keyToContent.get(str);
    }

    public Collection<CachedContent> getAll() {
        return this.keyToContent.values();
    }

    public long getContentLength(String str) {
        CachedContent cachedContent = get(str);
        if (cachedContent == null) {
            return -1;
        }
        return cachedContent.getLength();
    }

    public String getKeyForId(int i) {
        return this.idToKey.get(i);
    }

    public Set<String> getKeys() {
        return this.keyToContent.keySet();
    }

    public void load() {
        Assertions.checkState(!this.changed);
        if (!readFile()) {
            this.atomicFile.delete();
            this.keyToContent.clear();
            this.idToKey.clear();
        }
    }

    public void removeEmpty() {
        LinkedList linkedList = new LinkedList();
        for (CachedContent next : this.keyToContent.values()) {
            if (next.isEmpty()) {
                linkedList.add(next.key);
            }
        }
        Iterator it2 = linkedList.iterator();
        while (it2.hasNext()) {
            removeEmpty((String) it2.next());
        }
    }

    public void removeEmpty(String str) {
        CachedContent remove = this.keyToContent.remove(str);
        if (remove != null) {
            Assertions.checkState(remove.isEmpty());
            this.idToKey.remove(remove.id);
            this.changed = true;
        }
    }

    public void setContentLength(String str, long j) {
        CachedContent cachedContent = get(str);
        if (cachedContent == null) {
            addNew(str, j);
        } else if (cachedContent.getLength() != j) {
            cachedContent.setLength(j);
            this.changed = true;
        }
    }

    public void store() throws Cache.CacheException {
        if (this.changed) {
            writeFile();
            this.changed = false;
        }
    }
}
