package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.util.Assertions;
import java.util.Arrays;

public final class DefaultAllocator implements Allocator {
    private static final int AVAILABLE_EXTRA_CAPACITY = 100;
    private int allocatedCount;
    private Allocation[] availableAllocations;
    private int availableCount;
    private final int individualAllocationSize;
    private final byte[] initialAllocationBlock;
    private final Allocation[] singleAllocationReleaseHolder;
    private int targetBufferSize;
    private final boolean trimOnReset;

    public DefaultAllocator(boolean z, int i) {
        this(z, i, 0);
    }

    public DefaultAllocator(boolean z, int i, int i2) {
        boolean z2 = false;
        Assertions.checkArgument(i > 0);
        Assertions.checkArgument(i2 >= 0 ? true : z2);
        this.trimOnReset = z;
        this.individualAllocationSize = i;
        this.availableCount = i2;
        this.availableAllocations = new Allocation[(i2 + 100)];
        if (i2 > 0) {
            this.initialAllocationBlock = new byte[(i2 * i)];
            for (int i3 = 0; i3 < i2; i3++) {
                this.availableAllocations[i3] = new Allocation(this.initialAllocationBlock, i3 * i);
            }
        } else {
            this.initialAllocationBlock = null;
        }
        this.singleAllocationReleaseHolder = new Allocation[1];
    }

    public synchronized Allocation allocate() {
        Allocation allocation;
        this.allocatedCount++;
        if (this.availableCount > 0) {
            Allocation[] allocationArr = this.availableAllocations;
            int i = this.availableCount - 1;
            this.availableCount = i;
            allocation = allocationArr[i];
            this.availableAllocations[this.availableCount] = null;
        } else {
            allocation = new Allocation(new byte[this.individualAllocationSize], 0);
        }
        return allocation;
    }

    public int getIndividualAllocationLength() {
        return this.individualAllocationSize;
    }

    public synchronized int getTotalBytesAllocated() {
        return this.allocatedCount * this.individualAllocationSize;
    }

    public synchronized void release(Allocation allocation) {
        this.singleAllocationReleaseHolder[0] = allocation;
        release(this.singleAllocationReleaseHolder);
    }

    public synchronized void release(Allocation[] allocationArr) {
        if (this.availableCount + allocationArr.length >= this.availableAllocations.length) {
            this.availableAllocations = (Allocation[]) Arrays.copyOf(this.availableAllocations, Math.max(this.availableAllocations.length * 2, this.availableCount + allocationArr.length));
        }
        for (Allocation allocation : allocationArr) {
            Assertions.checkArgument(allocation.data == this.initialAllocationBlock || allocation.data.length == this.individualAllocationSize);
            Allocation[] allocationArr2 = this.availableAllocations;
            int i = this.availableCount;
            this.availableCount = i + 1;
            allocationArr2[i] = allocation;
        }
        this.allocatedCount -= allocationArr.length;
        notifyAll();
    }

    public synchronized void reset() {
        if (this.trimOnReset) {
            setTargetBufferSize(0);
        }
    }

    public synchronized void setTargetBufferSize(int i) {
        boolean z = i < this.targetBufferSize;
        this.targetBufferSize = i;
        if (z) {
            trim();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0059, code lost:
        if (r7 < r11.availableCount) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void trim() {
        /*
            r11 = this;
            monitor-enter(r11)
            int r8 = r11.targetBufferSize     // Catch:{ all -> 0x0050 }
            int r9 = r11.individualAllocationSize     // Catch:{ all -> 0x0050 }
            int r6 = com.google.android.exoplayer2.util.Util.ceilDivide((int) r8, (int) r9)     // Catch:{ all -> 0x0050 }
            r8 = 0
            int r9 = r11.allocatedCount     // Catch:{ all -> 0x0050 }
            int r9 = r6 - r9
            int r7 = java.lang.Math.max(r8, r9)     // Catch:{ all -> 0x0050 }
            int r8 = r11.availableCount     // Catch:{ all -> 0x0050 }
            if (r7 < r8) goto L_0x0018
        L_0x0016:
            monitor-exit(r11)
            return
        L_0x0018:
            byte[] r8 = r11.initialAllocationBlock     // Catch:{ all -> 0x0050 }
            if (r8 == 0) goto L_0x005b
            r4 = 0
            int r8 = r11.availableCount     // Catch:{ all -> 0x0050 }
            int r1 = r8 + -1
            r2 = r1
            r5 = r4
        L_0x0023:
            if (r5 > r2) goto L_0x0053
            com.google.android.exoplayer2.upstream.Allocation[] r8 = r11.availableAllocations     // Catch:{ all -> 0x0050 }
            r3 = r8[r5]     // Catch:{ all -> 0x0050 }
            byte[] r8 = r3.data     // Catch:{ all -> 0x0050 }
            byte[] r9 = r11.initialAllocationBlock     // Catch:{ all -> 0x0050 }
            if (r8 != r9) goto L_0x0035
            int r4 = r5 + 1
            r1 = r2
        L_0x0032:
            r2 = r1
            r5 = r4
            goto L_0x0023
        L_0x0035:
            com.google.android.exoplayer2.upstream.Allocation[] r8 = r11.availableAllocations     // Catch:{ all -> 0x0050 }
            r0 = r8[r2]     // Catch:{ all -> 0x0050 }
            byte[] r8 = r0.data     // Catch:{ all -> 0x0050 }
            byte[] r9 = r11.initialAllocationBlock     // Catch:{ all -> 0x0050 }
            if (r8 == r9) goto L_0x0043
            int r1 = r2 + -1
            r4 = r5
            goto L_0x0032
        L_0x0043:
            com.google.android.exoplayer2.upstream.Allocation[] r8 = r11.availableAllocations     // Catch:{ all -> 0x0050 }
            int r4 = r5 + 1
            r8[r5] = r0     // Catch:{ all -> 0x0050 }
            com.google.android.exoplayer2.upstream.Allocation[] r8 = r11.availableAllocations     // Catch:{ all -> 0x0050 }
            int r1 = r2 + -1
            r8[r2] = r3     // Catch:{ all -> 0x0050 }
            goto L_0x0032
        L_0x0050:
            r8 = move-exception
            monitor-exit(r11)
            throw r8
        L_0x0053:
            int r7 = java.lang.Math.max(r7, r5)     // Catch:{ all -> 0x0050 }
            int r8 = r11.availableCount     // Catch:{ all -> 0x0050 }
            if (r7 >= r8) goto L_0x0016
        L_0x005b:
            com.google.android.exoplayer2.upstream.Allocation[] r8 = r11.availableAllocations     // Catch:{ all -> 0x0050 }
            int r9 = r11.availableCount     // Catch:{ all -> 0x0050 }
            r10 = 0
            java.util.Arrays.fill(r8, r7, r9, r10)     // Catch:{ all -> 0x0050 }
            r11.availableCount = r7     // Catch:{ all -> 0x0050 }
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.DefaultAllocator.trim():void");
    }
}
