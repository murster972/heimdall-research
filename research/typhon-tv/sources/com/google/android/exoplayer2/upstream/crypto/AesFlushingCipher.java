package com.google.android.exoplayer2.upstream.crypto;

import com.google.android.exoplayer2.util.Assertions;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public final class AesFlushingCipher {
    private final int blockSize;
    private final Cipher cipher;
    private final byte[] flushedBlock;
    private int pendingXorBytes;
    private final byte[] zerosBlock;

    public AesFlushingCipher(int i, byte[] bArr, long j, long j2) {
        try {
            this.cipher = Cipher.getInstance("AES/CTR/NoPadding");
            this.blockSize = this.cipher.getBlockSize();
            this.zerosBlock = new byte[this.blockSize];
            this.flushedBlock = new byte[this.blockSize];
            long j3 = j2 / ((long) this.blockSize);
            int i2 = (int) (j2 % ((long) this.blockSize));
            this.cipher.init(i, new SecretKeySpec(bArr, this.cipher.getAlgorithm().split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)[0]), new IvParameterSpec(getInitializationVector(j, j3)));
            if (i2 != 0) {
                updateInPlace(new byte[i2], 0, i2);
            }
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] getInitializationVector(long j, long j2) {
        return ByteBuffer.allocate(16).putLong(j).putLong(j2).array();
    }

    private int nonFlushingUpdate(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        try {
            return this.cipher.update(bArr, i, i2, bArr2, i3);
        } catch (ShortBufferException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        while (this.pendingXorBytes > 0) {
            bArr2[i3] = (byte) (bArr[i] ^ this.flushedBlock[this.blockSize - this.pendingXorBytes]);
            i3++;
            i++;
            this.pendingXorBytes--;
            i2--;
            if (i2 == 0) {
                return;
            }
        }
        int nonFlushingUpdate = nonFlushingUpdate(bArr, i, i2, bArr2, i3);
        if (i2 != nonFlushingUpdate) {
            int i4 = i2 - nonFlushingUpdate;
            Assertions.checkState(i4 < this.blockSize);
            int i5 = i3 + nonFlushingUpdate;
            this.pendingXorBytes = this.blockSize - i4;
            Assertions.checkState(nonFlushingUpdate(this.zerosBlock, 0, this.pendingXorBytes, this.flushedBlock, 0) == this.blockSize);
            int i6 = 0;
            int i7 = i5;
            while (i6 < i4) {
                bArr2[i7] = this.flushedBlock[i6];
                i6++;
                i7++;
            }
            int i8 = i7;
        }
    }

    public void updateInPlace(byte[] bArr, int i, int i2) {
        update(bArr, i, i2, bArr, i);
    }
}
