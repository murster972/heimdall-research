package com.google.android.exoplayer2.upstream.cache;

import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.NavigableSet;

public final class CacheUtil {
    public static final int DEFAULT_BUFFER_SIZE_BYTES = 131072;

    public static class CachingCounters {
        public volatile long alreadyCachedBytes;
        public volatile long contentLength = -1;
        public volatile long newlyCachedBytes;

        public long totalCachedBytes() {
            return this.alreadyCachedBytes + this.newlyCachedBytes;
        }
    }

    private CacheUtil() {
    }

    public static void cache(DataSpec dataSpec, Cache cache, DataSource dataSource, CachingCounters cachingCounters) throws IOException, InterruptedException {
        cache(dataSpec, cache, new CacheDataSource(cache, dataSource), new byte[131072], (PriorityTaskManager) null, 0, cachingCounters, false);
    }

    public static void cache(DataSpec dataSpec, Cache cache, CacheDataSource cacheDataSource, byte[] bArr, PriorityTaskManager priorityTaskManager, int i, CachingCounters cachingCounters, boolean z) throws IOException, InterruptedException {
        Assertions.checkNotNull(cacheDataSource);
        Assertions.checkNotNull(bArr);
        if (cachingCounters != null) {
            getCached(dataSpec, cache, cachingCounters);
        } else {
            cachingCounters = new CachingCounters();
        }
        String key = getKey(dataSpec);
        long j = dataSpec.absoluteStreamPosition;
        long contentLength = dataSpec.length != -1 ? dataSpec.length : cache.getContentLength(key);
        while (contentLength != 0) {
            long cachedBytes = cache.getCachedBytes(key, j, contentLength != -1 ? contentLength : Long.MAX_VALUE);
            if (cachedBytes <= 0) {
                cachedBytes = -cachedBytes;
                if (readAndDiscard(dataSpec, j, cachedBytes, cacheDataSource, bArr, priorityTaskManager, i, cachingCounters) < cachedBytes) {
                    if (z && contentLength != -1) {
                        throw new EOFException();
                    }
                    return;
                }
            }
            j += cachedBytes;
            if (contentLength == -1) {
                cachedBytes = 0;
            }
            contentLength -= cachedBytes;
        }
    }

    public static String generateKey(Uri uri) {
        return uri.toString();
    }

    public static void getCached(DataSpec dataSpec, Cache cache, CachingCounters cachingCounters) {
        String key = getKey(dataSpec);
        long j = dataSpec.absoluteStreamPosition;
        long contentLength = dataSpec.length != -1 ? dataSpec.length : cache.getContentLength(key);
        cachingCounters.contentLength = contentLength;
        cachingCounters.alreadyCachedBytes = 0;
        cachingCounters.newlyCachedBytes = 0;
        while (contentLength != 0) {
            long cachedBytes = cache.getCachedBytes(key, j, contentLength != -1 ? contentLength : Long.MAX_VALUE);
            if (cachedBytes > 0) {
                cachingCounters.alreadyCachedBytes += cachedBytes;
            } else {
                cachedBytes = -cachedBytes;
                if (cachedBytes == Long.MAX_VALUE) {
                    return;
                }
            }
            j += cachedBytes;
            if (contentLength == -1) {
                cachedBytes = 0;
            }
            contentLength -= cachedBytes;
        }
    }

    public static String getKey(DataSpec dataSpec) {
        return dataSpec.key != null ? dataSpec.key : generateKey(dataSpec.uri);
    }

    private static long readAndDiscard(DataSpec dataSpec, long j, long j2, DataSource dataSource, byte[] bArr, PriorityTaskManager priorityTaskManager, int i, CachingCounters cachingCounters) throws IOException, InterruptedException {
        DataSpec dataSpec2;
        long j3;
        DataSpec dataSpec3 = dataSpec;
        loop0:
        while (true) {
            if (priorityTaskManager != null) {
                priorityTaskManager.proceed(i);
            }
            try {
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
                dataSpec2 = new DataSpec(dataSpec3.uri, dataSpec3.postBody, j, (dataSpec3.position + j) - dataSpec3.absoluteStreamPosition, -1, dataSpec3.key, dataSpec3.flags | 2);
                try {
                    long open = dataSource.open(dataSpec2);
                    if (cachingCounters.contentLength == -1 && open != -1) {
                        cachingCounters.contentLength = dataSpec2.absoluteStreamPosition + open;
                    }
                    j3 = 0;
                    while (true) {
                        if (j3 == j2) {
                            break loop0;
                        } else if (Thread.interrupted()) {
                            throw new InterruptedException();
                        } else {
                            int read = dataSource.read(bArr, 0, j2 != -1 ? (int) Math.min((long) bArr.length, j2 - j3) : bArr.length);
                            if (read != -1) {
                                j3 += (long) read;
                                cachingCounters.newlyCachedBytes += (long) read;
                            } else if (cachingCounters.contentLength == -1) {
                                cachingCounters.contentLength = dataSpec2.absoluteStreamPosition + j3;
                            }
                        }
                    }
                } catch (PriorityTaskManager.PriorityTooLowException e) {
                } catch (Throwable th) {
                    th = th;
                    Util.closeQuietly(dataSource);
                    throw th;
                }
            } catch (PriorityTaskManager.PriorityTooLowException e2) {
                dataSpec2 = dataSpec3;
            } catch (Throwable th2) {
                th = th2;
                DataSpec dataSpec4 = dataSpec3;
                Util.closeQuietly(dataSource);
                throw th;
            }
            Util.closeQuietly(dataSource);
            dataSpec3 = dataSpec2;
        }
        Util.closeQuietly(dataSource);
        return j3;
    }

    public static void remove(Cache cache, String str) {
        NavigableSet<CacheSpan> cachedSpans = cache.getCachedSpans(str);
        if (cachedSpans != null) {
            for (CacheSpan removeSpan : cachedSpans) {
                try {
                    cache.removeSpan(removeSpan);
                } catch (Cache.CacheException e) {
                }
            }
        }
    }
}
