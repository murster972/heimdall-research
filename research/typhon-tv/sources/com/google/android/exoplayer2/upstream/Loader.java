package com.google.android.exoplayer2.upstream;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

public final class Loader implements LoaderErrorThrower {
    public static final int DONT_RETRY = 2;
    public static final int DONT_RETRY_FATAL = 3;
    public static final int RETRY = 0;
    public static final int RETRY_RESET_ERROR_COUNT = 1;
    /* access modifiers changed from: private */
    public LoadTask<? extends Loadable> currentTask;
    /* access modifiers changed from: private */
    public final ExecutorService downloadExecutorService;
    /* access modifiers changed from: private */
    public IOException fatalError;

    public interface Callback<T extends Loadable> {
        void onLoadCanceled(T t, long j, long j2, boolean z);

        void onLoadCompleted(T t, long j, long j2);

        int onLoadError(T t, long j, long j2, IOException iOException);
    }

    @SuppressLint({"HandlerLeak"})
    private final class LoadTask<T extends Loadable> extends Handler implements Runnable {
        private static final int MSG_CANCEL = 1;
        private static final int MSG_END_OF_SOURCE = 2;
        private static final int MSG_FATAL_ERROR = 4;
        private static final int MSG_IO_EXCEPTION = 3;
        private static final int MSG_START = 0;
        private static final String TAG = "LoadTask";
        private final Callback<T> callback;
        private IOException currentError;
        public final int defaultMinRetryCount;
        private int errorCount;
        private volatile Thread executorThread;
        private final T loadable;
        private volatile boolean released;
        private final long startTimeMs;

        public LoadTask(Looper looper, T t, Callback<T> callback2, int i, long j) {
            super(looper);
            this.loadable = t;
            this.callback = callback2;
            this.defaultMinRetryCount = i;
            this.startTimeMs = j;
        }

        private void execute() {
            this.currentError = null;
            Loader.this.downloadExecutorService.execute(Loader.this.currentTask);
        }

        private void finish() {
            LoadTask unused = Loader.this.currentTask = null;
        }

        private long getRetryDelayMillis() {
            return (long) Math.min((this.errorCount - 1) * 1000, 5000);
        }

        public void cancel(boolean z) {
            this.released = z;
            this.currentError = null;
            if (hasMessages(0)) {
                removeMessages(0);
                if (!z) {
                    sendEmptyMessage(1);
                }
            } else {
                this.loadable.cancelLoad();
                if (this.executorThread != null) {
                    this.executorThread.interrupt();
                }
            }
            if (z) {
                finish();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                this.callback.onLoadCanceled(this.loadable, elapsedRealtime, elapsedRealtime - this.startTimeMs, true);
            }
        }

        public void handleMessage(Message message) {
            if (!this.released) {
                if (message.what == 0) {
                    execute();
                } else if (message.what == 4) {
                    throw ((Error) message.obj);
                } else {
                    finish();
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    long j = elapsedRealtime - this.startTimeMs;
                    if (this.loadable.isLoadCanceled()) {
                        this.callback.onLoadCanceled(this.loadable, elapsedRealtime, j, false);
                        return;
                    }
                    switch (message.what) {
                        case 1:
                            this.callback.onLoadCanceled(this.loadable, elapsedRealtime, j, false);
                            return;
                        case 2:
                            this.callback.onLoadCompleted(this.loadable, elapsedRealtime, j);
                            return;
                        case 3:
                            this.currentError = (IOException) message.obj;
                            int onLoadError = this.callback.onLoadError(this.loadable, elapsedRealtime, j, this.currentError);
                            if (onLoadError == 3) {
                                IOException unused = Loader.this.fatalError = this.currentError;
                                return;
                            } else if (onLoadError != 2) {
                                this.errorCount = onLoadError == 1 ? 1 : this.errorCount + 1;
                                start(getRetryDelayMillis());
                                return;
                            } else {
                                return;
                            }
                        default:
                            return;
                    }
                }
            }
        }

        public void maybeThrowError(int i) throws IOException {
            if (this.currentError != null && this.errorCount > i) {
                throw this.currentError;
            }
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                r4 = 2
                r3 = 3
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                r5.executorThread = r1     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                T r1 = r5.loadable     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                boolean r1 = r1.isLoadCanceled()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                if (r1 != 0) goto L_0x0039
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                r1.<init>()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                java.lang.String r2 = "load:"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                T r2 = r5.loadable     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                java.lang.Class r2 = r2.getClass()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                java.lang.String r2 = r2.getSimpleName()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                com.google.android.exoplayer2.util.TraceUtil.beginSection(r1)     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                T r1 = r5.loadable     // Catch:{ all -> 0x0042 }
                r1.load()     // Catch:{ all -> 0x0042 }
                com.google.android.exoplayer2.util.TraceUtil.endSection()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
            L_0x0039:
                boolean r1 = r5.released     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                if (r1 != 0) goto L_0x0041
                r1 = 2
                r5.sendEmptyMessage(r1)     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
            L_0x0041:
                return
            L_0x0042:
                r1 = move-exception
                com.google.android.exoplayer2.util.TraceUtil.endSection()     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
                throw r1     // Catch:{ IOException -> 0x0047, InterruptedException -> 0x0054, Exception -> 0x0066, OutOfMemoryError -> 0x0081, Error -> 0x009c }
            L_0x0047:
                r0 = move-exception
                boolean r1 = r5.released
                if (r1 != 0) goto L_0x0041
                android.os.Message r1 = r5.obtainMessage(r3, r0)
                r1.sendToTarget()
                goto L_0x0041
            L_0x0054:
                r0 = move-exception
                T r1 = r5.loadable
                boolean r1 = r1.isLoadCanceled()
                com.google.android.exoplayer2.util.Assertions.checkState(r1)
                boolean r1 = r5.released
                if (r1 != 0) goto L_0x0041
                r5.sendEmptyMessage(r4)
                goto L_0x0041
            L_0x0066:
                r0 = move-exception
                java.lang.String r1 = "LoadTask"
                java.lang.String r2 = "Unexpected exception loading stream"
                android.util.Log.e(r1, r2, r0)
                boolean r1 = r5.released
                if (r1 != 0) goto L_0x0041
                com.google.android.exoplayer2.upstream.Loader$UnexpectedLoaderException r1 = new com.google.android.exoplayer2.upstream.Loader$UnexpectedLoaderException
                r1.<init>(r0)
                android.os.Message r1 = r5.obtainMessage(r3, r1)
                r1.sendToTarget()
                goto L_0x0041
            L_0x0081:
                r0 = move-exception
                java.lang.String r1 = "LoadTask"
                java.lang.String r2 = "OutOfMemory error loading stream"
                android.util.Log.e(r1, r2, r0)
                boolean r1 = r5.released
                if (r1 != 0) goto L_0x0041
                com.google.android.exoplayer2.upstream.Loader$UnexpectedLoaderException r1 = new com.google.android.exoplayer2.upstream.Loader$UnexpectedLoaderException
                r1.<init>(r0)
                android.os.Message r1 = r5.obtainMessage(r3, r1)
                r1.sendToTarget()
                goto L_0x0041
            L_0x009c:
                r0 = move-exception
                java.lang.String r1 = "LoadTask"
                java.lang.String r2 = "Unexpected error loading stream"
                android.util.Log.e(r1, r2, r0)
                boolean r1 = r5.released
                if (r1 != 0) goto L_0x00b2
                r1 = 4
                android.os.Message r1 = r5.obtainMessage(r1, r0)
                r1.sendToTarget()
            L_0x00b2:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.Loader.LoadTask.run():void");
        }

        public void start(long j) {
            Assertions.checkState(Loader.this.currentTask == null);
            LoadTask unused = Loader.this.currentTask = this;
            if (j > 0) {
                sendEmptyMessageDelayed(0, j);
            } else {
                execute();
            }
        }
    }

    public interface Loadable {
        void cancelLoad();

        boolean isLoadCanceled();

        void load() throws IOException, InterruptedException;
    }

    public interface ReleaseCallback {
        void onLoaderReleased();
    }

    private static final class ReleaseTask extends Handler implements Runnable {
        private final ReleaseCallback callback;

        public ReleaseTask(ReleaseCallback releaseCallback) {
            this.callback = releaseCallback;
        }

        public void handleMessage(Message message) {
            this.callback.onLoaderReleased();
        }

        public void run() {
            sendEmptyMessage(0);
        }
    }

    public static final class UnexpectedLoaderException extends IOException {
        public UnexpectedLoaderException(Throwable th) {
            super("Unexpected " + th.getClass().getSimpleName() + ": " + th.getMessage(), th);
        }
    }

    public Loader(String str) {
        this.downloadExecutorService = Util.newSingleThreadExecutor(str);
    }

    public void cancelLoading() {
        this.currentTask.cancel(false);
    }

    public boolean isLoading() {
        return this.currentTask != null;
    }

    public void maybeThrowError() throws IOException {
        maybeThrowError(Integer.MIN_VALUE);
    }

    public void maybeThrowError(int i) throws IOException {
        if (this.fatalError != null) {
            throw this.fatalError;
        } else if (this.currentTask != null) {
            LoadTask<? extends Loadable> loadTask = this.currentTask;
            if (i == Integer.MIN_VALUE) {
                i = this.currentTask.defaultMinRetryCount;
            }
            loadTask.maybeThrowError(i);
        }
    }

    public void release() {
        release((ReleaseCallback) null);
    }

    public boolean release(ReleaseCallback releaseCallback) {
        boolean z = false;
        if (this.currentTask != null) {
            this.currentTask.cancel(true);
            if (releaseCallback != null) {
                this.downloadExecutorService.execute(new ReleaseTask(releaseCallback));
            }
        } else if (releaseCallback != null) {
            releaseCallback.onLoaderReleased();
            z = true;
        }
        this.downloadExecutorService.shutdown();
        return z;
    }

    public <T extends Loadable> long startLoading(T t, Callback<T> callback, int i) {
        Looper myLooper = Looper.myLooper();
        Assertions.checkState(myLooper != null);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        new LoadTask(myLooper, t, callback, i, elapsedRealtime).start(0);
        return elapsedRealtime;
    }
}
