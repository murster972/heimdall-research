package com.google.ads;

public enum AdRequest$Gender {
    UNKNOWN,
    MALE,
    FEMALE
}
