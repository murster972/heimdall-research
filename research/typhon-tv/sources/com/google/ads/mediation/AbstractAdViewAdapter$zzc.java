package com.google.ads.mediation;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.internal.zzje;

final class AbstractAdViewAdapter$zzc extends AdListener implements AppEventListener, zzje {

    /* renamed from: 靐  reason: contains not printable characters */
    private MediationBannerListener f6614;

    /* renamed from: 龘  reason: contains not printable characters */
    private AbstractAdViewAdapter f6615;

    public AbstractAdViewAdapter$zzc(AbstractAdViewAdapter abstractAdViewAdapter, MediationBannerListener mediationBannerListener) {
        this.f6615 = abstractAdViewAdapter;
        this.f6614 = mediationBannerListener;
    }

    public final void onAdClicked() {
        this.f6614.onAdClicked(this.f6615);
    }

    public final void onAdClosed() {
        this.f6614.onAdClosed(this.f6615);
    }

    public final void onAdFailedToLoad(int i) {
        this.f6614.onAdFailedToLoad(this.f6615, i);
    }

    public final void onAdLeftApplication() {
        this.f6614.onAdLeftApplication(this.f6615);
    }

    public final void onAdLoaded() {
        this.f6614.onAdLoaded(this.f6615);
    }

    public final void onAdOpened() {
        this.f6614.onAdOpened(this.f6615);
    }

    public final void onAppEvent(String str, String str2) {
        this.f6614.zza(this.f6615, str, str2);
    }
}
