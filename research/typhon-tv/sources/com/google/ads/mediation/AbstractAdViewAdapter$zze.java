package com.google.ads.mediation;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.mediation.MediationNativeListener;

final class AbstractAdViewAdapter$zze extends AdListener implements NativeAppInstallAd.OnAppInstallAdLoadedListener, NativeContentAd.OnContentAdLoadedListener, NativeCustomTemplateAd.OnCustomClickListener, NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private MediationNativeListener f6618;

    /* renamed from: 龘  reason: contains not printable characters */
    private AbstractAdViewAdapter f6619;

    public AbstractAdViewAdapter$zze(AbstractAdViewAdapter abstractAdViewAdapter, MediationNativeListener mediationNativeListener) {
        this.f6619 = abstractAdViewAdapter;
        this.f6618 = mediationNativeListener;
    }

    public final void onAdClicked() {
        this.f6618.onAdClicked(this.f6619);
    }

    public final void onAdClosed() {
        this.f6618.onAdClosed(this.f6619);
    }

    public final void onAdFailedToLoad(int i) {
        this.f6618.onAdFailedToLoad(this.f6619, i);
    }

    public final void onAdImpression() {
        this.f6618.onAdImpression(this.f6619);
    }

    public final void onAdLeftApplication() {
        this.f6618.onAdLeftApplication(this.f6619);
    }

    public final void onAdLoaded() {
    }

    public final void onAdOpened() {
        this.f6618.onAdOpened(this.f6619);
    }

    public final void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
        this.f6618.onAdLoaded(this.f6619, new AbstractAdViewAdapter$zza(nativeAppInstallAd));
    }

    public final void onContentAdLoaded(NativeContentAd nativeContentAd) {
        this.f6618.onAdLoaded(this.f6619, new AbstractAdViewAdapter$zzb(nativeContentAd));
    }

    public final void onCustomClick(NativeCustomTemplateAd nativeCustomTemplateAd, String str) {
        this.f6618.zza(this.f6619, nativeCustomTemplateAd, str);
    }

    public final void onCustomTemplateAdLoaded(NativeCustomTemplateAd nativeCustomTemplateAd) {
        this.f6618.zza(this.f6619, nativeCustomTemplateAd);
    }
}
