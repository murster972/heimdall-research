package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.Pinkamena;
import com.google.ads.AdRequest$ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.zzakb;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter<CustomEventExtras, CustomEventServerParameters>, MediationInterstitialAdapter<CustomEventExtras, CustomEventServerParameters> {

    /* renamed from: 靐  reason: contains not printable characters */
    private CustomEventBanner f6620;

    /* renamed from: 齉  reason: contains not printable characters */
    private CustomEventInterstitial f6621;

    /* renamed from: 龘  reason: contains not printable characters */
    private View f6622;

    static final class zza implements CustomEventBannerListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MediationBannerListener f6623;

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventAdapter f6624;

        public zza(CustomEventAdapter customEventAdapter, MediationBannerListener mediationBannerListener) {
            this.f6624 = customEventAdapter;
            this.f6623 = mediationBannerListener;
        }

        public final void onClick() {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6623.onClick(this.f6624);
        }

        public final void onDismissScreen() {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6623.onDismissScreen(this.f6624);
        }

        public final void onFailedToReceiveAd() {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6623.onFailedToReceiveAd(this.f6624, AdRequest$ErrorCode.NO_FILL);
        }

        public final void onLeaveApplication() {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6623.onLeaveApplication(this.f6624);
        }

        public final void onPresentScreen() {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6623.onPresentScreen(this.f6624);
        }

        public final void onReceivedAd(View view) {
            zzakb.m4792("Custom event adapter called onReceivedAd.");
            this.f6624.m7612(view);
            this.f6623.onReceivedAd(this.f6624);
        }
    }

    class zzb implements CustomEventInterstitialListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MediationInterstitialListener f6625;

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventAdapter f6627;

        public zzb(CustomEventAdapter customEventAdapter, MediationInterstitialListener mediationInterstitialListener) {
            this.f6627 = customEventAdapter;
            this.f6625 = mediationInterstitialListener;
        }

        public final void onDismissScreen() {
            zzakb.m4792("Custom event adapter called onDismissScreen.");
            this.f6625.onDismissScreen(this.f6627);
        }

        public final void onFailedToReceiveAd() {
            zzakb.m4792("Custom event adapter called onFailedToReceiveAd.");
            this.f6625.onFailedToReceiveAd(this.f6627, AdRequest$ErrorCode.NO_FILL);
        }

        public final void onLeaveApplication() {
            zzakb.m4792("Custom event adapter called onLeaveApplication.");
            this.f6625.onLeaveApplication(this.f6627);
        }

        public final void onPresentScreen() {
            zzakb.m4792("Custom event adapter called onPresentScreen.");
            this.f6625.onPresentScreen(this.f6627);
        }

        public final void onReceivedAd() {
            zzakb.m4792("Custom event adapter called onReceivedAd.");
            this.f6625.onReceivedAd(CustomEventAdapter.this);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> T m7611(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            zzakb.m4791(new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length()).append("Could not instantiate custom event adapter: ").append(str).append(". ").append(message).toString());
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7612(View view) {
        this.f6622 = view;
    }

    public final void destroy() {
        if (this.f6620 != null) {
            this.f6620.destroy();
        }
        if (this.f6621 != null) {
            this.f6621.destroy();
        }
    }

    public final Class<CustomEventExtras> getAdditionalParametersType() {
        return CustomEventExtras.class;
    }

    public final View getBannerView() {
        return this.f6622;
    }

    public final Class<CustomEventServerParameters> getServerParametersType() {
        return CustomEventServerParameters.class;
    }

    public final /* bridge */ /* synthetic */ void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, MediationServerParameters mediationServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, NetworkExtras networkExtras) {
        CustomEventServerParameters customEventServerParameters = (CustomEventServerParameters) mediationServerParameters;
        CustomEventExtras customEventExtras = (CustomEventExtras) networkExtras;
        MediationBannerListener mediationBannerListener2 = mediationBannerListener;
        Activity activity2 = activity;
        AdSize adSize2 = adSize;
        MediationAdRequest mediationAdRequest2 = mediationAdRequest;
        Pinkamena.DianePie();
    }

    public final void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, CustomEventServerParameters customEventServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        this.f6620 = (CustomEventBanner) m7611(customEventServerParameters.className);
        if (this.f6620 == null) {
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest$ErrorCode.INTERNAL_ERROR);
            return;
        }
        if (customEventExtras != null) {
            Object extra = customEventExtras.getExtra(customEventServerParameters.label);
        }
        CustomEventBanner customEventBanner = this.f6620;
        new zza(this, mediationBannerListener);
        String str = customEventServerParameters.label;
        String str2 = customEventServerParameters.parameter;
        Activity activity2 = activity;
        AdSize adSize2 = adSize;
        MediationAdRequest mediationAdRequest2 = mediationAdRequest;
        Pinkamena.DianePie();
    }

    public final /* bridge */ /* synthetic */ void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, MediationServerParameters mediationServerParameters, MediationAdRequest mediationAdRequest, NetworkExtras networkExtras) {
        CustomEventServerParameters customEventServerParameters = (CustomEventServerParameters) mediationServerParameters;
        CustomEventExtras customEventExtras = (CustomEventExtras) networkExtras;
        MediationInterstitialListener mediationInterstitialListener2 = mediationInterstitialListener;
        Activity activity2 = activity;
        MediationAdRequest mediationAdRequest2 = mediationAdRequest;
        Pinkamena.DianePie();
    }

    public final void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, CustomEventServerParameters customEventServerParameters, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        this.f6621 = (CustomEventInterstitial) m7611(customEventServerParameters.className);
        if (this.f6621 == null) {
            mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest$ErrorCode.INTERNAL_ERROR);
            return;
        }
        if (customEventExtras != null) {
            Object extra = customEventExtras.getExtra(customEventServerParameters.label);
        }
        CustomEventInterstitial customEventInterstitial = this.f6621;
        new zzb(this, mediationInterstitialListener);
        String str = customEventServerParameters.label;
        String str2 = customEventServerParameters.parameter;
        Activity activity2 = activity;
        MediationAdRequest mediationAdRequest2 = mediationAdRequest;
        Pinkamena.DianePie();
    }

    public final void showInterstitial() {
        CustomEventInterstitial customEventInterstitial = this.f6621;
        Pinkamena.DianePie();
    }
}
