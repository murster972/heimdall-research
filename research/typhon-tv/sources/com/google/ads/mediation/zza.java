package com.google.ads.mediation;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

final class zza implements RewardedVideoAdListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private /* synthetic */ AbstractAdViewAdapter f6628;

    zza(AbstractAdViewAdapter abstractAdViewAdapter) {
        this.f6628 = abstractAdViewAdapter;
    }

    public final void onRewarded(RewardItem rewardItem) {
        AbstractAdViewAdapter.zza(this.f6628).onRewarded(this.f6628, rewardItem);
    }

    public final void onRewardedVideoAdClosed() {
        AbstractAdViewAdapter.zza(this.f6628).onAdClosed(this.f6628);
        AbstractAdViewAdapter.zza(this.f6628, (InterstitialAd) null);
    }

    public final void onRewardedVideoAdFailedToLoad(int i) {
        AbstractAdViewAdapter.zza(this.f6628).onAdFailedToLoad(this.f6628, i);
    }

    public final void onRewardedVideoAdLeftApplication() {
        AbstractAdViewAdapter.zza(this.f6628).onAdLeftApplication(this.f6628);
    }

    public final void onRewardedVideoAdLoaded() {
        AbstractAdViewAdapter.zza(this.f6628).onAdLoaded(this.f6628);
    }

    public final void onRewardedVideoAdOpened() {
        AbstractAdViewAdapter.zza(this.f6628).onAdOpened(this.f6628);
    }

    public final void onRewardedVideoStarted() {
        AbstractAdViewAdapter.zza(this.f6628).onVideoStarted(this.f6628);
    }
}
