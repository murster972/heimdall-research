package com.google.ads.mediation;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.internal.zzje;

final class AbstractAdViewAdapter$zzd extends AdListener implements zzje {

    /* renamed from: 靐  reason: contains not printable characters */
    private MediationInterstitialListener f6616;

    /* renamed from: 龘  reason: contains not printable characters */
    private AbstractAdViewAdapter f6617;

    public AbstractAdViewAdapter$zzd(AbstractAdViewAdapter abstractAdViewAdapter, MediationInterstitialListener mediationInterstitialListener) {
        this.f6617 = abstractAdViewAdapter;
        this.f6616 = mediationInterstitialListener;
    }

    public final void onAdClicked() {
        this.f6616.onAdClicked(this.f6617);
    }

    public final void onAdClosed() {
        this.f6616.onAdClosed(this.f6617);
    }

    public final void onAdFailedToLoad(int i) {
        this.f6616.onAdFailedToLoad(this.f6617, i);
    }

    public final void onAdLeftApplication() {
        this.f6616.onAdLeftApplication(this.f6617);
    }

    public final void onAdLoaded() {
        this.f6616.onAdLoaded(this.f6617);
    }

    public final void onAdOpened() {
        this.f6616.onAdOpened(this.f6617);
    }
}
