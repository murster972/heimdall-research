package com.google.gson.internal.bind;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.PreJava9DateFormatProvider;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.gson.util.VersionUtils;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class DateTypeAdapter extends TypeAdapter<Date> {
    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == Date.class) {
                return new DateTypeAdapter();
            }
            return null;
        }
    };
    private final List<DateFormat> dateFormats = new ArrayList();

    public DateTypeAdapter() {
        this.dateFormats.add(DateFormat.getDateTimeInstance(2, 2, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.dateFormats.add(DateFormat.getDateTimeInstance(2, 2));
        }
        if (VersionUtils.isJava9OrLater()) {
            this.dateFormats.add(PreJava9DateFormatProvider.getUSDateTimeFormat(2, 2));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r2 = com.google.gson.internal.bind.util.ISO8601Utils.parse(r5, new java.text.ParsePosition(0));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.Date deserializeToDate(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.List<java.text.DateFormat> r2 = r4.dateFormats     // Catch:{ all -> 0x002b }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x002b }
        L_0x0007:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x002b }
            if (r3 == 0) goto L_0x0019
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x002b }
            java.text.DateFormat r0 = (java.text.DateFormat) r0     // Catch:{ all -> 0x002b }
            java.util.Date r2 = r0.parse(r5)     // Catch:{ ParseException -> 0x002e }
        L_0x0017:
            monitor-exit(r4)
            return r2
        L_0x0019:
            java.text.ParsePosition r2 = new java.text.ParsePosition     // Catch:{ ParseException -> 0x0024 }
            r3 = 0
            r2.<init>(r3)     // Catch:{ ParseException -> 0x0024 }
            java.util.Date r2 = com.google.gson.internal.bind.util.ISO8601Utils.parse(r5, r2)     // Catch:{ ParseException -> 0x0024 }
            goto L_0x0017
        L_0x0024:
            r1 = move-exception
            com.google.gson.JsonSyntaxException r2 = new com.google.gson.JsonSyntaxException     // Catch:{ all -> 0x002b }
            r2.<init>(r5, r1)     // Catch:{ all -> 0x002b }
            throw r2     // Catch:{ all -> 0x002b }
        L_0x002b:
            r2 = move-exception
            monitor-exit(r4)
            throw r2
        L_0x002e:
            r3 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.internal.bind.DateTypeAdapter.deserializeToDate(java.lang.String):java.util.Date");
    }

    public Date read(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() != JsonToken.NULL) {
            return deserializeToDate(jsonReader.nextString());
        }
        jsonReader.nextNull();
        return null;
    }

    public synchronized void write(JsonWriter jsonWriter, Date date) throws IOException {
        if (date == null) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(this.dateFormats.get(0).format(date));
        }
    }
}
