package com.thunderrise.animations;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.View;

public class PulseAnimation {

    /* renamed from: 靐  reason: contains not printable characters */
    private View f12481;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f12482 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f12483 = 1;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f12484 = 310;

    /* renamed from: 龘  reason: contains not printable characters */
    public static PulseAnimation m15698() {
        return new PulseAnimation();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PulseAnimation m15699(int i) {
        this.f12483 = i;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m15700() {
        if (this.f12481 == null) {
            throw new NullPointerException("View cant be null!");
        }
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.f12481, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scaleX", new float[]{1.2f}), PropertyValuesHolder.ofFloat("scaleY", new float[]{1.2f})});
        ofPropertyValuesHolder.setDuration((long) this.f12484);
        ofPropertyValuesHolder.setRepeatMode(this.f12483);
        ofPropertyValuesHolder.setRepeatCount(this.f12482);
        ofPropertyValuesHolder.start();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PulseAnimation m15701(int i) {
        this.f12482 = i;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PulseAnimation m15702(int i) {
        this.f12484 = i;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PulseAnimation m15703(View view) {
        this.f12481 = view;
        return this;
    }
}
