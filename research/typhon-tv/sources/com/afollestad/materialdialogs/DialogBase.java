package com.afollestad.materialdialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import com.afollestad.materialdialogs.internal.MDRootLayout;

class DialogBase extends Dialog implements DialogInterface.OnShowListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private DialogInterface.OnShowListener f3104;

    /* renamed from: 龘  reason: contains not printable characters */
    protected MDRootLayout f3105;

    DialogBase(Context context, int i) {
        super(context, i);
    }

    public View findViewById(int i) {
        return this.f3105.findViewById(i);
    }

    public void onShow(DialogInterface dialogInterface) {
        if (this.f3104 != null) {
            this.f3104.onShow(dialogInterface);
        }
    }

    @Deprecated
    public void setContentView(int i) throws IllegalAccessError {
        throw new IllegalAccessError("setContentView() is not supported in MaterialDialog. Specify a custom view in the Builder instead.");
    }

    @Deprecated
    public void setContentView(View view) throws IllegalAccessError {
        throw new IllegalAccessError("setContentView() is not supported in MaterialDialog. Specify a custom view in the Builder instead.");
    }

    @Deprecated
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) throws IllegalAccessError {
        throw new IllegalAccessError("setContentView() is not supported in MaterialDialog. Specify a custom view in the Builder instead.");
    }

    public final void setOnShowListener(DialogInterface.OnShowListener onShowListener) {
        this.f3104 = onShowListener;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3752() {
        super.setOnShowListener(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3753(View view) {
        super.setContentView(view);
    }
}
