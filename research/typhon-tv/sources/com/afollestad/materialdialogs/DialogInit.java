package com.afollestad.materialdialogs;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDAdapter;
import com.afollestad.materialdialogs.internal.MDButton;
import com.afollestad.materialdialogs.internal.MDRootLayout;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;
import java.util.ArrayList;
import java.util.Arrays;
import me.zhanghai.android.materialprogressbar.HorizontalProgressDrawable;
import me.zhanghai.android.materialprogressbar.IndeterminateCircularProgressDrawable;
import me.zhanghai.android.materialprogressbar.IndeterminateHorizontalProgressDrawable;

class DialogInit {
    /* renamed from: 靐  reason: contains not printable characters */
    static int m3754(MaterialDialog.Builder builder) {
        return builder.f3193 != null ? R.layout.md_dialog_custom : (builder.f3185 == null && builder.f3210 == null) ? builder.f3144 > -2 ? R.layout.md_dialog_progress : builder.f3142 ? builder.f3162 ? R.layout.md_dialog_progress_indeterminate_horizontal : R.layout.md_dialog_progress_indeterminate : builder.f3148 != null ? builder.f3156 != null ? R.layout.md_dialog_input_check : R.layout.md_dialog_input : builder.f3156 != null ? R.layout.md_dialog_basic_check : R.layout.md_dialog_basic : builder.f3156 != null ? R.layout.md_dialog_list_check : R.layout.md_dialog_list;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m3755(MaterialDialog materialDialog) {
        MaterialDialog.Builder builder = materialDialog.f3123;
        if (builder.f3142 || builder.f3144 > -2) {
            materialDialog.f3120 = (ProgressBar) materialDialog.f3105.findViewById(16908301);
            if (materialDialog.f3120 != null) {
                if (Build.VERSION.SDK_INT < 14) {
                    MDTintHelper.m3846(materialDialog.f3120, builder.f3183);
                } else if (!builder.f3142) {
                    HorizontalProgressDrawable horizontalProgressDrawable = new HorizontalProgressDrawable(builder.m3796());
                    horizontalProgressDrawable.setTint(builder.f3183);
                    materialDialog.f3120.setProgressDrawable(horizontalProgressDrawable);
                    materialDialog.f3120.setIndeterminateDrawable(horizontalProgressDrawable);
                } else if (builder.f3162) {
                    IndeterminateHorizontalProgressDrawable indeterminateHorizontalProgressDrawable = new IndeterminateHorizontalProgressDrawable(builder.m3796());
                    indeterminateHorizontalProgressDrawable.setTint(builder.f3183);
                    materialDialog.f3120.setProgressDrawable(indeterminateHorizontalProgressDrawable);
                    materialDialog.f3120.setIndeterminateDrawable(indeterminateHorizontalProgressDrawable);
                } else {
                    IndeterminateCircularProgressDrawable indeterminateCircularProgressDrawable = new IndeterminateCircularProgressDrawable(builder.m3796());
                    indeterminateCircularProgressDrawable.setTint(builder.f3183);
                    materialDialog.f3120.setProgressDrawable(indeterminateCircularProgressDrawable);
                    materialDialog.f3120.setIndeterminateDrawable(indeterminateCircularProgressDrawable);
                }
                if (!builder.f3142 || builder.f3162) {
                    materialDialog.f3120.setIndeterminate(builder.f3142 && builder.f3162);
                    materialDialog.f3120.setProgress(0);
                    materialDialog.f3120.setMax(builder.f3145);
                    materialDialog.f3121 = (TextView) materialDialog.f3105.findViewById(R.id.md_label);
                    if (materialDialog.f3121 != null) {
                        materialDialog.f3121.setTextColor(builder.f3203);
                        materialDialog.m3779(materialDialog.f3121, builder.f3194);
                        materialDialog.f3121.setText(builder.f3160.format(0));
                    }
                    materialDialog.f3115 = (TextView) materialDialog.f3105.findViewById(R.id.md_minMax);
                    if (materialDialog.f3115 != null) {
                        materialDialog.f3115.setTextColor(builder.f3203);
                        materialDialog.m3779(materialDialog.f3115, builder.f3196);
                        if (builder.f3143) {
                            materialDialog.f3115.setVisibility(0);
                            materialDialog.f3115.setText(String.format(builder.f3159, new Object[]{0, Integer.valueOf(builder.f3145)}));
                            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) materialDialog.f3120.getLayoutParams();
                            marginLayoutParams.leftMargin = 0;
                            marginLayoutParams.rightMargin = 0;
                        } else {
                            materialDialog.f3115.setVisibility(8);
                        }
                    } else {
                        builder.f3143 = false;
                    }
                }
            } else {
                return;
            }
        }
        if (materialDialog.f3120 != null) {
            m3758(materialDialog.f3120);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m3756(MaterialDialog materialDialog) {
        MaterialDialog.Builder builder = materialDialog.f3123;
        materialDialog.f3109 = (EditText) materialDialog.f3105.findViewById(16908297);
        if (materialDialog.f3109 != null) {
            materialDialog.m3779((TextView) materialDialog.f3109, builder.f3196);
            if (builder.f3146 != null) {
                materialDialog.f3109.setText(builder.f3146);
            }
            materialDialog.m3765();
            materialDialog.f3109.setHint(builder.f3147);
            materialDialog.f3109.setSingleLine();
            materialDialog.f3109.setTextColor(builder.f3203);
            materialDialog.f3109.setHintTextColor(DialogUtils.m3861(builder.f3203, 0.3f));
            MDTintHelper.m3845(materialDialog.f3109, materialDialog.f3123.f3183);
            if (builder.f3150 != -1) {
                materialDialog.f3109.setInputType(builder.f3150);
                if (builder.f3150 != 144 && (builder.f3150 & 128) == 128) {
                    materialDialog.f3109.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
            materialDialog.f3112 = (TextView) materialDialog.f3105.findViewById(R.id.md_minMax);
            if (builder.f3152 > 0 || builder.f3153 > -1) {
                materialDialog.m3778(materialDialog.f3109.getText().toString().length(), !builder.f3149);
                return;
            }
            materialDialog.f3112.setVisibility(8);
            materialDialog.f3112 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m3757(MaterialDialog.Builder builder) {
        boolean r0 = DialogUtils.m3872(builder.f3221, R.attr.md_dark_theme, builder.f3182 == Theme.DARK);
        builder.f3182 = r0 ? Theme.DARK : Theme.LIGHT;
        return r0 ? R.style.MD_Dark : R.style.MD_Light;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m3758(ProgressBar progressBar) {
        if (Build.VERSION.SDK_INT < 18 && progressBar.isHardwareAccelerated() && progressBar.getLayerType() != 1) {
            progressBar.setLayerType(1, (Paint) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m3759(MaterialDialog materialDialog) {
        boolean r26;
        MaterialDialog.Builder builder = materialDialog.f3123;
        materialDialog.setCancelable(builder.f3180);
        materialDialog.setCanceledOnTouchOutside(builder.f3202);
        if (builder.f3140 == 0) {
            builder.f3140 = DialogUtils.m3864(builder.f3221, R.attr.md_background_color, DialogUtils.m3863(materialDialog.getContext(), R.attr.colorBackgroundFloating));
        }
        if (builder.f3140 != 0) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setCornerRadius(builder.f3221.getResources().getDimension(R.dimen.md_bg_corner_radius));
            gradientDrawable.setColor(builder.f3140);
            materialDialog.getWindow().setBackgroundDrawable(gradientDrawable);
        }
        if (!builder.f3167) {
            builder.f3195 = DialogUtils.m3866(builder.f3221, R.attr.md_positive_color, builder.f3195);
        }
        if (!builder.f3168) {
            builder.f3201 = DialogUtils.m3866(builder.f3221, R.attr.md_neutral_color, builder.f3201);
        }
        if (!builder.f3169) {
            builder.f3199 = DialogUtils.m3866(builder.f3221, R.attr.md_negative_color, builder.f3199);
        }
        if (!builder.f3170) {
            builder.f3183 = DialogUtils.m3864(builder.f3221, R.attr.md_widget_color, builder.f3183);
        }
        if (!builder.f3164) {
            builder.f3197 = DialogUtils.m3864(builder.f3221, R.attr.md_title_color, DialogUtils.m3863(materialDialog.getContext(), 16842806));
        }
        if (!builder.f3165) {
            builder.f3203 = DialogUtils.m3864(builder.f3221, R.attr.md_content_color, DialogUtils.m3863(materialDialog.getContext(), 16842808));
        }
        if (!builder.f3166) {
            builder.f3141 = DialogUtils.m3864(builder.f3221, R.attr.md_item_color, builder.f3203);
        }
        materialDialog.f3124 = (TextView) materialDialog.f3105.findViewById(R.id.md_title);
        materialDialog.f3125 = (ImageView) materialDialog.f3105.findViewById(R.id.md_icon);
        materialDialog.f3111 = materialDialog.f3105.findViewById(R.id.md_titleFrame);
        materialDialog.f3122 = (TextView) materialDialog.f3105.findViewById(R.id.md_content);
        materialDialog.f3110 = (RecyclerView) materialDialog.f3105.findViewById(R.id.md_contentRecyclerView);
        materialDialog.f3113 = (CheckBox) materialDialog.f3105.findViewById(R.id.md_promptCheckbox);
        materialDialog.f3126 = (MDButton) materialDialog.f3105.findViewById(R.id.md_buttonDefaultPositive);
        materialDialog.f3127 = (MDButton) materialDialog.f3105.findViewById(R.id.md_buttonDefaultNeutral);
        materialDialog.f3116 = (MDButton) materialDialog.f3105.findViewById(R.id.md_buttonDefaultNegative);
        if (builder.f3148 != null && builder.f3179 == null) {
            builder.f3179 = builder.f3221.getText(17039370);
        }
        materialDialog.f3126.setVisibility(builder.f3179 != null ? 0 : 8);
        materialDialog.f3127.setVisibility(builder.f3181 != null ? 0 : 8);
        materialDialog.f3116.setVisibility(builder.f3224 != null ? 0 : 8);
        materialDialog.f3126.setFocusable(true);
        materialDialog.f3127.setFocusable(true);
        materialDialog.f3116.setFocusable(true);
        if (builder.f3226) {
            materialDialog.f3126.requestFocus();
        }
        if (builder.f3189) {
            materialDialog.f3127.requestFocus();
        }
        if (builder.f3191) {
            materialDialog.f3116.requestFocus();
        }
        if (builder.f3198 != null) {
            materialDialog.f3125.setVisibility(0);
            materialDialog.f3125.setImageDrawable(builder.f3198);
        } else {
            Drawable r8 = DialogUtils.m3855(builder.f3221, R.attr.md_icon);
            if (r8 != null) {
                materialDialog.f3125.setVisibility(0);
                materialDialog.f3125.setImageDrawable(r8);
            } else {
                materialDialog.f3125.setVisibility(8);
            }
        }
        int i = builder.f3200;
        if (i == -1) {
            i = DialogUtils.m3852(builder.f3221, R.attr.md_icon_max_size);
        }
        if (builder.f3212 || DialogUtils.m3853(builder.f3221, R.attr.md_icon_limit_icon_to_default_size)) {
            i = builder.f3221.getResources().getDimensionPixelSize(R.dimen.md_icon_max_size);
        }
        if (i > -1) {
            materialDialog.f3125.setAdjustViewBounds(true);
            materialDialog.f3125.setMaxHeight(i);
            materialDialog.f3125.setMaxWidth(i);
            materialDialog.f3125.requestLayout();
        }
        if (!builder.f3171) {
            builder.f3139 = DialogUtils.m3864(builder.f3221, R.attr.md_divider_color, DialogUtils.m3863(materialDialog.getContext(), R.attr.md_divider));
        }
        materialDialog.f3105.setDividerColor(builder.f3139);
        if (materialDialog.f3124 != null) {
            materialDialog.m3779(materialDialog.f3124, builder.f3194);
            materialDialog.f3124.setTextColor(builder.f3197);
            materialDialog.f3124.setGravity(builder.f3220.getGravityInt());
            if (Build.VERSION.SDK_INT >= 17) {
                materialDialog.f3124.setTextAlignment(builder.f3220.getTextAlignment());
            }
            if (builder.f3218 == null) {
                materialDialog.f3111.setVisibility(8);
            } else {
                materialDialog.f3124.setText(builder.f3218);
                materialDialog.f3111.setVisibility(0);
            }
        }
        if (materialDialog.f3122 != null) {
            materialDialog.f3122.setMovementMethod(new LinkMovementMethod());
            materialDialog.m3779(materialDialog.f3122, builder.f3196);
            materialDialog.f3122.setLineSpacing(0.0f, builder.f3184);
            if (builder.f3207 == null) {
                materialDialog.f3122.setLinkTextColor(DialogUtils.m3863(materialDialog.getContext(), 16842806));
            } else {
                materialDialog.f3122.setLinkTextColor(builder.f3207);
            }
            materialDialog.f3122.setTextColor(builder.f3203);
            materialDialog.f3122.setGravity(builder.f3219.getGravityInt());
            if (Build.VERSION.SDK_INT >= 17) {
                materialDialog.f3122.setTextAlignment(builder.f3219.getTextAlignment());
            }
            if (builder.f3205 != null) {
                materialDialog.f3122.setText(builder.f3205);
                materialDialog.f3122.setVisibility(0);
            } else {
                materialDialog.f3122.setVisibility(8);
            }
        }
        if (materialDialog.f3113 != null) {
            materialDialog.f3113.setText(builder.f3156);
            materialDialog.f3113.setChecked(builder.f3157);
            materialDialog.f3113.setOnCheckedChangeListener(builder.f3158);
            materialDialog.m3779((TextView) materialDialog.f3113, builder.f3196);
            materialDialog.f3113.setTextColor(builder.f3203);
            MDTintHelper.m3843(materialDialog.f3113, builder.f3183);
        }
        materialDialog.f3105.setButtonGravity(builder.f3161);
        materialDialog.f3105.setButtonStackedGravity(builder.f3217);
        materialDialog.f3105.setStackingBehavior(builder.f3137);
        if (Build.VERSION.SDK_INT >= 14) {
            r26 = DialogUtils.m3872(builder.f3221, 16843660, true);
            if (r26) {
                r26 = DialogUtils.m3872(builder.f3221, R.attr.textAllCaps, true);
            }
        } else {
            r26 = DialogUtils.m3872(builder.f3221, R.attr.textAllCaps, true);
        }
        MDButton mDButton = materialDialog.f3126;
        materialDialog.m3779((TextView) mDButton, builder.f3194);
        mDButton.setAllCapsCompat(r26);
        mDButton.setText(builder.f3179);
        mDButton.setTextColor(builder.f3195);
        materialDialog.f3126.setStackedSelector(materialDialog.m3775(DialogAction.POSITIVE, true));
        materialDialog.f3126.setDefaultSelector(materialDialog.m3775(DialogAction.POSITIVE, false));
        materialDialog.f3126.setTag(DialogAction.POSITIVE);
        materialDialog.f3126.setOnClickListener(materialDialog);
        MDButton mDButton2 = materialDialog.f3116;
        materialDialog.m3779((TextView) mDButton2, builder.f3194);
        mDButton2.setAllCapsCompat(r26);
        mDButton2.setText(builder.f3224);
        mDButton2.setTextColor(builder.f3199);
        materialDialog.f3116.setStackedSelector(materialDialog.m3775(DialogAction.NEGATIVE, true));
        materialDialog.f3116.setDefaultSelector(materialDialog.m3775(DialogAction.NEGATIVE, false));
        materialDialog.f3116.setTag(DialogAction.NEGATIVE);
        materialDialog.f3116.setOnClickListener(materialDialog);
        MDButton mDButton3 = materialDialog.f3127;
        materialDialog.m3779((TextView) mDButton3, builder.f3194);
        mDButton3.setAllCapsCompat(r26);
        mDButton3.setText(builder.f3181);
        mDButton3.setTextColor(builder.f3201);
        materialDialog.f3127.setStackedSelector(materialDialog.m3775(DialogAction.NEUTRAL, true));
        materialDialog.f3127.setDefaultSelector(materialDialog.m3775(DialogAction.NEUTRAL, false));
        materialDialog.f3127.setTag(DialogAction.NEUTRAL);
        materialDialog.f3127.setOnClickListener(materialDialog);
        if (builder.f3136 != null) {
            materialDialog.f3118 = new ArrayList();
        }
        if (materialDialog.f3110 != null) {
            if (builder.f3210 == null) {
                if (builder.f3208 != null) {
                    materialDialog.f3117 = MaterialDialog.ListType.SINGLE;
                } else if (builder.f3136 != null) {
                    materialDialog.f3117 = MaterialDialog.ListType.MULTI;
                    if (builder.f3186 != null) {
                        materialDialog.f3118 = new ArrayList(Arrays.asList(builder.f3186));
                        builder.f3186 = null;
                    }
                } else {
                    materialDialog.f3117 = MaterialDialog.ListType.REGULAR;
                }
                builder.f3210 = new DefaultRvAdapter(materialDialog, MaterialDialog.ListType.m3815(materialDialog.f3117));
            } else if (builder.f3210 instanceof MDAdapter) {
                ((MDAdapter) builder.f3210).m3817(materialDialog);
            }
        }
        m3755(materialDialog);
        m3756(materialDialog);
        if (builder.f3193 != null) {
            ((MDRootLayout) materialDialog.f3105.findViewById(R.id.md_root)).m3840();
            FrameLayout frameLayout = (FrameLayout) materialDialog.f3105.findViewById(R.id.md_customViewFrame);
            materialDialog.f3119 = frameLayout;
            View view = builder.f3193;
            if (view.getParent() != null) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            if (builder.f3138) {
                Resources resources = materialDialog.getContext().getResources();
                int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.md_dialog_frame_margin);
                ScrollView scrollView = new ScrollView(materialDialog.getContext());
                int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.md_content_padding_top);
                int dimensionPixelSize3 = resources.getDimensionPixelSize(R.dimen.md_content_padding_bottom);
                scrollView.setClipToPadding(false);
                if (view instanceof EditText) {
                    scrollView.setPadding(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize3);
                } else {
                    scrollView.setPadding(0, dimensionPixelSize2, 0, dimensionPixelSize3);
                    view.setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
                }
                scrollView.addView(view, new FrameLayout.LayoutParams(-1, -2));
                view = scrollView;
            }
            frameLayout.addView(view, new ViewGroup.LayoutParams(-1, -2));
        }
        if (builder.f3225 != null) {
            materialDialog.setOnShowListener(builder.f3225);
        }
        if (builder.f3223 != null) {
            materialDialog.setOnCancelListener(builder.f3223);
        }
        if (builder.f3216 != null) {
            materialDialog.setOnDismissListener(builder.f3216);
        }
        if (builder.f3204 != null) {
            materialDialog.setOnKeyListener(builder.f3204);
        }
        materialDialog.m3752();
        materialDialog.m3772();
        materialDialog.m3753(materialDialog.f3105);
        materialDialog.m3773();
        Display defaultDisplay = materialDialog.getWindow().getWindowManager().getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int i2 = point.x;
        int i3 = point.y;
        int dimensionPixelSize4 = builder.f3221.getResources().getDimensionPixelSize(R.dimen.md_dialog_vertical_margin);
        int dimensionPixelSize5 = builder.f3221.getResources().getDimensionPixelSize(R.dimen.md_dialog_horizontal_margin);
        materialDialog.f3105.setMaxHeight(i3 - (dimensionPixelSize4 * 2));
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(materialDialog.getWindow().getAttributes());
        layoutParams.width = Math.min(builder.f3221.getResources().getDimensionPixelSize(R.dimen.md_dialog_max_width), i2 - (dimensionPixelSize5 * 2));
        materialDialog.getWindow().setAttributes(layoutParams);
    }
}
