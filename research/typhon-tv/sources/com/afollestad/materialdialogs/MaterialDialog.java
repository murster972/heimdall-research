package com.afollestad.materialdialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import com.afollestad.materialdialogs.DefaultRvAdapter;
import com.afollestad.materialdialogs.internal.MDButton;
import com.afollestad.materialdialogs.internal.MDRootLayout;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.internal.ThemeSingleton;
import com.afollestad.materialdialogs.util.DialogUtils;
import com.afollestad.materialdialogs.util.RippleHelper;
import com.afollestad.materialdialogs.util.TypefaceHelper;
import com.google.android.exoplayer2.C;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class MaterialDialog extends DialogBase implements View.OnClickListener, DefaultRvAdapter.InternalListCallback {

    /* renamed from: ʻ  reason: contains not printable characters */
    EditText f3109;

    /* renamed from: ʼ  reason: contains not printable characters */
    RecyclerView f3110;

    /* renamed from: ʽ  reason: contains not printable characters */
    View f3111;

    /* renamed from: ʾ  reason: contains not printable characters */
    TextView f3112;

    /* renamed from: ʿ  reason: contains not printable characters */
    CheckBox f3113;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final Handler f3114 = new Handler();

    /* renamed from: ˈ  reason: contains not printable characters */
    TextView f3115;

    /* renamed from: ˊ  reason: contains not printable characters */
    MDButton f3116;

    /* renamed from: ˋ  reason: contains not printable characters */
    ListType f3117;

    /* renamed from: ˎ  reason: contains not printable characters */
    List<Integer> f3118;

    /* renamed from: ˑ  reason: contains not printable characters */
    FrameLayout f3119;

    /* renamed from: ٴ  reason: contains not printable characters */
    ProgressBar f3120;

    /* renamed from: ᐧ  reason: contains not printable characters */
    TextView f3121;

    /* renamed from: 连任  reason: contains not printable characters */
    protected TextView f3122;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final Builder f3123;

    /* renamed from: 麤  reason: contains not printable characters */
    protected TextView f3124;

    /* renamed from: 齉  reason: contains not printable characters */
    protected ImageView f3125;

    /* renamed from: ﹶ  reason: contains not printable characters */
    MDButton f3126;

    /* renamed from: ﾞ  reason: contains not printable characters */
    MDButton f3127;

    public static class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        protected GravityEnum f3135 = GravityEnum.START;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        protected ListCallbackMultiChoice f3136;

        /* renamed from: ʻʼ  reason: contains not printable characters */
        protected StackingBehavior f3137;

        /* renamed from: ʻʽ  reason: contains not printable characters */
        protected boolean f3138;

        /* renamed from: ʻʾ  reason: contains not printable characters */
        protected int f3139;

        /* renamed from: ʻʿ  reason: contains not printable characters */
        protected int f3140;

        /* renamed from: ʻˆ  reason: contains not printable characters */
        protected int f3141;

        /* renamed from: ʻˈ  reason: contains not printable characters */
        protected boolean f3142;

        /* renamed from: ʻˉ  reason: contains not printable characters */
        protected boolean f3143;

        /* renamed from: ʻˊ  reason: contains not printable characters */
        protected int f3144 = -2;

        /* renamed from: ʻˋ  reason: contains not printable characters */
        protected int f3145 = 0;

        /* renamed from: ʻˎ  reason: contains not printable characters */
        protected CharSequence f3146;

        /* renamed from: ʻˏ  reason: contains not printable characters */
        protected CharSequence f3147;

        /* renamed from: ʻˑ  reason: contains not printable characters */
        protected InputCallback f3148;

        /* renamed from: ʻי  reason: contains not printable characters */
        protected boolean f3149;

        /* renamed from: ʻـ  reason: contains not printable characters */
        protected int f3150 = -1;

        /* renamed from: ʻٴ  reason: contains not printable characters */
        protected boolean f3151;

        /* renamed from: ʻᐧ  reason: contains not printable characters */
        protected int f3152 = -1;

        /* renamed from: ʻᴵ  reason: contains not printable characters */
        protected int f3153 = -1;

        /* renamed from: ʻᵎ  reason: contains not printable characters */
        protected int f3154 = 0;

        /* renamed from: ʻᵔ  reason: contains not printable characters */
        protected int[] f3155;

        /* renamed from: ʻᵢ  reason: contains not printable characters */
        protected CharSequence f3156;

        /* renamed from: ʻⁱ  reason: contains not printable characters */
        protected boolean f3157;

        /* renamed from: ʻﹳ  reason: contains not printable characters */
        protected CompoundButton.OnCheckedChangeListener f3158;

        /* renamed from: ʻﹶ  reason: contains not printable characters */
        protected String f3159;

        /* renamed from: ʻﾞ  reason: contains not printable characters */
        protected NumberFormat f3160;

        /* renamed from: ʼ  reason: contains not printable characters */
        protected GravityEnum f3161 = GravityEnum.START;

        /* renamed from: ʼʻ  reason: contains not printable characters */
        protected boolean f3162;

        /* renamed from: ʼʼ  reason: contains not printable characters */
        protected boolean f3163 = false;

        /* renamed from: ʼʽ  reason: contains not printable characters */
        protected boolean f3164 = false;

        /* renamed from: ʼʾ  reason: contains not printable characters */
        protected boolean f3165 = false;

        /* renamed from: ʼʿ  reason: contains not printable characters */
        protected boolean f3166 = false;

        /* renamed from: ʼˆ  reason: contains not printable characters */
        protected boolean f3167 = false;

        /* renamed from: ʼˈ  reason: contains not printable characters */
        protected boolean f3168 = false;

        /* renamed from: ʼˉ  reason: contains not printable characters */
        protected boolean f3169 = false;

        /* renamed from: ʼˊ  reason: contains not printable characters */
        protected boolean f3170 = false;

        /* renamed from: ʼˋ  reason: contains not printable characters */
        protected boolean f3171 = false;

        /* renamed from: ʼˎ  reason: contains not printable characters */
        protected int f3172;

        /* renamed from: ʼˏ  reason: contains not printable characters */
        protected int f3173;

        /* renamed from: ʼˑ  reason: contains not printable characters */
        protected int f3174;

        /* renamed from: ʼי  reason: contains not printable characters */
        protected int f3175;

        /* renamed from: ʼـ  reason: contains not printable characters */
        protected int f3176;

        /* renamed from: ʽ  reason: contains not printable characters */
        protected int f3177 = 0;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        protected boolean f3178 = false;

        /* renamed from: ʾ  reason: contains not printable characters */
        protected CharSequence f3179;

        /* renamed from: ʾʾ  reason: contains not printable characters */
        protected boolean f3180 = true;

        /* renamed from: ʿ  reason: contains not printable characters */
        protected CharSequence f3181;

        /* renamed from: ʿʿ  reason: contains not printable characters */
        protected Theme f3182 = Theme.LIGHT;

        /* renamed from: ˆ  reason: contains not printable characters */
        protected int f3183;

        /* renamed from: ˆˆ  reason: contains not printable characters */
        protected float f3184 = 1.2f;

        /* renamed from: ˈ  reason: contains not printable characters */
        protected ArrayList<CharSequence> f3185;

        /* renamed from: ˈˈ  reason: contains not printable characters */
        protected Integer[] f3186 = null;

        /* renamed from: ˉ  reason: contains not printable characters */
        protected ColorStateList f3187;

        /* renamed from: ˉˉ  reason: contains not printable characters */
        protected int f3188 = -1;

        /* renamed from: ˊ  reason: contains not printable characters */
        protected boolean f3189;

        /* renamed from: ˊˊ  reason: contains not printable characters */
        protected boolean f3190 = true;

        /* renamed from: ˋ  reason: contains not printable characters */
        protected boolean f3191;

        /* renamed from: ˋˋ  reason: contains not printable characters */
        protected Integer[] f3192 = null;

        /* renamed from: ˎ  reason: contains not printable characters */
        protected View f3193;

        /* renamed from: ˎˎ  reason: contains not printable characters */
        protected Typeface f3194;

        /* renamed from: ˏ  reason: contains not printable characters */
        protected ColorStateList f3195;

        /* renamed from: ˏˏ  reason: contains not printable characters */
        protected Typeface f3196;

        /* renamed from: ˑ  reason: contains not printable characters */
        protected int f3197 = -1;

        /* renamed from: ˑˑ  reason: contains not printable characters */
        protected Drawable f3198;

        /* renamed from: י  reason: contains not printable characters */
        protected ColorStateList f3199;

        /* renamed from: יי  reason: contains not printable characters */
        protected int f3200 = -1;

        /* renamed from: ـ  reason: contains not printable characters */
        protected ColorStateList f3201;

        /* renamed from: ــ  reason: contains not printable characters */
        protected boolean f3202 = true;

        /* renamed from: ٴ  reason: contains not printable characters */
        protected int f3203 = -1;

        /* renamed from: ٴٴ  reason: contains not printable characters */
        protected DialogInterface.OnKeyListener f3204;

        /* renamed from: ᐧ  reason: contains not printable characters */
        protected CharSequence f3205;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        protected ListLongCallback f3206;

        /* renamed from: ᴵ  reason: contains not printable characters */
        protected ColorStateList f3207;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        protected ListCallbackSingleChoice f3208;

        /* renamed from: ᵎ  reason: contains not printable characters */
        protected ButtonCallback f3209;

        /* renamed from: ᵎᵎ  reason: contains not printable characters */
        protected RecyclerView.Adapter<?> f3210;

        /* renamed from: ᵔ  reason: contains not printable characters */
        protected SingleButtonCallback f3211;

        /* renamed from: ᵔᵔ  reason: contains not printable characters */
        protected boolean f3212;

        /* renamed from: ᵢ  reason: contains not printable characters */
        protected SingleButtonCallback f3213;

        /* renamed from: ᵢᵢ  reason: contains not printable characters */
        protected RecyclerView.LayoutManager f3214;

        /* renamed from: ⁱ  reason: contains not printable characters */
        protected SingleButtonCallback f3215;

        /* renamed from: ⁱⁱ  reason: contains not printable characters */
        protected DialogInterface.OnDismissListener f3216;

        /* renamed from: 连任  reason: contains not printable characters */
        protected GravityEnum f3217 = GravityEnum.END;

        /* renamed from: 靐  reason: contains not printable characters */
        protected CharSequence f3218;

        /* renamed from: 麤  reason: contains not printable characters */
        protected GravityEnum f3219 = GravityEnum.START;

        /* renamed from: 齉  reason: contains not printable characters */
        protected GravityEnum f3220 = GravityEnum.START;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final Context f3221;

        /* renamed from: ﹳ  reason: contains not printable characters */
        protected SingleButtonCallback f3222;

        /* renamed from: ﹳﹳ  reason: contains not printable characters */
        protected DialogInterface.OnCancelListener f3223;

        /* renamed from: ﹶ  reason: contains not printable characters */
        protected CharSequence f3224;

        /* renamed from: ﹶﹶ  reason: contains not printable characters */
        protected DialogInterface.OnShowListener f3225;

        /* renamed from: ﾞ  reason: contains not printable characters */
        protected boolean f3226;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        protected ListCallback f3227;

        public Builder(Context context) {
            this.f3221 = context;
            this.f3183 = DialogUtils.m3864(context, R.attr.colorAccent, DialogUtils.m3860(context, R.color.md_material_blue_600));
            if (Build.VERSION.SDK_INT >= 21) {
                this.f3183 = DialogUtils.m3864(context, 16843829, this.f3183);
            }
            this.f3195 = DialogUtils.m3854(context, this.f3183);
            this.f3199 = DialogUtils.m3854(context, this.f3183);
            this.f3201 = DialogUtils.m3854(context, this.f3183);
            this.f3207 = DialogUtils.m3854(context, DialogUtils.m3864(context, R.attr.md_link_color, this.f3183));
            this.f3177 = DialogUtils.m3864(context, R.attr.md_btn_ripple_color, DialogUtils.m3864(context, R.attr.colorControlHighlight, Build.VERSION.SDK_INT >= 21 ? DialogUtils.m3863(context, 16843820) : 0));
            this.f3160 = NumberFormat.getPercentInstance();
            this.f3159 = "%1d/%2d";
            this.f3182 = DialogUtils.m3871(DialogUtils.m3863(context, 16842806)) ? Theme.LIGHT : Theme.DARK;
            m3782();
            this.f3220 = DialogUtils.m3868(context, R.attr.md_title_gravity, this.f3220);
            this.f3219 = DialogUtils.m3868(context, R.attr.md_content_gravity, this.f3219);
            this.f3217 = DialogUtils.m3868(context, R.attr.md_btnstacked_gravity, this.f3217);
            this.f3135 = DialogUtils.m3868(context, R.attr.md_items_gravity, this.f3135);
            this.f3161 = DialogUtils.m3868(context, R.attr.md_buttons_gravity, this.f3161);
            try {
                m3802(DialogUtils.m3859(context, R.attr.md_medium_font), DialogUtils.m3859(context, R.attr.md_regular_font));
            } catch (Throwable th) {
            }
            if (this.f3194 == null) {
                try {
                    if (Build.VERSION.SDK_INT >= 21) {
                        this.f3194 = Typeface.create("sans-serif-medium", 0);
                    } else {
                        this.f3194 = Typeface.create(C.SANS_SERIF_NAME, 1);
                    }
                } catch (Throwable th2) {
                    this.f3194 = Typeface.DEFAULT_BOLD;
                }
            }
            if (this.f3196 == null) {
                try {
                    this.f3196 = Typeface.create(C.SANS_SERIF_NAME, 0);
                } catch (Throwable th3) {
                    this.f3196 = Typeface.SANS_SERIF;
                    if (this.f3196 == null) {
                        this.f3196 = Typeface.DEFAULT;
                    }
                }
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m3782() {
            if (ThemeSingleton.m3851(false) != null) {
                ThemeSingleton r0 = ThemeSingleton.m3850();
                if (r0.f3293) {
                    this.f3182 = Theme.DARK;
                }
                if (r0.f3290 != 0) {
                    this.f3197 = r0.f3290;
                }
                if (r0.f3292 != 0) {
                    this.f3203 = r0.f3292;
                }
                if (r0.f3291 != null) {
                    this.f3195 = r0.f3291;
                }
                if (r0.f3289 != null) {
                    this.f3201 = r0.f3289;
                }
                if (r0.f3274 != null) {
                    this.f3199 = r0.f3274;
                }
                if (r0.f3276 != 0) {
                    this.f3141 = r0.f3276;
                }
                if (r0.f3286 != null) {
                    this.f3198 = r0.f3286;
                }
                if (r0.f3287 != 0) {
                    this.f3140 = r0.f3287;
                }
                if (r0.f3288 != 0) {
                    this.f3139 = r0.f3288;
                }
                if (r0.f3278 != 0) {
                    this.f3173 = r0.f3278;
                }
                if (r0.f3277 != 0) {
                    this.f3172 = r0.f3277;
                }
                if (r0.f3294 != 0) {
                    this.f3174 = r0.f3294;
                }
                if (r0.f3295 != 0) {
                    this.f3175 = r0.f3295;
                }
                if (r0.f3282 != 0) {
                    this.f3176 = r0.f3282;
                }
                if (r0.f3275 != 0) {
                    this.f3183 = r0.f3275;
                }
                if (r0.f3280 != null) {
                    this.f3207 = r0.f3280;
                }
                this.f3220 = r0.f3283;
                this.f3219 = r0.f3284;
                this.f3217 = r0.f3279;
                this.f3135 = r0.f3281;
                this.f3161 = r0.f3285;
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public Builder m3783(boolean z) {
            this.f3190 = z;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m3784(int i) {
            return m3798(DialogUtils.m3857(this.f3221, i));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m3785(ColorStateList colorStateList) {
            this.f3199 = colorStateList;
            this.f3169 = true;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m3786(SingleButtonCallback singleButtonCallback) {
            this.f3213 = singleButtonCallback;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m3787(CharSequence charSequence) {
            if (this.f3193 != null) {
                throw new IllegalStateException("You cannot set content() when you're using a custom view.");
            }
            this.f3205 = charSequence;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m3788(boolean z) {
            this.f3191 = z;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public MaterialDialog m3789() {
            return new MaterialDialog(this);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m3790(CharSequence charSequence) {
            this.f3224 = charSequence;
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m3791(boolean z) {
            this.f3202 = z;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m3792(int i) {
            return m3785(DialogUtils.m3857(this.f3221, i));
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m3793(CharSequence charSequence) {
            this.f3179 = charSequence;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m3794(boolean z) {
            this.f3180 = z;
            this.f3202 = z;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public MaterialDialog m3795() {
            MaterialDialog r0 = m3789();
            r0.show();
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final Context m3796() {
            return this.f3221;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3797(int i) {
            this.f3198 = ResourcesCompat.getDrawable(this.f3221.getResources(), i, (Resources.Theme) null);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3798(ColorStateList colorStateList) {
            this.f3195 = colorStateList;
            this.f3167 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3799(SingleButtonCallback singleButtonCallback) {
            this.f3211 = singleButtonCallback;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3800(CharSequence charSequence) {
            this.f3218 = charSequence;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3801(CharSequence charSequence, boolean z, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
            this.f3156 = charSequence;
            this.f3157 = z;
            this.f3158 = onCheckedChangeListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3802(String str, String str2) {
            if (str != null && !str.trim().isEmpty()) {
                this.f3194 = TypefaceHelper.m3875(this.f3221, str);
                if (this.f3194 == null) {
                    throw new IllegalArgumentException("No font asset found for \"" + str + "\"");
                }
            }
            if (str2 != null && !str2.trim().isEmpty()) {
                this.f3196 = TypefaceHelper.m3875(this.f3221, str2);
                if (this.f3196 == null) {
                    throw new IllegalArgumentException("No font asset found for \"" + str2 + "\"");
                }
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3803(boolean z) {
            this.f3226 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3804(boolean z, int i) {
            if (this.f3193 != null) {
                throw new IllegalStateException("You cannot set progress() when you're using a custom view.");
            }
            if (z) {
                this.f3142 = true;
                this.f3144 = -2;
            } else {
                this.f3162 = false;
                this.f3142 = false;
                this.f3144 = -1;
                this.f3145 = i;
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m3805(boolean z, int i, boolean z2) {
            this.f3143 = z2;
            return m3804(z, i);
        }
    }

    @Deprecated
    public static abstract class ButtonCallback {
        /* access modifiers changed from: protected */
        public final Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public final boolean equals(Object obj) {
            return super.equals(obj);
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            super.finalize();
        }

        public final int hashCode() {
            return super.hashCode();
        }

        public final String toString() {
            return super.toString();
        }

        @Deprecated
        /* renamed from: 靐  reason: contains not printable characters */
        public void m3806(MaterialDialog materialDialog) {
        }

        @Deprecated
        /* renamed from: 麤  reason: contains not printable characters */
        public void m3807(MaterialDialog materialDialog) {
        }

        @Deprecated
        /* renamed from: 齉  reason: contains not printable characters */
        public void m3808(MaterialDialog materialDialog) {
        }

        @Deprecated
        /* renamed from: 龘  reason: contains not printable characters */
        public void m3809(MaterialDialog materialDialog) {
        }
    }

    private static class DialogException extends WindowManager.BadTokenException {
        DialogException(String str) {
            super(str);
        }
    }

    public interface InputCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3810(MaterialDialog materialDialog, CharSequence charSequence);
    }

    public interface ListCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3811(MaterialDialog materialDialog, View view, int i, CharSequence charSequence);
    }

    public interface ListCallbackMultiChoice {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m3812(MaterialDialog materialDialog, Integer[] numArr, CharSequence[] charSequenceArr);
    }

    public interface ListCallbackSingleChoice {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m3813(MaterialDialog materialDialog, View view, int i, CharSequence charSequence);
    }

    public interface ListLongCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m3814(MaterialDialog materialDialog, View view, int i, CharSequence charSequence);
    }

    enum ListType {
        REGULAR,
        SINGLE,
        MULTI;

        /* renamed from: 龘  reason: contains not printable characters */
        public static int m3815(ListType listType) {
            switch (listType) {
                case REGULAR:
                    return R.layout.md_listitem;
                case SINGLE:
                    return R.layout.md_listitem_singlechoice;
                case MULTI:
                    return R.layout.md_listitem_multichoice;
                default:
                    throw new IllegalArgumentException("Not a valid list type");
            }
        }
    }

    public interface SingleButtonCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3816(MaterialDialog materialDialog, DialogAction dialogAction);
    }

    @SuppressLint({"InflateParams"})
    protected MaterialDialog(Builder builder) {
        super(builder.f3221, DialogInit.m3757(builder));
        this.f3123 = builder;
        this.f3105 = (MDRootLayout) LayoutInflater.from(builder.f3221).inflate(DialogInit.m3754(builder), (ViewGroup) null);
        DialogInit.m3759(this);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean m3760() {
        if (this.f3123.f3136 == null) {
            return false;
        }
        Collections.sort(this.f3118);
        ArrayList arrayList = new ArrayList();
        for (Integer next : this.f3118) {
            if (next.intValue() >= 0 && next.intValue() <= this.f3123.f3185.size() - 1) {
                arrayList.add(this.f3123.f3185.get(next.intValue()));
            }
        }
        return this.f3123.f3136.m3812(this, (Integer[]) this.f3118.toArray(new Integer[this.f3118.size()]), (CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m3761(View view) {
        if (this.f3123.f3208 == null) {
            return false;
        }
        CharSequence charSequence = null;
        if (this.f3123.f3188 >= 0 && this.f3123.f3188 < this.f3123.f3185.size()) {
            charSequence = this.f3123.f3185.get(this.f3123.f3188);
        }
        return this.f3123.f3208.m3813(this, view, this.f3123.f3188, charSequence);
    }

    public void dismiss() {
        if (this.f3109 != null) {
            DialogUtils.m3858((DialogInterface) this, this.f3123);
        }
        super.dismiss();
    }

    public /* bridge */ /* synthetic */ View findViewById(int i) {
        return super.findViewById(i);
    }

    public final void onClick(View view) {
        DialogAction dialogAction = (DialogAction) view.getTag();
        switch (dialogAction) {
            case NEUTRAL:
                if (this.f3123.f3209 != null) {
                    this.f3123.f3209.m3809(this);
                    this.f3123.f3209.m3807(this);
                }
                if (this.f3123.f3215 != null) {
                    this.f3123.f3215.m3816(this, dialogAction);
                }
                if (this.f3123.f3190) {
                    dismiss();
                    break;
                }
                break;
            case NEGATIVE:
                if (this.f3123.f3209 != null) {
                    this.f3123.f3209.m3809(this);
                    this.f3123.f3209.m3808(this);
                }
                if (this.f3123.f3213 != null) {
                    this.f3123.f3213.m3816(this, dialogAction);
                }
                if (this.f3123.f3190) {
                    cancel();
                    break;
                }
                break;
            case POSITIVE:
                if (this.f3123.f3209 != null) {
                    this.f3123.f3209.m3809(this);
                    this.f3123.f3209.m3806(this);
                }
                if (this.f3123.f3211 != null) {
                    this.f3123.f3211.m3816(this, dialogAction);
                }
                if (!this.f3123.f3163) {
                    m3761(view);
                }
                if (!this.f3123.f3178) {
                    m3760();
                }
                if (!(this.f3123.f3148 == null || this.f3109 == null || this.f3123.f3151)) {
                    this.f3123.f3148.m3810(this, this.f3109.getText());
                }
                if (this.f3123.f3190) {
                    dismiss();
                    break;
                }
                break;
        }
        if (this.f3123.f3222 != null) {
            this.f3123.f3222.m3816(this, dialogAction);
        }
    }

    public final void onShow(DialogInterface dialogInterface) {
        if (this.f3109 != null) {
            DialogUtils.m3869((DialogInterface) this, this.f3123);
            if (this.f3109.getText().length() > 0) {
                this.f3109.setSelection(this.f3109.getText().length());
            }
        }
        super.onShow(dialogInterface);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ void setContentView(int i) throws IllegalAccessError {
        super.setContentView(i);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ void setContentView(View view) throws IllegalAccessError {
        super.setContentView(view);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ void setContentView(View view, ViewGroup.LayoutParams layoutParams) throws IllegalAccessError {
        super.setContentView(view, layoutParams);
    }

    public final void setTitle(int i) {
        setTitle((CharSequence) this.f3123.f3221.getString(i));
    }

    public final void setTitle(CharSequence charSequence) {
        this.f3124.setText(charSequence);
    }

    public void show() {
        try {
            super.show();
        } catch (WindowManager.BadTokenException e) {
            throw new DialogException("Bad window token, you cannot show a dialog before an Activity is created or after it's hidden.");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m3762() {
        return this.f3113 != null && this.f3113.isChecked();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final View m3763() {
        return this.f3105;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final EditText m3764() {
        return this.f3109;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m3765() {
        if (this.f3109 != null) {
            this.f3109.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable editable) {
                }

                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    boolean z = true;
                    int length = charSequence.toString().length();
                    boolean z2 = false;
                    if (!MaterialDialog.this.f3123.f3149) {
                        z2 = length == 0;
                        MDButton r2 = MaterialDialog.this.m3776(DialogAction.POSITIVE);
                        if (z2) {
                            z = false;
                        }
                        r2.setEnabled(z);
                    }
                    MaterialDialog.this.m3778(length, z2);
                    if (MaterialDialog.this.f3123.f3151) {
                        MaterialDialog.this.f3123.f3148.m3810(MaterialDialog.this, charSequence);
                    }
                }
            });
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m3766() {
        if (this.f3120 == null) {
            return -1;
        }
        return this.f3120.getProgress();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m3767() {
        if (this.f3120 == null) {
            return -1;
        }
        return this.f3120.getMax();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m3768() {
        return !isShowing();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final Drawable m3769() {
        if (this.f3123.f3172 != 0) {
            return ResourcesCompat.getDrawable(this.f3123.f3221.getResources(), this.f3123.f3172, (Resources.Theme) null);
        }
        Drawable r0 = DialogUtils.m3855(this.f3123.f3221, R.attr.md_list_selector);
        return r0 == null ? DialogUtils.m3855(getContext(), R.attr.md_list_selector) : r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Builder m3770() {
        return this.f3123;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3771(int i) {
        if (this.f3123.f3144 <= -2) {
            Log.w("MaterialDialog", "Calling setProgress(int) on an indeterminate progress dialog has no effect!");
            return;
        }
        this.f3120.setProgress(i);
        this.f3114.post(new Runnable() {
            public void run() {
                if (MaterialDialog.this.f3121 != null) {
                    MaterialDialog.this.f3121.setText(MaterialDialog.this.f3123.f3160.format((double) (((float) MaterialDialog.this.m3766()) / ((float) MaterialDialog.this.m3767()))));
                }
                if (MaterialDialog.this.f3115 != null) {
                    MaterialDialog.this.f3115.setText(String.format(MaterialDialog.this.f3123.f3159, new Object[]{Integer.valueOf(MaterialDialog.this.m3766()), Integer.valueOf(MaterialDialog.this.m3767())}));
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3772() {
        if (this.f3110 != null) {
            if ((this.f3123.f3185 != null && this.f3123.f3185.size() != 0) || this.f3123.f3210 != null) {
                if (this.f3123.f3214 == null) {
                    this.f3123.f3214 = new LinearLayoutManager(getContext());
                }
                if (this.f3110.getLayoutManager() == null) {
                    this.f3110.setLayoutManager(this.f3123.f3214);
                }
                this.f3110.setAdapter(this.f3123.f3210);
                if (this.f3117 != null) {
                    ((DefaultRvAdapter) this.f3123.f3210).m3750((DefaultRvAdapter.InternalListCallback) this);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3773() {
        if (this.f3110 != null) {
            this.f3110.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    int intValue;
                    if (Build.VERSION.SDK_INT < 16) {
                        MaterialDialog.this.f3110.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        MaterialDialog.this.f3110.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    if (MaterialDialog.this.f3117 == ListType.SINGLE || MaterialDialog.this.f3117 == ListType.MULTI) {
                        if (MaterialDialog.this.f3117 == ListType.SINGLE) {
                            if (MaterialDialog.this.f3123.f3188 >= 0) {
                                intValue = MaterialDialog.this.f3123.f3188;
                            } else {
                                return;
                            }
                        } else if (MaterialDialog.this.f3118 != null && MaterialDialog.this.f3118.size() != 0) {
                            Collections.sort(MaterialDialog.this.f3118);
                            intValue = MaterialDialog.this.f3118.get(0).intValue();
                        } else {
                            return;
                        }
                        final int i = intValue;
                        MaterialDialog.this.f3110.post(new Runnable() {
                            public void run() {
                                MaterialDialog.this.f3110.requestFocus();
                                MaterialDialog.this.f3123.f3214.scrollToPosition(i);
                            }
                        });
                    }
                }
            });
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3774(int i) {
        if (this.f3123.f3144 <= -2) {
            throw new IllegalStateException("Cannot use setMaxProgress() on this dialog.");
        }
        this.f3120.setMax(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Drawable m3775(DialogAction dialogAction, boolean z) {
        if (!z) {
            switch (dialogAction) {
                case NEUTRAL:
                    if (this.f3123.f3175 != 0) {
                        return ResourcesCompat.getDrawable(this.f3123.f3221.getResources(), this.f3123.f3175, (Resources.Theme) null);
                    }
                    Drawable r0 = DialogUtils.m3855(this.f3123.f3221, R.attr.md_btn_neutral_selector);
                    if (r0 != null) {
                        return r0;
                    }
                    Drawable r02 = DialogUtils.m3855(getContext(), R.attr.md_btn_neutral_selector);
                    if (Build.VERSION.SDK_INT < 21) {
                        return r02;
                    }
                    RippleHelper.m3874(r02, this.f3123.f3177);
                    return r02;
                case NEGATIVE:
                    if (this.f3123.f3176 != 0) {
                        return ResourcesCompat.getDrawable(this.f3123.f3221.getResources(), this.f3123.f3176, (Resources.Theme) null);
                    }
                    Drawable r03 = DialogUtils.m3855(this.f3123.f3221, R.attr.md_btn_negative_selector);
                    if (r03 != null) {
                        return r03;
                    }
                    Drawable r04 = DialogUtils.m3855(getContext(), R.attr.md_btn_negative_selector);
                    if (Build.VERSION.SDK_INT < 21) {
                        return r04;
                    }
                    RippleHelper.m3874(r04, this.f3123.f3177);
                    return r04;
                default:
                    if (this.f3123.f3174 != 0) {
                        return ResourcesCompat.getDrawable(this.f3123.f3221.getResources(), this.f3123.f3174, (Resources.Theme) null);
                    }
                    Drawable r05 = DialogUtils.m3855(this.f3123.f3221, R.attr.md_btn_positive_selector);
                    if (r05 != null) {
                        return r05;
                    }
                    Drawable r06 = DialogUtils.m3855(getContext(), R.attr.md_btn_positive_selector);
                    if (Build.VERSION.SDK_INT < 21) {
                        return r06;
                    }
                    RippleHelper.m3874(r06, this.f3123.f3177);
                    return r06;
            }
        } else if (this.f3123.f3173 != 0) {
            return ResourcesCompat.getDrawable(this.f3123.f3221.getResources(), this.f3123.f3173, (Resources.Theme) null);
        } else {
            Drawable r07 = DialogUtils.m3855(this.f3123.f3221, R.attr.md_btn_stacked_selector);
            return r07 == null ? DialogUtils.m3855(getContext(), R.attr.md_btn_stacked_selector) : r07;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final MDButton m3776(DialogAction dialogAction) {
        switch (dialogAction) {
            case NEUTRAL:
                return this.f3127;
            case NEGATIVE:
                return this.f3116;
            default:
                return this.f3126;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3777(int i) {
        m3771(m3766() + i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3778(int i, boolean z) {
        boolean z2 = true;
        if (this.f3112 != null) {
            if (this.f3123.f3153 > 0) {
                this.f3112.setText(String.format(Locale.getDefault(), "%d/%d", new Object[]{Integer.valueOf(i), Integer.valueOf(this.f3123.f3153)}));
                this.f3112.setVisibility(0);
            } else {
                this.f3112.setVisibility(8);
            }
            boolean z3 = (z && i == 0) || (this.f3123.f3153 > 0 && i > this.f3123.f3153) || i < this.f3123.f3152;
            int i2 = z3 ? this.f3123.f3154 : this.f3123.f3203;
            int i3 = z3 ? this.f3123.f3154 : this.f3123.f3183;
            if (this.f3123.f3153 > 0) {
                this.f3112.setTextColor(i2);
            }
            MDTintHelper.m3845(this.f3109, i3);
            MDButton r3 = m3776(DialogAction.POSITIVE);
            if (z3) {
                z2 = false;
            }
            r3.setEnabled(z2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3779(TextView textView, Typeface typeface) {
        if (typeface != null) {
            textView.setPaintFlags(textView.getPaintFlags() | 128);
            textView.setTypeface(typeface);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3780(CharSequence charSequence) {
        this.f3122.setText(charSequence);
        this.f3122.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m3781(MaterialDialog materialDialog, View view, int i, CharSequence charSequence, boolean z) {
        if (!view.isEnabled()) {
            return false;
        }
        if (this.f3117 == null || this.f3117 == ListType.REGULAR) {
            if (this.f3123.f3190) {
                dismiss();
            }
            if (!z && this.f3123.f3227 != null) {
                this.f3123.f3227.m3811(this, view, i, this.f3123.f3185.get(i));
            }
            if (z && this.f3123.f3206 != null) {
                return this.f3123.f3206.m3814(this, view, i, this.f3123.f3185.get(i));
            }
        } else if (this.f3117 == ListType.MULTI) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.md_control);
            if (!checkBox.isEnabled()) {
                return false;
            }
            if (!this.f3118.contains(Integer.valueOf(i))) {
                this.f3118.add(Integer.valueOf(i));
                if (!this.f3123.f3178) {
                    checkBox.setChecked(true);
                } else if (m3760()) {
                    checkBox.setChecked(true);
                } else {
                    this.f3118.remove(Integer.valueOf(i));
                }
            } else {
                this.f3118.remove(Integer.valueOf(i));
                if (!this.f3123.f3178) {
                    checkBox.setChecked(false);
                } else if (m3760()) {
                    checkBox.setChecked(false);
                } else {
                    this.f3118.add(Integer.valueOf(i));
                }
            }
        } else if (this.f3117 == ListType.SINGLE) {
            RadioButton radioButton = (RadioButton) view.findViewById(R.id.md_control);
            if (!radioButton.isEnabled()) {
                return false;
            }
            boolean z2 = true;
            int i2 = this.f3123.f3188;
            if (this.f3123.f3190 && this.f3123.f3179 == null) {
                dismiss();
                z2 = false;
                this.f3123.f3188 = i;
                m3761(view);
            } else if (this.f3123.f3163) {
                this.f3123.f3188 = i;
                z2 = m3761(view);
                this.f3123.f3188 = i2;
            }
            if (z2) {
                this.f3123.f3188 = i;
                radioButton.setChecked(true);
                this.f3123.f3210.notifyItemChanged(i2);
                this.f3123.f3210.notifyItemChanged(i);
            }
        }
        return true;
    }
}
