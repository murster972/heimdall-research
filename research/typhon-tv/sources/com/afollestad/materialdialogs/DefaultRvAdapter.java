package com.afollestad.materialdialogs;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;

class DefaultRvAdapter extends RecyclerView.Adapter<DefaultVH> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f3095;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public InternalListCallback f3096;

    /* renamed from: 齉  reason: contains not printable characters */
    private final GravityEnum f3097;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final MaterialDialog f3098;

    static class DefaultVH extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        /* renamed from: 靐  reason: contains not printable characters */
        final TextView f3100;

        /* renamed from: 齉  reason: contains not printable characters */
        final DefaultRvAdapter f3101;

        /* renamed from: 龘  reason: contains not printable characters */
        final CompoundButton f3102;

        DefaultVH(View view, DefaultRvAdapter defaultRvAdapter) {
            super(view);
            this.f3102 = (CompoundButton) view.findViewById(R.id.md_control);
            this.f3100 = (TextView) view.findViewById(R.id.md_title);
            this.f3101 = defaultRvAdapter;
            view.setOnClickListener(this);
            if (defaultRvAdapter.f3098.f3123.f3206 != null) {
                view.setOnLongClickListener(this);
            }
        }

        public void onClick(View view) {
            if (this.f3101.f3096 != null && getAdapterPosition() != -1) {
                CharSequence charSequence = null;
                if (this.f3101.f3098.f3123.f3185 != null && getAdapterPosition() < this.f3101.f3098.f3123.f3185.size()) {
                    charSequence = this.f3101.f3098.f3123.f3185.get(getAdapterPosition());
                }
                this.f3101.f3096.m3751(this.f3101.f3098, view, getAdapterPosition(), charSequence, false);
            }
        }

        public boolean onLongClick(View view) {
            if (this.f3101.f3096 == null || getAdapterPosition() == -1) {
                return false;
            }
            CharSequence charSequence = null;
            if (this.f3101.f3098.f3123.f3185 != null && getAdapterPosition() < this.f3101.f3098.f3123.f3185.size()) {
                charSequence = this.f3101.f3098.f3123.f3185.get(getAdapterPosition());
            }
            return this.f3101.f3096.m3751(this.f3101.f3098, view, getAdapterPosition(), charSequence, true);
        }
    }

    interface InternalListCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m3751(MaterialDialog materialDialog, View view, int i, CharSequence charSequence, boolean z);
    }

    DefaultRvAdapter(MaterialDialog materialDialog, int i) {
        this.f3098 = materialDialog;
        this.f3095 = i;
        this.f3097 = materialDialog.f3123.f3135;
    }

    @TargetApi(17)
    /* renamed from: 龘  reason: contains not printable characters */
    private void m3746(ViewGroup viewGroup) {
        ((LinearLayout) viewGroup).setGravity(this.f3097.getGravityInt() | 16);
        if (viewGroup.getChildCount() != 2) {
            return;
        }
        if (this.f3097 == GravityEnum.END && !m3747() && (viewGroup.getChildAt(0) instanceof CompoundButton)) {
            CompoundButton compoundButton = (CompoundButton) viewGroup.getChildAt(0);
            viewGroup.removeView(compoundButton);
            TextView textView = (TextView) viewGroup.getChildAt(0);
            viewGroup.removeView(textView);
            textView.setPadding(textView.getPaddingRight(), textView.getPaddingTop(), textView.getPaddingLeft(), textView.getPaddingBottom());
            viewGroup.addView(textView);
            viewGroup.addView(compoundButton);
        } else if (this.f3097 == GravityEnum.START && m3747() && (viewGroup.getChildAt(1) instanceof CompoundButton)) {
            CompoundButton compoundButton2 = (CompoundButton) viewGroup.getChildAt(1);
            viewGroup.removeView(compoundButton2);
            TextView textView2 = (TextView) viewGroup.getChildAt(0);
            viewGroup.removeView(textView2);
            textView2.setPadding(textView2.getPaddingRight(), textView2.getPaddingTop(), textView2.getPaddingRight(), textView2.getPaddingBottom());
            viewGroup.addView(compoundButton2);
            viewGroup.addView(textView2);
        }
    }

    @TargetApi(17)
    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m3747() {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 17) {
            return false;
        }
        if (this.f3098.m3770().m3796().getResources().getConfiguration().getLayoutDirection() != 1) {
            z = false;
        }
        return z;
    }

    public int getItemCount() {
        if (this.f3098.f3123.f3185 != null) {
            return this.f3098.f3123.f3185.size();
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DefaultVH onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(this.f3095, viewGroup, false);
        DialogUtils.m3870(inflate, this.f3098.m3769());
        return new DefaultVH(inflate, this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(DefaultVH defaultVH, int i) {
        View view = defaultVH.itemView;
        boolean r1 = DialogUtils.m3873(Integer.valueOf(i), (T[]) this.f3098.f3123.f3192);
        int r3 = r1 ? DialogUtils.m3861(this.f3098.f3123.f3141, 0.4f) : this.f3098.f3123.f3141;
        defaultVH.itemView.setEnabled(!r1);
        switch (this.f3098.f3117) {
            case SINGLE:
                RadioButton radioButton = (RadioButton) defaultVH.f3102;
                boolean z = this.f3098.f3123.f3188 == i;
                if (this.f3098.f3123.f3187 != null) {
                    MDTintHelper.m3849(radioButton, this.f3098.f3123.f3187);
                } else {
                    MDTintHelper.m3848(radioButton, this.f3098.f3123.f3183);
                }
                radioButton.setChecked(z);
                radioButton.setEnabled(!r1);
                break;
            case MULTI:
                CheckBox checkBox = (CheckBox) defaultVH.f3102;
                boolean contains = this.f3098.f3118.contains(Integer.valueOf(i));
                if (this.f3098.f3123.f3187 != null) {
                    MDTintHelper.m3844(checkBox, this.f3098.f3123.f3187);
                } else {
                    MDTintHelper.m3843(checkBox, this.f3098.f3123.f3183);
                }
                checkBox.setChecked(contains);
                checkBox.setEnabled(!r1);
                break;
        }
        defaultVH.f3100.setText(this.f3098.f3123.f3185.get(i));
        defaultVH.f3100.setTextColor(r3);
        this.f3098.m3779(defaultVH.f3100, this.f3098.f3123.f3196);
        m3746((ViewGroup) view);
        if (this.f3098.f3123.f3155 != null) {
            if (i < this.f3098.f3123.f3155.length) {
                view.setId(this.f3098.f3123.f3155[i]);
            } else {
                view.setId(-1);
            }
        }
        if (Build.VERSION.SDK_INT >= 21) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup.getChildCount() != 2) {
                return;
            }
            if (viewGroup.getChildAt(0) instanceof CompoundButton) {
                viewGroup.getChildAt(0).setBackground((Drawable) null);
            } else if (viewGroup.getChildAt(1) instanceof CompoundButton) {
                viewGroup.getChildAt(1).setBackground((Drawable) null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3750(InternalListCallback internalListCallback) {
        this.f3096 = internalListCallback;
    }
}
