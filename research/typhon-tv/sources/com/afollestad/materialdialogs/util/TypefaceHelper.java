package com.afollestad.materialdialogs.util;

import android.graphics.Typeface;
import android.support.v4.util.SimpleArrayMap;

public class TypefaceHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final SimpleArrayMap<String, Typeface> f3299 = new SimpleArrayMap<>();

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Typeface m3875(android.content.Context r7, java.lang.String r8) {
        /*
            android.support.v4.util.SimpleArrayMap<java.lang.String, android.graphics.Typeface> r3 = f3299
            monitor-enter(r3)
            android.support.v4.util.SimpleArrayMap<java.lang.String, android.graphics.Typeface> r2 = f3299     // Catch:{ all -> 0x002b }
            boolean r2 = r2.containsKey(r8)     // Catch:{ all -> 0x002b }
            if (r2 != 0) goto L_0x002e
            android.content.res.AssetManager r2 = r7.getAssets()     // Catch:{ RuntimeException -> 0x0027 }
            java.lang.String r4 = "fonts/%s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ RuntimeException -> 0x0027 }
            r6 = 0
            r5[r6] = r8     // Catch:{ RuntimeException -> 0x0027 }
            java.lang.String r4 = java.lang.String.format(r4, r5)     // Catch:{ RuntimeException -> 0x0027 }
            android.graphics.Typeface r1 = android.graphics.Typeface.createFromAsset(r2, r4)     // Catch:{ RuntimeException -> 0x0027 }
            android.support.v4.util.SimpleArrayMap<java.lang.String, android.graphics.Typeface> r2 = f3299     // Catch:{ RuntimeException -> 0x0027 }
            r2.put(r8, r1)     // Catch:{ RuntimeException -> 0x0027 }
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
        L_0x0026:
            return r1
        L_0x0027:
            r0 = move-exception
            r1 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            goto L_0x0026
        L_0x002b:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            throw r2
        L_0x002e:
            android.support.v4.util.SimpleArrayMap<java.lang.String, android.graphics.Typeface> r2 = f3299     // Catch:{ all -> 0x002b }
            java.lang.Object r2 = r2.get(r8)     // Catch:{ all -> 0x002b }
            android.graphics.Typeface r2 = (android.graphics.Typeface) r2     // Catch:{ all -> 0x002b }
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            r1 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.afollestad.materialdialogs.util.TypefaceHelper.m3875(android.content.Context, java.lang.String):android.graphics.Typeface");
    }
}
