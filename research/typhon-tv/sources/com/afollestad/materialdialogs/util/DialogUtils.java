package com.afollestad.materialdialogs.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

public class DialogUtils {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m3852(Context context, int i) {
        return m3856(context, i, -1);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m3853(Context context, int i) {
        return m3872(context, i, false);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static ColorStateList m3854(Context context, int i) {
        int r1 = m3863(context, 16842806);
        if (i == 0) {
            i = r1;
        }
        return new ColorStateList(new int[][]{new int[]{-16842910}, new int[0]}, new int[]{m3861(i, 0.4f), i});
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static Drawable m3855(Context context, int i) {
        return m3867(context, i, (Drawable) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m3856(Context context, int i, int i2) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getDimensionPixelSize(0, i2);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static ColorStateList m3857(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        context.getResources().getValue(i, typedValue, true);
        return (typedValue.type < 28 || typedValue.type > 31) ? Build.VERSION.SDK_INT <= 22 ? context.getResources().getColorStateList(i) : context.getColorStateList(i) : m3854(context, typedValue.data);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m3858(DialogInterface dialogInterface, MaterialDialog.Builder builder) {
        InputMethodManager inputMethodManager;
        MaterialDialog materialDialog = (MaterialDialog) dialogInterface;
        if (materialDialog.m3764() != null && (inputMethodManager = (InputMethodManager) builder.m3796().getSystemService("input_method")) != null) {
            View currentFocus = materialDialog.getCurrentFocus();
            IBinder iBinder = null;
            if (currentFocus != null) {
                iBinder = currentFocus.getWindowToken();
            } else if (materialDialog.m3763() != null) {
                iBinder = materialDialog.m3763().getWindowToken();
            }
            if (iBinder != null) {
                inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m3859(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i, typedValue, true);
        return (String) typedValue.string;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static int m3860(Context context, int i) {
        return ContextCompat.getColor(context, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3861(int i, float f) {
        return Color.argb(Math.round(((float) Color.alpha(i)) * f), Color.red(i), Color.green(i), Color.blue(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3862(Context context) {
        return m3861(m3871(m3863(context, 16842806)) ? -16777216 : -1, 0.3f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3863(Context context, int i) {
        return m3864(context, i, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3864(Context context, int i, int i2) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getColor(0, i2);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m3865(GravityEnum gravityEnum) {
        switch (gravityEnum) {
            case CENTER:
                return 1;
            case END:
                return 2;
            default:
                return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ColorStateList m3866(Context context, int i, ColorStateList colorStateList) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            TypedValue peekValue = obtainStyledAttributes.peekValue(0);
            if (peekValue == null) {
                return colorStateList;
            }
            if (peekValue.type < 28 || peekValue.type > 31) {
                ColorStateList colorStateList2 = obtainStyledAttributes.getColorStateList(0);
                if (colorStateList2 != null) {
                    obtainStyledAttributes.recycle();
                    return colorStateList2;
                }
                obtainStyledAttributes.recycle();
                return colorStateList;
            }
            ColorStateList r8 = m3854(context, peekValue.data);
            obtainStyledAttributes.recycle();
            return r8;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Drawable m3867(Context context, int i, Drawable drawable) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            Drawable drawable2 = obtainStyledAttributes.getDrawable(0);
            if (drawable2 == null && drawable != null) {
                drawable2 = drawable;
            }
            return drawable2;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GravityEnum m3868(Context context, int i, GravityEnum gravityEnum) {
        GravityEnum gravityEnum2;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            switch (obtainStyledAttributes.getInt(0, m3865(gravityEnum))) {
                case 1:
                    gravityEnum2 = GravityEnum.CENTER;
                    obtainStyledAttributes.recycle();
                    break;
                case 2:
                    gravityEnum2 = GravityEnum.END;
                    obtainStyledAttributes.recycle();
                    break;
                default:
                    gravityEnum2 = GravityEnum.START;
                    break;
            }
            return gravityEnum2;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3869(DialogInterface dialogInterface, final MaterialDialog.Builder builder) {
        final MaterialDialog materialDialog = (MaterialDialog) dialogInterface;
        if (materialDialog.m3764() != null) {
            materialDialog.m3764().post(new Runnable() {
                public void run() {
                    materialDialog.m3764().requestFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) builder.m3796().getSystemService("input_method");
                    if (inputMethodManager != null) {
                        inputMethodManager.showSoftInput(materialDialog.m3764(), 1);
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3870(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT < 16) {
            view.setBackgroundDrawable(drawable);
        } else {
            view.setBackground(drawable);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3871(int i) {
        return 1.0d - ((((0.299d * ((double) Color.red(i))) + (0.587d * ((double) Color.green(i)))) + (0.114d * ((double) Color.blue(i)))) / 255.0d) >= 0.5d;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3872(Context context, int i, boolean z) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getBoolean(0, z);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> boolean m3873(T t, T[] tArr) {
        if (tArr == null || tArr.length == 0) {
            return false;
        }
        for (T equals : tArr) {
            if (equals.equals(t)) {
                return true;
            }
        }
        return false;
    }
}
