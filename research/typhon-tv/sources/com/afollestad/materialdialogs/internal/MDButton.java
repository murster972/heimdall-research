package com.afollestad.materialdialogs.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.R;
import com.afollestad.materialdialogs.util.DialogUtils;

@SuppressLint({"AppCompatCustomView"})
public class MDButton extends TextView {

    /* renamed from: 连任  reason: contains not printable characters */
    private Drawable f3235;

    /* renamed from: 靐  reason: contains not printable characters */
    private GravityEnum f3236;

    /* renamed from: 麤  reason: contains not printable characters */
    private Drawable f3237;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f3238;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f3239 = false;

    public MDButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m3818(context);
    }

    public MDButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m3818(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3818(Context context) {
        this.f3238 = context.getResources().getDimensionPixelSize(R.dimen.md_dialog_frame_margin);
        this.f3236 = GravityEnum.END;
    }

    public void setAllCapsCompat(boolean z) {
        if (Build.VERSION.SDK_INT >= 14) {
            setAllCaps(z);
        } else if (z) {
            setTransformationMethod(new AllCapsTransformationMethod(getContext()));
        } else {
            setTransformationMethod((TransformationMethod) null);
        }
    }

    public void setDefaultSelector(Drawable drawable) {
        this.f3235 = drawable;
        if (!this.f3239) {
            m3819(false, true);
        }
    }

    public void setStackedGravity(GravityEnum gravityEnum) {
        this.f3236 = gravityEnum;
    }

    public void setStackedSelector(Drawable drawable) {
        this.f3237 = drawable;
        if (this.f3239) {
            m3819(true, true);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3819(boolean z, boolean z2) {
        if (this.f3239 != z || z2) {
            setGravity(z ? this.f3236.getGravityInt() | 16 : 17);
            if (Build.VERSION.SDK_INT >= 17) {
                setTextAlignment(z ? this.f3236.getTextAlignment() : 4);
            }
            DialogUtils.m3870((View) this, z ? this.f3237 : this.f3235);
            if (z) {
                setPadding(this.f3238, getPaddingTop(), this.f3238, getPaddingBottom());
            }
            this.f3239 = z;
        }
    }
}
