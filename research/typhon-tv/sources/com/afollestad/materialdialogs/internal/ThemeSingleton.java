package com.afollestad.materialdialogs.internal;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import com.afollestad.materialdialogs.GravityEnum;

public class ThemeSingleton {

    /* renamed from: י  reason: contains not printable characters */
    private static ThemeSingleton f3273;

    /* renamed from: ʻ  reason: contains not printable characters */
    public ColorStateList f3274 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f3275 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f3276 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f3277 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f3278 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    public GravityEnum f3279 = GravityEnum.END;

    /* renamed from: ˈ  reason: contains not printable characters */
    public ColorStateList f3280 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    public GravityEnum f3281 = GravityEnum.START;

    /* renamed from: ˊ  reason: contains not printable characters */
    public int f3282 = 0;

    /* renamed from: ˋ  reason: contains not printable characters */
    public GravityEnum f3283 = GravityEnum.START;

    /* renamed from: ˎ  reason: contains not printable characters */
    public GravityEnum f3284 = GravityEnum.START;

    /* renamed from: ˏ  reason: contains not printable characters */
    public GravityEnum f3285 = GravityEnum.START;

    /* renamed from: ˑ  reason: contains not printable characters */
    public Drawable f3286 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    public int f3287 = 0;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f3288 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    public ColorStateList f3289 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public int f3290 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    public ColorStateList f3291 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public int f3292 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f3293 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public int f3294 = 0;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public int f3295 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public static ThemeSingleton m3850() {
        return m3851(true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ThemeSingleton m3851(boolean z) {
        if (f3273 == null && z) {
            f3273 = new ThemeSingleton();
        }
        return f3273;
    }
}
