package com.afollestad.materialdialogs.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ScrollView;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.R;
import com.afollestad.materialdialogs.StackingBehavior;
import com.afollestad.materialdialogs.util.DialogUtils;

public class MDRootLayout extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f3240 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private StackingBehavior f3241 = StackingBehavior.ADAPTIVE;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f3242 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f3243;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f3244;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f3245;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f3246;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Paint f3247;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ViewTreeObserver.OnScrollChangedListener f3248;

    /* renamed from: ˎ  reason: contains not printable characters */
    private ViewTreeObserver.OnScrollChangedListener f3249;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f3250 = true;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f3251;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f3252;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f3253 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f3254;

    /* renamed from: 麤  reason: contains not printable characters */
    private View f3255;

    /* renamed from: 齉  reason: contains not printable characters */
    private View f3256;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final MDButton[] f3257 = new MDButton[3];

    /* renamed from: ﹶ  reason: contains not printable characters */
    private GravityEnum f3258 = GravityEnum.START;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f3259;

    public MDRootLayout(Context context) {
        super(context);
        m3825(context, (AttributeSet) null, 0);
    }

    public MDRootLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m3825(context, attributeSet, 0);
    }

    @TargetApi(11)
    public MDRootLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m3825(context, attributeSet, i);
    }

    @TargetApi(21)
    public MDRootLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m3825(context, attributeSet, i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static View m3820(ViewGroup viewGroup) {
        if (viewGroup == null || viewGroup.getChildCount() == 0) {
            return null;
        }
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt.getVisibility() == 0 && childAt.getTop() == 0) {
                return childAt;
            }
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m3821() {
        if (Build.VERSION.SDK_INT >= 17 && getResources().getConfiguration().getLayoutDirection() == 1) {
            switch (this.f3258) {
                case START:
                    this.f3258 = GravityEnum.END;
                    return;
                case END:
                    this.f3258 = GravityEnum.START;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m3822(WebView webView) {
        return ((float) webView.getMeasuredHeight()) < ((float) webView.getContentHeight()) * webView.getScale();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static View m3824(ViewGroup viewGroup) {
        if (viewGroup == null || viewGroup.getChildCount() == 0) {
            return null;
        }
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt.getVisibility() == 0 && childAt.getBottom() == viewGroup.getMeasuredHeight()) {
                return childAt;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3825(Context context, AttributeSet attributeSet, int i) {
        Resources resources = context.getResources();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.MDRootLayout, i, 0);
        this.f3251 = obtainStyledAttributes.getBoolean(R.styleable.MDRootLayout_md_reduce_padding_no_title_no_buttons, true);
        obtainStyledAttributes.recycle();
        this.f3246 = resources.getDimensionPixelSize(R.dimen.md_notitle_vertical_padding);
        this.f3243 = resources.getDimensionPixelSize(R.dimen.md_button_frame_vertical_padding);
        this.f3259 = resources.getDimensionPixelSize(R.dimen.md_button_padding_frame_side);
        this.f3244 = resources.getDimensionPixelSize(R.dimen.md_button_height);
        this.f3247 = new Paint();
        this.f3245 = resources.getDimensionPixelSize(R.dimen.md_divider_height);
        this.f3247.setColor(DialogUtils.m3863(context, R.attr.md_divider_color));
        setWillNotDraw(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3826(final View view, final boolean z, final boolean z2) {
        if (view != null) {
            if (view instanceof ScrollView) {
                ScrollView scrollView = (ScrollView) view;
                if (m3837(scrollView)) {
                    m3827((ViewGroup) scrollView, z, z2);
                    return;
                }
                if (z) {
                    this.f3253 = false;
                }
                if (z2) {
                    this.f3240 = false;
                }
            } else if (view instanceof AdapterView) {
                AdapterView adapterView = (AdapterView) view;
                if (m3836(adapterView)) {
                    m3827((ViewGroup) adapterView, z, z2);
                    return;
                }
                if (z) {
                    this.f3253 = false;
                }
                if (z2) {
                    this.f3240 = false;
                }
            } else if (view instanceof WebView) {
                view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        if (view.getMeasuredHeight() == 0) {
                            return true;
                        }
                        if (!MDRootLayout.m3822((WebView) view)) {
                            if (z) {
                                boolean unused = MDRootLayout.this.f3253 = false;
                            }
                            if (z2) {
                                boolean unused2 = MDRootLayout.this.f3240 = false;
                            }
                        } else {
                            MDRootLayout.this.m3827((ViewGroup) view, z, z2);
                        }
                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                        return true;
                    }
                });
            } else if (view instanceof RecyclerView) {
                boolean r1 = m3833((RecyclerView) view);
                if (z) {
                    this.f3253 = r1;
                }
                if (z2) {
                    this.f3240 = r1;
                }
                if (r1) {
                    m3827((ViewGroup) view, z, z2);
                }
            } else if (view instanceof ViewGroup) {
                View r3 = m3820((ViewGroup) view);
                m3826(r3, z, z2);
                View r0 = m3824((ViewGroup) view);
                if (r0 != r3) {
                    m3826(r0, false, true);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3827(final ViewGroup viewGroup, final boolean z, final boolean z2) {
        if ((!z2 && this.f3248 == null) || (z2 && this.f3249 == null)) {
            if (viewGroup instanceof RecyclerView) {
                AnonymousClass2 r1 = new RecyclerView.OnScrollListener() {
                    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                        super.onScrolled(recyclerView, i, i2);
                        boolean z = false;
                        MDButton[] r3 = MDRootLayout.this.f3257;
                        int length = r3.length;
                        int i3 = 0;
                        while (true) {
                            if (i3 < length) {
                                MDButton mDButton = r3[i3];
                                if (mDButton != null && mDButton.getVisibility() != 8) {
                                    z = true;
                                    break;
                                }
                                i3++;
                            } else {
                                break;
                            }
                        }
                        MDRootLayout.this.m3828(viewGroup, z, z2, z);
                        MDRootLayout.this.invalidate();
                    }
                };
                ((RecyclerView) viewGroup).addOnScrollListener(r1);
                r1.onScrolled((RecyclerView) viewGroup, 0, 0);
                return;
            }
            AnonymousClass3 r0 = new ViewTreeObserver.OnScrollChangedListener() {
                public void onScrollChanged() {
                    boolean z = false;
                    MDButton[] r3 = MDRootLayout.this.f3257;
                    int length = r3.length;
                    int i = 0;
                    while (true) {
                        if (i < length) {
                            MDButton mDButton = r3[i];
                            if (mDButton != null && mDButton.getVisibility() != 8) {
                                z = true;
                                break;
                            }
                            i++;
                        } else {
                            break;
                        }
                    }
                    if (viewGroup instanceof WebView) {
                        MDRootLayout.this.m3829((WebView) viewGroup, z, z2, z);
                    } else {
                        MDRootLayout.this.m3828(viewGroup, z, z2, z);
                    }
                    MDRootLayout.this.invalidate();
                }
            };
            if (!z2) {
                this.f3248 = r0;
                viewGroup.getViewTreeObserver().addOnScrollChangedListener(this.f3248);
            } else {
                this.f3249 = r0;
                viewGroup.getViewTreeObserver().addOnScrollChangedListener(this.f3249);
            }
            r0.onScrollChanged();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3828(ViewGroup viewGroup, boolean z, boolean z2, boolean z3) {
        boolean z4 = true;
        if (z && viewGroup.getChildCount() > 0) {
            this.f3253 = (this.f3256 == null || this.f3256.getVisibility() == 8 || viewGroup.getScrollY() + viewGroup.getPaddingTop() <= viewGroup.getChildAt(0).getTop()) ? false : true;
        }
        if (z2 && viewGroup.getChildCount() > 0) {
            if (!z3 || (viewGroup.getScrollY() + viewGroup.getHeight()) - viewGroup.getPaddingBottom() >= viewGroup.getChildAt(viewGroup.getChildCount() - 1).getBottom()) {
                z4 = false;
            }
            this.f3240 = z4;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3829(WebView webView, boolean z, boolean z2, boolean z3) {
        boolean z4 = true;
        if (z) {
            this.f3253 = (this.f3256 == null || this.f3256.getVisibility() == 8 || webView.getScrollY() + webView.getPaddingTop() <= 0) ? false : true;
        }
        if (z2) {
            if (!z3 || ((float) ((webView.getScrollY() + webView.getMeasuredHeight()) - webView.getPaddingBottom())) >= ((float) webView.getContentHeight()) * webView.getScale()) {
                z4 = false;
            }
            this.f3240 = z4;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3833(RecyclerView recyclerView) {
        return (recyclerView == null || recyclerView.getLayoutManager() == null || !recyclerView.getLayoutManager().canScrollVertically()) ? false : true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m3834(View view) {
        boolean z = (view == null || view.getVisibility() == 8) ? false : true;
        return (!z || !(view instanceof MDButton)) ? z : ((MDButton) view).getText().toString().trim().length() > 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m3836(AdapterView adapterView) {
        if (adapterView.getLastVisiblePosition() == -1) {
            return false;
        }
        return !(adapterView.getFirstVisiblePosition() == 0) || !(adapterView.getLastVisiblePosition() == adapterView.getCount() + -1) || adapterView.getChildCount() <= 0 || adapterView.getChildAt(0).getTop() < adapterView.getPaddingTop() || adapterView.getChildAt(adapterView.getChildCount() + -1).getBottom() > adapterView.getHeight() - adapterView.getPaddingBottom();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
        r0 = r4.getChildAt(0).getMeasuredHeight();
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m3837(android.widget.ScrollView r4) {
        /*
            r1 = 0
            int r2 = r4.getChildCount()
            if (r2 != 0) goto L_0x0008
        L_0x0007:
            return r1
        L_0x0008:
            android.view.View r2 = r4.getChildAt(r1)
            int r0 = r2.getMeasuredHeight()
            int r2 = r4.getMeasuredHeight()
            int r3 = r4.getPaddingTop()
            int r2 = r2 - r3
            int r3 = r4.getPaddingBottom()
            int r2 = r2 - r3
            if (r2 >= r0) goto L_0x0007
            r1 = 1
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.afollestad.materialdialogs.internal.MDRootLayout.m3837(android.widget.ScrollView):boolean");
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f3255 != null) {
            if (this.f3253) {
                int top = this.f3255.getTop();
                canvas.drawRect(0.0f, (float) (top - this.f3245), (float) getMeasuredWidth(), (float) top, this.f3247);
            }
            if (this.f3240) {
                int bottom = this.f3255.getBottom();
                canvas.drawRect(0.0f, (float) bottom, (float) getMeasuredWidth(), (float) (this.f3245 + bottom), this.f3247);
            }
        }
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getId() == R.id.md_titleFrame) {
                this.f3256 = childAt;
            } else if (childAt.getId() == R.id.md_buttonDefaultNeutral) {
                this.f3257[0] = (MDButton) childAt;
            } else if (childAt.getId() == R.id.md_buttonDefaultNegative) {
                this.f3257[1] = (MDButton) childAt;
            } else if (childAt.getId() == R.id.md_buttonDefaultPositive) {
                this.f3257[2] = (MDButton) childAt;
            } else {
                this.f3255 = childAt;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int measuredWidth;
        int i8;
        int measuredWidth2;
        if (m3834(this.f3256)) {
            int measuredHeight = this.f3256.getMeasuredHeight();
            this.f3256.layout(i, i2, i3, i2 + measuredHeight);
            i2 += measuredHeight;
        } else if (!this.f3252 && this.f3250) {
            i2 += this.f3246;
        }
        if (m3834(this.f3255)) {
            this.f3255.layout(i, i2, i3, this.f3255.getMeasuredHeight() + i2);
        }
        if (this.f3242) {
            int i9 = i4 - this.f3243;
            for (MDButton mDButton : this.f3257) {
                if (m3834((View) mDButton)) {
                    mDButton.layout(i, i9 - mDButton.getMeasuredHeight(), i3, i9);
                    i9 -= mDButton.getMeasuredHeight();
                }
            }
        } else {
            int i10 = i4;
            if (this.f3250) {
                i10 -= this.f3243;
            }
            int i11 = i10 - this.f3244;
            int i12 = this.f3259;
            int i13 = -1;
            int i14 = -1;
            if (m3834((View) this.f3257[2])) {
                if (this.f3258 == GravityEnum.END) {
                    measuredWidth2 = i + i12;
                    i8 = measuredWidth2 + this.f3257[2].getMeasuredWidth();
                } else {
                    i8 = i3 - i12;
                    measuredWidth2 = i8 - this.f3257[2].getMeasuredWidth();
                    i14 = measuredWidth2;
                }
                this.f3257[2].layout(measuredWidth2, i11, i8, i10);
                i12 += this.f3257[2].getMeasuredWidth();
            }
            if (m3834((View) this.f3257[1])) {
                if (this.f3258 == GravityEnum.END) {
                    i7 = i + i12;
                    measuredWidth = i7 + this.f3257[1].getMeasuredWidth();
                } else if (this.f3258 == GravityEnum.START) {
                    measuredWidth = i3 - i12;
                    i7 = measuredWidth - this.f3257[1].getMeasuredWidth();
                } else {
                    i7 = i + this.f3259;
                    measuredWidth = i7 + this.f3257[1].getMeasuredWidth();
                    i13 = measuredWidth;
                }
                this.f3257[1].layout(i7, i11, measuredWidth, i10);
            }
            if (m3834((View) this.f3257[0])) {
                if (this.f3258 == GravityEnum.END) {
                    i6 = i3 - this.f3259;
                    i5 = i6 - this.f3257[0].getMeasuredWidth();
                } else if (this.f3258 == GravityEnum.START) {
                    i5 = i + this.f3259;
                    i6 = i5 + this.f3257[0].getMeasuredWidth();
                } else {
                    if (i13 == -1 && i14 != -1) {
                        i13 = i14 - this.f3257[0].getMeasuredWidth();
                    } else if (i14 == -1 && i13 != -1) {
                        i14 = i13 + this.f3257[0].getMeasuredWidth();
                    } else if (i14 == -1) {
                        i13 = ((i3 - i) / 2) - (this.f3257[0].getMeasuredWidth() / 2);
                        i14 = i13 + this.f3257[0].getMeasuredWidth();
                    }
                    i5 = i13;
                    i6 = i14;
                }
                this.f3257[0].layout(i5, i11, i6, i10);
            }
        }
        m3826(this.f3255, true, true);
    }

    public void onMeasure(int i, int i2) {
        boolean z;
        int i3;
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        if (size2 > this.f3254) {
            size2 = this.f3254;
        }
        this.f3250 = true;
        boolean z2 = false;
        if (this.f3241 == StackingBehavior.ALWAYS) {
            z = true;
        } else if (this.f3241 == StackingBehavior.NEVER) {
            z = false;
        } else {
            int i4 = 0;
            for (MDButton mDButton : this.f3257) {
                if (mDButton != null && m3834((View) mDButton)) {
                    mDButton.m3819(false, false);
                    measureChild(mDButton, i, i2);
                    i4 += mDButton.getMeasuredWidth();
                    z2 = true;
                }
            }
            z = i4 > size - (getContext().getResources().getDimensionPixelSize(R.dimen.md_neutral_button_margin) * 2);
        }
        int i5 = 0;
        this.f3242 = z;
        if (z) {
            for (MDButton mDButton2 : this.f3257) {
                if (mDButton2 != null && m3834((View) mDButton2)) {
                    mDButton2.m3819(true, false);
                    measureChild(mDButton2, i, i2);
                    i5 += mDButton2.getMeasuredHeight();
                    z2 = true;
                }
            }
        }
        int i6 = size2;
        int i7 = 0;
        if (!z2) {
            i3 = 0 + (this.f3243 * 2);
        } else if (this.f3242) {
            i6 -= i5;
            i3 = 0 + (this.f3243 * 2);
            i7 = 0 + (this.f3243 * 2);
        } else {
            i6 -= this.f3244;
            i3 = 0 + (this.f3243 * 2);
        }
        if (m3834(this.f3256)) {
            this.f3256.measure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), 0);
            i6 -= this.f3256.getMeasuredHeight();
        } else if (!this.f3252) {
            i3 += this.f3246;
        }
        if (m3834(this.f3255)) {
            this.f3255.measure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(i6 - i7, Integer.MIN_VALUE));
            if (this.f3255.getMeasuredHeight() > i6 - i3) {
                this.f3250 = false;
                i6 = 0;
            } else if (!this.f3251 || m3834(this.f3256) || z2) {
                this.f3250 = true;
                i6 -= this.f3255.getMeasuredHeight() + i3;
            } else {
                this.f3250 = false;
                i6 -= this.f3255.getMeasuredHeight() + i7;
            }
        }
        setMeasuredDimension(size, size2 - i6);
    }

    public void setButtonGravity(GravityEnum gravityEnum) {
        this.f3258 = gravityEnum;
        m3821();
    }

    public void setButtonStackedGravity(GravityEnum gravityEnum) {
        for (MDButton mDButton : this.f3257) {
            if (mDButton != null) {
                mDButton.setStackedGravity(gravityEnum);
            }
        }
    }

    public void setDividerColor(int i) {
        this.f3247.setColor(i);
        invalidate();
    }

    public void setMaxHeight(int i) {
        this.f3254 = i;
    }

    public void setStackingBehavior(StackingBehavior stackingBehavior) {
        this.f3241 = stackingBehavior;
        invalidate();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3840() {
        this.f3252 = true;
    }
}
