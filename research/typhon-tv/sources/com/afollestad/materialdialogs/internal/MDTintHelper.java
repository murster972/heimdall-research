package com.afollestad.materialdialogs.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import com.afollestad.materialdialogs.R;
import com.afollestad.materialdialogs.util.DialogUtils;
import java.lang.reflect.Field;

@SuppressLint({"PrivateResource"})
public class MDTintHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    private static void m3841(EditText editText, int i) {
        try {
            Field declaredField = TextView.class.getDeclaredField("mCursorDrawableRes");
            declaredField.setAccessible(true);
            int i2 = declaredField.getInt(editText);
            Field declaredField2 = TextView.class.getDeclaredField("mEditor");
            declaredField2.setAccessible(true);
            Object obj = declaredField2.get(editText);
            Field declaredField3 = obj.getClass().getDeclaredField("mCursorDrawable");
            declaredField3.setAccessible(true);
            Drawable[] drawableArr = {ContextCompat.getDrawable(editText.getContext(), i2), ContextCompat.getDrawable(editText.getContext(), i2)};
            drawableArr[0].setColorFilter(i, PorterDuff.Mode.SRC_IN);
            drawableArr[1].setColorFilter(i, PorterDuff.Mode.SRC_IN);
            declaredField3.set(obj, drawableArr);
        } catch (NoSuchFieldException e) {
            Log.d("MDTintHelper", "Device issue with cursor tinting: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ColorStateList m3842(Context context, int i) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        iArr[0] = new int[]{-16842910};
        iArr2[0] = DialogUtils.m3863(context, R.attr.colorControlNormal);
        int i2 = 0 + 1;
        iArr[i2] = new int[]{-16842919, -16842908};
        iArr2[i2] = DialogUtils.m3863(context, R.attr.colorControlNormal);
        int i3 = i2 + 1;
        iArr[i3] = new int[0];
        iArr2[i3] = i;
        return new ColorStateList(iArr, iArr2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3843(CheckBox checkBox, int i) {
        int r0 = DialogUtils.m3862(checkBox.getContext());
        m3844(checkBox, new ColorStateList(new int[][]{new int[]{16842910, -16842912}, new int[]{16842910, 16842912}, new int[]{-16842910, -16842912}, new int[]{-16842910, 16842912}}, new int[]{DialogUtils.m3863(checkBox.getContext(), R.attr.colorControlNormal), i, r0, r0}));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3844(CheckBox checkBox, ColorStateList colorStateList) {
        if (Build.VERSION.SDK_INT >= 22) {
            checkBox.setButtonTintList(colorStateList);
            return;
        }
        Drawable wrap = DrawableCompat.wrap(ContextCompat.getDrawable(checkBox.getContext(), R.drawable.abc_btn_check_material));
        DrawableCompat.setTintList(wrap, colorStateList);
        checkBox.setButtonDrawable(wrap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3845(EditText editText, int i) {
        ColorStateList r0 = m3842(editText.getContext(), i);
        if (editText instanceof AppCompatEditText) {
            ((AppCompatEditText) editText).setSupportBackgroundTintList(r0);
        } else if (Build.VERSION.SDK_INT >= 21) {
            editText.setBackgroundTintList(r0);
        }
        m3841(editText, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3846(ProgressBar progressBar, int i) {
        m3847(progressBar, i, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m3847(ProgressBar progressBar, int i, boolean z) {
        ColorStateList valueOf = ColorStateList.valueOf(i);
        if (Build.VERSION.SDK_INT >= 21) {
            progressBar.setProgressTintList(valueOf);
            progressBar.setSecondaryProgressTintList(valueOf);
            if (!z) {
                progressBar.setIndeterminateTintList(valueOf);
                return;
            }
            return;
        }
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
        if (Build.VERSION.SDK_INT <= 10) {
            mode = PorterDuff.Mode.MULTIPLY;
        }
        if (!z && progressBar.getIndeterminateDrawable() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(i, mode);
        }
        if (progressBar.getProgressDrawable() != null) {
            progressBar.getProgressDrawable().setColorFilter(i, mode);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3848(RadioButton radioButton, int i) {
        int r0 = DialogUtils.m3862(radioButton.getContext());
        m3849(radioButton, new ColorStateList(new int[][]{new int[]{16842910, -16842912}, new int[]{16842910, 16842912}, new int[]{-16842910, -16842912}, new int[]{-16842910, 16842912}}, new int[]{DialogUtils.m3863(radioButton.getContext(), R.attr.colorControlNormal), i, r0, r0}));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3849(RadioButton radioButton, ColorStateList colorStateList) {
        if (Build.VERSION.SDK_INT >= 22) {
            radioButton.setButtonTintList(colorStateList);
            return;
        }
        Drawable wrap = DrawableCompat.wrap(ContextCompat.getDrawable(radioButton.getContext(), R.drawable.abc_btn_radio_material));
        DrawableCompat.setTintList(wrap, colorStateList);
        radioButton.setButtonDrawable(wrap);
    }
}
