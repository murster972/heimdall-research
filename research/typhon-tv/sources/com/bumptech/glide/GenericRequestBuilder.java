package com.bumptech.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.UnitTransformation;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.provider.ChildLoadProvider;
import com.bumptech.glide.provider.LoadProvider;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.GenericRequest;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestCoordinator;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.ThumbnailRequestCoordinator;
import com.bumptech.glide.request.animation.GlideAnimationFactory;
import com.bumptech.glide.request.animation.NoAnimation;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.EmptySignature;
import com.bumptech.glide.util.Util;

public class GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> implements Cloneable {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected final Lifecycle f19512;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ChildLoadProvider<ModelType, DataType, ResourceType, TranscodeType> f19513;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ModelType f19514;

    /* renamed from: ʾ  reason: contains not printable characters */
    private RequestListener<? super ModelType, TranscodeType> f19515;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Float f19516;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f19517;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f19518;

    /* renamed from: ˉ  reason: contains not printable characters */
    private GlideAnimationFactory<TranscodeType> f19519;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Drawable f19520;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Drawable f19521;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Priority f19522;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f19523;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Key f19524;

    /* renamed from: י  reason: contains not printable characters */
    private int f19525;

    /* renamed from: ـ  reason: contains not printable characters */
    private DiskCacheStrategy f19526;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f19527;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f19528;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private Transformation<ResourceType> f19529;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f19530;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f19531;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private Drawable f19532;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f19533;

    /* renamed from: 连任  reason: contains not printable characters */
    protected final RequestTracker f19534;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final Context f19535;

    /* renamed from: 麤  reason: contains not printable characters */
    protected final Class<TranscodeType> f19536;

    /* renamed from: 齉  reason: contains not printable characters */
    protected final Glide f19537;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Class<ModelType> f19538;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private GenericRequestBuilder<?, ?, ?, TranscodeType> f19539;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Float f19540;

    /* renamed from: com.bumptech.glide.GenericRequestBuilder$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f19543 = new int[ImageView.ScaleType.values().length];

        static {
            try {
                f19543[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f19543[ImageView.ScaleType.FIT_CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f19543[ImageView.ScaleType.FIT_START.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f19543[ImageView.ScaleType.FIT_END.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    GenericRequestBuilder(Context context, Class<ModelType> cls, LoadProvider<ModelType, DataType, ResourceType, TranscodeType> loadProvider, Class<TranscodeType> cls2, Glide glide, RequestTracker requestTracker, Lifecycle lifecycle) {
        ChildLoadProvider<ModelType, DataType, ResourceType, TranscodeType> childLoadProvider = null;
        this.f19524 = EmptySignature.m26164();
        this.f19540 = Float.valueOf(1.0f);
        this.f19522 = null;
        this.f19517 = true;
        this.f19519 = NoAnimation.m26103();
        this.f19523 = -1;
        this.f19525 = -1;
        this.f19526 = DiskCacheStrategy.RESULT;
        this.f19529 = UnitTransformation.m25719();
        this.f19535 = context;
        this.f19538 = cls;
        this.f19536 = cls2;
        this.f19537 = glide;
        this.f19534 = requestTracker;
        this.f19512 = lifecycle;
        this.f19513 = loadProvider != null ? new ChildLoadProvider<>(loadProvider) : childLoadProvider;
        if (context == null) {
            throw new NullPointerException("Context can't be null");
        } else if (cls != null && loadProvider == null) {
            throw new NullPointerException("LoadProvider must not be null");
        }
    }

    GenericRequestBuilder(LoadProvider<ModelType, DataType, ResourceType, TranscodeType> loadProvider, Class<TranscodeType> cls, GenericRequestBuilder<ModelType, ?, ?, ?> genericRequestBuilder) {
        this(genericRequestBuilder.f19535, genericRequestBuilder.f19538, loadProvider, cls, genericRequestBuilder.f19537, genericRequestBuilder.f19534, genericRequestBuilder.f19512);
        this.f19514 = genericRequestBuilder.f19514;
        this.f19527 = genericRequestBuilder.f19527;
        this.f19524 = genericRequestBuilder.f19524;
        this.f19526 = genericRequestBuilder.f19526;
        this.f19517 = genericRequestBuilder.f19517;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Request m25212(Target<TranscodeType> target) {
        if (this.f19522 == null) {
            this.f19522 = Priority.NORMAL;
        }
        return m25215(target, (ThumbnailRequestCoordinator) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Priority m25213() {
        return this.f19522 == Priority.LOW ? Priority.NORMAL : this.f19522 == Priority.NORMAL ? Priority.HIGH : Priority.IMMEDIATE;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Request m25214(Target<TranscodeType> target, float f, Priority priority, RequestCoordinator requestCoordinator) {
        return GenericRequest.m26026(this.f19513, this.f19514, this.f19524, this.f19535, priority, target, f, this.f19520, this.f19528, this.f19521, this.f19518, this.f19532, this.f19533, this.f19515, requestCoordinator, this.f19537.m3948(), this.f19529, this.f19536, this.f19517, this.f19519, this.f19525, this.f19523, this.f19526);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Request m25215(Target<TranscodeType> target, ThumbnailRequestCoordinator thumbnailRequestCoordinator) {
        if (this.f19539 != null) {
            if (this.f19531) {
                throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
            }
            if (this.f19539.f19519.equals(NoAnimation.m26103())) {
                this.f19539.f19519 = this.f19519;
            }
            if (this.f19539.f19522 == null) {
                this.f19539.f19522 = m25213();
            }
            if (Util.m26202(this.f19525, this.f19523) && !Util.m26202(this.f19539.f19525, this.f19539.f19523)) {
                this.f19539.m25221(this.f19525, this.f19523);
            }
            ThumbnailRequestCoordinator thumbnailRequestCoordinator2 = new ThumbnailRequestCoordinator(thumbnailRequestCoordinator);
            Request r1 = m25214(target, this.f19540.floatValue(), this.f19522, thumbnailRequestCoordinator2);
            this.f19531 = true;
            Request r2 = this.f19539.m25215(target, thumbnailRequestCoordinator2);
            this.f19531 = false;
            thumbnailRequestCoordinator2.m26089(r1, r2);
            return thumbnailRequestCoordinator2;
        } else if (this.f19516 == null) {
            return m25214(target, this.f19540.floatValue(), this.f19522, thumbnailRequestCoordinator);
        } else {
            ThumbnailRequestCoordinator thumbnailRequestCoordinator3 = new ThumbnailRequestCoordinator(thumbnailRequestCoordinator);
            thumbnailRequestCoordinator3.m26089(m25214(target, this.f19540.floatValue(), this.f19522, thumbnailRequestCoordinator3), m25214(target, this.f19516.floatValue(), m25213(), thumbnailRequestCoordinator3));
            return thumbnailRequestCoordinator3;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m25216() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m25217() {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> clone() {
        try {
            GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> genericRequestBuilder = (GenericRequestBuilder) super.clone();
            genericRequestBuilder.f19513 = this.f19513 != null ? this.f19513.clone() : null;
            return genericRequestBuilder;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25219() {
        return m25231(NoAnimation.m26103());
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25220() {
        return m25229((Transformation<ResourceType>[]) new Transformation[]{UnitTransformation.m25719()});
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25221(int i, int i2) {
        if (!Util.m26202(i, i2)) {
            throw new IllegalArgumentException("Width and height must be Target#SIZE_ORIGINAL or > 0");
        }
        this.f19525 = i;
        this.f19523 = i2;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25222(Drawable drawable) {
        this.f19520 = drawable;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25223(Encoder<DataType> encoder) {
        if (this.f19513 != null) {
            this.f19513.m25997(encoder);
        }
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25224(Key key) {
        if (key == null) {
            throw new NullPointerException("Signature must not be null");
        }
        this.f19524 = key;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25225(ResourceDecoder<DataType, ResourceType> resourceDecoder) {
        if (this.f19513 != null) {
            this.f19513.m25998(resourceDecoder);
        }
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25226(DiskCacheStrategy diskCacheStrategy) {
        this.f19526 = diskCacheStrategy;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25227(ModelType modeltype) {
        this.f19514 = modeltype;
        this.f19527 = true;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25228(boolean z) {
        this.f19517 = !z;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25229(Transformation<ResourceType>... transformationArr) {
        this.f19530 = true;
        if (transformationArr.length == 1) {
            this.f19529 = transformationArr[0];
        } else {
            this.f19529 = new MultiTransformation(transformationArr);
        }
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public FutureTarget<TranscodeType> m25230(int i, int i2) {
        final RequestFutureTarget requestFutureTarget = new RequestFutureTarget(this.f19537.m3944(), i, i2);
        this.f19537.m3944().post(new Runnable() {
            public void run() {
                if (!requestFutureTarget.isCancelled()) {
                    GenericRequestBuilder.this.m25233(requestFutureTarget);
                }
            }
        });
        return requestFutureTarget;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType> m25231(GlideAnimationFactory<TranscodeType> glideAnimationFactory) {
        if (glideAnimationFactory == null) {
            throw new NullPointerException("Animation factory must not be null!");
        }
        this.f19519 = glideAnimationFactory;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Target<TranscodeType> m25232(ImageView imageView) {
        Util.m26201();
        if (imageView == null) {
            throw new IllegalArgumentException("You must pass in a non null View");
        }
        if (!this.f19530 && imageView.getScaleType() != null) {
            switch (AnonymousClass2.f19543[imageView.getScaleType().ordinal()]) {
                case 1:
                    m25217();
                    break;
                case 2:
                case 3:
                case 4:
                    m25216();
                    break;
            }
        }
        return m25233(this.f19537.m3954(imageView, this.f19536));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <Y extends Target<TranscodeType>> Y m25233(Y y) {
        Util.m26201();
        if (y == null) {
            throw new IllegalArgumentException("You must pass in a non null Target");
        } else if (!this.f19527) {
            throw new IllegalArgumentException("You must first set a model (try #load())");
        } else {
            Request r0 = y.m26142();
            if (r0 != null) {
                r0.m26051();
                this.f19534.m25975(r0);
                r0.m26052();
            }
            Request r1 = m25212(y);
            y.m26144(r1);
            this.f19512.m25953(y);
            this.f19534.m25979(r1);
            return y;
        }
    }
}
