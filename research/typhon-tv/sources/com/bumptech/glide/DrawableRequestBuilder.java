package com.bumptech.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapperTransformation;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.provider.LoadProvider;
import com.bumptech.glide.request.animation.DrawableCrossFadeFactory;
import com.bumptech.glide.request.target.Target;

public class DrawableRequestBuilder<ModelType> extends GenericRequestBuilder<ModelType, ImageVideoWrapper, GifBitmapWrapper, GlideDrawable> {
    DrawableRequestBuilder(Context context, Class<ModelType> cls, LoadProvider<ModelType, ImageVideoWrapper, GifBitmapWrapper, GlideDrawable> loadProvider, Glide glide, RequestTracker requestTracker, Lifecycle lifecycle) {
        super(context, cls, loadProvider, GlideDrawable.class, glide, requestTracker, lifecycle);
        m25196();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m25178() {
        m25185();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m25179() {
        m25198();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25180() {
        return (DrawableRequestBuilder) super.clone();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25182() {
        super.m25220();
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25185() {
        return m25194(this.f19537.m3943());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25181() {
        super.m25219();
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final DrawableRequestBuilder<ModelType> m25196() {
        super.m25231(new DrawableCrossFadeFactory());
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25194(Transformation<GifBitmapWrapper>... transformationArr) {
        super.m25229((Transformation<ResourceType>[]) transformationArr);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25198() {
        return m25194(this.f19537.m3947());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25186(int i, int i2) {
        super.m25221(i, i2);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25187(Drawable drawable) {
        super.m25222(drawable);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25188(Encoder<ImageVideoWrapper> encoder) {
        super.m25223(encoder);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25189(Key key) {
        super.m25224(key);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25190(ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> resourceDecoder) {
        super.m25225(resourceDecoder);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25191(DiskCacheStrategy diskCacheStrategy) {
        super.m25226(diskCacheStrategy);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25192(ModelType modeltype) {
        super.m25227(modeltype);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25193(boolean z) {
        super.m25228(z);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25207(Transformation<Bitmap>... transformationArr) {
        GifBitmapWrapperTransformation[] gifBitmapWrapperTransformationArr = new GifBitmapWrapperTransformation[transformationArr.length];
        for (int i = 0; i < transformationArr.length; i++) {
            gifBitmapWrapperTransformationArr[i] = new GifBitmapWrapperTransformation(this.f19537.m3952(), transformationArr[i]);
        }
        return m25194(gifBitmapWrapperTransformationArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableRequestBuilder<ModelType> m25208(BitmapTransformation... bitmapTransformationArr) {
        return m25207((Transformation<Bitmap>[]) bitmapTransformationArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Target<GlideDrawable> m25209(ImageView imageView) {
        return super.m25232(imageView);
    }
}
