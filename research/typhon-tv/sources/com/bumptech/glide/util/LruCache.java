package com.bumptech.glide.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<T, Y> {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f20203;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f20204 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f20205;

    /* renamed from: 龘  reason: contains not printable characters */
    private final LinkedHashMap<T, Y> f20206 = new LinkedHashMap<>(100, 0.75f, true);

    public LruCache(int i) {
        this.f20205 = i;
        this.f20203 = i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26178() {
        m26182(this.f20203);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m26179() {
        return this.f20204;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Y m26180(T t) {
        return this.f20206.get(t);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Y m26181(T t, Y y) {
        if (m26184(y) >= this.f20203) {
            m26186(t, y);
            return null;
        }
        Y put = this.f20206.put(t, y);
        if (y != null) {
            this.f20204 += m26184(y);
        }
        if (put != null) {
            this.f20204 -= m26184(put);
        }
        m26178();
        return put;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26182(int i) {
        while (this.f20204 > i) {
            Map.Entry next = this.f20206.entrySet().iterator().next();
            Object value = next.getValue();
            this.f20204 -= m26184(value);
            Object key = next.getKey();
            this.f20206.remove(key);
            m26186(key, value);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Y m26183(T t) {
        Y remove = this.f20206.remove(t);
        if (remove != null) {
            this.f20204 -= m26184(remove);
        }
        return remove;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m26184(Y y) {
        return 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26185() {
        m26182(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26186(T t, Y y) {
    }
}
