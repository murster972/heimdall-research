package com.bumptech.glide.util;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;

public final class LogTime {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final double f20202;

    static {
        double d = 1.0d;
        if (17 <= Build.VERSION.SDK_INT) {
            d = 1.0d / Math.pow(10.0d, 6.0d);
        }
        f20202 = d;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static double m26176(long j) {
        return ((double) (m26177() - j)) * f20202;
    }

    @TargetApi(17)
    /* renamed from: 龘  reason: contains not printable characters */
    public static long m26177() {
        return 17 <= Build.VERSION.SDK_INT ? SystemClock.elapsedRealtimeNanos() : System.currentTimeMillis();
    }
}
