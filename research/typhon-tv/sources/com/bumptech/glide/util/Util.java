package com.bumptech.glide.util;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;

public final class Util {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final char[] f20210 = new char[64];

    /* renamed from: 齉  reason: contains not printable characters */
    private static final char[] f20211 = new char[40];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final char[] f20212 = "0123456789abcdef".toCharArray();

    /* renamed from: com.bumptech.glide.util.Util$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f20213 = new int[Bitmap.Config.values().length];

        static {
            try {
                f20213[Bitmap.Config.ALPHA_8.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f20213[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f20213[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f20213[Bitmap.Config.ARGB_8888.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m26190() {
        if (!m26192()) {
            throw new IllegalArgumentException("YOu must call this method on a background thread");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m26191(int i) {
        return i > 0 || i == Integer.MIN_VALUE;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m26192() {
        return !m26193();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m26193() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m26194(int i, int i2, Bitmap.Config config) {
        return i * i2 * m26195(config);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m26195(Bitmap.Config config) {
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        switch (AnonymousClass1.f20213[config.ordinal()]) {
            case 1:
                return 1;
            case 2:
            case 3:
                return 2;
            default:
                return 4;
        }
    }

    @TargetApi(19)
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m26196(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 19) {
            try {
                return bitmap.getAllocationByteCount();
            } catch (NullPointerException e) {
            }
        }
        return bitmap.getHeight() * bitmap.getRowBytes();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m26197(byte[] bArr) {
        String r0;
        synchronized (f20210) {
            r0 = m26198(bArr, f20210);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m26198(byte[] bArr, char[] cArr) {
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & 255;
            cArr[i * 2] = f20212[b >>> 4];
            cArr[(i * 2) + 1] = f20212[b & 15];
        }
        return new String(cArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> List<T> m26199(Collection<T> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (T add : collection) {
            arrayList.add(add);
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Queue<T> m26200(int i) {
        return new ArrayDeque(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m26201() {
        if (!m26193()) {
            throw new IllegalArgumentException("You must call this method on the main thread");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m26202(int i, int i2) {
        return m26191(i) && m26191(i2);
    }
}
