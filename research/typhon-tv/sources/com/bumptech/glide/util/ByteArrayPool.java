package com.bumptech.glide.util;

import android.util.Log;
import java.util.Queue;

public final class ByteArrayPool {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ByteArrayPool f20195 = new ByteArrayPool();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Queue<byte[]> f20196 = Util.m26200(0);

    private ByteArrayPool() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ByteArrayPool m26167() {
        return f20195;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public byte[] m26168() {
        byte[] poll;
        synchronized (this.f20196) {
            poll = this.f20196.poll();
        }
        if (poll == null) {
            poll = new byte[65536];
            if (Log.isLoggable("ByteArrayPool", 3)) {
                Log.d("ByteArrayPool", "Created temp bytes");
            }
        }
        return poll;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26169(byte[] bArr) {
        if (bArr.length != 65536) {
            return false;
        }
        boolean z = false;
        synchronized (this.f20196) {
            if (this.f20196.size() < 32) {
                z = true;
                this.f20196.offer(bArr);
            }
        }
        return z;
    }
}
