package com.bumptech.glide.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MarkEnforcingInputStream extends FilterInputStream {

    /* renamed from: 龘  reason: contains not printable characters */
    private int f20207 = Integer.MIN_VALUE;

    public MarkEnforcingInputStream(InputStream inputStream) {
        super(inputStream);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26187(long j) {
        if (this.f20207 != Integer.MIN_VALUE && j != -1) {
            this.f20207 = (int) (((long) this.f20207) - j);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m26188(long j) {
        if (this.f20207 == 0) {
            return -1;
        }
        return (this.f20207 == Integer.MIN_VALUE || j <= ((long) this.f20207)) ? j : (long) this.f20207;
    }

    public int available() throws IOException {
        return this.f20207 == Integer.MIN_VALUE ? super.available() : Math.min(this.f20207, super.available());
    }

    public void mark(int i) {
        super.mark(i);
        this.f20207 = i;
    }

    public int read() throws IOException {
        if (m26188(1) == -1) {
            return -1;
        }
        int read = super.read();
        m26187(1);
        return read;
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int r1 = (int) m26188((long) i2);
        if (r1 == -1) {
            return -1;
        }
        int read = super.read(bArr, i, r1);
        m26187((long) read);
        return read;
    }

    public void reset() throws IOException {
        super.reset();
        this.f20207 = Integer.MIN_VALUE;
    }

    public long skip(long j) throws IOException {
        long r2 = m26188(j);
        if (r2 == -1) {
            return -1;
        }
        long skip = super.skip(r2);
        m26187(skip);
        return skip;
    }
}
