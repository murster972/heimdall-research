package com.bumptech.glide.util;

public class MultiClassKey {

    /* renamed from: 靐  reason: contains not printable characters */
    private Class<?> f20208;

    /* renamed from: 龘  reason: contains not printable characters */
    private Class<?> f20209;

    public MultiClassKey() {
    }

    public MultiClassKey(Class<?> cls, Class<?> cls2) {
        m26189(cls, cls2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MultiClassKey multiClassKey = (MultiClassKey) obj;
        if (!this.f20209.equals(multiClassKey.f20209)) {
            return false;
        }
        return this.f20208.equals(multiClassKey.f20208);
    }

    public int hashCode() {
        return (this.f20209.hashCode() * 31) + this.f20208.hashCode();
    }

    public String toString() {
        return "MultiClassKey{first=" + this.f20209 + ", second=" + this.f20208 + '}';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26189(Class<?> cls, Class<?> cls2) {
        this.f20209 = cls;
        this.f20208 = cls2;
    }
}
