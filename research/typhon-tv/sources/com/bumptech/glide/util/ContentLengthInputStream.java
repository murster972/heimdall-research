package com.bumptech.glide.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ContentLengthInputStream extends FilterInputStream {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f20197;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f20198;

    ContentLengthInputStream(InputStream inputStream, long j) {
        super(inputStream);
        this.f20198 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m26170(int i) throws IOException {
        if (i >= 0) {
            this.f20197 += i;
        } else if (this.f20198 - ((long) this.f20197) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.f20198 + ", but read: " + this.f20197);
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static InputStream m26171(InputStream inputStream, long j) {
        return new ContentLengthInputStream(inputStream, j);
    }

    public synchronized int available() throws IOException {
        return (int) Math.max(this.f20198 - ((long) this.f20197), (long) this.in.available());
    }

    public synchronized int read() throws IOException {
        return m26170(super.read());
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        return m26170(super.read(bArr, i, i2));
    }
}
