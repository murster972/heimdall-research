package com.bumptech.glide.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class ExceptionCatchingInputStream extends InputStream {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Queue<ExceptionCatchingInputStream> f20199 = Util.m26200(0);

    /* renamed from: 靐  reason: contains not printable characters */
    private InputStream f20200;

    /* renamed from: 齉  reason: contains not printable characters */
    private IOException f20201;

    ExceptionCatchingInputStream() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ExceptionCatchingInputStream m26172(InputStream inputStream) {
        ExceptionCatchingInputStream poll;
        synchronized (f20199) {
            poll = f20199.poll();
        }
        if (poll == null) {
            poll = new ExceptionCatchingInputStream();
        }
        poll.m26174(inputStream);
        return poll;
    }

    public int available() throws IOException {
        return this.f20200.available();
    }

    public void close() throws IOException {
        this.f20200.close();
    }

    public void mark(int i) {
        this.f20200.mark(i);
    }

    public boolean markSupported() {
        return this.f20200.markSupported();
    }

    public int read() throws IOException {
        try {
            return this.f20200.read();
        } catch (IOException e) {
            this.f20201 = e;
            return -1;
        }
    }

    public int read(byte[] bArr) throws IOException {
        try {
            return this.f20200.read(bArr);
        } catch (IOException e) {
            this.f20201 = e;
            return -1;
        }
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        try {
            return this.f20200.read(bArr, i, i2);
        } catch (IOException e) {
            this.f20201 = e;
            return -1;
        }
    }

    public synchronized void reset() throws IOException {
        this.f20200.reset();
    }

    public long skip(long j) throws IOException {
        try {
            return this.f20200.skip(j);
        } catch (IOException e) {
            this.f20201 = e;
            return 0;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26173() {
        this.f20201 = null;
        this.f20200 = null;
        synchronized (f20199) {
            f20199.offer(this);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26174(InputStream inputStream) {
        this.f20200 = inputStream;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public IOException m26175() {
        return this.f20201;
    }
}
