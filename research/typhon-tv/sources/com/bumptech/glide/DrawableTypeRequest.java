package com.bumptech.glide;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.model.ImageVideoModelLoader;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.provider.FixedLoadProvider;
import java.io.InputStream;

public class DrawableTypeRequest<ModelType> extends DrawableRequestBuilder<ModelType> {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ModelLoader<ModelType, InputStream> f19509;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ModelLoader<ModelType, ParcelFileDescriptor> f19510;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final RequestManager.OptionsApplier f19511;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    DrawableTypeRequest(java.lang.Class<ModelType> r8, com.bumptech.glide.load.model.ModelLoader<ModelType, java.io.InputStream> r9, com.bumptech.glide.load.model.ModelLoader<ModelType, android.os.ParcelFileDescriptor> r10, android.content.Context r11, com.bumptech.glide.Glide r12, com.bumptech.glide.manager.RequestTracker r13, com.bumptech.glide.manager.Lifecycle r14, com.bumptech.glide.RequestManager.OptionsApplier r15) {
        /*
            r7 = this;
            java.lang.Class<com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper> r3 = com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper.class
            java.lang.Class<com.bumptech.glide.load.resource.drawable.GlideDrawable> r4 = com.bumptech.glide.load.resource.drawable.GlideDrawable.class
            r5 = 0
            r0 = r12
            r1 = r9
            r2 = r10
            com.bumptech.glide.provider.FixedLoadProvider r3 = m25210(r0, r1, r2, r3, r4, r5)
            r0 = r7
            r1 = r11
            r2 = r8
            r4 = r12
            r5 = r13
            r6 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r7.f19509 = r9
            r7.f19510 = r10
            r7.f19511 = r15
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.DrawableTypeRequest.<init>(java.lang.Class, com.bumptech.glide.load.model.ModelLoader, com.bumptech.glide.load.model.ModelLoader, android.content.Context, com.bumptech.glide.Glide, com.bumptech.glide.manager.RequestTracker, com.bumptech.glide.manager.Lifecycle, com.bumptech.glide.RequestManager$OptionsApplier):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <A, Z, R> FixedLoadProvider<A, ImageVideoWrapper, Z, R> m25210(Glide glide, ModelLoader<A, InputStream> modelLoader, ModelLoader<A, ParcelFileDescriptor> modelLoader2, Class<Z> cls, Class<R> cls2, ResourceTranscoder<Z, R> resourceTranscoder) {
        if (modelLoader == null && modelLoader2 == null) {
            return null;
        }
        if (resourceTranscoder == null) {
            resourceTranscoder = glide.m3953(cls, cls2);
        }
        return new FixedLoadProvider<>(new ImageVideoModelLoader(modelLoader, modelLoader2), resourceTranscoder, glide.m3949(ImageVideoWrapper.class, cls));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public BitmapTypeRequest<ModelType> m25211() {
        return (BitmapTypeRequest) this.f19511.m25245(new BitmapTypeRequest(this, this.f19509, this.f19510, this.f19511));
    }
}
