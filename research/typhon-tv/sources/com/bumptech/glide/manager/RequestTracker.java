package com.bumptech.glide.manager;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.util.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public class RequestTracker {

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<Request> f20085 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f20086;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Set<Request> f20087 = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25974() {
        this.f20086 = false;
        for (T t : Util.m26199(this.f20087)) {
            if (!t.m26046() && !t.m26048() && !t.m26045()) {
                t.m26050();
            }
        }
        this.f20085.clear();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25975(Request request) {
        this.f20087.remove(request);
        this.f20085.remove(request);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25976() {
        for (T t : Util.m26199(this.f20087)) {
            if (!t.m26046() && !t.m26048()) {
                t.m26049();
                if (!this.f20086) {
                    t.m26050();
                } else {
                    this.f20085.add(t);
                }
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25977() {
        for (T r1 : Util.m26199(this.f20087)) {
            r1.m26051();
        }
        this.f20085.clear();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25978() {
        this.f20086 = true;
        for (T t : Util.m26199(this.f20087)) {
            if (t.m26045()) {
                t.m26049();
                this.f20085.add(t);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25979(Request request) {
        this.f20087.add(request);
        if (!this.f20086) {
            request.m26050();
        } else {
            this.f20085.add(request);
        }
    }
}
