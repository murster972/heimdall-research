package com.bumptech.glide.manager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.util.Util;
import java.util.HashMap;
import java.util.Map;

public class RequestManagerRetriever implements Handler.Callback {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final RequestManagerRetriever f20080 = new RequestManagerRetriever();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Handler f20081 = new Handler(Looper.getMainLooper(), this);

    /* renamed from: 靐  reason: contains not printable characters */
    final Map<FragmentManager, SupportRequestManagerFragment> f20082 = new HashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private volatile RequestManager f20083;

    /* renamed from: 龘  reason: contains not printable characters */
    final Map<android.app.FragmentManager, RequestManagerFragment> f20084 = new HashMap();

    RequestManagerRetriever() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private RequestManager m25963(Context context) {
        if (this.f20083 == null) {
            synchronized (this) {
                if (this.f20083 == null) {
                    this.f20083 = new RequestManager(context.getApplicationContext(), new ApplicationLifecycle(), new EmptyRequestManagerTreeNode());
                }
            }
        }
        return this.f20083;
    }

    @TargetApi(17)
    /* renamed from: 靐  reason: contains not printable characters */
    private static void m25964(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RequestManagerRetriever m25965() {
        return f20080;
    }

    public boolean handleMessage(Message message) {
        boolean z = true;
        Object obj = null;
        android.app.FragmentManager fragmentManager = null;
        switch (message.what) {
            case 1:
                android.app.FragmentManager fragmentManager2 = (android.app.FragmentManager) message.obj;
                fragmentManager = fragmentManager2;
                obj = this.f20084.remove(fragmentManager2);
                break;
            case 2:
                FragmentManager fragmentManager3 = (FragmentManager) message.obj;
                fragmentManager = fragmentManager3;
                obj = this.f20082.remove(fragmentManager3);
                break;
            default:
                z = false;
                break;
        }
        if (z && obj == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + fragmentManager);
        }
        return z;
    }

    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManager m25966(Activity activity) {
        if (Util.m26192() || Build.VERSION.SDK_INT < 11) {
            return m25967(activity.getApplicationContext());
        }
        m25964(activity);
        return m25968((Context) activity, activity.getFragmentManager());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManager m25967(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("You cannot start a load on a null Context");
        }
        if (Util.m26193() && !(context instanceof Application)) {
            if (context instanceof FragmentActivity) {
                return m25971((FragmentActivity) context);
            }
            if (context instanceof Activity) {
                return m25966((Activity) context);
            }
            if (context instanceof ContextWrapper) {
                return m25967(((ContextWrapper) context).getBaseContext());
            }
        }
        return m25963(context);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManager m25968(Context context, android.app.FragmentManager fragmentManager) {
        RequestManagerFragment r0 = m25972(fragmentManager);
        RequestManager r1 = r0.m25959();
        if (r1 != null) {
            return r1;
        }
        RequestManager requestManager = new RequestManager(context, r0.m25961(), r0.m25960());
        r0.m25962(requestManager);
        return requestManager;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManager m25969(Context context, FragmentManager fragmentManager) {
        SupportRequestManagerFragment r0 = m25973(fragmentManager);
        RequestManager r1 = r0.m25982();
        if (r1 != null) {
            return r1;
        }
        RequestManager requestManager = new RequestManager(context, r0.m25984(), r0.m25983());
        r0.m25985(requestManager);
        return requestManager;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManager m25970(Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (Util.m26192()) {
            return m25967(fragment.getActivity().getApplicationContext());
        } else {
            return m25969((Context) fragment.getActivity(), fragment.getChildFragmentManager());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManager m25971(FragmentActivity fragmentActivity) {
        if (Util.m26192()) {
            return m25967(fragmentActivity.getApplicationContext());
        }
        m25964((Activity) fragmentActivity);
        return m25969((Context) fragmentActivity, fragmentActivity.getSupportFragmentManager());
    }

    /* access modifiers changed from: package-private */
    @TargetApi(17)
    /* renamed from: 龘  reason: contains not printable characters */
    public RequestManagerFragment m25972(android.app.FragmentManager fragmentManager) {
        RequestManagerFragment requestManagerFragment = (RequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (requestManagerFragment != null) {
            return requestManagerFragment;
        }
        RequestManagerFragment requestManagerFragment2 = this.f20084.get(fragmentManager);
        if (requestManagerFragment2 != null) {
            return requestManagerFragment2;
        }
        RequestManagerFragment requestManagerFragment3 = new RequestManagerFragment();
        this.f20084.put(fragmentManager, requestManagerFragment3);
        fragmentManager.beginTransaction().add(requestManagerFragment3, "com.bumptech.glide.manager").commitAllowingStateLoss();
        this.f20081.obtainMessage(1, fragmentManager).sendToTarget();
        return requestManagerFragment3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public SupportRequestManagerFragment m25973(FragmentManager fragmentManager) {
        SupportRequestManagerFragment supportRequestManagerFragment = (SupportRequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (supportRequestManagerFragment != null) {
            return supportRequestManagerFragment;
        }
        SupportRequestManagerFragment supportRequestManagerFragment2 = this.f20082.get(fragmentManager);
        if (supportRequestManagerFragment2 != null) {
            return supportRequestManagerFragment2;
        }
        SupportRequestManagerFragment supportRequestManagerFragment3 = new SupportRequestManagerFragment();
        this.f20082.put(fragmentManager, supportRequestManagerFragment3);
        fragmentManager.beginTransaction().add((Fragment) supportRequestManagerFragment3, "com.bumptech.glide.manager").commitAllowingStateLoss();
        this.f20081.obtainMessage(2, fragmentManager).sendToTarget();
        return supportRequestManagerFragment3;
    }
}
