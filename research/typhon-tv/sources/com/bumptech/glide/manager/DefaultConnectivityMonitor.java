package com.bumptech.glide.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.bumptech.glide.manager.ConnectivityMonitor;

class DefaultConnectivityMonitor implements ConnectivityMonitor {

    /* renamed from: 连任  reason: contains not printable characters */
    private final BroadcastReceiver f20068 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean r0 = DefaultConnectivityMonitor.this.f20071;
            boolean unused = DefaultConnectivityMonitor.this.f20071 = DefaultConnectivityMonitor.this.m25946(context);
            if (r0 != DefaultConnectivityMonitor.this.f20071) {
                DefaultConnectivityMonitor.this.f20069.m25941(DefaultConnectivityMonitor.this.f20071);
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectivityMonitor.ConnectivityListener f20069;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f20070;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f20071;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f20072;

    public DefaultConnectivityMonitor(Context context, ConnectivityMonitor.ConnectivityListener connectivityListener) {
        this.f20072 = context.getApplicationContext();
        this.f20069 = connectivityListener;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25944() {
        if (this.f20070) {
            this.f20072.unregisterReceiver(this.f20068);
            this.f20070 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25945() {
        if (!this.f20070) {
            this.f20071 = m25946(this.f20072);
            this.f20072.registerReceiver(this.f20068, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.f20070 = true;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25946(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m25950() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m25951() {
        m25944();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25952() {
        m25945();
    }
}
