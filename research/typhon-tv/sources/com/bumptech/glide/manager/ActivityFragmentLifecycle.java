package com.bumptech.glide.manager;

import com.bumptech.glide.util.Util;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

class ActivityFragmentLifecycle implements Lifecycle {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f20065;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f20066;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Set<LifecycleListener> f20067 = Collections.newSetFromMap(new WeakHashMap());

    ActivityFragmentLifecycle() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25936() {
        this.f20065 = false;
        for (T r1 : Util.m26199(this.f20067)) {
            r1.m3984();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m25937() {
        this.f20066 = true;
        for (T r1 : Util.m26199(this.f20067)) {
            r1.m3983();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25938() {
        this.f20065 = true;
        for (T r1 : Util.m26199(this.f20067)) {
            r1.m3985();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25939(LifecycleListener lifecycleListener) {
        this.f20067.add(lifecycleListener);
        if (this.f20066) {
            lifecycleListener.m3983();
        } else if (this.f20065) {
            lifecycleListener.m3985();
        } else {
            lifecycleListener.m3984();
        }
    }
}
