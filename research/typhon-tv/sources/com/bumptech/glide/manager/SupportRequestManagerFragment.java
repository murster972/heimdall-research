package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.Fragment;
import com.bumptech.glide.RequestManager;
import java.util.HashSet;

public class SupportRequestManagerFragment extends Fragment {

    /* renamed from: 连任  reason: contains not printable characters */
    private SupportRequestManagerFragment f20088;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ActivityFragmentLifecycle f20089;

    /* renamed from: 麤  reason: contains not printable characters */
    private final HashSet<SupportRequestManagerFragment> f20090;

    /* renamed from: 齉  reason: contains not printable characters */
    private final RequestManagerTreeNode f20091;

    /* renamed from: 龘  reason: contains not printable characters */
    private RequestManager f20092;

    private class SupportFragmentRequestManagerTreeNode implements RequestManagerTreeNode {
        private SupportFragmentRequestManagerTreeNode() {
        }
    }

    public SupportRequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    @SuppressLint({"ValidFragment"})
    public SupportRequestManagerFragment(ActivityFragmentLifecycle activityFragmentLifecycle) {
        this.f20091 = new SupportFragmentRequestManagerTreeNode();
        this.f20090 = new HashSet<>();
        this.f20089 = activityFragmentLifecycle;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25980(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.f20090.remove(supportRequestManagerFragment);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25981(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.f20090.add(supportRequestManagerFragment);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f20088 = RequestManagerRetriever.m25965().m25973(getActivity().getSupportFragmentManager());
        if (this.f20088 != this) {
            this.f20088.m25981(this);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f20089.m25937();
    }

    public void onDetach() {
        super.onDetach();
        if (this.f20088 != null) {
            this.f20088.m25980(this);
            this.f20088 = null;
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        if (this.f20092 != null) {
            this.f20092.m3976();
        }
    }

    public void onStart() {
        super.onStart();
        this.f20089.m25938();
    }

    public void onStop() {
        super.onStop();
        this.f20089.m25936();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RequestManager m25982() {
        return this.f20092;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public RequestManagerTreeNode m25983() {
        return this.f20091;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ActivityFragmentLifecycle m25984() {
        return this.f20089;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25985(RequestManager requestManager) {
        this.f20092 = requestManager;
    }
}
