package com.bumptech.glide.manager;

import android.content.Context;
import com.bumptech.glide.manager.ConnectivityMonitor;

public class ConnectivityMonitorFactory {
    /* renamed from: 龘  reason: contains not printable characters */
    public ConnectivityMonitor m25942(Context context, ConnectivityMonitor.ConnectivityListener connectivityListener) {
        return context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0 ? new DefaultConnectivityMonitor(context, connectivityListener) : new NullConnectivityMonitor();
    }
}
