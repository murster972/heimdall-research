package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import com.bumptech.glide.RequestManager;
import java.util.HashSet;

@TargetApi(11)
public class RequestManagerFragment extends Fragment {

    /* renamed from: 连任  reason: contains not printable characters */
    private RequestManagerFragment f20074;

    /* renamed from: 靐  reason: contains not printable characters */
    private final RequestManagerTreeNode f20075;

    /* renamed from: 麤  reason: contains not printable characters */
    private final HashSet<RequestManagerFragment> f20076;

    /* renamed from: 齉  reason: contains not printable characters */
    private RequestManager f20077;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ActivityFragmentLifecycle f20078;

    private class FragmentRequestManagerTreeNode implements RequestManagerTreeNode {
        private FragmentRequestManagerTreeNode() {
        }
    }

    public RequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    @SuppressLint({"ValidFragment"})
    RequestManagerFragment(ActivityFragmentLifecycle activityFragmentLifecycle) {
        this.f20075 = new FragmentRequestManagerTreeNode();
        this.f20076 = new HashSet<>();
        this.f20078 = activityFragmentLifecycle;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25957(RequestManagerFragment requestManagerFragment) {
        this.f20076.remove(requestManagerFragment);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25958(RequestManagerFragment requestManagerFragment) {
        this.f20076.add(requestManagerFragment);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f20074 = RequestManagerRetriever.m25965().m25972(getActivity().getFragmentManager());
        if (this.f20074 != this) {
            this.f20074.m25958(this);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f20078.m25937();
    }

    public void onDetach() {
        super.onDetach();
        if (this.f20074 != null) {
            this.f20074.m25957(this);
            this.f20074 = null;
        }
    }

    public void onLowMemory() {
        if (this.f20077 != null) {
            this.f20077.m3976();
        }
    }

    public void onStart() {
        super.onStart();
        this.f20078.m25938();
    }

    public void onStop() {
        super.onStop();
        this.f20078.m25936();
    }

    public void onTrimMemory(int i) {
        if (this.f20077 != null) {
            this.f20077.m3977(i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RequestManager m25959() {
        return this.f20077;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public RequestManagerTreeNode m25960() {
        return this.f20075;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ActivityFragmentLifecycle m25961() {
        return this.f20078;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25962(RequestManager requestManager) {
        this.f20077 = requestManager;
    }
}
