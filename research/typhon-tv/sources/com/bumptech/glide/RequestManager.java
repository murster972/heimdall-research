package com.bumptech.glide;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.LifecycleListener;
import com.bumptech.glide.manager.RequestManagerTreeNode;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.signature.ApplicationVersionSignature;
import com.bumptech.glide.util.Util;
import java.io.InputStream;

public class RequestManager implements LifecycleListener {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final OptionsApplier f3351;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public DefaultOptions f3352;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final Glide f3353;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Lifecycle f3354;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final RequestTracker f3355;

    /* renamed from: 齉  reason: contains not printable characters */
    private final RequestManagerTreeNode f3356;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context f3357;

    public interface DefaultOptions {
        /* renamed from: 龘  reason: contains not printable characters */
        <T> void m25240(GenericRequestBuilder<T, ?, ?, ?> genericRequestBuilder);
    }

    public final class GenericModelRequest<A, T> {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final ModelLoader<A, T> f19560;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final Class<T> f19561;

        public final class GenericTypeRequest {

            /* renamed from: 靐  reason: contains not printable characters */
            private final A f19563;

            /* renamed from: 麤  reason: contains not printable characters */
            private final boolean f19564 = true;

            /* renamed from: 齉  reason: contains not printable characters */
            private final Class<A> f19565;

            GenericTypeRequest(A a) {
                this.f19563 = a;
                this.f19565 = RequestManager.m3960(a);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public <Z> GenericTranscodeRequest<A, T, Z> m25244(Class<Z> cls) {
                GenericTranscodeRequest<A, T, Z> genericTranscodeRequest = (GenericTranscodeRequest) RequestManager.this.f3351.m25245(new GenericTranscodeRequest(RequestManager.this.f3357, RequestManager.this.f3353, this.f19565, GenericModelRequest.this.f19560, GenericModelRequest.this.f19561, cls, RequestManager.this.f3355, RequestManager.this.f3354, RequestManager.this.f3351));
                if (this.f19564) {
                    genericTranscodeRequest.m25227(this.f19563);
                }
                return genericTranscodeRequest;
            }
        }

        GenericModelRequest(ModelLoader<A, T> modelLoader, Class<T> cls) {
            this.f19560 = modelLoader;
            this.f19561 = cls;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public GenericModelRequest<A, T>.GenericTypeRequest m25243(A a) {
            return new GenericTypeRequest(a);
        }
    }

    class OptionsApplier {
        OptionsApplier() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public <A, X extends GenericRequestBuilder<A, ?, ?, ?>> X m25245(X x) {
            if (RequestManager.this.f3352 != null) {
                RequestManager.this.f3352.m25240(x);
            }
            return x;
        }
    }

    private static class RequestManagerConnectivityListener implements ConnectivityMonitor.ConnectivityListener {

        /* renamed from: 龘  reason: contains not printable characters */
        private final RequestTracker f19568;

        public RequestManagerConnectivityListener(RequestTracker requestTracker) {
            this.f19568 = requestTracker;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25246(boolean z) {
            if (z) {
                this.f19568.m25976();
            }
        }
    }

    public RequestManager(Context context, Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode) {
        this(context, lifecycle, requestManagerTreeNode, new RequestTracker(), new ConnectivityMonitorFactory());
    }

    RequestManager(Context context, final Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode, RequestTracker requestTracker, ConnectivityMonitorFactory connectivityMonitorFactory) {
        this.f3357 = context.getApplicationContext();
        this.f3354 = lifecycle;
        this.f3356 = requestManagerTreeNode;
        this.f3355 = requestTracker;
        this.f3353 = Glide.m3936(context);
        this.f3351 = new OptionsApplier();
        ConnectivityMonitor r0 = connectivityMonitorFactory.m25942(context, new RequestManagerConnectivityListener(requestTracker));
        if (Util.m26192()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    lifecycle.m25953(RequestManager.this);
                }
            });
        } else {
            lifecycle.m25953(this);
        }
        lifecycle.m25953(r0);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Class<T> m3960(T t) {
        if (t != null) {
            return t.getClass();
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T> DrawableTypeRequest<T> m3964(Class<T> cls) {
        ModelLoader<T, InputStream> r2 = Glide.m3939(cls, this.f3357);
        ModelLoader<T, ParcelFileDescriptor> r3 = Glide.m3935(cls, this.f3357);
        if (cls != null && r2 == null && r3 == null) {
            throw new IllegalArgumentException("Unknown type " + cls + ". You must provide a Model of a type for" + " which there is a registered ModelLoader, if you are using a custom model, you must first call" + " Glide#register with a ModelLoaderFactory for your custom model class");
        }
        return (DrawableTypeRequest) this.f3351.m25245(new DrawableTypeRequest(cls, r2, r3, this.f3357, this.f3353, this.f3355, this.f3354, this.f3351));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3966() {
        this.f3355.m25977();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DrawableTypeRequest<String> m3967() {
        return m3964(String.class);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public DrawableTypeRequest<Integer> m3968() {
        return (DrawableTypeRequest) m3964(Integer.class).m25189(ApplicationVersionSignature.m26163(this.f3357));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m3969() {
        m3970();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m3970() {
        Util.m26201();
        this.f3355.m25978();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m3971() {
        m3972();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m3972() {
        Util.m26201();
        this.f3355.m25974();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableTypeRequest<Integer> m3973(Integer num) {
        return (DrawableTypeRequest) m3968().m25192(num);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DrawableTypeRequest<String> m3974(String str) {
        return (DrawableTypeRequest) m3967().m25192(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <A, T> GenericModelRequest<A, T> m3975(ModelLoader<A, T> modelLoader, Class<T> cls) {
        return new GenericModelRequest<>(modelLoader, cls);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3976() {
        this.f3353.m3946();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3977(int i) {
        this.f3353.m3955(i);
    }
}
