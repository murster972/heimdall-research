package com.bumptech.glide.signature;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.bumptech.glide.load.Key;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public final class ApplicationVersionSignature {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ConcurrentHashMap<String, Key> f20192 = new ConcurrentHashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private static Key m26162(Context context) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return new StringSignature(packageInfo != null ? String.valueOf(packageInfo.versionCode) : UUID.randomUUID().toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Key m26163(Context context) {
        String packageName = context.getPackageName();
        Key key = f20192.get(packageName);
        if (key != null) {
            return key;
        }
        Key r2 = m26162(context);
        Key putIfAbsent = f20192.putIfAbsent(packageName, r2);
        return putIfAbsent == null ? r2 : putIfAbsent;
    }
}
