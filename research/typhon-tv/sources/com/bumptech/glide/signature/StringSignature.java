package com.bumptech.glide.signature;

import com.bumptech.glide.load.Key;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class StringSignature implements Key {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f20194;

    public StringSignature(String str) {
        if (str == null) {
            throw new NullPointerException("Signature cannot be null!");
        }
        this.f20194 = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f20194.equals(((StringSignature) obj).f20194);
    }

    public int hashCode() {
        return this.f20194.hashCode();
    }

    public String toString() {
        return "StringSignature{signature='" + this.f20194 + '\'' + '}';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26166(MessageDigest messageDigest) throws UnsupportedEncodingException {
        messageDigest.update(this.f20194.getBytes("UTF-8"));
    }
}
