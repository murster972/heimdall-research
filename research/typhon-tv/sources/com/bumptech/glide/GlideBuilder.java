package com.bumptech.glide;

import android.content.Context;
import android.os.Build;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPoolAdapter;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.load.engine.executor.FifoPriorityThreadPoolExecutor;
import java.util.concurrent.ExecutorService;

public class GlideBuilder {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ExecutorService f19548;

    /* renamed from: ʼ  reason: contains not printable characters */
    private DecodeFormat f19549;

    /* renamed from: ʽ  reason: contains not printable characters */
    private DiskCache.Factory f19550;

    /* renamed from: 连任  reason: contains not printable characters */
    private ExecutorService f19551;

    /* renamed from: 靐  reason: contains not printable characters */
    private Engine f19552;

    /* renamed from: 麤  reason: contains not printable characters */
    private MemoryCache f19553;

    /* renamed from: 齉  reason: contains not printable characters */
    private BitmapPool f19554;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f19555;

    public GlideBuilder(Context context) {
        this.f19555 = context.getApplicationContext();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Glide m25239() {
        if (this.f19551 == null) {
            this.f19551 = new FifoPriorityThreadPoolExecutor(Math.max(1, Runtime.getRuntime().availableProcessors()));
        }
        if (this.f19548 == null) {
            this.f19548 = new FifoPriorityThreadPoolExecutor(1);
        }
        MemorySizeCalculator memorySizeCalculator = new MemorySizeCalculator(this.f19555);
        if (this.f19554 == null) {
            if (Build.VERSION.SDK_INT >= 11) {
                this.f19554 = new LruBitmapPool(memorySizeCalculator.m25610());
            } else {
                this.f19554 = new BitmapPoolAdapter();
            }
        }
        if (this.f19553 == null) {
            this.f19553 = new LruResourceCache(memorySizeCalculator.m25611());
        }
        if (this.f19550 == null) {
            this.f19550 = new InternalCacheDiskCacheFactory(this.f19555);
        }
        if (this.f19552 == null) {
            this.f19552 = new Engine(this.f19553, this.f19550, this.f19548, this.f19551);
        }
        if (this.f19549 == null) {
            this.f19549 = DecodeFormat.DEFAULT;
        }
        return new Glide(this.f19552, this.f19553, this.f19554, this.f19555, this.f19549);
    }
}
