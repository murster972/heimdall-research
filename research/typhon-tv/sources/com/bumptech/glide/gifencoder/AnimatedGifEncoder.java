package com.bumptech.glide.gifencoder;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;

public class AnimatedGifEncoder {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f19654 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f19655 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private OutputStream f19656;

    /* renamed from: ʾ  reason: contains not printable characters */
    private byte[] f19657;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean[] f19658 = new boolean[256];

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f19659 = 10;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f19660;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f19661;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f19662 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f19663 = true;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f19664 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Bitmap f19665;

    /* renamed from: ٴ  reason: contains not printable characters */
    private byte[] f19666;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private byte[] f19667;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f19668 = -1;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f19669;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f19670;

    /* renamed from: 齉  reason: contains not printable characters */
    private Integer f19671 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f19672;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f19673 = 7;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f19674 = -1;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m25335() throws IOException {
        m25344(this.f19672);
        m25344(this.f19669);
        this.f19656.write(this.f19673 | 240);
        this.f19656.write(0);
        this.f19656.write(0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m25336() throws IOException {
        this.f19656.write(33);
        this.f19656.write(255);
        this.f19656.write(11);
        m25345("NETSCAPE2.0");
        this.f19656.write(3);
        this.f19656.write(1);
        m25344(this.f19668);
        this.f19656.write(0);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m25337() throws IOException {
        this.f19656.write(this.f19657, 0, this.f19657.length);
        int length = 768 - this.f19657.length;
        for (int i = 0; i < length; i++) {
            this.f19656.write(0);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m25338() throws IOException {
        new LZWEncoder(this.f19672, this.f19669, this.f19667, this.f19660).m25354(this.f19656);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m25339() throws IOException {
        this.f19656.write(44);
        m25344(0);
        m25344(0);
        m25344(this.f19672);
        m25344(this.f19669);
        if (this.f19663) {
            this.f19656.write(0);
        } else {
            this.f19656.write(this.f19673 | 128);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m25340(int i) {
        if (this.f19657 == null) {
            return -1;
        }
        int red = Color.red(i);
        int green = Color.green(i);
        int blue = Color.blue(i);
        int i2 = 0;
        int i3 = 16777216;
        int length = this.f19657.length;
        int i4 = 0;
        while (i4 < length) {
            int i5 = i4 + 1;
            int i6 = red - (this.f19657[i4] & 255);
            int i7 = i5 + 1;
            int i8 = green - (this.f19657[i5] & 255);
            int i9 = blue - (this.f19657[i7] & 255);
            int i10 = (i6 * i6) + (i8 * i8) + (i9 * i9);
            int i11 = i7 / 3;
            if (this.f19658[i11] && i10 < i3) {
                i3 = i10;
                i2 = i11;
            }
            i4 = i7 + 1;
        }
        return i2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25341() {
        int length = this.f19666.length;
        int i = length / 3;
        this.f19667 = new byte[i];
        NeuQuant neuQuant = new NeuQuant(this.f19666, length, this.f19659);
        this.f19657 = neuQuant.m25364();
        for (int i2 = 0; i2 < this.f19657.length; i2 += 3) {
            byte b = this.f19657[i2];
            this.f19657[i2] = this.f19657[i2 + 2];
            this.f19657[i2 + 2] = b;
            this.f19658[i2 / 3] = false;
        }
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            int i5 = i4 + 1;
            int i6 = i5 + 1;
            int r1 = neuQuant.m25366(this.f19666[i4] & 255, this.f19666[i5] & 255, this.f19666[i6] & 255);
            this.f19658[r1] = true;
            this.f19667[i3] = (byte) r1;
            i3++;
            i4 = i6 + 1;
        }
        this.f19666 = null;
        this.f19660 = 8;
        this.f19673 = 7;
        if (this.f19671 != null) {
            this.f19670 = m25340(this.f19671.intValue());
        } else if (this.f19661) {
            this.f19670 = m25340(0);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m25342() throws IOException {
        int i;
        int i2;
        this.f19656.write(33);
        this.f19656.write(249);
        this.f19656.write(4);
        if (this.f19671 != null || this.f19661) {
            i = 1;
            i2 = 2;
        } else {
            i = 0;
            i2 = 0;
        }
        if (this.f19674 >= 0) {
            i2 = this.f19674 & 7;
        }
        this.f19656.write((i2 << 2) | 0 | 0 | i);
        m25344(this.f19654);
        this.f19656.write(this.f19670);
        this.f19656.write(0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25343() {
        int width = this.f19665.getWidth();
        int height = this.f19665.getHeight();
        if (!(width == this.f19672 && height == this.f19669)) {
            Bitmap createBitmap = Bitmap.createBitmap(this.f19672, this.f19669, Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(createBitmap, 0.0f, 0.0f, (Paint) null);
            this.f19665 = createBitmap;
        }
        int[] iArr = new int[(width * height)];
        this.f19665.getPixels(iArr, 0, width, 0, 0, width, height);
        this.f19666 = new byte[(iArr.length * 3)];
        this.f19661 = false;
        int i = 0;
        int[] iArr2 = iArr;
        int length = iArr2.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            int i4 = iArr2[i2];
            if (i4 == 0) {
                i++;
            }
            int i5 = i3 + 1;
            this.f19666[i3] = (byte) (i4 & 255);
            int i6 = i5 + 1;
            this.f19666[i5] = (byte) ((i4 >> 8) & 255);
            this.f19666[i6] = (byte) ((i4 >> 16) & 255);
            i2++;
            i3 = i6 + 1;
        }
        double length2 = ((double) (i * 100)) / ((double) iArr.length);
        this.f19661 = length2 > 4.0d;
        if (Log.isLoggable("AnimatedGifEncoder", 3)) {
            Log.d("AnimatedGifEncoder", "got pixels for frame with " + length2 + "% transparent pixels");
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25344(int i) throws IOException {
        this.f19656.write(i & 255);
        this.f19656.write((i >> 8) & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25345(String str) throws IOException {
        for (int i = 0; i < str.length(); i++) {
            this.f19656.write((byte) str.charAt(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25346(int i) {
        this.f19654 = Math.round(((float) i) / 10.0f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25347(int i, int i2) {
        if (!this.f19655 || this.f19663) {
            this.f19672 = i;
            this.f19669 = i2;
            if (this.f19672 < 1) {
                this.f19672 = 320;
            }
            if (this.f19669 < 1) {
                this.f19669 = 240;
            }
            this.f19664 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25348() {
        if (!this.f19655) {
            return false;
        }
        boolean z = true;
        this.f19655 = false;
        try {
            this.f19656.write(59);
            this.f19656.flush();
            if (this.f19662) {
                this.f19656.close();
            }
        } catch (IOException e) {
            z = false;
        }
        this.f19670 = 0;
        this.f19656 = null;
        this.f19665 = null;
        this.f19666 = null;
        this.f19667 = null;
        this.f19657 = null;
        this.f19662 = false;
        this.f19663 = true;
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25349(Bitmap bitmap) {
        if (bitmap == null || !this.f19655) {
            return false;
        }
        try {
            if (!this.f19664) {
                m25347(bitmap.getWidth(), bitmap.getHeight());
            }
            this.f19665 = bitmap;
            m25343();
            m25341();
            if (this.f19663) {
                m25335();
                m25337();
                if (this.f19668 >= 0) {
                    m25336();
                }
            }
            m25342();
            m25339();
            if (!this.f19663) {
                m25337();
            }
            m25338();
            this.f19663 = false;
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25350(OutputStream outputStream) {
        if (outputStream == null) {
            return false;
        }
        boolean z = true;
        this.f19662 = false;
        this.f19656 = outputStream;
        try {
            m25345("GIF89a");
        } catch (IOException e) {
            z = false;
        }
        this.f19655 = z;
        return z;
    }
}
