package com.bumptech.glide.gifencoder;

import android.support.v4.app.FrameMetricsAggregator;
import java.io.IOException;
import java.io.OutputStream;

class LZWEncoder {

    /* renamed from: ʻ  reason: contains not printable characters */
    int[] f19675 = new int[5003];

    /* renamed from: ʼ  reason: contains not printable characters */
    int f19676 = 5003;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f19677 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f19678 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f19679 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private byte[] f19680;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f19681;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f19682;

    /* renamed from: ˊ  reason: contains not printable characters */
    byte[] f19683 = new byte[256];

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f19684;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f19685;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f19686;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f19687 = false;

    /* renamed from: י  reason: contains not printable characters */
    private int f19688;

    /* renamed from: ٴ  reason: contains not printable characters */
    int f19689;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int f19690;

    /* renamed from: 连任  reason: contains not printable characters */
    int[] f19691 = new int[5003];

    /* renamed from: 靐  reason: contains not printable characters */
    int f19692 = 12;

    /* renamed from: 麤  reason: contains not printable characters */
    int f19693 = 4096;

    /* renamed from: 齉  reason: contains not printable characters */
    int f19694;

    /* renamed from: 龘  reason: contains not printable characters */
    int f19695;

    /* renamed from: ﹶ  reason: contains not printable characters */
    int[] f19696 = {0, 1, 3, 7, 15, 31, 63, 127, 255, FrameMetricsAggregator.EVERY_DURATION, 1023, 2047, 4095, 8191, 16383, 32767, 65535};

    /* renamed from: ﾞ  reason: contains not printable characters */
    int f19697;

    LZWEncoder(int i, int i2, byte[] bArr, int i3) {
        this.f19684 = i;
        this.f19685 = i2;
        this.f19680 = bArr;
        this.f19682 = Math.max(2, i3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m25351() {
        if (this.f19686 == 0) {
            return -1;
        }
        this.f19686--;
        byte[] bArr = this.f19680;
        int i = this.f19688;
        this.f19688 = i + 1;
        return bArr[i] & 255;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final int m25352(int i) {
        return (1 << i) - 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25353(int i, OutputStream outputStream) throws IOException {
        this.f19678 &= this.f19696[this.f19679];
        if (this.f19679 > 0) {
            this.f19678 |= i << this.f19679;
        } else {
            this.f19678 = i;
        }
        this.f19679 += this.f19695;
        while (this.f19679 >= 8) {
            m25356((byte) (this.f19678 & 255), outputStream);
            this.f19678 >>= 8;
            this.f19679 -= 8;
        }
        if (this.f19677 > this.f19694 || this.f19687) {
            if (this.f19687) {
                int i2 = this.f19689;
                this.f19695 = i2;
                this.f19694 = m25352(i2);
                this.f19687 = false;
            } else {
                this.f19695++;
                if (this.f19695 == this.f19692) {
                    this.f19694 = this.f19693;
                } else {
                    this.f19694 = m25352(this.f19695);
                }
            }
        }
        if (i == this.f19681) {
            while (this.f19679 > 0) {
                m25356((byte) (this.f19678 & 255), outputStream);
                this.f19678 >>= 8;
                this.f19679 -= 8;
            }
            m25355(outputStream);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25354(OutputStream outputStream) throws IOException {
        outputStream.write(this.f19682);
        this.f19686 = this.f19684 * this.f19685;
        this.f19688 = 0;
        m25358(this.f19682 + 1, outputStream);
        outputStream.write(0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m25355(OutputStream outputStream) throws IOException {
        if (this.f19697 > 0) {
            outputStream.write(this.f19697);
            outputStream.write(this.f19683, 0, this.f19697);
            this.f19697 = 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25356(byte b, OutputStream outputStream) throws IOException {
        byte[] bArr = this.f19683;
        int i = this.f19697;
        this.f19697 = i + 1;
        bArr[i] = b;
        if (this.f19697 >= 254) {
            m25355(outputStream);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25357(int i) {
        for (int i2 = 0; i2 < i; i2++) {
            this.f19691[i2] = -1;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m25358(int r11, java.io.OutputStream r12) throws java.io.IOException {
        /*
            r10 = this;
            r9 = 0
            r10.f19689 = r11
            r10.f19687 = r9
            int r7 = r10.f19689
            r10.f19695 = r7
            int r7 = r10.f19695
            int r7 = r10.m25352((int) r7)
            r10.f19694 = r7
            r7 = 1
            int r8 = r11 + -1
            int r7 = r7 << r8
            r10.f19690 = r7
            int r7 = r10.f19690
            int r7 = r7 + 1
            r10.f19681 = r7
            int r7 = r10.f19690
            int r7 = r7 + 2
            r10.f19677 = r7
            r10.f19697 = r9
            int r2 = r10.m25351()
            r4 = 0
            int r3 = r10.f19676
        L_0x002c:
            r7 = 65536(0x10000, float:9.18355E-41)
            if (r3 >= r7) goto L_0x0035
            int r4 = r4 + 1
            int r3 = r3 * 2
            goto L_0x002c
        L_0x0035:
            int r4 = 8 - r4
            int r5 = r10.f19676
            r10.m25357((int) r5)
            int r7 = r10.f19690
            r10.m25353(r7, r12)
        L_0x0041:
            int r0 = r10.m25351()
            r7 = -1
            if (r0 == r7) goto L_0x009a
            int r7 = r10.f19692
            int r7 = r0 << r7
            int r3 = r7 + r2
            int r7 = r0 << r4
            r6 = r7 ^ r2
            int[] r7 = r10.f19691
            r7 = r7[r6]
            if (r7 != r3) goto L_0x005d
            int[] r7 = r10.f19675
            r2 = r7[r6]
            goto L_0x0041
        L_0x005d:
            int[] r7 = r10.f19691
            r7 = r7[r6]
            if (r7 < 0) goto L_0x007d
            int r1 = r5 - r6
            if (r6 != 0) goto L_0x0068
            r1 = 1
        L_0x0068:
            int r6 = r6 - r1
            if (r6 >= 0) goto L_0x006c
            int r6 = r6 + r5
        L_0x006c:
            int[] r7 = r10.f19691
            r7 = r7[r6]
            if (r7 != r3) goto L_0x0077
            int[] r7 = r10.f19675
            r2 = r7[r6]
            goto L_0x0041
        L_0x0077:
            int[] r7 = r10.f19691
            r7 = r7[r6]
            if (r7 >= 0) goto L_0x0068
        L_0x007d:
            r10.m25353(r2, r12)
            r2 = r0
            int r7 = r10.f19677
            int r8 = r10.f19693
            if (r7 >= r8) goto L_0x0096
            int[] r7 = r10.f19675
            int r8 = r10.f19677
            int r9 = r8 + 1
            r10.f19677 = r9
            r7[r6] = r8
            int[] r7 = r10.f19691
            r7[r6] = r3
            goto L_0x0041
        L_0x0096:
            r10.m25359((java.io.OutputStream) r12)
            goto L_0x0041
        L_0x009a:
            r10.m25353(r2, r12)
            int r7 = r10.f19681
            r10.m25353(r7, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.gifencoder.LZWEncoder.m25358(int, java.io.OutputStream):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25359(OutputStream outputStream) throws IOException {
        m25357(this.f19676);
        this.f19677 = this.f19690 + 2;
        this.f19687 = true;
        m25353(this.f19690, outputStream);
    }
}
