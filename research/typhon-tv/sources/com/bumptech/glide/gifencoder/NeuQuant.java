package com.bumptech.glide.gifencoder;

import com.mopub.nativeads.MoPubNativeAdPositioning;

class NeuQuant {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected int[] f19698 = new int[256];

    /* renamed from: ʼ  reason: contains not printable characters */
    protected int[] f19699 = new int[256];

    /* renamed from: ʽ  reason: contains not printable characters */
    protected int[] f19700 = new int[256];

    /* renamed from: ˑ  reason: contains not printable characters */
    protected int[] f19701 = new int[32];

    /* renamed from: 连任  reason: contains not printable characters */
    protected int[][] f19702;

    /* renamed from: 靐  reason: contains not printable characters */
    protected byte[] f19703;

    /* renamed from: 麤  reason: contains not printable characters */
    protected int f19704;

    /* renamed from: 齉  reason: contains not printable characters */
    protected int f19705;

    /* renamed from: 龘  reason: contains not printable characters */
    protected int f19706;

    public NeuQuant(byte[] bArr, int i, int i2) {
        this.f19703 = bArr;
        this.f19705 = i;
        this.f19704 = i2;
        this.f19702 = new int[256][];
        for (int i3 = 0; i3 < 256; i3++) {
            this.f19702[i3] = new int[4];
            int[] iArr = this.f19702[i3];
            int i4 = (i3 << 12) / 256;
            iArr[2] = i4;
            iArr[1] = i4;
            iArr[0] = i4;
            this.f19700[i3] = 256;
            this.f19699[i3] = 0;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m25360() {
        for (int i = 0; i < 256; i++) {
            int[] iArr = this.f19702[i];
            iArr[0] = iArr[0] >> 4;
            int[] iArr2 = this.f19702[i];
            iArr2[1] = iArr2[1] >> 4;
            int[] iArr3 = this.f19702[i];
            iArr3[2] = iArr3[2] >> 4;
            this.f19702[i][3] = i;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m25361(int i, int i2, int i3) {
        int i4 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int i5 = Integer.MAX_VALUE;
        int i6 = -1;
        int i7 = -1;
        for (int i8 = 0; i8 < 256; i8++) {
            int[] iArr = this.f19702[i8];
            int i9 = iArr[0] - i;
            if (i9 < 0) {
                i9 = -i9;
            }
            int i10 = iArr[1] - i2;
            if (i10 < 0) {
                i10 = -i10;
            }
            int i11 = i9 + i10;
            int i12 = iArr[2] - i3;
            if (i12 < 0) {
                i12 = -i12;
            }
            int i13 = i11 + i12;
            if (i13 < i4) {
                i4 = i13;
                i6 = i8;
            }
            int i14 = i13 - (this.f19699[i8] >> 12);
            if (i14 < i5) {
                i5 = i14;
                i7 = i8;
            }
            int i15 = this.f19700[i8] >> 10;
            int[] iArr2 = this.f19700;
            iArr2[i8] = iArr2[i8] - i15;
            int[] iArr3 = this.f19699;
            iArr3[i8] = iArr3[i8] + (i15 << 10);
        }
        int[] iArr4 = this.f19700;
        iArr4[i6] = iArr4[i6] + 64;
        int[] iArr5 = this.f19699;
        iArr5[i6] = iArr5[i6] - 65536;
        return i7;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25362() {
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < 256; i3++) {
            int[] iArr = this.f19702[i3];
            int i4 = i3;
            int i5 = iArr[1];
            for (int i6 = i3 + 1; i6 < 256; i6++) {
                int[] iArr2 = this.f19702[i6];
                if (iArr2[1] < i5) {
                    i4 = i6;
                    i5 = iArr2[1];
                }
            }
            int[] iArr3 = this.f19702[i4];
            if (i3 != i4) {
                int i7 = iArr3[0];
                iArr3[0] = iArr[0];
                iArr[0] = i7;
                int i8 = iArr3[1];
                iArr3[1] = iArr[1];
                iArr[1] = i8;
                int i9 = iArr3[2];
                iArr3[2] = iArr[2];
                iArr[2] = i9;
                int i10 = iArr3[3];
                iArr3[3] = iArr[3];
                iArr[3] = i10;
            }
            if (i5 != i) {
                this.f19698[i] = (i2 + i3) >> 1;
                for (int i11 = i + 1; i11 < i5; i11++) {
                    this.f19698[i11] = i3;
                }
                i = i5;
                i2 = i3;
            }
        }
        this.f19698[i] = (i2 + 255) >> 1;
        for (int i12 = i + 1; i12 < 256; i12++) {
            this.f19698[i12] = 255;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25363(int i, int i2, int i3, int i4, int i5) {
        int[] iArr = this.f19702[i2];
        iArr[0] = iArr[0] - (((iArr[0] - i3) * i) / 1024);
        iArr[1] = iArr[1] - (((iArr[1] - i4) * i) / 1024);
        iArr[2] = iArr[2] - (((iArr[2] - i5) * i) / 1024);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public byte[] m25364() {
        m25365();
        m25360();
        m25362();
        return m25368();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25365() {
        int i;
        if (this.f19705 < 1509) {
            this.f19704 = 1;
        }
        this.f19706 = ((this.f19704 - 1) / 3) + 30;
        byte[] bArr = this.f19703;
        int i2 = 0;
        int i3 = this.f19705;
        int i4 = this.f19705 / (this.f19704 * 3);
        int i5 = i4 / 100;
        int i6 = 1024;
        int i7 = 2048;
        int i8 = 2048 >> 6;
        if (i8 <= 1) {
            i8 = 0;
        }
        for (int i9 = 0; i9 < i; i9++) {
            this.f19701[i9] = ((((i * i) - (i9 * i9)) * 256) / (i * i)) * 1024;
        }
        int i10 = this.f19705 < 1509 ? 3 : this.f19705 % 499 != 0 ? 1497 : this.f19705 % 491 != 0 ? 1473 : this.f19705 % 487 != 0 ? 1461 : 1509;
        int i11 = 0;
        while (i11 < i4) {
            int i12 = (bArr[i2 + 0] & 255) << 4;
            int i13 = (bArr[i2 + 1] & 255) << 4;
            int i14 = (bArr[i2 + 2] & 255) << 4;
            int r3 = m25361(i12, i13, i14);
            m25363(i6, r3, i12, i13, i14);
            if (i != 0) {
                m25367(i, r3, i12, i13, i14);
            }
            i2 += i10;
            if (i2 >= i3) {
                i2 -= this.f19705;
            }
            i11++;
            if (i5 == 0) {
                i5 = 1;
            }
            if (i11 % i5 == 0) {
                i6 -= i6 / this.f19706;
                i7 -= i7 / 30;
                i = i7 >> 6;
                if (i <= 1) {
                    i = 0;
                }
                for (int i15 = 0; i15 < i; i15++) {
                    this.f19701[i15] = ((((i * i) - (i15 * i15)) * 256) / (i * i)) * i6;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m25366(int i, int i2, int i3) {
        int i4 = 1000;
        int i5 = -1;
        int i6 = this.f19698[i2];
        int i7 = i6 - 1;
        while (true) {
            if (i6 >= 256 && i7 < 0) {
                return i5;
            }
            if (i6 < 256) {
                int[] iArr = this.f19702[i6];
                int i8 = iArr[1] - i2;
                if (i8 >= i4) {
                    i6 = 256;
                } else {
                    i6++;
                    if (i8 < 0) {
                        i8 = -i8;
                    }
                    int i9 = iArr[0] - i;
                    if (i9 < 0) {
                        i9 = -i9;
                    }
                    int i10 = i8 + i9;
                    if (i10 < i4) {
                        int i11 = iArr[2] - i3;
                        if (i11 < 0) {
                            i11 = -i11;
                        }
                        int i12 = i10 + i11;
                        if (i12 < i4) {
                            i4 = i12;
                            i5 = iArr[3];
                        }
                    }
                }
            }
            if (i7 >= 0) {
                int[] iArr2 = this.f19702[i7];
                int i13 = i2 - iArr2[1];
                if (i13 >= i4) {
                    i7 = -1;
                } else {
                    i7--;
                    if (i13 < 0) {
                        i13 = -i13;
                    }
                    int i14 = iArr2[0] - i;
                    if (i14 < 0) {
                        i14 = -i14;
                    }
                    int i15 = i13 + i14;
                    if (i15 < i4) {
                        int i16 = iArr2[2] - i3;
                        if (i16 < 0) {
                            i16 = -i16;
                        }
                        int i17 = i15 + i16;
                        if (i17 < i4) {
                            i4 = i17;
                            i5 = iArr2[3];
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25367(int i, int i2, int i3, int i4, int i5) {
        int i6;
        int i7 = i2 - i;
        if (i7 < -1) {
            i7 = -1;
        }
        int i8 = i2 + i;
        if (i8 > 256) {
            i8 = 256;
        }
        int i9 = 1;
        int i10 = i2 - 1;
        int i11 = i2 + 1;
        while (true) {
            if (i11 < i8 || i10 > i7) {
                int i12 = i9 + 1;
                int i13 = this.f19701[i9];
                if (i11 < i8) {
                    i6 = i11 + 1;
                    int[] iArr = this.f19702[i11];
                    try {
                        iArr[0] = iArr[0] - (((iArr[0] - i3) * i13) / 262144);
                        iArr[1] = iArr[1] - (((iArr[1] - i4) * i13) / 262144);
                        iArr[2] = iArr[2] - (((iArr[2] - i5) * i13) / 262144);
                    } catch (Exception e) {
                    }
                } else {
                    i6 = i11;
                }
                if (i10 > i7) {
                    int i14 = i10 - 1;
                    int[] iArr2 = this.f19702[i10];
                    try {
                        iArr2[0] = iArr2[0] - (((iArr2[0] - i3) * i13) / 262144);
                        iArr2[1] = iArr2[1] - (((iArr2[1] - i4) * i13) / 262144);
                        iArr2[2] = iArr2[2] - (((iArr2[2] - i5) * i13) / 262144);
                        i9 = i12;
                        i10 = i14;
                        i11 = i6;
                    } catch (Exception e2) {
                        i9 = i12;
                        i10 = i14;
                        i11 = i6;
                    }
                } else {
                    i9 = i12;
                    i11 = i6;
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public byte[] m25368() {
        byte[] bArr = new byte[768];
        int[] iArr = new int[256];
        for (int i = 0; i < 256; i++) {
            iArr[this.f19702[i][3]] = i;
        }
        int i2 = 0;
        int i3 = 0;
        while (i2 < 256) {
            int i4 = iArr[i2];
            int i5 = i3 + 1;
            bArr[i3] = (byte) this.f19702[i4][0];
            int i6 = i5 + 1;
            bArr[i5] = (byte) this.f19702[i4][1];
            bArr[i6] = (byte) this.f19702[i4][2];
            i2++;
            i3 = i6 + 1;
        }
        return bArr;
    }
}
