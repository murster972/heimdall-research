package com.bumptech.glide.provider;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import java.io.File;

public class FixedLoadProvider<A, T, Z, R> implements LoadProvider<A, T, Z, R> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ResourceTranscoder<Z, R> f20104;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DataLoadProvider<T, Z> f20105;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelLoader<A, T> f20106;

    public FixedLoadProvider(ModelLoader<A, T> modelLoader, ResourceTranscoder<Z, R> resourceTranscoder, DataLoadProvider<T, Z> dataLoadProvider) {
        if (modelLoader == null) {
            throw new NullPointerException("ModelLoader must not be null");
        }
        this.f20106 = modelLoader;
        if (resourceTranscoder == null) {
            throw new NullPointerException("Transcoder must not be null");
        }
        this.f20104 = resourceTranscoder;
        if (dataLoadProvider == null) {
            throw new NullPointerException("DataLoadProvider must not be null");
        }
        this.f20105 = dataLoadProvider;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ResourceTranscoder<Z, R> m26010() {
        return this.f20104;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public ModelLoader<A, T> m26011() {
        return this.f20106;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<T, Z> m26012() {
        return this.f20105.m25999();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<Z> m26013() {
        return this.f20105.m26000();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<T> m26014() {
        return this.f20105.m26001();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, Z> m26015() {
        return this.f20105.m26002();
    }
}
