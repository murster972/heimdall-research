package com.bumptech.glide.provider;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import java.io.File;

public class ChildLoadProvider<A, T, Z, R> implements LoadProvider<A, T, Z, R>, Cloneable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Encoder<T> f20095;

    /* renamed from: 连任  reason: contains not printable characters */
    private ResourceTranscoder<Z, R> f20096;

    /* renamed from: 靐  reason: contains not printable characters */
    private ResourceDecoder<File, Z> f20097;

    /* renamed from: 麤  reason: contains not printable characters */
    private ResourceEncoder<Z> f20098;

    /* renamed from: 齉  reason: contains not printable characters */
    private ResourceDecoder<T, Z> f20099;

    /* renamed from: 龘  reason: contains not printable characters */
    private final LoadProvider<A, T, Z, R> f20100;

    public ChildLoadProvider(LoadProvider<A, T, Z, R> loadProvider) {
        this.f20100 = loadProvider;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ResourceTranscoder<Z, R> m25990() {
        return this.f20096 != null ? this.f20096 : this.f20100.m26016();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public ChildLoadProvider<A, T, Z, R> clone() {
        try {
            return (ChildLoadProvider) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public ModelLoader<A, T> m25992() {
        return this.f20100.m26017();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<T, Z> m25993() {
        return this.f20099 != null ? this.f20099 : this.f20100.m25999();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<Z> m25994() {
        return this.f20098 != null ? this.f20098 : this.f20100.m26000();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<T> m25995() {
        return this.f20095 != null ? this.f20095 : this.f20100.m26001();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, Z> m25996() {
        return this.f20097 != null ? this.f20097 : this.f20100.m26002();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25997(Encoder<T> encoder) {
        this.f20095 = encoder;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25998(ResourceDecoder<T, Z> resourceDecoder) {
        this.f20099 = resourceDecoder;
    }
}
