package com.bumptech.glide.provider;

import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;

public interface LoadProvider<A, T, Z, R> extends DataLoadProvider<T, Z> {
    /* renamed from: ʻ  reason: contains not printable characters */
    ResourceTranscoder<Z, R> m26016();

    /* renamed from: 连任  reason: contains not printable characters */
    ModelLoader<A, T> m26017();
}
