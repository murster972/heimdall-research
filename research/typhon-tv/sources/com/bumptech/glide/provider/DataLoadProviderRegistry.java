package com.bumptech.glide.provider;

import com.bumptech.glide.util.MultiClassKey;
import java.util.HashMap;
import java.util.Map;

public class DataLoadProviderRegistry {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final MultiClassKey f20101 = new MultiClassKey();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<MultiClassKey, DataLoadProvider<?, ?>> f20102 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    public <T, Z> DataLoadProvider<T, Z> m26003(Class<T> cls, Class<Z> cls2) {
        DataLoadProvider<T, Z> dataLoadProvider;
        synchronized (f20101) {
            f20101.m26189(cls, cls2);
            dataLoadProvider = this.f20102.get(f20101);
        }
        return dataLoadProvider == null ? EmptyDataLoadProvider.m26005() : dataLoadProvider;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T, Z> void m26004(Class<T> cls, Class<Z> cls2, DataLoadProvider<T, Z> dataLoadProvider) {
        this.f20102.put(new MultiClassKey(cls, cls2), dataLoadProvider);
    }
}
