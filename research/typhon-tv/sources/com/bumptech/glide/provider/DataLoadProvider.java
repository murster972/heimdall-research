package com.bumptech.glide.provider;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import java.io.File;

public interface DataLoadProvider<T, Z> {
    /* renamed from: 靐  reason: contains not printable characters */
    ResourceDecoder<T, Z> m25999();

    /* renamed from: 麤  reason: contains not printable characters */
    ResourceEncoder<Z> m26000();

    /* renamed from: 齉  reason: contains not printable characters */
    Encoder<T> m26001();

    /* renamed from: 龘  reason: contains not printable characters */
    ResourceDecoder<File, Z> m26002();
}
