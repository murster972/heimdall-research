package com.bumptech.glide.gifdecoder;

import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class GifHeaderParser {

    /* renamed from: 靐  reason: contains not printable characters */
    private ByteBuffer f19650;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f19651 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private GifHeader f19652;

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f19653 = new byte[256];

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m25318() {
        boolean z = true;
        this.f19652.f19647.f19636 = m25322();
        this.f19652.f19647.f19633 = m25322();
        this.f19652.f19647.f19635 = m25322();
        this.f19652.f19647.f19634 = m25322();
        int r2 = m25321();
        boolean z2 = (r2 & 128) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((r2 & 7) + 1));
        GifFrame gifFrame = this.f19652.f19647;
        if ((r2 & 64) == 0) {
            z = false;
        }
        gifFrame.f19632 = z;
        if (z2) {
            this.f19652.f19647.f19631 = m25330(pow);
        } else {
            this.f19652.f19647.f19631 = null;
        }
        this.f19652.f19647.f19630 = this.f19650.position();
        m25325();
        if (!m25331()) {
            this.f19652.f19648++;
            this.f19652.f19645.add(this.f19652.f19647);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000b  */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m25319() {
        /*
            r5 = this;
            r4 = 1
        L_0x0001:
            r5.m25323()
            byte[] r2 = r5.f19653
            r3 = 0
            byte r2 = r2[r3]
            if (r2 != r4) goto L_0x001f
            byte[] r2 = r5.f19653
            byte r2 = r2[r4]
            r0 = r2 & 255(0xff, float:3.57E-43)
            byte[] r2 = r5.f19653
            r3 = 2
            byte r2 = r2[r3]
            r1 = r2 & 255(0xff, float:3.57E-43)
            com.bumptech.glide.gifdecoder.GifHeader r2 = r5.f19652
            int r3 = r1 << 8
            r3 = r3 | r0
            r2.f19640 = r3
        L_0x001f:
            int r2 = r5.f19651
            if (r2 <= 0) goto L_0x0029
            boolean r2 = r5.m25331()
            if (r2 == 0) goto L_0x0001
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.gifdecoder.GifHeaderParser.m25319():void");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m25320() {
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + ((char) m25321());
        }
        if (!str.startsWith("GIF")) {
            this.f19652.f19646 = 1;
            return;
        }
        m25324();
        if (this.f19652.f19639 && !m25331()) {
            this.f19652.f19649 = m25330(this.f19652.f19642);
            this.f19652.f19641 = this.f19652.f19649[this.f19652.f19643];
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m25321() {
        try {
            return this.f19650.get() & 255;
        } catch (Exception e) {
            this.f19652.f19646 = 1;
            return 0;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private int m25322() {
        return this.f19650.getShort();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int m25323() {
        this.f19651 = m25321();
        int i = 0;
        if (this.f19651 > 0) {
            int i2 = 0;
            while (i < this.f19651) {
                try {
                    i2 = this.f19651 - i;
                    this.f19650.get(this.f19653, i, i2);
                    i += i2;
                } catch (Exception e) {
                    if (Log.isLoggable("GifHeaderParser", 3)) {
                        Log.d("GifHeaderParser", "Error Reading Block n: " + i + " count: " + i2 + " blockSize: " + this.f19651, e);
                    }
                    this.f19652.f19646 = 1;
                }
            }
        }
        return i;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m25324() {
        this.f19652.f19637 = m25322();
        this.f19652.f19638 = m25322();
        int r0 = m25321();
        this.f19652.f19639 = (r0 & 128) != 0;
        this.f19652.f19642 = 2 << (r0 & 7);
        this.f19652.f19643 = m25321();
        this.f19652.f19644 = m25321();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m25325() {
        m25321();
        m25326();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m25326() {
        int r0;
        do {
            r0 = m25321();
            this.f19650.position(this.f19650.position() + r0);
        } while (r0 > 0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m25327() {
        boolean z = true;
        m25321();
        int r1 = m25321();
        this.f19652.f19647.f19627 = (r1 & 28) >> 2;
        if (this.f19652.f19647.f19627 == 0) {
            this.f19652.f19647.f19627 = 1;
        }
        GifFrame gifFrame = this.f19652.f19647;
        if ((r1 & 1) == 0) {
            z = false;
        }
        gifFrame.f19626 = z;
        int r0 = m25322();
        if (r0 < 3) {
            r0 = 10;
        }
        this.f19652.f19647.f19629 = r0 * 10;
        this.f19652.f19647.f19628 = m25321();
        m25321();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m25328() {
        boolean z = false;
        while (!z && !m25331()) {
            switch (m25321()) {
                case 33:
                    switch (m25321()) {
                        case 1:
                            m25326();
                            break;
                        case 249:
                            this.f19652.f19647 = new GifFrame();
                            m25327();
                            break;
                        case 254:
                            m25326();
                            break;
                        case 255:
                            m25323();
                            String str = "";
                            for (int i = 0; i < 11; i++) {
                                str = str + ((char) this.f19653[i]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                m25326();
                                break;
                            } else {
                                m25319();
                                break;
                            }
                        default:
                            m25326();
                            break;
                    }
                case 44:
                    if (this.f19652.f19647 == null) {
                        this.f19652.f19647 = new GifFrame();
                    }
                    m25318();
                    break;
                case 59:
                    z = true;
                    break;
                default:
                    this.f19652.f19646 = 1;
                    break;
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25329() {
        this.f19650 = null;
        Arrays.fill(this.f19653, (byte) 0);
        this.f19652 = new GifHeader();
        this.f19651 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int[] m25330(int i) {
        int[] iArr = null;
        byte[] bArr = new byte[(i * 3)];
        try {
            this.f19650.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i3 < i) {
                int i4 = i2 + 1;
                byte b = bArr[i2] & 255;
                int i5 = i4 + 1;
                byte b2 = bArr[i4] & 255;
                int i6 = i5 + 1;
                int i7 = i3 + 1;
                iArr[i3] = -16777216 | (b << 16) | (b2 << 8) | (bArr[i5] & 255);
                i2 = i6;
                i3 = i7;
            }
        } catch (BufferUnderflowException e) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.f19652.f19646 = 1;
        }
        return iArr;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean m25331() {
        return this.f19652.f19646 != 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GifHeader m25332() {
        if (this.f19650 == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (m25331()) {
            return this.f19652;
        } else {
            m25320();
            if (!m25331()) {
                m25328();
                if (this.f19652.f19648 < 0) {
                    this.f19652.f19646 = 1;
                }
            }
            return this.f19652;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GifHeaderParser m25333(byte[] bArr) {
        m25329();
        if (bArr != null) {
            this.f19650 = ByteBuffer.wrap(bArr);
            this.f19650.rewind();
            this.f19650.order(ByteOrder.LITTLE_ENDIAN);
        } else {
            this.f19650 = null;
            this.f19652.f19646 = 2;
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25334() {
        this.f19650 = null;
        this.f19652 = null;
    }
}
