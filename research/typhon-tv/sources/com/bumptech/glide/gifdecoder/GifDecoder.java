package com.bumptech.glide.gifdecoder;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

public class GifDecoder {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Bitmap.Config f19609 = Bitmap.Config.ARGB_8888;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f19610 = GifDecoder.class.getSimpleName();

    /* renamed from: ʻ  reason: contains not printable characters */
    private short[] f19611;

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte[] f19612;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte[] f19613;

    /* renamed from: ʾ  reason: contains not printable characters */
    private GifHeader f19614;

    /* renamed from: ʿ  reason: contains not printable characters */
    private BitmapProvider f19615;

    /* renamed from: ˈ  reason: contains not printable characters */
    private byte[] f19616;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f19617;

    /* renamed from: ˑ  reason: contains not printable characters */
    private byte[] f19618;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int[] f19619;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f19620;

    /* renamed from: 连任  reason: contains not printable characters */
    private final byte[] f19621 = new byte[256];

    /* renamed from: 麤  reason: contains not printable characters */
    private ByteBuffer f19622;

    /* renamed from: 齉  reason: contains not printable characters */
    private int[] f19623;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Bitmap f19624;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f19625;

    public interface BitmapProvider {
        /* renamed from: 龘  reason: contains not printable characters */
        Bitmap m25314(int i, int i2, Bitmap.Config config);

        /* renamed from: 龘  reason: contains not printable characters */
        void m25315(Bitmap bitmap);
    }

    public GifDecoder(BitmapProvider bitmapProvider) {
        this.f19615 = bitmapProvider;
        this.f19614 = new GifHeader();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m25299() {
        try {
            return this.f19622.get() & 255;
        } catch (Exception e) {
            this.f19617 = 1;
            return 0;
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m25300() {
        int r0 = m25299();
        int i = 0;
        if (r0 > 0) {
            while (i < r0) {
                int i2 = r0 - i;
                try {
                    this.f19622.get(this.f19621, i, i2);
                    i += i2;
                } catch (Exception e) {
                    Log.w(f19610, "Error Reading Block", e);
                    this.f19617 = 1;
                }
            }
        }
        return i;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private Bitmap m25301() {
        Bitmap r0 = this.f19615.m25314(this.f19614.f19637, this.f19614.f19638, f19609);
        if (r0 == null) {
            r0 = Bitmap.createBitmap(this.f19614.f19637, this.f19614.f19638, f19609);
        }
        m25303(r0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Bitmap m25302(GifFrame gifFrame, GifFrame gifFrame2) {
        int i = this.f19614.f19637;
        int i2 = this.f19614.f19638;
        int[] iArr = this.f19619;
        if (gifFrame2 != null && gifFrame2.f19627 > 0) {
            if (gifFrame2.f19627 == 2) {
                int i3 = 0;
                if (!gifFrame.f19626) {
                    i3 = this.f19614.f19641;
                }
                Arrays.fill(iArr, i3);
            } else if (gifFrame2.f19627 == 3 && this.f19624 != null) {
                this.f19624.getPixels(iArr, 0, i, 0, 0, i, i2);
            }
        }
        m25304(gifFrame);
        int i4 = 1;
        int i5 = 8;
        int i6 = 0;
        for (int i7 = 0; i7 < gifFrame.f19634; i7++) {
            int i8 = i7;
            if (gifFrame.f19632) {
                if (i6 >= gifFrame.f19634) {
                    i4++;
                    switch (i4) {
                        case 2:
                            i6 = 4;
                            break;
                        case 3:
                            i6 = 2;
                            i5 = 4;
                            break;
                        case 4:
                            i6 = 1;
                            i5 = 2;
                            break;
                    }
                }
                i8 = i6;
                i6 += i5;
            }
            int i9 = i8 + gifFrame.f19633;
            if (i9 < this.f19614.f19638) {
                int i10 = i9 * this.f19614.f19637;
                int i11 = i10 + gifFrame.f19636;
                int i12 = i11 + gifFrame.f19635;
                if (this.f19614.f19637 + i10 < i12) {
                    i12 = i10 + this.f19614.f19637;
                }
                int i13 = i7 * gifFrame.f19635;
                while (i11 < i12) {
                    int i14 = i13 + 1;
                    int i15 = this.f19623[this.f19618[i13] & 255];
                    if (i15 != 0) {
                        iArr[i11] = i15;
                    }
                    i11++;
                    i13 = i14;
                }
            }
        }
        if (this.f19625 && (gifFrame.f19627 == 0 || gifFrame.f19627 == 1)) {
            if (this.f19624 == null) {
                this.f19624 = m25301();
            }
            this.f19624.setPixels(iArr, 0, i, 0, 0, i, i2);
        }
        Bitmap r1 = m25301();
        r1.setPixels(iArr, 0, i, 0, 0, i, i2);
        return r1;
    }

    @TargetApi(12)
    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25303(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 12) {
            bitmap.setHasAlpha(true);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m25304(com.bumptech.glide.gifdecoder.GifFrame r25) {
        /*
            r24 = this;
            if (r25 == 0) goto L_0x0011
            r0 = r24
            java.nio.ByteBuffer r0 = r0.f19622
            r22 = r0
            r0 = r25
            int r0 = r0.f19630
            r23 = r0
            r22.position(r23)
        L_0x0011:
            if (r25 != 0) goto L_0x00c5
            r0 = r24
            com.bumptech.glide.gifdecoder.GifHeader r0 = r0.f19614
            r22 = r0
            r0 = r22
            int r0 = r0.f19637
            r22 = r0
            r0 = r24
            com.bumptech.glide.gifdecoder.GifHeader r0 = r0.f19614
            r23 = r0
            r0 = r23
            int r0 = r0.f19638
            r23 = r0
            int r16 = r22 * r23
        L_0x002d:
            r0 = r24
            byte[] r0 = r0.f19618
            r22 = r0
            if (r22 == 0) goto L_0x0046
            r0 = r24
            byte[] r0 = r0.f19618
            r22 = r0
            r0 = r22
            int r0 = r0.length
            r22 = r0
            r0 = r22
            r1 = r16
            if (r0 >= r1) goto L_0x0052
        L_0x0046:
            r0 = r16
            byte[] r0 = new byte[r0]
            r22 = r0
            r0 = r22
            r1 = r24
            r1.f19618 = r0
        L_0x0052:
            r0 = r24
            short[] r0 = r0.f19611
            r22 = r0
            if (r22 != 0) goto L_0x0068
            r22 = 4096(0x1000, float:5.74E-42)
            r0 = r22
            short[] r0 = new short[r0]
            r22 = r0
            r0 = r22
            r1 = r24
            r1.f19611 = r0
        L_0x0068:
            r0 = r24
            byte[] r0 = r0.f19612
            r22 = r0
            if (r22 != 0) goto L_0x007e
            r22 = 4096(0x1000, float:5.74E-42)
            r0 = r22
            byte[] r0 = new byte[r0]
            r22 = r0
            r0 = r22
            r1 = r24
            r1.f19612 = r0
        L_0x007e:
            r0 = r24
            byte[] r0 = r0.f19613
            r22 = r0
            if (r22 != 0) goto L_0x0094
            r22 = 4097(0x1001, float:5.741E-42)
            r0 = r22
            byte[] r0 = new byte[r0]
            r22 = r0
            r0 = r22
            r1 = r24
            r1.f19613 = r0
        L_0x0094:
            int r10 = r24.m25299()
            r22 = 1
            int r5 = r22 << r10
            int r12 = r5 + 1
            int r2 = r5 + 2
            r17 = -1
            int r8 = r10 + 1
            r22 = 1
            int r22 = r22 << r8
            int r7 = r22 + -1
            r6 = 0
        L_0x00ab:
            if (r6 >= r5) goto L_0x00d5
            r0 = r24
            short[] r0 = r0.f19611
            r22 = r0
            r23 = 0
            r22[r6] = r23
            r0 = r24
            byte[] r0 = r0.f19612
            r22 = r0
            byte r0 = (byte) r6
            r23 = r0
            r22[r6] = r23
            int r6 = r6 + 1
            goto L_0x00ab
        L_0x00c5:
            r0 = r25
            int r0 = r0.f19635
            r22 = r0
            r0 = r25
            int r0 = r0.f19634
            r23 = r0
            int r16 = r22 * r23
            goto L_0x002d
        L_0x00d5:
            r3 = 0
            r18 = r3
            r20 = r3
            r13 = r3
            r9 = r3
            r4 = r3
            r11 = r3
            r14 = 0
        L_0x00df:
            r0 = r16
            if (r14 >= r0) goto L_0x00f3
            if (r9 != 0) goto L_0x0107
            int r9 = r24.m25300()
            if (r9 > 0) goto L_0x0106
            r22 = 3
            r0 = r22
            r1 = r24
            r1.f19617 = r0
        L_0x00f3:
            r14 = r18
        L_0x00f5:
            r0 = r16
            if (r14 >= r0) goto L_0x0200
            r0 = r24
            byte[] r0 = r0.f19618
            r22 = r0
            r23 = 0
            r22[r14] = r23
            int r14 = r14 + 1
            goto L_0x00f5
        L_0x0106:
            r3 = 0
        L_0x0107:
            r0 = r24
            byte[] r0 = r0.f19621
            r22 = r0
            byte r22 = r22[r3]
            r0 = r22
            r0 = r0 & 255(0xff, float:3.57E-43)
            r22 = r0
            int r22 = r22 << r4
            int r11 = r11 + r22
            int r4 = r4 + 8
            int r3 = r3 + 1
            int r9 = r9 + -1
            r21 = r20
        L_0x0121:
            if (r4 < r8) goto L_0x0207
            r6 = r11 & r7
            int r11 = r11 >> r8
            int r4 = r4 - r8
            if (r6 != r5) goto L_0x0136
            int r8 = r10 + 1
            r22 = 1
            int r22 = r22 << r8
            int r7 = r22 + -1
            int r2 = r5 + 2
            r17 = -1
            goto L_0x0121
        L_0x0136:
            if (r6 <= r2) goto L_0x0143
            r22 = 3
            r0 = r22
            r1 = r24
            r1.f19617 = r0
            r20 = r21
            goto L_0x00df
        L_0x0143:
            if (r6 != r12) goto L_0x0148
            r20 = r21
            goto L_0x00df
        L_0x0148:
            r22 = -1
            r0 = r17
            r1 = r22
            if (r0 != r1) goto L_0x0168
            r0 = r24
            byte[] r0 = r0.f19613
            r22 = r0
            int r20 = r21 + 1
            r0 = r24
            byte[] r0 = r0.f19612
            r23 = r0
            byte r23 = r23[r6]
            r22[r21] = r23
            r17 = r6
            r13 = r6
            r21 = r20
            goto L_0x0121
        L_0x0168:
            r15 = r6
            if (r6 < r2) goto L_0x017c
            r0 = r24
            byte[] r0 = r0.f19613
            r22 = r0
            int r20 = r21 + 1
            byte r0 = (byte) r13
            r23 = r0
            r22[r21] = r23
            r6 = r17
            r21 = r20
        L_0x017c:
            if (r6 < r5) goto L_0x019b
            r0 = r24
            byte[] r0 = r0.f19613
            r22 = r0
            int r20 = r21 + 1
            r0 = r24
            byte[] r0 = r0.f19612
            r23 = r0
            byte r23 = r23[r6]
            r22[r21] = r23
            r0 = r24
            short[] r0 = r0.f19611
            r22 = r0
            short r6 = r22[r6]
            r21 = r20
            goto L_0x017c
        L_0x019b:
            r0 = r24
            byte[] r0 = r0.f19612
            r22 = r0
            byte r22 = r22[r6]
            r0 = r22
            r13 = r0 & 255(0xff, float:3.57E-43)
            r0 = r24
            byte[] r0 = r0.f19613
            r22 = r0
            int r20 = r21 + 1
            byte r0 = (byte) r13
            r23 = r0
            r22[r21] = r23
            r22 = 4096(0x1000, float:5.74E-42)
            r0 = r22
            if (r2 >= r0) goto L_0x01e1
            r0 = r24
            short[] r0 = r0.f19611
            r22 = r0
            r0 = r17
            short r0 = (short) r0
            r23 = r0
            r22[r2] = r23
            r0 = r24
            byte[] r0 = r0.f19612
            r22 = r0
            byte r0 = (byte) r13
            r23 = r0
            r22[r2] = r23
            int r2 = r2 + 1
            r22 = r2 & r7
            if (r22 != 0) goto L_0x01e1
            r22 = 4096(0x1000, float:5.74E-42)
            r0 = r22
            if (r2 >= r0) goto L_0x01e1
            int r8 = r8 + 1
            int r7 = r7 + r2
        L_0x01e1:
            r17 = r15
            r19 = r18
        L_0x01e5:
            if (r20 <= 0) goto L_0x0201
            int r20 = r20 + -1
            r0 = r24
            byte[] r0 = r0.f19618
            r22 = r0
            int r18 = r19 + 1
            r0 = r24
            byte[] r0 = r0.f19613
            r23 = r0
            byte r23 = r23[r20]
            r22[r19] = r23
            int r14 = r14 + 1
            r19 = r18
            goto L_0x01e5
        L_0x0200:
            return
        L_0x0201:
            r18 = r19
            r21 = r20
            goto L_0x0121
        L_0x0207:
            r20 = r21
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.gifdecoder.GifDecoder.m25304(com.bumptech.glide.gifdecoder.GifFrame):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public synchronized Bitmap m25305() {
        Bitmap bitmap = null;
        synchronized (this) {
            if (this.f19614.f19648 <= 0 || this.f19620 < 0) {
                if (Log.isLoggable(f19610, 3)) {
                    Log.d(f19610, "unable to decode frame, frameCount=" + this.f19614.f19648 + " framePointer=" + this.f19620);
                }
                this.f19617 = 1;
            }
            if (this.f19617 != 1 && this.f19617 != 2) {
                this.f19617 = 0;
                GifFrame gifFrame = this.f19614.f19645.get(this.f19620);
                GifFrame gifFrame2 = null;
                int i = this.f19620 - 1;
                if (i >= 0) {
                    gifFrame2 = this.f19614.f19645.get(i);
                }
                if (gifFrame.f19631 == null) {
                    this.f19623 = this.f19614.f19649;
                } else {
                    this.f19623 = gifFrame.f19631;
                    if (this.f19614.f19643 == gifFrame.f19628) {
                        this.f19614.f19641 = 0;
                    }
                }
                int i2 = 0;
                if (gifFrame.f19626) {
                    i2 = this.f19623[gifFrame.f19628];
                    this.f19623[gifFrame.f19628] = 0;
                }
                if (this.f19623 == null) {
                    if (Log.isLoggable(f19610, 3)) {
                        Log.d(f19610, "No Valid Color Table");
                    }
                    this.f19617 = 1;
                } else {
                    bitmap = m25302(gifFrame, gifFrame2);
                    if (gifFrame.f19626) {
                        this.f19623[gifFrame.f19628] = i2;
                    }
                }
            } else if (Log.isLoggable(f19610, 3)) {
                Log.d(f19610, "Unable to decode frame, status=" + this.f19617);
            }
        }
        return bitmap;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m25306() {
        this.f19614 = null;
        this.f19616 = null;
        this.f19618 = null;
        this.f19619 = null;
        if (this.f19624 != null) {
            this.f19615.m25315(this.f19624);
        }
        this.f19624 = null;
        this.f19622 = null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m25307() {
        return this.f19614.f19640;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m25308() {
        if (this.f19614.f19648 <= 0 || this.f19620 < 0) {
            return -1;
        }
        return m25311(this.f19620);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m25309() {
        return this.f19620;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25310() {
        return this.f19614.f19648;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m25311(int i) {
        if (i < 0 || i >= this.f19614.f19648) {
            return -1;
        }
        return this.f19614.f19645.get(i).f19629;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25312() {
        this.f19620 = (this.f19620 + 1) % this.f19614.f19648;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25313(GifHeader gifHeader, byte[] bArr) {
        this.f19614 = gifHeader;
        this.f19616 = bArr;
        this.f19617 = 0;
        this.f19620 = -1;
        this.f19622 = ByteBuffer.wrap(bArr);
        this.f19622.rewind();
        this.f19622.order(ByteOrder.LITTLE_ENDIAN);
        this.f19625 = false;
        Iterator<GifFrame> it2 = gifHeader.f19645.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (it2.next().f19627 == 3) {
                    this.f19625 = true;
                    break;
                }
            } else {
                break;
            }
        }
        this.f19618 = new byte[(gifHeader.f19637 * gifHeader.f19638)];
        this.f19619 = new int[(gifHeader.f19637 * gifHeader.f19638)];
    }
}
