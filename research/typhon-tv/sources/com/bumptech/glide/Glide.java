package com.bumptech.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.engine.prefill.BitmapPreFiller;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorFileLoader;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorResourceLoader;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorStringLoader;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorUriLoader;
import com.bumptech.glide.load.model.stream.HttpUrlGlideUrlLoader;
import com.bumptech.glide.load.model.stream.StreamByteArrayLoader;
import com.bumptech.glide.load.model.stream.StreamFileLoader;
import com.bumptech.glide.load.model.stream.StreamResourceLoader;
import com.bumptech.glide.load.model.stream.StreamStringLoader;
import com.bumptech.glide.load.model.stream.StreamUriLoader;
import com.bumptech.glide.load.model.stream.StreamUrlLoader;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FileDescriptorBitmapDataLoadProvider;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.bitmap.ImageVideoDataLoadProvider;
import com.bumptech.glide.load.resource.bitmap.StreamBitmapDataLoadProvider;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.file.StreamFileDataLoadProvider;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawableLoadProvider;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapperTransformation;
import com.bumptech.glide.load.resource.gifbitmap.ImageVideoGifDrawableLoadProvider;
import com.bumptech.glide.load.resource.transcode.GifBitmapWrapperDrawableTranscoder;
import com.bumptech.glide.load.resource.transcode.GlideBitmapDrawableTranscoder;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.load.resource.transcode.TranscoderRegistry;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.module.GlideModule;
import com.bumptech.glide.module.ManifestParser;
import com.bumptech.glide.provider.DataLoadProvider;
import com.bumptech.glide.provider.DataLoadProviderRegistry;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ImageViewTargetFactory;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.util.Util;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

public class Glide {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile Glide f3336;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final DecodeFormat f3337;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ImageViewTargetFactory f3338 = new ImageViewTargetFactory();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final TranscoderRegistry f3339 = new TranscoderRegistry();

    /* renamed from: ʾ  reason: contains not printable characters */
    private final GifBitmapWrapperTransformation f3340;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Handler f3341;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final FitCenter f3342;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final DataLoadProviderRegistry f3343;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final CenterCrop f3344;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final GifBitmapWrapperTransformation f3345;

    /* renamed from: 连任  reason: contains not printable characters */
    private final MemoryCache f3346;

    /* renamed from: 靐  reason: contains not printable characters */
    private final GenericLoaderFactory f3347;

    /* renamed from: 麤  reason: contains not printable characters */
    private final BitmapPool f3348;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Engine f3349;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final BitmapPreFiller f3350;

    private static class ClearTarget extends ViewTarget<View, Object> {
        public ClearTarget(View view) {
            super(view);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m25235(Drawable drawable) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25236(Drawable drawable) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25237(Exception exc, Drawable drawable) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25238(Object obj, GlideAnimation<? super Object> glideAnimation) {
        }
    }

    Glide(Engine engine, MemoryCache memoryCache, BitmapPool bitmapPool, Context context, DecodeFormat decodeFormat) {
        this.f3349 = engine;
        this.f3348 = bitmapPool;
        this.f3346 = memoryCache;
        this.f3337 = decodeFormat;
        this.f3347 = new GenericLoaderFactory(context);
        this.f3341 = new Handler(Looper.getMainLooper());
        this.f3350 = new BitmapPreFiller(memoryCache, bitmapPool, decodeFormat);
        this.f3343 = new DataLoadProviderRegistry();
        StreamBitmapDataLoadProvider streamBitmapDataLoadProvider = new StreamBitmapDataLoadProvider(bitmapPool, decodeFormat);
        this.f3343.m26004(InputStream.class, Bitmap.class, streamBitmapDataLoadProvider);
        FileDescriptorBitmapDataLoadProvider fileDescriptorBitmapDataLoadProvider = new FileDescriptorBitmapDataLoadProvider(bitmapPool, decodeFormat);
        this.f3343.m26004(ParcelFileDescriptor.class, Bitmap.class, fileDescriptorBitmapDataLoadProvider);
        ImageVideoDataLoadProvider imageVideoDataLoadProvider = new ImageVideoDataLoadProvider(streamBitmapDataLoadProvider, fileDescriptorBitmapDataLoadProvider);
        this.f3343.m26004(ImageVideoWrapper.class, Bitmap.class, imageVideoDataLoadProvider);
        GifDrawableLoadProvider gifDrawableLoadProvider = new GifDrawableLoadProvider(context, bitmapPool);
        this.f3343.m26004(InputStream.class, GifDrawable.class, gifDrawableLoadProvider);
        this.f3343.m26004(ImageVideoWrapper.class, GifBitmapWrapper.class, new ImageVideoGifDrawableLoadProvider(imageVideoDataLoadProvider, gifDrawableLoadProvider, bitmapPool));
        this.f3343.m26004(InputStream.class, File.class, new StreamFileDataLoadProvider());
        m3956(File.class, ParcelFileDescriptor.class, new FileDescriptorFileLoader.Factory());
        m3956(File.class, InputStream.class, new StreamFileLoader.Factory());
        m3956(Integer.TYPE, ParcelFileDescriptor.class, new FileDescriptorResourceLoader.Factory());
        m3956(Integer.TYPE, InputStream.class, new StreamResourceLoader.Factory());
        m3956(Integer.class, ParcelFileDescriptor.class, new FileDescriptorResourceLoader.Factory());
        m3956(Integer.class, InputStream.class, new StreamResourceLoader.Factory());
        m3956(String.class, ParcelFileDescriptor.class, new FileDescriptorStringLoader.Factory());
        m3956(String.class, InputStream.class, new StreamStringLoader.Factory());
        m3956(Uri.class, ParcelFileDescriptor.class, new FileDescriptorUriLoader.Factory());
        m3956(Uri.class, InputStream.class, new StreamUriLoader.Factory());
        m3956(URL.class, InputStream.class, new StreamUrlLoader.Factory());
        m3956(GlideUrl.class, InputStream.class, new HttpUrlGlideUrlLoader.Factory());
        m3956(byte[].class, InputStream.class, new StreamByteArrayLoader.Factory());
        this.f3339.m25932(Bitmap.class, GlideBitmapDrawable.class, new GlideBitmapDrawableTranscoder(context.getResources(), bitmapPool));
        this.f3339.m25932(GifBitmapWrapper.class, GlideDrawable.class, new GifBitmapWrapperDrawableTranscoder(new GlideBitmapDrawableTranscoder(context.getResources(), bitmapPool)));
        this.f3344 = new CenterCrop(bitmapPool);
        this.f3345 = new GifBitmapWrapperTransformation(bitmapPool, (Transformation<Bitmap>) this.f3344);
        this.f3342 = new FitCenter(bitmapPool);
        this.f3340 = new GifBitmapWrapperTransformation(bitmapPool, (Transformation<Bitmap>) this.f3342);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private GenericLoaderFactory m3933() {
        return this.f3347;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static RequestManager m3934(Context context) {
        return RequestManagerRetriever.m25965().m25967(context);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> ModelLoader<T, ParcelFileDescriptor> m3935(Class<T> cls, Context context) {
        return m3940(cls, ParcelFileDescriptor.class, context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Glide m3936(Context context) {
        if (f3336 == null) {
            synchronized (Glide.class) {
                if (f3336 == null) {
                    Context applicationContext = context.getApplicationContext();
                    List<GlideModule> r4 = new ManifestParser(applicationContext).m25989();
                    GlideBuilder glideBuilder = new GlideBuilder(applicationContext);
                    for (GlideModule r3 : r4) {
                        r3.m25987(applicationContext, glideBuilder);
                    }
                    f3336 = glideBuilder.m25239();
                    for (GlideModule r32 : r4) {
                        r32.m25986(applicationContext, f3336);
                    }
                }
            }
        }
        return f3336;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RequestManager m3937(Fragment fragment) {
        return RequestManagerRetriever.m25965().m25970(fragment);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RequestManager m3938(FragmentActivity fragmentActivity) {
        return RequestManagerRetriever.m25965().m25971(fragmentActivity);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> ModelLoader<T, InputStream> m3939(Class<T> cls, Context context) {
        return m3940(cls, InputStream.class, context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T, Y> ModelLoader<T, Y> m3940(Class<T> cls, Class<Y> cls2, Context context) {
        if (cls != null) {
            return m3936(context).m3933().m25630(cls, cls2);
        }
        if (Log.isLoggable("Glide", 3)) {
            Log.d("Glide", "Unable to load null model, setting placeholder only");
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3941(View view) {
        m3942((Target<?>) new ClearTarget(view));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3942(Target<?> target) {
        Util.m26201();
        Request r0 = target.m26142();
        if (r0 != null) {
            r0.m26051();
            target.m26144((Request) null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public GifBitmapWrapperTransformation m3943() {
        return this.f3340;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Handler m3944() {
        return this.f3341;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public DecodeFormat m3945() {
        return this.f3337;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m3946() {
        Util.m26201();
        this.f3346.m25603();
        this.f3348.m25514();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public GifBitmapWrapperTransformation m3947() {
        return this.f3345;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Engine m3948() {
        return this.f3349;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public <T, Z> DataLoadProvider<T, Z> m3949(Class<T> cls, Class<Z> cls2) {
        return this.f3343.m26003(cls, cls2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public FitCenter m3950() {
        return this.f3342;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public CenterCrop m3951() {
        return this.f3344;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapPool m3952() {
        return this.f3348;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public <Z, R> ResourceTranscoder<Z, R> m3953(Class<Z> cls, Class<R> cls2) {
        return this.f3339.m25931(cls, cls2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public <R> Target<R> m3954(ImageView imageView, Class<R> cls) {
        return this.f3338.m26134(imageView, cls);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3955(int i) {
        Util.m26201();
        this.f3346.m25604(i);
        this.f3348.m25515(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T, Y> void m3956(Class<T> cls, Class<Y> cls2, ModelLoaderFactory<T, Y> modelLoaderFactory) {
        ModelLoaderFactory<T, Y> r0 = this.f3347.m25631(cls, cls2, modelLoaderFactory);
        if (r0 != null) {
            r0.m25665();
        }
    }
}
