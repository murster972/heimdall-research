package com.bumptech.glide;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.ParcelFileDescriptor;
import android.widget.ImageView;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.bitmap.FileDescriptorBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.StreamBitmapDecoder;
import com.bumptech.glide.provider.LoadProvider;
import com.bumptech.glide.request.target.Target;
import java.io.InputStream;

public class BitmapRequestBuilder<ModelType, TranscodeType> extends GenericRequestBuilder<ModelType, ImageVideoWrapper, Bitmap, TranscodeType> {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final BitmapPool f19500;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Downsampler f19501 = Downsampler.f19928;

    /* renamed from: ˑ  reason: contains not printable characters */
    private DecodeFormat f19502;

    /* renamed from: ٴ  reason: contains not printable characters */
    private ResourceDecoder<InputStream, Bitmap> f19503;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private ResourceDecoder<ParcelFileDescriptor, Bitmap> f19504;

    BitmapRequestBuilder(LoadProvider<ModelType, ImageVideoWrapper, Bitmap, TranscodeType> loadProvider, Class<TranscodeType> cls, GenericRequestBuilder<ModelType, ?, ?, ?> genericRequestBuilder) {
        super(loadProvider, cls, genericRequestBuilder);
        this.f19500 = genericRequestBuilder.f19537.m3952();
        this.f19502 = genericRequestBuilder.f19537.m3945();
        this.f19503 = new StreamBitmapDecoder(this.f19500, this.f19502);
        this.f19504 = new FileDescriptorBitmapDecoder(this.f19500, this.f19502);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m25147() {
        m25153();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m25148() {
        m25165();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25149() {
        return (BitmapRequestBuilder) super.clone();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25153() {
        return m25175(this.f19537.m3950());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25151() {
        super.m25220();
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25150() {
        super.m25219();
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25165() {
        return m25175(this.f19537.m3951());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25154(int i, int i2) {
        super.m25221(i, i2);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25155(Drawable drawable) {
        super.m25222(drawable);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25156(Encoder<ImageVideoWrapper> encoder) {
        super.m25223(encoder);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25157(Key key) {
        super.m25224(key);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25158(ResourceDecoder<ImageVideoWrapper, Bitmap> resourceDecoder) {
        super.m25225(resourceDecoder);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25159(DiskCacheStrategy diskCacheStrategy) {
        super.m25226(diskCacheStrategy);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25160(ModelType modeltype) {
        super.m25227(modeltype);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25161(boolean z) {
        super.m25228(z);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25162(Transformation<Bitmap>... transformationArr) {
        super.m25229((Transformation<ResourceType>[]) transformationArr);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BitmapRequestBuilder<ModelType, TranscodeType> m25175(BitmapTransformation... bitmapTransformationArr) {
        super.m25229((Transformation<ResourceType>[]) bitmapTransformationArr);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Target<TranscodeType> m25176(ImageView imageView) {
        return super.m25232(imageView);
    }
}
