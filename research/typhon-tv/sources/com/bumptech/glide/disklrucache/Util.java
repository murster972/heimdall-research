package com.bumptech.glide.disklrucache;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

final class Util {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Charset f19607 = Charset.forName("UTF-8");

    /* renamed from: 龘  reason: contains not printable characters */
    static final Charset f19608 = Charset.forName("US-ASCII");

    /* renamed from: 龘  reason: contains not printable characters */
    static void m25297(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m25298(File file) throws IOException {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            throw new IOException("not a readable directory: " + file);
        }
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                m25298(file2);
            }
            if (!file2.delete()) {
                throw new IOException("failed to delete file: " + file2);
            }
        }
    }
}
