package com.bumptech.glide.disklrucache;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

class StrictLineReader implements Closeable {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f19601;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Charset f19602;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f19603;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f19604;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InputStream f19605;

    public StrictLineReader(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw new NullPointerException();
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (!charset.equals(Util.f19608)) {
            throw new IllegalArgumentException("Unsupported encoding");
        } else {
            this.f19605 = inputStream;
            this.f19602 = charset;
            this.f19604 = new byte[i];
        }
    }

    public StrictLineReader(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25293() throws IOException {
        int read = this.f19605.read(this.f19604, 0, this.f19604.length);
        if (read == -1) {
            throw new EOFException();
        }
        this.f19603 = 0;
        this.f19601 = read;
    }

    public void close() throws IOException {
        synchronized (this.f19605) {
            if (this.f19604 != null) {
                this.f19604 = null;
                this.f19605.close();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m25295() {
        return this.f19601 == -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25296() throws IOException {
        int i;
        String byteArrayOutputStream;
        synchronized (this.f19605) {
            if (this.f19604 == null) {
                throw new IOException("LineReader is closed");
            }
            if (this.f19603 >= this.f19601) {
                m25293();
            }
            int i2 = this.f19603;
            while (true) {
                if (i2 == this.f19601) {
                    AnonymousClass1 r2 = new ByteArrayOutputStream((this.f19601 - this.f19603) + 80) {
                        public String toString() {
                            try {
                                return new String(this.buf, 0, (this.count <= 0 || this.buf[this.count + -1] != 13) ? this.count : this.count - 1, StrictLineReader.this.f19602.name());
                            } catch (UnsupportedEncodingException e) {
                                throw new AssertionError(e);
                            }
                        }
                    };
                    loop1:
                    while (true) {
                        r2.write(this.f19604, this.f19603, this.f19601 - this.f19603);
                        this.f19601 = -1;
                        m25293();
                        i = this.f19603;
                        while (true) {
                            if (i != this.f19601) {
                                if (this.f19604[i] == 10) {
                                    break loop1;
                                }
                                i++;
                            }
                        }
                    }
                    if (i != this.f19603) {
                        r2.write(this.f19604, this.f19603, i - this.f19603);
                    }
                    this.f19603 = i + 1;
                    byteArrayOutputStream = r2.toString();
                } else if (this.f19604[i2] == 10) {
                    byteArrayOutputStream = new String(this.f19604, this.f19603, ((i2 == this.f19603 || this.f19604[i2 + -1] != 13) ? i2 : i2 - 1) - this.f19603, this.f19602.name());
                    this.f19603 = i2 + 1;
                } else {
                    i2++;
                }
            }
        }
        return byteArrayOutputStream;
    }
}
