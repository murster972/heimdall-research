package com.bumptech.glide.disklrucache;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;

public final class DiskLruCache implements Closeable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f19569;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f19570;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f19571;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f19572 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Callable<Void> f19573 = new Callable<Void>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Void call() throws Exception {
            synchronized (DiskLruCache.this) {
                if (DiskLruCache.this.f19576 != null) {
                    DiskLruCache.this.m25249();
                    if (DiskLruCache.this.m25251()) {
                        DiskLruCache.this.m25254();
                        int unused = DiskLruCache.this.f19574 = 0;
                    }
                }
            }
            return null;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public int f19574;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f19575 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public Writer f19576;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final LinkedHashMap<String, Entry> f19577 = new LinkedHashMap<>(0, 0.75f, true);

    /* renamed from: 连任  reason: contains not printable characters */
    private final File f19578;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final File f19579;

    /* renamed from: 麤  reason: contains not printable characters */
    private final File f19580;

    /* renamed from: 齉  reason: contains not printable characters */
    private final File f19581;

    /* renamed from: 龘  reason: contains not printable characters */
    final ThreadPoolExecutor f19582 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());

    public final class Editor {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final Entry f19584;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f19585;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final boolean[] f19586;

        private Editor(Entry entry) {
            this.f19584 = entry;
            this.f19586 = entry.f19588 ? null : new boolean[DiskLruCache.this.f19571];
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m25274() throws IOException {
            DiskLruCache.this.m25263(this, false);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m25275() {
            if (!this.f19585) {
                try {
                    m25274();
                } catch (IOException e) {
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public File m25276(int i) throws IOException {
            File r0;
            synchronized (DiskLruCache.this) {
                if (this.f19584.f19589 != this) {
                    throw new IllegalStateException();
                }
                if (!this.f19584.f19588) {
                    this.f19586[i] = true;
                }
                r0 = this.f19584.m25289(i);
                if (!DiskLruCache.this.f19579.exists()) {
                    DiskLruCache.this.f19579.mkdirs();
                }
            }
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25277() throws IOException {
            DiskLruCache.this.m25263(this, true);
            this.f19585 = true;
        }
    }

    private final class Entry {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean f19588;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public Editor f19589;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public long f19590;
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public final long[] f19591;

        /* renamed from: 靐  reason: contains not printable characters */
        File[] f19592;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public final String f19593;

        /* renamed from: 龘  reason: contains not printable characters */
        File[] f19595;

        private Entry(String str) {
            this.f19593 = str;
            this.f19591 = new long[DiskLruCache.this.f19571];
            this.f19595 = new File[DiskLruCache.this.f19571];
            this.f19592 = new File[DiskLruCache.this.f19571];
            StringBuilder append = new StringBuilder(str).append('.');
            int length = append.length();
            for (int i = 0; i < DiskLruCache.this.f19571; i++) {
                append.append(i);
                this.f19595[i] = new File(DiskLruCache.this.f19579, append.toString());
                append.append(".tmp");
                this.f19592[i] = new File(DiskLruCache.this.f19579, append.toString());
                append.setLength(length);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private IOException m25279(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25287(String[] strArr) throws IOException {
            if (strArr.length != DiskLruCache.this.f19571) {
                throw m25279(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.f19591[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e) {
                    throw m25279(strArr);
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public File m25289(int i) {
            return this.f19592[i];
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public File m25290(int i) {
            return this.f19595[i];
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m25291() throws IOException {
            StringBuilder sb = new StringBuilder();
            for (long append : this.f19591) {
                sb.append(' ').append(append);
            }
            return sb.toString();
        }
    }

    public final class Value {

        /* renamed from: 连任  reason: contains not printable characters */
        private final File[] f19596;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f19597;

        /* renamed from: 麤  reason: contains not printable characters */
        private final long[] f19598;

        /* renamed from: 齉  reason: contains not printable characters */
        private final long f19599;

        private Value(String str, long j, File[] fileArr, long[] jArr) {
            this.f19597 = str;
            this.f19599 = j;
            this.f19596 = fileArr;
            this.f19598 = jArr;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public File m25292(int i) {
            return this.f19596[i];
        }
    }

    private DiskLruCache(File file, int i, int i2, long j) {
        this.f19579 = file;
        this.f19569 = i;
        this.f19581 = new File(file, "journal");
        this.f19580 = new File(file, "journal.tmp");
        this.f19578 = new File(file, "journal.bkp");
        this.f19571 = i2;
        this.f19570 = j;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m25248() {
        if (this.f19576 == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m25249() throws IOException {
        while (this.f19575 > this.f19570) {
            m25268((String) this.f19577.entrySet().iterator().next().getKey());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m25251() {
        return this.f19574 >= 2000 && this.f19574 >= this.f19577.size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25252() throws IOException {
        int i;
        StrictLineReader strictLineReader = new StrictLineReader(new FileInputStream(this.f19581), Util.f19608);
        try {
            String r4 = strictLineReader.m25296();
            String r7 = strictLineReader.m25296();
            String r0 = strictLineReader.m25296();
            String r6 = strictLineReader.m25296();
            String r1 = strictLineReader.m25296();
            if (!"libcore.io.DiskLruCache".equals(r4) || !PubnativeRequest.LEGACY_ZONE_ID.equals(r7) || !Integer.toString(this.f19569).equals(r0) || !Integer.toString(this.f19571).equals(r6) || !"".equals(r1)) {
                throw new IOException("unexpected journal header: [" + r4 + ", " + r7 + ", " + r6 + ", " + r1 + "]");
            }
            i = 0;
            while (true) {
                m25256(strictLineReader.m25296());
                i++;
            }
        } catch (EOFException e) {
            this.f19574 = i - this.f19577.size();
            if (strictLineReader.m25295()) {
                m25254();
            } else {
                this.f19576 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f19581, true), Util.f19608));
            }
            Util.m25297((Closeable) strictLineReader);
        } catch (Throwable th) {
            Util.m25297((Closeable) strictLineReader);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized void m25254() throws IOException {
        if (this.f19576 != null) {
            this.f19576.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f19580), Util.f19608));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(PubnativeRequest.LEGACY_ZONE_ID);
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(Integer.toString(this.f19569));
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(Integer.toString(this.f19571));
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(StringUtils.LF);
            for (Entry next : this.f19577.values()) {
                if (next.f19589 != null) {
                    bufferedWriter.write("DIRTY " + next.f19593 + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.f19593 + next.m25291() + 10);
                }
            }
            bufferedWriter.close();
            if (this.f19581.exists()) {
                m25266(this.f19581, this.f19578, true);
            }
            m25266(this.f19580, this.f19581, false);
            this.f19578.delete();
            this.f19576 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f19581, true), Util.f19608));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m25256(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i = indexOf + 1;
        int indexOf2 = str.indexOf(32, i);
        if (indexOf2 == -1) {
            str2 = str.substring(i);
            if (indexOf == "REMOVE".length() && str.startsWith("REMOVE")) {
                this.f19577.remove(str2);
                return;
            }
        } else {
            str2 = str.substring(i, indexOf2);
        }
        Entry entry = this.f19577.get(str2);
        if (entry == null) {
            entry = new Entry(str2);
            this.f19577.put(str2, entry);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(StringUtils.SPACE);
            boolean unused = entry.f19588 = true;
            Editor unused2 = entry.f19589 = null;
            entry.m25287(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            Editor unused3 = entry.f19589 = new Editor(entry);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25257() throws IOException {
        m25265(this.f19580);
        Iterator<Entry> it2 = this.f19577.values().iterator();
        while (it2.hasNext()) {
            Entry next = it2.next();
            if (next.f19589 == null) {
                for (int i = 0; i < this.f19571; i++) {
                    this.f19575 += next.f19591[i];
                }
            } else {
                Editor unused = next.f19589 = null;
                for (int i2 = 0; i2 < this.f19571; i2++) {
                    m25265(next.m25290(i2));
                    m25265(next.m25289(i2));
                }
                it2.remove();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005d, code lost:
        if (com.bumptech.glide.disklrucache.DiskLruCache.Entry.m25284(r1) != null) goto L_0x001d;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.bumptech.glide.disklrucache.DiskLruCache.Editor m25260(java.lang.String r5, long r6) throws java.io.IOException {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            r4.m25248()     // Catch:{ all -> 0x0056 }
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.disklrucache.DiskLruCache$Entry> r2 = r4.f19577     // Catch:{ all -> 0x0056 }
            java.lang.Object r1 = r2.get(r5)     // Catch:{ all -> 0x0056 }
            com.bumptech.glide.disklrucache.DiskLruCache$Entry r1 = (com.bumptech.glide.disklrucache.DiskLruCache.Entry) r1     // Catch:{ all -> 0x0056 }
            r2 = -1
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x001f
            if (r1 == 0) goto L_0x001d
            long r2 = r1.f19590     // Catch:{ all -> 0x0056 }
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x001f
        L_0x001d:
            monitor-exit(r4)
            return r0
        L_0x001f:
            if (r1 != 0) goto L_0x0059
            com.bumptech.glide.disklrucache.DiskLruCache$Entry r1 = new com.bumptech.glide.disklrucache.DiskLruCache$Entry     // Catch:{ all -> 0x0056 }
            r2 = 0
            r1.<init>(r5)     // Catch:{ all -> 0x0056 }
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.disklrucache.DiskLruCache$Entry> r2 = r4.f19577     // Catch:{ all -> 0x0056 }
            r2.put(r5, r1)     // Catch:{ all -> 0x0056 }
        L_0x002c:
            com.bumptech.glide.disklrucache.DiskLruCache$Editor r0 = new com.bumptech.glide.disklrucache.DiskLruCache$Editor     // Catch:{ all -> 0x0056 }
            r2 = 0
            r0.<init>(r1)     // Catch:{ all -> 0x0056 }
            com.bumptech.glide.disklrucache.DiskLruCache.Editor unused = r1.f19589 = r0     // Catch:{ all -> 0x0056 }
            java.io.Writer r2 = r4.f19576     // Catch:{ all -> 0x0056 }
            java.lang.String r3 = "DIRTY"
            r2.append(r3)     // Catch:{ all -> 0x0056 }
            java.io.Writer r2 = r4.f19576     // Catch:{ all -> 0x0056 }
            r3 = 32
            r2.append(r3)     // Catch:{ all -> 0x0056 }
            java.io.Writer r2 = r4.f19576     // Catch:{ all -> 0x0056 }
            r2.append(r5)     // Catch:{ all -> 0x0056 }
            java.io.Writer r2 = r4.f19576     // Catch:{ all -> 0x0056 }
            r3 = 10
            r2.append(r3)     // Catch:{ all -> 0x0056 }
            java.io.Writer r2 = r4.f19576     // Catch:{ all -> 0x0056 }
            r2.flush()     // Catch:{ all -> 0x0056 }
            goto L_0x001d
        L_0x0056:
            r2 = move-exception
            monitor-exit(r4)
            throw r2
        L_0x0059:
            com.bumptech.glide.disklrucache.DiskLruCache$Editor r2 = r1.f19589     // Catch:{ all -> 0x0056 }
            if (r2 == 0) goto L_0x002c
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.disklrucache.DiskLruCache.m25260(java.lang.String, long):com.bumptech.glide.disklrucache.DiskLruCache$Editor");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DiskLruCache m25261(File file, int i, int i2, long j) throws IOException {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    m25266(file2, file3, false);
                }
            }
            DiskLruCache diskLruCache = new DiskLruCache(file, i, i2, j);
            if (diskLruCache.f19581.exists()) {
                try {
                    diskLruCache.m25252();
                    diskLruCache.m25257();
                    DiskLruCache diskLruCache2 = diskLruCache;
                    return diskLruCache;
                } catch (IOException e) {
                    System.out.println("DiskLruCache " + file + " is corrupt: " + e.getMessage() + ", removing");
                    diskLruCache.m25270();
                }
            }
            file.mkdirs();
            DiskLruCache diskLruCache3 = new DiskLruCache(file, i, i2, j);
            diskLruCache3.m25254();
            DiskLruCache diskLruCache4 = diskLruCache3;
            return diskLruCache3;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m25263(Editor editor, boolean z) throws IOException {
        Entry r2 = editor.f19584;
        if (r2.f19589 != editor) {
            throw new IllegalStateException();
        }
        if (z) {
            if (!r2.f19588) {
                int i = 0;
                while (true) {
                    if (i >= this.f19571) {
                        break;
                    } else if (!editor.f19586[i]) {
                        editor.m25274();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                    } else if (!r2.m25289(i).exists()) {
                        editor.m25274();
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.f19571; i2++) {
            File r1 = r2.m25289(i2);
            if (!z) {
                m25265(r1);
            } else if (r1.exists()) {
                File r0 = r2.m25290(i2);
                r1.renameTo(r0);
                long j = r2.f19591[i2];
                long length = r0.length();
                r2.f19591[i2] = length;
                this.f19575 = (this.f19575 - j) + length;
            }
        }
        this.f19574++;
        Editor unused = r2.f19589 = null;
        if (r2.f19588 || z) {
            boolean unused2 = r2.f19588 = true;
            this.f19576.append("CLEAN");
            this.f19576.append(' ');
            this.f19576.append(r2.f19593);
            this.f19576.append(r2.m25291());
            this.f19576.append(10);
            if (z) {
                long j2 = this.f19572;
                this.f19572 = 1 + j2;
                long unused3 = r2.f19590 = j2;
            }
        } else {
            this.f19577.remove(r2.f19593);
            this.f19576.append("REMOVE");
            this.f19576.append(' ');
            this.f19576.append(r2.f19593);
            this.f19576.append(10);
        }
        this.f19576.flush();
        if (this.f19575 > this.f19570 || m25251()) {
            this.f19582.submit(this.f19573);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25265(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25266(File file, File file2, boolean z) throws IOException {
        if (z) {
            m25265(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    public synchronized void close() throws IOException {
        if (this.f19576 != null) {
            Iterator it2 = new ArrayList(this.f19577.values()).iterator();
            while (it2.hasNext()) {
                Entry entry = (Entry) it2.next();
                if (entry.f19589 != null) {
                    entry.f19589.m25274();
                }
            }
            m25249();
            this.f19576.close();
            this.f19576 = null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Editor m25267(String str) throws IOException {
        return m25260(str, -1);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized boolean m25268(String str) throws IOException {
        boolean z;
        m25248();
        Entry entry = this.f19577.get(str);
        if (entry == null || entry.f19589 != null) {
            z = false;
        } else {
            int i = 0;
            while (i < this.f19571) {
                File r1 = entry.m25290(i);
                if (!r1.exists() || r1.delete()) {
                    this.f19575 -= entry.f19591[i];
                    entry.f19591[i] = 0;
                    i++;
                } else {
                    throw new IOException("failed to delete " + r1);
                }
            }
            this.f19574++;
            this.f19576.append("REMOVE");
            this.f19576.append(' ');
            this.f19576.append(str);
            this.f19576.append(10);
            this.f19577.remove(str);
            if (m25251()) {
                this.f19582.submit(this.f19573);
            }
            z = true;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Value m25269(String str) throws IOException {
        Value value = null;
        synchronized (this) {
            m25248();
            Entry entry = this.f19577.get(str);
            if (entry != null) {
                if (entry.f19588) {
                    File[] fileArr = entry.f19595;
                    int length = fileArr.length;
                    int i = 0;
                    while (true) {
                        if (i < length) {
                            if (!fileArr[i].exists()) {
                                break;
                            }
                            i++;
                        } else {
                            this.f19574++;
                            this.f19576.append("READ");
                            this.f19576.append(' ');
                            this.f19576.append(str);
                            this.f19576.append(10);
                            if (m25251()) {
                                this.f19582.submit(this.f19573);
                            }
                            value = new Value(str, entry.f19590, entry.f19595, entry.f19591);
                        }
                    }
                }
            }
        }
        return value;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25270() throws IOException {
        close();
        Util.m25298(this.f19579);
    }
}
