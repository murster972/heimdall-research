package com.bumptech.glide;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import java.io.InputStream;

public class BitmapTypeRequest<ModelType> extends BitmapRequestBuilder<ModelType, Bitmap> {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ModelLoader<ModelType, InputStream> f19505;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ModelLoader<ModelType, ParcelFileDescriptor> f19506;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Glide f19507;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final RequestManager.OptionsApplier f19508;

    BitmapTypeRequest(GenericRequestBuilder<ModelType, ?, ?, ?> genericRequestBuilder, ModelLoader<ModelType, InputStream> modelLoader, ModelLoader<ModelType, ParcelFileDescriptor> modelLoader2, RequestManager.OptionsApplier optionsApplier) {
        super(m25177(genericRequestBuilder.f19537, modelLoader, modelLoader2, Bitmap.class, (ResourceTranscoder) null), Bitmap.class, genericRequestBuilder);
        this.f19505 = modelLoader;
        this.f19506 = modelLoader2;
        this.f19507 = genericRequestBuilder.f19537;
        this.f19508 = optionsApplier;
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [java.lang.Class, java.lang.Class<R>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <A, R> com.bumptech.glide.provider.FixedLoadProvider<A, com.bumptech.glide.load.model.ImageVideoWrapper, android.graphics.Bitmap, R> m25177(com.bumptech.glide.Glide r4, com.bumptech.glide.load.model.ModelLoader<A, java.io.InputStream> r5, com.bumptech.glide.load.model.ModelLoader<A, android.os.ParcelFileDescriptor> r6, java.lang.Class<R> r7, com.bumptech.glide.load.resource.transcode.ResourceTranscoder<android.graphics.Bitmap, R> r8) {
        /*
            if (r5 != 0) goto L_0x0006
            if (r6 != 0) goto L_0x0006
            r2 = 0
        L_0x0005:
            return r2
        L_0x0006:
            if (r8 != 0) goto L_0x000e
            java.lang.Class<android.graphics.Bitmap> r2 = android.graphics.Bitmap.class
            com.bumptech.glide.load.resource.transcode.ResourceTranscoder r8 = r4.m3953(r2, r7)
        L_0x000e:
            java.lang.Class<com.bumptech.glide.load.model.ImageVideoWrapper> r2 = com.bumptech.glide.load.model.ImageVideoWrapper.class
            java.lang.Class<android.graphics.Bitmap> r3 = android.graphics.Bitmap.class
            com.bumptech.glide.provider.DataLoadProvider r0 = r4.m3949(r2, r3)
            com.bumptech.glide.load.model.ImageVideoModelLoader r1 = new com.bumptech.glide.load.model.ImageVideoModelLoader
            r1.<init>(r5, r6)
            com.bumptech.glide.provider.FixedLoadProvider r2 = new com.bumptech.glide.provider.FixedLoadProvider
            r2.<init>(r1, r8, r0)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.BitmapTypeRequest.m25177(com.bumptech.glide.Glide, com.bumptech.glide.load.model.ModelLoader, com.bumptech.glide.load.model.ModelLoader, java.lang.Class, com.bumptech.glide.load.resource.transcode.ResourceTranscoder):com.bumptech.glide.provider.FixedLoadProvider");
    }
}
