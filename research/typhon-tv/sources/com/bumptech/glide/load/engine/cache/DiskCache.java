package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.Key;
import java.io.File;

public interface DiskCache {

    public interface Factory {
        /* renamed from: 龘  reason: contains not printable characters */
        DiskCache m25576();
    }

    public interface Writer {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m25577(File file);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    void m25573(Key key);

    /* renamed from: 龘  reason: contains not printable characters */
    File m25574(Key key);

    /* renamed from: 龘  reason: contains not printable characters */
    void m25575(Key key, Writer writer);
}
