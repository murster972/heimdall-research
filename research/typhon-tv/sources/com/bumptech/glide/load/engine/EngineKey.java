package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

class EngineKey implements Key {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Transformation f19774;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ResourceEncoder f19775;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ResourceTranscoder f19776;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Key f19777;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f19778;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Encoder f19779;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Key f19780;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f19781;

    /* renamed from: 连任  reason: contains not printable characters */
    private final ResourceDecoder f19782;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f19783;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ResourceDecoder f19784;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f19785;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f19786;

    public EngineKey(String str, Key key, int i, int i2, ResourceDecoder resourceDecoder, ResourceDecoder resourceDecoder2, Transformation transformation, ResourceEncoder resourceEncoder, ResourceTranscoder resourceTranscoder, Encoder encoder) {
        this.f19786 = str;
        this.f19780 = key;
        this.f19783 = i;
        this.f19785 = i2;
        this.f19784 = resourceDecoder;
        this.f19782 = resourceDecoder2;
        this.f19774 = transformation;
        this.f19775 = resourceEncoder;
        this.f19776 = resourceTranscoder;
        this.f19779 = encoder;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        EngineKey engineKey = (EngineKey) obj;
        if (!this.f19786.equals(engineKey.f19786) || !this.f19780.equals(engineKey.f19780) || this.f19785 != engineKey.f19785 || this.f19783 != engineKey.f19783) {
            return false;
        }
        if ((this.f19774 == null) ^ (engineKey.f19774 == null)) {
            return false;
        }
        if (this.f19774 != null && !this.f19774.m25375().equals(engineKey.f19774.m25375())) {
            return false;
        }
        if ((this.f19782 == null) ^ (engineKey.f19782 == null)) {
            return false;
        }
        if (this.f19782 != null && !this.f19782.m25373().equals(engineKey.f19782.m25373())) {
            return false;
        }
        if ((this.f19784 == null) ^ (engineKey.f19784 == null)) {
            return false;
        }
        if (this.f19784 != null && !this.f19784.m25373().equals(engineKey.f19784.m25373())) {
            return false;
        }
        if ((this.f19775 == null) ^ (engineKey.f19775 == null)) {
            return false;
        }
        if (this.f19775 != null && !this.f19775.m25369().equals(engineKey.f19775.m25369())) {
            return false;
        }
        if ((this.f19776 == null) ^ (engineKey.f19776 == null)) {
            return false;
        }
        if (this.f19776 != null && !this.f19776.m25930().equals(engineKey.f19776.m25930())) {
            return false;
        }
        if (!((this.f19779 == null) ^ (engineKey.f19779 == null))) {
            return this.f19779 == null || this.f19779.m25369().equals(engineKey.f19779.m25369());
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        if (this.f19778 == 0) {
            this.f19778 = this.f19786.hashCode();
            this.f19778 = (this.f19778 * 31) + this.f19780.hashCode();
            this.f19778 = (this.f19778 * 31) + this.f19783;
            this.f19778 = (this.f19778 * 31) + this.f19785;
            this.f19778 = (this.f19784 != null ? this.f19784.m25373().hashCode() : 0) + (this.f19778 * 31);
            this.f19778 = (this.f19782 != null ? this.f19782.m25373().hashCode() : 0) + (this.f19778 * 31);
            this.f19778 = (this.f19774 != null ? this.f19774.m25375().hashCode() : 0) + (this.f19778 * 31);
            this.f19778 = (this.f19775 != null ? this.f19775.m25369().hashCode() : 0) + (this.f19778 * 31);
            this.f19778 = (this.f19776 != null ? this.f19776.m25930().hashCode() : 0) + (this.f19778 * 31);
            int i2 = this.f19778 * 31;
            if (this.f19779 != null) {
                i = this.f19779.m25369().hashCode();
            }
            this.f19778 = i2 + i;
        }
        return this.f19778;
    }

    public String toString() {
        if (this.f19781 == null) {
            this.f19781 = "EngineKey{" + this.f19786 + '+' + this.f19780 + "+[" + this.f19783 + 'x' + this.f19785 + "]+" + '\'' + (this.f19784 != null ? this.f19784.m25373() : "") + '\'' + '+' + '\'' + (this.f19782 != null ? this.f19782.m25373() : "") + '\'' + '+' + '\'' + (this.f19774 != null ? this.f19774.m25375() : "") + '\'' + '+' + '\'' + (this.f19775 != null ? this.f19775.m25369() : "") + '\'' + '+' + '\'' + (this.f19776 != null ? this.f19776.m25930() : "") + '\'' + '+' + '\'' + (this.f19779 != null ? this.f19779.m25369() : "") + '\'' + '}';
        }
        return this.f19781;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Key m25470() {
        if (this.f19777 == null) {
            this.f19777 = new OriginalKey(this.f19786, this.f19780);
        }
        return this.f19777;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25471(MessageDigest messageDigest) throws UnsupportedEncodingException {
        byte[] array = ByteBuffer.allocate(8).putInt(this.f19783).putInt(this.f19785).array();
        this.f19780.m25371(messageDigest);
        messageDigest.update(this.f19786.getBytes("UTF-8"));
        messageDigest.update(array);
        messageDigest.update((this.f19784 != null ? this.f19784.m25373() : "").getBytes("UTF-8"));
        messageDigest.update((this.f19782 != null ? this.f19782.m25373() : "").getBytes("UTF-8"));
        messageDigest.update((this.f19774 != null ? this.f19774.m25375() : "").getBytes("UTF-8"));
        messageDigest.update((this.f19775 != null ? this.f19775.m25369() : "").getBytes("UTF-8"));
        messageDigest.update((this.f19779 != null ? this.f19779.m25369() : "").getBytes("UTF-8"));
    }
}
