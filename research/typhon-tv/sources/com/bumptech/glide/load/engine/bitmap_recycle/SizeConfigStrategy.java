package com.bumptech.glide.load.engine.bitmap_recycle;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import com.bumptech.glide.util.Util;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@TargetApi(19)
public class SizeConfigStrategy implements LruPoolStrategy {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Bitmap.Config[] f19829 = {Bitmap.Config.RGB_565};

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Bitmap.Config[] f19830 = {Bitmap.Config.ALPHA_8};

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Bitmap.Config[] f19831 = {Bitmap.Config.ARGB_4444};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Bitmap.Config[] f19832 = {Bitmap.Config.ARGB_8888, null};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final GroupedLinkedMap<Key, Bitmap> f19833 = new GroupedLinkedMap<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Map<Bitmap.Config, NavigableMap<Integer, Integer>> f19834 = new HashMap();

    /* renamed from: 连任  reason: contains not printable characters */
    private final KeyPool f19835 = new KeyPool();

    /* renamed from: com.bumptech.glide.load.engine.bitmap_recycle.SizeConfigStrategy$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f19836 = new int[Bitmap.Config.values().length];

        static {
            try {
                f19836[Bitmap.Config.ARGB_8888.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f19836[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f19836[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f19836[Bitmap.Config.ALPHA_8.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    static final class Key implements Poolable {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public int f19837;

        /* renamed from: 齉  reason: contains not printable characters */
        private Bitmap.Config f19838;

        /* renamed from: 龘  reason: contains not printable characters */
        private final KeyPool f19839;

        public Key(KeyPool keyPool) {
            this.f19839 = keyPool;
        }

        /* JADX WARNING: Removed duplicated region for block: B:8:0x0016 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r5) {
            /*
                r4 = this;
                r1 = 0
                boolean r2 = r5 instanceof com.bumptech.glide.load.engine.bitmap_recycle.SizeConfigStrategy.Key
                if (r2 == 0) goto L_0x0017
                r0 = r5
                com.bumptech.glide.load.engine.bitmap_recycle.SizeConfigStrategy$Key r0 = (com.bumptech.glide.load.engine.bitmap_recycle.SizeConfigStrategy.Key) r0
                int r2 = r4.f19837
                int r3 = r0.f19837
                if (r2 != r3) goto L_0x0017
                android.graphics.Bitmap$Config r2 = r4.f19838
                if (r2 != 0) goto L_0x0018
                android.graphics.Bitmap$Config r2 = r0.f19838
                if (r2 != 0) goto L_0x0017
            L_0x0016:
                r1 = 1
            L_0x0017:
                return r1
            L_0x0018:
                android.graphics.Bitmap$Config r2 = r4.f19838
                android.graphics.Bitmap$Config r3 = r0.f19838
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x0017
                goto L_0x0016
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.bitmap_recycle.SizeConfigStrategy.Key.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return (this.f19837 * 31) + (this.f19838 != null ? this.f19838.hashCode() : 0);
        }

        public String toString() {
            return SizeConfigStrategy.m25555(this.f19837, this.f19838);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25568() {
            this.f19839.m25511(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25569(int i, Bitmap.Config config) {
            this.f19837 = i;
            this.f19838 = config;
        }
    }

    static class KeyPool extends BaseKeyPool<Key> {
        KeyPool() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Key m25570() {
            return new Key(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Key m25572(int i, Bitmap.Config config) {
            Key key = (Key) m25510();
            key.m25569(i, config);
            return key;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m25555(int i, Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Bitmap.Config[] m25556(Bitmap.Config config) {
        switch (AnonymousClass1.f19836[config.ordinal()]) {
            case 1:
                return f19832;
            case 2:
                return f19829;
            case 3:
                return f19831;
            case 4:
                return f19830;
            default:
                return new Bitmap.Config[]{config};
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Key m25557(Key key, int i, Bitmap.Config config) {
        Key key2 = key;
        Bitmap.Config[] r0 = m25556(config);
        int length = r0.length;
        int i2 = 0;
        while (i2 < length) {
            Bitmap.Config config2 = r0[i2];
            Integer ceilingKey = m25559(config2).ceilingKey(Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i2++;
            } else {
                if (ceilingKey.intValue() == i) {
                    if (config2 == null) {
                        if (config == null) {
                            return key2;
                        }
                    } else if (config2.equals(config)) {
                        return key2;
                    }
                }
                this.f19835.m25511(key);
                return this.f19835.m25572(ceilingKey.intValue(), config2);
            }
        }
        return key2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private NavigableMap<Integer, Integer> m25559(Bitmap.Config config) {
        NavigableMap<Integer, Integer> navigableMap = this.f19834.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.f19834.put(config, treeMap);
        return treeMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25560(Integer num, Bitmap.Config config) {
        NavigableMap<Integer, Integer> r1 = m25559(config);
        Integer num2 = (Integer) r1.get(num);
        if (num2.intValue() == 1) {
            r1.remove(num);
        } else {
            r1.put(num, Integer.valueOf(num2.intValue() - 1));
        }
    }

    public String toString() {
        StringBuilder append = new StringBuilder().append("SizeConfigStrategy{groupedMap=").append(this.f19833).append(", sortedSizes=(");
        for (Map.Entry next : this.f19834.entrySet()) {
            append.append(next.getKey()).append('[').append(next.getValue()).append("], ");
        }
        if (!this.f19834.isEmpty()) {
            append.replace(append.length() - 2, append.length(), "");
        }
        return append.append(")}").toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25561(int i, int i2, Bitmap.Config config) {
        return m25555(Util.m26194(i, i2, config), config);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25562(Bitmap bitmap) {
        return m25555(Util.m26196(bitmap), bitmap.getConfig());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25563(Bitmap bitmap) {
        return Util.m26196(bitmap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25564() {
        Bitmap r0 = this.f19833.m25526();
        if (r0 != null) {
            m25560(Integer.valueOf(Util.m26196(r0)), r0.getConfig());
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25565(int i, int i2, Bitmap.Config config) {
        int r2 = Util.m26194(i, i2, config);
        Bitmap r1 = this.f19833.m25527(m25557(this.f19835.m25572(r2, config), r2, config));
        if (r1 != null) {
            m25560(Integer.valueOf(Util.m26196(r1)), r1.getConfig());
            r1.reconfigure(i, i2, r1.getConfig() != null ? r1.getConfig() : Bitmap.Config.ARGB_8888);
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25566(Bitmap bitmap) {
        Key r1 = this.f19835.m25572(Util.m26196(bitmap), bitmap.getConfig());
        this.f19833.m25528(r1, bitmap);
        NavigableMap<Integer, Integer> r3 = m25559(bitmap.getConfig());
        Integer num = (Integer) r3.get(Integer.valueOf(r1.f19837));
        r3.put(Integer.valueOf(r1.f19837), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }
}
