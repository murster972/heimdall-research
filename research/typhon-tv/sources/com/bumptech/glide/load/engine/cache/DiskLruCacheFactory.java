package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.engine.cache.DiskCache;
import java.io.File;

public class DiskLruCacheFactory implements DiskCache.Factory {

    /* renamed from: 靐  reason: contains not printable characters */
    private final CacheDirectoryGetter f19845;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f19846;

    public interface CacheDirectoryGetter {
        /* renamed from: 龘  reason: contains not printable characters */
        File m25586();
    }

    public DiskLruCacheFactory(CacheDirectoryGetter cacheDirectoryGetter, int i) {
        this.f19846 = i;
        this.f19845 = cacheDirectoryGetter;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DiskCache m25585() {
        File r0 = this.f19845.m25586();
        if (r0 == null) {
            return null;
        }
        if (r0.mkdirs() || (r0.exists() && r0.isDirectory())) {
            return DiskLruCacheWrapper.m25588(r0, this.f19846);
        }
        return null;
    }
}
