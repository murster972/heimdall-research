package com.bumptech.glide.load.engine.cache;

import android.util.Log;
import com.bumptech.glide.disklrucache.DiskLruCache;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.cache.DiskCache;
import java.io.File;
import java.io.IOException;

public class DiskLruCacheWrapper implements DiskCache {

    /* renamed from: 龘  reason: contains not printable characters */
    private static DiskLruCacheWrapper f19847 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    private DiskLruCache f19848;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f19849;

    /* renamed from: 靐  reason: contains not printable characters */
    private final DiskCacheWriteLocker f19850 = new DiskCacheWriteLocker();

    /* renamed from: 麤  reason: contains not printable characters */
    private final File f19851;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SafeKeyGenerator f19852;

    protected DiskLruCacheWrapper(File file, int i) {
        this.f19851 = file;
        this.f19849 = i;
        this.f19852 = new SafeKeyGenerator();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized DiskLruCache m25587() throws IOException {
        if (this.f19848 == null) {
            this.f19848 = DiskLruCache.m25261(this.f19851, 1, 1, (long) this.f19849);
        }
        return this.f19848;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized DiskCache m25588(File file, int i) {
        DiskLruCacheWrapper diskLruCacheWrapper;
        synchronized (DiskLruCacheWrapper.class) {
            if (f19847 == null) {
                f19847 = new DiskLruCacheWrapper(file, i);
            }
            diskLruCacheWrapper = f19847;
        }
        return diskLruCacheWrapper;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25589(Key key) {
        try {
            m25587().m25268(this.f19852.m25616(key));
        } catch (IOException e) {
            if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                Log.w("DiskLruCacheWrapper", "Unable to delete from disk cache", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public File m25590(Key key) {
        try {
            DiskLruCache.Value r3 = m25587().m25269(this.f19852.m25616(key));
            if (r3 != null) {
                return r3.m25292(0);
            }
            return null;
        } catch (IOException e) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25591(Key key, DiskCache.Writer writer) {
        DiskLruCache.Editor r1;
        String r3 = this.f19852.m25616(key);
        this.f19850.m25582(key);
        try {
            r1 = m25587().m25267(r3);
            if (r1 != null) {
                if (writer.m25577(r1.m25276(0))) {
                    r1.m25277();
                }
                r1.m25275();
            }
            this.f19850.m25581(key);
        } catch (IOException e) {
            try {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e);
                }
            } finally {
                this.f19850.m25581(key);
            }
        } catch (Throwable th) {
            r1.m25275();
            throw th;
        }
    }
}
