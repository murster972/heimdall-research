package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.executor.Prioritized;
import com.bumptech.glide.request.ResourceCallback;

class EngineRunnable implements Prioritized, Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile boolean f19793;

    /* renamed from: 靐  reason: contains not printable characters */
    private final EngineRunnableManager f19794;

    /* renamed from: 麤  reason: contains not printable characters */
    private Stage f19795 = Stage.CACHE;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DecodeJob<?, ?, ?> f19796;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Priority f19797;

    interface EngineRunnableManager extends ResourceCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m25489(EngineRunnable engineRunnable);
    }

    private enum Stage {
        CACHE,
        SOURCE
    }

    public EngineRunnable(EngineRunnableManager engineRunnableManager, DecodeJob<?, ?, ?> decodeJob, Priority priority) {
        this.f19794 = engineRunnableManager;
        this.f19796 = decodeJob;
        this.f19797 = priority;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Resource<?> m25481() throws Exception {
        return this.f19796.m25434();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private Resource<?> m25482() throws Exception {
        Resource<?> resource = null;
        try {
            resource = this.f19796.m25435();
        } catch (Exception e) {
            if (Log.isLoggable("EngineRunnable", 3)) {
                Log.d("EngineRunnable", "Exception decoding result from cache: " + e);
            }
        }
        return resource == null ? this.f19796.m25432() : resource;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Resource<?> m25483() throws Exception {
        return m25484() ? m25482() : m25481();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m25484() {
        return this.f19795 == Stage.CACHE;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25485(Resource resource) {
        this.f19794.m26073((Resource<?>) resource);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25486(Exception exc) {
        if (m25484()) {
            this.f19795 = Stage.SOURCE;
            this.f19794.m25489(this);
            return;
        }
        this.f19794.m26074(exc);
    }

    public void run() {
        if (!this.f19793) {
            Exception exc = null;
            Resource<?> resource = null;
            try {
                resource = m25483();
            } catch (Exception e) {
                if (Log.isLoggable("EngineRunnable", 2)) {
                    Log.v("EngineRunnable", "Exception decoding", e);
                }
                exc = e;
            }
            if (this.f19793) {
                if (resource != null) {
                    resource.m25492();
                }
            } else if (resource == null) {
                m25486(exc);
            } else {
                m25485((Resource) resource);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m25487() {
        return this.f19797.ordinal();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25488() {
        this.f19793 = true;
        this.f19796.m25433();
    }
}
