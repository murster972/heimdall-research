package com.bumptech.glide.load.engine.bitmap_recycle;

import android.graphics.Bitmap;
import com.bumptech.glide.util.Util;

class AttributeStrategy implements LruPoolStrategy {

    /* renamed from: 靐  reason: contains not printable characters */
    private final GroupedLinkedMap<Key, Bitmap> f19805 = new GroupedLinkedMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final KeyPool f19806 = new KeyPool();

    static class Key implements Poolable {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f19807;

        /* renamed from: 麤  reason: contains not printable characters */
        private Bitmap.Config f19808;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f19809;

        /* renamed from: 龘  reason: contains not printable characters */
        private final KeyPool f19810;

        public Key(KeyPool keyPool) {
            this.f19810 = keyPool;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Key)) {
                return false;
            }
            Key key = (Key) obj;
            return this.f19807 == key.f19807 && this.f19809 == key.f19809 && this.f19808 == key.f19808;
        }

        public int hashCode() {
            return (((this.f19807 * 31) + this.f19809) * 31) + (this.f19808 != null ? this.f19808.hashCode() : 0);
        }

        public String toString() {
            return AttributeStrategy.m25495(this.f19807, this.f19809, this.f19808);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25504() {
            this.f19810.m25511(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25505(int i, int i2, Bitmap.Config config) {
            this.f19807 = i;
            this.f19809 = i2;
            this.f19808 = config;
        }
    }

    static class KeyPool extends BaseKeyPool<Key> {
        KeyPool() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Key m25506() {
            return new Key(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Key m25508(int i, int i2, Bitmap.Config config) {
            Key key = (Key) m25510();
            key.m25505(i, i2, config);
            return key;
        }
    }

    AttributeStrategy() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static String m25495(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static String m25496(Bitmap bitmap) {
        return m25495(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    public String toString() {
        return "AttributeStrategy:\n  " + this.f19805;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25498(int i, int i2, Bitmap.Config config) {
        return m25495(i, i2, config);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25499(Bitmap bitmap) {
        return m25496(bitmap);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25500(Bitmap bitmap) {
        return Util.m26196(bitmap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25501() {
        return this.f19805.m25526();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25502(int i, int i2, Bitmap.Config config) {
        return this.f19805.m25527(this.f19806.m25508(i, i2, config));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25503(Bitmap bitmap) {
        this.f19805.m25528(this.f19806.m25508(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }
}
