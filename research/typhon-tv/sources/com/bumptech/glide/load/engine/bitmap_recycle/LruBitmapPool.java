package com.bumptech.glide.load.engine.bitmap_recycle;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class LruBitmapPool implements BitmapPool {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Bitmap.Config f19818 = Bitmap.Config.ARGB_8888;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f19819;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f19820;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f19821;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f19822;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f19823;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f19824;

    /* renamed from: 连任  reason: contains not printable characters */
    private final BitmapTracker f19825;

    /* renamed from: 靐  reason: contains not printable characters */
    private final LruPoolStrategy f19826;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f19827;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Set<Bitmap.Config> f19828;

    private interface BitmapTracker {
        /* renamed from: 靐  reason: contains not printable characters */
        void m25544(Bitmap bitmap);

        /* renamed from: 龘  reason: contains not printable characters */
        void m25545(Bitmap bitmap);
    }

    private static class NullBitmapTracker implements BitmapTracker {
        private NullBitmapTracker() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m25546(Bitmap bitmap) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25547(Bitmap bitmap) {
        }
    }

    public LruBitmapPool(int i) {
        this(i, m25534(), m25533());
    }

    LruBitmapPool(int i, LruPoolStrategy lruPoolStrategy, Set<Bitmap.Config> set) {
        this.f19827 = i;
        this.f19819 = i;
        this.f19826 = lruPoolStrategy;
        this.f19828 = set;
        this.f19825 = new NullBitmapTracker();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Set<Bitmap.Config> m25533() {
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList(Bitmap.Config.values()));
        if (Build.VERSION.SDK_INT >= 19) {
            hashSet.add((Object) null);
        }
        return Collections.unmodifiableSet(hashSet);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static LruPoolStrategy m25534() {
        return Build.VERSION.SDK_INT >= 19 ? new SizeConfigStrategy() : new AttributeStrategy();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25535() {
        m25536(this.f19819);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private synchronized void m25536(int i) {
        while (true) {
            if (this.f19820 <= i) {
                break;
            }
            Bitmap r0 = this.f19826.m25551();
            if (r0 == null) {
                if (Log.isLoggable("LruBitmapPool", 5)) {
                    Log.w("LruBitmapPool", "Size mismatch, resetting");
                    m25537();
                }
                this.f19820 = 0;
            } else {
                this.f19825.m25544(r0);
                this.f19820 -= this.f19826.m25550(r0);
                r0.recycle();
                this.f19824++;
                if (Log.isLoggable("LruBitmapPool", 3)) {
                    Log.d("LruBitmapPool", "Evicting bitmap=" + this.f19826.m25549(r0));
                }
                m25538();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m25537() {
        Log.v("LruBitmapPool", "Hits=" + this.f19821 + ", misses=" + this.f19822 + ", puts=" + this.f19823 + ", evictions=" + this.f19824 + ", currentSize=" + this.f19820 + ", maxSize=" + this.f19819 + "\nStrategy=" + this.f19826);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25538() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            m25537();
        }
    }

    @TargetApi(12)
    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized Bitmap m25539(int i, int i2, Bitmap.Config config) {
        Bitmap r0;
        r0 = this.f19826.m25552(i, i2, config != null ? config : f19818);
        if (r0 == null) {
            if (Log.isLoggable("LruBitmapPool", 3)) {
                Log.d("LruBitmapPool", "Missing bitmap=" + this.f19826.m25548(i, i2, config));
            }
            this.f19822++;
        } else {
            this.f19821++;
            this.f19820 -= this.f19826.m25550(r0);
            this.f19825.m25544(r0);
            if (Build.VERSION.SDK_INT >= 12) {
                r0.setHasAlpha(true);
            }
        }
        if (Log.isLoggable("LruBitmapPool", 2)) {
            Log.v("LruBitmapPool", "Get bitmap=" + this.f19826.m25548(i, i2, config));
        }
        m25538();
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Bitmap m25540(int i, int i2, Bitmap.Config config) {
        Bitmap r0;
        r0 = m25539(i, i2, config);
        if (r0 != null) {
            r0.eraseColor(0);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25541() {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "clearMemory");
        }
        m25536(0);
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25542(int i) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "trimMemory, level=" + i);
        }
        if (i >= 60) {
            m25541();
        } else if (i >= 40) {
            m25536(this.f19819 / 2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized boolean m25543(Bitmap bitmap) {
        boolean z;
        if (bitmap == null) {
            throw new NullPointerException("Bitmap must not be null");
        } else if (!bitmap.isMutable() || this.f19826.m25550(bitmap) > this.f19819 || !this.f19828.contains(bitmap.getConfig())) {
            if (Log.isLoggable("LruBitmapPool", 2)) {
                Log.v("LruBitmapPool", "Reject bitmap from pool, bitmap: " + this.f19826.m25549(bitmap) + ", is mutable: " + bitmap.isMutable() + ", is allowed config: " + this.f19828.contains(bitmap.getConfig()));
            }
            z = false;
        } else {
            int r0 = this.f19826.m25550(bitmap);
            this.f19826.m25553(bitmap);
            this.f19825.m25545(bitmap);
            this.f19823++;
            this.f19820 += r0;
            if (Log.isLoggable("LruBitmapPool", 2)) {
                Log.v("LruBitmapPool", "Put bitmap in pool=" + this.f19826.m25549(bitmap));
            }
            m25538();
            m25535();
            z = true;
        }
        return z;
    }
}
