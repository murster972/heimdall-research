package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.provider.DataLoadProvider;
import com.bumptech.glide.util.LogTime;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class DecodeJob<A, T, Z> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final FileOpener f19721 = new FileOpener();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final DataLoadProvider<A, T> f19722;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Transformation<T> f19723;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ResourceTranscoder<T, Z> f19724;

    /* renamed from: ʾ  reason: contains not printable characters */
    private volatile boolean f19725;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final FileOpener f19726;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final DiskCacheProvider f19727;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final DiskCacheStrategy f19728;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Priority f19729;

    /* renamed from: 连任  reason: contains not printable characters */
    private final DataFetcher<A> f19730;

    /* renamed from: 靐  reason: contains not printable characters */
    private final EngineKey f19731;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f19732;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f19733;

    interface DiskCacheProvider {
        /* renamed from: 龘  reason: contains not printable characters */
        DiskCache m25436();
    }

    static class FileOpener {
        FileOpener() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputStream m25437(File file) throws FileNotFoundException {
            return new BufferedOutputStream(new FileOutputStream(file));
        }
    }

    class SourceWriter<DataType> implements DiskCache.Writer {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Encoder<DataType> f19734;

        /* renamed from: 齉  reason: contains not printable characters */
        private final DataType f19735;

        public SourceWriter(Encoder<DataType> encoder, DataType datatype) {
            this.f19734 = encoder;
            this.f19735 = datatype;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m25438(File file) {
            boolean z = false;
            OutputStream outputStream = null;
            try {
                outputStream = DecodeJob.this.f19726.m25437(file);
                z = this.f19734.m25370(this.f19735, outputStream);
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                    }
                }
            } catch (FileNotFoundException e2) {
                if (Log.isLoggable("DecodeJob", 3)) {
                    Log.d("DecodeJob", "Failed to find file to write to disk cache", e2);
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (Throwable th) {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e4) {
                    }
                }
                throw th;
            }
            return z;
        }
    }

    public DecodeJob(EngineKey engineKey, int i, int i2, DataFetcher<A> dataFetcher, DataLoadProvider<A, T> dataLoadProvider, Transformation<T> transformation, ResourceTranscoder<T, Z> resourceTranscoder, DiskCacheProvider diskCacheProvider, DiskCacheStrategy diskCacheStrategy, Priority priority) {
        this(engineKey, i, i2, dataFetcher, dataLoadProvider, transformation, resourceTranscoder, diskCacheProvider, diskCacheStrategy, priority, f19721);
    }

    DecodeJob(EngineKey engineKey, int i, int i2, DataFetcher<A> dataFetcher, DataLoadProvider<A, T> dataLoadProvider, Transformation<T> transformation, ResourceTranscoder<T, Z> resourceTranscoder, DiskCacheProvider diskCacheProvider, DiskCacheStrategy diskCacheStrategy, Priority priority, FileOpener fileOpener) {
        this.f19731 = engineKey;
        this.f19733 = i;
        this.f19732 = i2;
        this.f19730 = dataFetcher;
        this.f19722 = dataLoadProvider;
        this.f19723 = transformation;
        this.f19724 = resourceTranscoder;
        this.f19727 = diskCacheProvider;
        this.f19728 = diskCacheStrategy;
        this.f19729 = priority;
        this.f19726 = fileOpener;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 连任  reason: contains not printable characters */
    private Resource<T> m25422() throws Exception {
        try {
            long r4 = LogTime.m26177();
            A r0 = this.f19730.m25389(this.f19729);
            if (Log.isLoggable("DecodeJob", 2)) {
                m25431("Fetched data", r4);
            }
            if (this.f19725) {
                this.f19730.m25390();
                return null;
            }
            Resource<T> r1 = m25430(r0);
            this.f19730.m25390();
            Resource<T> resource = r1;
            return r1;
        } catch (Throwable th) {
            this.f19730.m25390();
            throw th;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Resource<T> m25423(A a) throws IOException {
        long r2 = LogTime.m26177();
        this.f19727.m25436().m25575(this.f19731.m25470(), new SourceWriter(this.f19722.m26001(), a));
        if (Log.isLoggable("DecodeJob", 2)) {
            m25431("Wrote source to cache", r2);
        }
        long r22 = LogTime.m26177();
        Resource<T> r0 = m25428(this.f19731.m25470());
        if (Log.isLoggable("DecodeJob", 2) && r0 != null) {
            m25431("Decoded source from cache", r22);
        }
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25424(Resource<T> resource) {
        if (resource != null && this.f19728.cacheResult()) {
            long r0 = LogTime.m26177();
            this.f19727.m25436().m25575(this.f19731, new SourceWriter(this.f19722.m26000(), resource));
            if (Log.isLoggable("DecodeJob", 2)) {
                m25431("Wrote transformed from source to cache", r0);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Resource<Z> m25425(Resource<T> resource) {
        if (resource == null) {
            return null;
        }
        return this.f19724.m25929(resource);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Resource<T> m25426(Resource<T> resource) {
        if (resource == null) {
            return null;
        }
        Resource<T> r0 = this.f19723.m25374(resource, this.f19733, this.f19732);
        if (resource.equals(r0)) {
            return r0;
        }
        resource.m25492();
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Resource<T> m25428(Key key) throws IOException {
        File r0 = this.f19727.m25436().m25574(key);
        if (r0 == null) {
            return null;
        }
        boolean z = false;
        try {
            z = this.f19722.m26002().m25372(r0, this.f19733, this.f19732);
        } finally {
            if (!z) {
                this.f19727.m25436().m25573(key);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Resource<Z> m25429(Resource<T> resource) {
        long r2 = LogTime.m26177();
        Resource<T> r1 = m25426(resource);
        if (Log.isLoggable("DecodeJob", 2)) {
            m25431("Transformed resource from source", r2);
        }
        m25424(r1);
        long r22 = LogTime.m26177();
        Resource<Z> r0 = m25425(r1);
        if (Log.isLoggable("DecodeJob", 2)) {
            m25431("Transcoded transformed from source", r22);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Resource<T> m25430(A a) throws IOException {
        if (this.f19728.cacheSource()) {
            return m25423(a);
        }
        long r2 = LogTime.m26177();
        Resource<T> r0 = this.f19722.m25999().m25372(a, this.f19733, this.f19732);
        if (!Log.isLoggable("DecodeJob", 2)) {
            return r0;
        }
        m25431("Decoded from source", r2);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25431(String str, long j) {
        Log.v("DecodeJob", str + " in " + LogTime.m26176(j) + ", key: " + this.f19731);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Resource<Z> m25432() throws Exception {
        if (!this.f19728.cacheSource()) {
            return null;
        }
        long r2 = LogTime.m26177();
        Resource r0 = m25428(this.f19731.m25470());
        if (Log.isLoggable("DecodeJob", 2)) {
            m25431("Decoded source from cache", r2);
        }
        return m25429(r0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25433() {
        this.f19725 = true;
        this.f19730.m25388();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Resource<Z> m25434() throws Exception {
        return m25429(m25422());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<Z> m25435() throws Exception {
        if (!this.f19728.cacheResult()) {
            return null;
        }
        long r2 = LogTime.m26177();
        Resource r1 = m25428((Key) this.f19731);
        if (Log.isLoggable("DecodeJob", 2)) {
            m25431("Decoded transformed from cache", r2);
        }
        long r22 = LogTime.m26177();
        Resource<Z> r0 = m25425(r1);
        if (!Log.isLoggable("DecodeJob", 2)) {
            return r0;
        }
        m25431("Transcoded transformed from cache", r22);
        return r0;
    }
}
