package com.bumptech.glide.load.engine.cache;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

public class MemorySizeCalculator {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f19856;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f19857;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f19858;

    private static class DisplayMetricsScreenDimensions implements ScreenDimensions {

        /* renamed from: 龘  reason: contains not printable characters */
        private final DisplayMetrics f19859;

        public DisplayMetricsScreenDimensions(DisplayMetrics displayMetrics) {
            this.f19859 = displayMetrics;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m25612() {
            return this.f19859.heightPixels;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m25613() {
            return this.f19859.widthPixels;
        }
    }

    interface ScreenDimensions {
        /* renamed from: 靐  reason: contains not printable characters */
        int m25614();

        /* renamed from: 龘  reason: contains not printable characters */
        int m25615();
    }

    public MemorySizeCalculator(Context context) {
        this(context, (ActivityManager) context.getSystemService("activity"), new DisplayMetricsScreenDimensions(context.getResources().getDisplayMetrics()));
    }

    MemorySizeCalculator(Context context, ActivityManager activityManager, ScreenDimensions screenDimensions) {
        this.f19857 = context;
        int r0 = m25608(activityManager);
        int r2 = screenDimensions.m25615() * screenDimensions.m25614() * 4;
        int i = r2 * 4;
        int i2 = r2 * 2;
        if (i2 + i <= r0) {
            this.f19856 = i2;
            this.f19858 = i;
        } else {
            int round = Math.round(((float) r0) / 6.0f);
            this.f19856 = round * 2;
            this.f19858 = round * 4;
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            Log.d("MemorySizeCalculator", "Calculated memory cache size: " + m25609(this.f19856) + " pool size: " + m25609(this.f19858) + " memory class limited? " + (i2 + i > r0) + " max size: " + m25609(r0) + " memoryClass: " + activityManager.getMemoryClass() + " isLowMemoryDevice: " + m25607(activityManager));
        }
    }

    @TargetApi(19)
    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m25607(ActivityManager activityManager) {
        return Build.VERSION.SDK_INT >= 19 ? activityManager.isLowRamDevice() : Build.VERSION.SDK_INT < 11;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m25608(ActivityManager activityManager) {
        boolean r0 = m25607(activityManager);
        return Math.round((r0 ? 0.33f : 0.4f) * ((float) (activityManager.getMemoryClass() * 1024 * 1024)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m25609(int i) {
        return Formatter.formatFileSize(this.f19857, (long) i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m25610() {
        return this.f19858;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m25611() {
        return this.f19856;
    }
}
