package com.bumptech.glide.load.engine.prefill;

import android.os.Handler;
import android.os.Looper;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.cache.MemoryCache;

public final class BitmapPreFiller {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapPool f19868;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Handler f19869 = new Handler(Looper.getMainLooper());

    /* renamed from: 齉  reason: contains not printable characters */
    private final DecodeFormat f19870;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MemoryCache f19871;

    public BitmapPreFiller(MemoryCache memoryCache, BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this.f19871 = memoryCache;
        this.f19868 = bitmapPool;
        this.f19870 = decodeFormat;
    }
}
