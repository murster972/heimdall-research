package com.bumptech.glide.load.engine;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.EngineResource;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.DiskCacheAdapter;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.provider.DataLoadProvider;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Util;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class Engine implements EngineJobListener, EngineResource.ResourceListener, MemoryCache.ResourceRemovedListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ResourceRecycler f19738;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final LazyDiskCacheProvider f19739;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ReferenceQueue<EngineResource<?>> f19740;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<Key, WeakReference<EngineResource<?>>> f19741;

    /* renamed from: 靐  reason: contains not printable characters */
    private final EngineKeyFactory f19742;

    /* renamed from: 麤  reason: contains not printable characters */
    private final EngineJobFactory f19743;

    /* renamed from: 齉  reason: contains not printable characters */
    private final MemoryCache f19744;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<Key, EngineJob> f19745;

    static class EngineJobFactory {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ExecutorService f19746;

        /* renamed from: 齉  reason: contains not printable characters */
        private final EngineJobListener f19747;

        /* renamed from: 龘  reason: contains not printable characters */
        private final ExecutorService f19748;

        public EngineJobFactory(ExecutorService executorService, ExecutorService executorService2, EngineJobListener engineJobListener) {
            this.f19748 = executorService;
            this.f19746 = executorService2;
            this.f19747 = engineJobListener;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public EngineJob m25450(Key key, boolean z) {
            return new EngineJob(key, this.f19748, this.f19746, z, this.f19747);
        }
    }

    private static class LazyDiskCacheProvider implements DecodeJob.DiskCacheProvider {

        /* renamed from: 靐  reason: contains not printable characters */
        private volatile DiskCache f19749;

        /* renamed from: 龘  reason: contains not printable characters */
        private final DiskCache.Factory f19750;

        public LazyDiskCacheProvider(DiskCache.Factory factory) {
            this.f19750 = factory;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public DiskCache m25451() {
            if (this.f19749 == null) {
                synchronized (this) {
                    if (this.f19749 == null) {
                        this.f19749 = this.f19750.m25576();
                    }
                    if (this.f19749 == null) {
                        this.f19749 = new DiskCacheAdapter();
                    }
                }
            }
            return this.f19749;
        }
    }

    public static class LoadStatus {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ResourceCallback f19751;

        /* renamed from: 龘  reason: contains not printable characters */
        private final EngineJob f19752;

        public LoadStatus(ResourceCallback resourceCallback, EngineJob engineJob) {
            this.f19751 = resourceCallback;
            this.f19752 = engineJob;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25452() {
            this.f19752.m25461(this.f19751);
        }
    }

    private static class RefQueueIdleHandler implements MessageQueue.IdleHandler {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ReferenceQueue<EngineResource<?>> f19753;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<Key, WeakReference<EngineResource<?>>> f19754;

        public RefQueueIdleHandler(Map<Key, WeakReference<EngineResource<?>>> map, ReferenceQueue<EngineResource<?>> referenceQueue) {
            this.f19754 = map;
            this.f19753 = referenceQueue;
        }

        public boolean queueIdle() {
            ResourceWeakReference resourceWeakReference = (ResourceWeakReference) this.f19753.poll();
            if (resourceWeakReference == null) {
                return true;
            }
            this.f19754.remove(resourceWeakReference.f19755);
            return true;
        }
    }

    private static class ResourceWeakReference extends WeakReference<EngineResource<?>> {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final Key f19755;

        public ResourceWeakReference(Key key, EngineResource<?> engineResource, ReferenceQueue<? super EngineResource<?>> referenceQueue) {
            super(engineResource, referenceQueue);
            this.f19755 = key;
        }
    }

    public Engine(MemoryCache memoryCache, DiskCache.Factory factory, ExecutorService executorService, ExecutorService executorService2) {
        this(memoryCache, factory, executorService, executorService2, (Map<Key, EngineJob>) null, (EngineKeyFactory) null, (Map<Key, WeakReference<EngineResource<?>>>) null, (EngineJobFactory) null, (ResourceRecycler) null);
    }

    Engine(MemoryCache memoryCache, DiskCache.Factory factory, ExecutorService executorService, ExecutorService executorService2, Map<Key, EngineJob> map, EngineKeyFactory engineKeyFactory, Map<Key, WeakReference<EngineResource<?>>> map2, EngineJobFactory engineJobFactory, ResourceRecycler resourceRecycler) {
        this.f19744 = memoryCache;
        this.f19739 = new LazyDiskCacheProvider(factory);
        this.f19741 = map2 == null ? new HashMap<>() : map2;
        this.f19742 = engineKeyFactory == null ? new EngineKeyFactory() : engineKeyFactory;
        this.f19745 = map == null ? new HashMap<>() : map;
        this.f19743 = engineJobFactory == null ? new EngineJobFactory(executorService, executorService2, this) : engineJobFactory;
        this.f19738 = resourceRecycler == null ? new ResourceRecycler() : resourceRecycler;
        memoryCache.m25605((MemoryCache.ResourceRemovedListener) this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private EngineResource<?> m25439(Key key, boolean z) {
        if (!z) {
            return null;
        }
        EngineResource<?> r0 = m25440(key);
        if (r0 == null) {
            return r0;
        }
        r0.m25474();
        this.f19741.put(key, new ResourceWeakReference(key, r0, m25442()));
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private EngineResource<?> m25440(Key key) {
        Resource<?> r0 = this.f19744.m25602(key);
        if (r0 == null) {
            return null;
        }
        return r0 instanceof EngineResource ? (EngineResource) r0 : new EngineResource<>(r0, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private EngineResource<?> m25441(Key key, boolean z) {
        WeakReference weakReference;
        if (!z || (weakReference = this.f19741.get(key)) == null) {
            return null;
        }
        EngineResource<?> engineResource = (EngineResource) weakReference.get();
        if (engineResource != null) {
            engineResource.m25474();
            return engineResource;
        }
        this.f19741.remove(key);
        return engineResource;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ReferenceQueue<EngineResource<?>> m25442() {
        if (this.f19740 == null) {
            this.f19740 = new ReferenceQueue<>();
            Looper.myQueue().addIdleHandler(new RefQueueIdleHandler(this.f19741, this.f19740));
        }
        return this.f19740;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25443(String str, long j, Key key) {
        Log.v("Engine", str + " in " + LogTime.m26176(j) + "ms, key: " + key);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25444(Key key, EngineResource engineResource) {
        Util.m26201();
        this.f19741.remove(key);
        if (engineResource.m25479()) {
            this.f19744.m25601(key, engineResource);
        } else {
            this.f19738.m25494(engineResource);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25445(Resource<?> resource) {
        Util.m26201();
        this.f19738.m25494(resource);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T, Z, R> LoadStatus m25446(Key key, int i, int i2, DataFetcher<T> dataFetcher, DataLoadProvider<T, Z> dataLoadProvider, Transformation<Z> transformation, ResourceTranscoder<Z, R> resourceTranscoder, Priority priority, boolean z, DiskCacheStrategy diskCacheStrategy, ResourceCallback resourceCallback) {
        Util.m26201();
        long r22 = LogTime.m26177();
        EngineKey r7 = this.f19742.m25472(dataFetcher.m25387(), key, i, i2, dataLoadProvider.m26002(), dataLoadProvider.m25999(), transformation, dataLoadProvider.m26000(), resourceTranscoder, dataLoadProvider.m26001());
        EngineResource<?> r18 = m25439((Key) r7, z);
        if (r18 != null) {
            resourceCallback.m26073((Resource<?>) r18);
            if (Log.isLoggable("Engine", 2)) {
                m25443("Loaded resource from cache", r22, r7);
            }
            return null;
        }
        EngineResource<?> r17 = m25441((Key) r7, z);
        if (r17 != null) {
            resourceCallback.m26073((Resource<?>) r17);
            if (Log.isLoggable("Engine", 2)) {
                m25443("Loaded resource from active resources", r22, r7);
            }
            return null;
        }
        EngineJob engineJob = this.f19745.get(r7);
        if (engineJob != null) {
            engineJob.m25465(resourceCallback);
            if (Log.isLoggable("Engine", 2)) {
                m25443("Added to existing load", r22, r7);
            }
            return new LoadStatus(resourceCallback, engineJob);
        }
        EngineJob r20 = this.f19743.m25450(r7, z);
        EngineJob engineJob2 = r20;
        EngineRunnable engineRunnable = new EngineRunnable(engineJob2, new DecodeJob(r7, i, i2, dataFetcher, dataLoadProvider, transformation, resourceTranscoder, this.f19739, diskCacheStrategy, priority), priority);
        this.f19745.put(r7, r20);
        r20.m25465(resourceCallback);
        r20.m25463(engineRunnable);
        if (Log.isLoggable("Engine", 2)) {
            m25443("Started new load", r22, r7);
        }
        return new LoadStatus(resourceCallback, r20);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25447(Key key, EngineResource<?> engineResource) {
        Util.m26201();
        if (engineResource != null) {
            engineResource.m25478(key, this);
            if (engineResource.m25479()) {
                this.f19741.put(key, new ResourceWeakReference(key, engineResource, m25442()));
            }
        }
        this.f19745.remove(key);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25448(EngineJob engineJob, Key key) {
        Util.m26201();
        if (engineJob.equals(this.f19745.get(key))) {
            this.f19745.remove(key);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25449(Resource resource) {
        Util.m26201();
        if (resource instanceof EngineResource) {
            ((EngineResource) resource).m25473();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }
}
