package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.Resource;

public interface MemoryCache {

    public interface ResourceRemovedListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m25606(Resource<?> resource);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    Resource<?> m25601(Key key, Resource<?> resource);

    /* renamed from: 龘  reason: contains not printable characters */
    Resource<?> m25602(Key key);

    /* renamed from: 龘  reason: contains not printable characters */
    void m25603();

    /* renamed from: 龘  reason: contains not printable characters */
    void m25604(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m25605(ResourceRemovedListener resourceRemovedListener);
}
