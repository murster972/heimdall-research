package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.bumptech.glide.util.Util;

class ResourceRecycler {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Handler f19803 = new Handler(Looper.getMainLooper(), new ResourceRecyclerCallback());

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f19804;

    private static class ResourceRecyclerCallback implements Handler.Callback {
        private ResourceRecyclerCallback() {
        }

        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((Resource) message.obj).m25492();
            return true;
        }
    }

    ResourceRecycler() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25494(Resource<?> resource) {
        Util.m26201();
        if (this.f19804) {
            this.f19803.obtainMessage(1, resource).sendToTarget();
            return;
        }
        this.f19804 = true;
        resource.m25492();
        this.f19804 = false;
    }
}
