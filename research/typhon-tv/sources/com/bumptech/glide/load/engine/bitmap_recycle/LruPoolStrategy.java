package com.bumptech.glide.load.engine.bitmap_recycle;

import android.graphics.Bitmap;

interface LruPoolStrategy {
    /* renamed from: 靐  reason: contains not printable characters */
    String m25548(int i, int i2, Bitmap.Config config);

    /* renamed from: 靐  reason: contains not printable characters */
    String m25549(Bitmap bitmap);

    /* renamed from: 齉  reason: contains not printable characters */
    int m25550(Bitmap bitmap);

    /* renamed from: 龘  reason: contains not printable characters */
    Bitmap m25551();

    /* renamed from: 龘  reason: contains not printable characters */
    Bitmap m25552(int i, int i2, Bitmap.Config config);

    /* renamed from: 龘  reason: contains not printable characters */
    void m25553(Bitmap bitmap);
}
