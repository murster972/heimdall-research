package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.EngineRunnable;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.util.Util;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class EngineJob implements EngineRunnable.EngineRunnableManager {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Handler f19756 = new Handler(Looper.getMainLooper(), new MainThreadCallback());

    /* renamed from: 龘  reason: contains not printable characters */
    private static final EngineResourceFactory f19757 = new EngineResourceFactory();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Key f19758;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ExecutorService f19759;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ExecutorService f19760;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Exception f19761;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f19762;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f19763;

    /* renamed from: ˊ  reason: contains not printable characters */
    private EngineResource<?> f19764;

    /* renamed from: ˋ  reason: contains not printable characters */
    private volatile Future<?> f19765;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f19766;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f19767;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Resource<?> f19768;

    /* renamed from: 连任  reason: contains not printable characters */
    private final EngineJobListener f19769;

    /* renamed from: 麤  reason: contains not printable characters */
    private final EngineResourceFactory f19770;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<ResourceCallback> f19771;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Set<ResourceCallback> f19772;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private EngineRunnable f19773;

    static class EngineResourceFactory {
        EngineResourceFactory() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public <R> EngineResource<R> m25467(Resource<R> resource, boolean z) {
            return new EngineResource<>(resource, z);
        }
    }

    private static class MainThreadCallback implements Handler.Callback {
        private MainThreadCallback() {
        }

        public boolean handleMessage(Message message) {
            if (1 != message.what && 2 != message.what) {
                return false;
            }
            EngineJob engineJob = (EngineJob) message.obj;
            if (1 == message.what) {
                engineJob.m25454();
                return true;
            }
            engineJob.m25457();
            return true;
        }
    }

    public EngineJob(Key key, ExecutorService executorService, ExecutorService executorService2, boolean z, EngineJobListener engineJobListener) {
        this(key, executorService, executorService2, z, engineJobListener, f19757);
    }

    public EngineJob(Key key, ExecutorService executorService, ExecutorService executorService2, boolean z, EngineJobListener engineJobListener, EngineResourceFactory engineResourceFactory) {
        this.f19771 = new ArrayList();
        this.f19758 = key;
        this.f19759 = executorService;
        this.f19760 = executorService2;
        this.f19766 = z;
        this.f19769 = engineJobListener;
        this.f19770 = engineResourceFactory;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25454() {
        if (this.f19767) {
            this.f19768.m25492();
        } else if (this.f19771.isEmpty()) {
            throw new IllegalStateException("Received a resource without any callbacks to notify");
        } else {
            this.f19764 = this.f19770.m25467(this.f19768, this.f19766);
            this.f19763 = true;
            this.f19764.m25474();
            this.f19769.m25468(this.f19758, this.f19764);
            for (ResourceCallback next : this.f19771) {
                if (!m25456(next)) {
                    this.f19764.m25474();
                    next.m26073((Resource<?>) this.f19764);
                }
            }
            this.f19764.m25473();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m25456(ResourceCallback resourceCallback) {
        return this.f19772 != null && this.f19772.contains(resourceCallback);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m25457() {
        if (!this.f19767) {
            if (this.f19771.isEmpty()) {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            }
            this.f19762 = true;
            this.f19769.m25468(this.f19758, (EngineResource<?>) null);
            for (ResourceCallback next : this.f19771) {
                if (!m25456(next)) {
                    next.m26074(this.f19761);
                }
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m25458(ResourceCallback resourceCallback) {
        if (this.f19772 == null) {
            this.f19772 = new HashSet();
        }
        this.f19772.add(resourceCallback);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25460(EngineRunnable engineRunnable) {
        this.f19765 = this.f19760.submit(engineRunnable);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25461(ResourceCallback resourceCallback) {
        Util.m26201();
        if (this.f19763 || this.f19762) {
            m25458(resourceCallback);
            return;
        }
        this.f19771.remove(resourceCallback);
        if (this.f19771.isEmpty()) {
            m25462();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25462() {
        if (!this.f19762 && !this.f19763 && !this.f19767) {
            this.f19773.m25488();
            Future<?> future = this.f19765;
            if (future != null) {
                future.cancel(true);
            }
            this.f19767 = true;
            this.f19769.m25469(this, this.f19758);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25463(EngineRunnable engineRunnable) {
        this.f19773 = engineRunnable;
        this.f19765 = this.f19759.submit(engineRunnable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25464(Resource<?> resource) {
        this.f19768 = resource;
        f19756.obtainMessage(1, this).sendToTarget();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25465(ResourceCallback resourceCallback) {
        Util.m26201();
        if (this.f19763) {
            resourceCallback.m26073((Resource<?>) this.f19764);
        } else if (this.f19762) {
            resourceCallback.m26074(this.f19761);
        } else {
            this.f19771.add(resourceCallback);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25466(Exception exc) {
        this.f19761 = exc;
        f19756.obtainMessage(2, this).sendToTarget();
    }
}
