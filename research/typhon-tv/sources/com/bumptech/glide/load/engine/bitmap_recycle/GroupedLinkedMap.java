package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.load.engine.bitmap_recycle.Poolable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GroupedLinkedMap<K extends Poolable, V> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<K, LinkedEntry<K, V>> f19812 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private final LinkedEntry<K, V> f19813 = new LinkedEntry<>();

    private static class LinkedEntry<K, V> {

        /* renamed from: 靐  reason: contains not printable characters */
        LinkedEntry<K, V> f19814;

        /* renamed from: 麤  reason: contains not printable characters */
        private List<V> f19815;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final K f19816;

        /* renamed from: 龘  reason: contains not printable characters */
        LinkedEntry<K, V> f19817;

        public LinkedEntry() {
            this((Object) null);
        }

        public LinkedEntry(K k) {
            this.f19814 = this;
            this.f19817 = this;
            this.f19816 = k;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m25530() {
            if (this.f19815 != null) {
                return this.f19815.size();
            }
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public V m25531() {
            int r0 = m25530();
            if (r0 > 0) {
                return this.f19815.remove(r0 - 1);
            }
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25532(V v) {
            if (this.f19815 == null) {
                this.f19815 = new ArrayList();
            }
            this.f19815.add(v);
        }
    }

    GroupedLinkedMap() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m25522(LinkedEntry<K, V> linkedEntry) {
        m25523(linkedEntry);
        linkedEntry.f19814 = this.f19813.f19814;
        linkedEntry.f19817 = this.f19813;
        m25524(linkedEntry);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static <K, V> void m25523(LinkedEntry<K, V> linkedEntry) {
        linkedEntry.f19814.f19817 = linkedEntry.f19817;
        linkedEntry.f19817.f19814 = linkedEntry.f19814;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static <K, V> void m25524(LinkedEntry<K, V> linkedEntry) {
        linkedEntry.f19817.f19814 = linkedEntry;
        linkedEntry.f19814.f19817 = linkedEntry;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25525(LinkedEntry<K, V> linkedEntry) {
        m25523(linkedEntry);
        linkedEntry.f19814 = this.f19813;
        linkedEntry.f19817 = this.f19813.f19817;
        m25524(linkedEntry);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (LinkedEntry<K, V> linkedEntry = this.f19813.f19817; !linkedEntry.equals(this.f19813); linkedEntry = linkedEntry.f19817) {
            z = true;
            sb.append('{').append(linkedEntry.f19816).append(':').append(linkedEntry.m25530()).append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.append(" )").toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public V m25526() {
        for (LinkedEntry<K, V> linkedEntry = this.f19813.f19814; !linkedEntry.equals(this.f19813); linkedEntry = linkedEntry.f19814) {
            V r1 = linkedEntry.m25531();
            if (r1 != null) {
                return r1;
            }
            m25523(linkedEntry);
            this.f19812.remove(linkedEntry.f19816);
            ((Poolable) linkedEntry.f19816).m25554();
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public V m25527(K k) {
        LinkedEntry linkedEntry = this.f19812.get(k);
        if (linkedEntry == null) {
            linkedEntry = new LinkedEntry(k);
            this.f19812.put(k, linkedEntry);
        } else {
            k.m25554();
        }
        m25525(linkedEntry);
        return linkedEntry.m25531();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25528(K k, V v) {
        LinkedEntry linkedEntry = this.f19812.get(k);
        if (linkedEntry == null) {
            linkedEntry = new LinkedEntry(k);
            m25522(linkedEntry);
            this.f19812.put(k, linkedEntry);
        } else {
            k.m25554();
        }
        linkedEntry.m25532(v);
    }
}
