package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Key;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

class OriginalKey implements Key {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Key f19801;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f19802;

    public OriginalKey(String str, Key key) {
        this.f19802 = str;
        this.f19801 = key;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        OriginalKey originalKey = (OriginalKey) obj;
        if (!this.f19802.equals(originalKey.f19802)) {
            return false;
        }
        return this.f19801.equals(originalKey.f19801);
    }

    public int hashCode() {
        return (this.f19802.hashCode() * 31) + this.f19801.hashCode();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25490(MessageDigest messageDigest) throws UnsupportedEncodingException {
        messageDigest.update(this.f19802.getBytes("UTF-8"));
        this.f19801.m25371(messageDigest);
    }
}
