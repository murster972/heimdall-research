package com.bumptech.glide.load.engine;

import android.os.Looper;
import com.bumptech.glide.load.Key;

class EngineResource<Z> implements Resource<Z> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f19787;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f19788;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f19789;

    /* renamed from: 麤  reason: contains not printable characters */
    private Key f19790;

    /* renamed from: 齉  reason: contains not printable characters */
    private ResourceListener f19791;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Resource<Z> f19792;

    interface ResourceListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m25480(Key key, EngineResource<?> engineResource);
    }

    EngineResource(Resource<Z> resource, boolean z) {
        if (resource == null) {
            throw new NullPointerException("Wrapped resource must not be null");
        }
        this.f19792 = resource;
        this.f19789 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m25473() {
        if (this.f19788 <= 0) {
            throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
        } else if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalThreadStateException("Must call release on the main thread");
        } else {
            int i = this.f19788 - 1;
            this.f19788 = i;
            if (i == 0) {
                this.f19791.m25480(this.f19790, this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m25474() {
        if (this.f19787) {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        } else if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalThreadStateException("Must call acquire on the main thread");
        } else {
            this.f19788++;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Z m25475() {
        return this.f19792.m25491();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25476() {
        if (this.f19788 > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (this.f19787) {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        } else {
            this.f19787 = true;
            this.f19792.m25492();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25477() {
        return this.f19792.m25493();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25478(Key key, ResourceListener resourceListener) {
        this.f19790 = key;
        this.f19791 = resourceListener;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25479() {
        return this.f19789;
    }
}
