package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Util;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class SafeKeyGenerator {

    /* renamed from: 龘  reason: contains not printable characters */
    private final LruCache<Key, String> f19860 = new LruCache<>(1000);

    SafeKeyGenerator() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25616(Key key) {
        String r2;
        synchronized (this.f19860) {
            r2 = this.f19860.m26180(key);
        }
        if (r2 == null) {
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-256");
                key.m25371(instance);
                r2 = Util.m26197(instance.digest());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e2) {
                e2.printStackTrace();
            }
            synchronized (this.f19860) {
                this.f19860.m26181(key, r2);
            }
        }
        return r2;
    }
}
