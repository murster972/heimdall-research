package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.load.engine.bitmap_recycle.Poolable;
import com.bumptech.glide.util.Util;
import java.util.Queue;

abstract class BaseKeyPool<T extends Poolable> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Queue<T> f19811 = Util.m26200(20);

    BaseKeyPool() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract T m25509();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public T m25510() {
        T t = (Poolable) this.f19811.poll();
        return t == null ? m25509() : t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25511(T t) {
        if (this.f19811.size() < 20) {
            this.f19811.offer(t);
        }
    }
}
