package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.Key;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class DiskCacheWriteLocker {

    /* renamed from: 靐  reason: contains not printable characters */
    private final WriteLockPool f19840 = new WriteLockPool();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<Key, WriteLock> f19841 = new HashMap();

    private static class WriteLock {

        /* renamed from: 靐  reason: contains not printable characters */
        int f19842;

        /* renamed from: 龘  reason: contains not printable characters */
        final Lock f19843;

        private WriteLock() {
            this.f19843 = new ReentrantLock();
        }
    }

    private static class WriteLockPool {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Queue<WriteLock> f19844;

        private WriteLockPool() {
            this.f19844 = new ArrayDeque();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public WriteLock m25583() {
            WriteLock poll;
            synchronized (this.f19844) {
                poll = this.f19844.poll();
            }
            return poll == null ? new WriteLock() : poll;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25584(WriteLock writeLock) {
            synchronized (this.f19844) {
                if (this.f19844.size() < 10) {
                    this.f19844.offer(writeLock);
                }
            }
        }
    }

    DiskCacheWriteLocker() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25581(Key key) {
        WriteLock writeLock;
        synchronized (this) {
            writeLock = this.f19841.get(key);
            if (writeLock == null || writeLock.f19842 <= 0) {
                throw new IllegalArgumentException("Cannot release a lock that is not held, key: " + key + ", interestedThreads: " + (writeLock == null ? 0 : writeLock.f19842));
            }
            int i = writeLock.f19842 - 1;
            writeLock.f19842 = i;
            if (i == 0) {
                WriteLock remove = this.f19841.remove(key);
                if (!remove.equals(writeLock)) {
                    throw new IllegalStateException("Removed the wrong lock, expected to remove: " + writeLock + ", but actually removed: " + remove + ", key: " + key);
                }
                this.f19840.m25584(remove);
            }
        }
        writeLock.f19843.unlock();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25582(Key key) {
        WriteLock writeLock;
        synchronized (this) {
            writeLock = this.f19841.get(key);
            if (writeLock == null) {
                writeLock = this.f19840.m25583();
                this.f19841.put(key, writeLock);
            }
            writeLock.f19842++;
        }
        writeLock.f19843.lock();
    }
}
