package com.bumptech.glide.load.engine.cache;

import android.annotation.SuppressLint;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.util.LruCache;

public class LruResourceCache extends LruCache<Key, Resource<?>> implements MemoryCache {

    /* renamed from: 龘  reason: contains not printable characters */
    private MemoryCache.ResourceRemovedListener f19855;

    public LruResourceCache(int i) {
        super(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Resource m25593(Key key, Resource resource) {
        return (Resource) super.m26181(key, resource);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m25595(Resource<?> resource) {
        return resource.m25493();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* synthetic */ Resource m25596(Key key) {
        return (Resource) super.m26183(key);
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25597(int i) {
        if (i >= 60) {
            m26185();
        } else if (i >= 40) {
            m26182(m26179() / 2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25600(Key key, Resource<?> resource) {
        if (this.f19855 != null) {
            this.f19855.m25606(resource);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25599(MemoryCache.ResourceRemovedListener resourceRemovedListener) {
        this.f19855 = resourceRemovedListener;
    }
}
