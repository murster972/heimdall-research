package com.bumptech.glide.load.engine.executor;

import android.os.Process;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class FifoPriorityThreadPoolExecutor extends ThreadPoolExecutor {

    /* renamed from: 靐  reason: contains not printable characters */
    private final UncaughtThrowableStrategy f19861;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicInteger f19862;

    public static class DefaultThreadFactory implements ThreadFactory {

        /* renamed from: 龘  reason: contains not printable characters */
        int f19863 = 0;

        public Thread newThread(Runnable runnable) {
            AnonymousClass1 r0 = new Thread(runnable, "fifo-pool-thread-" + this.f19863) {
                public void run() {
                    Process.setThreadPriority(10);
                    super.run();
                }
            };
            this.f19863++;
            return r0;
        }
    }

    static class LoadTask<T> extends FutureTask<T> implements Comparable<LoadTask<?>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f19865;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f19866;

        public LoadTask(Runnable runnable, T t, int i) {
            super(runnable, t);
            if (!(runnable instanceof Prioritized)) {
                throw new IllegalArgumentException("FifoPriorityThreadPoolExecutor must be given Runnables that implement Prioritized");
            }
            this.f19866 = ((Prioritized) runnable).m25621();
            this.f19865 = i;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof LoadTask)) {
                return false;
            }
            LoadTask loadTask = (LoadTask) obj;
            return this.f19865 == loadTask.f19865 && this.f19866 == loadTask.f19866;
        }

        public int hashCode() {
            return (this.f19866 * 31) + this.f19865;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compareTo(LoadTask<?> loadTask) {
            int i = this.f19866 - loadTask.f19866;
            return i == 0 ? this.f19865 - loadTask.f19865 : i;
        }
    }

    public enum UncaughtThrowableStrategy {
        IGNORE,
        LOG {
            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m25619(Throwable th) {
                if (Log.isLoggable("PriorityExecutor", 6)) {
                    Log.e("PriorityExecutor", "Request threw uncaught throwable", th);
                }
            }
        },
        THROW {
            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m25620(Throwable th) {
                super.m25618(th);
                throw new RuntimeException(th);
            }
        };

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25618(Throwable th) {
        }
    }

    public FifoPriorityThreadPoolExecutor(int i) {
        this(i, UncaughtThrowableStrategy.LOG);
    }

    public FifoPriorityThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory, UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        super(i, i2, j, timeUnit, new PriorityBlockingQueue(), threadFactory);
        this.f19862 = new AtomicInteger();
        this.f19861 = uncaughtThrowableStrategy;
    }

    public FifoPriorityThreadPoolExecutor(int i, UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        this(i, i, 0, TimeUnit.MILLISECONDS, new DefaultThreadFactory(), uncaughtThrowableStrategy);
    }

    /* access modifiers changed from: protected */
    public void afterExecute(Runnable runnable, Throwable th) {
        super.afterExecute(runnable, th);
        if (th == null && (runnable instanceof Future)) {
            Future future = (Future) runnable;
            if (future.isDone() && !future.isCancelled()) {
                try {
                    future.get();
                } catch (InterruptedException e) {
                    this.f19861.m25618(e);
                } catch (ExecutionException e2) {
                    this.f19861.m25618(e2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new LoadTask(runnable, t, this.f19862.getAndIncrement());
    }
}
