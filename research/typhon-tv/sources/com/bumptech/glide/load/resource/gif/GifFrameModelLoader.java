package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.Priority;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;

class GifFrameModelLoader implements ModelLoader<GifDecoder, GifDecoder> {

    private static class GifFrameDataFetcher implements DataFetcher<GifDecoder> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final GifDecoder f20023;

        public GifFrameDataFetcher(GifDecoder gifDecoder) {
            this.f20023 = gifDecoder;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public GifDecoder m25872(Priority priority) {
            return this.f20023;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m25870() {
            return String.valueOf(this.f20023.m25309());
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m25871() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25873() {
        }
    }

    GifFrameModelLoader() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<GifDecoder> m25868(GifDecoder gifDecoder, int i, int i2) {
        return new GifFrameDataFetcher(gifDecoder);
    }
}
