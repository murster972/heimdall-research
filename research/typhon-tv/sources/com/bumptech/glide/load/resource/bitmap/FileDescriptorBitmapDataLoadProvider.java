package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.NullEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.provider.DataLoadProvider;
import java.io.File;

public class FileDescriptorBitmapDataLoadProvider implements DataLoadProvider<ParcelFileDescriptor, Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final FileDescriptorBitmapDecoder f19929;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Encoder<ParcelFileDescriptor> f19930 = NullEncoder.m25709();

    /* renamed from: 齉  reason: contains not printable characters */
    private final BitmapEncoder f19931 = new BitmapEncoder();

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResourceDecoder<File, Bitmap> f19932;

    public FileDescriptorBitmapDataLoadProvider(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this.f19932 = new FileToStreamDecoder(new StreamBitmapDecoder(bitmapPool, decodeFormat));
        this.f19929 = new FileDescriptorBitmapDecoder(bitmapPool, decodeFormat);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<ParcelFileDescriptor, Bitmap> m25754() {
        return this.f19929;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<Bitmap> m25755() {
        return this.f19931;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<ParcelFileDescriptor> m25756() {
        return this.f19930;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, Bitmap> m25757() {
        return this.f19932;
    }
}
