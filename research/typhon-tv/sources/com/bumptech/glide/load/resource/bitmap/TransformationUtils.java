package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.Log;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

public final class TransformationUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m25804(int i) {
        switch (i) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 6:
                return 90;
            case 7:
            case 8:
                return 270;
            default:
                return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Bitmap.Config m25805(Bitmap bitmap) {
        return bitmap.getConfig() != null ? bitmap.getConfig() : Bitmap.Config.ARGB_8888;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bitmap m25806(Bitmap bitmap, Bitmap bitmap2, int i, int i2) {
        float width;
        if (bitmap2 == null) {
            return null;
        }
        if (bitmap2.getWidth() == i && bitmap2.getHeight() == i2) {
            return bitmap2;
        }
        float f = 0.0f;
        float f2 = 0.0f;
        Matrix matrix = new Matrix();
        if (bitmap2.getWidth() * i2 > bitmap2.getHeight() * i) {
            width = ((float) i2) / ((float) bitmap2.getHeight());
            f = (((float) i) - (((float) bitmap2.getWidth()) * width)) * 0.5f;
        } else {
            width = ((float) i) / ((float) bitmap2.getWidth());
            f2 = (((float) i2) - (((float) bitmap2.getHeight()) * width)) * 0.5f;
        }
        matrix.setScale(width, width);
        matrix.postTranslate((float) ((int) (f + 0.5f)), (float) ((int) (f2 + 0.5f)));
        Bitmap createBitmap = bitmap != null ? bitmap : Bitmap.createBitmap(i, i2, m25805(bitmap2));
        m25810(bitmap2, createBitmap);
        new Canvas(createBitmap).drawBitmap(bitmap2, matrix, new Paint(6));
        return createBitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bitmap m25807(Bitmap bitmap, BitmapPool bitmapPool, int i) {
        Matrix matrix = new Matrix();
        m25809(i, matrix);
        if (matrix.isIdentity()) {
            return bitmap;
        }
        RectF rectF = new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        matrix.mapRect(rectF);
        int round = Math.round(rectF.width());
        int round2 = Math.round(rectF.height());
        Bitmap.Config r1 = m25805(bitmap);
        Bitmap r7 = bitmapPool.m25513(round, round2, r1);
        if (r7 == null) {
            r7 = Bitmap.createBitmap(round, round2, r1);
        }
        matrix.postTranslate(-rectF.left, -rectF.top);
        new Canvas(r7).drawBitmap(bitmap, matrix, new Paint(6));
        return r7;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Bitmap m25808(Bitmap bitmap, BitmapPool bitmapPool, int i, int i2) {
        if (bitmap.getWidth() != i || bitmap.getHeight() != i2) {
            float min = Math.min(((float) i) / ((float) bitmap.getWidth()), ((float) i2) / ((float) bitmap.getHeight()));
            int width = (int) (((float) bitmap.getWidth()) * min);
            int height = (int) (((float) bitmap.getHeight()) * min);
            if (bitmap.getWidth() != width || bitmap.getHeight() != height) {
                Bitmap.Config r2 = m25805(bitmap);
                Bitmap r9 = bitmapPool.m25513(width, height, r2);
                if (r9 == null) {
                    r9 = Bitmap.createBitmap(width, height, r2);
                }
                m25810(bitmap, r9);
                if (Log.isLoggable("TransformationUtils", 2)) {
                    Log.v("TransformationUtils", "request: " + i + "x" + i2);
                    Log.v("TransformationUtils", "toFit:   " + bitmap.getWidth() + "x" + bitmap.getHeight());
                    Log.v("TransformationUtils", "toReuse: " + r9.getWidth() + "x" + r9.getHeight());
                    Log.v("TransformationUtils", "minPct:   " + min);
                }
                Canvas canvas = new Canvas(r9);
                Matrix matrix = new Matrix();
                matrix.setScale(min, min);
                canvas.drawBitmap(bitmap, matrix, new Paint(6));
                return r9;
            } else if (!Log.isLoggable("TransformationUtils", 2)) {
                return bitmap;
            } else {
                Log.v("TransformationUtils", "adjusted target size matches input, returning input");
                return bitmap;
            }
        } else if (!Log.isLoggable("TransformationUtils", 2)) {
            return bitmap;
        } else {
            Log.v("TransformationUtils", "requested target size matches input, returning input");
            return bitmap;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m25809(int i, Matrix matrix) {
        switch (i) {
            case 2:
                matrix.setScale(-1.0f, 1.0f);
                return;
            case 3:
                matrix.setRotate(180.0f);
                return;
            case 4:
                matrix.setRotate(180.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 5:
                matrix.setRotate(90.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 6:
                matrix.setRotate(90.0f);
                return;
            case 7:
                matrix.setRotate(-90.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 8:
                matrix.setRotate(-90.0f);
                return;
            default:
                return;
        }
    }

    @TargetApi(12)
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m25810(Bitmap bitmap, Bitmap bitmap2) {
        if (Build.VERSION.SDK_INT >= 12 && bitmap2 != null) {
            bitmap2.setHasAlpha(bitmap.hasAlpha());
        }
    }
}
