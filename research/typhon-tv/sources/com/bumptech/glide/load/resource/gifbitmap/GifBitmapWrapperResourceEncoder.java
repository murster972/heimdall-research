package com.bumptech.glide.load.resource.gifbitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import java.io.OutputStream;

public class GifBitmapWrapperResourceEncoder implements ResourceEncoder<GifBitmapWrapper> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ResourceEncoder<GifDrawable> f20049;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f20050;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResourceEncoder<Bitmap> f20051;

    public GifBitmapWrapperResourceEncoder(ResourceEncoder<Bitmap> resourceEncoder, ResourceEncoder<GifDrawable> resourceEncoder2) {
        this.f20051 = resourceEncoder;
        this.f20049 = resourceEncoder2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25913() {
        if (this.f20050 == null) {
            this.f20050 = this.f20051.m25369() + this.f20049.m25369();
        }
        return this.f20050;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25915(Resource<GifBitmapWrapper> resource, OutputStream outputStream) {
        GifBitmapWrapper r1 = resource.m25491();
        Resource<Bitmap> r0 = r1.m25897();
        return r0 != null ? this.f20051.m25370(r0, outputStream) : this.f20049.m25370(r1.m25898(), outputStream);
    }
}
