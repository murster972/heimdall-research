package com.bumptech.glide.load.resource.drawable;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.Resource;

public abstract class DrawableResource<T extends Drawable> implements Resource<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final T f19974;

    public DrawableResource(T t) {
        if (t == null) {
            throw new NullPointerException("Drawable must not be null!");
        }
        this.f19974 = t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final T m25814() {
        return this.f19974.getConstantState().newDrawable();
    }
}
