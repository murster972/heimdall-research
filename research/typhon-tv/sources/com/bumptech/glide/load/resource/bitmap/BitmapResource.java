package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Util;

public class BitmapResource implements Resource<Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapPool f19921;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Bitmap f19922;

    public BitmapResource(Bitmap bitmap, BitmapPool bitmapPool) {
        if (bitmap == null) {
            throw new NullPointerException("Bitmap must not be null");
        } else if (bitmapPool == null) {
            throw new NullPointerException("BitmapPool must not be null");
        } else {
            this.f19922 = bitmap;
            this.f19921 = bitmapPool;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static BitmapResource m25727(Bitmap bitmap, BitmapPool bitmapPool) {
        if (bitmap == null) {
            return null;
        }
        return new BitmapResource(bitmap, bitmapPool);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25729() {
        if (!this.f19921.m25516(this.f19922)) {
            this.f19922.recycle();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25730() {
        return Util.m26196(this.f19922);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25728() {
        return this.f19922;
    }
}
