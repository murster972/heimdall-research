package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.io.IOException;

public class FileDescriptorBitmapDecoder implements ResourceDecoder<ParcelFileDescriptor, Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapPool f19933;

    /* renamed from: 齉  reason: contains not printable characters */
    private DecodeFormat f19934;

    /* renamed from: 龘  reason: contains not printable characters */
    private final VideoBitmapDecoder f19935;

    public FileDescriptorBitmapDecoder(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this(new VideoBitmapDecoder(), bitmapPool, decodeFormat);
    }

    public FileDescriptorBitmapDecoder(VideoBitmapDecoder videoBitmapDecoder, BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this.f19935 = videoBitmapDecoder;
        this.f19933 = bitmapPool;
        this.f19934 = decodeFormat;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<Bitmap> m25759(ParcelFileDescriptor parcelFileDescriptor, int i, int i2) throws IOException {
        return BitmapResource.m25727(this.f19935.m25811(parcelFileDescriptor, this.f19933, i, i2, this.f19934), this.f19933);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25760() {
        return "FileDescriptorBitmapDecoder.com.bumptech.glide.load.data.bitmap";
    }
}
