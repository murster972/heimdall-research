package com.bumptech.glide.load.resource.transcode;

import android.content.res.Resources;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawableResource;

public class GlideBitmapDrawableTranscoder implements ResourceTranscoder<Bitmap, GlideBitmapDrawable> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapPool f20060;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Resources f20061;

    public GlideBitmapDrawableTranscoder(Resources resources, BitmapPool bitmapPool) {
        this.f20061 = resources;
        this.f20060 = bitmapPool;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<GlideBitmapDrawable> m25927(Resource<Bitmap> resource) {
        return new GlideBitmapDrawableResource(new GlideBitmapDrawable(this.f20061, resource.m25491()), this.f20060);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25928() {
        return "GlideBitmapDrawableTranscoder.com.bumptech.glide.load.resource.transcode";
    }
}
