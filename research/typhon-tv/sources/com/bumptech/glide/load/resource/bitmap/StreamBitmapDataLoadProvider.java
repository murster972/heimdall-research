package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.provider.DataLoadProvider;
import java.io.File;
import java.io.InputStream;

public class StreamBitmapDataLoadProvider implements DataLoadProvider<InputStream, Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapEncoder f19963;

    /* renamed from: 麤  reason: contains not printable characters */
    private final FileToStreamDecoder<Bitmap> f19964;

    /* renamed from: 齉  reason: contains not printable characters */
    private final StreamEncoder f19965 = new StreamEncoder();

    /* renamed from: 龘  reason: contains not printable characters */
    private final StreamBitmapDecoder f19966;

    public StreamBitmapDataLoadProvider(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this.f19966 = new StreamBitmapDecoder(bitmapPool, decodeFormat);
        this.f19963 = new BitmapEncoder();
        this.f19964 = new FileToStreamDecoder<>(this.f19966);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<InputStream, Bitmap> m25797() {
        return this.f19966;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<Bitmap> m25798() {
        return this.f19963;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<InputStream> m25799() {
        return this.f19965;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, Bitmap> m25800() {
        return this.f19964;
    }
}
