package com.bumptech.glide.load.resource.transcode;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;

public class GifBitmapWrapperDrawableTranscoder implements ResourceTranscoder<GifBitmapWrapper, GlideDrawable> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResourceTranscoder<Bitmap, GlideBitmapDrawable> f20059;

    public GifBitmapWrapperDrawableTranscoder(ResourceTranscoder<Bitmap, GlideBitmapDrawable> resourceTranscoder) {
        this.f20059 = resourceTranscoder;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<GlideDrawable> m25925(Resource<GifBitmapWrapper> resource) {
        GifBitmapWrapper r1 = resource.m25491();
        Resource<Bitmap> r0 = r1.m25897();
        return r0 != null ? this.f20059.m25929(r0) : r1.m25898();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25926() {
        return "GifBitmapWrapperDrawableTranscoder.com.bumptech.glide.load.resource.transcode";
    }
}
