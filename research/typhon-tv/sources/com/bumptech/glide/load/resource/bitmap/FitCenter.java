package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

public class FitCenter extends BitmapTransformation {
    public FitCenter(BitmapPool bitmapPool) {
        super(bitmapPool);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25761(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2) {
        return TransformationUtils.m25808(bitmap, bitmapPool, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25762() {
        return "FitCenter.com.bumptech.glide.load.resource.bitmap";
    }
}
