package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.provider.DataLoadProvider;
import java.io.File;
import java.io.InputStream;

public class GifDrawableLoadProvider implements DataLoadProvider<InputStream, GifDrawable> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final GifResourceEncoder f20003;

    /* renamed from: 麤  reason: contains not printable characters */
    private final FileToStreamDecoder<GifDrawable> f20004 = new FileToStreamDecoder<>(this.f20006);

    /* renamed from: 齉  reason: contains not printable characters */
    private final StreamEncoder f20005;

    /* renamed from: 龘  reason: contains not printable characters */
    private final GifResourceDecoder f20006;

    public GifDrawableLoadProvider(Context context, BitmapPool bitmapPool) {
        this.f20006 = new GifResourceDecoder(context, bitmapPool);
        this.f20003 = new GifResourceEncoder(bitmapPool);
        this.f20005 = new StreamEncoder();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<InputStream, GifDrawable> m25846() {
        return this.f20006;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<GifDrawable> m25847() {
        return this.f20003;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<InputStream> m25848() {
        return this.f20005;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, GifDrawable> m25849() {
        return this.f20004;
    }
}
