package com.bumptech.glide.load.resource.gifbitmap;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import java.io.IOException;
import java.io.InputStream;

public class GifBitmapWrapperStreamResourceDecoder implements ResourceDecoder<InputStream, GifBitmapWrapper> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> f20052;

    public GifBitmapWrapperStreamResourceDecoder(ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> resourceDecoder) {
        this.f20052 = resourceDecoder;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<GifBitmapWrapper> m25917(InputStream inputStream, int i, int i2) throws IOException {
        return this.f20052.m25372(new ImageVideoWrapper(inputStream, (ParcelFileDescriptor) null), i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25918() {
        return this.f20052.m25373();
    }
}
