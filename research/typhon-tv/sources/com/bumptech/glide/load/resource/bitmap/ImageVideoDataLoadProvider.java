package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.model.ImageVideoWrapperEncoder;
import com.bumptech.glide.provider.DataLoadProvider;
import java.io.File;
import java.io.InputStream;

public class ImageVideoDataLoadProvider implements DataLoadProvider<ImageVideoWrapper, Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ResourceDecoder<File, Bitmap> f19954;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ImageVideoWrapperEncoder f19955;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ResourceEncoder<Bitmap> f19956;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ImageVideoBitmapDecoder f19957;

    public ImageVideoDataLoadProvider(DataLoadProvider<InputStream, Bitmap> dataLoadProvider, DataLoadProvider<ParcelFileDescriptor, Bitmap> dataLoadProvider2) {
        this.f19956 = dataLoadProvider.m26000();
        this.f19955 = new ImageVideoWrapperEncoder(dataLoadProvider.m26001(), dataLoadProvider2.m26001());
        this.f19954 = dataLoadProvider.m26002();
        this.f19957 = new ImageVideoBitmapDecoder(dataLoadProvider.m25999(), dataLoadProvider2.m25999());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<ImageVideoWrapper, Bitmap> m25790() {
        return this.f19957;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<Bitmap> m25791() {
        return this.f19956;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<ImageVideoWrapper> m25792() {
        return this.f19955;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, Bitmap> m25793() {
        return this.f19954;
    }
}
