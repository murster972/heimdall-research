package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.NullEncoder;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.UUID;

class GifFrameLoader {

    /* renamed from: ʻ  reason: contains not printable characters */
    private GenericRequestBuilder<GifDecoder, GifDecoder, Bitmap, Bitmap> f20009;

    /* renamed from: ʼ  reason: contains not printable characters */
    private DelayTarget f20010;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f20011;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f20012;

    /* renamed from: 靐  reason: contains not printable characters */
    private final GifDecoder f20013;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f20014;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Handler f20015;

    /* renamed from: 龘  reason: contains not printable characters */
    private final FrameCallback f20016;

    static class DelayTarget extends SimpleTarget<Bitmap> {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final int f20017;

        /* renamed from: 麤  reason: contains not printable characters */
        private Bitmap f20018;

        /* renamed from: 齉  reason: contains not printable characters */
        private final long f20019;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Handler f20020;

        public DelayTarget(Handler handler, int i, long j) {
            this.f20020 = handler;
            this.f20017 = i;
            this.f20019 = j;
        }

        public Bitmap q_() {
            return this.f20018;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25864(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
            this.f20018 = bitmap;
            this.f20020.sendMessageAtTime(this.f20020.obtainMessage(1, this), this.f20019);
        }
    }

    public interface FrameCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m25865(int i);
    }

    private class FrameLoaderCallback implements Handler.Callback {
        private FrameLoaderCallback() {
        }

        public boolean handleMessage(Message message) {
            if (message.what == 1) {
                GifFrameLoader.this.m25861((DelayTarget) message.obj);
                return true;
            }
            if (message.what == 2) {
                Glide.m3942((Target<?>) (DelayTarget) message.obj);
            }
            return false;
        }
    }

    static class FrameSignature implements Key {

        /* renamed from: 龘  reason: contains not printable characters */
        private final UUID f20022;

        public FrameSignature() {
            this(UUID.randomUUID());
        }

        FrameSignature(UUID uuid) {
            this.f20022 = uuid;
        }

        public boolean equals(Object obj) {
            if (obj instanceof FrameSignature) {
                return ((FrameSignature) obj).f20022.equals(this.f20022);
            }
            return false;
        }

        public int hashCode() {
            return this.f20022.hashCode();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25866(MessageDigest messageDigest) throws UnsupportedEncodingException {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    public GifFrameLoader(Context context, FrameCallback frameCallback, GifDecoder gifDecoder, int i, int i2) {
        this(frameCallback, gifDecoder, (Handler) null, m25855(context, gifDecoder, i, i2, Glide.m3936(context).m3952()));
    }

    GifFrameLoader(FrameCallback frameCallback, GifDecoder gifDecoder, Handler handler, GenericRequestBuilder<GifDecoder, GifDecoder, Bitmap, Bitmap> genericRequestBuilder) {
        this.f20014 = false;
        this.f20012 = false;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new FrameLoaderCallback()) : handler;
        this.f20016 = frameCallback;
        this.f20013 = gifDecoder;
        this.f20015 = handler;
        this.f20009 = genericRequestBuilder;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m25854() {
        if (this.f20014 && !this.f20012) {
            this.f20012 = true;
            this.f20013.m25312();
            this.f20009.m25224((Key) new FrameSignature()).m25233(new DelayTarget(this.f20015, this.f20013.m25309(), SystemClock.uptimeMillis() + ((long) this.f20013.m25308())));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static GenericRequestBuilder<GifDecoder, GifDecoder, Bitmap, Bitmap> m25855(Context context, GifDecoder gifDecoder, int i, int i2, BitmapPool bitmapPool) {
        GifFrameResourceDecoder gifFrameResourceDecoder = new GifFrameResourceDecoder(bitmapPool);
        GifFrameModelLoader gifFrameModelLoader = new GifFrameModelLoader();
        return Glide.m3934(context).m3975(gifFrameModelLoader, GifDecoder.class).m25243(gifDecoder).m25244(Bitmap.class).m25223(NullEncoder.m25709()).m25225(gifFrameResourceDecoder).m25228(true).m25226(DiskCacheStrategy.NONE).m25221(i, i2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m25856() {
        this.f20014 = false;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Bitmap m25857() {
        if (this.f20010 != null) {
            return this.f20010.q_();
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25858() {
        m25856();
        if (this.f20010 != null) {
            Glide.m3942((Target<?>) this.f20010);
            this.f20010 = null;
        }
        this.f20011 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25859() {
        if (!this.f20014) {
            this.f20014 = true;
            this.f20011 = false;
            m25854();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25860(Transformation<Bitmap> transformation) {
        if (transformation == null) {
            throw new NullPointerException("Transformation must not be null");
        }
        this.f20009 = this.f20009.m25229((Transformation<ResourceType>[]) new Transformation[]{transformation});
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25861(DelayTarget delayTarget) {
        if (this.f20011) {
            this.f20015.obtainMessage(2, delayTarget).sendToTarget();
            return;
        }
        DelayTarget delayTarget2 = this.f20010;
        this.f20010 = delayTarget;
        this.f20016.m25865(delayTarget.f20017);
        if (delayTarget2 != null) {
            this.f20015.obtainMessage(2, delayTarget2).sendToTarget();
        }
        this.f20012 = false;
        m25854();
    }
}
