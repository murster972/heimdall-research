package com.bumptech.glide.load.resource.gifbitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.provider.DataLoadProvider;
import java.io.File;
import java.io.InputStream;

public class ImageVideoGifDrawableLoadProvider implements DataLoadProvider<ImageVideoWrapper, GifBitmapWrapper> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> f20055;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Encoder<ImageVideoWrapper> f20056;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ResourceEncoder<GifBitmapWrapper> f20057;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResourceDecoder<File, GifBitmapWrapper> f20058;

    public ImageVideoGifDrawableLoadProvider(DataLoadProvider<ImageVideoWrapper, Bitmap> dataLoadProvider, DataLoadProvider<InputStream, GifDrawable> dataLoadProvider2, BitmapPool bitmapPool) {
        GifBitmapWrapperResourceDecoder gifBitmapWrapperResourceDecoder = new GifBitmapWrapperResourceDecoder(dataLoadProvider.m25999(), dataLoadProvider2.m25999(), bitmapPool);
        this.f20058 = new FileToStreamDecoder(new GifBitmapWrapperStreamResourceDecoder(gifBitmapWrapperResourceDecoder));
        this.f20055 = gifBitmapWrapperResourceDecoder;
        this.f20057 = new GifBitmapWrapperResourceEncoder(dataLoadProvider.m26000(), dataLoadProvider2.m26000());
        this.f20056 = dataLoadProvider.m26001();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> m25921() {
        return this.f20055;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<GifBitmapWrapper> m25922() {
        return this.f20057;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<ImageVideoWrapper> m25923() {
        return this.f20056;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, GifBitmapWrapper> m25924() {
        return this.f20058;
    }
}
