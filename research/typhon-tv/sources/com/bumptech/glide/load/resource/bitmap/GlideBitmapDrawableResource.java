package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.drawable.DrawableResource;
import com.bumptech.glide.util.Util;

public class GlideBitmapDrawableResource extends DrawableResource<GlideBitmapDrawable> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapPool f19946;

    public GlideBitmapDrawableResource(GlideBitmapDrawable glideBitmapDrawable, BitmapPool bitmapPool) {
        super(glideBitmapDrawable);
        this.f19946 = bitmapPool;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25769() {
        this.f19946.m25516(((GlideBitmapDrawable) this.f19974).m25763());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25770() {
        return Util.m26196(((GlideBitmapDrawable) this.f19974).m25763());
    }
}
