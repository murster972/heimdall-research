package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.ImageHeaderParser;
import com.bumptech.glide.util.ByteArrayPool;
import com.bumptech.glide.util.ExceptionCatchingInputStream;
import com.bumptech.glide.util.MarkEnforcingInputStream;
import com.bumptech.glide.util.Util;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.Queue;
import java.util.Set;

public abstract class Downsampler implements BitmapDecoder<InputStream> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Queue<BitmapFactory.Options> f19924 = Util.m26200(0);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Downsampler f19925 = new Downsampler() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m25750(int i, int i2, int i3, int i4) {
            int i5 = 1;
            int ceil = (int) Math.ceil((double) Math.max(((float) i2) / ((float) i4), ((float) i) / ((float) i3)));
            int max = Math.max(1, Integer.highestOneBit(ceil));
            if (max >= ceil) {
                i5 = 0;
            }
            return max << i5;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m25751() {
            return "AT_MOST.com.bumptech.glide.load.data.bitmap";
        }
    };

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Set<ImageHeaderParser.ImageType> f19926 = EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Downsampler f19927 = new Downsampler() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m25752(int i, int i2, int i3, int i4) {
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m25753() {
            return "NONE.com.bumptech.glide.load.data.bitmap";
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Downsampler f19928 = new Downsampler() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m25748(int i, int i2, int i3, int i4) {
            return Math.min(i2 / i4, i / i3);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m25749() {
            return "AT_LEAST.com.bumptech.glide.load.data.bitmap";
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private static Bitmap m25736(MarkEnforcingInputStream markEnforcingInputStream, RecyclableBufferedInputStream recyclableBufferedInputStream, BitmapFactory.Options options) {
        if (options.inJustDecodeBounds) {
            markEnforcingInputStream.mark(5242880);
        } else {
            recyclableBufferedInputStream.m25796();
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(markEnforcingInputStream, (Rect) null, options);
        try {
            if (options.inJustDecodeBounds) {
                markEnforcingInputStream.reset();
            }
        } catch (IOException e) {
            if (Log.isLoggable("Downsampler", 6)) {
                Log.e("Downsampler", "Exception loading inDecodeBounds=" + options.inJustDecodeBounds + " sample=" + options.inSampleSize, e);
            }
        }
        return decodeStream;
    }

    @TargetApi(11)
    /* renamed from: 靐  reason: contains not printable characters */
    private static synchronized BitmapFactory.Options m25737() {
        BitmapFactory.Options poll;
        synchronized (Downsampler.class) {
            synchronized (f19924) {
                poll = f19924.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                m25738(poll);
            }
        }
        return poll;
    }

    @TargetApi(11)
    /* renamed from: 靐  reason: contains not printable characters */
    private static void m25738(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        if (11 <= Build.VERSION.SDK_INT) {
            options.inBitmap = null;
            options.inMutable = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m25739(int i, int i2, int i3, int i4, int i5) {
        int i6 = i5 == Integer.MIN_VALUE ? i3 : i5;
        int i7 = i4 == Integer.MIN_VALUE ? i2 : i4;
        int r0 = (i == 90 || i == 270) ? m25745(i3, i2, i7, i6) : m25745(i2, i3, i7, i6);
        return Math.max(1, r0 == 0 ? 0 : Integer.highestOneBit(r0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Bitmap.Config m25740(InputStream inputStream, DecodeFormat decodeFormat) {
        if (decodeFormat == DecodeFormat.ALWAYS_ARGB_8888 || decodeFormat == DecodeFormat.PREFER_ARGB_8888 || Build.VERSION.SDK_INT == 16) {
            return Bitmap.Config.ARGB_8888;
        }
        boolean z = false;
        inputStream.mark(1024);
        try {
            z = new ImageHeaderParser(inputStream).m25777();
            try {
                inputStream.reset();
            } catch (IOException e) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e);
                }
            }
        } catch (IOException e2) {
            if (Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Cannot determine whether the image has alpha or not from header for format " + decodeFormat, e2);
            }
            try {
                inputStream.reset();
            } catch (IOException e3) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e3);
                }
            }
        } catch (Throwable th) {
            try {
                inputStream.reset();
            } catch (IOException e4) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e4);
                }
            }
            throw th;
        }
        return z ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Bitmap m25741(MarkEnforcingInputStream markEnforcingInputStream, RecyclableBufferedInputStream recyclableBufferedInputStream, BitmapFactory.Options options, BitmapPool bitmapPool, int i, int i2, int i3, DecodeFormat decodeFormat) {
        Bitmap.Config r2 = m25740((InputStream) markEnforcingInputStream, decodeFormat);
        options.inSampleSize = i3;
        options.inPreferredConfig = r2;
        if ((options.inSampleSize == 1 || 19 <= Build.VERSION.SDK_INT) && m25744((InputStream) markEnforcingInputStream)) {
            m25743(options, bitmapPool.m25512((int) Math.ceil(((double) i) / ((double) i3)), (int) Math.ceil(((double) i2) / ((double) i3)), r2));
        }
        return m25736(markEnforcingInputStream, recyclableBufferedInputStream, options);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25742(BitmapFactory.Options options) {
        m25738(options);
        synchronized (f19924) {
            f19924.offer(options);
        }
    }

    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25743(BitmapFactory.Options options, Bitmap bitmap) {
        if (11 <= Build.VERSION.SDK_INT) {
            options.inBitmap = bitmap;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m25744(InputStream inputStream) {
        if (19 <= Build.VERSION.SDK_INT) {
            return true;
        }
        inputStream.mark(1024);
        try {
            boolean contains = f19926.contains(new ImageHeaderParser(inputStream).m25775());
            try {
                inputStream.reset();
                return contains;
            } catch (IOException e) {
                if (!Log.isLoggable("Downsampler", 5)) {
                    return contains;
                }
                Log.w("Downsampler", "Cannot reset the input stream", e);
                return contains;
            }
        } catch (IOException e2) {
            if (Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Cannot determine the image type from header", e2);
            }
            try {
                inputStream.reset();
            } catch (IOException e3) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e3);
                }
            }
            return false;
        } catch (Throwable th) {
            try {
                inputStream.reset();
            } catch (IOException e4) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e4);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m25745(int i, int i2, int i3, int i4);

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25746(InputStream inputStream, BitmapPool bitmapPool, int i, int i2, DecodeFormat decodeFormat) {
        int i3;
        ByteArrayPool r16 = ByteArrayPool.m26167();
        byte[] r17 = r16.m26168();
        byte[] r18 = r16.m26168();
        BitmapFactory.Options r10 = m25737();
        RecyclableBufferedInputStream recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, r18);
        ExceptionCatchingInputStream r21 = ExceptionCatchingInputStream.m26172(recyclableBufferedInputStream);
        MarkEnforcingInputStream markEnforcingInputStream = new MarkEnforcingInputStream(r21);
        try {
            r21.mark(5242880);
            i3 = 0;
            try {
                i3 = new ImageHeaderParser(r21).m25776();
                r21.reset();
            } catch (IOException e) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot determine the image orientation from header", e);
                }
                try {
                    r21.reset();
                } catch (IOException e2) {
                    if (Log.isLoggable("Downsampler", 5)) {
                        Log.w("Downsampler", "Cannot reset the input stream", e2);
                    }
                }
            } catch (Throwable th) {
                try {
                    r21.reset();
                } catch (IOException e3) {
                    if (Log.isLoggable("Downsampler", 5)) {
                        Log.w("Downsampler", "Cannot reset the input stream", e3);
                    }
                }
                throw th;
            }
        } catch (IOException e4) {
            if (Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Cannot reset the input stream", e4);
            }
        } catch (Throwable th2) {
            r16.m26169(r17);
            r16.m26169(r18);
            r21.m26173();
            m25742(r10);
            throw th2;
        }
        r10.inTempStorage = r17;
        int[] r22 = m25747(markEnforcingInputStream, recyclableBufferedInputStream, r10);
        int i4 = r22[0];
        int i5 = r22[1];
        Bitmap r19 = m25741(markEnforcingInputStream, recyclableBufferedInputStream, r10, bitmapPool, i4, i5, m25739(TransformationUtils.m25804(i3), i4, i5, i, i2), decodeFormat);
        IOException r26 = r21.m26175();
        if (r26 != null) {
            throw new RuntimeException(r26);
        }
        Bitmap bitmap = null;
        if (r19 != null) {
            bitmap = TransformationUtils.m25807(r19, bitmapPool, i3);
            if (!r19.equals(bitmap) && !bitmapPool.m25516(r19)) {
                r19.recycle();
            }
        }
        r16.m26169(r17);
        r16.m26169(r18);
        r21.m26173();
        m25742(r10);
        return bitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m25747(MarkEnforcingInputStream markEnforcingInputStream, RecyclableBufferedInputStream recyclableBufferedInputStream, BitmapFactory.Options options) {
        options.inJustDecodeBounds = true;
        m25736(markEnforcingInputStream, recyclableBufferedInputStream, options);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }
}
