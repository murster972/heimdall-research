package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

public class CenterCrop extends BitmapTransformation {
    public CenterCrop(Context context) {
        super(context);
    }

    public CenterCrop(BitmapPool bitmapPool) {
        super(bitmapPool);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25734(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2) {
        Bitmap r0 = bitmapPool.m25513(i, i2, bitmap.getConfig() != null ? bitmap.getConfig() : Bitmap.Config.ARGB_8888);
        Bitmap r1 = TransformationUtils.m25806(r0, bitmap, i, i2);
        if (!(r0 == null || r0 == r1 || bitmapPool.m25516(r0))) {
            r0.recycle();
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25735() {
        return "CenterCrop.com.bumptech.glide.load.resource.bitmap";
    }
}
