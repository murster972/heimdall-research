package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;

class GifFrameResourceDecoder implements ResourceDecoder<GifDecoder, Bitmap> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BitmapPool f20024;

    public GifFrameResourceDecoder(BitmapPool bitmapPool) {
        this.f20024 = bitmapPool;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<Bitmap> m25875(GifDecoder gifDecoder, int i, int i2) {
        return BitmapResource.m25727(gifDecoder.m25305(), this.f20024);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25876() {
        return "GifFrameResourceDecoder.com.bumptech.glide.load.resource.gif";
    }
}
