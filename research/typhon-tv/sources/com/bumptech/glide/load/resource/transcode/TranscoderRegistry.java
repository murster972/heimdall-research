package com.bumptech.glide.load.resource.transcode;

import com.bumptech.glide.util.MultiClassKey;
import java.util.HashMap;
import java.util.Map;

public class TranscoderRegistry {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final MultiClassKey f20062 = new MultiClassKey();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<MultiClassKey, ResourceTranscoder<?, ?>> f20063 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    public <Z, R> ResourceTranscoder<Z, R> m25931(Class<Z> cls, Class<R> cls2) {
        ResourceTranscoder<Z, R> resourceTranscoder;
        if (cls.equals(cls2)) {
            return UnitTranscoder.m25933();
        }
        synchronized (f20062) {
            f20062.m26189(cls, cls2);
            resourceTranscoder = this.f20063.get(f20062);
        }
        if (resourceTranscoder != null) {
            return resourceTranscoder;
        }
        throw new IllegalArgumentException("No transcoder registered for " + cls + " and " + cls2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <Z, R> void m25932(Class<Z> cls, Class<R> cls2, ResourceTranscoder<Z, R> resourceTranscoder) {
        this.f20063.put(new MultiClassKey(cls, cls2), resourceTranscoder);
    }
}
