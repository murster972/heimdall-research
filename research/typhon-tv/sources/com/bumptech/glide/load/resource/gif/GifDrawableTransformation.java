package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;

public class GifDrawableTransformation implements Transformation<GifDrawable> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BitmapPool f20007;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Transformation<Bitmap> f20008;

    public GifDrawableTransformation(Transformation<Bitmap> transformation, BitmapPool bitmapPool) {
        this.f20008 = transformation;
        this.f20007 = bitmapPool;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<GifDrawable> m25852(Resource<GifDrawable> resource, int i, int i2) {
        GifDrawable r1 = resource.m25491();
        Bitmap r2 = resource.m25491().m25840();
        Bitmap r4 = this.f20008.m25374(new BitmapResource(r2, this.f20007), i, i2).m25491();
        return !r4.equals(r2) ? new GifDrawableResource(new GifDrawable(r1, r4, this.f20008)) : resource;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25853() {
        return this.f20008.m25375();
    }
}
