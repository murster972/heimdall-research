package com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RecyclableBufferedInputStream extends FilterInputStream {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f19958;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f19959;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f19960 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f19961;

    /* renamed from: 龘  reason: contains not printable characters */
    private volatile byte[] f19962;

    public static class InvalidMarkException extends RuntimeException {
        private static final long serialVersionUID = -4338378848813561757L;

        public InvalidMarkException(String str) {
            super(str);
        }
    }

    public RecyclableBufferedInputStream(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("buffer is null or empty");
        }
        this.f19962 = bArr;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static IOException m25794() throws IOException {
        throw new IOException("BufferedInputStream is closed");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m25795(InputStream inputStream, byte[] bArr) throws IOException {
        if (this.f19960 == -1 || this.f19958 - this.f19960 >= this.f19961) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                this.f19960 = -1;
                this.f19958 = 0;
                this.f19959 = read;
            }
            return read;
        }
        if (this.f19960 == 0 && this.f19961 > bArr.length && this.f19959 == bArr.length) {
            int length = bArr.length * 2;
            if (length > this.f19961) {
                length = this.f19961;
            }
            if (Log.isLoggable("BufferedIs", 3)) {
                Log.d("BufferedIs", "allocate buffer of length: " + length);
            }
            byte[] bArr2 = new byte[length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            this.f19962 = bArr2;
            bArr = bArr2;
        } else if (this.f19960 > 0) {
            System.arraycopy(bArr, this.f19960, bArr, 0, bArr.length - this.f19960);
        }
        this.f19958 -= this.f19960;
        this.f19960 = 0;
        this.f19959 = 0;
        int read2 = inputStream.read(bArr, this.f19958, bArr.length - this.f19958);
        this.f19959 = read2 <= 0 ? this.f19958 : this.f19958 + read2;
        return read2;
    }

    public synchronized int available() throws IOException {
        InputStream inputStream;
        inputStream = this.in;
        if (this.f19962 == null || inputStream == null) {
            throw m25794();
        }
        return (this.f19959 - this.f19958) + inputStream.available();
    }

    public void close() throws IOException {
        this.f19962 = null;
        InputStream inputStream = this.in;
        this.in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    public synchronized void mark(int i) {
        this.f19961 = Math.max(this.f19961, i);
        this.f19960 = this.f19958;
    }

    public boolean markSupported() {
        return true;
    }

    public synchronized int read() throws IOException {
        byte b = -1;
        synchronized (this) {
            byte[] bArr = this.f19962;
            InputStream inputStream = this.in;
            if (bArr == null || inputStream == null) {
                throw m25794();
            } else if (this.f19958 < this.f19959 || m25795(inputStream, bArr) != -1) {
                if (bArr != this.f19962 && (bArr = this.f19962) == null) {
                    throw m25794();
                } else if (this.f19959 - this.f19958 > 0) {
                    int i = this.f19958;
                    this.f19958 = i + 1;
                    b = bArr[i] & 255;
                }
            }
        }
        return b;
    }

    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5 = -1;
        synchronized (this) {
            byte[] bArr2 = this.f19962;
            if (bArr2 == null) {
                throw m25794();
            } else if (i2 == 0) {
                i5 = 0;
            } else {
                InputStream inputStream = this.in;
                if (inputStream == null) {
                    throw m25794();
                }
                if (this.f19958 < this.f19959) {
                    int i6 = this.f19959 - this.f19958 >= i2 ? i2 : this.f19959 - this.f19958;
                    System.arraycopy(bArr2, this.f19958, bArr, i, i6);
                    this.f19958 += i6;
                    if (i6 == i2 || inputStream.available() == 0) {
                        i5 = i6;
                    } else {
                        i += i6;
                        i3 = i2 - i6;
                    }
                } else {
                    i3 = i2;
                }
                while (true) {
                    if (this.f19960 == -1 && i3 >= bArr2.length) {
                        i4 = inputStream.read(bArr, i, i3);
                        if (i4 == -1) {
                            if (i3 != i2) {
                                i5 = i2 - i3;
                            }
                        }
                    } else if (m25795(inputStream, bArr2) == -1) {
                        if (i3 != i2) {
                            i5 = i2 - i3;
                        }
                    } else if (bArr2 == this.f19962 || (bArr2 = this.f19962) != null) {
                        i4 = this.f19959 - this.f19958 >= i3 ? i3 : this.f19959 - this.f19958;
                        System.arraycopy(bArr2, this.f19958, bArr, i, i4);
                        this.f19958 += i4;
                    } else {
                        throw m25794();
                    }
                    i3 -= i4;
                    if (i3 == 0) {
                        i5 = i2;
                        break;
                    } else if (inputStream.available() == 0) {
                        i5 = i2 - i3;
                        break;
                    } else {
                        i += i4;
                    }
                }
            }
        }
        return i5;
    }

    public synchronized void reset() throws IOException {
        if (this.f19962 == null) {
            throw new IOException("Stream is closed");
        } else if (-1 == this.f19960) {
            throw new InvalidMarkException("Mark has been invalidated");
        } else {
            this.f19958 = this.f19960;
        }
    }

    public synchronized long skip(long j) throws IOException {
        byte[] bArr = this.f19962;
        InputStream inputStream = this.in;
        if (bArr == null) {
            throw m25794();
        } else if (j < 1) {
            j = 0;
        } else if (inputStream == null) {
            throw m25794();
        } else if (((long) (this.f19959 - this.f19958)) >= j) {
            this.f19958 = (int) (((long) this.f19958) + j);
        } else {
            long j2 = (long) (this.f19959 - this.f19958);
            this.f19958 = this.f19959;
            if (this.f19960 == -1 || j > ((long) this.f19961)) {
                j = j2 + inputStream.skip(j - j2);
            } else if (m25795(inputStream, bArr) == -1) {
                j = j2;
            } else if (((long) (this.f19959 - this.f19958)) >= j - j2) {
                this.f19958 = (int) (((long) this.f19958) + (j - j2));
            } else {
                long j3 = (((long) this.f19959) + j2) - ((long) this.f19958);
                this.f19958 = this.f19959;
                j = j3;
            }
        }
        return j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m25796() {
        this.f19961 = this.f19962.length;
    }
}
