package com.bumptech.glide.load.resource.gifbitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;

public class GifBitmapWrapper {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Resource<Bitmap> f20038;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Resource<GifDrawable> f20039;

    public GifBitmapWrapper(Resource<Bitmap> resource, Resource<GifDrawable> resource2) {
        if (resource != null && resource2 != null) {
            throw new IllegalArgumentException("Can only contain either a bitmap resource or a gif resource, not both");
        } else if (resource == null && resource2 == null) {
            throw new IllegalArgumentException("Must contain either a bitmap resource or a gif resource");
        } else {
            this.f20038 = resource;
            this.f20039 = resource2;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Resource<Bitmap> m25897() {
        return this.f20038;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Resource<GifDrawable> m25898() {
        return this.f20039;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m25899() {
        return this.f20038 != null ? this.f20038.m25493() : this.f20039.m25493();
    }
}
