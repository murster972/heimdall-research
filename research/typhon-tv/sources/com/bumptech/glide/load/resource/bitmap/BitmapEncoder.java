package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Util;
import java.io.OutputStream;

public class BitmapEncoder implements ResourceEncoder<Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f19919;

    /* renamed from: 龘  reason: contains not printable characters */
    private Bitmap.CompressFormat f19920;

    public BitmapEncoder() {
        this((Bitmap.CompressFormat) null, 90);
    }

    public BitmapEncoder(Bitmap.CompressFormat compressFormat, int i) {
        this.f19920 = compressFormat;
        this.f19919 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Bitmap.CompressFormat m25723(Bitmap bitmap) {
        return this.f19920 != null ? this.f19920 : bitmap.hasAlpha() ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25724() {
        return "BitmapEncoder.com.bumptech.glide.load.resource.bitmap";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25726(Resource<Bitmap> resource, OutputStream outputStream) {
        Bitmap r0 = resource.m25491();
        long r2 = LogTime.m26177();
        Bitmap.CompressFormat r1 = m25723(r0);
        r0.compress(r1, this.f19919, outputStream);
        if (!Log.isLoggable("BitmapEncoder", 2)) {
            return true;
        }
        Log.v("BitmapEncoder", "Compressed with type: " + r1 + " of size " + Util.m26196(r0) + " in " + LogTime.m26176(r2));
        return true;
    }
}
