package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.gifdecoder.GifHeader;
import com.bumptech.glide.gifdecoder.GifHeaderParser;
import com.bumptech.glide.gifencoder.AnimatedGifEncoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.UnitTransformation;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.util.LogTime;
import java.io.IOException;
import java.io.OutputStream;

public class GifResourceEncoder implements ResourceEncoder<GifDrawable> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Factory f20034 = new Factory();

    /* renamed from: 靐  reason: contains not printable characters */
    private final GifDecoder.BitmapProvider f20035;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Factory f20036;

    /* renamed from: 齉  reason: contains not printable characters */
    private final BitmapPool f20037;

    static class Factory {
        Factory() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public AnimatedGifEncoder m25893() {
            return new AnimatedGifEncoder();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public GifDecoder m25894(GifDecoder.BitmapProvider bitmapProvider) {
            return new GifDecoder(bitmapProvider);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public GifHeaderParser m25895() {
            return new GifHeaderParser();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Resource<Bitmap> m25896(Bitmap bitmap, BitmapPool bitmapPool) {
            return new BitmapResource(bitmap, bitmapPool);
        }
    }

    public GifResourceEncoder(BitmapPool bitmapPool) {
        this(bitmapPool, f20034);
    }

    GifResourceEncoder(BitmapPool bitmapPool, Factory factory) {
        this.f20037 = bitmapPool;
        this.f20035 = new GifBitmapProvider(bitmapPool);
        this.f20036 = factory;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GifDecoder m25887(byte[] bArr) {
        GifHeaderParser r2 = this.f20036.m25895();
        r2.m25333(bArr);
        GifHeader r1 = r2.m25332();
        GifDecoder r0 = this.f20036.m25894(this.f20035);
        r0.m25313(r1, bArr);
        r0.m25312();
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Resource<Bitmap> m25888(Bitmap bitmap, Transformation<Bitmap> transformation, GifDrawable gifDrawable) {
        Resource r0 = this.f20036.m25896(bitmap, this.f20037);
        Resource<Bitmap> r1 = transformation.m25374(r0, gifDrawable.getIntrinsicWidth(), gifDrawable.getIntrinsicHeight());
        if (!r0.equals(r1)) {
            r0.m25492();
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m25889(byte[] bArr, OutputStream outputStream) {
        try {
            outputStream.write(bArr);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 3)) {
                Log.d("GifEncoder", "Failed to write data to output stream in GifResourceEncoder", e);
            }
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25890() {
        return "";
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25892(Resource<GifDrawable> resource, OutputStream outputStream) {
        long r10 = LogTime.m26177();
        GifDrawable r6 = resource.m25491();
        Transformation<Bitmap> r12 = r6.m25843();
        if (r12 instanceof UnitTransformation) {
            return m25889(r6.m25842(), outputStream);
        }
        GifDecoder r4 = m25887(r6.m25842());
        AnimatedGifEncoder r7 = this.f20036.m25893();
        if (!r7.m25350(outputStream)) {
            return false;
        }
        int i = 0;
        while (i < r4.m25310()) {
            Resource<Bitmap> r13 = m25888(r4.m25305(), r12, r6);
            try {
                if (!r7.m25349(r13.m25491())) {
                    r13.m25492();
                    return false;
                }
                r7.m25346(r4.m25311(r4.m25309()));
                r4.m25312();
                r13.m25492();
                i++;
            } catch (Throwable th) {
                r13.m25492();
                throw th;
            }
        }
        boolean r9 = r7.m25348();
        if (!Log.isLoggable("GifEncoder", 2)) {
            return r9;
        }
        Log.v("GifEncoder", "Encoded gif with " + r4.m25310() + " frames and " + r6.m25842().length + " bytes in " + LogTime.m26176(r10) + " ms");
        return r9;
    }
}
