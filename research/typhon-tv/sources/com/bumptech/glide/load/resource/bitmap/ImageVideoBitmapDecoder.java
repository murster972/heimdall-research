package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import java.io.InputStream;

public class ImageVideoBitmapDecoder implements ResourceDecoder<ImageVideoWrapper, Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ResourceDecoder<ParcelFileDescriptor, Bitmap> f19952;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ResourceDecoder<InputStream, Bitmap> f19953;

    public ImageVideoBitmapDecoder(ResourceDecoder<InputStream, Bitmap> resourceDecoder, ResourceDecoder<ParcelFileDescriptor, Bitmap> resourceDecoder2) {
        this.f19953 = resourceDecoder;
        this.f19952 = resourceDecoder2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        r1 = r7.m25646();
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.bumptech.glide.load.engine.Resource<android.graphics.Bitmap> m25788(com.bumptech.glide.load.model.ImageVideoWrapper r7, int r8, int r9) throws java.io.IOException {
        /*
            r6 = this;
            r3 = 0
            java.io.InputStream r2 = r7.m25647()
            if (r2 == 0) goto L_0x000d
            com.bumptech.glide.load.ResourceDecoder<java.io.InputStream, android.graphics.Bitmap> r4 = r6.f19953     // Catch:{ IOException -> 0x001c }
            com.bumptech.glide.load.engine.Resource r3 = r4.m25372(r2, r8, r9)     // Catch:{ IOException -> 0x001c }
        L_0x000d:
            if (r3 != 0) goto L_0x001b
            android.os.ParcelFileDescriptor r1 = r7.m25646()
            if (r1 == 0) goto L_0x001b
            com.bumptech.glide.load.ResourceDecoder<android.os.ParcelFileDescriptor, android.graphics.Bitmap> r4 = r6.f19952
            com.bumptech.glide.load.engine.Resource r3 = r4.m25372(r1, r8, r9)
        L_0x001b:
            return r3
        L_0x001c:
            r0 = move-exception
            java.lang.String r4 = "ImageVideoDecoder"
            r5 = 2
            boolean r4 = android.util.Log.isLoggable(r4, r5)
            if (r4 == 0) goto L_0x000d
            java.lang.String r4 = "ImageVideoDecoder"
            java.lang.String r5 = "Failed to load image from stream, trying FileDescriptor"
            android.util.Log.v(r4, r5, r0)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.resource.bitmap.ImageVideoBitmapDecoder.m25788(com.bumptech.glide.load.model.ImageVideoWrapper, int, int):com.bumptech.glide.load.engine.Resource");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25789() {
        return "ImageVideoBitmapDecoder.com.bumptech.glide.load.resource.bitmap";
    }
}
