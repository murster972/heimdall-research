package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.io.InputStream;

public class StreamBitmapDecoder implements ResourceDecoder<InputStream, Bitmap> {

    /* renamed from: 靐  reason: contains not printable characters */
    private BitmapPool f19967;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f19968;

    /* renamed from: 齉  reason: contains not printable characters */
    private DecodeFormat f19969;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Downsampler f19970;

    public StreamBitmapDecoder(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this(Downsampler.f19928, bitmapPool, decodeFormat);
    }

    public StreamBitmapDecoder(Downsampler downsampler, BitmapPool bitmapPool, DecodeFormat decodeFormat) {
        this.f19970 = downsampler;
        this.f19967 = bitmapPool;
        this.f19969 = decodeFormat;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<Bitmap> m25802(InputStream inputStream, int i, int i2) {
        return BitmapResource.m25727(this.f19970.m25746(inputStream, this.f19967, i, i2, this.f19969), this.f19967);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25803() {
        if (this.f19968 == null) {
            this.f19968 = "StreamBitmapDecoder.com.bumptech.glide.load.resource.bitmap" + this.f19970.m25722() + this.f19969.name();
        }
        return this.f19968;
    }
}
