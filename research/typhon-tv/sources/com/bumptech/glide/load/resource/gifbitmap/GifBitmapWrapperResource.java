package com.bumptech.glide.load.resource.gifbitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;

public class GifBitmapWrapperResource implements Resource<GifBitmapWrapper> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final GifBitmapWrapper f20040;

    public GifBitmapWrapperResource(GifBitmapWrapper gifBitmapWrapper) {
        if (gifBitmapWrapper == null) {
            throw new NullPointerException("Data must not be null");
        }
        this.f20040 = gifBitmapWrapper;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25901() {
        Resource<Bitmap> r0 = this.f20040.m25897();
        if (r0 != null) {
            r0.m25492();
        }
        Resource<GifDrawable> r1 = this.f20040.m25898();
        if (r1 != null) {
            r1.m25492();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25902() {
        return this.f20040.m25899();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GifBitmapWrapper m25900() {
        return this.f20040;
    }
}
