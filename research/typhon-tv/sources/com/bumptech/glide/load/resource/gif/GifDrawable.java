package com.bumptech.glide.load.resource.gif;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.gifdecoder.GifHeader;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gif.GifFrameLoader;

public class GifDrawable extends GlideDrawable implements GifFrameLoader.FrameCallback {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f19982;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f19983;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f19984;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f19985;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f19986;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f19987;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f19988;

    /* renamed from: 连任  reason: contains not printable characters */
    private final GifFrameLoader f19989;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Rect f19990;

    /* renamed from: 麤  reason: contains not printable characters */
    private final GifDecoder f19991;

    /* renamed from: 齉  reason: contains not printable characters */
    private final GifState f19992;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f19993;

    static class GifState extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f19994;

        /* renamed from: ʼ  reason: contains not printable characters */
        GifDecoder.BitmapProvider f19995;

        /* renamed from: ʽ  reason: contains not printable characters */
        BitmapPool f19996;

        /* renamed from: ˑ  reason: contains not printable characters */
        Bitmap f19997;

        /* renamed from: 连任  reason: contains not printable characters */
        int f19998;

        /* renamed from: 靐  reason: contains not printable characters */
        byte[] f19999;

        /* renamed from: 麤  reason: contains not printable characters */
        Transformation<Bitmap> f20000;

        /* renamed from: 齉  reason: contains not printable characters */
        Context f20001;

        /* renamed from: 龘  reason: contains not printable characters */
        GifHeader f20002;

        public GifState(GifHeader gifHeader, byte[] bArr, Context context, Transformation<Bitmap> transformation, int i, int i2, GifDecoder.BitmapProvider bitmapProvider, BitmapPool bitmapPool, Bitmap bitmap) {
            if (bitmap == null) {
                throw new NullPointerException("The first frame of the GIF must not be null");
            }
            this.f20002 = gifHeader;
            this.f19999 = bArr;
            this.f19996 = bitmapPool;
            this.f19997 = bitmap;
            this.f20001 = context.getApplicationContext();
            this.f20000 = transformation;
            this.f19998 = i;
            this.f19994 = i2;
            this.f19995 = bitmapProvider;
        }

        public int getChangingConfigurations() {
            return 0;
        }

        public Drawable newDrawable() {
            return new GifDrawable(this);
        }

        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }
    }

    public GifDrawable(Context context, GifDecoder.BitmapProvider bitmapProvider, BitmapPool bitmapPool, Transformation<Bitmap> transformation, int i, int i2, GifHeader gifHeader, byte[] bArr, Bitmap bitmap) {
        this(new GifState(gifHeader, bArr, context, transformation, i, i2, bitmapProvider, bitmapPool, bitmap));
    }

    GifDrawable(GifState gifState) {
        this.f19990 = new Rect();
        this.f19986 = true;
        this.f19988 = -1;
        if (gifState == null) {
            throw new NullPointerException("GifState must not be null");
        }
        this.f19992 = gifState;
        this.f19991 = new GifDecoder(gifState.f19995);
        this.f19993 = new Paint();
        this.f19991.m25313(gifState.f20002, gifState.f19999);
        this.f19989 = new GifFrameLoader(gifState.f20001, this, this.f19991, gifState.f19998, gifState.f19994);
        this.f19989.m25860(gifState.f20000);
    }

    public GifDrawable(GifDrawable gifDrawable, Bitmap bitmap, Transformation<Bitmap> transformation) {
        this(new GifState(gifDrawable.f19992.f20002, gifDrawable.f19992.f19999, gifDrawable.f19992.f20001, transformation, gifDrawable.f19992.f19998, gifDrawable.f19992.f19994, gifDrawable.f19992.f19995, gifDrawable.f19992.f19996, bitmap));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m25834() {
        this.f19987 = 0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m25835() {
        this.f19989.m25858();
        invalidateSelf();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m25836() {
        if (this.f19991.m25310() == 1) {
            invalidateSelf();
        } else if (!this.f19982) {
            this.f19982 = true;
            this.f19989.m25859();
            invalidateSelf();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m25837() {
        this.f19982 = false;
        this.f19989.m25856();
    }

    public void draw(Canvas canvas) {
        if (!this.f19984) {
            if (this.f19985) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), this.f19990);
                this.f19985 = false;
            }
            Bitmap r0 = this.f19989.m25857();
            canvas.drawBitmap(r0 != null ? r0 : this.f19992.f19997, (Rect) null, this.f19990, this.f19993);
        }
    }

    public Drawable.ConstantState getConstantState() {
        return this.f19992;
    }

    public int getIntrinsicHeight() {
        return this.f19992.f19997.getHeight();
    }

    public int getIntrinsicWidth() {
        return this.f19992.f19997.getWidth();
    }

    public int getOpacity() {
        return -2;
    }

    public boolean isRunning() {
        return this.f19982;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f19985 = true;
    }

    public void setAlpha(int i) {
        this.f19993.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f19993.setColorFilter(colorFilter);
    }

    public boolean setVisible(boolean z, boolean z2) {
        this.f19986 = z;
        if (!z) {
            m25837();
        } else if (this.f19983) {
            m25836();
        }
        return super.setVisible(z, z2);
    }

    public void start() {
        this.f19983 = true;
        m25834();
        if (this.f19986) {
            m25836();
        }
    }

    public void stop() {
        this.f19983 = false;
        m25837();
        if (Build.VERSION.SDK_INT < 11) {
            m25835();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m25838() {
        this.f19984 = true;
        this.f19992.f19996.m25516(this.f19992.f19997);
        this.f19989.m25858();
        this.f19989.m25856();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m25839() {
        return this.f19991.m25310();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Bitmap m25840() {
        return this.f19992.f19997;
    }

    @TargetApi(11)
    /* renamed from: 靐  reason: contains not printable characters */
    public void m25841(int i) {
        if (Build.VERSION.SDK_INT < 11 || getCallback() != null) {
            invalidateSelf();
            if (i == this.f19991.m25310() - 1) {
                this.f19987++;
            }
            if (this.f19988 != -1 && this.f19987 >= this.f19988) {
                stop();
                return;
            }
            return;
        }
        stop();
        m25835();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public byte[] m25842() {
        return this.f19992.f19999;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Transformation<Bitmap> m25843() {
        return this.f19992.f20000;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25844(int i) {
        if (i <= 0 && i != -1 && i != 0) {
            throw new IllegalArgumentException("Loop count must be greater than 0, or equal to GlideDrawable.LOOP_FOREVER, or equal to GlideDrawable.LOOP_INTRINSIC");
        } else if (i == 0) {
            this.f19988 = this.f19991.m25307();
        } else {
            this.f19988 = i;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25845() {
        return true;
    }
}
