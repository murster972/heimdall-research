package com.bumptech.glide.load.resource.file;

import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileToStreamDecoder<T> implements ResourceDecoder<File, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final FileOpener f19975 = new FileOpener();

    /* renamed from: 靐  reason: contains not printable characters */
    private ResourceDecoder<InputStream, T> f19976;

    /* renamed from: 齉  reason: contains not printable characters */
    private final FileOpener f19977;

    static class FileOpener {
        FileOpener() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public InputStream m25824(File file) throws FileNotFoundException {
            return new FileInputStream(file);
        }
    }

    public FileToStreamDecoder(ResourceDecoder<InputStream, T> resourceDecoder) {
        this(resourceDecoder, f19975);
    }

    FileToStreamDecoder(ResourceDecoder<InputStream, T> resourceDecoder, FileOpener fileOpener) {
        this.f19976 = resourceDecoder;
        this.f19977 = fileOpener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<T> m25822(File file, int i, int i2) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = this.f19977.m25824(file);
            return this.f19976.m25372(inputStream, i, i2);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25823() {
        return "";
    }
}
