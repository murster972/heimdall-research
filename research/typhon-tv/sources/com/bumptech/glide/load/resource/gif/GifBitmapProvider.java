package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

class GifBitmapProvider implements GifDecoder.BitmapProvider {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BitmapPool f19981;

    public GifBitmapProvider(BitmapPool bitmapPool) {
        this.f19981 = bitmapPool;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25832(int i, int i2, Bitmap.Config config) {
        return this.f19981.m25512(i, i2, config);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25833(Bitmap bitmap) {
        if (!this.f19981.m25516(bitmap)) {
            bitmap.recycle();
        }
    }
}
