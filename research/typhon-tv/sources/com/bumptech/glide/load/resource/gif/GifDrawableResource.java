package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.load.resource.drawable.DrawableResource;
import com.bumptech.glide.util.Util;

public class GifDrawableResource extends DrawableResource<GifDrawable> {
    public GifDrawableResource(GifDrawable gifDrawable) {
        super(gifDrawable);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25850() {
        ((GifDrawable) this.f19974).stop();
        ((GifDrawable) this.f19974).m25838();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25851() {
        return Util.m26196(((GifDrawable) this.f19974).m25840()) + ((GifDrawable) this.f19974).m25842().length;
    }
}
