package com.bumptech.glide.load.resource.gifbitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawableTransformation;

public class GifBitmapWrapperTransformation implements Transformation<GifBitmapWrapper> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Transformation<GifDrawable> f20053;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Transformation<Bitmap> f20054;

    GifBitmapWrapperTransformation(Transformation<Bitmap> transformation, Transformation<GifDrawable> transformation2) {
        this.f20054 = transformation;
        this.f20053 = transformation2;
    }

    public GifBitmapWrapperTransformation(BitmapPool bitmapPool, Transformation<Bitmap> transformation) {
        this(transformation, (Transformation<GifDrawable>) new GifDrawableTransformation(transformation, bitmapPool));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<GifBitmapWrapper> m25919(Resource<GifBitmapWrapper> resource, int i, int i2) {
        Resource<Bitmap> r0 = resource.m25491().m25897();
        Resource<GifDrawable> r2 = resource.m25491().m25898();
        if (r0 != null && this.f20054 != null) {
            Resource<Bitmap> r3 = this.f20054.m25374(r0, i, i2);
            return !r0.equals(r3) ? new GifBitmapWrapperResource(new GifBitmapWrapper(r3, resource.m25491().m25898())) : resource;
        } else if (r2 == null || this.f20053 == null) {
            return resource;
        } else {
            Resource<GifDrawable> r4 = this.f20053.m25374(r2, i, i2);
            return !r2.equals(r4) ? new GifBitmapWrapperResource(new GifBitmapWrapper(resource.m25491().m25897(), r4)) : resource;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25920() {
        return this.f20054.m25375();
    }
}
