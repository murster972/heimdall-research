package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Util;

public abstract class BitmapTransformation implements Transformation<Bitmap> {

    /* renamed from: 龘  reason: contains not printable characters */
    private BitmapPool f19923;

    public BitmapTransformation(Context context) {
        this(Glide.m3936(context).m3952());
    }

    public BitmapTransformation(BitmapPool bitmapPool) {
        this.f19923 = bitmapPool;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Bitmap m25732(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2);

    /* renamed from: 龘  reason: contains not printable characters */
    public final Resource<Bitmap> m25733(Resource<Bitmap> resource, int i, int i2) {
        if (!Util.m26202(i, i2)) {
            throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
        }
        Bitmap r3 = resource.m25491();
        Bitmap r4 = m25732(this.f19923, r3, i == Integer.MIN_VALUE ? r3.getWidth() : i, i2 == Integer.MIN_VALUE ? r3.getHeight() : i2);
        return r3.equals(r4) ? resource : BitmapResource.m25727(r4, this.f19923);
    }
}
