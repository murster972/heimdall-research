package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.engine.Resource;

public class SimpleResource<T> implements Resource<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final T f19917;

    public SimpleResource(T t) {
        if (t == null) {
            throw new NullPointerException("Data must not be null");
        }
        this.f19917 = t;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final T m25716() {
        return this.f19917;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m25717() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final int m25718() {
        return 1;
    }
}
