package com.bumptech.glide.load.resource.bitmap;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ImageHeaderParser {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int[] f19947 = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final byte[] f19948;

    /* renamed from: 齉  reason: contains not printable characters */
    private final StreamReader f19949;

    public enum ImageType {
        GIF(true),
        JPEG(false),
        PNG_A(true),
        PNG(false),
        UNKNOWN(false);
        
        private final boolean hasAlpha;

        private ImageType(boolean z) {
            this.hasAlpha = z;
        }

        public boolean hasAlpha() {
            return this.hasAlpha;
        }
    }

    private static class RandomAccessReader {

        /* renamed from: 龘  reason: contains not printable characters */
        private final ByteBuffer f19950;

        public RandomAccessReader(byte[] bArr) {
            this.f19950 = ByteBuffer.wrap(bArr);
            this.f19950.order(ByteOrder.BIG_ENDIAN);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public short m25778(int i) {
            return this.f19950.getShort(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m25779() {
            return this.f19950.array().length;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m25780(int i) {
            return this.f19950.getInt(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25781(ByteOrder byteOrder) {
            this.f19950.order(byteOrder);
        }
    }

    private static class StreamReader {

        /* renamed from: 龘  reason: contains not printable characters */
        private final InputStream f19951;

        public StreamReader(InputStream inputStream) {
            this.f19951 = inputStream;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public short m25782() throws IOException {
            return (short) (this.f19951.read() & 255);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m25783() throws IOException {
            return this.f19951.read();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m25784() throws IOException {
            return ((this.f19951.read() << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK) | (this.f19951.read() & 255);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m25785(byte[] bArr) throws IOException {
            int length = bArr.length;
            while (length > 0) {
                int read = this.f19951.read(bArr, bArr.length - length, length);
                if (read == -1) {
                    break;
                }
                length -= read;
            }
            return bArr.length - length;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m25786(long j) throws IOException {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.f19951.skip(j2);
                if (skip > 0) {
                    j2 -= skip;
                } else if (this.f19951.read() == -1) {
                    break;
                } else {
                    j2--;
                }
            }
            return j - j2;
        }
    }

    static {
        byte[] bArr = new byte[0];
        try {
            bArr = "Exif\u0000\u0000".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        f19948 = bArr;
    }

    public ImageHeaderParser(InputStream inputStream) {
        this.f19949 = new StreamReader(inputStream);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private byte[] m25771() throws IOException {
        short r4;
        int r3;
        long r6;
        do {
            short r2 = this.f19949.m25782();
            if (r2 != 255) {
                if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Unknown segmentId=" + r2);
                }
                return null;
            }
            r4 = this.f19949.m25782();
            if (r4 == 218) {
                return null;
            }
            if (r4 == 217) {
                if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
                return null;
            }
            r3 = this.f19949.m25784() - 2;
            if (r4 != 225) {
                r6 = this.f19949.m25786((long) r3);
            } else {
                byte[] bArr = new byte[r3];
                int r0 = this.f19949.m25785(bArr);
                if (r0 == r3) {
                    return bArr;
                }
                if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Unable to read segment data, type: " + r4 + ", length: " + r3 + ", actually read: " + r0);
                }
                return null;
            }
        } while (r6 == ((long) r3));
        if (Log.isLoggable("ImageHeaderParser", 3)) {
            Log.d("ImageHeaderParser", "Unable to skip enough data, type: " + r4 + ", wanted to skip: " + r3 + ", but actually skipped: " + r6);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m25772(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m25773(RandomAccessReader randomAccessReader) {
        ByteOrder byteOrder;
        int length = "Exif\u0000\u0000".length();
        short r2 = randomAccessReader.m25778(length);
        if (r2 == 19789) {
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else if (r2 == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else {
            if (Log.isLoggable("ImageHeaderParser", 3)) {
                Log.d("ImageHeaderParser", "Unknown endianness = " + r2);
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        randomAccessReader.m25781(byteOrder);
        int r4 = randomAccessReader.m25780(length + 4) + length;
        short r8 = randomAccessReader.m25778(r4);
        for (int i = 0; i < r8; i++) {
            int r9 = m25772(r4, i);
            short r10 = randomAccessReader.m25778(r9);
            if (r10 == 274) {
                short r5 = randomAccessReader.m25778(r9 + 2);
                if (r5 >= 1 && r5 <= 12) {
                    int r3 = randomAccessReader.m25780(r9 + 4);
                    if (r3 >= 0) {
                        if (Log.isLoggable("ImageHeaderParser", 3)) {
                            Log.d("ImageHeaderParser", "Got tagIndex=" + i + " tagType=" + r10 + " formatCode=" + r5 + " componentCount=" + r3);
                        }
                        int i2 = r3 + f19947[r5];
                        if (i2 <= 4) {
                            int i3 = r9 + 8;
                            if (i3 < 0 || i3 > randomAccessReader.m25779()) {
                                if (Log.isLoggable("ImageHeaderParser", 3)) {
                                    Log.d("ImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + r10);
                                }
                            } else if (i2 >= 0 && i3 + i2 <= randomAccessReader.m25779()) {
                                return randomAccessReader.m25778(i3);
                            } else {
                                if (Log.isLoggable("ImageHeaderParser", 3)) {
                                    Log.d("ImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + r10);
                                }
                            }
                        } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                            Log.d("ImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + r5);
                        }
                    } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                        Log.d("ImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Got invalid format code=" + r5);
                }
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m25774(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ImageType m25775() throws IOException {
        int r2 = this.f19949.m25784();
        if (r2 == 65496) {
            return ImageType.JPEG;
        }
        int r1 = ((r2 << 16) & SupportMenu.CATEGORY_MASK) | (this.f19949.m25784() & 65535);
        if (r1 != -1991225785) {
            return (r1 >> 8) == 4671814 ? ImageType.GIF : ImageType.UNKNOWN;
        }
        this.f19949.m25786(21);
        return this.f19949.m25783() >= 3 ? ImageType.PNG_A : ImageType.PNG;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m25776() throws IOException {
        if (!m25774(this.f19949.m25784())) {
            return -1;
        }
        byte[] r0 = m25771();
        boolean z = r0 != null && r0.length > f19948.length;
        if (z) {
            int i = 0;
            while (true) {
                if (i >= f19948.length) {
                    break;
                } else if (r0[i] != f19948[i]) {
                    z = false;
                    break;
                } else {
                    i++;
                }
            }
        }
        if (z) {
            return m25773(new RandomAccessReader(r0));
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25777() throws IOException {
        return m25775().hasAlpha();
    }
}
