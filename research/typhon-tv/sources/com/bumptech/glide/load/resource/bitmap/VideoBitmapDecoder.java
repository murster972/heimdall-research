package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.io.IOException;

public class VideoBitmapDecoder implements BitmapDecoder<ParcelFileDescriptor> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final MediaMetadataRetrieverFactory f19971 = new MediaMetadataRetrieverFactory();

    /* renamed from: 靐  reason: contains not printable characters */
    private MediaMetadataRetrieverFactory f19972;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f19973;

    static class MediaMetadataRetrieverFactory {
        MediaMetadataRetrieverFactory() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaMetadataRetriever m25813() {
            return new MediaMetadataRetriever();
        }
    }

    public VideoBitmapDecoder() {
        this(f19971, -1);
    }

    VideoBitmapDecoder(MediaMetadataRetrieverFactory mediaMetadataRetrieverFactory, int i) {
        this.f19972 = mediaMetadataRetrieverFactory;
        this.f19973 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m25811(ParcelFileDescriptor parcelFileDescriptor, BitmapPool bitmapPool, int i, int i2, DecodeFormat decodeFormat) throws IOException {
        MediaMetadataRetriever r0 = this.f19972.m25813();
        r0.setDataSource(parcelFileDescriptor.getFileDescriptor());
        Bitmap frameAtTime = this.f19973 >= 0 ? r0.getFrameAtTime((long) this.f19973) : r0.getFrameAtTime();
        r0.release();
        parcelFileDescriptor.close();
        return frameAtTime;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25812() {
        return "VideoBitmapDecoder.com.bumptech.glide.load.resource.bitmap";
    }
}
