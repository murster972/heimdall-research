package com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;

public class GlideBitmapDrawable extends GlideDrawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private BitmapState f19936;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f19937;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f19938;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f19939;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f19940;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Rect f19941;

    static class BitmapState extends Drawable.ConstantState {

        /* renamed from: 麤  reason: contains not printable characters */
        private static final Paint f19942 = new Paint(6);

        /* renamed from: 靐  reason: contains not printable characters */
        int f19943;

        /* renamed from: 齉  reason: contains not printable characters */
        Paint f19944;

        /* renamed from: 龘  reason: contains not printable characters */
        final Bitmap f19945;

        public BitmapState(Bitmap bitmap) {
            this.f19944 = f19942;
            this.f19945 = bitmap;
        }

        BitmapState(BitmapState bitmapState) {
            this(bitmapState.f19945);
            this.f19943 = bitmapState.f19943;
        }

        public int getChangingConfigurations() {
            return 0;
        }

        public Drawable newDrawable() {
            return new GlideBitmapDrawable((Resources) null, this);
        }

        public Drawable newDrawable(Resources resources) {
            return new GlideBitmapDrawable(resources, this);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25766() {
            if (f19942 == this.f19944) {
                this.f19944 = new Paint(6);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25767(int i) {
            m25766();
            this.f19944.setAlpha(i);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25768(ColorFilter colorFilter) {
            m25766();
            this.f19944.setColorFilter(colorFilter);
        }
    }

    public GlideBitmapDrawable(Resources resources, Bitmap bitmap) {
        this(resources, new BitmapState(bitmap));
    }

    GlideBitmapDrawable(Resources resources, BitmapState bitmapState) {
        int i;
        this.f19941 = new Rect();
        if (bitmapState == null) {
            throw new NullPointerException("BitmapState must not be null");
        }
        this.f19936 = bitmapState;
        if (resources != null) {
            int i2 = resources.getDisplayMetrics().densityDpi;
            i = i2 == 0 ? 160 : i2;
            bitmapState.f19943 = i;
        } else {
            i = bitmapState.f19943;
        }
        this.f19938 = bitmapState.f19945.getScaledWidth(i);
        this.f19940 = bitmapState.f19945.getScaledHeight(i);
    }

    public void draw(Canvas canvas) {
        if (this.f19939) {
            Gravity.apply(119, this.f19938, this.f19940, getBounds(), this.f19941);
            this.f19939 = false;
        }
        canvas.drawBitmap(this.f19936.f19945, (Rect) null, this.f19941, this.f19936.f19944);
    }

    public Drawable.ConstantState getConstantState() {
        return this.f19936;
    }

    public int getIntrinsicHeight() {
        return this.f19940;
    }

    public int getIntrinsicWidth() {
        return this.f19938;
    }

    public int getOpacity() {
        Bitmap bitmap = this.f19936.f19945;
        return (bitmap == null || bitmap.hasAlpha() || this.f19936.f19944.getAlpha() < 255) ? -3 : -1;
    }

    public boolean isRunning() {
        return false;
    }

    public Drawable mutate() {
        if (!this.f19937 && super.mutate() == this) {
            this.f19936 = new BitmapState(this.f19936);
            this.f19937 = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f19939 = true;
    }

    public void setAlpha(int i) {
        if (this.f19936.f19944.getAlpha() != i) {
            this.f19936.m25767(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f19936.m25768(colorFilter);
        invalidateSelf();
    }

    public void start() {
    }

    public void stop() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Bitmap m25763() {
        return this.f19936.f19945;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25764(int i) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25765() {
        return false;
    }
}
