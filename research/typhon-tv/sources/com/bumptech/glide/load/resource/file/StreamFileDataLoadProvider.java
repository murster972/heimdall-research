package com.bumptech.glide.load.resource.file;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.NullResourceEncoder;
import com.bumptech.glide.provider.DataLoadProvider;
import java.io.File;
import java.io.InputStream;

public class StreamFileDataLoadProvider implements DataLoadProvider<InputStream, File> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ErrorSourceDecoder f19978 = new ErrorSourceDecoder();

    /* renamed from: 靐  reason: contains not printable characters */
    private final ResourceDecoder<File, File> f19979 = new FileDecoder();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Encoder<InputStream> f19980 = new StreamEncoder();

    private static class ErrorSourceDecoder implements ResourceDecoder<InputStream, File> {
        private ErrorSourceDecoder() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Resource<File> m25830(InputStream inputStream, int i, int i2) {
            throw new Error("You cannot decode a File from an InputStream by default, try either #diskCacheStratey(DiskCacheStrategy.SOURCE) to avoid this call or #decoder(ResourceDecoder) to replace this Decoder");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m25831() {
            return "";
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResourceDecoder<InputStream, File> m25825() {
        return f19978;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ResourceEncoder<File> m25826() {
        return NullResourceEncoder.m25712();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Encoder<InputStream> m25827() {
        return this.f19980;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResourceDecoder<File, File> m25828() {
        return this.f19979;
    }
}
