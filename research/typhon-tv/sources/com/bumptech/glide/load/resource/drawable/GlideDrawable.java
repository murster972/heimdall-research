package com.bumptech.glide.load.resource.drawable;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;

public abstract class GlideDrawable extends Drawable implements Animatable {
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m25816(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m25817();
}
