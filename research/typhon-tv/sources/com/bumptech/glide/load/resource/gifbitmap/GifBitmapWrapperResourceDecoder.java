package com.bumptech.glide.load.resource.gifbitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.resource.bitmap.ImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.ByteArrayPool;
import java.io.IOException;
import java.io.InputStream;

public class GifBitmapWrapperResourceDecoder implements ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final BufferedStreamFactory f20041 = new BufferedStreamFactory();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ImageTypeParser f20042 = new ImageTypeParser();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ImageTypeParser f20043;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final BufferedStreamFactory f20044;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f20045;

    /* renamed from: 连任  reason: contains not printable characters */
    private final BitmapPool f20046;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ResourceDecoder<InputStream, GifDrawable> f20047;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ResourceDecoder<ImageVideoWrapper, Bitmap> f20048;

    static class BufferedStreamFactory {
        BufferedStreamFactory() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public InputStream m25911(InputStream inputStream, byte[] bArr) {
            return new RecyclableBufferedInputStream(inputStream, bArr);
        }
    }

    static class ImageTypeParser {
        ImageTypeParser() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ImageHeaderParser.ImageType m25912(InputStream inputStream) throws IOException {
            return new ImageHeaderParser(inputStream).m25775();
        }
    }

    public GifBitmapWrapperResourceDecoder(ResourceDecoder<ImageVideoWrapper, Bitmap> resourceDecoder, ResourceDecoder<InputStream, GifDrawable> resourceDecoder2, BitmapPool bitmapPool) {
        this(resourceDecoder, resourceDecoder2, bitmapPool, f20042, f20041);
    }

    GifBitmapWrapperResourceDecoder(ResourceDecoder<ImageVideoWrapper, Bitmap> resourceDecoder, ResourceDecoder<InputStream, GifDrawable> resourceDecoder2, BitmapPool bitmapPool, ImageTypeParser imageTypeParser, BufferedStreamFactory bufferedStreamFactory) {
        this.f20048 = resourceDecoder;
        this.f20047 = resourceDecoder2;
        this.f20046 = bitmapPool;
        this.f20043 = imageTypeParser;
        this.f20044 = bufferedStreamFactory;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private GifBitmapWrapper m25904(ImageVideoWrapper imageVideoWrapper, int i, int i2) throws IOException {
        Resource<Bitmap> r0 = this.f20048.m25372(imageVideoWrapper, i, i2);
        if (r0 != null) {
            return new GifBitmapWrapper(r0, (Resource<GifDrawable>) null);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private GifBitmapWrapper m25905(ImageVideoWrapper imageVideoWrapper, int i, int i2, byte[] bArr) throws IOException {
        InputStream r0 = this.f20044.m25911(imageVideoWrapper.m25647(), bArr);
        r0.mark(2048);
        ImageHeaderParser.ImageType r3 = this.f20043.m25912(r0);
        r0.reset();
        GifBitmapWrapper gifBitmapWrapper = null;
        if (r3 == ImageHeaderParser.ImageType.GIF) {
            gifBitmapWrapper = m25907(r0, i, i2);
        }
        return gifBitmapWrapper == null ? m25904(new ImageVideoWrapper(r0, imageVideoWrapper.m25646()), i, i2) : gifBitmapWrapper;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GifBitmapWrapper m25906(ImageVideoWrapper imageVideoWrapper, int i, int i2, byte[] bArr) throws IOException {
        return imageVideoWrapper.m25647() != null ? m25905(imageVideoWrapper, i, i2, bArr) : m25904(imageVideoWrapper, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GifBitmapWrapper m25907(InputStream inputStream, int i, int i2) throws IOException {
        Resource<GifDrawable> r2 = this.f20047.m25372(inputStream, i, i2);
        if (r2 == null) {
            return null;
        }
        GifDrawable r1 = r2.m25491();
        return r1.m25839() > 1 ? new GifBitmapWrapper((Resource<Bitmap>) null, r2) : new GifBitmapWrapper(new BitmapResource(r1.m25840(), this.f20046), (Resource<GifDrawable>) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<GifBitmapWrapper> m25909(ImageVideoWrapper imageVideoWrapper, int i, int i2) throws IOException {
        ByteArrayPool r0 = ByteArrayPool.m26167();
        byte[] r1 = r0.m26168();
        try {
            GifBitmapWrapper r2 = m25906(imageVideoWrapper, i, i2, r1);
            if (r2 != null) {
                return new GifBitmapWrapperResource(r2);
            }
            return null;
        } finally {
            r0.m26169(r1);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25910() {
        if (this.f20045 == null) {
            this.f20045 = this.f20047.m25373() + this.f20048.m25373();
        }
        return this.f20045;
    }
}
