package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.gifdecoder.GifHeader;
import com.bumptech.glide.gifdecoder.GifHeaderParser;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.UnitTransformation;
import com.bumptech.glide.util.Util;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class GifResourceDecoder implements ResourceDecoder<InputStream, GifDrawable> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final GifDecoderPool f20025 = new GifDecoderPool();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final GifHeaderParserPool f20026 = new GifHeaderParserPool();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final GifDecoderPool f20027;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final GifBitmapProvider f20028;

    /* renamed from: 连任  reason: contains not printable characters */
    private final BitmapPool f20029;

    /* renamed from: 麤  reason: contains not printable characters */
    private final GifHeaderParserPool f20030;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f20031;

    static class GifDecoderPool {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Queue<GifDecoder> f20032 = Util.m26200(0);

        GifDecoderPool() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public synchronized GifDecoder m25883(GifDecoder.BitmapProvider bitmapProvider) {
            GifDecoder poll;
            poll = this.f20032.poll();
            if (poll == null) {
                poll = new GifDecoder(bitmapProvider);
            }
            return poll;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public synchronized void m25884(GifDecoder gifDecoder) {
            gifDecoder.m25306();
            this.f20032.offer(gifDecoder);
        }
    }

    static class GifHeaderParserPool {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Queue<GifHeaderParser> f20033 = Util.m26200(0);

        GifHeaderParserPool() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public synchronized GifHeaderParser m25885(byte[] bArr) {
            GifHeaderParser poll;
            poll = this.f20033.poll();
            if (poll == null) {
                poll = new GifHeaderParser();
            }
            return poll.m25333(bArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public synchronized void m25886(GifHeaderParser gifHeaderParser) {
            gifHeaderParser.m25334();
            this.f20033.offer(gifHeaderParser);
        }
    }

    public GifResourceDecoder(Context context, BitmapPool bitmapPool) {
        this(context, bitmapPool, f20026, f20025);
    }

    GifResourceDecoder(Context context, BitmapPool bitmapPool, GifHeaderParserPool gifHeaderParserPool, GifDecoderPool gifDecoderPool) {
        this.f20031 = context;
        this.f20029 = bitmapPool;
        this.f20027 = gifDecoderPool;
        this.f20028 = new GifBitmapProvider(bitmapPool);
        this.f20030 = gifHeaderParserPool;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Bitmap m25877(GifDecoder gifDecoder, GifHeader gifHeader, byte[] bArr) {
        gifDecoder.m25313(gifHeader, bArr);
        gifDecoder.m25312();
        return gifDecoder.m25305();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GifDrawableResource m25878(byte[] bArr, int i, int i2, GifHeaderParser gifHeaderParser, GifDecoder gifDecoder) {
        Bitmap r9;
        GifHeader r7 = gifHeaderParser.m25332();
        if (r7.m25317() <= 0 || r7.m25316() != 0 || (r9 = m25877(gifDecoder, r7, bArr)) == null) {
            return null;
        }
        return new GifDrawableResource(new GifDrawable(this.f20031, this.f20028, this.f20029, UnitTransformation.m25719(), i, i2, r7, bArr, r9));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m25879(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byteArrayOutputStream.flush();
        } catch (IOException e) {
            Log.w("GifResourceDecoder", "Error reading data from stream", e);
        }
        return byteArrayOutputStream.toByteArray();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GifDrawableResource m25880(InputStream inputStream, int i, int i2) {
        byte[] r1 = m25879(inputStream);
        GifHeaderParser r4 = this.f20030.m25885(r1);
        GifDecoder r5 = this.f20027.m25883((GifDecoder.BitmapProvider) this.f20028);
        try {
            return m25878(r1, i, i2, r4, r5);
        } finally {
            this.f20030.m25886(r4);
            this.f20027.m25884(r5);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25882() {
        return "";
    }
}
