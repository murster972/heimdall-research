package com.bumptech.glide.load.data;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.bumptech.glide.Priority;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class LocalUriFetcher<T> implements DataFetcher<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f19718;

    /* renamed from: 齉  reason: contains not printable characters */
    private T f19719;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Uri f19720;

    public LocalUriFetcher(Context context, Uri uri) {
        this.f19718 = context.getApplicationContext();
        this.f19720 = uri;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract T m25408(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25409() {
        return this.f19720.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25410() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final T m25411(Priority priority) throws Exception {
        this.f19719 = m25408(this.f19720, this.f19718.getContentResolver());
        return this.f19719;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25412() {
        if (this.f19719 != null) {
            try {
                m25413(this.f19719);
            } catch (IOException e) {
                if (Log.isLoggable("LocalUriFetcher", 2)) {
                    Log.v("LocalUriFetcher", "failed to close data", e);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m25413(T t) throws IOException;
}
