package com.bumptech.glide.load.data;

import com.bumptech.glide.Priority;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class ByteArrayFetcher implements DataFetcher<InputStream> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f19710;

    /* renamed from: 龘  reason: contains not printable characters */
    private final byte[] f19711;

    public ByteArrayFetcher(byte[] bArr, String str) {
        this.f19711 = bArr;
        this.f19710 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public InputStream m25385(Priority priority) {
        return new ByteArrayInputStream(this.f19711);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25383() {
        return this.f19710;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25384() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25386() {
    }
}
