package com.bumptech.glide.load.data;

import android.content.res.AssetManager;
import android.util.Log;
import com.bumptech.glide.Priority;
import java.io.IOException;

public abstract class AssetPathFetcher<T> implements DataFetcher<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final AssetManager f19707;

    /* renamed from: 齉  reason: contains not printable characters */
    private T f19708;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f19709;

    public AssetPathFetcher(AssetManager assetManager, String str) {
        this.f19707 = assetManager;
        this.f19709 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25376() {
        return this.f19709;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25377() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m25378(AssetManager assetManager, String str) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public T m25379(Priority priority) throws Exception {
        this.f19708 = m25378(this.f19707, this.f19709);
        return this.f19708;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25380() {
        if (this.f19708 != null) {
            try {
                m25381(this.f19708);
            } catch (IOException e) {
                if (Log.isLoggable("AssetUriFetcher", 2)) {
                    Log.v("AssetUriFetcher", "Failed to close data", e);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m25381(T t) throws IOException;
}
