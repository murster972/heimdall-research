package com.bumptech.glide.load.data;

import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.util.ContentLengthInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

public class HttpUrlFetcher implements DataFetcher<InputStream> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final HttpUrlConnectionFactory f19712 = new DefaultHttpUrlConnectionFactory();

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile boolean f19713;

    /* renamed from: 连任  reason: contains not printable characters */
    private InputStream f19714;

    /* renamed from: 靐  reason: contains not printable characters */
    private final GlideUrl f19715;

    /* renamed from: 麤  reason: contains not printable characters */
    private HttpURLConnection f19716;

    /* renamed from: 齉  reason: contains not printable characters */
    private final HttpUrlConnectionFactory f19717;

    private static class DefaultHttpUrlConnectionFactory implements HttpUrlConnectionFactory {
        private DefaultHttpUrlConnectionFactory() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public HttpURLConnection m25406(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    }

    interface HttpUrlConnectionFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        HttpURLConnection m25407(URL url) throws IOException;
    }

    public HttpUrlFetcher(GlideUrl glideUrl) {
        this(glideUrl, f19712);
    }

    HttpUrlFetcher(GlideUrl glideUrl, HttpUrlConnectionFactory httpUrlConnectionFactory) {
        this.f19715 = glideUrl;
        this.f19717 = httpUrlConnectionFactory;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private InputStream m25399(HttpURLConnection httpURLConnection) throws IOException {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.f19714 = ContentLengthInputStream.m26171(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.f19714 = httpURLConnection.getInputStream();
        }
        return this.f19714;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private InputStream m25400(URL url, int i, URL url2, Map<String, String> map) throws IOException {
        if (i >= 5) {
            throw new IOException("Too many (> 5) redirects!");
        }
        if (url2 != null) {
            try {
                if (url.toURI().equals(url2.toURI())) {
                    throw new IOException("In re-direct loop");
                }
            } catch (URISyntaxException e) {
            }
        }
        this.f19716 = this.f19717.m25407(url);
        for (Map.Entry next : map.entrySet()) {
            this.f19716.addRequestProperty((String) next.getKey(), (String) next.getValue());
        }
        this.f19716.setConnectTimeout(2500);
        this.f19716.setReadTimeout(2500);
        this.f19716.setUseCaches(false);
        this.f19716.setDoInput(true);
        this.f19716.connect();
        if (this.f19713) {
            return null;
        }
        int responseCode = this.f19716.getResponseCode();
        if (responseCode / 100 == 2) {
            return m25399(this.f19716);
        }
        if (responseCode / 100 == 3) {
            String headerField = this.f19716.getHeaderField("Location");
            if (!TextUtils.isEmpty(headerField)) {
                return m25400(new URL(url, headerField), i + 1, url, map);
            }
            throw new IOException("Received empty or null redirect url");
        } else if (responseCode == -1) {
            throw new IOException("Unable to retrieve response code from HttpUrlConnection.");
        } else {
            throw new IOException("Request failed " + responseCode + ": " + this.f19716.getResponseMessage());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public InputStream m25404(Priority priority) throws Exception {
        return m25400(this.f19715.m25638(), 0, (URL) null, this.f19715.m25637());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25402() {
        return this.f19715.m25636();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m25403() {
        this.f19713 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25405() {
        if (this.f19714 != null) {
            try {
                this.f19714.close();
            } catch (IOException e) {
            }
        }
        if (this.f19716 != null) {
            this.f19716.disconnect();
        }
    }
}
