package com.bumptech.glide.load;

import com.bumptech.glide.load.engine.Resource;
import java.util.Arrays;
import java.util.Collection;

public class MultiTransformation<T> implements Transformation<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f3359;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Collection<? extends Transformation<T>> f3360;

    @SafeVarargs
    public MultiTransformation(Transformation<T>... transformationArr) {
        if (transformationArr.length < 1) {
            throw new IllegalArgumentException("MultiTransformation must contain at least one Transformation");
        }
        this.f3360 = Arrays.asList(transformationArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Resource<T> m3978(Resource<T> resource, int i, int i2) {
        Resource<T> resource2 = resource;
        for (Transformation r2 : this.f3360) {
            Resource<T> r3 = r2.m25374(resource2, i, i2);
            if (resource2 != null && !resource2.equals(resource) && !resource2.equals(r3)) {
                resource2.m25492();
            }
            resource2 = r3;
        }
        return resource2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m3979() {
        if (this.f3359 == null) {
            StringBuilder sb = new StringBuilder();
            for (Transformation r2 : this.f3360) {
                sb.append(r2.m25375());
            }
            this.f3359 = sb.toString();
        }
        return this.f3359;
    }
}
