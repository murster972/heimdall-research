package com.bumptech.glide.load.model;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.data.DataFetcher;
import com.mopub.common.TyphoonApp;

public abstract class UriLoader<T> implements ModelLoader<Uri, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ModelLoader<GlideUrl, T> f19910;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f19911;

    public UriLoader(Context context, ModelLoader<GlideUrl, T> modelLoader) {
        this.f19911 = context;
        this.f19910 = modelLoader;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m25674(String str) {
        return "file".equals(str) || "content".equals(str) || "android.resource".equals(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract DataFetcher<T> m25675(Context context, Uri uri);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract DataFetcher<T> m25676(Context context, String str);

    /* renamed from: 龘  reason: contains not printable characters */
    public final DataFetcher<T> m25678(Uri uri, int i, int i2) {
        String scheme = uri.getScheme();
        if (m25674(scheme)) {
            if (!AssetUriParser.m25623(uri)) {
                return m25675(this.f19911, uri);
            }
            return m25676(this.f19911, AssetUriParser.m25622(uri));
        } else if (this.f19910 == null) {
            return null;
        } else {
            if (TyphoonApp.HTTP.equals(scheme) || TyphoonApp.HTTPS.equals(scheme)) {
                return this.f19910.m25663(new GlideUrl(uri.toString()), i, i2);
            }
            return null;
        }
    }
}
