package com.bumptech.glide.load.model.stream;

import android.content.Context;
import com.bumptech.glide.load.data.ByteArrayFetcher;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import java.io.InputStream;

public class StreamByteArrayLoader implements StreamModelLoader<byte[]> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f3363;

    public static class Factory implements ModelLoaderFactory<byte[], InputStream> {
        /* renamed from: 龘  reason: contains not printable characters */
        public ModelLoader<byte[], InputStream> m25695(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new StreamByteArrayLoader();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25696() {
        }
    }

    public StreamByteArrayLoader() {
        this("");
    }

    @Deprecated
    public StreamByteArrayLoader(String str) {
        this.f3363 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<InputStream> m3981(byte[] bArr, int i, int i2) {
        return new ByteArrayFetcher(bArr, this.f3363);
    }
}
