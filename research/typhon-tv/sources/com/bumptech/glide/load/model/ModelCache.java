package com.bumptech.glide.load.model;

import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Util;
import java.util.Queue;

public class ModelCache<A, B> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final LruCache<ModelKey<A>, B> f19901;

    static final class ModelKey<A> {

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Queue<ModelKey<?>> f19903 = Util.m26200(0);

        /* renamed from: 靐  reason: contains not printable characters */
        private int f19904;

        /* renamed from: 麤  reason: contains not printable characters */
        private A f19905;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f19906;

        private ModelKey() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m25660(A a, int i, int i2) {
            this.f19905 = a;
            this.f19906 = i;
            this.f19904 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static <A> ModelKey<A> m25661(A a, int i, int i2) {
            ModelKey<A> poll = f19903.poll();
            if (poll == null) {
                poll = new ModelKey<>();
            }
            poll.m25660(a, i, i2);
            return poll;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof ModelKey)) {
                return false;
            }
            ModelKey modelKey = (ModelKey) obj;
            return this.f19906 == modelKey.f19906 && this.f19904 == modelKey.f19904 && this.f19905.equals(modelKey.f19905);
        }

        public int hashCode() {
            return (((this.f19904 * 31) + this.f19906) * 31) + this.f19905.hashCode();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25662() {
            f19903.offer(this);
        }
    }

    public ModelCache() {
        this(250);
    }

    public ModelCache(int i) {
        this.f19901 = new LruCache<ModelKey<A>, B>(i) {
            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m25659(ModelKey<A> modelKey, B b) {
                modelKey.m25662();
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public B m25656(A a, int i, int i2) {
        ModelKey r0 = ModelKey.m25661(a, i, i2);
        B r1 = this.f19901.m26180(r0);
        r0.m25662();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25657(A a, int i, int i2, B b) {
        this.f19901.m26181(ModelKey.m25661(a, i, i2), b);
    }
}
