package com.bumptech.glide.load.model;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import com.bumptech.glide.load.data.DataFetcher;

public class ResourceLoader<T> implements ModelLoader<Integer, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Resources f19907;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelLoader<Uri, T> f19908;

    public ResourceLoader(Context context, ModelLoader<Uri, T> modelLoader) {
        this(context.getResources(), modelLoader);
    }

    public ResourceLoader(Resources resources, ModelLoader<Uri, T> modelLoader) {
        this.f19907 = resources;
        this.f19908 = modelLoader;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<T> m25667(Integer num, int i, int i2) {
        Uri uri = null;
        try {
            uri = Uri.parse("android.resource://" + this.f19907.getResourcePackageName(num.intValue()) + '/' + this.f19907.getResourceTypeName(num.intValue()) + '/' + this.f19907.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            }
        }
        if (uri != null) {
            return this.f19908.m25663(uri, i, i2);
        }
        return null;
    }
}
