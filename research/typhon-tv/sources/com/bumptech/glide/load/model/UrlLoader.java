package com.bumptech.glide.load.model;

import com.bumptech.glide.load.data.DataFetcher;
import java.net.URL;

public class UrlLoader<T> implements ModelLoader<URL, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelLoader<GlideUrl, T> f19912;

    public UrlLoader(ModelLoader<GlideUrl, T> modelLoader) {
        this.f19912 = modelLoader;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<T> m25679(URL url, int i, int i2) {
        return this.f19912.m25663(new GlideUrl(url), i, i2);
    }
}
