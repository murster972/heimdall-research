package com.bumptech.glide.load.model.file_descriptor;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.model.FileLoader;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import java.io.File;

public class FileDescriptorFileLoader extends FileLoader<ParcelFileDescriptor> implements FileDescriptorModelLoader<File> {

    public static class Factory implements ModelLoaderFactory<File, ParcelFileDescriptor> {
        /* renamed from: 龘  reason: contains not printable characters */
        public ModelLoader<File, ParcelFileDescriptor> m25681(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new FileDescriptorFileLoader(genericLoaderFactory.m25630(Uri.class, ParcelFileDescriptor.class));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25682() {
        }
    }

    public FileDescriptorFileLoader(ModelLoader<Uri, ParcelFileDescriptor> modelLoader) {
        super(modelLoader);
    }
}
