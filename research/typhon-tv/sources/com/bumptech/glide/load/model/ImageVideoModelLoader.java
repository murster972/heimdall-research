package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.InputStream;

public class ImageVideoModelLoader<A> implements ModelLoader<A, ImageVideoWrapper> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ModelLoader<A, ParcelFileDescriptor> f19883;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelLoader<A, InputStream> f19884;

    static class ImageVideoFetcher implements DataFetcher<ImageVideoWrapper> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final DataFetcher<ParcelFileDescriptor> f19885;

        /* renamed from: 龘  reason: contains not printable characters */
        private final DataFetcher<InputStream> f19886;

        public ImageVideoFetcher(DataFetcher<InputStream> dataFetcher, DataFetcher<ParcelFileDescriptor> dataFetcher2) {
            this.f19886 = dataFetcher;
            this.f19885 = dataFetcher2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public ImageVideoWrapper m25644(Priority priority) throws Exception {
            InputStream inputStream = null;
            if (this.f19886 != null) {
                try {
                    inputStream = this.f19886.m25389(priority);
                } catch (Exception e) {
                    if (Log.isLoggable("IVML", 2)) {
                        Log.v("IVML", "Exception fetching input stream, trying ParcelFileDescriptor", e);
                    }
                    if (this.f19885 == null) {
                        throw e;
                    }
                }
            }
            ParcelFileDescriptor parcelFileDescriptor = null;
            if (this.f19885 != null) {
                try {
                    parcelFileDescriptor = this.f19885.m25389(priority);
                } catch (Exception e2) {
                    if (Log.isLoggable("IVML", 2)) {
                        Log.v("IVML", "Exception fetching ParcelFileDescriptor", e2);
                    }
                    if (inputStream == null) {
                        throw e2;
                    }
                }
            }
            return new ImageVideoWrapper(inputStream, parcelFileDescriptor);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m25642() {
            return this.f19886 != null ? this.f19886.m25387() : this.f19885.m25387();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m25643() {
            if (this.f19886 != null) {
                this.f19886.m25388();
            }
            if (this.f19885 != null) {
                this.f19885.m25388();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25645() {
            if (this.f19886 != null) {
                this.f19886.m25390();
            }
            if (this.f19885 != null) {
                this.f19885.m25390();
            }
        }
    }

    public ImageVideoModelLoader(ModelLoader<A, InputStream> modelLoader, ModelLoader<A, ParcelFileDescriptor> modelLoader2) {
        if (modelLoader == null && modelLoader2 == null) {
            throw new NullPointerException("At least one of streamLoader and fileDescriptorLoader must be non null");
        }
        this.f19884 = modelLoader;
        this.f19883 = modelLoader2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<ImageVideoWrapper> m25640(A a, int i, int i2) {
        DataFetcher<InputStream> dataFetcher = null;
        if (this.f19884 != null) {
            dataFetcher = this.f19884.m25663(a, i, i2);
        }
        DataFetcher<ParcelFileDescriptor> dataFetcher2 = null;
        if (this.f19883 != null) {
            dataFetcher2 = this.f19883.m25663(a, i, i2);
        }
        if (dataFetcher == null && dataFetcher2 == null) {
            return null;
        }
        return new ImageVideoFetcher(dataFetcher, dataFetcher2);
    }
}
