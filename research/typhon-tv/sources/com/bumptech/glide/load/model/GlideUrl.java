package com.bumptech.glide.load.model;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class GlideUrl {

    /* renamed from: 连任  reason: contains not printable characters */
    private URL f19878;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Headers f19879;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f19880;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f19881;

    /* renamed from: 龘  reason: contains not printable characters */
    private final URL f19882;

    public GlideUrl(String str) {
        this(str, Headers.f3361);
    }

    public GlideUrl(String str, Headers headers) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("String url must not be empty or null: " + str);
        } else if (headers == null) {
            throw new IllegalArgumentException("Headers must not be null");
        } else {
            this.f19881 = str;
            this.f19882 = null;
            this.f19879 = headers;
        }
    }

    public GlideUrl(URL url) {
        this(url, Headers.f3361);
    }

    public GlideUrl(URL url, Headers headers) {
        if (url == null) {
            throw new IllegalArgumentException("URL must not be null!");
        } else if (headers == null) {
            throw new IllegalArgumentException("Headers must not be null");
        } else {
            this.f19882 = url;
            this.f19881 = null;
            this.f19879 = headers;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private String m25633() {
        if (TextUtils.isEmpty(this.f19880)) {
            String str = this.f19881;
            if (TextUtils.isEmpty(str)) {
                str = this.f19882.toString();
            }
            this.f19880 = Uri.encode(str, "@#&=*+-_.,:!?()/~'%");
        }
        return this.f19880;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private URL m25634() throws MalformedURLException {
        if (this.f19878 == null) {
            this.f19878 = new URL(m25633());
        }
        return this.f19878;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GlideUrl)) {
            return false;
        }
        GlideUrl glideUrl = (GlideUrl) obj;
        return m25636().equals(glideUrl.m25636()) && this.f19879.equals(glideUrl.f19879);
    }

    public int hashCode() {
        return (m25636().hashCode() * 31) + this.f19879.hashCode();
    }

    public String toString() {
        return m25636() + 10 + this.f19879.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m25635() {
        return m25633();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m25636() {
        return this.f19881 != null ? this.f19881 : this.f19882.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Map<String, String> m25637() {
        return this.f19879.m3980();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public URL m25638() throws MalformedURLException {
        return m25634();
    }
}
