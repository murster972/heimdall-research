package com.bumptech.glide.load.model;

import android.content.Context;
import com.bumptech.glide.load.data.DataFetcher;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GenericLoaderFactory {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final ModelLoader f19874 = new ModelLoader() {
        public String toString() {
            return "NULL_MODEL_LOADER";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public DataFetcher m25632(Object obj, int i, int i2) {
            throw new NoSuchMethodError("This should never be called!");
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<Class, Map<Class, ModelLoader>> f19875 = new HashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f19876;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<Class, Map<Class, ModelLoaderFactory>> f19877 = new HashMap();

    public GenericLoaderFactory(Context context) {
        this.f19876 = context.getApplicationContext();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private <T, Y> void m25626(Class<T> cls, Class<Y> cls2) {
        m25629(cls, cls2, f19874);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private <T, Y> ModelLoaderFactory<T, Y> m25627(Class<T> cls, Class<Y> cls2) {
        Map map;
        Map map2 = this.f19877.get(cls);
        ModelLoaderFactory<T, Y> modelLoaderFactory = null;
        if (map2 != null) {
            modelLoaderFactory = (ModelLoaderFactory) map2.get(cls2);
        }
        if (modelLoaderFactory == null) {
            for (Class next : this.f19877.keySet()) {
                if (next.isAssignableFrom(cls) && (map = this.f19877.get(next)) != null && (modelLoaderFactory = (ModelLoaderFactory) map.get(cls2)) != null) {
                    break;
                }
            }
        }
        return modelLoaderFactory;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private <T, Y> ModelLoader<T, Y> m25628(Class<T> cls, Class<Y> cls2) {
        Map map = this.f19875.get(cls);
        if (map != null) {
            return (ModelLoader) map.get(cls2);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T, Y> void m25629(Class<T> cls, Class<Y> cls2, ModelLoader<T, Y> modelLoader) {
        Map map = this.f19875.get(cls);
        if (map == null) {
            map = new HashMap();
            this.f19875.put(cls, map);
        }
        map.put(cls2, modelLoader);
    }

    /* JADX WARNING: type inference failed for: r6v0, types: [java.lang.Class<Y>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T, Y> com.bumptech.glide.load.model.ModelLoader<T, Y> m25630(java.lang.Class<T> r5, java.lang.Class<Y> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            com.bumptech.glide.load.model.ModelLoader r1 = r4.m25628(r5, r6)     // Catch:{ all -> 0x002c }
            if (r1 == 0) goto L_0x0016
            com.bumptech.glide.load.model.ModelLoader r3 = f19874     // Catch:{ all -> 0x002c }
            boolean r3 = r3.equals(r1)     // Catch:{ all -> 0x002c }
            if (r3 == 0) goto L_0x0013
            r3 = 0
            r2 = r1
        L_0x0011:
            monitor-exit(r4)
            return r3
        L_0x0013:
            r2 = r1
            r3 = r1
            goto L_0x0011
        L_0x0016:
            com.bumptech.glide.load.model.ModelLoaderFactory r0 = r4.m25627(r5, r6)     // Catch:{ all -> 0x002c }
            if (r0 == 0) goto L_0x0028
            android.content.Context r3 = r4.f19876     // Catch:{ all -> 0x002c }
            com.bumptech.glide.load.model.ModelLoader r1 = r0.m25664(r3, r4)     // Catch:{ all -> 0x002c }
            r4.m25629(r5, r6, r1)     // Catch:{ all -> 0x002c }
        L_0x0025:
            r2 = r1
            r3 = r1
            goto L_0x0011
        L_0x0028:
            r4.m25626(r5, r6)     // Catch:{ all -> 0x002c }
            goto L_0x0025
        L_0x002c:
            r3 = move-exception
            monitor-exit(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.model.GenericLoaderFactory.m25630(java.lang.Class, java.lang.Class):com.bumptech.glide.load.model.ModelLoader");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized <T, Y> ModelLoaderFactory<T, Y> m25631(Class<T> cls, Class<Y> cls2, ModelLoaderFactory<T, Y> modelLoaderFactory) {
        ModelLoaderFactory<T, Y> modelLoaderFactory2;
        this.f19875.clear();
        Map map = this.f19877.get(cls);
        if (map == null) {
            map = new HashMap();
            this.f19877.put(cls, map);
        }
        modelLoaderFactory2 = (ModelLoaderFactory) map.put(cls2, modelLoaderFactory);
        if (modelLoaderFactory2 != null) {
            Iterator<Map<Class, ModelLoaderFactory>> it2 = this.f19877.values().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (it2.next().containsValue(modelLoaderFactory2)) {
                        modelLoaderFactory2 = null;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return modelLoaderFactory2;
    }
}
