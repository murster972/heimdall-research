package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.Encoder;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageVideoWrapperEncoder implements Encoder<ImageVideoWrapper> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Encoder<ParcelFileDescriptor> f19889;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f19890;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Encoder<InputStream> f19891;

    public ImageVideoWrapperEncoder(Encoder<InputStream> encoder, Encoder<ParcelFileDescriptor> encoder2) {
        this.f19891 = encoder;
        this.f19889 = encoder2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25648() {
        if (this.f19890 == null) {
            this.f19890 = this.f19891.m25369() + this.f19889.m25369();
        }
        return this.f19890;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25650(ImageVideoWrapper imageVideoWrapper, OutputStream outputStream) {
        return imageVideoWrapper.m25647() != null ? this.f19891.m25370(imageVideoWrapper.m25647(), outputStream) : this.f19889.m25370(imageVideoWrapper.m25646(), outputStream);
    }
}
