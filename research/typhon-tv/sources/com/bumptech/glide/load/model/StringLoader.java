package com.bumptech.glide.load.model;

import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.File;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class StringLoader<T> implements ModelLoader<String, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelLoader<Uri, T> f19909;

    public StringLoader(ModelLoader<Uri, T> modelLoader) {
        this.f19909 = modelLoader;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Uri m25671(String str) {
        return Uri.fromFile(new File(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<T> m25672(String str, int i, int i2) {
        Uri parse;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            parse = m25671(str);
        } else {
            parse = Uri.parse(str);
            if (parse.getScheme() == null) {
                parse = m25671(str);
            }
        }
        return this.f19909.m25663(parse, i, i2);
    }
}
