package com.bumptech.glide.load.model;

import android.util.Log;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.util.ByteArrayPool;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamEncoder implements Encoder<InputStream> {
    /* renamed from: 龘  reason: contains not printable characters */
    public String m25668() {
        return "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25670(InputStream inputStream, OutputStream outputStream) {
        byte[] r0 = ByteArrayPool.m26167().m26168();
        while (true) {
            try {
                int read = inputStream.read(r0);
                if (read != -1) {
                    outputStream.write(r0, 0, read);
                } else {
                    return true;
                }
            } catch (IOException e) {
                if (Log.isLoggable("StreamEncoder", 3)) {
                    Log.d("StreamEncoder", "Failed to encode data onto the OutputStream", e);
                }
                return false;
            } finally {
                ByteArrayPool.m26167().m26169(r0);
            }
        }
    }
}
