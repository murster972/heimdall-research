package com.bumptech.glide.load.model.file_descriptor;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.FileDescriptorAssetPathFetcher;
import com.bumptech.glide.load.data.FileDescriptorLocalUriFetcher;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.UriLoader;

public class FileDescriptorUriLoader extends UriLoader<ParcelFileDescriptor> implements FileDescriptorModelLoader<Uri> {

    public static class Factory implements ModelLoaderFactory<Uri, ParcelFileDescriptor> {
        /* renamed from: 龘  reason: contains not printable characters */
        public ModelLoader<Uri, ParcelFileDescriptor> m25689(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new FileDescriptorUriLoader(context, genericLoaderFactory.m25630(GlideUrl.class, ParcelFileDescriptor.class));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25690() {
        }
    }

    public FileDescriptorUriLoader(Context context, ModelLoader<GlideUrl, ParcelFileDescriptor> modelLoader) {
        super(context, modelLoader);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<ParcelFileDescriptor> m25687(Context context, Uri uri) {
        return new FileDescriptorLocalUriFetcher(context, uri);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<ParcelFileDescriptor> m25688(Context context, String str) {
        return new FileDescriptorAssetPathFetcher(context.getApplicationContext().getAssets(), str);
    }
}
