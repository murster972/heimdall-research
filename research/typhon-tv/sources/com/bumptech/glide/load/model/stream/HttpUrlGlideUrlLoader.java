package com.bumptech.glide.load.model.stream;

import android.content.Context;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.HttpUrlFetcher;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelCache;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import java.io.InputStream;

public class HttpUrlGlideUrlLoader implements ModelLoader<GlideUrl, InputStream> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelCache<GlideUrl, GlideUrl> f19913;

    public static class Factory implements ModelLoaderFactory<GlideUrl, InputStream> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final ModelCache<GlideUrl, GlideUrl> f19914 = new ModelCache<>(500);

        /* renamed from: 龘  reason: contains not printable characters */
        public ModelLoader<GlideUrl, InputStream> m25693(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new HttpUrlGlideUrlLoader(this.f19914);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25694() {
        }
    }

    public HttpUrlGlideUrlLoader() {
        this((ModelCache<GlideUrl, GlideUrl>) null);
    }

    public HttpUrlGlideUrlLoader(ModelCache<GlideUrl, GlideUrl> modelCache) {
        this.f19913 = modelCache;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<InputStream> m25692(GlideUrl glideUrl, int i, int i2) {
        GlideUrl glideUrl2 = glideUrl;
        if (this.f19913 != null && (glideUrl2 = this.f19913.m25656(glideUrl, 0, 0)) == null) {
            this.f19913.m25657(glideUrl, 0, 0, glideUrl);
            glideUrl2 = glideUrl;
        }
        return new HttpUrlFetcher(glideUrl2);
    }
}
