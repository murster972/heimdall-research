package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import java.io.InputStream;

public class ImageVideoWrapper {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ParcelFileDescriptor f19887;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InputStream f19888;

    public ImageVideoWrapper(InputStream inputStream, ParcelFileDescriptor parcelFileDescriptor) {
        this.f19888 = inputStream;
        this.f19887 = parcelFileDescriptor;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ParcelFileDescriptor m25646() {
        return this.f19887;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public InputStream m25647() {
        return this.f19888;
    }
}
