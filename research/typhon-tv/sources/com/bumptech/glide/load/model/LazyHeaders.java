package com.bumptech.glide.load.model;

import android.text.TextUtils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class LazyHeaders implements Headers {

    /* renamed from: 麤  reason: contains not printable characters */
    private volatile Map<String, String> f19892;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Map<String, List<LazyHeaderFactory>> f19893;

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private static final Map<String, List<LazyHeaderFactory>> f19894;

        /* renamed from: 龘  reason: contains not printable characters */
        private static final String f19895 = System.getProperty("http.agent");

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f19896 = true;

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f19897 = true;

        /* renamed from: 麤  reason: contains not printable characters */
        private Map<String, List<LazyHeaderFactory>> f19898 = f19894;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f19899 = true;

        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(f19895)) {
                hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, Collections.singletonList(new StringHeaderFactory(f19895)));
            }
            hashMap.put("Accept-Encoding", Collections.singletonList(new StringHeaderFactory("identity")));
            f19894 = Collections.unmodifiableMap(hashMap);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public LazyHeaders m25654() {
            this.f19899 = true;
            return new LazyHeaders(this.f19898);
        }
    }

    static final class StringHeaderFactory implements LazyHeaderFactory {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f19900;

        StringHeaderFactory(String str) {
            this.f19900 = str;
        }

        public boolean equals(Object obj) {
            if (obj instanceof StringHeaderFactory) {
                return this.f19900.equals(((StringHeaderFactory) obj).f19900);
            }
            return false;
        }

        public int hashCode() {
            return this.f19900.hashCode();
        }

        public String toString() {
            return "StringHeaderFactory{value='" + this.f19900 + '\'' + '}';
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m25655() {
            return this.f19900;
        }
    }

    LazyHeaders(Map<String, List<LazyHeaderFactory>> map) {
        this.f19893 = Collections.unmodifiableMap(map);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, String> m25652() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.f19893.entrySet()) {
            StringBuilder sb = new StringBuilder();
            List list = (List) next.getValue();
            for (int i = 0; i < list.size(); i++) {
                sb.append(((LazyHeaderFactory) list.get(i)).m25651());
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
            hashMap.put(next.getKey(), sb.toString());
        }
        return hashMap;
    }

    public boolean equals(Object obj) {
        if (obj instanceof LazyHeaders) {
            return this.f19893.equals(((LazyHeaders) obj).f19893);
        }
        return false;
    }

    public int hashCode() {
        return this.f19893.hashCode();
    }

    public String toString() {
        return "LazyHeaders{headers=" + this.f19893 + '}';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Map<String, String> m25653() {
        if (this.f19892 == null) {
            synchronized (this) {
                if (this.f19892 == null) {
                    this.f19892 = Collections.unmodifiableMap(m25652());
                }
            }
        }
        return this.f19892;
    }
}
