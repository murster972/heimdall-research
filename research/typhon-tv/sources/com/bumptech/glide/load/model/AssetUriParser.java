package com.bumptech.glide.load.model;

import android.net.Uri;

final class AssetUriParser {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f19872 = "file:///android_asset/".length();

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m25622(Uri uri) {
        return uri.toString().substring(f19872);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m25623(Uri uri) {
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }
}
