package com.bumptech.glide.load.model;

import com.bumptech.glide.load.model.LazyHeaders;
import java.util.Collections;
import java.util.Map;

public interface Headers {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Headers f3361 = new LazyHeaders.Builder().m25654();
    @Deprecated

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Headers f3362 = new Headers() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Map<String, String> m25639() {
            return Collections.emptyMap();
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    Map<String, String> m3980();
}
