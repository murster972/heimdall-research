package com.bumptech.glide.load.model;

import android.net.Uri;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.File;

public class FileLoader<T> implements ModelLoader<File, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ModelLoader<Uri, T> f19873;

    public FileLoader(ModelLoader<Uri, T> modelLoader) {
        this.f19873 = modelLoader;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<T> m25625(File file, int i, int i2) {
        return this.f19873.m25663(Uri.fromFile(file), i, i2);
    }
}
