package com.bumptech.glide.request;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.util.Util;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RequestFutureTarget<T, R> implements FutureTarget<R>, Runnable {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Waiter f20146 = new Waiter();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Waiter f20147;

    /* renamed from: ʼ  reason: contains not printable characters */
    private R f20148;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Request f20149;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f20150;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f20151;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Exception f20152;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f20153;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f20154;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Handler f20155;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f20156;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f20157;

    static class Waiter {
        Waiter() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m26069(Object obj) {
            obj.notifyAll();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m26070(Object obj, long j) throws InterruptedException {
            obj.wait(j);
        }
    }

    public RequestFutureTarget(Handler handler, int i, int i2) {
        this(handler, i, i2, true, f20146);
    }

    RequestFutureTarget(Handler handler, int i, int i2, boolean z, Waiter waiter) {
        this.f20155 = handler;
        this.f20157 = i;
        this.f20156 = i2;
        this.f20154 = z;
        this.f20147 = waiter;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized R m26057(Long l) throws ExecutionException, InterruptedException, TimeoutException {
        R r;
        if (this.f20154) {
            Util.m26190();
        }
        if (this.f20151) {
            throw new CancellationException();
        } else if (this.f20150) {
            throw new ExecutionException(this.f20152);
        } else if (this.f20153) {
            r = this.f20148;
        } else {
            if (l == null) {
                this.f20147.m26070(this, 0);
            } else if (l.longValue() > 0) {
                this.f20147.m26070(this, l.longValue());
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            } else if (this.f20150) {
                throw new ExecutionException(this.f20152);
            } else if (this.f20151) {
                throw new CancellationException();
            } else if (!this.f20153) {
                throw new TimeoutException();
            } else {
                r = this.f20148;
            }
        }
        return r;
    }

    public synchronized boolean cancel(boolean z) {
        boolean z2 = true;
        synchronized (this) {
            if (!this.f20151) {
                if (isDone()) {
                    z2 = false;
                }
                if (z2) {
                    this.f20151 = true;
                    if (z) {
                        m26060();
                    }
                    this.f20147.m26069(this);
                }
            }
        }
        return z2;
    }

    public R get() throws InterruptedException, ExecutionException {
        try {
            return m26057((Long) null);
        } catch (TimeoutException e) {
            throw new AssertionError(e);
        }
    }

    public R get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return m26057(Long.valueOf(timeUnit.toMillis(j)));
    }

    public synchronized boolean isCancelled() {
        return this.f20151;
    }

    public synchronized boolean isDone() {
        return this.f20151 || this.f20153;
    }

    public void run() {
        if (this.f20149 != null) {
            this.f20149.m26051();
            cancel(false);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m26058() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m26059() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26060() {
        this.f20155.post(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26061(Drawable drawable) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26062() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Request m26063() {
        return this.f20149;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26064(Drawable drawable) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26065(Request request) {
        this.f20149 = request;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26066(SizeReadyCallback sizeReadyCallback) {
        sizeReadyCallback.m26136(this.f20157, this.f20156);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m26067(Exception exc, Drawable drawable) {
        this.f20150 = true;
        this.f20152 = exc;
        this.f20147.m26069(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m26068(R r, GlideAnimation<? super R> glideAnimation) {
        this.f20153 = true;
        this.f20148 = r;
        this.f20147.m26069(this);
    }
}
