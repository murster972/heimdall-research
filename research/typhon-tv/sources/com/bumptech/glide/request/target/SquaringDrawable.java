package com.bumptech.glide.request.target;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;

public class SquaringDrawable extends GlideDrawable {

    /* renamed from: 靐  reason: contains not printable characters */
    private State f20178;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f20179;

    /* renamed from: 龘  reason: contains not printable characters */
    private GlideDrawable f20180;

    static class State extends Drawable.ConstantState {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final int f20181;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final Drawable.ConstantState f20182;

        State(Drawable.ConstantState constantState, int i) {
            this.f20182 = constantState;
            this.f20181 = i;
        }

        State(State state) {
            this(state.f20182, state.f20181);
        }

        public int getChangingConfigurations() {
            return 0;
        }

        public Drawable newDrawable() {
            return newDrawable((Resources) null);
        }

        public Drawable newDrawable(Resources resources) {
            return new SquaringDrawable(this, (GlideDrawable) null, resources);
        }
    }

    public SquaringDrawable(GlideDrawable glideDrawable, int i) {
        this(new State(glideDrawable.getConstantState(), i), glideDrawable, (Resources) null);
    }

    SquaringDrawable(State state, GlideDrawable glideDrawable, Resources resources) {
        this.f20178 = state;
        if (glideDrawable != null) {
            this.f20180 = glideDrawable;
        } else if (resources != null) {
            this.f20180 = (GlideDrawable) state.f20182.newDrawable(resources);
        } else {
            this.f20180 = (GlideDrawable) state.f20182.newDrawable();
        }
    }

    public void clearColorFilter() {
        this.f20180.clearColorFilter();
    }

    public void draw(Canvas canvas) {
        this.f20180.draw(canvas);
    }

    @TargetApi(19)
    public int getAlpha() {
        return this.f20180.getAlpha();
    }

    @TargetApi(11)
    public Drawable.Callback getCallback() {
        return this.f20180.getCallback();
    }

    public int getChangingConfigurations() {
        return this.f20180.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState() {
        return this.f20178;
    }

    public Drawable getCurrent() {
        return this.f20180.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f20178.f20181;
    }

    public int getIntrinsicWidth() {
        return this.f20178.f20181;
    }

    public int getMinimumHeight() {
        return this.f20180.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f20180.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f20180.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.f20180.getPadding(rect);
    }

    public void invalidateSelf() {
        super.invalidateSelf();
        this.f20180.invalidateSelf();
    }

    public boolean isRunning() {
        return this.f20180.isRunning();
    }

    public Drawable mutate() {
        if (!this.f20179 && super.mutate() == this) {
            this.f20180 = (GlideDrawable) this.f20180.mutate();
            this.f20178 = new State(this.f20178);
            this.f20179 = true;
        }
        return this;
    }

    public void scheduleSelf(Runnable runnable, long j) {
        super.scheduleSelf(runnable, j);
        this.f20180.scheduleSelf(runnable, j);
    }

    public void setAlpha(int i) {
        this.f20180.setAlpha(i);
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        this.f20180.setBounds(i, i2, i3, i4);
    }

    public void setBounds(Rect rect) {
        super.setBounds(rect);
        this.f20180.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f20180.setChangingConfigurations(i);
    }

    public void setColorFilter(int i, PorterDuff.Mode mode) {
        this.f20180.setColorFilter(i, mode);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f20180.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f20180.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f20180.setFilterBitmap(z);
    }

    public boolean setVisible(boolean z, boolean z2) {
        return this.f20180.setVisible(z, z2);
    }

    public void start() {
        this.f20180.start();
    }

    public void stop() {
        this.f20180.stop();
    }

    public void unscheduleSelf(Runnable runnable) {
        super.unscheduleSelf(runnable);
        this.f20180.unscheduleSelf(runnable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26137(int i) {
        this.f20180.m25816(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26138() {
        return this.f20180.m25817();
    }
}
