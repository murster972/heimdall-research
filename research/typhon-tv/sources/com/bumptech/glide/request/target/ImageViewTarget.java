package com.bumptech.glide.request.target;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.request.animation.GlideAnimation;

public abstract class ImageViewTarget<Z> extends ViewTarget<ImageView, Z> implements GlideAnimation.ViewAdapter {
    public ImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Drawable m26127() {
        return ((ImageView) this.f20186).getDrawable();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26128(Drawable drawable) {
        ((ImageView) this.f20186).setImageDrawable(drawable);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m26129(Drawable drawable) {
        ((ImageView) this.f20186).setImageDrawable(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26130(Drawable drawable) {
        ((ImageView) this.f20186).setImageDrawable(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26131(Exception exc, Drawable drawable) {
        ((ImageView) this.f20186).setImageDrawable(drawable);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26132(Z z);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26133(Z z, GlideAnimation<? super Z> glideAnimation) {
        if (glideAnimation == null || !glideAnimation.m26097(z, this)) {
            m26132(z);
        }
    }
}
