package com.bumptech.glide.request.target;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import com.bumptech.glide.request.Request;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public abstract class ViewTarget<T extends View, Z> extends BaseTarget<Z> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f20183 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Integer f20184 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private final SizeDeterminer f20185;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final T f20186;

    private static class SizeDeterminer {

        /* renamed from: 靐  reason: contains not printable characters */
        private final List<SizeReadyCallback> f20187 = new ArrayList();

        /* renamed from: 麤  reason: contains not printable characters */
        private Point f20188;

        /* renamed from: 齉  reason: contains not printable characters */
        private SizeDeterminerLayoutListener f20189;

        /* renamed from: 龘  reason: contains not printable characters */
        private final View f20190;

        private static class SizeDeterminerLayoutListener implements ViewTreeObserver.OnPreDrawListener {

            /* renamed from: 龘  reason: contains not printable characters */
            private final WeakReference<SizeDeterminer> f20191;

            public SizeDeterminerLayoutListener(SizeDeterminer sizeDeterminer) {
                this.f20191 = new WeakReference<>(sizeDeterminer);
            }

            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called listener=" + this);
                }
                SizeDeterminer sizeDeterminer = (SizeDeterminer) this.f20191.get();
                if (sizeDeterminer == null) {
                    return true;
                }
                sizeDeterminer.m26157();
                return true;
            }
        }

        public SizeDeterminer(View view) {
            this.f20190 = view;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private int m26153() {
            ViewGroup.LayoutParams layoutParams = this.f20190.getLayoutParams();
            if (m26160(this.f20190.getHeight())) {
                return this.f20190.getHeight();
            }
            if (layoutParams != null) {
                return m26156(layoutParams.height, true);
            }
            return 0;
        }

        @TargetApi(13)
        /* renamed from: 麤  reason: contains not printable characters */
        private Point m26154() {
            if (this.f20188 != null) {
                return this.f20188;
            }
            Display defaultDisplay = ((WindowManager) this.f20190.getContext().getSystemService("window")).getDefaultDisplay();
            if (Build.VERSION.SDK_INT >= 13) {
                this.f20188 = new Point();
                defaultDisplay.getSize(this.f20188);
            } else {
                this.f20188 = new Point(defaultDisplay.getWidth(), defaultDisplay.getHeight());
            }
            return this.f20188;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private int m26155() {
            ViewGroup.LayoutParams layoutParams = this.f20190.getLayoutParams();
            if (m26160(this.f20190.getWidth())) {
                return this.f20190.getWidth();
            }
            if (layoutParams != null) {
                return m26156(layoutParams.width, false);
            }
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m26156(int i, boolean z) {
            if (i != -2) {
                return i;
            }
            Point r0 = m26154();
            return z ? r0.y : r0.x;
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26157() {
            if (!this.f20187.isEmpty()) {
                int r1 = m26155();
                int r0 = m26153();
                if (m26160(r1) && m26160(r0)) {
                    m26158(r1, r0);
                    ViewTreeObserver viewTreeObserver = this.f20190.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.removeOnPreDrawListener(this.f20189);
                    }
                    this.f20189 = null;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m26158(int i, int i2) {
            for (SizeReadyCallback r0 : this.f20187) {
                r0.m26136(i, i2);
            }
            this.f20187.clear();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m26160(int i) {
            return i > 0 || i == -2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m26161(SizeReadyCallback sizeReadyCallback) {
            int r1 = m26155();
            int r0 = m26153();
            if (!m26160(r1) || !m26160(r0)) {
                if (!this.f20187.contains(sizeReadyCallback)) {
                    this.f20187.add(sizeReadyCallback);
                }
                if (this.f20189 == null) {
                    ViewTreeObserver viewTreeObserver = this.f20190.getViewTreeObserver();
                    this.f20189 = new SizeDeterminerLayoutListener(this);
                    viewTreeObserver.addOnPreDrawListener(this.f20189);
                    return;
                }
                return;
            }
            sizeReadyCallback.m26136(r1, r0);
        }
    }

    public ViewTarget(T t) {
        if (t == null) {
            throw new NullPointerException("View must not be null!");
        }
        this.f20186 = t;
        this.f20185 = new SizeDeterminer(t);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Object m26148() {
        return f20184 == null ? this.f20186.getTag() : this.f20186.getTag(f20184.intValue());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26149(Object obj) {
        if (f20184 == null) {
            f20183 = true;
            this.f20186.setTag(obj);
            return;
        }
        this.f20186.setTag(f20184.intValue(), obj);
    }

    public T p_() {
        return this.f20186;
    }

    public String toString() {
        return "Target for: " + this.f20186;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Request m26150() {
        Object r1 = m26148();
        if (r1 == null) {
            return null;
        }
        if (r1 instanceof Request) {
            return (Request) r1;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26151(Request request) {
        m26149((Object) request);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26152(SizeReadyCallback sizeReadyCallback) {
        this.f20185.m26161(sizeReadyCallback);
    }
}
