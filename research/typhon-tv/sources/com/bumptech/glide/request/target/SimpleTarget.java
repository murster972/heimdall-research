package com.bumptech.glide.request.target;

import com.bumptech.glide.util.Util;

public abstract class SimpleTarget<Z> extends BaseTarget<Z> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f20176;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f20177;

    public SimpleTarget() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public SimpleTarget(int i, int i2) {
        this.f20177 = i;
        this.f20176 = i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m26135(SizeReadyCallback sizeReadyCallback) {
        if (!Util.m26202(this.f20177, this.f20176)) {
            throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + this.f20177 + " and height: " + this.f20176 + ", either provide dimensions in the constructor" + " or call override()");
        }
        sizeReadyCallback.m26136(this.f20177, this.f20176);
    }
}
