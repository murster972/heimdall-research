package com.bumptech.glide.request.target;

import android.widget.ImageView;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;

public class GlideDrawableImageViewTarget extends ImageViewTarget<GlideDrawable> {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f20174;

    /* renamed from: 齉  reason: contains not printable characters */
    private GlideDrawable f20175;

    public GlideDrawableImageViewTarget(ImageView imageView) {
        this(imageView, -1);
    }

    public GlideDrawableImageViewTarget(ImageView imageView, int i) {
        super(imageView);
        this.f20174 = i;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m26121() {
        if (this.f20175 != null) {
            this.f20175.stop();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26122() {
        if (this.f20175 != null) {
            this.f20175.start();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26125(GlideDrawable glideDrawable) {
        ((ImageView) this.f20186).setImageDrawable(glideDrawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26126(GlideDrawable glideDrawable, GlideAnimation<? super GlideDrawable> glideAnimation) {
        if (!glideDrawable.m25817()) {
            float width = ((float) ((ImageView) this.f20186).getWidth()) / ((float) ((ImageView) this.f20186).getHeight());
            float intrinsicWidth = ((float) glideDrawable.getIntrinsicWidth()) / ((float) glideDrawable.getIntrinsicHeight());
            if (Math.abs(width - 1.0f) <= 0.05f && Math.abs(intrinsicWidth - 1.0f) <= 0.05f) {
                glideDrawable = new SquaringDrawable(glideDrawable, ((ImageView) this.f20186).getWidth());
            }
        }
        super.m26133(glideDrawable, glideAnimation);
        this.f20175 = glideDrawable;
        glideDrawable.m25816(this.f20174);
        glideDrawable.start();
    }
}
