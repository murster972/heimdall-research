package com.bumptech.glide.request.target;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.manager.LifecycleListener;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;

public interface Target<R> extends LifecycleListener {
    /* renamed from: 靐  reason: contains not printable characters */
    void m26141(Drawable drawable);

    /* renamed from: 龘  reason: contains not printable characters */
    Request m26142();

    /* renamed from: 龘  reason: contains not printable characters */
    void m26143(Drawable drawable);

    /* renamed from: 龘  reason: contains not printable characters */
    void m26144(Request request);

    /* renamed from: 龘  reason: contains not printable characters */
    void m26145(SizeReadyCallback sizeReadyCallback);

    /* renamed from: 龘  reason: contains not printable characters */
    void m26146(Exception exc, Drawable drawable);

    /* renamed from: 龘  reason: contains not printable characters */
    void m26147(R r, GlideAnimation<? super R> glideAnimation);
}
