package com.bumptech.glide.request;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.provider.LoadProvider;
import com.bumptech.glide.request.animation.GlideAnimationFactory;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Util;
import java.util.Queue;

public final class GenericRequest<A, T, Z, R> implements Request, ResourceCallback, SizeReadyCallback {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Queue<GenericRequest<?, ?, ?, ?>> f20107 = Util.m26200(0);

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f20108;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f20109;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Context f20110;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Class<R> f20111;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f20112;

    /* renamed from: ˆ  reason: contains not printable characters */
    private GlideAnimationFactory<R> f20113;

    /* renamed from: ˈ  reason: contains not printable characters */
    private A f20114;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f20115;

    /* renamed from: ˊ  reason: contains not printable characters */
    private RequestListener<? super A, R> f20116;

    /* renamed from: ˋ  reason: contains not printable characters */
    private float f20117;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Engine f20118;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f20119;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Transformation<Z> f20120;

    /* renamed from: י  reason: contains not printable characters */
    private DiskCacheStrategy f20121;

    /* renamed from: ـ  reason: contains not printable characters */
    private Drawable f20122;

    /* renamed from: ٴ  reason: contains not printable characters */
    private LoadProvider<A, T, Z, R> f20123;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private RequestCoordinator f20124;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private Drawable f20125;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f20126;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private Resource<?> f20127;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private Engine.LoadStatus f20128;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private long f20129;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f20130;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f20131 = String.valueOf(hashCode());

    /* renamed from: 麤  reason: contains not printable characters */
    private Drawable f20132;

    /* renamed from: 齉  reason: contains not printable characters */
    private Key f20133;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private Status f20134;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Priority f20135;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Target<R> f20136;

    private enum Status {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CANCELLED,
        CLEARED,
        PAUSED
    }

    private GenericRequest() {
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private Drawable m26018() {
        if (this.f20122 == null && this.f20108 > 0) {
            this.f20122 = this.f20110.getResources().getDrawable(this.f20108);
        }
        return this.f20122;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m26019() {
        return this.f20124 == null || this.f20124.m26056(this);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable m26020() {
        if (this.f20125 == null && this.f20109 > 0) {
            this.f20125 = this.f20110.getResources().getDrawable(this.f20109);
        }
        return this.f20125;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m26021() {
        if (this.f20124 != null) {
            this.f20124.m26054(this);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Drawable m26022() {
        if (this.f20132 == null && this.f20130 > 0) {
            this.f20132 = this.f20110.getResources().getDrawable(this.f20130);
        }
        return this.f20132;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26023(Resource resource) {
        this.f20118.m25449(resource);
        this.f20127 = null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26024(LoadProvider<A, T, Z, R> loadProvider, A a, Key key, Context context, Priority priority, Target<R> target, float f, Drawable drawable, int i, Drawable drawable2, int i2, Drawable drawable3, int i3, RequestListener<? super A, R> requestListener, RequestCoordinator requestCoordinator, Engine engine, Transformation<Z> transformation, Class<R> cls, boolean z, GlideAnimationFactory<R> glideAnimationFactory, int i4, int i5, DiskCacheStrategy diskCacheStrategy) {
        this.f20123 = loadProvider;
        this.f20114 = a;
        this.f20133 = key;
        this.f20132 = drawable3;
        this.f20130 = i3;
        this.f20110 = context.getApplicationContext();
        this.f20135 = priority;
        this.f20136 = target;
        this.f20117 = f;
        this.f20122 = drawable;
        this.f20108 = i;
        this.f20125 = drawable2;
        this.f20109 = i2;
        this.f20116 = requestListener;
        this.f20124 = requestCoordinator;
        this.f20118 = engine;
        this.f20120 = transformation;
        this.f20111 = cls;
        this.f20112 = z;
        this.f20113 = glideAnimationFactory;
        this.f20115 = i4;
        this.f20119 = i5;
        this.f20121 = diskCacheStrategy;
        this.f20134 = Status.PENDING;
        if (a != null) {
            m26029("ModelLoader", loadProvider.m26017(), "try .using(ModelLoader)");
            m26029("Transcoder", loadProvider.m26016(), "try .as*(Class).transcode(ResourceTranscoder)");
            m26029("Transformation", transformation, "try .transform(UnitTransformation.get())");
            if (diskCacheStrategy.cacheSource()) {
                m26029("SourceEncoder", loadProvider.m26001(), "try .sourceEncoder(Encoder) or .diskCacheStrategy(NONE/RESULT)");
            } else {
                m26029("SourceDecoder", loadProvider.m25999(), "try .decoder/.imageDecoder/.videoDecoder(ResourceDecoder) or .diskCacheStrategy(ALL/SOURCE)");
            }
            if (diskCacheStrategy.cacheSource() || diskCacheStrategy.cacheResult()) {
                m26029("CacheDecoder", loadProvider.m26002(), "try .cacheDecoder(ResouceDecoder) or .diskCacheStrategy(NONE)");
            }
            if (diskCacheStrategy.cacheResult()) {
                m26029("Encoder", loadProvider.m26000(), "try .encode(ResourceEncoder) or .diskCacheStrategy(NONE/SOURCE)");
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26025(Exception exc) {
        if (m26030()) {
            Drawable r0 = this.f20114 == null ? m26022() : null;
            if (r0 == null) {
                r0 = m26020();
            }
            if (r0 == null) {
                r0 = m26018();
            }
            this.f20136.m26146(exc, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <A, T, Z, R> GenericRequest<A, T, Z, R> m26026(LoadProvider<A, T, Z, R> loadProvider, A a, Key key, Context context, Priority priority, Target<R> target, float f, Drawable drawable, int i, Drawable drawable2, int i2, Drawable drawable3, int i3, RequestListener<? super A, R> requestListener, RequestCoordinator requestCoordinator, Engine engine, Transformation<Z> transformation, Class<R> cls, boolean z, GlideAnimationFactory<R> glideAnimationFactory, int i4, int i5, DiskCacheStrategy diskCacheStrategy) {
        GenericRequest<A, T, Z, R> poll = f20107.poll();
        if (poll == null) {
            poll = new GenericRequest<>();
        }
        poll.m26024(loadProvider, a, key, context, priority, target, f, drawable, i, drawable2, i2, drawable3, i3, requestListener, requestCoordinator, engine, transformation, cls, z, glideAnimationFactory, i4, i5, diskCacheStrategy);
        return poll;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001b, code lost:
        if (r10.f20116.m26072(r12, r10.f20114, r10.f20136, r10.f20126, r5) == false) goto L_0x001d;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m26027(com.bumptech.glide.load.engine.Resource<?> r11, R r12) {
        /*
            r10 = this;
            boolean r5 = r10.m26031()
            com.bumptech.glide.request.GenericRequest$Status r0 = com.bumptech.glide.request.GenericRequest.Status.COMPLETE
            r10.f20134 = r0
            r10.f20127 = r11
            com.bumptech.glide.request.RequestListener<? super A, R> r0 = r10.f20116
            if (r0 == 0) goto L_0x001d
            com.bumptech.glide.request.RequestListener<? super A, R> r0 = r10.f20116
            A r2 = r10.f20114
            com.bumptech.glide.request.target.Target<R> r3 = r10.f20136
            boolean r4 = r10.f20126
            r1 = r12
            boolean r0 = r0.m26072(r1, r2, r3, r4, r5)
            if (r0 != 0) goto L_0x002a
        L_0x001d:
            com.bumptech.glide.request.animation.GlideAnimationFactory<R> r0 = r10.f20113
            boolean r1 = r10.f20126
            com.bumptech.glide.request.animation.GlideAnimation r6 = r0.m26100(r1, r5)
            com.bumptech.glide.request.target.Target<R> r0 = r10.f20136
            r0.m26147(r12, r6)
        L_0x002a:
            r10.m26021()
            java.lang.String r0 = "GenericRequest"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0074
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Resource ready in "
            java.lang.StringBuilder r0 = r0.append(r1)
            long r2 = r10.f20129
            double r2 = com.bumptech.glide.util.LogTime.m26176(r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " size: "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r11.m25493()
            double r2 = (double) r1
            r8 = 4517110426252607488(0x3eb0000000000000, double:9.5367431640625E-7)
            double r2 = r2 * r8
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " fromCache: "
            java.lang.StringBuilder r0 = r0.append(r1)
            boolean r1 = r10.f20126
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.m26028((java.lang.String) r0)
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.GenericRequest.m26027(com.bumptech.glide.load.engine.Resource, java.lang.Object):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26028(String str) {
        Log.v("GenericRequest", str + " this: " + this.f20131);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26029(String str, Object obj, String str2) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(" must not be null");
            if (str2 != null) {
                sb.append(", ");
                sb.append(str2);
            }
            throw new NullPointerException(sb.toString());
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean m26030() {
        return this.f20124 == null || this.f20124.m26053(this);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean m26031() {
        return this.f20124 == null || !this.f20124.m26055();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m26032() {
        return this.f20134 == Status.RUNNING || this.f20134 == Status.WAITING_FOR_SIZE;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m26033() {
        return this.f20134 == Status.COMPLETE;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m26034() {
        return m26033();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m26035() {
        return this.f20134 == Status.CANCELLED || this.f20134 == Status.CLEARED;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m26036() {
        return this.f20134 == Status.FAILED;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m26037() {
        m26039();
        this.f20134 = Status.PAUSED;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26038() {
        this.f20129 = LogTime.m26177();
        if (this.f20114 == null) {
            m26044((Exception) null);
            return;
        }
        this.f20134 = Status.WAITING_FOR_SIZE;
        if (Util.m26202(this.f20115, this.f20119)) {
            m26042(this.f20115, this.f20119);
        } else {
            this.f20136.m26145((SizeReadyCallback) this);
        }
        if (!m26033() && !m26036() && m26030()) {
            this.f20136.m26143(m26018());
        }
        if (Log.isLoggable("GenericRequest", 2)) {
            m26028("finished run method in " + LogTime.m26176(this.f20129));
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26039() {
        Util.m26201();
        if (this.f20134 != Status.CLEARED) {
            m26040();
            if (this.f20127 != null) {
                m26023((Resource) this.f20127);
            }
            if (m26030()) {
                this.f20136.m26141(m26018());
            }
            this.f20134 = Status.CLEARED;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26040() {
        this.f20134 = Status.CANCELLED;
        if (this.f20128 != null) {
            this.f20128.m25452();
            this.f20128 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26041() {
        this.f20123 = null;
        this.f20114 = null;
        this.f20110 = null;
        this.f20136 = null;
        this.f20122 = null;
        this.f20125 = null;
        this.f20132 = null;
        this.f20116 = null;
        this.f20124 = null;
        this.f20120 = null;
        this.f20113 = null;
        this.f20126 = false;
        this.f20128 = null;
        f20107.offer(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26042(int i, int i2) {
        if (Log.isLoggable("GenericRequest", 2)) {
            m26028("Got onSizeReady in " + LogTime.m26176(this.f20129));
        }
        if (this.f20134 == Status.WAITING_FOR_SIZE) {
            this.f20134 = Status.RUNNING;
            int round = Math.round(this.f20117 * ((float) i));
            int round2 = Math.round(this.f20117 * ((float) i2));
            DataFetcher<T> r4 = this.f20123.m26017().m25663(this.f20114, round, round2);
            if (r4 == null) {
                m26044(new Exception("Failed to load model: '" + this.f20114 + "'"));
                return;
            }
            ResourceTranscoder<Z, R> r7 = this.f20123.m26016();
            if (Log.isLoggable("GenericRequest", 2)) {
                m26028("finished setup for calling load in " + LogTime.m26176(this.f20129));
            }
            this.f20126 = true;
            this.f20128 = this.f20118.m25446(this.f20133, round, round2, r4, this.f20123, this.f20120, r7, this.f20135, this.f20112, this.f20121, this);
            this.f20126 = this.f20127 != null;
            if (Log.isLoggable("GenericRequest", 2)) {
                m26028("finished onSizeReady in " + LogTime.m26176(this.f20129));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26043(Resource<?> resource) {
        if (resource == null) {
            m26044(new Exception("Expected to receive a Resource<R> with an object of " + this.f20111 + " inside, but instead got null."));
            return;
        }
        Object r0 = resource.m25491();
        if (r0 == null || !this.f20111.isAssignableFrom(r0.getClass())) {
            m26023((Resource) resource);
            m26044(new Exception("Expected to receive an object of " + this.f20111 + " but instead got " + (r0 != null ? r0.getClass() : "") + "{" + r0 + "}" + " inside Resource{" + resource + "}." + (r0 != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.")));
        } else if (!m26019()) {
            m26023((Resource) resource);
            this.f20134 = Status.COMPLETE;
        } else {
            m26027(resource, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26044(Exception exc) {
        if (Log.isLoggable("GenericRequest", 3)) {
            Log.d("GenericRequest", "load failed", exc);
        }
        this.f20134 = Status.FAILED;
        if (this.f20116 == null || !this.f20116.m26071(exc, this.f20114, this.f20136, m26031())) {
            m26025(exc);
        }
    }
}
