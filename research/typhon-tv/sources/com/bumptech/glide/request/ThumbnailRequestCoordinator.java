package com.bumptech.glide.request;

public class ThumbnailRequestCoordinator implements Request, RequestCoordinator {

    /* renamed from: 靐  reason: contains not printable characters */
    private Request f20158;

    /* renamed from: 齉  reason: contains not printable characters */
    private RequestCoordinator f20159;

    /* renamed from: 龘  reason: contains not printable characters */
    private Request f20160;

    public ThumbnailRequestCoordinator() {
        this((RequestCoordinator) null);
    }

    public ThumbnailRequestCoordinator(RequestCoordinator requestCoordinator) {
        this.f20159 = requestCoordinator;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean m26075() {
        return this.f20159 != null && this.f20159.m26055();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean m26076() {
        return this.f20159 == null || this.f20159.m26056(this);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean m26077() {
        return this.f20159 == null || this.f20159.m26053(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m26078() {
        return this.f20160.m26045();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m26079() {
        return this.f20160.m26046() || this.f20158.m26046();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m26080() {
        return this.f20160.m26047() || this.f20158.m26047();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m26081() {
        return this.f20160.m26048();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m26082() {
        this.f20160.m26049();
        this.f20158.m26049();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26083() {
        if (!this.f20158.m26045()) {
            this.f20158.m26050();
        }
        if (!this.f20160.m26045()) {
            this.f20160.m26050();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m26084(Request request) {
        return m26077() && request.equals(this.f20160) && !m26087();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26085() {
        this.f20158.m26051();
        this.f20160.m26051();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m26086(Request request) {
        if (!request.equals(this.f20158)) {
            if (this.f20159 != null) {
                this.f20159.m26054(this);
            }
            if (!this.f20158.m26046()) {
                this.f20158.m26051();
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m26087() {
        return m26075() || m26080();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26088() {
        this.f20160.m26052();
        this.f20158.m26052();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26089(Request request, Request request2) {
        this.f20160 = request;
        this.f20158 = request2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26090(Request request) {
        return m26076() && (request.equals(this.f20160) || !this.f20160.m26047());
    }
}
