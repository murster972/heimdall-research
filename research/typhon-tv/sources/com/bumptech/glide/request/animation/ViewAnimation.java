package com.bumptech.glide.request.animation;

import android.view.View;
import android.view.animation.Animation;
import com.bumptech.glide.request.animation.GlideAnimation;

public class ViewAnimation<R> implements GlideAnimation<R> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final AnimationFactory f20170;

    interface AnimationFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        Animation m26107();
    }

    ViewAnimation(AnimationFactory animationFactory) {
        this.f20170 = animationFactory;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26106(R r, GlideAnimation.ViewAdapter viewAdapter) {
        View p_ = viewAdapter.p_();
        if (p_ == null) {
            return false;
        }
        p_.clearAnimation();
        p_.startAnimation(this.f20170.m26107());
        return false;
    }
}
