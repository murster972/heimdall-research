package com.bumptech.glide.request.animation;

import android.graphics.drawable.Drawable;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import com.bumptech.glide.request.animation.ViewAnimation;

public class DrawableCrossFadeFactory<T extends Drawable> implements GlideAnimationFactory<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f20161;

    /* renamed from: 麤  reason: contains not printable characters */
    private DrawableCrossFadeViewAnimation<T> f20162;

    /* renamed from: 齉  reason: contains not printable characters */
    private DrawableCrossFadeViewAnimation<T> f20163;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ViewAnimationFactory<T> f20164;

    private static class DefaultAnimationFactory implements ViewAnimation.AnimationFactory {

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f20165;

        DefaultAnimationFactory(int i) {
            this.f20165 = i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Animation m26094() {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration((long) this.f20165);
            return alphaAnimation;
        }
    }

    public DrawableCrossFadeFactory() {
        this(300);
    }

    public DrawableCrossFadeFactory(int i) {
        this(new ViewAnimationFactory(new DefaultAnimationFactory(i)), i);
    }

    DrawableCrossFadeFactory(ViewAnimationFactory<T> viewAnimationFactory, int i) {
        this.f20164 = viewAnimationFactory;
        this.f20161 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private GlideAnimation<T> m26091() {
        if (this.f20162 == null) {
            this.f20162 = new DrawableCrossFadeViewAnimation<>(this.f20164.m26108(false, false), this.f20161);
        }
        return this.f20162;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GlideAnimation<T> m26092() {
        if (this.f20163 == null) {
            this.f20163 = new DrawableCrossFadeViewAnimation<>(this.f20164.m26108(false, true), this.f20161);
        }
        return this.f20163;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GlideAnimation<T> m26093(boolean z, boolean z2) {
        return z ? NoAnimation.m26101() : z2 ? m26092() : m26091();
    }
}
