package com.bumptech.glide.request.animation;

import com.bumptech.glide.request.animation.ViewAnimation;

public class ViewAnimationFactory<R> implements GlideAnimationFactory<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    private GlideAnimation<R> f20171;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ViewAnimation.AnimationFactory f20172;

    ViewAnimationFactory(ViewAnimation.AnimationFactory animationFactory) {
        this.f20172 = animationFactory;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GlideAnimation<R> m26108(boolean z, boolean z2) {
        if (z || !z2) {
            return NoAnimation.m26101();
        }
        if (this.f20171 == null) {
            this.f20171 = new ViewAnimation(this.f20172);
        }
        return this.f20171;
    }
}
