package com.bumptech.glide.request.animation;

import com.bumptech.glide.request.animation.GlideAnimation;

public class NoAnimation<R> implements GlideAnimation<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final GlideAnimationFactory<?> f20168 = new NoAnimationFactory();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final NoAnimation<?> f20169 = new NoAnimation<>();

    public static class NoAnimationFactory<R> implements GlideAnimationFactory<R> {
        /* renamed from: 龘  reason: contains not printable characters */
        public GlideAnimation<R> m26105(boolean z, boolean z2) {
            return NoAnimation.f20169;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <R> GlideAnimation<R> m26101() {
        return f20169;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <R> GlideAnimationFactory<R> m26103() {
        return f20168;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26104(Object obj, GlideAnimation.ViewAdapter viewAdapter) {
        return false;
    }
}
