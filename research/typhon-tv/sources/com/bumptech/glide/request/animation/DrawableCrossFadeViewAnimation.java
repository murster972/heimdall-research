package com.bumptech.glide.request.animation;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;

public class DrawableCrossFadeViewAnimation<T extends Drawable> implements GlideAnimation<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f20166;

    /* renamed from: 龘  reason: contains not printable characters */
    private final GlideAnimation<T> f20167;

    public DrawableCrossFadeViewAnimation(GlideAnimation<T> glideAnimation, int i) {
        this.f20167 = glideAnimation;
        this.f20166 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26096(T t, GlideAnimation.ViewAdapter viewAdapter) {
        Drawable r0 = viewAdapter.m26098();
        if (r0 != null) {
            TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{r0, t});
            transitionDrawable.setCrossFadeEnabled(true);
            transitionDrawable.startTransition(this.f20166);
            viewAdapter.m26099(transitionDrawable);
            return true;
        }
        this.f20167.m26097(t, viewAdapter);
        return false;
    }
}
