package com.bumptech.glide.request.animation;

import android.graphics.drawable.Drawable;
import android.view.View;

public interface GlideAnimation<R> {

    public interface ViewAdapter {
        View p_();

        /* renamed from: 靐  reason: contains not printable characters */
        Drawable m26098();

        /* renamed from: 齉  reason: contains not printable characters */
        void m26099(Drawable drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m26097(R r, ViewAdapter viewAdapter);
}
