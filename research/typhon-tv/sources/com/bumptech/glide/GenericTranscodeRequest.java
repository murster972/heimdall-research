package com.bumptech.glide;

import android.content.Context;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.load.resource.transcode.UnitTranscoder;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.provider.FixedLoadProvider;
import com.bumptech.glide.provider.LoadProvider;

public class GenericTranscodeRequest<ModelType, DataType, ResourceType> extends GenericRequestBuilder<ModelType, DataType, ResourceType, ResourceType> {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ModelLoader<ModelType, DataType> f19544;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Class<DataType> f19545;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Class<ResourceType> f19546;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final RequestManager.OptionsApplier f19547;

    GenericTranscodeRequest(Context context, Glide glide, Class<ModelType> cls, ModelLoader<ModelType, DataType> modelLoader, Class<DataType> cls2, Class<ResourceType> cls3, RequestTracker requestTracker, Lifecycle lifecycle, RequestManager.OptionsApplier optionsApplier) {
        super(context, cls, m25234(glide, modelLoader, cls2, cls3, UnitTranscoder.m25933()), cls3, glide, requestTracker, lifecycle);
        this.f19544 = modelLoader;
        this.f19545 = cls2;
        this.f19546 = cls3;
        this.f19547 = optionsApplier;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <A, T, Z, R> LoadProvider<A, T, Z, R> m25234(Glide glide, ModelLoader<A, T> modelLoader, Class<T> cls, Class<Z> cls2, ResourceTranscoder<Z, R> resourceTranscoder) {
        return new FixedLoadProvider(modelLoader, resourceTranscoder, glide.m3949(cls, cls2));
    }
}
