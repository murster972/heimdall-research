package com.facebook.device.yearclass;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collections;

public class YearClass {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile Integer f6609;

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m7604() {
        long r0 = (long) DeviceInfo.m7596();
        if (r0 == -1) {
            return -1;
        }
        if (r0 <= 528000) {
            return 2008;
        }
        if (r0 <= 620000) {
            return 2009;
        }
        if (r0 <= 1020000) {
            return 2010;
        }
        if (r0 <= 1220000) {
            return 2011;
        }
        if (r0 <= 1520000) {
            return 2012;
        }
        return r0 <= 2020000 ? 2013 : 2014;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m7605(Context context) {
        long r0 = DeviceInfo.m7603(context);
        if (r0 == -1) {
            return m7607(context);
        }
        if (r0 <= 805306368) {
            return DeviceInfo.m7599() <= 1 ? 2009 : 2010;
        }
        if (r0 <= 1073741824) {
            return DeviceInfo.m7596() < 1300000 ? 2011 : 2012;
        }
        if (r0 <= 1610612736) {
            return DeviceInfo.m7596() >= 1800000 ? 2013 : 2012;
        }
        if (r0 <= 2147483648L) {
            return 2013;
        }
        if (r0 <= 3221225472L) {
            return 2014;
        }
        return r0 <= 5368709120L ? 2015 : 2016;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static int m7606(Context context) {
        long r0 = DeviceInfo.m7603(context);
        if (r0 <= 0) {
            return -1;
        }
        if (r0 <= 201326592) {
            return 2008;
        }
        if (r0 <= 304087040) {
            return 2009;
        }
        if (r0 <= 536870912) {
            return 2010;
        }
        if (r0 <= 1073741824) {
            return 2011;
        }
        if (r0 <= 1610612736) {
            return 2012;
        }
        return r0 <= 2147483648L ? 2013 : 2014;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m7607(Context context) {
        ArrayList arrayList = new ArrayList();
        m7610(arrayList, m7608());
        m7610(arrayList, m7604());
        m7610(arrayList, m7606(context));
        if (arrayList.isEmpty()) {
            return -1;
        }
        Collections.sort(arrayList);
        if ((arrayList.size() & 1) == 1) {
            return ((Integer) arrayList.get(arrayList.size() / 2)).intValue();
        }
        int size = (arrayList.size() / 2) - 1;
        return ((((Integer) arrayList.get(size + 1)).intValue() - ((Integer) arrayList.get(size)).intValue()) / 2) + ((Integer) arrayList.get(size)).intValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m7608() {
        int r0 = DeviceInfo.m7599();
        if (r0 < 1) {
            return -1;
        }
        if (r0 == 1) {
            return 2008;
        }
        return r0 <= 3 ? 2011 : 2012;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7609(Context context) {
        if (f6609 == null) {
            synchronized (YearClass.class) {
                if (f6609 == null) {
                    f6609 = Integer.valueOf(m7605(context));
                }
            }
        }
        return f6609.intValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m7610(ArrayList<Integer> arrayList, int i) {
        if (i != -1) {
            arrayList.add(Integer.valueOf(i));
        }
    }
}
