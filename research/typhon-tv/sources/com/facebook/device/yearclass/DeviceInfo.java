package com.facebook.device.yearclass;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.media.session.PlaybackStateCompat;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;

public class DeviceInfo {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final FileFilter f6608 = new FileFilter() {
        public boolean accept(File file) {
            String name = file.getName();
            if (!name.startsWith("cpu")) {
                return false;
            }
            for (int i = 3; i < name.length(); i++) {
                if (!Character.isDigit(name.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    };

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int m7596() {
        /*
            r7 = -1
            r6 = 0
        L_0x0002:
            int r10 = m7599()     // Catch:{ IOException -> 0x0072 }
            if (r6 >= r10) goto L_0x007a
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0072 }
            r10.<init>()     // Catch:{ IOException -> 0x0072 }
            java.lang.String r11 = "/sys/devices/system/cpu/cpu"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x0072 }
            java.lang.StringBuilder r10 = r10.append(r6)     // Catch:{ IOException -> 0x0072 }
            java.lang.String r11 = "/cpufreq/cpuinfo_max_freq"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x0072 }
            java.lang.String r4 = r10.toString()     // Catch:{ IOException -> 0x0072 }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0072 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0072 }
            boolean r10 = r1.exists()     // Catch:{ IOException -> 0x0072 }
            if (r10 == 0) goto L_0x006a
            boolean r10 = r1.canRead()     // Catch:{ IOException -> 0x0072 }
            if (r10 == 0) goto L_0x006a
            r10 = 128(0x80, float:1.794E-43)
            byte[] r0 = new byte[r10]     // Catch:{ IOException -> 0x0072 }
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0072 }
            r9.<init>(r1)     // Catch:{ IOException -> 0x0072 }
            r9.read(r0)     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            r3 = 0
        L_0x0041:
            byte r10 = r0[r3]     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            boolean r10 = java.lang.Character.isDigit(r10)     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            if (r10 == 0) goto L_0x004f
            int r10 = r0.length     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            if (r3 >= r10) goto L_0x004f
            int r3 = r3 + 1
            goto L_0x0041
        L_0x004f:
            java.lang.String r8 = new java.lang.String     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            r10 = 0
            r8.<init>(r0, r10, r3)     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            int r10 = java.lang.Integer.parseInt(r8)     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r10)     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            int r10 = r5.intValue()     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
            if (r10 <= r7) goto L_0x0067
            int r7 = r5.intValue()     // Catch:{ NumberFormatException -> 0x006d, all -> 0x0075 }
        L_0x0067:
            r9.close()     // Catch:{ IOException -> 0x0072 }
        L_0x006a:
            int r6 = r6 + 1
            goto L_0x0002
        L_0x006d:
            r10 = move-exception
            r9.close()     // Catch:{ IOException -> 0x0072 }
            goto L_0x006a
        L_0x0072:
            r2 = move-exception
            r7 = -1
        L_0x0074:
            return r7
        L_0x0075:
            r10 = move-exception
            r9.close()     // Catch:{ IOException -> 0x0072 }
            throw r10     // Catch:{ IOException -> 0x0072 }
        L_0x007a:
            r10 = -1
            if (r7 != r10) goto L_0x0074
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0072 }
            java.lang.String r10 = "/proc/cpuinfo"
            r9.<init>(r10)     // Catch:{ IOException -> 0x0072 }
            java.lang.String r10 = "cpu MHz"
            int r5 = m7601((java.lang.String) r10, (java.io.FileInputStream) r9)     // Catch:{ all -> 0x0095 }
            int r5 = r5 * 1000
            if (r5 <= r7) goto L_0x0091
            r7 = r5
        L_0x0091:
            r9.close()     // Catch:{ IOException -> 0x0072 }
            goto L_0x0074
        L_0x0095:
            r10 = move-exception
            r9.close()     // Catch:{ IOException -> 0x0072 }
            throw r10     // Catch:{ IOException -> 0x0072 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.device.yearclass.DeviceInfo.m7596():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x002f A[SYNTHETIC, Splitter:B:18:0x002f] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int m7597(java.lang.String r7) {
        /*
            r3 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0022, all -> 0x002c }
            r4.<init>(r7)     // Catch:{ IOException -> 0x0022, all -> 0x002c }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            r0.<init>(r5)     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            java.lang.String r2 = r0.readLine()     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            r0.close()     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            int r5 = m7600((java.lang.String) r2)     // Catch:{ IOException -> 0x003a, all -> 0x0037 }
            if (r4 == 0) goto L_0x0020
            r4.close()     // Catch:{ IOException -> 0x0033 }
        L_0x0020:
            r3 = r4
        L_0x0021:
            return r5
        L_0x0022:
            r1 = move-exception
        L_0x0023:
            r5 = -1
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0021
        L_0x002a:
            r6 = move-exception
            goto L_0x0021
        L_0x002c:
            r5 = move-exception
        L_0x002d:
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0032:
            throw r5
        L_0x0033:
            r6 = move-exception
            goto L_0x0020
        L_0x0035:
            r6 = move-exception
            goto L_0x0032
        L_0x0037:
            r5 = move-exception
            r3 = r4
            goto L_0x002d
        L_0x003a:
            r1 = move-exception
            r3 = r4
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.device.yearclass.DeviceInfo.m7597(java.lang.String):int");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m7598() {
        return new File("/sys/devices/system/cpu/").listFiles(f6608).length;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7599() {
        if (Build.VERSION.SDK_INT <= 10) {
            return 1;
        }
        try {
            int r0 = m7597("/sys/devices/system/cpu/possible");
            if (r0 == -1) {
                r0 = m7597("/sys/devices/system/cpu/present");
            }
            return r0 == -1 ? m7598() : r0;
        } catch (SecurityException e) {
            return -1;
        } catch (NullPointerException e2) {
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m7600(String str) {
        if (str == null || !str.matches("0-[\\d]+$")) {
            return -1;
        }
        return Integer.valueOf(str.substring(2)).intValue() + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m7601(String str, FileInputStream fileInputStream) {
        byte[] bArr = new byte[1024];
        try {
            int read = fileInputStream.read(bArr);
            int i = 0;
            while (i < read) {
                if (bArr[i] == 10 || i == 0) {
                    if (bArr[i] == 10) {
                        i++;
                    }
                    int i2 = i;
                    while (i2 < read) {
                        int i3 = i2 - i;
                        if (bArr[i2] != str.charAt(i3)) {
                            continue;
                            break;
                        } else if (i3 == str.length() - 1) {
                            return m7602(bArr, i2);
                        } else {
                            i2++;
                        }
                    }
                    continue;
                }
                i++;
            }
        } catch (IOException | NumberFormatException e) {
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m7602(byte[] bArr, int i) {
        while (i < bArr.length && bArr[i] != 10) {
            if (Character.isDigit(bArr[i])) {
                int i2 = i;
                while (true) {
                    i++;
                    if (i >= bArr.length || !Character.isDigit(bArr[i])) {
                    }
                }
                return Integer.parseInt(new String(bArr, 0, i2, i - i2));
            }
            i++;
        }
        return -1;
    }

    @TargetApi(16)
    /* renamed from: 龘  reason: contains not printable characters */
    public static long m7603(Context context) {
        FileInputStream fileInputStream;
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
            if (memoryInfo != null) {
                return memoryInfo.totalMem;
            }
            return -1;
        }
        long j = -1;
        try {
            fileInputStream = new FileInputStream("/proc/meminfo");
            j = ((long) m7601("MemTotal", fileInputStream)) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
            fileInputStream.close();
            return j;
        } catch (IOException e) {
            return j;
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
    }
}
