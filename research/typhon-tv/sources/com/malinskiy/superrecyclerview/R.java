package com.malinskiy.superrecyclerview;

public final class R {

    public static final class attr {
        public static final int drag_edge = 2130968791;
        public static final int horizontalSwipeOffset = 2130968841;
        public static final int layoutManager = 2130968861;
        public static final int layout_empty = 2130968869;
        public static final int layout_moreProgress = 2130968880;
        public static final int layout_progress = 2130968881;
        public static final int mainLayoutId = 2130968900;
        public static final int recyclerClipToPadding = 2130969005;
        public static final int recyclerPadding = 2130969006;
        public static final int recyclerPaddingBottom = 2130969007;
        public static final int recyclerPaddingLeft = 2130969008;
        public static final int recyclerPaddingRight = 2130969009;
        public static final int recyclerPaddingTop = 2130969010;
        public static final int reverseLayout = 2130969013;
        public static final int scrollbarStyle = 2130969021;
        public static final int show_mode = 2130969036;
        public static final int spanCount = 2130969039;
        public static final int stackFromEnd = 2130969045;
        public static final int verticalSwipeOffset = 2130969126;
    }

    public static final class dimen {
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165390;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165391;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165392;
        public static final int swipe_progress_bar_height = 2131165516;
    }

    public static final class id {
        public static final int bottom = 2131296371;
        public static final int empty = 2131296457;
        public static final int insideInset = 2131296514;
        public static final int insideOverlay = 2131296515;
        public static final int item_touch_helper_previous_elevation = 2131296523;
        public static final int lay_down = 2131296539;
        public static final int left = 2131296555;
        public static final int more_progress = 2131296707;
        public static final int outsideInset = 2131296768;
        public static final int outsideOverlay = 2131296769;
        public static final int ptr_layout = 2131296792;
        public static final int pull_out = 2131296793;
        public static final int recyclerview_swipe = 2131296797;
        public static final int right = 2131296829;
        public static final int top = 2131296929;
    }

    public static final class layout {
        public static final int layout_more_progress = 2131492984;
        public static final int layout_progress = 2131492985;
        public static final int layout_progress_recyclerview = 2131492986;
        public static final int layout_recyclerview_horizontalscroll = 2131492987;
        public static final int layout_recyclerview_verticalscroll = 2131492988;
    }

    public static final class styleable {
        public static final int[] RecyclerView = {16842948, 16842993, com.typhoon.tv.R.attr.fastScrollEnabled, com.typhoon.tv.R.attr.fastScrollHorizontalThumbDrawable, com.typhoon.tv.R.attr.fastScrollHorizontalTrackDrawable, com.typhoon.tv.R.attr.fastScrollVerticalThumbDrawable, com.typhoon.tv.R.attr.fastScrollVerticalTrackDrawable, com.typhoon.tv.R.attr.layoutManager, com.typhoon.tv.R.attr.reverseLayout, com.typhoon.tv.R.attr.spanCount, com.typhoon.tv.R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 2;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
        public static final int RecyclerView_layoutManager = 7;
        public static final int RecyclerView_reverseLayout = 8;
        public static final int RecyclerView_spanCount = 9;
        public static final int RecyclerView_stackFromEnd = 10;
        public static final int[] SwipeLayout = {com.typhoon.tv.R.attr.drag_edge, com.typhoon.tv.R.attr.horizontalSwipeOffset, com.typhoon.tv.R.attr.show_mode, com.typhoon.tv.R.attr.verticalSwipeOffset};
        public static final int SwipeLayout_drag_edge = 0;
        public static final int SwipeLayout_horizontalSwipeOffset = 1;
        public static final int SwipeLayout_show_mode = 2;
        public static final int SwipeLayout_verticalSwipeOffset = 3;
        public static final int[] superrecyclerview = {com.typhoon.tv.R.attr.layout_empty, com.typhoon.tv.R.attr.layout_moreProgress, com.typhoon.tv.R.attr.layout_progress, com.typhoon.tv.R.attr.mainLayoutId, com.typhoon.tv.R.attr.recyclerClipToPadding, com.typhoon.tv.R.attr.recyclerPadding, com.typhoon.tv.R.attr.recyclerPaddingBottom, com.typhoon.tv.R.attr.recyclerPaddingLeft, com.typhoon.tv.R.attr.recyclerPaddingRight, com.typhoon.tv.R.attr.recyclerPaddingTop, com.typhoon.tv.R.attr.scrollbarStyle};
        public static final int superrecyclerview_layout_empty = 0;
        public static final int superrecyclerview_layout_moreProgress = 1;
        public static final int superrecyclerview_layout_progress = 2;
        public static final int superrecyclerview_mainLayoutId = 3;
        public static final int superrecyclerview_recyclerClipToPadding = 4;
        public static final int superrecyclerview_recyclerPadding = 5;
        public static final int superrecyclerview_recyclerPaddingBottom = 6;
        public static final int superrecyclerview_recyclerPaddingLeft = 7;
        public static final int superrecyclerview_recyclerPaddingRight = 8;
        public static final int superrecyclerview_recyclerPaddingTop = 9;
        public static final int superrecyclerview_scrollbarStyle = 10;
    }
}
