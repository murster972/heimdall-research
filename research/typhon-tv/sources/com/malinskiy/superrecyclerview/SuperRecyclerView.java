package com.malinskiy.superrecyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import com.malinskiy.superrecyclerview.swipe.SwipeDismissRecyclerViewTouchListener;
import com.malinskiy.superrecyclerview.util.FloatUtil;

public class SuperRecyclerView extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected View f20355;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected View f20356;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected View f20357;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected int f20358;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected int f20359;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected RecyclerView.OnScrollListener f20360;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected int f20361;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected OnMoreListener f20362;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected int f20363;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected LAYOUT_MANAGER_TYPE f20364;

    /* renamed from: ˎ  reason: contains not printable characters */
    protected RecyclerView.OnScrollListener f20365;

    /* renamed from: ˏ  reason: contains not printable characters */
    protected boolean f20366;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected boolean f20367;

    /* renamed from: י  reason: contains not printable characters */
    protected SwipeRefreshLayout f20368;

    /* renamed from: ـ  reason: contains not printable characters */
    protected int f20369;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected int f20370;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected int f20371;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public RecyclerView.OnScrollListener f20372;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f20373;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int[] f20374;

    /* renamed from: 连任  reason: contains not printable characters */
    protected ViewStub f20375;

    /* renamed from: 靐  reason: contains not printable characters */
    protected RecyclerView f20376;

    /* renamed from: 麤  reason: contains not printable characters */
    protected ViewStub f20377;

    /* renamed from: 齉  reason: contains not printable characters */
    protected ViewStub f20378;

    /* renamed from: 龘  reason: contains not printable characters */
    protected int f20379 = 10;

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected int f20380;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected int f20381;

    public enum LAYOUT_MANAGER_TYPE {
        LINEAR,
        GRID,
        STAGGERED_GRID
    }

    public SuperRecyclerView(Context context) {
        super(context);
        m26353();
    }

    public SuperRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m26369(attributeSet);
        m26353();
    }

    public SuperRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m26369(attributeSet);
        m26353();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m26353() {
        if (!isInEditMode()) {
            View inflate = LayoutInflater.from(getContext()).inflate(this.f20369, this);
            this.f20368 = (SwipeRefreshLayout) inflate.findViewById(R.id.ptr_layout);
            this.f20368.setEnabled(false);
            this.f20378 = (ViewStub) inflate.findViewById(16908301);
            this.f20378.setLayoutResource(this.f20373);
            this.f20355 = this.f20378.inflate();
            this.f20377 = (ViewStub) inflate.findViewById(R.id.more_progress);
            this.f20377.setLayoutResource(this.f20363);
            if (this.f20363 != 0) {
                this.f20356 = this.f20377.inflate();
            }
            this.f20377.setVisibility(8);
            this.f20375 = (ViewStub) inflate.findViewById(R.id.empty);
            this.f20375.setLayoutResource(this.f20381);
            if (this.f20381 != 0) {
                this.f20357 = this.f20375.inflate();
            }
            this.f20375.setVisibility(8);
            m26370(inflate);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m26354() {
        RecyclerView.LayoutManager layoutManager = this.f20376.getLayoutManager();
        int r0 = m26357(layoutManager);
        int childCount = layoutManager.getChildCount();
        int itemCount = layoutManager.getItemCount();
        if ((itemCount - r0 <= this.f20379 || (itemCount - r0 == 0 && itemCount > childCount)) && !this.f20366) {
            this.f20366 = true;
            if (this.f20362 != null) {
                this.f20377.setVisibility(0);
                this.f20362.m26352(this.f20376.getAdapter().getItemCount(), this.f20379, r0);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m26355(RecyclerView.LayoutManager layoutManager) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
        if (this.f20374 == null) {
            this.f20374 = new int[staggeredGridLayoutManager.getSpanCount()];
        }
        staggeredGridLayoutManager.findLastVisibleItemPositions(this.f20374);
        return m26358(this.f20374);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m26357(RecyclerView.LayoutManager layoutManager) {
        if (this.f20364 == null) {
            if (layoutManager instanceof GridLayoutManager) {
                this.f20364 = LAYOUT_MANAGER_TYPE.GRID;
            } else if (layoutManager instanceof LinearLayoutManager) {
                this.f20364 = LAYOUT_MANAGER_TYPE.LINEAR;
            } else if (layoutManager instanceof StaggeredGridLayoutManager) {
                this.f20364 = LAYOUT_MANAGER_TYPE.STAGGERED_GRID;
            } else {
                throw new RuntimeException("Unsupported LayoutManager used. Valid ones are LinearLayoutManager, GridLayoutManager and StaggeredGridLayoutManager");
            }
        }
        switch (this.f20364) {
            case LINEAR:
                return ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
            case GRID:
                return ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
            case STAGGERED_GRID:
                return m26355(layoutManager);
            default:
                return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m26358(int[] iArr) {
        int i = Integer.MIN_VALUE;
        for (int i2 : iArr) {
            if (i2 > i) {
                i = i2;
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26359(RecyclerView.Adapter adapter, boolean z, boolean z2) {
        int i = 8;
        if (z) {
            this.f20376.swapAdapter(adapter, z2);
        } else {
            this.f20376.setAdapter(adapter);
        }
        this.f20378.setVisibility(8);
        this.f20376.setVisibility(0);
        this.f20368.setRefreshing(false);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                /* renamed from: 龘  reason: contains not printable characters */
                private void m26371() {
                    SuperRecyclerView.this.f20378.setVisibility(8);
                    SuperRecyclerView.this.f20377.setVisibility(8);
                    SuperRecyclerView.this.f20366 = false;
                    SuperRecyclerView.this.f20368.setRefreshing(false);
                    if (SuperRecyclerView.this.f20376.getAdapter().getItemCount() == 0 && SuperRecyclerView.this.f20381 != 0) {
                        SuperRecyclerView.this.f20375.setVisibility(0);
                    } else if (SuperRecyclerView.this.f20381 != 0) {
                        SuperRecyclerView.this.f20375.setVisibility(8);
                    }
                }

                public void onChanged() {
                    super.onChanged();
                    m26371();
                }

                public void onItemRangeChanged(int i, int i2) {
                    super.onItemRangeChanged(i, i2);
                    m26371();
                }

                public void onItemRangeInserted(int i, int i2) {
                    super.onItemRangeInserted(i, i2);
                    m26371();
                }

                public void onItemRangeMoved(int i, int i2, int i3) {
                    super.onItemRangeMoved(i, i2, i3);
                    m26371();
                }

                public void onItemRangeRemoved(int i, int i2) {
                    super.onItemRangeRemoved(i, i2);
                    m26371();
                }
            });
        }
        if (this.f20381 != 0) {
            ViewStub viewStub = this.f20375;
            if (adapter == null || adapter.getItemCount() <= 0) {
                i = 0;
            }
            viewStub.setVisibility(i);
        }
    }

    public RecyclerView.Adapter getAdapter() {
        return this.f20376.getAdapter();
    }

    public View getEmptyView() {
        return this.f20357;
    }

    public View getMoreProgressView() {
        return this.f20356;
    }

    public View getProgressView() {
        return this.f20355;
    }

    public RecyclerView getRecyclerView() {
        return this.f20376;
    }

    public SwipeRefreshLayout getSwipeToRefresh() {
        return this.f20368;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        m26359(adapter, false, true);
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.f20376.setLayoutManager(layoutManager);
    }

    public void setLoadingMore(boolean z) {
        this.f20366 = z;
    }

    public void setNumberBeforeMoreIsCalled(int i) {
        this.f20379 = i;
    }

    public void setOnMoreListener(OnMoreListener onMoreListener) {
        this.f20362 = onMoreListener;
    }

    public void setOnScrollListener(RecyclerView.OnScrollListener onScrollListener) {
        this.f20360 = onScrollListener;
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.f20376.setOnTouchListener(onTouchListener);
    }

    public void setRefreshListener(SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        this.f20368.setEnabled(true);
        this.f20368.setOnRefreshListener(onRefreshListener);
    }

    public void setRefreshing(boolean z) {
        this.f20368.setRefreshing(z);
    }

    public void setRefreshingColor(int i, int i2, int i3, int i4) {
        this.f20368.setColorSchemeColors(i, i2, i3, i4);
    }

    public void setRefreshingColorResources(int i, int i2, int i3, int i4) {
        this.f20368.setColorSchemeResources(i, i2, i3, i4);
    }

    public void setupMoreListener(OnMoreListener onMoreListener, int i) {
        this.f20362 = onMoreListener;
        this.f20379 = i;
    }

    public void setupSwipeToDismiss(final SwipeDismissRecyclerViewTouchListener.DismissCallbacks dismissCallbacks) {
        SwipeDismissRecyclerViewTouchListener swipeDismissRecyclerViewTouchListener = new SwipeDismissRecyclerViewTouchListener(this.f20376, new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m26372(RecyclerView recyclerView, int[] iArr) {
                dismissCallbacks.m26391(recyclerView, iArr);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m26373(int i) {
                return dismissCallbacks.m26392(i);
            }
        });
        this.f20372 = swipeDismissRecyclerViewTouchListener.m26386();
        this.f20376.setOnTouchListener(swipeDismissRecyclerViewTouchListener);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m26361() {
        this.f20376.setVisibility(8);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m26362() {
        this.f20362 = null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m26363() {
        this.f20378.setVisibility(8);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26364() {
        m26363();
        if (this.f20376.getAdapter().getItemCount() == 0 && this.f20381 != 0) {
            this.f20375.setVisibility(0);
        } else if (this.f20381 != 0) {
            this.f20375.setVisibility(8);
        }
        this.f20376.setVisibility(0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26365() {
        this.f20377.setVisibility(8);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m26366() {
        this.f20377.setVisibility(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26367() {
        m26361();
        if (this.f20381 != 0) {
            this.f20375.setVisibility(4);
        }
        this.f20378.setVisibility(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26368(RecyclerView.ItemDecoration itemDecoration) {
        this.f20376.addItemDecoration(itemDecoration);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26369(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.styleable.superrecyclerview);
        try {
            this.f20369 = obtainStyledAttributes.getResourceId(R.styleable.superrecyclerview_mainLayoutId, R.layout.layout_progress_recyclerview);
            this.f20367 = obtainStyledAttributes.getBoolean(R.styleable.superrecyclerview_recyclerClipToPadding, false);
            this.f20370 = (int) obtainStyledAttributes.getDimension(R.styleable.superrecyclerview_recyclerPadding, -1.0f);
            this.f20371 = (int) obtainStyledAttributes.getDimension(R.styleable.superrecyclerview_recyclerPaddingTop, 0.0f);
            this.f20361 = (int) obtainStyledAttributes.getDimension(R.styleable.superrecyclerview_recyclerPaddingBottom, 0.0f);
            this.f20358 = (int) obtainStyledAttributes.getDimension(R.styleable.superrecyclerview_recyclerPaddingLeft, 0.0f);
            this.f20359 = (int) obtainStyledAttributes.getDimension(R.styleable.superrecyclerview_recyclerPaddingRight, 0.0f);
            this.f20380 = obtainStyledAttributes.getInt(R.styleable.superrecyclerview_scrollbarStyle, -1);
            this.f20381 = obtainStyledAttributes.getResourceId(R.styleable.superrecyclerview_layout_empty, 0);
            this.f20363 = obtainStyledAttributes.getResourceId(R.styleable.superrecyclerview_layout_moreProgress, R.layout.layout_more_progress);
            this.f20373 = obtainStyledAttributes.getResourceId(R.styleable.superrecyclerview_layout_progress, R.layout.layout_progress);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26370(View view) {
        View findViewById = view.findViewById(16908298);
        if (findViewById instanceof RecyclerView) {
            this.f20376 = (RecyclerView) findViewById;
            this.f20376.setClipToPadding(this.f20367);
            this.f20365 = new RecyclerView.OnScrollListener() {
                public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                    super.onScrollStateChanged(recyclerView, i);
                    if (SuperRecyclerView.this.f20360 != null) {
                        SuperRecyclerView.this.f20360.onScrollStateChanged(recyclerView, i);
                    }
                    if (SuperRecyclerView.this.f20372 != null) {
                        SuperRecyclerView.this.f20372.onScrollStateChanged(recyclerView, i);
                    }
                }

                public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                    super.onScrolled(recyclerView, i, i2);
                    SuperRecyclerView.this.m26354();
                    if (SuperRecyclerView.this.f20360 != null) {
                        SuperRecyclerView.this.f20360.onScrolled(recyclerView, i, i2);
                    }
                    if (SuperRecyclerView.this.f20372 != null) {
                        SuperRecyclerView.this.f20372.onScrolled(recyclerView, i, i2);
                    }
                }
            };
            this.f20376.addOnScrollListener(this.f20365);
            if (!FloatUtil.m26443((float) this.f20370, -1.0f)) {
                this.f20376.setPadding(this.f20370, this.f20370, this.f20370, this.f20370);
            } else {
                this.f20376.setPadding(this.f20358, this.f20371, this.f20359, this.f20361);
            }
            if (this.f20380 != -1) {
                this.f20376.setScrollBarStyle(this.f20380);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("SuperRecyclerView works with a RecyclerView!");
    }
}
