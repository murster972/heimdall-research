package com.malinskiy.superrecyclerview.swipe;

public interface SwipeItemManagerInterface {

    public enum Mode {
        Single,
        Multiple
    }
}
