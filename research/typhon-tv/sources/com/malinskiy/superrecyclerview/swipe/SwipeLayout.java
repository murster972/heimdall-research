package com.malinskiy.superrecyclerview.swipe;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import com.malinskiy.superrecyclerview.R;
import com.malinskiy.superrecyclerview.util.FloatUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SwipeLayout extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f20417;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public List<SwipeListener> f20418;

    /* renamed from: ʽ  reason: contains not printable characters */
    private List<SwipeDenier> f20419;

    /* renamed from: ʾ  reason: contains not printable characters */
    private ViewDragHelper.Callback f20420;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f20421;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f20422;

    /* renamed from: ˊ  reason: contains not printable characters */
    private float f20423;

    /* renamed from: ˋ  reason: contains not printable characters */
    private float f20424;

    /* renamed from: ˎ  reason: contains not printable characters */
    private GestureDetector f20425;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Map<View, ArrayList<OnRevealListener>> f20426;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Map<View, Boolean> f20427;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public DoubleClickListener f20428;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f20429;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public int f20430;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public ShowMode f20431;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public DragEdge f20432;

    /* renamed from: 龘  reason: contains not printable characters */
    private ViewDragHelper f20433;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private List<OnLayout> f20434;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f20435;

    public interface DoubleClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m26433(SwipeLayout swipeLayout, boolean z);
    }

    public enum DragEdge {
        Left,
        Right,
        Top,
        Bottom
    }

    public interface OnLayout {
        /* renamed from: 龘  reason: contains not printable characters */
        void m26434(SwipeLayout swipeLayout);
    }

    public interface OnRevealListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m26435(View view, DragEdge dragEdge, float f, int i);
    }

    public enum ShowMode {
        LayDown,
        PullOut
    }

    public enum Status {
        Middle,
        Open,
        Close
    }

    public interface SwipeDenier {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m26436(MotionEvent motionEvent);
    }

    class SwipeDetector extends GestureDetector.SimpleOnGestureListener {
        SwipeDetector() {
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            if (SwipeLayout.this.f20428 != null) {
                ViewGroup bottomView = SwipeLayout.this.getBottomView();
                ViewGroup surfaceView = SwipeLayout.this.getSurfaceView();
                SwipeLayout.this.f20428.m26433(SwipeLayout.this, (((motionEvent.getX() > ((float) bottomView.getLeft()) ? 1 : (motionEvent.getX() == ((float) bottomView.getLeft()) ? 0 : -1)) <= 0 || (motionEvent.getX() > ((float) bottomView.getRight()) ? 1 : (motionEvent.getX() == ((float) bottomView.getRight()) ? 0 : -1)) >= 0 || (motionEvent.getY() > ((float) bottomView.getTop()) ? 1 : (motionEvent.getY() == ((float) bottomView.getTop()) ? 0 : -1)) <= 0 || (motionEvent.getY() > ((float) bottomView.getBottom()) ? 1 : (motionEvent.getY() == ((float) bottomView.getBottom()) ? 0 : -1)) >= 0) ? surfaceView : bottomView) == surfaceView);
            }
            return true;
        }

        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        public void onLongPress(MotionEvent motionEvent) {
            SwipeLayout.this.performLongClick();
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            if (SwipeLayout.this.f20428 == null) {
                return true;
            }
            SwipeLayout.this.m26412(motionEvent);
            return true;
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            if (SwipeLayout.this.f20428 != null) {
                return true;
            }
            SwipeLayout.this.m26412(motionEvent);
            return true;
        }
    }

    public interface SwipeListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m26437(SwipeLayout swipeLayout);

        /* renamed from: 麤  reason: contains not printable characters */
        void m26438(SwipeLayout swipeLayout);

        /* renamed from: 齉  reason: contains not printable characters */
        void m26439(SwipeLayout swipeLayout);

        /* renamed from: 龘  reason: contains not printable characters */
        void m26440(SwipeLayout swipeLayout);

        /* renamed from: 龘  reason: contains not printable characters */
        void m26441(SwipeLayout swipeLayout, float f, float f2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m26442(SwipeLayout swipeLayout, int i, int i2);
    }

    public SwipeLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    public SwipeLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SwipeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f20430 = 0;
        this.f20418 = new ArrayList();
        this.f20419 = new ArrayList();
        this.f20426 = new HashMap();
        this.f20427 = new HashMap();
        this.f20422 = true;
        this.f20420 = new ViewDragHelper.Callback() {
            public int clampViewPositionHorizontal(View view, int i, int i2) {
                if (view == SwipeLayout.this.getSurfaceView()) {
                    switch (AnonymousClass2.f20437[SwipeLayout.this.f20432.ordinal()]) {
                        case 1:
                        case 2:
                            return SwipeLayout.this.getPaddingLeft();
                        case 3:
                            return i < SwipeLayout.this.getPaddingLeft() ? SwipeLayout.this.getPaddingLeft() : i > SwipeLayout.this.getPaddingLeft() + SwipeLayout.this.f20430 ? SwipeLayout.this.getPaddingLeft() + SwipeLayout.this.f20430 : i;
                        case 4:
                            return i > SwipeLayout.this.getPaddingLeft() ? SwipeLayout.this.getPaddingLeft() : i < SwipeLayout.this.getPaddingLeft() - SwipeLayout.this.f20430 ? SwipeLayout.this.getPaddingLeft() - SwipeLayout.this.f20430 : i;
                        default:
                            return i;
                    }
                } else if (view != SwipeLayout.this.getBottomView()) {
                    return i;
                } else {
                    switch (AnonymousClass2.f20437[SwipeLayout.this.f20432.ordinal()]) {
                        case 1:
                        case 2:
                            return SwipeLayout.this.getPaddingLeft();
                        case 3:
                            return (SwipeLayout.this.f20431 != ShowMode.PullOut || i <= SwipeLayout.this.getPaddingLeft()) ? i : SwipeLayout.this.getPaddingLeft();
                        case 4:
                            return (SwipeLayout.this.f20431 != ShowMode.PullOut || i >= SwipeLayout.this.getMeasuredWidth() - SwipeLayout.this.f20430) ? i : SwipeLayout.this.getMeasuredWidth() - SwipeLayout.this.f20430;
                        default:
                            return i;
                    }
                }
            }

            public int clampViewPositionVertical(View view, int i, int i2) {
                if (view == SwipeLayout.this.getSurfaceView()) {
                    switch (AnonymousClass2.f20437[SwipeLayout.this.f20432.ordinal()]) {
                        case 1:
                            return i < SwipeLayout.this.getPaddingTop() ? SwipeLayout.this.getPaddingTop() : i > SwipeLayout.this.getPaddingTop() + SwipeLayout.this.f20430 ? SwipeLayout.this.getPaddingTop() + SwipeLayout.this.f20430 : i;
                        case 2:
                            return i < SwipeLayout.this.getPaddingTop() - SwipeLayout.this.f20430 ? SwipeLayout.this.getPaddingTop() - SwipeLayout.this.f20430 : i > SwipeLayout.this.getPaddingTop() ? SwipeLayout.this.getPaddingTop() : i;
                        case 3:
                        case 4:
                            return SwipeLayout.this.getPaddingTop();
                        default:
                            return i;
                    }
                } else {
                    switch (AnonymousClass2.f20437[SwipeLayout.this.f20432.ordinal()]) {
                        case 1:
                            return SwipeLayout.this.f20431 == ShowMode.PullOut ? i > SwipeLayout.this.getPaddingTop() ? SwipeLayout.this.getPaddingTop() : i : SwipeLayout.this.getSurfaceView().getTop() + i2 < SwipeLayout.this.getPaddingTop() ? SwipeLayout.this.getPaddingTop() : SwipeLayout.this.getSurfaceView().getTop() + i2 > SwipeLayout.this.getPaddingTop() + SwipeLayout.this.f20430 ? SwipeLayout.this.getPaddingTop() + SwipeLayout.this.f20430 : i;
                        case 2:
                            return SwipeLayout.this.f20431 == ShowMode.PullOut ? i < SwipeLayout.this.getMeasuredHeight() - SwipeLayout.this.f20430 ? SwipeLayout.this.getMeasuredHeight() - SwipeLayout.this.f20430 : i : SwipeLayout.this.getSurfaceView().getTop() + i2 >= SwipeLayout.this.getPaddingTop() ? SwipeLayout.this.getPaddingTop() : SwipeLayout.this.getSurfaceView().getTop() + i2 <= SwipeLayout.this.getPaddingTop() - SwipeLayout.this.f20430 ? SwipeLayout.this.getPaddingTop() - SwipeLayout.this.f20430 : i;
                        case 3:
                        case 4:
                            return SwipeLayout.this.getPaddingTop();
                        default:
                            return i;
                    }
                }
            }

            public int getViewHorizontalDragRange(View view) {
                return SwipeLayout.this.f20430;
            }

            public int getViewVerticalDragRange(View view) {
                return SwipeLayout.this.f20430;
            }

            public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
                int left = SwipeLayout.this.getSurfaceView().getLeft();
                int right = SwipeLayout.this.getSurfaceView().getRight();
                int top = SwipeLayout.this.getSurfaceView().getTop();
                int bottom = SwipeLayout.this.getSurfaceView().getBottom();
                if (view == SwipeLayout.this.getSurfaceView()) {
                    if (SwipeLayout.this.f20431 == ShowMode.PullOut) {
                        if (SwipeLayout.this.f20432 == DragEdge.Left || SwipeLayout.this.f20432 == DragEdge.Right) {
                            SwipeLayout.this.getBottomView().offsetLeftAndRight(i3);
                        } else {
                            SwipeLayout.this.getBottomView().offsetTopAndBottom(i4);
                        }
                    }
                } else if (view == SwipeLayout.this.getBottomView()) {
                    if (SwipeLayout.this.f20431 == ShowMode.PullOut) {
                        SwipeLayout.this.getSurfaceView().offsetLeftAndRight(i3);
                        SwipeLayout.this.getSurfaceView().offsetTopAndBottom(i4);
                    } else {
                        Rect r8 = SwipeLayout.this.m26405(SwipeLayout.this.f20432);
                        SwipeLayout.this.getBottomView().layout(r8.left, r8.top, r8.right, r8.bottom);
                        int left2 = SwipeLayout.this.getSurfaceView().getLeft() + i3;
                        int top2 = SwipeLayout.this.getSurfaceView().getTop() + i4;
                        if (SwipeLayout.this.f20432 == DragEdge.Left && left2 < SwipeLayout.this.getPaddingLeft()) {
                            left2 = SwipeLayout.this.getPaddingLeft();
                        } else if (SwipeLayout.this.f20432 == DragEdge.Right && left2 > SwipeLayout.this.getPaddingLeft()) {
                            left2 = SwipeLayout.this.getPaddingLeft();
                        } else if (SwipeLayout.this.f20432 == DragEdge.Top && top2 < SwipeLayout.this.getPaddingTop()) {
                            top2 = SwipeLayout.this.getPaddingTop();
                        } else if (SwipeLayout.this.f20432 == DragEdge.Bottom && top2 > SwipeLayout.this.getPaddingTop()) {
                            top2 = SwipeLayout.this.getPaddingTop();
                        }
                        SwipeLayout.this.getSurfaceView().layout(left2, top2, SwipeLayout.this.getMeasuredWidth() + left2, SwipeLayout.this.getMeasuredHeight() + top2);
                    }
                }
                SwipeLayout.this.m26422(left, top, right, bottom);
                SwipeLayout.this.m26429(left, top, i3, i4);
                SwipeLayout.this.invalidate();
            }

            public void onViewReleased(View view, float f, float f2) {
                super.onViewReleased(view, f, f2);
                for (SwipeListener r0 : SwipeLayout.this.f20418) {
                    r0.m26441(SwipeLayout.this, f, f2);
                }
                if (view == SwipeLayout.this.getSurfaceView()) {
                    SwipeLayout.this.m26411(f, f2);
                } else if (view == SwipeLayout.this.getBottomView()) {
                    if (SwipeLayout.this.getShowMode() == ShowMode.PullOut) {
                        SwipeLayout.this.m26398(f, f2);
                    } else if (SwipeLayout.this.getShowMode() == ShowMode.LayDown) {
                        SwipeLayout.this.m26402(f, f2);
                    }
                }
                SwipeLayout.this.invalidate();
            }

            public boolean tryCaptureView(View view, int i) {
                return view == SwipeLayout.this.getSurfaceView() || view == SwipeLayout.this.getBottomView();
            }
        };
        this.f20421 = 0;
        this.f20435 = false;
        this.f20423 = -1.0f;
        this.f20424 = -1.0f;
        this.f20425 = new GestureDetector(getContext(), new SwipeDetector());
        this.f20433 = ViewDragHelper.create(this, this.f20420);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SwipeLayout);
        int i2 = obtainStyledAttributes.getInt(R.styleable.SwipeLayout_drag_edge, DragEdge.Right.ordinal());
        this.f20429 = obtainStyledAttributes.getDimension(R.styleable.SwipeLayout_horizontalSwipeOffset, 0.0f);
        this.f20417 = obtainStyledAttributes.getDimension(R.styleable.SwipeLayout_verticalSwipeOffset, 0.0f);
        this.f20432 = DragEdge.values()[i2];
        this.f20431 = ShowMode.values()[obtainStyledAttributes.getInt(R.styleable.SwipeLayout_show_mode, ShowMode.PullOut.ordinal())];
        obtainStyledAttributes.recycle();
    }

    private AdapterView getAdapterView() {
        for (ViewParent parent = getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof AdapterView) {
                return (AdapterView) parent;
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m26394() {
        Status openStatus = getOpenStatus();
        ViewGroup bottomView = getBottomView();
        if (openStatus == Status.Close) {
            if (bottomView.getVisibility() != 4) {
                bottomView.setVisibility(4);
            }
        } else if (bottomView.getVisibility() != 0) {
            bottomView.setVisibility(0);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m26395() {
        Adapter adapter;
        AdapterView adapterView = getAdapterView();
        boolean z = true;
        if (!(adapterView == null || (adapter = adapterView.getAdapter()) == null)) {
            int positionForView = adapterView.getPositionForView(this);
            if (adapter instanceof BaseAdapter) {
                z = ((BaseAdapter) adapter).isEnabled(positionForView);
            } else if (adapter instanceof ListAdapter) {
                z = ((ListAdapter) adapter).isEnabled(positionForView);
            }
        }
        return !z;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26398(float f, float f2) {
        if (FloatUtil.m26443(f, 0.0f) && getOpenStatus() == Status.Middle) {
            m26420();
        }
        if (this.f20432 == DragEdge.Left || this.f20432 == DragEdge.Right) {
            if (f > 0.0f) {
                if (this.f20432 == DragEdge.Left) {
                    m26425();
                } else {
                    m26420();
                }
            }
            if (f >= 0.0f) {
                return;
            }
            if (this.f20432 == DragEdge.Left) {
                m26420();
            } else {
                m26425();
            }
        } else {
            if (f2 > 0.0f) {
                if (this.f20432 == DragEdge.Top) {
                    m26425();
                } else {
                    m26420();
                }
            }
            if (f2 >= 0.0f) {
                return;
            }
            if (this.f20432 == DragEdge.Top) {
                m26420();
            } else {
                m26425();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26402(float f, float f2) {
        if (FloatUtil.m26443(f, 0.0f) && getOpenStatus() == Status.Middle) {
            m26420();
        }
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        if (f < 0.0f && this.f20432 == DragEdge.Right) {
            paddingLeft -= this.f20430;
        }
        if (f > 0.0f && this.f20432 == DragEdge.Left) {
            paddingLeft += this.f20430;
        }
        if (f2 > 0.0f && this.f20432 == DragEdge.Top) {
            paddingTop += this.f20430;
        }
        if (f2 < 0.0f && this.f20432 == DragEdge.Bottom) {
            paddingTop -= this.f20430;
        }
        this.f20433.smoothSlideViewTo(getSurfaceView(), paddingLeft, paddingTop);
        invalidate();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m26404(float f) {
        return (int) ((getContext().getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Rect m26405(DragEdge dragEdge) {
        int i;
        int i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        if (dragEdge == DragEdge.Right) {
            paddingLeft = getMeasuredWidth() - this.f20430;
        } else if (dragEdge == DragEdge.Bottom) {
            paddingTop = getMeasuredHeight() - this.f20430;
        }
        if (dragEdge == DragEdge.Left || dragEdge == DragEdge.Right) {
            i = paddingLeft + this.f20430;
            i2 = paddingTop + getMeasuredHeight();
        } else {
            i = paddingLeft + getMeasuredWidth();
            i2 = paddingTop + this.f20430;
        }
        return new Rect(paddingLeft, paddingTop, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Rect m26406(ShowMode showMode, Rect rect) {
        int i = rect.left;
        int i2 = rect.top;
        int i3 = rect.right;
        int i4 = rect.bottom;
        if (showMode == ShowMode.PullOut) {
            if (this.f20432 == DragEdge.Left) {
                i = rect.left - this.f20430;
            } else if (this.f20432 == DragEdge.Right) {
                i = rect.right;
            } else {
                i2 = this.f20432 == DragEdge.Top ? rect.top - this.f20430 : rect.bottom;
            }
            if (this.f20432 == DragEdge.Left || this.f20432 == DragEdge.Right) {
                i4 = rect.bottom;
                i3 = i + getBottomView().getMeasuredWidth();
            } else {
                i4 = i2 + getBottomView().getMeasuredHeight();
                i3 = rect.right;
            }
        } else if (showMode == ShowMode.LayDown) {
            if (this.f20432 == DragEdge.Left) {
                i3 = i + this.f20430;
            } else if (this.f20432 == DragEdge.Right) {
                i = i3 - this.f20430;
            } else if (this.f20432 == DragEdge.Top) {
                i4 = i2 + this.f20430;
            } else {
                i2 = i4 - this.f20430;
            }
        }
        return new Rect(i, i2, i3, i4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Rect m26408(boolean z) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        if (z) {
            if (this.f20432 == DragEdge.Left) {
                paddingLeft = getPaddingLeft() + this.f20430;
            } else if (this.f20432 == DragEdge.Right) {
                paddingLeft = getPaddingLeft() - this.f20430;
            } else {
                paddingTop = this.f20432 == DragEdge.Top ? getPaddingTop() + this.f20430 : getPaddingTop() - this.f20430;
            }
        }
        return new Rect(paddingLeft, paddingTop, getMeasuredWidth() + paddingLeft, getMeasuredHeight() + paddingTop);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private View m26409(ViewGroup viewGroup, MotionEvent motionEvent) {
        if (viewGroup == null) {
            return null;
        }
        if (viewGroup.onTouchEvent(motionEvent)) {
            return viewGroup;
        }
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt instanceof ViewGroup) {
                View r2 = m26409((ViewGroup) childAt, motionEvent);
                if (r2 != null) {
                    return r2;
                }
            } else if (m26419(viewGroup.getChildAt(childCount), motionEvent)) {
                return viewGroup.getChildAt(childCount);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26411(float f, float f2) {
        if (FloatUtil.m26443(f, 0.0f) && getOpenStatus() == Status.Middle) {
            m26420();
        }
        if (this.f20432 == DragEdge.Left || this.f20432 == DragEdge.Right) {
            if (f > 0.0f) {
                if (this.f20432 == DragEdge.Left) {
                    m26425();
                } else {
                    m26420();
                }
            }
            if (f >= 0.0f) {
                return;
            }
            if (this.f20432 == DragEdge.Left) {
                m26420();
            } else {
                m26425();
            }
        } else {
            if (f2 > 0.0f) {
                if (this.f20432 == DragEdge.Top) {
                    m26425();
                } else {
                    m26420();
                }
            }
            if (f2 >= 0.0f) {
                return;
            }
            if (this.f20432 == DragEdge.Top) {
                m26420();
            } else {
                m26425();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26412(MotionEvent motionEvent) {
        for (ViewParent parent = getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof AdapterView) {
                AdapterView adapterView = (AdapterView) parent;
                int positionForView = adapterView.getPositionForView(this);
                if (positionForView != -1 && adapterView.performItemClick(adapterView.getChildAt(positionForView - adapterView.getFirstVisiblePosition()), positionForView, adapterView.getAdapter().getItemId(positionForView))) {
                    return;
                }
            } else if ((parent instanceof View) && ((View) parent).performClick()) {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26413(MotionEvent motionEvent, ViewParent viewParent) {
        motionEvent.setAction(0);
        this.f20433.processTouchEvent(motionEvent);
        viewParent.requestDisallowInterceptTouchEvent(true);
        this.f20423 = motionEvent.getRawX();
        this.f20424 = motionEvent.getRawY();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26414(MotionEvent motionEvent, ViewParent viewParent, ViewGroup viewGroup) {
        this.f20433.processTouchEvent(motionEvent);
        viewParent.requestDisallowInterceptTouchEvent(true);
        this.f20423 = motionEvent.getRawX();
        this.f20424 = motionEvent.getRawY();
        if (viewGroup != null) {
            viewGroup.setPressed(true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26415(ViewGroup viewGroup) {
        this.f20423 = -1.0f;
        this.f20424 = -1.0f;
        if (viewGroup != null) {
            viewGroup.setPressed(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26418(MotionEvent motionEvent, Status status, ViewParent viewParent, ViewGroup viewGroup) {
        float rawX = motionEvent.getRawX() - this.f20423;
        float rawY = motionEvent.getRawY() - this.f20424;
        float degrees = (float) Math.toDegrees(Math.atan((double) Math.abs(rawY / rawX)));
        boolean z = false;
        if (this.f20432 == DragEdge.Right) {
            boolean z2 = ((status == Status.Open && (rawX > 0.0f ? 1 : (rawX == 0.0f ? 0 : -1)) > 0) || (status == Status.Close && (rawX > 0.0f ? 1 : (rawX == 0.0f ? 0 : -1)) < 0)) || status == Status.Middle;
            if (degrees > 30.0f || !z2) {
                z = true;
            }
        }
        if (this.f20432 == DragEdge.Left) {
            boolean z3 = ((status == Status.Open && (rawX > 0.0f ? 1 : (rawX == 0.0f ? 0 : -1)) < 0) || (status == Status.Close && (rawX > 0.0f ? 1 : (rawX == 0.0f ? 0 : -1)) > 0)) || status == Status.Middle;
            if (degrees > 30.0f || !z3) {
                z = true;
            }
        }
        if (this.f20432 == DragEdge.Top) {
            boolean z4 = ((status == Status.Open && (rawY > 0.0f ? 1 : (rawY == 0.0f ? 0 : -1)) < 0) || (status == Status.Close && (rawY > 0.0f ? 1 : (rawY == 0.0f ? 0 : -1)) > 0)) || status == Status.Middle;
            if (degrees < 60.0f || !z4) {
                z = true;
            }
        }
        if (this.f20432 == DragEdge.Bottom) {
            boolean z5 = ((status == Status.Open && (rawY > 0.0f ? 1 : (rawY == 0.0f ? 0 : -1)) > 0) || (status == Status.Close && (rawY > 0.0f ? 1 : (rawY == 0.0f ? 0 : -1)) < 0)) || status == Status.Middle;
            if (degrees < 60.0f || !z5) {
                z = true;
            }
        }
        if (z) {
            viewParent.requestDisallowInterceptTouchEvent(false);
        } else {
            if (viewGroup != null) {
                viewGroup.setPressed(false);
            }
            viewParent.requestDisallowInterceptTouchEvent(true);
            this.f20433.processTouchEvent(motionEvent);
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26419(View view, MotionEvent motionEvent) {
        if (view == null) {
            return false;
        }
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        if (motionEvent.getRawX() <= ((float) i) || motionEvent.getRawX() >= ((float) (view.getWidth() + i)) || motionEvent.getRawY() <= ((float) i2) || motionEvent.getRawY() >= ((float) (view.getHeight() + i2))) {
            return false;
        }
        return view.onTouchEvent(motionEvent);
    }

    public void computeScroll() {
        super.computeScroll();
        if (this.f20433.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public ViewGroup getBottomView() {
        return (ViewGroup) getChildAt(0);
    }

    public int getDragDistance() {
        return this.f20430;
    }

    public DragEdge getDragEdge() {
        return this.f20432;
    }

    public Status getOpenStatus() {
        int left = getSurfaceView().getLeft();
        int top = getSurfaceView().getTop();
        return (left == getPaddingLeft() && top == getPaddingTop()) ? Status.Close : (left == getPaddingLeft() - this.f20430 || left == getPaddingLeft() + this.f20430 || top == getPaddingTop() - this.f20430 || top == getPaddingTop() + this.f20430) ? Status.Open : Status.Middle;
    }

    public ShowMode getShowMode() {
        return this.f20431;
    }

    public ViewGroup getSurfaceView() {
        return (ViewGroup) getChildAt(1);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z = true;
        if (!isEnabled() || m26395()) {
            return true;
        }
        if (m26426()) {
            return false;
        }
        for (SwipeDenier next : this.f20419) {
            if (next != null && next.m26436(motionEvent)) {
                return false;
            }
        }
        switch (MotionEventCompat.getActionMasked(motionEvent)) {
            case 0:
                Status openStatus = getOpenStatus();
                if (openStatus != Status.Close) {
                    if (openStatus == Status.Open) {
                        if (m26409(getBottomView(), motionEvent) == null) {
                            z = false;
                        }
                        this.f20435 = z;
                        break;
                    }
                } else {
                    if (m26409(getSurfaceView(), motionEvent) == null) {
                        z = false;
                    }
                    this.f20435 = z;
                    break;
                }
                break;
            case 1:
            case 3:
                this.f20435 = false;
                break;
        }
        if (!this.f20435) {
            return this.f20433.shouldInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (getChildCount() != 2) {
            throw new IllegalStateException("You need 2  views in SwipeLayout");
        } else if (!(getChildAt(0) instanceof ViewGroup) || !(getChildAt(1) instanceof ViewGroup)) {
            throw new IllegalArgumentException("The 2 children in SwipeLayout must be an instance of ViewGroup");
        } else {
            if (this.f20431 == ShowMode.PullOut) {
                m26428();
            } else if (this.f20431 == ShowMode.LayDown) {
                m26421();
            }
            m26394();
            if (this.f20434 != null) {
                for (int i5 = 0; i5 < this.f20434.size(); i5++) {
                    this.f20434.get(i5).m26434(this);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f20432 == DragEdge.Left || this.f20432 == DragEdge.Right) {
            this.f20430 = getBottomView().getMeasuredWidth() - m26404(this.f20429);
        } else {
            this.f20430 = getBottomView().getMeasuredHeight() - m26404(this.f20417);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (m26395() || !isEnabled()) {
            return true;
        }
        if (m26426()) {
            return super.onTouchEvent(motionEvent);
        }
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent);
        ViewParent parent = getParent();
        this.f20425.onTouchEvent(motionEvent);
        Status openStatus = getOpenStatus();
        ViewGroup viewGroup = null;
        if (openStatus == Status.Close) {
            viewGroup = getSurfaceView();
        } else if (openStatus == Status.Open) {
            viewGroup = getBottomView();
        }
        switch (actionMasked) {
            case 0:
                m26414(motionEvent, parent, viewGroup);
                return true;
            case 1:
            case 3:
                m26415(viewGroup);
                break;
            case 2:
                if (!FloatUtil.m26443(this.f20423, -1.0f) && !FloatUtil.m26443(this.f20424, -1.0f)) {
                    return !m26418(motionEvent, openStatus, parent, viewGroup);
                }
                m26413(motionEvent, parent);
                return true;
        }
        parent.requestDisallowInterceptTouchEvent(true);
        this.f20433.processTouchEvent(motionEvent);
        return true;
    }

    public void setDragDistance(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Drag distance can not be < 0");
        }
        this.f20430 = m26404((float) i);
        requestLayout();
    }

    public void setDragEdge(DragEdge dragEdge) {
        this.f20432 = dragEdge;
        requestLayout();
    }

    public void setOnDoubleClickListener(DoubleClickListener doubleClickListener) {
        this.f20428 = doubleClickListener;
    }

    public void setShowMode(ShowMode showMode) {
        this.f20431 = showMode;
        requestLayout();
    }

    public void setSwipeEnabled(boolean z) {
        this.f20422 = z;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m26420() {
        m26423(true, true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26421() {
        Rect r0 = m26408(false);
        getSurfaceView().layout(r0.left, r0.top, r0.right, r0.bottom);
        Rect r02 = m26406(ShowMode.LayDown, r0);
        getBottomView().layout(r02.left, r02.top, r02.right, r02.bottom);
        bringChildToFront(getSurfaceView());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26422(int i, int i2, int i3, int i4) {
        if (!this.f20426.isEmpty()) {
            for (Map.Entry next : this.f20426.entrySet()) {
                View view = (View) next.getKey();
                Rect r2 = m26427(view);
                if (m26424(view, r2, this.f20432, i, i2, i3, i4)) {
                    this.f20427.put(view, false);
                    int i5 = 0;
                    float f = 0.0f;
                    if (getShowMode() != ShowMode.LayDown) {
                        if (getShowMode() == ShowMode.PullOut) {
                            switch (this.f20432) {
                                case Top:
                                    i5 = r2.bottom - getPaddingTop();
                                    f = ((float) i5) / ((float) view.getHeight());
                                    break;
                                case Bottom:
                                    i5 = r2.top - getHeight();
                                    f = ((float) i5) / ((float) view.getHeight());
                                    break;
                                case Left:
                                    i5 = r2.right - getPaddingLeft();
                                    f = ((float) i5) / ((float) view.getWidth());
                                    break;
                                case Right:
                                    i5 = r2.left - getWidth();
                                    f = ((float) i5) / ((float) view.getWidth());
                                    break;
                            }
                        }
                    } else {
                        switch (this.f20432) {
                            case Top:
                                i5 = r2.top - i2;
                                f = ((float) i5) / ((float) view.getHeight());
                                break;
                            case Bottom:
                                i5 = r2.bottom - i4;
                                f = ((float) i5) / ((float) view.getHeight());
                                break;
                            case Left:
                                i5 = r2.left - i;
                                f = ((float) i5) / ((float) view.getWidth());
                                break;
                            case Right:
                                i5 = r2.right - i3;
                                f = ((float) i5) / ((float) view.getWidth());
                                break;
                        }
                    }
                    Iterator it2 = ((ArrayList) next.getValue()).iterator();
                    while (it2.hasNext()) {
                        ((OnRevealListener) it2.next()).m26435(view, this.f20432, Math.abs(f), i5);
                        if (FloatUtil.m26443(Math.abs(f), 1.0f)) {
                            this.f20427.put(view, true);
                        }
                    }
                }
                if (m26432(view, r2, this.f20432, i, i2, i3, i4)) {
                    this.f20427.put(view, true);
                    Iterator it3 = ((ArrayList) next.getValue()).iterator();
                    while (it3.hasNext()) {
                        OnRevealListener onRevealListener = (OnRevealListener) it3.next();
                        if (this.f20432 == DragEdge.Left || this.f20432 == DragEdge.Right) {
                            onRevealListener.m26435(view, this.f20432, 1.0f, view.getWidth());
                        } else {
                            onRevealListener.m26435(view, this.f20432, 1.0f, view.getHeight());
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26423(boolean z, boolean z2) {
        ViewGroup surfaceView = getSurfaceView();
        if (z) {
            this.f20433.smoothSlideViewTo(getSurfaceView(), getPaddingLeft(), getPaddingTop());
        } else {
            Rect r2 = m26408(false);
            int left = r2.left - surfaceView.getLeft();
            int top = r2.top - surfaceView.getTop();
            surfaceView.layout(r2.left, r2.top, r2.right, r2.bottom);
            if (z2) {
                m26422(r2.left, r2.top, r2.right, r2.bottom);
                m26429(r2.left, r2.top, left, top);
            } else {
                m26394();
            }
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x001c A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m26424(android.view.View r8, android.graphics.Rect r9, com.malinskiy.superrecyclerview.swipe.SwipeLayout.DragEdge r10, int r11, int r12, int r13, int r14) {
        /*
            r7 = this;
            r4 = 1
            int r1 = r9.left
            int r2 = r9.right
            int r3 = r9.top
            int r0 = r9.bottom
            com.malinskiy.superrecyclerview.swipe.SwipeLayout$ShowMode r5 = r7.getShowMode()
            com.malinskiy.superrecyclerview.swipe.SwipeLayout$ShowMode r6 = com.malinskiy.superrecyclerview.swipe.SwipeLayout.ShowMode.LayDown
            if (r5 != r6) goto L_0x0032
            int[] r5 = com.malinskiy.superrecyclerview.swipe.SwipeLayout.AnonymousClass2.f20437
            int r6 = r10.ordinal()
            r5 = r5[r6]
            switch(r5) {
                case 1: goto L_0x0028;
                case 2: goto L_0x002d;
                case 3: goto L_0x0023;
                case 4: goto L_0x001e;
                default: goto L_0x001c;
            }
        L_0x001c:
            r4 = 0
        L_0x001d:
            return r4
        L_0x001e:
            if (r13 <= r1) goto L_0x001c
            if (r13 > r2) goto L_0x001c
            goto L_0x001d
        L_0x0023:
            if (r11 >= r2) goto L_0x001c
            if (r11 < r1) goto L_0x001c
            goto L_0x001d
        L_0x0028:
            if (r12 < r3) goto L_0x001c
            if (r12 >= r0) goto L_0x001c
            goto L_0x001d
        L_0x002d:
            if (r14 <= r3) goto L_0x001c
            if (r14 > r0) goto L_0x001c
            goto L_0x001d
        L_0x0032:
            com.malinskiy.superrecyclerview.swipe.SwipeLayout$ShowMode r5 = r7.getShowMode()
            com.malinskiy.superrecyclerview.swipe.SwipeLayout$ShowMode r6 = com.malinskiy.superrecyclerview.swipe.SwipeLayout.ShowMode.PullOut
            if (r5 != r6) goto L_0x001c
            int[] r5 = com.malinskiy.superrecyclerview.swipe.SwipeLayout.AnonymousClass2.f20437
            int r6 = r10.ordinal()
            r5 = r5[r6]
            switch(r5) {
                case 1: goto L_0x0046;
                case 2: goto L_0x006d;
                case 3: goto L_0x0060;
                case 4: goto L_0x0053;
                default: goto L_0x0045;
            }
        L_0x0045:
            goto L_0x001c
        L_0x0046:
            int r5 = r7.getPaddingTop()
            if (r3 >= r5) goto L_0x001c
            int r5 = r7.getPaddingTop()
            if (r0 < r5) goto L_0x001c
            goto L_0x001d
        L_0x0053:
            int r5 = r7.getWidth()
            if (r1 > r5) goto L_0x001c
            int r5 = r7.getWidth()
            if (r2 <= r5) goto L_0x001c
            goto L_0x001d
        L_0x0060:
            int r5 = r7.getPaddingLeft()
            if (r2 < r5) goto L_0x001c
            int r5 = r7.getPaddingLeft()
            if (r1 >= r5) goto L_0x001c
            goto L_0x001d
        L_0x006d:
            int r5 = r7.getHeight()
            if (r3 >= r5) goto L_0x001c
            int r5 = r7.getPaddingTop()
            if (r3 < r5) goto L_0x001c
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.malinskiy.superrecyclerview.swipe.SwipeLayout.m26424(android.view.View, android.graphics.Rect, com.malinskiy.superrecyclerview.swipe.SwipeLayout$DragEdge, int, int, int, int):boolean");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26425() {
        m26431(true, true);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m26426() {
        return !this.f20422;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Rect m26427(View view) {
        View view2 = view;
        Rect rect = new Rect(view2.getLeft(), view2.getTop(), 0, 0);
        while (view2.getParent() != null && view2 != getRootView() && (view2 = (View) view2.getParent()) != this) {
            rect.left += view2.getLeft();
            rect.top += view2.getTop();
        }
        rect.right = rect.left + view.getMeasuredWidth();
        rect.bottom = rect.top + view.getMeasuredHeight();
        return rect;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26428() {
        Rect r0 = m26408(false);
        getSurfaceView().layout(r0.left, r0.top, r0.right, r0.bottom);
        Rect r02 = m26406(ShowMode.PullOut, r0);
        getBottomView().layout(r02.left, r02.top, r02.right, r02.bottom);
        bringChildToFront(getSurfaceView());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26429(int i, int i2, int i3, int i4) {
        DragEdge dragEdge = getDragEdge();
        boolean z = true;
        if (dragEdge == DragEdge.Left) {
            if (i3 < 0) {
                z = false;
            }
        } else if (dragEdge == DragEdge.Right) {
            if (i3 > 0) {
                z = false;
            }
        } else if (dragEdge == DragEdge.Top) {
            if (i4 < 0) {
                z = false;
            }
        } else if (dragEdge == DragEdge.Bottom && i4 > 0) {
            z = false;
        }
        m26430(i, i2, z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26430(int i, int i2, boolean z) {
        m26394();
        Status openStatus = getOpenStatus();
        if (!this.f20418.isEmpty()) {
            this.f20421++;
            for (SwipeListener next : this.f20418) {
                if (this.f20421 == 1) {
                    if (z) {
                        next.m26440(this);
                    } else {
                        next.m26439(this);
                    }
                }
                next.m26442(this, i - getPaddingLeft(), i2 - getPaddingTop());
            }
            if (openStatus == Status.Close) {
                for (SwipeListener r0 : this.f20418) {
                    r0.m26438(this);
                }
                this.f20421 = 0;
            }
            if (openStatus == Status.Open) {
                getBottomView().setEnabled(true);
                for (SwipeListener r02 : this.f20418) {
                    r02.m26437(this);
                }
                this.f20421 = 0;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26431(boolean z, boolean z2) {
        ViewGroup surfaceView = getSurfaceView();
        ViewGroup bottomView = getBottomView();
        Rect r4 = m26408(true);
        if (z) {
            this.f20433.smoothSlideViewTo(getSurfaceView(), r4.left, r4.top);
        } else {
            int left = r4.left - surfaceView.getLeft();
            int top = r4.top - surfaceView.getTop();
            surfaceView.layout(r4.left, r4.top, r4.right, r4.bottom);
            if (getShowMode() == ShowMode.PullOut) {
                Rect r0 = m26406(ShowMode.PullOut, r4);
                bottomView.layout(r0.left, r0.top, r0.right, r0.bottom);
            }
            if (z2) {
                m26422(r4.left, r4.top, r4.right, r4.bottom);
                m26429(r4.left, r4.top, left, top);
            } else {
                m26394();
            }
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26432(View view, Rect rect, DragEdge dragEdge, int i, int i2, int i3, int i4) {
        if (this.f20427.get(view).booleanValue()) {
            return false;
        }
        int i5 = rect.left;
        int i6 = rect.right;
        int i7 = rect.top;
        int i8 = rect.bottom;
        if (getShowMode() == ShowMode.LayDown) {
            return (dragEdge == DragEdge.Right && i3 <= i5) || (dragEdge == DragEdge.Left && i >= i6) || ((dragEdge == DragEdge.Top && i2 >= i8) || (dragEdge == DragEdge.Bottom && i4 <= i7));
        }
        if (getShowMode() == ShowMode.PullOut) {
            return (dragEdge == DragEdge.Right && i6 <= getWidth()) || (dragEdge == DragEdge.Left && i5 >= getPaddingLeft()) || ((dragEdge == DragEdge.Top && i7 >= getPaddingTop()) || (dragEdge == DragEdge.Bottom && i8 <= getHeight()));
        }
        return false;
    }
}
