package com.malinskiy.superrecyclerview.swipe;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.SystemClock;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SwipeDismissRecyclerViewTouchListener implements View.OnTouchListener {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public DismissCallbacks f20388;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f20389 = 1;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public List<PendingDismissData> f20390 = new ArrayList();

    /* renamed from: ʾ  reason: contains not printable characters */
    private VelocityTracker f20391;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f20392;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f20393;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public int f20394 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f20395;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f20396;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public RecyclerView f20397;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f20398;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f20399;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f20400;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f20401;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private View f20402;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f20403;

    public interface DismissCallbacks {
        /* renamed from: 龘  reason: contains not printable characters */
        void m26391(RecyclerView recyclerView, int[] iArr);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m26392(int i);
    }

    class PendingDismissData implements Comparable<PendingDismissData> {

        /* renamed from: 靐  reason: contains not printable characters */
        public View f20413;

        /* renamed from: 龘  reason: contains not printable characters */
        public int f20415;

        public PendingDismissData(int i, View view) {
            this.f20415 = i;
            this.f20413 = view;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compareTo(PendingDismissData pendingDismissData) {
            return pendingDismissData.f20415 - this.f20415;
        }
    }

    public SwipeDismissRecyclerViewTouchListener(RecyclerView recyclerView, DismissCallbacks dismissCallbacks) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(recyclerView.getContext());
        this.f20401 = viewConfiguration.getScaledTouchSlop();
        this.f20398 = viewConfiguration.getScaledMinimumFlingVelocity() * 16;
        this.f20400 = viewConfiguration.getScaledMaximumFlingVelocity();
        this.f20399 = (long) recyclerView.getContext().getResources().getInteger(17694720);
        this.f20397 = recyclerView;
        this.f20388 = dismissCallbacks;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26376() {
        if (this.f20402 != null && this.f20393) {
            ViewPropertyAnimator.m14660(this.f20402).m14662(0.0f).m14661(1.0f).m14663(this.f20399).m14664((Animator.AnimatorListener) null);
        }
        this.f20391.recycle();
        this.f20391 = null;
        this.f20395 = 0.0f;
        this.f20396 = 0.0f;
        this.f20402 = null;
        this.f20392 = -1;
        this.f20393 = false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26377(MotionEvent motionEvent) {
        float rawX = motionEvent.getRawX() - this.f20395;
        this.f20391.addMovement(motionEvent);
        this.f20391.computeCurrentVelocity(1000);
        float xVelocity = this.f20391.getXVelocity();
        float abs = Math.abs(xVelocity);
        float abs2 = Math.abs(this.f20391.getYVelocity());
        boolean z = false;
        boolean z2 = false;
        if (Math.abs(rawX) > ((float) (this.f20389 / 2)) && this.f20393) {
            z = true;
            z2 = rawX > 0.0f;
        } else if (((float) this.f20398) <= abs && abs <= ((float) this.f20400) && abs2 < abs && this.f20393) {
            z = ((xVelocity > 0.0f ? 1 : (xVelocity == 0.0f ? 0 : -1)) < 0) == ((rawX > 0.0f ? 1 : (rawX == 0.0f ? 0 : -1)) < 0);
            z2 = this.f20391.getXVelocity() > 0.0f;
        }
        if (!z || this.f20392 == -1) {
            ViewPropertyAnimator.m14660(this.f20402).m14662(0.0f).m14661(1.0f).m14663(this.f20399).m14664((Animator.AnimatorListener) null);
        } else {
            final View view = this.f20402;
            final int i = this.f20392;
            this.f20394++;
            ViewPropertyAnimator.m14660(this.f20402).m14662(z2 ? (float) this.f20389 : (float) (-this.f20389)).m14661(0.0f).m14663(this.f20399).m14664((Animator.AnimatorListener) new AnimatorListenerAdapter() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m26388(Animator animator) {
                    super.m14560(animator);
                    SwipeDismissRecyclerViewTouchListener.this.m26384(view, i);
                }
            });
        }
        this.f20391.recycle();
        this.f20391 = null;
        this.f20395 = 0.0f;
        this.f20396 = 0.0f;
        this.f20402 = null;
        this.f20392 = -1;
        this.f20393 = false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26380(MotionEvent motionEvent) {
        Rect rect = new Rect();
        int childCount = this.f20397.getChildCount();
        int[] iArr = new int[2];
        this.f20397.getLocationOnScreen(iArr);
        int rawX = ((int) motionEvent.getRawX()) - iArr[0];
        int rawY = ((int) motionEvent.getRawY()) - iArr[1];
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            View childAt = this.f20397.getChildAt(i);
            childAt.getHitRect(rect);
            if (rect.contains(rawX, rawY)) {
                this.f20402 = childAt;
                break;
            }
            i++;
        }
        if (this.f20402 != null) {
            this.f20395 = motionEvent.getRawX();
            this.f20396 = motionEvent.getRawY();
            this.f20392 = this.f20397.getChildAdapterPosition(this.f20402);
            if (this.f20388.m26392(this.f20392)) {
                this.f20391 = VelocityTracker.obtain();
                this.f20391.addMovement(motionEvent);
                return;
            }
            this.f20402 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ int m26381(SwipeDismissRecyclerViewTouchListener swipeDismissRecyclerViewTouchListener) {
        int i = swipeDismissRecyclerViewTouchListener.f20394 - 1;
        swipeDismissRecyclerViewTouchListener.f20394 = i;
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26383(MotionEvent motionEvent) {
        this.f20391.addMovement(motionEvent);
        float rawX = motionEvent.getRawX() - this.f20395;
        float rawY = motionEvent.getRawY() - this.f20396;
        if (Math.abs(rawX) > ((float) this.f20401) && Math.abs(rawY) < Math.abs(rawX) / 2.0f) {
            this.f20393 = true;
            int i = rawX > 0.0f ? this.f20401 : -this.f20401;
            this.f20397.requestDisallowInterceptTouchEvent(true);
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            obtain.setAction((MotionEventCompat.getActionIndex(motionEvent) << 8) | 3);
            this.f20397.onTouchEvent(obtain);
            obtain.recycle();
            if (this.f20393) {
                ViewHelper.m14656(this.f20402, rawX - ((float) i));
                ViewHelper.m14657(this.f20402, Math.max(0.0f, Math.min(1.0f, 1.0f - ((Math.abs(rawX) * 2.0f) / ((float) this.f20389)))));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26384(final View view, int i) {
        final ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        final int height = view.getHeight();
        ValueAnimator r0 = ValueAnimator.m14626(height, 1).m14644(this.f20399);
        r0.m14552(new AnimatorListenerAdapter() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m26389(Animator animator) {
                SwipeDismissRecyclerViewTouchListener.m26381(SwipeDismissRecyclerViewTouchListener.this);
                if (SwipeDismissRecyclerViewTouchListener.this.f20394 == 0) {
                    Collections.sort(SwipeDismissRecyclerViewTouchListener.this.f20390);
                    int[] iArr = new int[SwipeDismissRecyclerViewTouchListener.this.f20390.size()];
                    for (int size = SwipeDismissRecyclerViewTouchListener.this.f20390.size() - 1; size >= 0; size--) {
                        iArr[size] = ((PendingDismissData) SwipeDismissRecyclerViewTouchListener.this.f20390.get(size)).f20415;
                    }
                    SwipeDismissRecyclerViewTouchListener.this.f20388.m26391(SwipeDismissRecyclerViewTouchListener.this.f20397, iArr);
                    int unused = SwipeDismissRecyclerViewTouchListener.this.f20392 = -1;
                    for (PendingDismissData pendingDismissData : SwipeDismissRecyclerViewTouchListener.this.f20390) {
                        ViewHelper.m14657(pendingDismissData.f20413, 1.0f);
                        ViewHelper.m14656(pendingDismissData.f20413, 0.0f);
                        ViewGroup.LayoutParams layoutParams = pendingDismissData.f20413.getLayoutParams();
                        layoutParams.height = height;
                        pendingDismissData.f20413.setLayoutParams(layoutParams);
                    }
                    long uptimeMillis = SystemClock.uptimeMillis();
                    SwipeDismissRecyclerViewTouchListener.this.f20397.dispatchTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
                    SwipeDismissRecyclerViewTouchListener.this.f20390.clear();
                }
            }
        });
        r0.m14648((ValueAnimator.AnimatorUpdateListener) new ValueAnimator.AnimatorUpdateListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m26390(ValueAnimator valueAnimator) {
                layoutParams.height = ((Integer) valueAnimator.m14632()).intValue();
                view.setLayoutParams(layoutParams);
            }
        });
        this.f20390.add(new PendingDismissData(i, view));
        r0.m14645();
    }

    @SuppressLint({"AndroidLintClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f20389 < 2) {
            this.f20389 = this.f20397.getWidth();
        }
        switch (MotionEventCompat.getActionMasked(motionEvent)) {
            case 0:
                if (this.f20403) {
                    return false;
                }
                m26380(motionEvent);
                return false;
            case 1:
                if (this.f20391 == null) {
                    return false;
                }
                m26377(motionEvent);
                return false;
            case 2:
                if (this.f20391 == null || this.f20403) {
                    return false;
                }
                m26383(motionEvent);
                return this.f20393;
            case 3:
                if (this.f20391 == null) {
                    return false;
                }
                m26376();
                return false;
            default:
                return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RecyclerView.OnScrollListener m26386() {
        return new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                boolean z = true;
                SwipeDismissRecyclerViewTouchListener swipeDismissRecyclerViewTouchListener = SwipeDismissRecyclerViewTouchListener.this;
                if (i == 1) {
                    z = false;
                }
                swipeDismissRecyclerViewTouchListener.m26387(z);
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26387(boolean z) {
        this.f20403 = !z;
    }
}
