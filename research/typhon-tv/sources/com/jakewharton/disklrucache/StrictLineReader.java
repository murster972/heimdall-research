package com.jakewharton.disklrucache;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

class StrictLineReader implements Closeable {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f20347;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Charset f20348;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f20349;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f20350;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InputStream f20351;

    public StrictLineReader(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw new NullPointerException();
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (!charset.equals(Util.f20354)) {
            throw new IllegalArgumentException("Unsupported encoding");
        } else {
            this.f20351 = inputStream;
            this.f20348 = charset;
            this.f20350 = new byte[i];
        }
    }

    public StrictLineReader(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26346() throws IOException {
        int read = this.f20351.read(this.f20350, 0, this.f20350.length);
        if (read == -1) {
            throw new EOFException();
        }
        this.f20349 = 0;
        this.f20347 = read;
    }

    public void close() throws IOException {
        synchronized (this.f20351) {
            if (this.f20350 != null) {
                this.f20350 = null;
                this.f20351.close();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m26348() throws IOException {
        int i;
        String byteArrayOutputStream;
        synchronized (this.f20351) {
            if (this.f20350 == null) {
                throw new IOException("LineReader is closed");
            }
            if (this.f20349 >= this.f20347) {
                m26346();
            }
            int i2 = this.f20349;
            while (true) {
                if (i2 == this.f20347) {
                    AnonymousClass1 r2 = new ByteArrayOutputStream((this.f20347 - this.f20349) + 80) {
                        public String toString() {
                            try {
                                return new String(this.buf, 0, (this.count <= 0 || this.buf[this.count + -1] != 13) ? this.count : this.count - 1, StrictLineReader.this.f20348.name());
                            } catch (UnsupportedEncodingException e) {
                                throw new AssertionError(e);
                            }
                        }
                    };
                    loop1:
                    while (true) {
                        r2.write(this.f20350, this.f20349, this.f20347 - this.f20349);
                        this.f20347 = -1;
                        m26346();
                        i = this.f20349;
                        while (true) {
                            if (i != this.f20347) {
                                if (this.f20350[i] == 10) {
                                    break loop1;
                                }
                                i++;
                            }
                        }
                    }
                    if (i != this.f20349) {
                        r2.write(this.f20350, this.f20349, i - this.f20349);
                    }
                    this.f20349 = i + 1;
                    byteArrayOutputStream = r2.toString();
                } else if (this.f20350[i2] == 10) {
                    byteArrayOutputStream = new String(this.f20350, this.f20349, ((i2 == this.f20349 || this.f20350[i2 + -1] != 13) ? i2 : i2 - 1) - this.f20349, this.f20348.name());
                    this.f20349 = i2 + 1;
                } else {
                    i2++;
                }
            }
        }
        return byteArrayOutputStream;
    }
}
