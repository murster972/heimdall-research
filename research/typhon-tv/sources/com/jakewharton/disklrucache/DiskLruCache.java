package com.jakewharton.disklrucache;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;

public final class DiskLruCache implements Closeable {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Pattern f20313 = Pattern.compile("[a-z0-9_-]{1,64}");
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final OutputStream f20314 = new OutputStream() {
        public void write(int i) throws IOException {
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final File f20315;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f20316;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f20317;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f20318;

    /* renamed from: ʿ  reason: contains not printable characters */
    private long f20319 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final LinkedHashMap<String, Entry> f20320 = new LinkedHashMap<>(0, 0.75f, true);
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int f20321;

    /* renamed from: ٴ  reason: contains not printable characters */
    private long f20322 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Writer f20323;

    /* renamed from: 连任  reason: contains not printable characters */
    private final File f20324;

    /* renamed from: 靐  reason: contains not printable characters */
    final ThreadPoolExecutor f20325 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());

    /* renamed from: 麤  reason: contains not printable characters */
    private final File f20326;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final File f20327;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Callable<Void> f20328 = new Callable<Void>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Void call() throws Exception {
            synchronized (DiskLruCache.this) {
                if (DiskLruCache.this.f20323 != null) {
                    DiskLruCache.this.m26297();
                    if (DiskLruCache.this.m26295()) {
                        DiskLruCache.this.m26294();
                        int unused = DiskLruCache.this.f20318 = 0;
                    }
                }
            }
            return null;
        }
    };

    public final class Editor {

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f20330;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final Entry f20331;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean f20332;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final boolean[] f20333;

        private class FaultHidingOutputStream extends FilterOutputStream {
            private FaultHidingOutputStream(OutputStream outputStream) {
                super(outputStream);
            }

            public void close() {
                try {
                    this.out.close();
                } catch (IOException e) {
                    boolean unused = Editor.this.f20332 = true;
                }
            }

            public void flush() {
                try {
                    this.out.flush();
                } catch (IOException e) {
                    boolean unused = Editor.this.f20332 = true;
                }
            }

            public void write(int i) {
                try {
                    this.out.write(i);
                } catch (IOException e) {
                    boolean unused = Editor.this.f20332 = true;
                }
            }

            public void write(byte[] bArr, int i, int i2) {
                try {
                    this.out.write(bArr, i, i2);
                } catch (IOException e) {
                    boolean unused = Editor.this.f20332 = true;
                }
            }
        }

        private Editor(Entry entry) {
            this.f20331 = entry;
            this.f20333 = entry.f20339 ? null : new boolean[DiskLruCache.this.f20321];
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m26326() throws IOException {
            DiskLruCache.this.m26313(this, false);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputStream m26327(int i) throws IOException {
            OutputStream r4;
            FileOutputStream fileOutputStream;
            synchronized (DiskLruCache.this) {
                if (this.f20331.f20337 != this) {
                    throw new IllegalStateException();
                }
                if (!this.f20331.f20339) {
                    this.f20333[i] = true;
                }
                File r0 = this.f20331.m26341(i);
                try {
                    fileOutputStream = new FileOutputStream(r0);
                } catch (FileNotFoundException e) {
                    DiskLruCache.this.f20327.mkdirs();
                    try {
                        fileOutputStream = new FileOutputStream(r0);
                    } catch (FileNotFoundException e2) {
                        r4 = DiskLruCache.f20314;
                    }
                }
                r4 = new FaultHidingOutputStream(fileOutputStream);
            }
            return r4;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m26328() throws IOException {
            if (this.f20332) {
                DiskLruCache.this.m26313(this, false);
                DiskLruCache.this.m26319(this.f20331.f20338);
            } else {
                DiskLruCache.this.m26313(this, true);
            }
            this.f20330 = true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m26329(int i, String str) throws IOException {
            OutputStreamWriter outputStreamWriter = null;
            try {
                OutputStreamWriter outputStreamWriter2 = new OutputStreamWriter(m26327(i), Util.f20353);
                try {
                    outputStreamWriter2.write(str);
                    Util.m26350((Closeable) outputStreamWriter2);
                } catch (Throwable th) {
                    th = th;
                    outputStreamWriter = outputStreamWriter2;
                    Util.m26350((Closeable) outputStreamWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                Util.m26350((Closeable) outputStreamWriter);
                throw th;
            }
        }
    }

    private final class Entry {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public long f20336;
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public Editor f20337;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final String f20338;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean f20339;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final long[] f20340;

        private Entry(String str) {
            this.f20338 = str;
            this.f20340 = new long[DiskLruCache.this.f20321];
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private IOException m26331(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26339(String[] strArr) throws IOException {
            if (strArr.length != DiskLruCache.this.f20321) {
                throw m26331(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.f20340[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e) {
                    throw m26331(strArr);
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public File m26341(int i) {
            return new File(DiskLruCache.this.f20327, this.f20338 + "." + i + ".tmp");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public File m26342(int i) {
            return new File(DiskLruCache.this.f20327, this.f20338 + "." + i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m26343() throws IOException {
            StringBuilder sb = new StringBuilder();
            for (long append : this.f20340) {
                sb.append(' ').append(append);
            }
            return sb.toString();
        }
    }

    public final class Snapshot implements Closeable {

        /* renamed from: 连任  reason: contains not printable characters */
        private final long[] f20342;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f20343;

        /* renamed from: 麤  reason: contains not printable characters */
        private final InputStream[] f20344;

        /* renamed from: 齉  reason: contains not printable characters */
        private final long f20345;

        private Snapshot(String str, long j, InputStream[] inputStreamArr, long[] jArr) {
            this.f20343 = str;
            this.f20345 = j;
            this.f20344 = inputStreamArr;
            this.f20342 = jArr;
        }

        public void close() {
            for (InputStream r2 : this.f20344) {
                Util.m26350((Closeable) r2);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m26344(int i) throws IOException {
            return DiskLruCache.m26301(m26345(i));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public InputStream m26345(int i) {
            return this.f20344[i];
        }
    }

    private DiskLruCache(File file, int i, int i2, long j) {
        this.f20327 = file;
        this.f20316 = i;
        this.f20326 = new File(file, "journal");
        this.f20324 = new File(file, "journal.tmp");
        this.f20315 = new File(file, "journal.bkp");
        this.f20321 = i2;
        this.f20317 = j;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public synchronized void m26294() throws IOException {
        if (this.f20323 != null) {
            this.f20323.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f20324), Util.f20354));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(PubnativeRequest.LEGACY_ZONE_ID);
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(Integer.toString(this.f20316));
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(Integer.toString(this.f20321));
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(StringUtils.LF);
            for (Entry next : this.f20320.values()) {
                if (next.f20337 != null) {
                    bufferedWriter.write("DIRTY " + next.f20338 + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.f20338 + next.m26343() + 10);
                }
            }
            bufferedWriter.close();
            if (this.f20326.exists()) {
                m26316(this.f20326, this.f20315, true);
            }
            m26316(this.f20324, this.f20326, false);
            this.f20315.delete();
            this.f20323 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f20326, true), Util.f20354));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m26295() {
        return this.f20318 >= 2000 && this.f20318 >= this.f20320.size();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m26296() {
        if (this.f20323 == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m26297() throws IOException {
        while (this.f20322 > this.f20317) {
            m26319((String) this.f20320.entrySet().iterator().next().getKey());
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m26299() throws IOException {
        m26315(this.f20324);
        Iterator<Entry> it2 = this.f20320.values().iterator();
        while (it2.hasNext()) {
            Entry next = it2.next();
            if (next.f20337 == null) {
                for (int i = 0; i < this.f20321; i++) {
                    this.f20322 += next.f20340[i];
                }
            } else {
                Editor unused = next.f20337 = null;
                for (int i2 = 0; i2 < this.f20321; i2++) {
                    m26315(next.m26342(i2));
                    m26315(next.m26341(i2));
                }
                it2.remove();
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m26300(String str) {
        if (!f20313.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,64}: \"" + str + "\"");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m26301(InputStream inputStream) throws IOException {
        return Util.m26349((Reader) new InputStreamReader(inputStream, Util.f20353));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m26303() throws IOException {
        int i;
        StrictLineReader strictLineReader = new StrictLineReader(new FileInputStream(this.f20326), Util.f20354);
        try {
            String r4 = strictLineReader.m26348();
            String r7 = strictLineReader.m26348();
            String r0 = strictLineReader.m26348();
            String r6 = strictLineReader.m26348();
            String r1 = strictLineReader.m26348();
            if (!"libcore.io.DiskLruCache".equals(r4) || !PubnativeRequest.LEGACY_ZONE_ID.equals(r7) || !Integer.toString(this.f20316).equals(r0) || !Integer.toString(this.f20321).equals(r6) || !"".equals(r1)) {
                throw new IOException("unexpected journal header: [" + r4 + ", " + r7 + ", " + r6 + ", " + r1 + "]");
            }
            i = 0;
            while (true) {
                m26305(strictLineReader.m26348());
                i++;
            }
        } catch (EOFException e) {
            this.f20318 = i - this.f20320.size();
            Util.m26350((Closeable) strictLineReader);
        } catch (Throwable th) {
            Util.m26350((Closeable) strictLineReader);
            throw th;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m26305(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i = indexOf + 1;
        int indexOf2 = str.indexOf(32, i);
        if (indexOf2 == -1) {
            str2 = str.substring(i);
            if (indexOf == "REMOVE".length() && str.startsWith("REMOVE")) {
                this.f20320.remove(str2);
                return;
            }
        } else {
            str2 = str.substring(i, indexOf2);
        }
        Entry entry = this.f20320.get(str2);
        if (entry == null) {
            entry = new Entry(str2);
            this.f20320.put(str2, entry);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(StringUtils.SPACE);
            boolean unused = entry.f20339 = true;
            Editor unused2 = entry.f20337 = null;
            entry.m26339(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            Editor unused3 = entry.f20337 = new Editor(entry);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0064, code lost:
        if (com.jakewharton.disklrucache.DiskLruCache.Entry.m26336(r1) != null) goto L_0x0020;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.jakewharton.disklrucache.DiskLruCache.Editor m26309(java.lang.String r7, long r8) throws java.io.IOException {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            r6.m26296()     // Catch:{ all -> 0x005d }
            r6.m26300((java.lang.String) r7)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.jakewharton.disklrucache.DiskLruCache$Entry> r2 = r6.f20320     // Catch:{ all -> 0x005d }
            java.lang.Object r1 = r2.get(r7)     // Catch:{ all -> 0x005d }
            com.jakewharton.disklrucache.DiskLruCache$Entry r1 = (com.jakewharton.disklrucache.DiskLruCache.Entry) r1     // Catch:{ all -> 0x005d }
            r2 = -1
            int r2 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x0022
            if (r1 == 0) goto L_0x0020
            long r2 = r1.f20336     // Catch:{ all -> 0x005d }
            int r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r2 == 0) goto L_0x0022
        L_0x0020:
            monitor-exit(r6)
            return r0
        L_0x0022:
            if (r1 != 0) goto L_0x0060
            com.jakewharton.disklrucache.DiskLruCache$Entry r1 = new com.jakewharton.disklrucache.DiskLruCache$Entry     // Catch:{ all -> 0x005d }
            r2 = 0
            r1.<init>(r7)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.jakewharton.disklrucache.DiskLruCache$Entry> r2 = r6.f20320     // Catch:{ all -> 0x005d }
            r2.put(r7, r1)     // Catch:{ all -> 0x005d }
        L_0x002f:
            com.jakewharton.disklrucache.DiskLruCache$Editor r0 = new com.jakewharton.disklrucache.DiskLruCache$Editor     // Catch:{ all -> 0x005d }
            r2 = 0
            r0.<init>(r1)     // Catch:{ all -> 0x005d }
            com.jakewharton.disklrucache.DiskLruCache.Editor unused = r1.f20337 = r0     // Catch:{ all -> 0x005d }
            java.io.Writer r2 = r6.f20323     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            r3.<init>()     // Catch:{ all -> 0x005d }
            java.lang.String r4 = "DIRTY "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ all -> 0x005d }
            r4 = 10
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x005d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x005d }
            r2.write(r3)     // Catch:{ all -> 0x005d }
            java.io.Writer r2 = r6.f20323     // Catch:{ all -> 0x005d }
            r2.flush()     // Catch:{ all -> 0x005d }
            goto L_0x0020
        L_0x005d:
            r2 = move-exception
            monitor-exit(r6)
            throw r2
        L_0x0060:
            com.jakewharton.disklrucache.DiskLruCache$Editor r2 = r1.f20337     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x002f
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jakewharton.disklrucache.DiskLruCache.m26309(java.lang.String, long):com.jakewharton.disklrucache.DiskLruCache$Editor");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DiskLruCache m26310(File file, int i, int i2, long j) throws IOException {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    m26316(file2, file3, false);
                }
            }
            DiskLruCache diskLruCache = new DiskLruCache(file, i, i2, j);
            if (diskLruCache.f20326.exists()) {
                try {
                    diskLruCache.m26303();
                    diskLruCache.m26299();
                    diskLruCache.f20323 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(diskLruCache.f20326, true), Util.f20354));
                    DiskLruCache diskLruCache2 = diskLruCache;
                    return diskLruCache;
                } catch (IOException e) {
                    System.out.println("DiskLruCache " + file + " is corrupt: " + e.getMessage() + ", removing");
                    diskLruCache.m26318();
                }
            }
            file.mkdirs();
            DiskLruCache diskLruCache3 = new DiskLruCache(file, i, i2, j);
            diskLruCache3.m26294();
            DiskLruCache diskLruCache4 = diskLruCache3;
            return diskLruCache3;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m26313(Editor editor, boolean z) throws IOException {
        Entry r2 = editor.f20331;
        if (r2.f20337 != editor) {
            throw new IllegalStateException();
        }
        if (z) {
            if (!r2.f20339) {
                int i = 0;
                while (true) {
                    if (i >= this.f20321) {
                        break;
                    } else if (!editor.f20333[i]) {
                        editor.m26326();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                    } else if (!r2.m26341(i).exists()) {
                        editor.m26326();
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.f20321; i2++) {
            File r1 = r2.m26341(i2);
            if (!z) {
                m26315(r1);
            } else if (r1.exists()) {
                File r0 = r2.m26342(i2);
                r1.renameTo(r0);
                long j = r2.f20340[i2];
                long length = r0.length();
                r2.f20340[i2] = length;
                this.f20322 = (this.f20322 - j) + length;
            }
        }
        this.f20318++;
        Editor unused = r2.f20337 = null;
        if (r2.f20339 || z) {
            boolean unused2 = r2.f20339 = true;
            this.f20323.write("CLEAN " + r2.f20338 + r2.m26343() + 10);
            if (z) {
                long j2 = this.f20319;
                this.f20319 = 1 + j2;
                long unused3 = r2.f20336 = j2;
            }
        } else {
            this.f20320.remove(r2.f20338);
            this.f20323.write("REMOVE " + r2.f20338 + 10);
        }
        this.f20323.flush();
        if (this.f20322 > this.f20317 || m26295()) {
            this.f20325.submit(this.f20328);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26315(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26316(File file, File file2, boolean z) throws IOException {
        if (z) {
            m26315(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    public synchronized void close() throws IOException {
        if (this.f20323 != null) {
            Iterator it2 = new ArrayList(this.f20320.values()).iterator();
            while (it2.hasNext()) {
                Entry entry = (Entry) it2.next();
                if (entry.f20337 != null) {
                    entry.f20337.m26326();
                }
            }
            m26297();
            this.f20323.close();
            this.f20323 = null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Editor m26317(String str) throws IOException {
        return m26309(str, -1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26318() throws IOException {
        close();
        Util.m26351(this.f20327);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized boolean m26319(String str) throws IOException {
        boolean z;
        m26296();
        m26300(str);
        Entry entry = this.f20320.get(str);
        if (entry == null || entry.f20337 != null) {
            z = false;
        } else {
            int i = 0;
            while (i < this.f20321) {
                File r1 = entry.m26342(i);
                if (!r1.exists() || r1.delete()) {
                    this.f20322 -= entry.f20340[i];
                    entry.f20340[i] = 0;
                    i++;
                } else {
                    throw new IOException("failed to delete " + r1);
                }
            }
            this.f20318++;
            this.f20323.append("REMOVE " + str + 10);
            this.f20320.remove(str);
            if (m26295()) {
                this.f20325.submit(this.f20328);
            }
            z = true;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized long m26320() {
        return this.f20322;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Snapshot m26321(String str) throws IOException {
        Snapshot snapshot = null;
        synchronized (this) {
            m26296();
            m26300(str);
            Entry entry = this.f20320.get(str);
            if (entry != null) {
                if (entry.f20339) {
                    InputStream[] inputStreamArr = new InputStream[this.f20321];
                    int i = 0;
                    while (i < this.f20321) {
                        try {
                            inputStreamArr[i] = new FileInputStream(entry.m26342(i));
                            i++;
                        } catch (FileNotFoundException e) {
                            int i2 = 0;
                            while (i2 < this.f20321 && inputStreamArr[i2] != null) {
                                Util.m26350((Closeable) inputStreamArr[i2]);
                                i2++;
                            }
                        }
                    }
                    this.f20318++;
                    this.f20323.append("READ " + str + 10);
                    if (m26295()) {
                        this.f20325.submit(this.f20328);
                    }
                    snapshot = new Snapshot(str, entry.f20336, inputStreamArr, entry.f20340);
                }
            }
        }
        return snapshot;
    }
}
