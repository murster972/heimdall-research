package com.franmontiel.persistentcookiejar;

import com.franmontiel.persistentcookiejar.cache.CookieCache;
import com.franmontiel.persistentcookiejar.persistence.CookiePersistor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import okhttp3.Cookie;
import okhttp3.HttpUrl;

public class PersistentCookieJar implements ClearableCookieJar {

    /* renamed from: 靐  reason: contains not printable characters */
    private CookieCache f20303;

    /* renamed from: 齉  reason: contains not printable characters */
    private CookiePersistor f20304;

    public PersistentCookieJar(CookieCache cookieCache, CookiePersistor cookiePersistor) {
        this.f20303 = cookieCache;
        this.f20304 = cookiePersistor;
        this.f20303.m26278(cookiePersistor.m26285());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<Cookie> m26274(List<Cookie> list) {
        ArrayList arrayList = new ArrayList();
        for (Cookie next : list) {
            if (next.m6912()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m26275(Cookie cookie) {
        return cookie.m6911() < System.currentTimeMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized List<Cookie> m26276(HttpUrl httpUrl) {
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        arrayList = new ArrayList();
        Iterator it2 = this.f20303.iterator();
        while (it2.hasNext()) {
            Cookie cookie = (Cookie) it2.next();
            if (m26275(cookie)) {
                arrayList2.add(cookie);
                it2.remove();
            } else if (cookie.m6915(httpUrl)) {
                arrayList.add(cookie);
            }
        }
        this.f20304.m26284(arrayList2);
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m26277(HttpUrl httpUrl, List<Cookie> list) {
        this.f20303.m26278(list);
        this.f20304.m26286(m26274(list));
    }
}
