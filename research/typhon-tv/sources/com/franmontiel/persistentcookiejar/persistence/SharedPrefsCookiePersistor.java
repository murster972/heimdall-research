package com.franmontiel.persistentcookiejar.persistence;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.mopub.common.TyphoonApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import okhttp3.Cookie;

@SuppressLint({"CommitPrefEdits"})
public class SharedPrefsCookiePersistor implements CookiePersistor {

    /* renamed from: 龘  reason: contains not printable characters */
    private final SharedPreferences f20312;

    public SharedPrefsCookiePersistor(Context context) {
        this(context.getSharedPreferences("CookiePersistence", 0));
    }

    public SharedPrefsCookiePersistor(SharedPreferences sharedPreferences) {
        this.f20312 = sharedPreferences;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m26289(Cookie cookie) {
        return (cookie.m6908() ? TyphoonApp.HTTPS : TyphoonApp.HTTP) + "://" + cookie.m6905() + cookie.m6906() + "|" + cookie.m6913();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m26290(Collection<Cookie> collection) {
        SharedPreferences.Editor edit = this.f20312.edit();
        for (Cookie r0 : collection) {
            edit.remove(m26289(r0));
        }
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Cookie> m26291() {
        ArrayList arrayList = new ArrayList(this.f20312.getAll().size());
        for (Map.Entry<String, ?> value : this.f20312.getAll().entrySet()) {
            Cookie decode = new SerializableCookie().decode((String) value.getValue());
            if (decode != null) {
                arrayList.add(decode);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26292(Collection<Cookie> collection) {
        SharedPreferences.Editor edit = this.f20312.edit();
        for (Cookie next : collection) {
            edit.putString(m26289(next), new SerializableCookie().encode(next));
        }
        edit.commit();
    }
}
