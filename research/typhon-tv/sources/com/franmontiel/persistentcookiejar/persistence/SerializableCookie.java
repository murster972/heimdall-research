package com.franmontiel.persistentcookiejar.persistence;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import okhttp3.Cookie;

public class SerializableCookie implements Serializable {
    private static final long serialVersionUID = -8594045714036645534L;

    /* renamed from: 齉  reason: contains not printable characters */
    private static long f20309 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f20310 = SerializableCookie.class.getSimpleName();

    /* renamed from: 靐  reason: contains not printable characters */
    private transient Cookie f20311;

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        Cookie.Builder builder = new Cookie.Builder();
        builder.m19908((String) objectInputStream.readObject());
        builder.m19902((String) objectInputStream.readObject());
        long readLong = objectInputStream.readLong();
        if (readLong != f20309) {
            builder.m19907(readLong);
        }
        String str = (String) objectInputStream.readObject();
        builder.m19904(str);
        builder.m19900((String) objectInputStream.readObject());
        if (objectInputStream.readBoolean()) {
            builder.m19906();
        }
        if (objectInputStream.readBoolean()) {
            builder.m19901();
        }
        if (objectInputStream.readBoolean()) {
            builder.m19903(str);
        }
        this.f20311 = builder.m19905();
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.f20311.m6913());
        objectOutputStream.writeObject(this.f20311.m6910());
        objectOutputStream.writeLong(this.f20311.m6912() ? this.f20311.m6911() : f20309);
        objectOutputStream.writeObject(this.f20311.m6905());
        objectOutputStream.writeObject(this.f20311.m6906());
        objectOutputStream.writeBoolean(this.f20311.m6908());
        objectOutputStream.writeBoolean(this.f20311.m6907());
        objectOutputStream.writeBoolean(this.f20311.m6909());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m26287(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            byte b2 = b & 255;
            if (b2 < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(b2));
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m26288(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035 A[SYNTHETIC, Splitter:B:16:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e A[SYNTHETIC, Splitter:B:24:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005f A[SYNTHETIC, Splitter:B:30:0x005f] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0044=Splitter:B:21:0x0044, B:13:0x002b=Splitter:B:13:0x002b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Cookie decode(java.lang.String r10) {
        /*
            r9 = this;
            byte[] r1 = m26288((java.lang.String) r10)
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            r0.<init>(r1)
            r2 = 0
            r4 = 0
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x002a, ClassNotFoundException -> 0x0043 }
            r5.<init>(r0)     // Catch:{ IOException -> 0x002a, ClassNotFoundException -> 0x0043 }
            java.lang.Object r6 = r5.readObject()     // Catch:{ IOException -> 0x0073, ClassNotFoundException -> 0x0070, all -> 0x006d }
            com.franmontiel.persistentcookiejar.persistence.SerializableCookie r6 = (com.franmontiel.persistentcookiejar.persistence.SerializableCookie) r6     // Catch:{ IOException -> 0x0073, ClassNotFoundException -> 0x0070, all -> 0x006d }
            okhttp3.Cookie r2 = r6.f20311     // Catch:{ IOException -> 0x0073, ClassNotFoundException -> 0x0070, all -> 0x006d }
            if (r5 == 0) goto L_0x0076
            r5.close()     // Catch:{ IOException -> 0x001f }
            r4 = r5
        L_0x001e:
            return r2
        L_0x001f:
            r3 = move-exception
            java.lang.String r6 = f20310
            java.lang.String r7 = "Stream not closed in decodeCookie"
            android.util.Log.d(r6, r7, r3)
            r4 = r5
            goto L_0x001e
        L_0x002a:
            r3 = move-exception
        L_0x002b:
            java.lang.String r6 = f20310     // Catch:{ all -> 0x005c }
            java.lang.String r7 = "IOException in decodeCookie"
            android.util.Log.d(r6, r7, r3)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x001e
            r4.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x001e
        L_0x0039:
            r3 = move-exception
            java.lang.String r6 = f20310
            java.lang.String r7 = "Stream not closed in decodeCookie"
            android.util.Log.d(r6, r7, r3)
            goto L_0x001e
        L_0x0043:
            r3 = move-exception
        L_0x0044:
            java.lang.String r6 = f20310     // Catch:{ all -> 0x005c }
            java.lang.String r7 = "ClassNotFoundException in decodeCookie"
            android.util.Log.d(r6, r7, r3)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x001e
            r4.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x001e
        L_0x0052:
            r3 = move-exception
            java.lang.String r6 = f20310
            java.lang.String r7 = "Stream not closed in decodeCookie"
            android.util.Log.d(r6, r7, r3)
            goto L_0x001e
        L_0x005c:
            r6 = move-exception
        L_0x005d:
            if (r4 == 0) goto L_0x0062
            r4.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0062:
            throw r6
        L_0x0063:
            r3 = move-exception
            java.lang.String r7 = f20310
            java.lang.String r8 = "Stream not closed in decodeCookie"
            android.util.Log.d(r7, r8, r3)
            goto L_0x0062
        L_0x006d:
            r6 = move-exception
            r4 = r5
            goto L_0x005d
        L_0x0070:
            r3 = move-exception
            r4 = r5
            goto L_0x0044
        L_0x0073:
            r3 = move-exception
            r4 = r5
            goto L_0x002b
        L_0x0076:
            r4 = r5
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.franmontiel.persistentcookiejar.persistence.SerializableCookie.decode(java.lang.String):okhttp3.Cookie");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035 A[SYNTHETIC, Splitter:B:16:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0046 A[SYNTHETIC, Splitter:B:22:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String encode(okhttp3.Cookie r8) {
        /*
            r7 = this;
            r7.f20311 = r8
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            r2 = 0
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0029 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0029 }
            r3.writeObject(r7)     // Catch:{ IOException -> 0x0057, all -> 0x0054 }
            if (r3 == 0) goto L_0x0015
            r3.close()     // Catch:{ IOException -> 0x001f }
        L_0x0015:
            byte[] r4 = r0.toByteArray()
            java.lang.String r4 = m26287((byte[]) r4)
            r2 = r3
        L_0x001e:
            return r4
        L_0x001f:
            r1 = move-exception
            java.lang.String r4 = f20310
            java.lang.String r5 = "Stream not closed in encodeCookie"
            android.util.Log.d(r4, r5, r1)
            goto L_0x0015
        L_0x0029:
            r1 = move-exception
        L_0x002a:
            java.lang.String r4 = f20310     // Catch:{ all -> 0x0043 }
            java.lang.String r5 = "IOException in encodeCookie"
            android.util.Log.d(r4, r5, r1)     // Catch:{ all -> 0x0043 }
            r4 = 0
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x001e
        L_0x0039:
            r1 = move-exception
            java.lang.String r5 = f20310
            java.lang.String r6 = "Stream not closed in encodeCookie"
            android.util.Log.d(r5, r6, r1)
            goto L_0x001e
        L_0x0043:
            r4 = move-exception
        L_0x0044:
            if (r2 == 0) goto L_0x0049
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x0049:
            throw r4
        L_0x004a:
            r1 = move-exception
            java.lang.String r5 = f20310
            java.lang.String r6 = "Stream not closed in encodeCookie"
            android.util.Log.d(r5, r6, r1)
            goto L_0x0049
        L_0x0054:
            r4 = move-exception
            r2 = r3
            goto L_0x0044
        L_0x0057:
            r1 = move-exception
            r2 = r3
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.franmontiel.persistentcookiejar.persistence.SerializableCookie.encode(okhttp3.Cookie):java.lang.String");
    }
}
