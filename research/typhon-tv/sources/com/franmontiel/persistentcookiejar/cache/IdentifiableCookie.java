package com.franmontiel.persistentcookiejar.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import okhttp3.Cookie;

class IdentifiableCookie {

    /* renamed from: 龘  reason: contains not printable characters */
    private Cookie f20305;

    IdentifiableCookie(Cookie cookie) {
        this.f20305 = cookie;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static List<IdentifiableCookie> m26279(Collection<Cookie> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (Cookie identifiableCookie : collection) {
            arrayList.add(new IdentifiableCookie(identifiableCookie));
        }
        return arrayList;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IdentifiableCookie)) {
            return false;
        }
        IdentifiableCookie identifiableCookie = (IdentifiableCookie) obj;
        return identifiableCookie.f20305.m6913().equals(this.f20305.m6913()) && identifiableCookie.f20305.m6905().equals(this.f20305.m6905()) && identifiableCookie.f20305.m6906().equals(this.f20305.m6906()) && identifiableCookie.f20305.m6908() == this.f20305.m6908() && identifiableCookie.f20305.m6909() == this.f20305.m6909();
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.f20305.m6913().hashCode() + 527) * 31) + this.f20305.m6905().hashCode()) * 31) + this.f20305.m6906().hashCode()) * 31) + (this.f20305.m6908() ? 0 : 1)) * 31;
        if (!this.f20305.m6909()) {
            i = 1;
        }
        return hashCode + i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Cookie m26280() {
        return this.f20305;
    }
}
