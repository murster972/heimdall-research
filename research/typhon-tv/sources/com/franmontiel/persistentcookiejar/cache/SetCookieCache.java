package com.franmontiel.persistentcookiejar.cache;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import okhttp3.Cookie;

public class SetCookieCache implements CookieCache {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<IdentifiableCookie> f20306 = new HashSet();

    private class SetCookieCacheIterator implements Iterator<Cookie> {

        /* renamed from: 靐  reason: contains not printable characters */
        private Iterator<IdentifiableCookie> f20307;

        public SetCookieCacheIterator() {
            this.f20307 = SetCookieCache.this.f20306.iterator();
        }

        public boolean hasNext() {
            return this.f20307.hasNext();
        }

        public void remove() {
            this.f20307.remove();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Cookie next() {
            return this.f20307.next().m26280();
        }
    }

    public Iterator<Cookie> iterator() {
        return new SetCookieCacheIterator();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26282(Collection<Cookie> collection) {
        for (IdentifiableCookie next : IdentifiableCookie.m26279(collection)) {
            this.f20306.remove(next);
            this.f20306.add(next);
        }
    }
}
