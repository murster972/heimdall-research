package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.CalendarMovieEntry;
import com.uwetrottmann.trakt5.entities.CalendarShowEntry;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Calendars {
    @GET(m7325 = "calendars/all/movies/{startdate}/{days}")
    Call<List<CalendarMovieEntry>> movies(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/my/movies/{startdate}/{days}")
    Call<List<CalendarMovieEntry>> myMovies(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/my/shows/new/{startdate}/{days}")
    Call<List<CalendarShowEntry>> myNewShows(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/my/shows/premieres/{startdate}/{days}")
    Call<List<CalendarShowEntry>> mySeasonPremieres(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/my/shows/{startdate}/{days}")
    Call<List<CalendarShowEntry>> myShows(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/all/shows/new/{startdate}/{days}")
    Call<List<CalendarShowEntry>> newShows(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/all/shows/premieres/{startdate}/{days}")
    Call<List<CalendarShowEntry>> seasonPremieres(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);

    @GET(m7325 = "calendars/all/shows/{startdate}/{days}")
    Call<List<CalendarShowEntry>> shows(@Path(m7340 = "startdate") String str, @Path(m7340 = "days") int i);
}
