package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.Comment;
import com.uwetrottmann.trakt5.entities.Episode;
import com.uwetrottmann.trakt5.entities.Ratings;
import com.uwetrottmann.trakt5.entities.Stats;
import com.uwetrottmann.trakt5.enums.Extended;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Episodes {
    @GET(m7325 = "shows/{id}/seasons/{season}/episodes/{episode}/comments")
    Call<List<Comment>> comments(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i, @Path(m7340 = "episode") int i2, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/seasons/{season}/episodes/{episode}/ratings")
    Call<Ratings> ratings(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i, @Path(m7340 = "episode") int i2);

    @GET(m7325 = "shows/{id}/seasons/{season}/episodes/{episode}/stats")
    Call<Stats> stats(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i, @Path(m7340 = "episode") int i2);

    @GET(m7325 = "shows/{id}/seasons/{season}/episodes/{episode}")
    Call<Episode> summary(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i, @Path(m7340 = "episode") int i2, @Query(m7341 = true, m7342 = "extended") Extended extended);
}
