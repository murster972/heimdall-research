package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.Credits;
import com.uwetrottmann.trakt5.entities.Person;
import com.uwetrottmann.trakt5.enums.Extended;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface People {
    @GET(m7325 = "people/{id}/movies")
    Call<Credits> movieCredits(@Path(m7340 = "id") String str);

    @GET(m7325 = "people/{id}/shows")
    Call<Credits> showCredits(@Path(m7340 = "id") String str);

    @GET(m7325 = "people/{id}")
    Call<Person> summary(@Path(m7340 = "id") String str, @Query(m7342 = "extended") Extended extended);
}
