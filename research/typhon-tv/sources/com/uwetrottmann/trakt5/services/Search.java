package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.SearchResult;
import com.uwetrottmann.trakt5.enums.Extended;
import com.uwetrottmann.trakt5.enums.IdType;
import com.uwetrottmann.trakt5.enums.Type;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Search {
    @GET(m7325 = "search/{id_type}/{id}")
    Call<List<SearchResult>> idLookup(@Path(m7339 = true, m7340 = "id_type") IdType idType, @Path(m7339 = true, m7340 = "id") String str, @Query(m7342 = "type") Type type, @Query(m7342 = "extended") Extended extended, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2);

    @GET(m7325 = "search/{type}")
    Call<List<SearchResult>> textQuery(@Path(m7340 = "type") Type type, @Query(m7342 = "query") String str, @Query(m7342 = "years") String str2, @Query(m7342 = "genres") String str3, @Query(m7342 = "languages") String str4, @Query(m7342 = "countries") String str5, @Query(m7342 = "runtimes") String str6, @Query(m7342 = "ratings") String str7, @Query(m7342 = "extended") Extended extended, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2);

    @GET(m7325 = "search/movie")
    Call<List<SearchResult>> textQueryMovie(@Query(m7342 = "query") String str, @Query(m7342 = "years") String str2, @Query(m7342 = "genres") String str3, @Query(m7342 = "languages") String str4, @Query(m7342 = "countries") String str5, @Query(m7342 = "runtimes") String str6, @Query(m7342 = "ratings") String str7, @Query(m7342 = "certifications") String str8, @Query(m7342 = "extended") Extended extended, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2);

    @GET(m7325 = "search/show")
    Call<List<SearchResult>> textQueryShow(@Query(m7342 = "query") String str, @Query(m7342 = "years") String str2, @Query(m7342 = "genres") String str3, @Query(m7342 = "languages") String str4, @Query(m7342 = "countries") String str5, @Query(m7342 = "runtimes") String str6, @Query(m7342 = "ratings") String str7, @Query(m7342 = "certifications") String str8, @Query(m7342 = "networks") String str9, @Query(m7342 = "status") String str10, @Query(m7342 = "extended") Extended extended, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2);
}
