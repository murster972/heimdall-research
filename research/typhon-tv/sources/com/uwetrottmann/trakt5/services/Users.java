package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.BaseMovie;
import com.uwetrottmann.trakt5.entities.BaseShow;
import com.uwetrottmann.trakt5.entities.Followed;
import com.uwetrottmann.trakt5.entities.Follower;
import com.uwetrottmann.trakt5.entities.Friend;
import com.uwetrottmann.trakt5.entities.HistoryEntry;
import com.uwetrottmann.trakt5.entities.ListEntry;
import com.uwetrottmann.trakt5.entities.RatedEpisode;
import com.uwetrottmann.trakt5.entities.RatedMovie;
import com.uwetrottmann.trakt5.entities.RatedSeason;
import com.uwetrottmann.trakt5.entities.RatedShow;
import com.uwetrottmann.trakt5.entities.Settings;
import com.uwetrottmann.trakt5.entities.SyncItems;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import com.uwetrottmann.trakt5.entities.TraktList;
import com.uwetrottmann.trakt5.entities.User;
import com.uwetrottmann.trakt5.entities.UserSlug;
import com.uwetrottmann.trakt5.entities.WatchlistedEpisode;
import com.uwetrottmann.trakt5.entities.WatchlistedSeason;
import com.uwetrottmann.trakt5.enums.Extended;
import com.uwetrottmann.trakt5.enums.HistoryType;
import com.uwetrottmann.trakt5.enums.RatingsFilter;
import java.util.List;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Users {
    @POST(m7334 = "users/{username}/lists/{id}/items")
    Call<SyncResponse> addListItems(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "id") String str, @Body SyncItems syncItems);

    @GET(m7325 = "users/{username}/collection/movies")
    Call<List<BaseMovie>> collectionMovies(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/collection/shows")
    Call<List<BaseShow>> collectionShows(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @POST(m7334 = "users/{username}/lists")
    Call<TraktList> createList(@Path(m7340 = "username") UserSlug userSlug, @Body TraktList traktList);

    @DELETE(m7321 = "users/{username}/lists/{id}")
    Call<Void> deleteList(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "id") String str);

    @POST(m7334 = "users/{username}/lists/{id}/items/remove")
    Call<SyncResponse> deleteListItems(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "id") String str, @Body SyncItems syncItems);

    @POST(m7334 = "users/{username}/follow")
    Call<Followed> follow(@Path(m7340 = "username") UserSlug userSlug);

    @GET(m7325 = "users/{username}/followers")
    Call<List<Follower>> followers(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/following")
    Call<List<Follower>> following(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/friends")
    Call<List<Friend>> friends(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/history/{type}/{id}")
    Call<List<HistoryEntry>> history(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "type") HistoryType historyType, @Path(m7340 = "id") int i, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended, @Query(m7342 = "start_at") DateTime dateTime, @Query(m7342 = "end_at") DateTime dateTime2);

    @GET(m7325 = "users/{username}/history/{type}")
    Call<List<HistoryEntry>> history(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "type") HistoryType historyType, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended, @Query(m7342 = "start_at") DateTime dateTime, @Query(m7342 = "end_at") DateTime dateTime2);

    @GET(m7325 = "users/{username}/history")
    Call<List<HistoryEntry>> history(@Path(m7340 = "username") UserSlug userSlug, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended, @Query(m7342 = "start_at") DateTime dateTime, @Query(m7342 = "end_at") DateTime dateTime2);

    @GET(m7325 = "users/{username}/lists/{id}/items")
    Call<List<ListEntry>> listItems(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "id") String str, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/lists")
    Call<List<TraktList>> lists(@Path(m7340 = "username") UserSlug userSlug);

    @GET(m7325 = "users/{username}")
    Call<User> profile(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/ratings/episodes{rating}")
    Call<List<RatedEpisode>> ratingsEpisodes(@Path(m7340 = "username") UserSlug userSlug, @Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/ratings/movies{rating}")
    Call<List<RatedMovie>> ratingsMovies(@Path(m7340 = "username") UserSlug userSlug, @Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/ratings/seasons{rating}")
    Call<List<RatedSeason>> ratingsSeasons(@Path(m7340 = "username") UserSlug userSlug, @Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/ratings/shows{rating}")
    Call<List<RatedShow>> ratingsShows(@Path(m7340 = "username") UserSlug userSlug, @Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/settings")
    Call<Settings> settings();

    @DELETE(m7321 = "users/{username}/follow")
    Call<Void> unfollow(@Path(m7340 = "username") UserSlug userSlug);

    @PUT(m7335 = "users/{username}/lists/{id}")
    Call<TraktList> updateList(@Path(m7340 = "username") UserSlug userSlug, @Path(m7340 = "id") String str, @Body TraktList traktList);

    @GET(m7325 = "users/{username}/watched/movies")
    Call<List<BaseMovie>> watchedMovies(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/watched/shows")
    Call<List<BaseShow>> watchedShows(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/watchlist/episodes")
    Call<List<WatchlistedEpisode>> watchlistEpisodes(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/watchlist/movies")
    Call<List<BaseMovie>> watchlistMovies(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/watchlist/seasons")
    Call<List<WatchlistedSeason>> watchlistSeasons(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "users/{username}/watchlist/shows")
    Call<List<BaseShow>> watchlistShows(@Path(m7340 = "username") UserSlug userSlug, @Query(m7341 = true, m7342 = "extended") Extended extended);
}
