package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.AccessToken;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Authentication {
    @FormUrlEncoded
    @POST(m7334 = "https://trakt.tv/oauth/token")
    Call<AccessToken> exchangeCodeForAccessToken(@Field(m7323 = "grant_type") String str, @Field(m7323 = "code") String str2, @Field(m7323 = "client_id") String str3, @Field(m7323 = "client_secret") String str4, @Field(m7323 = "redirect_uri") String str5);

    @FormUrlEncoded
    @POST(m7334 = "https://trakt.tv/oauth/token")
    Call<AccessToken> refreshAccessToken(@Field(m7323 = "grant_type") String str, @Field(m7323 = "refresh_token") String str2, @Field(m7323 = "client_id") String str3, @Field(m7323 = "client_secret") String str4, @Field(m7323 = "redirect_uri") String str5);
}
