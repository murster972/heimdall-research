package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.BaseShow;
import com.uwetrottmann.trakt5.entities.Comment;
import com.uwetrottmann.trakt5.entities.Credits;
import com.uwetrottmann.trakt5.entities.Ratings;
import com.uwetrottmann.trakt5.entities.Show;
import com.uwetrottmann.trakt5.entities.Stats;
import com.uwetrottmann.trakt5.entities.Translation;
import com.uwetrottmann.trakt5.entities.TrendingShow;
import com.uwetrottmann.trakt5.enums.Extended;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Shows {
    @GET(m7325 = "shows/{id}/progress/collection")
    Call<BaseShow> collectedProgress(@Path(m7340 = "id") String str, @Query(m7342 = "hidden") Boolean bool, @Query(m7342 = "specials") Boolean bool2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/comments")
    Call<List<Comment>> comments(@Path(m7340 = "id") String str, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/people")
    Call<Credits> people(@Path(m7340 = "id") String str);

    @GET(m7325 = "shows/popular")
    Call<List<Show>> popular(@Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/ratings")
    Call<Ratings> ratings(@Path(m7340 = "id") String str);

    @GET(m7325 = "shows/{id}/stats")
    Call<Stats> stats(@Path(m7340 = "id") String str);

    @GET(m7325 = "shows/{id}")
    Call<Show> summary(@Path(m7340 = "id") String str, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/translations/{language}")
    Call<List<Translation>> translation(@Path(m7340 = "id") String str, @Path(m7340 = "language") String str2);

    @GET(m7325 = "shows/{id}/translations")
    Call<List<Translation>> translations(@Path(m7340 = "id") String str);

    @GET(m7325 = "shows/trending")
    Call<List<TrendingShow>> trending(@Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/progress/watched")
    Call<BaseShow> watchedProgress(@Path(m7340 = "id") String str, @Query(m7342 = "hidden") Boolean bool, @Query(m7342 = "specials") Boolean bool2, @Query(m7341 = true, m7342 = "extended") Extended extended);
}
