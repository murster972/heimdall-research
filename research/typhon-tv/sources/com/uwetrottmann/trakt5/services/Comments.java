package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.Comment;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Comments {
    @DELETE(m7321 = "comments/{id}")
    Call<Void> delete(@Path(m7340 = "id") int i);

    @GET(m7325 = "comments/{id}")
    Call<Comment> get(@Path(m7340 = "id") int i);

    @POST(m7334 = "comments")
    Call<Comment> post(@Body Comment comment);

    @POST(m7334 = "comments/{id}/replies")
    Call<Comment> postReply(@Path(m7340 = "id") int i, @Body Comment comment);

    @GET(m7325 = "comments/{id}/replies")
    Call<List<Comment>> replies(@Path(m7340 = "id") int i);

    @PUT(m7335 = "comments/{id}")
    Call<Comment> update(@Path(m7340 = "id") int i, @Body Comment comment);
}
