package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.Comment;
import com.uwetrottmann.trakt5.entities.Credits;
import com.uwetrottmann.trakt5.entities.Movie;
import com.uwetrottmann.trakt5.entities.MovieTranslation;
import com.uwetrottmann.trakt5.entities.Ratings;
import com.uwetrottmann.trakt5.entities.Stats;
import com.uwetrottmann.trakt5.entities.TrendingMovie;
import com.uwetrottmann.trakt5.enums.Extended;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Movies {
    @GET(m7325 = "movies/{id}/comments")
    Call<List<Comment>> comments(@Path(m7340 = "id") String str, @Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "movies/{id}/people")
    Call<Credits> people(@Path(m7340 = "id") String str);

    @GET(m7325 = "movies/popular")
    Call<List<Movie>> popular(@Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "movies/{id}/ratings")
    Call<Ratings> ratings(@Path(m7340 = "id") String str);

    @GET(m7325 = "movies/{id}/stats")
    Call<Stats> stats(@Path(m7340 = "id") String str);

    @GET(m7325 = "movies/{id}")
    Call<Movie> summary(@Path(m7340 = "id") String str, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "movies/{id}/translations/{language}")
    Call<List<MovieTranslation>> translation(@Path(m7340 = "id") String str, @Path(m7340 = "language") String str2);

    @GET(m7325 = "movies/{id}/translations")
    Call<List<MovieTranslation>> translations(@Path(m7340 = "id") String str);

    @GET(m7325 = "movies/trending")
    Call<List<TrendingMovie>> trending(@Query(m7342 = "page") Integer num, @Query(m7342 = "limit") Integer num2, @Query(m7341 = true, m7342 = "extended") Extended extended);
}
