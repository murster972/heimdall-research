package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.Comment;
import com.uwetrottmann.trakt5.entities.Episode;
import com.uwetrottmann.trakt5.entities.Ratings;
import com.uwetrottmann.trakt5.entities.Season;
import com.uwetrottmann.trakt5.entities.Stats;
import com.uwetrottmann.trakt5.enums.Extended;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Seasons {
    @GET(m7325 = "shows/{id}/seasons/{season}/comments")
    Call<List<Comment>> comments(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i);

    @GET(m7325 = "shows/{id}/seasons/{season}/ratings")
    Call<Ratings> ratings(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i);

    @GET(m7325 = "shows/{id}/seasons/{season}")
    Call<List<Episode>> season(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "shows/{id}/seasons/{season}/stats")
    Call<Stats> stats(@Path(m7340 = "id") String str, @Path(m7340 = "season") int i);

    @GET(m7325 = "shows/{id}/seasons")
    Call<List<Season>> summary(@Path(m7340 = "id") String str, @Query(m7341 = true, m7342 = "extended") Extended extended);
}
