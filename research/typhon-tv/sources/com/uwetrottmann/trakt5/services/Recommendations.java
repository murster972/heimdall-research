package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.Movie;
import com.uwetrottmann.trakt5.entities.Show;
import com.uwetrottmann.trakt5.enums.Extended;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Recommendations {
    @DELETE(m7321 = "recommendations/movies/{id}")
    Call<Void> dismissMovie(@Path(m7340 = "id") String str);

    @DELETE(m7321 = "recommendations/shows/{id}")
    Call<Void> dismissShow(@Path(m7340 = "id") String str);

    @GET(m7325 = "recommendations/movies")
    Call<List<Movie>> movies(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "recommendations/shows")
    Call<List<Show>> shows(@Query(m7341 = true, m7342 = "extended") Extended extended);
}
