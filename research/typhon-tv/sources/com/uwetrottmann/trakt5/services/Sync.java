package com.uwetrottmann.trakt5.services;

import com.uwetrottmann.trakt5.entities.BaseMovie;
import com.uwetrottmann.trakt5.entities.BaseShow;
import com.uwetrottmann.trakt5.entities.LastActivities;
import com.uwetrottmann.trakt5.entities.RatedEpisode;
import com.uwetrottmann.trakt5.entities.RatedMovie;
import com.uwetrottmann.trakt5.entities.RatedSeason;
import com.uwetrottmann.trakt5.entities.RatedShow;
import com.uwetrottmann.trakt5.entities.SyncItems;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import com.uwetrottmann.trakt5.entities.WatchlistedEpisode;
import com.uwetrottmann.trakt5.entities.WatchlistedSeason;
import com.uwetrottmann.trakt5.enums.Extended;
import com.uwetrottmann.trakt5.enums.RatingsFilter;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Sync {
    @POST(m7334 = "sync/collection")
    Call<SyncResponse> addItemsToCollection(@Body SyncItems syncItems);

    @POST(m7334 = "sync/history")
    Call<SyncResponse> addItemsToWatchedHistory(@Body SyncItems syncItems);

    @POST(m7334 = "sync/watchlist")
    Call<SyncResponse> addItemsToWatchlist(@Body SyncItems syncItems);

    @POST(m7334 = "sync/ratings")
    Call<SyncResponse> addRatings(@Body SyncItems syncItems);

    @GET(m7325 = "sync/collection/movies")
    Call<List<BaseMovie>> collectionMovies(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/collection/shows")
    Call<List<BaseShow>> collectionShows(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @POST(m7334 = "sync/collection/remove")
    Call<SyncResponse> deleteItemsFromCollection(@Body SyncItems syncItems);

    @POST(m7334 = "sync/history/remove")
    Call<SyncResponse> deleteItemsFromWatchedHistory(@Body SyncItems syncItems);

    @POST(m7334 = "sync/watchlist/remove")
    Call<SyncResponse> deleteItemsFromWatchlist(@Body SyncItems syncItems);

    @POST(m7334 = "sync/ratings/remove")
    Call<SyncResponse> deleteRatings(@Body SyncItems syncItems);

    @GET(m7325 = "sync/last_activities")
    Call<LastActivities> lastActivities();

    @GET(m7325 = "sync/ratings/episodes{rating}")
    Call<List<RatedEpisode>> ratingsEpisodes(@Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/ratings/movies{rating}")
    Call<List<RatedMovie>> ratingsMovies(@Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/ratings/seasons{rating}")
    Call<List<RatedSeason>> ratingsSeasons(@Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/ratings/shows{rating}")
    Call<List<RatedShow>> ratingsShows(@Path(m7339 = true, m7340 = "rating") RatingsFilter ratingsFilter, @Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/watched/movies")
    Call<List<BaseMovie>> watchedMovies(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/watched/shows")
    Call<List<BaseShow>> watchedShows(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/watchlist/episodes")
    Call<List<WatchlistedEpisode>> watchlistEpisodes(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/watchlist/movies")
    Call<List<BaseMovie>> watchlistMovies(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/watchlist/seasons")
    Call<List<WatchlistedSeason>> watchlistSeasons(@Query(m7341 = true, m7342 = "extended") Extended extended);

    @GET(m7325 = "sync/watchlist/shows")
    Call<List<BaseShow>> watchlistShows(@Query(m7341 = true, m7342 = "extended") Extended extended);
}
