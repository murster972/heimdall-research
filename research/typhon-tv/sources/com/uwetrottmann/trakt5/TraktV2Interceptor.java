package com.uwetrottmann.trakt5;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.oltu.oauth2.common.OAuth;

public class TraktV2Interceptor implements Interceptor {

    /* renamed from: 龘  reason: contains not printable characters */
    private TraktV2 f14070;

    public TraktV2Interceptor(TraktV2 traktV2) {
        this.f14070 = traktV2;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        return m17928(chain, this.f14070.m17911(), this.f14070.m17913());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Response m17928(Interceptor.Chain chain, String str, String str2) throws IOException {
        Request r0 = chain.m6994();
        if (!"api.trakt.tv".equals(r0.m7053().m6951())) {
            return chain.m6995(r0);
        }
        Request.Builder r1 = r0.m7044();
        r1.m19993(OAuth.HeaderType.CONTENT_TYPE, "application/json");
        r1.m19993("trakt-api-key", str);
        r1.m19993("trakt-api-version", "2");
        if (m17930(r0) && m17929(str2)) {
            r1.m19993(OAuth.HeaderType.AUTHORIZATION, "Bearer " + str2);
        }
        return chain.m6995(r1.m19989());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m17930(Request request) {
        return request.m7052(OAuth.HeaderType.AUTHORIZATION) == null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m17929(String str) {
        return (str == null || str.length() == 0) ? false : true;
    }
}
