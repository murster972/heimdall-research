package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class WatchlistedEpisode {
    public Episode episode;
    public DateTime listed_at;
    public Show show;
}
