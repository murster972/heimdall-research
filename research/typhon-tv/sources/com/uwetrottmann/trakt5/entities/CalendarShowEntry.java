package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class CalendarShowEntry {
    public Episode episode;
    public DateTime first_aired;
    public Show show;
}
