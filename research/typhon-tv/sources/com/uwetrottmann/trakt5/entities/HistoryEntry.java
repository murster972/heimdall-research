package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class HistoryEntry {
    public String action;
    public Episode episode;
    public Long id;
    public Movie movie;
    public Show show;
    public String type;
    public DateTime watched_at;
}
