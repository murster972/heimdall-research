package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class CalendarMovieEntry {
    public Movie movie;
    public DateTime released;
}
