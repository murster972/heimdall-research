package com.uwetrottmann.trakt5.entities;

import com.uwetrottmann.trakt5.enums.Rating;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;

public class SyncSeason {
    public DateTime collected_at;
    public List<SyncEpisode> episodes;
    public Integer number;
    public DateTime rated_at;
    public Rating rating;
    public DateTime watched_at;

    public SyncSeason number(int i) {
        this.number = Integer.valueOf(i);
        return this;
    }

    public SyncSeason episodes(List<SyncEpisode> list) {
        this.episodes = list;
        return this;
    }

    public SyncSeason episodes(SyncEpisode syncEpisode) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(syncEpisode);
        return episodes((List<SyncEpisode>) arrayList);
    }

    public SyncSeason collectedAt(DateTime dateTime) {
        this.collected_at = dateTime;
        return this;
    }

    public SyncSeason watchedAt(DateTime dateTime) {
        this.watched_at = dateTime;
        return this;
    }

    public SyncSeason ratedAt(DateTime dateTime) {
        this.rated_at = dateTime;
        return this;
    }

    public SyncSeason rating(Rating rating2) {
        this.rating = rating2;
        return this;
    }
}
