package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class Episode extends BaseEntity {
    public DateTime first_aired;
    public EpisodeIds ids;
    public Integer number;
    public Integer number_abs;
    public Integer season;
}
