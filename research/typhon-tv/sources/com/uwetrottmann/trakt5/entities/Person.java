package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class Person {
    public String biography;
    public DateTime birthday;
    public String birthplace;
    public DateTime death;
    public String homepage;
    public PersonIds ids;
    public String name;
}
