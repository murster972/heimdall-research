package com.uwetrottmann.trakt5.entities;

import java.util.ArrayList;
import java.util.List;

public class SyncItems {
    public List<SyncEpisode> episodes;
    public List<Integer> ids;
    public List<SyncMovie> movies;
    public List<SyncShow> shows;

    public SyncItems movies(SyncMovie syncMovie) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(syncMovie);
        return movies((List<SyncMovie>) arrayList);
    }

    public SyncItems movies(List<SyncMovie> list) {
        this.movies = list;
        return this;
    }

    public SyncItems shows(SyncShow syncShow) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(syncShow);
        return shows((List<SyncShow>) arrayList);
    }

    public SyncItems shows(List<SyncShow> list) {
        this.shows = list;
        return this;
    }

    public SyncItems episodes(SyncEpisode syncEpisode) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(syncEpisode);
        return episodes((List<SyncEpisode>) arrayList);
    }

    public SyncItems episodes(List<SyncEpisode> list) {
        this.episodes = list;
        return this;
    }

    public SyncItems ids(int i) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(Integer.valueOf(i));
        return ids((List<Integer>) arrayList);
    }

    public SyncItems ids(List<Integer> list) {
        this.ids = list;
        return this;
    }
}
