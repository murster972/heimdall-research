package com.uwetrottmann.trakt5.entities;

import java.util.List;
import org.joda.time.DateTime;

public class BaseShow {
    public Integer aired;
    public Integer completed;
    public List<Season> hidden_seasons;
    public DateTime last_collected_at;
    public DateTime last_watched_at;
    public DateTime listed_at;
    public Episode next_episode;
    public Integer plays;
    public List<BaseSeason> seasons;
    public Show show;
}
