package com.uwetrottmann.trakt5.entities;

import com.uwetrottmann.trakt5.enums.Rating;
import org.joda.time.DateTime;

public class SyncEpisode {
    public DateTime collected_at;
    public EpisodeIds ids;
    public Integer number;
    public DateTime rated_at;
    public Rating rating;
    public Integer season;
    public DateTime watched_at;

    public SyncEpisode number(int i) {
        this.number = Integer.valueOf(i);
        return this;
    }

    public SyncEpisode season(int i) {
        this.season = Integer.valueOf(i);
        return this;
    }

    public SyncEpisode id(EpisodeIds episodeIds) {
        this.ids = episodeIds;
        return this;
    }

    public SyncEpisode collectedAt(DateTime dateTime) {
        this.collected_at = dateTime;
        return this;
    }

    public SyncEpisode watchedAt(DateTime dateTime) {
        this.watched_at = dateTime;
        return this;
    }

    public SyncEpisode ratedAt(DateTime dateTime) {
        this.rated_at = dateTime;
        return this;
    }

    public SyncEpisode rating(Rating rating2) {
        this.rating = rating2;
        return this;
    }
}
