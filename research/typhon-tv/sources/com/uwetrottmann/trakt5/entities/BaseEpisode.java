package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class BaseEpisode {
    public DateTime collected_at;
    public Boolean completed;
    public DateTime last_watched_at;
    public Integer number;
    public Integer plays;
}
