package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class LastActivityMore extends LastActivity {
    public DateTime collected_at;
    public DateTime watched_at;
}
