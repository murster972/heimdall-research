package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public abstract class BaseCheckinResponse {
    public ShareSettings sharing;
    public DateTime watched_at;
}
