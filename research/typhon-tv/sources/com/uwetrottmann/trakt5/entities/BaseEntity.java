package com.uwetrottmann.trakt5.entities;

import java.util.List;
import org.joda.time.DateTime;

public abstract class BaseEntity {
    public List<String> available_translations;
    public String overview;
    public Double rating;
    public String title;
    public DateTime updated_at;
    public Integer votes;
}
