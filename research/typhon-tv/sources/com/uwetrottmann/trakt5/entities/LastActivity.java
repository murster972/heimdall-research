package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class LastActivity {
    public DateTime commented_at;
    public DateTime rated_at;
    public DateTime watchlisted_at;
}
