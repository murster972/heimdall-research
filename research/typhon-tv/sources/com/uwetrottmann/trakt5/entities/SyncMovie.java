package com.uwetrottmann.trakt5.entities;

import com.uwetrottmann.trakt5.enums.Rating;
import org.joda.time.DateTime;

public class SyncMovie {
    public DateTime collected_at;
    public MovieIds ids;
    public DateTime rated_at;
    public Rating rating;
    public DateTime watched_at;

    public SyncMovie id(MovieIds movieIds) {
        this.ids = movieIds;
        return this;
    }

    public SyncMovie collectedAt(DateTime dateTime) {
        this.collected_at = dateTime;
        return this;
    }

    public SyncMovie watchedAt(DateTime dateTime) {
        this.watched_at = dateTime;
        return this;
    }

    public SyncMovie ratedAt(DateTime dateTime) {
        this.rated_at = dateTime;
        return this;
    }

    public SyncMovie rating(Rating rating2) {
        this.rating = rating2;
        return this;
    }
}
