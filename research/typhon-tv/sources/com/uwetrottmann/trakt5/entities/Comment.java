package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class Comment {
    public String comment;
    public DateTime created_at;
    public Episode episode;
    public Integer id;
    public Movie movie;
    public Integer parent_id;
    public Integer replies;
    public Boolean review;
    public Show show;
    public Boolean spoiler;
    public User user;

    public Comment() {
    }

    public Comment(Movie movie2, String str, boolean z, boolean z2) {
        this(str, z, z2);
        this.movie = movie2;
    }

    public Comment(Show show2, String str, boolean z, boolean z2) {
        this(str, z, z2);
        this.show = show2;
    }

    public Comment(Episode episode2, String str, boolean z, boolean z2) {
        this(str, z, z2);
        this.episode = episode2;
    }

    public Comment(String str, boolean z, boolean z2) {
        this.comment = str;
        this.spoiler = Boolean.valueOf(z);
        this.review = Boolean.valueOf(z2);
    }
}
