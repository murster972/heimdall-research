package com.uwetrottmann.trakt5.entities;

import com.uwetrottmann.trakt5.enums.Rating;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;

public class SyncShow {
    public DateTime collected_at;
    public ShowIds ids;
    public DateTime rated_at;
    public Rating rating;
    public List<SyncSeason> seasons;
    public DateTime watched_at;

    public SyncShow id(ShowIds showIds) {
        this.ids = showIds;
        return this;
    }

    public SyncShow seasons(List<SyncSeason> list) {
        this.seasons = list;
        return this;
    }

    public SyncShow seasons(SyncSeason syncSeason) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(syncSeason);
        return seasons((List<SyncSeason>) arrayList);
    }

    public SyncShow collectedAt(DateTime dateTime) {
        this.collected_at = dateTime;
        return this;
    }

    public SyncShow watchedAt(DateTime dateTime) {
        this.watched_at = dateTime;
        return this;
    }

    public SyncShow ratedAt(DateTime dateTime) {
        this.rated_at = dateTime;
        return this;
    }

    public SyncShow rating(Rating rating2) {
        this.rating = rating2;
        return this;
    }
}
