package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class ListEntry {
    public Episode episode;
    public DateTime listed_at;
    public Movie movie;
    public Person person;
    public Show show;
}
