package com.uwetrottmann.trakt5.entities;

import org.joda.time.DateTime;

public class LastActivities {
    public DateTime all;
    public LastActivityMore episodes;
    public LastActivityMore movies;
    public LastActivity seasons;
    public LastActivity shows;
}
