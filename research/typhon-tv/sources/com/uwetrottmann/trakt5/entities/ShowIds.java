package com.uwetrottmann.trakt5.entities;

public class ShowIds extends BaseIds {
    public String slug;
    public Integer tvdb;
    public Integer tvrage;

    public static ShowIds trakt(int trakt) {
        ShowIds ids = new ShowIds();
        ids.trakt = Integer.valueOf(trakt);
        return ids;
    }

    public static ShowIds imdb(String imdb) {
        ShowIds ids = new ShowIds();
        ids.imdb = imdb;
        return ids;
    }

    public static ShowIds tmdb(int tmdb) {
        ShowIds ids = new ShowIds();
        ids.tmdb = Integer.valueOf(tmdb);
        return ids;
    }

    public static ShowIds slug(String slug2) {
        ShowIds ids = new ShowIds();
        ids.slug = slug2;
        return ids;
    }

    public static ShowIds tvdb(int tvdb2) {
        ShowIds ids = new ShowIds();
        ids.tvdb = Integer.valueOf(tvdb2);
        return ids;
    }

    public static ShowIds tvrage(int tvrage2) {
        ShowIds ids = new ShowIds();
        ids.tvrage = Integer.valueOf(tvrage2);
        return ids;
    }
}
