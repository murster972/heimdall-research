package com.uwetrottmann.trakt5;

import com.uwetrottmann.trakt5.entities.AccessToken;
import com.uwetrottmann.trakt5.services.Authentication;
import com.uwetrottmann.trakt5.services.Checkin;
import com.uwetrottmann.trakt5.services.Sync;
import com.uwetrottmann.trakt5.services.Users;
import java.io.IOException;
import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TraktV2 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f14061;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f14062;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f14063;

    /* renamed from: 靐  reason: contains not printable characters */
    private Retrofit f14064;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f14065;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f14066;

    /* renamed from: 龘  reason: contains not printable characters */
    private OkHttpClient f14067;

    public TraktV2(String str) {
        this.f14066 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m17911() {
        return this.f14066;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m17913() {
        return this.f14061;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TraktV2 m17914(String str) {
        this.f14061 = str;
        return this;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m17912() {
        return this.f14062;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TraktV2 m17910(String str) {
        this.f14062 = str;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public Retrofit.Builder m17909() {
        return new Retrofit.Builder().m24404("https://api.trakt.tv/").m24409((Converter.Factory) GsonConverterFactory.m24464(TraktV2Helper.m17921().create())).m24407(m17915());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized OkHttpClient m17915() {
        if (this.f14067 == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            m17916(builder);
            this.f14067 = builder.m7043();
        }
        return this.f14067;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17916(OkHttpClient.Builder builder) {
        builder.m7031(new TraktV2Interceptor(this));
        builder.m7037((Authenticator) new TraktV2Authenticator(this));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Retrofit m17903() {
        if (this.f14064 == null) {
            this.f14064 = m17909().m24410();
        }
        return this.f14064;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Response<AccessToken> m17904() throws IOException {
        return m17905().refreshAccessToken(GrantType.REFRESH_TOKEN.toString(), m17912(), m17911(), this.f14065, this.f14063).m24294();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Authentication m17905() {
        return (Authentication) m17903().m24396(Authentication.class);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Checkin m17906() {
        return (Checkin) m17903().m24396(Checkin.class);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Sync m17907() {
        return (Sync) m17903().m24396(Sync.class);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Users m17908() {
        return (Users) m17903().m24396(Users.class);
    }
}
