package com.uwetrottmann.trakt5;

import com.uwetrottmann.trakt5.entities.AccessToken;
import java.io.IOException;
import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import org.apache.oltu.oauth2.common.OAuth;

public class TraktV2Authenticator implements Authenticator {

    /* renamed from: 龘  reason: contains not printable characters */
    public final TraktV2 f14068;

    public TraktV2Authenticator(TraktV2 traktV2) {
        this.f14068 = traktV2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Request m17919(Route route, Response response) throws IOException {
        return m17918(response, this.f14068);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Request m17918(Response response, TraktV2 traktV2) throws IOException {
        if (!"api.trakt.tv".equals(response.m7069().m7053().m6951()) || m17917(response) >= 2 || traktV2.m17912() == null || traktV2.m17912().length() == 0) {
            return null;
        }
        retrofit2.Response<AccessToken> r1 = traktV2.m17904();
        if (!r1.m24390()) {
            return null;
        }
        String str = r1.m24389().access_token;
        traktV2.m17914(str);
        traktV2.m17910(r1.m24389().refresh_token);
        return response.m7069().m7044().m19993(OAuth.HeaderType.AUTHORIZATION, "Bearer " + str).m19989();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m17917(Response response) {
        int i = 1;
        while (true) {
            response = response.m7062();
            if (response == null) {
                return i;
            }
            i++;
        }
    }
}
