package com.uwetrottmann.trakt5;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.uwetrottmann.trakt5.enums.ListPrivacy;
import com.uwetrottmann.trakt5.enums.Rating;
import com.uwetrottmann.trakt5.enums.Status;
import java.lang.reflect.Type;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class TraktV2Helper {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateTimeFormatter f14069 = ISODateTimeFormat.m21449().m21234();

    /* renamed from: 龘  reason: contains not printable characters */
    public static GsonBuilder m17921() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new JsonDeserializer<DateTime>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public DateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                return TraktV2Helper.f14069.m21236(jsonElement.getAsString());
            }
        });
        gsonBuilder.registerTypeAdapter(DateTime.class, new JsonSerializer<DateTime>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public JsonElement serialize(DateTime dateTime, Type type, JsonSerializationContext jsonSerializationContext) {
                return new JsonPrimitive(dateTime.toString());
            }
        });
        gsonBuilder.registerTypeAdapter(ListPrivacy.class, new JsonDeserializer<ListPrivacy>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public ListPrivacy deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                return ListPrivacy.fromValue(jsonElement.getAsString());
            }
        });
        gsonBuilder.registerTypeAdapter(Rating.class, new JsonDeserializer<Rating>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Rating deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                return Rating.fromValue(jsonElement.getAsInt());
            }
        });
        gsonBuilder.registerTypeAdapter(Rating.class, new JsonSerializer<Rating>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public JsonElement serialize(Rating rating, Type type, JsonSerializationContext jsonSerializationContext) {
                return new JsonPrimitive((Number) Integer.valueOf(rating.value));
            }
        });
        gsonBuilder.registerTypeAdapter(Status.class, new JsonDeserializer<Status>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Status deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                return Status.fromValue(jsonElement.getAsString());
            }
        });
        return gsonBuilder;
    }
}
