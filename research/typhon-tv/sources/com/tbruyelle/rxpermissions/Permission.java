package com.tbruyelle.rxpermissions;

public class Permission {

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f12470;

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean f12471;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f12472;

    public Permission(String str, boolean z, boolean z2) {
        this.f12472 = str;
        this.f12470 = z;
        this.f12471 = z2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Permission permission = (Permission) obj;
        if (this.f12470 == permission.f12470 && this.f12471 == permission.f12471) {
            return this.f12472.equals(permission.f12472);
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int hashCode = ((this.f12472.hashCode() * 31) + (this.f12470 ? 1 : 0)) * 31;
        if (!this.f12471) {
            i = 0;
        }
        return hashCode + i;
    }

    public String toString() {
        return "Permission{name='" + this.f12472 + '\'' + ", granted=" + this.f12470 + ", shouldShowRequestPermissionRationale=" + this.f12471 + '}';
    }
}
