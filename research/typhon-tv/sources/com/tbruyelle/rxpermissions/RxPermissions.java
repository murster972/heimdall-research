package com.tbruyelle.rxpermissions;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Build;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.subjects.PublishSubject;

public class RxPermissions {

    /* renamed from: 龘  reason: contains not printable characters */
    RxPermissionsFragment f12473;

    public RxPermissions(Activity activity) {
        this.f12473 = m15676(activity);
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    /* renamed from: 连任  reason: contains not printable characters */
    public Observable<Permission> m15673(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        ArrayList arrayList2 = new ArrayList();
        for (String str : strArr) {
            this.f12473.m15690("Requesting permission " + str);
            if (m15686(str)) {
                arrayList.add(Observable.m7356(new Permission(str, true, false)));
            } else if (m15682(str)) {
                arrayList.add(Observable.m7356(new Permission(str, false, false)));
            } else {
                PublishSubject<Permission> r2 = this.f12473.m15693(str);
                if (r2 == null) {
                    arrayList2.add(str);
                    r2 = PublishSubject.m25061();
                    this.f12473.m15694(str, r2);
                }
                arrayList.add(r2);
            }
        }
        if (!arrayList2.isEmpty()) {
            m15683((String[]) arrayList2.toArray(new String[arrayList2.size()]));
        }
        return Observable.m7360(Observable.m7355(arrayList));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private RxPermissionsFragment m15674(Activity activity) {
        return (RxPermissionsFragment) activity.getFragmentManager().findFragmentByTag("RxPermissions");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Observable<?> m15675(String... strArr) {
        for (String r0 : strArr) {
            if (!this.f12473.m15692(r0)) {
                return Observable.m7349();
            }
        }
        return Observable.m7356(null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private RxPermissionsFragment m15676(Activity activity) {
        RxPermissionsFragment r2 = m15674(activity);
        if (!(r2 == null)) {
            return r2;
        }
        RxPermissionsFragment rxPermissionsFragment = new RxPermissionsFragment();
        FragmentManager fragmentManager = activity.getFragmentManager();
        fragmentManager.beginTransaction().add(rxPermissionsFragment, "RxPermissions").commitAllowingStateLoss();
        fragmentManager.executePendingTransactions();
        return rxPermissionsFragment;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Observable<?> m15679(Observable<?> observable, Observable<?> observable2) {
        return observable == null ? Observable.m7356(null) : Observable.m7361(observable, observable2);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<Permission> m15680(Observable<?> observable, final String... strArr) {
        if (strArr != null && strArr.length != 0) {
            return m15679(observable, m15675(strArr)).m7396(new Func1<Object, Observable<Permission>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Observable<Permission> call(Object obj) {
                    return RxPermissions.this.m15673(strArr);
                }
            });
        }
        throw new IllegalArgumentException("RxPermissions.request/requestEach requires at least one input permission");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Observable<Boolean> m15681(String... strArr) {
        return Observable.m7356(null).m7405(m15684(strArr));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m15682(String str) {
        return m15685() && this.f12473.m15691(str);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(23)
    /* renamed from: 齉  reason: contains not printable characters */
    public void m15683(String[] strArr) {
        this.f12473.m15690("requestPermissionsFromFragment " + TextUtils.join(", ", strArr));
        this.f12473.m15695(strArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable.Transformer<Object, Boolean> m15684(final String... strArr) {
        return new Observable.Transformer<Object, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<Boolean> call(Observable<Object> observable) {
                return RxPermissions.this.m15680((Observable<?>) observable, strArr).m7399(strArr.length).m7396(new Func1<List<Permission>, Observable<Boolean>>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public Observable<Boolean> call(List<Permission> list) {
                        if (list.isEmpty()) {
                            return Observable.m7349();
                        }
                        for (Permission permission : list) {
                            if (!permission.f12470) {
                                return Observable.m7356(false);
                            }
                        }
                        return Observable.m7356(true);
                    }
                });
            }
        };
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m15685() {
        return Build.VERSION.SDK_INT >= 23;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m15686(String str) {
        return !m15685() || this.f12473.m15697(str);
    }
}
