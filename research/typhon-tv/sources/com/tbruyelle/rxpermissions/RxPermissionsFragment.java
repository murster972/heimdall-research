package com.tbruyelle.rxpermissions;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import rx.subjects.PublishSubject;

public class RxPermissionsFragment extends Fragment {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f12479;

    /* renamed from: 龘  reason: contains not printable characters */
    private Map<String, PublishSubject<Permission>> f12480 = new HashMap();

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 42) {
            boolean[] zArr = new boolean[strArr.length];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                zArr[i2] = shouldShowRequestPermissionRationale(strArr[i2]);
            }
            m15696(strArr, iArr, zArr);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m15690(String str) {
        if (this.f12479) {
            Log.d("RxPermissions", str);
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(23)
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m15691(String str) {
        return getActivity().getPackageManager().isPermissionRevokedByPolicy(str, getActivity().getPackageName());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m15692(String str) {
        return this.f12480.containsKey(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PublishSubject<Permission> m15693(String str) {
        return this.f12480.get(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PublishSubject<Permission> m15694(String str, PublishSubject<Permission> publishSubject) {
        return this.f12480.put(str, publishSubject);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(23)
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15695(String[] strArr) {
        requestPermissions(strArr, 42);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15696(String[] strArr, int[] iArr, boolean[] zArr) {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            m15690("onRequestPermissionsResult  " + strArr[i]);
            PublishSubject publishSubject = this.f12480.get(strArr[i]);
            if (publishSubject == null) {
                Log.e("RxPermissions", "RxPermissions.onRequestPermissionsResult invoked but didn't find the corresponding permission request.");
                return;
            }
            this.f12480.remove(strArr[i]);
            publishSubject.onNext(new Permission(strArr[i], iArr[i] == 0, zArr[i]));
            publishSubject.onCompleted();
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(23)
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m15697(String str) {
        return getActivity().checkSelfPermission(str) == 0;
    }
}
