package com.typhoon.tv.helper.trakt;

import android.content.SharedPreferences;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.trakt.TraktCredentialsInfo;

public class TraktCredentialsHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized void m16109() {
        synchronized (TraktCredentialsHelper.class) {
            SharedPreferences.Editor edit = TVApplication.m6285().edit();
            boolean z = true & false;
            edit.putString("trakt_user", (String) null);
            edit.putString("trakt_access_token", (String) null);
            edit.putString("trakt_refresh_token", (String) null);
            edit.apply();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TraktCredentialsInfo m16110() {
        SharedPreferences r0 = TVApplication.m6285();
        TraktCredentialsInfo traktCredentialsInfo = new TraktCredentialsInfo();
        traktCredentialsInfo.setUser(r0.getString("trakt_user", (String) null));
        traktCredentialsInfo.setAccessToken(r0.getString("trakt_access_token", (String) null));
        traktCredentialsInfo.setRefreshToken(r0.getString("trakt_refresh_token", (String) null));
        return traktCredentialsInfo;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m16111(TraktCredentialsInfo traktCredentialsInfo) {
        Class<TraktCredentialsHelper> cls = TraktCredentialsHelper.class;
        synchronized (TraktCredentialsHelper.class) {
            try {
                m16112(traktCredentialsInfo.getUser(), traktCredentialsInfo.getAccessToken(), traktCredentialsInfo.getRefreshToken());
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m16112(String str, String str2, String str3) {
        synchronized (TraktCredentialsHelper.class) {
            try {
                SharedPreferences.Editor edit = TVApplication.m6285().edit();
                edit.putString("trakt_user", str);
                edit.putString("trakt_access_token", str2);
                edit.putString("trakt_refresh_token", str3);
                edit.apply();
            } catch (Throwable th) {
                throw th;
            }
        }
    }
}
