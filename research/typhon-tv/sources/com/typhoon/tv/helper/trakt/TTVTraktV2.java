package com.typhoon.tv.helper.trakt;

import com.typhoon.tv.helper.http.HttpHelper;
import com.uwetrottmann.trakt5.TraktV2;
import okhttp3.OkHttpClient;

public class TTVTraktV2 extends TraktV2 {

    /* renamed from: 龘  reason: contains not printable characters */
    private OkHttpClient f12673;

    public TTVTraktV2(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized OkHttpClient m16108() {
        if (this.f12673 == null) {
            OkHttpClient.Builder r0 = HttpHelper.m6343().m6354();
            m17916(r0);
            this.f12673 = r0.m7043();
        }
        return this.f12673;
    }
}
