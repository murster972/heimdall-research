package com.typhoon.tv.helper.trakt;

import com.typhoon.tv.Logger;
import com.typhoon.tv.model.trakt.TraktCredentialsInfo;
import com.uwetrottmann.trakt5.TraktV2;
import com.uwetrottmann.trakt5.entities.AccessToken;
import org.apache.oltu.oauth2.common.OAuth;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TraktHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m16113() {
        final TraktCredentialsInfo r6 = TraktCredentialsHelper.m16110();
        if (r6.isValid()) {
            m16114().m17905().refreshAccessToken(OAuth.OAUTH_REFRESH_TOKEN, r6.getRefreshToken(), "701afbabf01f9fb0e2bc524e94afa810d9c6742026d27c8da2611acbc757cbb6", "95dbc0e003bc1a0882588485a127ace45413022970ceeaaf2c7012c1c72b1781", "https://Typhoontv.com").m24295(new Callback<AccessToken>() {
                public void onFailure(Call<AccessToken> call, Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }

                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                    if (response.m24390()) {
                        AccessToken r0 = response.m24389();
                        r6.setAccessToken(r0.access_token);
                        r6.setRefreshToken(r0.refresh_token);
                        TraktCredentialsHelper.m16111(r6);
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TraktV2 m16114() {
        TraktV2 tTVTraktV2 = new TTVTraktV2("701afbabf01f9fb0e2bc524e94afa810d9c6742026d27c8da2611acbc757cbb6");
        TraktCredentialsInfo r0 = TraktCredentialsHelper.m16110();
        if (r0.isValid()) {
            tTVTraktV2 = tTVTraktV2.m17914(r0.getAccessToken()).m17910(r0.getRefreshToken());
        }
        return tTVTraktV2;
    }
}
