package com.typhoon.tv.helper.category;

import android.util.SparseArray;
import android.util.SparseIntArray;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import java.util.ArrayList;

public class TvShowCategoryHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile SparseIntArray f12641 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile SparseIntArray f12642 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public static SparseIntArray m16001() {
        if (f12642 == null || f12642.size() <= 0) {
            synchronized (SparseIntArray.class) {
                if (f12642 == null || f12642.size() <= 0) {
                    f12642 = new SparseIntArray();
                    f12642.append(14, 10759);
                    f12642.append(15, 10765);
                    f12642.append(16, 9648);
                    f12642.append(17, 80);
                    f12642.append(18, 18);
                    f12642.append(19, 35);
                    f12642.append(20, 10751);
                    f12642.append(21, 37);
                    f12642.append(22, 10768);
                    f12642.append(23, 10766);
                    f12642.append(24, 10764);
                    f12642.append(25, 99);
                    f12642.append(26, 10762);
                    f12642.append(27, 16);
                }
            }
        }
        return f12642;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static ArrayList<String> m16002() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(0, I18N.m15706(R.string.action_bookmark));
        arrayList.add(1, I18N.m15706(R.string.trending));
        arrayList.add(2, I18N.m15706(R.string.recently_updated));
        arrayList.add(3, I18N.m15706(R.string.new_shows));
        arrayList.add(4, I18N.m15706(R.string.premieres));
        arrayList.add(5, I18N.m15706(R.string.on_the_air));
        arrayList.add(6, I18N.m15706(R.string.most_popular));
        arrayList.add(7, I18N.m15706(R.string.most_played));
        arrayList.add(8, I18N.m15706(R.string.most_watched));
        arrayList.add(9, I18N.m15706(R.string.top_rated));
        arrayList.add(10, "Netflix™ List");
        arrayList.add(11, "Amazon™ List");
        arrayList.add(12, "Hulu™ List");
        arrayList.add(13, "CBS™");
        arrayList.add(14, I18N.m15706(R.string.genre_tv_action_adventure));
        arrayList.add(15, I18N.m15706(R.string.genre_tv_sci_fantasy));
        arrayList.add(16, I18N.m15706(R.string.genre_tv_mystery));
        arrayList.add(17, I18N.m15706(R.string.genre_tv_crime));
        arrayList.add(18, I18N.m15706(R.string.genre_tv_drama));
        arrayList.add(19, I18N.m15706(R.string.genre_tv_comedy));
        arrayList.add(20, I18N.m15706(R.string.genre_tv_family));
        arrayList.add(21, I18N.m15706(R.string.genre_tv_western));
        arrayList.add(22, I18N.m15706(R.string.genre_tv_war_politics));
        arrayList.add(23, I18N.m15706(R.string.genre_tv_soap));
        arrayList.add(24, I18N.m15706(R.string.genre_tv_reality));
        arrayList.add(25, I18N.m15706(R.string.genre_tv_documentary));
        arrayList.add(26, I18N.m15706(R.string.genre_tv_kids));
        arrayList.add(27, I18N.m15706(R.string.genre_tv_animation));
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static SparseIntArray m16003() {
        if (f12641 == null || f12641.size() <= 0) {
            synchronized (SparseIntArray.class) {
                if (f12641 == null || f12641.size() <= 0) {
                    f12641 = new SparseIntArray();
                    f12641.append(10, 7059554);
                    f12641.append(11, 7059555);
                    f12641.append(12, 7059553);
                    f12641.append(13, 7059552);
                }
            }
        }
        return f12641;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SparseArray<String> m16004() {
        SparseArray<String> sparseArray = new SparseArray<>();
        sparseArray.put(10759, I18N.m15706(R.string.genre_tv_action_adventure));
        sparseArray.put(10765, I18N.m15706(R.string.genre_tv_sci_fantasy));
        sparseArray.put(9648, I18N.m15706(R.string.genre_tv_mystery));
        sparseArray.put(80, I18N.m15706(R.string.genre_tv_crime));
        sparseArray.put(18, I18N.m15706(R.string.genre_tv_drama));
        sparseArray.put(35, I18N.m15706(R.string.genre_tv_comedy));
        sparseArray.put(10751, I18N.m15706(R.string.genre_tv_family));
        sparseArray.put(37, I18N.m15706(R.string.genre_tv_western));
        sparseArray.put(10768, I18N.m15706(R.string.genre_tv_war_politics));
        sparseArray.put(10766, I18N.m15706(R.string.genre_tv_soap));
        sparseArray.put(10764, I18N.m15706(R.string.genre_tv_reality));
        sparseArray.put(99, I18N.m15706(R.string.genre_tv_documentary));
        sparseArray.put(10762, I18N.m15706(R.string.genre_tv_kids));
        sparseArray.put(16, I18N.m15706(R.string.genre_tv_animation));
        return sparseArray;
    }
}
