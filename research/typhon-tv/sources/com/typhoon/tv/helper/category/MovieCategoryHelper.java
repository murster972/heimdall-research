package com.typhoon.tv.helper.category;

import android.util.SparseArray;
import android.util.SparseIntArray;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import java.util.ArrayList;

public class MovieCategoryHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile SparseIntArray f12639 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile SparseIntArray f12640 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static SparseArray<String> m16000() {
        SparseArray<String> map = new SparseArray<>();
        map.put(28, I18N.m15706(R.string.genre_movie_action));
        map.put(12, I18N.m15706(R.string.genre_movie_adventure));
        map.put(53, I18N.m15706(R.string.genre_movie_thriller));
        map.put(878, I18N.m15706(R.string.genre_movie_sci));
        map.put(14, I18N.m15706(R.string.genre_movie_fantasy));
        map.put(9648, I18N.m15706(R.string.genre_movie_history));
        map.put(27, I18N.m15706(R.string.genre_movie_horror));
        map.put(80, I18N.m15706(R.string.genre_movie_crime));
        map.put(18, I18N.m15706(R.string.genre_movie_drama));
        map.put(10749, I18N.m15706(R.string.genre_movie_romance));
        map.put(35, I18N.m15706(R.string.genre_movie_comedy));
        map.put(10751, I18N.m15706(R.string.genre_movie_family));
        map.put(16, I18N.m15706(R.string.genre_movie_animation));
        map.put(37, I18N.m15706(R.string.genre_movie_western));
        map.put(36, I18N.m15706(R.string.genre_movie_history));
        map.put(10752, I18N.m15706(R.string.genre_movie_war));
        map.put(99, I18N.m15706(R.string.genre_movie_documentary));
        map.put(10402, I18N.m15706(R.string.genre_movie_music));
        return map;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static SparseIntArray m15997() {
        if (f12640 == null || f12640.size() <= 0) {
            synchronized (SparseIntArray.class) {
                if (f12640 == null || f12640.size() <= 0) {
                    f12640 = new SparseIntArray();
                    f12640.append(5, 28);
                    f12640.append(6, 12);
                    f12640.append(7, 53);
                    f12640.append(8, 878);
                    f12640.append(9, 14);
                    f12640.append(10, 9648);
                    f12640.append(11, 27);
                    f12640.append(12, 80);
                    f12640.append(13, 18);
                    f12640.append(14, 10749);
                    f12640.append(15, 35);
                    f12640.append(16, 10751);
                    f12640.append(17, 16);
                    f12640.append(18, 37);
                    f12640.append(19, 36);
                    f12640.append(20, 10752);
                    f12640.append(21, 99);
                    f12640.append(22, 10402);
                }
            }
        }
        return f12640;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static SparseIntArray m15999() {
        if (f12639 == null || f12639.size() <= 0) {
            synchronized (SparseIntArray.class) {
                if (f12639 == null || f12639.size() <= 0) {
                    f12639 = new SparseIntArray();
                    f12639.append(23, 7067226);
                    f12639.append(24, 7067227);
                    f12639.append(25, 7067230);
                    f12639.append(26, 7067229);
                    f12639.append(27, 7067228);
                    f12639.append(28, 7067232);
                    f12639.append(29, 7067249);
                }
            }
        }
        return f12639;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static ArrayList<String> m15998() {
        ArrayList<String> list = new ArrayList<>();
        list.add(0, I18N.m15706(R.string.action_bookmark));
        list.add(1, I18N.m15706(R.string.most_popular));
        list.add(2, I18N.m15706(R.string.now_playing));
        list.add(3, I18N.m15706(R.string.new_hd_releases));
        list.add(4, I18N.m15706(R.string.top_rated));
        list.add(5, I18N.m15706(R.string.genre_movie_action));
        list.add(6, I18N.m15706(R.string.genre_movie_adventure));
        list.add(7, I18N.m15706(R.string.genre_movie_thriller));
        list.add(8, I18N.m15706(R.string.genre_movie_sci));
        list.add(9, I18N.m15706(R.string.genre_movie_fantasy));
        list.add(10, I18N.m15706(R.string.genre_movie_mystery));
        list.add(11, I18N.m15706(R.string.genre_movie_horror));
        list.add(12, I18N.m15706(R.string.genre_movie_crime));
        list.add(13, I18N.m15706(R.string.genre_movie_drama));
        list.add(14, I18N.m15706(R.string.genre_movie_romance));
        list.add(15, I18N.m15706(R.string.genre_movie_comedy));
        list.add(16, I18N.m15706(R.string.genre_movie_family));
        list.add(17, I18N.m15706(R.string.genre_movie_animation));
        list.add(18, I18N.m15706(R.string.genre_movie_western));
        list.add(19, I18N.m15706(R.string.genre_movie_history));
        list.add(20, I18N.m15706(R.string.genre_movie_war));
        list.add(21, I18N.m15706(R.string.genre_movie_documentary));
        list.add(22, I18N.m15706(R.string.genre_movie_music));
        list.add(23, I18N.m15706(R.string.list_stand_up_comedy));
        list.add(24, "Standup");
        list.add(25, "Disney+ Movies");
        list.add(26, I18N.m15706(R.string.list_movie_oscars_winners));
        list.add(27, I18N.m15706(R.string.list_movie_christmas));
        list.add(28, I18N.m15706(R.string.list_movie_marvels));
        list.add(29, I18N.m15706(R.string.list_movie_disney_classics));
        return list;
    }
}
