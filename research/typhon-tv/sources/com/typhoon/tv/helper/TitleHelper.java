package com.typhoon.tv.helper;

import com.typhoon.tv.Logger;
import java.text.Normalizer;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;

public class TitleHelper {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m15966(String str) {
        return m15967(str);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m15967(String str) {
        try {
            str = Normalizer.normalize(str, Normalizer.Form.NFKD);
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        try {
            str = str.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        } catch (Throwable th2) {
            Logger.m6281(th2, new boolean[0]);
        }
        return str.replace("&quot;", "\"").replace("&amp;", "&").replace("‘", "'").replace("’", "'").replace("`", "'").replace("´", "'").replace("“", "\"").replace("”", "\"");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static String m15968(String str) {
        return m15967(str).replace(" (UK)", "").replace(" (US)", "").replace(" US", "").replace("The Americans (2013)", "The Americans").replace("Once Upon a Time (2011)", "Once Upon a Time").replace("Castle (2009)", "Castle").replace("Scandal (2012)", "Scandal").replace("National Geographic: ", "").replace("National Geographic:", "").replace("The Late Show with Stephen Colbert", "Late Show with Stephen Colbert");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m15969(String str) {
        if (str == null || str.isEmpty()) {
            return "";
        }
        String r0 = m15967(str);
        if (str.endsWith(StringUtils.SPACE) || str.endsWith(".")) {
            r0 = str.substring(0, str.length() - 1);
        }
        if (str.startsWith(StringUtils.SPACE)) {
            r0 = str.substring(1, str.length());
        }
        return r0.replace("\"", "").replace("'", "").replace("*", "").replace("?", "").replace("!", "").replace("%", "").replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "").replace("\\", "").replace("·", "-").replace(". ", "-").replace(" .", "-").replace(".", "-").replace(", ", "-").replace(" ,", "-").replace(",", "-").replace(": ", "-").replace(" :", "-").replace(":", "-").replace(StringUtils.SPACE, "-").replaceAll("--+", "-");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m15970(String str) {
        return m15967(m15968(m15966(str))).replace(" ! ", "").replace(" !", "").replace("! ", "").replace("!", "").replace(" ? ", "").replace(" ?", "").replace("? ", "").replace("?", "").replace(" # ", "").replace(" #", "").replace("# ", "").replace("#", "").replace(" / ", "").replace("/ ", "").replace(" /", "").replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "").replace(" % ", "").replace("% ", "").replace(" %", "").replace("%", "").replace(" & ", StringUtils.SPACE).replace("& ", StringUtils.SPACE).replace(" &", StringUtils.SPACE).replace("&", StringUtils.SPACE);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m15971(String str) {
        return m15967(str).replaceAll("\n|([\\[].+?[\\]])|([\\(].+?[\\)])|\\s(vs|v[.])\\s|(:|;|-|–|\"|,|·|'|_|\\.|\\?|!)|\\s", "").toLowerCase();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15972(String str) {
        return m15969(str).toLowerCase();
    }
}
