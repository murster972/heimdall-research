package com.typhoon.tv.helper;

import com.mopub.common.TyphoonApp;
import com.typhoon.tv.Logger;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

public class GoogleVideoHelper {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static HashMap<String, String> m15939(String str) {
        String str2;
        String decode;
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            ArrayList<String> arrayList = new ArrayList<>();
            if (!str.contains(",")) {
                if (str.contains("|")) {
                    arrayList.add(str);
                }
                return hashMap;
            }
            arrayList.addAll(new ArrayList(Arrays.asList(str.split(","))));
            for (String str3 : arrayList) {
                try {
                    if (str3.contains("|")) {
                        String[] split = str3.split("\\|");
                        String replace = split[0].replace("\\\\", "");
                        String str4 = split[1];
                        if (str4.trim().startsWith(TyphoonApp.HTTP)) {
                            try {
                                decode = URLDecoder.decode(str4.replace("\\u003d", "=").replace("\\u0026", "&"), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                decode = URLDecoder.decode(str2);
                            }
                            String r6 = m15949("=m" + replace);
                            if (r6.equals("HQ")) {
                                r6 = m15949(decode);
                            }
                            hashMap.put(decode, r6);
                        }
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            }
        } catch (Exception e3) {
            Logger.m6281((Throwable) e3, new boolean[0]);
        }
        return hashMap;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m15940(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        String replace = str.replace("/preview", "/edit");
        String str2 = "";
        String str3 = "";
        if ((replace.contains("drive.google") || replace.contains("docs.google")) && !replace.contains("/open?") && !replace.contains("/uc?")) {
            str2 = replace;
            str3 = m15951(str2);
        } else if (replace.contains("youtube") || replace.contains("youtu.be") || ((replace.contains(".google.") && replace.contains("/open?")) || ((replace.contains(".google.") && replace.contains("/uc?")) || (replace.contains(".google.") && replace.contains("/get_player") && replace.contains("docid"))))) {
            String r0 = Regex.m17800(replace, "docid=(.*?)(?:&|$)", 1);
            String trim = replace.trim();
            if (r0.isEmpty() && trim.contains("v=")) {
                r0 = Regex.m17800(trim, "v=(.*?)(?:&|$)", 1);
            }
            if (r0.isEmpty() && trim.contains("cid")) {
                r0 = Regex.m17800(trim, "cid=([\\w]+)", 1);
            }
            if (r0.isEmpty() && trim.contains("/open?")) {
                r0 = Regex.m17800(trim, "/open\\?.*?id=(.*?)\\s*(?:&|$)", 1);
            }
            if (r0.isEmpty() && trim.contains("/uc?")) {
                r0 = Regex.m17800(trim, "/uc\\?.*?id=(.*?)\\s*(?:&|$)", 1);
            }
            if (r0.isEmpty()) {
                return false;
            }
            str2 = String.format("https://drive.google.com/file/d/%s/edit", new Object[]{r0});
            str3 = r0;
        }
        if (str2.isEmpty() || str3.isEmpty()) {
            return false;
        }
        String r1 = HttpHelper.m6343().m6351(str2, (Map<String, String>[]) new Map[0]);
        String r5 = Regex.m17800(r1, "['\"]hl['\"]\\s*,\\s*['\"]([^'\"]+)", 1);
        if (r5.isEmpty()) {
            r5 = "en";
        }
        String r9 = Regex.m17800(r1, "['\"]ttsurl['\"]\\s*,\\s*['\"]([^'\"]+)", 1);
        if (r9.isEmpty()) {
            return false;
        }
        String r8 = Regex.m17801(StringEscapeUtils.unescapeJava(r9), "vid(?:=|\\\\u003d)(.*)", 1, 2);
        if (r8.isEmpty()) {
            return false;
        }
        return HttpHelper.m6343().m6358(String.format("https://drive.google.com/timedtext?id=%s&vid=%s&hl=%s&type=list&tlangs=1&v=%s&fmts=1&vssids=1", new Object[]{str3, r8, r5, str3}), str2).trim().toLowerCase().contains("<track");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static String m15941(String str) {
        try {
            String format = String.format("https://drive.google.com/file/d/%s/edit", new Object[]{str});
            String format2 = String.format("https://drive.google.com/uc?export=download&id=%s", new Object[]{str});
            String r7 = HttpHelper.m6343().m6362(format2, false, format);
            if (!HttpHelper.m6343().m6353(format2, r7)) {
                return r7;
            }
            String str2 = format2 + "&confirm=" + Regex.m17802(HttpHelper.m6343().m6358(format2, format).replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).replace("&amp;", "&"), "(?:\\?|&)confirm=(.*?)(?:&|'|\"|>|$)", 1, true);
            String r1 = HttpHelper.m6343().m6362(str2, false, format2);
            return (HttpHelper.m6343().m6353(format2, r1) || HttpHelper.m6343().m6353(str2, r1)) ? "" : r1;
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return "";
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static String m15942(String str) {
        try {
            if (!Regex.m17802(str, "(https?://)(\\d)(\\.bp\\.blogspot\\.com)(.*)", 3, true).isEmpty()) {
                return str.replaceAll("(https?://)(\\d)(\\.bp\\.blogspot\\.com)(.*)", "$1" + String.format("lh%s.googleusercontent.com", new Object[]{"$2"}) + "$4");
            } else if (Regex.m17802(str, "(https?://)(lh)(\\d)(\\.googleusercontent\\.com)(.*)", 4, true).isEmpty()) {
                return null;
            } else {
                return str.replaceAll("(https?://)(lh)(\\d)(\\.googleusercontent\\.com)(.*)", "$1" + String.format("%s.bp.blogspot.com", new Object[]{"$3"}) + "$5");
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return null;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static boolean m15943(String str) {
        String lowerCase = str.trim().toLowerCase();
        return m15955(lowerCase) && (lowerCase.contains("redirector.") || lowerCase.contains("googleusercontent") || lowerCase.contains(".bp.blogspot.com"));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static String m15944(String str) {
        String r1 = Regex.m17802(str, ".*?-apidata\\..*?google.*?\\.com/.*?storage/v1/b/(.*?)/o/(.*?)(?:$|\\?)", 1, true);
        String r2 = Regex.m17802(str, ".*?-apidata\\..*?google.*?\\.com/.*?storage/v1/b/(.*?)/o/(.*?)(?:$|\\?)", 2, true);
        if (r1.isEmpty() || r2.isEmpty()) {
            return null;
        }
        return String.format("https://www.googleapis.com/download/storage/v1/b/%s/o/%s?alt=media", new Object[]{r1, r2});
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static boolean m15945(String str) {
        String lowerCase = str.trim().toLowerCase();
        return lowerCase.contains("-apidata.") && lowerCase.contains("google");
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static boolean m15946(String str) {
        return !Regex.m17802(str, ".*?-apidata\\..*?google.*?\\.com/.*?storage/v1/b/(.*?)/o/(.*?)(?:$|\\?)", 1, true).isEmpty();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static boolean m15947(String str) {
        return !Regex.m17802(str, "(https?://)(\\d)(\\.bp\\.blogspot\\.com)(.*)", 3, true).isEmpty() || !Regex.m17802(str, "(https?://)(lh)(\\d)(\\.googleusercontent\\.com)(.*)", 4, true).isEmpty();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static HashMap<String, String> m15948(String str) {
        if (str == null || str.isEmpty()) {
            return new HashMap<>();
        }
        String replace = str.replace("/preview", "/edit");
        String r4 = HttpHelper.m6343().m6351(replace, (Map<String, String>[]) new Map[0]);
        if ((replace.contains("drive.google") || replace.contains("docs.google")) && !replace.contains("/open?") && !replace.contains("/uc?")) {
            return m15950(replace, r4);
        }
        if (replace.contains("youtube") || replace.contains("youtu.be") || ((replace.contains(".google.") && replace.contains("/open?")) || ((replace.contains(".google.") && replace.contains("/uc?")) || (replace.contains(".google.") && replace.contains("/get_player") && replace.contains("docid"))))) {
            String r0 = Regex.m17800(replace, "docid=(.*?)(?:&|$)", 1);
            String trim = replace.trim();
            if (r0.isEmpty() && trim.contains("v=")) {
                r0 = Regex.m17800(trim, "v=(.*?)(?:&|$)", 1);
            }
            if (r0.isEmpty() && trim.contains("cid")) {
                r0 = Regex.m17800(trim, "cid=([\\w]+)", 1);
            }
            if (r0.isEmpty() && trim.contains("/open?")) {
                r0 = Regex.m17800(trim, "/open\\?.*?id=(.*?)\\s*(?:&|$)", 1);
            }
            if (r0.isEmpty() && trim.contains("/uc?")) {
                r0 = Regex.m17800(trim, "/uc\\?.*?id=(.*?)\\s*(?:&|$)", 1);
            }
            if (r0.isEmpty()) {
                return new HashMap<>();
            }
            String format = String.format("https://drive.google.com/file/d/%s/edit", new Object[]{r0});
            return m15950(format, HttpHelper.m6343().m6351(format, (Map<String, String>[]) new Map[0]));
        }
        if (replace.contains("picasaweb")) {
        }
        return new HashMap<>();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m15949(String str) {
        if (str == null || str.isEmpty()) {
            return "HQ";
        }
        String r1 = m15956(str);
        char c = 65535;
        switch (r1.hashCode()) {
            case 53:
                if (r1.equals("5")) {
                    c = ':';
                    break;
                }
                break;
            case 1444:
                if (r1.equals("-1")) {
                    c = 1;
                    break;
                }
                break;
            case 1572:
                if (r1.equals("15")) {
                    c = 30;
                    break;
                }
                break;
            case 1574:
                if (r1.equals("17")) {
                    c = '@';
                    break;
                }
                break;
            case 1575:
                if (r1.equals("18")) {
                    c = '6';
                    break;
                }
                break;
            case 1600:
                if (r1.equals("22")) {
                    c = 20;
                    break;
                }
                break;
            case 1633:
                if (r1.equals("34")) {
                    c = '7';
                    break;
                }
                break;
            case 1634:
                if (r1.equals("35")) {
                    c = '.';
                    break;
                }
                break;
            case 1636:
                if (r1.equals("37")) {
                    c = 10;
                    break;
                }
                break;
            case 1637:
                if (r1.equals("38")) {
                    c = 6;
                    break;
                }
                break;
            case 1663:
                if (r1.equals("43")) {
                    c = '3';
                    break;
                }
                break;
            case 1664:
                if (r1.equals("44")) {
                    c = '%';
                    break;
                }
                break;
            case 1665:
                if (r1.equals("45")) {
                    c = 28;
                    break;
                }
                break;
            case 1666:
                if (r1.equals("46")) {
                    c = 18;
                    break;
                }
                break;
            case 1700:
                if (r1.equals("59")) {
                    c = '-';
                    break;
                }
                break;
            case 1761:
                if (r1.equals("78")) {
                    c = '\'';
                    break;
                }
                break;
            case 1786:
                if (r1.equals("82")) {
                    c = '0';
                    break;
                }
                break;
            case 1787:
                if (r1.equals("83")) {
                    c = '#';
                    break;
                }
                break;
            case 1788:
                if (r1.equals("84")) {
                    c = 21;
                    break;
                }
                break;
            case 1789:
                if (r1.equals("85")) {
                    c = 19;
                    break;
                }
                break;
            case 1816:
                if (r1.equals("91")) {
                    c = '?';
                    break;
                }
                break;
            case 1817:
                if (r1.equals("92")) {
                    c = ';';
                    break;
                }
                break;
            case 1818:
                if (r1.equals("93")) {
                    c = '1';
                    break;
                }
                break;
            case 1819:
                if (r1.equals("94")) {
                    c = '&';
                    break;
                }
                break;
            case 1820:
                if (r1.equals("95")) {
                    c = 25;
                    break;
                }
                break;
            case 1821:
                if (r1.equals("96")) {
                    c = 15;
                    break;
                }
                break;
            case 48625:
                if (r1.equals("100")) {
                    c = '5';
                    break;
                }
                break;
            case 48626:
                if (r1.equals("101")) {
                    c = ',';
                    break;
                }
                break;
            case 48627:
                if (r1.equals("102")) {
                    c = 29;
                    break;
                }
                break;
            case 48687:
                if (r1.equals("120")) {
                    c = 24;
                    break;
                }
                break;
            case 48688:
                if (r1.equals("121")) {
                    c = 11;
                    break;
                }
                break;
            case 48720:
                if (r1.equals("132")) {
                    c = '<';
                    break;
                }
                break;
            case 48721:
                if (r1.equals("133")) {
                    c = '9';
                    break;
                }
                break;
            case 48722:
                if (r1.equals("134")) {
                    c = '/';
                    break;
                }
                break;
            case 48723:
                if (r1.equals("135")) {
                    c = '\"';
                    break;
                }
                break;
            case 48724:
                if (r1.equals("136")) {
                    c = 22;
                    break;
                }
                break;
            case 48725:
                if (r1.equals("137")) {
                    c = 12;
                    break;
                }
                break;
            case 48781:
                if (r1.equals("151")) {
                    c = 'A';
                    break;
                }
                break;
            case 48811:
                if (r1.equals("160")) {
                    c = '=';
                    break;
                }
                break;
            case 48818:
                if (r1.equals("167")) {
                    c = '2';
                    break;
                }
                break;
            case 48819:
                if (r1.equals("168")) {
                    c = '+';
                    break;
                }
                break;
            case 48820:
                if (r1.equals("169")) {
                    c = 31;
                    break;
                }
                break;
            case 48842:
                if (r1.equals("170")) {
                    c = CharUtils.CR;
                    break;
                }
                break;
            case 49619:
                if (r1.equals("212")) {
                    c = ' ';
                    break;
                }
                break;
            case 49625:
                if (r1.equals("218")) {
                    c = '$';
                    break;
                }
                break;
            case 49626:
                if (r1.equals("219")) {
                    c = '!';
                    break;
                }
                break;
            case 49712:
                if (r1.equals("242")) {
                    c = '8';
                    break;
                }
                break;
            case 49713:
                if (r1.equals("243")) {
                    c = '4';
                    break;
                }
                break;
            case 49714:
                if (r1.equals("244")) {
                    c = ')';
                    break;
                }
                break;
            case 49715:
                if (r1.equals("245")) {
                    c = '(';
                    break;
                }
                break;
            case 49716:
                if (r1.equals("246")) {
                    c = '*';
                    break;
                }
                break;
            case 49717:
                if (r1.equals("247")) {
                    c = 26;
                    break;
                }
                break;
            case 49718:
                if (r1.equals("248")) {
                    c = 16;
                    break;
                }
                break;
            case 49776:
                if (r1.equals("264")) {
                    c = 7;
                    break;
                }
                break;
            case 49778:
                if (r1.equals("266")) {
                    c = 2;
                    break;
                }
                break;
            case 49804:
                if (r1.equals("271")) {
                    c = 8;
                    break;
                }
                break;
            case 49805:
                if (r1.equals("272")) {
                    c = 3;
                    break;
                }
                break;
            case 49811:
                if (r1.equals("278")) {
                    c = '>';
                    break;
                }
                break;
            case 49873:
                if (r1.equals("298")) {
                    c = 23;
                    break;
                }
                break;
            case 49874:
                if (r1.equals("299")) {
                    c = 14;
                    break;
                }
                break;
            case 50549:
                if (r1.equals("302")) {
                    c = 27;
                    break;
                }
                break;
            case 50550:
                if (r1.equals("303")) {
                    c = 17;
                    break;
                }
                break;
            case 50555:
                if (r1.equals("308")) {
                    c = 9;
                    break;
                }
                break;
            case 50581:
                if (r1.equals("313")) {
                    c = 4;
                    break;
                }
                break;
            case 50583:
                if (r1.equals("315")) {
                    c = 5;
                    break;
                }
                break;
            case 54395385:
                if (r1.equals("99999")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return "HD";
            case 1:
                return "HQ";
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return "4K";
            case 7:
            case 8:
            case 9:
                return "2K";
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                return "1080p";
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
                return "720p";
            case ' ':
            case '!':
            case '\"':
            case '#':
            case '$':
            case '%':
            case '&':
            case '\'':
            case '(':
            case ')':
            case '*':
            case '+':
            case ',':
            case '-':
            case '.':
                return "480p";
            case '/':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                return "360p";
            case '8':
            case '9':
            case ':':
            case ';':
            case '<':
                return "240p";
            case '=':
            case '>':
            case '?':
            case '@':
                return "144p";
            case 'A':
                return "72p";
            default:
                return "HQ";
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static HashMap<String, String> m15950(String str, String str2) {
        String str3 = str + "";
        if (!str3.trim().toLowerCase().startsWith(TyphoonApp.HTTP)) {
            str3 = "https://" + str3;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            ArrayList<ArrayList<String>> r13 = Regex.m17803(str2, "\\[\\s*\"([^\"]+)\"\\s*,\\s*\"([^\"]+)\"\\s*\\]", 2);
            List list = r13.get(0);
            List list2 = r13.get(1);
            int i = 0;
            while (i < list.size()) {
                try {
                    String str4 = (String) list.get(i);
                    String str5 = i < list2.size() ? (String) list2.get(i) : null;
                    if (str5 != null && !str5.isEmpty() && str4.equals("fmt_stream_map")) {
                        hashMap.putAll(m15939(str5));
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                i++;
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        String r4 = m15951(str3);
        if (hashMap.isEmpty() && !r4.isEmpty()) {
            try {
                String r9 = HttpHelper.m6343().m6358("https://docs.google.com/get_video_info?docid={docId}&authuser=2&eurl=https://www.youtube.com/embed%2F?status=ok&allow_embed=1&controls=0&ps=docs&partnerid=30&autoplay=0&showinfo=0&public=false&enab&playerretry=2".replace("{docId}", r4), str3);
                if (!r9.isEmpty()) {
                    try {
                        r9 = URLDecoder.decode(r9, "UTF-8");
                    } catch (Exception e3) {
                        Logger.m6281((Throwable) e3, new boolean[0]);
                        try {
                            r9 = URLDecoder.decode(r9);
                        } catch (Exception e4) {
                            Logger.m6281((Throwable) e4, new boolean[0]);
                        }
                    }
                    String r7 = Regex.m17801(r9, "fmt_stream_map=(.*?)&url_encoded_fmt_stream_map", 1, 34);
                    if (!r7.isEmpty()) {
                        hashMap.putAll(m15939(r7));
                    }
                }
            } catch (Exception e5) {
                Logger.m6281((Throwable) e5, new boolean[0]);
            }
        }
        if (!r4.isEmpty()) {
            try {
                String r15 = m15941(r4);
                if (!r15.isEmpty()) {
                    hashMap.put(r15, "HD");
                }
            } catch (Exception e6) {
                Logger.m6281((Throwable) e6, new boolean[0]);
            }
        }
        if (!r4.isEmpty()) {
            try {
                hashMap.put(String.format("https://www.googleapis.com/drive/v3/files/%s?alt=media&key=AIzaSyBXV3qGJ2rwDaxvUmAzaVpZMmn1t6PyU0E", new Object[]{r4}), "HD");
                hashMap.put(String.format("https://www.googleapis.com/drive/v3/files/%s?alt=media&key=AIzaSyDPBU51cTvGzh8k8OwUxOS8D_fibFDoNAI", new Object[]{r4}), "HD");
            } catch (Exception e7) {
                Logger.m6281((Throwable) e7, new boolean[0]);
            }
        }
        return hashMap;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m15951(String str) {
        String str2;
        if (str != null) {
            try {
                if (!str.isEmpty()) {
                    if (!str.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                        str = str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                    }
                    try {
                        Map<String, String> r3 = Utils.m6418(new URL(str));
                        if (r3.containsKey("id") && (str2 = r3.get("id")) != null && !str2.isEmpty()) {
                            return str2;
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                    String r0 = Regex.m17800(str, "/d/([^/]{20,40})/", 1);
                    if (r0.isEmpty()) {
                        r0 = Regex.m17800(str, "/d/([^/]+)/", 1);
                    }
                    if (r0.isEmpty()) {
                        r0 = Regex.m17800(str, "google.+?([a-zA-Z0-9-_]{25,})", 1);
                    }
                    return r0;
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return "";
            }
        }
        return "";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static List<MediaSource> m15952(String str, String str2) {
        List<ResolveResult> r12;
        ArrayList arrayList = new ArrayList();
        if (m15955(str) && str.contains("docs") && str.contains("*/")) {
            try {
                String r3 = Regex.m17800(str, "\\*/(.*?)(?:$|\\?)", 1);
                if (!r3.isEmpty()) {
                    String format = String.format("https://drive.google.com/file/d/%s/edit", new Object[]{r3});
                    if (m15953(format)) {
                        try {
                            if (RealDebridCredentialsHelper.m15838().isValid() && (r12 = RealDebridUserApi.m15845().m15850(format, "GoogleVideo-DEB")) != null && !r12.isEmpty()) {
                                for (ResolveResult next : r12) {
                                    String resolvedLink = next.getResolvedLink();
                                    String resolvedQuality = next.getResolvedQuality();
                                    if (resolvedQuality.isEmpty()) {
                                        resolvedQuality = "HD";
                                    }
                                    MediaSource mediaSource = new MediaSource(str2, "GoogleVideo-DEB", false);
                                    mediaSource.setStreamLink(resolvedLink);
                                    mediaSource.setQuality(resolvedQuality);
                                    if (next.getPlayHeader() != null) {
                                        mediaSource.setPlayHeader(next.getPlayHeader());
                                    }
                                    arrayList.add(mediaSource);
                                }
                            }
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        HashMap<String, String> r16 = m15948(format);
                        if (r16 != null && !r16.isEmpty()) {
                            for (Map.Entry next2 : r16.entrySet()) {
                                MediaSource mediaSource2 = new MediaSource(str2, "GoogleVideo", false);
                                mediaSource2.setStreamLink((String) next2.getKey());
                                mediaSource2.setQuality(((String) next2.getValue()).isEmpty() ? "HD" : (String) next2.getValue());
                                HashMap hashMap = new HashMap();
                                hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, com.typhoon.tv.TyphoonApp.f5835);
                                hashMap.put("Cookie", m15954(format, (String) next2.getKey()));
                                mediaSource2.setPlayHeader(hashMap);
                                arrayList.add(mediaSource2);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m15953(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "");
        if ((!replace.contains("drive.google") || !replace.contains("/file/d/")) && ((!replace.contains("docs.google") || !replace.contains("/file/d/")) && ((!replace.contains(".google.") || !replace.contains("/open?")) && ((!replace.contains(".google.") || !replace.contains("/uc?")) && !replace.contains("youtube.googleapis"))))) {
            if (replace.contains("youtube") || replace.contains("youtu.be")) {
                return replace.contains("type=docs") || replace.contains("docid");
            }
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15954(String str, String str2) {
        String r3 = HttpHelper.m6343().m6349(str);
        String r4 = str2 != null ? HttpHelper.m6343().m6349(str2) : null;
        String str3 = "";
        String str4 = "";
        if (!r3.isEmpty()) {
            str3 = r3;
            str4 = r4;
        } else if (r4 != null && !r4.isEmpty()) {
            str3 = r4;
            str4 = r3;
        }
        String str5 = str3.isEmpty() ? "" : str3 + "; " + str4;
        String r2 = HttpHelper.m6343().m6349("docs.google.com");
        if (!r2.isEmpty()) {
            if (!str5.isEmpty()) {
                r2 = "; " + r2;
            }
            str5 = str5 + r2;
        }
        return str5.contains(";;") ? str5.replace(";;", ";") : str5;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15955(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        String lowerCase = str.trim().toLowerCase();
        return lowerCase.contains("google") || lowerCase.contains("picasa") || lowerCase.contains("blogspot") || lowerCase.contains("youtube.com/videoplayback") || lowerCase.contains("youtu.be/videoplayback");
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static String m15956(String str) {
        if (str == null || str.isEmpty()) {
            return "-1";
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(Regex.m17800(str, "itag=(\\d+)", 1));
        arrayList.add(Regex.m17800(str, "itag%3D(\\d+)", 1));
        arrayList.add(Regex.m17800(str, "=m(\\d+)$", 1));
        arrayList.add(Regex.m17800(str, "%3Dm(\\d+)$", 1));
        arrayList.add(Regex.m17800(str, "=m(\\d+)", 1));
        arrayList.add(Regex.m17800(str, "%3Dm(\\d+)", 1));
        arrayList.add(Regex.m17800(str, "\\/m(\\d+?)\\/", 1));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add("");
        arrayList.removeAll(arrayList2);
        return (!arrayList.isEmpty() || !m15955(str)) ? arrayList.isEmpty() ? "-1" : (String) arrayList.get(0) : "99999";
    }
}
