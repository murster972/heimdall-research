package com.typhoon.tv.helper;

import com.typhoon.tv.model.media.MediaInfo;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class TyphoonTV {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f12629 = {"(.*?){delim}(\\d{4}){delim}.*?(\\d{3,4})p{delim}(.*)", "(.*?){delim}(\\d{3,4})p{delim}.*?(\\d{4}){delim}(.*)", "(.*?){delim}(\\d{4}){delim}(.*)", "(.*?){delim}(\\d{3,4})p{delim}(.*)", "(.*)(\\.[A-Z\\d]{3}$)", "(.*)"};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f12630 = {"(.*?){delim}S(\\d+){delim}*E(\\d+)(?:E\\d+)*.*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}(\\d+)x(\\d+)(?:-\\d+)*.*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}SEASON{delim}*(\\d+){delim}*EPISODE{delim}*(\\d+).*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}\\[S(\\d+)\\]{delim}*\\[E(\\d+)(?:E\\d+)*\\].*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}S(\\d+){delim}*EP(\\d+)(?:EP\\d+)*.*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}S(\\d+){delim}*E(\\d+)(?:E\\d+)*{delim}?(.*)", "(.*?){delim}(\\d+)x(\\d+)(?:-\\d+)*{delim}?(.*)", "(.*?){delim}SEASON{delim}*(\\d+){delim}*EPISODE{delim}*(\\d+){delim}?(.*)", "(.*?){delim}\\[S(\\d+)\\]{delim}*\\[E(\\d+)(?:E\\d+)*\\]{delim}?(.*)", "(.*?){delim}S(\\d+){delim}*EP(\\d+)(?:E\\d+)*{delim}?(.*)", "(.*?){delim}(\\d{3,4})p{delim}?(.*)", "(.*)"};

    /* renamed from: 齉  reason: contains not printable characters */
    private String f12631;

    public TyphoonTV() {
        this("\\s*<a\\s+href=\"([^\"]+)\">([^<]+)</a>");
    }

    public TyphoonTV(String rowPattern) {
        this.f12631 = rowPattern;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ParsedLinkModel m15980(String link) {
        return m15978(link, f12630, 0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ParsedLinkModel m15979(String link) {
        return m15978(link, f12629, 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ParsedLinkModel m15978(String link, String[] patterns, int type) {
        String fileName;
        String fileName2 = link;
        try {
            fileName = URLDecoder.decode(fileName2, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            fileName = URLDecoder.decode(fileName2);
        }
        if (fileName.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            String[] arr = fileName.split("\\/");
            if (arr.length > 1) {
                fileName = arr[arr.length - 1];
            }
        }
        ParsedLinkModel parsedLinkModel = null;
        for (int i = 0; i < patterns.length; i++) {
            Matcher m = Pattern.compile(patterns[i].replace("{delim}", "[._ -]"), 34).matcher(fileName);
            int groupCount = m.groupCount();
            while (m.find()) {
                if (m.matches()) {
                    if (type == 0) {
                        if (groupCount == 5) {
                            String title = m.group(1);
                            String season = m.group(2);
                            String episode = m.group(3);
                            String quality = m.group(4);
                            String extra = m.group(5);
                            parsedLinkModel = new ParsedLinkModel(type, title);
                            parsedLinkModel.m15987(Integer.parseInt(season));
                            parsedLinkModel.m15991(Integer.parseInt(episode));
                            parsedLinkModel.m15994(quality);
                            parsedLinkModel.m15988(extra);
                        } else if (groupCount == 4) {
                            String title2 = m.group(1);
                            String season2 = m.group(2);
                            String episode2 = m.group(3);
                            String extra2 = m.group(4);
                            parsedLinkModel = new ParsedLinkModel(type, title2);
                            parsedLinkModel.m15987(Integer.parseInt(season2));
                            parsedLinkModel.m15991(Integer.parseInt(episode2));
                            parsedLinkModel.m15988(extra2);
                        } else if (groupCount == 3) {
                            String title3 = m.group(1);
                            String quality2 = m.group(2);
                            String extra3 = m.group(3);
                            parsedLinkModel = new ParsedLinkModel(type, title3);
                            parsedLinkModel.m15994(quality2);
                            parsedLinkModel.m15988(extra3);
                        } else if (groupCount == 1) {
                            parsedLinkModel = new ParsedLinkModel(type, m.group(1));
                        }
                    } else if (type == 1) {
                        if (i == 0) {
                            String title4 = m.group(1);
                            String year = m.group(2);
                            String quality3 = m.group(3);
                            String extra4 = m.group(4);
                            parsedLinkModel = new ParsedLinkModel(type, title4);
                            parsedLinkModel.m15993(Integer.parseInt(year));
                            parsedLinkModel.m15994(quality3);
                            parsedLinkModel.m15988(extra4);
                        } else if (i == 1) {
                            String title5 = m.group(1);
                            String quality4 = m.group(2);
                            String year2 = m.group(3);
                            String extra5 = m.group(4);
                            parsedLinkModel = new ParsedLinkModel(type, title5);
                            parsedLinkModel.m15994(quality4);
                            parsedLinkModel.m15993(Integer.parseInt(year2));
                            parsedLinkModel.m15988(extra5);
                        } else if (i == 2) {
                            String title6 = m.group(1);
                            String year3 = m.group(2);
                            String extra6 = m.group(3);
                            parsedLinkModel = new ParsedLinkModel(type, title6);
                            parsedLinkModel.m15993(Integer.parseInt(year3));
                            parsedLinkModel.m15988(extra6);
                        } else if (i == 3) {
                            String title7 = m.group(1);
                            String quality5 = m.group(2);
                            String extra7 = m.group(3);
                            parsedLinkModel = new ParsedLinkModel(type, title7);
                            parsedLinkModel.m15994(quality5);
                            parsedLinkModel.m15988(extra7);
                        } else if (i >= 4) {
                            parsedLinkModel = new ParsedLinkModel(type, m.group(1));
                        }
                    }
                    ParsedLinkModel parsedLinkModel2 = parsedLinkModel;
                    return parsedLinkModel;
                }
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m15982(ParsedLinkModel parsedLinkModel, MediaInfo mediaInfo, String season, String episode) {
        return m15981(parsedLinkModel, mediaInfo, Integer.parseInt(season), Integer.parseInt(episode));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m15981(ParsedLinkModel parsedLinkModel, MediaInfo mediaInfo, int season, int episode) {
        boolean isYearMatch;
        boolean isSeasonEpisodeMatch;
        boolean z = true;
        if (parsedLinkModel == null) {
            return false;
        }
        boolean isTitleMatch = TitleHelper.m15971(mediaInfo.getName()).equals(TitleHelper.m15971(parsedLinkModel.m15992()));
        if (parsedLinkModel.m15986() <= 0 || mediaInfo.getYear() <= 0 || parsedLinkModel.m15986() == mediaInfo.getYear()) {
            isYearMatch = true;
        } else {
            isYearMatch = false;
        }
        if (mediaInfo.getType() != 0 || (season == parsedLinkModel.m15990() && episode == parsedLinkModel.m15989())) {
            isSeasonEpisodeMatch = true;
        } else {
            isSeasonEpisodeMatch = false;
        }
        if (!isTitleMatch || !isYearMatch || !isSeasonEpisodeMatch) {
            z = false;
        }
        return z;
    }

    public static class ParsedLinkModel {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f12632 = "HQ";

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f12633 = "";

        /* renamed from: 连任  reason: contains not printable characters */
        private int f12634 = -1;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f12635;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f12636 = -1;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f12637 = -1;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f12638;

        public ParsedLinkModel(int type, String title) {
            this.f12638 = type;
            this.f12635 = title;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m15992() {
            return this.f12635;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m15986() {
            return this.f12637;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m15993(int year) {
            this.f12637 = year;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m15990() {
            return this.f12636;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m15987(int season) {
            this.f12636 = season;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public int m15989() {
            return this.f12634;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m15991(int episode) {
            this.f12634 = episode;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public String m15985() {
            if (this.f12633 != null && !this.f12632.equalsIgnoreCase("4K") && (this.f12633.trim().toLowerCase().contains("4k") || this.f12633.trim().toLowerCase().contains("ultrahd"))) {
                this.f12632 = "4K";
            } else if (this.f12633 != null && this.f12632.equalsIgnoreCase("HQ") && (this.f12633.trim().toLowerCase().contains("brrip") || this.f12633.trim().toLowerCase().contains("bdrip") || this.f12633.trim().toLowerCase().contains("web-dl"))) {
                this.f12632 = "HD";
            }
            return this.f12632;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m15994(String quality) {
            this.f12632 = quality;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public String m15983() {
            return this.f12633;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m15988(String extra) {
            this.f12633 = extra;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m15984() {
            String e = this.f12633.trim().toLowerCase();
            if (e.contains("dub") || e.contains("3d") || e.contains("cam") || e.contains("ts") || e.contains("tc") || e.contains("sample") || e.contains("part") || e.contains(".zip") || e.contains(".7z") || e.contains(".rar") || e.contains(".jpg") || e.contains(".png") || e.contains(".srt") || e.contains(".ass") || e.contains(".vtx") || e.contains(".txt") || e.contains(".exe") || e.contains(".mp3") || e.contains(".iso")) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ParsedLinkModel{type=" + this.f12638 + ", title='" + this.f12635 + '\'' + ", year=" + this.f12637 + ", season=" + this.f12636 + ", episode=" + this.f12634 + ", quality='" + this.f12632 + '\'' + ", extra='" + this.f12633 + '\'' + '}';
        }
    }
}
