package com.typhoon.tv.helper;

import com.typhoon.tv.utils.Regex;

public class LimelightNetworksHelper {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15957(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        return !Regex.m17801(str.trim().toLowerCase(), "(\\.llnw[a-zA-Z0-9_]\\.net)", 1, 34).isEmpty();
    }
}
