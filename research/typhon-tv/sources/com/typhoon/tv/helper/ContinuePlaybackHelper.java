package com.typhoon.tv.helper;

import com.typhoon.tv.I18N;
import com.typhoon.tv.R;

public class ContinuePlaybackHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] f12621;

    /* renamed from: 龘  reason: contains not printable characters */
    public static String[] m15931() {
        if (f12621 == null) {
            f12621 = new String[]{I18N.m15706(R.string.option_player_default), I18N.m15706(R.string.option_always_ask), I18N.m15706(R.string.option_start_over), I18N.m15706(R.string.option_resume)};
        }
        return f12621;
    }
}
