package com.typhoon.tv.helper;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import java.util.HashMap;
import java.util.Iterator;

public class GkPluginsHelper {

    private static class GkLink {
        private String label;
        private String link;
        private String type;

        private GkLink() {
        }

        public String getLabel() {
            return this.label;
        }

        public String getLink() {
            return this.link;
        }

        public String getType() {
            return this.type;
        }

        public void setLabel(String str) {
            this.label = str;
        }

        public void setLink(String str) {
            this.link = str;
        }

        public void setType(String str) {
            this.type = str;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static HashMap<String, String> m15938(String str) {
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            JsonElement parse = new JsonParser().parse(str);
            if (parse != null) {
                try {
                    if (parse.isJsonObject()) {
                        JsonElement jsonElement = parse.getAsJsonObject().get("link");
                        if (jsonElement != null && !jsonElement.isJsonNull()) {
                            if (jsonElement.isJsonArray()) {
                                Iterator<JsonElement> it2 = jsonElement.getAsJsonArray().iterator();
                                while (it2.hasNext()) {
                                    GkLink gkLink = (GkLink) new Gson().fromJson(it2.next(), GkLink.class);
                                    if (gkLink.getLink() != null && !gkLink.getLink().isEmpty()) {
                                        String link = gkLink.getLink();
                                        String str2 = "";
                                        if (gkLink.getLabel() != null && !gkLink.getLabel().isEmpty()) {
                                            str2 = gkLink.getLabel().replace("P", TtmlNode.TAG_P);
                                        }
                                        if (str2.isEmpty()) {
                                            str2 = GoogleVideoHelper.m15955(link) ? GoogleVideoHelper.m15949(link) : "HQ";
                                        }
                                        hashMap.put(link, str2);
                                    }
                                }
                            } else if (jsonElement.getAsString() != null) {
                                hashMap.put(jsonElement.getAsString(), "HQ");
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        return hashMap;
    }
}
