package com.typhoon.tv.helper;

import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import java.util.ArrayList;
import java.util.List;

public class PlayActionHelper {
    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m15963(boolean z) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(I18N.m15706(R.string.always_ask));
        if (z) {
            arrayList.add(I18N.m15706(R.string.action_cast));
        } else {
            arrayList.add(I18N.m15706(R.string.action_play));
        }
        if (z) {
            arrayList.add(I18N.m15706(R.string.action_cast_with_subs));
        } else {
            arrayList.add(I18N.m15706(R.string.action_play_with_subs));
        }
        arrayList.add(I18N.m15706(R.string.action_play_via_other_players));
        return arrayList;
    }
}
