package com.typhoon.tv.helper;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.utils.Utils;
import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;

public class MediaPosterUrlCacheHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile MediaPosterUrlCacheHelper f12623;

    /* renamed from: 靐  reason: contains not printable characters */
    private DualCache<String> f12624;

    public MediaPosterUrlCacheHelper() {
        try {
            long r2 = Utils.m6396(TVApplication.m6288().getCacheDir());
            this.f12624 = new Builder("media_poster_url_cache", Utils.m6393()).noRam().useSerializerInDisk(r2 > 2147483647L ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : (int) r2, true, new CacheSerializer<String>() {
                /* renamed from: 靐  reason: contains not printable characters */
                public String toString(String str) {
                    return str;
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public String fromString(String str) {
                    return str;
                }
            }, TVApplication.m6288()).build();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaPosterUrlCacheHelper m15958() {
        MediaPosterUrlCacheHelper mediaPosterUrlCacheHelper = f12623;
        if (mediaPosterUrlCacheHelper == null) {
            synchronized (MediaPosterUrlCacheHelper.class) {
                try {
                    mediaPosterUrlCacheHelper = f12623;
                    if (mediaPosterUrlCacheHelper == null) {
                        MediaPosterUrlCacheHelper mediaPosterUrlCacheHelper2 = new MediaPosterUrlCacheHelper();
                        try {
                            f12623 = mediaPosterUrlCacheHelper2;
                            mediaPosterUrlCacheHelper = mediaPosterUrlCacheHelper2;
                        } catch (Throwable th) {
                            th = th;
                            MediaPosterUrlCacheHelper mediaPosterUrlCacheHelper3 = mediaPosterUrlCacheHelper2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return mediaPosterUrlCacheHelper;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized String m15959(int i, int i2, String str) {
        String str2;
        if (this.f12624 == null) {
            str2 = null;
        } else {
            str2 = null;
            String str3 = "";
            String str4 = "";
            if (i > -1) {
                try {
                    str3 = "tmdb-" + i;
                    str4 = this.f12624.get(str3);
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            if ((str4 == null || str4.isEmpty()) && i2 > -1) {
                str3 = "tvdb-" + i2;
                str4 = this.f12624.get(str3);
            }
            if ((str4 == null || str4.isEmpty()) && str != null && !str.isEmpty()) {
                str3 = "imdb-" + str;
                str4 = this.f12624.get(str3);
            }
            if (str4 != null && !str4.isEmpty()) {
                if (str4.contains("|")) {
                    try {
                        String[] split = str4.split("\\|");
                        String str5 = split[0];
                        String str6 = split[1];
                        String r4 = DateTimeHelper.m15934();
                        boolean z = false;
                        try {
                            if (!str6.isEmpty() && !r4.isEmpty()) {
                                if (Long.parseLong(r4) - Long.parseLong(str6) >= 2592000000L) {
                                    z = true;
                                }
                            }
                        } catch (Exception e2) {
                            Logger.m6281((Throwable) e2, new boolean[0]);
                        }
                        if (z) {
                            this.f12624.delete(str3);
                            str2 = null;
                        } else {
                            str2 = str5;
                        }
                    } catch (Exception e3) {
                        Logger.m6281((Throwable) e3, new boolean[0]);
                    }
                } else {
                    str2 = str4;
                }
            }
            if (str2 != null) {
                if (str2.contains("~%7C~")) {
                    str2 = str2.replace("~%7C~", "|");
                }
            }
        }
        return str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m15960(String str, int i, int i2, String str2) {
        if (!(this.f12624 == null || str == null)) {
            if (!str.isEmpty()) {
                try {
                    if (str.contains("|")) {
                        str = str.replace("|", "~%7C~");
                    }
                    String str3 = str + "|" + DateTimeHelper.m15934();
                    if (i > -1) {
                        try {
                            this.f12624.put("tmdb-" + i, str3);
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    } else if (i2 > -1) {
                        this.f12624.put("tvdb-" + i2, str3);
                    } else if (str2 != null && !str2.isEmpty()) {
                        this.f12624.put("imdb-" + str2, str3);
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            }
        }
        return;
    }
}
