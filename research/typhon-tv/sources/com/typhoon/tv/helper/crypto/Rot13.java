package com.typhoon.tv.helper.crypto;

import com.typhoon.tv.Logger;
import org.apache.commons.lang3.CharUtils;

public class Rot13 {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m16009(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if (charAt >= 'a' && charAt <= 'm') {
                    charAt = (char) (charAt + CharUtils.CR);
                } else if (charAt >= 'A' && charAt <= 'M') {
                    charAt = (char) (charAt + CharUtils.CR);
                } else if (charAt >= 'n' && charAt <= 'z') {
                    charAt = (char) (charAt - 13);
                } else if (charAt >= 'N' && charAt <= 'Z') {
                    charAt = (char) (charAt - 13);
                }
                sb.append(charAt);
            }
            return sb.toString();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return "";
        }
    }
}
