package com.typhoon.tv.helper.crypto;

public class XORCipher {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m16010(String str, String str2) {
        char[] charArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str2.length(); i++) {
            sb.append((char) (str2.charAt(i) ^ charArray[i % charArray.length]));
        }
        return sb.toString();
    }
}
