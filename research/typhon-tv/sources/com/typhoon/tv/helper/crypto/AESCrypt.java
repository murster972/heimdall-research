package com.typhoon.tv.helper.crypto;

import android.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESCrypt {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m16005(String str, String str2, String str3) throws Exception {
        byte[] decode = Base64.decode(str2.getBytes(str3), 0);
        byte[] copyOfRange = Arrays.copyOfRange(decode, 8, 16);
        byte[] copyOfRange2 = Arrays.copyOfRange(decode, 16, decode.length);
        byte[] bArr = new byte[32];
        byte[] bArr2 = new byte[16];
        m16008(str.getBytes(str3), 256, 128, copyOfRange, bArr, bArr2);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
        return new String(instance.doFinal(copyOfRange2), str3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m16006(String str, String str2, String str3) throws Exception {
        byte[] decode = Base64.decode(str2.getBytes(str3), 0);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(new byte[16]);
        SecretKeySpec secretKeySpec = new SecretKeySpec(str.getBytes(str3), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, secretKeySpec, ivParameterSpec);
        try {
            return new String(instance.doFinal(decode), str3);
        } catch (BadPaddingException e) {
            return new String(instance.doFinal(decode), str3);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m16007(byte[] bArr, int i, int i2, byte[] bArr2, int i3, String str, byte[] bArr3, byte[] bArr4) throws NoSuchAlgorithmException {
        int i4 = i / 32;
        int i5 = i2 / 32;
        int i6 = i4 + i5;
        byte[] bArr5 = new byte[(i6 * 4)];
        int i7 = 0;
        byte[] bArr6 = null;
        MessageDigest instance = MessageDigest.getInstance(str);
        while (i7 < i6) {
            if (bArr6 != null) {
                instance.update(bArr6);
            }
            instance.update(bArr);
            bArr6 = instance.digest(bArr2);
            instance.reset();
            for (int i8 = 1; i8 < i3; i8++) {
                bArr6 = instance.digest(bArr6);
                instance.reset();
            }
            System.arraycopy(bArr6, 0, bArr5, i7 * 4, Math.min(bArr6.length, (i6 - i7) * 4));
            i7 += bArr6.length / 4;
        }
        System.arraycopy(bArr5, 0, bArr3, 0, i4 * 4);
        System.arraycopy(bArr5, i4 * 4, bArr4, 0, i5 * 4);
        return bArr5;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m16008(byte[] bArr, int i, int i2, byte[] bArr2, byte[] bArr3, byte[] bArr4) throws NoSuchAlgorithmException {
        return m16007(bArr, i, i2, bArr2, 1, "MD5", bArr3, bArr4);
    }
}
