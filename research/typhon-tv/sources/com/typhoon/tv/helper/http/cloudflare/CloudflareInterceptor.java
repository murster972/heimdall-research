package com.typhoon.tv.helper.http.cloudflare;

import com.squareup.duktape.DuktapeException;
import com.typhoon.tv.Logger;
import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Response;

public class CloudflareInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response r1 = chain.m6995(chain.m6994());
        if (r1.m7066() != 503 || r1.m7067("Server") == null || !r1.m7067("Server").toLowerCase().contains("cloudflare")) {
            return r1;
        }
        try {
            return chain.m6995(CloudflareHelper.m16013(r1));
        } catch (DuktapeException | CloudflareException | IOException | InterruptedException e) {
            if (!(e instanceof CloudflareException)) {
                Logger.m6281(e, new boolean[0]);
            }
            return chain.m6995(r1.m7069().m7044().m19995(CacheControl.f6219).m19989());
        }
    }
}
