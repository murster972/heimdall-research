package com.typhoon.tv.helper.http.interceptor;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

public class RemoveHeadersInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.m6995(chain.m6994().m7044().m19987("X-Request-CC").m19989());
    }
}
