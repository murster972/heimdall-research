package com.typhoon.tv.helper.http.cloudflare;

import com.squareup.duktape.Duktape;
import com.squareup.duktape.DuktapeException;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.utils.Regex;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class CloudflareHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    private static String m16011(String str) {
        if (!TyphoonApp.f5859) {
            return "";
        }
        Duktape duktape = null;
        String str2 = "";
        try {
            Duktape create = Duktape.create();
            Object evaluate = create.evaluate(str);
            if (evaluate != null) {
                str2 = evaluate.toString();
            }
            if (create == null) {
                return str2;
            }
            create.close();
            return str2;
        } catch (Throwable th) {
            if (duktape != null) {
                duktape.close();
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m16012(String str) {
        String replaceAll = str.replaceFirst("a\\.value\\s*=\\s*(.+?)\\s*\\+.*", "$1;").replaceAll("\\s{3,}[a-z](?:\\s*=\\s*|\\.).+", "").replaceAll("[\\n\\\\']", "");
        return replaceAll.substring(0, replaceAll.length() - 1) + ".toString();";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Request m16013(Response response) throws InterruptedException, IOException, CloudflareException, DuktapeException {
        String l;
        Request r12 = response.m7069();
        HttpUrl r20 = r12.m7053();
        String r17 = r20.m6961();
        String r5 = r20.m6951();
        ResponseBody r13 = response.m7056();
        String r9 = r13 != null ? r13.m7091() : "";
        if (r13 != null) {
            try {
                r13.close();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        if (r9.contains("Please complete the security check to access")) {
            return r12.m7044().m19989();
        }
        String r22 = Regex.m17800(r9, "input[\\#\\ ]+type=\"hidden\"[\\#\\ ]+name=\"jschl_vc\"[\\#\\ ]+value=\"([^\"]+)", 1);
        String r10 = Regex.m17800(r9, "input[\\#\\ ]+type=\"hidden\"[\\#\\ ]+name=\"pass\"[\\#\\ ]+value=\"([^\"]+)", 1);
        String r4 = Regex.m17800(r9, "setTimeout\\(function\\(\\)\\s*\\{\\s*(var\\s+(?:s,t,o,p,b,r,e,a,k,i,n,g|t,r,a),f.+?\\r?\\n[\\s\\S]+?a\\.value\\s*=\\s*.+?)\\r?\\n", 1);
        if (r22.isEmpty() || r10.isEmpty() || r4.isEmpty()) {
            throw new CloudflareException("Empty args");
        }
        String r7 = m16011(m16012(r4));
        if (r7.isEmpty()) {
            throw new CloudflareException("Eval failed");
        }
        try {
            l = Double.toString(Double.parseDouble(r7) + ((double) r5.length()));
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
            l = Long.toString(Long.parseLong(r7) + ((long) r5.length()));
        }
        String str = r17 + "://" + r5 + "/cdn-cgi/l/chk_jschl?jschl_vc=" + r22 + "&jschl_answer=" + l + "&pass=" + r10.replace("+", "%2B");
        String str2 = TyphoonApp.f5835;
        String r18 = r12.m7052(AbstractSpiCall.HEADER_USER_AGENT);
        if (r18 == null || r18.isEmpty()) {
            r18 = r12.m7052("user-agent");
        }
        if (r18 != null && !r18.isEmpty()) {
            str2 = r18;
        }
        String httpUrl = r12.m7053().toString();
        Thread.sleep(8000);
        return new Request.Builder().m19990().m19992(str).m19995(CacheControl.f6219).m19993(AbstractSpiCall.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8").m19993("Accept-Language", "en-US;q=0.6,en;q=0.4").m19993(AbstractSpiCall.HEADER_USER_AGENT, str2).m19993("Referer", httpUrl).m19989();
    }
}
