package com.typhoon.tv.helper.http.interceptor;

import java.util.concurrent.TimeUnit;
import okhttp3.CacheControl;
import okhttp3.Interceptor;

public class CacheInterceptor implements Interceptor {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final String[] f12643 = {"oauth", ".apk", "app-release", "yesplayer", "movie_token", "update_en", "update", "isAppDead", "showMultipleInterstitials", "showMultipleInterstitialsFromOgury", "multipleAdsAllowedNetworks", "secondAdAllowedNetworks", "loadOguryMaxTrialCount", "isMuteAllowed", "isAutoLoadOguryAllowed", "isAutoCloseOguryAllowed", "isOpenloadEnabled", "latestVersionCode", "av", "frame", "iframe", "token", "ajax_new.php"};

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f12644 = new CacheControl.Builder().m19882(2, TimeUnit.DAYS).m19880().toString();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final String[] f12645 = {"www.putlocker.systems", "putlocker-movies.tv", "putlocker.yt", "movieshd.is", "movieshd.tv", "movieshd.watch", "flixanity.mobi", "www.flixanity.me", "flixanity.watch", "flixanity.online", "flixanity.mobi", "cartoonhd.website", "cartoonhd.online", "cartoonhd.global", "cartoonhd.com", "cartoonhd.be", "cartoonhd.life", "pidtv.com", "watch5s.is", "kingmovies.is", "the123movieshub.net", "cmovieshd.net", "pmovies.to", "watchonline.pro", "watchfilm.to", "onlinemovies.tube", "afdah.tv", "afdah.to", "www6.123moviesfree.com", "vumoo.com", "vumoo.li", "chillax.ws", "hollymoviehd.com", "www.hollymoviehd.com", "streamango.com", "streamcherry.com", "fruitstreams.com", "openload.co", "openload.io", "openload.tv", "openload.stream", "openload.link", "oload.tv", "oload.stream", "oload.link", "oload.xyz", "oload.win", "oloadcdn.net", "streamdor.co", "embed.streamdor.co", "api.streamdor.co", "real-debrid.com", "up2stream.me", "pelispedia.vip", "pelispedia.video", "www1.pelispedia.tv", "player.pelispedia.tv", "www.pelispedia.tv", "api.pelispedia.tv", "cloud.pelispedia.tv", "cloud.pelispedia.vip", "pelispedia.tv", "fmovies.is", "fmovies.se", "tunemovie.com", "vivo.to", "html5player.to", "tunefiles.com", "amazonaws.com", "www.dizimvar1.com", "dizimvar1.com", "www.dizimvarx.com", "dizimvarx.com", "minhateca.com.br", "putstream.com", "movieocean.net", "api.movieocean.net", "https://www.facebook.com/groups/159702338161964/", "https://www.facebook.com/groups/159702338161964/", "putlocker.sk", "putlockertv.se", "vidlink.org", "pubfilm.is", "player.pubfilm.is", "streamcherry.xyz", "googleusercontent.com", "=", "=", "llnwi.net", "llnwd.net", "llnw.net", "fruity.pw", "cdn1.fruity.pw", "orange.fruity.pw", "streamhub.co", "titan.streamhub.co", "streamy.pw", "dobby.streamy.pw", "nagini.streamy.pw", "stream.moviestime.is", "www.scnsrc.me", "gdplayer.site", "getmypopcornnow.xyz", "gphoto.stream", "hulu.so", "mycdn.me", "player.miradetodo.net", "jw.miradetodo.net", "xvidstage.com", "faststream.ws", "www.ddlvalley.me", "ddlvalley.me", "gomovieshd.cc", "master.gomovieshd.cc", "cmovieshd.com", "master.cmovieshd.com", "hevcbluray.info", "hevcbluray.net"};

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String f12646 = new CacheControl.Builder().m19881().m19880().toString();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f12647 = new CacheControl.Builder().m19882(60, TimeUnit.MINUTES).m19880().toString();

    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0294, code lost:
        if (com.typhoon.tv.helper.GoogleVideoHelper.m15955(r16) == false) goto L_0x0297;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Response intercept(okhttp3.Interceptor.Chain r22) throws java.io.IOException {
        /*
            r21 = this;
            okhttp3.Request r12 = r22.m6994()
            r14 = 0
            okhttp3.HttpUrl r17 = r12.m7053()
            java.lang.String r5 = r17.m6951()
            okhttp3.HttpUrl r17 = r12.m7053()
            java.lang.String r16 = r17.toString()
            java.lang.String r17 = "##forceNoCache##"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x001f
            r14 = 1
        L_0x001f:
            if (r14 != 0) goto L_0x004e
            okhttp3.CacheControl r17 = r12.m7045()     // Catch:{ Exception -> 0x0570 }
            if (r17 == 0) goto L_0x004e
            okhttp3.CacheControl r17 = r12.m7045()     // Catch:{ Exception -> 0x0570 }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x0570 }
            if (r17 == 0) goto L_0x004e
            okhttp3.CacheControl r17 = okhttp3.CacheControl.f6219     // Catch:{ Exception -> 0x0570 }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x0570 }
            if (r17 == 0) goto L_0x004e
            okhttp3.CacheControl r17 = r12.m7045()     // Catch:{ Exception -> 0x0570 }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x0570 }
            okhttp3.CacheControl r18 = okhttp3.CacheControl.f6219     // Catch:{ Exception -> 0x0570 }
            java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x0570 }
            boolean r17 = r17.equalsIgnoreCase(r18)     // Catch:{ Exception -> 0x0570 }
            if (r17 == 0) goto L_0x004e
            r14 = 1
        L_0x004e:
            if (r14 != 0) goto L_0x0078
            java.lang.String r17 = r12.m7048()
            if (r17 == 0) goto L_0x0078
            java.lang.String r17 = r12.m7048()
            java.lang.String r17 = r17.trim()
            java.lang.String r18 = " "
            java.lang.String r18 = " "
            java.lang.String r19 = ""
            java.lang.String r19 = ""
            java.lang.String r17 = r17.replace(r18, r19)
            java.lang.String r18 = "GET"
            boolean r17 = r17.equalsIgnoreCase(r18)
            if (r17 != 0) goto L_0x0078
            r14 = 1
        L_0x0078:
            if (r14 != 0) goto L_0x010a
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r17 = "Range"
            r0 = r17
            r0 = r17
            java.util.List r17 = r12.m7049(r0)
            if (r17 == 0) goto L_0x009e
            java.lang.String r17 = "Range"
            r0 = r17
            r0 = r17
            java.util.List r17 = r12.m7049(r0)
            r0 = r17
            r0 = r17
            r11.addAll(r0)
        L_0x009e:
            java.lang.String r17 = "range"
            r0 = r17
            r0 = r17
            java.util.List r17 = r12.m7049(r0)
            if (r17 == 0) goto L_0x00be
            java.lang.String r17 = "range"
            java.lang.String r17 = "range"
            r0 = r17
            r0 = r17
            java.util.List r17 = r12.m7049(r0)
            r0 = r17
            r11.addAll(r0)
        L_0x00be:
            java.util.Iterator r17 = r11.iterator()
        L_0x00c2:
            boolean r18 = r17.hasNext()
            if (r18 == 0) goto L_0x010a
            java.lang.Object r10 = r17.next()
            java.lang.String r10 = (java.lang.String) r10
            java.lang.String r18 = r10.trim()
            java.lang.String r18 = r18.toLowerCase()
            java.lang.String r19 = " "
            java.lang.String r19 = " "
            java.lang.String r20 = ""
            java.lang.String r20 = ""
            java.lang.String r18 = r18.replace(r19, r20)
            java.lang.String r19 = "\r"
            java.lang.String r20 = ""
            java.lang.String r18 = r18.replace(r19, r20)
            java.lang.String r19 = "\n"
            java.lang.String r20 = ""
            java.lang.String r20 = ""
            java.lang.String r18 = r18.replace(r19, r20)
            java.lang.String r19 = "bytes=0-1"
            java.lang.String r19 = "bytes=0-1"
            boolean r18 = r18.contains(r19)
            if (r18 == 0) goto L_0x00c2
            r14 = 1
        L_0x010a:
            if (r14 != 0) goto L_0x0119
            java.lang.String[] r17 = f12645
            java.util.List r15 = java.util.Arrays.asList(r17)
            boolean r17 = r15.contains(r5)
            if (r17 == 0) goto L_0x0119
            r14 = 1
        L_0x0119:
            if (r14 != 0) goto L_0x012c
            java.lang.String r17 = r16.toLowerCase()
            java.lang.String r18 = "="
            java.lang.String r18 = "="
            boolean r17 = r17.contains(r18)
            if (r17 == 0) goto L_0x012c
            r14 = 1
        L_0x012c:
            if (r14 != 0) goto L_0x013f
            java.lang.String r17 = r16.toLowerCase()
            java.lang.String r18 = "hollymoviehd"
            java.lang.String r18 = "hollymoviehd"
            boolean r17 = r17.contains(r18)
            if (r17 == 0) goto L_0x013f
            r14 = 1
        L_0x013f:
            if (r14 != 0) goto L_0x0154
            java.lang.String r17 = "/cdn-cgi/l/chk_jschl"
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x0153
            java.lang.String r17 = "/cdn-cgi/l/chk_captcha"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x0154
        L_0x0153:
            r14 = 1
        L_0x0154:
            if (r14 != 0) goto L_0x0192
            java.lang.String r17 = "mehliz"
            java.lang.String r17 = "mehliz"
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0192
            java.lang.String r17 = "?s="
            java.lang.String r17 = "?s="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x0191
            java.lang.String r17 = "&s="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x0191
            java.lang.String r17 = "?search="
            java.lang.String r17 = "?search="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x0191
            java.lang.String r17 = "&search="
            java.lang.String r17 = "&search="
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x0192
        L_0x0191:
            r14 = 1
        L_0x0192:
            if (r14 != 0) goto L_0x01e2
            java.lang.String r17 = "hevcbluray"
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x01e2
            java.lang.String r17 = "?s="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x01e1
            java.lang.String r17 = "&s="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x01e1
            java.lang.String r17 = "?search="
            java.lang.String r17 = "?search="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x01e1
            java.lang.String r17 = "&search="
            java.lang.String r17 = "&search="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x01e1
            java.lang.String r17 = "?d="
            java.lang.String r17 = "?d="
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x01e1
            java.lang.String r17 = "&d="
            java.lang.String r17 = "&d="
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x01e2
        L_0x01e1:
            r14 = 1
        L_0x01e2:
            if (r14 != 0) goto L_0x01ee
            java.lang.String r17 = "/suggest.php?ajax=1"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x01ee
            r14 = 1
        L_0x01ee:
            if (r14 != 0) goto L_0x01fd
            java.lang.String r17 = "vidlink.org"
            java.lang.String r17 = "vidlink.org"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x01fd
            r14 = 1
        L_0x01fd:
            if (r14 != 0) goto L_0x0209
            java.lang.String r17 = "real-debrid"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x0209
            r14 = 1
        L_0x0209:
            if (r14 != 0) goto L_0x0215
            java.lang.String r17 = "/customsearch/"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x0215
            r14 = 1
        L_0x0215:
            if (r14 != 0) goto L_0x021e
            boolean r17 = com.typhoon.tv.helper.GoogleVideoHelper.m15945(r16)
            if (r17 == 0) goto L_0x021e
            r14 = 1
        L_0x021e:
            if (r14 != 0) goto L_0x0231
            java.lang.String r17 = "minhateca.com.br"
            java.lang.String r17 = "minhateca.com.br"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0231
            r14 = 1
        L_0x0231:
            if (r14 != 0) goto L_0x0297
            java.lang.String r17 = "google"
            java.lang.String r17 = "google"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0296
            java.lang.String r17 = "picasa"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0296
            java.lang.String r17 = "blogspot"
            java.lang.String r17 = "blogspot"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0296
            java.lang.String r17 = "youtube"
            java.lang.String r17 = "youtube"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0296
            java.lang.String r17 = "youtu.be"
            java.lang.String r17 = "youtu.be"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0296
            java.lang.String r17 = "googleapis"
            java.lang.String r17 = "googleapis"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0296
            boolean r17 = com.typhoon.tv.helper.GoogleVideoHelper.m15955(r16)
            if (r17 == 0) goto L_0x0297
        L_0x0296:
            r14 = 1
        L_0x0297:
            if (r14 != 0) goto L_0x02a6
            java.lang.String r17 = "drive.google.com/uc?"
            java.lang.String r17 = "drive.google.com/uc?"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x02a6
            r14 = 1
        L_0x02a6:
            if (r14 != 0) goto L_0x02bb
            java.lang.String r17 = "/proxy"
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x02ba
            java.lang.String r17 = "proxy/"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x02bb
        L_0x02ba:
            r14 = 1
        L_0x02bb:
            if (r14 != 0) goto L_0x02cc
            java.lang.String r17 = "cloudfront"
            java.lang.String r17 = "cloudfront"
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x02cc
            r14 = 1
        L_0x02cc:
            if (r14 != 0) goto L_0x02ea
            java.lang.String r17 = ".m4ufree."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x02e9
            java.lang.String r17 = "m4ukido."
            java.lang.String r17 = "m4ukido."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x02ea
        L_0x02e9:
            r14 = 1
        L_0x02ea:
            if (r14 != 0) goto L_0x02fd
            java.lang.String r17 = "gdplayer."
            java.lang.String r17 = "gdplayer."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x02fd
            r14 = 1
        L_0x02fd:
            if (r14 != 0) goto L_0x031d
            java.lang.String r17 = "mehliz"
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x031d
            java.lang.String r17 = "cdn."
            boolean r17 = r16.contains(r17)
            if (r17 != 0) goto L_0x031c
            java.lang.String r17 = ".php"
            boolean r17 = r16.contains(r17)
            if (r17 == 0) goto L_0x031d
        L_0x031c:
            r14 = 1
        L_0x031d:
            if (r14 != 0) goto L_0x0330
            java.lang.String r17 = "gomovieshd.cc"
            java.lang.String r17 = "gomovieshd.cc"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0330
            r14 = 1
        L_0x0330:
            if (r14 != 0) goto L_0x0341
            java.lang.String r17 = "the123movieshub.net"
            java.lang.String r17 = "cmovieshd.com"
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0341
            r14 = 1
        L_0x0341:
            if (r14 != 0) goto L_0x0352
            java.lang.String r17 = "embed.streamdor.co"
            java.lang.String r17 = "embed.streamdor.co"
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0352
            r14 = 1
        L_0x0352:
            if (r14 != 0) goto L_0x0360
            java.lang.String r17 = "mycdn."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0360
            r14 = 1
        L_0x0360:
            if (r14 != 0) goto L_0x0374
            java.lang.String r17 = "vidcdn."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 != 0) goto L_0x0373
            boolean r17 = com.typhoon.tv.helper.VidCDNHelper.m15996(r16)
            if (r17 == 0) goto L_0x0374
        L_0x0373:
            r14 = 1
        L_0x0374:
            if (r14 != 0) goto L_0x0384
            java.lang.String r17 = "ahcdn."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0384
            r14 = 1
        L_0x0384:
            if (r14 != 0) goto L_0x0395
            java.lang.String r17 = "ntcdn."
            java.lang.String r17 = "ntcdn."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0395
            r14 = 1
        L_0x0395:
            if (r14 != 0) goto L_0x03a6
            java.lang.String r17 = "micetop."
            java.lang.String r17 = "micetop."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x03a6
            r14 = 1
        L_0x03a6:
            if (r14 != 0) goto L_0x03b9
            java.lang.String r17 = "cdn.vidnode."
            java.lang.String r17 = "cdn.vidnode."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x03b9
            r14 = 1
        L_0x03b9:
            if (r14 != 0) goto L_0x03c9
            java.lang.String r17 = "fbcdn."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x03c9
            r14 = 1
        L_0x03c9:
            if (r14 != 0) goto L_0x03da
            java.lang.String r17 = "amazonaws."
            java.lang.String r17 = "amazonaws."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x03da
            r14 = 1
        L_0x03da:
            if (r14 != 0) goto L_0x03eb
            java.lang.String r17 = "yandex."
            java.lang.String r17 = "yandex."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x03eb
            r14 = 1
        L_0x03eb:
            if (r14 != 0) goto L_0x03fe
            java.lang.String r17 = "amazon."
            java.lang.String r17 = "amazon."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x03fe
            r14 = 1
        L_0x03fe:
            if (r14 != 0) goto L_0x0411
            java.lang.String r17 = "anyplayer."
            java.lang.String r17 = "anyplayer."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0411
            r14 = 1
        L_0x0411:
            if (r14 != 0) goto L_0x041f
            java.lang.String r17 = "fruity."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x041f
            r14 = 1
        L_0x041f:
            if (r14 != 0) goto L_0x0432
            java.lang.String r17 = "streamhub.co"
            java.lang.String r17 = "streamhub.co"
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0432
            r14 = 1
        L_0x0432:
            if (r14 != 0) goto L_0x0445
            java.lang.String r17 = "streamy."
            java.lang.String r17 = "streamy."
            r0 = r17
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0445
            r14 = 1
        L_0x0445:
            if (r14 != 0) goto L_0x046b
            okhttp3.HttpUrl r17 = r12.m7053()
            java.lang.String r4 = r17.m6953()
            java.lang.String[] r18 = f12643
            r0 = r18
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r19 = r0
            r17 = 0
        L_0x045c:
            r0 = r17
            r1 = r19
            if (r0 >= r1) goto L_0x046b
            r9 = r18[r17]
            boolean r20 = r4.contains(r9)
            if (r20 == 0) goto L_0x0582
            r14 = 1
        L_0x046b:
            if (r14 == 0) goto L_0x05bf
            okhttp3.Request$Builder r17 = r12.m7044()
            java.lang.String r18 = "Pragma"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "C3-Cache-Control"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "X-Cache"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "X-Cache-Hit"
            java.lang.String r18 = "X-Cache-Hit"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "pragma"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "c3-cache-control"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "x-cache"
            java.lang.String r18 = "x-cache"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            java.lang.String r18 = "x-cache-hit"
            okhttp3.Request$Builder r17 = r17.m19987(r18)
            okhttp3.CacheControl r18 = okhttp3.CacheControl.f6219
            okhttp3.Request$Builder r7 = r17.m19995((okhttp3.CacheControl) r18)
            java.lang.String r17 = "##forceNoCache##"
            boolean r17 = r16.contains(r17)     // Catch:{ Exception -> 0x0586 }
            if (r17 == 0) goto L_0x04d1
            java.lang.String r17 = "##forceNoCache##"
            java.lang.String r18 = ""
            java.lang.String r18 = ""
            java.lang.String r17 = r16.replace(r17, r18)     // Catch:{ Exception -> 0x0586 }
            r0 = r17
            okhttp3.Request$Builder r7 = r7.m19992((java.lang.String) r0)     // Catch:{ Exception -> 0x0586 }
        L_0x04d1:
            java.lang.String r17 = r16.toLowerCase()     // Catch:{ Exception -> 0x059a }
            java.lang.String r18 = "hollymoviehd"
            boolean r17 = r17.contains(r18)     // Catch:{ Exception -> 0x059a }
            if (r17 == 0) goto L_0x04ee
            java.lang.String r17 = "If-Modified-Since"
            r0 = r17
            okhttp3.Request$Builder r17 = r7.m19987(r0)     // Catch:{ Exception -> 0x059a }
            java.lang.String r18 = "if-modified-since"
            okhttp3.Request$Builder r7 = r17.m19987(r18)     // Catch:{ Exception -> 0x059a }
        L_0x04ee:
            okhttp3.Request r6 = r7.m19989()
            r0 = r22
            r0 = r22
            okhttp3.Response r13 = r0.m6995(r6)
            okhttp3.Response$Builder r17 = r13.m7060()
            java.lang.String r18 = "Pragma"
            java.lang.String r18 = "Pragma"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "C3-Cache-Control"
            java.lang.String r18 = "C3-Cache-Control"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "X-Cache"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "X-Cache-Hit"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "pragma"
            java.lang.String r18 = "pragma"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "c3-cache-control"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "x-cache"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "x-cache-hit"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "Cache-Control"
            java.lang.String r19 = f12646
            okhttp3.Response$Builder r8 = r17.m7080((java.lang.String) r18, (java.lang.String) r19)
            java.lang.String r17 = r16.toLowerCase()     // Catch:{ Exception -> 0x05ae }
            java.lang.String r18 = "hollymoviehd"
            boolean r17 = r17.contains(r18)     // Catch:{ Exception -> 0x05ae }
            if (r17 == 0) goto L_0x056b
            java.lang.String r17 = "If-Modified-Since"
            java.lang.String r17 = "If-Modified-Since"
            r0 = r17
            okhttp3.Response$Builder r17 = r8.m7073((java.lang.String) r0)     // Catch:{ Exception -> 0x05ae }
            java.lang.String r18 = "if-modified-since"
            java.lang.String r18 = "if-modified-since"
            okhttp3.Response$Builder r8 = r17.m7073((java.lang.String) r18)     // Catch:{ Exception -> 0x05ae }
        L_0x056b:
            okhttp3.Response r17 = r8.m7087()
        L_0x056f:
            return r17
        L_0x0570:
            r3 = move-exception
            r17 = 0
            r0 = r17
            r0 = r17
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r0)
            goto L_0x004e
        L_0x0582:
            int r17 = r17 + 1
            goto L_0x045c
        L_0x0586:
            r3 = move-exception
            r17 = 0
            r0 = r17
            r0 = r17
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r0 = r17
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r0)
            goto L_0x04d1
        L_0x059a:
            r3 = move-exception
            r17 = 0
            r0 = r17
            r0 = r17
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r17 = r0
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r0)
            goto L_0x04ee
        L_0x05ae:
            r3 = move-exception
            r17 = 0
            r0 = r17
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r0 = r17
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r0)
            goto L_0x056b
        L_0x05bf:
            java.lang.String r17 = "moviegrabber."
            java.lang.String r17 = "moviegrabber."
            r0 = r17
            boolean r17 = r5.contains(r0)
            if (r17 == 0) goto L_0x0666
            java.lang.String r2 = f12644
        L_0x05cf:
            okhttp3.Request$Builder r7 = r12.m7044()
            java.lang.String r17 = "##forceNoCache##"
            boolean r17 = r16.contains(r17)     // Catch:{ Exception -> 0x066a }
            if (r17 == 0) goto L_0x05f1
            java.lang.String r17 = "##forceNoCache##"
            java.lang.String r18 = ""
            java.lang.String r18 = ""
            java.lang.String r17 = r16.replace(r17, r18)     // Catch:{ Exception -> 0x066a }
            r0 = r17
            r0 = r17
            okhttp3.Request$Builder r7 = r7.m19992((java.lang.String) r0)     // Catch:{ Exception -> 0x066a }
        L_0x05f1:
            okhttp3.Request r17 = r7.m19989()
            r0 = r22
            r1 = r17
            r1 = r17
            okhttp3.Response r13 = r0.m6995(r1)
            okhttp3.Response$Builder r17 = r13.m7060()
            java.lang.String r18 = "Pragma"
            java.lang.String r18 = "Pragma"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "C3-Cache-Control"
            java.lang.String r18 = "C3-Cache-Control"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "X-Cache"
            java.lang.String r18 = "X-Cache"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "X-Cache-Hit"
            java.lang.String r18 = "X-Cache-Hit"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "pragma"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "c3-cache-control"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "x-cache"
            java.lang.String r18 = "x-cache"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "x-cache-hit"
            okhttp3.Response$Builder r17 = r17.m7073((java.lang.String) r18)
            java.lang.String r18 = "Cache-Control"
            java.lang.String r18 = "Cache-Control"
            boolean r19 = r13.m7065()
            if (r19 != 0) goto L_0x0658
            java.lang.String r2 = f12646
        L_0x0658:
            r0 = r17
            r1 = r18
            okhttp3.Response$Builder r17 = r0.m7080((java.lang.String) r1, (java.lang.String) r2)
            okhttp3.Response r17 = r17.m7087()
            goto L_0x056f
        L_0x0666:
            java.lang.String r2 = f12647
            goto L_0x05cf
        L_0x066a:
            r3 = move-exception
            r17 = 0
            r0 = r17
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r0)
            goto L_0x05f1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.helper.http.interceptor.CacheInterceptor.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }
}
