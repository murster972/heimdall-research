package com.typhoon.tv.helper.http;

import android.annotation.SuppressLint;
import android.os.Build;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.http.cloudflare.CloudflareInterceptor;
import com.typhoon.tv.helper.http.interceptor.CacheInterceptor;
import com.typhoon.tv.helper.http.interceptor.CloseConnectionInterceptor;
import com.typhoon.tv.helper.http.interceptor.ForceNoCacheSegmentInterceptor;
import com.typhoon.tv.helper.http.interceptor.HeadersInterceptor;
import com.typhoon.tv.helper.http.interceptor.MehlizMoviesInterceptor;
import com.typhoon.tv.helper.http.interceptor.PostRedirectInterceptor;
import com.typhoon.tv.helper.http.interceptor.PostRewriteResponseCodeInterceptor;
import com.typhoon.tv.helper.http.interceptor.RemoveHeadersInterceptor;
import com.typhoon.tv.model.HttpHeaderBodyResult;
import com.typhoon.tv.utils.SourceObservableUtils;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.CipherSuite;
import okhttp3.ConnectionPool;
import okhttp3.ConnectionSpec;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.TlsVersion;
import org.apache.oltu.oauth2.common.OAuth;

public class HttpHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile HttpHelper f5933;

    /* renamed from: 靐  reason: contains not printable characters */
    private CookieJar f5934;

    /* renamed from: 齉  reason: contains not printable characters */
    private OkHttpClient f5935;

    private HttpHelper() {
        if (this.f5934 == null) {
            this.f5934 = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(TVApplication.m6288()));
        }
        if (this.f5935 == null) {
            this.f5935 = m6352();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static OkHttpClient.Builder m6346(OkHttpClient.Builder builder) {
        return builder;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static X509TrustManager m6338() {
        return new X509TrustManager() {
            @SuppressLint({"TrustAllX509TrustManager"})
            public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
            }

            @SuppressLint({"TrustAllX509TrustManager"})
            public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static SSLSocketFactory m6345(X509TrustManager trustManager) {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init((KeyManager[]) null, new TrustManager[]{trustManager}, (SecureRandom) null);
            if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT >= 22) {
                return sslContext.getSocketFactory();
            }
            try {
                return new Tls12SocketFactory(sslContext.getSocketFactory());
            } catch (Throwable throwable) {
                Logger.m6281(throwable, true);
                return sslContext.getSocketFactory();
            }
        } catch (GeneralSecurityException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return null;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static HostnameVerifier m6339() {
        return new HostnameVerifier() {
            @SuppressLint({"BadHostnameVerifier"})
            public boolean verify(String str, SSLSession sSLSession) {
                return true;
            }
        };
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private static ConnectionSpec m6340() {
        return new ConnectionSpec.Builder(ConnectionSpec.f6245).m6894(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0).m6893(CipherSuite.f15954, CipherSuite.f15967, CipherSuite.f15927, CipherSuite.f15928).m6895();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static OkHttpClient.Builder m6341(OkHttpClient.Builder builder) {
        List<ConnectionSpec> specList = new ArrayList<>();
        specList.add(ConnectionSpec.f6247);
        specList.add(ConnectionSpec.f6245);
        specList.add(m6340());
        specList.add(ConnectionSpec.f6246);
        return builder.m7034(specList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static HttpHelper m6343() {
        HttpHelper instance = f5933;
        if (instance == null) {
            synchronized (HttpHelper.class) {
                try {
                    instance = f5933;
                    if (instance == null) {
                        HttpHelper instance2 = new HttpHelper();
                        try {
                            f5933 = instance2;
                            instance = instance2;
                        } catch (Throwable th) {
                            th = th;
                            HttpHelper httpHelper = instance2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return instance;
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b9 A[Catch:{ Exception -> 0x0145 }] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long m6342(okhttp3.Response r16, boolean r17, boolean... r18) {
        /*
            r4 = -1
            if (r16 != 0) goto L_0x0008
            r12 = -1
            r6 = r4
        L_0x0007:
            return r12
        L_0x0008:
            okhttp3.Headers r12 = r16.m7055()     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x0088
            okhttp3.Headers r12 = r16.m7055()     // Catch:{ Exception -> 0x0130 }
            java.util.Map r8 = r12.m6933()     // Catch:{ Exception -> 0x0130 }
            if (r8 == 0) goto L_0x0088
            if (r17 == 0) goto L_0x00d9
            java.lang.String r12 = "Content-Range"
            boolean r12 = r8.containsKey(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 != 0) goto L_0x002c
            java.lang.String r12 = "content-range"
            boolean r12 = r8.containsKey(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x00d9
        L_0x002c:
            java.lang.String r12 = "Content-Range"
            boolean r12 = r8.containsKey(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x00cd
            java.lang.String r12 = "Content-Range"
            java.lang.Object r12 = r8.get(r12)     // Catch:{ Exception -> 0x0130 }
            java.util.List r12 = (java.util.List) r12     // Catch:{ Exception -> 0x0130 }
            r10 = r12
        L_0x003f:
            if (r10 == 0) goto L_0x0088
            int r12 = r10.size()     // Catch:{ Exception -> 0x0130 }
            if (r12 <= 0) goto L_0x0088
            r12 = 0
            java.lang.Object r9 = r10.get(r12)     // Catch:{ Exception -> 0x0130 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ Exception -> 0x0130 }
            if (r9 == 0) goto L_0x0088
            boolean r12 = r9.isEmpty()     // Catch:{ Exception -> 0x0130 }
            if (r12 != 0) goto L_0x0088
            r11 = 0
            java.lang.String r12 = "/"
            boolean r12 = r9.contains(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x0067
            java.lang.String r12 = "/"
            java.lang.String[] r11 = r9.split(r12)     // Catch:{ Exception -> 0x0130 }
        L_0x0067:
            if (r11 == 0) goto L_0x0074
            int r12 = r11.length     // Catch:{ Exception -> 0x0130 }
            r13 = 2
            if (r12 != r13) goto L_0x0074
            r12 = 1
            r12 = r11[r12]     // Catch:{ Exception -> 0x0130 }
            java.lang.String r9 = r12.trim()     // Catch:{ Exception -> 0x0130 }
        L_0x0074:
            boolean r12 = r9.isEmpty()     // Catch:{ Exception -> 0x0130 }
            if (r12 != 0) goto L_0x0088
            boolean r12 = com.typhoon.tv.utils.Utils.m6426((java.lang.String) r9)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x0088
            java.lang.Long r12 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x0130 }
            long r4 = r12.longValue()     // Catch:{ Exception -> 0x0130 }
        L_0x0088:
            if (r18 == 0) goto L_0x0139
            r0 = r18
            int r12 = r0.length     // Catch:{ Exception -> 0x0145 }
            if (r12 <= 0) goto L_0x0139
            r12 = 0
            boolean r12 = r18[r12]     // Catch:{ Exception -> 0x0145 }
            if (r12 == 0) goto L_0x0139
            r2 = 1
        L_0x0095:
            if (r2 != 0) goto L_0x00ab
            r12 = -1
            int r12 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
            if (r12 != 0) goto L_0x00ab
            okhttp3.ResponseBody r12 = r16.m7056()     // Catch:{ Exception -> 0x0145 }
            if (r12 == 0) goto L_0x00ab
            okhttp3.ResponseBody r12 = r16.m7056()     // Catch:{ Throwable -> 0x013c }
            long r4 = r12.m7093()     // Catch:{ Throwable -> 0x013c }
        L_0x00ab:
            if (r2 != 0) goto L_0x014c
            r12 = -1
            int r12 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
            if (r12 > 0) goto L_0x014c
            okhttp3.Response r12 = r16.m7061()     // Catch:{ Exception -> 0x0145 }
            if (r12 == 0) goto L_0x014c
            okhttp3.Response r12 = r16.m7061()     // Catch:{ Exception -> 0x0145 }
            r13 = 1
            boolean[] r13 = new boolean[r13]     // Catch:{ Exception -> 0x0145 }
            r14 = 0
            r15 = 1
            r13[r14] = r15     // Catch:{ Exception -> 0x0145 }
            r0 = r17
            long r12 = m6342((okhttp3.Response) r12, (boolean) r0, (boolean[]) r13)     // Catch:{ Exception -> 0x0145 }
            r6 = r4
            goto L_0x0007
        L_0x00cd:
            java.lang.String r12 = "content-range"
            java.lang.Object r12 = r8.get(r12)     // Catch:{ Exception -> 0x0130 }
            java.util.List r12 = (java.util.List) r12     // Catch:{ Exception -> 0x0130 }
            r10 = r12
            goto L_0x003f
        L_0x00d9:
            java.lang.String r12 = "Content-Length"
            boolean r12 = r8.containsKey(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 != 0) goto L_0x00eb
            java.lang.String r12 = "content-length"
            boolean r12 = r8.containsKey(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x0088
        L_0x00eb:
            java.lang.String r12 = "Content-Length"
            boolean r12 = r8.containsKey(r12)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x0125
            java.lang.String r12 = "Content-Length"
            java.lang.Object r12 = r8.get(r12)     // Catch:{ Exception -> 0x0130 }
            java.util.List r12 = (java.util.List) r12     // Catch:{ Exception -> 0x0130 }
            r10 = r12
        L_0x00fe:
            if (r10 == 0) goto L_0x0088
            int r12 = r10.size()     // Catch:{ Exception -> 0x0130 }
            if (r12 <= 0) goto L_0x0088
            r12 = 0
            java.lang.Object r9 = r10.get(r12)     // Catch:{ Exception -> 0x0130 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ Exception -> 0x0130 }
            if (r9 == 0) goto L_0x0088
            boolean r12 = r9.isEmpty()     // Catch:{ Exception -> 0x0130 }
            if (r12 != 0) goto L_0x0088
            boolean r12 = com.typhoon.tv.utils.Utils.m6426((java.lang.String) r9)     // Catch:{ Exception -> 0x0130 }
            if (r12 == 0) goto L_0x0088
            java.lang.Long r12 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x0130 }
            long r4 = r12.longValue()     // Catch:{ Exception -> 0x0130 }
            goto L_0x0088
        L_0x0125:
            java.lang.String r12 = "content-length"
            java.lang.Object r12 = r8.get(r12)     // Catch:{ Exception -> 0x0130 }
            java.util.List r12 = (java.util.List) r12     // Catch:{ Exception -> 0x0130 }
            r10 = r12
            goto L_0x00fe
        L_0x0130:
            r3 = move-exception
            r12 = 0
            boolean[] r12 = new boolean[r12]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r12)
            goto L_0x0088
        L_0x0139:
            r2 = 0
            goto L_0x0095
        L_0x013c:
            r3 = move-exception
            r12 = 0
            boolean[] r12 = new boolean[r12]     // Catch:{ Exception -> 0x0145 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r12)     // Catch:{ Exception -> 0x0145 }
            goto L_0x00ab
        L_0x0145:
            r3 = move-exception
            r12 = 0
            boolean[] r12 = new boolean[r12]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r12)
        L_0x014c:
            r6 = r4
            r12 = r4
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.helper.http.HttpHelper.m6342(okhttp3.Response, boolean, boolean[]):long");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public OkHttpClient m6352() {
        OkHttpClient.Builder builder = m6346(new OkHttpClient.Builder().m7038(new Cache(TVApplication.m6288().getCacheDir(), Utils.m6396(TVApplication.m6288().getCacheDir()))));
        X509TrustManager unsafeTrustManager = m6338();
        SSLSocketFactory sslSocketFactory = m6345(unsafeTrustManager);
        HostnameVerifier hostnameVerifier = m6339();
        OkHttpClient.Builder builder2 = builder.m7041((Interceptor) new ForceNoCacheSegmentInterceptor()).m7031(new PostRewriteResponseCodeInterceptor()).m7041((Interceptor) new HeadersInterceptor()).m7041((Interceptor) new PostRedirectInterceptor()).m7041((Interceptor) new CloseConnectionInterceptor()).m7041((Interceptor) new CloudflareInterceptor()).m7041((Interceptor) new MehlizMoviesInterceptor()).m7031(new CacheInterceptor()).m7041((Interceptor) new RemoveHeadersInterceptor()).m7033(45, TimeUnit.SECONDS).m7030(45, TimeUnit.SECONDS).m7032(45, TimeUnit.SECONDS).m7042(true).m7040(this.f5934);
        if (sslSocketFactory != null) {
            try {
                builder2 = builder2.m7036(sslSocketFactory, unsafeTrustManager);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        if (hostnameVerifier != null) {
            builder2 = builder2.m7035(hostnameVerifier);
        }
        return m6341(builder2).m7043();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public OkHttpClient m6356() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        X509TrustManager unsafeTrustManager = m6338();
        SSLSocketFactory sslSocketFactory = m6345(unsafeTrustManager);
        HostnameVerifier hostnameVerifier = m6339();
        OkHttpClient.Builder builder2 = builder.m7031(new PostRewriteResponseCodeInterceptor()).m7041((Interceptor) new HeadersInterceptor()).m7041((Interceptor) new PostRedirectInterceptor()).m7041((Interceptor) new RemoveHeadersInterceptor()).m7033(45, TimeUnit.SECONDS).m7030(45, TimeUnit.SECONDS).m7032(45, TimeUnit.SECONDS).m7042(true).m7039(new ConnectionPool(0, 1, TimeUnit.NANOSECONDS)).m7040(this.f5934);
        if (sslSocketFactory != null) {
            try {
                builder2 = builder2.m7036(sslSocketFactory, unsafeTrustManager);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        if (hostnameVerifier != null) {
            builder2 = builder2.m7035(hostnameVerifier);
        }
        return m6341(builder2).m7043();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public OkHttpClient.Builder m6354() {
        OkHttpClient.Builder builder = m6346(new OkHttpClient.Builder());
        X509TrustManager unsafeTrustManager = m6338();
        SSLSocketFactory sslSocketFactory = m6345(unsafeTrustManager);
        HostnameVerifier hostnameVerifier = m6339();
        OkHttpClient.Builder builder2 = builder.m7031(new PostRewriteResponseCodeInterceptor()).m7041((Interceptor) new PostRedirectInterceptor()).m7033(45, TimeUnit.SECONDS).m7030(45, TimeUnit.SECONDS).m7032(45, TimeUnit.SECONDS).m7042(true).m7040(this.f5934);
        if (sslSocketFactory != null) {
            try {
                builder2 = builder2.m7036(sslSocketFactory, unsafeTrustManager);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        if (hostnameVerifier != null) {
            builder2 = builder2.m7035(hostnameVerifier);
        }
        return m6341(builder2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public synchronized OkHttpClient m6348() {
        return this.f5935;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response m6366(String url) {
        return m6368(new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835).m19989(), new int[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response m6367(String url, boolean useHead, Map<String, String> headers) {
        if (url == null || url.trim().isEmpty()) {
            return null;
        }
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        if (useHead) {
            reqBuilder = reqBuilder.m19986();
        }
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        return m6368(reqBuilder.m19989(), new int[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response m6368(Request request, int... retryCount) {
        int maxRetryCount;
        int _retryCount = (retryCount == null || retryCount.length <= 0) ? 0 : retryCount[0];
        boolean _isRetried = _retryCount > 0;
        Response response = null;
        try {
            OkHttpClient okHttpClient = this.f5935;
            if (request.m7053().toString().contains("/api.thetvdb.com/")) {
                okHttpClient = okHttpClient.m7021().m7033(30, TimeUnit.SECONDS).m7030(30, TimeUnit.SECONDS).m7032(30, TimeUnit.SECONDS).m7043();
            } else if (request.m7053().toString().contains(".gomovieshd.to/") || request.m7053().toString().contains(".the123movieshub.net/")) {
                okHttpClient = okHttpClient.m7021().m7033(10, TimeUnit.SECONDS).m7030(10, TimeUnit.SECONDS).m7032(10, TimeUnit.SECONDS).m7043();
            }
            response = okHttpClient.m7027(request).m19883();
            if (response == null) {
                Response response2 = response;
                return null;
            } else if (response.m7066() == 429) {
                boolean shouldRetry429 = !_isRetried;
                String urlStr = request.m7053().toString();
                if (GoogleVideoHelper.m15955(urlStr)) {
                    if (GoogleVideoHelper.m15945(urlStr)) {
                        maxRetryCount = 20;
                    } else {
                        maxRetryCount = GoogleVideoHelper.m15947(urlStr) ? 5 : 2;
                    }
                    if ((GoogleVideoHelper.m15947(urlStr) || GoogleVideoHelper.m15945(urlStr)) && _retryCount < maxRetryCount) {
                        shouldRetry429 = true;
                    }
                }
                if (shouldRetry429) {
                    if (response.m7056() != null) {
                        response.m7056().close();
                    }
                    _retryCount++;
                    try {
                        Response response3 = response;
                        return m6368(request.m7044().m19995(CacheControl.f6219).m19989(), _retryCount);
                    } catch (StackOverflowError stackOverflowError) {
                        Logger.m6281((Throwable) stackOverflowError, true);
                        Response response4 = response;
                        return response;
                    }
                } else {
                    return response;
                }
            } else {
                return response;
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        } catch (Throwable e2) {
            try {
                boolean stackOverflow = e2 instanceof StackOverflowError;
                Logger.m6281(e2, stackOverflow);
                if (stackOverflow) {
                    Response response5 = response;
                    return null;
                }
                if (response != null) {
                    if (response.m7056() != null) {
                        response.m7056().close();
                    }
                }
                if (_isRetried) {
                    Response response6 = response;
                    return null;
                }
                if ((e2 instanceof IOException) && e2.getMessage() != null && e2.getMessage().toLowerCase().contains("unexpected end of stream on")) {
                    try {
                        Response response7 = response;
                        return m6368(request.m7044().m19993("X-Request-CC", "true").m19993("Connection", "close").m19995(CacheControl.f6219).m19989(), _retryCount + 1);
                    } catch (StackOverflowError stackOverflowError2) {
                        Logger.m6281((Throwable) stackOverflowError2, true);
                        Response response8 = response;
                        return null;
                    }
                } else {
                    Response response9 = response;
                    return null;
                }
            } catch (Exception e22) {
                Logger.m6281((Throwable) e22, new boolean[0]);
            } catch (Throwable e23) {
                Logger.m6281(e23, e23 instanceof StackOverflowError);
                Response response10 = response;
                return null;
            }
        }
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    public final ResponseBody m6369(String url, Map<String, String>... extraHeaders) {
        if (url.isEmpty()) {
            return null;
        }
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
            for (Map.Entry<String, String> entry : extraHeaders[0].entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        Response res = m6368(reqBuilder.m19989(), new int[0]);
        if (res == null) {
            return null;
        }
        if (res.m7066() != 404) {
            return res.m7056();
        }
        if (res.m7056() != null) {
            res.m7056().close();
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6364(Request request) {
        Response res = m6368(request, new int[0]);
        if (res == null) {
            return "";
        }
        if (res.m7066() == 404 || res.m7066() == 400) {
            if (res.m7056() != null) {
                res.m7056().close();
            }
            return "";
        }
        if (res.m7066() >= 500) {
            try {
                if (request.m7053().toString().contains("/api.thetvdb.com/")) {
                    return "TvdbApi Error";
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        try {
            if (request.m7048().equalsIgnoreCase(OAuth.HttpMethod.GET) || request.m7048().equalsIgnoreCase(OAuth.HttpMethod.POST)) {
                boolean doesRequestHaveRangeHeader = false;
                try {
                    if (request.m7049("Range").size() > 0 || request.m7049("range").size() > 0) {
                        doesRequestHaveRangeHeader = true;
                    } else {
                        doesRequestHaveRangeHeader = false;
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
                if (m6342(res, doesRequestHaveRangeHeader, new boolean[0]) >= 10485760) {
                    if (res.m7056() != null) {
                        res.m7056().close();
                    }
                    return "";
                }
            }
        } catch (Exception e3) {
            Logger.m6281((Throwable) e3, new boolean[0]);
        }
        String resBody = "";
        try {
            resBody = res.m7056().m7091();
        } catch (Exception e4) {
            Logger.m6281((Throwable) e4, new boolean[0]);
        }
        if (res.m7056() == null) {
            return resBody;
        }
        res.m7056().close();
        return resBody;
    }

    @SafeVarargs
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m6351(String url, Map<String, String>... extraHeaders) {
        if (url.isEmpty()) {
            return "";
        }
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
            for (Map.Entry<String, String> entry : extraHeaders[0].entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        return m6364(reqBuilder.m19989());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6358(String url, String referer) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Referer", referer);
        return m6351(url, (Map<String, String>[]) new Map[]{headers});
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m6361(String url, String referer, Map<String, String>... extraHeaders) {
        if (url.isEmpty()) {
            return "";
        }
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
            for (Map.Entry<String, String> entry : extraHeaders[0].entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        if (referer != null && !referer.isEmpty()) {
            reqBuilder.m19988("Referer", referer);
        }
        return m6364(reqBuilder.m19989());
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m6359(String url, String userAgent, String referer, Map<String, String>... extraHeaders) {
        if (url.isEmpty()) {
            return "";
        }
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, userAgent);
        if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
            for (Map.Entry<String, String> entry : extraHeaders[0].entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        if (referer != null && !referer.isEmpty()) {
            reqBuilder.m19988("Referer", referer);
        }
        return m6364(reqBuilder.m19989());
    }

    @SafeVarargs
    /* renamed from: 齉  reason: contains not printable characters */
    public final HttpHeaderBodyResult m6355(String url, Map<String, String>... extraHeaders) {
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
            for (Map.Entry<String, String> entry : extraHeaders[0].entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        Response res = m6368(reqBuilder.m19989(), new int[0]);
        if (res == null) {
            return null;
        }
        String resBodyString = "";
        if (res.m7066() != 404) {
            try {
                resBodyString = res.m7056().m7091();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        if (res.m7056() != null) {
            res.m7056().close();
        }
        return new HttpHeaderBodyResult(res.m7055().m6933(), resBodyString);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6362(String url, boolean useHead, String referer) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Referer", referer);
        return m6363(url, useHead, (Map<String, String>[]) new Map[]{headers});
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m6363(String url, boolean useHead, Map<String, String>... extraHeaders) {
        try {
            Map<String, String> reqHeaders = new HashMap<>();
            if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
                reqHeaders.putAll(extraHeaders[0]);
            }
            if (useHead && !SourceObservableUtils.m17832(url)) {
                useHead = false;
            }
            if (!useHead) {
                reqHeaders.put("Range", "bytes=0-1");
            }
            Response res = m6367(url, useHead, reqHeaders);
            if (res == null && !useHead && (reqHeaders.containsKey("Range") || reqHeaders.containsKey("range"))) {
                if (reqHeaders.containsKey("Range")) {
                    reqHeaders.remove("Range");
                }
                if (reqHeaders.containsKey("range")) {
                    reqHeaders.remove("range");
                }
                res = m6367(url, useHead, reqHeaders);
            }
            return m6365(res, url, useHead, true, reqHeaders);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return url;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ba, code lost:
        if (m6353(r12, r6) == false) goto L_0x00e9;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m6365(okhttp3.Response r11, java.lang.String r12, boolean r13, boolean r14, java.util.Map<java.lang.String, java.lang.String> r15) {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x00fa
            okhttp3.Response r5 = r11.m7062()     // Catch:{ Exception -> 0x00ff }
        L_0x0006:
            java.lang.String r6 = ""
            if (r11 == 0) goto L_0x0018
            okhttp3.ResponseBody r8 = r11.m7056()     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x0018
            okhttp3.ResponseBody r8 = r11.m7056()     // Catch:{ Exception -> 0x00ff }
            r8.close()     // Catch:{ Exception -> 0x00ff }
        L_0x0018:
            if (r5 == 0) goto L_0x0027
            okhttp3.ResponseBody r8 = r5.m7056()     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x0027
            okhttp3.ResponseBody r8 = r5.m7056()     // Catch:{ Exception -> 0x00ff }
            r8.close()     // Catch:{ Exception -> 0x00ff }
        L_0x0027:
            if (r11 == 0) goto L_0x0048
            okhttp3.Request r8 = r11.m7069()     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x0048
            okhttp3.Request r8 = r11.m7069()     // Catch:{ Exception -> 0x00ff }
            okhttp3.HttpUrl r8 = r8.m7053()     // Catch:{ Exception -> 0x00ff }
            java.lang.String r7 = r8.toString()     // Catch:{ Exception -> 0x00ff }
            boolean r8 = r7.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x0048
            boolean r8 = r10.m6353((java.lang.String) r12, (java.lang.String) r7)     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x0048
            r6 = r7
        L_0x0048:
            boolean r8 = r6.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x0054
            boolean r8 = r10.m6353((java.lang.String) r6, (java.lang.String) r12)     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x0080
        L_0x0054:
            if (r5 == 0) goto L_0x0080
            java.lang.String r8 = "Location"
            java.lang.String r9 = ""
            java.lang.String r4 = r5.m7068(r8, r9)     // Catch:{ Exception -> 0x00ff }
            if (r4 == 0) goto L_0x0068
            boolean r8 = r4.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x0071
        L_0x0068:
            java.lang.String r8 = "location"
            java.lang.String r9 = ""
            r5.m7068(r8, r9)     // Catch:{ Exception -> 0x00ff }
        L_0x0071:
            if (r4 == 0) goto L_0x0080
            boolean r8 = r4.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x0080
            boolean r8 = r10.m6353((java.lang.String) r12, (java.lang.String) r4)     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x0080
            r6 = r4
        L_0x0080:
            if (r14 == 0) goto L_0x00ae
            boolean r8 = r6.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x008e
            boolean r8 = r10.m6353((java.lang.String) r12, (java.lang.String) r6)     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x00ae
        L_0x008e:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ Throwable -> 0x0107 }
            r1.<init>()     // Catch:{ Throwable -> 0x0107 }
            if (r15 == 0) goto L_0x0098
            r1.putAll(r15)     // Catch:{ Throwable -> 0x0107 }
        L_0x0098:
            boolean r8 = com.typhoon.tv.utils.SourceObservableUtils.m17832((java.lang.String) r12)     // Catch:{ Throwable -> 0x0107 }
            if (r8 != 0) goto L_0x00fd
            r3 = 0
        L_0x009f:
            if (r13 != 0) goto L_0x00aa
            java.lang.String r8 = "Range"
            java.lang.String r9 = "bytes=0-1"
            r1.put(r8, r9)     // Catch:{ Throwable -> 0x0107 }
        L_0x00aa:
            java.lang.String r6 = com.mopub.common.util.Network.getRedirectLocation(r12, r3, r1)     // Catch:{ Throwable -> 0x0107 }
        L_0x00ae:
            if (r6 == 0) goto L_0x00bc
            boolean r8 = r6.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x00bc
            boolean r8 = r10.m6353((java.lang.String) r12, (java.lang.String) r6)     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x00e9
        L_0x00bc:
            if (r11 == 0) goto L_0x00e9
            java.lang.String r8 = "Location"
            java.lang.String r9 = ""
            java.lang.String r2 = r11.m7068(r8, r9)     // Catch:{ Exception -> 0x00ff }
            if (r2 == 0) goto L_0x00d0
            boolean r8 = r2.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x00da
        L_0x00d0:
            java.lang.String r8 = "location"
            java.lang.String r9 = ""
            java.lang.String r2 = r11.m7068(r8, r9)     // Catch:{ Exception -> 0x00ff }
        L_0x00da:
            if (r2 == 0) goto L_0x00e9
            boolean r8 = r2.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x00e9
            boolean r8 = r10.m6353((java.lang.String) r12, (java.lang.String) r2)     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x00e9
            r6 = r2
        L_0x00e9:
            if (r6 == 0) goto L_0x00f7
            boolean r8 = r6.isEmpty()     // Catch:{ Exception -> 0x00ff }
            if (r8 != 0) goto L_0x00f7
            boolean r8 = r10.m6353((java.lang.String) r12, (java.lang.String) r6)     // Catch:{ Exception -> 0x00ff }
            if (r8 == 0) goto L_0x00f8
        L_0x00f7:
            r6 = r12
        L_0x00f8:
            r12 = r6
        L_0x00f9:
            return r12
        L_0x00fa:
            r5 = 0
            goto L_0x0006
        L_0x00fd:
            r3 = r13
            goto L_0x009f
        L_0x00ff:
            r0 = move-exception
            r8 = 0
            boolean[] r8 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r8)
            goto L_0x00f9
        L_0x0107:
            r8 = move-exception
            goto L_0x00ae
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.helper.http.HttpHelper.m6365(okhttp3.Response, java.lang.String, boolean, boolean, java.util.Map):java.lang.String");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6353(String url1, String url2) {
        return m6371(HttpUrl.m6937(url1), HttpUrl.m6937(url2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6371(HttpUrl url1, HttpUrl url2) {
        if (url1 == null || url2 == null) {
            return false;
        }
        boolean isEncodedQueryMatch = false;
        try {
            if (url1.m6959() == null && url2.m6959() == null) {
                isEncodedQueryMatch = true;
            } else if (!(url1.m6959() == null || url2.m6959() == null || !url1.m6959().equals(url2.m6959()))) {
                isEncodedQueryMatch = true;
            }
            boolean isEncodedFragmentMatch = false;
            if (url1.m6954() == null && url2.m6954() == null) {
                isEncodedFragmentMatch = true;
            } else if (!(url1.m6954() == null || url2.m6954() == null || !url1.m6954().equals(url2.m6954()))) {
                isEncodedFragmentMatch = true;
            }
            if (!url1.m6962().equals(url2.m6962()) || !url1.m6960().equals(url2.m6960()) || !url1.m6951().equals(url2.m6951()) || url1.m6952() != url2.m6952() || !url1.m6953().equals(url2.m6953()) || !isEncodedQueryMatch || !isEncodedFragmentMatch) {
                return false;
            }
            return true;
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                return url1.toString().equals(url2.toString());
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return false;
            }
        }
    }

    @SafeVarargs
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m6350(String url, String data, Map<String, String>... extraHeaders) {
        return m6344(url, RequestBody.create(MediaType.m6996(OAuth.ContentType.URL_ENCODED), data), true, extraHeaders);
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    public final String m6360(String url, String data, boolean forceForm, Map<String, String>... extraHeaders) {
        RequestBody body = null;
        if (forceForm) {
            body = RequestBody.create(MediaType.m6996(OAuth.ContentType.URL_ENCODED), data);
        } else if (extraHeaders.length > 0) {
            Map<String, String> map = extraHeaders[0];
            if (map.containsKey(OAuth.HeaderType.CONTENT_TYPE)) {
                body = RequestBody.create(MediaType.m6996(map.get(OAuth.HeaderType.CONTENT_TYPE)), data);
            } else if (map.containsKey("content-type")) {
                body = RequestBody.create(MediaType.m6996(map.get("content-type")), data);
            }
        }
        if (body == null) {
            body = RequestBody.create(MediaType.m6996(OAuth.ContentType.URL_ENCODED), data);
        }
        return m6344(url, body, forceForm, extraHeaders);
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    private final String m6344(String url, RequestBody body, boolean forceForm, Map<String, String>... extraHeaders) {
        if (url.isEmpty()) {
            return "";
        }
        Request.Builder reqBuilder = new Request.Builder().m19992(url).m19998(body).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        if (!(extraHeaders == null || extraHeaders.length <= 0 || extraHeaders[0] == null)) {
            for (Map.Entry<String, String> entry : extraHeaders[0].entrySet()) {
                if (entry.getKey().toLowerCase().equals("user-agent")) {
                    reqBuilder.m19993(AbstractSpiCall.HEADER_USER_AGENT, entry.getValue());
                } else {
                    reqBuilder.m19988(entry.getKey(), entry.getValue());
                }
            }
        }
        if (forceForm) {
            reqBuilder.m19993(OAuth.HeaderType.CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8");
        }
        return m6364(reqBuilder.m19989());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6347() {
        this.f5935.m7008().m6920();
        for (Call call : this.f5935.m7008().m6924()) {
            call.m19885();
        }
        for (Call call2 : this.f5935.m7008().m6923()) {
            call2.m19885();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6370(Object tag) {
        for (Call call : this.f5935.m7008().m6924()) {
            if (call.m19886().m7047().equals(tag)) {
                call.m19885();
            }
        }
        for (Call call2 : this.f5935.m7008().m6923()) {
            if (call2.m19886().m7047().equals(tag)) {
                call2.m19885();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6349(String url) {
        HttpUrl httpUrl = HttpUrl.m6937(url);
        StringBuilder cookieString = new StringBuilder();
        if (httpUrl != null) {
            for (Cookie cookie : this.f5935.m7004().m19909(httpUrl)) {
                cookieString.append(cookie.m6913()).append("=").append(cookie.m6910()).append(";");
            }
        }
        return cookieString.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m6357(String url, String cookieString) {
        if (!url.isEmpty() && !cookieString.isEmpty()) {
            if (!url.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                url = url + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
            }
            HttpUrl httpUrl = HttpUrl.m6937(url);
            if (httpUrl != null) {
                CookieJar jar = this.f5935.m7004();
                for (String splitCookieString : cookieString.contains("|||") ? cookieString.split("\\|\\|\\|") : new String[]{cookieString}) {
                    Cookie cookie = Cookie.m6903(httpUrl, splitCookieString);
                    if (cookie != null) {
                        List<Cookie> cookieList = new ArrayList<>();
                        cookieList.add(cookie);
                        jar.m19910(httpUrl, cookieList);
                    }
                }
            }
        }
    }
}
