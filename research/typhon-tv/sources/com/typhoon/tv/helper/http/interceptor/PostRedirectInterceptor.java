package com.typhoon.tv.helper.http.interceptor;

import com.mopub.common.TyphoonApp;
import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.http.HttpHelper;
import java.io.IOException;
import java.util.Map;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.oltu.oauth2.common.OAuth;

public class PostRedirectInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request r10 = chain.m6994();
        Response r1 = chain.m6995(r10);
        try {
            int r11 = r1.m7066();
            if (r10.m7048().equalsIgnoreCase(OAuth.HttpMethod.POST) && (r11 == 301 || r11 == 302 || r11 == 307 || r11 == 308)) {
                String r9 = HttpHelper.m6343().m6365(r1, r10.m7053().toString(), false, false, (Map<String, String>) null);
                try {
                    if (r9.startsWith(TyphoonApp.HTTP)) {
                        HttpUrl r7 = HttpUrl.m6937(r9);
                    } else {
                        HttpUrl r72 = r10.m7053().m6964(r9);
                    }
                    r1 = chain.m6995(r10.m7044().m19992(r9).m19989());
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        return r1;
    }
}
