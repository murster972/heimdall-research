package com.typhoon.tv.helper.http.sucuri;

import android.util.Base64;
import com.squareup.duktape.Duktape;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.CacheControl;
import okhttp3.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SucuriCloudProxyHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    private static String m16015(String str) {
        if (!TyphoonApp.f5859) {
            return "";
        }
        Duktape duktape = null;
        Object obj = null;
        try {
            duktape = Duktape.create();
            obj = duktape.evaluate(m16017(str));
            if (duktape != null) {
                duktape.close();
            }
        } catch (Throwable th) {
            if (duktape != null) {
                duktape.close();
            }
            throw th;
        }
        if (obj == null) {
            return "";
        }
        String obj2 = obj.toString();
        try {
            return new String(Base64.decode(obj2, 0), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                return new String(Base64.decode(obj2, 0));
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return "";
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m16016(String str, String str2) {
        Iterator it2 = Jsoup.m21674(str2).m21815("script").iterator();
        while (it2.hasNext()) {
            String r3 = ((Element) it2.next()).m21822();
            if (m16019(r3)) {
                HttpHelper.m6343().m6357(str, m16015(r3));
                return true;
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m16017(String str) {
        return "document = {cookie: \"\"};\nlocation = {reload: function(){/*Empty method of location.reload()*/}};\n\n{jsCode};\n\nDuktape.enc('base64', document.cookie.toString());".replace("{jsCode}", str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m16018(String str, String str2) {
        String r0 = HttpHelper.m6343().m6351(str2, (Map<String, String>[]) new Map[0]);
        if (!m16016(str, r0)) {
            return r0;
        }
        return HttpHelper.m6343().m6364(new Request.Builder().m19990().m19992(str2).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835).m19993("Referer", str).m19993("Host", str.replace("https://", "").replace("http://", "").replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "")).m19988("Cookie", HttpHelper.m6343().m6349(str2)).m19993(AbstractSpiCall.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8").m19993("Accept-Language", "en-US;q=0.6,en;q=0.4").m19995(CacheControl.f6219).m19989());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m16019(String str) {
        return str.contains("sucuri_cloudproxy_js");
    }
}
