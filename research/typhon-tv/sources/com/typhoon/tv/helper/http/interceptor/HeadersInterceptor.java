package com.typhoon.tv.helper.http.interceptor;

import com.typhoon.tv.Logger;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeadersInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        boolean z = false;
        Request r5 = chain.m6994();
        Request.Builder r4 = r5.m7044();
        try {
            String r0 = r5.m7052(AbstractSpiCall.HEADER_ACCEPT);
            if (r0 == null || r0.trim().isEmpty()) {
                r0 = r5.m7052("accept");
            }
            if (r0 == null || r0.trim().isEmpty()) {
                r4.m19993(AbstractSpiCall.HEADER_ACCEPT, "*/*");
                z = true;
            }
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        try {
            String r1 = r5.m7052("Accept-Language");
            if (r1 == null || r1.trim().isEmpty()) {
                r1 = r5.m7052("accept-language");
            }
            if (r1 == null || r1.trim().isEmpty()) {
                r4.m19993("Accept-Language", "en-US;q=0.6,en;q=0.4");
                z = true;
            }
        } catch (Throwable th2) {
            Logger.m6281(th2, new boolean[0]);
        }
        if (z) {
            r5 = r4.m19989();
        }
        return chain.m6995(r5);
    }
}
