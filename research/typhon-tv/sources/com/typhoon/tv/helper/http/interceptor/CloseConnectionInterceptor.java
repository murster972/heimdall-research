package com.typhoon.tv.helper.http.interceptor;

import com.typhoon.tv.Logger;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CloseConnectionInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        boolean z = false;
        Request r2 = chain.m6994();
        String r4 = r2.m7052("X-Request-CC");
        if (r4 != null && !r4.isEmpty() && r4.equalsIgnoreCase("true")) {
            z = true;
        }
        Response r3 = chain.m6995(r2.m7044().m19987("X-Request-CC").m19989());
        if (z) {
            try {
                r3 = r3.m7060().m7073("X-Request-CC").m7080("Connection", "close").m7087();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        } else {
            try {
                r3 = r3.m7060().m7073("X-Request-CC").m7087();
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
        }
        return r3;
    }
}
