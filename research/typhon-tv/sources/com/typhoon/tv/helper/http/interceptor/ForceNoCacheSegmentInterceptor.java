package com.typhoon.tv.helper.http.interceptor;

import com.typhoon.tv.Logger;
import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ForceNoCacheSegmentInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response r3;
        Request r0 = chain.m6994();
        try {
            if (r0.m7053().toString().contains("##forceNoCache##")) {
                r3 = chain.m6995(r0.m7044().m19992(r0.m7053().toString().replace("##forceNoCache##", "")).m19987("Pragma").m19987("C3-Cache-Control").m19987("X-Cache").m19987("X-Cache-Hit").m19987("pragma").m19987("c3-cache-control").m19987("x-cache").m19987("x-cache-hit").m19995(CacheControl.f6219).m19989());
                return r3;
            }
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        r3 = chain.m6995(r0);
        return r3;
    }
}
