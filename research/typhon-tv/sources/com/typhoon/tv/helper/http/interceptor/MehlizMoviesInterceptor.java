package com.typhoon.tv.helper.http.interceptor;

import java.io.EOFException;
import okhttp3.Interceptor;
import okio.Buffer;

public class MehlizMoviesInterceptor implements Interceptor {
    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m16014(Buffer buffer) {
        boolean z;
        long j = 64;
        try {
            Buffer buffer2 = new Buffer();
            if (buffer.m7242() < 64) {
                j = buffer.m7242();
            }
            buffer.m7269(buffer2, 0, j);
            int i = 0;
            while (true) {
                if (i < 16 && !buffer2.m7210()) {
                    int r6 = buffer2.m7226();
                    if (Character.isISOControl(r6) && !Character.isWhitespace(r6)) {
                        z = false;
                        break;
                    }
                    i++;
                } else {
                    z = true;
                }
            }
            z = true;
        } catch (EOFException e) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x015d, code lost:
        if (r21.contains("/cdn-cgi/l/chk_captcha") == false) goto L_0x015f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0033, code lost:
        if (r38.contains("mehlizmovies.") != false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Response intercept(okhttp3.Interceptor.Chain r41) throws java.io.IOException {
        /*
            r40 = this;
            okhttp3.Request r23 = r41.m6994()
            r0 = r41
            r0 = r41
            r1 = r23
            r1 = r23
            okhttp3.Response r24 = r0.m6995(r1)
            okhttp3.HttpUrl r22 = r23.m7053()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r38 = r22.toString()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = "mehlizmovieshd."
            r0 = r38
            r0 = r38
            boolean r2 = r0.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x0035
            java.lang.String r2 = "mehlizmovies."
            java.lang.String r2 = "mehlizmovies."
            r0 = r38
            r0 = r38
            boolean r2 = r0.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x026f
        L_0x0035:
            java.lang.String r2 = "video."
            r0 = r38
            boolean r2 = r0.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "/cdn-cgi/l/chk_jschl"
            r0 = r38
            r0 = r38
            boolean r2 = r0.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "Content-Type"
            r5 = 0
            r0 = r24
            r0 = r24
            java.lang.String r15 = r0.m7068(r2, r5)     // Catch:{ Throwable -> 0x0264 }
            if (r15 != 0) goto L_0x0079
            java.lang.String r2 = "content-type"
            java.lang.String r2 = "content-type"
            r5 = 0
            r0 = r24
            java.lang.String r15 = r0.m7068(r2, r5)     // Catch:{ Throwable -> 0x0264 }
            if (r15 != 0) goto L_0x0079
            java.lang.String r2 = "Content-type"
            java.lang.String r2 = "Content-type"
            r5 = 0
            r0 = r24
            r0 = r24
            java.lang.String r15 = r0.m7068(r2, r5)     // Catch:{ Throwable -> 0x0264 }
        L_0x0079:
            if (r15 == 0) goto L_0x026f
            java.lang.String r15 = r15.toLowerCase()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = " "
            java.lang.String r2 = " "
            java.lang.String r5 = ""
            java.lang.String r5 = ""
            java.lang.String r2 = r15.replace(r2, r5)     // Catch:{ Throwable -> 0x0264 }
            boolean r2 = r2.isEmpty()     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "video"
            boolean r2 = r15.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "binary"
            java.lang.String r2 = "binary"
            boolean r2 = r15.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "octet"
            java.lang.String r2 = "octet"
            boolean r2 = r15.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "stream"
            boolean r2 = r15.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            java.lang.String r2 = "image"
            boolean r2 = r15.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
            okhttp3.ResponseBody r33 = r24.m7056()     // Catch:{ Throwable -> 0x0264 }
            if (r33 == 0) goto L_0x026f
            okio.BufferedSource r35 = r33.m7095()     // Catch:{ Throwable -> 0x0264 }
            r6 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0 = r35
            r0.m20464(r6)     // Catch:{ Throwable -> 0x0264 }
            okio.Buffer r10 = r35.m20466()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = "UTF-8"
            java.nio.charset.Charset r8 = java.nio.charset.Charset.forName(r2)     // Catch:{ Throwable -> 0x0264 }
            r13 = r8
            r13 = r8
            okhttp3.MediaType r14 = r33.m7096()     // Catch:{ Throwable -> 0x0264 }
            if (r14 == 0) goto L_0x00f7
            java.nio.charset.Charset r36 = r14.m6999((java.nio.charset.Charset) r8)     // Catch:{ Throwable -> 0x0264 }
            if (r36 == 0) goto L_0x00f7
            r13 = r36
        L_0x00f7:
            boolean r2 = m16014(r10)     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x026f
            okio.Buffer r2 = r10.clone()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r21 = r2.m7264((java.nio.charset.Charset) r13)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = r21.toLowerCase()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "validate that you"
            boolean r2 = r2.contains(r5)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x015f
            java.lang.String r2 = r21.toLowerCase()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "are indeed human"
            boolean r2 = r2.contains(r5)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x015f
            java.lang.String r2 = r21.toLowerCase()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "something wrong with your browser"
            java.lang.String r5 = "something wrong with your browser"
            boolean r2 = r2.contains(r5)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x015f
            java.lang.String r2 = r21.toLowerCase()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "check the box to enter"
            java.lang.String r5 = "check the box to enter"
            boolean r2 = r2.contains(r5)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x015f
            java.lang.String r2 = "captcha-verification"
            java.lang.String r2 = "captcha-verification"
            r0 = r21
            r0 = r21
            boolean r2 = r0.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x026f
            java.lang.String r2 = "/cdn-cgi/l/chk_captcha"
            java.lang.String r2 = "/cdn-cgi/l/chk_captcha"
            r0 = r21
            r0 = r21
            boolean r2 = r0.contains(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x026f
        L_0x015f:
            okhttp3.Request r2 = r24.m7069()     // Catch:{ Throwable -> 0x0264 }
            okhttp3.HttpUrl r32 = r2.m7053()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r34 = r32.m6961()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r20 = r32.m6951()     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0264 }
            r2.<init>()     // Catch:{ Throwable -> 0x0264 }
            r0 = r34
            r0 = r34
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "://"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Throwable -> 0x0264 }
            r0 = r20
            r0 = r20
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r9 = r2.toString()     // Catch:{ Throwable -> 0x0264 }
            org.jsoup.nodes.Document r16 = org.jsoup.Jsoup.m21674(r21)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = "form[action]"
            r0 = r16
            r0 = r16
            org.jsoup.nodes.Element r18 = r0.m21837(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r18 == 0) goto L_0x0274
            java.lang.String r2 = "action"
            java.lang.String r2 = "action"
            r0 = r18
            r0 = r18
            java.lang.String r12 = r0.m21947(r2)     // Catch:{ Throwable -> 0x0264 }
        L_0x01ae:
            boolean r2 = r12.isEmpty()     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x0279
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0264 }
            r2.<init>()     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "/wp-content/plugins/spam-detection21/captcha-verification.php"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r12 = r2.toString()     // Catch:{ Throwable -> 0x0264 }
        L_0x01c8:
            java.lang.String r11 = ""
            java.lang.String r11 = ""
            if (r18 == 0) goto L_0x0339
            java.lang.StringBuilder r29 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0264 }
            r29.<init>()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = "input"
            java.lang.String r2 = "input"
            r0 = r18
            r0 = r18
            org.jsoup.select.Elements r2 = r0.m21815(r2)     // Catch:{ Throwable -> 0x0264 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ Throwable -> 0x0264 }
        L_0x01e7:
            boolean r5 = r2.hasNext()     // Catch:{ Throwable -> 0x0264 }
            if (r5 == 0) goto L_0x031e
            java.lang.Object r19 = r2.next()     // Catch:{ Throwable -> 0x0264 }
            org.jsoup.nodes.Element r19 = (org.jsoup.nodes.Element) r19     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "submit"
            java.lang.String r5 = "submit"
            java.lang.String r6 = "type"
            r0 = r19
            r0 = r19
            java.lang.String r6 = r0.m21947(r6)     // Catch:{ Throwable -> 0x0264 }
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x0264 }
            if (r5 != 0) goto L_0x01e7
            java.lang.String r5 = "checkbox"
            java.lang.String r5 = "checkbox"
            java.lang.String r6 = "type"
            java.lang.String r6 = "type"
            r0 = r19
            r0 = r19
            java.lang.String r6 = r0.m21947(r6)     // Catch:{ Throwable -> 0x0264 }
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x0264 }
            if (r5 != 0) goto L_0x023c
            java.lang.String r5 = "radio"
            java.lang.String r5 = "radio"
            java.lang.String r6 = "type"
            java.lang.String r6 = "type"
            r0 = r19
            java.lang.String r6 = r0.m21947(r6)     // Catch:{ Throwable -> 0x0264 }
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x0264 }
            if (r5 == 0) goto L_0x02da
        L_0x023c:
            java.lang.String r5 = "name"
            java.lang.String r5 = "name"
            r0 = r19
            java.lang.String r25 = r0.m21947(r5)     // Catch:{ Throwable -> 0x0264 }
            boolean r5 = r25.isEmpty()     // Catch:{ Throwable -> 0x0264 }
            if (r5 != 0) goto L_0x01e7
            r0 = r29
            r0 = r29
            r1 = r25
            r1 = r25
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r6 = "=on&"
            java.lang.String r6 = "=on&"
            r5.append(r6)     // Catch:{ Throwable -> 0x0264 }
            goto L_0x01e7
        L_0x0264:
            r37 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            r0 = r37
            r0 = r37
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r2)
        L_0x026f:
            r3 = r24
            r3 = r24
        L_0x0273:
            return r3
        L_0x0274:
            java.lang.String r12 = ""
            goto L_0x01ae
        L_0x0279:
            java.lang.String r2 = "//"
            boolean r2 = r12.startsWith(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x0298
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0264 }
            r2.<init>()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "http:"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r12 = r2.toString()     // Catch:{ Throwable -> 0x0264 }
            goto L_0x01c8
        L_0x0298:
            java.lang.String r2 = "/"
            boolean r2 = r12.startsWith(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x02b4
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0264 }
            r2.<init>()     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r12 = r2.toString()     // Catch:{ Throwable -> 0x0264 }
            goto L_0x01c8
        L_0x02b4:
            java.lang.String r2 = "http"
            java.lang.String r2 = "http"
            boolean r2 = r12.startsWith(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 != 0) goto L_0x01c8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0264 }
            r2.<init>()     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r12 = r2.toString()     // Catch:{ Throwable -> 0x0264 }
            goto L_0x01c8
        L_0x02da:
            java.lang.String r5 = "name"
            java.lang.String r5 = "name"
            r0 = r19
            java.lang.String r25 = r0.m21947(r5)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "value"
            r0 = r19
            java.lang.String r39 = r0.m21947(r5)     // Catch:{ Throwable -> 0x0264 }
            boolean r5 = r25.isEmpty()     // Catch:{ Throwable -> 0x0264 }
            if (r5 != 0) goto L_0x01e7
            r0 = r29
            r1 = r25
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r6 = "="
            java.lang.String r6 = "="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x0264 }
            r6 = 0
            boolean[] r6 = new boolean[r6]     // Catch:{ Throwable -> 0x0264 }
            r0 = r39
            r0 = r39
            java.lang.String r6 = com.typhoon.tv.utils.Utils.m6414((java.lang.String) r0, (boolean[]) r6)     // Catch:{ Throwable -> 0x0264 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r6 = "&"
            r5.append(r6)     // Catch:{ Throwable -> 0x0264 }
            goto L_0x01e7
        L_0x031e:
            java.lang.String r11 = r29.toString()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = "&"
            java.lang.String r2 = "&"
            boolean r2 = r11.endsWith(r2)     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x0339
            r2 = 0
            int r5 = r11.length()     // Catch:{ Throwable -> 0x0264 }
            int r5 = r5 + -1
            java.lang.String r11 = r11.substring(r2, r5)     // Catch:{ Throwable -> 0x0264 }
        L_0x0339:
            boolean r2 = r11.isEmpty()     // Catch:{ Throwable -> 0x0264 }
            if (r2 == 0) goto L_0x0345
            java.lang.String r11 = "verification_box=on"
            java.lang.String r11 = "verification_box=on"
        L_0x0345:
            java.lang.String r31 = r32.toString()     // Catch:{ Throwable -> 0x0264 }
            okhttp3.Request$Builder r2 = r23.m7044()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "application/x-www-form-urlencoded"
            java.lang.String r5 = "application/x-www-form-urlencoded"
            okhttp3.MediaType r5 = okhttp3.MediaType.m6996((java.lang.String) r5)     // Catch:{ Throwable -> 0x0264 }
            okhttp3.RequestBody r5 = okhttp3.RequestBody.create((okhttp3.MediaType) r5, (java.lang.String) r11)     // Catch:{ Throwable -> 0x0264 }
            okhttp3.Request$Builder r2 = r2.m19998((okhttp3.RequestBody) r5)     // Catch:{ Throwable -> 0x0264 }
            okhttp3.Request$Builder r2 = r2.m19992((java.lang.String) r12)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "Referer"
            java.lang.String r5 = "Referer"
            r0 = r31
            okhttp3.Request$Builder r2 = r2.m19993((java.lang.String) r5, (java.lang.String) r0)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r5 = "Content-Type"
            java.lang.String r5 = "Content-Type"
            java.lang.String r6 = "application/x-www-form-urlencoded"
            okhttp3.Request$Builder r2 = r2.m19993((java.lang.String) r5, (java.lang.String) r6)     // Catch:{ Throwable -> 0x0264 }
            okhttp3.CacheControl r5 = okhttp3.CacheControl.f6219     // Catch:{ Throwable -> 0x0264 }
            okhttp3.Request$Builder r2 = r2.m19995((okhttp3.CacheControl) r5)     // Catch:{ Throwable -> 0x0264 }
            okhttp3.Request r27 = r2.m19989()     // Catch:{ Throwable -> 0x0264 }
            r0 = r41
            r0 = r41
            r1 = r27
            okhttp3.Response r3 = r0.m6995(r1)     // Catch:{ Throwable -> 0x0264 }
            int r28 = r3.m7066()     // Catch:{ Throwable -> 0x0264 }
            r2 = 301(0x12d, float:4.22E-43)
            r0 = r28
            r0 = r28
            if (r0 == r2) goto L_0x03b2
            r2 = 302(0x12e, float:4.23E-43)
            r0 = r28
            r0 = r28
            if (r0 == r2) goto L_0x03b2
            r2 = 307(0x133, float:4.3E-43)
            r0 = r28
            r0 = r28
            if (r0 == r2) goto L_0x03b2
            r2 = 308(0x134, float:4.32E-43)
            r0 = r28
            if (r0 != r2) goto L_0x0273
        L_0x03b2:
            okhttp3.HttpUrl r2 = r27.m7053()     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r4 = r2.toString()     // Catch:{ Throwable -> 0x0264 }
            com.typhoon.tv.helper.http.HttpHelper r2 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Throwable -> 0x0264 }
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r30 = r2.m6365(r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0264 }
            java.lang.String r2 = "http"
            r0 = r30
            r0 = r30
            boolean r2 = r0.startsWith(r2)     // Catch:{ Exception -> 0x0430 }
            if (r2 == 0) goto L_0x0423
            okhttp3.HttpUrl r26 = okhttp3.HttpUrl.m6937(r30)     // Catch:{ Exception -> 0x0430 }
        L_0x03d6:
            okhttp3.Request$Builder r2 = r27.m7044()     // Catch:{ Exception -> 0x0430 }
            okhttp3.Request$Builder r2 = r2.m19990()     // Catch:{ Exception -> 0x0430 }
            r0 = r30
            r0 = r30
            okhttp3.Request$Builder r2 = r2.m19992((java.lang.String) r0)     // Catch:{ Exception -> 0x0430 }
            java.lang.String r5 = "Referer"
            java.lang.String r5 = "Referer"
            r0 = r30
            r0 = r30
            okhttp3.Request$Builder r2 = r2.m19993((java.lang.String) r5, (java.lang.String) r0)     // Catch:{ Exception -> 0x0430 }
            java.lang.String r5 = "Content-Type"
            okhttp3.Request$Builder r2 = r2.m19987(r5)     // Catch:{ Exception -> 0x0430 }
            java.lang.String r5 = "Content-type"
            java.lang.String r5 = "Content-type"
            okhttp3.Request$Builder r2 = r2.m19987(r5)     // Catch:{ Exception -> 0x0430 }
            java.lang.String r5 = "content-type"
            java.lang.String r5 = "content-type"
            okhttp3.Request$Builder r2 = r2.m19987(r5)     // Catch:{ Exception -> 0x0430 }
            okhttp3.CacheControl r5 = okhttp3.CacheControl.f6219     // Catch:{ Exception -> 0x0430 }
            okhttp3.Request$Builder r2 = r2.m19995((okhttp3.CacheControl) r5)     // Catch:{ Exception -> 0x0430 }
            okhttp3.Request r2 = r2.m19989()     // Catch:{ Exception -> 0x0430 }
            r0 = r41
            r0 = r41
            okhttp3.Response r3 = r0.m6995(r2)     // Catch:{ Exception -> 0x0430 }
            goto L_0x0273
        L_0x0423:
            okhttp3.HttpUrl r2 = r27.m7053()     // Catch:{ Exception -> 0x0430 }
            r0 = r30
            r0 = r30
            okhttp3.HttpUrl r26 = r2.m6964(r0)     // Catch:{ Exception -> 0x0430 }
            goto L_0x03d6
        L_0x0430:
            r17 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]     // Catch:{ Throwable -> 0x0264 }
            r0 = r17
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ Throwable -> 0x0264 }
            goto L_0x0273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.helper.http.interceptor.MehlizMoviesInterceptor.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }
}
