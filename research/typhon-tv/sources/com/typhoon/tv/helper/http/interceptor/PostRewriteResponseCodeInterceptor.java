package com.typhoon.tv.helper.http.interceptor;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.oltu.oauth2.common.OAuth;

public class PostRewriteResponseCodeInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request r0 = chain.m6994();
        Response r1 = chain.m6995(r0);
        if (r0.m7048().equalsIgnoreCase(OAuth.HttpMethod.POST) && (r1.m7066() == 301 || r1.m7066() == 302)) {
            r1 = r1.m7060().m7077(r1.m7066() == 301 ? 308 : 307).m7087();
        }
        return r1;
    }
}
