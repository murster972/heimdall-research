package com.typhoon.tv.helper;

import com.typhoon.tv.Logger;
import java.util.Date;
import java.util.Locale;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;

public class DateTimeHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m15932() {
        return DateTime.now().toDateTime(DateTimeZone.forID("UTC")).toString("yyyy-MM-dd", Locale.US);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m15933(DateTime dateTime) {
        if (dateTime == null) {
            return true;
        }
        DateTime dateTime2 = DateTime.now().toDateTime(DateTimeZone.forID("UTC"));
        return dateTime.isBefore((ReadableInstant) dateTime2) || dateTime.isEqual((ReadableInstant) dateTime2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15934() {
        try {
            return String.valueOf(new Date().getTime());
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return "";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15935(int i) {
        return DateTime.now().toDateTime(DateTimeZone.forID("UTC")).minusDays(Math.abs(i)).toString("yyyy-MM-dd", Locale.US);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15936(DateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return dateTime.toString(DateTimeFormat.m21228("yyyy-MM-dd"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTime m15937(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        return DateTimeFormat.m21228("yyyy-MM-dd").m21236(str);
    }
}
