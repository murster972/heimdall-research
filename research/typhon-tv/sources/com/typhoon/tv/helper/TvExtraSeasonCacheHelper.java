package com.typhoon.tv.helper;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.utils.Utils;
import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;

public class TvExtraSeasonCacheHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile TvExtraSeasonCacheHelper f12626;

    /* renamed from: 靐  reason: contains not printable characters */
    private DualCache<String> f12627;

    public TvExtraSeasonCacheHelper() {
        try {
            long r2 = Utils.m6396(TVApplication.m6288().getCacheDir());
            this.f12627 = new Builder("tv_extra_season_cache", Utils.m6393()).noRam().useSerializerInDisk(r2 > 2147483647L ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : (int) r2, true, new CacheSerializer<String>() {
                /* renamed from: 靐  reason: contains not printable characters */
                public String toString(String str) {
                    return str;
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public String fromString(String str) {
                    return str;
                }
            }, TVApplication.m6288()).build();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TvExtraSeasonCacheHelper m15973() {
        TvExtraSeasonCacheHelper tvExtraSeasonCacheHelper = f12626;
        if (tvExtraSeasonCacheHelper == null) {
            synchronized (TvExtraSeasonCacheHelper.class) {
                try {
                    tvExtraSeasonCacheHelper = f12626;
                    if (tvExtraSeasonCacheHelper == null) {
                        TvExtraSeasonCacheHelper tvExtraSeasonCacheHelper2 = new TvExtraSeasonCacheHelper();
                        try {
                            f12626 = tvExtraSeasonCacheHelper2;
                            tvExtraSeasonCacheHelper = tvExtraSeasonCacheHelper2;
                        } catch (Throwable th) {
                            th = th;
                            TvExtraSeasonCacheHelper tvExtraSeasonCacheHelper3 = tvExtraSeasonCacheHelper2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return tvExtraSeasonCacheHelper;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m15974(int i, String str) {
        if (this.f12627 != null) {
            try {
                String str2 = this.f12627.get("tmdb-" + i);
                if (str2 == null || str2.isEmpty()) {
                    this.f12627.put("tmdb-" + i, DateTimeHelper.m15934() + "|" + str);
                } else {
                    this.f12627.put("tmdb-" + i, str2 + "-" + str);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized boolean m15975(int i, String str) {
        boolean z;
        if (this.f12627 == null) {
            z = false;
        } else {
            try {
                String str2 = "tmdb-" + i;
                String str3 = this.f12627.get(str2);
                if (str3 != null && !str3.isEmpty()) {
                    if (str3.contains("|")) {
                        String[] split = str3.split("\\|");
                        String str4 = split[0];
                        str3 = split[1];
                        String r4 = DateTimeHelper.m15934();
                        boolean z2 = false;
                        try {
                            if (!str4.isEmpty() && !r4.isEmpty()) {
                                if (Long.parseLong(r4) - Long.parseLong(str4) >= 2592000000L) {
                                    z2 = true;
                                }
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                        if (z2) {
                            this.f12627.delete(str2);
                            z = false;
                        }
                    }
                    if (str3.contains("-")) {
                        String[] split2 = str3.split("-");
                        int length = split2.length;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= length) {
                                break;
                            }
                            try {
                                if (Integer.parseInt(split2[i2]) == Integer.parseInt(str)) {
                                    z = true;
                                    break;
                                }
                                i2++;
                            } catch (Exception e2) {
                                Logger.m6281((Throwable) e2, new boolean[0]);
                            }
                        }
                    } else {
                        z = Integer.parseInt(str3) == Integer.parseInt(str);
                    }
                }
            } catch (Exception e3) {
                Logger.m6281((Throwable) e3, new boolean[0]);
            }
            z = false;
        }
        return z;
    }
}
