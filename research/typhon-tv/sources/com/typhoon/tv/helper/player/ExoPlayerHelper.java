package com.typhoon.tv.helper.player;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import com.google.android.exoplayer2.TTVWorkaroundUtils;
import com.mopub.mobileads.VastIconXmlManager;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity;
import java.util.ArrayList;

public class ExoPlayerHelper extends BasePlayerHelper {
    @SuppressLint({"ObsoleteSdkInt"})
    /* renamed from: ٴ  reason: contains not printable characters */
    public static boolean m16055() {
        return Build.VERSION.SDK_INT >= 16;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m16056() {
        return 9151;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m16057() {
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m16058() {
        return "Exo";
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m16059() {
        return "ExoPlayer (Built-in)";
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m16060() {
        return m16055();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m16061(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return intent.getLongExtra(VastIconXmlManager.DURATION, -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m16062(Activity activity) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m16063(Activity activity, MediaSource mediaSource, String str, long j) {
        try {
            System.gc();
        } catch (Throwable th) {
            Logger.m6281(th, true);
        }
        TTVWorkaroundUtils.disableAllWorkaround();
        return super.m16045(activity, mediaSource, str, j);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m16064(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        try {
            System.gc();
        } catch (Throwable th) {
            Logger.m6281(th, true);
        }
        TTVWorkaroundUtils.disableAllWorkaround();
        return super.m16046(activity, mediaSource, str, j, arrayList, arrayList2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m16065(AppCompatActivity appCompatActivity, int i, int i2, Intent intent) {
        try {
            System.gc();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        if (i != m16056()) {
            return -1;
        }
        if (i2 == 1) {
            String r0 = I18N.m15706(R.string.error);
            boolean z = false;
            if (!(intent == null || intent.getExtras() == null)) {
                r0 = intent.getExtras().getString("errorMsg", r0);
                z = intent.getExtras().getBoolean("isNetworkErr", false);
            }
            m16053(appCompatActivity, (r0 + "\r\n\r\n") + (z ? "The link is broken. Please try another link." : "Please switch to MX Player and try again."), (Uri) null);
            return 1;
        }
        if (i2 == -1) {
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m16066(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return intent.getLongExtra("lastPosition", -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16067(Activity activity, MediaSource mediaSource, String str, long j) {
        Intent intent = new Intent(activity, ExoPlayerActivity.class);
        intent.putExtra("mediaSource", mediaSource);
        intent.putExtra("playTitle", str);
        intent.putExtra("position", j);
        intent.putExtra("hasSubtitles", false);
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16068(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        Intent intent = new Intent(activity, ExoPlayerActivity.class);
        intent.putExtra("mediaSource", mediaSource);
        intent.putExtra("playTitle", str);
        intent.putExtra("position", j);
        intent.putExtra("hasSubtitles", true);
        intent.putStringArrayListExtra("subtitlePaths", arrayList);
        intent.putStringArrayListExtra("subtitleTitles", arrayList2);
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16069(Context context) {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16070(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle("Unable to start player");
            r0.m519((CharSequence) "Typhoon is unable to start its built-in player (ExoPlayer). Please consider to use MX Player or VLC Player instead");
            r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }
}
