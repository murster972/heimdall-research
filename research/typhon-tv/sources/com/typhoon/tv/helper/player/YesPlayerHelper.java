package com.typhoon.tv.helper.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.mobileads.VastIconXmlManager;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.ui.activity.SourceActivity;
import com.typhoon.tv.ui.activity.SubtitlesActivity;
import com.typhoon.tv.ui.activity.base.BaseActivity;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.GmsUtils;
import com.typhoon.tv.utils.SourceUtils;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;

public class YesPlayerHelper extends BasePlayerHelper {
    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m16096() {
        return 32123;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m16097() {
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m16098() {
        return "Yes";
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m16099() {
        return "YesPlayer (BEST!) (Recommended) (Less Video Ads!)";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m16100(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return intent.getLongExtra(VastIconXmlManager.DURATION, -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m16101(final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            final AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle(I18N.m15706(R.string.yesplayer_not_installed));
            r0.m519((CharSequence) I18N.m15706(R.string.yesplayer_not_installed_message));
            r0.m514(R.drawable.ic_error_white_36dp);
            r0.m517(-1, I18N.m15706(R.string.install), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!DeviceUtils.m6389(new boolean[0]) && GmsUtils.m6390()) {
                        Utils.m6402(activity, "com.Ty.yesplayer");
                    } else if (activity instanceof BaseActivity) {
                        ((BaseActivity) activity).m17303(false);
                    } else {
                        Toast.makeText(activity, "Failed to download YesPlayer. Please use ExoPlayer or MX Player instead.", 1).show();
                    }
                }
            });
            r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            r0.setOnShowListener(new DialogInterface.OnShowListener() {
                public void onShow(DialogInterface dialogInterface) {
                    try {
                        r0.m515(-1).setTextColor(Color.parseColor("#FFFFBB33"));
                        r0.m515(-2).setTextColor(Color.parseColor("#AAAAAA"));
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m16102(AppCompatActivity appCompatActivity, int i, int i2, Intent intent) {
        try {
            System.gc();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        if (i != m16096()) {
            return -1;
        }
        if (i2 == 1) {
            String r0 = I18N.m15706(R.string.error);
            boolean z = false;
            if (!(intent == null || intent.getExtras() == null)) {
                r0 = intent.getExtras().getString("errorMsg", r0);
                z = intent.getExtras().getBoolean("isNetworkErr", false);
            }
            m16053(appCompatActivity, (r0 + "\r\n\r\n") + (z ? "The link is broken. Please try another link." : "Please switch to ExoPlayer or MX Player and try again."), (Uri) null);
            return 1;
        }
        if (i2 == -1) {
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m16103(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return intent.getLongExtra("lastPosition", -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16104(Activity activity, MediaSource mediaSource, String str, long j) {
        String r8 = m16106((Context) activity);
        if (r8 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? MimeTypes.APPLICATION_M3U8 : "video/*");
        intent.setPackage(r8);
        intent.setClassName(r8, "com.Ty.yesplayer.ui.activity.exoplayer.ExoPlayerActivity");
        intent.putExtra("streamLink", streamLink);
        intent.putExtra("fromOtherApp", true);
        intent.putExtra("gdprScope", TyphoonApp.f5874);
        intent.putExtra("showStartupADM", TyphoonApp.f5868);
        intent.putExtra("showErrorMessage", false);
        intent.putExtra("hls", mediaSource.isHLS());
        intent.putExtra("providerName", mediaSource.getProviderName());
        intent.putExtra(PubnativeAsset.TITLE, str);
        intent.putExtra("hostAppName", TVApplication.m6288().getPackageName());
        intent.putExtra("forceATVMode", DeviceUtils.m6389(new boolean[0]));
        intent.putExtra("position", j);
        intent.putExtra("hasSubtitles", false);
        intent.putExtra("subsFontScale", TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f));
        intent.putExtra("subsFontColorHex", TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        intent.putExtra("subsBgColorHex", TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        try {
            if (activity instanceof SourceActivity) {
                intent.putExtra("isChildAppAdShown", ((SourceActivity) activity).m17126());
            } else if (activity instanceof SubtitlesActivity) {
                intent.putExtra("isChildAppAdShown", ((SubtitlesActivity) activity).m17203());
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        boolean z = true;
        try {
            if (activity instanceof BaseAdActivity) {
                z = ((BaseAdActivity) activity).m17420();
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        intent.putExtra("showHostAppAd", z && !BasePlayerHelper.m16037());
        intent.putExtra("disableAds", TyphoonApp.f5865 && !BasePlayerHelper.m16037());
        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
        if (playHeader == null || playHeader.size() <= 0) {
            return intent;
        }
        HashMap<String, String> r5 = SourceUtils.m17835(playHeader);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : r5.entrySet()) {
            arrayList.add(next.getKey());
            arrayList.add(next.getValue());
        }
        intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16105(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        String r8 = m16106((Context) activity);
        if (r8 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? MimeTypes.APPLICATION_M3U8 : "video/*");
        intent.setPackage(r8);
        intent.setClassName(r8, "com.Ty.yesplayer.ui.activity.exoplayer.ExoPlayerActivity");
        intent.putExtra("streamLink", streamLink);
        intent.putExtra("fromOtherApp", true);
        intent.putExtra("gdprScope", TyphoonApp.f5874);
        intent.putExtra("showStartupADM", TyphoonApp.f5868);
        intent.putExtra("showErrorMessage", false);
        intent.putExtra("hls", mediaSource.isHLS());
        intent.putExtra("providerName", mediaSource.getProviderName());
        intent.putExtra(PubnativeAsset.TITLE, str);
        intent.putExtra("hostAppName", TVApplication.m6288().getPackageName());
        intent.putExtra("forceATVMode", DeviceUtils.m6389(new boolean[0]));
        intent.putExtra("position", j);
        intent.putExtra("hasSubtitles", true);
        intent.putStringArrayListExtra("subtitlePaths", arrayList);
        intent.putStringArrayListExtra("subtitleTitles", arrayList2);
        intent.putExtra("subsFontScale", TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f));
        intent.putExtra("subsFontColorHex", TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        intent.putExtra("subsBgColorHex", TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        try {
            if (activity instanceof SourceActivity) {
                intent.putExtra("isChildAppAdShown", ((SourceActivity) activity).m17126());
            } else if (activity instanceof SubtitlesActivity) {
                intent.putExtra("isChildAppAdShown", ((SubtitlesActivity) activity).m17203());
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        boolean z = true;
        try {
            if (activity instanceof BaseAdActivity) {
                z = ((BaseAdActivity) activity).m17420();
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        intent.putExtra("showHostAppAd", z && !BasePlayerHelper.m16037());
        intent.putExtra("disableAds", TyphoonApp.f5865 && !BasePlayerHelper.m16037());
        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
        if (playHeader == null || playHeader.size() <= 0) {
            return intent;
        }
        HashMap<String, String> r5 = SourceUtils.m17835(playHeader);
        ArrayList arrayList3 = new ArrayList();
        for (Map.Entry next : r5.entrySet()) {
            arrayList3.add(next.getKey());
            arrayList3.add(next.getValue());
        }
        intent.putExtra("headers", (String[]) arrayList3.toArray(new String[arrayList3.size()]));
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16106(Context context) {
        if (Utils.m6424(context, "com.Ty.yesplayer")) {
            return "com.Ty.yesplayer";
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16107(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle(I18N.m15706(R.string.yesplayer_unable_to_start));
            r0.m519((CharSequence) I18N.m15706(R.string.yesplayer_unable_to_start_message));
            r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }
}
