package com.typhoon.tv.helper.player;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import com.google.android.exoplayer2.util.MimeTypes;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import net.pubnative.library.request.PubnativeAsset;

public class VLCPlayerHelper extends BasePlayerHelper {

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f12662 = false;

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m16084() {
        return 431;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m16085() {
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m16086() {
        return "VLC";
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m16087() {
        return "VLC Player";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m16088(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return intent.getLongExtra("extra_duration", -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m16089(final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle(I18N.m15706(R.string.vlc_player_not_installed));
            r0.m519((CharSequence) I18N.m15706(R.string.vlc_player_not_installed_message));
            r0.m517(-1, I18N.m15706(R.string.install), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utils.m6402(activity, "org.videolan.vlc");
                }
            });
            r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m16090(AppCompatActivity appCompatActivity, int i, int i2, Intent intent) {
        Uri data = intent != null ? intent.getData() : null;
        if (i != m16084()) {
            return -1;
        }
        switch (i2) {
            case 0:
                m16053(appCompatActivity, "No compatible cpu, incorrect VLC abi variant installed", data);
                return 1;
            case 2:
                m16053(appCompatActivity, "Connection failed to audio service", data);
                return 1;
            case 3:
                m16053(appCompatActivity, "VLC is not able to play this file, it could be incorrect path/uri, not supported codec or broken file", data);
                return 1;
            case 4:
                m16053(appCompatActivity, "Error with hardware acceleration, user refused to switch to software decoding", data);
                return 0;
            case 5:
                m16053(appCompatActivity, "VLC continues playback, but for audio track only. (Audio file detected or user chose to)", data);
                return 0;
            default:
                return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m16091(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return intent.getLongExtra("extra_position", -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16092(Activity activity, MediaSource mediaSource, String str, long j) {
        String r3 = m16094((Context) activity);
        if (r3 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? MimeTypes.APPLICATION_M3U8 : "video/*");
        intent.setPackage(r3);
        intent.setComponent(new ComponentName(r3, this.f12662 ? "org.videolan.vlc.gui.video.VideoPlayerActivity" : "org.videolan.vlc.gui.video.VideoPlayerActivity"));
        intent.putExtra("from_start", j == 0);
        intent.putExtra(PubnativeAsset.TITLE, str);
        if (j <= -1) {
            return intent;
        }
        intent.putExtra("position", j);
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16093(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        String r6 = m16094((Context) activity);
        if (r6 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), "video/*");
        intent.setPackage(r6);
        intent.setComponent(new ComponentName(r6, this.f12662 ? "org.videolan.vlc.gui.video.VideoPlayerActivity" : "org.videolan.vlc.gui.video.VideoPlayerActivity"));
        intent.putExtra("from_start", j == 0);
        intent.putExtra(PubnativeAsset.TITLE, str);
        if (j > -1) {
            intent.putExtra("position", j);
        }
        if (arrayList == null || arrayList.size() <= 0 || arrayList.get(0) == null) {
            return intent;
        }
        intent.putExtra("subtitles_location", arrayList.get(0));
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16094(Context context) {
        if (Utils.m6424(context, "org.videolan.vlc")) {
            return "org.videolan.vlc";
        }
        if (!Utils.m6424(context, "org.videolan.vlc.debug")) {
            return null;
        }
        this.f12662 = true;
        return "org.videolan.vlc.debug";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16095(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle(I18N.m15706(R.string.vlc_player_unable_to_start));
            r0.m519((CharSequence) I18N.m15706(R.string.vlc_player_unable_to_start_message));
            r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }
}
