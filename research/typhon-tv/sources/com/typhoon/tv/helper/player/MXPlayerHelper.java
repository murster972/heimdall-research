package com.typhoon.tv.helper.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.mobileads.VastIconXmlManager;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.utils.SourceUtils;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;

public class MXPlayerHelper extends BasePlayerHelper {
    /* renamed from: 龘  reason: contains not printable characters */
    private void m16071(Uri uri, Intent intent) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            Object[] objArr = new Object[1];
            objArr[0] = uri == null ? "null" : uri.toString().replace("\"", "\\\"");
            sb.append(String.format("\"mediaUri\": \"%s\",", objArr));
            Object[] objArr2 = new Object[1];
            objArr2[0] = this.f12651 == null ? "null" : this.f12651.toStringAllObjs().replace("\"", "\\\"");
            sb.append(String.format("\"mediaSource\": \"%s\",", objArr2));
            Object[] objArr3 = new Object[1];
            objArr3[0] = this.f12648 == null ? "null" : this.f12648.replace("\"", "\\\"");
            sb.append(String.format("\"playTitle\": \"%s\",", objArr3));
            Object[] objArr4 = new Object[1];
            objArr4[0] = this.f12650 == null ? "null" : this.f12650.toString().replace("\"", "\\\"");
            sb.append(String.format("\"subtitles\": \"%s\",", objArr4));
            sb.append(String.format("\"position\": \"%s\",", new Object[]{Long.valueOf(this.f12649)}));
            String r6 = m16082(TVApplication.m6288());
            Object[] objArr5 = new Object[1];
            objArr5[0] = r6 == null ? "null" : r6.replace("\"", "\\\"");
            sb.append(String.format("\"mxpPkgName\": \"%s\",", objArr5));
            if (r6 != null) {
                try {
                    PackageInfo packageInfo = TVApplication.m6288().getPackageManager().getPackageInfo(r6, 0);
                    sb.append(String.format("\"mxpVersion\": \"%s\",", new Object[]{String.format("%s (%s)", new Object[]{packageInfo.versionName, String.valueOf(packageInfo.versionCode)}).replace("\"", "\\\"")}));
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    sb.append("\"mxpVersion\": \"Unknown\",");
                }
            } else {
                sb.append("\"mxpVersion\": \"N/A\",");
            }
            if (intent != null) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    sb.append("\"resultData\": [");
                    for (String str : extras.keySet()) {
                        try {
                            Object obj = extras.get(str);
                            Object[] objArr6 = new Object[3];
                            objArr6[0] = str;
                            objArr6[1] = obj == null ? "null" : obj.toString();
                            objArr6[2] = obj == null ? "Null" : obj.getClass().getName();
                            sb.append("\"").append(String.format("%s: %s (%s)", objArr6).replace("\"", "\\\"")).append("\",");
                        } catch (Exception e2) {
                            Logger.m6281((Throwable) e2, new boolean[0]);
                        }
                    }
                    if (sb.toString().endsWith(",")) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    sb.append("]");
                }
            }
            sb.append("}");
        } catch (Exception e3) {
            Logger.m6281((Throwable) e3, new boolean[0]);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m16072() {
        return 90;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m16073() {
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m16074() {
        return "MX";
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m16075() {
        return "MX Player";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m16076(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return (long) intent.getIntExtra(VastIconXmlManager.DURATION, -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m16077(final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle(I18N.m15706(R.string.mxplayer_not_installed));
            r0.m519((CharSequence) I18N.m15706(R.string.mxplayer_not_installed_message));
            r0.m517(-1, I18N.m15706(R.string.install), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utils.m6402(activity, "com.mxtech.videoplayer.ad");
                }
            });
            r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m16078(AppCompatActivity appCompatActivity, int i, int i2, Intent intent) {
        Uri data = intent != null ? intent.getData() : null;
        m16071(data, intent);
        if (i != m16072()) {
            return -1;
        }
        if (i2 != 1) {
            return i2 == -1 ? 0 : 0;
        }
        m16053(appCompatActivity, "Last playback was ended with an error.", data);
        return 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m16079(Intent intent) {
        if (intent == null) {
            return -1;
        }
        return (long) intent.getIntExtra("position", -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16080(Activity activity, MediaSource mediaSource, String str, long j) {
        String r7 = m16082((Context) activity);
        if (r7 == null || r7.isEmpty()) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? MimeTypes.APPLICATION_M3U8 : "video/*");
        intent.setPackage(r7);
        if (r7.equals("com.mxtech.videoplayer.ad")) {
            intent.setClassName(r7, "com.mxtech.videoplayer.ad.ActivityScreen");
        } else if (r7.equals("com.mxtech.videoplayer.pro")) {
            intent.setClassName(r7, "com.mxtech.videoplayer.ActivityScreen");
        }
        intent.putExtra("return_result", true);
        intent.putExtra("suppress_error_message", false);
        intent.putExtra("secure_uri", true);
        intent.putExtra(PubnativeAsset.TITLE, str);
        if (j > -1) {
            if (j <= 0) {
                j = 1;
            }
            try {
                intent.putExtra("position", (int) j);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
        if (playHeader == null || playHeader.size() <= 0) {
            return intent;
        }
        HashMap<String, String> r4 = SourceUtils.m17835(playHeader);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : r4.entrySet()) {
            arrayList.add(next.getKey());
            arrayList.add(next.getValue());
        }
        intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Intent m16081(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        String r7 = m16082((Context) activity);
        if (r7 == null || r7.isEmpty()) {
            return null;
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator<String> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            arrayList3.add(Uri.parse(it2.next()));
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), "video/*");
        intent.setPackage(r7);
        if (r7.equals("com.mxtech.videoplayer.ad")) {
            intent.setClassName(r7, "com.mxtech.videoplayer.ad.ActivityScreen");
        } else if (r7.equals("com.mxtech.videoplayer.pro")) {
            intent.setClassName(r7, "com.mxtech.videoplayer.ActivityScreen");
        }
        intent.putExtra("return_result", true);
        intent.putExtra("suppress_error_message", false);
        intent.putExtra("secure_uri", true);
        intent.putExtra(PubnativeAsset.TITLE, str);
        intent.putExtra("subs", (Parcelable[]) arrayList3.toArray(new Uri[arrayList3.size()]));
        intent.putExtra("subs.name", (String[]) arrayList2.toArray(new String[arrayList2.size()]));
        if (j > -1) {
            if (j <= 0) {
                j = 1;
            }
            try {
                intent.putExtra("position", (int) j);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
        if (playHeader == null || playHeader.size() <= 0) {
            return intent;
        }
        HashMap<String, String> r4 = SourceUtils.m17835(playHeader);
        ArrayList arrayList4 = new ArrayList();
        for (Map.Entry next : r4.entrySet()) {
            arrayList4.add(next.getKey());
            arrayList4.add(next.getValue());
        }
        intent.putExtra("headers", (String[]) arrayList4.toArray(new String[arrayList4.size()]));
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16082(Context context) {
        if (Utils.m6424(context, "com.mxtech.videoplayer.pro")) {
            return "com.mxtech.videoplayer.pro";
        }
        if (Utils.m6424(context, "com.mxtech.videoplayer.ad")) {
            return "com.mxtech.videoplayer.ad";
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16083(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(activity).m525();
            r0.setTitle(I18N.m15706(R.string.mxplayer_unable_to_start));
            r0.m519((CharSequence) I18N.m15706(R.string.mxplayer_unable_to_start_message));
            r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            if (!activity.isFinishing()) {
                r0.show();
            }
        }
    }
}
