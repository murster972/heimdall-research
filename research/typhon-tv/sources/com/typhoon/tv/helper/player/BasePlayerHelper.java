package com.typhoon.tv.helper.player;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.List;

public abstract class BasePlayerHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    protected String f12648;

    /* renamed from: 麤  reason: contains not printable characters */
    protected long f12649;

    /* renamed from: 齉  reason: contains not printable characters */
    protected ArrayList<String> f12650;

    /* renamed from: 龘  reason: contains not printable characters */
    protected MediaSource f12651;

    public static class CustomErrorDialogFragment extends DialogFragment {
        public Dialog onCreateDialog(Bundle bundle) {
            String r2 = BasePlayerHelper.m16032().m16042();
            String r1 = I18N.m15706(R.string.error);
            Bundle arguments = getArguments();
            if (arguments != null) {
                r2 = arguments.getString("playerDisplayName", r2);
                r1 = arguments.getString("errorMsg", r1);
            }
            return new AlertDialog.Builder(getActivity()).m536((CharSequence) r2).m523((CharSequence) I18N.m15707(R.string.error_details, r1)).m520((int) R.drawable.ic_error_white_36dp).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).m525();
        }
    }

    public interface OnChoosePlayerListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m16054(String str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m16039();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract boolean m16040();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract String m16041();

    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract String m16042();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract long m16043(Intent intent);

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m16044(Activity activity);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m16047(AppCompatActivity appCompatActivity, int i, int i2, Intent intent);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract long m16048(Intent intent);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Intent m16049(Activity activity, MediaSource mediaSource, String str, long j);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Intent m16050(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m16051(Context context);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m16052(Activity activity);

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m16037() {
        try {
            if (!ExoPlayerHelper.m16055()) {
                return false;
            }
            if (TyphoonApp.f5845 || (TyphoonApp.f5846 && !Utils.m6403(Utils.m6409(TVApplication.m6288())))) {
                return true;
            }
            return false;
        } catch (Throwable e) {
            Logger.m6281(e, new boolean[0]);
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static BasePlayerHelper m16032() {
        if (m16037()) {
            if (!TVApplication.m6285().getString("pref_choose_default_player", m16035().m16041()).equalsIgnoreCase("Exo")) {
                TVApplication.m6285().edit().putString("pref_choose_default_player", "Exo").apply();
            }
            return new ExoPlayerHelper();
        }
        String string = TVApplication.m6285().getString("pref_choose_default_player", m16035().m16041());
        char c = 65535;
        switch (string.hashCode()) {
            case 2475:
                if (string.equals("MX")) {
                    c = 2;
                    break;
                }
                break;
            case 70140:
                if (string.equals("Exo")) {
                    c = 1;
                    break;
                }
                break;
            case 85069:
                if (string.equals("VLC")) {
                    c = 3;
                    break;
                }
                break;
            case 88775:
                if (string.equals("Exo")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return new YesPlayerHelper();
            case 1:
                return new ExoPlayerHelper();
            case 2:
                return new MXPlayerHelper();
            case 3:
                return new VLCPlayerHelper();
            default:
                return new ExoPlayerHelper();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static BasePlayerHelper m16035() {
        return ExoPlayerHelper.m16055() ? new ExoPlayerHelper() : new MXPlayerHelper();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static BasePlayerHelper[] m16034() {
        if (ExoPlayerHelper.m16055()) {
            return new BasePlayerHelper[]{new ExoPlayerHelper(), new MXPlayerHelper(), new VLCPlayerHelper()};
        }
        return new BasePlayerHelper[]{new MXPlayerHelper(), new VLCPlayerHelper()};
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m16036(final Activity activity, final OnChoosePlayerListener onChoosePlayerListener) {
        TVApplication.m6285().edit().putBoolean("choose_default_video_player_dialog_shown", true).apply();
        String currentPlayerName = TVApplication.m6285().getString("pref_choose_default_player", m16035().m16041());
        if (m16037() && !currentPlayerName.equalsIgnoreCase("Exo")) {
            TVApplication.m6285().edit().putString("pref_choose_default_player", "Exo").apply();
            currentPlayerName = "Exo";
        }
        BasePlayerHelper[] allPlayersArr = m16034();
        final List<String> allPlayersNamesList = new ArrayList<>();
        List<String> allPlayersDisplayNamesList = new ArrayList<>();
        for (BasePlayerHelper playerHelper : allPlayersArr) {
            allPlayersNamesList.add(playerHelper.m16041());
            allPlayersDisplayNamesList.add(playerHelper.m16042());
        }
        new AlertDialog.Builder(activity).m536((CharSequence) I18N.m15706(R.string.pref_choose_default_player)).m538(true).m539((CharSequence[]) allPlayersDisplayNamesList.toArray(new String[allPlayersDisplayNamesList.size()]), allPlayersNamesList.indexOf(currentPlayerName), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String str = (String) allPlayersNamesList.get(i);
                TVApplication.m6285().edit().putString("pref_choose_default_player", str).apply();
                if (onChoosePlayerListener != null) {
                    onChoosePlayerListener.m16054(str);
                }
                if (!activity.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!activity.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        }).m528();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m16031() {
        return !TVApplication.m6285().getBoolean("choose_default_video_player_dialog_shown", false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m16038(Activity activity, Intent playIntent) {
        boolean needCheck = true;
        if (m16041() != null && m16041().trim().toLowerCase().contains("exo")) {
            needCheck = false;
        }
        if (needCheck) {
            if (m16051((Context) activity) == null) {
                m16044(activity);
                return false;
            } else if (playIntent == null) {
                m16052(activity);
                return false;
            }
        }
        if (Utils.m6410()) {
            Utils.m6422(false);
        }
        return m16033(activity, playIntent);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m16045(Activity activity, MediaSource mediaSource, String playTitle, long position) {
        try {
            Intent playIntent = m16049(activity, mediaSource, playTitle, position);
            this.f12651 = mediaSource;
            this.f12648 = playTitle;
            this.f12650 = null;
            this.f12649 = position;
            return m16038(activity, playIntent);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            m16052(activity);
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m16046(Activity activity, MediaSource mediaSource, String playTitle, long position, ArrayList<String> subtitlePaths, ArrayList<String> subtitleTitles) {
        try {
            Intent playIntent = m16050(activity, mediaSource, playTitle, position, subtitlePaths, subtitleTitles);
            this.f12651 = mediaSource;
            this.f12648 = playTitle;
            this.f12650 = subtitleTitles;
            this.f12649 = position;
            return m16038(activity, playIntent);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            m16052(activity);
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m16033(Activity activity, Intent intent) {
        try {
            activity.startActivityForResult(intent, m16039());
            return true;
        } catch (ActivityNotFoundException ex) {
            Logger.m6281((Throwable) ex, new boolean[0]);
            m16052(activity);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16053(AppCompatActivity activity, String errorMsg, Uri mediaUri) {
        if (activity != null) {
            try {
                if (!activity.isFinishing()) {
                    CustomErrorDialogFragment errorDialogFragment = new CustomErrorDialogFragment();
                    Bundle args = new Bundle();
                    args.putString("playerDisplayName", m16042());
                    args.putString("errorMsg", errorMsg);
                    errorDialogFragment.setArguments(args);
                    errorDialogFragment.show(activity.getSupportFragmentManager(), "playerErrorFrag");
                    return;
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                Toast.makeText(TVApplication.m6288(), m16041() + ":\n\n" + errorMsg, 1).show();
                return;
            } catch (Throwable e2) {
                Logger.m6281(e2, new boolean[0]);
                return;
            }
        }
        Toast.makeText(TVApplication.m6288(), m16041() + ":\n\n" + errorMsg, 1).show();
    }
}
