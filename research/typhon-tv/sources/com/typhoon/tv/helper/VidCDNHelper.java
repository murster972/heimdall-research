package com.typhoon.tv.helper;

import com.typhoon.tv.Logger;
import com.typhoon.tv.utils.Regex;
import java.net.URLDecoder;

public class VidCDNHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m15995(String str) {
        String str2;
        String str3;
        if (str == null || str.isEmpty()) {
            return null;
        }
        String str4 = null;
        String r2 = Regex.m17801(str, "//([^/]*vidcdn\\.[^/]{2,8})/play.*?(?:\\?|&)fileName=(.*?)(?:$|&)", 1, 34);
        try {
            str2 = URLDecoder.decode(Regex.m17801(str, "//([^/]*vidcdn\\.[^/]{2,8})/play.*?(?:\\?|&)fileName=(.*?)(?:$|&)", 2, 34), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                str2 = URLDecoder.decode(str2);
            } catch (Exception e2) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        try {
            if (r2.isEmpty() || str2.isEmpty()) {
                String r3 = Regex.m17801(str, "//[^/]*statics?\\.vidcdn\\.[^/]{2,8}/storage/(.*?)/video/(.*?)(?:$|\\?)", 1, 34);
                try {
                    str3 = URLDecoder.decode(Regex.m17801(str, "//[^/]*statics?\\.vidcdn\\.[^/]{2,8}/storage/(.*?)/video/(.*?)(?:$|\\?)", 2, 34), "UTF-8");
                } catch (Exception e3) {
                    Logger.m6281((Throwable) e3, new boolean[0]);
                    try {
                        str3 = URLDecoder.decode(str3);
                    } catch (Exception e4) {
                        Logger.m6281((Throwable) e3, new boolean[0]);
                    }
                }
                if (!r3.isEmpty() && !str3.isEmpty()) {
                    str4 = String.format("http://%s/play?fileName=%s", new Object[]{r3, str3});
                }
            } else {
                str4 = String.format("http://statics.vidcdn.pro/storage/%s/video/%s", new Object[]{r2, str2});
            }
            return str4 != null ? str4 : str4;
        } catch (Exception e5) {
            Logger.m6281((Throwable) e5, new boolean[0]);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15996(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        return !Regex.m17801(str, "//([^/]*vidcdn\\.[^/]{2,8})/", 1, 34).isEmpty();
    }
}
