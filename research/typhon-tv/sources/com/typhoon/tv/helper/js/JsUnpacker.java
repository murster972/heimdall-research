package com.typhoon.tv.helper.js;

import android.util.Base64;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.squareup.duktape.Duktape;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class JsUnpacker {

    private interface Unpacker {
        String unpack(String str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static ArrayList<String> m16022(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(m16023(str));
        if (TyphoonApp.f5859) {
            arrayList.addAll(m16024(str));
        }
        return arrayList;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static ArrayList<String> m16023(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<ArrayList<String>> r12 = Regex.m17803(str, "\\}\\s*\\('(.*)',\\s*(.*?),\\s*(\\d+),\\s*'([^']+)'\\.split\\('\\|'\\)", 4);
        ArrayList arrayList2 = r12.get(0);
        ArrayList arrayList3 = r12.get(1);
        ArrayList arrayList4 = r12.get(2);
        ArrayList arrayList5 = r12.get(3);
        for (int i = 0; i < arrayList2.size(); i++) {
            try {
                HashMap hashMap = new HashMap();
                String str2 = (String) arrayList2.get(i);
                String str3 = (String) arrayList3.get(i);
                String str4 = (String) arrayList4.get(i);
                String str5 = (String) arrayList5.get(i);
                int i2 = 36;
                if (Utils.m6426(str3)) {
                    i2 = Integer.parseInt(str3);
                }
                int parseInt = Integer.parseInt(str4);
                String[] split = str5.split("\\|");
                while (parseInt > 0) {
                    parseInt--;
                    String r6 = m16025(parseInt, i2);
                    hashMap.put(r6, split.length > parseInt && split[parseInt] != null ? split[parseInt] : r6);
                }
                StringBuilder sb = new StringBuilder();
                char[] charArray = str2.toCharArray();
                int length = charArray.length;
                for (int i3 = 0; i3 < length; i3++) {
                    char c = charArray[i3];
                    if (!Regex.m17800(String.valueOf(c), "\\b(\\w+)\\b", 1).isEmpty()) {
                        sb.append((String) hashMap.get(String.valueOf(c)));
                    } else {
                        sb.append(c);
                    }
                }
                arrayList.add(sb.toString().replace("\\'", "'").replace("\\\"", "\""));
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static ArrayList<String> m16024(String str) {
        String unpack;
        Duktape duktape = null;
        String str2 = "";
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            duktape = Duktape.create();
            duktape.evaluate("//\n// Unpacker for Dean Edward's p.a.c.k.e.r, a part of javascript beautifier\n// written by Einar Lielmanis <einar@jsbeautifier.org>\n// edited by NitroXenon for Typhoon\n//\n// Coincidentally, it can defeat a couple of other eval-based compressors.\n//\n// usage:\n//\n// if (P_A_C_K_E_R.detect(some_string)) {\n//     var unpacked = P_A_C_K_E_R.unpack(some_string);\n// }\n//\n//\nvar Unpacker = {\n    PATTERN: /(eval\\s*\\(\\(?function\\(.*?(,0,\\{\\}\\)\\)|split\\('\\|'\\)\\)\\)))/g,\n\n    unpack: function(html) {\n        var results = [];\n        var matches = getMatches(html, Unpacker.PATTERN, 1)\n        for (var i = 0; i < matches.length; i++) {\n            try {\n               var match = matches[i];\n               results.push(P_A_C_K_E_R.unpack(match));\n            } catch (lErr) {}\n        }\n\n        return Duktape.enc('base64', JSON.stringify(results));\n    },\n}\n\nvar P_A_C_K_E_R = {\n    PATTERN: /eval\\s*\\(\\(?function\\(.*?(,0,\\{\\}\\)\\)|split\\('\\|'\\)\\)\\))/g,\n\n    detect: function(str) {\n        return (P_A_C_K_E_R.get_chunks(str).length > 0);\n    },\n\n    get_chunks: function(str) {\n        var chunks = str.match(P_A_C_K_E_R.PATTERN);\n        return chunks ? chunks : [];\n    },\n\n    unpack: function(str) {\n        var chunks = P_A_C_K_E_R.get_chunks(str),\n            chunk;\n        for (var i = 0; i < chunks.length; i++) {\n            chunk = chunks[i].replace(/\\n$/, '');\n            str = str.split(chunk).join(P_A_C_K_E_R.unpack_chunk(chunk));\n        }\n        return str;\n    },\n\n    unpack_chunk: function(str) {\n        var unpacked_source = '';\n        var __eval = eval;\n        if (P_A_C_K_E_R.detect(str)) {\n            try {\n                eval = function(s) { // jshint ignore:line\n                    unpacked_source += s;\n                    return unpacked_source;\n                }; // jshint ignore:line\n                __eval(str);\n                if (typeof unpacked_source === 'string' && unpacked_source) {\n                    str = unpacked_source;\n                }\n            } catch (e) {\n                //console.log(e.toString());\n            }\n        }\n        eval = __eval; // jshint ignore:line\n        return str;\n    },\n};\n\nfunction getMatches(string, regex, index) {\n    index || (index = 1); // default to the first capturing group\n    var matches = [];\n    var match;\n    while (match = regex.exec(string)) {\n        matches.push(match[index]);\n    }\n    return matches;\n}");
            unpack = ((Unpacker) duktape.get("Unpacker", Unpacker.class)).unpack(str);
            str2 = new String(Base64.decode(unpack, 0), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            str2 = new String(Base64.decode(unpack, 0));
        } catch (Throwable th) {
            try {
                Logger.m6281(th, new boolean[0]);
                if (duktape != null) {
                    duktape.close();
                }
            } catch (Throwable th2) {
                if (duktape != null) {
                    duktape.close();
                }
                throw th2;
            }
        }
        if (duktape != null) {
            duktape.close();
        }
        if (!str2.isEmpty()) {
            try {
                Iterator<JsonElement> it2 = new JsonParser().parse(str2).getAsJsonArray().iterator();
                while (it2.hasNext()) {
                    arrayList.add(it2.next().getAsString());
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m16025(int i, int i2) throws Exception {
        String substring = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(0, i2);
        if (substring.length() > i2) {
            throw new Exception("base " + i2 + " exceeds table length " + substring.length());
        } else if (i == 0) {
            return Character.toString(substring.charAt(0));
        } else {
            StringBuilder sb = new StringBuilder();
            while (i > 0) {
                sb.insert(0, Character.toString(substring.charAt(i % i2)));
                i /= i2;
            }
            return sb.toString();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m16026(String str) {
        return !Regex.m17802(str, "(eval\\s*\\(\\(?function\\(.*?(,0,\\{\\}\\)\\)|split\\('\\|'\\)\\)\\)))", 1, true).isEmpty();
    }
}
