package com.typhoon.tv.helper.js;

import android.util.Base64;
import com.squareup.duktape.Duktape;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;

public class AADecoder {

    private interface AADecode {
        String decode(String str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m16020(String str) {
        if (!TyphoonApp.f5859 || !m16021(str)) {
            return "";
        }
        String str2 = "";
        Duktape duktape = null;
        try {
            duktape = Duktape.create();
            duktape.evaluate("var AADecode = {\n    decode: function(text) {\n        var evalPreamble = \"(ﾟДﾟ) ['_'] ( (ﾟДﾟ) ['_'] (\";\n        var decodePreamble = \"( (ﾟДﾟ) ['_'] (\";\n        var evalPostamble = \") (ﾟΘﾟ)) ('_');\";\n        var decodePostamble = \") ());\";\n        text = text.replace(/^\\s*/, \"\").replace(/\\s*$/, \"\");\n        if (/^\\s*$/.test(text)) return \"\";\n        if (text.lastIndexOf(evalPreamble) < 0) throw new Error(\"Given code is not encoded as aaencode.\");\n        if (text.lastIndexOf(evalPostamble) != text.length - evalPostamble.length) throw new Error(\"Given code is not encoded as aaencode.\");\n        var decodingScript = text.replace(evalPreamble, decodePreamble).replace(evalPostamble, decodePostamble);\n        return Duktape.enc(\"base64\", eval(decodingScript));\n    }\n};");
            str2 = ((AADecode) duktape.get("AADecode", AADecode.class)).decode(str);
            if (duktape != null) {
                duktape.close();
            }
        } catch (Throwable th) {
            if (duktape != null) {
                duktape.close();
            }
            throw th;
        }
        try {
            return new String(Base64.decode(str2, 0), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                return new String(Base64.decode(str2, 0));
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return "";
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m16021(String str) {
        return str.contains("(ﾟωﾟﾉ");
    }
}
