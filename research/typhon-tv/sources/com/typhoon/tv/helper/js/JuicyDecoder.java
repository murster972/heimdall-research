package com.typhoon.tv.helper.js;

import android.util.Base64;
import com.squareup.duktape.Duktape;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.utils.Regex;

public class JuicyDecoder {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m16027(String str) {
        String replaceAll = Regex.m17801(str, "JuicyCodes\\.Run\\(([^\\)]+)", 1, 34).replaceAll("\"\\s*\\+\\s*\"", "").replaceAll("[^A-Za-z0-9+\\\\/=]", "");
        if (replaceAll.isEmpty()) {
            return "";
        }
        try {
            return new String(Base64.decode(replaceAll, 0), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                return new String(Base64.decode(replaceAll, 0));
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return "";
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m16028(String str) {
        String str2 = "";
        Duktape duktape = null;
        try {
            Duktape create = Duktape.create();
            Object evaluate = create.evaluate("var JuicyCodes = {\n    \"Juice\": 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',\n    \"Run\": function(e) {\n        var t = '',\n            n,\n            r,\n            i,\n            s,\n            o,\n            u,\n            a,\n            f = 0;\n        for (e = e['replace'](new RegExp('[^A-Za-z0-9+\\\\/=]', 'g'), ''); f < e['length'];) s = this['Juice']['indexOf'](e['charAt'](f++)),\n            o = this['Juice']['indexOf'](e['charAt'](f++)),\n            u = this['Juice']['indexOf'](e['charAt'](f++)),\n            a = this['Juice']['indexOf'](e['charAt'](f++)),\n            n = s << 2 | o >> 4,\n            r = (15 & o) << 4 | u >> 2,\n            i = (3 & u) << 6 | a,\n            t += String['fromCharCode'](n),\n            64 != u && (t += String['fromCharCode'](r)),\n            64 != a && (t += String['fromCharCode'](i));\n        return t = JuicyCodes['utf8'](t); //,\n        //eval(t)\n    },\n    \"utf8\": function(a) {\n        for (var b = '', c = 0, d = c1 = c2 = 0; c < a['length'];) d = a['charCodeAt'](c),\n            d < 128 ? (b += String['fromCharCode'](d), c++) : d > 191 && d < 224 ? (c2 = a['charCodeAt'](c + 1), b += String['fromCharCode']((31 & d) << 6 | 63 & c2), c += 2) : (c2 = a['charCodeAt'](c + 1), c3 = a['charCodeAt'](c + 2), b += String['fromCharCode']((15 & d) << 12 | (63 & c2) << 6 | 63 & c3), c += 3);\n        return b\n    }\n};\n\nvar x = {jsCode};\n\nDuktape.enc(\"base64\", x);".replace("{jsCode}", str));
            if (evaluate != null) {
                str2 = evaluate.toString();
            }
            if (create != null) {
                create.close();
            }
        } catch (Throwable th) {
            if (duktape != null) {
                duktape.close();
            }
            throw th;
        }
        try {
            return new String(Base64.decode(str2, 0), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                return new String(Base64.decode(str2, 0));
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return "";
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m16029(String str) {
        String replaceAll = Regex.m17801(str, "JuicyCodes\\.Run\\(([^\\)]+)", 1, 34).replaceAll("\"\\s*\\+\\s*\"", "").replaceAll("[^A-Za-z0-9+\\\\/=]", "");
        if (replaceAll.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < replaceAll.length()) {
            try {
                int indexOf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(replaceAll.charAt(i));
                int i2 = i + 1;
                int indexOf2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(replaceAll.charAt(i2));
                int i3 = i2 + 1;
                int indexOf3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(replaceAll.charAt(i3));
                int i4 = i3 + 1;
                int indexOf4 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(replaceAll.charAt(i4));
                i = i4 + 1;
                int i5 = ((indexOf2 & 15) << 4) | (indexOf3 >> 2);
                int i6 = ((indexOf3 & 3) << 6) | indexOf4;
                sb.append(Character.toString((char) ((indexOf << 2) | (indexOf2 >> 4))));
                if (64 != indexOf3) {
                    sb.append(Character.toString((char) i5));
                }
                if (64 != indexOf4) {
                    sb.append(Character.toString((char) i6));
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m16030(String str) {
        if (!str.trim().toLowerCase().contains("juicycodes")) {
            return "";
        }
        String r0 = m16027(str);
        if (r0.isEmpty()) {
            r0 = m16029(str);
        }
        return (!r0.isEmpty() || !TyphoonApp.f5859) ? r0 : m16028(str);
    }
}
