package com.typhoon.tv.backup;

import android.os.Environment;
import com.typhoon.tv.Logger;
import com.typhoon.tv.utils.Utils;
import java.io.File;

public class SubsMapBackupRestoreHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f12565 = (Environment.getExternalStorageDirectory() + "/Typhoon/Backup");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f12566 = (Environment.getExternalStorageDirectory() + "/Typhoon/Backup/subsMap.backup");

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m15826() {
        try {
            new File(f12565).mkdirs();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        try {
            Utils.m6421(new File(f12566));
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x004b A[SYNTHETIC, Splitter:B:26:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0050 A[SYNTHETIC, Splitter:B:29:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0065 A[SYNTHETIC, Splitter:B:37:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x006a A[SYNTHETIC, Splitter:B:40:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    @android.annotation.SuppressLint({"ApplySharedPref"})
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> m15827() {
        /*
            r11 = 0
            java.io.File r8 = new java.io.File
            java.lang.String r9 = f12566
            r8.<init>(r9)
            boolean r9 = r8.exists()
            if (r9 != 0) goto L_0x0010
            r2 = 0
        L_0x000f:
            return r2
        L_0x0010:
            r4 = 0
            r6 = 0
            r2 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0042 }
            r5.<init>(r8)     // Catch:{ Exception -> 0x0042 }
            java.io.ObjectInputStream r7 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            r7.<init>(r5)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            java.lang.Object r9 = r7.readObject()     // Catch:{ Exception -> 0x0086, all -> 0x007f }
            r0 = r9
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ Exception -> 0x0086, all -> 0x007f }
            r2 = r0
            if (r7 == 0) goto L_0x002a
            r7.close()     // Catch:{ IOException -> 0x0032 }
        L_0x002a:
            if (r5 == 0) goto L_0x002f
            r5.close()     // Catch:{ Exception -> 0x0039 }
        L_0x002f:
            r6 = r7
            r4 = r5
            goto L_0x000f
        L_0x0032:
            r3 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r9)
            goto L_0x002a
        L_0x0039:
            r1 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r9)
            r6 = r7
            r4 = r5
            goto L_0x000f
        L_0x0042:
            r1 = move-exception
        L_0x0043:
            r9 = 0
            boolean[] r9 = new boolean[r9]     // Catch:{ all -> 0x0062 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r9)     // Catch:{ all -> 0x0062 }
            if (r6 == 0) goto L_0x004e
            r6.close()     // Catch:{ IOException -> 0x005b }
        L_0x004e:
            if (r4 == 0) goto L_0x000f
            r4.close()     // Catch:{ Exception -> 0x0054 }
            goto L_0x000f
        L_0x0054:
            r1 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r9)
            goto L_0x000f
        L_0x005b:
            r3 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r9)
            goto L_0x004e
        L_0x0062:
            r9 = move-exception
        L_0x0063:
            if (r6 == 0) goto L_0x0068
            r6.close()     // Catch:{ IOException -> 0x006e }
        L_0x0068:
            if (r4 == 0) goto L_0x006d
            r4.close()     // Catch:{ Exception -> 0x0075 }
        L_0x006d:
            throw r9
        L_0x006e:
            r3 = move-exception
            boolean[] r10 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r10)
            goto L_0x0068
        L_0x0075:
            r1 = move-exception
            boolean[] r10 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r10)
            goto L_0x006d
        L_0x007c:
            r9 = move-exception
            r4 = r5
            goto L_0x0063
        L_0x007f:
            r9 = move-exception
            r6 = r7
            r4 = r5
            goto L_0x0063
        L_0x0083:
            r1 = move-exception
            r4 = r5
            goto L_0x0043
        L_0x0086:
            r1 = move-exception
            r6 = r7
            r4 = r5
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.backup.SubsMapBackupRestoreHelper.m15827():java.util.Map");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0063 A[SYNTHETIC, Splitter:B:34:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006b A[SYNTHETIC, Splitter:B:37:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0083 A[SYNTHETIC, Splitter:B:45:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x008b A[SYNTHETIC, Splitter:B:48:0x008b] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m15828(java.util.Map<java.lang.String, java.lang.String> r12) {
        /*
            r11 = 0
            r8 = 0
            java.io.File r1 = new java.io.File
            java.lang.String r9 = f12565
            r1.<init>(r9)
            r1.mkdirs()     // Catch:{ Exception -> 0x003c }
        L_0x000c:
            r4 = 0
            r6 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r9 = f12566
            r0.<init>(r9)
            com.typhoon.tv.utils.Utils.m6421((java.io.File) r0)     // Catch:{ Exception -> 0x0043 }
            r0.createNewFile()     // Catch:{ Exception -> 0x0043 }
        L_0x001b:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x005a }
            r5.<init>(r0)     // Catch:{ Exception -> 0x005a }
            java.io.ObjectOutputStream r7 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00a7, all -> 0x00a0 }
            r7.<init>(r5)     // Catch:{ Exception -> 0x00a7, all -> 0x00a0 }
            r7.writeObject(r12)     // Catch:{ Exception -> 0x00aa, all -> 0x00a3 }
            r8 = 1
            if (r7 == 0) goto L_0x0031
            r7.flush()     // Catch:{ IOException -> 0x004a }
            r7.close()     // Catch:{ IOException -> 0x004a }
        L_0x0031:
            if (r5 == 0) goto L_0x0039
            r5.flush()     // Catch:{ Exception -> 0x0051 }
            r5.close()     // Catch:{ Exception -> 0x0051 }
        L_0x0039:
            r6 = r7
            r4 = r5
        L_0x003b:
            return r8
        L_0x003c:
            r2 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r9)
            goto L_0x000c
        L_0x0043:
            r2 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r9)
            goto L_0x001b
        L_0x004a:
            r3 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r9)
            goto L_0x0031
        L_0x0051:
            r2 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r9)
            r6 = r7
            r4 = r5
            goto L_0x003b
        L_0x005a:
            r2 = move-exception
        L_0x005b:
            r9 = 0
            boolean[] r9 = new boolean[r9]     // Catch:{ all -> 0x0080 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r9)     // Catch:{ all -> 0x0080 }
            if (r6 == 0) goto L_0x0069
            r6.flush()     // Catch:{ IOException -> 0x0079 }
            r6.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0069:
            if (r4 == 0) goto L_0x003b
            r4.flush()     // Catch:{ Exception -> 0x0072 }
            r4.close()     // Catch:{ Exception -> 0x0072 }
            goto L_0x003b
        L_0x0072:
            r2 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r9)
            goto L_0x003b
        L_0x0079:
            r3 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r9)
            goto L_0x0069
        L_0x0080:
            r9 = move-exception
        L_0x0081:
            if (r6 == 0) goto L_0x0089
            r6.flush()     // Catch:{ IOException -> 0x0092 }
            r6.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0089:
            if (r4 == 0) goto L_0x0091
            r4.flush()     // Catch:{ Exception -> 0x0099 }
            r4.close()     // Catch:{ Exception -> 0x0099 }
        L_0x0091:
            throw r9
        L_0x0092:
            r3 = move-exception
            boolean[] r10 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r10)
            goto L_0x0089
        L_0x0099:
            r2 = move-exception
            boolean[] r10 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r10)
            goto L_0x0091
        L_0x00a0:
            r9 = move-exception
            r4 = r5
            goto L_0x0081
        L_0x00a3:
            r9 = move-exception
            r6 = r7
            r4 = r5
            goto L_0x0081
        L_0x00a7:
            r2 = move-exception
            r4 = r5
            goto L_0x005b
        L_0x00aa:
            r2 = move-exception
            r6 = r7
            r4 = r5
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.backup.SubsMapBackupRestoreHelper.m15828(java.util.Map):boolean");
    }
}
