package com.typhoon.tv.backup;

import android.os.Environment;

public class PrefsBackupRestoreHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f12563 = (Environment.getExternalStorageDirectory() + "/Typhoon/Backup");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f12564 = (Environment.getExternalStorageDirectory() + "/Typhoon/Backup/prefs.backup");

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0067, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, new boolean[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0092, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0093, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00d7, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00d8, code lost:
        com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, new boolean[0]);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0092 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x001c] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0096 A[SYNTHETIC, Splitter:B:38:0x0096] */
    @android.annotation.SuppressLint({"ApplySharedPref"})
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m15824() {
        /*
            r11 = 0
            r9 = 0
            java.io.File r8 = new java.io.File
            java.lang.String r12 = f12564
            r8.<init>(r12)
            boolean r12 = r8.exists()
            if (r12 != 0) goto L_0x0011
            r9 = r11
        L_0x0010:
            return r9
        L_0x0011:
            r4 = 0
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x00e0 }
            java.io.FileInputStream r12 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00e0 }
            r12.<init>(r8)     // Catch:{ Exception -> 0x00e0 }
            r5.<init>(r12)     // Catch:{ Exception -> 0x00e0 }
            android.content.SharedPreferences r12 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            android.content.SharedPreferences$Editor r7 = r12.edit()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            java.lang.Object r1 = r5.readObject()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            java.util.Set r12 = r1.entrySet()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            java.util.Iterator r12 = r12.iterator()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
        L_0x0032:
            boolean r13 = r12.hasNext()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            if (r13 == 0) goto L_0x00c2
            java.lang.Object r2 = r12.next()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            java.lang.Object r10 = r2.getValue()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            java.lang.Object r6 = r2.getKey()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            java.lang.String r13 = r6.trim()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            java.lang.String r13 = r13.toLowerCase()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            java.lang.String r14 = "pref_"
            boolean r13 = r13.startsWith(r14)     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            if (r13 == 0) goto L_0x0032
            boolean r13 = r10 instanceof java.lang.Boolean     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            if (r13 == 0) goto L_0x0084
            java.lang.Boolean r10 = (java.lang.Boolean) r10     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            boolean r13 = r10.booleanValue()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            r7.putBoolean(r6, r13)     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            goto L_0x0032
        L_0x0067:
            r0 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r13)     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            goto L_0x0032
        L_0x006f:
            r0 = move-exception
            r4 = r5
        L_0x0071:
            r12 = 0
            boolean[] r12 = new boolean[r12]     // Catch:{ all -> 0x00de }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r12)     // Catch:{ all -> 0x00de }
            if (r4 == 0) goto L_0x0010
            r4.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0010
        L_0x007d:
            r3 = move-exception
            boolean[] r11 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r11)
            goto L_0x0010
        L_0x0084:
            boolean r13 = r10 instanceof java.lang.Float     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            if (r13 == 0) goto L_0x009a
            java.lang.Float r10 = (java.lang.Float) r10     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            float r13 = r10.floatValue()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            r7.putFloat(r6, r13)     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            goto L_0x0032
        L_0x0092:
            r12 = move-exception
            r4 = r5
        L_0x0094:
            if (r4 == 0) goto L_0x0099
            r4.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x0099:
            throw r12
        L_0x009a:
            boolean r13 = r10 instanceof java.lang.Integer     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            if (r13 == 0) goto L_0x00a8
            java.lang.Integer r10 = (java.lang.Integer) r10     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            int r13 = r10.intValue()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            r7.putInt(r6, r13)     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            goto L_0x0032
        L_0x00a8:
            boolean r13 = r10 instanceof java.lang.Long     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            if (r13 == 0) goto L_0x00b7
            java.lang.Long r10 = (java.lang.Long) r10     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            long r14 = r10.longValue()     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            r7.putLong(r6, r14)     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            goto L_0x0032
        L_0x00b7:
            boolean r13 = r10 instanceof java.lang.String     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            if (r13 == 0) goto L_0x0032
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            r7.putString(r6, r10)     // Catch:{ Exception -> 0x0067, all -> 0x0092 }
            goto L_0x0032
        L_0x00c2:
            r7.commit()     // Catch:{ Exception -> 0x006f, all -> 0x0092 }
            r9 = 1
            if (r5 == 0) goto L_0x00cb
            r5.close()     // Catch:{ IOException -> 0x00ce }
        L_0x00cb:
            r4 = r5
            goto L_0x0010
        L_0x00ce:
            r3 = move-exception
            boolean[] r11 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r11)
            r4 = r5
            goto L_0x0010
        L_0x00d7:
            r3 = move-exception
            boolean[] r11 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r11)
            goto L_0x0099
        L_0x00de:
            r12 = move-exception
            goto L_0x0094
        L_0x00e0:
            r0 = move-exception
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.backup.PrefsBackupRestoreHelper.m15824():boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0059 A[SYNTHETIC, Splitter:B:27:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006a A[SYNTHETIC, Splitter:B:33:0x006a] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m15825() {
        /*
            r9 = 0
            r7 = 0
            java.io.File r1 = new java.io.File
            java.lang.String r8 = f12563
            r1.<init>(r8)
            r1.mkdirs()     // Catch:{ Exception -> 0x003a }
        L_0x000c:
            r4 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r8 = f12564
            r0.<init>(r8)
            com.typhoon.tv.utils.Utils.m6421((java.io.File) r0)     // Catch:{ Exception -> 0x0041 }
            r0.createNewFile()     // Catch:{ Exception -> 0x0041 }
        L_0x001a:
            java.io.ObjectOutputStream r5 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0050 }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0050 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0050 }
            r5.<init>(r8)     // Catch:{ Exception -> 0x0050 }
            android.content.SharedPreferences r6 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            java.util.Map r8 = r6.getAll()     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            r5.writeObject(r8)     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            r7 = 1
            if (r5 == 0) goto L_0x0038
            r5.flush()     // Catch:{ IOException -> 0x0048 }
            r5.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0038:
            r4 = r5
        L_0x0039:
            return r7
        L_0x003a:
            r2 = move-exception
            boolean[] r8 = new boolean[r9]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r8)
            goto L_0x000c
        L_0x0041:
            r2 = move-exception
            boolean[] r8 = new boolean[r9]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r8)
            goto L_0x001a
        L_0x0048:
            r3 = move-exception
            boolean[] r8 = new boolean[r9]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r8)
            r4 = r5
            goto L_0x0039
        L_0x0050:
            r2 = move-exception
        L_0x0051:
            r8 = 0
            boolean[] r8 = new boolean[r8]     // Catch:{ all -> 0x0067 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r8)     // Catch:{ all -> 0x0067 }
            if (r4 == 0) goto L_0x0039
            r4.flush()     // Catch:{ IOException -> 0x0060 }
            r4.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x0039
        L_0x0060:
            r3 = move-exception
            boolean[] r8 = new boolean[r9]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r8)
            goto L_0x0039
        L_0x0067:
            r8 = move-exception
        L_0x0068:
            if (r4 == 0) goto L_0x0070
            r4.flush()     // Catch:{ IOException -> 0x0071 }
            r4.close()     // Catch:{ IOException -> 0x0071 }
        L_0x0070:
            throw r8
        L_0x0071:
            r3 = move-exception
            boolean[] r9 = new boolean[r9]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r9)
            goto L_0x0070
        L_0x0078:
            r8 = move-exception
            r4 = r5
            goto L_0x0068
        L_0x007b:
            r2 = move-exception
            r4 = r5
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.backup.PrefsBackupRestoreHelper.m15825():boolean");
    }
}
