package com.typhoon.tv.backup;

import android.os.Environment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.tv.TvWatchedEpisode;
import com.typhoon.tv.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.List;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

public class WatchedEpsBackupRestoreHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String f12567 = (Environment.getExternalStorageDirectory() + "/Typhoon/Backup");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f12568 = (Environment.getExternalStorageDirectory() + "/Typhoon/Backup/watched_eps.backup");

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m15829() {
        File file = new File(f12568);
        if (!file.exists()) {
            return false;
        }
        BufferedSource bufferedSource = null;
        try {
            BufferedSource r4 = Okio.m20507(Okio.m20512(file));
            for (TvWatchedEpisode tvWatchedEpisode : (List) new Gson().fromJson(r4.m20458(), new TypeToken<List<TvWatchedEpisode>>() {
            }.getType())) {
                if (!TVApplication.m6287().m6321(Integer.valueOf(tvWatchedEpisode.getTmdbId()), Integer.valueOf(tvWatchedEpisode.getSeason()), Integer.valueOf(tvWatchedEpisode.getEpisode()))) {
                    tvWatchedEpisode.save();
                }
            }
            if (r4 == null) {
                return true;
            }
            try {
                r4.close();
                return true;
            } catch (IOException e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                return true;
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            if (bufferedSource == null) {
                return false;
            }
            try {
                bufferedSource.close();
                return false;
            } catch (IOException e3) {
                Logger.m6281((Throwable) e3, new boolean[0]);
                return false;
            }
        } catch (Throwable th) {
            if (bufferedSource != null) {
                try {
                    bufferedSource.close();
                } catch (IOException e4) {
                    Logger.m6281((Throwable) e4, new boolean[0]);
                }
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15830() {
        boolean z = false;
        try {
            new File(f12567).mkdirs();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        File file = new File(f12568);
        BufferedSink bufferedSink = null;
        try {
            Utils.m6421(file);
            file.createNewFile();
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        try {
            bufferedSink = Okio.m20506(Okio.m20502(file));
            bufferedSink.m20445(new Gson().toJson((Object) TVApplication.m6287().m6313(), new TypeToken<List<TvWatchedEpisode>>() {
            }.getType()));
            z = true;
            if (bufferedSink != null) {
                try {
                    bufferedSink.flush();
                    bufferedSink.close();
                } catch (IOException e3) {
                    Logger.m6281((Throwable) e3, new boolean[0]);
                }
            }
        } catch (Exception e4) {
            Logger.m6281((Throwable) e4, new boolean[0]);
            if (bufferedSink != null) {
                try {
                    bufferedSink.flush();
                    bufferedSink.close();
                } catch (IOException e5) {
                    Logger.m6281((Throwable) e5, new boolean[0]);
                }
            }
        } catch (Throwable th) {
            if (bufferedSink != null) {
                try {
                    bufferedSink.flush();
                    bufferedSink.close();
                } catch (IOException e6) {
                    Logger.m6281((Throwable) e6, new boolean[0]);
                }
            }
            throw th;
        }
        return z;
    }
}
