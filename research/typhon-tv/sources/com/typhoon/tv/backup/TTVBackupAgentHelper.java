package com.typhoon.tv.backup;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import com.typhoon.tv.utils.Utils;

public class TTVBackupAgentHelper extends BackupAgentHelper {
    public void onCreate() {
        addHelper("prefs", new SharedPreferencesBackupHelper(this, new String[]{Utils.m6398((Context) this)}));
    }
}
