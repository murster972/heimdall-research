package com.typhoon.tv.api;

import com.google.gson.Gson;
import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class FanartTVApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile FanartTVApi f12503;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Hashtable<String, String> f12504 = new Hashtable<>();

    private static class GetTvShowImageResult {
        private String name;
        private String thetvdb_id;
        private List<TvposterBean> tvposter;

        public static class TvposterBean {
            private String id;
            private String lang;
            private String likes;
            private String url;

            public String getId() {
                return this.id;
            }

            public String getLang() {
                return this.lang;
            }

            public String getLikes() {
                return this.likes;
            }

            public String getUrl() {
                return this.url;
            }

            public void setId(String str) {
                this.id = str;
            }

            public void setLang(String str) {
                this.lang = str;
            }

            public void setLikes(String str) {
                this.likes = str;
            }

            public void setUrl(String str) {
                this.url = str;
            }
        }

        private GetTvShowImageResult() {
        }

        public String getName() {
            return this.name;
        }

        public List<TvposterBean> getTvposter() {
            return this.tvposter;
        }

        public void setName(String str) {
            this.name = str;
        }

        public void setTvposter(List<TvposterBean> list) {
            this.tvposter = list;
        }
    }

    private FanartTVApi() {
        this.f12504.put("api-key", "021b59b4d93a4fa729dc7af7f2e6833c");
        this.f12504.put("client-key", "daac735e214d05269d79bc50cdbe277c");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static FanartTVApi m15723() {
        FanartTVApi fanartTVApi = f12503;
        if (fanartTVApi == null) {
            Class<FanartTVApi> cls = FanartTVApi.class;
            synchronized (FanartTVApi.class) {
                try {
                    fanartTVApi = f12503;
                    if (fanartTVApi == null) {
                        FanartTVApi fanartTVApi2 = new FanartTVApi();
                        try {
                            f12503 = fanartTVApi2;
                            fanartTVApi = fanartTVApi2;
                        } catch (Throwable th) {
                            th = th;
                            FanartTVApi fanartTVApi3 = fanartTVApi2;
                            FanartTVApi fanartTVApi4 = fanartTVApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return fanartTVApi;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m15724(String str) {
        return HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[]{this.f12504});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m15725(int i) {
        String str;
        try {
            String r0 = m15724("http://webservice.fanart.tv/v3/tv/" + i);
            if (r0.isEmpty()) {
                str = null;
            } else {
                GetTvShowImageResult getTvShowImageResult = (GetTvShowImageResult) new Gson().fromJson(r0, GetTvShowImageResult.class);
                if (getTvShowImageResult == null) {
                    str = null;
                } else {
                    List<GetTvShowImageResult.TvposterBean> tvposter = getTvShowImageResult.getTvposter();
                    if (tvposter == null || tvposter.isEmpty()) {
                        str = null;
                    } else {
                        ArrayList arrayList = new ArrayList();
                        for (GetTvShowImageResult.TvposterBean next : tvposter) {
                            String lang = next.getLang();
                            String url = next.getUrl();
                            if (!(lang == null || url == null || url.isEmpty())) {
                                String lowerCase = lang.trim().toLowerCase();
                                if (lowerCase.equals("00") || lowerCase.isEmpty() || lowerCase.equals("en")) {
                                    arrayList.add(next);
                                }
                            }
                        }
                        if (arrayList.isEmpty()) {
                            str = null;
                        } else {
                            Collections.sort(arrayList, new Comparator<GetTvShowImageResult.TvposterBean>() {
                                public int compare(GetTvShowImageResult.TvposterBean tvposterBean, GetTvShowImageResult.TvposterBean tvposterBean2) {
                                    String likes = tvposterBean.getLikes();
                                    int parseInt = (likes == null || likes.isEmpty() || !Utils.m6426(likes)) ? -1 : Integer.parseInt(likes);
                                    String likes2 = tvposterBean2.getLikes();
                                    int parseInt2 = (likes2 == null || likes2.isEmpty() || !Utils.m6426(likes2)) ? -1 : Integer.parseInt(likes2);
                                    int i = -1;
                                    String lowerCase = tvposterBean.getLang().trim().toLowerCase();
                                    if (lowerCase.equals("en")) {
                                        i = 2;
                                    } else if (lowerCase.isEmpty()) {
                                        i = 1;
                                    } else if (lowerCase.equals("00")) {
                                        i = 0;
                                    }
                                    int i2 = -1;
                                    String lowerCase2 = tvposterBean2.getLang().trim().toLowerCase();
                                    if (lowerCase2.equals("en")) {
                                        i2 = 2;
                                    } else if (lowerCase2.isEmpty()) {
                                        i2 = 1;
                                    } else if (lowerCase2.equals("00")) {
                                        i2 = 0;
                                        int i3 = 3 >> 0;
                                    }
                                    int i4 = 0;
                                    if (parseInt > -1 && parseInt2 > -1) {
                                        i4 = Utils.m6411(parseInt2, parseInt);
                                    }
                                    return i4 == 0 ? Utils.m6411(i2, i) : i4;
                                }
                            });
                            str = !arrayList.isEmpty() ? ((GetTvShowImageResult.TvposterBean) arrayList.get(0)).getUrl() : null;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            str = null;
        }
        return str;
    }
}
