package com.typhoon.tv.api;

import com.google.gson.Gson;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaRatingsModel;
import com.typhoon.tv.model.media.OmdbMediaInfoModel;
import com.typhoon.tv.utils.Utils;
import java.util.Iterator;
import java.util.Map;
import okhttp3.ResponseBody;

public class OmdbApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile OmdbApi f12510;

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaRatingsModel m15734(String str) {
        MediaRatingsModel mediaRatingsModel;
        ResponseBody r0 = HttpHelper.m6343().m6369(str, (Map<String, String>[]) new Map[0]);
        if (r0 == null) {
            mediaRatingsModel = null;
        } else {
            try {
                OmdbMediaInfoModel omdbMediaInfoModel = (OmdbMediaInfoModel) new Gson().fromJson(r0.m7092(), OmdbMediaInfoModel.class);
                mediaRatingsModel = new MediaRatingsModel();
                if (omdbMediaInfoModel.getImdbRating() != null && !omdbMediaInfoModel.getImdbRating().isEmpty() && !omdbMediaInfoModel.getImdbRating().trim().toLowerCase().equals("n/a")) {
                    mediaRatingsModel.setImdbRating(omdbMediaInfoModel.getImdbRating().trim());
                }
                if (omdbMediaInfoModel.getTomatoMeter() == null || omdbMediaInfoModel.getTomatoMeter().isEmpty() || omdbMediaInfoModel.getTomatoMeter().trim().toLowerCase().equals("n/a")) {
                    try {
                        Iterator<OmdbMediaInfoModel.RatingsBean> it2 = omdbMediaInfoModel.getRatings().iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            OmdbMediaInfoModel.RatingsBean next = it2.next();
                            try {
                                if (next.getSource().toLowerCase().equals("rotten tomatoes")) {
                                    mediaRatingsModel.setRottenTomatoesRating(next.getValue().trim().replace("%", ""));
                                    break;
                                }
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                            }
                        }
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                } else {
                    mediaRatingsModel.setRottenTomatoesRating(omdbMediaInfoModel.getTomatoMeter().trim().replace("%", ""));
                }
                r0.close();
            } catch (Exception e3) {
                Logger.m6281((Throwable) e3, new boolean[0]);
                r0.close();
                mediaRatingsModel = null;
            } catch (Throwable th) {
                r0.close();
                throw th;
            }
        }
        return mediaRatingsModel;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static OmdbApi m15735() {
        OmdbApi omdbApi = f12510;
        if (omdbApi == null) {
            synchronized (OmdbApi.class) {
                try {
                    omdbApi = f12510;
                    if (omdbApi == null) {
                        OmdbApi omdbApi2 = new OmdbApi();
                        try {
                            f12510 = omdbApi2;
                            OmdbApi omdbApi3 = omdbApi2;
                            omdbApi = omdbApi2;
                        } catch (Throwable th) {
                            th = th;
                            OmdbApi omdbApi4 = omdbApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return omdbApi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaRatingsModel m15736(String str) {
        return m15734("http://www.omdbapi.com/?i=" + str + "&type=series&tomatoes=true&apikey=" + TyphoonApp.f5836);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaRatingsModel m15737(String str, int... iArr) {
        int i = -1;
        if (iArr != null && iArr.length > 0) {
            i = iArr[0];
        }
        String str2 = "http://www.omdbapi.com/?t=" + Utils.m6414(str, new boolean[0]) + "&type=series&tomatoes=true&apikey=" + TyphoonApp.f5836;
        if (i > 0) {
            str2 = str2 + "&y=" + i;
        }
        return m15734(str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRatingsModel m15738(String str) {
        return m15734("http://www.omdbapi.com/?i=" + str + "&type=movie&tomatoes=true&apikey=" + TyphoonApp.f5836);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRatingsModel m15739(String str, int... iArr) {
        int i = -1;
        if (iArr != null && iArr.length > 0) {
            i = iArr[0];
        }
        String str2 = "http://www.omdbapi.com/?t=" + Utils.m6414(str, new boolean[0]) + "&type=movie&tomatoes=true&apikey=" + TyphoonApp.f5836;
        if (i > 0) {
            str2 = str2 + "&y=" + i;
        }
        return m15734(str2);
    }
}
