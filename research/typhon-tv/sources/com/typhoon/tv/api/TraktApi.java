package com.typhoon.tv.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.helper.MediaPosterUrlCacheHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.HttpHeaderBodyResult;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.movie.tmdb.TmdbMovieInfoResult;
import com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;

public class TraktApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile TraktApi f12516;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Hashtable<String, String> f12517 = new Hashtable<>();

    private TraktApi() {
        this.f12517.put(OAuth.HeaderType.CONTENT_TYPE, "application/json");
        this.f12517.put("trakt-api-key", "701afbabf01f9fb0e2bc524e94afa810d9c6742026d27c8da2611acbc757cbb6");
        this.f12517.put("trakt-api-version", "2");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TraktApi m15764() {
        TraktApi traktApi = f12516;
        if (traktApi == null) {
            Class<TraktApi> cls = TraktApi.class;
            synchronized (TraktApi.class) {
                try {
                    traktApi = f12516;
                    if (traktApi == null) {
                        TraktApi traktApi2 = new TraktApi();
                        try {
                            f12516 = traktApi2;
                            TraktApi traktApi3 = traktApi2;
                            traktApi = traktApi2;
                        } catch (Throwable th) {
                            th = th;
                            TraktApi traktApi4 = traktApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return traktApi;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private HttpHeaderBodyResult m15765(String str) {
        boolean z = true | true;
        return HttpHelper.m6343().m6355(str, (Map<String, String>[]) new Map[]{this.f12517});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaApiResult m15766(HttpHeaderBodyResult httpHeaderBodyResult) {
        MediaApiResult mediaApiResult;
        List list;
        Map<String, List<String>> headers = httpHeaderBodyResult.getHeaders();
        String body = httpHeaderBodyResult.getBody();
        if (body.isEmpty()) {
            mediaApiResult = null;
        } else {
            int i = 1;
            if (headers.containsKey("X-Pagination-Page-Count") && (list = headers.get("X-Pagination-Page-Count")) != null && list.size() > 0) {
                i = Integer.parseInt((String) list.get(0));
            }
            try {
                JsonElement parse = new JsonParser().parse(body);
                if (parse == null || !parse.isJsonArray()) {
                    mediaApiResult = null;
                } else {
                    JsonArray asJsonArray = parse.getAsJsonArray();
                    if (asJsonArray.size() <= 0) {
                        mediaApiResult = null;
                    } else {
                        ArrayList arrayList = new ArrayList();
                        Iterator<JsonElement> it2 = asJsonArray.iterator();
                        while (it2.hasNext()) {
                            JsonElement next = it2.next();
                            if (!next.isJsonNull()) {
                                boolean z = true & true;
                                MediaInfo r4 = m15767(next.getAsJsonObject(), true);
                                if (r4 != null) {
                                    arrayList.add(r4);
                                }
                            }
                        }
                        mediaApiResult = new MediaApiResult(arrayList, i);
                    }
                }
            } catch (Exception e) {
                mediaApiResult = null;
            }
        }
        return mediaApiResult;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo m15767(JsonObject jsonObject, boolean z) {
        JsonObject jsonObject2;
        int asInt;
        MediaInfo mediaInfo = null;
        if (jsonObject.get("show") == null || jsonObject.get("show").isJsonNull()) {
            if (!(jsonObject.get(PubnativeAsset.TITLE) == null || jsonObject.get(PubnativeAsset.TITLE).getAsString() == null)) {
                jsonObject2 = jsonObject;
            }
            return mediaInfo;
        }
        jsonObject2 = jsonObject.get("show").getAsJsonObject();
        if (!TVApplication.m6285().getBoolean("pref_filter_out_non_english_shows", false) || (jsonObject2.get("language") != null && !jsonObject2.get("language").isJsonNull() && jsonObject2.get("language").getAsString().toLowerCase().equals("en"))) {
            JsonElement jsonElement = jsonObject2.get(PubnativeAsset.TITLE);
            if (!jsonElement.isJsonNull() && jsonElement.getAsString() != null) {
                String asString = jsonObject2.get(PubnativeAsset.TITLE).getAsString();
                Integer num = 0;
                if (!jsonObject2.get("year").isJsonNull()) {
                    num = Integer.valueOf(jsonObject2.get("year").getAsInt());
                }
                if (!jsonObject2.get("ids").isJsonNull()) {
                    JsonObject asJsonObject = jsonObject2.get("ids").getAsJsonObject();
                    if (!asJsonObject.get("tmdb").isJsonNull() && (asInt = asJsonObject.get("tmdb").getAsInt()) > 0) {
                        int asInt2 = asJsonObject.get("tvdb").isJsonNull() ? -1 : asJsonObject.get("tvdb").getAsInt();
                        String str = null;
                        if (!asJsonObject.get("imdb").isJsonNull() && asJsonObject.get("imdb").getAsString() != null) {
                            str = asJsonObject.get("imdb").getAsString();
                        }
                        String str2 = "";
                        if (z && !TVApplication.m6285().getBoolean("pref_hide_poster_thumb", false)) {
                            str2 = m15769(0, asInt, asInt2, str);
                        }
                        mediaInfo = new MediaInfo(0, 2, asInt, asString, num.intValue());
                        mediaInfo.setPosterUrl(str2);
                        if (str != null && !str.isEmpty()) {
                            mediaInfo.setImdbId(str);
                        }
                        mediaInfo.setTvdbId(asInt2);
                    }
                }
            }
        }
        return mediaInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaApiResult m15768(int i, int i2, int i3) {
        String str;
        String str2;
        if (i == 3) {
            str2 = "https://api.trakt.tv/calendars/all/shows/new/" + DateTimeHelper.m15935(14) + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + 15 + "?limit=20&extended=full,images&page=" + i3;
        } else if (i == 4) {
            str2 = "https://api.trakt.tv/calendars/all/shows/premieres/" + DateTimeHelper.m15935(14) + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + 15 + "?limit=20&extended=full,images&page=" + i3;
        } else {
            switch (i) {
                case 1:
                    str = "trending";
                    break;
                case 6:
                    str = "popular";
                    break;
                case 7:
                    str = "played/all";
                    break;
                case 8:
                    str = "watched/all";
                    break;
                default:
                    str = "trending";
                    break;
            }
            str2 = "https://api.trakt.tv/shows/" + str + "?limit=20&extended=full,images&page=" + i3;
            if (i2 > -1) {
                str2 = str2 + "&years=" + i2;
            }
        }
        HttpHeaderBodyResult r0 = m15765(str2);
        if (r0 == null) {
            return null;
        }
        return m15766(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m15769(int i, int i2, int i3, String str) {
        String imdb_id;
        boolean z = i == 1;
        String str2 = null;
        try {
            str2 = MediaPosterUrlCacheHelper.m15958().m15959(i2, i3, str);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        boolean z2 = i2 > -1 && new Random().nextInt(100) > 50;
        boolean z3 = false;
        if ((str2 == null || str2.isEmpty()) && !z && i3 > -1 && !z2) {
            str2 = TvdbApi.m15813().m15818(i3);
            z3 = true;
        }
        if ((str2 == null || str2.isEmpty()) && i2 > -1) {
            if (z) {
                try {
                    TmdbMovieInfoResult r7 = TmdbApi.m15742().m15751(i2);
                    if (r7 != null) {
                        if (!(r7.getImdb_id() == null || (imdb_id = r7.getImdb_id()) == null || !imdb_id.startsWith(TtmlNode.TAG_TT))) {
                            str = imdb_id;
                        }
                        if (r7.getPoster_path() != null) {
                            str2 = r7.getPoster_path();
                        }
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            } else {
                TmdbTvInfoResult r8 = TmdbApi.m15742().m15759(i2);
                if (r8 != null) {
                    if (r8.getExternal_ids() != null) {
                        String imdb_id2 = r8.getExternal_ids().getImdb_id();
                        if (imdb_id2 != null && imdb_id2.startsWith(TtmlNode.TAG_TT)) {
                            String str3 = imdb_id2;
                            str = imdb_id2;
                        }
                        int tvdb_id = r8.getExternal_ids().getTvdb_id();
                        if (tvdb_id > -1) {
                            int i4 = tvdb_id;
                            i3 = tvdb_id;
                        }
                    }
                    if (r8.getPoster_path() != null) {
                        str2 = r8.getPoster_path();
                    }
                }
            }
        }
        if ((str2 == null || str2.isEmpty()) && !z3 && !z && i3 > -1) {
            str2 = TvdbApi.m15813().m15818(i3);
        }
        if ((str2 == null || str2.isEmpty()) && !z && i3 > -1) {
            str2 = FanartTVApi.m15723().m15725(i3);
        }
        if (str2 != null && !str2.isEmpty()) {
            MediaPosterUrlCacheHelper.m15958().m15960(str2, i2, i3, str);
        }
        return str2 == null ? "" : str2;
    }
}
