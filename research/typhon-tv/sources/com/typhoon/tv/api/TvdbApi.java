package com.typhoon.tv.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.CheckEpisodeResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvEpisodeInfo;
import com.typhoon.tv.model.media.tv.tvdb.TvdbEpisodeInfoResult;
import com.typhoon.tv.model.media.tv.tvdb.TvdbTvImagesResult;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.Request;
import org.apache.oltu.oauth2.common.OAuth;

public class TvdbApi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile TvdbApi f12557;

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f12558 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Hashtable<String, String> f12559 = new Hashtable<>();

    private TvdbApi() {
        this.f12559.put(OAuth.HeaderType.CONTENT_TYPE, "application/json");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static TvdbApi m15813() {
        TvdbApi tvdbApi = f12557;
        if (tvdbApi == null) {
            Class<TvdbApi> cls = TvdbApi.class;
            synchronized (TvdbApi.class) {
                try {
                    tvdbApi = f12557;
                    if (tvdbApi == null) {
                        TvdbApi tvdbApi2 = new TvdbApi();
                        try {
                            f12557 = tvdbApi2;
                            TvdbApi tvdbApi3 = tvdbApi2;
                            tvdbApi = tvdbApi2;
                        } catch (Throwable th) {
                            th = th;
                            TvdbApi tvdbApi4 = tvdbApi2;
                            TvdbApi tvdbApi5 = tvdbApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return tvdbApi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m15814(String str) {
        if (!this.f12559.containsKey(OAuth.HeaderType.AUTHORIZATION)) {
            m15815();
        }
        if (!this.f12559.containsKey(OAuth.HeaderType.AUTHORIZATION)) {
            return "";
        }
        Request.Builder r1 = new Request.Builder().m19990().m19992(str).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
        for (Map.Entry next : this.f12559.entrySet()) {
            r1.m19993((String) next.getKey(), (String) next.getValue());
        }
        String r2 = HttpHelper.m6343().m6364(r1.m19989());
        if (!r2.isEmpty() && !r2.contains("TvdbApi Error") && !r2.contains("525: SSL handshake failed") && !r2.contains("522: Connection timed out") && !r2.contains("503 Service Unavailable") && !r2.contains("<span class=\"cf-status-label\">Error</span>")) {
            return r2;
        }
        f12558 = true;
        return "";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m15815() {
        JsonElement parse;
        JsonElement jsonElement;
        String asString;
        int i = 0;
        while (i < 5) {
            try {
                String r4 = HttpHelper.m6343().m6360("https://api.thetvdb.com/login", "{\"apikey\":\"" + Utils.m6414("639087D0F3EE3087", new boolean[0]) + "\"}", false, (Map<String, String>[]) new Map[]{this.f12559});
                if (r4.isEmpty() || r4.contains("TvdbApi Error") || r4.contains("525: SSL handshake failed") || r4.contains("522: Connection timed out") || r4.contains("503 Service Unavailable") || r4.contains("<span class=\"cf-status-label\">Error</span>")) {
                    f12558 = true;
                    return;
                }
                if (!r4.trim().isEmpty() && (parse = new JsonParser().parse(r4)) != null && parse.isJsonObject() && (jsonElement = parse.getAsJsonObject().get("token")) != null && !jsonElement.isJsonNull() && (asString = jsonElement.getAsString()) != null && !asString.isEmpty()) {
                    this.f12559.put(OAuth.HeaderType.AUTHORIZATION, "Bearer " + asString);
                    return;
                }
                i++;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m15816() {
        Class<TvdbApi> cls = TvdbApi.class;
        synchronized (TvdbApi.class) {
            int i = 0 >> 0;
            try {
                f12558 = false;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m15817(String str) {
        int i;
        int i2 = 0;
        if (f12558) {
            i = 0;
        } else {
            try {
                if (!str.startsWith(TtmlNode.TAG_TT)) {
                    str = TtmlNode.TAG_TT + str;
                }
                boolean z = true | true;
                String r1 = Regex.m17802(m15814("https://api.thetvdb.com/search/series?imdbId=" + str), "['\"]?id['\"]?\\s*:\\s*['\"]?(\\d+)['\"]?", 1, true);
                if (!r1.isEmpty() && Utils.m6426(r1)) {
                    i2 = Integer.parseInt(r1);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, true);
            }
            int i3 = i2;
            i = i2;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m15818(int i) {
        List<TvdbTvImagesResult.DataBean> data;
        boolean z = false & false;
        if (f12558) {
            return null;
        }
        try {
            String r0 = m15814("https://api.thetvdb.com/series/" + i + "/images/query?keyType=poster");
            if (r0.isEmpty()) {
                return null;
            }
            Class<TvdbTvImagesResult> cls = TvdbTvImagesResult.class;
            TvdbTvImagesResult tvdbTvImagesResult = (TvdbTvImagesResult) new Gson().fromJson(r0, TvdbTvImagesResult.class);
            if (tvdbTvImagesResult == null || (data = tvdbTvImagesResult.getData()) == null || data.isEmpty()) {
                return null;
            }
            ArrayList arrayList = new ArrayList(data);
            Collections.sort(arrayList, new Comparator<TvdbTvImagesResult.DataBean>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public int compare(TvdbTvImagesResult.DataBean dataBean, TvdbTvImagesResult.DataBean dataBean2) {
                    int i;
                    TvdbTvImagesResult.DataBean.RatingsInfoBean ratingsInfo = dataBean.getRatingsInfo();
                    TvdbTvImagesResult.DataBean.RatingsInfoBean ratingsInfo2 = dataBean2.getRatingsInfo();
                    if (ratingsInfo == null || ratingsInfo2 == null) {
                        i = 0;
                    } else {
                        i = 0;
                        if (ratingsInfo.getCount() > -1 && ratingsInfo2.getCount() > -1) {
                            i = Utils.m6411(ratingsInfo2.getCount(), ratingsInfo.getCount());
                        }
                        if (i == 0 && ratingsInfo.getAverage() > -1.0d && ratingsInfo2.getAverage() > -1.0d) {
                            i = Double.compare(ratingsInfo2.getAverage(), ratingsInfo.getAverage());
                        }
                    }
                    return i;
                }
            });
            return "http://thetvdb.com/banners/" + ((TvdbTvImagesResult.DataBean) arrayList.get(0)).getFileName();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public LinkedHashMap<Integer, TvEpisodeInfo> m15819(MediaInfo mediaInfo, int i) {
        LinkedHashMap<Integer, TvEpisodeInfo> linkedHashMap = new LinkedHashMap<>();
        if (!f12558) {
            try {
                int tvdbId = mediaInfo.getTvdbId();
                if (tvdbId > 0) {
                    StringBuilder append = new StringBuilder().append("https://api.thetvdb.com/series/");
                    StringBuilder sb = append;
                    StringBuilder append2 = append.append(tvdbId).append("/episodes/query?airedSeason=");
                    StringBuilder sb2 = append2;
                    int i2 = i;
                    String r8 = m15814(append2.append(i).toString());
                    Gson gson = new Gson();
                    Class<TvdbEpisodeInfoResult> cls = TvdbEpisodeInfoResult.class;
                    Class<TvdbEpisodeInfoResult> cls2 = TvdbEpisodeInfoResult.class;
                    Gson gson2 = gson;
                    Class<TvdbEpisodeInfoResult> cls3 = cls2;
                    TvdbEpisodeInfoResult tvdbEpisodeInfoResult = (TvdbEpisodeInfoResult) gson.fromJson(r8, cls2);
                    if (tvdbEpisodeInfoResult != null) {
                        CheckEpisodeResult checkEpisodeResult = null;
                        try {
                            checkEpisodeResult = TVApplication.m6287().m6306(Integer.valueOf(mediaInfo.getTmdbId()));
                        } catch (Exception e) {
                            boolean[] zArr = new boolean[0];
                            boolean[] zArr2 = zArr;
                            boolean[] zArr3 = zArr;
                            boolean[] zArr4 = zArr3;
                            Logger.m6281((Throwable) e, zArr3);
                        }
                        boolean z = false;
                        if (tvdbEpisodeInfoResult.getData() != null) {
                            for (TvdbEpisodeInfoResult.DataBean next : tvdbEpisodeInfoResult.getData()) {
                                try {
                                    int airedEpisodeNumber = next.getAiredEpisodeNumber();
                                    if (airedEpisodeNumber > 0) {
                                        String str = "";
                                        if (next.getFirstAired() != null) {
                                            str = next.getFirstAired();
                                        }
                                        if (TVApplication.m6285().getBoolean("pref_hide_unaired_episode", true)) {
                                            if (!str.isEmpty()) {
                                                if (!DateTimeHelper.m15933(DateTimeHelper.m15937(str))) {
                                                    if (!z && checkEpisodeResult != null) {
                                                        int lastSeason = checkEpisodeResult.getLastSeason();
                                                        int i3 = i;
                                                        int i4 = lastSeason;
                                                        if (i == lastSeason && airedEpisodeNumber == checkEpisodeResult.getLastEpisode()) {
                                                            z = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        String str2 = "";
                                        if (next.getOverview() != null) {
                                            str2 = next.getOverview();
                                        }
                                        String episodeName = next.getEpisodeName();
                                        if (episodeName == null || episodeName.isEmpty()) {
                                            StringBuilder append3 = new StringBuilder().append("Episode ");
                                            StringBuilder sb3 = append3;
                                            episodeName = append3.append(airedEpisodeNumber).toString();
                                        }
                                        Integer valueOf = Integer.valueOf(i);
                                        Integer valueOf2 = Integer.valueOf(airedEpisodeNumber);
                                        Integer num = valueOf;
                                        Integer num2 = valueOf2;
                                        TvEpisodeInfo tvEpisodeInfo = new TvEpisodeInfo(valueOf, valueOf2, episodeName, "");
                                        tvEpisodeInfo.setAirDate(str);
                                        tvEpisodeInfo.setOverview(str2);
                                        Integer valueOf3 = Integer.valueOf(airedEpisodeNumber);
                                        Integer num3 = valueOf3;
                                        linkedHashMap.put(valueOf3, tvEpisodeInfo);
                                    }
                                } catch (Exception e2) {
                                    boolean[] zArr5 = new boolean[0];
                                    boolean[] zArr6 = zArr5;
                                    boolean[] zArr7 = zArr5;
                                    boolean[] zArr8 = zArr7;
                                    Logger.m6281((Throwable) e2, zArr7);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e3) {
                boolean[] zArr9 = new boolean[0];
                boolean[] zArr10 = zArr9;
                boolean[] zArr11 = zArr9;
                boolean[] zArr12 = zArr11;
                Logger.m6281((Throwable) e3, zArr11);
            }
        }
        return linkedHashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Integer> m15820(int i, String str) {
        int parseInt;
        ArrayList arrayList = new ArrayList();
        if (i <= 0 && str != null) {
            try {
                if (!str.isEmpty()) {
                    i = m15817(str);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, true);
            }
        }
        if (i > 0) {
            Iterator it2 = Utils.m6415(Regex.m17805(HttpHelper.m6343().m6358("https://www.thetvdb.com/?tab=series&id=" + i, "https://www.thetvdb.com"), "<a[^>]+href=['\"][^'\"]*series/[^'\"]+/seasons/(\\d+)/?['\"][^>]*>", 1, true).get(0)).iterator();
            while (it2.hasNext()) {
                String str2 = (String) it2.next();
                try {
                    if (Utils.m6426(str2) && (parseInt = Integer.parseInt(str2)) >= 0) {
                        if (parseInt != 0 || TVApplication.m6285().getBoolean("pref_show_season_special", false)) {
                            arrayList.add(Integer.valueOf(parseInt));
                        }
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            }
        }
        return arrayList;
    }
}
