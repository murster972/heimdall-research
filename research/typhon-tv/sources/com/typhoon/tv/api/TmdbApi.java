package com.typhoon.tv.api;

import com.google.gson.Gson;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.helper.category.MovieCategoryHelper;
import com.typhoon.tv.helper.category.TvShowCategoryHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.movie.tmdb.TmdbMovieInfoResult;
import com.typhoon.tv.model.media.tv.TvSeasonInfo;
import com.typhoon.tv.model.media.tv.tmdb.TmdbTvFindResult;
import com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Map;

public class TmdbApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile TmdbApi f12511;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m15740(int i, int i2) {
        return i == 1408 && i2 == 6;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v40, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: java.util.List<com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult$ResultsBean>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v43, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.typhoon.tv.model.media.MediaApiResult m15741(java.lang.String r22) {
        /*
            r21 = this;
            com.typhoon.tv.helper.http.HttpHelper r2 = com.typhoon.tv.helper.http.HttpHelper.m6343()
            r3 = 0
            java.util.Map[] r3 = new java.util.Map[r3]
            r0 = r22
            r0 = r22
            java.lang.String r7 = r2.m6351((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r3)
            boolean r2 = r7.isEmpty()
            if (r2 == 0) goto L_0x0017
            r2 = 0
        L_0x0016:
            return r2
        L_0x0017:
            r8 = 0
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ Exception -> 0x002c }
            r2.<init>()     // Catch:{ Exception -> 0x002c }
            java.lang.Class<com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult> r3 = com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult.class
            java.lang.Object r2 = r2.fromJson((java.lang.String) r7, r3)     // Catch:{ Exception -> 0x002c }
            r0 = r2
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult r0 = (com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult) r0     // Catch:{ Exception -> 0x002c }
            r8 = r0
            r8 = r0
        L_0x0028:
            if (r8 != 0) goto L_0x0034
            r2 = 0
            goto L_0x0016
        L_0x002c:
            r11 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r11, (boolean[]) r2)
            goto L_0x0028
        L_0x0034:
            java.util.List r16 = r8.getResults()
            if (r16 != 0) goto L_0x007a
            com.google.gson.JsonParser r2 = new com.google.gson.JsonParser     // Catch:{ Exception -> 0x0072 }
            r2.<init>()     // Catch:{ Exception -> 0x0072 }
            com.google.gson.JsonElement r2 = r2.parse((java.lang.String) r7)     // Catch:{ Exception -> 0x0072 }
            com.google.gson.JsonObject r2 = r2.getAsJsonObject()     // Catch:{ Exception -> 0x0072 }
            java.lang.String r3 = "items"
            java.lang.String r3 = "items"
            com.google.gson.JsonElement r2 = r2.get(r3)     // Catch:{ Exception -> 0x0072 }
            com.google.gson.JsonArray r13 = r2.getAsJsonArray()     // Catch:{ Exception -> 0x0072 }
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0072 }
            r2.<init>()     // Catch:{ Exception -> 0x0072 }
            com.typhoon.tv.api.TmdbApi$1 r3 = new com.typhoon.tv.api.TmdbApi$1     // Catch:{ Exception -> 0x0072 }
            r0 = r21
            r3.<init>()     // Catch:{ Exception -> 0x0072 }
            java.lang.reflect.Type r3 = r3.getType()     // Catch:{ Exception -> 0x0072 }
            java.lang.Object r2 = r2.fromJson((com.google.gson.JsonElement) r13, (java.lang.reflect.Type) r3)     // Catch:{ Exception -> 0x0072 }
            r0 = r2
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0072 }
            r16 = r0
        L_0x006e:
            if (r16 != 0) goto L_0x007a
            r2 = 0
            goto L_0x0016
        L_0x0072:
            r10 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r10, (boolean[]) r2)
            goto L_0x006e
        L_0x007a:
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            java.util.Iterator r20 = r16.iterator()
        L_0x0083:
            boolean r2 = r20.hasNext()
            if (r2 == 0) goto L_0x015e
            java.lang.Object r9 = r20.next()
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult$ResultsBean r9 = (com.typhoon.tv.model.media.tv.tmdb.TmdbTvShowResult.ResultsBean) r9
            android.content.SharedPreferences r2 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x0155 }
            java.lang.String r3 = "pref_filter_out_non_english_shows"
            java.lang.String r3 = "pref_filter_out_non_english_shows"
            r6 = 0
            boolean r2 = r2.getBoolean(r3, r6)     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x00bb
            java.lang.String r2 = r9.getOriginal_language()     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x00bb
            java.lang.String r2 = r9.getOriginal_language()     // Catch:{ Exception -> 0x0155 }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0155 }
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ Exception -> 0x0155 }
            java.lang.String r3 = "en"
            boolean r2 = r2.contains(r3)     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x0083
        L_0x00bb:
            java.lang.String r5 = r9.getName()     // Catch:{ Exception -> 0x0155 }
            if (r5 == 0) goto L_0x0083
            java.lang.String r19 = "0"
            java.lang.String r19 = "0"
            java.lang.String r2 = r9.getFirst_air_date()     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x00e4
            java.lang.String r12 = r9.getFirst_air_date()     // Catch:{ Exception -> 0x0155 }
            boolean r2 = r12.isEmpty()     // Catch:{ Exception -> 0x0155 }
            if (r2 != 0) goto L_0x00e4
            int r2 = r12.length()     // Catch:{ Exception -> 0x0155 }
            r3 = 4
            if (r2 < r3) goto L_0x00e4
            r2 = 0
            r3 = 4
            java.lang.String r19 = r12.substring(r2, r3)     // Catch:{ Exception -> 0x0155 }
        L_0x00e4:
            java.lang.String r2 = "Will & Grace"
            boolean r2 = r5.equals(r2)     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x00fd
            java.lang.String r2 = "2017"
            java.lang.String r2 = "2017"
            r0 = r19
            r0 = r19
            boolean r2 = r0.equals(r2)     // Catch:{ Exception -> 0x0155 }
            if (r2 != 0) goto L_0x0083
        L_0x00fd:
            java.lang.String r17 = ""
            java.lang.String r17 = ""
            java.lang.String r2 = r9.getBackdrop_path()     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x010d
            java.lang.String r17 = r9.getBackdrop_path()     // Catch:{ Exception -> 0x0155 }
        L_0x010d:
            java.lang.String r18 = ""
            java.lang.String r18 = ""
            android.content.SharedPreferences r2 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x0155 }
            java.lang.String r3 = "pref_hide_poster_thumb"
            java.lang.String r3 = "pref_hide_poster_thumb"
            r6 = 0
            boolean r2 = r2.getBoolean(r3, r6)     // Catch:{ Exception -> 0x0155 }
            if (r2 != 0) goto L_0x012e
            java.lang.String r2 = r9.getPoster_path()     // Catch:{ Exception -> 0x0155 }
            if (r2 == 0) goto L_0x012e
            java.lang.String r18 = r9.getPoster_path()     // Catch:{ Exception -> 0x0155 }
        L_0x012e:
            int r4 = r9.getId()     // Catch:{ Exception -> 0x0155 }
            com.typhoon.tv.model.media.MediaInfo r1 = new com.typhoon.tv.model.media.MediaInfo     // Catch:{ Exception -> 0x0155 }
            r2 = 0
            r3 = 1
            int r6 = java.lang.Integer.parseInt(r19)     // Catch:{ Exception -> 0x0155 }
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0155 }
            r0 = r17
            r0 = r17
            r1.setBannerUrl(r0)     // Catch:{ Exception -> 0x0155 }
            r0 = r18
            r1.setPosterUrl(r0)     // Catch:{ Exception -> 0x0155 }
            java.lang.String r2 = r9.getOriginal_name()     // Catch:{ Exception -> 0x0155 }
            r1.setOriginalName(r2)     // Catch:{ Exception -> 0x0155 }
            r14.add(r1)     // Catch:{ Exception -> 0x0155 }
            goto L_0x0083
        L_0x0155:
            r10 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r10, (boolean[]) r2)
            goto L_0x0083
        L_0x015e:
            r15 = 1
            int r15 = r8.getTotal_pages()     // Catch:{ Exception -> 0x016a }
        L_0x0163:
            com.typhoon.tv.model.media.MediaApiResult r2 = new com.typhoon.tv.model.media.MediaApiResult
            r2.<init>(r14, r15)
            goto L_0x0016
        L_0x016a:
            r10 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r10, (boolean[]) r2)
            goto L_0x0163
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.api.TmdbApi.m15741(java.lang.String):com.typhoon.tv.model.media.MediaApiResult");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TmdbApi m15742() {
        TmdbApi tmdbApi = f12511;
        if (tmdbApi == null) {
            Class<TmdbApi> cls = TmdbApi.class;
            synchronized (TmdbApi.class) {
                try {
                    tmdbApi = f12511;
                    if (tmdbApi == null) {
                        TmdbApi tmdbApi2 = new TmdbApi();
                        try {
                            f12511 = tmdbApi2;
                            tmdbApi = tmdbApi2;
                        } catch (Throwable th) {
                            th = th;
                            TmdbApi tmdbApi3 = tmdbApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return tmdbApi;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private TmdbTvFindResult m15743(String str, String str2) {
        TmdbTvFindResult tmdbTvFindResult = null;
        try {
            Class<TmdbTvFindResult> cls = TmdbTvFindResult.class;
            Object fromJson = new Gson().fromJson(HttpHelper.m6343().m6351("https://api.themoviedb.org/3/find/" + str2 + "?language=en-US&external_source=" + str + "&api_key=" + "c24e18a4a36e5bc4457fc52c790a4091", (Map<String, String>[]) new Map[0]), TmdbTvFindResult.class);
            Object obj = fromJson;
            TmdbTvFindResult tmdbTvFindResult2 = (TmdbTvFindResult) fromJson;
            TmdbTvFindResult tmdbTvFindResult3 = tmdbTvFindResult2;
            tmdbTvFindResult = tmdbTvFindResult2;
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return tmdbTvFindResult;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m15744(int i) {
        TmdbMovieInfoResult r0 = m15742().m15751(i);
        return r0 != null ? r0.getImdb_id() : null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m15745(int i) {
        TmdbTvInfoResult r0 = m15742().m15759(i);
        return (r0 == null || r0.getExternal_ids() == null) ? null : r0.getExternal_ids().getImdb_id();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public MediaApiResult m15746(int i) {
        return m15749("https://api.themoviedb.org/3/movie/" + i + "/recommendations?append_to_response=external_ids&api_key=" + "17834cfece29c7a34ccee906f356e660");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaApiResult m15747(int i) {
        return m15741("https://api.themoviedb.org/3/tv/" + i + "/recommendations?append_to_response=external_ids&api_key=" + "e4974305320f56b59353f0c32cabe6d0");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaApiResult m15748(int i, int i2, int i3) {
        String str;
        String str2;
        if (i == 3) {
            str2 = TyphoonApp.f5907 + "/newMovieReleases.json";
        } else if (i >= 5 && i <= 22) {
            str2 = "https://api.themoviedb.org/3/discover/movie?page=" + i3 + "&with_genres=" + MovieCategoryHelper.m15997().get(i, 28) + "&sort_by=popularity.desc&append_to_response=external_ids&api_key=" + "1bb93c63004e6a24f6d43aec78ea100f";
            if (i2 > -1) {
                str2 = str2 + "&primary_release_year=" + i2;
            }
        } else if (i < 23 || i > 29) {
            switch (i) {
                case 1:
                    str = "popular";
                    break;
                case 2:
                    str = "now_playing";
                    break;
                case 4:
                    str = "top_rated";
                    break;
                default:
                    str = "popular";
                    break;
            }
            str2 = "https://api.themoviedb.org/3/movie/" + str + "?page=" + i3 + "&append_to_response=external_ids&api_key=" + "8fa16ca5d5ea2aa228629528c28e54c2";
            if (str.equals("now_playing")) {
                str2 = str2 + "&language=en-US";
            }
        } else {
            str2 = "https://api.themoviedb.org/4/list/" + MovieCategoryHelper.m15999().get(i, 13392) + "?page=" + i3 + "&sort_by=popularity.desc&append_to_response=external_ids&api_key=" + "7357e022f87434042346d60ff148b56c";
        }
        return m15749(str2);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v35, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: java.util.List<com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult$ResultsBean>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.typhoon.tv.model.media.MediaApiResult m15749(java.lang.String r22) {
        /*
            r21 = this;
            com.typhoon.tv.helper.http.HttpHelper r2 = com.typhoon.tv.helper.http.HttpHelper.m6343()
            r3 = 0
            java.util.Map[] r3 = new java.util.Map[r3]
            r0 = r22
            r0 = r22
            java.lang.String r7 = r2.m6351((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r3)
            boolean r2 = r7.isEmpty()
            if (r2 == 0) goto L_0x0017
            r2 = 0
        L_0x0016:
            return r2
        L_0x0017:
            com.google.gson.Gson r2 = new com.google.gson.Gson
            r2.<init>()
            java.lang.Class<com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult> r3 = com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult.class
            java.lang.Class<com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult> r3 = com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult.class
            java.lang.Object r8 = r2.fromJson((java.lang.String) r7, r3)
            com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult r8 = (com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult) r8
            if (r8 != 0) goto L_0x002a
            r2 = 0
            goto L_0x0016
        L_0x002a:
            java.util.List r19 = r8.getResults()
            if (r19 != 0) goto L_0x0072
            com.google.gson.JsonParser r2 = new com.google.gson.JsonParser     // Catch:{ Exception -> 0x006a }
            r2.<init>()     // Catch:{ Exception -> 0x006a }
            com.google.gson.JsonElement r2 = r2.parse((java.lang.String) r7)     // Catch:{ Exception -> 0x006a }
            com.google.gson.JsonObject r2 = r2.getAsJsonObject()     // Catch:{ Exception -> 0x006a }
            java.lang.String r3 = "items"
            java.lang.String r3 = "items"
            com.google.gson.JsonElement r2 = r2.get(r3)     // Catch:{ Exception -> 0x006a }
            com.google.gson.JsonArray r12 = r2.getAsJsonArray()     // Catch:{ Exception -> 0x006a }
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ Exception -> 0x006a }
            r2.<init>()     // Catch:{ Exception -> 0x006a }
            com.typhoon.tv.api.TmdbApi$4 r3 = new com.typhoon.tv.api.TmdbApi$4     // Catch:{ Exception -> 0x006a }
            r0 = r21
            r0 = r21
            r3.<init>()     // Catch:{ Exception -> 0x006a }
            java.lang.reflect.Type r3 = r3.getType()     // Catch:{ Exception -> 0x006a }
            java.lang.Object r2 = r2.fromJson((com.google.gson.JsonElement) r12, (java.lang.reflect.Type) r3)     // Catch:{ Exception -> 0x006a }
            r0 = r2
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x006a }
            r19 = r0
        L_0x0066:
            if (r19 != 0) goto L_0x0072
            r2 = 0
            goto L_0x0016
        L_0x006a:
            r10 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r10, (boolean[]) r2)
            goto L_0x0066
        L_0x0072:
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            r11 = 0
            java.lang.String r2 = "movieNewReleases"
            java.lang.String r2 = "movieNewReleases"
            r0 = r22
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x009d
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r2 = "11:55"
            java.lang.String r2 = "11:55"
            r11.add(r2)
            java.lang.String r2 = "2:22"
            java.lang.String r2 = "2:22"
            r11.add(r2)
        L_0x009d:
            java.util.Iterator r20 = r19.iterator()
        L_0x00a1:
            boolean r2 = r20.hasNext()
            if (r2 == 0) goto L_0x0137
            java.lang.Object r9 = r20.next()
            com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult$ResultsBean r9 = (com.typhoon.tv.model.media.movie.tmdb.TmdbMovieResult.ResultsBean) r9
            java.lang.String r5 = r9.getTitle()     // Catch:{ Exception -> 0x012e }
            if (r5 == 0) goto L_0x00a1
            if (r11 == 0) goto L_0x00bb
            boolean r2 = r11.contains(r5)     // Catch:{ Exception -> 0x012e }
            if (r2 != 0) goto L_0x00a1
        L_0x00bb:
            java.lang.String r16 = "0"
            java.lang.String r16 = "0"
            java.lang.String r2 = r9.getRelease_date()     // Catch:{ Exception -> 0x012e }
            if (r2 == 0) goto L_0x00e2
            java.lang.String r18 = r9.getRelease_date()     // Catch:{ Exception -> 0x012e }
            boolean r2 = r18.isEmpty()     // Catch:{ Exception -> 0x012e }
            if (r2 != 0) goto L_0x00e2
            int r2 = r18.length()     // Catch:{ Exception -> 0x012e }
            r3 = 4
            if (r2 < r3) goto L_0x00e2
            r2 = 0
            r3 = 4
            r0 = r18
            r0 = r18
            java.lang.String r16 = r0.substring(r2, r3)     // Catch:{ Exception -> 0x012e }
        L_0x00e2:
            java.lang.String r14 = ""
            java.lang.String r2 = r9.getBackdrop_path()     // Catch:{ Exception -> 0x012e }
            if (r2 == 0) goto L_0x00ef
            java.lang.String r14 = r9.getBackdrop_path()     // Catch:{ Exception -> 0x012e }
        L_0x00ef:
            java.lang.String r15 = ""
            android.content.SharedPreferences r2 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x012e }
            java.lang.String r3 = "pref_hide_poster_thumb"
            java.lang.String r3 = "pref_hide_poster_thumb"
            r6 = 0
            boolean r2 = r2.getBoolean(r3, r6)     // Catch:{ Exception -> 0x012e }
            if (r2 != 0) goto L_0x010d
            java.lang.String r2 = r9.getPoster_path()     // Catch:{ Exception -> 0x012e }
            if (r2 == 0) goto L_0x010d
            java.lang.String r15 = r9.getPoster_path()     // Catch:{ Exception -> 0x012e }
        L_0x010d:
            int r4 = r9.getId()     // Catch:{ Exception -> 0x012e }
            com.typhoon.tv.model.media.MediaInfo r1 = new com.typhoon.tv.model.media.MediaInfo     // Catch:{ Exception -> 0x012e }
            r2 = 1
            r3 = 1
            int r6 = java.lang.Integer.parseInt(r16)     // Catch:{ Exception -> 0x012e }
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x012e }
            r1.setBannerUrl(r14)     // Catch:{ Exception -> 0x012e }
            r1.setPosterUrl(r15)     // Catch:{ Exception -> 0x012e }
            java.lang.String r2 = r9.getOriginal_title()     // Catch:{ Exception -> 0x012e }
            r1.setOriginalName(r2)     // Catch:{ Exception -> 0x012e }
            r13.add(r1)     // Catch:{ Exception -> 0x012e }
            goto L_0x00a1
        L_0x012e:
            r10 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r10, (boolean[]) r2)
            goto L_0x00a1
        L_0x0137:
            r17 = 1
            int r17 = r8.getTotal_pages()     // Catch:{ Exception -> 0x0148 }
        L_0x013d:
            com.typhoon.tv.model.media.MediaApiResult r2 = new com.typhoon.tv.model.media.MediaApiResult
            r0 = r17
            r0 = r17
            r2.<init>(r13, r0)
            goto L_0x0016
        L_0x0148:
            r10 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r10, (boolean[]) r2)
            goto L_0x013d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.api.TmdbApi.m15749(java.lang.String):com.typhoon.tv.model.media.MediaApiResult");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaApiResult m15750(String str, Integer num) {
        return m15749("https://api.themoviedb.org/3/search/movie?query=" + Utils.m6414(str, new boolean[0]) + "&page=" + num.toString() + "&append_to_response=external_ids&api_key=" + "b7d93c0311a88e7044011b2a46ec104b");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public TmdbMovieInfoResult m15751(int i) {
        String string = TVApplication.m6285().getString("pref_synopsis_language", "en-US");
        String r1 = HttpHelper.m6343().m6351("https://api.themoviedb.org/3/movie/" + i + "?append_to_response=external_ids,videos,releases&language=" + string + "&api_key=" + "3676dc8d063ee86f625d23d874bf0edb", (Map<String, String>[]) new Map[0]);
        if (!Regex.m17800(r1, "\\\"overview\\\"\\s*:\\s*(null|\\\"\\\")\\s*,", 1).isEmpty() && !string.equals("en-US")) {
            r1 = HttpHelper.m6343().m6351("https://api.themoviedb.org/3/movie/" + i + "?append_to_response=external_ids&api_key=" + "d70b4baecd23e5a7f5820739ec77a471", (Map<String, String>[]) new Map[0]);
        }
        if (r1.isEmpty()) {
            return null;
        }
        try {
            Class<TmdbMovieInfoResult> cls = TmdbMovieInfoResult.class;
            Object fromJson = new Gson().fromJson(r1, TmdbMovieInfoResult.class);
            Object obj = fromJson;
            TmdbMovieInfoResult tmdbMovieInfoResult = (TmdbMovieInfoResult) fromJson;
            TmdbMovieInfoResult tmdbMovieInfoResult2 = tmdbMovieInfoResult;
            return tmdbMovieInfoResult;
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public TmdbTvFindResult m15752(int i) {
        return m15743("tvdb_id", String.valueOf(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m15753(int i, String str) {
        TmdbTvFindResult r2;
        TmdbTvFindResult r22;
        ArrayList<TmdbTvFindResult> arrayList = new ArrayList<>();
        if (i > 0 && (r22 = m15752(i)) != null) {
            arrayList.add(r22);
        }
        if (!(str == null || str.isEmpty() || (r2 = m15758(str)) == null)) {
            arrayList.add(r2);
        }
        for (TmdbTvFindResult tv_results : arrayList) {
            try {
                for (TmdbTvFindResult.TvResultsBean id : tv_results.getTv_results()) {
                    try {
                        int id2 = id.getId();
                        if (id2 > 0) {
                            return id2;
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                continue;
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m15754(MediaInfo mediaInfo, String str) {
        for (TvSeasonInfo next : m15761(mediaInfo, true)) {
            try {
                if (next.getSeasonNum() == Integer.parseInt(str)) {
                    int intValue = next.getSeasonYear().intValue();
                    if (intValue > 0) {
                        return intValue;
                    }
                } else {
                    continue;
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return mediaInfo.getYear();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaApiResult m15755(int i, int i2) {
        return m15756(i, -1, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaApiResult m15756(int i, int i2, int i3) {
        String str;
        String str2;
        if (i >= 14 && i <= 27) {
            str2 = "https://api.themoviedb.org/3/discover/tv?page=" + i3 + "&with_genres=" + TvShowCategoryHelper.m16001().get(i, 10759) + "&sort_by=popularity.desc&append_to_response=external_ids&api_key=" + "64a7798fb102b6498aac589f0bd5b550";
            if (i2 > -1) {
                str2 = str2 + "&first_air_date_year=" + i2 + "&include_null_first_air_dates=false";
            }
        } else if (i >= 10 && i <= 13) {
            str2 = "https://api.themoviedb.org/4/list/" + TvShowCategoryHelper.m16003().get(i, 23751) + "?page=" + i3 + "&sort_by=popularity.desc&append_to_response=external_ids&api_key=" + "fc0a992c4c6431df085a01d5d9d4166c";
        } else if (i == 2) {
            str2 = "https://api.themoviedb.org/3/discover/tv?page=" + i3 + "&air_date.gte=" + DateTimeHelper.m15935(2) + "&air_date.lte=" + DateTimeHelper.m15932() + "&sort_by=popularity.desc&&append_to_response=external_ids&api_key=" + "d09686c35972cb819878bcacd52055bd";
        } else {
            switch (i) {
                case 5:
                    str = "airing_today";
                    break;
                case 6:
                    str = "popular";
                    break;
                case 9:
                    str = "top_rated";
                    break;
                default:
                    str = "popular";
                    break;
            }
            str2 = "https://api.themoviedb.org/3/tv/" + str + "?page=" + i3 + "&append_to_response=external_ids&api_key=" + "0aeecdb61eab3df79527b81d2e9cd9c5";
        }
        return m15741(str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaApiResult m15757(String str, Integer num) {
        return m15741("https://api.themoviedb.org/3/search/tv?query=" + Utils.m6414(str, new boolean[0]) + "&append_to_response=external_ids&page=" + num.toString() + "&api_key=" + "53dfeebd4b7580a8df88281f296f9c03");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TmdbTvFindResult m15758(String str) {
        return m15743("imdb_id", str);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v18, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult m15759(int r15) {
        /*
            r14 = this;
            r8 = 0
            r13 = 4
            r12 = 1
            r13 = 1
            r11 = 0
            android.content.SharedPreferences r7 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x00df }
            java.lang.String r9 = "pref_synopsis_language"
            java.lang.String r10 = "en-US"
            java.lang.String r4 = r7.getString(r9, r10)     // Catch:{ Exception -> 0x00df }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00df }
            r7.<init>()     // Catch:{ Exception -> 0x00df }
            r13 = 4
            java.lang.String r9 = "https://api.themoviedb.org/3/tv/"
            java.lang.String r9 = "https://api.themoviedb.org/3/tv/"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            java.lang.StringBuilder r7 = r7.append(r15)     // Catch:{ Exception -> 0x00df }
            r13 = 4
            java.lang.String r9 = "?append_to_response=external_ids,content_ratings,videos&language="
            java.lang.String r9 = "?append_to_response=external_ids,content_ratings,videos&language="
            r13 = 5
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            r13 = 5
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Exception -> 0x00df }
            r13 = 7
            java.lang.String r9 = "&api_key="
            java.lang.String r9 = "&api_key="
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            r13 = 7
            java.lang.String r9 = "86e2c4dcdfad365de42a9a85d184787f"
            java.lang.String r9 = "86e2c4dcdfad365de42a9a85d184787f"
            r13 = 3
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            r13 = 3
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x00df }
            r13 = 6
            com.typhoon.tv.helper.http.HttpHelper r7 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Exception -> 0x00df }
            r9 = 0
            int r13 = r13 << r9
            java.util.Map[] r9 = new java.util.Map[r9]     // Catch:{ Exception -> 0x00df }
            r13 = 7
            java.lang.String r1 = r7.m6351((java.lang.String) r5, (java.util.Map<java.lang.String, java.lang.String>[]) r9)     // Catch:{ Exception -> 0x00df }
            r13 = 3
            java.lang.String r7 = "\\\"overview\\\"\\s*:\\s*(null|\\\"\\\")\\s*,"
            java.lang.String r7 = "\\\"overview\\\"\\s*:\\s*(null|\\\"\\\")\\s*,"
            r13 = 6
            r9 = 1
            r13 = 1
            java.lang.String r7 = com.typhoon.tv.utils.Regex.m17800(r1, r7, r9)     // Catch:{ Exception -> 0x00df }
            r13 = 2
            boolean r7 = r7.isEmpty()     // Catch:{ Exception -> 0x00df }
            r13 = 2
            if (r7 != 0) goto L_0x00ba
            r13 = 4
            java.lang.String r7 = "en-US"
            boolean r7 = r4.equals(r7)     // Catch:{ Exception -> 0x00df }
            if (r7 != 0) goto L_0x00ba
            r13 = 7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00df }
            r13 = 0
            r7.<init>()     // Catch:{ Exception -> 0x00df }
            java.lang.String r9 = "https://api.themoviedb.org/3/tv/"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            r13 = 2
            java.lang.StringBuilder r7 = r7.append(r15)     // Catch:{ Exception -> 0x00df }
            java.lang.String r9 = "?append_to_response=external_ids&api_key="
            java.lang.String r9 = "?append_to_response=external_ids&api_key="
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            java.lang.String r9 = "3ff69dbc8fd3d763fd27e462781fe01e"
            java.lang.String r9 = "3ff69dbc8fd3d763fd27e462781fe01e"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00df }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x00df }
            com.typhoon.tv.helper.http.HttpHelper r7 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Exception -> 0x00df }
            r13 = 6
            r9 = 0
            java.util.Map[] r9 = new java.util.Map[r9]     // Catch:{ Exception -> 0x00df }
            r13 = 6
            java.lang.String r1 = r7.m6351((java.lang.String) r5, (java.util.Map<java.lang.String, java.lang.String>[]) r9)     // Catch:{ Exception -> 0x00df }
        L_0x00ba:
            boolean r7 = r1.isEmpty()     // Catch:{ Exception -> 0x00df }
            r13 = 2
            if (r7 == 0) goto L_0x00c4
            r6 = r8
        L_0x00c2:
            r13 = 7
            return r6
        L_0x00c4:
            r13 = 3
            r6 = 0
            com.google.gson.Gson r7 = new com.google.gson.Gson     // Catch:{ Exception -> 0x00d6 }
            r7.<init>()     // Catch:{ Exception -> 0x00d6 }
            java.lang.Class<com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult> r9 = com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult.class
            java.lang.Object r7 = r7.fromJson((java.lang.String) r1, r9)     // Catch:{ Exception -> 0x00d6 }
            r0 = r7
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult r0 = (com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult) r0     // Catch:{ Exception -> 0x00d6 }
            r6 = r0
            goto L_0x00c2
        L_0x00d6:
            r3 = move-exception
            r7 = 0
            boolean[] r7 = new boolean[r7]     // Catch:{ Exception -> 0x00df }
            r13 = 3
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r7)     // Catch:{ Exception -> 0x00df }
            goto L_0x00c2
        L_0x00df:
            r2 = move-exception
            r13 = 5
            boolean[] r7 = new boolean[r12]
            r13 = 3
            r7[r11] = r12
            r13 = 1
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r7)
            r6 = r8
            r6 = r8
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.api.TmdbApi.m15759(int):com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00b8 A[SYNTHETIC, Splitter:B:17:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.typhoon.tv.model.media.tv.TvEpisodeInfo> m15760(com.typhoon.tv.model.media.MediaInfo r33, java.lang.Integer r34, boolean... r35) {
        /*
            r32 = this;
            if (r35 == 0) goto L_0x012a
            r0 = r35
            r0 = r35
            int r0 = r0.length
            r28 = r0
            r28 = r0
            if (r28 <= 0) goto L_0x012a
            r28 = 0
            boolean r28 = r35[r28]
            if (r28 == 0) goto L_0x012a
            r18 = 1
        L_0x0015:
            android.content.SharedPreferences r28 = com.typhoon.tv.TVApplication.m6285()
            java.lang.String r29 = "pref_synopsis_language"
            java.lang.String r29 = "pref_synopsis_language"
            java.lang.String r30 = "en-US"
            java.lang.String r30 = "en-US"
            java.lang.String r19 = r28.getString(r29, r30)
            if (r18 == 0) goto L_0x002e
            java.lang.String r19 = "en-US"
        L_0x002e:
            java.util.ArrayList r16 = new java.util.ArrayList
            r16.<init>()
            java.lang.StringBuilder r28 = new java.lang.StringBuilder
            r28.<init>()
            java.lang.String r29 = "https://api.themoviedb.org/3/tv/"
            java.lang.StringBuilder r28 = r28.append(r29)
            int r29 = r33.getTmdbId()
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r29 = "/season/"
            java.lang.String r29 = "/season/"
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r29 = r34.toString()
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r29 = "?append_to_response=external_ids&language="
            java.lang.String r29 = "?append_to_response=external_ids&language="
            java.lang.StringBuilder r28 = r28.append(r29)
            r0 = r28
            r1 = r19
            r1 = r19
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "&api_key="
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r29 = "f6ce01cf80ce85b769812a7030bcfc2e"
            java.lang.String r29 = "f6ce01cf80ce85b769812a7030bcfc2e"
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r22 = r28.toString()
            java.util.LinkedHashMap r14 = new java.util.LinkedHashMap
            r14.<init>()
            java.lang.String r28 = r33.getImdbId()
            if (r28 == 0) goto L_0x00ad
            java.lang.String r28 = r33.getImdbId()
            boolean r28 = r28.isEmpty()
            if (r28 != 0) goto L_0x00ad
            com.typhoon.tv.api.ImdbApi r28 = com.typhoon.tv.api.ImdbApi.m15726()     // Catch:{ Throwable -> 0x012e }
            int r29 = r34.intValue()     // Catch:{ Throwable -> 0x012e }
            r0 = r28
            r1 = r33
            r1 = r33
            r2 = r29
            r2 = r29
            java.util.LinkedHashMap r14 = r0.m15730(r1, r2)     // Catch:{ Throwable -> 0x012e }
        L_0x00ad:
            java.util.LinkedHashMap r26 = new java.util.LinkedHashMap
            r26.<init>()
            int r28 = r33.getTvdbId()
            if (r28 <= 0) goto L_0x00ce
            com.typhoon.tv.api.TvdbApi r28 = com.typhoon.tv.api.TvdbApi.m15813()     // Catch:{ Throwable -> 0x0142 }
            int r29 = r34.intValue()     // Catch:{ Throwable -> 0x0142 }
            r0 = r28
            r0 = r28
            r1 = r33
            r2 = r29
            r2 = r29
            java.util.LinkedHashMap r26 = r0.m15819((com.typhoon.tv.model.media.MediaInfo) r1, (int) r2)     // Catch:{ Throwable -> 0x0142 }
        L_0x00ce:
            boolean r28 = r14.isEmpty()
            if (r28 == 0) goto L_0x00d4
        L_0x00d4:
            boolean r28 = r26.isEmpty()
            if (r28 == 0) goto L_0x00da
        L_0x00da:
            int r28 = r33.getTmdbId()
            int r29 = r34.intValue()
            r0 = r32
            r0 = r32
            r1 = r28
            r1 = r28
            r2 = r29
            r2 = r29
            boolean r12 = r0.m15740((int) r1, (int) r2)
            if (r12 == 0) goto L_0x015a
            boolean r28 = r14.isEmpty()
            if (r28 != 0) goto L_0x015a
            java.util.Collection r28 = r14.values()
            r0 = r16
            r0 = r16
            r1 = r28
            r0.addAll(r1)
        L_0x0107:
            boolean r28 = r16.isEmpty()
            if (r28 != 0) goto L_0x0125
            com.typhoon.tv.api.TmdbApi$3 r28 = new com.typhoon.tv.api.TmdbApi$3
            r0 = r28
            r0 = r28
            r1 = r32
            r1 = r32
            r0.<init>()
            r0 = r16
            r0 = r16
            r1 = r28
            r1 = r28
            java.util.Collections.sort(r0, r1)
        L_0x0125:
            r28 = r16
            r28 = r16
        L_0x0129:
            return r28
        L_0x012a:
            r18 = 0
            goto L_0x0015
        L_0x012e:
            r24 = move-exception
            r28 = 0
            r0 = r28
            boolean[] r0 = new boolean[r0]
            r28 = r0
            r0 = r24
            r1 = r28
            r1 = r28
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
            goto L_0x00ad
        L_0x0142:
            r24 = move-exception
            r28 = 0
            r0 = r28
            boolean[] r0 = new boolean[r0]
            r28 = r0
            r28 = r0
            r0 = r24
            r0 = r24
            r1 = r28
            r1 = r28
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
            goto L_0x00ce
        L_0x015a:
            com.typhoon.tv.helper.http.HttpHelper r28 = com.typhoon.tv.helper.http.HttpHelper.m6343()
            r29 = 0
            r0 = r29
            r0 = r29
            java.util.Map[] r0 = new java.util.Map[r0]
            r29 = r0
            r29 = r0
            r0 = r28
            r0 = r28
            r1 = r22
            r2 = r29
            r2 = r29
            okhttp3.ResponseBody r5 = r0.m6369((java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
            if (r5 != 0) goto L_0x01a0
            boolean r28 = r14.isEmpty()
            if (r28 != 0) goto L_0x018a
            java.util.ArrayList r28 = new java.util.ArrayList
            java.util.Collection r29 = r14.values()
            r28.<init>(r29)
            goto L_0x0129
        L_0x018a:
            boolean r28 = r26.isEmpty()
            if (r28 != 0) goto L_0x019a
            java.util.ArrayList r28 = new java.util.ArrayList
            java.util.Collection r29 = r26.values()
            r28.<init>(r29)
            goto L_0x0129
        L_0x019a:
            java.util.ArrayList r28 = new java.util.ArrayList
            r28.<init>()
            goto L_0x0129
        L_0x01a0:
            r23 = 0
            com.google.gson.Gson r28 = new com.google.gson.Gson     // Catch:{ Exception -> 0x01cf }
            r28.<init>()     // Catch:{ Exception -> 0x01cf }
            java.io.Reader r29 = r5.m7092()     // Catch:{ Exception -> 0x01cf }
            java.lang.Class<com.typhoon.tv.model.media.tv.tmdb.TmdbSeasonInfoResult> r30 = com.typhoon.tv.model.media.tv.tmdb.TmdbSeasonInfoResult.class
            java.lang.Object r28 = r28.fromJson((java.io.Reader) r29, r30)     // Catch:{ Exception -> 0x01cf }
            r0 = r28
            r0 = r28
            com.typhoon.tv.model.media.tv.tmdb.TmdbSeasonInfoResult r0 = (com.typhoon.tv.model.media.tv.tmdb.TmdbSeasonInfoResult) r0     // Catch:{ Exception -> 0x01cf }
            r23 = r0
            r5.close()
        L_0x01bc:
            if (r23 != 0) goto L_0x0200
            boolean r28 = r14.isEmpty()
            if (r28 != 0) goto L_0x01e8
            java.util.ArrayList r28 = new java.util.ArrayList
            java.util.Collection r29 = r14.values()
            r28.<init>(r29)
            goto L_0x0129
        L_0x01cf:
            r7 = move-exception
            r28 = 0
            r0 = r28
            boolean[] r0 = new boolean[r0]     // Catch:{ all -> 0x01e3 }
            r28 = r0
            r28 = r0
            r0 = r28
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)     // Catch:{ all -> 0x01e3 }
            r5.close()
            goto L_0x01bc
        L_0x01e3:
            r28 = move-exception
            r5.close()
            throw r28
        L_0x01e8:
            boolean r28 = r26.isEmpty()
            if (r28 != 0) goto L_0x01f9
            java.util.ArrayList r28 = new java.util.ArrayList
            java.util.Collection r29 = r26.values()
            r28.<init>(r29)
            goto L_0x0129
        L_0x01f9:
            java.util.ArrayList r28 = new java.util.ArrayList
            r28.<init>()
            goto L_0x0129
        L_0x0200:
            java.util.List r11 = r23.getEpisodes()
            com.typhoon.tv.TVDatabase r28 = com.typhoon.tv.TVApplication.m6287()
            int r29 = r33.getTmdbId()
            java.lang.Integer r29 = java.lang.Integer.valueOf(r29)
            com.typhoon.tv.model.CheckEpisodeResult r20 = r28.m6306((java.lang.Integer) r29)
            r17 = 0
            java.util.Iterator r29 = r11.iterator()
        L_0x021a:
            boolean r28 = r29.hasNext()
            if (r28 == 0) goto L_0x03f9
            java.lang.Object r8 = r29.next()
            com.typhoon.tv.model.media.tv.tmdb.TmdbSeasonInfoResult$EpisodesBean r8 = (com.typhoon.tv.model.media.tv.tmdb.TmdbSeasonInfoResult.EpisodesBean) r8
            int r10 = r8.getEpisode_number()     // Catch:{ Exception -> 0x03e2 }
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r0 = r28
            boolean r28 = r14.containsKey(r0)     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x02d5
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r0 = r28
            java.lang.Object r28 = r14.get(r0)     // Catch:{ Exception -> 0x03e2 }
            com.typhoon.tv.model.media.tv.TvEpisodeInfo r28 = (com.typhoon.tv.model.media.tv.TvEpisodeInfo) r28     // Catch:{ Exception -> 0x03e2 }
            r13 = r28
        L_0x0248:
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r26
            r1 = r28
            boolean r28 = r0.containsKey(r1)     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x02d8
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r26
            r1 = r28
            java.lang.Object r28 = r0.get(r1)     // Catch:{ Exception -> 0x03e2 }
            com.typhoon.tv.model.media.tv.TvEpisodeInfo r28 = (com.typhoon.tv.model.media.tv.TvEpisodeInfo) r28     // Catch:{ Exception -> 0x03e2 }
            r25 = r28
            r25 = r28
        L_0x0268:
            if (r13 == 0) goto L_0x026a
        L_0x026a:
            if (r25 == 0) goto L_0x026c
        L_0x026c:
            java.lang.String r4 = ""
            java.lang.String r4 = ""
            java.lang.String r28 = r8.getAir_date()     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x0286
            java.lang.String r28 = r8.getAir_date()     // Catch:{ Exception -> 0x03e2 }
            boolean r28 = r28.isEmpty()     // Catch:{ Exception -> 0x03e2 }
            if (r28 != 0) goto L_0x0286
            java.lang.String r4 = r8.getAir_date()     // Catch:{ Exception -> 0x03e2 }
        L_0x0286:
            java.lang.String r28 = r8.getOverview()     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x02db
            java.lang.String r28 = r8.getOverview()     // Catch:{ Exception -> 0x03e2 }
            boolean r28 = r28.isEmpty()     // Catch:{ Exception -> 0x03e2 }
            if (r28 != 0) goto L_0x02db
            java.lang.String r21 = r8.getOverview()     // Catch:{ Exception -> 0x03e2 }
        L_0x029a:
            java.lang.String r28 = r21.trim()     // Catch:{ Exception -> 0x03e2 }
            boolean r28 = r28.isEmpty()     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x02e2
            if (r18 != 0) goto L_0x02e2
            java.lang.String r28 = "en-US"
            r0 = r19
            r1 = r28
            boolean r28 = r0.equals(r1)     // Catch:{ Exception -> 0x03e2 }
            if (r28 != 0) goto L_0x02e2
            r28 = 1
            r0 = r28
            boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x03e2 }
            r28 = r0
            r28 = r0
            r30 = 0
            r31 = 1
            r28[r30] = r31     // Catch:{ Exception -> 0x03e2 }
            r0 = r32
            r1 = r33
            r1 = r33
            r2 = r34
            r3 = r28
            r3 = r28
            java.util.ArrayList r28 = r0.m15760((com.typhoon.tv.model.media.MediaInfo) r1, (java.lang.Integer) r2, (boolean[]) r3)     // Catch:{ Exception -> 0x03e2 }
            goto L_0x0129
        L_0x02d5:
            r13 = 0
            goto L_0x0248
        L_0x02d8:
            r25 = 0
            goto L_0x0268
        L_0x02db:
            java.lang.String r21 = ""
            java.lang.String r21 = ""
            goto L_0x029a
        L_0x02e2:
            android.content.SharedPreferences r28 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x03e2 }
            java.lang.String r30 = "pref_hide_unaired_episode"
            java.lang.String r30 = "pref_hide_unaired_episode"
            r31 = 1
            r0 = r28
            r0 = r28
            r1 = r30
            r2 = r31
            r2 = r31
            boolean r28 = r0.getBoolean(r1, r2)     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x032e
            boolean r28 = r4.isEmpty()     // Catch:{ Exception -> 0x03e2 }
            if (r28 != 0) goto L_0x021a
            org.joda.time.DateTime r28 = com.typhoon.tv.helper.DateTimeHelper.m15937((java.lang.String) r4)     // Catch:{ Exception -> 0x03e2 }
            boolean r28 = com.typhoon.tv.helper.DateTimeHelper.m15933(r28)     // Catch:{ Exception -> 0x03e2 }
            if (r28 != 0) goto L_0x032e
            if (r17 != 0) goto L_0x021a
            if (r20 == 0) goto L_0x021a
            int r28 = r34.intValue()     // Catch:{ Exception -> 0x03e2 }
            int r30 = r20.getLastSeason()     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r1 = r30
            r1 = r30
            if (r0 != r1) goto L_0x021a
            int r28 = r20.getLastEpisode()     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r0 = r28
            if (r10 != r0) goto L_0x021a
            r17 = 1
        L_0x032e:
            java.lang.String r6 = ""
            java.lang.String r6 = ""
            java.lang.String r28 = r8.getStill_path()     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x03f2
            java.lang.String r28 = r8.getStill_path()     // Catch:{ Exception -> 0x03e2 }
            boolean r28 = r28.isEmpty()     // Catch:{ Exception -> 0x03e2 }
            if (r28 != 0) goto L_0x03f2
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03e2 }
            r28.<init>()     // Catch:{ Exception -> 0x03e2 }
            java.lang.String r30 = "https://image.tmdb.org/t/p/w780"
            java.lang.String r30 = "https://image.tmdb.org/t/p/w780"
            r0 = r28
            r1 = r30
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ Exception -> 0x03e2 }
            java.lang.String r30 = r8.getStill_path()     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r1 = r30
            r1 = r30
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ Exception -> 0x03e2 }
            java.lang.String r30 = "?api_key="
            r0 = r28
            r1 = r30
            r1 = r30
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ Exception -> 0x03e2 }
            java.lang.String r30 = "b710e41889faec41995f5bce73ce400c"
            java.lang.String r30 = "b710e41889faec41995f5bce73ce400c"
            r0 = r28
            r0 = r28
            r1 = r30
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ Exception -> 0x03e2 }
            java.lang.String r6 = r28.toString()     // Catch:{ Exception -> 0x03e2 }
        L_0x0386:
            java.lang.String r9 = r8.getName()     // Catch:{ Exception -> 0x03e2 }
            com.typhoon.tv.model.media.tv.TvEpisodeInfo r15 = new com.typhoon.tv.model.media.tv.TvEpisodeInfo     // Catch:{ Exception -> 0x03e2 }
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r34
            r1 = r28
            r1 = r28
            r15.<init>(r0, r1, r9, r6)     // Catch:{ Exception -> 0x03e2 }
            r15.setAirDate(r4)     // Catch:{ Exception -> 0x03e2 }
            r0 = r21
            r0 = r21
            r15.setOverview(r0)     // Catch:{ Exception -> 0x03e2 }
            r0 = r16
            r0.add(r15)     // Catch:{ Exception -> 0x03e2 }
            if (r13 == 0) goto L_0x03c1
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r0 = r28
            boolean r28 = r14.containsKey(r0)     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x03c1
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r28
            r14.remove(r0)     // Catch:{ Exception -> 0x03e2 }
        L_0x03c1:
            if (r25 == 0) goto L_0x021a
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r26
            r0 = r26
            r1 = r28
            boolean r28 = r0.containsKey(r1)     // Catch:{ Exception -> 0x03e2 }
            if (r28 == 0) goto L_0x021a
            java.lang.Integer r28 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x03e2 }
            r0 = r26
            r0 = r26
            r1 = r28
            r0.remove(r1)     // Catch:{ Exception -> 0x03e2 }
            goto L_0x021a
        L_0x03e2:
            r7 = move-exception
            r28 = 0
            r0 = r28
            boolean[] r0 = new boolean[r0]
            r28 = r0
            r0 = r28
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x021a
        L_0x03f2:
            if (r13 == 0) goto L_0x0386
            java.lang.String r6 = r13.getBannerUrl()     // Catch:{ Exception -> 0x03e2 }
            goto L_0x0386
        L_0x03f9:
            boolean r28 = r14.isEmpty()
            if (r28 != 0) goto L_0x042c
            java.util.Collection r27 = r14.values()
            java.util.Iterator r28 = r27.iterator()
        L_0x0407:
            boolean r29 = r28.hasNext()
            if (r29 == 0) goto L_0x042c
            java.lang.Object r15 = r28.next()
            com.typhoon.tv.model.media.tv.TvEpisodeInfo r15 = (com.typhoon.tv.model.media.tv.TvEpisodeInfo) r15
            r0 = r16
            r0 = r16
            r0.add(r15)
            int r29 = r15.getEpisode()
            java.lang.Integer r29 = java.lang.Integer.valueOf(r29)
            r0 = r26
            r0 = r26
            r1 = r29
            r0.remove(r1)
            goto L_0x0407
        L_0x042c:
            boolean r28 = r26.isEmpty()
            if (r28 != 0) goto L_0x0107
            java.util.ArrayList r28 = new java.util.ArrayList
            java.util.Collection r29 = r26.values()
            r28.<init>(r29)
            r0 = r16
            r0 = r16
            r1 = r28
            r1 = r28
            r0.addAll(r1)
            goto L_0x0107
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.api.TmdbApi.m15760(com.typhoon.tv.model.media.MediaInfo, java.lang.Integer, boolean[]):java.util.ArrayList");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0025 A[SYNTHETIC, Splitter:B:12:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007a A[SYNTHETIC, Splitter:B:31:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0142  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.typhoon.tv.model.media.tv.TvSeasonInfo> m15761(com.typhoon.tv.model.media.MediaInfo r40, boolean... r41) {
        /*
            r39 = this;
            if (r41 == 0) goto L_0x00e1
            r0 = r41
            r0 = r41
            int r0 = r0.length
            r35 = r0
            if (r35 <= 0) goto L_0x00e1
            r35 = 0
            boolean r35 = r41[r35]
            if (r35 == 0) goto L_0x00e1
            r4 = 1
        L_0x0012:
            java.lang.String r35 = r40.getImdbId()
            if (r35 == 0) goto L_0x00e4
            java.lang.String r35 = r40.getImdbId()
            boolean r35 = r35.isEmpty()
            if (r35 != 0) goto L_0x00e4
            r11 = 1
        L_0x0023:
            if (r4 != 0) goto L_0x0078
            int r35 = r40.getTvdbId()     // Catch:{ Exception -> 0x00e7 }
            if (r35 <= 0) goto L_0x002d
            if (r11 != 0) goto L_0x0078
        L_0x002d:
            com.typhoon.tv.api.TmdbApi r35 = m15742()     // Catch:{ Exception -> 0x00e7 }
            int r36 = r40.getTmdbId()     // Catch:{ Exception -> 0x00e7 }
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult r30 = r35.m15759((int) r36)     // Catch:{ Exception -> 0x00e7 }
            if (r30 == 0) goto L_0x0078
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult$ExternalIdsBean r35 = r30.getExternal_ids()     // Catch:{ Exception -> 0x00e7 }
            if (r35 == 0) goto L_0x0078
            int r35 = r40.getTvdbId()     // Catch:{ Exception -> 0x00e7 }
            if (r35 > 0) goto L_0x005a
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult$ExternalIdsBean r35 = r30.getExternal_ids()     // Catch:{ Exception -> 0x00e7 }
            int r35 = r35.getTvdb_id()     // Catch:{ Exception -> 0x00e7 }
            r0 = r40
            r0 = r40
            r1 = r35
            r1 = r35
            r0.setTvdbId(r1)     // Catch:{ Exception -> 0x00e7 }
        L_0x005a:
            if (r11 != 0) goto L_0x0078
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult$ExternalIdsBean r35 = r30.getExternal_ids()     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r20 = r35.getImdb_id()     // Catch:{ Exception -> 0x00e7 }
            if (r20 == 0) goto L_0x0078
            boolean r35 = r20.isEmpty()     // Catch:{ Exception -> 0x00e7 }
            if (r35 != 0) goto L_0x0078
            r0 = r40
            r0 = r40
            r1 = r20
            r1 = r20
            r0.setImdbId(r1)     // Catch:{ Exception -> 0x00e7 }
            r11 = 1
        L_0x0078:
            if (r4 != 0) goto L_0x0099
            int r35 = r40.getTvdbId()     // Catch:{ Exception -> 0x00fd }
            if (r35 > 0) goto L_0x0099
            if (r11 == 0) goto L_0x0099
            com.typhoon.tv.api.TvdbApi r35 = com.typhoon.tv.api.TvdbApi.m15813()     // Catch:{ Exception -> 0x00fd }
            java.lang.String r36 = r40.getImdbId()     // Catch:{ Exception -> 0x00fd }
            int r31 = r35.m15817((java.lang.String) r36)     // Catch:{ Exception -> 0x00fd }
            if (r31 <= 0) goto L_0x0099
            r0 = r40
            r0 = r40
            r1 = r31
            r0.setTvdbId(r1)     // Catch:{ Exception -> 0x00fd }
        L_0x0099:
            java.util.ArrayList r24 = new java.util.ArrayList
            r24.<init>()
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            java.util.ArrayList r33 = new java.util.ArrayList
            r33.<init>()
            if (r4 != 0) goto L_0x00b8
            if (r11 == 0) goto L_0x00b8
            com.typhoon.tv.api.ImdbApi r35 = com.typhoon.tv.api.ImdbApi.m15726()     // Catch:{ Throwable -> 0x0114 }
            java.lang.String r36 = r40.getImdbId()     // Catch:{ Throwable -> 0x0114 }
            java.util.List r14 = r35.m15731(r36)     // Catch:{ Throwable -> 0x0114 }
        L_0x00b8:
            if (r4 != 0) goto L_0x00d0
            int r35 = r40.getTvdbId()
            if (r35 <= 0) goto L_0x00d0
            com.typhoon.tv.api.TvdbApi r35 = com.typhoon.tv.api.TvdbApi.m15813()     // Catch:{ Throwable -> 0x012d }
            int r36 = r40.getTvdbId()     // Catch:{ Throwable -> 0x012d }
            java.lang.String r37 = r40.getImdbId()     // Catch:{ Throwable -> 0x012d }
            java.util.List r33 = r35.m15820((int) r36, (java.lang.String) r37)     // Catch:{ Throwable -> 0x012d }
        L_0x00d0:
            int r35 = r40.getTmdbId()
            r0 = r39
            r1 = r35
            r1 = r35
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult r21 = r0.m15759((int) r1)
            if (r21 != 0) goto L_0x0142
        L_0x00e0:
            return r24
        L_0x00e1:
            r4 = 0
            goto L_0x0012
        L_0x00e4:
            r11 = 0
            goto L_0x0023
        L_0x00e7:
            r7 = move-exception
            r35 = 1
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r36 = 0
            r37 = 1
            r35[r36] = r37
            r0 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x0078
        L_0x00fd:
            r7 = move-exception
            r35 = 1
            r0 = r35
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r36 = 0
            r37 = 1
            r35[r36] = r37
            r0 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x0099
        L_0x0114:
            r29 = move-exception
            r35 = 0
            r0 = r35
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r35 = r0
            r0 = r29
            r0 = r29
            r1 = r35
            r1 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
            goto L_0x00b8
        L_0x012d:
            r29 = move-exception
            r35 = 0
            r0 = r35
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r0 = r29
            r1 = r35
            r1 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
            goto L_0x00d0
        L_0x0142:
            java.util.List r28 = r21.getSeasons()
            r17 = 0
            com.typhoon.tv.TVDatabase r35 = com.typhoon.tv.TVApplication.m6287()     // Catch:{ Exception -> 0x02c3 }
            int r36 = r40.getTmdbId()     // Catch:{ Exception -> 0x02c3 }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r36)     // Catch:{ Exception -> 0x02c3 }
            com.typhoon.tv.model.CheckEpisodeResult r17 = r35.m6306((java.lang.Integer) r36)     // Catch:{ Exception -> 0x02c3 }
        L_0x0158:
            r16 = 0
            if (r28 == 0) goto L_0x02e2
            java.lang.String r35 = r40.getName()
            java.lang.String r36 = "Will & Grace"
            boolean r35 = r35.equalsIgnoreCase(r36)
            if (r35 == 0) goto L_0x0193
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult$SeasonsBean r34 = new com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult$SeasonsBean
            r34.<init>()
            java.lang.String r35 = ""
            r34.setAir_date(r35)
            r35 = 1
            r34.setEpisode_count(r35)
            r36 = 1
            r0 = r34
            r1 = r36
            r0.setId(r1)
            r35 = 0
            r34.setPoster_path(r35)
            r35 = 9
            r34.setSeason_number(r35)
            r0 = r28
            r1 = r34
            r0.add(r1)
        L_0x0193:
            java.util.Iterator r35 = r28.iterator()
        L_0x0197:
            boolean r36 = r35.hasNext()
            if (r36 == 0) goto L_0x02e2
            java.lang.Object r26 = r35.next()
            com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult$SeasonsBean r26 = (com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult.SeasonsBean) r26
            int r36 = r26.getEpisode_count()     // Catch:{ Exception -> 0x02b3 }
            if (r36 <= 0) goto L_0x0197
            int r25 = r26.getSeason_number()     // Catch:{ Exception -> 0x02b3 }
            if (r25 != 0) goto L_0x01c1
            android.content.SharedPreferences r36 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r37 = "pref_show_season_special"
            java.lang.String r37 = "pref_show_season_special"
            r38 = 0
            boolean r36 = r36.getBoolean(r37, r38)     // Catch:{ Exception -> 0x02b3 }
            if (r36 == 0) goto L_0x0197
        L_0x01c1:
            r27 = 0
            java.lang.String r5 = ""
            java.lang.String r36 = r26.getAir_date()     // Catch:{ Exception -> 0x02b3 }
            if (r36 == 0) goto L_0x01d0
            java.lang.String r5 = r26.getAir_date()     // Catch:{ Exception -> 0x02b3 }
        L_0x01d0:
            if (r5 == 0) goto L_0x022a
            boolean r36 = r5.isEmpty()     // Catch:{ Exception -> 0x02b3 }
            if (r36 != 0) goto L_0x022a
            org.joda.time.DateTime r6 = com.typhoon.tv.helper.DateTimeHelper.m15937((java.lang.String) r5)     // Catch:{ Exception -> 0x02b3 }
            if (r6 == 0) goto L_0x022a
            int r36 = r5.length()     // Catch:{ Exception -> 0x02b3 }
            r37 = 4
            r0 = r36
            r1 = r37
            r1 = r37
            if (r0 < r1) goto L_0x02d7
            r36 = 0
            r37 = 4
            r0 = r36
            r0 = r36
            r1 = r37
            java.lang.String r36 = r5.substring(r0, r1)     // Catch:{ Exception -> 0x02b3 }
            int r27 = java.lang.Integer.parseInt(r36)     // Catch:{ Exception -> 0x02b3 }
        L_0x01fe:
            android.content.SharedPreferences r36 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r37 = "pref_hide_unaired_season"
            java.lang.String r37 = "pref_hide_unaired_season"
            r38 = 1
            boolean r36 = r36.getBoolean(r37, r38)     // Catch:{ Exception -> 0x02b3 }
            if (r36 == 0) goto L_0x022a
            boolean r36 = com.typhoon.tv.helper.DateTimeHelper.m15933(r6)     // Catch:{ Exception -> 0x02b3 }
            if (r36 != 0) goto L_0x022a
            if (r16 != 0) goto L_0x0197
            if (r17 == 0) goto L_0x0197
            int r36 = r17.getLastSeason()     // Catch:{ Exception -> 0x02b3 }
            r0 = r25
            r0 = r25
            r1 = r36
            r1 = r36
            if (r0 != r1) goto L_0x0197
            r16 = 1
        L_0x022a:
            java.lang.String r36 = r26.getPoster_path()     // Catch:{ Exception -> 0x02b3 }
            if (r36 == 0) goto L_0x02dd
            java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b3 }
            r36.<init>()     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r37 = "https://image.tmdb.org/t/p/w500"
            java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r37 = r26.getPoster_path()     // Catch:{ Exception -> 0x02b3 }
            java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r37 = "?api_key="
            java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r37 = "d1c25758a8824cb91191b60265a74c3b"
            java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ Exception -> 0x02b3 }
            java.lang.String r22 = r36.toString()     // Catch:{ Exception -> 0x02b3 }
        L_0x0256:
            com.typhoon.tv.model.media.tv.TvSeasonInfo r15 = new com.typhoon.tv.model.media.tv.TvSeasonInfo     // Catch:{ Exception -> 0x02b3 }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r25)     // Catch:{ Exception -> 0x02b3 }
            r0 = r36
            r0 = r36
            r15.<init>((java.lang.Integer) r0)     // Catch:{ Exception -> 0x02b3 }
            r15.setAirDate(r5)     // Catch:{ Exception -> 0x02b3 }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r27)     // Catch:{ Exception -> 0x02b3 }
            r0 = r36
            r15.setSeasonYear(r0)     // Catch:{ Exception -> 0x02b3 }
            r0 = r22
            r0 = r22
            r15.setBannerUrl(r0)     // Catch:{ Exception -> 0x02b3 }
            r0 = r24
            r0 = r24
            r0.add(r15)     // Catch:{ Exception -> 0x02b3 }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r25)     // Catch:{ Exception -> 0x02b3 }
            r0 = r36
            boolean r36 = r14.contains(r0)     // Catch:{ Exception -> 0x02b3 }
            if (r36 == 0) goto L_0x0292
            java.lang.Integer r36 = java.lang.Integer.valueOf(r25)     // Catch:{ Exception -> 0x02b3 }
            r0 = r36
            r14.remove(r0)     // Catch:{ Exception -> 0x02b3 }
        L_0x0292:
            java.lang.Integer r36 = java.lang.Integer.valueOf(r25)     // Catch:{ Exception -> 0x02b3 }
            r0 = r33
            r0 = r33
            r1 = r36
            boolean r36 = r0.contains(r1)     // Catch:{ Exception -> 0x02b3 }
            if (r36 == 0) goto L_0x0197
            java.lang.Integer r36 = java.lang.Integer.valueOf(r25)     // Catch:{ Exception -> 0x02b3 }
            r0 = r33
            r0 = r33
            r1 = r36
            r1 = r36
            r0.remove(r1)     // Catch:{ Exception -> 0x02b3 }
            goto L_0x0197
        L_0x02b3:
            r7 = move-exception
            r36 = 0
            r0 = r36
            boolean[] r0 = new boolean[r0]
            r36 = r0
            r0 = r36
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x0197
        L_0x02c3:
            r7 = move-exception
            r35 = 0
            r0 = r35
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r35 = r0
            r0 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x0158
        L_0x02d7:
            int r27 = r6.getYear()     // Catch:{ Exception -> 0x02b3 }
            goto L_0x01fe
        L_0x02dd:
            java.lang.String r22 = ""
            goto L_0x0256
        L_0x02e2:
            com.typhoon.tv.api.TmdbApi$2 r23 = new com.typhoon.tv.api.TmdbApi$2
            r0 = r23
            r0 = r23
            r1 = r39
            r0.<init>()
            r19 = -1
            boolean r35 = r24.isEmpty()
            if (r35 != 0) goto L_0x0305
            r0 = r24
            r0 = r24
            r1 = r23
            java.lang.Object r18 = java.util.Collections.max(r0, r1)
            com.typhoon.tv.model.media.tv.TvSeasonInfo r18 = (com.typhoon.tv.model.media.tv.TvSeasonInfo) r18
            int r19 = r18.getSeasonNum()
        L_0x0305:
            int r35 = r40.getTmdbId()
            r36 = 4454(0x1166, float:6.241E-42)
            r0 = r35
            r1 = r36
            r1 = r36
            if (r0 != r1) goto L_0x0315
            r19 = -1
        L_0x0315:
            r12 = 0
        L_0x0316:
            int r35 = r14.size()
            r0 = r35
            r0 = r35
            if (r12 >= r0) goto L_0x0463
            java.lang.Object r35 = r14.get(r12)     // Catch:{ Exception -> 0x03ff }
            java.lang.Integer r35 = (java.lang.Integer) r35     // Catch:{ Exception -> 0x03ff }
            int r13 = r35.intValue()     // Catch:{ Exception -> 0x03ff }
            com.typhoon.tv.helper.TvExtraSeasonCacheHelper r35 = com.typhoon.tv.helper.TvExtraSeasonCacheHelper.m15973()     // Catch:{ Exception -> 0x03ff }
            int r36 = r40.getTmdbId()     // Catch:{ Exception -> 0x03ff }
            java.lang.String r37 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            boolean r35 = r35.m15975(r36, r37)     // Catch:{ Exception -> 0x03ff }
            if (r35 == 0) goto L_0x0370
            com.typhoon.tv.model.media.tv.TvSeasonInfo r35 = new com.typhoon.tv.model.media.tv.TvSeasonInfo     // Catch:{ Exception -> 0x03ff }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r35.<init>((java.lang.Integer) r36)     // Catch:{ Exception -> 0x03ff }
            r0 = r24
            r1 = r35
            r1 = r35
            r0.add(r1)     // Catch:{ Exception -> 0x03ff }
            java.lang.Integer r35 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r0 = r33
            r0 = r33
            r1 = r35
            r1 = r35
            boolean r35 = r0.contains(r1)     // Catch:{ Exception -> 0x03ff }
            if (r35 == 0) goto L_0x036d
            java.lang.Integer r35 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r0 = r33
            r1 = r35
            r1 = r35
            r0.remove(r1)     // Catch:{ Exception -> 0x03ff }
        L_0x036d:
            int r12 = r12 + 1
            goto L_0x0316
        L_0x0370:
            if (r11 == 0) goto L_0x0411
            com.typhoon.tv.api.ImdbApi r35 = com.typhoon.tv.api.ImdbApi.m15726()     // Catch:{ Exception -> 0x03ff }
            r0 = r35
            r0 = r35
            r1 = r40
            r1 = r40
            java.util.LinkedHashMap r8 = r0.m15730(r1, r13)     // Catch:{ Exception -> 0x03ff }
            if (r8 == 0) goto L_0x036d
            boolean r35 = r8.isEmpty()     // Catch:{ Exception -> 0x03ff }
            if (r35 != 0) goto L_0x036d
            android.content.SharedPreferences r35 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x03ff }
            java.lang.String r36 = "pref_hide_unaired_season"
            java.lang.String r36 = "pref_hide_unaired_season"
            r37 = 1
            boolean r35 = r35.getBoolean(r36, r37)     // Catch:{ Exception -> 0x03ff }
            if (r35 == 0) goto L_0x0411
            int r35 = r8.size()     // Catch:{ Exception -> 0x03ff }
            r36 = 3
            r0 = r35
            r1 = r36
            r1 = r36
            if (r0 < r1) goto L_0x036d
            java.util.Collection r35 = r8.values()     // Catch:{ Exception -> 0x03ff }
            java.util.Collection r36 = r8.values()     // Catch:{ Exception -> 0x03ff }
            int r36 = r36.size()     // Catch:{ Exception -> 0x03ff }
            r0 = r36
            com.typhoon.tv.model.media.tv.TvEpisodeInfo[] r0 = new com.typhoon.tv.model.media.tv.TvEpisodeInfo[r0]     // Catch:{ Exception -> 0x03ff }
            r36 = r0
            r36 = r0
            java.lang.Object[] r35 = r35.toArray(r36)     // Catch:{ Exception -> 0x03ff }
            com.typhoon.tv.model.media.tv.TvEpisodeInfo[] r35 = (com.typhoon.tv.model.media.tv.TvEpisodeInfo[]) r35     // Catch:{ Exception -> 0x03ff }
            r36 = 0
            r10 = r35[r36]     // Catch:{ Exception -> 0x03ff }
            if (r10 == 0) goto L_0x0411
            java.lang.String r9 = r10.getAirDate()     // Catch:{ Exception -> 0x03ff }
            if (r9 == 0) goto L_0x0411
            boolean r35 = r9.isEmpty()     // Catch:{ Exception -> 0x03ff }
            if (r35 != 0) goto L_0x0411
            org.joda.time.DateTime r35 = com.typhoon.tv.helper.DateTimeHelper.m15937((java.lang.String) r9)     // Catch:{ Exception -> 0x03ff }
            boolean r35 = com.typhoon.tv.helper.DateTimeHelper.m15933(r35)     // Catch:{ Exception -> 0x03ff }
            if (r35 != 0) goto L_0x0411
            java.lang.Integer r35 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r0 = r33
            r1 = r35
            r1 = r35
            boolean r35 = r0.contains(r1)     // Catch:{ Exception -> 0x03ff }
            if (r35 == 0) goto L_0x036d
            java.lang.Integer r35 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r0 = r33
            r0 = r33
            r1 = r35
            r0.remove(r1)     // Catch:{ Exception -> 0x03ff }
            goto L_0x036d
        L_0x03ff:
            r7 = move-exception
            r35 = 0
            r0 = r35
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r0 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x036d
        L_0x0411:
            r0 = r19
            if (r13 > r0) goto L_0x041f
            r35 = -1
            r0 = r19
            r0 = r19
            r1 = r35
            if (r0 != r1) goto L_0x036d
        L_0x041f:
            com.typhoon.tv.model.media.tv.TvSeasonInfo r35 = new com.typhoon.tv.model.media.tv.TvSeasonInfo     // Catch:{ Exception -> 0x03ff }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r35.<init>((java.lang.Integer) r36)     // Catch:{ Exception -> 0x03ff }
            r0 = r24
            r0 = r24
            r1 = r35
            r1 = r35
            r0.add(r1)     // Catch:{ Exception -> 0x03ff }
            java.lang.Integer r35 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r0 = r33
            r0 = r33
            r1 = r35
            r1 = r35
            boolean r35 = r0.contains(r1)     // Catch:{ Exception -> 0x03ff }
            if (r35 == 0) goto L_0x0452
            java.lang.Integer r35 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r0 = r33
            r1 = r35
            r1 = r35
            r0.remove(r1)     // Catch:{ Exception -> 0x03ff }
        L_0x0452:
            com.typhoon.tv.helper.TvExtraSeasonCacheHelper r35 = com.typhoon.tv.helper.TvExtraSeasonCacheHelper.m15973()     // Catch:{ Exception -> 0x03ff }
            int r36 = r40.getTmdbId()     // Catch:{ Exception -> 0x03ff }
            java.lang.String r37 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x03ff }
            r35.m15974(r36, r37)     // Catch:{ Exception -> 0x03ff }
            goto L_0x036d
        L_0x0463:
            r12 = 0
        L_0x0464:
            int r35 = r33.size()
            r0 = r35
            if (r12 >= r0) goto L_0x0545
            r0 = r33
            r0 = r33
            java.lang.Object r35 = r0.get(r12)     // Catch:{ Exception -> 0x0533 }
            java.lang.Integer r35 = (java.lang.Integer) r35     // Catch:{ Exception -> 0x0533 }
            int r32 = r35.intValue()     // Catch:{ Exception -> 0x0533 }
            com.typhoon.tv.helper.TvExtraSeasonCacheHelper r35 = com.typhoon.tv.helper.TvExtraSeasonCacheHelper.m15973()     // Catch:{ Exception -> 0x0533 }
            int r36 = r40.getTmdbId()     // Catch:{ Exception -> 0x0533 }
            java.lang.String r37 = java.lang.String.valueOf(r32)     // Catch:{ Exception -> 0x0533 }
            boolean r35 = r35.m15975(r36, r37)     // Catch:{ Exception -> 0x0533 }
            if (r35 == 0) goto L_0x04a3
            com.typhoon.tv.model.media.tv.TvSeasonInfo r35 = new com.typhoon.tv.model.media.tv.TvSeasonInfo     // Catch:{ Exception -> 0x0533 }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r32)     // Catch:{ Exception -> 0x0533 }
            r35.<init>((java.lang.Integer) r36)     // Catch:{ Exception -> 0x0533 }
            r0 = r24
            r0 = r24
            r1 = r35
            r1 = r35
            r0.add(r1)     // Catch:{ Exception -> 0x0533 }
        L_0x04a0:
            int r12 = r12 + 1
            goto L_0x0464
        L_0x04a3:
            com.typhoon.tv.api.TvdbApi r35 = com.typhoon.tv.api.TvdbApi.m15813()     // Catch:{ Exception -> 0x0533 }
            r0 = r35
            r0 = r35
            r1 = r40
            r1 = r40
            r2 = r32
            r2 = r32
            java.util.LinkedHashMap r8 = r0.m15819((com.typhoon.tv.model.media.MediaInfo) r1, (int) r2)     // Catch:{ Exception -> 0x0533 }
            if (r8 == 0) goto L_0x04a0
            boolean r35 = r8.isEmpty()     // Catch:{ Exception -> 0x0533 }
            if (r35 != 0) goto L_0x04a0
            android.content.SharedPreferences r35 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x0533 }
            java.lang.String r36 = "pref_hide_unaired_season"
            r37 = 1
            boolean r35 = r35.getBoolean(r36, r37)     // Catch:{ Exception -> 0x0533 }
            if (r35 == 0) goto L_0x0512
            int r35 = r8.size()     // Catch:{ Exception -> 0x0533 }
            r36 = 3
            r0 = r35
            r1 = r36
            r1 = r36
            if (r0 < r1) goto L_0x04a0
            java.util.Collection r35 = r8.values()     // Catch:{ Exception -> 0x0533 }
            java.util.Collection r36 = r8.values()     // Catch:{ Exception -> 0x0533 }
            int r36 = r36.size()     // Catch:{ Exception -> 0x0533 }
            r0 = r36
            r0 = r36
            com.typhoon.tv.model.media.tv.TvEpisodeInfo[] r0 = new com.typhoon.tv.model.media.tv.TvEpisodeInfo[r0]     // Catch:{ Exception -> 0x0533 }
            r36 = r0
            java.lang.Object[] r35 = r35.toArray(r36)     // Catch:{ Exception -> 0x0533 }
            com.typhoon.tv.model.media.tv.TvEpisodeInfo[] r35 = (com.typhoon.tv.model.media.tv.TvEpisodeInfo[]) r35     // Catch:{ Exception -> 0x0533 }
            r36 = 0
            r10 = r35[r36]     // Catch:{ Exception -> 0x0533 }
            if (r10 == 0) goto L_0x0512
            java.lang.String r9 = r10.getAirDate()     // Catch:{ Exception -> 0x0533 }
            if (r9 == 0) goto L_0x0512
            boolean r35 = r9.isEmpty()     // Catch:{ Exception -> 0x0533 }
            if (r35 != 0) goto L_0x0512
            org.joda.time.DateTime r35 = com.typhoon.tv.helper.DateTimeHelper.m15937((java.lang.String) r9)     // Catch:{ Exception -> 0x0533 }
            boolean r35 = com.typhoon.tv.helper.DateTimeHelper.m15933(r35)     // Catch:{ Exception -> 0x0533 }
            if (r35 == 0) goto L_0x04a0
        L_0x0512:
            com.typhoon.tv.model.media.tv.TvSeasonInfo r35 = new com.typhoon.tv.model.media.tv.TvSeasonInfo     // Catch:{ Exception -> 0x0533 }
            java.lang.Integer r36 = java.lang.Integer.valueOf(r32)     // Catch:{ Exception -> 0x0533 }
            r35.<init>((java.lang.Integer) r36)     // Catch:{ Exception -> 0x0533 }
            r0 = r24
            r1 = r35
            r0.add(r1)     // Catch:{ Exception -> 0x0533 }
            com.typhoon.tv.helper.TvExtraSeasonCacheHelper r35 = com.typhoon.tv.helper.TvExtraSeasonCacheHelper.m15973()     // Catch:{ Exception -> 0x0533 }
            int r36 = r40.getTmdbId()     // Catch:{ Exception -> 0x0533 }
            java.lang.String r37 = java.lang.String.valueOf(r32)     // Catch:{ Exception -> 0x0533 }
            r35.m15974(r36, r37)     // Catch:{ Exception -> 0x0533 }
            goto L_0x04a0
        L_0x0533:
            r7 = move-exception
            r35 = 0
            r0 = r35
            r0 = r35
            boolean[] r0 = new boolean[r0]
            r35 = r0
            r0 = r35
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x04a0
        L_0x0545:
            boolean r35 = r24.isEmpty()
            if (r35 != 0) goto L_0x00e0
            r0 = r24
            r1 = r23
            r1 = r23
            java.util.Collections.sort(r0, r1)
            goto L_0x00e0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.api.TmdbApi.m15761(com.typhoon.tv.model.media.MediaInfo, boolean[]):java.util.ArrayList");
    }
}
