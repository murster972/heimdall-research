package com.typhoon.tv.api;

import com.google.gson.Gson;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.ImdbSearchSuggestionModel;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.Call;
import okhttp3.Request;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ImdbApi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final DateTimeFormatter f12505 = DateTimeFormat.m21228("dd MMM yyyy");

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile ImdbApi f12506;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DateTimeFormatter f12507 = DateTimeFormat.m21228("yyyy-MM-dd");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeFormatter f12508 = DateTimeFormat.m21228("dd MMM. yyyy");

    /* renamed from: 龘  reason: contains not printable characters */
    public static ImdbApi m15726() {
        ImdbApi imdbApi = f12506;
        if (imdbApi == null) {
            synchronized (ImdbApi.class) {
                try {
                    imdbApi = f12506;
                    if (imdbApi == null) {
                        ImdbApi imdbApi2 = new ImdbApi();
                        try {
                            f12506 = imdbApi2;
                            imdbApi = imdbApi2;
                        } catch (Throwable th) {
                            th = th;
                            ImdbApi imdbApi3 = imdbApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return imdbApi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Call m15727(String str) {
        Call call;
        if (str == null || str.isEmpty()) {
            call = null;
        } else {
            String replace = str.toLowerCase().replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "");
            call = HttpHelper.m6343().m6348().m7027(new Request.Builder().m19990().m19992("https://v2.sg.media-imdb.com" + String.format("/suggests/%s/%s.json", new Object[]{replace.substring(0, 1), replace})).m19993(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835).m19993("Referer", "http://www.imdb.com").m19991((Object) "ImdbSuggestionsApi").m19989());
        }
        return call;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m15728() {
        HttpHelper.m6343().m6370((Object) "ImdbSuggestionsApi");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public List<String> m15729(String str) {
        ArrayList arrayList = new ArrayList();
        for (ImdbSearchSuggestionModel.DBean l : ((ImdbSearchSuggestionModel) new Gson().fromJson(str, ImdbSearchSuggestionModel.class)).getD()) {
            try {
                String l2 = l.getL();
                if (l2 != null && !l2.trim().isEmpty() && !arrayList.contains(l2) && !l2.contains("IMDb")) {
                    arrayList.add(l2);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:70:0x02fa, code lost:
        if (r8.contains(".gif") == false) goto L_0x0302;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.LinkedHashMap<java.lang.Integer, com.typhoon.tv.model.media.tv.TvEpisodeInfo> m15730(com.typhoon.tv.model.media.MediaInfo r37, int r38) {
        /*
            r36 = this;
            java.util.LinkedHashMap r19 = new java.util.LinkedHashMap
            r19.<init>()
            java.lang.String r25 = r37.getImdbId()
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            java.lang.String r33 = "http://www.imdb.com/title/"
            java.lang.String r33 = "http://www.imdb.com/title/"
            java.lang.StringBuilder r32 = r32.append(r33)
            java.lang.String r33 = "tt"
            java.lang.String r33 = "tt"
            r0 = r25
            r0 = r25
            r1 = r33
            r1 = r33
            boolean r33 = r0.startsWith(r1)
            if (r33 == 0) goto L_0x00a4
        L_0x002c:
            r0 = r32
            r1 = r25
            r1 = r25
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r33 = "/episodes"
            java.lang.String r33 = "/episodes"
            java.lang.StringBuilder r32 = r32.append(r33)
            java.lang.String r23 = r32.toString()
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            r0 = r32
            r1 = r23
            r1 = r23
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r33 = "/_ajax?season="
            java.lang.String r33 = "/_ajax?season="
            java.lang.StringBuilder r32 = r32.append(r33)
            r0 = r32
            r0 = r32
            r1 = r38
            r1 = r38
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r6 = r32.toString()
            com.typhoon.tv.helper.http.HttpHelper r32 = com.typhoon.tv.helper.http.HttpHelper.m6343()
            r33 = 1
            r0 = r33
            r0 = r33
            java.util.Map[] r0 = new java.util.Map[r0]
            r33 = r0
            r34 = 0
            java.util.HashMap r35 = com.typhoon.tv.TyphoonApp.m6325()
            r33[r34] = r35
            r0 = r32
            r1 = r23
            r1 = r23
            r2 = r33
            r2 = r33
            java.lang.String r24 = r0.m6361((java.lang.String) r6, (java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
            org.jsoup.nodes.Document r10 = org.jsoup.Jsoup.m21674(r24)
            java.lang.String r32 = "h3#episode_top"
            java.lang.String r32 = "h3#episode_top"
            r0 = r32
            org.jsoup.nodes.Element r16 = r10.m21837(r0)
            if (r16 != 0) goto L_0x00c3
        L_0x00a3:
            return r19
        L_0x00a4:
            java.lang.StringBuilder r33 = new java.lang.StringBuilder
            r33.<init>()
            java.lang.String r34 = "tt"
            java.lang.String r34 = "tt"
            java.lang.StringBuilder r33 = r33.append(r34)
            r0 = r33
            r0 = r33
            r1 = r25
            java.lang.StringBuilder r33 = r0.append(r1)
            java.lang.String r25 = r33.toString()
            goto L_0x002c
        L_0x00c3:
            java.lang.String r32 = r16.m21868()
            java.lang.String r33 = "&nbsp;"
            java.lang.String r34 = ""
            java.lang.String r32 = r32.replace(r33, r34)
            java.lang.String r33 = "Season\\s+(\\d+)"
            r34 = 1
            r35 = 2
            java.lang.String r29 = com.typhoon.tv.utils.Regex.m17801((java.lang.String) r32, (java.lang.String) r33, (int) r34, (int) r35)
            boolean r32 = r29.isEmpty()
            if (r32 != 0) goto L_0x00a3
            int r32 = java.lang.Integer.parseInt(r29)
            r0 = r32
            r1 = r38
            if (r0 != r1) goto L_0x00a3
            com.typhoon.tv.TVDatabase r32 = com.typhoon.tv.TVApplication.m6287()
            int r33 = r37.getTmdbId()
            java.lang.Integer r33 = java.lang.Integer.valueOf(r33)
            com.typhoon.tv.model.CheckEpisodeResult r28 = r32.m6306((java.lang.Integer) r33)
            r27 = 0
            java.lang.String r32 = "div.list_item"
            java.lang.String r32 = "div.list_item"
            r0 = r32
            r0 = r32
            org.jsoup.select.Elements r32 = r10.m21815(r0)
            java.util.Iterator r32 = r32.iterator()
        L_0x0110:
            boolean r33 = r32.hasNext()
            if (r33 == 0) goto L_0x00a3
            java.lang.Object r15 = r32.next()
            org.jsoup.nodes.Element r15 = (org.jsoup.nodes.Element) r15
            java.lang.String r33 = "div.info[itemprop=\"episodes\"]"
            r0 = r33
            org.jsoup.nodes.Element r12 = r15.m21837(r0)     // Catch:{ Exception -> 0x0372 }
            if (r12 == 0) goto L_0x0110
            java.lang.String r33 = "meta[content][itemprop=\"episodeNumber\"]"
            java.lang.String r33 = "meta[content][itemprop=\"episodeNumber\"]"
            r0 = r33
            r0 = r33
            org.jsoup.nodes.Element r13 = r12.m21837(r0)     // Catch:{ Exception -> 0x0372 }
            if (r13 == 0) goto L_0x0110
            java.lang.String r33 = "content"
            r0 = r33
            r0 = r33
            java.lang.String r22 = r13.m21947(r0)     // Catch:{ Exception -> 0x0372 }
            boolean r33 = r22.isEmpty()     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x0110
            int r21 = java.lang.Integer.parseInt(r22)     // Catch:{ Exception -> 0x0372 }
            if (r21 <= 0) goto L_0x0110
            java.lang.String r4 = ""
            java.lang.String r33 = "div.airdate"
            r0 = r33
            r0 = r33
            org.jsoup.nodes.Element r11 = r12.m21837(r0)     // Catch:{ Exception -> 0x0372 }
            if (r11 == 0) goto L_0x0185
            java.lang.String r33 = r11.m21868()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r5 = r33.trim()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r33 = "."
            r0 = r33
            boolean r33 = r5.contains(r0)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x0375
            org.joda.time.format.DateTimeFormatter r33 = f12508     // Catch:{ Exception -> 0x0372 }
            r0 = r33
            r0 = r33
            org.joda.time.DateTime r9 = r0.m21236(r5)     // Catch:{ Exception -> 0x0372 }
        L_0x017b:
            org.joda.time.format.DateTimeFormatter r33 = f12507     // Catch:{ Exception -> 0x0372 }
            r0 = r33
            r0 = r33
            java.lang.String r4 = r9.toString(r0)     // Catch:{ Exception -> 0x0372 }
        L_0x0185:
            android.content.SharedPreferences r33 = com.typhoon.tv.TVApplication.m6285()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "pref_hide_unaired_episode"
            r35 = 1
            boolean r33 = r33.getBoolean(r34, r35)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x01c4
            if (r4 == 0) goto L_0x0110
            boolean r33 = r4.isEmpty()     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x0110
            org.joda.time.DateTime r33 = com.typhoon.tv.helper.DateTimeHelper.m15937((java.lang.String) r4)     // Catch:{ Exception -> 0x0372 }
            boolean r33 = com.typhoon.tv.helper.DateTimeHelper.m15933(r33)     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x01c4
            if (r27 != 0) goto L_0x0110
            if (r28 == 0) goto L_0x0110
            int r33 = r28.getLastSeason()     // Catch:{ Exception -> 0x0372 }
            r0 = r38
            r0 = r38
            r1 = r33
            if (r0 != r1) goto L_0x0110
            int r33 = r28.getLastEpisode()     // Catch:{ Exception -> 0x0372 }
            r0 = r21
            r0 = r21
            r1 = r33
            if (r0 != r1) goto L_0x0110
            r27 = 1
        L_0x01c4:
            java.lang.String r31 = ""
            java.lang.String r33 = "div.item_description"
            r0 = r33
            r0 = r33
            org.jsoup.nodes.Element r17 = r12.m21837(r0)     // Catch:{ Exception -> 0x0372 }
            if (r17 == 0) goto L_0x0252
            java.lang.String r33 = r17.m21868()     // Catch:{ Exception -> 0x0372 }
            boolean r33 = r33.isEmpty()     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x0252
            java.lang.String r33 = r17.m21868()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "Know what this is about?"
            java.lang.String r34 = "Know what this is about?"
            boolean r33 = r33.contains(r34)     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x0252
            java.lang.String r33 = r17.m21868()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "The plot is unknown at this time"
            boolean r33 = r33.contains(r34)     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x0252
            java.lang.String r30 = r17.m21868()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r33 = r30.trim()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "..."
            java.lang.String r34 = "..."
            boolean r33 = r33.endsWith(r34)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x024e
            java.lang.String r33 = ". "
            r0 = r30
            r0 = r30
            r1 = r33
            boolean r33 = r0.contains(r1)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x024e
            java.lang.String r33 = r30.trim()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "\\.\\.\\.$"
            java.lang.String r34 = "\\.\\.\\.$"
            java.lang.String r35 = ""
            java.lang.String r35 = ""
            java.lang.String r30 = r33.replaceAll(r34, r35)     // Catch:{ Exception -> 0x0372 }
            r33 = 0
            r34 = 46
            r0 = r30
            r0 = r30
            r1 = r34
            int r34 = r0.lastIndexOf(r1)     // Catch:{ Exception -> 0x0372 }
            r0 = r30
            r0 = r30
            r1 = r33
            r2 = r34
            r2 = r34
            java.lang.String r30 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0372 }
        L_0x024e:
            r31 = r30
            r31 = r30
        L_0x0252:
            java.lang.String r7 = ""
            java.lang.String r20 = ""
            java.lang.String r20 = ""
            java.lang.String r33 = "img[src]"
            r0 = r33
            r0 = r33
            org.jsoup.nodes.Element r14 = r15.m21837(r0)     // Catch:{ Exception -> 0x0372 }
            if (r14 == 0) goto L_0x02cd
            java.lang.String r33 = "src"
            java.lang.String r33 = "src"
            r0 = r33
            r0 = r33
            java.lang.String r26 = r14.m21947(r0)     // Catch:{ Exception -> 0x0372 }
            java.lang.String r33 = "/"
            r0 = r26
            r1 = r33
            r1 = r33
            boolean r33 = r0.startsWith(r1)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x029f
            java.lang.StringBuilder r33 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0372 }
            r33.<init>()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "http://www.imdb.com"
            java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x0372 }
            r0 = r33
            r1 = r26
            r1 = r26
            java.lang.StringBuilder r33 = r0.append(r1)     // Catch:{ Exception -> 0x0372 }
            java.lang.String r26 = r33.toString()     // Catch:{ Exception -> 0x0372 }
        L_0x029f:
            java.lang.String r33 = "(.*?\\._V\\d+)(.*)(\\..*)"
            java.lang.String r34 = "$1$3"
            r0 = r26
            r0 = r26
            r1 = r33
            r1 = r33
            r2 = r34
            java.lang.String r7 = r0.replaceAll(r1, r2)     // Catch:{ Exception -> 0x0372 }
            java.lang.String r33 = "alt"
            r0 = r33
            r0 = r33
            boolean r33 = r14.m21951((java.lang.String) r0)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x02cd
            java.lang.String r33 = "alt"
            r0 = r33
            java.lang.String r33 = r14.m21947(r0)     // Catch:{ Exception -> 0x0372 }
            java.lang.String r20 = r33.trim()     // Catch:{ Exception -> 0x0372 }
        L_0x02cd:
            java.lang.String r8 = r7.toLowerCase()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r33 = "spinning"
            r0 = r33
            r0 = r33
            boolean r33 = r8.contains(r0)     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x02fc
            java.lang.String r33 = "progress"
            java.lang.String r33 = "progress"
            r0 = r33
            r0 = r33
            boolean r33 = r8.contains(r0)     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x02fc
            java.lang.String r33 = ".gif"
            java.lang.String r33 = ".gif"
            r0 = r33
            boolean r33 = r8.contains(r0)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x0302
        L_0x02fc:
            java.lang.String r7 = ""
            java.lang.String r7 = ""
        L_0x0302:
            boolean r33 = r20.isEmpty()     // Catch:{ Exception -> 0x0372 }
            if (r33 != 0) goto L_0x0318
            java.lang.String r33 = r20.toLowerCase()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "loading"
            java.lang.String r34 = "loading"
            boolean r33 = r33.contains(r34)     // Catch:{ Exception -> 0x0372 }
            if (r33 == 0) goto L_0x0332
        L_0x0318:
            java.lang.StringBuilder r33 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0372 }
            r33.<init>()     // Catch:{ Exception -> 0x0372 }
            java.lang.String r34 = "Episode "
            java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x0372 }
            r0 = r33
            r0 = r33
            r1 = r21
            java.lang.StringBuilder r33 = r0.append(r1)     // Catch:{ Exception -> 0x0372 }
            java.lang.String r20 = r33.toString()     // Catch:{ Exception -> 0x0372 }
        L_0x0332:
            com.typhoon.tv.model.media.tv.TvEpisodeInfo r18 = new com.typhoon.tv.model.media.tv.TvEpisodeInfo     // Catch:{ Exception -> 0x0372 }
            java.lang.Integer r33 = java.lang.Integer.valueOf(r38)     // Catch:{ Exception -> 0x0372 }
            java.lang.Integer r34 = java.lang.Integer.valueOf(r21)     // Catch:{ Exception -> 0x0372 }
            r0 = r18
            r0 = r18
            r1 = r33
            r1 = r33
            r2 = r34
            r3 = r20
            r3 = r20
            r0.<init>(r1, r2, r3, r7)     // Catch:{ Exception -> 0x0372 }
            r0 = r18
            r0 = r18
            r0.setAirDate(r4)     // Catch:{ Exception -> 0x0372 }
            r0 = r18
            r1 = r31
            r1 = r31
            r0.setOverview(r1)     // Catch:{ Exception -> 0x0372 }
            java.lang.Integer r33 = java.lang.Integer.valueOf(r21)     // Catch:{ Exception -> 0x0372 }
            r0 = r19
            r0 = r19
            r1 = r33
            r1 = r33
            r2 = r18
            r2 = r18
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0372 }
            goto L_0x0110
        L_0x0372:
            r33 = move-exception
            goto L_0x0110
        L_0x0375:
            org.joda.time.format.DateTimeFormatter r33 = f12505     // Catch:{ Exception -> 0x0372 }
            r0 = r33
            org.joda.time.DateTime r9 = r0.m21236(r5)     // Catch:{ Exception -> 0x0372 }
            goto L_0x017b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.api.ImdbApi.m15730(com.typhoon.tv.model.media.MediaInfo, int):java.util.LinkedHashMap");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Integer> m15731(String str) {
        int parseInt;
        StringBuilder append = new StringBuilder().append("http://www.imdb.com/title/");
        if (!str.startsWith(TtmlNode.TAG_TT)) {
            str = TtmlNode.TAG_TT + str;
        }
        Document r0 = Jsoup.m21674(HttpHelper.m6343().m6351(append.append(str).append("/episodes").toString(), (Map<String, String>[]) new Map[0]));
        ArrayList arrayList = new ArrayList();
        Element r3 = r0.m21837("select#bySeason");
        if (r3 != null) {
            Iterator it2 = r3.m21815("option[value]").iterator();
            while (it2.hasNext()) {
                try {
                    String r6 = ((Element) it2.next()).m21947("value");
                    if (!r6.trim().isEmpty() && !r6.trim().equals("-1") && Utils.m6426(r6) && (parseInt = Integer.parseInt(r6)) >= 0) {
                        if (parseInt != 0 || TVApplication.m6285().getBoolean("pref_show_season_special", false)) {
                            arrayList.add(Integer.valueOf(parseInt));
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        }
        return arrayList;
    }
}
