package com.typhoon.tv.api;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvNewEpisodeInfo;
import com.typhoon.tv.model.media.tv.tvmaze.TvMazeScheduleResult;
import com.typhoon.tv.utils.Regex;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TvMazeApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile TvMazeApi f12556;

    /* renamed from: 龘  reason: contains not printable characters */
    public static TvMazeApi m15810() {
        TvMazeApi tvMazeApi = f12556;
        if (tvMazeApi == null) {
            Class<TvMazeApi> cls = TvMazeApi.class;
            synchronized (TvMazeApi.class) {
                try {
                    tvMazeApi = f12556;
                    if (tvMazeApi == null) {
                        TvMazeApi tvMazeApi2 = new TvMazeApi();
                        try {
                            f12556 = tvMazeApi2;
                            tvMazeApi = tvMazeApi2;
                        } catch (Throwable th) {
                            th = th;
                            TvMazeApi tvMazeApi3 = tvMazeApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return tvMazeApi;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo m15811(TvMazeScheduleResult.ShowBean showBean) {
        String name;
        String premiered;
        String language = showBean.getLanguage();
        if (language == null || language.isEmpty() || !language.trim().toLowerCase().contains("english") || (name = showBean.getName()) == null || name.isEmpty() || (premiered = showBean.getPremiered()) == null || premiered.isEmpty() || premiered.length() < 4) {
            return null;
        }
        int i = -1;
        String r13 = Regex.m17800(premiered, "(\\d{4})", 1);
        if (!r13.isEmpty()) {
            i = Integer.parseInt(r13);
        }
        TvMazeScheduleResult.ShowBean.ExternalsBean externals = showBean.getExternals();
        if (externals == null) {
            return null;
        }
        int i2 = -1;
        Object thetvdb = externals.getThetvdb();
        if (thetvdb != null && (thetvdb instanceof Integer) && ((Integer) thetvdb).intValue() > 0) {
            i2 = ((Integer) thetvdb).intValue();
        }
        String str = "";
        Object imdb = externals.getImdb();
        if (!(imdb == null || imdb.toString() == null)) {
            str = imdb.toString();
            if (!str.isEmpty()) {
                str = TtmlNode.TAG_TT + str.replaceAll("[^0-9]", "");
            }
        }
        if (i2 <= -1 && str.isEmpty()) {
            return null;
        }
        TvMazeScheduleResult.ShowBean.ImageBean image = showBean.getImage();
        String str2 = "";
        if (image != null) {
            str2 = image.getOriginal();
        }
        MediaInfo mediaInfo = new MediaInfo(0, 1, -1, name, i);
        if (i2 > -1) {
            mediaInfo.setTvdbId(i2);
        }
        if (!str.isEmpty()) {
            mediaInfo.setImdbId(str);
        }
        if (str2 == null || str2.isEmpty()) {
            return mediaInfo;
        }
        mediaInfo.setPosterUrl(str2);
        return mediaInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<TvNewEpisodeInfo> m15812(String str, int i) {
        String str2 = str;
        String r5 = HttpHelper.m6343().m6351("http://api.tvmaze.com/schedule?date=" + str, (Map<String, String>[]) new Map[0]);
        ArrayList arrayList = new ArrayList();
        JsonArray jsonArray = null;
        try {
            jsonArray = new JsonParser().parse(r5).getAsJsonArray();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (jsonArray != null) {
            Iterator<JsonElement> it2 = jsonArray.iterator();
            while (it2.hasNext()) {
                try {
                    Class<TvMazeScheduleResult> cls = TvMazeScheduleResult.class;
                    TvMazeScheduleResult tvMazeScheduleResult = (TvMazeScheduleResult) new Gson().fromJson(it2.next(), TvMazeScheduleResult.class);
                    TvMazeScheduleResult.ShowBean show = tvMazeScheduleResult.getShow();
                    if (show != null) {
                        MediaInfo r6 = m15811(show);
                        if (r6 != null) {
                            int season = tvMazeScheduleResult.getSeason();
                            int number = tvMazeScheduleResult.getNumber();
                            if (season > 0 && number >= 0) {
                                TvNewEpisodeInfo tvNewEpisodeInfo = new TvNewEpisodeInfo(r6, season, number);
                                tvNewEpisodeInfo.setTitle(tvMazeScheduleResult.getName());
                                String summary = tvMazeScheduleResult.getSummary();
                                if (summary != null && !summary.isEmpty()) {
                                    tvNewEpisodeInfo.setOverview(summary.replaceAll("<.+?>|</.+?>|\\n", ""));
                                }
                                arrayList.add(tvNewEpisodeInfo);
                            }
                        }
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            }
        }
        return arrayList;
    }
}
