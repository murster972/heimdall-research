package com.typhoon.tv.api;

import android.widget.Toast;
import com.google.gson.Gson;
import com.typhoon.tv.Logger;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.event.trakt.TraktWaitingToVerifyEvent;
import com.typhoon.tv.exception.FailedToSyncTraktCollectionsException;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.helper.trakt.TraktHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.trakt.TraktCredentialsInfo;
import com.typhoon.tv.model.trakt.TraktGetDeviceCodeResult;
import com.typhoon.tv.model.trakt.TraktGetTokenResult;
import com.typhoon.tv.model.trakt.TraktUserInfo;
import com.typhoon.tv.utils.Utils;
import com.uwetrottmann.trakt5.entities.BaseEpisode;
import com.uwetrottmann.trakt5.entities.BaseMovie;
import com.uwetrottmann.trakt5.entities.BaseSeason;
import com.uwetrottmann.trakt5.entities.BaseShow;
import com.uwetrottmann.trakt5.entities.Movie;
import com.uwetrottmann.trakt5.entities.MovieIds;
import com.uwetrottmann.trakt5.entities.Show;
import com.uwetrottmann.trakt5.entities.ShowIds;
import com.uwetrottmann.trakt5.entities.SyncEpisode;
import com.uwetrottmann.trakt5.entities.SyncItems;
import com.uwetrottmann.trakt5.entities.SyncMovie;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import com.uwetrottmann.trakt5.entities.SyncSeason;
import com.uwetrottmann.trakt5.entities.SyncShow;
import com.uwetrottmann.trakt5.entities.UserSlug;
import com.uwetrottmann.trakt5.enums.Extended;
import com.uwetrottmann.trakt5.services.Sync;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.apache.oltu.oauth2.common.OAuth;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class TraktUserApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile TraktUserApi f12518;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Hashtable<String, String> f12519 = new Hashtable<>();

    private TraktUserApi() {
        this.f12519.put(OAuth.HeaderType.CONTENT_TYPE, "application/json");
        this.f12519.put("trakt-api-key", "701afbabf01f9fb0e2bc524e94afa810d9c6742026d27c8da2611acbc757cbb6");
        this.f12519.put("trakt-api-version", String.valueOf(2));
        TraktCredentialsInfo r0 = TraktCredentialsHelper.m16110();
        if (r0.isValid()) {
            this.f12519.put(OAuth.HeaderType.AUTHORIZATION, "Bearer " + r0.getAccessToken());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Observable<List<MediaInfo>> m15770(final boolean z) {
        return Observable.m7356(TraktHelper.m16114().m17907().collectionMovies(Extended.FULL)).m7392(new Func1<Call<List<BaseMovie>>, Response<List<BaseMovie>>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Response<List<BaseMovie>> call(Call<List<BaseMovie>> call) {
                try {
                    return call.m24294();
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    throw new FailedToSyncTraktCollectionsException(e, 2, 1);
                }
            }
        }).m7392(new Func1<Response<List<BaseMovie>>, List<BaseMovie>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public List<BaseMovie> call(Response<List<BaseMovie>> response) {
                if (response != null && response.m24390()) {
                    return response.m24389();
                }
                throw new FailedToSyncTraktCollectionsException("listResponse is null", 2, 1);
            }
        }).m7396(new Func1<List<BaseMovie>, Observable<MediaInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaInfo> call(List<BaseMovie> list) {
                if (list != null) {
                    return Observable.m7355(list).m7396(new Func1<BaseMovie, Observable<MediaInfo>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Observable<MediaInfo> call(BaseMovie baseMovie) {
                            Observable<MediaInfo> r1;
                            try {
                                Movie movie = baseMovie.movie;
                                MediaInfo mediaInfo = new MediaInfo(1, 1, movie.ids.tmdb.intValue(), movie.title, movie.year.intValue());
                                mediaInfo.setImdbId(movie.ids.imdb);
                                r1 = Observable.m7356(mediaInfo);
                            } catch (Exception e) {
                                r1 = Observable.m7349();
                            }
                            return r1;
                        }
                    }).m7385(new Func1<MediaInfo, Boolean>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Boolean call(MediaInfo mediaInfo) {
                            return Boolean.valueOf(mediaInfo != null);
                        }
                    }).m7396(new Func1<MediaInfo, Observable<MediaInfo>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Observable<MediaInfo> call(final MediaInfo mediaInfo) {
                            return (!z || (mediaInfo.getPosterUrl() != null && !mediaInfo.getPosterUrl().isEmpty())) ? Observable.m7356(mediaInfo) : Observable.m7356(mediaInfo).m7392(new Func1<MediaInfo, MediaInfo>() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public MediaInfo call(MediaInfo mediaInfo) {
                                    mediaInfo.setPosterUrl(TraktApi.m15764().m15769(mediaInfo.getType(), mediaInfo.getTmdbId(), mediaInfo.getTvdbId(), mediaInfo.getImdbId()));
                                    return mediaInfo;
                                }
                            }).m7376(new Func1<Throwable, MediaInfo>() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public MediaInfo call(Throwable th) {
                                    Logger.m6281(th, new boolean[0]);
                                    return mediaInfo;
                                }
                            }).m7382(Schedulers.io());
                        }
                    });
                }
                boolean z = !true;
                throw new FailedToSyncTraktCollectionsException("baseMovies is null", 2, 1);
            }
        }).m7373();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15772(List<BaseShow> list) {
        for (BaseShow next : list) {
            try {
                ShowIds showIds = next.show.ids;
                if (showIds.tmdb.intValue() > 0) {
                    for (BaseSeason next2 : next.seasons) {
                        for (BaseEpisode baseEpisode : next2.episodes) {
                            TVApplication.m6287().m6300(showIds.tmdb, next2.number, baseEpisode.number);
                        }
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Observable<SyncResponse> m15773() {
        return Observable.m7359(new Observable.OnSubscribe<SyncItems>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super SyncItems> subscriber) {
                ArrayList<MediaInfo> r4 = TVApplication.m6287().m6312((Integer) 0);
                ArrayList<MediaInfo> r2 = TVApplication.m6287().m6312((Integer) 1);
                SyncItems syncItems = new SyncItems();
                ArrayList arrayList = new ArrayList();
                for (MediaInfo next : r4) {
                    try {
                        ShowIds showIds = new ShowIds();
                        showIds.imdb = next.getImdbId();
                        showIds.tmdb = Integer.valueOf(next.getTmdbId());
                        showIds.tvdb = Integer.valueOf(next.getTvdbId());
                        arrayList.add(new SyncShow().id(showIds));
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                for (MediaInfo next2 : r2) {
                    try {
                        MovieIds movieIds = new MovieIds();
                        movieIds.imdb = next2.getImdbId();
                        movieIds.tmdb = Integer.valueOf(next2.getTmdbId());
                        arrayList2.add(new SyncMovie().id(movieIds));
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                }
                subscriber.onNext(syncItems.shows((List<SyncShow>) arrayList).movies((List<SyncMovie>) arrayList2));
                subscriber.onCompleted();
            }
        }).m7392(new Func1<SyncItems, Response<SyncResponse>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Response<SyncResponse> call(SyncItems syncItems) {
                try {
                    return TraktHelper.m16114().m17907().addItemsToCollection(syncItems).m24294();
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    throw new FailedToSyncTraktCollectionsException(e, 1, -1);
                }
            }
        }).m7392(new Func1<Response<SyncResponse>, SyncResponse>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public SyncResponse call(Response<SyncResponse> response) {
                if (response != null && response.m24390()) {
                    return response.m24389();
                }
                throw new FailedToSyncTraktCollectionsException("listResponse is null", 1, -1);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TraktUserApi m15774() {
        TraktUserApi traktUserApi = f12518;
        if (traktUserApi == null) {
            Class<TraktUserApi> cls = TraktUserApi.class;
            synchronized (TraktUserApi.class) {
                try {
                    traktUserApi = f12518;
                    if (traktUserApi == null) {
                        TraktUserApi traktUserApi2 = new TraktUserApi();
                        try {
                            f12518 = traktUserApi2;
                            TraktUserApi traktUserApi3 = traktUserApi2;
                            traktUserApi = traktUserApi2;
                        } catch (Throwable th) {
                            th = th;
                            TraktUserApi traktUserApi4 = traktUserApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return traktUserApi;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Observable<List<MediaInfo>> m15776(final boolean z) {
        return Observable.m7356(TraktHelper.m16114().m17907().collectionShows(Extended.FULL)).m7392(new Func1<Call<List<BaseShow>>, Response<List<BaseShow>>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Response<List<BaseShow>> call(Call<List<BaseShow>> call) {
                try {
                    return call.m24294();
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    throw new FailedToSyncTraktCollectionsException(e, 2, 0);
                }
            }
        }).m7392(new Func1<Response<List<BaseShow>>, List<BaseShow>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public List<BaseShow> call(Response<List<BaseShow>> response) {
                if (response != null && response.m24390()) {
                    return response.m24389();
                }
                throw new FailedToSyncTraktCollectionsException("listResponse is null", 2, 0);
            }
        }).m7396(new Func1<List<BaseShow>, Observable<MediaInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaInfo> call(List<BaseShow> list) {
                if (list != null) {
                    return Observable.m7355(list).m7396(new Func1<BaseShow, Observable<MediaInfo>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Observable<MediaInfo> call(BaseShow baseShow) {
                            Observable<MediaInfo> r1;
                            try {
                                Show show = baseShow.show;
                                MediaInfo mediaInfo = new MediaInfo(0, 1, show.ids.tmdb.intValue(), show.title, show.year.intValue());
                                mediaInfo.setImdbId(show.ids.imdb);
                                mediaInfo.setTvdbId(show.ids.tvdb.intValue());
                                r1 = Observable.m7356(mediaInfo);
                            } catch (Exception e) {
                                r1 = Observable.m7349();
                            }
                            return r1;
                        }
                    }).m7385(new Func1<MediaInfo, Boolean>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Boolean call(MediaInfo mediaInfo) {
                            boolean z;
                            if (mediaInfo != null) {
                                z = true;
                                int i = 5 >> 1;
                            } else {
                                z = false;
                            }
                            return Boolean.valueOf(z);
                        }
                    }).m7396(new Func1<MediaInfo, Observable<MediaInfo>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Observable<MediaInfo> call(final MediaInfo mediaInfo) {
                            return (!z || (mediaInfo.getPosterUrl() != null && !mediaInfo.getPosterUrl().isEmpty())) ? Observable.m7356(mediaInfo) : Observable.m7356(mediaInfo).m7392(new Func1<MediaInfo, MediaInfo>() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public MediaInfo call(MediaInfo mediaInfo) {
                                    mediaInfo.setPosterUrl(TraktApi.m15764().m15769(mediaInfo.getType(), mediaInfo.getTmdbId(), mediaInfo.getTvdbId(), mediaInfo.getImdbId()));
                                    return mediaInfo;
                                }
                            }).m7376(new Func1<Throwable, MediaInfo>() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public MediaInfo call(Throwable th) {
                                    Logger.m6281(th, new boolean[0]);
                                    return mediaInfo;
                                }
                            }).m7382(Schedulers.io());
                        }
                    });
                }
                throw new FailedToSyncTraktCollectionsException("baseShows is null", 2, 0);
            }
        }).m7373();
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15778(List<BaseMovie> list) {
        for (BaseMovie baseMovie : list) {
            try {
                MovieIds movieIds = baseMovie.movie.ids;
                if (movieIds.tmdb.intValue() > 0) {
                    TVApplication.m6287().m6301(movieIds.tmdb, movieIds.imdb);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Observable<Boolean> m15779() {
        return Observable.m7359(new Observable.OnSubscribe<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super Boolean> subscriber) {
                if (TraktCredentialsHelper.m16110().isValid()) {
                    Subscriber<? super Boolean> subscriber2 = subscriber;
                    subscriber.onNext(1);
                    subscriber.onCompleted();
                    return;
                }
                try {
                    boolean[] zArr = new boolean[0];
                    boolean[] zArr2 = zArr;
                    String str = "{\"client_id\":\"" + Utils.m6414("701afbabf01f9fb0e2bc524e94afa810d9c6742026d27c8da2611acbc757cbb6", zArr) + "\"}";
                    String r9 = HttpHelper.m6343().m6360("https://api.trakt.tv/oauth/device/code", str, false, (Map<String, String>[]) new Map[]{TraktUserApi.this.f12519});
                    if (r9.isEmpty()) {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                        return;
                    }
                    Gson gson = new Gson();
                    Class<TraktGetDeviceCodeResult> cls = TraktGetDeviceCodeResult.class;
                    Class<TraktGetDeviceCodeResult> cls2 = TraktGetDeviceCodeResult.class;
                    Gson gson2 = gson;
                    Class<TraktGetDeviceCodeResult> cls3 = cls2;
                    TraktGetDeviceCodeResult traktGetDeviceCodeResult = (TraktGetDeviceCodeResult) gson.fromJson(r9, cls2);
                    String verification_url = traktGetDeviceCodeResult.getVerification_url();
                    String user_code = traktGetDeviceCodeResult.getUser_code();
                    String device_code = traktGetDeviceCodeResult.getDevice_code();
                    int expires_in = traktGetDeviceCodeResult.getExpires_in();
                    int interval = traktGetDeviceCodeResult.getInterval();
                    String str2 = verification_url;
                    RxBus.m15709().m15711(new TraktWaitingToVerifyEvent(verification_url, user_code));
                    TraktGetTokenResult traktGetTokenResult = null;
                    int i = 0;
                    while (true) {
                        if (i >= expires_in || subscriber.isUnsubscribed()) {
                            break;
                        }
                        try {
                            Thread.sleep(1000);
                            float f = (float) interval;
                            float f2 = f;
                            if (((float) i) % f != 0.0f) {
                                continue;
                            } else {
                                boolean[] zArr3 = new boolean[0];
                                boolean[] zArr4 = zArr3;
                                boolean[] zArr5 = new boolean[0];
                                boolean[] zArr6 = zArr5;
                                boolean[] zArr7 = new boolean[0];
                                boolean[] zArr8 = zArr7;
                                String str3 = "{\"client_id\":\"" + Utils.m6414("701afbabf01f9fb0e2bc524e94afa810d9c6742026d27c8da2611acbc757cbb6", zArr3) + "\",\"client_secret\":\"" + Utils.m6414("95dbc0e003bc1a0882588485a127ace45413022970ceeaaf2c7012c1c72b1781", zArr5) + "\",\"code\":\"" + Utils.m6414(device_code, zArr7) + "\"}";
                                HttpHelper r19 = HttpHelper.m6343();
                                Map[] mapArr = {TraktUserApi.this.f12519};
                                HttpHelper httpHelper = r19;
                                Object obj = "https://api.trakt.tv/oauth/device/token";
                                Map[] mapArr2 = mapArr;
                                String r11 = r19.m6360("https://api.trakt.tv/oauth/device/token", str3, false, (Map<String, String>[]) mapArr);
                                if (r11.contains("access_token")) {
                                    Gson gson3 = new Gson();
                                    Class<TraktGetTokenResult> cls4 = TraktGetTokenResult.class;
                                    Class<TraktGetTokenResult> cls5 = TraktGetTokenResult.class;
                                    Gson gson4 = gson3;
                                    Class<TraktGetTokenResult> cls6 = cls5;
                                    Object fromJson = gson3.fromJson(r11, cls5);
                                    Object obj2 = fromJson;
                                    traktGetTokenResult = (TraktGetTokenResult) fromJson;
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            boolean[] zArr9 = new boolean[0];
                            boolean[] zArr10 = zArr9;
                            Logger.m6281((Throwable) e, zArr9);
                        }
                        i++;
                    }
                    if (traktGetTokenResult == null || traktGetTokenResult.getAccess_token() == null || traktGetTokenResult.getAccess_token().isEmpty()) {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                        return;
                    }
                    String access_token = traktGetTokenResult.getAccess_token();
                    TraktUserApi traktUserApi = TraktUserApi.this;
                    TraktUserApi traktUserApi2 = traktUserApi;
                    traktUserApi.f12519.put(OAuth.HeaderType.AUTHORIZATION, "Bearer " + access_token);
                    TraktCredentialsInfo traktCredentialsInfo = new TraktCredentialsInfo();
                    traktCredentialsInfo.setAccessToken(access_token);
                    traktCredentialsInfo.setRefreshToken(traktGetTokenResult.getRefresh_token());
                    try {
                        HttpHelper r192 = HttpHelper.m6343();
                        Map[] mapArr3 = new Map[1];
                        Map[] mapArr4 = mapArr3;
                        Map[] mapArr5 = mapArr3;
                        mapArr5[0] = TraktUserApi.this.f12519;
                        traktCredentialsInfo.setUser(((TraktUserInfo) new Gson().fromJson(r192.m6351("https://api.trakt.tv/users/me", (Map<String, String>[]) mapArr5), TraktUserInfo.class)).getUsername());
                    } catch (Exception e2) {
                        boolean[] zArr11 = new boolean[0];
                        boolean[] zArr12 = zArr11;
                        boolean[] zArr13 = zArr11;
                        boolean[] zArr14 = zArr13;
                        Logger.m6280(e2, "Unable to get trakt user info", zArr13);
                    }
                    TraktCredentialsHelper.m16111(traktCredentialsInfo);
                    subscriber.onNext(1);
                    subscriber.onCompleted();
                } catch (Exception e3) {
                    boolean[] zArr15 = new boolean[0];
                    boolean[] zArr16 = zArr15;
                    boolean[] zArr17 = zArr15;
                    boolean[] zArr18 = zArr17;
                    Logger.m6281((Throwable) e3, zArr17);
                    subscriber.onNext(null);
                }
            }
        });
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m15780(MediaInfo mediaInfo, boolean z, Callback<SyncResponse> callback) {
        boolean z2 = true;
        if (mediaInfo.getType() != 1) {
            z2 = false;
        }
        SyncItems syncItems = new SyncItems();
        if (z2) {
            MovieIds movieIds = new MovieIds();
            movieIds.imdb = mediaInfo.getImdbId();
            movieIds.tmdb = Integer.valueOf(mediaInfo.getTmdbId());
            syncItems.movies(new SyncMovie().id(movieIds));
        } else {
            ShowIds showIds = new ShowIds();
            showIds.imdb = mediaInfo.getImdbId();
            showIds.tmdb = Integer.valueOf(mediaInfo.getTmdbId());
            showIds.tvdb = Integer.valueOf(mediaInfo.getTvdbId());
            syncItems.shows(new SyncShow().id(showIds));
        }
        Sync r3 = TraktHelper.m16114().m17907();
        (z ? r3.addItemsToCollection(syncItems) : r3.deleteItemsFromCollection(syncItems)).m24295(callback);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m15781(final boolean z, final boolean z2) {
        TraktHelper.m16114().m17908().watchedMovies(UserSlug.ME, Extended.FULL).m24295(new Callback<List<BaseMovie>>() {
            public void onFailure(Call<List<BaseMovie>> call, Throwable th) {
                Logger.m6281(th, new boolean[0]);
                if (z) {
                    Toast.makeText(TVApplication.m6288(), "Failed to download watched movies from Trakt...", 1).show();
                }
            }

            public void onResponse(Call<List<BaseMovie>> call, Response<List<BaseMovie>> response) {
                if (response.m24390()) {
                    final List r0 = response.m24389();
                    if (r0 != null) {
                        if (z2) {
                            Observable.m7359(new Observable.OnSubscribe<Boolean>() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public void call(Subscriber<? super Boolean> subscriber) {
                                    TraktUserApi.this.m15778((List<BaseMovie>) r0);
                                    subscriber.onNext(true);
                                    subscriber.onCompleted();
                                }
                            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7393(1).m7411((Action0) new Action0() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public void m15790() {
                                    if (z) {
                                        boolean z = false | true;
                                        Toast.makeText(TVApplication.m6288(), "Downloaded watched movies from Trakt successfully!", 1).show();
                                    }
                                }
                            }).m7372();
                        } else {
                            TraktUserApi.this.m15778((List<BaseMovie>) r0);
                        }
                        if (!z2 && z) {
                            Toast.makeText(TVApplication.m6288(), "Downloaded watched movies from Trakt successfully!", 1).show();
                        }
                    } else if (z) {
                        Toast.makeText(TVApplication.m6288(), "Failed to download watched movies from Trakt...", 1).show();
                    }
                } else if (z) {
                    Toast.makeText(TVApplication.m6288(), "Failed to download watched movies from Trakt...", 1).show();
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<Object> m15782(boolean z, Action1<Throwable> action1) {
        return Observable.m7362(m15773().m7412((Action1<? super Throwable>) action1).m7375(Observable.m7349()).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()), (Observable<SyncResponse>) m15776(z).m7412((Action1<? super Throwable>) action1).m7375(Observable.m7349()).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()), (Observable<SyncResponse>) m15770(z).m7412((Action1<? super Throwable>) action1).m7375(Observable.m7349()).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15783(MediaInfo mediaInfo, int i, int i2, boolean z, Callback<SyncResponse> callback) {
        ShowIds showIds = new ShowIds();
        showIds.imdb = mediaInfo.getImdbId();
        showIds.tmdb = Integer.valueOf(mediaInfo.getTmdbId());
        showIds.tvdb = Integer.valueOf(mediaInfo.getTvdbId());
        SyncShow id = new SyncShow().id(showIds);
        id.seasons(new SyncSeason().number(i).episodes(new SyncEpisode().number(i2)));
        SyncItems syncItems = new SyncItems();
        syncItems.shows(id);
        Sync r1 = TraktHelper.m16114().m17907();
        (z ? r1.addItemsToWatchedHistory(syncItems) : r1.deleteItemsFromWatchedHistory(syncItems)).m24295(callback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15784(MediaInfo mediaInfo, boolean z, Callback<SyncResponse> callback) {
        MovieIds movieIds = new MovieIds();
        movieIds.imdb = mediaInfo.getImdbId();
        movieIds.tmdb = Integer.valueOf(mediaInfo.getTmdbId());
        SyncMovie id = new SyncMovie().id(movieIds);
        SyncItems syncItems = new SyncItems();
        syncItems.movies(id);
        Sync r1 = TraktHelper.m16114().m17907();
        (z ? r1.addItemsToWatchedHistory(syncItems) : r1.deleteItemsFromWatchedHistory(syncItems)).m24295(callback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15785(final boolean z, final boolean z2) {
        TraktHelper.m16114().m17908().watchedShows(UserSlug.ME, Extended.FULL).m24295(new Callback<List<BaseShow>>() {
            public void onFailure(Call<List<BaseShow>> call, Throwable th) {
                Logger.m6281(th, new boolean[0]);
                if (z) {
                    boolean z = true | true;
                    Toast.makeText(TVApplication.m6288(), "Failed to download watched episodes from Trakt...", 1).show();
                }
            }

            public void onResponse(Call<List<BaseShow>> call, Response<List<BaseShow>> response) {
                if (response.m24390()) {
                    final List r0 = response.m24389();
                    if (r0 != null) {
                        if (z2) {
                            Observable.m7359(new Observable.OnSubscribe<Boolean>() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public void call(Subscriber<? super Boolean> subscriber) {
                                    TraktUserApi.this.m15772((List<BaseShow>) r0);
                                    subscriber.onNext(true);
                                    subscriber.onCompleted();
                                }
                            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7393(1).m7411((Action0) new Action0() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public void m15788() {
                                    if (z) {
                                        Toast.makeText(TVApplication.m6288(), "Downloaded watched episodes from Trakt successfully!", 1).show();
                                    }
                                }
                            }).m7372();
                        } else {
                            TraktUserApi.this.m15772((List<BaseShow>) r0);
                        }
                        if (!z2 && z) {
                            Toast.makeText(TVApplication.m6288(), "Downloaded watched episodes from Trakt successfully!", 1).show();
                        }
                    } else if (z) {
                        Toast.makeText(TVApplication.m6288(), "Failed to download watched episodes from Trakt...", 1).show();
                    }
                } else if (z) {
                    Toast.makeText(TVApplication.m6288(), "Failed to download watched episodes from Trakt...", 1).show();
                }
            }
        });
    }
}
