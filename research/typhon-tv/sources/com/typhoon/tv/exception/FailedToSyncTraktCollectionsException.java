package com.typhoon.tv.exception;

public class FailedToSyncTraktCollectionsException extends RuntimeException {
    public static final int DOWNLOAD = 2;
    public static final int UPLOAD = 1;
    private int mMediaType;
    private int mTransferType;

    public FailedToSyncTraktCollectionsException(Exception exc, int i, int i2) {
        super(exc);
        this.mTransferType = i;
        this.mMediaType = i2;
    }

    public FailedToSyncTraktCollectionsException(String str, int i, int i2) {
        super(str);
        this.mTransferType = i;
        this.mMediaType = i2;
    }

    public int getMediaType() {
        return this.mMediaType;
    }

    public int getTransferType() {
        return this.mTransferType;
    }

    public void setMediaType(int i) {
        this.mMediaType = i;
    }

    public void setTransferType(int i) {
        this.mTransferType = i;
    }

    public String toString() {
        return "FailedToSyncTraktCollectionsException{mTransferType=" + this.mTransferType + ", mMediaType=" + this.mMediaType + '}';
    }
}
