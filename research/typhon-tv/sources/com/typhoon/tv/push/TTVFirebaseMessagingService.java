package com.typhoon.tv.push;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;

public class TTVFirebaseMessagingService extends FirebaseMessagingService {
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.app.PendingIntent m16526(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            r5 = 0
            r4 = -1
            int r6 = r8.hashCode()
            switch(r6) {
                case 0: goto L_0x0035;
                case 517573928: goto L_0x002a;
                case 1027411513: goto L_0x001f;
                default: goto L_0x0009;
            }
        L_0x0009:
            switch(r4) {
                case 0: goto L_0x0040;
                case 1: goto L_0x0053;
                default: goto L_0x000c;
            }
        L_0x000c:
            android.content.Intent r3 = new android.content.Intent
            java.lang.Class<com.typhoon.tv.ui.activity.HomeActivity> r4 = com.typhoon.tv.ui.activity.HomeActivity.class
            r3.<init>(r7, r4)
        L_0x0013:
            r4 = 67108864(0x4000000, float:1.5046328E-36)
            r3.addFlags(r4)
            r4 = 1073741824(0x40000000, float:2.0)
            android.app.PendingIntent r4 = android.app.PendingIntent.getActivity(r7, r5, r3, r4)
            return r4
        L_0x001f:
            java.lang.String r6 = "openWebpage"
            boolean r6 = r8.equals(r6)
            if (r6 == 0) goto L_0x0009
            r4 = r5
            goto L_0x0009
        L_0x002a:
            java.lang.String r6 = "openMediaDetails"
            boolean r6 = r8.equals(r6)
            if (r6 == 0) goto L_0x0009
            r4 = 1
            goto L_0x0009
        L_0x0035:
            java.lang.String r6 = ""
            boolean r6 = r8.equals(r6)
            if (r6 == 0) goto L_0x0009
            r4 = 2
            goto L_0x0009
        L_0x0040:
            boolean r4 = r9.isEmpty()
            if (r4 != 0) goto L_0x0053
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r6 = android.net.Uri.parse(r9)
            r3.<init>(r4, r6)
            goto L_0x0013
        L_0x0053:
            boolean r4 = r9.isEmpty()
            if (r4 != 0) goto L_0x000c
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x008f }
            r4 = 0
            byte[] r4 = android.util.Base64.decode(r9, r4)     // Catch:{ Exception -> 0x008f }
            java.lang.String r6 = "UTF-8"
            r0.<init>(r4, r6)     // Catch:{ Exception -> 0x008f }
        L_0x0066:
            com.google.gson.Gson r4 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0081 }
            r4.<init>()     // Catch:{ Exception -> 0x0081 }
            java.lang.Class<com.typhoon.tv.model.media.MediaInfo> r6 = com.typhoon.tv.model.media.MediaInfo.class
            java.lang.Object r2 = r4.fromJson((java.lang.String) r0, r6)     // Catch:{ Exception -> 0x0081 }
            com.typhoon.tv.model.media.MediaInfo r2 = (com.typhoon.tv.model.media.MediaInfo) r2     // Catch:{ Exception -> 0x0081 }
            android.content.Intent r3 = new android.content.Intent     // Catch:{ Exception -> 0x0081 }
            java.lang.Class<com.typhoon.tv.ui.activity.MediaDetailsActivity> r4 = com.typhoon.tv.ui.activity.MediaDetailsActivity.class
            r3.<init>(r7, r4)     // Catch:{ Exception -> 0x0081 }
            java.lang.String r4 = "mediaInfo"
            r3.putExtra(r4, r2)     // Catch:{ Exception -> 0x0081 }
            goto L_0x0013
        L_0x0081:
            r1 = move-exception
            boolean[] r4 = new boolean[r5]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r4)
            android.content.Intent r3 = new android.content.Intent
            java.lang.Class<com.typhoon.tv.ui.activity.HomeActivity> r4 = com.typhoon.tv.ui.activity.HomeActivity.class
            r3.<init>(r7, r4)
            goto L_0x0013
        L_0x008f:
            r1 = move-exception
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0081 }
            r4 = 0
            byte[] r4 = android.util.Base64.decode(r9, r4)     // Catch:{ Exception -> 0x0081 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0081 }
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.push.TTVFirebaseMessagingService.m16526(java.lang.String, java.lang.String):android.app.PendingIntent");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16527(PendingIntent pendingIntent, String str, String str2, String str3) {
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        NotificationManagerCompat from = NotificationManagerCompat.from(this);
        NotificationCompat.Builder contentIntent = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)).setAutoCancel(true).setSound(defaultUri).setLights(ContextCompat.getColor(this, R.color.light_blue), 500, 5000).setPriority(1).setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= 21) {
            contentIntent.setSmallIcon(R.drawable.ic_movie_white_36dp);
        }
        if (!str.isEmpty()) {
            contentIntent = contentIntent.setContentTitle(str);
        }
        if (!str2.isEmpty()) {
            contentIntent = contentIntent.setContentText(str2);
        }
        if (!str3.isEmpty()) {
            try {
                Bitmap bitmap = (Bitmap) Glide.m3934(TVApplication.m6288()).m3974(str3).m25211().m25230(Integer.MIN_VALUE, Integer.MIN_VALUE).get();
                contentIntent = contentIntent.setLargeIcon(bitmap).setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        from.notify(0, contentIntent.build());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16528(RemoteMessage remoteMessage) {
        Map<String, String> r3 = remoteMessage.m13897();
        m16527(m16526(r3.containsKey("action") ? r3.get("action") : "", r3.containsKey("actionData") ? r3.get("actionData") : ""), r3.containsKey(PubnativeAsset.TITLE) ? r3.get(PubnativeAsset.TITLE) : "", r3.containsKey("message") ? r3.get("message") : "", r3.containsKey("imageUrl") ? r3.get("imageUrl") : "");
    }
}
