package com.typhoon.tv;

import android.support.v4.view.InputDeviceCompat;
import com.adincube.sdk.AdinCube;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.provider.movie.Movie25V2;
import com.typhoon.tv.provider.tv.Dizibox;
import com.typhoon.tv.provider.tv.WatchEpisodes;
import com.typhoon.tv.provider.universal.FliXanityBased;
import com.typhoon.tv.provider.universal.MiraDeTodo;
import com.typhoon.tv.provider.universal.Monster;
import com.typhoon.tv.provider.universal.ReleaseBB;
import com.typhoon.tv.provider.universal.Wfeed;
import com.typhoon.tv.provider.universal.directdlstream;
import com.typhoon.tv.provider.universal.maxrls;
import com.typhoon.tv.resolver.AbcVideo;
import com.typhoon.tv.resolver.ClipWatching;
import com.typhoon.tv.resolver.CloudHost;
import com.typhoon.tv.resolver.CloudVideo;
import com.typhoon.tv.resolver.DatafileHost;
import com.typhoon.tv.resolver.DoodWatch;
import com.typhoon.tv.resolver.DropAPK;
import com.typhoon.tv.resolver.EnterVideo;
import com.typhoon.tv.resolver.EuropeUP;
import com.typhoon.tv.resolver.FastPlay;
import com.typhoon.tv.resolver.FileCad;
import com.typhoon.tv.resolver.FileRio;
import com.typhoon.tv.resolver.FileUP;
import com.typhoon.tv.resolver.FilesIM;
import com.typhoon.tv.resolver.GamoVideo;
import com.typhoon.tv.resolver.GoUnlimited;
import com.typhoon.tv.resolver.HxLoad;
import com.typhoon.tv.resolver.JetLoad;
import com.typhoon.tv.resolver.LetsUpload;
import com.typhoon.tv.resolver.MegaUpload;
import com.typhoon.tv.resolver.MixDrop;
import com.typhoon.tv.resolver.MixLoads;
import com.typhoon.tv.resolver.MpFour;
import com.typhoon.tv.resolver.OK;
import com.typhoon.tv.resolver.OnlyStream;
import com.typhoon.tv.resolver.Openload;
import com.typhoon.tv.resolver.Putload;
import com.typhoon.tv.resolver.SamaUp;
import com.typhoon.tv.resolver.SendVid;
import com.typhoon.tv.resolver.SenditCloud;
import com.typhoon.tv.resolver.SevenUP;
import com.typhoon.tv.resolver.SolidFiles;
import com.typhoon.tv.resolver.StreamWire;
import com.typhoon.tv.resolver.SuperVideo;
import com.typhoon.tv.resolver.TheVideo;
import com.typhoon.tv.resolver.TusFiles;
import com.typhoon.tv.resolver.UpStream;
import com.typhoon.tv.resolver.UpToBox;
import com.typhoon.tv.resolver.UqLoad;
import com.typhoon.tv.resolver.UsersCloud;
import com.typhoon.tv.resolver.VK;
import com.typhoon.tv.resolver.VShareEU;
import com.typhoon.tv.resolver.VidDo;
import com.typhoon.tv.resolver.VidFast;
import com.typhoon.tv.resolver.VideYO;
import com.typhoon.tv.resolver.VideoBee;
import com.typhoon.tv.resolver.VideoBin;
import com.typhoon.tv.resolver.VidiaTV;
import com.typhoon.tv.resolver.Vidlox;
import com.typhoon.tv.resolver.Vidoza;
import com.typhoon.tv.resolver.ViduPlayer;
import com.typhoon.tv.resolver.Yandex;
import com.typhoon.tv.resolver.YoudBox;
import com.typhoon.tv.resolver.YourUpload;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.resolver.debrid.realdebrid.AlfaFile;
import com.typhoon.tv.resolver.debrid.realdebrid.AllTheRest;
import com.typhoon.tv.resolver.debrid.realdebrid.BdUpload;
import com.typhoon.tv.resolver.debrid.realdebrid.ClicknUpload;
import com.typhoon.tv.resolver.debrid.realdebrid.FileFactory;
import com.typhoon.tv.resolver.debrid.realdebrid.FlashX;
import com.typhoon.tv.resolver.debrid.realdebrid.KatFile;
import com.typhoon.tv.resolver.debrid.realdebrid.KeepShare;
import com.typhoon.tv.resolver.debrid.realdebrid.MediaFire;
import com.typhoon.tv.resolver.debrid.realdebrid.NitroFlare;
import com.typhoon.tv.resolver.debrid.realdebrid.Oboom;
import com.typhoon.tv.resolver.debrid.realdebrid.OneFichier;
import com.typhoon.tv.resolver.debrid.realdebrid.RapidGator;
import com.typhoon.tv.resolver.debrid.realdebrid.RgTo;
import com.typhoon.tv.resolver.debrid.realdebrid.RockFile;
import com.typhoon.tv.resolver.debrid.realdebrid.Rotate;
import com.typhoon.tv.resolver.debrid.realdebrid.TurboBit;
import com.typhoon.tv.resolver.debrid.realdebrid.Uploaded;
import com.typhoon.tv.resolver.debrid.realdebrid.Uploadev;
import com.typhoon.tv.resolver.debrid.realdebrid.WipFiles;
import com.typhoon.tv.resolver.debrid.realdebrid.WupFile;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.subtitles.chinese.Makedie;
import com.typhoon.tv.subtitles.chinese.SubHD;
import com.typhoon.tv.subtitles.international.OpenSubtitles;
import com.typhoon.tv.subtitles.international.Subscene;
import com.typhoon.tv.utils.Utils;
import io.michaelrocks.paranoid.Deobfuscator$app$Release;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class TyphoonApp {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final String f5828 = Deobfuscator$app$Release.m19454(158);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public static final String f5829 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/603.3.9 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.9";

    /* renamed from: ʻʼ  reason: contains not printable characters */
    public static final String f5830 = (f5907 + "/isNadMechEnabled");

    /* renamed from: ʻʽ  reason: contains not printable characters */
    public static final String f5831 = (f5907 + Deobfuscator$app$Release.m19454(264));

    /* renamed from: ʻʾ  reason: contains not printable characters */
    public static final String f5832 = (f5907 + String.format(Deobfuscator$app$Release.m19454(265), new Object[]{Integer.valueOf(Utils.m6393())}));

    /* renamed from: ʻʿ  reason: contains not printable characters */
    public static final String f5833 = (f5907 + Deobfuscator$app$Release.m19454(266));

    /* renamed from: ʻˆ  reason: contains not printable characters */
    public static final String f5834 = (f5907 + Deobfuscator$app$Release.m19454(267));

    /* renamed from: ʻˈ  reason: contains not printable characters */
    public static String f5835 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/70.0.3538.103 Safari/537.37";

    /* renamed from: ʻˉ  reason: contains not printable characters */
    public static String f5836 = "2f16c80c";

    /* renamed from: ʻˊ  reason: contains not printable characters */
    public static String f5837 = "e95809ea173d426a866c5182f824bcf2";

    /* renamed from: ʻˋ  reason: contains not printable characters */
    public static String f5838 = "6ddd9ec11f214639b5c964c849ef6d47";

    /* renamed from: ʻˎ  reason: contains not printable characters */
    public static String f5839 = "4a06f131c72c4313b9862ff2016b8980";

    /* renamed from: ʻˏ  reason: contains not printable characters */
    public static String f5840 = "X245A4XAIBGVM";

    /* renamed from: ʻˑ  reason: contains not printable characters */
    public static String f5841 = "OkHttp/3.11.0";

    /* renamed from: ʻי  reason: contains not printable characters */
    public static String f5842 = "";

    /* renamed from: ʻـ  reason: contains not printable characters */
    public static String f5843 = "";

    /* renamed from: ʻٴ  reason: contains not printable characters */
    public static int f5844 = 1;

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    public static boolean f5845 = false;

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    public static boolean f5846 = false;

    /* renamed from: ʻᵎ  reason: contains not printable characters */
    public static boolean f5847 = false;

    /* renamed from: ʻᵔ  reason: contains not printable characters */
    public static boolean f5848 = false;

    /* renamed from: ʻᵢ  reason: contains not printable characters */
    public static boolean f5849 = true;

    /* renamed from: ʻⁱ  reason: contains not printable characters */
    public static boolean f5850 = false;

    /* renamed from: ʻﹳ  reason: contains not printable characters */
    public static boolean f5851 = false;

    /* renamed from: ʻﹶ  reason: contains not printable characters */
    public static boolean f5852 = false;

    /* renamed from: ʻﾞ  reason: contains not printable characters */
    public static boolean f5853 = true;

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final String f5854 = Deobfuscator$app$Release.m19454(159);

    /* renamed from: ʼʻ  reason: contains not printable characters */
    public static boolean f5855 = false;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public static final String[] f5856 = {Deobfuscator$app$Release.m19454(188), Deobfuscator$app$Release.m19454(PsExtractor.PRIVATE_STREAM_1), Deobfuscator$app$Release.m19454(190), Deobfuscator$app$Release.m19454(191), Deobfuscator$app$Release.m19454(PsExtractor.AUDIO_STREAM), Deobfuscator$app$Release.m19454(193), Deobfuscator$app$Release.m19454(194), Deobfuscator$app$Release.m19454(195), Deobfuscator$app$Release.m19454(196), Deobfuscator$app$Release.m19454(197), Deobfuscator$app$Release.m19454(198), Deobfuscator$app$Release.m19454(199), Deobfuscator$app$Release.m19454(200), Deobfuscator$app$Release.m19454(201), Deobfuscator$app$Release.m19454(202), Deobfuscator$app$Release.m19454(203), Deobfuscator$app$Release.m19454(204), Deobfuscator$app$Release.m19454(205), Deobfuscator$app$Release.m19454(206), Deobfuscator$app$Release.m19454(207), Deobfuscator$app$Release.m19454(208), Deobfuscator$app$Release.m19454(209), Deobfuscator$app$Release.m19454(210), Deobfuscator$app$Release.m19454(211), Deobfuscator$app$Release.m19454(212), Deobfuscator$app$Release.m19454(213), Deobfuscator$app$Release.m19454(214), Deobfuscator$app$Release.m19454(215), Deobfuscator$app$Release.m19454(216), Deobfuscator$app$Release.m19454(217), Deobfuscator$app$Release.m19454(218), Deobfuscator$app$Release.m19454(219), Deobfuscator$app$Release.m19454(220), Deobfuscator$app$Release.m19454(221)};

    /* renamed from: ʼʽ  reason: contains not printable characters */
    public static boolean f5857 = false;

    /* renamed from: ʼʾ  reason: contains not printable characters */
    public static boolean f5858 = false;

    /* renamed from: ʼʿ  reason: contains not printable characters */
    public static boolean f5859 = true;

    /* renamed from: ʼˆ  reason: contains not printable characters */
    public static boolean f5860 = true;

    /* renamed from: ʼˈ  reason: contains not printable characters */
    public static boolean f5861 = true;

    /* renamed from: ʼˉ  reason: contains not printable characters */
    public static boolean f5862 = false;

    /* renamed from: ʼˊ  reason: contains not printable characters */
    public static boolean f5863 = false;

    /* renamed from: ʼˋ  reason: contains not printable characters */
    public static boolean f5864 = false;

    /* renamed from: ʼˎ  reason: contains not printable characters */
    public static boolean f5865 = true;

    /* renamed from: ʼˏ  reason: contains not printable characters */
    public static boolean f5866 = true;

    /* renamed from: ʼˑ  reason: contains not printable characters */
    public static boolean f5867 = true;

    /* renamed from: ʼי  reason: contains not printable characters */
    public static boolean f5868 = true;

    /* renamed from: ʼـ  reason: contains not printable characters */
    public static boolean f5869 = true;

    /* renamed from: ʼٴ  reason: contains not printable characters */
    public static boolean f5870 = true;

    /* renamed from: ʼᐧ  reason: contains not printable characters */
    public static boolean f5871 = true;

    /* renamed from: ʼᴵ  reason: contains not printable characters */
    public static boolean f5872 = true;

    /* renamed from: ʼᵎ  reason: contains not printable characters */
    public static boolean f5873 = true;

    /* renamed from: ʼᵔ  reason: contains not printable characters */
    public static int f5874 = -1;

    /* renamed from: ʼᵢ  reason: contains not printable characters */
    public static boolean f5875 = true;

    /* renamed from: ʼⁱ  reason: contains not printable characters */
    private static final String[] f5876 = {"61.0.3163.79", "61.0.3163.100", "64.0.3282.140", "64.0.3282.167", "65.0.3325.162", "65.0.3325.181", "66.0.3359.181"};

    /* renamed from: ʼﹳ  reason: contains not printable characters */
    private static final String[] f5877 = {"55", "56", "58", "59", "60"};

    /* renamed from: ʼﹶ  reason: contains not printable characters */
    private static final String[] f5878 = {"Windows NT 6.1", "Windows NT 6.2", "Windows NT 6.3", "Windows NT 7.0", "Windows NT 10.0"};

    /* renamed from: ʼﾞ  reason: contains not printable characters */
    private static final String[] f5879 = {"; WOW64", "; Win64; IA64", "; Win64; x64", ""};

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final String f5880 = Deobfuscator$app$Release.m19454(160);

    /* renamed from: ʽʻ  reason: contains not printable characters */
    private static final String[] f5881 = {Deobfuscator$app$Release.m19454(243), Deobfuscator$app$Release.m19454(244)};

    /* renamed from: ʽʼ  reason: contains not printable characters */
    private static int f5882 = 1;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public static final String f5883 = "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/64.0.3282.187 YaBrowser/18.3.1.1233 Yowser/2.5 Safari/537.37";

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final String f5884 = Deobfuscator$app$Release.m19454(165);

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public static List<String> f5885;

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final String f5886 = Deobfuscator$app$Release.m19454(166);

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public static String f5887 = Deobfuscator$app$Release.m19454(245);

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final String f5888 = Deobfuscator$app$Release.m19454(172);

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public static final String f5889 = (f5907 + Deobfuscator$app$Release.m19454(247));

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final String f5890 = Deobfuscator$app$Release.m19454(164);

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public static final String f5891 = (f5907 + Deobfuscator$app$Release.m19454(249));

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final String f5892 = Deobfuscator$app$Release.m19454(173);

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public static final String f5893 = (f5907 + Deobfuscator$app$Release.m19454(248));

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final String f5894 = Deobfuscator$app$Release.m19454(169);

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public static final String f5895 = (f5907 + Deobfuscator$app$Release.m19454(251));

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final String f5896 = Deobfuscator$app$Release.m19454(170);

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public static final String f5897 = (f5907 + Deobfuscator$app$Release.m19454(250));

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final String f5898 = Deobfuscator$app$Release.m19454(171);

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public static final String f5899 = (f5907 + Deobfuscator$app$Release.m19454(253));

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final String f5900 = Deobfuscator$app$Release.m19454(174);

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public static final String f5901 = (f5907 + Deobfuscator$app$Release.m19454(252));

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final String f5902 = Deobfuscator$app$Release.m19454(161);

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public static final String f5903 = (f5907 + Deobfuscator$app$Release.m19454(254));

    /* renamed from: י  reason: contains not printable characters */
    public static final String f5904 = Deobfuscator$app$Release.m19454(175);

    /* renamed from: יי  reason: contains not printable characters */
    public static final String f5905 = (f5907 + Deobfuscator$app$Release.m19454(256));

    /* renamed from: ـ  reason: contains not printable characters */
    public static final String f5906 = Deobfuscator$app$Release.m19454(176);

    /* renamed from: ــ  reason: contains not printable characters */
    public static String f5907 = "https://fastbrothers.ml";

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final String f5908 = Deobfuscator$app$Release.m19454(162);

    /* renamed from: ٴٴ  reason: contains not printable characters */
    public static final String f5909 = (f5907 + Deobfuscator$app$Release.m19454(261));

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final String f5910 = Deobfuscator$app$Release.m19454(163);

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public static final String f5911 = Deobfuscator$app$Release.m19454(184);

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static final String f5912 = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public static final String f5913 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/58.0.3029.117 Safari/537.37";

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static final String f5914 = "Mozilla/5.0 AppleWebKit/537.37 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.37";

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public static final String f5915 = (f5907 + Deobfuscator$app$Release.m19454(InputDeviceCompat.SOURCE_KEYBOARD));

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static final String f5916 = Deobfuscator$app$Release.m19454(179);

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public static final String f5917 = (f5907 + Deobfuscator$app$Release.m19454(255));

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static final String f5918 = Deobfuscator$app$Release.m19454(180);

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    public static final String f5919 = (f5907 + Deobfuscator$app$Release.m19454(258));

    /* renamed from: ⁱ  reason: contains not printable characters */
    public static final String f5920 = Deobfuscator$app$Release.m19454(181);

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    public static final String f5921 = (f5907 + Deobfuscator$app$Release.m19454(259));

    /* renamed from: 连任  reason: contains not printable characters */
    public static final String f5922 = Deobfuscator$app$Release.m19454(157);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String f5923 = Deobfuscator$app$Release.m19454(154);

    /* renamed from: 麤  reason: contains not printable characters */
    public static final String f5924 = Deobfuscator$app$Release.m19454(156);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final String f5925 = Deobfuscator$app$Release.m19454(155);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f5926 = Deobfuscator$app$Release.m19454(153);

    /* renamed from: ﹳ  reason: contains not printable characters */
    public static final String f5927 = Deobfuscator$app$Release.m19454(182);

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    public static final String f5928 = (f5907 + Deobfuscator$app$Release.m19454(260));

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static final String f5929 = Deobfuscator$app$Release.m19454(167);

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    public static final String f5930 = (f5907 + Deobfuscator$app$Release.m19454(262));

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final String f5931 = Deobfuscator$app$Release.m19454(168);

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public static final String f5932 = Deobfuscator$app$Release.m19454(183);

    /* renamed from: ʻ  reason: contains not printable characters */
    public static ArrayList<String> m6323() {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 2021; i >= 1850; i--) {
            arrayList.add(String.valueOf(i));
        }
        return arrayList;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Map<String, Object> m6324() {
        Map<String, Object> map = new HashMap<>();
        map.put(Deobfuscator$app$Release.m19454(20), f5907);
        return map;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static HashMap<String, String> m6325() {
        HashMap<String, String> map = new HashMap<>();
        map.put(Deobfuscator$app$Release.m19454(21), Deobfuscator$app$Release.m19454(22));
        return map;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static BaseResolver[] m6326() {
        return new BaseResolver[]{new VideoBin(), new Yandex(), new TusFiles(), new LetsUpload(), new SenditCloud(), new YoudBox(), new OK(), new VK(), new FileCad(), new DatafileHost(), new TheVideo(), new CloudHost(), new UpToBox(), new SolidFiles(), new JetLoad(), new OnlyStream(), new FastPlay(), new VidiaTV(), new SevenUP(), new Putload(), new Vidoza(), new MixDrop(), new Openload(), new ClipWatching(), new CloudVideo(), new MpFour(), new DoodWatch(), new VideYO(), new FilesIM(), new StreamWire(), new UqLoad(), new SamaUp(), new AbcVideo(), new VShareEU(), new UpStream(), new VidFast(), new MegaUpload(), new FileUP(), new GoUnlimited(), new ViduPlayer(), new Vidlox(), new EnterVideo(), new MixLoads(), new UsersCloud(), new GamoVideo(), new HxLoad(), new SendVid(), new VideoBee(), new VidDo(), new FileRio(), new YourUpload(), new SuperVideo(), new DropAPK(), new EuropeUP(), new Uploaded(), new AllTheRest(), new RapidGator(), new ClicknUpload(), new Oboom(), new NitroFlare(), new FileFactory(), new AlfaFile(), new OneFichier(), new TurboBit(), new RockFile(), new FlashX(), new MediaFire(), new WupFile(), new WipFiles(), new Rotate(), new RgTo(), new KatFile(), new Uploadev(), new BdUpload(), new KeepShare()};
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static BaseSubtitlesService[] m6327() {
        return new BaseSubtitlesService[]{new OpenSubtitles(), new Subscene(), new Makedie()};
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static BaseProvider[] m6328() {
        return new BaseProvider[]{new FliXanityBased(), new Wfeed(), new MiraDeTodo(), new Movie25V2(), new ReleaseBB(), new Monster(), new directdlstream(), new maxrls()};
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static TreeMap<String, String> m6329() {
        TreeMap<String, String> treeMap = new TreeMap<>();
        treeMap.put("1-Typhoon TV Developer", "App Developer;Creator");
        treeMap.put("Typhoon TV APK Download Location", "https://typhoontv.ml/app-release.apk");
        treeMap.put("2-Dark❶", "Web Developer");
        treeMap.put("3-The Tesla Builds", "Sponsor;Server and Web Development");
        treeMap.put("Facebook Group", "https://www.facebook.com/groups/typhooonGR/");
        treeMap.put("FILELINKED DIRECT DOWNLOAD LINK!", "https://bit.ly/filelinkedtyp");
        treeMap.put("Typhoon TV OFFICIAL Filelinked Download Code", "CODE: 29021883 PIN: 1441");
        treeMap.put("Typhoon TV Sponsor", "http://bit.ly/cordcuttersbyTKB");
        treeMap.put("Thanks my team who works on all types of networks!", "All my guys on FB and Discord who work hard and help you in service big thanks to them!");
        return treeMap;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static LinkedHashMap<String, String> m6330() {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put(Deobfuscator$app$Release.m19454(87), Deobfuscator$app$Release.m19454(88));
        map.put(Deobfuscator$app$Release.m19454(89), Deobfuscator$app$Release.m19454(90));
        map.put(Deobfuscator$app$Release.m19454(91), Deobfuscator$app$Release.m19454(92));
        map.put(Deobfuscator$app$Release.m19454(93), Deobfuscator$app$Release.m19454(94));
        map.put(Deobfuscator$app$Release.m19454(95), Deobfuscator$app$Release.m19454(96));
        map.put(Deobfuscator$app$Release.m19454(97), Deobfuscator$app$Release.m19454(98));
        map.put(Deobfuscator$app$Release.m19454(99), Deobfuscator$app$Release.m19454(100));
        map.put(Deobfuscator$app$Release.m19454(101), Deobfuscator$app$Release.m19454(102));
        map.put(Deobfuscator$app$Release.m19454(103), Deobfuscator$app$Release.m19454(104));
        map.put(Deobfuscator$app$Release.m19454(105), Deobfuscator$app$Release.m19454(106));
        map.put(Deobfuscator$app$Release.m19454(107), Deobfuscator$app$Release.m19454(108));
        map.put(Deobfuscator$app$Release.m19454(109), Deobfuscator$app$Release.m19454(110));
        map.put(Deobfuscator$app$Release.m19454(111), Deobfuscator$app$Release.m19454(112));
        map.put(Deobfuscator$app$Release.m19454(113), Deobfuscator$app$Release.m19454(114));
        map.put(Deobfuscator$app$Release.m19454(115), Deobfuscator$app$Release.m19454(116));
        map.put(Deobfuscator$app$Release.m19454(117), Deobfuscator$app$Release.m19454(118));
        map.put(Deobfuscator$app$Release.m19454(119), Deobfuscator$app$Release.m19454(120));
        map.put(Deobfuscator$app$Release.m19454(121), Deobfuscator$app$Release.m19454(122));
        map.put(Deobfuscator$app$Release.m19454(123), Deobfuscator$app$Release.m19454(124));
        map.put(Deobfuscator$app$Release.m19454(125), Deobfuscator$app$Release.m19454(126));
        map.put(Deobfuscator$app$Release.m19454(127), Deobfuscator$app$Release.m19454(128));
        map.put(Deobfuscator$app$Release.m19454(129), Deobfuscator$app$Release.m19454(130));
        map.put(Deobfuscator$app$Release.m19454(131), Deobfuscator$app$Release.m19454(132));
        map.put(Deobfuscator$app$Release.m19454(133), Deobfuscator$app$Release.m19454(TsExtractor.TS_STREAM_TYPE_SPLICE_INFO));
        map.put(Deobfuscator$app$Release.m19454(TsExtractor.TS_STREAM_TYPE_E_AC3), Deobfuscator$app$Release.m19454(136));
        map.put(Deobfuscator$app$Release.m19454(137), Deobfuscator$app$Release.m19454(TsExtractor.TS_STREAM_TYPE_DTS));
        map.put(Deobfuscator$app$Release.m19454(139), Deobfuscator$app$Release.m19454(140));
        map.put(Deobfuscator$app$Release.m19454(141), Deobfuscator$app$Release.m19454(142));
        map.put(Deobfuscator$app$Release.m19454(143), Deobfuscator$app$Release.m19454(144));
        map.put(Deobfuscator$app$Release.m19454(145), Deobfuscator$app$Release.m19454(146));
        map.put(Deobfuscator$app$Release.m19454(147), Deobfuscator$app$Release.m19454(148));
        map.put(Deobfuscator$app$Release.m19454(149), Deobfuscator$app$Release.m19454(150));
        map.put(Deobfuscator$app$Release.m19454(151), Deobfuscator$app$Release.m19454(152));
        return map;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static BaseProvider[] m6331() {
        return new BaseProvider[]{new FliXanityBased(), new MiraDeTodo(), new Wfeed(), new WatchEpisodes(), new Dizibox(), new ReleaseBB(), new Monster(), new directdlstream(), new maxrls()};
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static void m6332() {
        List<String> uaList = m6335();
        f5835 = uaList.get(new Random().nextInt(uaList.size()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized boolean m6333() {
        boolean z = false;
        synchronized (TyphoonApp.class) {
            if (f5882 > 4) {
                f5882 = 0;
                z = true;
            } else {
                f5882++;
            }
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static List<String> m6334() {
        List<String> uaList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            try {
                int index = new Random().nextInt(f5881.length);
                uaList.add(f5881[index].replace(Deobfuscator$app$Release.m19454(17), f5878[new Random().nextInt(f5878.length)]).replace(Deobfuscator$app$Release.m19454(18), f5879[new Random().nextInt(f5879.length)]).replace(Deobfuscator$app$Release.m19454(19), index == 0 ? f5877[new Random().nextInt(f5877.length)] : f5876[new Random().nextInt(f5876.length)]));
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return !uaList.isEmpty() ? Utils.m6415(uaList) : uaList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static List<String> m6335() {
        List<String> uaList = new ArrayList<>();
        uaList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.37");
        uaList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.37");
        uaList.add("Mozilla/5.0 (X11; U; Linux Core i7-4980HQ; de; rv:32.0; compatible; JobboerseBot; https://www.jobboerse.com/bot.htm) Gecko/20100101 Firefox/38.0");
        uaList.add("MovieTrakt/2.2.3 (Linux;Android 6.0.1) ExoPlayerLib/2.7.2");
        uaList.add("Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)");
        uaList.add("facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)");
        uaList.add("Feedfetcher-Google; (+http://www.google.com/feedfetcher.html; 3 subscribers; feed-id=17583705103843181935)");
        uaList.add("Mozilla/5.0 (compatible; DuckDuckGo-Favicons-Bot/1.0; +http://duckduckgo.com)");
        uaList.add("AdsBot-Google (+http://www.google.com/adsbot.html)");
        uaList.add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.37; 360Spider");
        uaList.add("Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/58.0.3029.111 YaBrowser/17.6.1.750 Yowser/2.5 Safari/537.37");
        uaList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/73.0.3683.87 Safari/537.37");
        uaList.add("Mozilla/5.0 (Unknown; Linux) AppleWebKit/538.2 (KHTML, like Gecko) Chrome/v1.0.0 Safari/538.2");
        uaList.add("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/70.0.3538.68 Safari/537.37");
        uaList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.3 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.3");
        return uaList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6336() {
        if (!f5873) {
            try {
                String nextNetworkFromAdinCube = AdinCube.Interstitial.m2306();
                if (nextNetworkFromAdinCube != null && !nextNetworkFromAdinCube.isEmpty()) {
                    String nextNetworkFromAdinCube2 = nextNetworkFromAdinCube.trim().toLowerCase();
                    if (nextNetworkFromAdinCube2.contains(Deobfuscator$app$Release.m19454(0)) || nextNetworkFromAdinCube2.contains(Deobfuscator$app$Release.m19454(1))) {
                        f5873 = true;
                    }
                }
            } catch (Throwable throwable) {
                Logger.m6281(throwable, true);
            }
        }
        return f5873;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static BaseSubtitlesService[] m6337() {
        return new BaseSubtitlesService[]{new Makedie(), new SubHD(), new OpenSubtitles(), new Subscene()};
    }
}
