package com.typhoon.tv;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.UpdateInfo;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import rx.Observable;
import rx.Subscriber;

public class UpdateService {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f12497 = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/Typhoon/Updates");

    /* renamed from: 靐  reason: contains not printable characters */
    public static Observable<Boolean> m15713() {
        return Observable.m7359(new Observable.OnSubscribe<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super Boolean> subscriber) {
                JsonElement jStatusEle;
                String versionCode = UpdateService.m15714();
                if (versionCode == null) {
                    subscriber.onCompleted();
                    return;
                }
                try {
                    JsonElement jEle = new JsonParser().parse(HttpHelper.m6343().m6351(TyphoonApp.f5907 + "/isForceUpdateNeeded" + versionCode.trim(), (Map<String, String>[]) new Map[0]));
                    if (jEle != null && jEle.isJsonObject() && (jStatusEle = jEle.getAsJsonObject().get(NotificationCompat.CATEGORY_STATUS)) != null && !jStatusEle.isJsonNull()) {
                        subscriber.onNext(Boolean.valueOf(jStatusEle.getAsBoolean()));
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m15716(String oldVersionName, String newVersionName) {
        int res = 0;
        String[] oldNumbers = oldVersionName.split("\\.");
        String[] newNumbers = newVersionName.split("\\.");
        int maxIndex = Math.min(oldNumbers.length, newNumbers.length);
        int i = 0;
        while (true) {
            if (i >= maxIndex) {
                break;
            }
            int oldVersionPart = Integer.valueOf(oldNumbers[i]).intValue();
            int newVersionPart = Integer.valueOf(newNumbers[i]).intValue();
            if (oldVersionPart < newVersionPart) {
                res = -1;
                break;
            } else if (oldVersionPart > newVersionPart) {
                res = 1;
                break;
            } else {
                i++;
            }
        }
        return (res != 0 || oldNumbers.length == newNumbers.length) ? res : oldNumbers.length > newNumbers.length ? 1 : -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static UpdateInfo m15717() {
        String versionCode = m15714();
        if (versionCode == null) {
            return null;
        }
        String resRawJson = HttpHelper.m6343().m6351(Locale.getDefault().getLanguage().startsWith("zh") ? TyphoonApp.f5895 : TyphoonApp.f5901, (Map<String, String>[]) new Map[0]);
        if (resRawJson.isEmpty() || !resRawJson.contains("newestVersion")) {
            return null;
        }
        try {
            JsonObject versionObj = new JsonParser().parse(resRawJson).getAsJsonObject();
            String newestVersion = versionObj.get("newestVersion").getAsString();
            if (m15716(versionCode, newestVersion) == 0) {
                return null;
            }
            Iterator<JsonElement> it2 = versionObj.get("versions").getAsJsonArray().iterator();
            while (it2.hasNext()) {
                try {
                    JsonObject verObj = it2.next().getAsJsonObject();
                    if (verObj.get("version").getAsString().equals(newestVersion)) {
                        UpdateInfo updateInfo = new UpdateInfo(newestVersion, verObj.get("filename").getAsString(), verObj.get(PubnativeAsset.DESCRIPTION).getAsString());
                        try {
                            JsonElement jUpdateDisabledEle = verObj.get("updateDisabled");
                            if (jUpdateDisabledEle == null || jUpdateDisabledEle.isJsonNull() || !jUpdateDisabledEle.getAsBoolean()) {
                                return updateInfo;
                            }
                            updateInfo.setUpdateDisabled(true);
                            return updateInfo;
                        } catch (Throwable e) {
                            Logger.m6281(e, new boolean[0]);
                            return updateInfo;
                        }
                    } else {
                        continue;
                    }
                } catch (Exception e2) {
                    Logger.m6281(e2, new boolean[0]);
                }
            }
            return null;
        } catch (Exception e3) {
            Logger.m6281((Throwable) e3, new boolean[0]);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<Integer> m15718(final UpdateInfo info) {
        final String updateUrl = TyphoonApp.f5907 + "/ttv/apk/" + info.getVersion() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + info.getFileName() + "/download";
        return Observable.m7359(new Observable.OnSubscribe<Integer>() {
            /* JADX WARNING: Removed duplicated region for block: B:112:0x0288 A[Catch:{ Exception -> 0x0128 }] */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00fe A[SYNTHETIC, Splitter:B:20:0x00fe] */
            /* JADX WARNING: Removed duplicated region for block: B:71:0x01a5 A[SYNTHETIC, Splitter:B:71:0x01a5] */
            /* JADX WARNING: Removed duplicated region for block: B:74:0x01aa A[SYNTHETIC, Splitter:B:74:0x01aa] */
            /* JADX WARNING: Removed duplicated region for block: B:77:0x01af A[SYNTHETIC, Splitter:B:77:0x01af] */
            /* JADX WARNING: Removed duplicated region for block: B:80:0x01b4 A[SYNTHETIC, Splitter:B:80:0x01b4] */
            /* JADX WARNING: Removed duplicated region for block: B:83:0x01b9 A[SYNTHETIC, Splitter:B:83:0x01b9] */
            /* renamed from: 龘  reason: contains not printable characters */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void call(rx.Subscriber<? super java.lang.Integer> r28) {
                /*
                    r27 = this;
                    java.io.File r16 = new java.io.File     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r21 = com.typhoon.tv.UpdateService.f12497     // Catch:{ Exception -> 0x0128 }
                    r0 = r16
                    r1 = r21
                    r0.<init>(r1)     // Catch:{ Exception -> 0x0128 }
                    boolean r21 = r16.exists()     // Catch:{ Exception -> 0x0128 }
                    if (r21 != 0) goto L_0x0117
                    r16.mkdirs()     // Catch:{ Exception -> 0x0128 }
                L_0x0014:
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128 }
                    r21.<init>()     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r24 = com.typhoon.tv.UpdateService.f12497     // Catch:{ Exception -> 0x0128 }
                    r0 = r21
                    r1 = r24
                    java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r24 = "/"
                    r0 = r21
                    r1 = r24
                    java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ Exception -> 0x0128 }
                    r0 = r27
                    com.typhoon.tv.model.UpdateInfo r0 = r3     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    java.lang.String r24 = r24.getFileName()     // Catch:{ Exception -> 0x0128 }
                    r0 = r21
                    r1 = r24
                    java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r24 = ".apk"
                    r0 = r21
                    r1 = r24
                    java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r15 = r21.toString()     // Catch:{ Exception -> 0x0128 }
                    java.io.File r14 = new java.io.File     // Catch:{ Exception -> 0x0128 }
                    r14.<init>(r15)     // Catch:{ Exception -> 0x0128 }
                    boolean r21 = r14.exists()     // Catch:{ Exception -> 0x0128 }
                    if (r21 == 0) goto L_0x005d
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r14)     // Catch:{ Exception -> 0x0128 }
                L_0x005d:
                    com.typhoon.tv.helper.http.HttpHelper r21 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Exception -> 0x0128 }
                    r0 = r27
                    java.lang.String r0 = r0     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    r25 = 0
                    r26 = 0
                    r0 = r26
                    java.util.Map[] r0 = new java.util.Map[r0]     // Catch:{ Exception -> 0x0128 }
                    r26 = r0
                    r0 = r21
                    r1 = r24
                    r2 = r25
                    r3 = r26
                    java.lang.String r11 = r0.m6363((java.lang.String) r1, (boolean) r2, (java.util.Map<java.lang.String, java.lang.String>[]) r3)     // Catch:{ Exception -> 0x0128 }
                    okhttp3.Request$Builder r21 = new okhttp3.Request$Builder     // Catch:{ Exception -> 0x0128 }
                    r21.<init>()     // Catch:{ Exception -> 0x0128 }
                    okhttp3.Request$Builder r21 = r21.m19990()     // Catch:{ Exception -> 0x0128 }
                    r0 = r21
                    okhttp3.Request$Builder r21 = r0.m19992((java.lang.String) r11)     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r24 = "User-Agent"
                    java.lang.String r25 = com.typhoon.tv.TyphoonApp.f5835     // Catch:{ Exception -> 0x0128 }
                    r0 = r21
                    r1 = r24
                    r2 = r25
                    okhttp3.Request$Builder r21 = r0.m19993((java.lang.String) r1, (java.lang.String) r2)     // Catch:{ Exception -> 0x0128 }
                    okhttp3.CacheControl r24 = okhttp3.CacheControl.f6219     // Catch:{ Exception -> 0x0128 }
                    r0 = r21
                    r1 = r24
                    okhttp3.Request$Builder r21 = r0.m19995((okhttp3.CacheControl) r1)     // Catch:{ Exception -> 0x0128 }
                    java.lang.String r24 = "downloadUpdate"
                    r0 = r21
                    r1 = r24
                    okhttp3.Request$Builder r21 = r0.m19991((java.lang.Object) r1)     // Catch:{ Exception -> 0x0128 }
                    okhttp3.Request r18 = r21.m19989()     // Catch:{ Exception -> 0x0128 }
                    com.typhoon.tv.helper.http.HttpHelper r21 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Exception -> 0x0128 }
                    r24 = 0
                    r0 = r24
                    int[] r0 = new int[r0]     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    r0 = r21
                    r1 = r18
                    r2 = r24
                    okhttp3.Response r19 = r0.m6368((okhttp3.Request) r1, (int[]) r2)     // Catch:{ Exception -> 0x0128 }
                    okhttp3.ResponseBody r20 = r19.m7056()     // Catch:{ Exception -> 0x0128 }
                    r10 = 0
                    r12 = 0
                    r9 = 0
                    java.io.InputStream r10 = r20.m7094()     // Catch:{ Exception -> 0x0290 }
                    java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0290 }
                    r13.<init>(r14)     // Catch:{ Exception -> 0x0290 }
                    long r4 = r20.m7093()     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    r24 = 0
                    int r21 = (r4 > r24 ? 1 : (r4 == r24 ? 0 : -1))
                    if (r21 <= 0) goto L_0x0194
                    r21 = 1024(0x400, float:1.435E-42)
                    r0 = r21
                    byte[] r7 = new byte[r0]     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    r22 = 0
                L_0x00ec:
                    int r6 = r10.read(r7)     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    r21 = -1
                    r0 = r21
                    if (r6 == r0) goto L_0x00fc
                    boolean r21 = r28.isUnsubscribed()     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    if (r21 == 0) goto L_0x0142
                L_0x00fc:
                    if (r20 == 0) goto L_0x0101
                    r20.close()     // Catch:{ Exception -> 0x01bd }
                L_0x0101:
                    if (r13 == 0) goto L_0x0106
                    r13.flush()     // Catch:{ Exception -> 0x01cd }
                L_0x0106:
                    if (r13 == 0) goto L_0x010b
                    r13.close()     // Catch:{ Exception -> 0x01dd }
                L_0x010b:
                    if (r10 == 0) goto L_0x0110
                    r10.close()     // Catch:{ Exception -> 0x01ed }
                L_0x0110:
                    if (r9 == 0) goto L_0x01fd
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r14)     // Catch:{ Exception -> 0x0128 }
                    r12 = r13
                L_0x0116:
                    return
                L_0x0117:
                    boolean r21 = r16.isDirectory()     // Catch:{ Exception -> 0x0128 }
                    if (r21 != 0) goto L_0x0014
                    boolean r21 = r16.delete()     // Catch:{ Exception -> 0x0128 }
                    if (r21 == 0) goto L_0x0014
                    r16.mkdirs()     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0014
                L_0x0128:
                    r8 = move-exception
                    r21 = 1
                    r0 = r21
                    boolean[] r0 = new boolean[r0]
                    r21 = r0
                    r24 = 0
                    r25 = 1
                    r21[r24] = r25
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)
                    r0 = r28
                    r0.onError(r8)
                    goto L_0x0116
                L_0x0142:
                    long r0 = (long) r6
                    r24 = r0
                    long r22 = r22 + r24
                    r21 = 0
                    r0 = r21
                    r13.write(r7, r0, r6)     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    r24 = 100
                    long r24 = r24 * r22
                    long r24 = r24 / r4
                    r0 = r24
                    int r0 = (int) r0     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    r17 = r0
                    java.lang.Integer r21 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    r0 = r28
                    r1 = r21
                    r0.onNext(r1)     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    goto L_0x00ec
                L_0x0165:
                    r8 = move-exception
                    r12 = r13
                L_0x0167:
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ all -> 0x028d }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ all -> 0x028d }
                    r0 = r28
                    r0.onError(r8)     // Catch:{ all -> 0x028d }
                    r9 = 1
                    if (r20 == 0) goto L_0x017f
                    r20.close()     // Catch:{ Exception -> 0x0203 }
                L_0x017f:
                    if (r12 == 0) goto L_0x0184
                    r12.flush()     // Catch:{ Exception -> 0x0213 }
                L_0x0184:
                    if (r12 == 0) goto L_0x0189
                    r12.close()     // Catch:{ Exception -> 0x0223 }
                L_0x0189:
                    if (r10 == 0) goto L_0x018e
                    r10.close()     // Catch:{ Exception -> 0x0233 }
                L_0x018e:
                    if (r9 == 0) goto L_0x0243
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r14)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0116
                L_0x0194:
                    java.lang.RuntimeException r21 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    java.lang.String r24 = "Content-Length is or less than 0"
                    r0 = r21
                    r1 = r24
                    r0.<init>(r1)     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                    throw r21     // Catch:{ Exception -> 0x0165, all -> 0x01a1 }
                L_0x01a1:
                    r21 = move-exception
                    r12 = r13
                L_0x01a3:
                    if (r20 == 0) goto L_0x01a8
                    r20.close()     // Catch:{ Exception -> 0x0248 }
                L_0x01a8:
                    if (r12 == 0) goto L_0x01ad
                    r12.flush()     // Catch:{ Exception -> 0x0258 }
                L_0x01ad:
                    if (r12 == 0) goto L_0x01b2
                    r12.close()     // Catch:{ Exception -> 0x0268 }
                L_0x01b2:
                    if (r10 == 0) goto L_0x01b7
                    r10.close()     // Catch:{ Exception -> 0x0278 }
                L_0x01b7:
                    if (r9 == 0) goto L_0x0288
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r14)     // Catch:{ Exception -> 0x0128 }
                L_0x01bc:
                    throw r21     // Catch:{ Exception -> 0x0128 }
                L_0x01bd:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0101
                L_0x01cd:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0106
                L_0x01dd:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x010b
                L_0x01ed:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0110
                L_0x01fd:
                    r28.onCompleted()     // Catch:{ Exception -> 0x0128 }
                    r12 = r13
                    goto L_0x0116
                L_0x0203:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x017f
                L_0x0213:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0184
                L_0x0223:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0189
                L_0x0233:
                    r8 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x018e
                L_0x0243:
                    r28.onCompleted()     // Catch:{ Exception -> 0x0128 }
                    goto L_0x0116
                L_0x0248:
                    r8 = move-exception
                    r24 = 0
                    r0 = r24
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    r0 = r24
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x01a8
                L_0x0258:
                    r8 = move-exception
                    r24 = 0
                    r0 = r24
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    r0 = r24
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x01ad
                L_0x0268:
                    r8 = move-exception
                    r24 = 0
                    r0 = r24
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    r0 = r24
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x01b2
                L_0x0278:
                    r8 = move-exception
                    r24 = 0
                    r0 = r24
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0128 }
                    r24 = r0
                    r0 = r24
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)     // Catch:{ Exception -> 0x0128 }
                    goto L_0x01b7
                L_0x0288:
                    r28.onCompleted()     // Catch:{ Exception -> 0x0128 }
                    goto L_0x01bc
                L_0x028d:
                    r21 = move-exception
                    goto L_0x01a3
                L_0x0290:
                    r8 = move-exception
                    goto L_0x0167
                */
                throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.UpdateService.AnonymousClass1.call(rx.Subscriber):void");
            }
        });
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static String m15714() {
        try {
            PackageInfo packageInfo = TVApplication.m6288().getPackageManager().getPackageInfo(TVApplication.m6288().getPackageName(), 0);
            if (packageInfo == null) {
                return null;
            }
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Logger.m6281((Throwable) e, true);
            return null;
        }
    }
}
