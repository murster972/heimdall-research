package com.typhoon.tv;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.support.v4.os.EnvironmentCompat;
import android.widget.Toast;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.config.CaocConfig;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.mediation.chartboost.ChartboostActivity;
import com.bumptech.glide.Glide;
import com.chartboost.sdk.CBImpressionActivity;
import com.chartboost.sdk.c;
import com.chartboost.sdk.h;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobManager;
import com.evernote.android.state.StateSaver;
import com.getkeepsafe.relinker.ReLinker;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.security.ProviderInstaller;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.joanzapata.iconify.Iconify;
import com.livefront.bridge.Bridge;
import com.livefront.bridge.SavedStateHandler;
import com.mopub.common.CreativeOrientation;
import com.orm.SugarContext;
import com.typhoon.tv.font.fontawesome.FontAwesomeModule47;
import com.typhoon.tv.jobs.TTVJobCreator;
import com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.GmsUtils;
import com.typhoon.tv.utils.Utils;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.util.HashMap;
import java.util.List;
import net.danlew.android.joda.JodaTimeAndroid;
import org.apache.commons.lang3.StringUtils;
import xiaofei.library.hermeseventbus.HermesEventBus;

public class TVApplication extends MultiDexApplication {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile TVDatabase f5820;

    /* renamed from: 麤  reason: contains not printable characters */
    private static AudioManager f5821;

    /* renamed from: 齉  reason: contains not printable characters */
    private static HashMap<String, Typeface> f5822;
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: 龘  reason: contains not printable characters */
    public static Context f5823;

    /* renamed from: 龘  reason: contains not printable characters */
    public static Context m6288() {
        return f5823;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static SharedPreferences m6285() {
        return PreferenceManager.getDefaultSharedPreferences(f5823);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static TVDatabase m6287() {
        if (f5820 == null) {
            synchronized (TVApplication.class) {
                if (f5820 == null) {
                    f5820 = new TVDatabase();
                }
            }
        }
        return f5820;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Typeface m6289(String key) {
        if (!f5822.containsKey(key)) {
            f5822.put(key, Typeface.createFromAsset(f5823.getAssets(), key));
        }
        return f5822.get(key);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static AudioManager m6286() {
        return f5821;
    }

    public void onCreate() {
        super.onCreate();
        f5823 = getApplicationContext();
        f5822 = new HashMap<>();
        f5821 = (AudioManager) m6288().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        CaocConfig.Builder.m2291().m2298(true).m2295(1).m2292(true).m2294(true).m2297(Integer.valueOf(R.drawable.ic_ttv_black_white)).m2296((CustomActivityOnCrash.EventListener) new CustomCrashActivityListener()).m2293();
        Iconify.with(new FontAwesomeModule47());
        JodaTimeAndroid.m6827(f5823);
        SugarContext.m6268(f5823);
        TyphoonApp.m6332();
        Bridge.initialize(f5823, new SavedStateHandler() {
            public void restoreInstanceState(Object obj, Bundle bundle) {
                StateSaver.restoreInstanceState(obj, bundle);
            }

            public void saveInstanceState(Object obj, Bundle bundle) {
                StateSaver.saveInstanceState(obj, bundle);
            }
        });
        AdinCube.m2301("734595550db947ffab46");
        try {
            JobManager.create(f5823).addJobCreator(new TTVJobCreator());
            JobConfig.setLogcatEnabled(false);
            JobConfig.setForceAllowApi14(true);
            JobConfig.setJobIdOffset(100);
        } catch (Throwable e) {
            Logger.m6281(e, true);
        }
        try {
            ReLinker.log(new ReLinker.Logger() {
                public void log(String msg) {
                }
            }).loadLibrary(f5823, "duktape");
        } catch (Throwable e2) {
            Logger.m6281(e2, true);
            TyphoonApp.f5859 = false;
        }
        CookieHandler.setDefault(new CookieManager((CookieStore) null, CookiePolicy.ACCEPT_ALL));
        try {
            HermesEventBus.getDefault().init(this);
        } catch (Throwable throwable) {
            Logger.m6281(throwable, new boolean[0]);
        }
        try {
            if (GmsUtils.m6392(m6288())) {
                ProviderInstaller.m6099(m6288());
            }
        } catch (Throwable throwable2) {
            Logger.m6281(throwable2, new boolean[0]);
            Logger.m6279("TVApplication", "Failed to patch GMS Security Provider. Reason: " + (throwable2.getMessage() != null ? throwable2.getMessage() : EnvironmentCompat.MEDIA_UNKNOWN));
        }
        registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                if (activity != null && activity.getClass().getName().trim().toLowerCase().contains("io.presage.activities.")) {
                    TyphoonApp.f5873 = false;
                }
            }

            public void onActivityStarted(final Activity activity) {
                c cClass;
                Field kField;
                try {
                    if (activity instanceof ExoPlayerActivity) {
                        TyphoonApp.f5852 = true;
                    }
                    String activityFullName = activity.getClass().getName();
                    if (!DeviceUtils.m6389(new boolean[0]) || !activityFullName.toLowerCase().startsWith("com.Typhoon.TV.ui.activity")) {
                        if (DeviceUtils.m6389(new boolean[0]) && (activity instanceof ChartboostActivity)) {
                            try {
                                if (h.a() != null && (cClass = h.a().r) != null && (kField = cClass.getClass().getDeclaredField("k")) != null) {
                                    kField.setAccessible(true);
                                    kField.set(cClass, false);
                                }
                            } catch (Throwable throwable) {
                                Logger.m6281(throwable, new boolean[0]);
                            }
                        } else if (activity instanceof CBImpressionActivity) {
                            if (DeviceUtils.m6389(new boolean[0])) {
                                Toast.makeText(TVApplication.f5823, "Please wait *5* and you can close the ad...", 1).show();
                            }
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    Method declaredMethod;
                                    try {
                                        if (!activity.isFinishing()) {
                                            CBImpressionActivity cBImpressionActivity = activity;
                                            Field declaredField = cBImpressionActivity.getClass().getDeclaredField("c");
                                            if (declaredField != null) {
                                                declaredField.setAccessible(true);
                                                c cVar = (c) declaredField.get(cBImpressionActivity);
                                                if (cVar != null && (declaredMethod = cVar.getClass().getDeclaredMethod("e", new Class[0])) != null) {
                                                    declaredMethod.setAccessible(true);
                                                    com.chartboost.sdk.Model.c cVar2 = (com.chartboost.sdk.Model.c) declaredMethod.invoke(cVar, new Object[0]);
                                                    if (cVar2 != null) {
                                                        cVar2.m = 0;
                                                        Toast.makeText(TVApplication.f5823, "You can press the back button to close the ad now!", 1).show();
                                                    }
                                                }
                                            }
                                        }
                                    } catch (Throwable th) {
                                        Logger.m6281(th, new boolean[0]);
                                        if (DeviceUtils.m6389(new boolean[0])) {
                                            activity.finish();
                                        }
                                    }
                                }
                            }, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                        } else if (DeviceUtils.m6389(new boolean[0]) && ((activityFullName.equalsIgnoreCase("io.presage.activities.PresageActivity") || activityFullName.startsWith("com.aerserv.sdk.view.AS") || activityFullName.contains("aerserv") || activityFullName.equalsIgnoreCase("com.loopme.AdActivity") || activityFullName.equalsIgnoreCase("ai.vi.mobileads.api.ViAdActivity") || activityFullName.equalsIgnoreCase("com.inmobi.rendering.InMobiAdActivity")) && TyphoonApp.f5848)) {
                            Toast.makeText(TVApplication.f5823, "Please wait *20* seconds and the ad will be closed automatically...", 1).show();
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    Toast.makeText(TVApplication.f5823, "Please wait *10* more seconds and the ad will be closed automatically...", 1).show();
                                }
                            }, 10000);
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    Toast.makeText(TVApplication.f5823, "Please wait *5* more seconds and the ad will be closed automatically...", 1).show();
                                }
                            }, 15000);
                            new Handler().postDelayed(new Runnable() {
                                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v22, resolved type: java.lang.Object} */
                                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.aerserv.sdk.view.component.BackButton} */
                                /* JADX WARNING: Multi-variable type inference failed */
                                /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
                                /* Code decompiled incorrectly, please refer to instructions dump. */
                                public void run() {
                                    /*
                                        r16 = this;
                                        r15 = 0
                                        r14 = 1
                                        r0 = r16
                                        android.app.Activity r11 = r12     // Catch:{ Throwable -> 0x002a }
                                        boolean r11 = r11 instanceof io.presage.activities.PresageActivity     // Catch:{ Throwable -> 0x002a }
                                        if (r11 == 0) goto L_0x003f
                                        r0 = r16
                                        android.app.Activity r8 = r12     // Catch:{ Throwable -> 0x002a }
                                        io.presage.activities.PresageActivity r8 = (io.presage.activities.PresageActivity) r8     // Catch:{ Throwable -> 0x002a }
                                        boolean r11 = r8.isFinishing()     // Catch:{ Throwable -> 0x002a }
                                        if (r11 == 0) goto L_0x0017
                                    L_0x0016:
                                        return
                                    L_0x0017:
                                        r8.finish()     // Catch:{ Throwable -> 0x002a }
                                        android.content.Context r11 = com.typhoon.tv.TVApplication.f5823     // Catch:{ Throwable -> 0x002a }
                                        java.lang.String r12 = "Successfully closed the ad!"
                                        r13 = 1
                                        android.widget.Toast r11 = android.widget.Toast.makeText(r11, r12, r13)     // Catch:{ Throwable -> 0x002a }
                                        r11.show()     // Catch:{ Throwable -> 0x002a }
                                        goto L_0x0016
                                    L_0x002a:
                                        r7 = move-exception
                                        boolean[] r11 = new boolean[r15]
                                        com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r11)
                                        android.content.Context r11 = com.typhoon.tv.TVApplication.f5823
                                        java.lang.String r12 = "Unable to close the ad..."
                                        android.widget.Toast r11 = android.widget.Toast.makeText(r11, r12, r14)
                                        r11.show()
                                        goto L_0x0016
                                    L_0x003f:
                                        r0 = r16
                                        android.app.Activity r11 = r12     // Catch:{ Throwable -> 0x002a }
                                        boolean r11 = r11 instanceof com.aerserv.sdk.view.ASInterstitialActivity     // Catch:{ Throwable -> 0x002a }
                                        if (r11 == 0) goto L_0x0016
                                        r0 = r16
                                        android.app.Activity r1 = r12     // Catch:{ Throwable -> 0x002a }
                                        com.aerserv.sdk.view.ASInterstitialActivity r1 = (com.aerserv.sdk.view.ASInterstitialActivity) r1     // Catch:{ Throwable -> 0x002a }
                                        boolean r11 = r1.isFinishing()     // Catch:{ Throwable -> 0x002a }
                                        if (r11 != 0) goto L_0x0016
                                        java.lang.Class r4 = r1.getClass()     // Catch:{ Throwable -> 0x002a }
                                        r2 = 0
                                        java.lang.String r11 = "backButton"
                                        java.lang.reflect.Field r3 = r4.getDeclaredField(r11)     // Catch:{ Exception -> 0x0077 }
                                        r11 = 1
                                        r3.setAccessible(r11)     // Catch:{ Exception -> 0x0077 }
                                        java.lang.Object r11 = r3.get(r1)     // Catch:{ Exception -> 0x0077 }
                                        r0 = r11
                                        com.aerserv.sdk.view.component.BackButton r0 = (com.aerserv.sdk.view.component.BackButton) r0     // Catch:{ Exception -> 0x0077 }
                                        r2 = r0
                                    L_0x006b:
                                        if (r2 == 0) goto L_0x007f
                                        boolean r11 = r2.isBackButtonEnable()     // Catch:{ Throwable -> 0x002a }
                                        if (r11 == 0) goto L_0x007f
                                        r1.onBackPressed()     // Catch:{ Throwable -> 0x002a }
                                        goto L_0x0016
                                    L_0x0077:
                                        r7 = move-exception
                                        r11 = 0
                                        boolean[] r11 = new boolean[r11]     // Catch:{ Throwable -> 0x002a }
                                        com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r11)     // Catch:{ Throwable -> 0x002a }
                                        goto L_0x006b
                                    L_0x007f:
                                        java.lang.String r11 = "controllerId"
                                        java.lang.reflect.Field r6 = r4.getDeclaredField(r11)     // Catch:{ Exception -> 0x00b2 }
                                        r11 = 1
                                        r6.setAccessible(r11)     // Catch:{ Exception -> 0x00b2 }
                                        java.lang.Object r5 = r6.get(r1)     // Catch:{ Exception -> 0x00b2 }
                                        java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x00b2 }
                                        if (r5 == 0) goto L_0x0097
                                        com.aerserv.sdk.AerServEvent r11 = com.aerserv.sdk.AerServEvent.AD_DISMISSED     // Catch:{ Exception -> 0x00b2 }
                                        com.aerserv.sdk.controller.listener.AerServEventListenerLocator.fireEvent(r5, r11)     // Catch:{ Exception -> 0x00b2 }
                                    L_0x0097:
                                        java.lang.String r11 = "providerListener"
                                        java.lang.reflect.Field r10 = r4.getDeclaredField(r11)     // Catch:{ Exception -> 0x00ba }
                                        r11 = 1
                                        r10.setAccessible(r11)     // Catch:{ Exception -> 0x00ba }
                                        java.lang.Object r9 = r10.get(r1)     // Catch:{ Exception -> 0x00ba }
                                        com.aerserv.sdk.controller.listener.ProviderListener r9 = (com.aerserv.sdk.controller.listener.ProviderListener) r9     // Catch:{ Exception -> 0x00ba }
                                        if (r9 == 0) goto L_0x00ad
                                        r9.onProviderFinished()     // Catch:{ Exception -> 0x00ba }
                                    L_0x00ad:
                                        r1.finish()     // Catch:{ Throwable -> 0x002a }
                                        goto L_0x0016
                                    L_0x00b2:
                                        r7 = move-exception
                                        r11 = 0
                                        boolean[] r11 = new boolean[r11]     // Catch:{ Throwable -> 0x002a }
                                        com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r11)     // Catch:{ Throwable -> 0x002a }
                                        goto L_0x0097
                                    L_0x00ba:
                                        r7 = move-exception
                                        r11 = 0
                                        boolean[] r11 = new boolean[r11]     // Catch:{ Throwable -> 0x002a }
                                        com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r11)     // Catch:{ Throwable -> 0x002a }
                                        goto L_0x00ad
                                    */
                                    throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.TVApplication.AnonymousClass3.AnonymousClass4.run():void");
                                }
                            }, 20000);
                        } else if (activityFullName.equalsIgnoreCase("com.apptracker.android.module.AppModuleActivity")) {
                            Toast.makeText(TVApplication.f5823, I18N.m15706(R.string.please_wait), 1).show();
                        }
                    } else if (TVApplication.m6285().getBoolean("pref_force_landscape_mode_on_tv", true)) {
                        com.mopub.common.util.DeviceUtils.lockOrientation(activity, CreativeOrientation.LANDSCAPE);
                    }
                } catch (Throwable e) {
                    Logger.m6281(e, new boolean[0]);
                }
            }

            public void onActivityResumed(Activity activity) {
                if (activity != null) {
                    try {
                        if (TyphoonApp.f5849) {
                            String activityFullName = activity.getClass().getName().trim().toLowerCase();
                            if (activityFullName.contains("com.applovin.adview.") || activityFullName.contains("com.vungle.publisher.") || activityFullName.contains("com.chartboost.sdk.") || activityFullName.contains("com.adincube.sdk.mediation.chartboost.") || activityFullName.contains("com.adcolony.sdk.") || activityFullName.contains("com.jirbo.adcolony.") || activityFullName.contains("com.exo.player.") || activityFullName.contains("com.startapp.android.publish.") || activityFullName.contains("com.mobvista.") || activityFullName.contains("com.apptracker.android.module.") || activityFullName.contains("com.loopme.") || activityFullName.contains("ai.vi.mobileads.") || activityFullName.contains("com.inmobi.")) {
                                Utils.m6422(true);
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }

            public void onActivityPaused(Activity activity) {
                if (activity != null) {
                    try {
                        if (TyphoonApp.f5849) {
                            if (DeviceUtils.m6389(new boolean[0])) {
                                Utils.m6422(false);
                                return;
                            }
                            String activityFullName = activity.getClass().getName().trim().toLowerCase();
                            if (activityFullName.contains("com.applovin.adview.") || activityFullName.contains("com.vungle.publisher.") || activityFullName.contains("com.chartboost.sdk.") || activityFullName.contains("com.adincube.sdk.mediation.chartboost.") || activityFullName.contains("com.adcolony.sdk.") || activityFullName.contains("com.jirbo.adcolony.") || activityFullName.contains("com.exo.player.") || activityFullName.contains("com.startapp.android.publish.") || activityFullName.contains("com.mobvista.") || activityFullName.contains("com.apptracker.android.module.") || activityFullName.contains("com.loopme.") || activityFullName.contains("ai.vi.mobileads.") || activityFullName.contains("com.inmobi.")) {
                                Utils.m6422(false);
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }

            public void onActivityStopped(Activity activity) {
                if (activity instanceof ExoPlayerActivity) {
                    TyphoonApp.f5852 = false;
                }
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            public void onActivityDestroyed(Activity activity) {
            }
        });
        m6282();
        m6283();
        try {
            List<String> abiList = DeviceUtils.m6386();
            if (!abiList.isEmpty()) {
                StringBuilder abiStrBuilder = new StringBuilder();
                for (String abi : abiList) {
                    abiStrBuilder.append(abi);
                    abiStrBuilder.append(",");
                }
                String abiStr = abiStrBuilder.toString().replace(StringUtils.SPACE, "");
                if (abiStr.endsWith(",")) {
                    abiStr = abiStr.substring(0, abiStr.length() - 1);
                }
                if (!abiStr.isEmpty()) {
                }
            }
        } catch (Exception e3) {
            Logger.m6281((Throwable) e3, new boolean[0]);
        }
    }

    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        try {
            Glide.m3936(f5823).m3955(level);
        } catch (Throwable e) {
            Logger.m6281(e, new boolean[0]);
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        try {
            Glide.m3934(f5823).m3976();
        } catch (Throwable e) {
            Logger.m6281(e, new boolean[0]);
        }
    }

    public void onTerminate() {
        super.onTerminate();
        SugarContext.m6266();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6282() {
        try {
            FirebaseMessaging.m6109().m6110("allDevices");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        try {
            PackageInfo pInfo = m6288().getPackageManager().getPackageInfo(m6288().getPackageName(), 0);
            if (!(pInfo == null || pInfo.versionName == null || pInfo.versionName.length() <= 0)) {
                FirebaseMessaging.m6109().m6110(pInfo.versionName);
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        try {
            if (m6287().m6320((Integer) 0, (Integer) 1399)) {
                FirebaseMessaging.m6109().m6110("got");
            }
        } catch (Throwable e3) {
            Logger.m6281(e3, new boolean[0]);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m6283() {
        try {
            FirebaseRemoteConfig.m6116().m6126(new FirebaseRemoteConfigSettings.Builder().m6132(false).m6133());
            FirebaseRemoteConfig.m6116().m6127(TyphoonApp.m6324());
            long cacheExpiration = 43200;
            if (FirebaseRemoteConfig.m6116().m6121().m6129().m6130()) {
                cacheExpiration = 0;
            }
            FirebaseRemoteConfig.m6116().m6122(cacheExpiration).m6104(new OnCompleteListener<Void>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m6290(Task<Void> task) {
                    if (task.m6101()) {
                        try {
                            FirebaseRemoteConfig.m6116().m6120();
                            TyphoonApp.f5907 = FirebaseRemoteConfig.m6116().m6123("ttv_base_server");
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                }
            });
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    private static class CustomCrashActivityListener implements CustomActivityOnCrash.EventListener {
        private CustomCrashActivityListener() {
        }

        public void onLaunchErrorActivity() {
            try {
                long availableMemory = Runtime.getRuntime().maxMemory();
                long usedMemory = Runtime.getRuntime().totalMemory();
                float percentAvailable = 100.0f * (1.0f - (((float) usedMemory) / ((float) availableMemory)));
                Logger.m6279("CustomCrashActivityListener", "availableMemory=" + availableMemory + "; usedMemory=" + usedMemory + "; percentAvailable=" + percentAvailable);
                if (percentAvailable <= 5.0f) {
                    m6291();
                }
            } catch (Throwable e) {
                Logger.m6281(e, new boolean[0]);
            }
        }

        public void onRestartAppFromErrorActivity() {
        }

        public void onCloseAppFromErrorActivity() {
        }

        @SuppressLint({"ApplySharedPref"})
        /* renamed from: 龘  reason: contains not printable characters */
        private void m6291() {
            SharedPreferences.Editor editor = TVApplication.m6285().edit();
            editor.putBoolean("pref_auto_resolve_hd_links_only", true);
            editor.commit();
        }
    }
}
