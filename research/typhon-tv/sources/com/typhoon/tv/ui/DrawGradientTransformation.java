package com.typhoon.tv.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.support.v4.view.ViewCompat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

public class DrawGradientTransformation extends CenterCrop {

    /* renamed from: 龘  reason: contains not printable characters */
    private static DrawGradientTransformation f13057;

    private DrawGradientTransformation(Context context) {
        super(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DrawGradientTransformation m16845(Context context) {
        DrawGradientTransformation drawGradientTransformation = f13057;
        if (drawGradientTransformation == null) {
            synchronized (DrawGradientTransformation.class) {
                try {
                    drawGradientTransformation = f13057;
                    if (drawGradientTransformation == null) {
                        DrawGradientTransformation drawGradientTransformation2 = new DrawGradientTransformation(context);
                        try {
                            f13057 = drawGradientTransformation2;
                            drawGradientTransformation = drawGradientTransformation2;
                        } catch (Throwable th) {
                            th = th;
                            DrawGradientTransformation drawGradientTransformation3 = drawGradientTransformation2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return drawGradientTransformation;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m16846(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2) {
        Bitmap r16 = super.m25734(bitmapPool, bitmap, i, i2);
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(r16, 0.0f, 0.0f, (Paint) null);
        Paint paint = new Paint();
        float f = ((float) i2) / 2.0f;
        paint.setShader(new LinearGradient(0.0f, ((float) i2) - f, 0.0f, (float) i2, -1, ViewCompat.MEASURED_SIZE_MASK, Shader.TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, ((float) i2) - f, (float) i, (float) i2, paint);
        return createBitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16847() {
        return "gradient()";
    }
}
