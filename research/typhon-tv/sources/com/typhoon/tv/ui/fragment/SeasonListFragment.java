package com.typhoon.tv.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.event.ReverseSeasonListEvent;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvLatestPlayed;
import com.typhoon.tv.model.media.tv.TvSeasonInfo;
import com.typhoon.tv.presenter.ISeasonPresenter;
import com.typhoon.tv.presenter.impl.SeasonPresenterImpl;
import com.typhoon.tv.ui.activity.EpisodeListActivity;
import com.typhoon.tv.ui.activity.MediaDetailsActivity;
import com.typhoon.tv.ui.adapter.TvSeasonAdapter;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.widget.AutofitSuperRecyclerView;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.view.ISeasonView;
import java.util.ArrayList;
import rx.Subscription;
import rx.functions.Action1;

public class SeasonListFragment extends Fragment implements ISeasonView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public ISeasonPresenter f13867;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Subscription f13868;

    /* renamed from: 连任  reason: contains not printable characters */
    private final MediaCardViewHolder.OnCardClickListener f13869 = new MediaCardViewHolder.OnCardClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17617(int i) {
            Intent intent = new Intent(SeasonListFragment.this.f13872, EpisodeListActivity.class);
            intent.putExtra("mediaInfo", SeasonListFragment.this.f13873);
            intent.putExtra("selectedSeasonInfo", (TvSeasonInfo) SeasonListFragment.this.f13871.m17495(i));
            intent.putParcelableArrayListExtra("seasonInfoList", SeasonListFragment.this.f13871.m17496());
            SeasonListFragment.this.f13872.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public AutofitSuperRecyclerView f13870;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public TvSeasonAdapter f13871;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public Context f13872;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaInfo f13873;

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17608() {
        this.f13867 = new SeasonPresenterImpl(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SeasonListFragment m17612(MediaInfo mediaInfo) {
        SeasonListFragment seasonListFragment = new SeasonListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("mediaInfo", mediaInfo);
        seasonListFragment.setArguments(bundle);
        return seasonListFragment;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17614(final boolean z) {
        this.f13870.getSwipeToRefresh().post(new Runnable() {
            public void run() {
                SeasonListFragment.this.f13870.getSwipeToRefresh().setRefreshing(z);
            }
        });
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (NetworkUtils.m17799()) {
            this.f13867.m16148(this.f13873);
        } else {
            this.f13870.m26364();
            this.f13870.m26363();
            Toast.makeText(getActivity(), I18N.m15706(R.string.no_internet), 0).show();
        }
        this.f13868 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                if ((obj instanceof ReverseSeasonListEvent) && !SeasonListFragment.this.f13871.m17496().isEmpty()) {
                    SeasonListFragment.this.f13871.m17493();
                }
            }
        });
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f13872 = getActivity();
        View inflate = layoutInflater.inflate(R.layout.fragment_season_list, viewGroup, false);
        this.f13870 = (AutofitSuperRecyclerView) inflate.findViewById(R.id.rvSeasonList);
        this.f13870.getRecyclerView().setHasFixedSize(true);
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f13867.m16146();
        this.f13867 = null;
        if (this.f13868 != null && !this.f13868.isUnsubscribed()) {
            this.f13868.unsubscribe();
        }
        this.f13868 = null;
    }

    public void onViewCreated(View view, Bundle bundle) {
        this.f13873 = (MediaInfo) getArguments().getParcelable("mediaInfo");
        this.f13871 = new TvSeasonAdapter(this.f13873, new ArrayList());
        this.f13871.m17497(this.f13869);
        this.f13870.setAdapter(this.f13871);
        this.f13870.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        this.f13870.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                SeasonListFragment.this.f13867.m16147();
                if (NetworkUtils.m17799()) {
                    SeasonListFragment.this.f13867.m16148(SeasonListFragment.this.f13873);
                    return;
                }
                SeasonListFragment.this.m17614(false);
                ((MediaDetailsActivity) SeasonListFragment.this.f13872).m16967(I18N.m15706(R.string.no_internet));
            }
        });
        this.f13870.m26361();
        this.f13870.m26367();
        m17608();
    }

    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        this.f13871.m17497(this.f13869);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17615() {
        m17614(false);
        this.f13870.m26364();
        this.f13870.m26363();
        ((MediaDetailsActivity) this.f13872).m16967(I18N.m15706(R.string.error));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17616(ArrayList<TvSeasonInfo> arrayList) {
        m17614(false);
        this.f13870.m26364();
        this.f13870.m26363();
        this.f13871.m17492();
        this.f13871.m17500(arrayList);
        if (TVApplication.m6285().getInt("pref_season_reverse_order", 0) == 1) {
            this.f13871.m17493();
        }
        TvLatestPlayed r4 = TVApplication.m6287().m6296(Integer.valueOf(this.f13873.getTmdbId()));
        if (r4 != null) {
            int season = r4.getSeason();
            ArrayList r0 = this.f13871.m17496();
            int i = 0;
            while (i < this.f13871.getItemCount()) {
                if (((TvSeasonInfo) r0.get(i)).getSeasonNum() != season || !(this.f13870.getRecyclerView().getLayoutManager() instanceof LinearLayoutManager)) {
                    i++;
                } else {
                    ((LinearLayoutManager) this.f13870.getRecyclerView().getLayoutManager()).scrollToPositionWithOffset(i, 0);
                    return;
                }
            }
        }
    }
}
