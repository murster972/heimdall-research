package com.typhoon.tv.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.IMediaSuggestionPresenter;
import com.typhoon.tv.presenter.impl.MediaSuggestionPresenterImpl;
import com.typhoon.tv.ui.activity.MediaDetailsActivity;
import com.typhoon.tv.ui.activity.base.BaseActivity;
import com.typhoon.tv.ui.adapter.MediaListAdapter;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.widget.AutofitSuperRecyclerView;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.view.IMediaSuggestionView;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Callback;

public class MediaSuggestionFragment extends Fragment implements IMediaSuggestionView {

    /* renamed from: 连任  reason: contains not printable characters */
    private IMediaSuggestionPresenter f13834;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public AutofitSuperRecyclerView f13835;

    /* renamed from: 麤  reason: contains not printable characters */
    private final MediaCardViewHolder.OnCardClickListener f13836 = new MediaCardViewHolder.OnCardClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17578(int i) {
            Intent intent = new Intent(MediaSuggestionFragment.this.getActivity(), MediaDetailsActivity.class);
            intent.putExtra("mediaInfo", (MediaInfo) MediaSuggestionFragment.this.f13837.m17495(i));
            MediaSuggestionFragment.this.getActivity().startActivity(intent);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public MediaListAdapter f13837;

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo f13838;

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17569() {
        int tmdbId = this.f13838.getTmdbId();
        if (this.f13838.getType() == 1) {
            this.f13834.m16143(tmdbId);
        } else {
            this.f13834.m16141(tmdbId);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaSuggestionFragment m17573(MediaInfo mediaInfo) {
        MediaSuggestionFragment mediaSuggestionFragment = new MediaSuggestionFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("mediaInfo", mediaInfo);
        mediaSuggestionFragment.setArguments(bundle);
        return mediaSuggestionFragment;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17575(final boolean z) {
        this.f13835.getSwipeToRefresh().post(new Runnable() {
            public void run() {
                MediaSuggestionFragment.this.f13835.getSwipeToRefresh().setRefreshing(z);
            }
        });
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (NetworkUtils.m17799()) {
            m17569();
            return;
        }
        this.f13835.m26364();
        m17575(false);
        ((MediaDetailsActivity) getActivity()).m16967(I18N.m15706(R.string.no_internet));
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f13838 = (MediaInfo) getArguments().getParcelable("mediaInfo");
        this.f13834 = new MediaSuggestionPresenterImpl(this);
        View inflate = layoutInflater.inflate(R.layout.fragment_media_suggestion, viewGroup, false);
        this.f13835 = (AutofitSuperRecyclerView) inflate.findViewById(R.id.rvSuggestionList);
        this.f13835.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        this.f13835.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                if (NetworkUtils.m17799()) {
                    MediaSuggestionFragment.this.m17569();
                    return;
                }
                ((MediaDetailsActivity) MediaSuggestionFragment.this.getActivity()).m16967(I18N.m15706(R.string.no_internet));
                MediaSuggestionFragment.this.m17575(false);
            }
        });
        this.f13835.getRecyclerView().setHasFixedSize(true);
        this.f13837 = new MediaListAdapter(new ArrayList());
        this.f13837.m17497(this.f13836);
        this.f13837.m17498((MediaCardViewHolder.OnCardLongClickListener) new MediaCardViewHolder.OnCardLongClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17579(View view, int i) {
                if (MediaSuggestionFragment.this.getActivity() instanceof BaseActivity) {
                    final MediaInfo mediaInfo = (MediaInfo) MediaSuggestionFragment.this.f13837.m17495(i);
                    final boolean r2 = TVApplication.m6287().m6319(mediaInfo);
                    String[] strArr = new String[1];
                    strArr[0] = I18N.m15706(r2 ? R.string.action_remove_from_bookmark : R.string.action_add_to_bookmark);
                    new AlertDialog.Builder(MediaSuggestionFragment.this.getActivity()).m536((CharSequence) mediaInfo.getNameAndYear()).m540((CharSequence[]) strArr, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            boolean z = true;
                            if (MediaSuggestionFragment.this.getActivity() instanceof BaseActivity) {
                                switch (i) {
                                    case 0:
                                        if (r2) {
                                            TVApplication.m6287().m6304(mediaInfo);
                                        } else {
                                            TVApplication.m6287().m6299(mediaInfo);
                                        }
                                        try {
                                            BaseActivity baseActivity = (BaseActivity) MediaSuggestionFragment.this.getActivity();
                                            MediaInfo mediaInfo = mediaInfo;
                                            if (r2) {
                                                z = false;
                                            }
                                            baseActivity.m17299(mediaInfo, z, true, (Callback<SyncResponse>) null);
                                            return;
                                        } catch (Exception e) {
                                            Logger.m6281((Throwable) e, new boolean[0]);
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            }
                        }
                    }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (!MediaSuggestionFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                                dialogInterface.dismiss();
                            }
                        }
                    }).m528();
                }
            }
        });
        this.f13835.setAdapter(this.f13837);
        this.f13835.m26367();
        return inflate;
    }

    public void onDestroy() {
        if (this.f13834 != null) {
            this.f13834.m16142();
        }
        this.f13834 = null;
        super.onDestroy();
    }

    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        this.f13837.m17497(this.f13836);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17576() {
        this.f13835.m26364();
        m17575(false);
        MediaDetailsActivity mediaDetailsActivity = (getActivity() == null || !(getActivity() instanceof MediaDetailsActivity)) ? null : (MediaDetailsActivity) getActivity();
        if (mediaDetailsActivity != null) {
            mediaDetailsActivity.m16967(I18N.m15706(R.string.error));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17577(List<MediaInfo> list) {
        this.f13835.m26364();
        this.f13837.m17492();
        this.f13837.m17500(list);
    }
}
