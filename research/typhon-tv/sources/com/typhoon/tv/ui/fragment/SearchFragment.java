package com.typhoon.tv.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.ISearchPresenter;
import com.typhoon.tv.presenter.impl.SearchPresenterImpl;
import com.typhoon.tv.ui.activity.MediaDetailsActivity;
import com.typhoon.tv.ui.activity.SearchActivity;
import com.typhoon.tv.ui.activity.base.BaseActivity;
import com.typhoon.tv.ui.adapter.MediaListAdapter;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.widget.AutofitSuperRecyclerView;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.view.ISearchView;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import java.util.ArrayList;
import retrofit2.Callback;

public class SearchFragment extends Fragment implements ISearchView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f13848;

    /* renamed from: ʼ  reason: contains not printable characters */
    private RelativeLayout f13849;

    /* renamed from: ʽ  reason: contains not printable characters */
    private TextView f13850;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ImageView f13851;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public int f13852;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaListAdapter f13853;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public ISearchPresenter f13854;

    /* renamed from: 齉  reason: contains not printable characters */
    private final MediaCardViewHolder.OnCardClickListener f13855 = new MediaCardViewHolder.OnCardClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17603(int i) {
            Intent intent = new Intent(SearchFragment.this.getActivity(), MediaDetailsActivity.class);
            intent.putExtra("mediaInfo", (MediaInfo) SearchFragment.this.f13853.m17495(i));
            SearchFragment.this.getActivity().startActivity(intent);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public AutofitSuperRecyclerView f13856;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m17581() {
        if (this.f13856 != null) {
            this.f13856.m26361();
            this.f13856.m26367();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m17582() {
        if (this.f13856 != null) {
            this.f13856.m26364();
            this.f13856.m26363();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m17583() {
        try {
            ((LinearLayoutManager) this.f13856.getRecyclerView().getLayoutManager()).scrollToPositionWithOffset(0, 0);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m17584() {
        try {
            this.f13850.setText(I18N.m15706(R.string.no_internet));
            Glide.m3937((Fragment) this).m3973(Integer.valueOf(R.drawable.ic_cloud_off_white_36dp)).m25186(100, 100).m25185().m25181().m25209(this.f13851);
            this.f13856.setVisibility(8);
            this.f13849.setVisibility(0);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m17585() {
        try {
            this.f13850.setText(I18N.m15706(R.string.search_something));
            Glide.m3937((Fragment) this).m3973(Integer.valueOf(R.drawable.ic_search_white_36dp)).m25186(100, 100).m25185().m25181().m25209(this.f13851);
            this.f13856.setVisibility(8);
            this.f13849.setVisibility(0);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m17586() {
        if (this.f13849 != null) {
            this.f13849.setVisibility(8);
        }
        if (this.f13856 != null) {
            this.f13856.setVisibility(0);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m17588() {
        if (this.f13856 != null) {
            this.f13856.m26362();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static /* synthetic */ int m17590(SearchFragment searchFragment) {
        int i = searchFragment.f13852;
        searchFragment.f13852 = i + 1;
        return i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17591() {
        if (this.f13856 != null) {
            this.f13856.setupMoreListener(new OnMoreListener() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17605(int i, int i2, int i3) {
                    if (!NetworkUtils.m17799()) {
                        SearchFragment.this.f13856.m26365();
                        SearchFragment.this.m17596(I18N.m15706(R.string.no_internet));
                    } else if (SearchFragment.this.f13848 != null) {
                        SearchFragment.this.f13856.m26366();
                        SearchFragment.m17590(SearchFragment.this);
                        SearchFragment.this.f13854.m16145(SearchFragment.this.m17595(), SearchFragment.this.f13848, SearchFragment.this.f13852);
                    }
                }
            }, 3);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SearchFragment m17594(int i, boolean z) {
        SearchFragment searchFragment = new SearchFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(VastExtensionXmlManager.TYPE, i);
        bundle.putBoolean("isSearchNow", z);
        searchFragment.setArguments(bundle);
        return searchFragment;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        TextView textView;
        this.f13854 = new SearchPresenterImpl(this);
        this.f13852 = 1;
        View inflate = layoutInflater.inflate(R.layout.fragment_search, viewGroup, false);
        this.f13849 = (RelativeLayout) inflate.findViewById(R.id.rlPlaceholder);
        this.f13850 = (TextView) inflate.findViewById(R.id.tvSearchPlaceholder);
        this.f13851 = (ImageView) inflate.findViewById(R.id.ivSearchPlaceholder);
        this.f13851.setColorFilter(-7829368, PorterDuff.Mode.SRC_ATOP);
        this.f13856 = (AutofitSuperRecyclerView) inflate.findViewById(R.id.rvSearchResult);
        View emptyView = this.f13856.getEmptyView();
        if (!(emptyView == null || (textView = (TextView) emptyView.findViewById(R.id.tvEmpty)) == null)) {
            textView.setText(I18N.m15706(m17595() == 1 ? R.string.no_movies_found : R.string.no_tv_shows_found));
            textView.setTextSize(2, 14.0f);
        }
        this.f13853 = new MediaListAdapter(new ArrayList());
        this.f13853.m17497(this.f13855);
        this.f13853.m17498((MediaCardViewHolder.OnCardLongClickListener) new MediaCardViewHolder.OnCardLongClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17604(View view, int i) {
                final MediaInfo mediaInfo = (MediaInfo) SearchFragment.this.f13853.m17495(i);
                final boolean r2 = TVApplication.m6287().m6319(mediaInfo);
                String[] strArr = new String[1];
                strArr[0] = I18N.m15706(r2 ? R.string.action_remove_from_bookmark : R.string.action_add_to_bookmark);
                new AlertDialog.Builder(SearchFragment.this.getActivity()).m536((CharSequence) mediaInfo.getNameAndYear()).m540((CharSequence[]) strArr, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        boolean z = true;
                        switch (i) {
                            case 0:
                                if (r2) {
                                    TVApplication.m6287().m6304(mediaInfo);
                                } else {
                                    TVApplication.m6287().m6299(mediaInfo);
                                }
                                try {
                                    if (SearchFragment.this.getActivity() instanceof BaseActivity) {
                                        BaseActivity baseActivity = (BaseActivity) SearchFragment.this.getActivity();
                                        MediaInfo mediaInfo = mediaInfo;
                                        if (r2) {
                                            z = false;
                                        }
                                        baseActivity.m17299(mediaInfo, z, true, (Callback<SyncResponse>) null);
                                        return;
                                    }
                                    return;
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                    return;
                                }
                            default:
                                return;
                        }
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!SearchFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                            dialogInterface.dismiss();
                        }
                    }
                }).m528();
            }
        });
        m17591();
        this.f13856.setAdapter(this.f13853);
        this.f13856.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        this.f13856.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                if (!NetworkUtils.m17799()) {
                    SearchFragment.this.m17596(SearchFragment.this.getString(R.string.no_internet));
                    SearchFragment.this.f13856.getSwipeToRefresh().post(new Runnable() {
                        public void run() {
                            SearchFragment.this.f13856.getSwipeToRefresh().setRefreshing(false);
                        }
                    });
                } else if (SearchFragment.this.f13848 != null) {
                    SearchFragment.this.m17601(SearchFragment.this.f13848, false);
                }
            }
        });
        if (!NetworkUtils.m17799()) {
            m17584();
        } else if (!m17597()) {
            m17585();
        } else {
            m17586();
        }
        return inflate;
    }

    public void onDestroyView() {
        this.f13854.m16144();
        this.f13854 = null;
        super.onDestroyView();
    }

    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        this.f13853.m17497(this.f13855);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m17595() {
        if (getArguments() == null) {
            return 0;
        }
        return getArguments().getInt(VastExtensionXmlManager.TYPE, 0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17596(String str) {
        FragmentActivity activity = getActivity();
        if (activity instanceof SearchActivity) {
            ((SearchActivity) activity).m16991(str);
        } else {
            Toast.makeText(getActivity(), str, 0).show();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m17597() {
        return getArguments() != null && getArguments().getBoolean("isSearchNow", false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17598() {
        if (this.f13856 != null) {
            this.f13853.m17492();
            m17583();
            m17588();
            this.f13856.m26365();
            m17582();
            m17586();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17599(MediaApiResult mediaApiResult) {
        if (this.f13852 <= 1) {
            this.f13853.m17492();
        }
        this.f13853.m17500(mediaApiResult.getMediaInfoList());
        if (this.f13852 == 1) {
            m17583();
        }
        int totalPage = mediaApiResult.getTotalPage();
        if (this.f13852 >= totalPage || totalPage == 1) {
            m17588();
            this.f13856.m26365();
        }
        m17582();
        m17586();
        if (this.f13852 == 1 && DeviceUtils.m6389(new boolean[0]) && this.f13856 != null) {
            try {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        MediaCardViewHolder mediaCardViewHolder;
                        try {
                            SearchFragment.this.f13856.requestFocus();
                            SearchFragment.this.f13856.getRecyclerView().requestFocus();
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        try {
                            if (SearchFragment.this.f13853 != null && SearchFragment.this.f13853.getItemCount() > 0 && !SearchFragment.this.f13856.getRecyclerView().isComputingLayout() && (mediaCardViewHolder = (MediaCardViewHolder) SearchFragment.this.f13856.getRecyclerView().findViewHolderForLayoutPosition(0)) != null && mediaCardViewHolder.itemView != null) {
                                mediaCardViewHolder.itemView.requestFocus();
                            }
                        } catch (Throwable th2) {
                            Logger.m6281(th2, new boolean[0]);
                        }
                    }
                }, 500);
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17600(String str) {
        m17601(str, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17601(String str, boolean z) {
        this.f13848 = str;
        this.f13852 = 1;
        m17602(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17602(boolean z) {
        if (this.f13848 != null) {
            m17586();
            if (z) {
                m17581();
            }
            m17591();
            this.f13854.m16145(m17595(), this.f13848, this.f13852);
        }
    }
}
