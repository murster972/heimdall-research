package com.typhoon.tv.ui.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.event.RetrievedImdbIdEvent;
import com.typhoon.tv.event.RetrievedTmdbMovieInfoEvent;
import com.typhoon.tv.event.RetrievedTmdbTvInfoEvent;
import com.typhoon.tv.helper.category.MovieCategoryHelper;
import com.typhoon.tv.helper.category.TvShowCategoryHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaRatingsModel;
import com.typhoon.tv.model.media.movie.tmdb.TmdbMovieInfoResult;
import com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult;
import com.typhoon.tv.presenter.IMediaPresenter;
import com.typhoon.tv.presenter.IMediaRatingsPresenter;
import com.typhoon.tv.presenter.impl.MediaPresenterImpl;
import com.typhoon.tv.presenter.impl.MediaRatingsPresenterImpl;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.TypefaceUtils;
import com.typhoon.tv.utils.YouTubeUtils;
import com.typhoon.tv.view.IMediaInfoView;
import com.typhoon.tv.view.IMediaRatingsView;
import java.util.Iterator;
import java.util.List;

public class MediaDetailsFragment extends Fragment implements IMediaInfoView, IMediaRatingsView {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ProgressBar f13819;

    /* renamed from: ʼ  reason: contains not printable characters */
    private TextView f13820;

    /* renamed from: ʽ  reason: contains not printable characters */
    private TextView f13821;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f13822 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f13823 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f13824;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ImageView f13825;

    /* renamed from: ٴ  reason: contains not printable characters */
    private IMediaPresenter f13826;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private IMediaRatingsPresenter f13827;

    /* renamed from: 连任  reason: contains not printable characters */
    private TextView f13828;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f13829;

    /* renamed from: 麤  reason: contains not printable characters */
    private View f13830;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f13831;

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo f13832;

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaDetailsFragment m17557(MediaInfo mediaInfo, int activityId) {
        MediaDetailsFragment fragment = new MediaDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable("mediaInfo", mediaInfo);
        args.putInt("activityId", activityId);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        boolean z;
        View view = inflater.inflate(R.layout.fragment_media_details, container, false);
        if (Build.VERSION.SDK_INT >= 16 && container != null) {
            view.setMinimumHeight(container.getMinimumHeight());
        }
        this.f13830 = view;
        this.f13832 = (MediaInfo) getArguments().getParcelable("mediaInfo");
        if (this.f13832.getType() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.f13829 = z;
        this.f13831 = getArguments().getInt("activityId", 0);
        TextView tvTitle = (TextView) view.findViewById(R.id.mediaInfoDetailsTitle);
        tvTitle.setText(this.f13832.getName());
        tvTitle.setTypeface(TypefaceUtils.m17839());
        this.f13828 = (TextView) view.findViewById(R.id.mediaInfoSynopsis);
        this.f13819 = (ProgressBar) view.findViewById(R.id.pbShowDetails);
        this.f13820 = (TextView) view.findViewById(R.id.tvRating);
        this.f13821 = (TextView) view.findViewById(R.id.mediaInfoMeta);
        this.f13825 = (ImageView) view.findViewById(R.id.ivBanner);
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.f13826 = new MediaPresenterImpl(this);
        this.f13827 = new MediaRatingsPresenterImpl(this);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (NetworkUtils.m17799()) {
            if (this.f13829) {
                this.f13826.m16136(this.f13832.getTmdbId());
            } else {
                this.f13826.m16138(this.f13832.getTmdbId());
            }
            this.f13827.m16140(this.f13832);
        } else if (this.f13829) {
            m17561();
        } else {
            m17564();
        }
        this.f13823 = true;
        if (this.f13824 != null && !this.f13824.isEmpty() && !m17562()) {
            m17563();
        }
    }

    public void onDestroyView() {
        this.f13826.m16137();
        this.f13826 = null;
        this.f13827.m16139();
        this.f13827 = null;
        super.onDestroyView();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17567(TmdbTvInfoResult result) {
        String countryCode;
        String rating;
        if (result.getOverview() == null || result.getOverview().isEmpty()) {
            this.f13828.setText(I18N.m15706(R.string.no_synopsis));
        } else {
            this.f13828.setText(result.getOverview());
        }
        this.f13828.setVisibility(0);
        StringBuilder metaDataStr = new StringBuilder();
        boolean addedYear = false;
        if (this.f13832.getYear() > 0) {
            metaDataStr.append(this.f13832.getYear());
            addedYear = true;
        }
        boolean addedRuntime = false;
        if (result.getEpisode_run_time() != null && result.getEpisode_run_time().size() > 0 && result.getEpisode_run_time().get(0).intValue() > 0) {
            if (addedYear) {
                metaDataStr.append(" • ");
            }
            metaDataStr.append(result.getEpisode_run_time().get(0).intValue()).append("min");
            addedRuntime = true;
        }
        if (result.getStatus() != null && !result.getStatus().isEmpty()) {
            if (addedRuntime || addedYear) {
                metaDataStr.append(" • ");
            }
            String status = result.getStatus().toLowerCase();
            if (status.contains("ended")) {
                metaDataStr.append(I18N.m15706(R.string.meta_ended));
            } else if (status.contains("production")) {
                metaDataStr.append(I18N.m15706(R.string.meta_in_production));
            } else if (status.contains("returning")) {
                metaDataStr.append(I18N.m15706(R.string.meta_returning_series));
            } else {
                metaDataStr.append(I18N.m15706(R.string.meta_ended));
            }
        }
        boolean addedGenres = false;
        if (result.getGenres() != null && !result.getGenres().isEmpty()) {
            List<TmdbTvInfoResult.GenresBean> genres = result.getGenres();
            SparseArray<String> genreMap = TvShowCategoryHelper.m16004();
            for (int i = 0; i < genres.size(); i++) {
                TmdbTvInfoResult.GenresBean genre = genres.get(i);
                if (genre.getId() >= 0 && genreMap.indexOfKey(genre.getId()) > -1) {
                    if (!addedGenres) {
                        metaDataStr.append(" • ");
                    }
                    addedGenres = true;
                    metaDataStr.append(genreMap.get(genre.getId()));
                    metaDataStr.append(", ");
                }
            }
        }
        String tmpStr = metaDataStr.toString();
        if (!tmpStr.isEmpty() && tmpStr.endsWith(", ")) {
            metaDataStr = new StringBuilder(tmpStr.substring(0, tmpStr.length() - 2));
        }
        if (result.getNetworks() != null && !result.getNetworks().isEmpty()) {
            metaDataStr.append(" • ");
            for (TmdbTvInfoResult.NetworksBean network : result.getNetworks()) {
                String name = network.getName();
                if (name != null && !name.isEmpty()) {
                    metaDataStr.append(name);
                    metaDataStr.append(", ");
                }
            }
        }
        String metaStr = metaDataStr.toString();
        if (!metaStr.isEmpty() && metaStr.endsWith(", ")) {
            metaStr = metaStr.substring(0, metaStr.length() - 2);
        }
        String contentRating = "";
        String contentRatingCountry = "";
        try {
            TmdbTvInfoResult.ContentRatingsBean contentRatingsBean = result.getContent_ratings();
            if (contentRatingsBean != null && contentRatingsBean.getResults() != null) {
                String[] strArr = {"US", "GB"};
                int length = strArr.length;
                for (int i2 = 0; i2 < length; i2++) {
                    String targetedCountry = strArr[i2];
                    Iterator<TmdbTvInfoResult.ContentRatingsBean.ResultsBean> it2 = contentRatingsBean.getResults().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            TmdbTvInfoResult.ContentRatingsBean.ResultsBean resultsBean = it2.next();
                            if (resultsBean != null && (countryCode = resultsBean.getIso_3166_1()) != null && countryCode.equalsIgnoreCase(targetedCountry) && (rating = resultsBean.getRating()) != null && !rating.isEmpty()) {
                                contentRating = rating;
                                contentRatingCountry = targetedCountry.replace("GB", "UK");
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if (!contentRating.isEmpty()) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (!contentRating.isEmpty() && !contentRatingCountry.isEmpty()) {
            metaStr = metaStr + " • " + contentRatingCountry + ": " + contentRating;
        }
        if (!metaStr.isEmpty()) {
            this.f13821.setText(metaStr);
            this.f13821.setVisibility(0);
        }
        if (this.f13825 != null) {
            if ((this.f13832.getPosterUrl() == null || this.f13832.getPosterUrl().isEmpty()) && result.getPoster_path() != null && !result.getPoster_path().isEmpty()) {
                this.f13832.setPosterUrl(result.getPoster_path());
            }
            if (this.f13832.getPosterUrl() != null && !this.f13832.getPosterUrl().isEmpty()) {
                Glide.m3937((Fragment) this).m3974(this.f13832.getPosterUrl()).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(Color.parseColor("#80111111"))).m25196().m25182().m25209(this.f13825);
                this.f13825.setVisibility(0);
            }
        }
        this.f13819.setVisibility(8);
        this.f13830.findViewById(R.id.showDetailsBlock).setVisibility(0);
        try {
            if (result.getExternal_ids() != null) {
                String imdbId = result.getExternal_ids().getImdb_id();
                if (imdbId != null && imdbId.startsWith(TtmlNode.TAG_TT)) {
                    this.f13832.setImdbId(imdbId);
                }
                int tvdbId = result.getExternal_ids().getTvdb_id();
                if (tvdbId > -1) {
                    this.f13832.setTvdbId(tvdbId);
                }
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        this.f13832.setOriginalName(result.getOriginal_name());
        RxBus.m15709().m15711(new RetrievedTmdbTvInfoEvent(result, this.f13831));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17564() {
        m17556();
        RxBus.m15709().m15711(new RetrievedTmdbTvInfoEvent((TmdbTvInfoResult) null, this.f13831));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17566(TmdbMovieInfoResult result) {
        String countryCode;
        String rating;
        String imdbId = result.getImdb_id();
        if (imdbId != null && imdbId.startsWith(TtmlNode.TAG_TT)) {
            this.f13832.setImdbId(imdbId);
            RxBus.m15709().m15711(new RetrievedImdbIdEvent(imdbId, this.f13831));
        }
        if (result.getOverview() == null || result.getOverview().isEmpty()) {
            this.f13828.setText(I18N.m15706(R.string.no_synopsis));
        } else {
            this.f13828.setText(result.getOverview());
        }
        this.f13828.setVisibility(0);
        StringBuilder metaDataStr = new StringBuilder();
        boolean addedYear = false;
        if (this.f13832.getYear() > 0) {
            metaDataStr.append(this.f13832.getYear());
            addedYear = true;
        }
        boolean addedRuntime = false;
        if (result.getRuntime() > 0) {
            if (addedYear) {
                metaDataStr.append(" • ");
            }
            metaDataStr.append(result.getRuntime()).append("min");
            addedRuntime = true;
        }
        if (result.getGenres() != null && !result.getGenres().isEmpty()) {
            if (addedRuntime || addedYear) {
                metaDataStr.append(" • ");
            }
            SparseArray<String> genreMap = MovieCategoryHelper.m16000();
            for (TmdbMovieInfoResult.GenresBean genre : result.getGenres()) {
                if (genre.getId() >= 0 && genreMap.indexOfKey(genre.getId()) > -1) {
                    metaDataStr.append(genreMap.get(genre.getId()));
                    metaDataStr.append(", ");
                }
            }
        }
        String metaStr = metaDataStr.toString();
        if (!metaStr.isEmpty() && metaStr.endsWith(", ")) {
            metaStr = metaStr.substring(0, metaStr.length() - 2);
        }
        String contentRating = "";
        String contentRatingCountry = "";
        try {
            TmdbMovieInfoResult.ReleasesBean releasesBean = result.getReleases();
            if (releasesBean != null && releasesBean.getCountries() != null) {
                String[] strArr = {"US", "GB"};
                int length = strArr.length;
                for (int i = 0; i < length; i++) {
                    String targetedCountry = strArr[i];
                    Iterator<TmdbMovieInfoResult.ReleasesBean.CountriesBean> it2 = releasesBean.getCountries().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            TmdbMovieInfoResult.ReleasesBean.CountriesBean countriesBean = it2.next();
                            if (countriesBean != null && (countryCode = countriesBean.getIso_3166_1()) != null && countryCode.equalsIgnoreCase(targetedCountry) && (rating = countriesBean.getCertification()) != null && !rating.isEmpty()) {
                                contentRating = rating;
                                contentRatingCountry = targetedCountry.replace("GB", "UK");
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if (!contentRating.isEmpty()) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (!contentRating.isEmpty() && !contentRatingCountry.isEmpty()) {
            metaStr = metaStr + " • " + contentRatingCountry + ": " + contentRating;
        }
        if (!metaStr.isEmpty()) {
            this.f13821.setText(metaStr);
            this.f13821.setVisibility(0);
        }
        if (this.f13825 != null) {
            if ((this.f13832.getPosterUrl() == null || this.f13832.getPosterUrl().isEmpty()) && result.getPoster_path() != null && !result.getPoster_path().isEmpty()) {
                this.f13832.setPosterUrl(result.getPoster_path());
            }
            if (this.f13832.getPosterUrl() != null && !this.f13832.getPosterUrl().isEmpty()) {
                Glide.m3937((Fragment) this).m3974(this.f13832.getPosterUrl()).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(Color.parseColor("#80111111"))).m25196().m25182().m25209(this.f13825);
                this.f13825.setVisibility(0);
            }
        }
        this.f13819.setVisibility(8);
        this.f13830.findViewById(R.id.showDetailsBlock).setVisibility(0);
        this.f13832.setOriginalName(result.getOriginal_title());
        RxBus.m15709().m15711(new RetrievedTmdbMovieInfoEvent(result, this.f13831));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17561() {
        m17556();
        RxBus.m15709().m15711(new RetrievedTmdbMovieInfoEvent((TmdbMovieInfoResult) null, this.f13831));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m17556() {
        this.f13828.setText(NetworkUtils.m17799() ? I18N.m15706(R.string.no_data) : I18N.m15706(R.string.no_internet));
        this.f13828.setTypeface(TypefaceUtils.m17838());
        this.f13828.setVisibility(0);
        if (!(this.f13825 == null || this.f13832.getPosterUrl() == null || this.f13832.getPosterUrl().isEmpty())) {
            Glide.m3937((Fragment) this).m3974(this.f13832.getPosterUrl()).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(Color.parseColor("#80111111"))).m25196().m25182().m25209(this.f13825);
            this.f13825.setVisibility(0);
        }
        this.f13819.setVisibility(8);
        this.f13830.findViewById(R.id.showDetailsBlock).setVisibility(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17565(MediaRatingsModel ratingsModel) {
        String ratingsText;
        String imdbRating = ratingsModel.getImdbRating();
        String tomatoesRating = ratingsModel.getRottenTomatoesRating();
        if (imdbRating != null && tomatoesRating != null) {
            ratingsText = "IMDb : " + imdbRating + "/10 | TomatoMeter : " + tomatoesRating + "%";
        } else if (imdbRating != null) {
            ratingsText = "IMDb : " + imdbRating + "/10";
        } else if (tomatoesRating != null) {
            ratingsText = "TomatoMeter : " + tomatoesRating + "%";
        } else {
            ratingsText = I18N.m15706(R.string.no_ratings);
        }
        this.f13820.setText(ratingsText);
        this.f13820.setVisibility(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17568(String youtubeId) {
        this.f13824 = youtubeId;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m17563() {
        Button btnPlayTrailer = (Button) this.f13830.findViewById(R.id.btnPlayTrailer);
        if (btnPlayTrailer != null && this.f13824 != null && !this.f13824.isEmpty()) {
            btnPlayTrailer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    YouTubeUtils.m17840(MediaDetailsFragment.this.getActivity(), MediaDetailsFragment.this.f13824);
                }
            });
            btnPlayTrailer.setVisibility(0);
            this.f13822 = true;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m17562() {
        return this.f13822;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m17560() {
        return this.f13823;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m17559() {
        this.f13820.setText(I18N.m15706(R.string.no_ratings));
        this.f13820.setVisibility(0);
    }
}
