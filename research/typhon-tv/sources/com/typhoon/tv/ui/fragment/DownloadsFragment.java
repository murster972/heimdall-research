package com.typhoon.tv.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.event.DownloadCompleteEvent;
import com.typhoon.tv.model.download.DownloadItem;
import com.typhoon.tv.ui.GridLayoutManagerWrapper;
import com.typhoon.tv.ui.GridSpacingItemDecoration;
import com.typhoon.tv.ui.LinearLayoutManagerWrapper;
import com.typhoon.tv.ui.activity.DownloadsActivity;
import com.typhoon.tv.ui.adapter.DownloadItemAdapter;
import com.typhoon.tv.ui.viewholder.DownloadItemViewHolder;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.Downloader;
import java.util.ArrayList;
import java.util.List;
import rx.Subscription;
import rx.functions.Action1;

public class DownloadsFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public DownloadItemAdapter f13803;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DownloadItemViewHolder.OnCardClickListener f13804 = new DownloadItemViewHolder.OnCardClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17553(View view, ImageButton imageButton, final int i) {
            if (i != -1) {
                final DownloadItem r0 = DownloadsFragment.this.f13803.m17503(i);
                if (view.getId() == R.id.ivBanner || view.getId() == R.id.overlay || view.getId() == R.id.ivPlay) {
                    DownloadsFragment.this.m17547(r0);
                    return;
                }
                PopupMenu popupMenu = new PopupMenu(DownloadsFragment.this.getActivity(), imageButton);
                popupMenu.getMenuInflater().inflate(R.menu.menu_manage_download_popup, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_play:
                                DownloadsFragment.this.m17547(r0);
                                break;
                            case R.id.action_remove:
                                DownloadsFragment.this.m17548(r0, i, false);
                                break;
                            case R.id.action_remove_with_files:
                                DownloadsFragment.this.m17548(r0, i, true);
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    private Subscription f13805;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<DownloadItem> m17543() {
        return m17552() == 0 ? Downloader.m17783() : Downloader.m17779();
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m17544() {
        this.f13803.m17501();
        this.f13803.m17508(m17543());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DownloadsFragment m17546(int i) {
        DownloadsFragment downloadsFragment = new DownloadsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(VastExtensionXmlManager.TYPE, i);
        downloadsFragment.setArguments(bundle);
        return downloadsFragment;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17547(DownloadItem downloadItem) {
        if (getActivity() instanceof DownloadsActivity) {
            ((DownloadsActivity) getActivity()).m16859(downloadItem);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17548(DownloadItem downloadItem, int i, boolean z) {
        if (z || (downloadItem.isDownloading() && m17552() == 0)) {
            Downloader.m17785(getActivity(), downloadItem.getDownloadId());
        }
        TVApplication.m6287().m6298(downloadItem.getDownloadId());
        if (this.f13803.getItemCount() > i) {
            this.f13803.m17502(i);
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        m17544();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_downloads, viewGroup, false);
        SuperRecyclerView superRecyclerView = (SuperRecyclerView) inflate.findViewById(R.id.rvDownloadsInProgressList);
        if (DeviceUtils.m6389(new boolean[0]) || getResources().getConfiguration().orientation == 2) {
            superRecyclerView.setLayoutManager(new GridLayoutManagerWrapper(getActivity(), 2));
            superRecyclerView.m26368((RecyclerView.ItemDecoration) new GridSpacingItemDecoration(2, 0, true));
        } else {
            superRecyclerView.setLayoutManager(new LinearLayoutManagerWrapper(getActivity(), 1, false));
        }
        superRecyclerView.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        superRecyclerView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                DownloadsFragment.this.m17544();
            }
        });
        this.f13803 = new DownloadItemAdapter(new ArrayList());
        this.f13803.m17506(this.f13804);
        superRecyclerView.setAdapter(this.f13803);
        if (this.f13805 != null && !this.f13805.isUnsubscribed()) {
            this.f13805.unsubscribe();
        }
        this.f13805 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                if ((obj instanceof DownloadCompleteEvent) && !DownloadsFragment.this.isRemoving()) {
                    DownloadsFragment.this.m17544();
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.f13805 != null && !this.f13805.isUnsubscribed()) {
            this.f13805.unsubscribe();
        }
    }

    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        this.f13803.m17506(this.f13804);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17551() {
        final ArrayList<DownloadItem> r0 = this.f13803.m17505();
        if (!getActivity().isFinishing() && !r0.isEmpty()) {
            new MaterialDialog.Builder(getActivity()).m3800((CharSequence) I18N.m15706(R.string.action_remove_all)).m3797((int) R.drawable.ic_delete_white_36dp).m3793((CharSequence) I18N.m15706(R.string.ok)).m3784(17170454).m3790((CharSequence) I18N.m15706(R.string.cancel)).m3788(true).m3799((MaterialDialog.SingleButtonCallback) new MaterialDialog.SingleButtonCallback() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17554(MaterialDialog materialDialog, DialogAction dialogAction) {
                    for (DownloadItem downloadItem : r0) {
                        if (DownloadsFragment.this.m17552() == 0) {
                            Downloader.m17785(DownloadsFragment.this.getActivity(), downloadItem.getDownloadId());
                        }
                        TVApplication.m6287().m6298(downloadItem.getDownloadId());
                    }
                    DownloadsFragment.this.f13803.m17501();
                }
            }).m3795();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m17552() {
        if (getArguments() == null) {
            return 0;
        }
        return getArguments().getInt(VastExtensionXmlManager.TYPE, 0);
    }
}
