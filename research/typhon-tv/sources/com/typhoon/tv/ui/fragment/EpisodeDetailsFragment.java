package com.typhoon.tv.ui.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.tv.TvEpisodeInfo;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.TypefaceUtils;

public class EpisodeDetailsFragment extends Fragment {

    /* renamed from: 连任  reason: contains not printable characters */
    private ImageView f13814;

    /* renamed from: 靐  reason: contains not printable characters */
    private TextView f13815;

    /* renamed from: 麤  reason: contains not printable characters */
    private TextView f13816;

    /* renamed from: 齉  reason: contains not printable characters */
    private TextView f13817;

    /* renamed from: 龘  reason: contains not printable characters */
    private TvEpisodeInfo f13818;

    /* renamed from: 龘  reason: contains not printable characters */
    public static EpisodeDetailsFragment m17555(TvEpisodeInfo tvEpisodeInfo) {
        EpisodeDetailsFragment episodeDetailsFragment = new EpisodeDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("episodeInfo", tvEpisodeInfo);
        episodeDetailsFragment.setArguments(bundle);
        return episodeDetailsFragment;
    }

    public void onActivityCreated(Bundle bundle) {
        String r0;
        super.onActivityCreated(bundle);
        TextView textView = this.f13815;
        if (!this.f13818.getName().isEmpty()) {
            r0 = this.f13818.getName();
        } else {
            r0 = I18N.m15707(R.string.episode, Integer.valueOf(this.f13818.getEpisode()));
        }
        textView.setText(r0);
        this.f13815.setTypeface(TypefaceUtils.m17839());
        if (this.f13818.getAirDate() == null || this.f13818.getAirDate().isEmpty()) {
            this.f13817.setText(I18N.m15706(R.string.unknown));
        } else {
            this.f13817.setText(this.f13818.getAirDate());
        }
        if (TVApplication.m6285().getBoolean("pref_hide_episode_synopsis", false)) {
            this.f13816.setText(I18N.m15706(R.string.synopsis_is_hidden));
            this.f13816.setTypeface(TypefaceUtils.m17838());
        } else if (this.f13818.getOverview() == null || this.f13818.getOverview().isEmpty()) {
            this.f13816.setText(I18N.m15706(R.string.no_synopsis));
            this.f13816.setTypeface(TypefaceUtils.m17838());
        } else {
            this.f13816.setText(this.f13818.getOverview());
        }
        if (TVApplication.m6285().getBoolean("pref_hide_episode_image", false) || this.f13818.getBannerUrl() == null || this.f13818.getBannerUrl().isEmpty()) {
            this.f13814.setScaleType(ImageView.ScaleType.CENTER);
            Glide.m3937((Fragment) this).m3973(Integer.valueOf(R.drawable.ic_ttv_black_white)).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(Color.parseColor("#80111111"))).m25196().m25182().m25209(this.f13814);
            return;
        }
        Glide.m3937((Fragment) this).m3974(this.f13818.getBannerUrl()).m25191(DiskCacheStrategy.SOURCE).m25198().m25187((Drawable) new ColorDrawable(Color.parseColor("#80111111"))).m25196().m25209(this.f13814);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(DeviceUtils.m6389(new boolean[0]) ? R.layout.fragment_episode_details_tv : R.layout.fragment_episode_details, viewGroup, false);
        this.f13818 = (TvEpisodeInfo) getArguments().getParcelable("episodeInfo");
        this.f13815 = (TextView) inflate.findViewById(R.id.tvEpisodeDetailsTitle);
        this.f13817 = (TextView) inflate.findViewById(R.id.tvEpisodeDetailsDate);
        this.f13816 = (TextView) inflate.findViewById(R.id.tvEpisodeInfoSynopsis);
        this.f13814 = (ImageView) inflate.findViewById(R.id.ivEpisodeDetailsBanner);
        return inflate;
    }
}
