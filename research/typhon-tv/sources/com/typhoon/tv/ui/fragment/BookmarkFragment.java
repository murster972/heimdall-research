package com.typhoon.tv.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.event.UpdateBookmarkEvent;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.IBookmarkPresenter;
import com.typhoon.tv.presenter.impl.BookmarkPresenterImpl;
import com.typhoon.tv.ui.activity.MediaDetailsActivity;
import com.typhoon.tv.ui.activity.base.BaseActivity;
import com.typhoon.tv.ui.adapter.MediaListAdapter;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.widget.AutofitSuperRecyclerView;
import com.typhoon.tv.view.IBookmarkView;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import java.util.ArrayList;
import retrofit2.Callback;
import rx.Subscription;
import rx.functions.Action1;

public class BookmarkFragment extends Fragment implements IBookmarkView {

    /* renamed from: 靐  reason: contains not printable characters */
    private final MediaCardViewHolder.OnCardClickListener f13791 = new MediaCardViewHolder.OnCardClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17540(int i) {
            Intent intent = new Intent(BookmarkFragment.this.getActivity(), MediaDetailsActivity.class);
            intent.putExtra("mediaInfo", (MediaInfo) BookmarkFragment.this.f13794.m17495(i));
            BookmarkFragment.this.getActivity().startActivity(intent);
        }
    };

    /* renamed from: 麤  reason: contains not printable characters */
    private Subscription f13792;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public IBookmarkPresenter f13793;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaListAdapter f13794;

    /* renamed from: 龘  reason: contains not printable characters */
    public static BookmarkFragment m17537(int i) {
        BookmarkFragment bookmarkFragment = new BookmarkFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(VastExtensionXmlManager.TYPE, i);
        bookmarkFragment.setArguments(bundle);
        return bookmarkFragment;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.f13793.m16126(m17538());
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f13793 = new BookmarkPresenterImpl(this);
        View inflate = layoutInflater.inflate(R.layout.fragment_bookmark, viewGroup, false);
        AutofitSuperRecyclerView autofitSuperRecyclerView = (AutofitSuperRecyclerView) inflate.findViewById(R.id.rvBookmarkList);
        autofitSuperRecyclerView.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        autofitSuperRecyclerView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                BookmarkFragment.this.f13793.m16126(BookmarkFragment.this.m17538());
            }
        });
        this.f13794 = new MediaListAdapter(new ArrayList());
        this.f13794.m17497(this.f13791);
        this.f13794.m17498((MediaCardViewHolder.OnCardLongClickListener) new MediaCardViewHolder.OnCardLongClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17541(View view, int i) {
                if (BookmarkFragment.this.getActivity() instanceof BaseActivity) {
                    final MediaInfo mediaInfo = (MediaInfo) BookmarkFragment.this.f13794.m17495(i);
                    final boolean r2 = TVApplication.m6287().m6319(mediaInfo);
                    String[] strArr = new String[1];
                    strArr[0] = I18N.m15706(r2 ? R.string.action_remove_from_bookmark : R.string.action_add_to_bookmark);
                    new AlertDialog.Builder(BookmarkFragment.this.getActivity()).m536((CharSequence) mediaInfo.getNameAndYear()).m540((CharSequence[]) strArr, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            boolean z = true;
                            if (BookmarkFragment.this.getActivity() instanceof BaseActivity) {
                                switch (i) {
                                    case 0:
                                        if (r2) {
                                            TVApplication.m6287().m6304(mediaInfo);
                                        } else {
                                            TVApplication.m6287().m6299(mediaInfo);
                                        }
                                        try {
                                            BaseActivity baseActivity = (BaseActivity) BookmarkFragment.this.getActivity();
                                            MediaInfo mediaInfo = mediaInfo;
                                            if (r2) {
                                                z = false;
                                            }
                                            baseActivity.m17299(mediaInfo, z, true, (Callback<SyncResponse>) null);
                                        } catch (Exception e) {
                                            Logger.m6281((Throwable) e, new boolean[0]);
                                        }
                                        RxBus.m15709().m15711(new UpdateBookmarkEvent());
                                        return;
                                    default:
                                        return;
                                }
                            }
                        }
                    }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (!BookmarkFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                                dialogInterface.dismiss();
                            }
                        }
                    }).m528();
                }
            }
        });
        autofitSuperRecyclerView.setAdapter(this.f13794);
        this.f13792 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                if ((obj instanceof UpdateBookmarkEvent) && !BookmarkFragment.this.isRemoving() && BookmarkFragment.this.f13793 != null) {
                    BookmarkFragment.this.f13793.m16126(BookmarkFragment.this.m17538());
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        this.f13793.m16125();
        this.f13793 = null;
        if (this.f13792 != null && !this.f13792.isUnsubscribed()) {
            this.f13792.unsubscribe();
        }
        this.f13792 = null;
        super.onDestroyView();
    }

    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        this.f13794.m17497(this.f13791);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m17538() {
        if (getArguments() == null) {
            return 0;
        }
        return getArguments().getInt(VastExtensionXmlManager.TYPE, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17539(ArrayList<MediaInfo> arrayList) {
        this.f13794.m17492();
        this.f13794.m17500(arrayList);
    }
}
