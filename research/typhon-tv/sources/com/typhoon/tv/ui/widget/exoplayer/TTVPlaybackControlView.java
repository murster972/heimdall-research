package com.typhoon.tv.ui.widget.exoplayer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.SystemClock;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity;
import com.typhoon.tv.ui.widget.BatteryLevelView;
import com.typhoon.tv.utils.DeviceUtils;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Locale;

public class TTVPlaybackControlView extends FrameLayout {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ControlDispatcher f13945 = new ControlDispatcher() {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m17714(ExoPlayer exoPlayer, int i, long j) {
            exoPlayer.seekTo(i, j);
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m17715(ExoPlayer exoPlayer, boolean z) {
            exoPlayer.setPlayWhenReady(z);
            return true;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final View f13946;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private BatteryLevelView f13947;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final View f13948;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private TextView f13949;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final View f13950;
    /* access modifiers changed from: private */

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public TextView f13951;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final TimeBar f13952;
    /* access modifiers changed from: private */

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public boolean f13953;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public final StringBuilder f13954;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private final BroadcastReceiver f13955;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public VisibilityListener f13956;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private Animation f13957;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final TextView f13958;
    /* access modifiers changed from: private */

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final Runnable f13959;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public VisibilityAnimationListener f13960;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private Animation f13961;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final Timeline.Window f13962;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public ExoPlayer f13963;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public ControlDispatcher f13964;

    /* renamed from: ˏ  reason: contains not printable characters */
    private OnControllerTouchListener f13965;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final View f13966;
    /* access modifiers changed from: private */

    /* renamed from: י  reason: contains not printable characters */
    public OnControllerContentWrapperTouchListener f13967;

    /* renamed from: ـ  reason: contains not printable characters */
    private ExoPlayerActivity.OnUserSeekListener f13968;
    /* access modifiers changed from: private */

    /* renamed from: ــ  reason: contains not printable characters */
    public ExoClickListener f13969;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public final View f13970;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final TextView f13971;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f13972;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f13973;
    /* access modifiers changed from: private */

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public long f13974;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f13975;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f13976;
    /* access modifiers changed from: private */

    /* renamed from: ᵢ  reason: contains not printable characters */
    public boolean f13977;
    /* access modifiers changed from: private */

    /* renamed from: ⁱ  reason: contains not printable characters */
    public final Runnable f13978;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final View f13979;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ComponentListener f13980;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final View f13981;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final View f13982;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int f13983;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Formatter f13984;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final Timeline.Period f13985;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f13986;

    private final class ComponentListener implements View.OnClickListener, Player.EventListener, TimeBar.OnScrubListener {
        private ComponentListener() {
        }

        public void onClick(View view) {
            if (TTVPlaybackControlView.this.f13963 != null) {
                if (TTVPlaybackControlView.this.f13981 == view) {
                    TTVPlaybackControlView.this.m17677();
                } else if (TTVPlaybackControlView.this.f13982 == view) {
                    TTVPlaybackControlView.this.m17675();
                } else if (TTVPlaybackControlView.this.f13948 == view) {
                    TTVPlaybackControlView.this.m17658();
                } else if (TTVPlaybackControlView.this.f13950 == view) {
                    TTVPlaybackControlView.this.m17664();
                } else if (TTVPlaybackControlView.this.f13979 == view) {
                    TTVPlaybackControlView.this.f13964.m17717(TTVPlaybackControlView.this.f13963, true);
                } else if (TTVPlaybackControlView.this.f13946 == view) {
                    TTVPlaybackControlView.this.f13964.m17717(TTVPlaybackControlView.this.f13963, false);
                }
            }
            if (TTVPlaybackControlView.this.f13951 == view && TTVPlaybackControlView.this.f13969 != null && !TTVPlaybackControlView.this.f13969.m17718(view, TTVPlaybackControlView.this.f13953)) {
                try {
                    TTVPlaybackControlView.this.f13951.setEnabled(false);
                } catch (Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }
            }
            TTVPlaybackControlView.this.m17709(view);
        }

        public void onLoadingChanged(boolean z) {
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }

        public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        }

        public void onPlayerStateChanged(boolean z, int i) {
            TTVPlaybackControlView.this.m17689();
            TTVPlaybackControlView.this.m17655();
            if (i == 3) {
                TTVPlaybackControlView.this.m17698(true, TTVPlaybackControlView.this.f13970);
            }
        }

        public void onPositionDiscontinuity() {
            TTVPlaybackControlView.this.m17684();
            TTVPlaybackControlView.this.m17655();
        }

        public void onRepeatModeChanged(int i) {
        }

        public void onScrubMove(TimeBar timeBar, long j) {
            if (TTVPlaybackControlView.this.f13958 != null) {
                TTVPlaybackControlView.this.f13958.setText(Util.getStringForTime(TTVPlaybackControlView.this.f13954, TTVPlaybackControlView.this.f13984, j));
            }
        }

        public void onScrubStart(TimeBar timeBar, long j) {
            TTVPlaybackControlView.this.removeCallbacks(TTVPlaybackControlView.this.f13959);
            boolean unused = TTVPlaybackControlView.this.f13977 = true;
        }

        public void onScrubStop(TimeBar timeBar, long j, boolean z) {
            boolean unused = TTVPlaybackControlView.this.f13977 = false;
            if (!z && TTVPlaybackControlView.this.f13963 != null) {
                TTVPlaybackControlView.this.m17685(j);
            }
            if (TTVPlaybackControlView.this.f13963 != null && TTVPlaybackControlView.this.f13963.getPlayWhenReady() && TTVPlaybackControlView.this.m17712()) {
                TTVPlaybackControlView.this.m17708(true);
            }
        }

        public void onTimelineChanged(Timeline timeline, Object obj) {
            TTVPlaybackControlView.this.m17684();
            TTVPlaybackControlView.this.m17652();
            TTVPlaybackControlView.this.m17655();
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        }
    }

    public interface ControlDispatcher {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m17716(ExoPlayer exoPlayer, int i, long j);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m17717(ExoPlayer exoPlayer, boolean z);
    }

    public interface ExoClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m17718(View view, boolean z);
    }

    public interface OnControllerContentWrapperTouchListener {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m17719(MotionEvent motionEvent);
    }

    public interface OnControllerTouchListener {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m17720(MotionEvent motionEvent);
    }

    public interface VisibilityAnimationListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17721(int i);
    }

    public interface VisibilityListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17722(int i);
    }

    static {
        ExoPlayerLibraryInfo.registerModule("goog.exo.ui");
    }

    public TTVPlaybackControlView(Context context) {
        this(context, (AttributeSet) null);
    }

    public TTVPlaybackControlView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TTVPlaybackControlView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, attributeSet);
    }

    public TTVPlaybackControlView(Context context, AttributeSet attributeSet, int i, AttributeSet attributeSet2) {
        super(context, attributeSet, i);
        this.f13978 = new Runnable() {
            public void run() {
                TTVPlaybackControlView.this.m17655();
            }
        };
        this.f13955 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null && action.equals("android.intent.action.TIME_TICK")) {
                    TTVPlaybackControlView.this.m17657();
                }
            }
        };
        this.f13953 = true;
        this.f13959 = new Runnable() {
            public void run() {
                TTVPlaybackControlView.this.m17707(new boolean[0]);
            }
        };
        int i2 = R.layout.ttv_playback_control_view;
        this.f13983 = 5000;
        this.f13986 = 15000;
        this.f13972 = 5000;
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, R.styleable.PlaybackControlView, 0, 0);
            try {
                this.f13983 = obtainStyledAttributes.getInt(3, this.f13983);
                this.f13986 = obtainStyledAttributes.getInt(1, this.f13986);
                this.f13972 = obtainStyledAttributes.getInt(4, this.f13972);
                i2 = obtainStyledAttributes.getResourceId(0, R.layout.ttv_playback_control_view);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.f13985 = new Timeline.Period();
        this.f13962 = new Timeline.Window();
        this.f13954 = new StringBuilder();
        this.f13984 = new Formatter(this.f13954, Locale.getDefault());
        this.f13980 = new ComponentListener();
        this.f13964 = f13945;
        LayoutInflater.from(context).inflate(i2, this);
        setDescendantFocusability(262144);
        this.f13971 = (TextView) findViewById(R.id.exo_duration);
        this.f13958 = (TextView) findViewById(R.id.exo_position);
        this.f13952 = (TimeBar) findViewById(R.id.exo_progress);
        if (this.f13952 != null) {
            this.f13952.setListener(this.f13980);
            if (DeviceUtils.m6389(new boolean[0])) {
                this.f13952.setKeyCountIncrement(40);
            }
        }
        this.f13979 = findViewById(R.id.exo_play);
        if (this.f13979 != null) {
            this.f13979.setOnClickListener(this.f13980);
        }
        this.f13946 = findViewById(R.id.exo_pause);
        if (this.f13946 != null) {
            this.f13946.setOnClickListener(this.f13980);
        }
        this.f13982 = findViewById(R.id.exo_prev);
        if (this.f13982 != null) {
            this.f13982.setOnClickListener(this.f13980);
        }
        this.f13981 = findViewById(R.id.exo_next);
        if (this.f13981 != null) {
            this.f13981.setOnClickListener(this.f13980);
        }
        this.f13950 = findViewById(R.id.exo_rew);
        if (this.f13950 != null) {
            this.f13950.setOnClickListener(this.f13980);
        }
        this.f13948 = findViewById(R.id.exo_ffwd);
        if (this.f13948 != null) {
            this.f13948.setOnClickListener(this.f13980);
        }
        this.f13966 = findViewById(R.id.exo_cc);
        if (this.f13966 != null) {
            m17698(false, this.f13966);
            this.f13966.setOnClickListener(this.f13980);
        }
        this.f13970 = findViewById(R.id.exo_resize_mode);
        if (this.f13970 != null) {
            m17698(false, this.f13970);
            this.f13970.setOnClickListener(this.f13980);
        }
        this.f13951 = (TextView) findViewById(R.id.displayName);
        if (this.f13951 != null) {
            this.f13951.setOnClickListener(this.f13980);
            if (DeviceUtils.m6389(new boolean[0])) {
                this.f13951.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.centerContentWrapper);
        if (frameLayout != null) {
            frameLayout.setOnClickListener(this.f13980);
            frameLayout.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return TTVPlaybackControlView.this.f13967 != null && TTVPlaybackControlView.this.f13967.m17719(motionEvent);
                }
            });
        }
        this.f13947 = (BatteryLevelView) findViewById(R.id.battery);
        this.f13949 = (TextView) findViewById(R.id.localTime);
        m17671();
        m17657();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m17652() {
        if (this.f13963 != null) {
            this.f13976 = this.f13975 && m17700(this.f13963.getCurrentTimeline(), this.f13985);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m17655() {
        long j;
        if (m17712() && this.f13973) {
            long j2 = 0;
            long j3 = 0;
            long j4 = 0;
            if (!(this.f13963 == null || this.f13963.getCurrentTimeline() == null)) {
                if (this.f13976) {
                    Timeline currentTimeline = this.f13963.getCurrentTimeline();
                    int windowCount = currentTimeline.getWindowCount();
                    int currentPeriodIndex = this.f13963.getCurrentPeriodIndex();
                    long j5 = 0;
                    long j6 = 0;
                    long j7 = 0;
                    for (int i = 0; i < windowCount; i++) {
                        currentTimeline.getWindow(i, this.f13962);
                        for (int i2 = this.f13962.firstPeriodIndex; i2 <= this.f13962.lastPeriodIndex; i2++) {
                            long durationUs = this.f13985.getDurationUs();
                            Assertions.checkState(durationUs != C.TIME_UNSET);
                            long j8 = durationUs;
                            if (i2 == this.f13962.firstPeriodIndex) {
                                j8 -= this.f13962.positionInFirstPeriodUs;
                            }
                            if (i < currentPeriodIndex) {
                                j5 += j8;
                                j6 += j8;
                            }
                            j7 += j8;
                        }
                    }
                    long usToMs = C.usToMs(j5);
                    long usToMs2 = C.usToMs(j6);
                    j4 = C.usToMs(j7);
                    j2 = usToMs + this.f13963.getCurrentPosition();
                    j3 = usToMs2 + this.f13963.getBufferedPosition();
                } else {
                    j2 = this.f13963.getCurrentPosition();
                    j3 = this.f13963.getBufferedPosition();
                    j4 = this.f13963.getDuration();
                }
            }
            if (this.f13971 != null) {
                this.f13971.setText(Util.getStringForTime(this.f13954, this.f13984, j4));
            }
            if (this.f13958 != null && !this.f13977) {
                this.f13958.setText(Util.getStringForTime(this.f13954, this.f13984, j2));
            }
            if (this.f13952 != null) {
                this.f13952.setPosition(j2);
                this.f13952.setBufferedPosition(j3);
                this.f13952.setDuration(j4);
            }
            removeCallbacks(this.f13978);
            int playbackState = this.f13963 == null ? 1 : this.f13963.getPlaybackState();
            if (playbackState != 1 && playbackState != 4) {
                if (!this.f13963.getPlayWhenReady() || playbackState != 3) {
                    j = 1000;
                } else {
                    float f = this.f13963.getPlaybackParameters().speed;
                    if (f <= 0.1f) {
                        j = 1000;
                    } else if (f <= 5.0f) {
                        long max = (long) (1000 / Math.max(1, Math.round(1.0f / f)));
                        long j9 = max - (j2 % max);
                        if (j9 < max / 5) {
                            j9 += max;
                        }
                        j = f == 1.0f ? j9 : (long) (((float) j9) / f);
                    } else {
                        j = 200;
                    }
                }
                postDelayed(this.f13978, j);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m17657() {
        if (this.f13949 != null) {
            Calendar instance = Calendar.getInstance();
            int i = instance.get(11);
            int i2 = instance.get(12);
            String str = (i >= 10 ? "" + Integer.toString(i) : "" + "0" + i) + ":";
            this.f13949.setText(i2 >= 10 ? str + Integer.toString(i2) : str + "0" + i2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m17658() {
        if (this.f13986 > 0 && this.f13963 != null) {
            long duration = this.f13963.getDuration();
            long currentPosition = this.f13963.getCurrentPosition() + ((long) this.f13986);
            if (duration != C.TIME_UNSET) {
                currentPosition = Math.min(currentPosition, duration);
            }
            m17694(currentPosition);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m17661() {
        getContext().registerReceiver(this.f13955, new IntentFilter("android.intent.action.TIME_TICK"));
    }

    /* access modifiers changed from: private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m17664() {
        if (this.f13983 > 0 && this.f13963 != null) {
            m17694(Math.max(this.f13963.getCurrentPosition() - ((long) this.f13983), 0));
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m17671() {
        int i = 8;
        if (this.f13947 != null) {
            if (DeviceUtils.m6389(new boolean[0])) {
                this.f13947.setVisibility(8);
            } else {
                this.f13947.setVisibility(this.f13953 ? 8 : 0);
            }
        }
        if (this.f13949 != null) {
            TextView textView = this.f13949;
            if (!this.f13953) {
                i = 0;
            }
            textView.setVisibility(i);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m17675() {
        Timeline currentTimeline = this.f13963.getCurrentTimeline();
        if (currentTimeline != null && !currentTimeline.isEmpty()) {
            int currentWindowIndex = this.f13963.getCurrentWindowIndex();
            currentTimeline.getWindow(currentWindowIndex, this.f13962);
            if (currentWindowIndex <= 0 || (this.f13963.getCurrentPosition() > 3000 && (!this.f13962.isDynamic || this.f13962.isSeekable))) {
                m17694(0);
            } else {
                m17693(currentWindowIndex - 1, (long) C.TIME_UNSET);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m17677() {
        Timeline currentTimeline = this.f13963.getCurrentTimeline();
        if (currentTimeline != null && !currentTimeline.isEmpty()) {
            int currentWindowIndex = this.f13963.getCurrentWindowIndex();
            if (currentWindowIndex < currentTimeline.getWindowCount() - 1) {
                m17693(currentWindowIndex + 1, (long) C.TIME_UNSET);
            } else if (currentTimeline.getWindow(currentWindowIndex, this.f13962, false).isDynamic) {
                m17693(currentWindowIndex, (long) C.TIME_UNSET);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m17684() {
        boolean z = true;
        if (m17712() && this.f13973) {
            Timeline currentTimeline = this.f13963 != null ? this.f13963.getCurrentTimeline() : null;
            boolean z2 = false;
            boolean z3 = false;
            boolean z4 = false;
            if (currentTimeline != null && !currentTimeline.isEmpty()) {
                int currentWindowIndex = this.f13963.getCurrentWindowIndex();
                currentTimeline.getWindow(currentWindowIndex, this.f13962);
                z2 = this.f13962.isSeekable;
                z3 = z2 || !this.f13962.isDynamic || currentTimeline.getPreviousWindowIndex(currentWindowIndex, this.f13963.getRepeatMode()) != -1;
                z4 = this.f13962.isDynamic || currentTimeline.getNextWindowIndex(currentWindowIndex, this.f13963.getRepeatMode()) != -1;
            }
            m17698(z3, this.f13982);
            m17698(z4, this.f13981);
            m17698(this.f13986 > 0 && z2, this.f13948);
            if (this.f13983 <= 0 || !z2) {
                z = false;
            }
            m17698(z, this.f13950);
            if (this.f13952 != null) {
                this.f13952.setEnabled(z2);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17685(long j) {
        if (!this.f13976 || this.f13963.getCurrentTimeline() == null) {
            m17694(j);
            return;
        }
        Timeline currentTimeline = this.f13963.getCurrentTimeline();
        int windowCount = currentTimeline.getWindowCount();
        long j2 = j;
        for (int i = 0; i < windowCount; i++) {
            currentTimeline.getWindow(i, this.f13962);
            int i2 = this.f13962.firstPeriodIndex;
            while (i2 <= this.f13962.lastPeriodIndex) {
                long durationMs = this.f13985.getDurationMs();
                if (durationMs == C.TIME_UNSET) {
                    throw new IllegalStateException();
                }
                if (i2 == this.f13962.firstPeriodIndex) {
                    durationMs -= this.f13962.getPositionInFirstPeriodMs();
                }
                if (i == windowCount - 1 && i2 == this.f13962.lastPeriodIndex && j2 >= durationMs) {
                    m17693(i, this.f13962.getDurationMs());
                    return;
                } else if (j2 < durationMs) {
                    m17693(i, this.f13985.getPositionInWindowMs() + j2);
                    return;
                } else {
                    j2 -= durationMs;
                    i2++;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m17689() {
        int i = 8;
        boolean z = true;
        if (m17712() && this.f13973) {
            boolean z2 = false;
            boolean z3 = this.f13963 != null && this.f13963.getPlayWhenReady();
            if (this.f13979 != null) {
                z2 = false | (z3 && this.f13979.isFocused());
                this.f13979.setVisibility(z3 ? 8 : 0);
            }
            if (this.f13946 != null) {
                if (z3 || !this.f13946.isFocused()) {
                    z = false;
                }
                z2 |= z;
                View view = this.f13946;
                if (z3) {
                    i = 0;
                }
                view.setVisibility(i);
            }
            if (z2) {
                m17706();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m17691() {
        m17689();
        m17684();
        m17655();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17693(int i, long j) {
        if (!this.f13964.m17716(this.f13963, i, j)) {
            m17655();
        } else if (this.f13968 != null) {
            this.f13968.m17491();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17694(long j) {
        if (this.f13963 != null) {
            m17693(this.f13963.getCurrentWindowIndex(), j);
        }
    }

    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    private void m17695(View view, float f) {
        view.setAlpha(f);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17698(boolean z, View view) {
        if (view != null) {
            view.setEnabled(z);
            m17695(view, z ? 1.0f : 0.3f);
            view.setVisibility(0);
        }
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m17699(int i) {
        return i == 90 || i == 89 || i == 85 || i == 126 || i == 127 || i == 87 || i == 88;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m17700(Timeline timeline, Timeline.Period period) {
        if (timeline.getWindowCount() > 100) {
            return false;
        }
        int periodCount = timeline.getPeriodCount();
        for (int i = 0; i < periodCount; i++) {
            timeline.getPeriod(i, period);
            if (period.durationUs == C.TIME_UNSET) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m17703() {
        getContext().unregisterReceiver(this.f13955);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        boolean z = m17713(keyEvent) || super.dispatchKeyEvent(keyEvent);
        if (z) {
            if (this.f13963 == null || !this.f13963.getPlayWhenReady()) {
                m17711(new boolean[0]);
            } else {
                m17710(true, new boolean[0]);
            }
        }
        return z;
    }

    public ExoPlayer getPlayer() {
        return this.f13963;
    }

    public int getShowTimeoutMs() {
        return this.f13972;
    }

    public Timeline.Window getWindow() {
        return this.f13962;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f13973 = true;
        m17661();
        if (this.f13974 != C.TIME_UNSET) {
            long uptimeMillis = this.f13974 - SystemClock.uptimeMillis();
            if (uptimeMillis > 0) {
                removeCallbacks(this.f13959);
                postDelayed(this.f13959, uptimeMillis);
            }
        }
        m17691();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f13973 = false;
        m17703();
        removeCallbacks(this.f13978);
        removeCallbacks(this.f13959);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.f13965 != null && this.f13965.m17720(motionEvent);
    }

    public void setCcButtonEnabled(boolean z) {
        if (this.f13966 != null) {
            m17698(z, this.f13966);
        }
    }

    public void setCcButtonOnClickListener(View.OnClickListener onClickListener) {
        if (this.f13966 != null) {
            this.f13966.setOnClickListener(onClickListener);
        }
    }

    public void setControlDispatcher(ControlDispatcher controlDispatcher) {
        if (controlDispatcher == null) {
            controlDispatcher = f13945;
        }
        this.f13964 = controlDispatcher;
    }

    public void setDisplayName(String str) {
        if (this.f13951 != null) {
            this.f13951.setText(str);
        }
    }

    public void setFastForwardIncrementMs(int i) {
        this.f13986 = i;
        m17684();
    }

    public void setOnControllerContentWrapperTouchListener(OnControllerContentWrapperTouchListener onControllerContentWrapperTouchListener) {
        this.f13967 = onControllerContentWrapperTouchListener;
    }

    public void setOnControllerTouchListener(OnControllerTouchListener onControllerTouchListener) {
        this.f13965 = onControllerTouchListener;
    }

    public void setOnUserSeekListener(ExoPlayerActivity.OnUserSeekListener onUserSeekListener) {
        this.f13968 = onUserSeekListener;
    }

    public void setPlayer(ExoPlayer exoPlayer) {
        if (this.f13963 != exoPlayer) {
            if (this.f13963 != null) {
                this.f13963.removeListener(this.f13980);
            }
            this.f13963 = exoPlayer;
            if (exoPlayer != null) {
                exoPlayer.addListener(this.f13980);
            }
            m17691();
        }
    }

    public void setPortrait(boolean z) {
        this.f13953 = z;
        m17671();
    }

    public void setPortraitBackListener(ExoClickListener exoClickListener) {
        this.f13969 = exoClickListener;
    }

    public void setResizeModeButtonIconDrawable(int i) {
        if (this.f13970 != null && (this.f13970 instanceof ImageButton)) {
            ((ImageButton) this.f13970).setImageDrawable(ResourcesCompat.getDrawable(getResources(), i, getContext().getTheme()));
        }
    }

    public void setResizeModeButtonOnClickListener(View.OnClickListener onClickListener) {
        if (this.f13970 != null) {
            this.f13970.setOnClickListener(onClickListener);
        }
    }

    public void setRewindIncrementMs(int i) {
        this.f13983 = i;
        m17684();
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        this.f13975 = z;
        m17652();
    }

    public void setShowTimeoutMs(int i) {
        this.f13972 = i;
    }

    public void setTopWrapperTextSize(float f) {
        if (f != Float.MIN_VALUE) {
            if (this.f13951 != null) {
                this.f13951.setTextSize(f);
            }
            if (this.f13949 != null) {
                this.f13949.setTextSize(f);
            }
        }
    }

    public void setVisibilityAnimationStartListener(VisibilityAnimationListener visibilityAnimationListener) {
        this.f13960 = visibilityAnimationListener;
    }

    public void setVisibilityListener(VisibilityListener visibilityListener) {
        this.f13956 = visibilityListener;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17706() {
        boolean z = this.f13963 != null && this.f13963.getPlayWhenReady();
        if (!z && this.f13979 != null) {
            this.f13979.requestFocus();
        } else if (z && this.f13946 != null) {
            this.f13946.requestFocus();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17707(boolean... zArr) {
        boolean z = false;
        if (m17712()) {
            if (zArr != null && zArr.length > 0 && zArr[0]) {
                z = true;
            }
            if (this.f13961 == null || this.f13961.hasEnded() || z) {
                this.f13961 = AnimationUtils.loadAnimation(getContext(), 17432577);
                this.f13961.setDuration(300);
                this.f13961.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        TTVPlaybackControlView.this.setVisibility(8);
                        if (TTVPlaybackControlView.this.f13956 != null) {
                            TTVPlaybackControlView.this.f13956.m17722(TTVPlaybackControlView.this.getVisibility());
                        }
                        TTVPlaybackControlView.this.removeCallbacks(TTVPlaybackControlView.this.f13978);
                        TTVPlaybackControlView.this.removeCallbacks(TTVPlaybackControlView.this.f13959);
                        long unused = TTVPlaybackControlView.this.f13974 = C.TIME_UNSET;
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                        if (TTVPlaybackControlView.this.f13960 != null) {
                            TTVPlaybackControlView.this.f13960.m17721(TTVPlaybackControlView.this.getVisibility());
                        }
                    }
                });
                startAnimation(this.f13961);
                return;
            }
            postDelayed(new Runnable() {
                public void run() {
                    TTVPlaybackControlView.this.m17707(true);
                }
            }, 250);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m17708(boolean... zArr) {
        boolean z = false;
        if (zArr != null && zArr.length > 0 && zArr[0]) {
            z = true;
        }
        removeCallbacks(this.f13959);
        if (z) {
            this.f13974 = SystemClock.uptimeMillis() + ((long) this.f13972);
            postDelayed(this.f13959, 3000);
        } else if (this.f13972 > 0) {
            this.f13974 = SystemClock.uptimeMillis() + ((long) this.f13972);
            if (this.f13973) {
                postDelayed(this.f13959, (long) this.f13972);
            }
        } else {
            this.f13974 = C.TIME_UNSET;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17709(View view) {
        if (this.f13963 != null) {
            removeCallbacks(this.f13959);
            if (this.f13963.getPlayWhenReady() && this.f13946 != view && this.f13951 != view) {
                m17708(true);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17710(final boolean z, boolean... zArr) {
        boolean z2 = true;
        if (!m17712()) {
            if (zArr == null || zArr.length <= 0 || !zArr[0]) {
                z2 = false;
            }
            if (this.f13957 == null || this.f13957.hasEnded() || z2) {
                this.f13957 = AnimationUtils.loadAnimation(getContext(), 17432576);
                this.f13957.setDuration(300);
                this.f13957.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        TTVPlaybackControlView.this.setVisibility(0);
                        if (TTVPlaybackControlView.this.f13956 != null) {
                            TTVPlaybackControlView.this.f13956.m17722(TTVPlaybackControlView.this.getVisibility());
                        }
                        TTVPlaybackControlView.this.m17691();
                        TTVPlaybackControlView.this.m17706();
                        TTVPlaybackControlView.this.m17708(z);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                        if (TTVPlaybackControlView.this.f13960 != null) {
                            TTVPlaybackControlView.this.f13960.m17721(TTVPlaybackControlView.this.getVisibility());
                        }
                    }
                });
                startAnimation(this.f13957);
                return;
            }
            postDelayed(new Runnable() {
                public void run() {
                    TTVPlaybackControlView.this.m17710(z, true);
                }
            }, 250);
            return;
        }
        m17708(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17711(boolean... zArr) {
        boolean z = false;
        if (!m17712()) {
            if (zArr != null && zArr.length > 0 && zArr[0]) {
                z = true;
            }
            if (this.f13957 == null || this.f13957.hasEnded() || z) {
                this.f13957 = AnimationUtils.loadAnimation(getContext(), 17432576);
                this.f13957.setDuration(300);
                this.f13957.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        TTVPlaybackControlView.this.setVisibility(0);
                        if (TTVPlaybackControlView.this.f13956 != null) {
                            TTVPlaybackControlView.this.f13956.m17722(TTVPlaybackControlView.this.getVisibility());
                        }
                        TTVPlaybackControlView.this.m17706();
                        TTVPlaybackControlView.this.m17691();
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                        if (TTVPlaybackControlView.this.f13960 != null) {
                            TTVPlaybackControlView.this.f13960.m17721(TTVPlaybackControlView.this.getVisibility());
                        }
                    }
                });
                startAnimation(this.f13957);
                return;
            }
            postDelayed(new Runnable() {
                public void run() {
                    TTVPlaybackControlView.this.m17711(true);
                }
            }, 250);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17712() {
        return getVisibility() == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17713(KeyEvent keyEvent) {
        boolean z = false;
        int keyCode = keyEvent.getKeyCode();
        if (this.f13963 == null || !m17699(keyCode)) {
            return false;
        }
        if (keyEvent.getAction() != 0) {
            return true;
        }
        if (keyCode == 90) {
            m17658();
            return true;
        } else if (keyCode == 89) {
            m17664();
            return true;
        } else if (keyEvent.getRepeatCount() != 0) {
            return true;
        } else {
            switch (keyCode) {
                case 85:
                    ControlDispatcher controlDispatcher = this.f13964;
                    ExoPlayer exoPlayer = this.f13963;
                    if (!this.f13963.getPlayWhenReady()) {
                        z = true;
                    }
                    controlDispatcher.m17717(exoPlayer, z);
                    return true;
                case 87:
                    m17677();
                    return true;
                case 88:
                    m17675();
                    return true;
                case 126:
                    this.f13964.m17717(this.f13963, true);
                    return true;
                case 127:
                    this.f13964.m17717(this.f13963, false);
                    return true;
                default:
                    return true;
            }
        }
    }
}
