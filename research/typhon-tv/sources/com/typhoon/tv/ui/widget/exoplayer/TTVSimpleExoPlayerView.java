package com.typhoon.tv.ui.widget.exoplayer;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextRenderer;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity;
import com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView;
import com.typhoon.tv.utils.DeviceUtils;
import java.util.Formatter;
import java.util.List;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

@TargetApi(16)
public final class TTVSimpleExoPlayerView extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ProgressBar f14000;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private boolean f14001;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final TTVPlaybackControlView f14002;
    /* access modifiers changed from: private */

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public boolean f14003;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ComponentListener f14004;
    /* access modifiers changed from: private */

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public boolean f14005;

    /* renamed from: ʾ  reason: contains not printable characters */
    private TextView f14006;
    /* access modifiers changed from: private */

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public boolean f14007;

    /* renamed from: ʿ  reason: contains not printable characters */
    private AudioManager f14008;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private boolean f14009;

    /* renamed from: ˆ  reason: contains not printable characters */
    private float f14010;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private ValueAnimator f14011;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Formatter f14012;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private Runnable f14013;

    /* renamed from: ˉ  reason: contains not printable characters */
    private float f14014;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private Runnable f14015;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f14016;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f14017;

    /* renamed from: ˎ  reason: contains not printable characters */
    private float f14018;

    /* renamed from: ˏ  reason: contains not printable characters */
    private long f14019;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final FrameLayout f14020;

    /* renamed from: י  reason: contains not printable characters */
    private float f14021;

    /* renamed from: ـ  reason: contains not printable characters */
    private float f14022;

    /* renamed from: ــ  reason: contains not printable characters */
    private boolean f14023;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public final TextView f14024;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final StringBuilder f14025;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f14026;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ExoPlayerActivity.OnUserSeekListener f14027;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f14028;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f14029;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f14030;
    /* access modifiers changed from: private */

    /* renamed from: ᵢ  reason: contains not printable characters */
    public SimpleExoPlayer f14031;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f14032;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final SubtitleView f14033;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final View f14034;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ImageView f14035;

    /* renamed from: 齉  reason: contains not printable characters */
    private final View f14036;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final AspectRatioFrameLayout f14037;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f14038;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f14039;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private float f14040;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private Bitmap f14041;

    private final class ComponentListener implements Player.EventListener, SimpleExoPlayer.VideoListener, TextRenderer.Output {
        private ComponentListener() {
        }

        public void onCues(List<Cue> list) {
            if (TTVSimpleExoPlayerView.this.f14033 != null) {
                TTVSimpleExoPlayerView.this.f14033.onCues(list);
            }
        }

        public void onLoadingChanged(boolean z) {
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }

        public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        }

        public void onPlayerStateChanged(boolean z, int i) {
            Integer r2;
            if (i == 1 && (r2 = TTVSimpleExoPlayerView.this.m17748(TTVSimpleExoPlayerView.this.getResizeMode())) != null) {
                TTVSimpleExoPlayerView.this.setResizeModeButtonDrawable(r2.intValue());
            }
            boolean z2 = false;
            if (!(TTVSimpleExoPlayerView.this.f14002 == null || TTVSimpleExoPlayerView.this.f14031 == null)) {
                if (i == 1) {
                    TTVSimpleExoPlayerView.this.f14002.m17711(new boolean[0]);
                    TTVSimpleExoPlayerView.this.setLoading(true);
                    z2 = true;
                } else if (i == 2) {
                    TTVSimpleExoPlayerView.this.setLoading(true);
                    z2 = true;
                } else if (i == 3) {
                    TTVSimpleExoPlayerView.this.setLoading(false);
                    if (!TTVSimpleExoPlayerView.this.f14005 && TTVSimpleExoPlayerView.this.f14002.m17712() && !TTVSimpleExoPlayerView.this.f14007) {
                        TTVSimpleExoPlayerView.this.f14002.m17708(true);
                        boolean unused = TTVSimpleExoPlayerView.this.f14005 = true;
                    }
                    z2 = true;
                } else if (i == 4) {
                    TTVSimpleExoPlayerView.this.setLoading(false);
                    TTVSimpleExoPlayerView.this.f14002.m17711(new boolean[0]);
                    z2 = true;
                }
            }
            if (!z2) {
                TTVSimpleExoPlayerView.this.m17758(false, new boolean[0]);
            }
        }

        public void onPositionDiscontinuity() {
        }

        public void onRenderedFirstFrame() {
            if (TTVSimpleExoPlayerView.this.f14034 != null) {
                TTVSimpleExoPlayerView.this.f14034.setVisibility(8);
            }
        }

        public void onRepeatModeChanged(int i) {
        }

        public void onTimelineChanged(Timeline timeline, Object obj) {
            if (TTVSimpleExoPlayerView.this.f14002 != null) {
                boolean unused = TTVSimpleExoPlayerView.this.f14003 = !timeline.isEmpty() && !timeline.getWindow(timeline.getWindowCount() + -1, TTVSimpleExoPlayerView.this.f14002.getWindow()).isDynamic;
            }
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            TTVSimpleExoPlayerView.this.m17726();
        }

        public void onVideoSizeChanged(int i, int i2, int i3, float f) {
            if (TTVSimpleExoPlayerView.this.f14037 != null) {
                TTVSimpleExoPlayerView.this.f14037.setAspectRatio(i2 == 0 ? 1.0f : (((float) i) * f) / ((float) i2));
            }
        }
    }

    public TTVSimpleExoPlayerView(Context context) {
        this(context, (AttributeSet) null);
    }

    public TTVSimpleExoPlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: type inference failed for: r20v61 */
    /* JADX WARNING: type inference failed for: r0v127, types: [android.view.SurfaceView] */
    /* JADX WARNING: type inference failed for: r0v128, types: [android.view.TextureView] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TTVSimpleExoPlayerView(android.content.Context r25, android.util.AttributeSet r26, int r27) {
        /*
            r24 = this;
            r24.<init>(r25, r26, r27)
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14016 = r0
            r20 = -1082130432(0xffffffffbf800000, float:-1.0)
            r0 = r20
            r1 = r24
            r1.f14010 = r0
            r20 = -1082130432(0xffffffffbf800000, float:-1.0)
            r0 = r20
            r1 = r24
            r1.f14014 = r0
            r20 = 1
            r0 = r20
            r1 = r24
            r1.f14030 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14005 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14023 = r0
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$1 r20 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$1
            r0 = r20
            r1 = r24
            r0.<init>()
            r0 = r20
            r1 = r24
            r1.f14015 = r0
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$2 r20 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$2
            r0 = r20
            r1 = r24
            r0.<init>()
            r0 = r20
            r1 = r24
            r1.f14013 = r0
            boolean r20 = r24.isInEditMode()
            if (r20 == 0) goto L_0x00e2
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14037 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14034 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14036 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14035 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14033 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14000 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14002 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14004 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14020 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14024 = r0
            r6 = 0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14025 = r0
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14012 = r0
            android.widget.ImageView r12 = new android.widget.ImageView
            r0 = r25
            r12.<init>(r0)
            int r20 = com.google.android.exoplayer2.util.Util.SDK_INT
            r21 = 23
            r0 = r20
            r1 = r21
            if (r0 < r1) goto L_0x00d8
            android.content.res.Resources r20 = r24.getResources()
            r0 = r20
            m17753((android.content.res.Resources) r0, (android.widget.ImageView) r12)
        L_0x00d2:
            r0 = r24
            r0.addView(r12)
        L_0x00d7:
            return
        L_0x00d8:
            android.content.res.Resources r20 = r24.getResources()
            r0 = r20
            m17739((android.content.res.Resources) r0, (android.widget.ImageView) r12)
            goto L_0x00d2
        L_0x00e2:
            r15 = 2131493074(0x7f0c00d2, float:1.8609618E38)
            r18 = 1
            r11 = 0
            r19 = 1
            r17 = 1
            r16 = 0
            r10 = 5000(0x1388, float:7.006E-42)
            r7 = 1
            if (r26 == 0) goto L_0x0156
            android.content.res.Resources$Theme r20 = r25.getTheme()
            int[] r21 = com.typhoon.tv.R.styleable.SimpleExoPlayerView
            r22 = 0
            r23 = 0
            r0 = r20
            r1 = r26
            r2 = r21
            r3 = r22
            r4 = r23
            android.content.res.TypedArray r5 = r0.obtainStyledAttributes(r1, r2, r3, r4)
            r20 = 5
            r0 = r20
            int r15 = r5.getResourceId(r0, r15)     // Catch:{ all -> 0x03b7 }
            r20 = 10
            r0 = r20
            r1 = r18
            boolean r18 = r5.getBoolean(r0, r1)     // Catch:{ all -> 0x03b7 }
            r20 = 2
            r0 = r20
            int r11 = r5.getResourceId(r0, r11)     // Catch:{ all -> 0x03b7 }
            r20 = 11
            r0 = r20
            r1 = r19
            boolean r19 = r5.getBoolean(r0, r1)     // Catch:{ all -> 0x03b7 }
            r20 = 9
            r0 = r20
            r1 = r17
            int r17 = r5.getInt(r0, r1)     // Catch:{ all -> 0x03b7 }
            r20 = 6
            r0 = r20
            r1 = r16
            int r16 = r5.getInt(r0, r1)     // Catch:{ all -> 0x03b7 }
            r20 = 8
            r0 = r20
            int r10 = r5.getInt(r0, r10)     // Catch:{ all -> 0x03b7 }
            r20 = 4
            r0 = r20
            boolean r7 = r5.getBoolean(r0, r7)     // Catch:{ all -> 0x03b7 }
            r5.recycle()
        L_0x0156:
            java.lang.StringBuilder r20 = new java.lang.StringBuilder
            r20.<init>()
            r0 = r20
            r1 = r24
            r1.f14025 = r0
            java.util.Formatter r20 = new java.util.Formatter
            r0 = r24
            java.lang.StringBuilder r0 = r0.f14025
            r21 = r0
            java.util.Locale r22 = java.util.Locale.getDefault()
            r20.<init>(r21, r22)
            r0 = r20
            r1 = r24
            r1.f14012 = r0
            android.view.LayoutInflater r20 = android.view.LayoutInflater.from(r25)
            r0 = r20
            r1 = r24
            r0.inflate(r15, r1)
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$ComponentListener r20 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$ComponentListener
            r21 = 0
            r0 = r20
            r1 = r24
            r2 = r21
            r0.<init>()
            r0 = r20
            r1 = r24
            r1.f14004 = r0
            r20 = 262144(0x40000, float:3.67342E-40)
            r0 = r24
            r1 = r20
            r0.setDescendantFocusability(r1)
            r20 = 2131296467(0x7f0900d3, float:1.8210852E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r20 = (com.google.android.exoplayer2.ui.AspectRatioFrameLayout) r20
            r0 = r20
            r1 = r24
            r1.f14037 = r0
            r0 = r24
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.f14037
            r20 = r0
            if (r20 == 0) goto L_0x01c5
            r0 = r24
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.f14037
            r20 = r0
            r0 = r20
            r1 = r16
            m17754((com.google.android.exoplayer2.ui.AspectRatioFrameLayout) r0, (int) r1)
        L_0x01c5:
            r20 = 2131296482(0x7f0900e2, float:1.8210882E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            r0 = r20
            r1 = r24
            r1.f14034 = r0
            r20 = 2131296469(0x7f0900d5, float:1.8210856E38)
            r0 = r24
            r1 = r20
            android.view.View r9 = r0.findViewById(r1)
            if (r9 == 0) goto L_0x03bc
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r20 = new com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView
            r21 = 0
            r22 = 0
            r0 = r20
            r1 = r25
            r2 = r21
            r3 = r22
            r4 = r26
            r0.<init>(r1, r2, r3, r4)
            r0 = r20
            r1 = r24
            r1.f14002 = r0
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            android.view.ViewGroup$LayoutParams r21 = r9.getLayoutParams()
            r20.setLayoutParams(r21)
            android.view.ViewParent r14 = r9.getParent()
            android.view.ViewGroup r14 = (android.view.ViewGroup) r14
            int r8 = r14.indexOfChild(r9)
            r14.removeView(r9)
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            r0 = r20
            r14.addView(r0, r8)
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$3 r21 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$3
            r0 = r21
            r1 = r24
            r0.<init>()
            r20.setOnControllerTouchListener(r21)
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$4 r21 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$4
            r0 = r21
            r1 = r24
            r0.<init>()
            r20.setOnControllerContentWrapperTouchListener(r21)
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$5 r21 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$5
            r0 = r21
            r1 = r24
            r0.<init>()
            r20.setResizeModeButtonOnClickListener(r21)
        L_0x0257:
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            if (r20 == 0) goto L_0x03c6
        L_0x025f:
            r0 = r24
            r0.f14026 = r10
            r0 = r24
            r0.f14028 = r7
            if (r19 == 0) goto L_0x03c9
            r0 = r24
            com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r0.f14002
            r20 = r0
            if (r20 == 0) goto L_0x03c9
            r20 = 1
        L_0x0273:
            r0 = r20
            r1 = r24
            r1.f14032 = r0
            r20 = 2131296578(0x7f090142, float:1.8211077E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            android.widget.ProgressBar r20 = (android.widget.ProgressBar) r20
            r0 = r20
            r1 = r24
            r1.f14000 = r0
            r20 = 2131296400(0x7f090090, float:1.8210716E38)
            r0 = r24
            r1 = r20
            android.view.View r6 = r0.findViewById(r1)
            android.widget.FrameLayout r6 = (android.widget.FrameLayout) r6
            if (r6 == 0) goto L_0x02b7
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$6 r20 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$6
            r0 = r20
            r1 = r24
            r0.<init>()
            r0 = r20
            r6.setOnClickListener(r0)
            com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$7 r20 = new com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView$7
            r0 = r20
            r1 = r24
            r0.<init>()
            r0 = r20
            r6.setOnTouchListener(r0)
        L_0x02b7:
            r20 = 2131296401(0x7f090091, float:1.8210718E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            android.widget.TextView r20 = (android.widget.TextView) r20
            r0 = r20
            r1 = r24
            r1.f14006 = r0
            r0 = r24
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.f14037
            r20 = r0
            if (r20 == 0) goto L_0x03d8
            if (r17 == 0) goto L_0x03d8
            android.view.ViewGroup$LayoutParams r13 = new android.view.ViewGroup$LayoutParams
            r20 = -1
            r21 = -1
            r0 = r20
            r1 = r21
            r13.<init>(r0, r1)
            r20 = 2
            r0 = r17
            r1 = r20
            if (r0 != r1) goto L_0x03cd
            android.view.TextureView r20 = new android.view.TextureView
            r0 = r20
            r1 = r25
            r0.<init>(r1)
        L_0x02f2:
            r0 = r20
            r1 = r24
            r1.f14036 = r0
            r0 = r24
            android.view.View r0 = r0.f14036
            r20 = r0
            r0 = r20
            r0.setLayoutParams(r13)
            r0 = r24
            com.google.android.exoplayer2.ui.AspectRatioFrameLayout r0 = r0.f14037
            r20 = r0
            r0 = r24
            android.view.View r0 = r0.f14036
            r21 = r0
            r22 = 0
            r20.addView(r21, r22)
        L_0x0314:
            r20 = 2131296800(0x7f090220, float:1.8211527E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            android.widget.FrameLayout r20 = (android.widget.FrameLayout) r20
            r0 = r20
            r1 = r24
            r1.f14020 = r0
            r0 = r24
            android.widget.FrameLayout r0 = r0.f14020
            r20 = r0
            if (r20 == 0) goto L_0x03e2
            r0 = r24
            android.widget.FrameLayout r0 = r0.f14020
            r20 = r0
            r21 = 2131296801(0x7f090221, float:1.8211529E38)
            android.view.View r20 = r20.findViewById(r21)
            android.widget.TextView r20 = (android.widget.TextView) r20
            r0 = r20
            r1 = r24
            r1.f14024 = r0
            r0 = r24
            android.widget.TextView r0 = r0.f14024
            r20 = r0
            android.graphics.Typeface r21 = com.typhoon.tv.utils.TypefaceUtils.m17839()
            r20.setTypeface(r21)
        L_0x0351:
            r20 = 2131296465(0x7f0900d1, float:1.8210847E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            android.widget.ImageView r20 = (android.widget.ImageView) r20
            r0 = r20
            r1 = r24
            r1.f14035 = r0
            if (r18 == 0) goto L_0x03ec
            r0 = r24
            android.widget.ImageView r0 = r0.f14035
            r20 = r0
            if (r20 == 0) goto L_0x03ec
            r20 = 1
        L_0x0370:
            r0 = r20
            r1 = r24
            r1.f14038 = r0
            if (r11 == 0) goto L_0x0388
            android.content.res.Resources r20 = r25.getResources()
            r0 = r20
            android.graphics.Bitmap r20 = android.graphics.BitmapFactory.decodeResource(r0, r11)
            r0 = r20
            r1 = r24
            r1.f14041 = r0
        L_0x0388:
            r20 = 2131296483(0x7f0900e3, float:1.8210884E38)
            r0 = r24
            r1 = r20
            android.view.View r20 = r0.findViewById(r1)
            com.google.android.exoplayer2.ui.SubtitleView r20 = (com.google.android.exoplayer2.ui.SubtitleView) r20
            r0 = r20
            r1 = r24
            r1.f14033 = r0
            r0 = r24
            com.google.android.exoplayer2.ui.SubtitleView r0 = r0.f14033
            r20 = r0
            if (r20 == 0) goto L_0x00d7
            r0 = r24
            com.google.android.exoplayer2.ui.SubtitleView r0 = r0.f14033
            r20 = r0
            r20.setUserDefaultStyle()
            r0 = r24
            com.google.android.exoplayer2.ui.SubtitleView r0 = r0.f14033
            r20 = r0
            r20.setUserDefaultTextSize()
            goto L_0x00d7
        L_0x03b7:
            r20 = move-exception
            r5.recycle()
            throw r20
        L_0x03bc:
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14002 = r0
            goto L_0x0257
        L_0x03c6:
            r10 = 0
            goto L_0x025f
        L_0x03c9:
            r20 = 0
            goto L_0x0273
        L_0x03cd:
            android.view.SurfaceView r20 = new android.view.SurfaceView
            r0 = r20
            r1 = r25
            r0.<init>(r1)
            goto L_0x02f2
        L_0x03d8:
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14036 = r0
            goto L_0x0314
        L_0x03e2:
            r20 = 0
            r0 = r20
            r1 = r24
            r1.f14024 = r0
            goto L_0x0351
        L_0x03ec:
            r20 = 0
            goto L_0x0370
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    private void setWindowBrightness(float f) {
        if (getContext() instanceof Activity) {
            Activity activity = (Activity) getContext();
            WindowManager.LayoutParams attributes = activity.getWindow().getAttributes();
            attributes.screenBrightness = f;
            activity.getWindow().setAttributes(attributes);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m17725() {
        this.f14008 = TVApplication.m6286();
        this.f14039 = this.f14008.getStreamMaxVolume(3);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m17726() {
        if (this.f14031 != null) {
            TrackSelectionArray currentTrackSelections = this.f14031.getCurrentTrackSelections();
            int i = 0;
            while (i < currentTrackSelections.length) {
                if (this.f14031.getRendererType(i) != 2 || currentTrackSelections.get(i) == null) {
                    i++;
                } else {
                    m17730();
                    return;
                }
            }
            if (this.f14034 != null) {
                this.f14034.setVisibility(0);
            }
            if (this.f14038) {
                for (int i2 = 0; i2 < currentTrackSelections.length; i2++) {
                    TrackSelection trackSelection = currentTrackSelections.get(i2);
                    if (trackSelection != null) {
                        int i3 = 0;
                        while (i3 < trackSelection.length()) {
                            Metadata metadata = trackSelection.getFormat(i3).metadata;
                            if (metadata == null || !m17761(metadata)) {
                                i3++;
                            } else {
                                return;
                            }
                        }
                        continue;
                    }
                }
                if (m17759(this.f14041)) {
                    return;
                }
            }
            m17730();
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean m17728() {
        Timeline currentTimeline = this.f14031 != null ? this.f14031.getCurrentTimeline() : null;
        boolean z = false;
        if (currentTimeline != null && !currentTimeline.isEmpty()) {
            int currentWindowIndex = this.f14031.getCurrentWindowIndex();
            if (this.f14002 != null) {
                Timeline.Window window = this.f14002.getWindow();
                currentTimeline.getWindow(currentWindowIndex, window);
                z = window.isSeekable;
            }
        }
        return z && this.f14003;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m17730() {
        if (this.f14035 != null) {
            this.f14035.setImageResource(17170445);
            this.f14035.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m17731() {
        if (this.f14006 != null) {
            this.f14006.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
            this.f14006.setVisibility(8);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m17733() {
        if (getContext() instanceof Activity) {
            Activity activity = (Activity) getContext();
            WindowManager.LayoutParams attributes = activity.getWindow().getAttributes();
            float f = attributes.screenBrightness != -1.0f ? attributes.screenBrightness : 0.6f;
            try {
                if (Settings.System.getInt(activity.getContentResolver(), "screen_brightness_mode") == 1) {
                    this.f14023 = true;
                    if (Build.VERSION.SDK_INT < 23 || Settings.System.canWrite(activity)) {
                        Settings.System.putInt(activity.getContentResolver(), "screen_brightness_mode", 0);
                    } else {
                        return;
                    }
                } else if (f == 0.6f) {
                    f = ((float) Settings.System.getInt(activity.getContentResolver(), "screen_brightness")) / 255.0f;
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            attributes.screenBrightness = f;
            activity.getWindow().setAttributes(attributes);
            this.f14030 = false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m17736(int i) {
        return i <= 15 ? R.drawable.ic_brightness_1_white_36dp : (i > 30 || i <= 15) ? (i > 45 || i <= 30) ? (i > 60 || i <= 45) ? (i > 75 || i <= 60) ? (i > 90 || i <= 75) ? R.drawable.ic_brightness_7_white_36dp : R.drawable.ic_brightness_6_white_36dp : R.drawable.ic_brightness_5_white_36dp : R.drawable.ic_brightness_4_white_36dp : R.drawable.ic_brightness_3_white_36dp : R.drawable.ic_brightness_2_white_36dp;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17738(float f) {
        boolean z = true;
        if (this.f14016 == 0 || this.f14016 == 1) {
            int i = (int) this.f14040;
            this.f14016 = 1;
            float f2 = -((f / ((float) this.f14017)) * ((float) this.f14039));
            this.f14040 += f2;
            int min = (int) Math.min(Math.max(this.f14040, 0.0f), (float) this.f14039);
            if (f2 != 0.0f) {
                if (min <= i) {
                    z = false;
                }
                m17751(min, z);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m17739(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(R.drawable.exo_edit_mode_logo));
        imageView.setBackgroundColor(resources.getColor(R.color.exo_edit_mode_background_color));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17742(float f) {
        if (getContext() instanceof Activity) {
            float min = Math.min(Math.max(((Activity) getContext()).getWindow().getAttributes().screenBrightness + f, 0.01f), 1.0f);
            setWindowBrightness(min);
            int round = (int) ((float) Math.round(100.0f * min));
            String valueOf = String.valueOf(round);
            if (!valueOf.isEmpty()) {
                m17757(I18N.m15707(R.string.exo_player_brightness, valueOf), m17736(round));
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17744(float f) {
        if (this.f14016 == 0 || this.f14016 == 2) {
            this.f14016 = 2;
            if (this.f14030) {
                m17733();
            }
            this.f14016 = 2;
            m17742((-f) / ((float) this.f14017));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private float m17745(float f) {
        return f / getResources().getDisplayMetrics().density;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private float m17746(float f, float f2, float f3, float f4) {
        float f5 = f - f3;
        float f6 = f2 - f4;
        return m17745((float) Math.sqrt((double) ((f5 * f5) + (f6 * f6))));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private CharSequence m17747(long j) {
        String str = Util.getStringForTime(this.f14025, this.f14012, j) + " / " + Util.getStringForTime(this.f14025, this.f14012, this.f14031 == null ? 0 : this.f14031.getDuration());
        int indexOf = str.indexOf(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
        SpannableString spannableString = new SpannableString(str);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new TypedValue().data, new int[]{R.attr.colorAccent});
        int color = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        spannableString.setSpan(new ForegroundColorSpan(color), 0, indexOf, 17);
        return spannableString;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m17748(int i) {
        switch (i) {
            case 0:
                return Integer.valueOf(R.drawable.ic_zoom_out_map_white_36dp);
            case 1:
                return Integer.valueOf(R.drawable.ic_swap_vert_white_36dp);
            case 2:
                return Integer.valueOf(R.drawable.ic_aspect_ratio_white_36dp);
            case 3:
                return Integer.valueOf(R.drawable.ic_crop_white_36dp);
            case 4:
                return Integer.valueOf(R.drawable.ic_swap_horiz_white_36dp);
            default:
                return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17750(int i, float f, boolean z) {
        if (i == 0) {
            i = 1;
        }
        if (Math.abs(f) >= 1.0f && m17728()) {
            if ((this.f14016 == 0 || this.f14016 == 3) && this.f14031 != null) {
                this.f14016 = 3;
                long duration = this.f14031.getDuration();
                long currentPosition = this.f14031.getCurrentPosition();
                int signum = (int) ((((double) Math.signum(f)) * ((300000.0d * Math.pow((double) (f / 8.0f), 4.0d)) + 3000.0d)) / ((double) i));
                if (signum > 0 && ((long) signum) + currentPosition > duration) {
                    signum = (int) (duration - currentPosition);
                }
                if (signum < 0 && ((long) signum) + currentPosition < 0) {
                    signum = (int) (-currentPosition);
                }
                if (z && duration > 0 && this.f14031 != null) {
                    this.f14031.seekTo(((long) signum) + currentPosition);
                    if (this.f14027 != null) {
                        this.f14027.m17491();
                    }
                }
                if (duration > 0) {
                    m17752(((long) signum) + currentPosition, signum > 0 ? R.drawable.ic_fast_forward_white_36dp : R.drawable.ic_fast_rewind_white_36dp);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17751(int i, boolean z) {
        try {
            this.f14008.setStreamVolume(3, i, 0);
        } catch (Exception e) {
        }
        int streamVolume = this.f14008.getStreamVolume(3);
        if (i != streamVolume) {
            try {
                this.f14008.setStreamVolume(3, i, 1);
            } catch (Exception e2) {
            }
            streamVolume = this.f14008.getStreamVolume(3);
        }
        this.f14016 = 1;
        int i2 = (i * 100) / this.f14039;
        int i3 = streamVolume == 0 ? R.drawable.ic_volume_mute_white_36dp : i2 >= 50 ? R.drawable.ic_volume_up_white_36dp : R.drawable.ic_volume_down_white_36dp;
        String valueOf = String.valueOf(i2);
        if (!valueOf.isEmpty()) {
            m17757(I18N.m15707(R.string.exo_player_volume, valueOf), i3);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17752(long j, int i) {
        if (this.f14006 != null) {
            this.f14006.setVisibility(0);
            this.f14006.setText(m17747(j));
            this.f14006.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, ContextCompat.getDrawable(getContext(), i), (Drawable) null, (Drawable) null);
        }
    }

    @TargetApi(23)
    /* renamed from: 龘  reason: contains not printable characters */
    private static void m17753(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(R.drawable.exo_edit_mode_logo, (Resources.Theme) null));
        imageView.setBackgroundColor(resources.getColor(R.color.exo_edit_mode_background_color, (Resources.Theme) null));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m17754(AspectRatioFrameLayout aspectRatioFrameLayout, int i) {
        aspectRatioFrameLayout.setResizeMode(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17757(String str, int i) {
        if (this.f14006 != null) {
            this.f14006.setVisibility(0);
            this.f14006.setText(str);
            this.f14006.setTextColor(ContextCompat.getColor(getContext(), 17170443));
            this.f14006.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, ContextCompat.getDrawable(getContext(), i), (Drawable) null, (Drawable) null);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17758(boolean z, boolean... zArr) {
        if (this.f14032 && this.f14031 != null && this.f14002 != null) {
            int playbackState = this.f14031.getPlaybackState();
            boolean z2 = playbackState == 1 || playbackState == 4 || !this.f14031.getPlayWhenReady();
            boolean z3 = this.f14002.m17712() && this.f14002.getShowTimeoutMs() <= 0;
            this.f14002.setShowTimeoutMs(z2 ? 0 : this.f14026);
            if (z || z2 || z3) {
                this.f14002.m17710(zArr != null && zArr.length > 0 && zArr[0], new boolean[0]);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17759(Bitmap bitmap) {
        if (bitmap == null) {
            return false;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= 0 || height <= 0) {
            return false;
        }
        if (this.f14037 != null) {
            this.f14037.setAspectRatio(((float) width) / ((float) height));
        }
        this.f14035.setImageBitmap(bitmap);
        this.f14035.setVisibility(0);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17760(MotionEvent motionEvent) {
        float f;
        float f2;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        if (this.f14017 == 0) {
            this.f14017 = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        if (this.f14010 == -1.0f || this.f14014 == -1.0f) {
            f = 0.0f;
            f2 = 0.0f;
        } else {
            f2 = motionEvent.getRawY() - this.f14014;
            f = motionEvent.getRawX() - this.f14010;
        }
        float abs = Math.abs(f2 / f);
        float rawX = ((motionEvent.getRawX() - this.f14010) / displayMetrics.xdpi) * 2.54f;
        float max = Math.max(1.0f, ((Math.abs(this.f14018 - motionEvent.getRawY()) / displayMetrics.xdpi) + 0.5f) * 2.0f);
        switch (motionEvent.getAction()) {
            case 0:
                this.f14019 = System.currentTimeMillis();
                this.f14021 = motionEvent.getX();
                this.f14022 = motionEvent.getY();
                this.f14029 = true;
                this.f14016 = 0;
                this.f14010 = motionEvent.getRawX();
                this.f14040 = (float) this.f14008.getStreamVolume(3);
                float rawY = motionEvent.getRawY();
                this.f14018 = rawY;
                this.f14014 = rawY;
                this.f14007 = false;
                break;
            case 1:
                this.f14007 = false;
                if (this.f14016 == 3) {
                    m17750(Math.round(max), rawX, true);
                }
                this.f14010 = -1.0f;
                this.f14014 = -1.0f;
                if (this.f14016 == 0) {
                    if (System.currentTimeMillis() - this.f14019 < 500 && this.f14029) {
                        m17764();
                        break;
                    }
                } else {
                    m17731();
                    if (this.f14031 == null || !this.f14031.getPlayWhenReady() || this.f14009) {
                    }
                }
            case 2:
                this.f14007 = true;
                if (this.f14029 && m17746(this.f14021, this.f14022, motionEvent.getX(), motionEvent.getY()) > 15.0f) {
                    this.f14029 = false;
                }
                if (this.f14016 != 3 && abs > 2.0f) {
                    if (((double) Math.abs(f2 / ((float) this.f14017))) >= 0.05d) {
                        this.f14010 = motionEvent.getRawX();
                        this.f14014 = motionEvent.getRawY();
                        if (((int) this.f14010) > (displayMetrics.widthPixels * 4) / 7) {
                            m17738(f2);
                        }
                        if (((int) this.f14010) < (displayMetrics.widthPixels * 3) / 7) {
                            m17744(f2);
                            break;
                        }
                    } else {
                        return false;
                    }
                } else {
                    m17750(Math.round(max), rawX, false);
                    break;
                }
                break;
        }
        return this.f14016 != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17761(Metadata metadata) {
        for (int i = 0; i < metadata.length(); i++) {
            Metadata.Entry entry = metadata.get(i);
            if (entry instanceof ApicFrame) {
                byte[] bArr = ((ApicFrame) entry).pictureData;
                return m17759(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
            }
        }
        return false;
    }

    public TTVPlaybackControlView getController() {
        return this.f14002;
    }

    public boolean getControllerHideOnTouch() {
        return this.f14028;
    }

    public int getControllerShowTimeoutMs() {
        return this.f14026;
    }

    public Bitmap getDefaultArtwork() {
        return this.f14041;
    }

    public SimpleExoPlayer getPlayer() {
        return this.f14031;
    }

    public int getResizeMode() {
        Assertions.checkState(this.f14037 != null);
        return this.f14037.getResizeMode();
    }

    public SubtitleView getSubtitleView() {
        return this.f14033;
    }

    public boolean getUseArtwork() {
        return this.f14038;
    }

    public boolean getUseController() {
        return this.f14032;
    }

    public View getVideoSurfaceView() {
        return this.f14036;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f14009 = true;
        m17725();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f14009 = false;
        this.f14008 = null;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f14032 || this.f14031 == null || this.f14002 == null || motionEvent.getActionMasked() != 0) {
            return false;
        }
        m17764();
        return true;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!this.f14032 || this.f14031 == null || this.f14002 == null) {
            return false;
        }
        m17758(true, new boolean[0]);
        return true;
    }

    public void setControlDispatcher(TTVPlaybackControlView.ControlDispatcher controlDispatcher) {
        Assertions.checkState(this.f14002 != null);
        this.f14002.setControlDispatcher(controlDispatcher);
    }

    public void setControllerHideOnTouch(boolean z) {
        Assertions.checkState(this.f14002 != null);
        this.f14028 = z;
    }

    public void setControllerShowTimeoutMs(int i) {
        Assertions.checkState(this.f14002 != null);
        this.f14026 = i;
    }

    public void setControllerVisibilityAnimationStartListener(TTVPlaybackControlView.VisibilityAnimationListener visibilityAnimationListener) {
        Assertions.checkState(this.f14002 != null);
        this.f14002.setVisibilityAnimationStartListener(visibilityAnimationListener);
    }

    public void setControllerVisibilityListener(TTVPlaybackControlView.VisibilityListener visibilityListener) {
        Assertions.checkState(this.f14002 != null);
        this.f14002.setVisibilityListener(visibilityListener);
    }

    public void setDefaultArtwork(Bitmap bitmap) {
        if (this.f14041 != bitmap) {
            this.f14041 = bitmap;
            m17726();
        }
    }

    public void setDisplayName(String str) {
        if (this.f14002 != null) {
            this.f14002.setDisplayName(str);
        }
    }

    public void setFastForwardIncrementMs(int i) {
        Assertions.checkState(this.f14002 != null);
        this.f14002.setFastForwardIncrementMs(i);
    }

    public void setFirstTimeReady(boolean z) {
        this.f14005 = z;
    }

    public void setLoading(boolean z) {
        this.f14001 = z;
        m17773(z);
    }

    public void setOnUserSeekListener(ExoPlayerActivity.OnUserSeekListener onUserSeekListener) {
        this.f14027 = onUserSeekListener;
    }

    public void setPlayer(SimpleExoPlayer simpleExoPlayer) {
        if (this.f14031 != simpleExoPlayer) {
            if (this.f14031 != null) {
                this.f14031.removeListener(this.f14004);
                this.f14031.removeTextOutput(this.f14004);
                this.f14031.removeVideoListener(this.f14004);
                if (this.f14036 instanceof TextureView) {
                    this.f14031.clearVideoTextureView((TextureView) this.f14036);
                } else if (this.f14036 instanceof SurfaceView) {
                    this.f14031.clearVideoSurfaceView((SurfaceView) this.f14036);
                }
            }
            this.f14031 = simpleExoPlayer;
            if (this.f14032) {
                this.f14002.setPlayer(simpleExoPlayer);
            }
            if (this.f14034 != null) {
                this.f14034.setVisibility(0);
            }
            if (simpleExoPlayer != null) {
                if (this.f14036 instanceof TextureView) {
                    simpleExoPlayer.setVideoTextureView((TextureView) this.f14036);
                } else if (this.f14036 instanceof SurfaceView) {
                    simpleExoPlayer.setVideoSurfaceView((SurfaceView) this.f14036);
                }
                simpleExoPlayer.addVideoListener(this.f14004);
                simpleExoPlayer.addTextOutput(this.f14004);
                simpleExoPlayer.addListener(this.f14004);
                m17726();
                return;
            }
            m17768();
            m17730();
        }
    }

    public void setPortrait(boolean z) {
        if (this.f14002 != null) {
            this.f14002.setPortrait(z);
        }
    }

    public void setPortraitBackListener(TTVPlaybackControlView.ExoClickListener exoClickListener) {
        if (this.f14002 != null) {
            this.f14002.setPortraitBackListener(exoClickListener);
        }
    }

    public void setResizeMode(int i) {
        Assertions.checkState(this.f14037 != null);
        this.f14037.setResizeMode(i);
    }

    public void setResizeModeButtonDrawable(int i) {
        if (this.f14002 != null) {
            this.f14002.setResizeModeButtonIconDrawable(i);
        }
    }

    public void setRewindIncrementMs(int i) {
        Assertions.checkState(this.f14002 != null);
        this.f14002.setRewindIncrementMs(i);
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        Assertions.checkState(this.f14002 != null);
        this.f14002.setShowMultiWindowTimeBar(z);
    }

    public void setUseArtwork(boolean z) {
        Assertions.checkState(!z || this.f14035 != null);
        if (this.f14038 != z) {
            this.f14038 = z;
            m17726();
        }
    }

    public void setUseController(boolean z) {
        Assertions.checkState(!z || this.f14002 != null);
        if (this.f14032 != z) {
            this.f14032 = z;
            if (z) {
                this.f14002.setPlayer(this.f14031);
            } else if (this.f14002 != null) {
                this.f14002.m17707(new boolean[0]);
                this.f14002.setPlayer((ExoPlayer) null);
            }
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        if (this.f14036 instanceof SurfaceView) {
            this.f14036.setVisibility(i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m17764() {
        if (this.f14002 == null) {
            return;
        }
        if (!this.f14002.m17712()) {
            m17758(true, new boolean[0]);
        } else if (this.f14028) {
            this.f14002.m17707(new boolean[0]);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m17765() {
        if (this.f14011 != null && this.f14011.isRunning()) {
            this.f14011.cancel();
        }
        this.f14011 = ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        this.f14011.setDuration(500);
        this.f14011.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float parseFloat = Float.parseFloat(valueAnimator.getAnimatedValue().toString());
                if (TTVSimpleExoPlayerView.this.f14024 != null) {
                    TTVSimpleExoPlayerView.this.f14024.setAlpha(parseFloat);
                }
            }
        });
        this.f14011.addListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                if (TTVSimpleExoPlayerView.this.f14024 != null) {
                    TTVSimpleExoPlayerView.this.f14024.setVisibility(8);
                    TTVSimpleExoPlayerView.this.f14024.setAlpha(1.0f);
                }
                if (TTVSimpleExoPlayerView.this.f14020 != null) {
                    TTVSimpleExoPlayerView.this.f14020.setVisibility(8);
                }
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }
        });
        this.f14011.start();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17766(boolean z) {
        if (DeviceUtils.m6388()) {
            m17769(z);
        } else {
            m17770(z);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m17767() {
        return this.f14005;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m17768() {
        if (this.f14002 != null) {
            this.f14002.m17707(new boolean[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002c  */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m17769(boolean r14) {
        /*
            r13 = this;
            r12 = 0
            r11 = 1065353216(0x3f800000, float:1.0)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            r8 = 0
            r9 = 1
            com.google.android.exoplayer2.SimpleExoPlayer r7 = r13.f14031     // Catch:{ Exception -> 0x005a }
            float r6 = r7.getVolume()     // Catch:{ Exception -> 0x005a }
            if (r14 == 0) goto L_0x0063
            float r5 = r6 + r10
            int r7 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r7 < 0) goto L_0x0054
            com.google.android.exoplayer2.SimpleExoPlayer r7 = r13.f14031     // Catch:{ Exception -> 0x005a }
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setVolume(r8)     // Catch:{ Exception -> 0x005a }
        L_0x001d:
            com.google.android.exoplayer2.SimpleExoPlayer r7 = r13.f14031
            float r0 = r7.getVolume()
            r7 = 1120403456(0x42c80000, float:100.0)
            float r7 = r7 * r0
            int r1 = java.lang.Math.round(r7)
            if (r1 != 0) goto L_0x0076
            r3 = 2131231180(0x7f0801cc, float:1.8078434E38)
        L_0x002f:
            java.lang.String r2 = java.lang.String.valueOf(r1)
            boolean r7 = r2.isEmpty()
            if (r7 != 0) goto L_0x0047
            r7 = 2131820785(0x7f1100f1, float:1.9274295E38)
            java.lang.Object[] r8 = new java.lang.Object[r9]
            r8[r12] = r2
            java.lang.String r7 = com.typhoon.tv.I18N.m15707(r7, r8)
            r13.m17757((java.lang.String) r7, (int) r3)
        L_0x0047:
            java.lang.Runnable r7 = r13.f14015
            r13.removeCallbacks(r7)
            java.lang.Runnable r7 = r13.f14015
            r8 = 1500(0x5dc, double:7.41E-321)
            r13.postDelayed(r7, r8)
            return
        L_0x0054:
            com.google.android.exoplayer2.SimpleExoPlayer r7 = r13.f14031     // Catch:{ Exception -> 0x005a }
            r7.setVolume(r5)     // Catch:{ Exception -> 0x005a }
            goto L_0x001d
        L_0x005a:
            r4 = move-exception
            boolean[] r7 = new boolean[r9]
            r7[r12] = r9
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r7)
            goto L_0x001d
        L_0x0063:
            float r5 = r6 - r10
            int r7 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r7 > 0) goto L_0x0070
            com.google.android.exoplayer2.SimpleExoPlayer r7 = r13.f14031     // Catch:{ Exception -> 0x005a }
            r8 = 0
            r7.setVolume(r8)     // Catch:{ Exception -> 0x005a }
            goto L_0x001d
        L_0x0070:
            com.google.android.exoplayer2.SimpleExoPlayer r7 = r13.f14031     // Catch:{ Exception -> 0x005a }
            r7.setVolume(r5)     // Catch:{ Exception -> 0x005a }
            goto L_0x001d
        L_0x0076:
            r7 = 50
            if (r1 < r7) goto L_0x007e
            r3 = 2131231181(0x7f0801cd, float:1.8078436E38)
            goto L_0x002f
        L_0x007e:
            r3 = 2131231179(0x7f0801cb, float:1.8078432E38)
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView.m17769(boolean):void");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m17770(boolean z) {
        if (z) {
            try {
                this.f14008.adjustStreamVolume(3, 1, 0);
            } catch (Exception e) {
            }
        } else {
            this.f14008.adjustStreamVolume(3, -1, 0);
        }
        int streamVolume = this.f14008.getStreamVolume(3);
        if (streamVolume == 1) {
            streamVolume = 0;
        }
        int i = (streamVolume * 100) / this.f14039;
        int i2 = i == 0 ? R.drawable.ic_volume_mute_white_36dp : i >= 50 ? R.drawable.ic_volume_up_white_36dp : R.drawable.ic_volume_down_white_36dp;
        String valueOf = String.valueOf(i);
        if (!valueOf.isEmpty()) {
            m17757(I18N.m15707(R.string.exo_player_volume, valueOf), i2);
        }
        removeCallbacks(this.f14015);
        postDelayed(this.f14015, 1500);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m17771() {
        return this.f14023;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17772(String str) {
        if (this.f14020 != null && this.f14024 != null) {
            removeCallbacks(this.f14013);
            if (this.f14011 != null && this.f14011.isRunning()) {
                this.f14011.cancel();
            }
            this.f14024.setAlpha(1.0f);
            this.f14024.setText(str);
            this.f14024.setVisibility(0);
            this.f14020.setVisibility(0);
            postDelayed(this.f14013, 1500);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17773(boolean z) {
        this.f14001 = z;
        if (this.f14000 == null) {
            return;
        }
        if (z) {
            this.f14000.setVisibility(0);
        } else {
            this.f14000.setVisibility(8);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17774(boolean... zArr) {
        if (this.f14032) {
            m17758(true, zArr);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17775() {
        return this.f14002 != null && this.f14002.m17712();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17776(KeyEvent keyEvent) {
        return this.f14032 && this.f14002.m17713(keyEvent);
    }
}
