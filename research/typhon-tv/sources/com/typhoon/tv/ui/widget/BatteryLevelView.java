package com.typhoon.tv.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.app.NotificationCompat;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.util.AttributeSet;
import android.view.View;

public class BatteryLevelView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f13906;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f13907;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f13908;

    /* renamed from: ʾ  reason: contains not printable characters */
    private float f13909;

    /* renamed from: ʿ  reason: contains not printable characters */
    private RectF f13910;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f13911;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f13912;

    /* renamed from: ˋ  reason: contains not printable characters */
    private BroadcastReceiver f13913;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f13914;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f13915;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f13916;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f13917;

    /* renamed from: 靐  reason: contains not printable characters */
    private Paint f13918;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f13919;

    /* renamed from: 齉  reason: contains not printable characters */
    private float f13920;

    /* renamed from: 龘  reason: contains not printable characters */
    private Paint f13921;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private RectF f13922;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private RectF f13923;

    public BatteryLevelView(Context context) {
        this(context, (AttributeSet) null);
    }

    public BatteryLevelView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BatteryLevelView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f13920 = 2.0f;
        this.f13906 = 30.0f;
        this.f13907 = 55.0f;
        this.f13908 = 15.0f;
        this.f13914 = 5.0f;
        this.f13915 = 1.0f;
        this.f13916 = (this.f13906 - this.f13920) - (this.f13915 * 2.0f);
        this.f13911 = (this.f13907 - this.f13920) - (this.f13915 * 2.0f);
        this.f13909 = 10.0f;
        this.f13912 = false;
        this.f13913 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    int intExtra = intent.getIntExtra(NotificationCompat.CATEGORY_STATUS, -1);
                    boolean unused = BatteryLevelView.this.f13912 = intExtra == 2 || intExtra == 5;
                    BatteryLevelView.this.setPower(((float) (intent.getIntExtra("level", -1) * 100)) / ((float) intent.getIntExtra("scale", -1)));
                }
            }
        };
        m17632();
    }

    private int getPowerColor() {
        if (this.f13912) {
            return -16711936;
        }
        if (this.f13909 <= 15.0f) {
            return SupportMenu.CATEGORY_MASK;
        }
        if (this.f13909 <= 30.0f) {
            return InputDeviceCompat.SOURCE_ANY;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getContext().registerReceiver(this.f13913, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getContext().unregisterReceiver(this.f13913);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        canvas.translate((float) (this.f13919 / 2), (float) (this.f13917 / 2));
        canvas.drawRoundRect(this.f13910, 2.0f, 2.0f, this.f13921);
        canvas.drawRoundRect(this.f13922, 2.0f, 2.0f, this.f13921);
        canvas.drawRect(this.f13923, this.f13918);
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.f13919 = View.MeasureSpec.getSize(i);
        this.f13917 = View.MeasureSpec.getSize(i2);
        setMeasuredDimension(this.f13919, this.f13917);
    }

    public void setPower(float f) {
        this.f13909 = f;
        if (this.f13909 < 0.0f) {
            this.f13909 = 0.0f;
        }
        this.f13918.setColor(getPowerColor());
        this.f13923 = new RectF(this.f13914 + (this.f13920 / 2.0f) + this.f13915 + (this.f13911 * ((100.0f - this.f13909) / 100.0f)), this.f13915 + (this.f13920 / 2.0f), this.f13907 - (this.f13915 * 2.0f), (this.f13920 / 2.0f) + this.f13915 + this.f13916);
        invalidate();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17632() {
        this.f13921 = new Paint();
        this.f13921.setColor(-7829368);
        this.f13921.setAntiAlias(true);
        this.f13921.setStyle(Paint.Style.STROKE);
        this.f13921.setStrokeWidth(this.f13920);
        this.f13918 = new Paint();
        this.f13918.setColor(getPowerColor());
        this.f13918.setAntiAlias(true);
        this.f13918.setStyle(Paint.Style.FILL);
        this.f13918.setStrokeWidth(this.f13920);
        this.f13910 = new RectF(this.f13914, 0.0f, this.f13907, this.f13906);
        this.f13922 = new RectF(0.0f, (this.f13906 - this.f13908) / 2.0f, this.f13914, ((this.f13906 - this.f13908) / 2.0f) + this.f13908);
        this.f13923 = new RectF(this.f13914 + (this.f13920 / 2.0f) + this.f13915 + (this.f13911 * ((100.0f - this.f13909) / 100.0f)), this.f13915 + (this.f13920 / 2.0f), this.f13907 - (this.f13915 * 2.0f), (this.f13920 / 2.0f) + this.f13915 + this.f13916);
    }
}
