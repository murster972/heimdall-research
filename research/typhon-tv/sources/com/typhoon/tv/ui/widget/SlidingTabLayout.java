package com.typhoon.tv.ui.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.typhoon.tv.R;
import com.typhoon.tv.utils.DeviceUtils;

public class SlidingTabLayout extends HorizontalScrollView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public ViewPager f13925;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public ViewPager.OnPageChangeListener f13926;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public OnTabClickListener f13927;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public OnTabFocusChangeListener f13928;

    /* renamed from: 连任  reason: contains not printable characters */
    private SparseIntArray f13929;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final SlidingTabStrip f13930;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f13931;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f13932;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f13933;

    private class InternalViewPagerListener implements ViewPager.OnPageChangeListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f13934;

        private InternalViewPagerListener() {
        }

        public void onPageScrollStateChanged(int i) {
            this.f13934 = i;
            if (SlidingTabLayout.this.f13926 != null) {
                SlidingTabLayout.this.f13926.onPageScrollStateChanged(i);
            }
        }

        public void onPageScrolled(int i, float f, int i2) {
            int childCount = SlidingTabLayout.this.f13930.getChildCount();
            if (childCount != 0 && i >= 0 && i < childCount) {
                SlidingTabLayout.this.f13930.m17647(i, f);
                View childAt = SlidingTabLayout.this.f13930.getChildAt(i);
                SlidingTabLayout.this.m17639(i, childAt != null ? (int) (((float) childAt.getWidth()) * f) : 0);
                if (SlidingTabLayout.this.f13926 != null) {
                    SlidingTabLayout.this.f13926.onPageScrolled(i, f, i2);
                }
            }
        }

        public void onPageSelected(int i) {
            if (this.f13934 == 0) {
                SlidingTabLayout.this.f13930.m17647(i, 0.0f);
                SlidingTabLayout.this.m17639(i, 0);
            }
            if (SlidingTabLayout.this.f13926 != null) {
                SlidingTabLayout.this.f13926.onPageSelected(i);
            }
        }
    }

    public interface OnTabClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17643(int i);
    }

    public interface OnTabFocusChangeListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17644(int i, boolean z);
    }

    private class TabClickListener implements View.OnClickListener {
        private TabClickListener() {
        }

        public void onClick(View view) {
            for (int i = 0; i < SlidingTabLayout.this.f13930.getChildCount(); i++) {
                if (view == SlidingTabLayout.this.f13930.getChildAt(i)) {
                    if (SlidingTabLayout.this.f13927 != null) {
                        SlidingTabLayout.this.f13927.m17643(i);
                    }
                    SlidingTabLayout.this.f13925.setCurrentItem(i);
                    return;
                }
            }
        }
    }

    public interface TabColorizer {
        /* renamed from: 龘  reason: contains not printable characters */
        int m17645(int i);
    }

    private class TabFocusChangeListener implements View.OnFocusChangeListener {
        private TabFocusChangeListener() {
        }

        public void onFocusChange(View view, boolean z) {
            int i = 0;
            while (i < SlidingTabLayout.this.f13930.getChildCount()) {
                if (view != SlidingTabLayout.this.f13930.getChildAt(i)) {
                    i++;
                } else if (SlidingTabLayout.this.f13928 != null) {
                    SlidingTabLayout.this.f13928.m17644(i, z);
                    return;
                } else {
                    return;
                }
            }
        }
    }

    public SlidingTabLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    public SlidingTabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SlidingTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setHorizontalScrollBarEnabled(false);
        setFillViewport(true);
        this.f13933 = (int) (24.0f * getResources().getDisplayMetrics().density);
        this.f13930 = new SlidingTabStrip(context);
        addView(this.f13930, -1, -2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17635() {
        PagerAdapter adapter = this.f13925.getAdapter();
        TabClickListener tabClickListener = new TabClickListener();
        TabFocusChangeListener tabFocusChangeListener = new TabFocusChangeListener();
        for (int i = 0; i < adapter.getCount(); i++) {
            View view = null;
            TextView textView = null;
            if (this.f13932 != 0) {
                view = LayoutInflater.from(getContext()).inflate(this.f13932, this.f13930, false);
                textView = (TextView) view.findViewById(this.f13931);
            }
            if (view == null) {
                view = m17641(getContext());
            }
            if (textView == null && TextView.class.isInstance(view)) {
                textView = (TextView) view;
            }
            if (textView != null) {
                textView.setText(adapter.getPageTitle(i));
                boolean z = getContext().getResources().getBoolean(R.bool.is_right_to_left);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
                layoutParams.addRule(13, z ? 0 : -1);
                textView.setLayoutParams(layoutParams);
            }
            view.setOnClickListener(tabClickListener);
            view.setOnFocusChangeListener(tabFocusChangeListener);
            if (!(this.f13929 == null || this.f13929.size() <= 0 || this.f13929.get(i, -1) == -1)) {
                if (DeviceUtils.m6389(new boolean[0])) {
                    View findViewById = view.findViewById(R.id.atvIndicatorLine);
                    if (findViewById != null) {
                        findViewById.setBackgroundColor(this.f13929.get(i));
                        findViewById.setVisibility(0);
                    }
                } else {
                    view.setBackgroundColor(this.f13929.get(i));
                }
            }
            this.f13930.addView(view);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17639(int i, int i2) {
        View childAt;
        int childCount = this.f13930.getChildCount();
        if (childCount != 0 && i >= 0 && i < childCount && (childAt = this.f13930.getChildAt(i)) != null) {
            int left = childAt.getLeft() + i2;
            if (i > 0 || i2 > 0) {
                left -= this.f13933;
            }
            scrollTo(left, 0);
        }
    }

    public SparseIntArray getHighlightMap() {
        return this.f13929;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f13925 != null) {
            m17639(this.f13925.getCurrentItem(), 0);
        }
    }

    public void setCustomTabColorizer(TabColorizer tabColorizer) {
        this.f13930.m17648(tabColorizer);
    }

    public void setCustomTabView(int i, int i2) {
        this.f13932 = i;
        this.f13931 = i2;
    }

    public void setCustomTabViewWithHighlight(int i, int i2, SparseIntArray sparseIntArray) {
        setCustomTabView(i, i2);
        this.f13929 = sparseIntArray;
    }

    public void setHighlightMap(SparseIntArray sparseIntArray) {
        this.f13929 = sparseIntArray;
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.f13926 = onPageChangeListener;
    }

    public void setOnTabClickListener(OnTabClickListener onTabClickListener) {
        this.f13927 = onTabClickListener;
    }

    public void setOnTabFocusChangeListener(OnTabFocusChangeListener onTabFocusChangeListener) {
        this.f13928 = onTabFocusChangeListener;
    }

    public void setSelectedIndicatorColors(int... iArr) {
        this.f13930.m17649(iArr);
    }

    public void setViewPager(ViewPager viewPager) {
        this.f13930.removeAllViews();
        this.f13925 = viewPager;
        if (viewPager != null) {
            viewPager.addOnPageChangeListener(new InternalViewPagerListener());
            m17635();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public TextView m17641(Context context) {
        TextView textView = new TextView(context);
        textView.setGravity(17);
        textView.setTextSize(2, 12.0f);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(16843534, typedValue, true);
        textView.setBackgroundResource(typedValue.resourceId);
        if (Build.VERSION.SDK_INT >= 14) {
            textView.setAllCaps(true);
        }
        int i = (int) (16.0f * getResources().getDisplayMetrics().density);
        textView.setPadding(i, i, i, i);
        return textView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17642() {
        this.f13930.removeAllViews();
        if (this.f13925 != null) {
            m17635();
        }
    }
}
