package com.typhoon.tv.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.typhoon.tv.ui.widget.SlidingTabLayout;

class SlidingTabStrip extends LinearLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private SlidingTabLayout.TabColorizer f13938;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f13939;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Paint f13940;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f13941;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SimpleTabColorizer f13942;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f13943;

    private static class SimpleTabColorizer implements SlidingTabLayout.TabColorizer {

        /* renamed from: 龘  reason: contains not printable characters */
        private int[] f13944;

        private SimpleTabColorizer() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final int m17650(int i) {
            return this.f13944[i % this.f13944.length];
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17651(int... iArr) {
            this.f13944 = iArr;
        }
    }

    SlidingTabStrip(Context context) {
        this(context, (AttributeSet) null);
    }

    SlidingTabStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setWillNotDraw(false);
        float f = getResources().getDisplayMetrics().density;
        this.f13942 = new SimpleTabColorizer();
        this.f13942.m17651(-13388315);
        this.f13943 = (int) (2.0f * f);
        this.f13940 = new Paint();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m17646(int i, int i2, float f) {
        float f2 = 1.0f - f;
        return Color.rgb((int) ((((float) Color.red(i)) * f) + (((float) Color.red(i2)) * f2)), (int) ((((float) Color.green(i)) * f) + (((float) Color.green(i2)) * f2)), (int) ((((float) Color.blue(i)) * f) + (((float) Color.blue(i2)) * f2)));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int height = getHeight();
        int childCount = getChildCount();
        SlidingTabLayout.TabColorizer tabColorizer = this.f13938 != null ? this.f13938 : this.f13942;
        if (childCount > 0) {
            View childAt = getChildAt(this.f13941);
            int left = childAt.getLeft();
            int right = childAt.getRight();
            int r7 = tabColorizer.m17645(this.f13941);
            if (this.f13939 > 0.0f && this.f13941 < getChildCount() - 1) {
                int r10 = tabColorizer.m17645(this.f13941 + 1);
                if (r7 != r10) {
                    r7 = m17646(r10, r7, this.f13939);
                }
                View childAt2 = getChildAt(this.f13941 + 1);
                left = (int) ((this.f13939 * ((float) childAt2.getLeft())) + ((1.0f - this.f13939) * ((float) left)));
                right = (int) ((this.f13939 * ((float) childAt2.getRight())) + ((1.0f - this.f13939) * ((float) right)));
            }
            this.f13940.setColor(r7);
            canvas.drawRect((float) left, (float) (height - this.f13943), (float) right, (float) height, this.f13940);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17647(int i, float f) {
        this.f13941 = i;
        this.f13939 = f;
        invalidate();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17648(SlidingTabLayout.TabColorizer tabColorizer) {
        this.f13938 = tabColorizer;
        invalidate();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17649(int... iArr) {
        this.f13938 = null;
        this.f13942.m17651(iArr);
        invalidate();
    }
}
