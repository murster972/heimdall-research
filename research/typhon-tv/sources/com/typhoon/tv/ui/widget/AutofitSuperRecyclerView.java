package com.typhoon.tv.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.ui.GridLayoutManagerWrapper;
import com.typhoon.tv.ui.GridSpacingItemDecoration;
import com.typhoon.tv.ui.LinearLayoutManagerWrapper;
import com.typhoon.tv.utils.DeviceUtils;

public class AutofitSuperRecyclerView extends SuperRecyclerView {

    /* renamed from: ᴵ  reason: contains not printable characters */
    private RecyclerView.LayoutManager f13903;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f13904 = -1;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f13905 = false;

    public AutofitSuperRecyclerView(Context context) {
        super(context);
        m17630((AttributeSet) null);
    }

    public AutofitSuperRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m17630(attributeSet);
    }

    public AutofitSuperRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m17630(attributeSet);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17630(AttributeSet attributeSet) {
        if (TVApplication.m6285().getBoolean("pref_modern_ui", true)) {
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, new int[]{16843031});
                this.f13904 = obtainStyledAttributes.getDimensionPixelSize(0, -1);
                if (this.f13904 > 0) {
                    switch (TVApplication.m6285().getInt("pref_poster_size", 1)) {
                        case 0:
                            this.f13904 = (int) (((double) this.f13904) * 0.75d);
                            break;
                        case 2:
                            this.f13904 = (int) (((double) this.f13904) * 1.33d);
                            break;
                    }
                }
                obtainStyledAttributes.recycle();
            }
            this.f13903 = new GridLayoutManagerWrapper(getContext(), 1);
            setLayoutManager(this.f13903);
        } else if (DeviceUtils.m6389(new boolean[0]) || getResources().getConfiguration().orientation == 2) {
            this.f13903 = new GridLayoutManagerWrapper(getContext(), 2);
            setLayoutManager(this.f13903);
            m26368((RecyclerView.ItemDecoration) new GridSpacingItemDecoration(2, 0, true));
        } else {
            this.f13903 = new LinearLayoutManagerWrapper(getContext(), 1, false);
            setLayoutManager(this.f13903);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f13903 == null) {
            return;
        }
        if (((this.f13903 instanceof GridLayoutManagerWrapper) || (this.f13903 instanceof GridLayoutManager)) && this.f13904 > 0) {
            int max = Math.max(1, getMeasuredWidth() / this.f13904);
            ((GridLayoutManager) this.f13903).setSpanCount(max);
            if (!this.f13905) {
                m26368((RecyclerView.ItemDecoration) new GridSpacingItemDecoration(max, getContext().getResources().getDimensionPixelSize(R.dimen.image_poster_spacing), true));
                this.f13905 = true;
            }
        }
    }
}
