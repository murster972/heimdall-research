package com.typhoon.tv.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.typhoon.tv.R;

public class DownloadItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final ProgressBar f13879;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ImageButton f13880;

    /* renamed from: ʽ  reason: contains not printable characters */
    private OnCardClickListener f13881;

    /* renamed from: 连任  reason: contains not printable characters */
    public final ImageView f13882;

    /* renamed from: 靐  reason: contains not printable characters */
    public final TextView f13883;

    /* renamed from: 麤  reason: contains not printable characters */
    public final TextView f13884;

    /* renamed from: 齉  reason: contains not printable characters */
    public final TextView f13885;

    /* renamed from: 龘  reason: contains not printable characters */
    public final ImageView f13886;

    public interface OnCardClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17619(View view, ImageButton imageButton, int i);
    }

    public DownloadItemViewHolder(View view) {
        super(view);
        this.f13886 = (ImageView) view.findViewById(R.id.ivBanner);
        this.f13883 = (TextView) view.findViewById(R.id.tvTitleYear);
        View findViewById = view.findViewById(R.id.ivPlay);
        View findViewById2 = view.findViewById(R.id.overlay);
        this.f13885 = (TextView) view.findViewById(R.id.tvDownloadPath);
        this.f13884 = (TextView) view.findViewById(R.id.tvFileSize);
        this.f13882 = (ImageView) view.findViewById(R.id.ivDownload);
        this.f13879 = (ProgressBar) view.findViewById(R.id.pbDownload);
        this.f13880 = (ImageButton) view.findViewById(R.id.ibManageDownload);
        view.setOnClickListener(this);
        this.f13886.setOnClickListener(this);
        this.f13883.setOnClickListener(this);
        findViewById.setOnClickListener(this);
        findViewById2.setOnClickListener(this);
        this.f13885.setOnClickListener(this);
        this.f13884.setOnClickListener(this);
        this.f13882.setOnClickListener(this);
        this.f13879.setOnClickListener(this);
        this.f13880.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.f13881 != null) {
            this.f13881.m17619(view, this.f13880, getAdapterPosition());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17618(OnCardClickListener onCardClickListener) {
        this.f13881 = onCardClickListener;
    }
}
