package com.typhoon.tv.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.typhoon.tv.R;

public class TvCalendarItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private OnCardLongClickListener f13897;

    /* renamed from: 连任  reason: contains not printable characters */
    private OnCardClickListener f13898;

    /* renamed from: 靐  reason: contains not printable characters */
    public final TextView f13899;

    /* renamed from: 麤  reason: contains not printable characters */
    public final TextView f13900;

    /* renamed from: 齉  reason: contains not printable characters */
    public final TextView f13901;

    /* renamed from: 龘  reason: contains not printable characters */
    public final ImageView f13902;

    public interface OnCardClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17628(View view, int i);
    }

    public interface OnCardLongClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17629(View view, int i);
    }

    public TvCalendarItemViewHolder(View view) {
        super(view);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(R.id.rlContainer);
        ViewGroup viewGroup2 = (ViewGroup) view.findViewById(R.id.llContainer);
        this.f13902 = (ImageView) view.findViewById(R.id.ivBanner);
        this.f13899 = (TextView) view.findViewById(R.id.tvEpisodeNumName);
        this.f13901 = (TextView) view.findViewById(R.id.tvTitleYear);
        this.f13900 = (TextView) view.findViewById(R.id.tvSynopsis);
        view.setOnClickListener(this);
        viewGroup.setOnClickListener(this);
        viewGroup2.setOnClickListener(this);
        this.f13902.setOnClickListener(this);
        this.f13899.setOnClickListener(this);
        this.f13901.setOnClickListener(this);
        this.f13900.setOnClickListener(this);
        view.setOnLongClickListener(this);
        viewGroup.setOnLongClickListener(this);
        viewGroup2.setOnLongClickListener(this);
        this.f13902.setOnLongClickListener(this);
        this.f13899.setOnLongClickListener(this);
        this.f13901.setOnLongClickListener(this);
        this.f13900.setOnLongClickListener(this);
    }

    public void onClick(View view) {
        if (this.f13898 != null) {
            this.f13898.m17628(view, getAdapterPosition());
        }
    }

    public boolean onLongClick(View view) {
        if (this.f13897 == null) {
            return true;
        }
        this.f13897.m17629(view, getAdapterPosition());
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17626(OnCardClickListener onCardClickListener) {
        this.f13898 = onCardClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17627(OnCardLongClickListener onCardLongClickListener) {
        this.f13897 = onCardLongClickListener;
    }
}
