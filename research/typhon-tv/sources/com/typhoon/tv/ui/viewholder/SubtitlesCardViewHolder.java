package com.typhoon.tv.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.typhoon.tv.R;

public class SubtitlesCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    public final TextView f13894;

    /* renamed from: 齉  reason: contains not printable characters */
    private OnCardClickListener f13895;

    /* renamed from: 龘  reason: contains not printable characters */
    public final TextView f13896;

    public interface OnCardClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17625(int i);
    }

    public SubtitlesCardViewHolder(View view) {
        super(view);
        this.f13896 = (TextView) view.findViewById(R.id.tvSubName);
        this.f13894 = (TextView) view.findViewById(R.id.tvSubLanguage);
        view.setOnClickListener(this);
        this.f13896.setOnClickListener(this);
        this.f13894.setOnClickListener(this);
        view.findViewById(R.id.tvSubNameText).setOnClickListener(this);
        view.findViewById(R.id.tvSubLanguageText).setOnClickListener(this);
        view.findViewById(R.id.cvSubLl1).setOnClickListener(this);
        view.findViewById(R.id.cvSubLl2).setOnClickListener(this);
        view.findViewById(R.id.cvSubLl3).setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.f13895 != null) {
            this.f13895.m17625(getAdapterPosition());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17624(OnCardClickListener onCardClickListener) {
        this.f13895 = onCardClickListener;
    }
}
