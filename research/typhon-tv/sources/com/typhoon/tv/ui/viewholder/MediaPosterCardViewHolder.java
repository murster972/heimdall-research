package com.typhoon.tv.ui.viewholder;

import android.view.View;
import android.widget.TextView;
import com.typhoon.tv.R;

public class MediaPosterCardViewHolder extends MediaCardViewHolder {

    /* renamed from: 连任  reason: contains not printable characters */
    public final TextView f13893;

    public MediaPosterCardViewHolder(View view) {
        super(view);
        this.f13893 = (TextView) view.findViewById(R.id.tvYear);
        this.f13893.setOnClickListener(this);
    }
}
