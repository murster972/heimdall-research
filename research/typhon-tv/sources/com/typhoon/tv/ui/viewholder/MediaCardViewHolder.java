package com.typhoon.tv.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.typhoon.tv.R;

public class MediaCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private OnCardLongClickListener f13887;

    /* renamed from: 连任  reason: contains not printable characters */
    private OnCardClickListener f13888;

    /* renamed from: 靐  reason: contains not printable characters */
    public final TextView f13889;

    /* renamed from: 麤  reason: contains not printable characters */
    public final View f13890;

    /* renamed from: 齉  reason: contains not printable characters */
    public final TextView f13891;

    /* renamed from: 龘  reason: contains not printable characters */
    public final ImageView f13892;

    public interface OnCardClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17622(int i);
    }

    public interface OnCardLongClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17623(View view, int i);
    }

    public MediaCardViewHolder(View view) {
        super(view);
        View findViewById = view.findViewById(R.id.ivBanner);
        if (findViewById != null) {
            this.f13892 = (ImageView) findViewById;
            this.f13891 = (TextView) view.findViewById(R.id.tvTime);
            this.f13890 = view.findViewById(R.id.indicatorLine);
        } else {
            this.f13892 = null;
            this.f13891 = null;
            this.f13890 = null;
        }
        this.f13889 = (TextView) view.findViewById(R.id.tvTvTitle);
        if (this.f13892 != null) {
            this.f13892.setOnClickListener(this);
            this.f13892.setOnLongClickListener(this);
        }
        if (this.f13891 != null) {
            this.f13891.setOnClickListener(this);
            this.f13891.setOnLongClickListener(this);
        }
        if (this.f13890 != null) {
            this.f13890.setOnClickListener(this);
            this.f13890.setOnLongClickListener(this);
        }
        this.f13889.setOnClickListener(this);
        this.f13889.setOnLongClickListener(this);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
    }

    public void onClick(View view) {
        if (this.f13888 != null) {
            this.f13888.m17622(getAdapterPosition());
        }
    }

    public boolean onLongClick(View view) {
        if (this.f13887 == null) {
            return true;
        }
        this.f13887.m17623(view, getAdapterPosition());
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17620(OnCardClickListener onCardClickListener) {
        this.f13888 = onCardClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17621(OnCardLongClickListener onCardLongClickListener) {
        this.f13887 = onCardLongClickListener;
    }
}
