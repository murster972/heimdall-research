package com.typhoon.tv.ui;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import com.typhoon.tv.Logger;

public class GridLayoutManagerWrapper extends GridLayoutManager {
    public GridLayoutManagerWrapper(Context context, int i) {
        super(context, i);
    }

    public GridLayoutManagerWrapper(Context context, int i, int i2, boolean z) {
        super(context, i, i2, z);
    }

    public GridLayoutManagerWrapper(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        } catch (IndexOutOfBoundsException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }
}
