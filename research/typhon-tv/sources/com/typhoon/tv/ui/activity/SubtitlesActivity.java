package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.Pinkamena;
import com.Ty.subtitle.converter.subtitleFile.FormatTTML;
import com.Ty.xapp.event.AdStatusEvent;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.cast.MediaMetadata;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.mopub.nativeads.MoPubNativeAdLoadedListener;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.MoPubRecyclerAdapter;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.RequestParameters;
import com.mopub.nativeads.ViewBinder;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.backup.SubsMapBackupRestoreHelper;
import com.typhoon.tv.chromecast.CastHelper;
import com.typhoon.tv.event.OnInterstitialAdShownEvent;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.helper.TapTargetViewHelper;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.player.ExoPlayerHelper;
import com.typhoon.tv.helper.player.YesPlayerHelper;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.presenter.ISubtitlesPresenter;
import com.typhoon.tv.presenter.impl.SubtitlesPresenterImpl;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.subtitles.SubtitlesConverter;
import com.typhoon.tv.ui.GridLayoutManagerWrapper;
import com.typhoon.tv.ui.GridSpacingItemDecoration;
import com.typhoon.tv.ui.LinearLayoutManagerWrapper;
import com.typhoon.tv.ui.activity.base.BasePlayActivity;
import com.typhoon.tv.ui.adapter.SubtitlesAdapter;
import com.typhoon.tv.ui.viewholder.SubtitlesCardViewHolder;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.Downloader;
import com.typhoon.tv.utils.GmsUtils;
import com.typhoon.tv.utils.IntentUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.SourceUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.ISubtitlesView;
import com.typhoon.tv.webserver.CastSubtitlesWebServer;
import com.typhoon.tv.webserver.WebServerManager;
import com.typhoon.tv.webserver.WebServerService;
import fi.iki.elonen.NanoHTTPD;
import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import xiaofei.library.hermeseventbus.HermesEventBus;

public class SubtitlesActivity extends BasePlayActivity implements ISubtitlesView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f13455;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private ScheduledFuture<?> f13456;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f13457;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public ISubtitlesPresenter f13458;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f13459;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public Spinner f13460;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public String f13461;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f13462;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean f13463 = true;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public MoPubRecyclerAdapter f13464;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public SubtitlesAdapter f13465;

    /* renamed from: ˎ  reason: contains not printable characters */
    private ProgressDialog f13466;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean f13467 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f13468;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f13469 = false;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public Subscription f13470;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f13471;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f13472;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private boolean f13473 = false;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private Subscription f13474;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private ScheduledExecutorService f13475;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private Subscription f13476;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f13477 = 0;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private ArrayList<Snackbar> f13478;
    /* access modifiers changed from: private */

    /* renamed from: ⁱ  reason: contains not printable characters */
    public Snackbar f13479;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public MediaInfo f13480;

    /* renamed from: 靐  reason: contains not printable characters */
    private final EnumSet<RequestParameters.NativeAdAsset> f13481 = EnumSet.of(RequestParameters.NativeAdAsset.TITLE, RequestParameters.NativeAdAsset.TEXT, RequestParameters.NativeAdAsset.ICON_IMAGE, RequestParameters.NativeAdAsset.MAIN_IMAGE, RequestParameters.NativeAdAsset.CALL_TO_ACTION_TEXT);
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public MediaSource f13482;

    /* renamed from: 齉  reason: contains not printable characters */
    private final RequestParameters f13483 = new RequestParameters.Builder().desiredAssets(this.f13481).build();
    /* access modifiers changed from: private */

    /* renamed from: ﹳ  reason: contains not printable characters */
    public boolean f13484 = false;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public ArrayList<String> f13485;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public SuperRecyclerView f13486;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean f13487 = false;

    private interface OnOneSubtitlesFileSelected {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17215(int i);
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private void m17140() {
        this.f13487 = true;
        this.f13462 = true;
        Toast.makeText(this, I18N.m15706(R.string.download_started), 0).show();
        finish();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static /* synthetic */ int m17148(SubtitlesActivity subtitlesActivity) {
        int i = subtitlesActivity.f13477;
        subtitlesActivity.f13477 = i + 1;
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public void m17157() {
        if (!TyphoonApp.f5865) {
            if ((!this.f13467 && this.f13477 < 1) || m17422(new boolean[0])) {
                m17421();
            }
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private void m17159() {
        setTitle(I18N.m15706(R.string.subtitle));
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey("mediaSrc") || !extras.containsKey("mediaInfo") || !extras.containsKey("playTitle")) {
            m17200(I18N.m15706(R.string.error));
            finish();
            return;
        }
        this.f13482 = (MediaSource) extras.getParcelable("mediaSrc");
        this.f13480 = (MediaInfo) extras.getParcelable("mediaInfo");
        this.f13455 = extras.getInt("season", -1);
        this.f13457 = extras.getInt("episode", -1);
        this.f13468 = this.f13480.getType() == 1;
        this.f13471 = extras.getString("playTitle");
        this.f13472 = extras.getBoolean("isDownload", false);
        this.f13467 = extras.getBoolean("isSubtitlesAdShown", this.f13467);
        this.f13473 = extras.getBoolean("isSubtitlesChildAppAdShown", this.f13473);
        this.f13486 = (SuperRecyclerView) findViewById(R.id.rvSubtitlesList);
        if (DeviceUtils.m6389(new boolean[0]) || getResources().getConfiguration().orientation == 2) {
            this.f13486.setLayoutManager(new GridLayoutManagerWrapper(this, 2));
            this.f13486.m26368((RecyclerView.ItemDecoration) new GridSpacingItemDecoration(2, 0, true));
        } else {
            this.f13486.setLayoutManager(new LinearLayoutManagerWrapper(this, 1, false));
        }
        this.f13465 = new SubtitlesAdapter(new ArrayList());
        this.f13465.m17519((SubtitlesCardViewHolder.OnCardClickListener) new SubtitlesCardViewHolder.OnCardClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17212(final int i) {
                if (!NetworkUtils.m17799()) {
                    SubtitlesActivity.this.m17200(I18N.m15706(R.string.no_internet));
                } else if (i < 0) {
                    SubtitlesActivity.this.m17200(I18N.m15706(R.string.error));
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
                    if (Build.VERSION.SDK_INT > 25) {
                        arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                    }
                    Subscription unused = SubtitlesActivity.this.f13470 = new RxPermissions(SubtitlesActivity.this).m15681((String[]) arrayList.toArray(new String[arrayList.size()])).m7376(new Func1<Throwable, Boolean>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Boolean call(Throwable th) {
                            return false;
                        }
                    }).m7397(new Action1<Boolean>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void call(Boolean bool) {
                            if (bool.booleanValue()) {
                                int originalPosition = SubtitlesActivity.this.f13464.getOriginalPosition(i);
                                if (originalPosition < 0) {
                                    SubtitlesActivity.this.m17200(I18N.m15706(R.string.error));
                                    return;
                                }
                                SubtitlesInfo r0 = SubtitlesActivity.this.f13465.m17516(originalPosition);
                                SubtitlesActivity.this.f13458.m16155(r0.getService(), r0.getDownloadPageUrl());
                                return;
                            }
                            Toast.makeText(SubtitlesActivity.this, I18N.m15706(R.string.permission_grant_toast), 1).show();
                            Utils.m6420((Context) SubtitlesActivity.this);
                        }
                    });
                }
            }
        });
        this.f13464 = new MoPubRecyclerAdapter((Activity) this, (RecyclerView.Adapter) this.f13465, MoPubNativeAdPositioning.clientPositioning().addFixedPosition(1).enableRepeatingPositions(10));
        this.f13464.registerAdRenderer(new MoPubStaticNativeAdRenderer(new ViewBinder.Builder(R.layout.item_native_ad_recyclerview).titleId(R.id.tvNativeAdTitle).textId(R.id.tvNativeAdText).mainImageId(R.id.ivNativeAdImage).iconImageId(R.id.ivNativeAdIcon).callToActionId(R.id.btnNativeAdCta).build()));
        this.f13464.setAdLoadedListener(new MoPubNativeAdLoadedListener() {
            public void onAdLoaded(int i) {
                SubtitlesActivity.this.m17174(SubtitlesActivity.this.f13484);
            }

            public void onAdRemoved(int i) {
            }
        });
        this.f13486.setAdapter(this.f13464);
        this.f13486.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        this.f13486.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                SubtitlesActivity.this.f13458.m16154();
                if (NetworkUtils.m17799()) {
                    SubtitlesActivity.this.f13458.m16156(SubtitlesActivity.this.f13480, SubtitlesActivity.this.f13455, SubtitlesActivity.this.f13457, SubtitlesActivity.this.f13461);
                    return;
                }
                SubtitlesActivity.this.m17200(I18N.m15706(R.string.no_internet));
                SubtitlesActivity.this.m17174(false);
            }
        });
        this.f13486.m26367();
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSubtitles);
        setSupportActionBar(toolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.m427(true);
        }
        this.f13460 = (Spinner) findViewById(R.id.toolbar_spinner_subtitles);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, this.f13485);
        arrayAdapter.setDropDownViewResource(17367049);
        this.f13460.setAdapter(arrayAdapter);
        this.f13460.setSelection(0);
        this.f13460.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                if (SubtitlesActivity.this.f13463) {
                    boolean unused = SubtitlesActivity.this.f13463 = false;
                } else if (!NetworkUtils.m17799()) {
                    SubtitlesActivity.this.m17200(I18N.m15706(R.string.no_internet));
                } else {
                    String unused2 = SubtitlesActivity.this.f13461 = (String) SubtitlesActivity.this.f13485.get(i);
                    SubtitlesActivity.this.f13458.m16156(SubtitlesActivity.this.f13480, SubtitlesActivity.this.f13455, SubtitlesActivity.this.f13457, SubtitlesActivity.this.f13461);
                    SubtitlesActivity.this.m17196();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (!TapTargetViewHelper.m15965("ttv_subtitles") && !DeviceUtils.m6389(new boolean[0])) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    try {
                        new TapTargetSequence((Activity) SubtitlesActivity.this).targets(TapTarget.forView(SubtitlesActivity.this.f13460, I18N.m15706(R.string.ttv_switch_subtitles_provider), I18N.m15706(R.string.ttv_switch_subtitles_provider_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(false).id(1), TapTarget.forToolbarMenuItem(toolbar, (int) R.id.action_switch_player, (CharSequence) I18N.m15706(R.string.ttv_switch_video_player), (CharSequence) I18N.m15706(R.string.ttv_switch_video_player_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(false).id(2)).start();
                        TapTargetViewHelper.m15964("ttv_subtitles");
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }, 1000);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public void m17160() {
        if (this.f13478 != null) {
            Iterator<Snackbar> it2 = this.f13478.iterator();
            while (it2.hasNext()) {
                Snackbar next = it2.next();
                if (next.isShownOrQueued()) {
                    next.dismiss();
                }
            }
            this.f13478 = null;
        }
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private void m17161() {
        this.f13458 = new SubtitlesPresenterImpl(this);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private void m17162() {
        this.f13721 = BasePlayerHelper.m16032();
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    private void m17163() {
        this.f13485 = new ArrayList<>();
        BaseSubtitlesService[] r2 = Locale.getDefault().getLanguage().toLowerCase().startsWith("zh") ? TyphoonApp.m6337() : TyphoonApp.m6327();
        this.f13461 = r2[0].m6379();
        for (BaseSubtitlesService r1 : r2) {
            this.f13485.add(r1.m6379());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ⁱ  reason: contains not printable characters */
    public void m17164() {
        m17188();
        try {
            this.f13475 = Executors.newSingleThreadScheduledExecutor();
            this.f13456 = this.f13475.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
                        final boolean r0 = SubtitlesActivity.this.m17420();
                        SubtitlesActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    HermesEventBus.getDefault().post(new AdStatusEvent(r0));
                                } catch (Throwable th) {
                                    Logger.m6281(th, new boolean[0]);
                                }
                            }
                        });
                    } catch (Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                    }
                }
            }, 0, 10, TimeUnit.MINUTES);
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17167(MediaSource mediaSource) {
        m17180(mediaSource, CastHelper.m15832(CastHelper.m15834(this.f13480, this.f13455, this.f13457, mediaSource), mediaSource), false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17169(ArrayList<String> arrayList) {
        File file = new File(new File(new File(TVApplication.m6285().getString("pref_media_down_path", Utils.m6397())), this.f13471), "Subtitles");
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!file.isDirectory()) {
            file.delete();
            file.mkdirs();
        }
        Iterator<String> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            File file2 = new File(it2.next());
            file2.renameTo(new File(file, file2.getName()));
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17171(String str) {
        boolean z = true;
        String r5 = Utils.m6408(str);
        if (r5.isEmpty()) {
            r5 = this.f13482.isHLS() ? ".m3u8" : ".mp4";
        } else if (!r5.equalsIgnoreCase(".avi") && !r5.equalsIgnoreCase(".rmvb") && !r5.equalsIgnoreCase(".flv") && !r5.equalsIgnoreCase(".mkv") && !r5.equalsIgnoreCase(".mp2") && !r5.equalsIgnoreCase(".mp2v") && !r5.equalsIgnoreCase(".mp4") && !r5.equalsIgnoreCase(".mp4v") && !r5.equalsIgnoreCase(".mpe") && !r5.equalsIgnoreCase(".mpeg") && !r5.equalsIgnoreCase(".mpeg1") && !r5.equalsIgnoreCase(".mpeg2") && !r5.equalsIgnoreCase(".mpeg4") && !r5.equalsIgnoreCase(".mpg") && !r5.equalsIgnoreCase(".ts") && !r5.equalsIgnoreCase(".m3u8")) {
            r5 = this.f13482.isHLS() ? ".m3u8" : ".mp4";
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        if (Build.VERSION.SDK_INT >= 16) {
            intent.setDataAndTypeAndNormalize(Uri.parse(str), "video/*");
        } else {
            intent.setDataAndType(Uri.parse(str), "video/*");
        }
        intent.addCategory("android.intent.category.DEFAULT");
        intent.putExtra(PubnativeAsset.TITLE, this.f13471 + r5);
        intent.putExtra("suppress_error_message", false);
        intent.putExtra("secure_uri", true);
        intent.putExtra("fromOtherApp", true);
        intent.putExtra("gdprScope", TyphoonApp.f5874);
        intent.putExtra("showStartupADM", TyphoonApp.f5868);
        intent.putExtra("showErrorMessage", true);
        intent.putExtra("hls", this.f13482.isHLS());
        intent.putExtra("providerName", this.f13482.getProviderName());
        intent.putExtra("hostAppName", getPackageName());
        if (!m17420() || BasePlayerHelper.m16037()) {
            z = false;
        }
        intent.putExtra("showHostAppAd", z);
        intent.putExtra("disableAds", TyphoonApp.f5865);
        intent.putExtra("isChildAppAdShown", this.f13473);
        intent.putExtra("forceATVMode", DeviceUtils.m6389(new boolean[0]));
        intent.putExtra("subsFontScale", TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f));
        intent.putExtra("subsFontColorHex", TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        intent.putExtra("subsBgColorHex", TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        if (this.f13482.getPlayHeader() != null && !this.f13482.getPlayHeader().isEmpty()) {
            HashMap<String, String> r6 = SourceUtils.m17835(this.f13482.getPlayHeader());
            Bundle bundle = new Bundle();
            ArrayList arrayList = new ArrayList();
            for (Map.Entry next : r6.entrySet()) {
                bundle.putString((String) next.getKey(), (String) next.getValue());
                arrayList.add(next.getKey());
                arrayList.add(next.getValue());
            }
            intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
            intent.putExtra("android.media.intent.extra.HTTP_HEADERS", bundle);
            String str2 = null;
            if (r6.containsKey("Cookie")) {
                str2 = r6.get("Cookie");
            } else if (r6.containsKey("cookie")) {
                str2 = r6.get("cookie");
            }
            if (str2 != null) {
                intent.putExtra("Cookies", str2);
            }
        }
        Intent r1 = IntentUtils.m17788(this, intent, I18N.m15706(R.string.choose_external_download_manager));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(r1, 1112);
            m17164();
            return;
        }
        m17200(I18N.m15706(R.string.no_external_download_manager_was_found));
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m17174(final boolean z) {
        this.f13486.getSwipeToRefresh().post(new Runnable() {
            public void run() {
                SubtitlesActivity.this.f13486.getSwipeToRefresh().setRefreshing(z);
            }
        });
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m17176(String str) {
        String r6 = Utils.m6408(str);
        if (r6.isEmpty()) {
            r6 = this.f13482.isHLS() ? ".m3u8" : ".mp4";
        } else if (!r6.equalsIgnoreCase(".avi") && !r6.equalsIgnoreCase(".rmvb") && !r6.equalsIgnoreCase(".flv") && !r6.equalsIgnoreCase(".mkv") && !r6.equalsIgnoreCase(".mp2") && !r6.equalsIgnoreCase(".mp2v") && !r6.equalsIgnoreCase(".mp4") && !r6.equalsIgnoreCase(".mp4v") && !r6.equalsIgnoreCase(".mpe") && !r6.equalsIgnoreCase(".mpeg") && !r6.equalsIgnoreCase(".mpeg1") && !r6.equalsIgnoreCase(".mpeg2") && !r6.equalsIgnoreCase(".mpeg4") && !r6.equalsIgnoreCase(".mpg") && !r6.equalsIgnoreCase(".ts") && !r6.equalsIgnoreCase(".m3u8")) {
            r6 = this.f13482.isHLS() ? ".m3u8" : ".mp4";
        }
        return Downloader.m17787(TVApplication.m6288(), str, this.f13471 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + this.f13471 + r6, this.f13480, this.f13471, SourceUtils.m17835(this.f13482.getPlayHeader()));
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17180(final MediaSource mediaSource, final com.google.android.gms.cast.MediaInfo mediaInfo, boolean z) {
        if (m17185(mediaSource)) {
            if (this.f13478 == null) {
                this.f13478 = new ArrayList<>();
            }
            this.f13478.add(m17199(I18N.m15706(R.string.please_wait), 0));
            if (m17288()) {
                m17431(this.f13480, this.f13455, this.f13457, this.f13471, new BasePlayActivity.OnReceiveLastPlaybackPositionListener() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m17209(long j) {
                        boolean z = false;
                        try {
                            z = SubtitlesActivity.this.m17304(mediaInfo, j);
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        if (z) {
                            SubtitlesActivity.this.m17160();
                            if (Utils.m6410()) {
                                Utils.m6422(false);
                            }
                            Toast.makeText(SubtitlesActivity.this, I18N.m15706(R.string.preparing_to_cast), 1).show();
                            SubtitlesActivity.this.m17157();
                            return;
                        }
                        SubtitlesActivity.this.m17200(I18N.m15706(R.string.error));
                    }
                });
            } else if (!z) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        SubtitlesActivity.this.m17180(mediaSource, mediaInfo, true);
                    }
                }, 2000);
            } else {
                m17160();
                m17199(I18N.m15706(R.string.chromecast_not_connected), 0);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17181(final MediaSource mediaSource, final ArrayList<String> arrayList) {
        if (m17185(mediaSource)) {
            if (this.f13479 != null && this.f13479.isShownOrQueued()) {
                this.f13479.dismiss();
            }
            this.f13479 = m17199(I18N.m15706(R.string.loading), -2);
            final MediaMetadata r0 = CastHelper.m15834(this.f13480, this.f13455, this.f13457, mediaSource);
            if (this.f13476 != null && !this.f13476.isUnsubscribed()) {
                this.f13476.unsubscribe();
            }
            final LinkedList linkedList = new LinkedList();
            this.f13476 = Observable.m7359(new Observable.OnSubscribe<Map<String, String>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super Map<String, String>> subscriber) {
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    int i = 1;
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        String str = (String) it2.next();
                        try {
                            String str2 = "sub-" + i + "-" + DateTimeHelper.m15934() + "-" + new Random().nextInt(99999) + ".ttml";
                            String r5 = SubtitlesConverter.m16818(str, new FormatTTML(), false);
                            if (r5 != null && !r5.isEmpty()) {
                                linkedHashMap.put(str2, r5);
                                linkedList.add(str);
                                i++;
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                    try {
                        SubsMapBackupRestoreHelper.m15828(linkedHashMap);
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                    subscriber.onNext(linkedHashMap);
                    subscriber.onCompleted();
                }
            }).m7383((Action0) new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17207() {
                    SubtitlesActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (SubtitlesActivity.this.f13479 != null && !SubtitlesActivity.this.f13479.isShownOrQueued()) {
                                SubtitlesActivity.this.f13479.show();
                            }
                        }
                    });
                }
            }).m7376(new Func1<Throwable, Map<String, String>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Map<String, String> call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    try {
                        SubsMapBackupRestoreHelper.m15826();
                        return null;
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                        return null;
                    }
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Map<String, String>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(final Map<String, String> map) {
                    SubtitlesActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (SubtitlesActivity.this.f13479 != null && SubtitlesActivity.this.f13479.isShownOrQueued()) {
                                SubtitlesActivity.this.f13479.dismiss();
                            }
                            if (map == null || map.isEmpty()) {
                                try {
                                    SubsMapBackupRestoreHelper.m15826();
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                }
                                SubtitlesActivity.this.m17199(I18N.m15706(R.string.subtitle_extract_failed), 0);
                                return;
                            }
                            if (WebServerManager.m17893().m17896() == null) {
                                WebServerManager.m17893().m17899((NanoHTTPD) new CastSubtitlesWebServer(59104));
                            }
                            WebServerManager.m17893().m17900((Map<String, String>) map);
                            SubtitlesActivity.this.startService(new Intent(SubtitlesActivity.this, WebServerService.class));
                            SubtitlesActivity.this.m17180(mediaSource, CastHelper.m15833(r0, mediaSource, (List<String>) linkedList, (List<String>) new LinkedList(map.keySet())), false);
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17184(ArrayList<String> arrayList, final OnOneSubtitlesFileSelected onOneSubtitlesFileSelected) {
        new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.choose_subtitles_file)).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!SubtitlesActivity.this.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        }).m540((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                onOneSubtitlesFileSelected.m17215(i);
                if (!SubtitlesActivity.this.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        }).m528();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17185(MediaSource mediaSource) {
        if (DeviceUtils.m6388()) {
            m17199(I18N.m15706(R.string.google_play_services_not_installed), 0);
            return false;
        } else if (GmsUtils.m6392((Context) this)) {
            if (!m17305(true)) {
                m17199(I18N.m15706(R.string.chromecast_not_connected), 0);
                return false;
            }
            HashMap<String, String> playHeader = mediaSource.getPlayHeader();
            if ((playHeader == null || playHeader.isEmpty() || (!playHeader.containsKey("Cookie") && !playHeader.containsKey("Referer"))) && !Utils.m6408(mediaSource.getStreamLink()).equalsIgnoreCase(".flv")) {
                return true;
            }
            new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) I18N.m15706(R.string.cant_be_casted_message)).m538(true).m524((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!SubtitlesActivity.this.isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            }).m528();
            return false;
        } else if (GmsUtils.m6391((Activity) this)) {
            return false;
        } else {
            m17199(I18N.m15706(R.string.google_play_services_not_installed), 0);
            return false;
        }
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    private void m17188() {
        if (this.f13456 != null) {
            try {
                this.f13456.cancel(true);
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
        if (this.f13475 != null) {
            try {
                this.f13475.shutdownNow();
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
        }
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private void m17191() {
        MoPubRecyclerAdapter moPubRecyclerAdapter = this.f13464;
        String str = TyphoonApp.f5838;
        RequestParameters requestParameters = this.f13483;
        Pinkamena.DianePie();
    }

    public void finish() {
        Intent intent = new Intent();
        intent.putExtra("isSubtitlesAdShown", this.f13467);
        intent.putExtra("isSubtitlesChildAppAdShown", this.f13473);
        intent.putExtra("isDownload", this.f13472);
        intent.putExtra("isDownloaded", this.f13462);
        intent.putExtra("isPlayed", this.f13469);
        intent.putExtra("forceShowAd", this.f13487 && !this.f13473);
        intent.putExtra("mediaSrc", this.f13482);
        setResult(-1, intent);
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        int r8 = this.f13721.m16047((AppCompatActivity) this, i, i2, intent);
        if (r8 > -1) {
            m17188();
            boolean z = true;
            if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey("isChildAppAdShown") && intent.getBooleanExtra("isChildAppAdShown", false)) {
                z = false;
                this.f13473 = true;
                this.f13467 = true;
                this.f13477++;
            }
            if (r8 == 1) {
                if (this.f13721 instanceof ExoPlayerHelper) {
                    m17430(this.f13721, this.f13468, this.f13480.getTmdbId(), this.f13455, this.f13457, intent, true);
                }
                m17200(I18N.m15706(R.string.video_player_crashed_unexpectedly));
                this.f13459 = false;
                return;
            }
            this.f13469 = true;
            m17430(this.f13721, this.f13468, this.f13480.getTmdbId(), this.f13455, this.f13457, intent, false);
            this.f13459 = false;
            if (z) {
                m17157();
            }
        } else if (i == 1112) {
            m17188();
            if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey("isChildAppAdShown") && intent.getBooleanExtra("isChildAppAdShown", false)) {
                this.f13473 = true;
                this.f13467 = true;
                this.f13477++;
            }
            m17140();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_subtitles);
        m17284();
        m17163();
        m17159();
        m17161();
        m17162();
        boolean z = false;
        if (bundle != null && !bundle.isEmpty()) {
            int i = bundle.getInt("currentListNum", 0);
            if (i > -1 && i < this.f13485.size()) {
                getSupportActionBar().m437(i);
                this.f13461 = this.f13485.get(i);
            }
            ArrayList parcelableArrayList = bundle.getParcelableArrayList("subtitles");
            if (parcelableArrayList != null) {
                m17202((List<SubtitlesInfo>) parcelableArrayList);
                z = true;
            }
            this.f13469 = bundle.getBoolean("isPlayed", this.f13469);
            this.f13462 = bundle.getBoolean("isDownloaded", this.f13462);
            this.f13459 = bundle.getBoolean("isPlaying", this.f13459);
            this.f13487 = bundle.getBoolean("returnForceShowAd", this.f13487);
            this.f13467 = bundle.getBoolean("isSubtitlesAdShown", this.f13467);
            this.f13473 = bundle.getBoolean("isSubtitlesChildAppAdShown", this.f13473);
            this.f13477 = bundle.getInt("adShownTimes", this.f13477);
        }
        View findViewById = findViewById(R.id.osSnackbar);
        if (findViewById != null) {
            findViewById.setVisibility(this.f13461.equalsIgnoreCase("OpenSubtitles") ? 0 : 8);
        }
        if (!z) {
            if (NetworkUtils.m17799()) {
                this.f13458.m16156(this.f13480, this.f13455, this.f13457, this.f13461);
                m17196();
            } else {
                m17200(I18N.m15706(R.string.no_internet));
            }
        }
        m17419();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem findItem;
        getMenuInflater().inflate(R.menu.menu_subtitles, menu);
        m17295(menu);
        if (!this.f13472 && (findItem = menu.findItem(R.id.action_play_without_subtitle)) != null) {
            findItem.setVisible(true);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        m17198();
        m17188();
        if (this.f13458 != null) {
            this.f13458.m16153();
        }
        this.f13458 = null;
        if (this.f13470 != null && !this.f13470.isUnsubscribed()) {
            this.f13470.unsubscribe();
        }
        if (this.f13474 != null && !this.f13474.isUnsubscribed()) {
            this.f13474.unsubscribe();
        }
        if (this.f13476 != null && !this.f13476.isUnsubscribed()) {
            this.f13476.unsubscribe();
        }
        if (this.f13464 != null) {
            this.f13464.destroy();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_play_without_subtitle:
                if (this.f13458 != null) {
                    this.f13458.m16152();
                }
                if (m17305(true)) {
                    m17167(this.f13482);
                } else {
                    m17431(this.f13480, this.f13455, this.f13457, this.f13471, new BasePlayActivity.OnReceiveLastPlaybackPositionListener() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m17204(long j) {
                            if (SubtitlesActivity.this.f13721.m16045(SubtitlesActivity.this, SubtitlesActivity.this.f13482, SubtitlesActivity.this.f13471, j)) {
                                boolean unused = SubtitlesActivity.this.f13459 = true;
                                if (SubtitlesActivity.this.f13721 instanceof YesPlayerHelper) {
                                    SubtitlesActivity.this.m17164();
                                }
                                Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.ready_to_play_toast), 0).show();
                            }
                        }
                    });
                }
                return true;
            case R.id.action_switch_player:
                if (!this.f13459) {
                    m17429();
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("currentListNum", getSupportActionBar().m434());
        if (this.f13465 != null) {
            bundle.putParcelableArrayList("subtitles", this.f13465.m17518());
        }
        bundle.putBoolean("isPlayed", this.f13469);
        bundle.putBoolean("isDownloaded", this.f13462);
        bundle.putBoolean("isPlaying", this.f13459);
        bundle.putBoolean("returnForceShowAd", this.f13487);
        bundle.putBoolean("isSubtitlesAdShown", this.f13467);
        bundle.putBoolean("isSubtitlesChildAppAdShown", this.f13473);
        bundle.putInt("adShownTimes", this.f13477);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.f13474 == null && !TyphoonApp.f5865) {
            this.f13474 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
                public void call(Object obj) {
                    if (obj instanceof OnInterstitialAdShownEvent) {
                        boolean unused = SubtitlesActivity.this.f13467 = true;
                        SubtitlesActivity.m17148(SubtitlesActivity.this);
                    }
                }
            });
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m17192() {
        m17199(I18N.m15706(R.string.subtitle_download_failed), 0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m17193() {
        m17199(I18N.m15706(R.string.subtitle_extract_failed), 0);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m17194() {
        if (this.f13460.getSelectedItemPosition() != 0 || !NetworkUtils.m17799()) {
            m17198();
        } else {
            this.f13460.setSelection(1, true);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m17195() {
        if (!isFinishing() && this.f13466 != null && this.f13466.isShowing()) {
            try {
                this.f13466.dismiss();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17196() {
        this.f13484 = true;
        this.f13486.setRefreshing(true);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m17197() {
        try {
            this.f13466 = new ProgressDialog(this);
            this.f13466.setTitle(I18N.m15706(R.string.please_wait));
            this.f13466.setMessage(I18N.m15706(R.string.downloading_subtitle_toast));
            this.f13466.setIndeterminate(true);
            this.f13466.setCancelable(true);
            this.f13466.setCanceledOnTouchOutside(false);
            this.f13466.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    SubtitlesActivity.this.f13458.m16152();
                    if (!SubtitlesActivity.this.isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            this.f13466.show();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m17198() {
        this.f13486.m26364();
        this.f13486.m26363();
        this.f13484 = false;
        this.f13486.setRefreshing(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Snackbar m17199(String str, int i) {
        final Snackbar make = Snackbar.make(findViewById(R.id.subtitles_rootLayout), (CharSequence) str, i);
        make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View view) {
                make.dismiss();
            }
        }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        make.show();
        return make;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17200(String str) {
        m17199(str, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17201(final ArrayList<String> arrayList) {
        final ArrayList arrayList2 = new ArrayList();
        Iterator<String> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            arrayList2.add(new File(it2.next()).getName());
        }
        if (this.f13472) {
            String streamLink = this.f13482.getStreamLink();
            m17169(arrayList);
            if (TVApplication.m6285().getInt("pref_choose_media_down_manager", 0) != 0) {
                m17171(streamLink);
            } else if (!Downloader.m17786((Context) this)) {
                Downloader.m17781(this).show();
            } else if (m17176(streamLink)) {
                m17140();
            }
        } else {
            if (m17305(true)) {
                m17181(this.f13482, arrayList);
                return;
            }
            m17431(this.f13480, this.f13455, this.f13457, this.f13471, new BasePlayActivity.OnReceiveLastPlaybackPositionListener() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17210(final long j) {
                    if (SubtitlesActivity.this.f13721.m16040() || arrayList.size() <= 1 || arrayList2.size() <= 1) {
                        if (SubtitlesActivity.this.f13721.m16046(SubtitlesActivity.this, SubtitlesActivity.this.f13482, SubtitlesActivity.this.f13471, j, arrayList, arrayList2)) {
                            boolean unused = SubtitlesActivity.this.f13459 = true;
                            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.ready_to_play_toast), 0).show();
                            return;
                        }
                        return;
                    }
                    SubtitlesActivity.this.m17184((ArrayList<String>) arrayList2, (OnOneSubtitlesFileSelected) new OnOneSubtitlesFileSelected() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m17211(int i) {
                            try {
                                ArrayList arrayList = new ArrayList();
                                arrayList.add((String) arrayList2.get(i));
                                ArrayList arrayList2 = new ArrayList();
                                arrayList2.add((String) arrayList.get(i));
                                if (SubtitlesActivity.this.f13721.m16046(SubtitlesActivity.this, SubtitlesActivity.this.f13482, SubtitlesActivity.this.f13471, j, arrayList2, arrayList)) {
                                    boolean unused = SubtitlesActivity.this.f13459 = true;
                                    Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.ready_to_play_toast), 0).show();
                                }
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                                if (SubtitlesActivity.this.f13721.m16046(SubtitlesActivity.this, SubtitlesActivity.this.f13482, SubtitlesActivity.this.f13471, j, arrayList, arrayList2)) {
                                    boolean unused2 = SubtitlesActivity.this.f13459 = true;
                                    Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.ready_to_play_toast), 0).show();
                                }
                            }
                        }
                    });
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17202(List<SubtitlesInfo> list) {
        int i = 0;
        if (this.f13460 == null || ((list != null && !list.isEmpty()) || this.f13460.getSelectedItemPosition() != 0 || !NetworkUtils.m17799())) {
            this.f13465.m17515();
            this.f13465.m17521(list);
            this.f13486.getRecyclerView().smoothScrollToPosition(0);
            m17198();
            if (!TyphoonApp.f5865) {
                m17191();
            }
            View findViewById = findViewById(R.id.osSnackbar);
            if (findViewById != null) {
                if (!this.f13461.equalsIgnoreCase("OpenSubtitles")) {
                    i = 8;
                }
                findViewById.setVisibility(i);
                return;
            }
            return;
        }
        this.f13460.setSelection(1, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17203() {
        return this.f13473;
    }
}
