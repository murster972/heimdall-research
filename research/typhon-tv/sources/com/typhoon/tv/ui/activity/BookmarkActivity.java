package com.typhoon.tv.ui.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.api.TraktUserApi;
import com.typhoon.tv.event.UpdateBookmarkEvent;
import com.typhoon.tv.exception.FailedToSyncTraktCollectionsException;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.helper.trakt.TraktHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.ViewPagerStateAdapter;
import com.typhoon.tv.ui.fragment.BookmarkFragment;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import java.util.ArrayList;
import java.util.List;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;

public class BookmarkActivity extends BaseAdActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public Subscription f13061;

    /* renamed from: 麤  reason: contains not printable characters */
    private Action1<Throwable> f13062 = new Action1<Throwable>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void call(final Throwable th) {
            if (th instanceof FailedToSyncTraktCollectionsException) {
                boolean unused = BookmarkActivity.this.f13063 = true;
                if (NetworkUtils.m17799()) {
                    Logger.m6281(th, true);
                }
                BookmarkActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        FailedToSyncTraktCollectionsException failedToSyncTraktCollectionsException = (FailedToSyncTraktCollectionsException) th;
                        Toast makeText = Toast.makeText(BookmarkActivity.this, "", 0);
                        if (failedToSyncTraktCollectionsException.getTransferType() == 1) {
                            makeText.setText("Failed to upload favorites to Trakt.tv collections...\n\nReason:\n%s");
                        } else if (failedToSyncTraktCollectionsException.getTransferType() == 2) {
                            Object[] objArr = new Object[2];
                            objArr[0] = failedToSyncTraktCollectionsException.getMediaType() == 1 ? "movies" : "TV shows";
                            objArr[1] = failedToSyncTraktCollectionsException.getMessage();
                            makeText.setText(String.format("Failed to download %s favorites from Trakt.tv collections...\n\nReason:\n%s", objArr));
                        }
                        makeText.show();
                    }
                });
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f13063 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private ViewPager f13064;

    /* renamed from: 靐  reason: contains not printable characters */
    private void m16849() {
        m17423(findViewById(R.id.adViewBookmark));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m16850() {
        if (!NetworkUtils.m17799()) {
            Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
        } else if (this.f13064 != null) {
            Toast.makeText(this, I18N.m15706(R.string.please_wait), 0).show();
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(I18N.m15706(R.string.please_wait));
            progressDialog.setMessage(I18N.m15706(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(0);
            progressDialog.setIndeterminate(true);
            this.f13061 = TraktUserApi.m15774().m15782(true, this.f13062).m7383((Action0) new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m16855() {
                    progressDialog.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (BookmarkActivity.this.f13061 != null && !BookmarkActivity.this.f13061.isUnsubscribed()) {
                                BookmarkActivity.this.f13061.unsubscribe();
                            }
                            if (!BookmarkActivity.this.isFinishing() && dialogInterface != null) {
                                dialogInterface.dismiss();
                            }
                        }
                    });
                    progressDialog.show();
                }
            }).m7386(new Subscriber<Object>() {
                public void onCompleted() {
                    if (!BookmarkActivity.this.isFinishing() && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (BookmarkActivity.this.f13063) {
                        TraktHelper.m16113();
                    }
                    boolean unused = BookmarkActivity.this.f13063 = false;
                }

                public void onError(Throwable th) {
                    Logger.m6280(th, "onError", new boolean[0]);
                    boolean unused = BookmarkActivity.this.f13063 = true;
                }

                public void onNext(Object obj) {
                    if (obj != null) {
                        if (obj instanceof SyncResponse) {
                            BookmarkActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(BookmarkActivity.this, "Uploaded favorites to Trakt.tv collections successfully!", 0).show();
                                }
                            });
                        } else if (obj instanceof List) {
                            boolean z = false;
                            for (Object next : (List) obj) {
                                try {
                                    if (next instanceof MediaInfo) {
                                        MediaInfo mediaInfo = (MediaInfo) next;
                                        ArrayList<MediaInfo> r2 = TVApplication.m6287().m6297(Integer.valueOf(mediaInfo.getType()), Integer.valueOf(mediaInfo.getTmdbId()));
                                        if (r2.isEmpty()) {
                                            TVApplication.m6287().m6299(mediaInfo);
                                        } else {
                                            MediaInfo mediaInfo2 = r2.get(0);
                                            if (mediaInfo2.getPosterUrl() == null || mediaInfo2.getPosterUrl().isEmpty()) {
                                                mediaInfo2.setPosterUrl(mediaInfo.getPosterUrl());
                                                mediaInfo2.save();
                                            }
                                        }
                                        z = mediaInfo.getType() == 1;
                                    }
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                }
                            }
                            RxBus.m15709().m15711(new UpdateBookmarkEvent());
                            final boolean z2 = z;
                            BookmarkActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    BookmarkActivity bookmarkActivity = BookmarkActivity.this;
                                    Object[] objArr = new Object[1];
                                    objArr[0] = z2 ? "movies" : "TV shows";
                                    Toast.makeText(bookmarkActivity, String.format("Downloaded %s favorites from Trakt.tv collections successfully!", objArr), 0).show();
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16851() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarBookmark);
        setSupportActionBar(toolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), toolbar);
        this.f13064 = (ViewPager) findViewById(R.id.viewpagerBookmark);
        this.f13064.setSaveEnabled(false);
        this.f13064.setOffscreenPageLimit(1);
        ViewPagerStateAdapter viewPagerStateAdapter = new ViewPagerStateAdapter(getSupportFragmentManager());
        viewPagerStateAdapter.m17534(BookmarkFragment.m17537(0), I18N.m15706(R.string.tv_shows));
        viewPagerStateAdapter.m17534(BookmarkFragment.m17537(1), I18N.m15706(R.string.movies));
        this.f13064.setAdapter(viewPagerStateAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsBookmark);
        tabLayout.setupWithViewPager(this.f13064);
        tabLayout.setVisibility(0);
        this.f13064.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(this.f13064));
        if (tabLayout.getTabCount() > 0) {
            tabLayout.getTabAt(0).select();
        } else {
            tabLayout.setVisibility(8);
        }
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (IllegalStateException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_bookmark);
        m17284();
        m16851();
        m16849();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bookmark, menu);
        m17295(menu);
        if (TraktCredentialsHelper.m16110().isValid()) {
            menu.findItem(R.id.action_sync_bookmark).setVisible(true);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f13061 != null && !this.f13061.isUnsubscribed()) {
            this.f13061.unsubscribe();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_sync_bookmark:
                m16850();
                TraktUserApi.m15774().m15781(true, true);
                TraktUserApi.m15774().m15785(true, true);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
