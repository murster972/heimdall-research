package com.typhoon.tv.ui.activity.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.Pinkamena;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.AdinCubeBannerEventListener;
import com.adincube.sdk.AdinCubeNativeEventListener;
import com.adincube.sdk.BannerView;
import com.evernote.android.state.State;
import com.livefront.bridge.Bridge;
import com.mopub.common.FullAdType;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.nativeads.MoPubNative;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.NativeAd;
import com.mopub.nativeads.NativeErrorCode;
import com.mopub.nativeads.RequestParameters;
import com.mopub.nativeads.ViewBinder;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.OnInterstitialAdShownEvent;
import com.typhoon.tv.utils.NativeAdsUtils;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import rx.Subscription;

@SuppressLint({"Registered"})
public class BaseAdActivity extends BaseActivity {
    @State
    public boolean mIsSecondAdShown = false;
    @State
    public int mLoadOguryTrialCount = 0;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f13705 = false;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f13706 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Subscription f13707;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public NativeAd f13708;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MoPubInterstitial f13709;

    /* renamed from: 麤  reason: contains not printable characters */
    private BannerView f13710;

    /* renamed from: 齉  reason: contains not printable characters */
    private MoPubNative f13711;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public List<com.adincube.sdk.NativeAd> f13712;

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m17402(ViewGroup viewGroup) {
        if (isFinishing() || viewGroup == null || this.f13711 == null || this.f13708 == null || this.f13708.isDestroyed()) {
            this.f13705 = true;
            return;
        }
        try {
            View createAdView = this.f13708.createAdView(this, (ViewGroup) null);
            this.f13708.renderAdView(createAdView);
            this.f13708.prepare(createAdView);
            try {
                TextView textView = (TextView) createAdView.findViewById(R.id.btnNativeAdCta);
                if (textView.getText() != null && textView.getText().toString().toLowerCase().startsWith("download now")) {
                    textView.setText("Download\nNow!");
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
            layoutParams.height = (int) TypedValue.applyDimension(1, (float) getResources().getInteger(R.integer.native_height), getResources().getDisplayMetrics());
            viewGroup.addView(createAdView, layoutParams);
            viewGroup.setVisibility(0);
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            this.f13705 = true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17404(final ViewGroup viewGroup) {
        try {
            final boolean z = TVApplication.m6285().getBoolean("is_nad_mech_enabled", false);
            AnonymousClass6 r2 = new AdinCubeNativeEventListener() {
                public void onAdLoaded(List<com.adincube.sdk.NativeAd> list) {
                    if (BaseAdActivity.this.f13712 != null && !BaseAdActivity.this.f13712.isEmpty()) {
                        AdinCube.Native.m2314((List<com.adincube.sdk.NativeAd>) BaseAdActivity.this.f13712);
                        List unused = BaseAdActivity.this.f13712 = null;
                    }
                    List unused2 = BaseAdActivity.this.f13712 = list;
                    if (z) {
                        try {
                            List unused3 = BaseAdActivity.this.f13712 = NativeAdsUtils.m17796((List<com.adincube.sdk.NativeAd>) BaseAdActivity.this.f13712);
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                    if (BaseAdActivity.this.f13705 && !BaseAdActivity.this.f13706) {
                        BaseAdActivity.this.m17407(viewGroup);
                    }
                }

                public void onLoadError(String str) {
                    super.onLoadError(str);
                }
            };
            if (z) {
                AdinCube.Native.m2309(this, 3, r2);
            } else {
                AdinCube.Native.m2310((Context) this, (AdinCubeNativeEventListener) r2);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m17407(ViewGroup viewGroup) {
        if (!isFinishing() && viewGroup != null && this.f13712 != null && !this.f13712.isEmpty() && this.f13712.get(0) != null) {
            try {
                com.adincube.sdk.NativeAd nativeAd = this.f13712.get(0);
                this.f13706 = true;
                RelativeLayout relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_native_ad_banner, (ViewGroup) null).findViewById(R.id.relativeLayoutNativeAd);
                AdinCube.Native.m2312((ImageView) relativeLayout.findViewById(R.id.ivNativeAdIcon), nativeAd.m2360());
                if (nativeAd.m2358() != null) {
                    AdinCube.Native.m2312((ImageView) relativeLayout.findViewById(R.id.ivNativeAdImage), nativeAd.m2358());
                }
                TextView textView = (TextView) relativeLayout.findViewById(R.id.tvNativeAdTitle);
                textView.setText(nativeAd.m2362());
                if (textView.getEllipsize() != null && textView.getEllipsize().equals(TextUtils.TruncateAt.MARQUEE)) {
                    textView.setSelected(true);
                    textView.setSingleLine(true);
                }
                if (nativeAd.m2361() != null) {
                    String r3 = nativeAd.m2361();
                    if (nativeAd.m2356() != null && ((double) nativeAd.m2356().floatValue()) >= 4.5d) {
                        r3 = "★ " + String.valueOf(nativeAd.m2356()) + " - " + r3;
                    }
                    ((TextView) relativeLayout.findViewById(R.id.tvNativeAdText)).setText(r3);
                }
                String r2 = nativeAd.m2359();
                if (r2 != null) {
                    if (r2.toLowerCase().startsWith("download now")) {
                        r2 = "Download\nNow!";
                    }
                    ((TextView) relativeLayout.findViewById(R.id.btnNativeAdCta)).setText(r2);
                }
                relativeLayout.findViewById(R.id.ivNativeAdPrivacy).setVisibility(8);
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
                layoutParams.height = (int) TypedValue.applyDimension(1, (float) getResources().getInteger(R.integer.native_height), getResources().getDisplayMetrics());
                AdinCube.Native.m2311((ViewGroup) relativeLayout, nativeAd);
                viewGroup.addView(relativeLayout, layoutParams);
                viewGroup.setVisibility(0);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17409(final ViewGroup viewGroup) {
        try {
            this.f13711 = new MoPubNative(this, TyphoonApp.f5838, new MoPubNative.MoPubNativeNetworkListener() {
                public void onNativeFail(NativeErrorCode nativeErrorCode) {
                    if (nativeErrorCode == null || nativeErrorCode.name() != null) {
                    }
                    boolean unused = BaseAdActivity.this.f13705 = true;
                    if (BaseAdActivity.this.f13712 != null && !BaseAdActivity.this.f13712.isEmpty() && BaseAdActivity.this.f13712.get(0) != null && !BaseAdActivity.this.f13706) {
                        BaseAdActivity.this.m17407(viewGroup);
                    }
                }

                public void onNativeLoad(NativeAd nativeAd) {
                    if (BaseAdActivity.this.f13708 != null && !BaseAdActivity.this.f13708.isDestroyed()) {
                        BaseAdActivity.this.f13708.destroy();
                    }
                    NativeAd unused = BaseAdActivity.this.f13708 = nativeAd;
                    if (!BaseAdActivity.this.f13706) {
                        BaseAdActivity.this.m17402(viewGroup);
                    }
                }
            });
            this.f13711.registerAdRenderer(new MoPubStaticNativeAdRenderer(new ViewBinder.Builder(R.layout.item_native_ad_banner).titleId(R.id.tvNativeAdTitle).textId(R.id.tvNativeAdText).iconImageId(R.id.ivNativeAdIcon).callToActionId(R.id.btnNativeAdCta).build()));
            RequestParameters build = new RequestParameters.Builder().desiredAssets(EnumSet.of(RequestParameters.NativeAdAsset.TITLE, RequestParameters.NativeAdAsset.TEXT, RequestParameters.NativeAdAsset.ICON_IMAGE, RequestParameters.NativeAdAsset.CALL_TO_ACTION_TEXT)).build();
            HashMap hashMap = new HashMap();
            hashMap.put("isBanner", true);
            hashMap.put("loadAdinCubeNativeBanner", false);
            this.f13711.setLocalExtras(hashMap);
            this.f13711.makeRequest(build);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            this.f13705 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17414(final ViewGroup viewGroup) {
        if (this.f13710 != null) {
            try {
                this.f13710.m2352();
                this.f13710 = null;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        this.f13710 = AdinCube.Banner.m2302((Context) this, AdinCube.Banner.Size.BANNER_AUTO);
        AdinCube.Banner.m2303(this.f13710, (AdinCubeBannerEventListener) new AdinCubeBannerEventListener() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m17424(BannerView bannerView) {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m17425(BannerView bannerView, String str) {
            }

            /* renamed from: 齉  reason: contains not printable characters */
            public void m17426(BannerView bannerView) {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m17427(BannerView bannerView) {
                boolean unused = BaseAdActivity.this.f13705 = true;
                boolean unused2 = BaseAdActivity.this.f13706 = true;
                try {
                    viewGroup.removeAllViews();
                } catch (Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }
                viewGroup.addView(bannerView, new ViewGroup.LayoutParams(-1, -2));
                viewGroup.setVisibility(0);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m17428(BannerView bannerView, String str) {
            }
        });
        this.f13710.setAutoDestroyOnDetach(true);
        this.f13710.m2353();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Bridge.restoreInstanceState(this, bundle);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (bundle != null && !bundle.isEmpty()) {
            if (bundle.containsKey("loadOguryTrialCount")) {
                this.mLoadOguryTrialCount = bundle.getInt("loadOguryTrialCount", 0);
            }
            if (bundle.containsKey("isSecondAdShown")) {
                this.mIsSecondAdShown = bundle.getBoolean("isSecondAdShown", false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13707 != null && !this.f13707.isUnsubscribed()) {
            this.f13707.unsubscribe();
        }
        if (this.f13709 != null) {
            this.f13709.destroy();
        }
        this.f13709 = null;
        if (this.f13710 != null) {
            this.f13710.m2352();
        }
        this.f13710 = null;
        if (this.f13708 != null && !this.f13708.isDestroyed()) {
            this.f13708.destroy();
        }
        this.f13708 = null;
        if (this.f13711 != null) {
            this.f13711.destroy();
        }
        this.f13711 = null;
        if (this.f13712 != null && this.f13712.size() > 0) {
            AdinCube.Native.m2314(this.f13712);
        }
        this.f13712 = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        try {
            super.onSaveInstanceState(bundle);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        try {
            Bridge.saveInstanceState(this, bundle);
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: ˆ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m17417() {
        /*
            r8 = this;
            r3 = 0
            r4 = 1
            r0 = 0
            r1 = 0
            java.lang.String r1 = com.adincube.sdk.AdinCube.Interstitial.m2306()     // Catch:{ Throwable -> 0x002c }
        L_0x0008:
            if (r1 == 0) goto L_0x002b
            boolean r5 = r1.isEmpty()     // Catch:{ Throwable -> 0x0038 }
            if (r5 != 0) goto L_0x002b
            java.lang.String r5 = r1.trim()     // Catch:{ Throwable -> 0x0038 }
            java.lang.String r1 = r5.toLowerCase()     // Catch:{ Throwable -> 0x0038 }
            java.lang.String r5 = "unity"
            boolean r5 = r1.contains(r5)     // Catch:{ Throwable -> 0x0038 }
            if (r5 != 0) goto L_0x002a
            java.lang.String r5 = "rtb"
            boolean r5 = r1.equalsIgnoreCase(r5)     // Catch:{ Throwable -> 0x0038 }
            if (r5 == 0) goto L_0x0041
        L_0x002a:
            r0 = r4
        L_0x002b:
            return r0
        L_0x002c:
            r2 = move-exception
            r5 = 1
            boolean[] r5 = new boolean[r5]     // Catch:{ Throwable -> 0x0038 }
            r6 = 0
            r7 = 1
            r5[r6] = r7     // Catch:{ Throwable -> 0x0038 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r5)     // Catch:{ Throwable -> 0x0038 }
            goto L_0x0008
        L_0x0038:
            r2 = move-exception
            boolean[] r5 = new boolean[r4]
            r5[r3] = r4
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r5)
            goto L_0x002b
        L_0x0041:
            r0 = r3
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.base.BaseAdActivity.m17417():boolean");
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m17418() {
        String str = null;
        try {
            str = AdinCube.Interstitial.m2306();
        } catch (Throwable th) {
            Logger.m6281(th, true);
        }
        if (str != null && !str.isEmpty()) {
            String lowerCase = str.trim().toLowerCase();
            if (lowerCase.contains("ogury") || lowerCase.contains("presage")) {
                TyphoonApp.f5873 = true;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m17419() {
        if (this.f13709 != null) {
            try {
                this.f13709.destroy();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            this.f13709 = null;
        }
        this.f13709 = new MoPubInterstitial(this, TyphoonApp.f5837);
        this.f13709.setInterstitialAdListener(new MoPubInterstitial.InterstitialAdListener() {
            public void onInterstitialClicked(MoPubInterstitial moPubInterstitial) {
            }

            public void onInterstitialDismissed(MoPubInterstitial moPubInterstitial) {
                if (BaseAdActivity.this.f13709 != null) {
                    MoPubInterstitial r0 = BaseAdActivity.this.f13709;
                    Pinkamena.DianePie();
                }
            }

            public void onInterstitialFailed(MoPubInterstitial moPubInterstitial, MoPubErrorCode moPubErrorCode) {
            }

            public void onInterstitialLoaded(MoPubInterstitial moPubInterstitial) {
            }

            public void onInterstitialShown(MoPubInterstitial moPubInterstitial) {
                RxBus.m15709().m15711(new OnInterstitialAdShownEvent());
                if (!TyphoonApp.f5847 || !TyphoonApp.m6336()) {
                }
            }
        });
        MoPubInterstitial moPubInterstitial = this.f13709;
        Pinkamena.DianePie();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m17420() {
        return ((m17418() || m17417()) && AdinCube.Interstitial.m2305(this)) || (this.f13709 != null && this.f13709.isReady());
    }

    /* access modifiers changed from: protected */
    /* renamed from: י  reason: contains not printable characters */
    public void m17421() {
        if (!isFinishing()) {
            if ((m17418() || m17417()) && AdinCube.Interstitial.m2305(this)) {
                AdinCube.Interstitial.m2304(this);
            } else if (this.f13709 != null && this.f13709.isReady()) {
                MoPubInterstitial moPubInterstitial = this.f13709;
                Pinkamena.DianePieNull();
            } else if (AdinCube.Interstitial.m2305(this)) {
                AdinCube.Interstitial.m2304(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m17422(boolean... zArr) {
        boolean z = zArr != null && zArr.length > 0 && zArr[0];
        if (TyphoonApp.f5865) {
            return false;
        }
        if ((!z && TyphoonApp.f5866) || m17418()) {
            return true;
        }
        if (!TyphoonApp.f5847 || TyphoonApp.m6336() || !z) {
        }
        boolean z2 = true;
        if (this.f13709 != null && this.f13709.isReady()) {
            String customEventClassName = this.f13709.getMoPubInterstitialView().getCustomEventClassName();
            if (customEventClassName != null) {
                Logger.m6279("BaseAdActivity:Ads", "MoPub ad is ready: customEventClassName = " + customEventClassName);
                if (customEventClassName.trim().toLowerCase().contains("inmobi") && customEventClassName.trim().toLowerCase().contains("video")) {
                    z2 = false;
                }
                if (customEventClassName.trim().toLowerCase().contains("viai")) {
                    z2 = false;
                }
                if (customEventClassName.trim().toLowerCase().contains("loopme")) {
                    z2 = false;
                }
                if (customEventClassName.trim().toLowerCase().contains(FullAdType.VAST) && customEventClassName.trim().toLowerCase().contains("video")) {
                    z2 = false;
                }
            } else if (z) {
                z2 = false;
            }
            if (z2) {
                return true;
            }
        }
        String str = null;
        String str2 = null;
        try {
            str = AdinCube.Interstitial.m2306();
        } catch (Throwable th) {
            Logger.m6281(th, true);
        }
        if (str != null && !str.isEmpty()) {
            str2 = "|" + str.trim().toLowerCase() + "|";
        }
        if (z) {
            return str2 != null && "|ogury|presage|aerserv|rtb|".contains(str2);
        }
        if (str2 != null && TyphoonApp.f5842.contains(str2)) {
            return true;
        }
        if (!TyphoonApp.m6336() || this.mIsSecondAdShown || str2 == null || !TyphoonApp.f5843.contains(str2)) {
            return false;
        }
        this.mIsSecondAdShown = true;
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17423(View view) {
        if (!TyphoonApp.f5865 && view != null && (view instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) view;
            try {
                viewGroup.removeAllViews();
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
            try {
                if (this.f13708 != null && !this.f13708.isDestroyed()) {
                    this.f13708.destroy();
                }
                this.f13708 = null;
                if (this.f13711 != null) {
                    this.f13711.destroy();
                }
                this.f13711 = null;
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
            if (this.f13712 != null && !this.f13712.isEmpty()) {
                AdinCube.Native.m2314(this.f13712);
                this.f13712 = null;
            }
            m17414(viewGroup);
            m17404(viewGroup);
            m17409(viewGroup);
        }
    }
}
