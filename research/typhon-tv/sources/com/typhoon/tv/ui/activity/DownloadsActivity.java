package com.typhoon.tv.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.evernote.android.state.State;
import com.google.android.exoplayer2.util.MimeTypes;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.OnInterstitialAdShownEvent;
import com.typhoon.tv.model.download.DownloadItem;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.ViewPagerStateAdapter;
import com.typhoon.tv.ui.fragment.DownloadsFragment;
import com.typhoon.tv.utils.IntentUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import java.io.File;
import net.pubnative.library.request.PubnativeAsset;
import rx.Subscription;
import rx.functions.Action1;

public class DownloadsActivity extends BaseAdActivity {
    @State
    public int mAdShownTimes = 0;
    @State
    public boolean mIsInterstitialShown = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private ViewPagerStateAdapter f13076;

    /* renamed from: 齉  reason: contains not printable characters */
    private Subscription f13077;

    /* renamed from: 龘  reason: contains not printable characters */
    private ViewPager f13078;

    /* renamed from: 靐  reason: contains not printable characters */
    private void m16856() {
        m17423(findViewById(R.id.adViewDownloads));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m16857() {
        if (!TyphoonApp.f5865) {
            if ((!this.mIsInterstitialShown && this.mAdShownTimes < 1) || m17422(new boolean[0])) {
                m17421();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16858() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarDownloads);
        setSupportActionBar(toolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), toolbar);
        this.f13078 = (ViewPager) findViewById(R.id.viewpagerDownloads);
        this.f13078.setSaveEnabled(false);
        this.f13078.setOffscreenPageLimit(1);
        this.f13076 = new ViewPagerStateAdapter(getSupportFragmentManager());
        this.f13076.m17534(DownloadsFragment.m17546(0), I18N.m15706(R.string.in_progress));
        this.f13076.m17534(DownloadsFragment.m17546(1), I18N.m15706(R.string.finished));
        this.f13078.setAdapter(this.f13076);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsDownloads);
        tabLayout.setupWithViewPager(this.f13078);
        tabLayout.setVisibility(0);
        this.f13078.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        this.f13078.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int i) {
            }

            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                DownloadsActivity.this.invalidateOptionsMenu();
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(this.f13078));
        if (tabLayout.getTabCount() > 0) {
            tabLayout.getTabAt(0).select();
        } else {
            tabLayout.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 525) {
            m16857();
        }
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (IllegalStateException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_downloads);
        m17284();
        m16858();
        m16856();
        m17419();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_downloads, menu);
        m17295(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_remove_all:
                Fragment item = this.f13076.getItem(this.f13078.getCurrentItem());
                if (item != null && (item instanceof DownloadsFragment)) {
                    ((DownloadsFragment) item).m17551();
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.f13077 == null && !TyphoonApp.f5865) {
            this.f13077 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
                public void call(Object obj) {
                    if (obj instanceof OnInterstitialAdShownEvent) {
                        DownloadsActivity.this.mIsInterstitialShown = true;
                        DownloadsActivity.this.mAdShownTimes++;
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16859(DownloadItem downloadItem) {
        String title = downloadItem.getTitle();
        String localUri = downloadItem.getLocalUri();
        if (new File(localUri).exists()) {
            Intent intent = new Intent("android.intent.action.VIEW");
            if (Build.VERSION.SDK_INT >= 16) {
                intent.setDataAndTypeAndNormalize(Uri.parse(localUri), MimeTypes.VIDEO_MP4);
            } else {
                intent.setDataAndType(Uri.parse(localUri), MimeTypes.VIDEO_MP4);
            }
            intent.putExtra(PubnativeAsset.TITLE, title);
            Intent r0 = IntentUtils.m17788(this, intent, I18N.m15706(R.string.choose_player));
            if (intent.resolveActivity(getPackageManager()) != null) {
                if (this.f13077 != null && !this.f13077.isUnsubscribed()) {
                    this.f13077.unsubscribe();
                    this.f13077 = null;
                }
                startActivityForResult(r0, 525);
                return;
            }
            m16860(I18N.m15706(R.string.no_player_was_found), 0);
            return;
        }
        m16860(I18N.m15706(R.string.file_not_found), 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16860(String str, int i) {
        final Snackbar make = Snackbar.make(findViewById(R.id.downloads_rootLayout), (CharSequence) str, i);
        make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View view) {
                make.dismiss();
            }
        }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        make.show();
    }
}
