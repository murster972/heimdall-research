package com.typhoon.tv.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.TextView;
import android.widget.Toast;
import com.androidadvance.topsnackbar.TSnackbar;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.trakt.TraktGetTokenFailedEvent;
import com.typhoon.tv.event.trakt.TraktGetTokenSuccessEvent;
import com.typhoon.tv.event.trakt.TraktUserCancelledAuthEvent;
import com.typhoon.tv.ui.activity.base.BaseWebViewActivity;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import rx.Subscription;
import rx.functions.Action1;

public class TraktAuthWebViewActivity extends BaseWebViewActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public TSnackbar f13533;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f13534;

    /* renamed from: 齉  reason: contains not printable characters */
    private Subscription f13535;

    /* renamed from: 龘  reason: contains not printable characters */
    private WebView f13536;

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17219(boolean isLoading) {
        int i = 0;
        findViewById(R.id.webView).setVisibility(isLoading ? 8 : 0);
        findViewById(R.id.tvPleaseWait).setVisibility(isLoading ? 0 : 8);
        View findViewById = findViewById(R.id.pbPleaseWait);
        if (!isLoading) {
            i = 8;
        }
        findViewById.setVisibility(i);
    }

    public void finish() {
        if (!this.f13534) {
            RxBus.m15709().m15711(new TraktUserCancelledAuthEvent());
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        final String lastUrlParam = null;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_web_view);
        getSupportActionBar().m427(true);
        Bundle b = getIntent().getExtras();
        if (b.getString("verificationUrl", (String) null) == null || b.getString("verificationUrl", (String) null).isEmpty() || b.getString("userCode", (String) null) == null || b.getString("userCode", (String) null).isEmpty() || !NetworkUtils.m17799()) {
            if (!NetworkUtils.m17799()) {
                Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
            } else {
                Toast.makeText(this, I18N.m15706(R.string.error), 1).show();
            }
            setResult(0);
            finish();
            return;
        }
        String verificationUrl = b.getString("verificationUrl");
        String userCode = b.getString("userCode");
        String[] splittedUrl = (verificationUrl == null || !verificationUrl.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) ? null : verificationUrl.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
        if (splittedUrl != null) {
            lastUrlParam = splittedUrl[splittedUrl.length - 1];
        }
        setTitle("Trakt Auth");
        if (DeviceUtils.m6389(new boolean[0])) {
            TextView tvWait = (TextView) findViewById(R.id.tvPleaseWait);
            tvWait.setTextSize(2, 24.0f);
            tvWait.setText(String.format("1) Visit \"%s\" in a browser of any of your devices\n2) Login to Trakt.tv\n3) Input the following code: %s\n\nThis window will be closed automatically after you have granted the Trakt.tv API access to Typhoon", new Object[]{verificationUrl, userCode}));
            m17219(true);
        } else {
            this.f13533 = TSnackbar.m3909(findViewById(R.id.webViewActivityRoot), "Enter the code: " + userCode, -2);
            try {
                WebViewDatabase.getInstance(this).clearFormData();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            this.f13536 = (WebView) findViewById(R.id.webView);
            this.f13536.getSettings().setJavaScriptEnabled(true);
            this.f13536.getSettings().setAllowFileAccess(false);
            this.f13536.getSettings().setSaveFormData(false);
            this.f13536.getSettings().setSavePassword(false);
            this.f13536.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
            this.f13536.getSettings().setCacheMode(2);
            this.f13536.getSettings().setAppCacheEnabled(false);
            this.f13536.getSettings().setUserAgentString(TyphoonApp.f5835);
            try {
                this.f13536.clearCache(true);
                this.f13536.clearFormData();
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
            CookieManager.getInstance().setAcceptCookie(true);
            this.f13536.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if (view.getTitle().equals("Trakt.tv") && url.endsWith("/authorize")) {
                        Toast.makeText(TraktAuthWebViewActivity.this, I18N.m15706(R.string.please_wait), 1).show();
                        if (TraktAuthWebViewActivity.this.f13533 != null) {
                            TraktAuthWebViewActivity.this.f13533.m3911();
                        }
                        TraktAuthWebViewActivity.this.m17219(true);
                    } else if (view.getTitle().toLowerCase().contains("activate your device") || (lastUrlParam != null && url.contains(lastUrlParam))) {
                        if (TraktAuthWebViewActivity.this.f13533 != null) {
                            TraktAuthWebViewActivity.this.f13533.m3917();
                        }
                        TraktAuthWebViewActivity.this.m17219(false);
                    } else {
                        if (TraktAuthWebViewActivity.this.f13533 != null) {
                            TraktAuthWebViewActivity.this.f13533.m3911();
                        }
                        TraktAuthWebViewActivity.this.m17219(false);
                    }
                }

                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    TraktAuthWebViewActivity.this.m17219(true);
                    return false;
                }
            });
            this.f13536.loadUrl(verificationUrl);
        }
        this.f13535 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object o) {
                if ((o instanceof TraktGetTokenSuccessEvent) || (o instanceof TraktGetTokenFailedEvent)) {
                    if (o instanceof TraktGetTokenSuccessEvent) {
                        boolean unused = TraktAuthWebViewActivity.this.f13534 = true;
                    }
                    TraktAuthWebViewActivity.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13536 != null) {
            if (this.f13536.getParent() != null && (this.f13536.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.f13536.getParent()).removeView(this.f13536);
            }
            this.f13536.removeAllViews();
            this.f13536.destroy();
        }
        if (this.f13535 != null && !this.f13535.isUnsubscribed()) {
            this.f13535.unsubscribe();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                setResult(0);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.f13536 != null) {
            this.f13536.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f13536 != null) {
            this.f13536.onResume();
        }
    }
}
