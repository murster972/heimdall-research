package com.typhoon.tv.ui.activity.base;

import android.os.Bundle;
import com.evernote.android.state.Bundler;
import com.evernote.android.state.InjectionHelper;
import com.evernote.android.state.Injector;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import java.util.HashMap;

public class BaseAdActivity$$StateSaver<T extends BaseAdActivity> extends Injector.Object<T> {
    private static final HashMap<String, Bundler<?>> BUNDLERS = new HashMap<>();
    private static final InjectionHelper HELPER = new InjectionHelper("com.typhoon.tv.ui.activity.base.BaseAdActivity$$StateSaver", BUNDLERS);

    public void restore(T t, Bundle bundle) {
        t.mIsSecondAdShown = HELPER.getBoolean(bundle, "mIsSecondAdShown");
        t.mLoadOguryTrialCount = HELPER.getInt(bundle, "mLoadOguryTrialCount");
    }

    public void save(T t, Bundle bundle) {
        HELPER.putBoolean(bundle, "mIsSecondAdShown", t.mIsSecondAdShown);
        HELPER.putInt(bundle, "mLoadOguryTrialCount", t.mLoadOguryTrialCount);
    }
}
