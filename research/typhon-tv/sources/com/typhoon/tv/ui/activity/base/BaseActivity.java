package com.typhoon.tv.ui.activity.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.Toast;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.AdinCubeInterstitialEventListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.UpdateService;
import com.typhoon.tv.YesPlayerDownloader;
import com.typhoon.tv.api.ImdbApi;
import com.typhoon.tv.api.TraktUserApi;
import com.typhoon.tv.backup.FavBackupRestoreHelper;
import com.typhoon.tv.backup.PrefsBackupRestoreHelper;
import com.typhoon.tv.backup.SubsMapBackupRestoreHelper;
import com.typhoon.tv.backup.WatchedEpsBackupRestoreHelper;
import com.typhoon.tv.event.OnInterstitialAdShownEvent;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.player.ExoPlayerHelper;
import com.typhoon.tv.helper.player.MXPlayerHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.jobs.CheckNewEpisodeJob;
import com.typhoon.tv.jobs.CheckNewMovieReleaseJob;
import com.typhoon.tv.model.CheckNewMovieReleaseResult;
import com.typhoon.tv.model.UpdateInfo;
import com.typhoon.tv.presenter.IDownloadYPPresenter;
import com.typhoon.tv.presenter.IUpdatePresenter;
import com.typhoon.tv.presenter.impl.DownloadYPPresenterImpl;
import com.typhoon.tv.presenter.impl.UpdatePresenterImpl;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.GmsUtils;
import com.typhoon.tv.utils.LocaleUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.IDownloadYPView;
import com.typhoon.tv.view.IUpdateView;
import com.typhoon.tv.webserver.CastSubtitlesWebServer;
import com.typhoon.tv.webserver.WebServerManager;
import com.typhoon.tv.webserver.WebServerService;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import fi.iki.elonen.NanoHTTPD;
import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.pubnative.AdvertisingIdClient;
import okhttp3.ResponseBody;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseActivity extends AppCompatActivity implements IDownloadYPView, IUpdateView {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ProgressDialog f13594;

    /* renamed from: ʼ  reason: contains not printable characters */
    private CompositeSubscription f13595;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f13596;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Toast f13597;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f13598;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public UpdateInfo f13599;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public CastSession f13600;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public SessionManager f13601;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public IDownloadYPPresenter f13602;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Callback<SyncResponse> f13603 = new Callback<SyncResponse>() {
        public void onFailure(Call<SyncResponse> call, Throwable th) {
            Toast.makeText(BaseActivity.this, "Failed to send to Trakt...", 1).show();
        }

        public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
            Toast.makeText(BaseActivity.this, "Sent to Trakt successfully!", 1).show();
        }
    };

    /* renamed from: 麤  reason: contains not printable characters */
    private ProgressDialog f13604;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public IUpdatePresenter f13605;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SessionManagerListener f13606 = new SessionManagerListenerImpl();

    private class SessionManagerListenerImpl implements SessionManagerListener {
        private SessionManagerListenerImpl() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m17392(Session session) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m17393(Session session, int i) {
            BaseActivity.this.invalidateOptionsMenu();
            if (BaseActivity.this.f13600 != null) {
                BaseActivity.this.f13600.m8022();
            }
            BaseActivity.this.stopService(new Intent(BaseActivity.this, WebServerService.class));
            try {
                SubsMapBackupRestoreHelper.m15826();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m17394(Session session, String str) {
            CastSession unused = BaseActivity.this.f13600 = BaseActivity.this.f13601.m8075();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m17395(Session session, int i) {
            BaseActivity.this.invalidateOptionsMenu();
            if (BaseActivity.this.f13600 != null) {
                BaseActivity.this.f13600.m8022();
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m17396(Session session, int i) {
            BaseActivity.this.invalidateOptionsMenu();
            if (BaseActivity.this.f13600 != null) {
                BaseActivity.this.f13600.m8022();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m17397(Session session) {
            CastSession unused = BaseActivity.this.f13600 = BaseActivity.this.f13601.m8075();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m17398(Session session, int i) {
            BaseActivity.this.invalidateOptionsMenu();
            if (BaseActivity.this.f13600 != null) {
                BaseActivity.this.f13600.m8022();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m17399(Session session, String str) {
            BaseActivity.this.invalidateOptionsMenu();
            CastSession unused = BaseActivity.this.f13600 = BaseActivity.this.f13601.m8075();
            if (BaseActivity.this.f13600 != null) {
                BaseActivity.this.f13600.m8022();
            }
            BaseActivity.this.m17281();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m17400(Session session, boolean z) {
            BaseActivity.this.invalidateOptionsMenu();
            CastSession unused = BaseActivity.this.f13600 = BaseActivity.this.f13601.m8075();
            if (BaseActivity.this.f13600 != null) {
                BaseActivity.this.f13600.m8022();
            }
            BaseActivity.this.m17281();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m17265() {
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                if (getApplicationInfo().targetSdkVersion < 26 || getPackageManager().canRequestPackageInstalls()) {
                    m17267();
                } else {
                    startActivityForResult(new Intent("android.settings.MANAGE_UNKNOWN_APP_SOURCES", Uri.parse("package:" + getPackageName())), 7465);
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
                m17267();
            }
        } else {
            m17267();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m17267() {
        String str = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Typhoon/Updates/" + (this.f13599 != null ? this.f13599.getFileName() : "app-release") + ".apk";
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 24) {
            intent.setData(FileProvider.getUriForFile(TVApplication.m6288(), TVApplication.m6288().getPackageName() + ".fileProvider", new File(str)));
            intent.setAction("android.intent.action.INSTALL_PACKAGE");
            intent.setFlags(268435457);
        } else {
            intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
            intent.setAction("android.intent.action.VIEW");
            intent.setFlags(268435456);
        }
        startActivity(intent);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m17268() {
        Uri fromFile;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(268435456);
        if (Build.VERSION.SDK_INT >= 24) {
            fromFile = FileProvider.getUriForFile(TVApplication.m6288(), TVApplication.m6288().getPackageName() + ".fileProvider", new File(YesPlayerDownloader.f12501));
            intent.addFlags(1);
        } else {
            fromFile = Uri.fromFile(new File(YesPlayerDownloader.f12501));
        }
        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
        startActivity(intent);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m17269() {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 24) {
            intent.setData(FileProvider.getUriForFile(TVApplication.m6288(), TVApplication.m6288().getPackageName() + ".fileProvider", new File(YesPlayerDownloader.f12501)));
            intent.setAction("android.intent.action.INSTALL_PACKAGE");
            intent.setFlags(268435457);
        } else {
            intent.setDataAndType(Uri.fromFile(new File(YesPlayerDownloader.f12501)), "application/vnd.android.package-archive");
            intent.setAction("android.intent.action.VIEW");
            intent.setFlags(268435456);
        }
        startActivity(intent);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private void m17270() {
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                if (getApplicationInfo().targetSdkVersion < 26 || getPackageManager().canRequestPackageInstalls()) {
                    m17269();
                } else {
                    startActivityForResult(new Intent("android.settings.MANAGE_UNKNOWN_APP_SOURCES", Uri.parse("package:" + getPackageName())), 8454);
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
                m17269();
            }
        } else {
            m17269();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m17272() {
        Uri fromFile;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(268435456);
        String str = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Typhoon/Updates/" + (this.f13599 != null ? this.f13599.getFileName() : "app-release") + ".apk";
        if (Build.VERSION.SDK_INT >= 24) {
            fromFile = FileProvider.getUriForFile(TVApplication.m6288(), TVApplication.m6288().getPackageName() + ".fileProvider", new File(str));
            intent.addFlags(1);
        } else {
            fromFile = Uri.fromFile(new File(str));
        }
        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
        startActivity(intent);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17274() {
        try {
            if (!TyphoonApp.f5875 && TyphoonApp.f5874 == 0 && !Fabric.m19036()) {
                Fabric.m19044((Context) this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(false).build()).build());
                TyphoonApp.f5875 = true;
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17276() {
        Set<JobRequest> allJobRequestsForTag = JobManager.instance().getAllJobRequestsForTag("CheckNewMovieReleaseJob");
        if (TVApplication.m6285().getBoolean("pref_enable_new_movie_release_notification", true) && (allJobRequestsForTag == null || allJobRequestsForTag.isEmpty())) {
            CheckNewMovieReleaseJob.m16118();
        } else if (!TVApplication.m6285().getBoolean("pref_enable_new_movie_release_notification", true)) {
            for (JobRequest jobId : allJobRequestsForTag) {
                try {
                    JobManager.instance().cancel(jobId.getJobId());
                } catch (Throwable th) {
                    Logger.m6281(th, true);
                }
            }
            try {
                CheckNewMovieReleaseJob.m16117();
            } catch (Throwable th2) {
                Logger.m6281(th2, true);
            }
            try {
                List<CheckNewMovieReleaseResult> r0 = TVApplication.m6287().m6308();
                if (r0 != null) {
                    for (CheckNewMovieReleaseResult next : r0) {
                        if (next != null) {
                            next.delete();
                        }
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17278() {
        Set<JobRequest> allJobRequestsForTag = JobManager.instance().getAllJobRequestsForTag("CheckNewEpisodeJob");
        if (TVApplication.m6285().getBoolean("pref_enable_new_episode_notification", true) && (allJobRequestsForTag == null || allJobRequestsForTag.isEmpty())) {
            CheckNewEpisodeJob.m16116();
        } else if (!TVApplication.m6285().getBoolean("pref_enable_new_episode_notification", true)) {
            for (JobRequest jobId : allJobRequestsForTag) {
                try {
                    JobManager.instance().cancel(jobId.getJobId());
                } catch (Throwable th) {
                    Logger.m6281(th, true);
                }
            }
            try {
                CheckNewEpisodeJob.m16115();
            } catch (Throwable th2) {
                Logger.m6281(th2, true);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17281() {
        if (WebServerManager.m17893().m17896() == null && m17305(new boolean[0])) {
            WebServerManager.m17893().m17899((NanoHTTPD) new CastSubtitlesWebServer(59104));
            try {
                if (WebServerManager.m17893().m17894() == null && m17305(new boolean[0]) && !WebServerManager.m17893().m17898()) {
                    this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<Map<String, String>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void call(Subscriber<? super Map<String, String>> subscriber) {
                            subscriber.onNext(SubsMapBackupRestoreHelper.m15827());
                            subscriber.onCompleted();
                        }
                    }).m7376(new Func1<Throwable, Map<String, String>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public Map<String, String> call(Throwable th) {
                            return null;
                        }
                    }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Map<String, String>>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void call(Map<String, String> map) {
                            MediaInfo r1;
                            if (map != null && !map.isEmpty() && BaseActivity.this.m17305(new boolean[0]) && !WebServerManager.m17893().m17898()) {
                                try {
                                    if (BaseActivity.this.m17287() != null && BaseActivity.this.m17287().m8022() != null && (r1 = BaseActivity.this.m17287().m8022().m4137()) != null && r1.m7838() != null) {
                                        boolean z = false;
                                        boolean z2 = false;
                                        for (MediaTrack next : r1.m7838()) {
                                            try {
                                                if (next.m7929() != 3 && next.m7929() == 1 && !z2) {
                                                    z = true;
                                                    z2 = true;
                                                }
                                            } catch (Exception e) {
                                                Logger.m6281((Throwable) e, new boolean[0]);
                                            }
                                        }
                                        if (z) {
                                            WebServerManager.m17893().m17900(map);
                                            Bundle bundle = new Bundle();
                                            Intent intent = new Intent(BaseActivity.this, WebServerService.class);
                                            intent.putExtras(bundle);
                                            BaseActivity.this.startService(intent);
                                        }
                                    }
                                } catch (Exception e2) {
                                    Logger.m6281((Throwable) e2, new boolean[0]);
                                }
                            }
                        }
                    }));
                }
            } catch (Exception e) {
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        try {
            if (Build.VERSION.SDK_INT < 16 && GmsUtils.m6392((Context) this)) {
                return CastContext.m7977((Context) this).m7985(keyEvent) || super.dispatchKeyEvent(keyEvent);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 7465) {
            m17267();
        } else if (i == 8454) {
            m17269();
        }
    }

    public void onBackPressed() {
        if (!isTaskRoot()) {
            try {
                super.onBackPressed();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        } else if (this.f13598 + 2000 > System.currentTimeMillis()) {
            if (this.f13597 != null) {
                this.f13597.cancel();
            }
            super.onBackPressed();
        } else {
            this.f13597 = Toast.makeText(getBaseContext(), I18N.m15706(R.string.press_back_again_to_exit), 0);
            this.f13597.show();
            this.f13598 = System.currentTimeMillis();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (TVApplication.m6285().getBoolean("pref_force_tv_mode", false)) {
            try {
                Resources resources = getResources();
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                Configuration configuration = resources.getConfiguration();
                configuration.uiMode = 4;
                resources.updateConfiguration(configuration, displayMetrics);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        super.onCreate(bundle);
        this.f13595 = new CompositeSubscription();
        if (GmsUtils.m6392((Context) this)) {
            try {
                this.f13601 = CastContext.m7977((Context) this).m7981();
            } catch (Throwable th) {
                TyphoonApp.f5853 = false;
                Logger.m6281(th, new boolean[0]);
            }
        }
        AdinCube.m2301("734595550db947ffab46");
        if (!TyphoonApp.f5865 && !TyphoonApp.f5863) {
            AdinCube.Interstitial.m2307((Activity) this);
            AdinCube.Interstitial.m2308((AdinCubeInterstitialEventListener) new AdinCubeInterstitialEventListener() {
                /* renamed from: 靐  reason: contains not printable characters */
                public void m17367() {
                    RxBus.m15709().m15711(new OnInterstitialAdShownEvent());
                }

                /* renamed from: 麤  reason: contains not printable characters */
                public void m17368() {
                }

                /* renamed from: 齉  reason: contains not printable characters */
                public void m17369() {
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void m17370() {
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void m17371(String str) {
                }
            });
            TyphoonApp.f5863 = true;
        }
        m17274();
        if (!TyphoonApp.f5857) {
            this.f13595.m25086(UpdateService.m15713().m7376(new Func1<Throwable, Boolean>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Boolean call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return false;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Boolean>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Boolean bool) {
                    if (bool.booleanValue()) {
                        TyphoonApp.f5857 = true;
                    }
                }
            }));
        }
        this.f13605 = new UpdatePresenterImpl(this);
        if (!TVApplication.m6285().getBoolean("pref_auto_check_update", false)) {
            TVApplication.m6285().edit().putBoolean("pref_auto_check_update", true).apply();
        }
        if (NetworkUtils.m17799() && (DeviceUtils.m6389(new boolean[0]) || !TyphoonApp.f5855 || TyphoonApp.f5857)) {
            m17302((Action1<Boolean>) new Action1<Boolean>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Boolean bool) {
                    BaseActivity.this.f13605.m16162();
                }
            });
        }
        String trim = TVApplication.m6285().getString("pref_app_lang", "").trim();
        if (!trim.isEmpty()) {
            LocaleUtils.m17793(TVApplication.m6288(), LocaleUtils.m17792(trim));
        } else {
            LocaleUtils.m17793(TVApplication.m6288(), Resources.getSystem().getConfiguration().locale);
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
        if (Build.VERSION.SDK_INT > 25) {
            arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        if (Build.VERSION.SDK_INT >= 26) {
            arrayList.add("android.permission.REQUEST_INSTALL_PACKAGES");
        }
        arrayList.add("android.permission.WAKE_LOCK");
        this.f13595.m25086(new RxPermissions(this).m15681((String[]) arrayList.toArray(new String[arrayList.size()])).m7372());
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351("", (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null) {
                    try {
                        if (!str.isEmpty()) {
                            String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                            if (replace.contains("Hong Kong") || replace.contains("HongKong") || replace.contains("\"countryCode\":\"HK\"") || replace.contains("hong kong") || replace.contains("hongkong") || replace.contains("\"countrycode\":\"hk\"")) {
                                Utils.m6401((Activity) BaseActivity.this);
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351("", (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null) {
                    try {
                        if (!str.isEmpty()) {
                            String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                            if (TyphoonApp.f5874 != 0 && replace.contains("countrycode")) {
                                String[] strArr = TyphoonApp.f5856;
                                int length = strArr.length;
                                int i = 0;
                                while (true) {
                                    if (i >= length) {
                                        break;
                                    }
                                    if (replace.contains("\"countrycode\":\"" + strArr[i] + "\"")) {
                                        TyphoonApp.f5874 = 1;
                                        break;
                                    }
                                    i++;
                                }
                                if (TyphoonApp.f5874 == -1) {
                                    TyphoonApp.f5874 = 0;
                                }
                                if (TyphoonApp.f5874 != 0 && replace.contains("\"countrycode\":\"us\"")) {
                                    TyphoonApp.f5874 = 0;
                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5889, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null) {
                    try {
                        if (!str.isEmpty() && str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "").equalsIgnoreCase("true")) {
                            Utils.m6401((Activity) BaseActivity.this);
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }));
        if (ExoPlayerHelper.m16055()) {
            this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super String> subscriber) {
                    subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5834, (Map<String, String>[]) new Map[0]));
                    subscriber.onCompleted();
                }
            }).m7376(new Func1<Throwable, String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public String call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return null;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(String str) {
                    if (str != null) {
                        try {
                            if (!str.isEmpty() && ExoPlayerHelper.m16055()) {
                                String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                                TyphoonApp.f5858 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5858);
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                }
            }));
        }
        if (ExoPlayerHelper.m16055() && BasePlayerHelper.m16032().m16041().equalsIgnoreCase("Yes")) {
            this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super String> subscriber) {
                    subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5833, (Map<String, String>[]) new Map[0]));
                    subscriber.onCompleted();
                }
            }).m7376(new Func1<Throwable, String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public String call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return null;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(String str) {
                    if (str != null) {
                        try {
                            if (!str.isEmpty() && ExoPlayerHelper.m16055() && BasePlayerHelper.m16032().m16041().equalsIgnoreCase("Yes")) {
                                String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                                PackageInfo packageInfo = TVApplication.m6288().getPackageManager().getPackageInfo("com.Ty.yesplayer", 0);
                                if (packageInfo != null && packageInfo.versionCode < Integer.parseInt(replace) && !BaseActivity.this.isFinishing()) {
                                    final AlertDialog r0 = new AlertDialog.Builder(BaseActivity.this).m525();
                                    r0.setTitle("New version found!");
                                    r0.m519((CharSequence) "New version found for YesPlayer! Update it to stream videos much faster now!");
                                    r0.m517(-1, I18N.m15706(R.string.install), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            try {
                                                if (DeviceUtils.m6389(new boolean[0]) || !GmsUtils.m6390()) {
                                                    BaseActivity.this.m17303(true);
                                                } else {
                                                    Utils.m6402(BaseActivity.this, "com.Ty.yesplayer");
                                                }
                                            } catch (Exception e) {
                                                Logger.m6281((Throwable) e, new boolean[0]);
                                            }
                                        }
                                    });
                                    r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    r0.setOnShowListener(new DialogInterface.OnShowListener() {
                                        public void onShow(DialogInterface dialogInterface) {
                                            Button r1 = r0.m515(-1);
                                            r1.setFocusable(true);
                                            r1.setFocusableInTouchMode(true);
                                            r1.requestFocus();
                                            if (TyphoonApp.f5858) {
                                                r0.m515(-2).setEnabled(false);
                                            }
                                        }
                                    });
                                    if (TyphoonApp.f5858) {
                                        r0.setCancelable(false);
                                        r0.setCanceledOnTouchOutside(false);
                                    }
                                    if (!BaseActivity.this.isFinishing()) {
                                        r0.show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                }
            }));
        }
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5899, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5849 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5849);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5903, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5847 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5847);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5917, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5848 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5848);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5915, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5866 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5866);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5919, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5867 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5867);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5921, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5868 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5868);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5928, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty() && str.contains("|")) {
                    TyphoonApp.f5842 = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5909, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty() && str.contains("|")) {
                    TyphoonApp.f5843 = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5930, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    try {
                        TyphoonApp.f5844 = Integer.parseInt(str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, ""));
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }));
        if (!TyphoonApp.f5862) {
            this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super String> subscriber) {
                    subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5905, (Map<String, String>[]) new Map[0]));
                    subscriber.onCompleted();
                }
            }).m7376(new Func1<Throwable, String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public String call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return null;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(String str) {
                    if (str != null && !str.isEmpty()) {
                        String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                        if (replace.equals("true") || replace.equals("false")) {
                            TyphoonApp.f5861 = replace.equals("true");
                            TyphoonApp.f5862 = true;
                        }
                    }
                }
            }));
        }
        if (!TyphoonApp.f5870) {
            this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super String> subscriber) {
                    String r2 = HttpHelper.m6343().m6351(TyphoonApp.f5893, (Map<String, String>[]) new Map[0]);
                    String str = "";
                    try {
                        str = new String(Base64.decode(r2, 0), "UTF-8");
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                        try {
                            str = new String(Base64.decode(r2, 0));
                        } catch (Exception e2) {
                            Logger.m6281((Throwable) e2, new boolean[0]);
                        }
                    }
                    subscriber.onNext(str);
                    subscriber.onCompleted();
                }
            }).m7376(new Func1<Throwable, String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public String call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return "";
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<String>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void onNext(String str) {
                    if (str != null && !str.trim().isEmpty()) {
                        TyphoonApp.f5836 = str.trim();
                        TyphoonApp.f5870 = true;
                    }
                }
            }));
        }
        if (!TyphoonApp.f5871) {
            this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super String> subscriber) {
                    String r2 = HttpHelper.m6343().m6351(TyphoonApp.f5891, (Map<String, String>[]) new Map[0]);
                    String str = "";
                    try {
                        str = new String(Base64.decode(r2, 0), "UTF-8");
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                        try {
                            str = new String(Base64.decode(r2, 0));
                        } catch (Exception e2) {
                            Logger.m6281((Throwable) e2, new boolean[0]);
                        }
                    }
                    subscriber.onNext(str);
                    subscriber.onCompleted();
                }
            }).m7376(new Func1<Throwable, String>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public String call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return "";
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<String>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void onNext(String str) {
                    if (str != null && !str.trim().isEmpty() && str.contains("|")) {
                        String[] split = str.split("\\|");
                        TyphoonApp.f5840 = split[0];
                        TyphoonApp.f5841 = split[1];
                        TyphoonApp.f5871 = true;
                    }
                }
            }));
        }
        if (!TyphoonApp.f5865 && !TyphoonApp.f5869) {
            this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<HashMap<String, String>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super HashMap<String, String>> subscriber) {
                    JsonElement parse = new JsonParser().parse(HttpHelper.m6343().m6351(TyphoonApp.f5897, (Map<String, String>[]) new Map[0]));
                    String asString = parse.getAsJsonObject().get("interstitialId").getAsString();
                    String asString2 = parse.getAsJsonObject().get("nativeId").getAsString();
                    String asString3 = parse.getAsJsonObject().get("bannerId").getAsString();
                    String str = "";
                    try {
                        str = new String(Base64.decode(asString, 0), "UTF-8").trim();
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                        try {
                            str = new String(Base64.decode(asString, 0)).trim();
                        } catch (Exception e2) {
                            Logger.m6281((Throwable) e2, new boolean[0]);
                        }
                    }
                    String str2 = "";
                    try {
                        str2 = new String(Base64.decode(asString2, 0), "UTF-8").trim();
                    } catch (Exception e3) {
                        Logger.m6281((Throwable) e3, new boolean[0]);
                        try {
                            str2 = new String(Base64.decode(asString2, 0)).trim();
                        } catch (Exception e4) {
                            Logger.m6281((Throwable) e4, new boolean[0]);
                        }
                    }
                    String str3 = "";
                    try {
                        str3 = new String(Base64.decode(asString3, 0), "UTF-8").trim();
                    } catch (Exception e5) {
                        Logger.m6281((Throwable) e5, new boolean[0]);
                        try {
                            str3 = new String(Base64.decode(asString3, 0)).trim();
                        } catch (Exception e6) {
                            Logger.m6281((Throwable) e6, new boolean[0]);
                        }
                    }
                    HashMap hashMap = new HashMap();
                    hashMap.put("interstitialId", str);
                    hashMap.put("nativeId", str2);
                    hashMap.put("bannerId", str3);
                    subscriber.onNext(hashMap);
                    subscriber.onCompleted();
                }
            }).m7376(new Func1<Throwable, HashMap<String, String>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public HashMap<String, String> call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return null;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<HashMap<String, String>>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void onNext(HashMap<String, String> hashMap) {
                    if (hashMap != null && hashMap.containsKey("interstitialId") && hashMap.containsKey("nativeId") && hashMap.containsKey("bannerId")) {
                        String str = hashMap.get("interstitialId");
                        String str2 = hashMap.get("nativeId");
                        String str3 = hashMap.get("bannerId");
                        if (!str.trim().isEmpty()) {
                            TyphoonApp.f5837 = str;
                        }
                        if (!str2.trim().isEmpty()) {
                            TyphoonApp.f5838 = str2;
                        }
                        if (!str3.trim().isEmpty()) {
                            TyphoonApp.f5839 = str3;
                        }
                        TyphoonApp.f5869 = true;
                    }
                }
            }));
        }
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5830, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                boolean z = false;
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    boolean z2 = TVApplication.m6285().getBoolean("is_nad_mech_enabled", false);
                    if (replace.equals("true") || (!replace.equals("false") && z2)) {
                        z = true;
                    }
                    if (z2 != z) {
                        TVApplication.m6285().edit().putBoolean("is_nad_mech_enabled", z).apply();
                    }
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5831, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5845 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5845);
                }
            }
        }));
        this.f13595.m25086(Observable.m7359(new Observable.OnSubscribe<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(HttpHelper.m6343().m6351(TyphoonApp.f5832, (Map<String, String>[]) new Map[0]));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return null;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(String str) {
                if (str != null && !str.isEmpty()) {
                    String replace = str.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    TyphoonApp.f5846 = replace.equals("true") || (!replace.equals("false") && TyphoonApp.f5846);
                }
            }
        }));
        if (!TyphoonApp.f5872) {
            try {
                AdvertisingIdClient.getAdvertisingId(this, new AdvertisingIdClient.Listener() {
                    public void onAdvertisingIdClientFail(Exception exc) {
                        Logger.m6281((Throwable) exc, new boolean[0]);
                    }

                    public void onAdvertisingIdClientFinish(AdvertisingIdClient.AdInfo adInfo) {
                        String id;
                        if (adInfo != null && (id = adInfo.getId()) != null && !id.isEmpty()) {
                            TVApplication.m6285().edit().putString("google_aid", id).apply();
                            TyphoonApp.f5872 = true;
                        }
                    }
                });
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
        }
        ExoPlayerHelper exoPlayerHelper = new ExoPlayerHelper();
        if (!exoPlayerHelper.m16060() && TVApplication.m6285().getString("pref_choose_default_player", "").equalsIgnoreCase(exoPlayerHelper.m16058())) {
            TVApplication.m6285().edit().putString("pref_choose_default_player", new MXPlayerHelper().m16074()).apply();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13595 != null && !this.f13595.isUnsubscribed()) {
            this.f13595.unsubscribe();
        }
        this.f13595 = null;
        if (this.f13605 != null) {
            this.f13605.m16161();
        }
        this.f13605 = null;
        if (this.f13602 != null) {
            this.f13602.m16127();
        }
        this.f13602 = null;
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return true;
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f13596 = false;
        if (this.f13601 == null && GmsUtils.m6392((Context) this)) {
            try {
                this.f13601 = CastContext.m7977((Context) this).m7981();
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
        if (this.f13601 != null) {
            try {
                this.f13601.m8076(this.f13606);
                this.f13600 = null;
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"RestrictedApi"})
    public boolean onPrepareOptionsPanel(View view, Menu menu) {
        Method declaredMethod;
        if (menu != null) {
            try {
                if (menu.getClass().getSimpleName().equalsIgnoreCase("MenuBuilder") && (declaredMethod = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", new Class[]{Boolean.TYPE})) != null) {
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(menu, new Object[]{true});
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f13596 = true;
        m17281();
        if (this.f13601 == null && GmsUtils.m6392((Context) this)) {
            try {
                this.f13601 = CastContext.m7977((Context) this).m7981();
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
        if (this.f13601 != null) {
            try {
                this.f13600 = this.f13601.m8075();
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
            try {
                this.f13601.m8081((SessionManagerListener<Session>) this.f13606);
            } catch (Throwable th3) {
                Logger.m6281(th3, new boolean[0]);
            }
        }
        m17278();
        m17276();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m17282() {
        return this.f13596;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m17283() {
        if (this.f13604 != null && this.f13604.isShowing() && !isFinishing()) {
            try {
                this.f13604.dismiss();
            } catch (Exception e) {
            }
            this.f13604 = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m17284() {
        if (GmsUtils.m6392((Context) this) && !DeviceUtils.m6388()) {
            try {
                ((ViewStub) findViewById(R.id.castMiniControllerContainer)).inflate();
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m17285() {
        if (this.f13594 != null && this.f13594.isShowing() && !isFinishing()) {
            try {
                this.f13594.dismiss();
            } catch (Exception e) {
            }
            this.f13594 = null;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m17286() {
        if (Utils.m6423()) {
            try {
                m17270();
            } catch (Throwable th) {
                Logger.m6281(th, true);
                Toast.makeText(this, "Failed to install YesPlayer", 1).show();
            }
        } else {
            Toast.makeText(this, I18N.m15706(DeviceUtils.m6388() ? R.string.files_error_unknown_sources_amazon : R.string.files_error_unknown_sources), 1).show();
            try {
                startActivity(new Intent("android.settings.SECURITY_SETTINGS"));
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public CastSession m17287() {
        CastSession castSession = null;
        this.f13601 = CastContext.m7977((Context) this) != null ? CastContext.m7977((Context) this).m7981() : null;
        if (this.f13601 != null) {
            castSession = this.f13601.m8075();
        }
        this.f13600 = castSession;
        return this.f13600;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m17288() {
        return (this.f13600 == null || this.f13600.m8022() == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m17289() {
        if (!ExoPlayerHelper.m16055()) {
            String r0 = BasePlayerHelper.m16032().m16041();
            if (r0.equalsIgnoreCase("Yes") || r0.equalsIgnoreCase("Exo")) {
                TVApplication.m6285().edit().putString("pref_choose_default_player", "MX").apply();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17290(com.typhoon.tv.model.media.MediaInfo mediaInfo, boolean z, boolean z2, Callback<SyncResponse> callback) {
        if (TraktCredentialsHelper.m16110().isValid() && TVApplication.m6285().getBoolean("pref_auto_add_watched_episode_trakt", true)) {
            if (!NetworkUtils.m17799()) {
                Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
                return;
            }
            if (z2) {
                Toast.makeText(this, "Sending to Trakt...", 0).show();
            }
            if (callback == null) {
                callback = this.f13603;
            }
            try {
                TraktUserApi.m15774().m15784(mediaInfo, z, callback);
            } catch (Exception e) {
                if (z2) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    Toast.makeText(this, "Failed to send to Trakt...", 0).show();
                    return;
                }
                throw e;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17291(Integer num) {
        if (!isFinishing() && this.f13594 != null && this.f13594.isShowing()) {
            this.f13594.setProgress(num.intValue());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17292(String str) {
        if (!isFinishing()) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(I18N.m15707(R.string.update_downloading, str));
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(1);
            progressDialog.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!BaseActivity.this.isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                    Toast.makeText(BaseActivity.this, I18N.m15706(R.string.update_failed), 0).show();
                    BaseActivity.this.f13605.m16160();
                }
            });
            progressDialog.show();
            if (TyphoonApp.f5857) {
                progressDialog.getButton(-2).setEnabled(false);
            }
            this.f13604 = progressDialog;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17293(Throwable th) {
        String str = "Failed to download YesPlayer.";
        if (!(th == null || th.getMessage() == null)) {
            str = str + "\n\n" + th.getMessage();
        }
        Toast.makeText(this, str, 1).show();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17294(boolean z) {
        if (!isFinishing()) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(z ? "Updating YesPlayer..." : "Downloading YesPlayer...");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(1);
            progressDialog.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!BaseActivity.this.isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                    Toast.makeText(BaseActivity.this, I18N.m15706(R.string.update_failed), 0).show();
                    BaseActivity.this.f13602.m16128();
                }
            });
            progressDialog.show();
            if (TyphoonApp.f5858) {
                progressDialog.getButton(-2).setEnabled(false);
            }
            this.f13594 = progressDialog;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public MenuItem m17295(Menu menu) {
        if (GmsUtils.m6392((Context) this)) {
            try {
                return CastButtonFactory.m7974(TVApplication.m6288(), menu, R.id.media_route_menu_item);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                TyphoonApp.f5853 = false;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17296(MenuItem menuItem, SearchView searchView, boolean z) {
        if (!DeviceUtils.m6389(new boolean[0])) {
            SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this, R.layout.custom_simple_list_item_1, (Cursor) null, new String[]{"suggest_text_1"}, new int[]{16908308}, 0);
            final String[] strArr = {"_id", "suggest_text_1", "suggest_intent_data"};
            final ArrayList arrayList = new ArrayList();
            searchView.setSuggestionsAdapter(simpleCursorAdapter);
            final SearchView searchView2 = searchView;
            final boolean z2 = z;
            final MenuItem menuItem2 = menuItem;
            searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                public boolean onSuggestionClick(int i) {
                    try {
                        ImdbApi.m15726().m15728();
                        searchView2.clearFocus();
                        searchView2.setQuery((CharSequence) arrayList.get(i), true);
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                    if (z2 && menuItem2 != null) {
                        menuItem2.collapseActionView();
                    }
                    return true;
                }

                public boolean onSuggestionSelect(int i) {
                    return false;
                }
            });
            final boolean z3 = z;
            final MenuItem menuItem3 = menuItem;
            final ArrayList arrayList2 = arrayList;
            final SimpleCursorAdapter simpleCursorAdapter2 = simpleCursorAdapter;
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                public boolean onQueryTextChange(String str) {
                    okhttp3.Call r0;
                    ImdbApi.m15726().m15728();
                    arrayList2.clear();
                    simpleCursorAdapter2.swapCursor(new MatrixCursor(strArr));
                    if (str != null && !str.isEmpty() && NetworkUtils.m17799() && (r0 = ImdbApi.m15726().m15727(str)) != null) {
                        r0.m19887(new okhttp3.Callback() {
                            /* renamed from: 龘  reason: contains not printable characters */
                            public void m17386(okhttp3.Call call, IOException iOException) {
                                Logger.m6281((Throwable) iOException, new boolean[0]);
                            }

                            /* renamed from: 龘  reason: contains not printable characters */
                            public void m17387(okhttp3.Call call, okhttp3.Response response) throws IOException {
                                ResponseBody r3;
                                try {
                                    if (response.m7065() && (r3 = response.m7056()) != null) {
                                        String r1 = r3.m7091();
                                        try {
                                            r3.close();
                                        } catch (Exception e) {
                                            Logger.m6281((Throwable) e, new boolean[0]);
                                        }
                                        String r2 = Regex.m17802(r1, "\\((.*)\\)", 1, true);
                                        if (!r2.isEmpty()) {
                                            List<String> r4 = ImdbApi.m15726().m15729(r2);
                                            if (!r4.isEmpty()) {
                                                arrayList2.addAll(r4);
                                                BaseActivity.this.runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        try {
                                                            MatrixCursor matrixCursor = new MatrixCursor(strArr);
                                                            for (int i = 0; i < arrayList2.size(); i++) {
                                                                matrixCursor.addRow(new String[]{Integer.toString(i), (String) arrayList2.get(i), (String) arrayList2.get(i)});
                                                            }
                                                            simpleCursorAdapter2.swapCursor(matrixCursor);
                                                        } catch (Exception e) {
                                                            Logger.m6281((Throwable) e, new boolean[0]);
                                                            arrayList2.clear();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                } catch (Exception e2) {
                                    Logger.m6281((Throwable) e2, new boolean[0]);
                                }
                            }
                        });
                    }
                    return false;
                }

                public boolean onQueryTextSubmit(String str) {
                    if (!z3 || menuItem3 == null) {
                        return false;
                    }
                    menuItem3.collapseActionView();
                    return false;
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17297(UpdateInfo updateInfo) {
        this.f13599 = updateInfo;
        if (!isFinishing()) {
            AlertDialog r0 = new AlertDialog.Builder(this).m538(false).m536((CharSequence) I18N.m15707(R.string.update_title, updateInfo.getVersion())).m523((CharSequence) I18N.m15707(R.string.update_message, updateInfo.getVersion()) + StringUtils.LF + updateInfo.getDescription()).m537((CharSequence) I18N.m15706(R.string.update_button), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        dialogInterface.dismiss();
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                    BaseActivity.this.m17302((Action1<Boolean>) new Action1<Boolean>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void call(Boolean bool) {
                            if (!bool.booleanValue()) {
                                Toast.makeText(BaseActivity.this, I18N.m15706(R.string.permission_grant_toast), 1).show();
                                Utils.m6420((Context) BaseActivity.this);
                            } else if (BaseActivity.this.f13599 != null) {
                                BaseActivity.this.f13605.m16163(BaseActivity.this.f13599);
                            }
                        }
                    });
                }
            }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        dialogInterface.dismiss();
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }).m528();
            if (TyphoonApp.f5857) {
                HttpHelper.m6343().m6347();
                r0.m515(-2).setEnabled(false);
            }
            try {
                if (updateInfo.isUpdateDisabled()) {
                    HttpHelper.m6343().m6347();
                    r0.m515(-1).setEnabled(false);
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17298(com.typhoon.tv.model.media.MediaInfo mediaInfo, int i, int i2, boolean z, boolean z2, Callback<SyncResponse> callback) {
        if (TraktCredentialsHelper.m16110().isValid() && TVApplication.m6285().getBoolean("pref_auto_add_watched_episode_trakt", true)) {
            if (!NetworkUtils.m17799()) {
                Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
                return;
            }
            if (z2) {
                Toast.makeText(this, "Sending to Trakt...", 0).show();
            }
            if (callback == null) {
                callback = this.f13603;
            }
            try {
                TraktUserApi.m15774().m15783(mediaInfo, i, i2, z, callback);
            } catch (Exception e) {
                if (z2) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    Toast.makeText(this, "Failed to send to Trakt...", 0).show();
                    return;
                }
                throw e;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17299(com.typhoon.tv.model.media.MediaInfo mediaInfo, boolean z, boolean z2, Callback<SyncResponse> callback) {
        if (TraktCredentialsHelper.m16110().isValid() && TVApplication.m6285().getBoolean("pref_auto_add_favorite_trakt", true)) {
            if (!NetworkUtils.m17799()) {
                Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
                return;
            }
            if (z2) {
                Toast.makeText(this, "Sending to Trakt...", 0).show();
            }
            if (callback == null) {
                callback = this.f13603;
            }
            try {
                TraktUserApi.m15774().m15780(mediaInfo, z, callback);
            } catch (Exception e) {
                if (z2) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    Toast.makeText(this, "Failed to send to Trakt...", 0).show();
                    return;
                }
                throw e;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17300(Integer num) {
        if (!isFinishing() && this.f13604 != null && this.f13604.isShowing()) {
            this.f13604.setProgress(num.intValue());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17301(Throwable th) {
        String r0 = I18N.m15706(R.string.update_failed);
        if (!(th == null || th.getMessage() == null)) {
            r0 = r0 + "\n\n" + th.getMessage();
        }
        Toast.makeText(this, r0, 1).show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17302(Action1<Boolean> action1) {
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
        if (Build.VERSION.SDK_INT > 25) {
            arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        Subscription r1 = new RxPermissions(this).m15681((String[]) arrayList.toArray(new String[arrayList.size()])).m7376(new Func1<Throwable, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Boolean call(Throwable th) {
                return false;
            }
        }).m7397(action1);
        if (this.f13595 != null) {
            this.f13595.m25086(r1);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17303(boolean z) {
        if (this.f13602 != null) {
            this.f13602.m16127();
        }
        this.f13602 = new DownloadYPPresenterImpl(this);
        this.f13602.m16129(z);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        r1 = r12.f13600.m8022();
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m17304(com.google.android.gms.cast.MediaInfo r13, long r14) {
        /*
            r12 = this;
            com.google.android.gms.cast.framework.CastSession r2 = r12.f13600
            if (r2 == 0) goto L_0x000a
            boolean r2 = com.typhoon.tv.utils.GmsUtils.m6392((android.content.Context) r12)
            if (r2 != 0) goto L_0x000c
        L_0x000a:
            r2 = 0
        L_0x000b:
            return r2
        L_0x000c:
            com.google.android.gms.cast.framework.CastSession r2 = r12.f13600
            com.google.android.gms.cast.framework.media.RemoteMediaClient r1 = r2.m8022()
            if (r1 != 0) goto L_0x0016
            r2 = 0
            goto L_0x000b
        L_0x0016:
            com.typhoon.tv.ui.activity.base.BaseActivity$2 r2 = new com.typhoon.tv.ui.activity.base.BaseActivity$2
            r2.<init>(r1)
            r1.m4165((com.google.android.gms.cast.framework.media.RemoteMediaClient.Listener) r2)
            r11 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.List r2 = r13.m7838()
            if (r2 == 0) goto L_0x0069
            java.util.List r2 = r13.m7838()
            java.util.Iterator r2 = r2.iterator()
        L_0x0032:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0069
            java.lang.Object r10 = r2.next()
            com.google.android.gms.cast.MediaTrack r10 = (com.google.android.gms.cast.MediaTrack) r10
            int r3 = r10.m7929()     // Catch:{ Exception -> 0x0061 }
            switch(r3) {
                case 1: goto L_0x0046;
                case 2: goto L_0x0045;
                case 3: goto L_0x0055;
                default: goto L_0x0045;
            }     // Catch:{ Exception -> 0x0061 }
        L_0x0045:
            goto L_0x0032
        L_0x0046:
            if (r11 != 0) goto L_0x0032
            long r4 = r10.m7935()     // Catch:{ Exception -> 0x0061 }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0061 }
            r0.add(r3)     // Catch:{ Exception -> 0x0061 }
            r11 = 1
            goto L_0x0032
        L_0x0055:
            long r4 = r10.m7935()     // Catch:{ Exception -> 0x0061 }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0061 }
            r0.add(r3)     // Catch:{ Exception -> 0x0061 }
            goto L_0x0032
        L_0x0061:
            r8 = move-exception
            r3 = 0
            boolean[] r3 = new boolean[r3]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r3)
            goto L_0x0032
        L_0x0069:
            r2 = 0
            int r2 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0071
            r14 = 1
        L_0x0071:
            boolean r2 = r0.isEmpty()     // Catch:{ Exception -> 0x00a2 }
            if (r2 != 0) goto L_0x009d
            int r2 = r0.size()     // Catch:{ Exception -> 0x00a2 }
            long[] r6 = new long[r2]     // Catch:{ Exception -> 0x00a2 }
            r9 = 0
        L_0x007e:
            int r2 = r0.size()     // Catch:{ Exception -> 0x00a2 }
            if (r9 >= r2) goto L_0x0093
            java.lang.Object r2 = r0.get(r9)     // Catch:{ Exception -> 0x00a2 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ Exception -> 0x00a2 }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x00a2 }
            r6[r9] = r2     // Catch:{ Exception -> 0x00a2 }
            int r9 = r9 + 1
            goto L_0x007e
        L_0x0093:
            r3 = 1
            r7 = 0
            r2 = r13
            r4 = r14
            r1.m4160(r2, r3, r4, r6, r7)     // Catch:{ Exception -> 0x00a2 }
        L_0x009a:
            r2 = 1
            goto L_0x000b
        L_0x009d:
            r2 = 1
            r1.m4159((com.google.android.gms.cast.MediaInfo) r13, (boolean) r2, (long) r14)     // Catch:{ Exception -> 0x00a2 }
            goto L_0x009a
        L_0x00a2:
            r8 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r2)
            r2 = 0
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.base.BaseActivity.m17304(com.google.android.gms.cast.MediaInfo, long):boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17305(boolean... zArr) {
        boolean z = zArr != null && zArr.length > 0 && zArr[0];
        CastSession castSession = null;
        try {
            castSession = m17287();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        return castSession != null && (castSession.m8053() || (!z && castSession.m8054()));
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public void m17306() {
        TyphoonApp.f5855 = true;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public void m17307() {
        try {
            if (TVApplication.m6285().getBoolean("pref_auto_backup_before_update", false)) {
                PrefsBackupRestoreHelper.m15825();
                FavBackupRestoreHelper.m15823();
                WatchedEpsBackupRestoreHelper.m15830();
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (Utils.m6423()) {
            try {
                m17265();
            } catch (Throwable th) {
                Logger.m6281(th, true);
                m17272();
            }
        } else {
            Toast.makeText(this, I18N.m15706(DeviceUtils.m6388() ? R.string.files_error_unknown_sources_amazon : R.string.files_error_unknown_sources), 1).show();
            try {
                startActivity(new Intent("android.settings.SECURITY_SETTINGS"));
            } catch (Throwable th2) {
                Logger.m6281(th2, new boolean[0]);
            }
        }
    }
}
