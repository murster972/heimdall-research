package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.Ty.xapp.event.AdStatusEvent;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.bumptech.glide.Glide;
import com.evernote.android.state.State;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.gson.Gson;
import com.livefront.bridge.Bridge;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.backup.SubsMapBackupRestoreHelper;
import com.typhoon.tv.chromecast.CastHelper;
import com.typhoon.tv.event.OnInterstitialAdShownEvent;
import com.typhoon.tv.event.ReCaptchaRequiredEvent;
import com.typhoon.tv.helper.ClipboardHelper;
import com.typhoon.tv.helper.PlayActionHelper;
import com.typhoon.tv.helper.TapTargetViewHelper;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.player.ExoPlayerHelper;
import com.typhoon.tv.helper.player.YesPlayerHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.helper.trakt.TraktHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.presenter.ISourcePresenter;
import com.typhoon.tv.presenter.impl.SourcePresenterImpl;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.ui.activity.base.BasePlayActivity;
import com.typhoon.tv.ui.adapter.MediaSourceArrayAdapter;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.Downloader;
import com.typhoon.tv.utils.GmsUtils;
import com.typhoon.tv.utils.IntentUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.SourceUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.utils.comparator.MediaAlphanumComparator;
import com.typhoon.tv.view.ISourceView;
import com.uwetrottmann.trakt5.entities.EpisodeCheckin;
import com.uwetrottmann.trakt5.entities.EpisodeCheckinResponse;
import com.uwetrottmann.trakt5.entities.MovieCheckin;
import com.uwetrottmann.trakt5.entities.MovieCheckinResponse;
import com.uwetrottmann.trakt5.entities.MovieIds;
import com.uwetrottmann.trakt5.entities.Show;
import com.uwetrottmann.trakt5.entities.ShowIds;
import com.uwetrottmann.trakt5.entities.SyncEpisode;
import com.uwetrottmann.trakt5.entities.SyncMovie;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import xiaofei.library.hermeseventbus.HermesEventBus;

public class SourceActivity extends BasePlayActivity implements ISourceView {
    @State
    public boolean dontRestoreSourcesState = false;
    @State
    public boolean isPlaying = false;
    @State
    public int mAdShownTimes = 0;
    @State
    public int mCurrentEpisodeListIndex = 0;
    @State
    public int mEpisode = -1;
    @State
    public boolean mForceSetWatchedOnBackPressed = false;
    @State
    public boolean mIsChildAppAdShown = false;
    @State
    public boolean mIsFromAnotherApp = false;
    @State
    public boolean mIsInterstitialShown = false;
    @State
    public boolean mIsSubtitlesAdShown = false;
    @State
    public boolean mIsSubtitlesChildAppAdShown = false;
    @State
    public boolean mIsWatchedAnyLink = false;
    @State
    public ArrayList<MediaSource> mMediaSources;
    @State
    public boolean mNeedMarkAsWatched = false;
    @State
    public ArrayList<MediaSource> mPreResolvedMediaSources;
    @State
    public ArrayList<MediaSource> mWatchedMediaSources;

    /* renamed from: ʻ  reason: contains not printable characters */
    private ISourcePresenter f13342;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ListView f13343;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public MediaSourceArrayAdapter f13344;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public MediaAlphanumComparator f13345;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String[] f13346;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Subscription f13347;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public List<TapTargetView> f13348;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean f13349;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ArrayList<String> f13350;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Subscription f13351;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Subscription f13352;

    /* renamed from: ˏ  reason: contains not printable characters */
    private ArrayList<Snackbar> f13353;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean f13354;
    /* access modifiers changed from: private */

    /* renamed from: י  reason: contains not printable characters */
    public Snackbar f13355;

    /* renamed from: ـ  reason: contains not printable characters */
    private ScheduledExecutorService f13356;

    /* renamed from: ٴ  reason: contains not printable characters */
    private MenuItem f13357;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public MenuItem f13358;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ScheduledFuture<?> f13359;

    /* renamed from: 连任  reason: contains not printable characters */
    private ArrayList<Integer> f13360 = null;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MediaInfo f13361;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public int f13362 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f13363;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String[] f13364;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Subscription f13365;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m17057() {
        this.f13342 = new SourcePresenterImpl(this);
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private void m17060() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    SourceActivity.this.findViewById(R.id.pbSource).setVisibility(8);
                    SourceActivity.this.m17099(false);
                    if (SourceActivity.this.f13344.getCount() <= 0) {
                        SourceActivity.this.m17095(false);
                    } else {
                        SourceActivity.this.m17095(true);
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        });
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m17061() {
        this.f13721 = BasePlayerHelper.m16032();
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private String m17063() {
        if (this.f13363) {
            return this.f13361.getNameAndYear();
        }
        return I18N.m15707(R.string.season_episode, this.f13361.getName(), Integer.valueOf(this.f13362), Integer.valueOf(this.mEpisode));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m17065() {
        if (!TapTargetViewHelper.m15965("ttv_source")) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (SourceActivity.this.f13348 == null) {
                        List unused = SourceActivity.this.f13348 = new ArrayList();
                    }
                    try {
                        SourceActivity.this.f13348.add(TapTargetView.showFor((Activity) SourceActivity.this, TapTarget.forToolbarMenuItem((Toolbar) SourceActivity.this.findViewById(R.id.toolbarSource), (int) R.id.action_switch_player, (CharSequence) I18N.m15706(R.string.ttv_switch_video_player), (CharSequence) I18N.m15706(R.string.ttv_switch_video_player_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(false)));
                        TapTargetViewHelper.m15964("ttv_source");
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }, 1000);
        }
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private void m17066() {
        if (this.f13360 != null && !this.f13360.isEmpty() && this.mCurrentEpisodeListIndex < this.f13360.size()) {
            if (!NetworkUtils.m17799()) {
                m17122(I18N.m15706(R.string.no_internet));
                return;
            }
            m17077();
            int nextEp = this.f13360.get(this.mCurrentEpisodeListIndex).intValue();
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.m426((CharSequence) this.f13362 + "x" + Utils.m6413(nextEp));
            }
            this.mCurrentEpisodeListIndex++;
            invalidateOptionsMenu();
            this.mIsInterstitialShown = false;
            this.mIsSubtitlesAdShown = false;
            this.mIsChildAppAdShown = false;
            this.mIsSubtitlesChildAppAdShown = false;
            this.mIsSecondAdShown = false;
            this.mAdShownTimes = 0;
            if (TVApplication.m6285().getBoolean("pref_auto_mark_episode_as_watched", true)) {
                TVApplication.m6287().m6300(Integer.valueOf(this.f13361.getTmdbId()), Integer.valueOf(this.f13362), Integer.valueOf(this.mEpisode));
            }
            this.mEpisode = nextEp;
            m17075();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʿʿ  reason: contains not printable characters */
    public void m17069() {
        if (this.f13353 != null) {
            Iterator<Snackbar> it2 = this.f13353.iterator();
            while (it2.hasNext()) {
                Snackbar sb = it2.next();
                if (sb.isShownOrQueued()) {
                    sb.dismiss();
                }
            }
            this.f13353 = null;
        }
    }

    /* renamed from: ــ  reason: contains not printable characters */
    private void m17072() {
        final String prefKey = "is_buffering_tips_dialog_shown_" + Utils.m6393();
        if (!TVApplication.m6285().getBoolean(prefKey, false)) {
            try {
                final MaterialDialog materialDialog = new MaterialDialog.Builder(this).m3797((int) R.drawable.ic_error_white_36dp).m3800((CharSequence) "Video keeps buffering?").m3787((CharSequence) "Switch players if that doesn't help we recommend that you sign up for real-debrid you can do so in the settings.").m3793((CharSequence) "Got it!").m3784(17170456).m3803(true).m3799((MaterialDialog.SingleButtonCallback) new MaterialDialog.SingleButtonCallback() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m17138(MaterialDialog materialDialog, DialogAction dialogAction) {
                        if (materialDialog.m3762()) {
                            TVApplication.m6285().edit().putBoolean(prefKey, true).apply();
                        }
                    }
                }).m3801((CharSequence) I18N.m15706(R.string.dont_show_again), true, (CompoundButton.OnCheckedChangeListener) null).m3794(true).m3791(true).m3789();
                materialDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    public void onShow(DialogInterface dialogInterface) {
                        try {
                            if (materialDialog != null) {
                                MDButton r0 = materialDialog.m3776(DialogAction.POSITIVE);
                                r0.setFocusable(true);
                                r0.setFocusableInTouchMode(true);
                                r0.requestFocus();
                            }
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                    }
                });
                materialDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        try {
                            if (materialDialog != null && materialDialog.m3762()) {
                                TVApplication.m6285().edit().putBoolean(prefKey, true).apply();
                            }
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                    }
                });
                materialDialog.show();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private void m17075() {
        if (!this.f13354) {
            this.f13350 = new ArrayList<>();
            if (this.mMediaSources == null) {
                this.mMediaSources = new ArrayList<>();
            } else {
                this.mMediaSources.clear();
                this.f13344.m17511();
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    SourceActivity.this.f13344.notifyDataSetChanged();
                    SourceActivity.this.findViewById(R.id.pbSource).setVisibility(0);
                    SourceActivity.this.m17099(true);
                    SourceActivity.this.m17095(true);
                }
            });
            this.f13342.m16151(this.f13361, this.f13362, this.mEpisode);
            TVApplication.m6285();
            if (!TVApplication.m6285().getBoolean("pref_resolve_all_links_immediately", true)) {
                View snackbarContainer = findViewById(R.id.source_rootLayout);
                snackbarContainer.setVisibility(0);
                final Snackbar snackbar = Snackbar.make(snackbarContainer, (CharSequence) I18N.m15706(R.string.resolve_all_links_immediately_is_disabled), 0);
                snackbar.setAction((CharSequence) I18N.m15706(R.string.action_settings), (View.OnClickListener) new View.OnClickListener() {
                    public void onClick(View view) {
                        SourceActivity.this.m17077();
                        snackbar.dismiss();
                        SourceActivity.this.startActivity(new Intent(SourceActivity.this, SettingsActivity.class));
                    }
                }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
                snackbar.show();
            }
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private void m17076() {
        m17423(findViewById(R.id.adViewSource));
    }

    /* access modifiers changed from: private */
    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public void m17077() {
        if (this.f13354) {
            this.f13342.m16150();
        }
        m17060();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private void m17078() {
        m17079();
        try {
            this.f13356 = Executors.newSingleThreadScheduledExecutor();
            this.f13359 = this.f13356.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
                        final boolean r0 = SourceActivity.this.m17420();
                        SourceActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    HermesEventBus.getDefault().post(new AdStatusEvent(r0));
                                } catch (Throwable th) {
                                    Logger.m6281(th, new boolean[0]);
                                }
                            }
                        });
                    } catch (Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                    }
                }
            }, 0, 10, TimeUnit.MINUTES);
        } catch (Throwable throwable) {
            Logger.m6281(throwable, new boolean[0]);
        }
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private void m17079() {
        if (this.f13359 != null) {
            try {
                this.f13359.cancel(true);
            } catch (Throwable throwable) {
                Logger.m6281(throwable, new boolean[0]);
            }
        }
        if (this.f13356 != null) {
            try {
                this.f13356.shutdownNow();
            } catch (Throwable throwable2) {
                Logger.m6281(throwable2, new boolean[0]);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ᵢ  reason: contains not printable characters */
    public void m17080() {
        if (!TyphoonApp.f5865) {
            if ((!this.mIsInterstitialShown && this.mAdShownTimes < 1) || m17422(new boolean[0])) {
                m17421();
            }
        }
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    private void m17081() {
        try {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (SourceActivity.this.f13358 != null && SourceActivity.this.f13358.isVisible()) {
                        new IntroductoryOverlay.Builder(SourceActivity.this, SourceActivity.this.f13358).m8049(I18N.m15706(R.string.chromecast_available)).m8045(I18N.m15706(R.string.ok)).m8048().m8042().m8037();
                    }
                }
            }, 1000);
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m17082(MediaSource selectedMediaSource) {
        String playLink = selectedMediaSource.getStreamLink();
        String playTitle = m17063();
        String fileExt = Utils.m6408(playLink);
        if (fileExt.isEmpty()) {
            fileExt = selectedMediaSource.isHLS() ? ".m3u8" : ".mp4";
        } else if (!fileExt.equalsIgnoreCase(".avi") && !fileExt.equalsIgnoreCase(".rmvb") && !fileExt.equalsIgnoreCase(".flv") && !fileExt.equalsIgnoreCase(".mkv") && !fileExt.equalsIgnoreCase(".mp2") && !fileExt.equalsIgnoreCase(".mp2v") && !fileExt.equalsIgnoreCase(".mp4") && !fileExt.equalsIgnoreCase(".mp4v") && !fileExt.equalsIgnoreCase(".mpe") && !fileExt.equalsIgnoreCase(".mpeg") && !fileExt.equalsIgnoreCase(".mpeg1") && !fileExt.equalsIgnoreCase(".mpeg2") && !fileExt.equalsIgnoreCase(".mpeg4") && !fileExt.equalsIgnoreCase(".mpg") && !fileExt.equalsIgnoreCase(".ts") && !fileExt.equalsIgnoreCase(".m3u8")) {
            fileExt = selectedMediaSource.isHLS() ? ".m3u8" : ".mp4";
        }
        if (!Downloader.m17786((Context) this)) {
            Downloader.m17781(this).show();
        } else if (Downloader.m17787(TVApplication.m6288(), playLink, playTitle + fileExt, this.f13361, playTitle, SourceUtils.m17835(selectedMediaSource.getPlayHeader()))) {
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.download_started), 0).show();
            m17080();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m17083() {
        boolean z = false;
        if (!m17100()) {
            Toast.makeText(this, I18N.m15706(R.string.error), 0).show();
            return false;
        }
        if (this.f13361.getType() == 1) {
            z = true;
        }
        this.f13363 = z;
        this.f13348 = new ArrayList();
        this.f13345 = new MediaAlphanumComparator();
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbarSource);
        setSupportActionBar(mToolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.m427(true);
            actionBar.m433(true);
            actionBar.m440((CharSequence) this.f13361.getNameAndYear());
            if (!this.f13363) {
                if (this.f13362 >= 1) {
                    actionBar.m426((CharSequence) this.f13362 + "x" + Utils.m6413(this.mEpisode));
                } else {
                    actionBar.m426((CharSequence) I18N.m15706(R.string.season_special) + " - " + Utils.m6413(this.mEpisode));
                }
            }
        }
        this.f13343 = (ListView) findViewById(R.id.lvSources);
        if (this.mMediaSources == null || this.mMediaSources.isEmpty()) {
            this.mMediaSources = new ArrayList<>();
        }
        this.f13344 = new MediaSourceArrayAdapter(this, 17367043, this.mMediaSources);
        this.f13343.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                SourceActivity.this.m17105(i);
            }
        });
        this.f13343.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                SourceActivity.this.m17086(i);
                return true;
            }
        });
        ImageView ivNoSource = (ImageView) findViewById(R.id.ivNoSource);
        ivNoSource.setColorFilter(-7829368, PorterDuff.Mode.SRC_ATOP);
        Glide.m3938((FragmentActivity) this).m3973(Integer.valueOf(R.drawable.ic_sentiment_very_dissatisfied_white_48dp)).m25186(100, 100).m25185().m25181().m25209(ivNoSource);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17086(int i) {
        MediaSource mediaSource = (MediaSource) this.f13344.getItem(i);
        if (mediaSource != null && mediaSource.getStreamLink() != null && !mediaSource.getStreamLink().isEmpty()) {
            if (!mediaSource.isResolved()) {
                m17107(-1, mediaSource, i);
            } else {
                m17108(mediaSource);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17087(final MediaSource selectedDto) {
        m17077();
        String streamLink = selectedDto.getStreamLink();
        String fileExt = Utils.m6408(streamLink);
        if (fileExt.isEmpty()) {
            fileExt = selectedDto.isHLS() ? ".m3u8" : ".mp4";
        } else if (!fileExt.equalsIgnoreCase(".avi") && !fileExt.equalsIgnoreCase(".rmvb") && !fileExt.equalsIgnoreCase(".flv") && !fileExt.equalsIgnoreCase(".mkv") && !fileExt.equalsIgnoreCase(".mp2") && !fileExt.equalsIgnoreCase(".mp2v") && !fileExt.equalsIgnoreCase(".mp4") && !fileExt.equalsIgnoreCase(".mp4v") && !fileExt.equalsIgnoreCase(".mpe") && !fileExt.equalsIgnoreCase(".mpeg") && !fileExt.equalsIgnoreCase(".mpeg1") && !fileExt.equalsIgnoreCase(".mpeg2") && !fileExt.equalsIgnoreCase(".mpeg4") && !fileExt.equalsIgnoreCase(".mpg") && !fileExt.equalsIgnoreCase(".ts") && !fileExt.equalsIgnoreCase(".m3u8")) {
            fileExt = selectedDto.isHLS() ? ".m3u8" : ".mp4";
        }
        Intent i = new Intent("android.intent.action.VIEW");
        if (Build.VERSION.SDK_INT >= 16) {
            i.setDataAndTypeAndNormalize(Uri.parse(streamLink), "video/*");
        } else {
            i.setDataAndType(Uri.parse(streamLink), "video/*");
        }
        i.putExtra(PubnativeAsset.TITLE, m17063() + fileExt);
        i.putExtra("suppress_error_message", false);
        i.putExtra("secure_uri", true);
        i.putExtra("fromOtherApp", true);
        i.putExtra("gdprScope", TyphoonApp.f5874);
        i.putExtra("showStartupADM", TyphoonApp.f5868);
        i.putExtra("showErrorMessage", true);
        i.putExtra("hls", selectedDto.isHLS());
        i.putExtra("providerName", selectedDto.getProviderName());
        i.putExtra("hostAppName", getPackageName());
        i.putExtra("showHostAppAd", m17420() && !BasePlayerHelper.m16037());
        i.putExtra("disableAds", TyphoonApp.f5865);
        i.putExtra("isChildAppAdShown", this.mIsChildAppAdShown);
        i.putExtra("forceATVMode", DeviceUtils.m6389(new boolean[0]));
        i.putExtra("subsFontScale", TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f));
        i.putExtra("subsFontColorHex", TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        i.putExtra("subsBgColorHex", TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        if (selectedDto.getPlayHeader() != null && !selectedDto.getPlayHeader().isEmpty()) {
            HashMap<String, String> headers = SourceUtils.m17835(selectedDto.getPlayHeader());
            Bundle castHeadersBundle = new Bundle();
            List<String> headersList = new ArrayList<>();
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                castHeadersBundle.putString(entry.getKey(), entry.getValue());
                headersList.add(entry.getKey());
                headersList.add(entry.getValue());
            }
            i.putExtra("headers", (String[]) headersList.toArray(new String[headersList.size()]));
            i.putExtra("android.media.intent.extra.HTTP_HEADERS", castHeadersBundle);
            String cookie = null;
            if (headers.containsKey("Cookie")) {
                cookie = headers.get("Cookie");
            } else if (headers.containsKey("cookie")) {
                cookie = headers.get("cookie");
            }
            if (cookie != null) {
                i.putExtra("Cookies", cookie);
            }
        }
        Intent chooserIntent = IntentUtils.m17788(this, i, I18N.m15706(R.string.choose_player));
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(chooserIntent, 74);
            m17078();
            m17120();
            this.mIsWatchedAnyLink = true;
            this.mNeedMarkAsWatched = TVApplication.m6285().getBoolean("pref_auto_mark_episode_as_watched", true);
            runOnUiThread(new Runnable() {
                public void run() {
                    SourceActivity.this.f13344.m17513(selectedDto);
                    SourceActivity.this.f13344.notifyDataSetChanged();
                }
            });
            return;
        }
        m17122(I18N.m15706(R.string.no_player_was_found));
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17088(final MediaSource mediaSource, boolean isRetry) {
        if (m17116(mediaSource, true)) {
            runOnUiThread(new Runnable() {
                public void run() {
                    SourceActivity.this.f13344.m17513(mediaSource);
                    SourceActivity.this.f13344.notifyDataSetChanged();
                }
            });
            if (this.f13353 == null) {
                this.f13353 = new ArrayList<>();
            }
            final Snackbar snackbar = Snackbar.make(findViewById(R.id.source_rootLayout), (CharSequence) I18N.m15706(R.string.please_wait), 0);
            snackbar.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
            snackbar.show();
            this.f13353.add(snackbar);
            m17077();
            if (m17288()) {
                m17431(this.f13361, this.f13362, this.mEpisode, m17063(), new BasePlayActivity.OnReceiveLastPlaybackPositionListener() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m17133(long j) {
                        boolean z = false;
                        try {
                            z = SourceActivity.this.m17304(CastHelper.m15832(CastHelper.m15834(SourceActivity.this.f13361, SourceActivity.this.f13362, SourceActivity.this.mEpisode, mediaSource), mediaSource), j);
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        if (z) {
                            SourceActivity.this.m17069();
                            if (Utils.m6410()) {
                                Utils.m6422(false);
                            }
                            try {
                                SubsMapBackupRestoreHelper.m15826();
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                            }
                            Toast.makeText(SourceActivity.this, I18N.m15706(R.string.preparing_to_cast), 1).show();
                            SourceActivity.this.m17080();
                            return;
                        }
                        SourceActivity.this.m17122(I18N.m15706(R.string.error));
                    }
                });
            } else if (!isRetry) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        SourceActivity.this.m17088(mediaSource, true);
                    }
                }, 2000);
            } else {
                m17069();
                m17123(I18N.m15706(R.string.chromecast_not_connected), 0);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17092() {
        List<String> actionList = PlayActionHelper.m15963(false);
        actionList.remove(0);
        this.f13346 = (String[]) actionList.toArray(new String[actionList.size()]);
        List<String> castActionList = PlayActionHelper.m15963(true);
        castActionList.remove(0);
        this.f13364 = (String[]) castActionList.toArray(new String[castActionList.size()]);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17093(final MediaSource selectedMediaSource) {
        if (selectedMediaSource.isHLS()) {
            new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) I18N.m15706(R.string.cant_be_opened_by_other_players)).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).m528();
            return;
        }
        m17077();
        if (TVApplication.m6285().getInt("pref_choose_media_down_manager", 0) == 0) {
            List<String> permissions = new ArrayList<>();
            permissions.add("android.permission.WRITE_EXTERNAL_STORAGE");
            if (Build.VERSION.SDK_INT > 25) {
                permissions.add("android.permission.READ_EXTERNAL_STORAGE");
            }
            this.f13365 = new RxPermissions(this).m15681((String[]) permissions.toArray(new String[permissions.size()])).m7376(new Func1<Throwable, Boolean>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Boolean call(Throwable th) {
                    return false;
                }
            }).m7397(new Action1<Boolean>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Boolean bool) {
                    if (bool.booleanValue()) {
                        SourceActivity.this.m17082(selectedMediaSource);
                        return;
                    }
                    Toast.makeText(SourceActivity.this, I18N.m15706(R.string.permission_grant_toast), 1).show();
                    Utils.m6420((Context) SourceActivity.this);
                }
            });
        } else {
            m17058(selectedMediaSource);
        }
        runOnUiThread(new Runnable() {
            public void run() {
                SourceActivity.this.f13344.m17513(selectedMediaSource);
                SourceActivity.this.f13344.notifyDataSetChanged();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m17095(boolean hasSource) {
        TextView tvNoSource = (TextView) findViewById(R.id.tvNoSource);
        ImageView ivNoSource = (ImageView) findViewById(R.id.ivNoSource);
        if (hasSource) {
            tvNoSource.setVisibility(8);
            ivNoSource.setVisibility(8);
            this.f13343.setVisibility(0);
            return;
        }
        this.f13343.setVisibility(8);
        tvNoSource.setVisibility(0);
        ivNoSource.setVisibility(0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17097(final MediaSource selectedDto) {
        if (selectedDto.isHLS() || (selectedDto.getPlayHeader() != null && !selectedDto.getPlayHeader().isEmpty())) {
            new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) I18N.m15706(R.string.link_must_be_played_in_app)).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).m528();
            return;
        }
        ClipboardHelper.m15930(TVApplication.m6288(), I18N.m15706(R.string.app_name), selectedDto.getStreamLink());
        runOnUiThread(new Runnable() {
            public void run() {
                SourceActivity.this.f13344.m17513(selectedDto);
                SourceActivity.this.f13344.notifyDataSetChanged();
            }
        });
        Toast.makeText(this, I18N.m15706(R.string.copied_link), 1).show();
        m17080();
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m17099(boolean z) {
        this.f13354 = z;
        if (this.f13357 != null) {
            this.f13357.setIcon(z ? R.drawable.ic_close_white_36dp : R.drawable.ic_refresh_white_36dp);
            this.f13357.setTitle(z ? I18N.m15706(R.string.action_stop) : I18N.m15706(R.string.action_refresh));
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m17100() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey("mediaInfo")) {
                Object obj = extras.get("mediaInfo");
                if (obj instanceof MediaInfo) {
                    this.f13361 = (MediaInfo) obj;
                } else {
                    this.f13361 = (MediaInfo) new Gson().fromJson((String) obj, MediaInfo.class);
                }
                if (this.f13362 == -1) {
                    this.f13362 = extras.getInt("season", this.f13362);
                }
                if (this.mEpisode == -1) {
                    this.mEpisode = extras.getInt("episode", this.mEpisode);
                }
                this.f13360 = extras.getIntegerArrayList("nextEpisodeList");
                this.mIsFromAnotherApp = extras.getBoolean("isFromAnotherApp", false);
                this.mForceSetWatchedOnBackPressed = extras.getBoolean("forceSetWatchedOnBackPressed", false);
                return true;
            } else if (extras != null && intent.getAction() != null && intent.getAction().equals("com.typhoon.tv.GET_SOURCES") && extras.containsKey(VastExtensionXmlManager.TYPE) && extras.containsKey("name") && extras.containsKey("year")) {
                this.mIsFromAnotherApp = true;
                String name = extras.getString("name");
                int year = extras.getInt("year", -1);
                if (this.f13362 == -1) {
                    this.f13362 = extras.getInt("season", this.f13362);
                }
                if (this.mEpisode == -1) {
                    this.mEpisode = extras.getInt("episode", this.mEpisode);
                }
                int tmdbId = extras.getInt("tmdbId", -1);
                String imdbId = extras.containsKey("imdbId") ? extras.getString("imdbId") : null;
                int type = extras.getInt(VastExtensionXmlManager.TYPE);
                if ((type == 0 || type == 1) && name != null && !name.isEmpty() && year > -1) {
                    this.f13361 = new MediaInfo(type, 1, tmdbId, name, year);
                    if (imdbId == null) {
                        return true;
                    }
                    this.f13361.setImdbId(imdbId);
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17105(int i) {
        MediaSource mediaSource = (MediaSource) this.f13344.getItem(i);
        if (mediaSource != null && mediaSource.getStreamLink() != null && !mediaSource.getStreamLink().isEmpty()) {
            int defaultPlayAction = TVApplication.m6285().getInt("pref_choose_default_play_action", 0) - 1;
            if (!mediaSource.isResolved()) {
                m17107(defaultPlayAction, mediaSource, i);
            } else if (defaultPlayAction == -1) {
                m17108(mediaSource);
            } else {
                m17106(defaultPlayAction, mediaSource);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17106(int actionNum, MediaSource mediaSource) {
        switch (actionNum + 1) {
            case 1:
                m17110(mediaSource, false, false);
                break;
            case 2:
                m17110(mediaSource, true, false);
                break;
            case 3:
                m17087(mediaSource);
                break;
            case 4:
                m17093(mediaSource);
                break;
            case 5:
                m17110(mediaSource, true, true);
                break;
            case 6:
                m17097(mediaSource);
                break;
            default:
                return;
        }
        if (this.f13361.getType() == 0 && this.f13361.getTmdbId() > -1) {
            TVApplication.m6287().m6315(Integer.valueOf(this.f13361.getTmdbId()), this.f13362, this.mEpisode);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17107(final int actionNum, MediaSource mediaSource, int mediaSourcePosition) {
        if (mediaSource == null || mediaSource.getStreamLink() == null || mediaSource.getStreamLink().isEmpty()) {
            m17123(I18N.m15706(R.string.failed_to_resolve_link), 0);
            return;
        }
        ArrayList<MediaSource> pendingSources = new ArrayList<>();
        for (int i = mediaSourcePosition; i < this.f13344.getCount(); i++) {
            pendingSources.add(this.f13344.getItem(i));
        }
        final String noStreamLinkIsAvailableStr = pendingSources.size() <= 1 ? I18N.m15706(R.string.link_cant_be_resolved) : I18N.m15706(R.string.no_streamlink_is_available);
        m17119();
        final MaterialDialog progressDialog = new MaterialDialog.Builder(this).m3800((CharSequence) I18N.m15706(R.string.resolving_links)).m3805(false, pendingSources.size(), true).m3783(false).m3794(false).m3791(false).m3790((CharSequence) I18N.m15706(R.string.action_stop)).m3786((MaterialDialog.SingleButtonCallback) new MaterialDialog.SingleButtonCallback() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17127(MaterialDialog materialDialog, DialogAction dialogAction) {
                SourceActivity.this.m17119();
                if (!SourceActivity.this.isFinishing()) {
                    materialDialog.dismiss();
                }
            }
        }).m3789();
        progressDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface dialogInterface) {
                try {
                    MDButton r0 = progressDialog.m3776(DialogAction.NEGATIVE);
                    r0.setFocusable(true);
                    r0.setFocusableInTouchMode(true);
                    r0.requestFocus();
                } catch (Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }
            }
        });
        progressDialog.show();
        this.f13351 = BaseResolver.m16775(pendingSources, (Action1<MediaSource>) new Action1<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(final MediaSource mediaSource) {
                SourceActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.m3780((CharSequence) I18N.m15706(R.string.resolving) + StringUtils.SPACE + mediaSource.toString());
                        try {
                            if (progressDialog.m3766() == progressDialog.m3767()) {
                                progressDialog.m3774(progressDialog.m3767() + 1);
                            } else if (progressDialog.m3766() > progressDialog.m3767()) {
                                progressDialog.m3774(progressDialog.m3766() + 1);
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, true);
                        }
                        progressDialog.m3777(1);
                        if (SourceActivity.this.mPreResolvedMediaSources == null) {
                            SourceActivity.this.mPreResolvedMediaSources = new ArrayList<>();
                        }
                        SourceActivity.this.mPreResolvedMediaSources.add(mediaSource);
                    }
                });
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<MediaSource>() {
            public void onCompleted() {
                SourceActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (!SourceActivity.this.isFinishing() && !progressDialog.m3768()) {
                            progressDialog.dismiss();
                        }
                        if (!SourceActivity.this.f13349 && !SourceActivity.this.isFinishing()) {
                            new AlertDialog.Builder(SourceActivity.this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) noStreamLinkIsAvailableStr).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (!SourceActivity.this.isFinishing() && dialogInterface != null) {
                                        dialogInterface.dismiss();
                                    }
                                }
                            }).m528();
                        }
                        if (SourceActivity.this.mPreResolvedMediaSources != null) {
                            int size = SourceActivity.this.mPreResolvedMediaSources.size();
                            int i = 0;
                            while (i < size && (!SourceActivity.this.f13349 || i + 1 != size)) {
                                SourceActivity.this.f13344.m17513(SourceActivity.this.mPreResolvedMediaSources.get(i));
                                i++;
                            }
                        }
                        boolean unused = SourceActivity.this.f13349 = false;
                        SourceActivity.this.f13344.notifyDataSetChanged();
                        if (SourceActivity.this.mPreResolvedMediaSources != null) {
                            SourceActivity.this.mPreResolvedMediaSources.clear();
                        }
                        SourceActivity.this.mPreResolvedMediaSources = null;
                    }
                });
            }

            public void onError(final Throwable th) {
                SourceActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Logger.m6280(th, "Error occurred while resolving link", new boolean[0]);
                        if (!SourceActivity.this.isFinishing() && !progressDialog.m3768()) {
                            progressDialog.dismiss();
                        }
                        if (!SourceActivity.this.isFinishing()) {
                            new AlertDialog.Builder(SourceActivity.this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) noStreamLinkIsAvailableStr).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (!SourceActivity.this.isFinishing() && dialogInterface != null) {
                                        dialogInterface.dismiss();
                                    }
                                }
                            }).m528();
                        }
                        SourceActivity.this.f13344.notifyDataSetChanged();
                        if (SourceActivity.this.mPreResolvedMediaSources != null) {
                            SourceActivity.this.mPreResolvedMediaSources.clear();
                        }
                        SourceActivity.this.mPreResolvedMediaSources = null;
                    }
                });
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(final MediaSource mediaSource) {
                if (mediaSource == null) {
                    return;
                }
                if (!mediaSource.getProviderName().isEmpty() || !mediaSource.getHostName().isEmpty()) {
                    boolean unused = SourceActivity.this.f13349 = true;
                    SourceActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (actionNum == -1) {
                                SourceActivity.this.m17108(mediaSource);
                            } else {
                                SourceActivity.this.m17106(actionNum, mediaSource);
                            }
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17108(final MediaSource mediaSource) {
        if (this.f13346 == null || this.f13364 == null || this.f13346.length <= 0 || this.f13364.length <= 0) {
            m17092();
        }
        new AlertDialog.Builder(this).m536((CharSequence) mediaSource.toString()).m538(true).m540((CharSequence[]) m17116(mediaSource, false) ? this.f13364 : this.f13346, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                SourceActivity.this.m17106(i, mediaSource);
            }
        }).m528();
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17109(final MediaSource mediaSource, String str, long j) {
        runOnUiThread(new Runnable() {
            public void run() {
                SourceActivity.this.f13344.m17513(mediaSource);
                SourceActivity.this.f13344.notifyDataSetChanged();
            }
        });
        if (this.f13721.m16045(this, mediaSource, str, j)) {
            this.isPlaying = true;
            if (this.f13721 instanceof YesPlayerHelper) {
                m17078();
            }
            m17120();
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.ready_to_play_toast), 0).show();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17110(final MediaSource mediaSource, boolean z, boolean z2) {
        m17077();
        final String r4 = m17063();
        if (!z) {
            if (m17305(true)) {
                m17088(mediaSource, false);
                return;
            }
            m17431(this.f13361, this.f13362, this.mEpisode, r4, new BasePlayActivity.OnReceiveLastPlaybackPositionListener() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17132(long j) {
                    SourceActivity.this.m17109(mediaSource, r4, j);
                }
            });
        } else if (!mediaSource.isHLS() || !z2) {
            if (this.f13347 != null && !this.f13347.isUnsubscribed()) {
                this.f13347.unsubscribe();
                this.f13347 = null;
            }
            Intent intent = new Intent(this, SubtitlesActivity.class);
            intent.putExtra("mediaInfo", this.f13361);
            intent.putExtra("season", this.f13362);
            intent.putExtra("episode", this.mEpisode);
            intent.putExtra("mediaSrc", mediaSource);
            intent.putExtra("playTitle", r4);
            intent.putExtra("isSubtitlesAdShown", this.mIsSubtitlesAdShown);
            intent.putExtra("isSubtitlesChildAppAdShown", this.mIsSubtitlesChildAppAdShown);
            intent.putExtra("isDownload", z2);
            startActivityForResult(intent, 3);
        } else {
            new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) I18N.m15706(R.string.cant_be_opened_by_other_players)).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).m528();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17116(MediaSource mediaSource, boolean showToastOrDialog) {
        if (DeviceUtils.m6388()) {
            if (!showToastOrDialog) {
                return false;
            }
            m17123(I18N.m15706(R.string.google_play_services_not_installed), 0);
            return false;
        } else if (GmsUtils.m6392((Context) this)) {
            if (m17305(true)) {
                HashMap<String, String> headers = mediaSource.getPlayHeader();
                if ((headers == null || headers.isEmpty() || (!headers.containsKey("Cookie") && !headers.containsKey("Referer"))) && !Utils.m6408(mediaSource.getStreamLink()).equalsIgnoreCase(".flv")) {
                    return true;
                }
                if (!showToastOrDialog) {
                    return false;
                }
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.sorry)).m523((CharSequence) I18N.m15706(R.string.cant_be_casted_message)).m538(true).m524((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!SourceActivity.this.isFinishing() && dialogInterface != null) {
                            dialogInterface.dismiss();
                        }
                    }
                }).m528();
                return false;
            } else if (!showToastOrDialog) {
                return false;
            } else {
                m17123(I18N.m15706(R.string.chromecast_not_connected), 0);
                return false;
            }
        } else if (!showToastOrDialog || GmsUtils.m6391((Activity) this)) {
            return false;
        } else {
            m17123(I18N.m15706(R.string.google_play_services_not_installed), 0);
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹳ  reason: contains not printable characters */
    public void m17119() {
        this.f13349 = false;
        if (this.f13351 != null && !this.f13351.isUnsubscribed()) {
            this.f13351.unsubscribe();
        }
        this.f13351 = null;
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private void m17120() {
        if (TraktCredentialsHelper.m16110().isValid() && TVApplication.m6285().getBoolean("pref_auto_check_in_trakt", true) && NetworkUtils.m17799()) {
            m17123("Checking you in...", 0);
            try {
                if (this.f13363) {
                    MovieIds movieIds = new MovieIds();
                    movieIds.imdb = this.f13361.getImdbId();
                    movieIds.tmdb = Integer.valueOf(this.f13361.getTmdbId());
                    final MovieCheckin checkin = new MovieCheckin.Builder(new SyncMovie().id(movieIds), "Typhoon 3.0.21", (String) null).message("I'm watching " + this.f13361.getNameAndYear()).build();
                    TraktHelper.m16114().m17906().deleteActiveCheckin().m24295(new Callback<Void>() {
                        public void onFailure(Call<Void> call, Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                            SourceActivity.this.m17123("Failed to check in...", 0);
                        }

                        public void onResponse(Call<Void> call, Response<Void> response) {
                            TraktHelper.m16114().m17906().checkin(checkin).m24295(new Callback<MovieCheckinResponse>() {
                                public void onFailure(Call<MovieCheckinResponse> call, Throwable th) {
                                    Logger.m6281(th, new boolean[0]);
                                    SourceActivity.this.m17123("Failed to check in...", 0);
                                }

                                public void onResponse(Call<MovieCheckinResponse> call, Response<MovieCheckinResponse> response) {
                                    if (response.m24390()) {
                                        SourceActivity.this.m17123("Checked in successfully...", 0);
                                    } else {
                                        SourceActivity.this.m17123("Failed to check in...", 0);
                                    }
                                }
                            });
                        }
                    });
                    return;
                }
                ShowIds showIds = new ShowIds();
                showIds.imdb = this.f13361.getImdbId();
                showIds.tmdb = Integer.valueOf(this.f13361.getTmdbId());
                showIds.tvdb = Integer.valueOf(this.f13361.getTvdbId());
                Show show = new Show();
                show.ids = showIds;
                final EpisodeCheckin checkin2 = new EpisodeCheckin.Builder(new SyncEpisode().season(this.f13362).number(this.mEpisode), "Typhoon 3.0.21", (String) null).show(show).message("I'm watching " + this.f13361.getName() + StringUtils.SPACE + this.f13362 + "x" + Utils.m6413(this.mEpisode)).build();
                TraktHelper.m16114().m17906().deleteActiveCheckin().m24295(new Callback<Void>() {
                    public void onFailure(Call<Void> call, Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                        SourceActivity.this.m17123("Failed to check in...", 0);
                    }

                    public void onResponse(Call<Void> call, Response<Void> response) {
                        TraktHelper.m16114().m17906().checkin(checkin2).m24295(new Callback<EpisodeCheckinResponse>() {
                            public void onFailure(Call<EpisodeCheckinResponse> call, Throwable th) {
                                Logger.m6281(th, new boolean[0]);
                                SourceActivity.this.m17123("Failed to check in...", 0);
                            }

                            public void onResponse(Call<EpisodeCheckinResponse> call, Response<EpisodeCheckinResponse> response) {
                                if (response.m24390()) {
                                    SourceActivity.this.m17123("Checked in successfully...", 0);
                                } else {
                                    SourceActivity.this.m17123("Failed to check in...", 0);
                                }
                            }
                        });
                    }
                });
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                m17123("Failed to check in...", 0);
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (m17282() && (((event.getAction() == 0 && (event.getKeyCode() == 4 || event.getKeyCode() == 23 || event.getKeyCode() == 66 || event.getKeyCode() == 160 || event.getKeyCode() == 126 || event.getKeyCode() == 85 || event.getKeyCode() == 0)) || event.isLongPress()) && this.f13348 != null && !this.f13348.isEmpty())) {
            boolean dismissedAny = false;
            try {
                for (TapTargetView tapTargetView : this.f13348) {
                    if (tapTargetView != null) {
                        try {
                            if (tapTargetView.isVisible()) {
                                tapTargetView.dismiss(false);
                                this.f13348.remove(tapTargetView);
                                dismissedAny = true;
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                }
                if (dismissedAny) {
                    return true;
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void finish() {
        boolean z = true;
        Intent intent = new Intent();
        intent.putExtra("isInterstitialShown", this.mIsInterstitialShown || this.mIsSubtitlesAdShown || this.mIsChildAppAdShown || this.mIsSubtitlesChildAppAdShown);
        intent.putExtra("isWatchedAnyLink", this.mIsWatchedAnyLink);
        intent.putExtra("needMarkAsWatched", this.mNeedMarkAsWatched);
        if (this.mMediaSources == null || this.mMediaSources.size() <= 0) {
            z = false;
        }
        intent.putExtra("hasLink", z);
        intent.putExtra("episode", this.mEpisode);
        setResult(-1, intent);
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MediaSource mediaSource;
        super.onActivityResult(requestCode, resultCode, data);
        int res = this.f13721.m16047((AppCompatActivity) this, requestCode, resultCode, data);
        if (res > -1) {
            m17079();
            boolean z = true;
            if (data != null && data.getExtras() != null && data.getExtras().containsKey("isChildAppAdShown") && data.getBooleanExtra("isChildAppAdShown", false)) {
                z = false;
                this.mIsChildAppAdShown = true;
                this.mIsInterstitialShown = true;
                this.mAdShownTimes++;
            }
            if (res == 1) {
                if (this.f13721 instanceof ExoPlayerHelper) {
                    m17430(this.f13721, this.f13363, this.f13361.getTmdbId(), this.f13362, this.mEpisode, data, true);
                }
                m17122(I18N.m15706(R.string.video_player_crashed_unexpectedly));
                this.isPlaying = false;
                return;
            }
            this.mIsWatchedAnyLink = true;
            this.mNeedMarkAsWatched = TVApplication.m6285().getBoolean("pref_auto_mark_episode_as_watched", true);
            m17430(this.f13721, this.f13363, this.f13361.getTmdbId(), this.f13362, this.mEpisode, data, false);
            this.isPlaying = false;
            if (z) {
                m17080();
                return;
            }
            return;
        }
        switch (requestCode) {
            case 3:
                if (resultCode == -1 && data != null && data.getExtras() != null) {
                    this.mIsInterstitialShown = data.getExtras().getBoolean("isInterstitialShown", this.mIsInterstitialShown);
                    this.mIsChildAppAdShown = data.getExtras().getBoolean("isChildAppAdShown", this.mIsChildAppAdShown);
                    this.mIsSubtitlesAdShown = data.getExtras().getBoolean("isSubtitlesAdShown", this.mIsSubtitlesAdShown);
                    this.mIsSubtitlesChildAppAdShown = data.getExtras().getBoolean("isSubtitlesChildAppAdShown", this.mIsSubtitlesChildAppAdShown);
                    boolean z2 = data.getExtras().getBoolean("isDownload", false);
                    boolean z3 = data.getExtras().getBoolean("isDownloaded", false);
                    boolean z4 = data.getExtras().getBoolean("isPlayed", false);
                    boolean z5 = data.getExtras().getBoolean("forceShowAd", false);
                    if (!z2 && z4) {
                        m17120();
                        this.mIsWatchedAnyLink = true;
                        this.mNeedMarkAsWatched = TVApplication.m6285().getBoolean("pref_auto_mark_episode_as_watched", true);
                    }
                    if ((z4 || (z2 && z3)) && (mediaSource = (MediaSource) data.getExtras().getParcelable("mediaSrc")) != null) {
                        this.f13344.m17513(mediaSource);
                        runOnUiThread(new Runnable() {
                            public void run() {
                                SourceActivity.this.f13344.notifyDataSetChanged();
                            }
                        });
                    }
                    if (z5) {
                        m17080();
                        return;
                    }
                    return;
                }
                return;
            case 5:
                if (resultCode == -1) {
                    m17077();
                    m17075();
                    return;
                }
                return;
            case 74:
                m17079();
                boolean z6 = true;
                if (data != null && data.getExtras() != null && data.getExtras().containsKey("isChildAppAdShown") && data.getBooleanExtra("isChildAppAdShown", false)) {
                    z6 = false;
                    this.mIsChildAppAdShown = true;
                    this.mIsInterstitialShown = true;
                    this.mAdShownTimes++;
                }
                if (z6) {
                    m17080();
                    return;
                }
                return;
            case 76:
                m17079();
                boolean z7 = true;
                if (data != null && data.getBooleanExtra("isChildAppAdShown", false)) {
                    z7 = false;
                    this.mIsChildAppAdShown = true;
                    this.mIsInterstitialShown = true;
                    this.mAdShownTimes++;
                }
                if (z7) {
                    m17080();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        if ((this.mForceSetWatchedOnBackPressed || this.mIsFromAnotherApp) && this.mNeedMarkAsWatched) {
            if (this.f13363) {
                TVApplication.m6287().m6301(Integer.valueOf(this.f13361.getTmdbId()), this.f13361.getImdbId());
                m17290(this.f13361, true, true, (Callback<SyncResponse>) null);
            } else if (this.f13361.getTmdbId() > 0) {
                TVApplication.m6287().m6300(Integer.valueOf(this.f13361.getTmdbId()), Integer.valueOf(this.f13362), Integer.valueOf(this.mEpisode));
                m17298(this.f13361, this.f13362, this.mEpisode, true, true, (Callback<SyncResponse>) null);
            }
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Bridge.restoreInstanceState(this, bundle);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        setContentView((int) R.layout.activity_source);
        m17061();
        m17284();
        m17092();
        if (!m17083()) {
            if (isTaskRoot()) {
                startActivity(new Intent(this, HomeActivity.class));
            }
            finish();
            return;
        }
        m17057();
        if (!TyphoonApp.f5865) {
            m17076();
        }
        m17289();
        this.f13343.setAdapter(this.f13344);
        this.f13352 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                if ((obj instanceof ReCaptchaRequiredEvent) && !TVApplication.m6285().getBoolean("pref_hide_recaptcha_verification_message", false)) {
                    ReCaptchaRequiredEvent reCaptchaRequiredEvent = (ReCaptchaRequiredEvent) obj;
                    SourceActivity.this.m17124(reCaptchaRequiredEvent.getProviderName(), reCaptchaRequiredEvent.getUrl(), reCaptchaRequiredEvent.getUserAgent());
                }
            }
        });
        this.f13344.m17514((ArrayList<MediaSource>) new ArrayList());
        if (this.mMediaSources == null || this.mMediaSources.isEmpty() || this.dontRestoreSourcesState) {
            this.dontRestoreSourcesState = false;
            if (NetworkUtils.m17799()) {
                m17075();
            } else {
                m17121();
                m17122(I18N.m15706(R.string.no_internet));
            }
        } else {
            this.dontRestoreSourcesState = false;
            if (this.mWatchedMediaSources != null) {
                this.f13344.m17514(this.mWatchedMediaSources);
            }
            try {
                Collections.sort(this.mMediaSources, this.f13345);
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
            this.f13344.notifyDataSetChanged();
            m17121();
        }
        m17419();
        if (!TapTargetViewHelper.m15965("ttv_source")) {
            m17065();
        } else {
            try {
                if (GmsUtils.m6392((Context) this)) {
                    CastSession r0 = CastContext.m7977((Context) this).m7981().m8075();
                    if (CastContext.m7977((Context) this).m7982() != 1 && (r0 == null || r0.m8057() || r0.m8056())) {
                        m17081();
                    }
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
        m17072();
        m17068();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_source_choosing, menu);
        this.f13358 = m17295(menu);
        if (this.f13358 == null) {
            this.f13358 = menu.findItem(R.id.media_route_menu_item);
        }
        this.f13357 = menu.findItem(R.id.action_refresh);
        if (this.f13354) {
            this.f13357.setIcon(R.drawable.ic_close_white_36dp);
            this.f13357.setTitle(I18N.m15706(R.string.action_stop));
        } else {
            this.f13357.setIcon(R.drawable.ic_refresh_white_36dp);
            this.f13357.setTitle(I18N.m15706(R.string.action_refresh));
        }
        MenuItem findItem = menu.findItem(R.id.action_next_episode);
        if (this.f13360 == null || this.f13360.isEmpty() || this.mCurrentEpisodeListIndex >= this.f13360.size()) {
            findItem.setVisible(false);
        } else {
            findItem.setVisible(true);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        m17079();
        if (this.f13354 && this.f13342 != null) {
            this.f13342.m16149();
            this.f13342 = null;
        }
        if (this.f13365 != null && !this.f13365.isUnsubscribed()) {
            this.f13365.unsubscribe();
        }
        if (this.f13352 != null && !this.f13352.isUnsubscribed()) {
            this.f13352.unsubscribe();
        }
        if (this.f13347 != null && !this.f13347.isUnsubscribed()) {
            this.f13347.unsubscribe();
        }
        m17119();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            case R.id.action_next_episode:
                m17066();
                return true;
            case R.id.action_refresh:
                if (this.f13354) {
                    m17077();
                    return true;
                } else if (NetworkUtils.m17799()) {
                    m17075();
                    return true;
                } else {
                    m17122(I18N.m15706(R.string.no_internet));
                    return true;
                }
            case R.id.action_switch_player:
                if (this.isPlaying) {
                    return true;
                }
                m17429();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        if (this.f13344 != null) {
            this.mWatchedMediaSources = this.f13344.m17512();
        }
        if (this.f13354) {
            this.dontRestoreSourcesState = true;
        } else {
            this.dontRestoreSourcesState = false;
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.f13347 == null && !TyphoonApp.f5865) {
            this.f13347 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
                public void call(Object obj) {
                    if (obj instanceof OnInterstitialAdShownEvent) {
                        SourceActivity.this.mIsInterstitialShown = true;
                        SourceActivity.this.mAdShownTimes++;
                    }
                }
            });
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17121() {
        m17060();
        try {
            System.gc();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17122(String str) {
        m17123(str, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17123(String str, int i) {
        final Snackbar make = Snackbar.make(findViewById(R.id.source_rootLayout), (CharSequence) str, i);
        make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View view) {
                make.dismiss();
            }
        }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        make.show();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17124(final String str, final String str2, final String str3) {
        if (this.f13350 == null || !this.f13350.contains(str)) {
            if (this.f13350 != null) {
                this.f13350.add(str);
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    if (SourceActivity.this.f13355 == null || !SourceActivity.this.f13355.isShownOrQueued()) {
                        try {
                            if (SourceActivity.this.f13355 != null) {
                                SourceActivity.this.f13355.dismiss();
                            }
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        Snackbar unused = SourceActivity.this.f13355 = null;
                        View findViewById = SourceActivity.this.findViewById(R.id.source_rootLayout);
                        findViewById.setVisibility(0);
                        Snackbar unused2 = SourceActivity.this.f13355 = Snackbar.make(findViewById, (CharSequence) I18N.m15707(R.string.recaptcha_verify, str), 5000);
                        SourceActivity.this.f13355.setAction((CharSequence) I18N.m15706(R.string.verify), (View.OnClickListener) new View.OnClickListener() {
                            public void onClick(View view) {
                                SourceActivity.this.m17077();
                                if (SourceActivity.this.f13355 != null) {
                                    SourceActivity.this.f13355.dismiss();
                                }
                                Intent intent = new Intent(SourceActivity.this, RecaptchaWebViewActivity.class);
                                intent.putExtra("url", str2);
                                intent.putExtra("userAgent", str3);
                                SourceActivity.this.startActivityForResult(intent, 5);
                            }
                        }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
                        if (DeviceUtils.m6389(new boolean[0])) {
                            try {
                                SourceActivity.this.f13355.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                    /* renamed from: 龘  reason: contains not printable characters */
                                    public void onShown(Snackbar snackbar) {
                                        super.onShown(snackbar);
                                        try {
                                            View findViewById = SourceActivity.this.f13355.getView().findViewById(R.id.snackbar_action);
                                            if (findViewById != null) {
                                                findViewById.setFocusable(true);
                                                findViewById.setFocusableInTouchMode(true);
                                                findViewById.requestFocus();
                                            }
                                        } catch (Throwable th) {
                                            Logger.m6281(th, new boolean[0]);
                                        }
                                    }

                                    /* renamed from: 龘  reason: contains not printable characters */
                                    public void onDismissed(Snackbar snackbar, int i) {
                                        super.onDismissed(snackbar, i);
                                        try {
                                            View findViewById = SourceActivity.this.f13355.getView().findViewById(R.id.snackbar_action);
                                            if (findViewById != null) {
                                                findViewById.clearFocus();
                                            }
                                        } catch (Throwable th) {
                                            Logger.m6281(th, new boolean[0]);
                                        }
                                    }
                                });
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                            }
                        }
                        SourceActivity.this.f13355.show();
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17125(final List<MediaSource> list) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (SourceActivity.this.f13354 && list != null && !list.isEmpty()) {
                    boolean z = false;
                    for (MediaSource mediaSource : list) {
                        if (!SourceActivity.this.mMediaSources.contains(mediaSource)) {
                            SourceActivity.this.mMediaSources.add(mediaSource);
                            z = true;
                        }
                    }
                    if (z) {
                        try {
                            Collections.sort(SourceActivity.this.mMediaSources, SourceActivity.this.f13345);
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        try {
                            SourceActivity.this.f13344.notifyDataSetChanged();
                        } catch (Throwable th2) {
                            Logger.m6281(th2, new boolean[0]);
                        }
                    }
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m17126() {
        return this.mIsChildAppAdShown;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m17058(MediaSource selectedMediaSource) {
        String playLink = selectedMediaSource.getStreamLink();
        String fileExt = Utils.m6408(playLink);
        if (fileExt.isEmpty()) {
            fileExt = selectedMediaSource.isHLS() ? ".m3u8" : ".mp4";
        } else if (!fileExt.equalsIgnoreCase(".avi") && !fileExt.equalsIgnoreCase(".rmvb") && !fileExt.equalsIgnoreCase(".flv") && !fileExt.equalsIgnoreCase(".mkv") && !fileExt.equalsIgnoreCase(".mp2") && !fileExt.equalsIgnoreCase(".mp2v") && !fileExt.equalsIgnoreCase(".mp4") && !fileExt.equalsIgnoreCase(".mp4v") && !fileExt.equalsIgnoreCase(".mpe") && !fileExt.equalsIgnoreCase(".mpeg") && !fileExt.equalsIgnoreCase(".mpeg1") && !fileExt.equalsIgnoreCase(".mpeg2") && !fileExt.equalsIgnoreCase(".mpeg4") && !fileExt.equalsIgnoreCase(".mpg") && !fileExt.equalsIgnoreCase(".ts") && !fileExt.equalsIgnoreCase(".m3u8")) {
            fileExt = selectedMediaSource.isHLS() ? ".m3u8" : ".mp4";
        }
        Intent i = new Intent("android.intent.action.VIEW");
        if (Build.VERSION.SDK_INT >= 16) {
            i.setDataAndTypeAndNormalize(Uri.parse(playLink), "video/*");
        } else {
            i.setDataAndType(Uri.parse(playLink), "video/*");
        }
        i.putExtra(PubnativeAsset.TITLE, m17063() + fileExt);
        i.putExtra("suppress_error_message", false);
        i.putExtra("secure_uri", true);
        i.putExtra("fromOtherApp", true);
        i.putExtra("gdprScope", TyphoonApp.f5874);
        i.putExtra("showStartupADM", TyphoonApp.f5868);
        i.putExtra("showErrorMessage", true);
        i.putExtra("hls", selectedMediaSource.isHLS());
        i.putExtra("providerName", selectedMediaSource.getProviderName());
        i.putExtra("hostAppName", getPackageName());
        i.putExtra("showHostAppAd", m17420() && !BasePlayerHelper.m16037());
        i.putExtra("disableAds", TyphoonApp.f5865);
        i.putExtra("isChildAppAdShown", this.mIsChildAppAdShown);
        i.putExtra("forceATVMode", DeviceUtils.m6389(new boolean[0]));
        i.putExtra("subsFontScale", TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f));
        i.putExtra("subsFontColorHex", TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        i.putExtra("subsBgColorHex", TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        if (selectedMediaSource.getPlayHeader() != null && !selectedMediaSource.getPlayHeader().isEmpty()) {
            HashMap<String, String> headers = SourceUtils.m17835(selectedMediaSource.getPlayHeader());
            Bundle castHeadersBundle = new Bundle();
            List<String> headersList = new ArrayList<>();
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                castHeadersBundle.putString(entry.getKey(), entry.getValue());
                headersList.add(entry.getKey());
                headersList.add(entry.getValue());
            }
            i.putExtra("headers", (String[]) headersList.toArray(new String[headersList.size()]));
            i.putExtra("android.media.intent.extra.HTTP_HEADERS", castHeadersBundle);
            String cookie = null;
            if (headers.containsKey("Cookie")) {
                cookie = headers.get("Cookie");
            } else if (headers.containsKey("cookie")) {
                cookie = headers.get("cookie");
            }
            if (cookie != null) {
                i.putExtra("Cookies", cookie);
            }
        }
        Intent chooserIntent = IntentUtils.m17788(this, i, I18N.m15706(R.string.choose_external_download_manager));
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(chooserIntent, 76);
            m17078();
            return;
        }
        m17122(I18N.m15706(R.string.no_external_download_manager_was_found));
    }

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private void m17068() {
        final String prefKey = "is_yesplayer_promotional_dialog_shown_" + Utils.m6393();
        if (ExoPlayerHelper.m16055()) {
            boolean equalsIgnoreCase = BasePlayerHelper.m16032().m16041().equalsIgnoreCase("Yes");
            if (1 == 0 && !TVApplication.m6285().getBoolean(prefKey, false)) {
                try {
                    final MaterialDialog materialDialog = new MaterialDialog.Builder(this).m3797((int) R.drawable.no_ads_icon).m3800((CharSequence) "See LESS video ADs now!").m3787((CharSequence) "Use YesPlayer now and you will see 70% less video ADs! Its functionality is same as ExoPlayer. Click \"LESS ADS\" to switch video player to YesPlayer now!").m3793((CharSequence) "LESS ADS").m3784(17170456).m3803(true).m3790((CharSequence) "MORE ADS").m3792(17170432).m3799((MaterialDialog.SingleButtonCallback) new MaterialDialog.SingleButtonCallback() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m17137(MaterialDialog materialDialog, DialogAction dialogAction) {
                            if (materialDialog.m3762()) {
                                TVApplication.m6285().edit().putBoolean(prefKey, true).apply();
                            }
                            if (ExoPlayerHelper.m16055()) {
                                TVApplication.m6285().edit().putBoolean("choose_default_video_player_dialog_shown", true).apply();
                                TVApplication.m6285().edit().putString("pref_choose_default_player", "Yes").apply();
                                BasePlayerHelper unused = SourceActivity.this.f13721 = new YesPlayerHelper();
                            }
                        }
                    }).m3786((MaterialDialog.SingleButtonCallback) new MaterialDialog.SingleButtonCallback() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m17136(MaterialDialog materialDialog, DialogAction dialogAction) {
                            if (materialDialog.m3762()) {
                                TVApplication.m6285().edit().putBoolean(prefKey, true).apply();
                            }
                        }
                    }).m3801((CharSequence) I18N.m15706(R.string.dont_ask_again_checkbox), false, (CompoundButton.OnCheckedChangeListener) null).m3794(false).m3791(false).m3789();
                    materialDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        public void onShow(DialogInterface dialogInterface) {
                            try {
                                if (materialDialog != null) {
                                    MDButton r0 = materialDialog.m3776(DialogAction.POSITIVE);
                                    r0.setFocusable(true);
                                    r0.setFocusableInTouchMode(true);
                                    r0.requestFocus();
                                }
                            } catch (Throwable th) {
                                Logger.m6281(th, new boolean[0]);
                            }
                        }
                    });
                    materialDialog.show();
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        }
    }
}
