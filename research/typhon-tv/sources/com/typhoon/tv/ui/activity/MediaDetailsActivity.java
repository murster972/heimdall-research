package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconDrawable;
import com.thunderrise.animations.PulseAnimation;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TVDatabase;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.RetrievedImdbIdEvent;
import com.typhoon.tv.event.RetrievedTmdbMovieInfoEvent;
import com.typhoon.tv.event.RetrievedTmdbTvInfoEvent;
import com.typhoon.tv.event.ReverseSeasonListEvent;
import com.typhoon.tv.event.UpdateBookmarkEvent;
import com.typhoon.tv.font.fontawesome.FontAwesomeIcons47;
import com.typhoon.tv.helper.TapTargetViewHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.helper.trakt.TraktHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.TmdbVideosBean;
import com.typhoon.tv.model.media.movie.tmdb.TmdbMovieInfoResult;
import com.typhoon.tv.model.media.tv.TvLatestPlayed;
import com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.ViewPagerStateAdapter;
import com.typhoon.tv.ui.fragment.MediaDetailsFragment;
import com.typhoon.tv.ui.fragment.MediaSuggestionFragment;
import com.typhoon.tv.ui.fragment.SeasonListFragment;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import com.typhoon.tv.utils.YouTubeUtils;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.pubnative.library.request.PubnativeRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;
import rx.functions.Action1;

public class MediaDetailsActivity extends BaseAdActivity {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TabLayout f13157;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ImageView f13158;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ImageView f13159;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f13160 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f13161 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private MenuItem f13162;

    /* renamed from: ˑ  reason: contains not printable characters */
    private IconDrawable f13163;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public List<TapTargetView> f13164;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Subscription f13165;

    /* renamed from: 连任  reason: contains not printable characters */
    private ViewPager f13166;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f13167;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public MenuItem f13168;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f13169;

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaInfo f13170;

    private interface OnSetMovieAsWatched {
        /* renamed from: 靐  reason: contains not printable characters */
        void m16975(boolean z);

        /* renamed from: 龘  reason: contains not printable characters */
        void m16976(boolean z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m16947(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            this.f13159.setVisibility(0);
            if (Build.VERSION.SDK_INT >= 16) {
                this.f13159.setImageAlpha(30);
            } else {
                this.f13159.setAlpha(30);
            }
            Glide.m3938((FragmentActivity) this).m3974(imageUrl).m25191(DiskCacheStrategy.SOURCE).m25198().m25196().m25209(this.f13159);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m16948(String posterUrl) {
        if (posterUrl != null && !posterUrl.isEmpty()) {
            this.f13170.setPosterUrl(posterUrl);
            String posterUrl2 = this.f13170.getPosterUrl();
            if (this.f13159 != null && !DeviceUtils.m6389(new boolean[0])) {
                m16947(posterUrl2);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m16949() {
        m17423(findViewById(R.id.adViewMediaDetails));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m16951(String bannerUrl) {
        if (bannerUrl != null && !bannerUrl.isEmpty()) {
            this.f13170.setBannerUrl(bannerUrl);
            String bannerUrl2 = this.f13170.getBannerUrl();
            if (this.f13159 != null && DeviceUtils.m6389(new boolean[0])) {
                m16947(bannerUrl2);
            } else if (this.f13158 != null && !bannerUrl2.isEmpty()) {
                findViewById(R.id.coverContainerMediaDetails).setVisibility(0);
                Glide.m3938((FragmentActivity) this).m3974(bannerUrl2).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(-16777216)).m25196().m25198().m25209(this.f13158);
                this.f13158.setVisibility(0);
                findViewById(R.id.appBarLayoutMediaDetails).setBackgroundColor(0);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m16953() {
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("mediaInfo", this.f13170);
        startActivityForResult(intent, 1);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m16954(String imdbId) {
        this.f13170.setImdbId(imdbId);
        invalidateOptionsMenu();
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m16955(boolean isWatched) {
        if (isWatched) {
            TVApplication.m6287().m6301(Integer.valueOf(this.f13170.getTmdbId()), this.f13170.getImdbId());
        } else {
            TVApplication.m6287().m6311(Integer.valueOf(this.f13170.getTmdbId()), this.f13170.getImdbId());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16958() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMediaDetails);
        setSupportActionBar(toolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), toolbar);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarMediaDetails);
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(TVApplication.m6288(), 17170445));
        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(TVApplication.m6288(), R.color.text_color));
        Bundle b = getIntent().getExtras();
        if (b == null || !b.containsKey("mediaInfo") || b.getParcelable("mediaInfo") == null) {
            Toast.makeText(this, I18N.m15706(R.string.error), 0).show();
            finish();
            return;
        }
        this.f13170 = (MediaInfo) b.getParcelable("mediaInfo");
        this.f13167 = this.f13170.getType() == 1;
        setTitle(this.f13170.getNameAndYear());
        this.f13164 = new ArrayList();
        this.f13165 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                MediaDetailsActivity.this.m16963(obj);
            }
        });
        this.f13158 = (ImageView) findViewById(R.id.ivCoverMediaDetails);
        this.f13159 = (ImageView) findViewById(R.id.ivMediaDetailsBg);
        m16948(this.f13170.getPosterUrl());
        this.f13166 = (ViewPager) findViewById(R.id.viewpagerMediaDetails);
        this.f13166.setSaveEnabled(false);
        this.f13166.setOffscreenPageLimit(5);
        ViewPagerStateAdapter mAdapter = new ViewPagerStateAdapter(getSupportFragmentManager());
        mAdapter.m17534(MediaDetailsFragment.m17557(this.f13170, this.f13169), I18N.m15706(R.string.show_details_overview));
        if (!this.f13167) {
            mAdapter.m17534(SeasonListFragment.m17612(this.f13170), I18N.m15706(R.string.show_details_season));
        }
        mAdapter.m17534(MediaSuggestionFragment.m17573(this.f13170), I18N.m15706(R.string.media_suggestion));
        this.f13166.setAdapter(mAdapter);
        this.f13166.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                MediaDetailsActivity.this.invalidateOptionsMenu();
            }
        });
        this.f13157 = (TabLayout) findViewById(R.id.tabsMediaDetails);
        this.f13157.setupWithViewPager(this.f13166);
        this.f13157.setVisibility(0);
        this.f13166.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(this.f13157));
        this.f13157.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(this.f13166));
        if (this.f13157.getTabCount() > 0) {
            this.f13157.getTabAt(0).select();
        } else {
            this.f13157.setVisibility(8);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16959(int tvdbId) {
        this.f13170.setTvdbId(tvdbId);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16960(MenuItem item) {
        if (!this.f13167 && !this.f13161) {
            return;
        }
        if (!this.f13167 || this.f13160) {
            TVDatabase db = TVApplication.m6287();
            boolean isBookmarked = db.m6319(this.f13170);
            if (isBookmarked) {
                db.m6304(this.f13170);
                item.setIcon(R.drawable.ic_star_border_white_36dp);
                item.setTitle(I18N.m15706(R.string.action_remove_from_bookmark));
            } else {
                db.m6299(this.f13170);
                item.setIcon(R.drawable.ic_star_white_36dp);
                item.setTitle(I18N.m15706(R.string.action_add_to_bookmark));
            }
            invalidateOptionsMenu();
            RxBus.m15709().m15711(new UpdateBookmarkEvent());
            m17299(this.f13170, !isBookmarked, true, (Callback<SyncResponse>) null);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16963(Object obj) {
        if (obj instanceof RetrievedImdbIdEvent) {
            RetrievedImdbIdEvent event = (RetrievedImdbIdEvent) obj;
            if (event.getActivityId() == this.f13169) {
                m16954(event.getImdbId());
            }
        } else if (obj instanceof RetrievedTmdbMovieInfoEvent) {
            if (this.f13167) {
                if (this.f13162 != null) {
                    this.f13162.setVisible(true);
                }
                invalidateOptionsMenu();
                if (!DeviceUtils.m6389(new boolean[0])) {
                    final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMediaDetails);
                    fab.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            MediaDetailsActivity.this.m16953();
                        }
                    });
                    fab.setBackgroundColor(ContextCompat.getColor(TVApplication.m6288(), R.color.light_blue));
                    fab.setVisibility(0);
                    try {
                        PulseAnimation.m15698().m15703((View) fab).m15702(600).m15701(-1).m15699(2).m15700();
                    } catch (Throwable e) {
                        Logger.m6281(e, new boolean[0]);
                    }
                    if (!TapTargetViewHelper.m15965("ttv_media_details")) {
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                try {
                                    if (MediaDetailsActivity.this.f13164 == null) {
                                        List unused = MediaDetailsActivity.this.f13164 = new ArrayList();
                                    }
                                    MediaDetailsActivity.this.f13164.add(TapTargetView.showFor((Activity) MediaDetailsActivity.this, TapTarget.forView(fab, I18N.m15706(R.string.ttv_watch_now), I18N.m15706(R.string.ttv_watch_now_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(true)));
                                    TapTargetViewHelper.m15964("ttv_media_details");
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                }
                            }
                        }, 1000);
                    }
                } else {
                    ((LovelyInfoDialog) ((LovelyInfoDialog) ((LovelyInfoDialog) ((LovelyInfoDialog) new LovelyInfoDialog(this).m18218(R.color.blue)).m18220((int) R.drawable.ic_media_play_dark)).m18227(30).m18230(true).m18214((CharSequence) I18N.m15706(R.string.try_it))).m18221((CharSequence) I18N.m15706(R.string.click_play_button_on_your_rc))).m18228();
                }
                RetrievedTmdbMovieInfoEvent event2 = (RetrievedTmdbMovieInfoEvent) obj;
                if (event2.getActivityId() == this.f13169 && event2.getInfo() != null) {
                    TmdbMovieInfoResult info = event2.getInfo();
                    String imdbId = info.getImdb_id();
                    if (imdbId != null && imdbId.startsWith(TtmlNode.TAG_TT)) {
                        m16954(imdbId);
                    }
                    m16966(info.getVideos());
                    this.f13170.setOriginalName(info.getOriginal_title());
                    m16951(info.getBackdrop_path());
                    if ((this.f13170.getPosterUrl() == null || this.f13170.getPosterUrl().isEmpty()) && TVApplication.m6287().m6319(this.f13170)) {
                        m16948(info.getPoster_path());
                        TVApplication.m6287().m6309(this.f13170);
                        RxBus.m15709().m15711(new UpdateBookmarkEvent());
                    }
                }
                this.f13160 = true;
            }
        } else if ((obj instanceof RetrievedTmdbTvInfoEvent) && !this.f13167) {
            RetrievedTmdbTvInfoEvent event3 = (RetrievedTmdbTvInfoEvent) obj;
            TmdbTvInfoResult info2 = event3.getInfo();
            if (event3.getActivityId() == this.f13169 && info2 != null) {
                try {
                    if (info2.getExternal_ids() != null) {
                        String imdbId2 = info2.getExternal_ids().getImdb_id();
                        if (imdbId2 != null && imdbId2.startsWith(TtmlNode.TAG_TT)) {
                            m16954(imdbId2);
                        }
                        int tvdbId = info2.getExternal_ids().getTvdb_id();
                        if (tvdbId > -1) {
                            m16959(tvdbId);
                        }
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
                m16966(info2.getVideos());
                this.f13170.setOriginalName(info2.getOriginal_name());
                m16951(info2.getBackdrop_path());
                if ((this.f13170.getPosterUrl() == null || this.f13170.getPosterUrl().isEmpty()) && TVApplication.m6287().m6319(this.f13170)) {
                    m16948(info2.getPoster_path());
                    TVApplication.m6287().m6309(this.f13170);
                    RxBus.m15709().m15711(new UpdateBookmarkEvent());
                }
            }
            this.f13161 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16964(final boolean setIsWatched, final OnSetMovieAsWatched onSetMovieAsWatched) {
        if (!TraktCredentialsHelper.m16110().isValid() || !TVApplication.m6285().getBoolean("pref_auto_add_watched_episode_trakt", true)) {
            onSetMovieAsWatched.m16976(false);
            m16955(setIsWatched);
        } else if (!NetworkUtils.m17799()) {
            onSetMovieAsWatched.m16975(true);
        } else {
            try {
                if (this.f13168 != null) {
                    this.f13168.setEnabled(false);
                }
                final Snackbar snackbar = m16965("Sending to Trakt...", -2, false);
                m17290(this.f13170, setIsWatched, false, new Callback<SyncResponse>() {
                    public void onFailure(Call<SyncResponse> call, Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                        if (MediaDetailsActivity.this.f13168 != null) {
                            MediaDetailsActivity.this.f13168.setEnabled(true);
                        }
                        if (snackbar != null && snackbar.isShownOrQueued()) {
                            snackbar.dismiss();
                        }
                        TraktHelper.m16113();
                        onSetMovieAsWatched.m16975(true);
                    }

                    public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
                        if (MediaDetailsActivity.this.f13168 != null) {
                            MediaDetailsActivity.this.f13168.setEnabled(true);
                        }
                        if (snackbar != null && snackbar.isShownOrQueued()) {
                            snackbar.dismiss();
                        }
                        if (response.m24390()) {
                            MediaDetailsActivity.this.m16955(setIsWatched);
                            onSetMovieAsWatched.m16976(true);
                            return;
                        }
                        onSetMovieAsWatched.m16975(true);
                    }
                });
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                m16965("Failed to send to Trakt...", 0, true);
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (m17282()) {
            if (!(event.getAction() == 1 && (event.getKeyCode() == 126 || event.getKeyCode() == 85)) && (!event.isLongPress() || event.getKeyCode() != 23)) {
                if (((event.getAction() == 0 && (event.getKeyCode() == 4 || event.getKeyCode() == 23 || event.getKeyCode() == 66 || event.getKeyCode() == 0)) || event.isLongPress()) && this.f13164 != null && !this.f13164.isEmpty()) {
                    boolean dismissedAny = false;
                    try {
                        for (TapTargetView tapTargetView : this.f13164) {
                            if (tapTargetView != null) {
                                try {
                                    if (tapTargetView.isVisible()) {
                                        tapTargetView.dismiss(false);
                                        this.f13164.remove(tapTargetView);
                                        dismissedAny = true;
                                    }
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                }
                            }
                        }
                        if (dismissedAny) {
                            return true;
                        }
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                }
            } else if (!this.f13160) {
                return true;
            } else {
                m16953();
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (m17422(true) != false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r5, int r6, android.content.Intent r7) {
        /*
            r4 = this;
            r3 = 1
            r2 = 0
            super.onActivityResult(r5, r6, r7)
            r0 = -1
            if (r6 != r0) goto L_0x0069
            if (r5 != r3) goto L_0x0069
            if (r7 == 0) goto L_0x0069
            android.os.Bundle r0 = r7.getExtras()
            if (r0 == 0) goto L_0x0069
            boolean r0 = com.typhoon.tv.TyphoonApp.f5865
            if (r0 != 0) goto L_0x0043
            android.os.Bundle r0 = r7.getExtras()
            java.lang.String r1 = "isInterstitialShown"
            boolean r0 = r0.getBoolean(r1, r2)
            if (r0 != 0) goto L_0x0043
            boolean r0 = com.typhoon.tv.TyphoonApp.m6333()
            if (r0 != 0) goto L_0x0040
            android.os.Bundle r0 = r7.getExtras()
            java.lang.String r1 = "hasLink"
            boolean r0 = r0.getBoolean(r1, r2)
            if (r0 == 0) goto L_0x0043
            boolean[] r0 = new boolean[r3]
            r0[r2] = r3
            boolean r0 = r4.m17422((boolean[]) r0)
            if (r0 == 0) goto L_0x0043
        L_0x0040:
            r4.m17421()
        L_0x0043:
            android.os.Bundle r0 = r7.getExtras()
            java.lang.String r1 = "isWatchedAnyLink"
            boolean r0 = r0.getBoolean(r1, r2)
            if (r0 == 0) goto L_0x0069
            boolean r0 = r4.f13167
            if (r0 == 0) goto L_0x0069
            android.os.Bundle r0 = r7.getExtras()
            java.lang.String r1 = "needMarkAsWatched"
            boolean r0 = r0.getBoolean(r1, r2)
            if (r0 == 0) goto L_0x0069
            com.typhoon.tv.ui.activity.MediaDetailsActivity$4 r0 = new com.typhoon.tv.ui.activity.MediaDetailsActivity$4
            r0.<init>()
            r4.m16964((boolean) r3, (com.typhoon.tv.ui.activity.MediaDetailsActivity.OnSetMovieAsWatched) r0)
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.MediaDetailsActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (IllegalStateException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_media_details);
        m17284();
        if (savedInstanceState == null || !savedInstanceState.containsKey("activityId")) {
            this.f13169 = new Random().nextInt(9999);
        } else {
            this.f13169 = savedInstanceState.getInt("activityId", 0);
        }
        m16958();
        if (!TyphoonApp.f5865) {
            m16949();
        }
        if (!NetworkUtils.m17799()) {
            m16967(I18N.m15706(R.string.no_internet));
            if (this.f13157.getTabCount() > 0) {
                this.f13157.getTabAt(0).select();
            }
        }
        TvLatestPlayed latestPlayedRecord = null;
        if (!this.f13167) {
            latestPlayedRecord = TVApplication.m6287().m6296(Integer.valueOf(this.f13170.getTmdbId()));
        }
        if (this.f13157.getTabCount() > 2 && !this.f13167 && (TVApplication.m6287().m6320(Integer.valueOf(this.f13170.getType()), Integer.valueOf(this.f13170.getTmdbId())) || latestPlayedRecord != null)) {
            this.f13157.getTabAt(1).select();
        }
        m17419();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_media_details, menu);
        MenuItem bookmarkItem = menu.findItem(R.id.action_bookmark);
        if (TVApplication.m6287().m6319(this.f13170)) {
            bookmarkItem.setIcon(R.drawable.ic_star_white_36dp);
            bookmarkItem.setTitle(I18N.m15706(R.string.action_remove_from_bookmark));
        } else {
            bookmarkItem.setIcon(R.drawable.ic_star_border_white_36dp);
            bookmarkItem.setTitle(I18N.m15706(R.string.action_add_to_bookmark));
        }
        this.f13168 = menu.findItem(R.id.action_set_watched);
        if (this.f13167) {
            if (TVApplication.m6287().m6322(Integer.valueOf(this.f13170.getTmdbId()), this.f13170.getImdbId())) {
                this.f13168.setIcon(R.drawable.ic_check_box_white_24dp);
                this.f13168.setTitle(I18N.m15706(R.string.action_remove_watched));
            }
            this.f13168.setVisible(true);
        } else {
            this.f13168.setVisible(false);
        }
        MenuItem rItem = menu.findItem(R.id.action_reverse);
        if (this.f13167 || this.f13166.getCurrentItem() != 1) {
            rItem.setVisible(false);
        } else {
            rItem.setVisible(true);
        }
        MenuItem miImdb = menu.findItem(R.id.action_imdb);
        String imdbId = this.f13170.getImdbId();
        if (imdbId == null || imdbId.isEmpty()) {
            miImdb.setVisible(false);
        } else {
            if (this.f13163 == null) {
                this.f13163 = new IconDrawable((Context) this, (Icon) FontAwesomeIcons47.fa_imdb).sizeDp(36).colorRes(17170443);
            }
            miImdb.setIcon(this.f13163);
            miImdb.setVisible(true);
        }
        this.f13162 = menu.findItem(R.id.action_play);
        if (this.f13160) {
            this.f13162.setVisible(true);
            if (DeviceUtils.m6389(new boolean[0]) && !TapTargetViewHelper.m15965("ttv_media_details")) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        try {
                            if (MediaDetailsActivity.this.f13164 == null) {
                                List unused = MediaDetailsActivity.this.f13164 = new ArrayList();
                            }
                            MediaDetailsActivity.this.f13164.add(TapTargetView.showFor((Activity) MediaDetailsActivity.this, TapTarget.forToolbarMenuItem((Toolbar) MediaDetailsActivity.this.findViewById(R.id.toolbarMediaDetails), (int) R.id.action_play, (CharSequence) I18N.m15706(R.string.ttv_watch_now), (CharSequence) I18N.m15706(R.string.ttv_watch_now_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).drawShadow(true).tintTarget(true).cancelable(true)));
                            TapTargetViewHelper.m15964("ttv_media_details");
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                }, 1000);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13166 != null) {
            this.f13166.clearOnPageChangeListeners();
        }
        if (this.f13165 != null && !this.f13165.isUnsubscribed()) {
            this.f13165.unsubscribe();
        }
        this.f13165 = null;
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        int i = 0;
        switch (item.getItemId()) {
            case R.id.action_bookmark:
                m16960(item);
                return true;
            case R.id.action_imdb:
                try {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.parse("https://database.gdriveplayer.us/player.php?imdb=" + this.f13170.getImdbId() + ""), "video/*");
                    intent.setFlags(268435456);
                    startActivity(intent);
                    return true;
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    m16967(I18N.m15706(R.string.error));
                    return true;
                }
            case R.id.action_play:
                m16953();
                return true;
            case R.id.action_reverse:
                RxBus.m15709().m15711(new ReverseSeasonListEvent());
                SharedPreferences.Editor edit = TVApplication.m6285().edit();
                if (TVApplication.m6285().getInt("pref_season_reverse_order", 0) == 0) {
                    i = 1;
                }
                edit.putInt("pref_season_reverse_order", i).apply();
                return true;
            case R.id.action_set_watched:
                if (!this.f13167 && !this.f13161) {
                    return true;
                }
                if (this.f13167 && !this.f13160) {
                    return true;
                }
                if (TVApplication.m6287().m6322(Integer.valueOf(this.f13170.getTmdbId()), this.f13170.getImdbId())) {
                    m16964(false, (OnSetMovieAsWatched) new OnSetMovieAsWatched() {
                        /* renamed from: 靐  reason: contains not printable characters */
                        public void m16969(boolean z) {
                            if (!z) {
                                return;
                            }
                            if (!NetworkUtils.m17799()) {
                                MediaDetailsActivity.this.m16965(I18N.m15706(R.string.no_internet), 0, true);
                            } else {
                                MediaDetailsActivity.this.m16965("Failed to send to Trakt...", 0, true);
                            }
                        }

                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m16970(boolean z) {
                            item.setIcon(R.drawable.ic_check_box_outline_blank_white_24dp);
                            item.setTitle(I18N.m15706(R.string.action_set_watched));
                            MediaDetailsActivity.this.invalidateOptionsMenu();
                            if (z) {
                                MediaDetailsActivity.this.m16965("Sent to Trakt successfully!", 0, true);
                            }
                        }
                    });
                    return true;
                }
                m16964(true, (OnSetMovieAsWatched) new OnSetMovieAsWatched() {
                    /* renamed from: 靐  reason: contains not printable characters */
                    public void m16971(boolean z) {
                        if (!z) {
                            return;
                        }
                        if (!NetworkUtils.m17799()) {
                            MediaDetailsActivity.this.m16965(I18N.m15706(R.string.no_internet), 0, true);
                        } else {
                            MediaDetailsActivity.this.m16965("Failed to send to Trakt...", 0, true);
                        }
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m16972(boolean z) {
                        item.setIcon(R.drawable.ic_check_box_white_24dp);
                        item.setTitle(I18N.m15706(R.string.action_remove_watched));
                        MediaDetailsActivity.this.invalidateOptionsMenu();
                        if (z) {
                            MediaDetailsActivity.this.m16965("Sent to Trakt successfully!", 0, true);
                        }
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("activityId", this.f13169);
        super.onSaveInstanceState(outState);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Snackbar m16965(String msg, int length, boolean showCloseAction) {
        final Snackbar snackbar = Snackbar.make(findViewById(R.id.media_details_rootLayout), (CharSequence) msg, length);
        if (showCloseAction) {
            snackbar.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
        }
        snackbar.setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        snackbar.show();
        return snackbar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16966(TmdbVideosBean videosBean) {
        if (videosBean != null) {
            String ytId = "";
            try {
                Iterator<TmdbVideosBean.ResultsBean> it2 = videosBean.getResults().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    TmdbVideosBean.ResultsBean resultBean = it2.next();
                    try {
                        if (resultBean.getSite().trim().equalsIgnoreCase("YouTube") && resultBean.getType().trim().equalsIgnoreCase("Trailer") && resultBean.getIso_639_1().trim().equalsIgnoreCase("en")) {
                            if ((this.f13167 || resultBean.getName() == null || !resultBean.getName().toLowerCase().contains("season") || resultBean.getName().toLowerCase().contains(PubnativeRequest.LEGACY_ZONE_ID)) && resultBean.getKey() != null && !resultBean.getKey().isEmpty()) {
                                ytId = resultBean.getKey();
                                break;
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
            if (!ytId.isEmpty()) {
                final String youtubeId = ytId;
                View.OnClickListener onPlayTrailerClickListener = new View.OnClickListener() {
                    public void onClick(View view) {
                        YouTubeUtils.m17840(MediaDetailsActivity.this, youtubeId);
                    }
                };
                if (!DeviceUtils.m6389(new boolean[0])) {
                    findViewById(R.id.coverOverlay).setVisibility(0);
                    findViewById(R.id.trailerContainer).setVisibility(0);
                    IconDrawable youtubeIconDrawable = new IconDrawable((Context) this, (Icon) FontAwesomeIcons47.fa_youtube_play).sizeDp(36).colorRes(17170443);
                    ImageView ivPlayTrailer = (ImageView) findViewById(R.id.ivPlayTrailer);
                    ivPlayTrailer.setImageDrawable(youtubeIconDrawable);
                    ivPlayTrailer.setClickable(true);
                    TextView tvPlayTrailer = (TextView) findViewById(R.id.tvPlayTrailer);
                    tvPlayTrailer.setClickable(true);
                    ivPlayTrailer.setOnClickListener(onPlayTrailerClickListener);
                    tvPlayTrailer.setOnClickListener(onPlayTrailerClickListener);
                    return;
                }
                Fragment fragment = ((ViewPagerStateAdapter) this.f13166.getAdapter()).getItem(0);
                if (fragment instanceof MediaDetailsFragment) {
                    MediaDetailsFragment mediaDetailsFragment = (MediaDetailsFragment) fragment;
                    mediaDetailsFragment.m17568(youtubeId);
                    if (mediaDetailsFragment.m17560() && !mediaDetailsFragment.m17562()) {
                        mediaDetailsFragment.m17563();
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16967(String msg) {
        m16968(msg, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16968(String msg, int length) {
        final Snackbar snackbar = Snackbar.make(findViewById(R.id.media_details_rootLayout), (CharSequence) msg, length);
        snackbar.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View view) {
                snackbar.dismiss();
            }
        }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        snackbar.show();
    }
}
