package com.typhoon.tv.ui.activity.exoplayer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;
import com.Ty.subtitle.converter.subtitleFile.FormatSRT;
import com.evernote.android.state.State;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.TTVWorkaroundUtils;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.ext.ffmpeg.FfmpegLibrary;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.AdaptiveMediaSourceEventListener;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.livefront.bridge.Bridge;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.exoplayer.ExoEventLogger;
import com.typhoon.tv.exoplayer.TTVCustomTrackSelector;
import com.typhoon.tv.exoplayer.TTVLoadControl;
import com.typhoon.tv.exoplayer.TTVRenderersFactory;
import com.typhoon.tv.exoplayer.TrackSelectionHelper;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.subtitles.SubtitlesConverter;
import com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView;
import com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.LocaleUtils;
import com.typhoon.tv.utils.SourceUtils;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import okhttp3.CacheControl;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;
import org.apache.commons.lang3.StringUtils;
import rx.Subscription;

public class ExoPlayerActivity extends AppCompatActivity {
    @State
    public int mAdaptiveSourceErrCount = 0;
    @State
    public int mAudioTrackInitializationErrCount = 0;
    @State
    public int mCurrentExtensionRendererMode;
    @State
    public int mCurrentSubIndex = -1;
    @State
    public int mDecoderInitializationErrCount = 0;
    @State
    public long mDuration = -1;
    @State
    public int mExtractorSourceErrCount = 0;
    @State
    public int mExtractorSourceHttpErrCount = 0;
    @State
    public boolean mForceExtractorSource = false;
    @State
    public boolean mForceHttpUrlConnection = false;
    @State
    public boolean mForceLowestResHLS = false;
    @State
    public boolean mForceNoPlayerCache = false;
    @State
    public boolean mForceSoftwareAudioDecoder = false;
    @State
    public boolean mForceSoftwareVideoDecoder = false;
    @State
    public boolean mIsExitCalled = false;
    @State
    public boolean mIsLoaded = false;
    @State
    public long mResumePosition;
    @State
    public int mResumeWindow;
    @State
    public int mRuntimeErrCount = 0;
    @State
    public int mSourceErrCount = 0;
    @State
    public int mTrackErrCount = 0;

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f13730;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public long f13731;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public ArrayList<String> f13732;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public SimpleExoPlayer f13733;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final OnUserSeekListener f13734 = new OnUserSeekListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17483() {
            ExoPlayerActivity.this.m17446();
        }
    };

    /* renamed from: ˆ  reason: contains not printable characters */
    private Subscription f13735;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public TTVSimpleExoPlayerView f13736;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f13737;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public TrackGroupArray f13738;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f13739;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public ArrayList<String> f13740;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Handler f13741;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public ExoEventLogger f13742;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public List<MediaSource> f13743;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ExtractorMediaSource.EventListener f13744 = new CustomExtractorMediaSourceEventListener();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public com.typhoon.tv.model.media.MediaSource f13745;

    /* renamed from: 齉  reason: contains not printable characters */
    private final AdaptiveMediaSourceEventListener f13746 = new CustomAdaptiveMediaSourceEventListener();

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f13747 = false;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public DefaultTrackSelector f13748;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private TrackSelectionHelper f13749;

    private class CustomAdaptiveMediaSourceEventListener implements AdaptiveMediaSourceEventListener {
        private CustomAdaptiveMediaSourceEventListener() {
        }

        public void onDownstreamFormatChanged(int i, Format format, int i2, Object obj, long j) {
            if (ExoPlayerActivity.this.f13742 != null) {
                ExoPlayerActivity.this.f13742.onDownstreamFormatChanged(i, format, i2, obj, j);
            }
        }

        public void onLoadCanceled(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5) {
            if (ExoPlayerActivity.this.f13742 != null) {
                ExoPlayerActivity.this.f13742.onLoadCanceled(dataSpec, i, i2, format, i3, obj, j, j2, j3, j4, j5);
            }
        }

        public void onLoadCompleted(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5) {
            if (ExoPlayerActivity.this.f13742 != null) {
                ExoPlayerActivity.this.f13742.onLoadCompleted(dataSpec, i, i2, format, i3, obj, j, j2, j3, j4, j5);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
            if ((r42 instanceof com.google.android.exoplayer2.source.UnrecognizedInputFormatException) != false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0043, code lost:
            r24 = false;
            r4 = r25.f13764;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0049, code lost:
            monitor-enter(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            r2 = r25.f13764.mAdaptiveSourceErrCount;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
            monitor-exit(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0053, code lost:
            if (r2 < 30) goto L_0x009c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0055, code lost:
            r4 = new java.lang.StringBuilder().append("Media error: ");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0065, code lost:
            if (r42.getMessage() == null) goto L_0x0094;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0067, code lost:
            r3 = r42.getMessage();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
            r23 = r4.append(r3).toString();
            com.typhoon.tv.Logger.m6280(r42, r23, true);
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17474(r25.f13764, r23, new boolean[0]);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0094, code lost:
            r3 = com.typhoon.tv.I18N.m15706(com.typhoon.tv.R.string.error);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x009e, code lost:
            if (r2 <= 10) goto L_0x00c3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a2, code lost:
            if (r2 >= 30) goto L_0x00c3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a4, code lost:
            r4 = r25.f13764;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a8, code lost:
            monitor-enter(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            r22 = r25.f13764.mForceLowestResHLS;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b1, code lost:
            monitor-exit(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b2, code lost:
            if (r22 != false) goto L_0x00c3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b4, code lost:
            r4 = r25.f13764;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
            monitor-enter(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
            r25.f13764.mForceLowestResHLS = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c0, code lost:
            monitor-exit(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c1, code lost:
            r24 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c3, code lost:
            r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x00c5, code lost:
            monitor-enter(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
            r25.f13764.mAdaptiveSourceErrCount++;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d0, code lost:
            monitor-exit(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x00d1, code lost:
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17463(r25.f13764, true);
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17476(r25.f13764, true, r24);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onLoadError(com.google.android.exoplayer2.upstream.DataSpec r26, int r27, int r28, com.google.android.exoplayer2.Format r29, int r30, java.lang.Object r31, long r32, long r34, long r36, long r38, long r40, java.io.IOException r42, boolean r43) {
            /*
                r25 = this;
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.typhoon.tv.exoplayer.ExoEventLogger r3 = r3.f13742
                if (r3 == 0) goto L_0x002f
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.typhoon.tv.exoplayer.ExoEventLogger r3 = r3.f13742
                r4 = r26
                r5 = r27
                r6 = r28
                r7 = r29
                r8 = r30
                r9 = r31
                r10 = r32
                r12 = r34
                r14 = r36
                r16 = r38
                r18 = r40
                r20 = r42
                r21 = r43
                r3.onLoadError(r4, r5, r6, r7, r8, r9, r10, r12, r14, r16, r18, r20, r21)
            L_0x002f:
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r4)
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x008e }
                boolean r3 = r3.mIsExitCalled     // Catch:{ all -> 0x008e }
                if (r3 == 0) goto L_0x003c
                monitor-exit(r4)     // Catch:{ all -> 0x008e }
            L_0x003b:
                return
            L_0x003c:
                monitor-exit(r4)     // Catch:{ all -> 0x008e }
                r0 = r42
                boolean r3 = r0 instanceof com.google.android.exoplayer2.source.UnrecognizedInputFormatException
                if (r3 != 0) goto L_0x003b
                r24 = 0
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r4)
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0091 }
                int r2 = r3.mAdaptiveSourceErrCount     // Catch:{ all -> 0x0091 }
                monitor-exit(r4)     // Catch:{ all -> 0x0091 }
                r3 = 30
                if (r2 < r3) goto L_0x009c
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Media error: "
                java.lang.StringBuilder r4 = r3.append(r4)
                java.lang.String r3 = r42.getMessage()
                if (r3 == 0) goto L_0x0094
                java.lang.String r3 = r42.getMessage()
            L_0x006b:
                java.lang.StringBuilder r3 = r4.append(r3)
                java.lang.String r23 = r3.toString()
                r3 = 1
                boolean[] r3 = new boolean[r3]
                r4 = 0
                r5 = 1
                r3[r4] = r5
                r0 = r42
                r1 = r23
                com.typhoon.tv.Logger.m6280(r0, r1, r3)
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r4 = 0
                boolean[] r4 = new boolean[r4]
                r0 = r23
                r3.m17478((java.lang.String) r0, (boolean[]) r4)
                goto L_0x003b
            L_0x008e:
                r3 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x008e }
                throw r3
            L_0x0091:
                r3 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0091 }
                throw r3
            L_0x0094:
                r3 = 2131820746(0x7f1100ca, float:1.9274216E38)
                java.lang.String r3 = com.typhoon.tv.I18N.m15706(r3)
                goto L_0x006b
            L_0x009c:
                r3 = 10
                if (r2 <= r3) goto L_0x00c3
                r3 = 30
                if (r2 >= r3) goto L_0x00c3
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r4)
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x00e5 }
                boolean r0 = r3.mForceLowestResHLS     // Catch:{ all -> 0x00e5 }
                r22 = r0
                monitor-exit(r4)     // Catch:{ all -> 0x00e5 }
                if (r22 != 0) goto L_0x00c3
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r4)
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x00e8 }
                r5 = 1
                r3.mForceLowestResHLS = r5     // Catch:{ all -> 0x00e8 }
                monitor-exit(r4)     // Catch:{ all -> 0x00e8 }
                r24 = 1
            L_0x00c3:
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r4)
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x00eb }
                int r5 = r3.mAdaptiveSourceErrCount     // Catch:{ all -> 0x00eb }
                int r5 = r5 + 1
                r3.mAdaptiveSourceErrCount = r5     // Catch:{ all -> 0x00eb }
                monitor-exit(r4)     // Catch:{ all -> 0x00eb }
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r4 = 1
                boolean unused = r3.f13737 = r4
                r0 = r25
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r4 = 1
                r0 = r24
                r3.m17480((boolean) r4, (boolean) r0)
                goto L_0x003b
            L_0x00e5:
                r3 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x00e5 }
                throw r3
            L_0x00e8:
                r3 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x00e8 }
                throw r3
            L_0x00eb:
                r3 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x00eb }
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.CustomAdaptiveMediaSourceEventListener.onLoadError(com.google.android.exoplayer2.upstream.DataSpec, int, int, com.google.android.exoplayer2.Format, int, java.lang.Object, long, long, long, long, long, java.io.IOException, boolean):void");
        }

        public void onLoadStarted(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3) {
            if (ExoPlayerActivity.this.f13742 != null) {
                ExoPlayerActivity.this.f13742.onLoadStarted(dataSpec, i, i2, format, i3, obj, j, j2, j3);
            }
        }

        public void onUpstreamDiscarded(int i, long j, long j2) {
            if (ExoPlayerActivity.this.f13742 != null) {
                ExoPlayerActivity.this.f13742.onUpstreamDiscarded(i, j, j2);
            }
        }
    }

    public static class CustomErrorDialogFragment extends DialogFragment {
        public Dialog onCreateDialog(Bundle bundle) {
            final FragmentActivity activity = getActivity();
            if (!(activity == null)) {
                return new AlertDialog.Builder(activity).m536((CharSequence) I18N.m15706(R.string.exit)).m523((CharSequence) I18N.m15706(R.string.exit_confirm)).m520((int) R.drawable.ic_error_white_36dp).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!activity.isFinishing()) {
                            try {
                                if (activity instanceof ExoPlayerActivity) {
                                    ExoPlayerActivity exoPlayerActivity = (ExoPlayerActivity) activity;
                                    exoPlayerActivity.m17482();
                                    exoPlayerActivity.m17441();
                                } else {
                                    activity.finish();
                                }
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, true);
                            }
                        }
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m525();
            }
            throw new IllegalStateException("Activity is already destroyed");
        }
    }

    private class CustomExtractorMediaSourceEventListener implements ExtractorMediaSource.EventListener {
        private CustomExtractorMediaSourceEventListener() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:111:0x015e, code lost:
            if (r10 != false) goto L_0x0143;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x0160, code lost:
            if (r9 == false) goto L_0x0143;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x0164, code lost:
            if (r2 <= 10) goto L_0x0143;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:115:0x0166, code lost:
            r13 = r18.f13768;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:0x016a, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
            r5 = r18.f13768.mForceNoPlayerCache;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x0171, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:121:0x0172, code lost:
            if (r5 != false) goto L_0x0181;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x0174, code lost:
            r13 = r18.f13768;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x0178, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:?, code lost:
            r18.f13768.mForceNoPlayerCache = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x0180, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x0181, code lost:
            r7 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
            r18.f13768.mExtractorSourceErrCount++;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:160:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0067, code lost:
            r13 = r18.f13768;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
            r2 = r18.f13768.mExtractorSourceErrCount;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0072, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0073, code lost:
            r13 = r18.f13768;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0077, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
            r3 = r18.f13768.mExtractorSourceHttpErrCount;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x007e, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x007f, code lost:
            r9 = r19 instanceof com.google.android.exoplayer2.upstream.DataSourceException;
            r10 = r19 instanceof com.google.android.exoplayer2.upstream.HttpDataSource.HttpDataSourceException;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x008b, code lost:
            if ((r19 instanceof com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException) == false) goto L_0x00cd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x008d, code lost:
            r11 = ((com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException) r19).responseCode;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0095, code lost:
            if (r11 != 429) goto L_0x00cf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0097, code lost:
            r8 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0098, code lost:
            if (r10 != false) goto L_0x00d1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x009a, code lost:
            r7 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x009b, code lost:
            if (r8 == false) goto L_0x00d3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a5, code lost:
            if (com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17449(r18.f13768) == null) goto L_0x00b5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b3, code lost:
            if (com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17449(r18.f13768).getPlayWhenReady() == false) goto L_0x00b5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b5, code lost:
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17463(r18.f13768, true);
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17476(r18.f13768, true, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x00cd, code lost:
            r11 = -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x00cf, code lost:
            r8 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d1, code lost:
            r7 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x00d3, code lost:
            if (r10 == false) goto L_0x00d9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00d7, code lost:
            if (r3 >= 30) goto L_0x00df;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x00d9, code lost:
            if (r10 != false) goto L_0x011e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x00dd, code lost:
            if (r2 < 30) goto L_0x011e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x00df, code lost:
            r6 = r19.getMessage();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x00e3, code lost:
            if (r6 == null) goto L_0x00eb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x00e9, code lost:
            if (r6.isEmpty() == false) goto L_0x00ef;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x00eb, code lost:
            r6 = r19.toString();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f0, code lost:
            if (r11 <= -1) goto L_0x0103;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x00f2, code lost:
            r6 = com.typhoon.tv.I18N.m15707(com.typhoon.tv.R.string.exo_player_error_http, java.lang.String.valueOf(r11));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x0103, code lost:
            com.typhoon.tv.Logger.m6280(r19, r6, true);
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17474(r18.f13768, r6, new boolean[]{r10});
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x011e, code lost:
            if (r10 == false) goto L_0x015e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x0121, code lost:
            if (r11 != -1) goto L_0x015e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x0125, code lost:
            if (r3 <= 15) goto L_0x015e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x0127, code lost:
            r13 = r18.f13768;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x012b, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
            r4 = r18.f13768.mForceHttpUrlConnection;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x0132, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x0133, code lost:
            if (r4 != false) goto L_0x0142;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x0135, code lost:
            r13 = r18.f13768;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x0139, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
            r18.f13768.mForceHttpUrlConnection = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0141, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x0142, code lost:
            r7 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0143, code lost:
            r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0145, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0146, code lost:
            if (r10 == false) goto L_0x0189;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
            r18.f13768.mExtractorSourceHttpErrCount++;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x0152, code lost:
            monitor-exit(r13);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onLoadError(java.io.IOException r19) {
            /*
                r18 = this;
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r12 = r12.f13733
                if (r12 != 0) goto L_0x000b
            L_0x000a:
                return
            L_0x000b:
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.typhoon.tv.exoplayer.ExoEventLogger r12 = r12.f13742
                if (r12 == 0) goto L_0x0022
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.typhoon.tv.exoplayer.ExoEventLogger r12 = r12.f13742
                r0 = r19
                r12.onLoadError(r0)
            L_0x0022:
                r12 = 0
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r14 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r14 = r14.f13733
                long r14 = r14.getCurrentPosition()
                long r12 = java.lang.Math.max(r12, r14)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r14 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                long r14 = r14.mResumePosition
                int r12 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
                if (r12 <= 0) goto L_0x0056
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r14 = 0
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r13 = r13.f13733
                long r16 = r13.getCurrentPosition()
                long r14 = java.lang.Math.max(r14, r16)
                r12.mResumePosition = r14
            L_0x0056:
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0063 }
                boolean r12 = r12.mIsExitCalled     // Catch:{ all -> 0x0063 }
                if (r12 == 0) goto L_0x0066
                monitor-exit(r13)     // Catch:{ all -> 0x0063 }
                goto L_0x000a
            L_0x0063:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x0063 }
                throw r12
            L_0x0066:
                monitor-exit(r13)     // Catch:{ all -> 0x0063 }
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x00c7 }
                int r2 = r12.mExtractorSourceErrCount     // Catch:{ all -> 0x00c7 }
                monitor-exit(r13)     // Catch:{ all -> 0x00c7 }
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x00ca }
                int r3 = r12.mExtractorSourceHttpErrCount     // Catch:{ all -> 0x00ca }
                monitor-exit(r13)     // Catch:{ all -> 0x00ca }
                r0 = r19
                boolean r9 = r0 instanceof com.google.android.exoplayer2.upstream.DataSourceException
                r0 = r19
                boolean r10 = r0 instanceof com.google.android.exoplayer2.upstream.HttpDataSource.HttpDataSourceException
                r0 = r19
                boolean r12 = r0 instanceof com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException
                if (r12 == 0) goto L_0x00cd
                r12 = r19
                com.google.android.exoplayer2.upstream.HttpDataSource$InvalidResponseCodeException r12 = (com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException) r12
                int r11 = r12.responseCode
            L_0x0093:
                r12 = 429(0x1ad, float:6.01E-43)
                if (r11 != r12) goto L_0x00cf
                r8 = 1
            L_0x0098:
                if (r10 != 0) goto L_0x00d1
                r7 = 1
            L_0x009b:
                if (r8 == 0) goto L_0x00d3
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r12 = r12.f13733
                if (r12 == 0) goto L_0x00b5
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r12 = r12.f13733
                boolean r12 = r12.getPlayWhenReady()
                if (r12 == 0) goto L_0x00b5
            L_0x00b5:
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r13 = 1
                boolean unused = r12.f13737 = r13
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r13 = 1
                r12.m17480((boolean) r13, (boolean) r7)
                goto L_0x000a
            L_0x00c7:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x00c7 }
                throw r12
            L_0x00ca:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x00ca }
                throw r12
            L_0x00cd:
                r11 = -1
                goto L_0x0093
            L_0x00cf:
                r8 = 0
                goto L_0x0098
            L_0x00d1:
                r7 = 0
                goto L_0x009b
            L_0x00d3:
                if (r10 == 0) goto L_0x00d9
                r12 = 30
                if (r3 >= r12) goto L_0x00df
            L_0x00d9:
                if (r10 != 0) goto L_0x011e
                r12 = 30
                if (r2 < r12) goto L_0x011e
            L_0x00df:
                java.lang.String r6 = r19.getMessage()
                if (r6 == 0) goto L_0x00eb
                boolean r12 = r6.isEmpty()
                if (r12 == 0) goto L_0x00ef
            L_0x00eb:
                java.lang.String r6 = r19.toString()
            L_0x00ef:
                r12 = -1
                if (r11 <= r12) goto L_0x0103
                r12 = 2131820767(0x7f1100df, float:1.9274258E38)
                r13 = 1
                java.lang.Object[] r13 = new java.lang.Object[r13]
                r14 = 0
                java.lang.String r15 = java.lang.String.valueOf(r11)
                r13[r14] = r15
                java.lang.String r6 = com.typhoon.tv.I18N.m15707(r12, r13)
            L_0x0103:
                r12 = 1
                boolean[] r12 = new boolean[r12]
                r13 = 0
                r14 = 1
                r12[r13] = r14
                r0 = r19
                com.typhoon.tv.Logger.m6280(r0, r6, r12)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r13 = 1
                boolean[] r13 = new boolean[r13]
                r14 = 0
                r13[r14] = r10
                r12.m17478((java.lang.String) r6, (boolean[]) r13)
                goto L_0x000a
            L_0x011e:
                if (r10 == 0) goto L_0x015e
                r12 = -1
                if (r11 != r12) goto L_0x015e
                r12 = 15
                if (r3 <= r12) goto L_0x015e
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0158 }
                boolean r4 = r12.mForceHttpUrlConnection     // Catch:{ all -> 0x0158 }
                monitor-exit(r13)     // Catch:{ all -> 0x0158 }
                if (r4 != 0) goto L_0x0142
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x015b }
                r14 = 1
                r12.mForceHttpUrlConnection = r14     // Catch:{ all -> 0x015b }
                monitor-exit(r13)     // Catch:{ all -> 0x015b }
            L_0x0142:
                r7 = 1
            L_0x0143:
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r13)
                if (r10 == 0) goto L_0x0189
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0155 }
                int r14 = r12.mExtractorSourceHttpErrCount     // Catch:{ all -> 0x0155 }
                int r14 = r14 + 1
                r12.mExtractorSourceHttpErrCount = r14     // Catch:{ all -> 0x0155 }
            L_0x0152:
                monitor-exit(r13)     // Catch:{ all -> 0x0155 }
                goto L_0x00b5
            L_0x0155:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x0155 }
                throw r12
            L_0x0158:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x0158 }
                throw r12
            L_0x015b:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x015b }
                throw r12
            L_0x015e:
                if (r10 != 0) goto L_0x0143
                if (r9 == 0) goto L_0x0143
                r12 = 10
                if (r2 <= r12) goto L_0x0143
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0183 }
                boolean r5 = r12.mForceNoPlayerCache     // Catch:{ all -> 0x0183 }
                monitor-exit(r13)     // Catch:{ all -> 0x0183 }
                if (r5 != 0) goto L_0x0181
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r13 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                monitor-enter(r13)
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0186 }
                r14 = 1
                r12.mForceNoPlayerCache = r14     // Catch:{ all -> 0x0186 }
                monitor-exit(r13)     // Catch:{ all -> 0x0186 }
            L_0x0181:
                r7 = 1
                goto L_0x0143
            L_0x0183:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x0183 }
                throw r12
            L_0x0186:
                r12 = move-exception
                monitor-exit(r13)     // Catch:{ all -> 0x0186 }
                throw r12
            L_0x0189:
                r0 = r18
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r12 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0155 }
                int r14 = r12.mExtractorSourceErrCount     // Catch:{ all -> 0x0155 }
                int r14 = r14 + 1
                r12.mExtractorSourceErrCount = r14     // Catch:{ all -> 0x0155 }
                goto L_0x0152
            */
            throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.CustomExtractorMediaSourceEventListener.onLoadError(java.io.IOException):void");
        }
    }

    private class CustomPlayerEventListener implements Player.EventListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f13769;

        public CustomPlayerEventListener(boolean z) {
            this.f13769 = z;
        }

        public void onLoadingChanged(boolean z) {
            if (ExoPlayerActivity.this.f13733 != null && z && Math.max(0, ExoPlayerActivity.this.f13733.getCurrentPosition()) > 0) {
                ExoPlayerActivity.this.m17446();
            }
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }

        /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
            java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
            	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
            	at java.util.ArrayList.get(ArrayList.java:435)
            	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processLoop(RegionMaker.java:225)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:106)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processHandlersOutBlocks(RegionMaker.java:1008)
            	at jadx.core.dex.visitors.regions.RegionMaker.processTryCatchBlocks(RegionMaker.java:978)
            	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
            */
        public void onPlayerError(com.google.android.exoplayer2.ExoPlaybackException r39) {
            /*
                r38 = this;
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                com.google.android.exoplayer2.SimpleExoPlayer r33 = r33.f13733
                if (r33 != 0) goto L_0x000d
            L_0x000c:
                return
            L_0x000d:
                r34 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                com.google.android.exoplayer2.SimpleExoPlayer r33 = r33.f13733
                long r36 = r33.getCurrentPosition()
                long r34 = java.lang.Math.max(r34, r36)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r0 = r33
                long r0 = r0.mResumePosition
                r36 = r0
                int r33 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1))
                if (r33 <= 0) goto L_0x0051
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r34 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r36 = r0
                com.google.android.exoplayer2.SimpleExoPlayer r36 = r36.f13733
                long r36 = r36.getCurrentPosition()
                long r34 = java.lang.Math.max(r34, r36)
                r0 = r34
                r2 = r33
                r2.mResumePosition = r0
            L_0x0051:
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r34 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r34)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0064 }
                r33 = r0
                r0 = r33
                boolean r0 = r0.mIsExitCalled     // Catch:{ all -> 0x0064 }
                r33 = r0
                if (r33 == 0) goto L_0x0067
                monitor-exit(r34)     // Catch:{ all -> 0x0064 }
                goto L_0x000c
            L_0x0064:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x0064 }
                throw r33
            L_0x0067:
                monitor-exit(r34)     // Catch:{ all -> 0x0064 }
                java.util.ArrayList r14 = new java.util.ArrayList
                r14.<init>()
                java.lang.Exception r33 = r39.getRendererException()     // Catch:{ Throwable -> 0x076d }
                r0 = r33
                r14.add(r0)     // Catch:{ Throwable -> 0x076d }
            L_0x0076:
                java.io.IOException r33 = r39.getSourceException()     // Catch:{ Throwable -> 0x076a }
                r0 = r33
                r14.add(r0)     // Catch:{ Throwable -> 0x076a }
            L_0x007f:
                java.lang.RuntimeException r33 = r39.getUnexpectedException()     // Catch:{ Throwable -> 0x0767 }
                r0 = r33
                r14.add(r0)     // Catch:{ Throwable -> 0x0767 }
            L_0x0088:
                r17 = 0
                r20 = 0
                r24 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r34 = r0
                monitor-enter(r34)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0127 }
                r33 = r0
                r0 = r33
                int r6 = r0.mDecoderInitializationErrCount     // Catch:{ all -> 0x0127 }
                monitor-exit(r34)     // Catch:{ all -> 0x0127 }
                r22 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r34 = r0
                monitor-enter(r34)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x012a }
                r33 = r0
                r0 = r33
                int r5 = r0.mAudioTrackInitializationErrCount     // Catch:{ all -> 0x012a }
                monitor-exit(r34)     // Catch:{ all -> 0x012a }
                r27 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r34 = r0
                monitor-enter(r34)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x012d }
                r33 = r0
                r0 = r33
                int r11 = r0.mSourceErrCount     // Catch:{ all -> 0x012d }
                monitor-exit(r34)     // Catch:{ all -> 0x012d }
                r21 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r34 = r0
                monitor-enter(r34)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0130 }
                r33 = r0
                r0 = r33
                int r4 = r0.mAdaptiveSourceErrCount     // Catch:{ all -> 0x0130 }
                monitor-exit(r34)     // Catch:{ all -> 0x0130 }
                r26 = 0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r34 = r0
                monitor-enter(r34)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0133 }
                r33 = r0
                r0 = r33
                int r10 = r0.mRuntimeErrCount     // Catch:{ all -> 0x0133 }
                monitor-exit(r34)     // Catch:{ all -> 0x0133 }
                r13 = 0
                java.util.Iterator r34 = r14.iterator()
            L_0x00f5:
                boolean r33 = r34.hasNext()
                if (r33 == 0) goto L_0x0615
                java.lang.Object r12 = r34.next()
                java.lang.Throwable r12 = (java.lang.Throwable) r12
                boolean r0 = r12 instanceof com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 == 0) goto L_0x0299
                r24 = 1
                r0 = r12
                com.google.android.exoplayer2.mediacodec.MediaCodecRenderer$DecoderInitializationException r0 = (com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException) r0     // Catch:{ Throwable -> 0x0255 }
                r15 = r0
                java.lang.String r0 = r15.decoderName     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 != 0) goto L_0x0174
                java.lang.Throwable r33 = r15.getCause()     // Catch:{ Throwable -> 0x0255 }
                r0 = r33
                boolean r0 = r0 instanceof com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 == 0) goto L_0x0136
                r33 = 2131820776(0x7f1100e8, float:1.9274276E38)
                java.lang.String r17 = com.typhoon.tv.I18N.m15706(r33)     // Catch:{ Throwable -> 0x0255 }
                goto L_0x00f5
            L_0x0127:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x0127 }
                throw r33
            L_0x012a:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x012a }
                throw r33
            L_0x012d:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x012d }
                throw r33
            L_0x0130:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x0130 }
                throw r33
            L_0x0133:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x0133 }
                throw r33
            L_0x0136:
                boolean r0 = r15.secureDecoderRequired     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 == 0) goto L_0x0158
                r33 = 2131820770(0x7f1100e2, float:1.9274264E38)
                r35 = 1
                r0 = r35
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                r36 = 0
                java.lang.String r0 = r15.mimeType     // Catch:{ Throwable -> 0x0255 }
                r37 = r0
                r35[r36] = r37     // Catch:{ Throwable -> 0x0255 }
                r0 = r33
                r1 = r35
                java.lang.String r17 = com.typhoon.tv.I18N.m15707(r0, r1)     // Catch:{ Throwable -> 0x0255 }
                goto L_0x00f5
            L_0x0158:
                r33 = 2131820769(0x7f1100e1, float:1.9274262E38)
                r35 = 1
                r0 = r35
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                r36 = 0
                java.lang.String r0 = r15.mimeType     // Catch:{ Throwable -> 0x0255 }
                r37 = r0
                r35[r36] = r37     // Catch:{ Throwable -> 0x0255 }
                r0 = r33
                r1 = r35
                java.lang.String r17 = com.typhoon.tv.I18N.m15707(r0, r1)     // Catch:{ Throwable -> 0x0255 }
                goto L_0x00f5
            L_0x0174:
                java.lang.String r0 = r15.decoderName     // Catch:{ Throwable -> 0x0255 }
                r16 = r0
                java.lang.String r33 = r16.trim()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = " "
                java.lang.String r36 = ""
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = r0.replace(r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r28 = r33.toLowerCase()     // Catch:{ Throwable -> 0x0255 }
                r33 = 2131820774(0x7f1100e6, float:1.9274272E38)
                r35 = 1
                r0 = r35
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                r36 = 0
                java.lang.String r0 = r15.decoderName     // Catch:{ Throwable -> 0x0255 }
                r37 = r0
                r35[r36] = r37     // Catch:{ Throwable -> 0x0255 }
                r0 = r33
                r1 = r35
                java.lang.String r17 = com.typhoon.tv.I18N.m15707(r0, r1)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r33 = "aml.google.ac3.decoder"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "aml.google.ec3.decoder"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "omx.ffmpeg.ac3.decoder"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "omx.sec.ac3.dec"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "omx.sec.aac.dec"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "omx.google.aac.decoder"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "omx.ms.ac3.decoder"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x0213
                java.lang.String r33 = "omx.rtk.audio.decoder"
                r0 = r28
                r1 = r33
                boolean r33 = r0.equalsIgnoreCase(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x024f
            L_0x0213:
                r25 = 1
            L_0x0215:
                r33 = 10
                r0 = r33
                if (r6 <= r0) goto L_0x00f5
                r33 = 50
                r0 = r33
                if (r6 >= r0) goto L_0x00f5
                if (r25 == 0) goto L_0x0270
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0252 }
                r33 = r0
                r0 = r33
                boolean r8 = r0.mForceSoftwareAudioDecoder     // Catch:{ all -> 0x0252 }
                monitor-exit(r35)     // Catch:{ all -> 0x0252 }
                if (r8 != 0) goto L_0x024b
                boolean r33 = com.google.android.exoplayer2.ext.ffmpeg.FfmpegLibrary.isAvailable()     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x024b
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x026d }
                r33 = r0
                r36 = 1
                r0 = r36
                r1 = r33
                r1.mForceSoftwareAudioDecoder = r0     // Catch:{ all -> 0x026d }
                monitor-exit(r35)     // Catch:{ all -> 0x026d }
            L_0x024b:
                r20 = 1
                goto L_0x00f5
            L_0x024f:
                r25 = 0
                goto L_0x0215
            L_0x0252:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x0252 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x0255:
                r31 = move-exception
                r33 = 1
                r0 = r33
                boolean[] r0 = new boolean[r0]
                r33 = r0
                r35 = 0
                r36 = 1
                r33[r35] = r36
                r0 = r31
                r1 = r33
                com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
                goto L_0x00f5
            L_0x026d:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x026d }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x0270:
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0296 }
                r33 = r0
                r0 = r33
                boolean r9 = r0.mForceSoftwareVideoDecoder     // Catch:{ all -> 0x0296 }
                monitor-exit(r35)     // Catch:{ all -> 0x0296 }
                if (r9 != 0) goto L_0x024b
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0293 }
                r33 = r0
                r36 = 1
                r0 = r36
                r1 = r33
                r1.mForceSoftwareVideoDecoder = r0     // Catch:{ all -> 0x0293 }
                monitor-exit(r35)     // Catch:{ all -> 0x0293 }
                goto L_0x024b
            L_0x0293:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x0293 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x0296:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x0296 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x0299:
                boolean r0 = r12 instanceof com.google.android.exoplayer2.audio.AudioTrack.InitializationException     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 == 0) goto L_0x02e2
                r22 = 1
                java.lang.String r17 = "Could not initialize audio track..."
                r33 = 10
                r0 = r33
                if (r5 <= r0) goto L_0x00f5
                r33 = 50
                r0 = r33
                if (r5 >= r0) goto L_0x00f5
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x02dc }
                r33 = r0
                r0 = r33
                boolean r8 = r0.mForceSoftwareAudioDecoder     // Catch:{ all -> 0x02dc }
                monitor-exit(r35)     // Catch:{ all -> 0x02dc }
                if (r8 != 0) goto L_0x02d8
                boolean r33 = com.google.android.exoplayer2.ext.ffmpeg.FfmpegLibrary.isAvailable()     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x02d8
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x02df }
                r33 = r0
                r36 = 1
                r0 = r36
                r1 = r33
                r1.mForceSoftwareAudioDecoder = r0     // Catch:{ all -> 0x02df }
                monitor-exit(r35)     // Catch:{ all -> 0x02df }
            L_0x02d8:
                r20 = 1
                goto L_0x00f5
            L_0x02dc:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x02dc }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x02df:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x02df }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x02e2:
                boolean r0 = r12 instanceof com.google.android.exoplayer2.source.UnrecognizedInputFormatException     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 == 0) goto L_0x03cd
                java.lang.String r33 = r12.getMessage()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r33 = r33.trim()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = " "
                java.lang.String r36 = ""
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = r0.replace(r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r33 = r33.toLowerCase()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = "#extm3u"
                r0 = r33
                r1 = r35
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x0384
                r21 = 1
                java.lang.String r17 = "Input does not start with the #EXTM3U header. MediaSource should not be a HLS stream."
                r33 = 10
                r0 = r33
                if (r4 <= r0) goto L_0x00f5
                r33 = 30
                r0 = r33
                if (r4 >= r0) goto L_0x00f5
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x037e }
                r33 = r0
                r0 = r33
                boolean r7 = r0.mForceExtractorSource     // Catch:{ all -> 0x037e }
                monitor-exit(r35)     // Catch:{ all -> 0x037e }
                if (r7 != 0) goto L_0x00f5
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0381 }
                r33 = r0
                r36 = 1
                r0 = r36
                r1 = r33
                r1.mForceExtractorSource = r0     // Catch:{ all -> 0x0381 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0381 }
                r33 = r0
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0381 }
                r36 = r0
                com.typhoon.tv.model.media.MediaSource r36 = r36.f13745     // Catch:{ all -> 0x0381 }
                com.typhoon.tv.model.media.MediaSource r36 = r36.cloneDeeply()     // Catch:{ all -> 0x0381 }
                r0 = r33
                r1 = r36
                com.typhoon.tv.model.media.MediaSource unused = r0.f13745 = r1     // Catch:{ all -> 0x0381 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0381 }
                r33 = r0
                com.typhoon.tv.model.media.MediaSource r33 = r33.f13745     // Catch:{ all -> 0x0381 }
                r36 = 0
                r0 = r33
                r1 = r36
                r0.setHLS(r1)     // Catch:{ all -> 0x0381 }
                monitor-exit(r35)     // Catch:{ all -> 0x0381 }
                r20 = 1
                goto L_0x00f5
            L_0x037e:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x037e }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x0381:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x0381 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x0384:
                r27 = 1
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                com.typhoon.tv.model.media.MediaSource r33 = r33.f13745     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r33 = r33.getStreamLink()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = "\\.(\\w{3,4})(?:$|\\?)"
                r36 = 1
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = com.typhoon.tv.utils.Regex.m17800(r0, r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r32 = r33.toUpperCase()     // Catch:{ Throwable -> 0x0255 }
                boolean r33 = r32.isEmpty()     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x03b4
                r33 = 2131821122(0x7f110242, float:1.9274978E38)
                java.lang.String r32 = com.typhoon.tv.I18N.m15706(r33)     // Catch:{ Throwable -> 0x0255 }
            L_0x03b4:
                java.lang.String r33 = "Video format (%s) is not supported by ExoPlayer"
                r35 = 1
                r0 = r35
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                r36 = 0
                r35[r36] = r32     // Catch:{ Throwable -> 0x0255 }
                r0 = r33
                r1 = r35
                java.lang.String r17 = java.lang.String.format(r0, r1)     // Catch:{ Throwable -> 0x0255 }
                goto L_0x00f5
            L_0x03cd:
                boolean r0 = r12 instanceof java.lang.IllegalStateException     // Catch:{ Throwable -> 0x0255 }
                r33 = r0
                if (r33 == 0) goto L_0x060d
                r26 = 1
                r19 = 0
                r18 = 0
                java.lang.String r30 = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(r12)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r33 = r30.trim()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = " "
                java.lang.String r36 = ""
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = r0.replace(r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = "\r"
                java.lang.String r36 = ""
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = r0.replace(r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = "\n"
                java.lang.String r36 = ""
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = r0.replace(r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r35 = "\t"
                java.lang.String r36 = ""
                r0 = r33
                r1 = r35
                r2 = r36
                java.lang.String r33 = r0.replace(r1, r2)     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r29 = r33.toLowerCase()     // Catch:{ Throwable -> 0x0255 }
                java.lang.String r33 = "simpledecoderaudiorenderer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x04cf
                java.lang.String r33 = "drainoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x04cf
                java.lang.String r33 = "assertions"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x04cf
                java.lang.String r33 = "checkstate"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x04cf
                r18 = 1
                java.lang.String r17 = "ExoPlayer failed to decode the audio."
            L_0x045e:
                r33 = 15
                r0 = r33
                if (r10 <= r0) goto L_0x05eb
                r33 = 35
                r0 = r33
                if (r10 > r0) goto L_0x05eb
                r33 = 50
                r0 = r33
                if (r10 >= r0) goto L_0x05eb
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                com.google.android.exoplayer2.TTVWorkaroundUtils.disableAllWorkaround()     // Catch:{ all -> 0x05dc }
                monitor-exit(r35)     // Catch:{ all -> 0x05dc }
                if (r19 == 0) goto L_0x04a1
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x05df }
                r33 = r0
                r0 = r33
                boolean r9 = r0.mForceSoftwareVideoDecoder     // Catch:{ all -> 0x05df }
                monitor-exit(r35)     // Catch:{ all -> 0x05df }
                if (r9 != 0) goto L_0x049f
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x05e2 }
                r33 = r0
                r36 = 1
                r0 = r36
                r1 = r33
                r1.mForceSoftwareVideoDecoder = r0     // Catch:{ all -> 0x05e2 }
                monitor-exit(r35)     // Catch:{ all -> 0x05e2 }
            L_0x049f:
                r20 = 1
            L_0x04a1:
                if (r18 == 0) goto L_0x00f5
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x05e5 }
                r33 = r0
                r0 = r33
                boolean r8 = r0.mForceSoftwareAudioDecoder     // Catch:{ all -> 0x05e5 }
                monitor-exit(r35)     // Catch:{ all -> 0x05e5 }
                if (r8 != 0) goto L_0x04cb
                boolean r33 = com.google.android.exoplayer2.ext.ffmpeg.FfmpegLibrary.isAvailable()     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x04cb
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r35 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x05e8 }
                r33 = r0
                r36 = 1
                r0 = r36
                r1 = r33
                r1.mForceSoftwareAudioDecoder = r0     // Catch:{ all -> 0x05e8 }
                monitor-exit(r35)     // Catch:{ all -> 0x05e8 }
            L_0x04cb:
                r20 = 1
                goto L_0x00f5
            L_0x04cf:
                java.lang.String r33 = ".configure"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_configure"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".setup"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_setup"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".start"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_start"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".queueinputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_queueinputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".dequeueinputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_dequeueinputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".queueoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_queueoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".dequeueoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_dequeueoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".feedinputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_feedinputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".setsurface"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_setsurface"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".releaseoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 != 0) goto L_0x05d3
                java.lang.String r33 = ".native_releaseoutputbuffer"
                r0 = r29
                r1 = r33
                boolean r33 = r0.contains(r1)     // Catch:{ Throwable -> 0x0255 }
                if (r33 == 0) goto L_0x045e
            L_0x05d3:
                r19 = 1
                r18 = 1
                java.lang.String r17 = "ExoPlayer failed to decode the video."
                goto L_0x045e
            L_0x05dc:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x05dc }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x05df:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x05df }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x05e2:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x05e2 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x05e5:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x05e5 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x05e8:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x05e8 }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x05eb:
                r33 = 35
                r0 = r33
                if (r10 <= r0) goto L_0x00f5
                r33 = 50
                r0 = r33
                if (r10 >= r0) goto L_0x00f5
                if (r18 != 0) goto L_0x05fb
                if (r19 == 0) goto L_0x00f5
            L_0x05fb:
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ Throwable -> 0x0255 }
                r35 = r0
                monitor-enter(r35)     // Catch:{ Throwable -> 0x0255 }
                com.google.android.exoplayer2.TTVWorkaroundUtils.enableAllWorkaround()     // Catch:{ all -> 0x060a }
                monitor-exit(r35)     // Catch:{ all -> 0x060a }
                r20 = 1
                goto L_0x00f5
            L_0x060a:
                r33 = move-exception
                monitor-exit(r35)     // Catch:{ all -> 0x060a }
                throw r33     // Catch:{ Throwable -> 0x0255 }
            L_0x060d:
                if (r13 != 0) goto L_0x00f5
                java.lang.String r13 = r12.getMessage()     // Catch:{ Throwable -> 0x0255 }
                goto L_0x00f5
            L_0x0615:
                if (r17 == 0) goto L_0x061d
                boolean r33 = r17.isEmpty()
                if (r33 == 0) goto L_0x0627
            L_0x061d:
                if (r13 == 0) goto L_0x069c
                boolean r33 = r13.isEmpty()
                if (r33 != 0) goto L_0x069c
                r17 = r13
            L_0x0627:
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r34 = 1
                boolean unused = r33.f13737 = r34
                boolean r23 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17456((com.google.android.exoplayer2.ExoPlaybackException) r39)
                if (r23 == 0) goto L_0x06a1
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r33.m17447()
            L_0x0641:
                r33 = 50
                r0 = r33
                if (r6 >= r0) goto L_0x065f
                r33 = 50
                r0 = r33
                if (r5 >= r0) goto L_0x065f
                r33 = 50
                r0 = r33
                if (r11 >= r0) goto L_0x065f
                r33 = 30
                r0 = r33
                if (r4 >= r0) goto L_0x065f
                r33 = 50
                r0 = r33
                if (r10 < r0) goto L_0x06ab
            L_0x065f:
                r33 = 1
                r0 = r33
                boolean[] r0 = new boolean[r0]
                r33 = r0
                r34 = 0
                r35 = 1
                r33[r34] = r35
                r0 = r39
                r1 = r17
                r2 = r33
                com.typhoon.tv.Logger.m6280(r0, r1, r2)
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r34 = 2131820766(0x7f1100de, float:1.9274256E38)
                r35 = 1
                r0 = r35
                java.lang.Object[] r0 = new java.lang.Object[r0]
                r35 = r0
                r36 = 0
                r35[r36] = r17
                java.lang.String r34 = com.typhoon.tv.I18N.m15707(r34, r35)
                r35 = 0
                r0 = r35
                boolean[] r0 = new boolean[r0]
                r35 = r0
                r33.m17478((java.lang.String) r34, (boolean[]) r35)
                goto L_0x000c
            L_0x069c:
                java.lang.String r17 = r39.getMessage()
                goto L_0x0627
            L_0x06a1:
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r33.m17446()
                goto L_0x0641
            L_0x06ab:
                r33 = 0
                r0 = r33
                boolean[] r0 = new boolean[r0]
                r33 = r0
                r0 = r39
                r1 = r17
                r2 = r33
                com.typhoon.tv.Logger.m6280(r0, r1, r2)
                if (r23 != 0) goto L_0x0730
                java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r34 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
                monitor-enter(r34)
                if (r24 == 0) goto L_0x06d7
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0764 }
                r33 = r0
                r0 = r33
                int r0 = r0.mDecoderInitializationErrCount     // Catch:{ all -> 0x0764 }
                r35 = r0
                int r35 = r35 + 1
                r0 = r35
                r1 = r33
                r1.mDecoderInitializationErrCount = r0     // Catch:{ all -> 0x0764 }
            L_0x06d7:
                if (r22 == 0) goto L_0x06ed
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0764 }
                r33 = r0
                r0 = r33
                int r0 = r0.mAudioTrackInitializationErrCount     // Catch:{ all -> 0x0764 }
                r35 = r0
                int r35 = r35 + 1
                r0 = r35
                r1 = r33
                r1.mAudioTrackInitializationErrCount = r0     // Catch:{ all -> 0x0764 }
            L_0x06ed:
                if (r27 == 0) goto L_0x0703
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0764 }
                r33 = r0
                r0 = r33
                int r0 = r0.mSourceErrCount     // Catch:{ all -> 0x0764 }
                r35 = r0
                int r35 = r35 + 1
                r0 = r35
                r1 = r33
                r1.mSourceErrCount = r0     // Catch:{ all -> 0x0764 }
            L_0x0703:
                if (r21 == 0) goto L_0x0719
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0764 }
                r33 = r0
                r0 = r33
                int r0 = r0.mAdaptiveSourceErrCount     // Catch:{ all -> 0x0764 }
                r35 = r0
                int r35 = r35 + 1
                r0 = r35
                r1 = r33
                r1.mAdaptiveSourceErrCount = r0     // Catch:{ all -> 0x0764 }
            L_0x0719:
                if (r26 == 0) goto L_0x072f
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this     // Catch:{ all -> 0x0764 }
                r33 = r0
                r0 = r33
                int r0 = r0.mRuntimeErrCount     // Catch:{ all -> 0x0764 }
                r35 = r0
                int r35 = r35 + 1
                r0 = r35
                r1 = r33
                r1.mRuntimeErrCount = r0     // Catch:{ all -> 0x0764 }
            L_0x072f:
                monitor-exit(r34)     // Catch:{ all -> 0x0764 }
            L_0x0730:
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView r33 = r33.f13736
                if (r33 == 0) goto L_0x0751
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView r33 = r33.f13736
                r34 = 0
                r0 = r34
                boolean[] r0 = new boolean[r0]
                r34 = r0
                r33.m17774((boolean[]) r34)
            L_0x0751:
                r0 = r38
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r0 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r33 = r0
                r34 = 1
                r0 = r33
                r1 = r34
                r2 = r20
                r0.m17480((boolean) r1, (boolean) r2)
                goto L_0x000c
            L_0x0764:
                r33 = move-exception
                monitor-exit(r34)     // Catch:{ all -> 0x0764 }
                throw r33
            L_0x0767:
                r33 = move-exception
                goto L_0x0088
            L_0x076a:
                r33 = move-exception
                goto L_0x007f
            L_0x076d:
                r33 = move-exception
                goto L_0x0076
            */
            throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.CustomPlayerEventListener.onPlayerError(com.google.android.exoplayer2.ExoPlaybackException):void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0096, code lost:
            r1 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17436(r10.f13770).getCurrentMappedTrackInfo();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPlayerStateChanged(boolean r11, int r12) {
            /*
                r10 = this;
                r6 = 0
                r9 = 1
                r8 = 3
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                if (r4 == 0) goto L_0x0034
                if (r12 == r8) goto L_0x001d
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                boolean r4 = r4.getPlayWhenReady()
                if (r4 == 0) goto L_0x0034
                r4 = 2
                if (r12 != r4) goto L_0x0034
            L_0x001d:
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                long r4 = r4.getCurrentPosition()
                long r4 = java.lang.Math.max(r6, r4)
                int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r4 <= 0) goto L_0x0034
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r4.m17446()
            L_0x0034:
                r4 = 4
                if (r12 != r4) goto L_0x003d
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r4.m17441()
            L_0x003c:
                return
            L_0x003d:
                if (r12 != r8) goto L_0x003c
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                if (r4 == 0) goto L_0x003c
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r5 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r5 = r5.f13733
                long r6 = r5.getDuration()
                r4.mDuration = r6
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                boolean r4 = r4.mIsLoaded
                if (r4 != 0) goto L_0x007e
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                long r4 = r4.f13731
                r6 = -1
                int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r4 <= 0) goto L_0x007e
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                if (r4 == 0) goto L_0x007e
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r5 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                long r6 = r5.f13731
                r4.seekTo(r6)
            L_0x007e:
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.typhoon.tv.ui.widget.exoplayer.TTVSimpleExoPlayerView r4 = r4.f13736
                com.typhoon.tv.ui.widget.exoplayer.TTVPlaybackControlView r0 = r4.getController()
                if (r0 == 0) goto L_0x00b6
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.trackselection.DefaultTrackSelector r4 = r4.f13748
                com.google.android.exoplayer2.trackselection.MappingTrackSelector$MappedTrackInfo r4 = r4.getCurrentMappedTrackInfo()
                if (r4 == 0) goto L_0x00b6
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.trackselection.DefaultTrackSelector r4 = r4.f13748
                com.google.android.exoplayer2.trackselection.MappingTrackSelector$MappedTrackInfo r1 = r4.getCurrentMappedTrackInfo()
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                com.google.android.exoplayer2.SimpleExoPlayer r4 = r4.f13733
                int r3 = com.typhoon.tv.exoplayer.TrackSelectionHelper.m15899((com.google.android.exoplayer2.Player) r4, (com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo) r1, (int) r8)
                r4 = -1
                if (r3 <= r4) goto L_0x00b6
                int r2 = com.typhoon.tv.exoplayer.TrackSelectionHelper.m15900((com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo) r1, (int) r3)
                if (r2 <= 0) goto L_0x00bb
                r0.setCcButtonEnabled(r9)
            L_0x00b6:
                com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity r4 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.this
                r4.mIsLoaded = r9
                goto L_0x003c
            L_0x00bb:
                r4 = 0
                r0.setCcButtonEnabled(r4)
                goto L_0x00b6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.CustomPlayerEventListener.onPlayerStateChanged(boolean, int):void");
        }

        public void onPositionDiscontinuity() {
            if (ExoPlayerActivity.this.f13737) {
                ExoPlayerActivity.this.m17446();
            }
        }

        public void onRepeatModeChanged(int i) {
        }

        public void onTimelineChanged(Timeline timeline, Object obj) {
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            int i;
            boolean z;
            boolean z2;
            boolean z3 = false;
            boolean z4 = true;
            String str = null;
            if (trackGroupArray != ExoPlayerActivity.this.f13738) {
                MappingTrackSelector.MappedTrackInfo currentMappedTrackInfo = ExoPlayerActivity.this.f13748.getCurrentMappedTrackInfo();
                if (currentMappedTrackInfo != null) {
                    if (currentMappedTrackInfo.getTrackTypeRendererSupport(2) == 1) {
                        str = I18N.m15706(R.string.exo_player_error_none_video_tracks_are_playable);
                        synchronized (ExoPlayerActivity.class) {
                            z2 = ExoPlayerActivity.this.mForceSoftwareVideoDecoder;
                        }
                        if (!z2) {
                            synchronized (ExoPlayerActivity.class) {
                                ExoPlayerActivity.this.mForceSoftwareVideoDecoder = true;
                            }
                        }
                        z4 = false;
                        z3 = true;
                    } else if (currentMappedTrackInfo.getTrackTypeRendererSupport(1) == 1) {
                        str = I18N.m15706(R.string.exo_player_error_none_audio_tracks_are_playable);
                        synchronized (ExoPlayerActivity.class) {
                            z = ExoPlayerActivity.this.mForceSoftwareAudioDecoder;
                        }
                        if (!z && FfmpegLibrary.isAvailable()) {
                            synchronized (ExoPlayerActivity.class) {
                                ExoPlayerActivity.this.mForceSoftwareAudioDecoder = true;
                            }
                        }
                        z3 = true;
                        z4 = false;
                    } else if (currentMappedTrackInfo.getTrackTypeRendererSupport(3) == 1) {
                        str = I18N.m15706(R.string.exo_player_error_unable_to_render_subs);
                        z3 = false;
                        z4 = true;
                    }
                    if (str != null && z4) {
                        Toast.makeText(TVApplication.m6288(), str, 1).show();
                    }
                }
                TrackGroupArray unused = ExoPlayerActivity.this.f13738 = trackGroupArray;
            }
            if (z3) {
                synchronized (ExoPlayerActivity.this) {
                    i = ExoPlayerActivity.this.mTrackErrCount;
                }
                if (i >= 50) {
                    ExoPlayerActivity.this.m17478(str, new boolean[0]);
                    return;
                }
                synchronized (ExoPlayerActivity.class) {
                    ExoPlayerActivity.this.mTrackErrCount++;
                }
                boolean unused2 = ExoPlayerActivity.this.f13737 = true;
                ExoPlayerActivity.this.m17480(true, true);
            }
        }
    }

    private interface OnMediaSourceBuiltListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17490(MediaSource mediaSource);
    }

    public interface OnUserSeekListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17491();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m17435() {
        Intent intent = getIntent();
        this.f13745 = (com.typhoon.tv.model.media.MediaSource) intent.getParcelableExtra("mediaSource");
        if (this.f13745 == null) {
        }
        this.f13730 = intent.getStringExtra("playTitle");
        if (this.f13730 == null) {
            this.f13730 = I18N.m15706(R.string.app_name);
        }
        this.f13731 = intent.getLongExtra("position", -1);
        if (intent.getBooleanExtra("hasSubtitles", false)) {
            this.f13732 = intent.getStringArrayListExtra("subtitlePaths");
            this.f13740 = intent.getStringArrayListExtra("subtitleTitles");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private DataSource.Factory m17437() {
        FileDataSourceFactory fileDataSourceFactory = new FileDataSourceFactory();
        return this.mForceNoPlayerCache ? fileDataSourceFactory : m17466((DataSource.Factory) fileDataSourceFactory);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m17439() {
        int parseColor;
        int parseColor2;
        SubtitleView subtitleView = this.f13736.getSubtitleView();
        if (subtitleView == null) {
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.exo_player_error_unable_to_set_subs_style), 1).show();
            return;
        }
        float f = TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f);
        try {
            parseColor = Color.parseColor(TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            parseColor = Color.parseColor("#FFFFFFFF");
            TVApplication.m6285().edit().putString("pref_cc_subs_font_color", "#FFFFFFFF").apply();
        }
        try {
            parseColor2 = Color.parseColor(TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            parseColor2 = Color.parseColor("#00FFFFFF");
            TVApplication.m6285().edit().putString("pref_cc_subs_bg_color", "#00FFFFFF").apply();
        }
        CaptionStyleCompat captionStyleCompat = new CaptionStyleCompat(parseColor, parseColor2, 0, 1, Color.argb(255, 43, 43, 43), Typeface.DEFAULT);
        subtitleView.setApplyEmbeddedFontSizes(false);
        subtitleView.setApplyEmbeddedStyles(false);
        subtitleView.setFractionalTextSize(0.0533f * f);
        subtitleView.setStyle(captionStyleCompat);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m17441() {
        m17478((String) null, new boolean[0]);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m17444() {
        if (this.f13733 != null) {
            try {
                this.f13739 = this.f13733.getPlayWhenReady();
                m17446();
                this.f13733.release();
                this.f13733 = null;
                this.f13748 = null;
                this.f13749 = null;
                this.f13742 = null;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public synchronized void m17446() {
        if (this.f13733 != null) {
            this.mResumeWindow = this.f13733.getCurrentWindowIndex();
            this.mResumePosition = Math.max(0, this.f13733.getCurrentPosition());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public synchronized void m17447() {
        this.mResumeWindow = -1;
        this.mResumePosition = C.TIME_UNSET;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m17450() {
        this.f13739 = true;
        this.f13741 = new Handler();
        setContentView((int) R.layout.activity_exo_player);
        this.f13736 = (TTVSimpleExoPlayerView) findViewById(R.id.player_view);
        this.f13736.setResizeMode(0);
        this.f13736.setPortrait(false);
        this.f13736.setControllerHideOnTouch(true);
        this.f13736.setFirstTimeReady(this.f13747);
        this.f13736.setDisplayName(this.f13730);
        this.f13736.setPortraitBackListener(new TTVPlaybackControlView.ExoClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m17484(View view, boolean z) {
                ExoPlayerActivity.this.m17441();
                return false;
            }
        });
        this.f13736.setControllerVisibilityAnimationStartListener(new TTVPlaybackControlView.VisibilityAnimationListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17485(int i) {
                switch (i) {
                    case 0:
                        ExoPlayerActivity.this.m17479(true);
                        return;
                    default:
                        ExoPlayerActivity.this.m17479(false);
                        return;
                }
            }
        });
        TTVPlaybackControlView controller = this.f13736.getController();
        if (controller != null) {
            controller.setCcButtonOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ExoPlayerActivity.this.m17477(I18N.m15706(R.string.subtitle), 3);
                }
            });
        }
        m17439();
        this.f13736.requestFocus();
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m17451(boolean z) {
        if (this.f13733 != null) {
            m17444();
        }
        AnonymousClass9 r12 = new BandwidthMeter() {
            public long getBitrateEstimate() {
                return Long.MAX_VALUE;
            }
        };
        BandwidthMeter r13 = new BandwidthMeter() {
            public long getBitrateEstimate() {
                return 1;
            }
        };
        if (!this.mForceLowestResHLS) {
            r13 = r12;
        }
        this.f13748 = new TTVCustomTrackSelector(r13);
        this.f13748.setParameters(this.f13748.getParameters().withExceedRendererCapabilitiesIfNecessary(true).withPreferredAudioLanguage("eng").withAllowMixedMimeAdaptiveness(true));
        this.f13749 = new TrackSelectionHelper(this.f13748, (TrackSelection.Factory) null);
        this.f13738 = null;
        this.f13742 = new ExoEventLogger(this.f13748);
        String lowerCase = this.f13745.getProviderName().trim().replace(StringUtils.SPACE, "").toLowerCase();
        int i = (((lowerCase.contains("x264") || lowerCase.contains("x265") || lowerCase.contains("5.1ch") || lowerCase.contains("7.1ch") || lowerCase.contains("8ch") || lowerCase.contains("truehd")) || this.mForceSoftwareAudioDecoder) && FfmpegLibrary.isAvailable()) ? 2 : 1;
        synchronized (ExoPlayerActivity.class) {
            this.mCurrentExtensionRendererMode = i;
        }
        TTVRenderersFactory tTVRenderersFactory = new TTVRenderersFactory(this, (DrmSessionManager<FrameworkMediaCrypto>) null, i);
        synchronized (ExoPlayerActivity.class) {
            if (this.mForceSoftwareVideoDecoder) {
                tTVRenderersFactory.m15889(true);
            }
        }
        if (this.f13733 != null) {
            m17444();
        }
        this.f13733 = ExoPlayerFactory.newSimpleInstance((RenderersFactory) tTVRenderersFactory, (TrackSelector) this.f13748, (LoadControl) new TTVLoadControl(new DefaultAllocator(true, 65536), 60000, 90000, 100, 3000));
        this.f13733.addListener(new CustomPlayerEventListener(z));
        if (this.f13742 != null) {
        }
        this.f13736.setPlayer(this.f13733);
        this.f13733.setPlayWhenReady(this.f13739);
        this.f13733.setVolume(1.0f);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private DataSource.Factory m17452(boolean z) {
        return (this.mForceHttpUrlConnection || GoogleVideoHelper.m15945(this.f13745.getStreamLink())) ? m17457(z) : m17460(z);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17453() {
        if (!DeviceUtils.m6389(new boolean[0])) {
            m17462();
        } else if (this.f13736 == null || !this.f13736.m17775()) {
            m17441();
        } else {
            this.f13736.m17768();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m17456(ExoPlaybackException exoPlaybackException) {
        if (exoPlaybackException.type != 0) {
            return false;
        }
        for (Throwable sourceException = exoPlaybackException.getSourceException(); sourceException != null; sourceException = sourceException.getCause()) {
            if (sourceException instanceof BehindLiveWindowException) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private DataSource.Factory m17457(boolean z) {
        String str = TyphoonApp.f5835;
        HashMap<String, String> r8 = SourceUtils.m17835(this.f13745.getPlayHeader());
        if (r8 != null && z) {
            if (r8.containsKey(AbstractSpiCall.HEADER_USER_AGENT)) {
                str = r8.get(AbstractSpiCall.HEADER_USER_AGENT);
            } else if (r8.containsKey("user-agent")) {
                str = r8.get("user-agent");
            }
        }
        DefaultHttpDataSourceFactory defaultHttpDataSourceFactory = new DefaultHttpDataSourceFactory(str, (TransferListener<? super DataSource>) null, 45000, 45000, true);
        HttpDataSource.RequestProperties defaultRequestProperties = defaultHttpDataSourceFactory.getDefaultRequestProperties();
        if (r8 != null && z) {
            for (Map.Entry next : r8.entrySet()) {
                if (!(next.getKey() == null || next.getValue() == null || ((String) next.getKey()).equalsIgnoreCase("user-agent"))) {
                    defaultRequestProperties.set((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        Map<String, String> snapshot = defaultRequestProperties.getSnapshot();
        if (!snapshot.containsKey("Cache-Control")) {
            defaultRequestProperties.set("Cache-Control", "no-cache");
        }
        if (!snapshot.containsKey(AbstractSpiCall.HEADER_ACCEPT)) {
            defaultRequestProperties.set(AbstractSpiCall.HEADER_ACCEPT, "*/*");
        }
        if (!snapshot.containsKey("Accept-Language")) {
            defaultRequestProperties.set("Accept-Language", "en-US;q=0.6,en;q=0.4");
        }
        return this.mForceNoPlayerCache ? defaultHttpDataSourceFactory : m17466((DataSource.Factory) defaultHttpDataSourceFactory);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17459() {
        if (this.f13735 != null && !this.f13735.isUnsubscribed()) {
            this.f13735.unsubscribe();
        }
        this.f13735 = null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private DataSource.Factory m17460(boolean z) {
        String str = TyphoonApp.f5835;
        HashMap<String, String> r2 = SourceUtils.m17835(this.f13745.getPlayHeader());
        if (r2 != null && z) {
            if (r2.containsKey(AbstractSpiCall.HEADER_USER_AGENT)) {
                str = r2.get(AbstractSpiCall.HEADER_USER_AGENT);
            } else if (r2.containsKey("user-agent")) {
                str = r2.get("user-agent");
            }
        }
        OkHttpDataSourceFactory okHttpDataSourceFactory = new OkHttpDataSourceFactory(HttpHelper.m6343().m6356(), str, (TransferListener<? super DataSource>) null, CacheControl.f6219);
        HttpDataSource.RequestProperties defaultRequestProperties = okHttpDataSourceFactory.getDefaultRequestProperties();
        if (r2 != null && z) {
            for (Map.Entry next : r2.entrySet()) {
                if (!(next.getKey() == null || next.getValue() == null || ((String) next.getKey()).equalsIgnoreCase("user-agent"))) {
                    defaultRequestProperties.set((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        return this.mForceNoPlayerCache ? okHttpDataSourceFactory : m17466((DataSource.Factory) okHttpDataSourceFactory);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17462() {
        new CustomErrorDialogFragment().show(getSupportFragmentManager(), "playerExitFrag");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaSource m17464(com.typhoon.tv.model.media.MediaSource mediaSource) {
        try {
            Uri parse = Uri.parse(mediaSource.getStreamLink());
            switch ((this.mForceExtractorSource || (!mediaSource.isHLS() && Util.inferContentType(parse) != 2)) ? (char) 3 : 2) {
                case 2:
                    return new HlsMediaSource(parse, m17452(true), this.f13741, this.f13746);
                case 3:
                    return new ExtractorMediaSource(parse, m17452(true), new DefaultExtractorsFactory(), this.f13741, this.f13744);
                default:
                    return null;
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return null;
        }
        Logger.m6281((Throwable) e, new boolean[0]);
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DataSource.Factory m17466(DataSource.Factory factory) {
        File cacheDir = getCacheDir();
        return new CacheDataSourceFactory(new SimpleCache(cacheDir, new LeastRecentlyUsedCacheEvictor(Utils.m6396(cacheDir))), factory, 6);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<MediaSource> m17470(List<String> list, List<String> list2) {
        String str;
        DataSource.Factory r4 = m17437();
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        for (int i = 0; i < list.size(); i++) {
            try {
                String str2 = list.get(i);
                String str3 = list2.get(i);
                switch (BaseSubtitlesService.m6374(str2)) {
                    case 3:
                        str = MimeTypes.APPLICATION_SUBRIP;
                        break;
                    case 4:
                        str = MimeTypes.TEXT_SSA;
                        break;
                    case 5:
                        str = MimeTypes.TEXT_VTT;
                        break;
                    case 6:
                        str = MimeTypes.APPLICATION_TTML;
                        break;
                    default:
                        str = null;
                        break;
                }
                if (str != null) {
                    File file = new File(str2);
                    if (file.exists()) {
                        String r9 = SubtitlesConverter.m16818(str2, new FormatSRT(), true);
                        File file2 = new File(file.getParent(), "exo-" + new Random(9999) + "-" + file.getName());
                        if (file2.createNewFile() && r9 != null) {
                            Sink sink = null;
                            BufferedSink bufferedSink = null;
                            try {
                                sink = Okio.m20502(file2);
                                bufferedSink = Okio.m20506(sink);
                                bufferedSink.m20445(r9);
                                str2 = file2.getAbsolutePath();
                                str = MimeTypes.APPLICATION_SUBRIP;
                                if (bufferedSink != null) {
                                    try {
                                        bufferedSink.flush();
                                    } catch (Exception e) {
                                    }
                                    try {
                                        bufferedSink.close();
                                    } catch (Exception e2) {
                                    }
                                }
                                if (sink != null) {
                                    try {
                                        sink.flush();
                                    } catch (Exception e3) {
                                    }
                                    try {
                                        sink.close();
                                    } catch (Exception e4) {
                                    }
                                }
                            } catch (Throwable th) {
                                if (bufferedSink != null) {
                                    try {
                                        bufferedSink.flush();
                                    } catch (Exception e5) {
                                    }
                                    try {
                                        bufferedSink.close();
                                    } catch (Exception e6) {
                                    }
                                }
                                if (sink != null) {
                                    try {
                                        sink.flush();
                                    } catch (Exception e7) {
                                    }
                                    try {
                                        sink.close();
                                    } catch (Exception e8) {
                                    }
                                }
                                throw th;
                            }
                            arrayList.add(new SingleSampleMediaSource(Uri.parse(str2), r4, Format.createTextSampleFormat(str3, str, z ? -1 : 1, (String) null), C.TIME_UNSET));
                            z = true;
                        }
                    }
                }
            } catch (Exception e9) {
                Logger.m6281((Throwable) e9, new boolean[0]);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        if (r5.f13732 == null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0055, code lost:
        if (r5.f13732.isEmpty() != false) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        if (r5.f13740 == null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0061, code lost:
        if (r5.f13740.isEmpty() != false) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0063, code lost:
        m17459();
        r5.f13735 = rx.Observable.m7359(new com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.AnonymousClass7(r5)).m7376(new com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.AnonymousClass6(r5)).m7382(rx.schedulers.Schedulers.io()).m7407(rx.android.schedulers.AndroidSchedulers.m24520()).m7386(new com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.AnonymousClass5(r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0095, code lost:
        r6.m17490(new com.google.android.exoplayer2.source.MergingMediaSource((com.google.android.exoplayer2.source.MediaSource[]) r0.toArray(new com.google.android.exoplayer2.source.MediaSource[r0.size()])));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m17471(final com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.OnMediaSourceBuiltListener r6) {
        /*
            r5 = this;
            r3 = 0
            com.typhoon.tv.model.media.MediaSource r2 = r5.f13745
            if (r2 != 0) goto L_0x0009
            r6.m17490(r3)
        L_0x0008:
            return
        L_0x0009:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.typhoon.tv.model.media.MediaSource r2 = r5.f13745
            com.google.android.exoplayer2.source.MediaSource r1 = r5.m17464((com.typhoon.tv.model.media.MediaSource) r2)
            if (r1 != 0) goto L_0x001a
            r6.m17490(r3)
            goto L_0x0008
        L_0x001a:
            r0.add(r1)
            java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r3 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
            monitor-enter(r3)
            java.util.List<com.google.android.exoplayer2.source.MediaSource> r2 = r5.f13743     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x004a
            java.util.List<com.google.android.exoplayer2.source.MediaSource> r2 = r5.f13743     // Catch:{ all -> 0x0047 }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0047 }
            if (r2 != 0) goto L_0x0031
            java.util.List<com.google.android.exoplayer2.source.MediaSource> r2 = r5.f13743     // Catch:{ all -> 0x0047 }
            r0.addAll(r2)     // Catch:{ all -> 0x0047 }
        L_0x0031:
            com.google.android.exoplayer2.source.MergingMediaSource r4 = new com.google.android.exoplayer2.source.MergingMediaSource     // Catch:{ all -> 0x0047 }
            int r2 = r0.size()     // Catch:{ all -> 0x0047 }
            com.google.android.exoplayer2.source.MediaSource[] r2 = new com.google.android.exoplayer2.source.MediaSource[r2]     // Catch:{ all -> 0x0047 }
            java.lang.Object[] r2 = r0.toArray(r2)     // Catch:{ all -> 0x0047 }
            com.google.android.exoplayer2.source.MediaSource[] r2 = (com.google.android.exoplayer2.source.MediaSource[]) r2     // Catch:{ all -> 0x0047 }
            r4.<init>(r2)     // Catch:{ all -> 0x0047 }
            r6.m17490(r4)     // Catch:{ all -> 0x0047 }
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            goto L_0x0008
        L_0x0047:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            throw r2
        L_0x004a:
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            java.util.ArrayList<java.lang.String> r2 = r5.f13732
            if (r2 == 0) goto L_0x0095
            java.util.ArrayList<java.lang.String> r2 = r5.f13732
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x0095
            java.util.ArrayList<java.lang.String> r2 = r5.f13740
            if (r2 == 0) goto L_0x0095
            java.util.ArrayList<java.lang.String> r2 = r5.f13740
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x0095
            r5.m17459()
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$7 r2 = new com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$7
            r2.<init>()
            rx.Observable r2 = rx.Observable.m7359(r2)
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$6 r3 = new com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$6
            r3.<init>()
            rx.Observable r2 = r2.m7376(r3)
            rx.Scheduler r3 = rx.schedulers.Schedulers.io()
            rx.Observable r2 = r2.m7382((rx.Scheduler) r3)
            rx.Scheduler r3 = rx.android.schedulers.AndroidSchedulers.m24520()
            rx.Observable r2 = r2.m7407((rx.Scheduler) r3)
            com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$5 r3 = new com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$5
            r3.<init>(r0, r6)
            rx.Subscription r2 = r2.m7386(r3)
            r5.f13735 = r2
            goto L_0x0008
        L_0x0095:
            com.google.android.exoplayer2.source.MergingMediaSource r3 = new com.google.android.exoplayer2.source.MergingMediaSource
            int r2 = r0.size()
            com.google.android.exoplayer2.source.MediaSource[] r2 = new com.google.android.exoplayer2.source.MediaSource[r2]
            java.lang.Object[] r2 = r0.toArray(r2)
            com.google.android.exoplayer2.source.MediaSource[] r2 = (com.google.android.exoplayer2.source.MediaSource[]) r2
            r3.<init>(r2)
            r6.m17490(r3)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17471(com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$OnMediaSourceBuiltListener):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17477(String str, int i) {
        int r4;
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        MappingTrackSelector.MappedTrackInfo currentMappedTrackInfo = this.f13748.getCurrentMappedTrackInfo();
        if (currentMappedTrackInfo != null && (r4 = TrackSelectionHelper.m15899((Player) this.f13733, currentMappedTrackInfo, i)) != -1) {
            this.f13749.m15906(this, str, currentMappedTrackInfo, r4, z);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        if (r14.length <= 0) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0016, code lost:
        if (r14[0] == false) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0018, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        if (com.typhoon.tv.utils.NetworkUtils.m17799() != false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001f, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0020, code lost:
        if (r13 == null) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0022, code lost:
        r13 = r13 + org.apache.commons.lang3.StringUtils.LF + com.typhoon.tv.I18N.m15706(com.typhoon.tv.R.string.no_internet);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        if (r13 == null) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0047, code lost:
        if (r13.isEmpty() != false) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0049, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004a, code lost:
        if (r1 == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004c, code lost:
        if (r1 == false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004e, code lost:
        if (r0 != false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005b, code lost:
        if (r13.toLowerCase().contains("socketexception") != false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        if (r13.toLowerCase().contains("net.protocolexception") != false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0075, code lost:
        if (r13.toLowerCase().contains("response code") != false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0082, code lost:
        if (r13.toLowerCase().contains("okhttp3.") != false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008f, code lost:
        if (r13.toLowerCase().contains("unable to connect") == false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0091, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0092, code lost:
        r3 = new android.content.Intent();
        r3.putExtra("mediaSource", r12.f13745);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a6, code lost:
        if (r12.mDuration <= -1) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a8, code lost:
        r8 = r12.mDuration;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00aa, code lost:
        r3.putExtra(com.mopub.mobileads.VastIconXmlManager.DURATION, r8);
        m17446();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b9, code lost:
        if (r12.mResumePosition == com.google.android.exoplayer2.C.TIME_UNSET) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bf, code lost:
        if (r12.mResumePosition <= -1) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c1, code lost:
        r4 = r12.mResumePosition;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c3, code lost:
        if (r1 == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c5, code lost:
        r3.putExtra("errorMsg", r13);
        r3.putExtra("isNetworkErr", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d5, code lost:
        if (r4 <= 0) goto L_0x00dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d7, code lost:
        r3.putExtra("lastPosition", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00dd, code lost:
        setResult(r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e0, code lost:
        monitor-enter(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r12.mIsExitCalled = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00e5, code lost:
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00ed, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r13 = com.typhoon.tv.I18N.m15706(com.typhoon.tv.R.string.no_internet);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f9, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00fc, code lost:
        r2 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00ff, code lost:
        r8 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0101, code lost:
        r3.putExtra("lastPosition", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0108, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0109, code lost:
        com.typhoon.tv.Logger.m6281(r6, new boolean[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        if (r14 == null) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m17478(java.lang.String r13, boolean... r14) {
        /*
            r12 = this;
            r4 = -1
            r2 = 1
            r7 = 0
            java.lang.Class<com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity> r8 = com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.class
            monitor-enter(r8)
            boolean r9 = r12.mIsExitCalled     // Catch:{ all -> 0x00ea }
            if (r9 == 0) goto L_0x000d
            monitor-exit(r8)     // Catch:{ all -> 0x00ea }
        L_0x000c:
            return
        L_0x000d:
            monitor-exit(r8)     // Catch:{ all -> 0x00ea }
            if (r14 == 0) goto L_0x00ed
            int r8 = r14.length     // Catch:{ Throwable -> 0x0108 }
            if (r8 <= 0) goto L_0x00ed
            r8 = 0
            boolean r8 = r14[r8]     // Catch:{ Throwable -> 0x0108 }
            if (r8 == 0) goto L_0x00ed
            r0 = r2
        L_0x0019:
            boolean r8 = com.typhoon.tv.utils.NetworkUtils.m17799()     // Catch:{ Throwable -> 0x0108 }
            if (r8 != 0) goto L_0x0041
            r0 = 1
            if (r13 == 0) goto L_0x00f0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0108 }
            r8.<init>()     // Catch:{ Throwable -> 0x0108 }
            java.lang.StringBuilder r8 = r8.append(r13)     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r9 = "\n"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Throwable -> 0x0108 }
            r9 = 2131820956(0x7f11019c, float:1.9274642E38)
            java.lang.String r9 = com.typhoon.tv.I18N.m15706(r9)     // Catch:{ Throwable -> 0x0108 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r13 = r8.toString()     // Catch:{ Throwable -> 0x0108 }
        L_0x0041:
            if (r13 == 0) goto L_0x00f9
            boolean r8 = r13.isEmpty()     // Catch:{ Throwable -> 0x0108 }
            if (r8 != 0) goto L_0x00f9
            r1 = r2
        L_0x004a:
            if (r1 == 0) goto L_0x00fc
        L_0x004c:
            if (r1 == 0) goto L_0x0092
            if (r0 != 0) goto L_0x0092
            java.lang.String r8 = r13.toLowerCase()     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r9 = "socketexception"
            boolean r8 = r8.contains(r9)     // Catch:{ Throwable -> 0x0108 }
            if (r8 != 0) goto L_0x0091
            java.lang.String r8 = r13.toLowerCase()     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r9 = "net.protocolexception"
            boolean r8 = r8.contains(r9)     // Catch:{ Throwable -> 0x0108 }
            if (r8 != 0) goto L_0x0091
            java.lang.String r8 = r13.toLowerCase()     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r9 = "response code"
            boolean r8 = r8.contains(r9)     // Catch:{ Throwable -> 0x0108 }
            if (r8 != 0) goto L_0x0091
            java.lang.String r8 = r13.toLowerCase()     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r9 = "okhttp3."
            boolean r8 = r8.contains(r9)     // Catch:{ Throwable -> 0x0108 }
            if (r8 != 0) goto L_0x0091
            java.lang.String r8 = r13.toLowerCase()     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r9 = "unable to connect"
            boolean r8 = r8.contains(r9)     // Catch:{ Throwable -> 0x0108 }
            if (r8 == 0) goto L_0x0092
        L_0x0091:
            r0 = 1
        L_0x0092:
            android.content.Intent r3 = new android.content.Intent     // Catch:{ Throwable -> 0x0108 }
            r3.<init>()     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r8 = "mediaSource"
            com.typhoon.tv.model.media.MediaSource r9 = r12.f13745     // Catch:{ Throwable -> 0x0108 }
            r3.putExtra(r8, r9)     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r10 = "duration"
            long r8 = r12.mDuration     // Catch:{ Throwable -> 0x0108 }
            int r8 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r8 <= 0) goto L_0x00ff
            long r8 = r12.mDuration     // Catch:{ Throwable -> 0x0108 }
        L_0x00aa:
            r3.putExtra(r10, r8)     // Catch:{ Throwable -> 0x0108 }
            r12.m17446()     // Catch:{ Throwable -> 0x0108 }
            long r8 = r12.mResumePosition     // Catch:{ Throwable -> 0x0108 }
            r10 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r8 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x00c3
            long r8 = r12.mResumePosition     // Catch:{ Throwable -> 0x0108 }
            int r8 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r8 <= 0) goto L_0x00c3
            long r4 = r12.mResumePosition     // Catch:{ Throwable -> 0x0108 }
        L_0x00c3:
            if (r1 == 0) goto L_0x0101
            java.lang.String r8 = "errorMsg"
            r3.putExtra(r8, r13)     // Catch:{ Throwable -> 0x0108 }
            java.lang.String r8 = "isNetworkErr"
            r3.putExtra(r8, r0)     // Catch:{ Throwable -> 0x0108 }
            r8 = 0
            int r8 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r8 <= 0) goto L_0x00dd
            java.lang.String r8 = "lastPosition"
            r3.putExtra(r8, r4)     // Catch:{ Throwable -> 0x0108 }
        L_0x00dd:
            r12.setResult(r2, r3)     // Catch:{ Throwable -> 0x0108 }
        L_0x00e0:
            monitor-enter(r12)
            r7 = 1
            r12.mIsExitCalled = r7     // Catch:{ all -> 0x010f }
            monitor-exit(r12)     // Catch:{ all -> 0x010f }
            r12.finish()
            goto L_0x000c
        L_0x00ea:
            r7 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00ea }
            throw r7
        L_0x00ed:
            r0 = r7
            goto L_0x0019
        L_0x00f0:
            r8 = 2131820956(0x7f11019c, float:1.9274642E38)
            java.lang.String r13 = com.typhoon.tv.I18N.m15706(r8)     // Catch:{ Throwable -> 0x0108 }
            goto L_0x0041
        L_0x00f9:
            r1 = r7
            goto L_0x004a
        L_0x00fc:
            r2 = -1
            goto L_0x004c
        L_0x00ff:
            r8 = r4
            goto L_0x00aa
        L_0x0101:
            java.lang.String r8 = "lastPosition"
            r3.putExtra(r8, r4)     // Catch:{ Throwable -> 0x0108 }
            goto L_0x00dd
        L_0x0108:
            r6 = move-exception
            boolean[] r7 = new boolean[r7]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r7)
            goto L_0x00e0
        L_0x010f:
            r7 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x010f }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity.m17478(java.lang.String, boolean[]):void");
    }

    /* access modifiers changed from: private */
    @SuppressLint({"ObsoleteSdkInt"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17479(boolean z) {
        boolean z2 = true;
        int i = 1;
        try {
            View decorView = getWindow().getDecorView();
            View findViewById = findViewById(R.id.player_root);
            if (decorView != null && Build.VERSION.SDK_INT >= 14) {
                decorView.setFitsSystemWindows(!z);
                if (z) {
                    z2 = false;
                }
                findViewById.setFitsSystemWindows(z2);
                if (!z) {
                    i = 0;
                }
                if (Build.VERSION.SDK_INT >= 19) {
                    i |= 1792;
                    if (z) {
                        i |= 2054;
                    }
                }
                decorView.setSystemUiVisibility(i);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17480(final boolean z, boolean z2) {
        boolean z3 = this.f13733 == null;
        if (z3 || z2) {
            if (z2) {
                m17444();
            }
            m17451(z);
        }
        if (z3 || z2 || (this.f13737 && z)) {
            m17471((OnMediaSourceBuiltListener) new OnMediaSourceBuiltListener() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17489(MediaSource mediaSource) {
                    boolean z = true;
                    if (mediaSource == null) {
                        ExoPlayerActivity.this.m17478(I18N.m15706(R.string.error), new boolean[0]);
                        return;
                    }
                    if (ExoPlayerActivity.this.f13733 == null) {
                        ExoPlayerActivity.this.m17451(z);
                    }
                    boolean z2 = (ExoPlayerActivity.this.mResumeWindow == -1 || ExoPlayerActivity.this.mResumePosition == C.TIME_UNSET || ExoPlayerActivity.this.mResumePosition <= 0) ? false : true;
                    if (z2) {
                        ExoPlayerActivity.this.f13733.seekTo(ExoPlayerActivity.this.mResumeWindow, ExoPlayerActivity.this.mResumePosition);
                        ExoPlayerActivity.this.mIsLoaded = true;
                    } else {
                        ExoPlayerActivity.this.mIsLoaded = false;
                    }
                    SimpleExoPlayer r3 = ExoPlayerActivity.this.f13733;
                    if (z2) {
                        z = false;
                    }
                    r3.prepare(mediaSource, z, false);
                    boolean unused = ExoPlayerActivity.this.f13737 = false;
                }
            });
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        boolean z = false;
        if (!TyphoonApp.f5852) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyEvent.getAction() == 0 && keyEvent.getKeyCode() == 4) {
            m17453();
            return true;
        } else if (keyEvent.getKeyCode() == 4 || this.f13736 == null) {
            return super.dispatchKeyEvent(keyEvent);
        } else {
            if (DeviceUtils.m6389(new boolean[0]) && this.f13736 != null && this.f13736.getController() != null && !this.f13736.getController().m17712()) {
                if (keyEvent.getKeyCode() == 19) {
                    if (keyEvent.getAction() != 0) {
                        return true;
                    }
                    this.f13736.m17766(true);
                    return true;
                } else if (keyEvent.getKeyCode() == 20) {
                    if (keyEvent.getAction() != 0) {
                        return true;
                    }
                    this.f13736.m17766(false);
                    return true;
                }
            }
            if (this.f13736.getController() != null) {
                if (!this.f13736.getController().m17712()) {
                    this.f13736.getController().m17706();
                }
                if (this.f13733 == null || !this.f13733.getPlayWhenReady()) {
                    this.f13736.getController().m17711(new boolean[0]);
                } else {
                    this.f13736.m17774(true);
                }
            }
            if (super.dispatchKeyEvent(keyEvent) || this.f13736.m17776(keyEvent)) {
                z = true;
            }
            return z;
        }
    }

    public void onBackPressed() {
        m17453();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (TVApplication.m6285().getBoolean("pref_force_tv_mode", false)) {
            try {
                Resources resources = getResources();
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                Configuration configuration = resources.getConfiguration();
                configuration.uiMode = 4;
                resources.updateConfiguration(configuration, displayMetrics);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        super.onCreate(bundle);
        try {
            Bridge.restoreInstanceState(this, bundle);
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        setContentView((int) R.layout.activity_exo_player);
        String trim = TVApplication.m6285().getString("pref_app_lang", "").trim();
        if (!trim.isEmpty()) {
            LocaleUtils.m17793(TVApplication.m6288(), LocaleUtils.m17792(trim));
        } else {
            LocaleUtils.m17793(TVApplication.m6288(), Resources.getSystem().getConfiguration().locale);
        }
        try {
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.m430();
            }
        } catch (Exception e3) {
        }
        TTVWorkaroundUtils.disableAllWorkaround();
        if (this.mIsExitCalled) {
            m17441();
            return;
        }
        Utils.m6422(false);
        m17435();
        if (this.f13745 == null) {
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.error), 0).show();
            m17478(I18N.m15706(R.string.exo_player_error_media_source_does_not_exist), new boolean[0]);
            return;
        }
        m17450();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        m17459();
        if (this.f13736 != null && this.f13736.m17771()) {
            try {
                if (Settings.System.getInt(getContentResolver(), "screen_brightness_mode") == 0) {
                    if (Build.VERSION.SDK_INT < 23 || Settings.System.canWrite(this)) {
                        Settings.System.putInt(getContentResolver(), "screen_brightness_mode", 1);
                    } else {
                        return;
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        super.onDestroy();
    }

    public void onNewIntent(Intent intent) {
        m17444();
        this.f13739 = true;
        m17447();
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            m17444();
        }
    }

    public void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23 || this.f13733 == null) {
            m17480(false, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        this.f13747 = this.f13736 != null && this.f13736.m17767();
        m17446();
        try {
            super.onSaveInstanceState(bundle);
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        try {
            Bridge.saveInstanceState(this, bundle);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    public void onStart() {
        super.onStart();
        if (this.mIsExitCalled) {
            m17441();
        } else if (Util.SDK_INT > 23) {
            m17480(false, false);
        }
    }

    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            m17444();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17482() {
        if (this.f13733 != null) {
            this.f13733.setPlayWhenReady(false);
            this.f13733.getPlaybackState();
        }
    }
}
