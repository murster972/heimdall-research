package com.typhoon.tv.ui.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.AdinCubeNativeEventListener;
import com.adincube.sdk.NativeAd;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.widget.ExpandedControllerActivity;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.backup.SubsMapBackupRestoreHelper;
import com.typhoon.tv.utils.NativeAdsUtils;
import com.typhoon.tv.webserver.WebServerService;
import java.util.List;

public class ExpandedControlsActivity extends ExpandedControllerActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public List<NativeAd> f13117;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SessionManagerListener f13118 = new SessionManagerListenerImpl();

    private class SessionManagerListenerImpl implements SessionManagerListener {
        private SessionManagerListenerImpl() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m16900(Session session) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m16901(Session session, int i) {
            ExpandedControlsActivity.this.stopService(new Intent(ExpandedControlsActivity.this, WebServerService.class));
            try {
                SubsMapBackupRestoreHelper.m15826();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            ExpandedControlsActivity.this.finish();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m16902(Session session, String str) {
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m16903(Session session, int i) {
            ExpandedControlsActivity.this.invalidateOptionsMenu();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m16904(Session session, int i) {
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.chromecast_resume_failed), 1).show();
            ExpandedControlsActivity.this.invalidateOptionsMenu();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m16905(Session session) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m16906(Session session, int i) {
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.chromecast_failed_to_play), 1).show();
            ExpandedControlsActivity.this.invalidateOptionsMenu();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m16907(Session session, String str) {
            ExpandedControlsActivity.this.invalidateOptionsMenu();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m16908(Session session, boolean z) {
            ExpandedControlsActivity.this.invalidateOptionsMenu();
        }
    }

    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    private void m16899() {
        int systemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
        if (Build.VERSION.SDK_INT >= 14) {
            systemUiVisibility ^= 2;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            systemUiVisibility ^= 4;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            systemUiVisibility ^= 4096;
        }
        getWindow().getDecorView().setSystemUiVisibility(systemUiVisibility);
        if (Build.VERSION.SDK_INT >= 18) {
            setImmersive(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        try {
            super.onCreate(bundle);
        } catch (RuntimeException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.error), 1).show();
            finish();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        final View findViewById = findViewById(R.id.adViewExpandedController);
        if (!TyphoonApp.f5865 && findViewById != null && (findViewById instanceof ViewGroup)) {
            try {
                final boolean z = TVApplication.m6285().getBoolean("is_nad_mech_enabled", false);
                AnonymousClass1 r4 = new AdinCubeNativeEventListener() {
                    public void onAdLoaded(List<NativeAd> list) {
                        if (list.size() > 0) {
                            if (ExpandedControlsActivity.this.f13117 != null && ExpandedControlsActivity.this.f13117.size() > 0) {
                                AdinCube.Native.m2314((List<NativeAd>) ExpandedControlsActivity.this.f13117);
                                List unused = ExpandedControlsActivity.this.f13117 = null;
                            }
                            List unused2 = ExpandedControlsActivity.this.f13117 = list;
                            if (z) {
                                try {
                                    List unused3 = ExpandedControlsActivity.this.f13117 = NativeAdsUtils.m17796((List<NativeAd>) ExpandedControlsActivity.this.f13117);
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                }
                            }
                            NativeAd nativeAd = (NativeAd) ExpandedControlsActivity.this.f13117.get(0);
                            ViewGroup viewGroup = (ViewGroup) findViewById;
                            RelativeLayout relativeLayout = (RelativeLayout) ExpandedControlsActivity.this.getLayoutInflater().inflate(R.layout.item_native_ad_banner, (ViewGroup) null).findViewById(R.id.relativeLayoutNativeAd);
                            AdinCube.Native.m2312((ImageView) relativeLayout.findViewById(R.id.ivNativeAdIcon), nativeAd.m2360());
                            if (nativeAd.m2358() != null) {
                                AdinCube.Native.m2312((ImageView) relativeLayout.findViewById(R.id.ivNativeAdImage), nativeAd.m2358());
                            }
                            ((TextView) relativeLayout.findViewById(R.id.tvNativeAdTitle)).setText(nativeAd.m2362());
                            if (nativeAd.m2361() != null) {
                                String r3 = nativeAd.m2361();
                                if (nativeAd.m2356() != null && ((double) nativeAd.m2356().floatValue()) >= 4.5d) {
                                    r3 = "★ " + String.valueOf(nativeAd.m2356()) + " - " + r3;
                                }
                                ((TextView) relativeLayout.findViewById(R.id.tvNativeAdText)).setText(r3);
                            }
                            ((TextView) relativeLayout.findViewById(R.id.btnNativeAdCta)).setText(nativeAd.m2359());
                            relativeLayout.findViewById(R.id.ivNativeAdPrivacy).setVisibility(8);
                            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
                            layoutParams.height = (int) TypedValue.applyDimension(1, (float) ExpandedControlsActivity.this.getResources().getInteger(R.integer.native_height), ExpandedControlsActivity.this.getResources().getDisplayMetrics());
                            viewGroup.addView(relativeLayout, layoutParams);
                            viewGroup.setVisibility(0);
                            AdinCube.Native.m2311((ViewGroup) relativeLayout, nativeAd);
                        }
                    }

                    public void onLoadError(String str) {
                    }
                };
                if (z) {
                    AdinCube.Native.m2309(this, 3, r4);
                } else {
                    AdinCube.Native.m2310((Context) this, (AdinCubeNativeEventListener) r4);
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_expanded_controller, menu);
        CastButtonFactory.m7974(this, menu, R.id.media_route_menu_item);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            super.onDestroy();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        if (this.f13117 != null && this.f13117.size() > 0) {
            AdinCube.Native.m2314(this.f13117);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        CastContext.m7977((Context) this).m7981().m8076(this.f13118);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        CastContext.m7977((Context) this).m7981().m8081((SessionManagerListener<Session>) this.f13118);
        CastSession r0 = CastContext.m7977((Context) this).m7981().m8075();
        if (r0 == null || (!r0.m8053() && !r0.m8054())) {
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.chromecast_not_connected), 1).show();
            finish();
        }
        m16899();
        super.onResume();
    }

    public void setContentView(int i) {
        super.setContentView((int) R.layout.cast_expanded_controller_custom_activity);
    }
}
