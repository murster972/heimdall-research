package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Spinner;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.api.TraktUserApi;
import com.typhoon.tv.api.TvdbApi;
import com.typhoon.tv.helper.category.MovieCategoryHelper;
import com.typhoon.tv.helper.category.TvShowCategoryHelper;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.IMediaListPresenter;
import com.typhoon.tv.presenter.impl.MediaListPresenterImpl;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.MediaListAdapter;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.widget.AutofitSuperRecyclerView;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.SettingsMigrator;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.IBookmarkView;
import com.typhoon.tv.view.IMediaListView;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import it.gmariotti.changelibs.library.view.ChangeLogRecyclerView;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import retrofit2.Callback;
import rx.functions.Action1;

public class HomeActivity extends BaseAdActivity implements NavigationView.OnNavigationItemSelectedListener, IBookmarkView, IMediaListView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f13123 = -1;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f13124 = 1;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f13125;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public IMediaListPresenter f13126;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean f13127;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public int f13128;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public AutofitSuperRecyclerView f13129;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public int f13130;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public MediaListAdapter f13131;

    /* renamed from: 龘  reason: contains not printable characters */
    private Toolbar f13132;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m16910() {
        this.f13132 = (Toolbar) findViewById(R.id.toolbar_home);
        setSupportActionBar(this.f13132);
        getSupportActionBar().m433(false);
        getSupportActionBar().m427(false);
        getSupportActionBar().m441(false);
        Spinner spinner = (Spinner) findViewById(R.id.toolbar_spinner_home);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, this.f13130 == 0 ? TvShowCategoryHelper.m16002() : MovieCategoryHelper.m15998());
        arrayAdapter.setDropDownViewResource(17367049);
        spinner.setAdapter(arrayAdapter);
        spinner.setSelection(this.f13128);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                if (HomeActivity.this.f13127) {
                    boolean unused = HomeActivity.this.f13127 = false;
                    return;
                }
                HomeActivity.this.f13126.m16134();
                HomeActivity.this.f13129.setRefreshing(false);
                int unused2 = HomeActivity.this.f13124 = 1;
                int unused3 = HomeActivity.this.f13123 = -1;
                int unused4 = HomeActivity.this.f13128 = i;
                HomeActivity.this.invalidateOptionsMenu();
                if (HomeActivity.this.m16912()) {
                    HomeActivity.this.f13129.m26367();
                    HomeActivity.this.f13126.m16135(HomeActivity.this.f13130, HomeActivity.this.f13128, HomeActivity.this.f13123, HomeActivity.this.f13124, false);
                    return;
                }
                HomeActivity.this.m16940(I18N.m15706(R.string.no_internet));
                HomeActivity.this.f13129.m26364();
                HomeActivity.this.f13131.m17492();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m16912() {
        return NetworkUtils.m17799() || this.f13128 == 0 || this.f13128 == 0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m16914() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(I18N.m15706(R.string.all));
        arrayList.addAll(TyphoonApp.m6323());
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367057, arrayList);
        new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.filter_year)).m524((CharSequence) I18N.m15707(R.string.cancel, new Object[0]), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!HomeActivity.this.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        }).m538(true).m535((ListAdapter) arrayAdapter, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String str = (String) arrayAdapter.getItem(i);
                HomeActivity.this.f13126.m16134();
                HomeActivity.this.f13129.setRefreshing(false);
                if (Utils.m6426(str)) {
                    int unused = HomeActivity.this.f13123 = Integer.parseInt(str);
                } else {
                    int unused2 = HomeActivity.this.f13123 = -1;
                }
                int unused3 = HomeActivity.this.f13124 = 1;
                HomeActivity.this.invalidateOptionsMenu();
                if (HomeActivity.this.m16912()) {
                    HomeActivity.this.f13129.m26367();
                    HomeActivity.this.f13126.m16135(HomeActivity.this.f13130, HomeActivity.this.f13128, HomeActivity.this.f13123, HomeActivity.this.f13124, false);
                    return;
                }
                HomeActivity.this.m16940(I18N.m15706(R.string.no_internet));
                HomeActivity.this.f13129.m26364();
                HomeActivity.this.f13131.m17492();
            }
        }).m528();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private void m16916() {
        this.f13126 = new MediaListPresenterImpl(this, this);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private void m16918() {
        m17423(findViewById(R.id.adViewMain));
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private void m16919() {
        this.f13129.setLoadingMore(false);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private void m16920() {
        boolean z = true & false;
        if (!isFinishing()) {
            int i = TVApplication.m6285().getInt("pref_version", 0);
            if (i < Utils.m6393()) {
                SettingsMigrator.m17826(i);
            }
            boolean z2 = TVApplication.m6285().getBoolean("pref_show_disclaimer", true);
            TVApplication.m6285().edit().putInt("pref_version", Utils.m6393()).apply();
            if (z2) {
                m16928(true);
            }
            boolean z3 = false;
            if (i < Utils.m6393()) {
                if (BasePlayerHelper.m16031()) {
                    BasePlayerHelper.m16036((Activity) this, (BasePlayerHelper.OnChoosePlayerListener) null);
                    z3 = true;
                }
                if (!isFinishing()) {
                    TVApplication.m6285().edit().remove("pref_enabled_tv_shows_providers").apply();
                    TVApplication.m6285().edit().remove("pref_enabled_movies_providers").apply();
                    TVApplication.m6285().edit().remove("pref_enabled_resolvers").apply();
                    final AlertDialog r0 = new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.changelog)).m522((View) (ChangeLogRecyclerView) getLayoutInflater().inflate(R.layout.dialog_changelog, (ViewGroup) null)).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (!HomeActivity.this.isFinishing() && dialogInterface != null) {
                                dialogInterface.dismiss();
                            }
                        }
                    }).m525();
                    r0.setOnShowListener(new DialogInterface.OnShowListener() {
                        public void onShow(DialogInterface dialogInterface) {
                            Button r0 = r0.m515(-1);
                            r0.setFocusable(true);
                            r0.setFocusableInTouchMode(true);
                            r0.requestFocus();
                        }
                    });
                    r0.show();
                    if (i == 0 && !z3) {
                        BasePlayerHelper.m16036((Activity) this, (BasePlayerHelper.OnChoosePlayerListener) null);
                    }
                }
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    static /* synthetic */ int m16921(HomeActivity homeActivity) {
        int i = homeActivity.f13124;
        homeActivity.f13124 = i + 1;
        return i;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m16922() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_home);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, this.f13132, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            public void onDrawerClosed(View view) {
            }

            public void onDrawerOpened(View view) {
                NavigationView navigationView = (NavigationView) HomeActivity.this.findViewById(R.id.nav_view_home);
                if (navigationView != null) {
                    navigationView.setCheckedItem(HomeActivity.this.f13130 == 0 ? R.id.menu_drawer_tv_shows : R.id.menu_drawer_movies);
                    navigationView.requestFocus();
                }
            }

            public void onDrawerSlide(View view, float f) {
            }

            public void onDrawerStateChanged(int i) {
            }
        });
        actionBarDrawerToggle.m457();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_home);
        navigationView.setNavigationItemSelectedListener(this);
        AnonymousClass3 r9 = new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://typhoontv.ml"));
                intent.setFlags(268435456);
                try {
                    HomeActivity.this.startActivity(intent);
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    HomeActivity.this.m16940(I18N.m15706(R.string.error));
                }
            }
        };
        if (navigationView.getHeaderCount() > 0) {
            View headerView = navigationView.getHeaderView(0);
            View findViewById = headerView.findViewById(R.id.ivNavLogo);
            if (findViewById != null) {
                findViewById.setOnClickListener(r9);
                if (DeviceUtils.m6389(new boolean[0])) {
                    findViewById.setFocusable(false);
                    findViewById.setClickable(false);
                }
            }
            View findViewById2 = headerView.findViewById(R.id.tvNavAppName);
            if (findViewById2 != null) {
                findViewById2.setOnClickListener(r9);
                if (DeviceUtils.m6389(new boolean[0])) {
                    findViewById2.setFocusable(false);
                    findViewById2.setClickable(false);
                }
            }
            View findViewById3 = headerView.findViewById(R.id.tvNavWebsite);
            if (findViewById3 != null) {
                findViewById3.setOnClickListener(r9);
                if (DeviceUtils.m6389(new boolean[0])) {
                    findViewById3.setFocusable(false);
                    findViewById3.setClickable(false);
                }
            }
        }
        this.f13129 = (AutofitSuperRecyclerView) findViewById(R.id.rvTvShowList);
        this.f13131 = new MediaListAdapter(new ArrayList());
        this.f13131.m17497((MediaCardViewHolder.OnCardClickListener) new MediaCardViewHolder.OnCardClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m16944(int i) {
                Intent intent = new Intent(HomeActivity.this, MediaDetailsActivity.class);
                intent.putExtra("mediaInfo", (MediaInfo) HomeActivity.this.f13131.m17495(i));
                HomeActivity.this.startActivity(intent);
            }
        });
        this.f13131.m17498((MediaCardViewHolder.OnCardLongClickListener) new MediaCardViewHolder.OnCardLongClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m16945(View view, int i) {
                final MediaInfo mediaInfo = (MediaInfo) HomeActivity.this.f13131.m17495(i);
                final boolean r2 = TVApplication.m6287().m6319(mediaInfo);
                String[] strArr = new String[1];
                strArr[0] = I18N.m15706(r2 ? R.string.action_remove_from_bookmark : R.string.action_add_to_bookmark);
                new AlertDialog.Builder(HomeActivity.this).m536((CharSequence) mediaInfo.getNameAndYear()).m540((CharSequence[]) strArr, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean z = true;
                        switch (which) {
                            case 0:
                                if (r2) {
                                    TVApplication.m6287().m6304(mediaInfo);
                                } else {
                                    TVApplication.m6287().m6299(mediaInfo);
                                }
                                try {
                                    HomeActivity homeActivity = HomeActivity.this;
                                    MediaInfo mediaInfo = mediaInfo;
                                    if (r2) {
                                        z = false;
                                    }
                                    homeActivity.m17299(mediaInfo, z, true, (Callback<SyncResponse>) null);
                                    return;
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                    return;
                                }
                            default:
                                return;
                        }
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!HomeActivity.this.isFinishing() && dialogInterface != null) {
                            dialogInterface.dismiss();
                        }
                    }
                }).m528();
            }
        });
        this.f13129.setAdapter(this.f13131);
        this.f13129.getProgressView().setVisibility(0);
        this.f13129.setupMoreListener(new OnMoreListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m16946(int i, int i2, int i3) {
                if (HomeActivity.this.m16912()) {
                    HomeActivity.this.f13129.m26366();
                    HomeActivity.m16921(HomeActivity.this);
                    HomeActivity.this.f13126.m16135(HomeActivity.this.f13130, HomeActivity.this.f13128, HomeActivity.this.f13123, HomeActivity.this.f13124, true);
                    return;
                }
                HomeActivity.this.f13129.m26365();
                HomeActivity.this.m16940(I18N.m15706(R.string.no_internet));
            }
        }, 3);
        this.f13129.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        this.f13129.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                HomeActivity.this.f13126.m16134();
                if (HomeActivity.this.m16912()) {
                    int unused = HomeActivity.this.f13124 = 1;
                    HomeActivity.this.f13126.m16135(HomeActivity.this.f13130, HomeActivity.this.f13128, HomeActivity.this.f13123, HomeActivity.this.f13124, false);
                    return;
                }
                HomeActivity.this.m16940(HomeActivity.this.getString(R.string.no_internet));
                HomeActivity.this.f13129.getSwipeToRefresh().post(new Runnable() {
                    public void run() {
                        HomeActivity.this.f13129.getSwipeToRefresh().setRefreshing(false);
                    }
                });
            }
        });
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m16926() {
        this.f13126.m16134();
        this.f13129.setRefreshing(false);
        this.f13124 = 1;
        this.f13123 = -1;
        if (this.f13130 == 0) {
            this.f13128 = TVApplication.m6285().getInt("pref_choose_default_category_tv_shows", 1);
        } else {
            this.f13128 = TVApplication.m6285().getInt("pref_choose_default_category_movies", 1);
        }
        getSupportActionBar().m433(false);
        setTitle("");
        Spinner spinner = (Spinner) findViewById(R.id.toolbar_spinner_home);
        if (spinner != null) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, this.f13130 == 0 ? TvShowCategoryHelper.m16002() : MovieCategoryHelper.m15998());
            arrayAdapter.setDropDownViewResource(17367049);
            spinner.setAdapter(arrayAdapter);
            spinner.setSelection(this.f13128);
            spinner.setVisibility(0);
        }
        invalidateOptionsMenu();
        if (m16912()) {
            this.f13129.m26367();
            this.f13126.m16135(this.f13130, this.f13128, this.f13123, this.f13124, false);
        } else {
            m16940(I18N.m15706(R.string.no_internet));
            this.f13129.m26364();
            this.f13131.m17492();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m16928(boolean z) {
        PackageInfo packageInfo = null;
        boolean z2 = true;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
        }
        boolean z3 = true | true;
        AlertDialog.Builder r0 = new AlertDialog.Builder(this).m538(false).m520((int) R.mipmap.ic_launcher).m523((CharSequence) I18N.m15707(R.string.about_app_message, packageInfo != null ? packageInfo.versionName : I18N.m15706(R.string.unknown))).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        if (z) {
            r0.m536((CharSequence) I18N.m15706(R.string.disclaimer));
            r0.m537((CharSequence) I18N.m15706(R.string.accept), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    TVApplication.m6285().edit().putBoolean("pref_show_disclaimer", false).apply();
                    dialogInterface.dismiss();
                }
            });
            r0.m524((CharSequence) I18N.m15706(R.string.decline), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    TVApplication.m6285().edit().putBoolean("pref_show_disclaimer", true).apply();
                    ActivityCompat.finishAffinity(HomeActivity.this);
                }
            });
        } else {
            r0.m536((CharSequence) I18N.m15706(R.string.about_app_title));
        }
        if (!isFinishing()) {
            r0.m528();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16932(MenuItem menuItem, boolean z) {
        if (z && menuItem != null) {
            menuItem.setEnabled(true);
            menuItem.setVisible(true);
        } else if (menuItem != null) {
            menuItem.setVisible(false);
            menuItem.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 543) {
            boolean z = true ^ true;
            if (i2 == -1) {
                new BackupManager(TVApplication.m6288()).dataChanged();
            }
        }
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_home);
        if (drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
            drawerLayout.closeDrawer((int) GravityCompat.START);
        } else if (!isFinishing()) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Bundle extras;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_home);
        m17284();
        this.f13127 = true;
        if (bundle == null || bundle.isEmpty()) {
            this.f13130 = TVApplication.m6285().getInt("pref_choose_default_nav", 0);
            if (this.f13130 == 0) {
                this.f13128 = TVApplication.m6285().getInt("pref_choose_default_category_tv_shows", 1);
            } else {
                this.f13128 = TVApplication.m6285().getInt("pref_choose_default_category_movies", 1);
            }
        } else {
            this.f13130 = bundle.getInt("mCurrentType", TVApplication.m6285().getInt("pref_choose_default_nav", 0));
            if (this.f13130 == 0) {
                this.f13128 = bundle.getInt("mCurrentList", TVApplication.m6285().getInt("pref_choose_default_category_tv_shows", 1));
            } else {
                this.f13128 = bundle.getInt("mCurrentList", TVApplication.m6285().getInt("pref_choose_default_category_movies", 1));
            }
        }
        if (getIntent() != null && (extras = getIntent().getExtras()) != null && !extras.isEmpty() && extras.getBoolean("displayNewMovieReleasesPage", false)) {
            this.f13130 = 1;
            this.f13128 = 3;
        }
        m16910();
        m16922();
        m16916();
        m16918();
        ((NavigationView) findViewById(R.id.nav_view_home)).getMenu().getItem(this.f13130).setChecked(true);
        try {
            m16920();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, true);
        }
        m17289();
        if (m16912()) {
            this.f13129.setRefreshing(false);
            this.f13129.m26367();
            this.f13126.m16135(this.f13130, this.f13128, this.f13123, this.f13124, false);
        } else {
            m16940(I18N.m15706(R.string.no_internet));
            this.f13129.m26364();
        }
        m17302((Action1<Boolean>) new Action1<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Boolean bool) {
                if (!bool.booleanValue()) {
                    HomeActivity.this.m16940(I18N.m15706(R.string.permission_grant_toast));
                    return;
                }
                try {
                    File file = new File(SettingsActivity.f13215);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        });
        if (TraktCredentialsHelper.m16110().isValid() && TVApplication.m6285().getBoolean("pref_auto_sync_watched_histories_on_startup_trakt", true) && NetworkUtils.m17799()) {
            TraktUserApi.m15774().m15781(false, true);
            TraktUserApi.m15774().m15785(false, true);
        }
        TvdbApi.m15816();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        m17295(menu);
        MenuItem findItem = menu.findItem(R.id.action_year_filter);
        if (this.f13130 == 0) {
            if (this.f13128 != 1) {
                boolean z = true & true;
                if (!(this.f13128 == 6 || this.f13128 == 7 || this.f13128 == 8 || (this.f13128 >= 14 && this.f13128 <= 27))) {
                    m16932(findItem, false);
                }
            }
            m16932(findItem, true);
        } else if (this.f13124 == 1) {
            if (this.f13128 < 5 || this.f13128 > 22) {
                m16932(findItem, false);
            } else {
                m16932(findItem, true);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        m17283();
        this.f13126.m16133();
        this.f13126 = null;
        super.onDestroy();
    }

    public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_drawer_bookmark:
                startActivity(new Intent(this, BookmarkActivity.class));
                break;
            case R.id.menu_drawer_downloads:
                startActivity(new Intent(this, DownloadsActivity.class));
                break;
            case R.id.menu_drawer_movies:
                this.f13130 = 1;
                m16926();
                break;
            case R.id.menu_drawer_settings:
                startActivityForResult(new Intent(this, SettingsActivity.class), 543);
                break;
            case R.id.menu_drawer_tv_calendar:
                startActivity(new Intent(this, TvCalendarActivity.class));
                break;
            case R.id.menu_drawer_tv_shows:
                this.f13130 = 0;
                m16926();
                break;
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout_home)).closeDrawer((int) GravityCompat.START);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_about:
                if (isFinishing()) {
                    return true;
                }
                m16928(false);
                return true;
            case R.id.action_bookmark_list:
                Class<BookmarkActivity> cls = BookmarkActivity.class;
                startActivity(new Intent(this, BookmarkActivity.class));
                return true;
            case R.id.action_dmca:
                try {
                    Intent intent = new Intent("android.intent.action.SENDTO");
                    intent.setData(Uri.parse("mailto:typhoon.tv.s@gmail.com"));
                    intent.putExtra("android.intent.extra.EMAIL", "nx@typhoon.tv.s@gmail.com");
                    intent.putExtra("android.intent.extra.SUBJECT", "Typhoon TV - DMCA & Questions!");
                    intent.setFlags(268435456);
                    startActivity(intent);
                    return true;
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    return true;
                }
            case R.id.action_official_website:
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setData(Uri.parse("https://typhoontv.ml"));
                    intent2.setFlags(268435456);
                    startActivity(intent2);
                    return true;
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                    m16940(I18N.m15706(R.string.error));
                    return true;
                }
            case R.id.action_reddit_page:
                try {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    intent3.setData(Uri.parse("https://sites.google.com/view/typhoondiscord"));
                    intent3.setFlags(268435456);
                    startActivity(intent3);
                    return true;
                } catch (Exception e3) {
                    Logger.m6281((Throwable) e3, new boolean[0]);
                    m16940(I18N.m15706(R.string.error));
                    return true;
                }
            case R.id.action_search:
                Class<SearchActivity> cls2 = SearchActivity.class;
                Intent intent4 = new Intent(this, SearchActivity.class);
                intent4.putExtra("mediaType", this.f13130);
                startActivity(intent4);
                return true;
            case R.id.action_settings:
                startActivityForResult(new Intent(this, SettingsActivity.class), 543);
                return true;
            case R.id.action_translators:
                Utils.m6419((Activity) this);
                return true;
            case R.id.action_year_filter:
                m16914();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("mCurrentType", this.f13130);
        bundle.putInt("mCurrentList", this.f13128);
        super.onSaveInstanceState(bundle);
    }

    public void startActivity(Intent intent) {
        if (!(intent == null || intent.getAction() == null || !intent.getAction().equalsIgnoreCase("android.intent.action.SEARCH"))) {
            intent.putExtra("mediaType", this.f13130);
        }
        super.startActivity(intent);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16934() {
        this.f13129.setLoadingMore(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16935(ArrayList<MediaInfo> arrayList) {
        this.f13131.m17492();
        m16937(arrayList);
        if (this.f13124 == 1 && this.f13131.getItemCount() > 0) {
            this.f13129.getRecyclerView().smoothScrollToPosition(0);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m16936() {
        this.f13131.m17492();
        m16934();
        this.f13125 = 1;
        this.f13123 = -1;
        this.f13124 = 1;
        m16940(I18N.m15706(R.string.no_data));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m16937(ArrayList<MediaInfo> arrayList) {
        ArrayList r2 = this.f13131.m17496();
        ArrayList arrayList2 = new ArrayList();
        Iterator<MediaInfo> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            MediaInfo next = it2.next();
            if (!r2.contains(next)) {
                arrayList2.add(next);
            }
        }
        this.f13131.m17500(arrayList2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16938() {
        this.f13129.m26364();
        if (this.f13124 >= this.f13125 || this.f13124 >= 1000 || this.f13125 == 1) {
            m16934();
        } else {
            m16919();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16939(int i) {
        this.f13125 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16940(String str) {
        m16941(str, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16941(String str, int i) {
        if (!isFinishing()) {
            final Snackbar make = Snackbar.make(findViewById(R.id.drawer_layout_home), (CharSequence) str, i);
            make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    make.dismiss();
                }
            }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456)).show();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16942(ArrayList<MediaInfo> arrayList) {
        m16935(arrayList);
        int i = 3 >> 1;
        m16939(1);
        m16938();
    }
}
