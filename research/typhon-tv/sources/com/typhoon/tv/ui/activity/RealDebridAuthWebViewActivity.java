package com.typhoon.tv.ui.activity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.TextView;
import android.widget.Toast;
import com.androidadvance.topsnackbar.TSnackbar;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridGetTokenFailedEvent;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridGetTokenSuccessEvent;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridUserCancelledAuthEvent;
import com.typhoon.tv.ui.activity.base.BaseWebViewActivity;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import java.util.HashMap;
import rx.Subscription;
import rx.functions.Action1;

public class RealDebridAuthWebViewActivity extends BaseWebViewActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public TSnackbar f13192;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f13193;

    /* renamed from: 齉  reason: contains not printable characters */
    private Subscription f13194;

    /* renamed from: 龘  reason: contains not printable characters */
    private WebView f13195;

    private class HtmlViewerJavaScriptInterface {
        private HtmlViewerJavaScriptInterface() {
        }

        @JavascriptInterface
        public void showHTML(String str) {
            if (str != null && !str.isEmpty()) {
                if (str.toLowerCase().contains("application allowed")) {
                    Toast.makeText(RealDebridAuthWebViewActivity.this, I18N.m15706(R.string.please_wait), 1).show();
                    RealDebridAuthWebViewActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RealDebridAuthWebViewActivity.this.f13192 != null) {
                                RealDebridAuthWebViewActivity.this.f13192.m3911();
                            }
                            RealDebridAuthWebViewActivity.this.m16980(true);
                        }
                    });
                } else if (str.toLowerCase().contains("the code")) {
                    RealDebridAuthWebViewActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RealDebridAuthWebViewActivity.this.f13192 != null) {
                                RealDebridAuthWebViewActivity.this.f13192.m3917();
                            }
                            RealDebridAuthWebViewActivity.this.m16980(false);
                        }
                    });
                } else {
                    RealDebridAuthWebViewActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RealDebridAuthWebViewActivity.this.f13192 != null) {
                                RealDebridAuthWebViewActivity.this.f13192.m3911();
                            }
                            RealDebridAuthWebViewActivity.this.m16980(false);
                        }
                    });
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16980(boolean z) {
        int i = 8;
        try {
            findViewById(R.id.webView).setVisibility(z ? 8 : 0);
            findViewById(R.id.tvPleaseWait).setVisibility(z ? 0 : 8);
            View findViewById = findViewById(R.id.pbPleaseWait);
            if (z) {
                i = 0;
            }
            findViewById.setVisibility(i);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    public void finish() {
        if (!this.f13193) {
            RxBus.m15709().m15711(new RealDebridUserCancelledAuthEvent());
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_web_view);
        getSupportActionBar().m427(true);
        Bundle extras = getIntent().getExtras();
        if (extras.getString("verificationUrl", (String) null) == null || extras.getString("verificationUrl", (String) null).isEmpty() || extras.getString("userCode", (String) null) == null || extras.getString("userCode", (String) null).isEmpty() || !NetworkUtils.m17799()) {
            if (!NetworkUtils.m17799()) {
                Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
            } else {
                Toast.makeText(this, I18N.m15706(R.string.error), 1).show();
            }
            setResult(0);
            finish();
            return;
        }
        String string = extras.getString("verificationUrl");
        String string2 = extras.getString("userCode");
        setTitle("Real-Debrid Auth");
        if (DeviceUtils.m6389(new boolean[0])) {
            TextView textView = (TextView) findViewById(R.id.tvPleaseWait);
            textView.setTextSize(2, 24.0f);
            textView.setText(String.format("1) Visit \"%s\" in a browser of any of your devices\n2) Login to Real-Debrid\n3) Input the following code: %s\n\nThis window will be closed automatically after you have granted the Real-Debrid API access to Typhoon", new Object[]{string, string2}));
            m16980(true);
        } else {
            this.f13192 = TSnackbar.m3909(findViewById(R.id.webViewActivityRoot), "Enter the code: " + string2, -2);
            try {
                WebViewDatabase.getInstance(this).clearFormData();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            this.f13195 = (WebView) findViewById(R.id.webView);
            this.f13195.getSettings().setJavaScriptEnabled(true);
            this.f13195.getSettings().setAllowFileAccess(false);
            this.f13195.getSettings().setSaveFormData(false);
            this.f13195.getSettings().setSavePassword(false);
            this.f13195.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
            this.f13195.getSettings().setCacheMode(2);
            this.f13195.getSettings().setAppCacheEnabled(false);
            this.f13195.getSettings().setUserAgentString(TyphoonApp.f5841);
            try {
                this.f13195.clearCache(true);
                this.f13195.clearFormData();
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
            CookieManager.getInstance().setAcceptCookie(true);
            this.f13195.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    if (Build.VERSION.SDK_INT >= 16) {
                        webView.loadUrl("javascript:HtmlViewer.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                    } else {
                        webView.loadUrl("javascript:window.HtmlViewer.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                    }
                }

                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    RealDebridAuthWebViewActivity.this.m16980(true);
                    return false;
                }
            });
            this.f13195.addJavascriptInterface(new HtmlViewerJavaScriptInterface(), "HtmlViewer");
            HashMap hashMap = new HashMap();
            hashMap.put("Accept-Language", "en-US,en;q=0.5");
            this.f13195.loadUrl(string, hashMap);
        }
        this.f13194 = RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                if ((obj instanceof RealDebridGetTokenSuccessEvent) || (obj instanceof RealDebridGetTokenFailedEvent)) {
                    if (obj instanceof RealDebridGetTokenSuccessEvent) {
                        boolean unused = RealDebridAuthWebViewActivity.this.f13193 = true;
                    }
                    RealDebridAuthWebViewActivity.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13195 != null) {
            if (this.f13195.getParent() != null && (this.f13195.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.f13195.getParent()).removeView(this.f13195);
            }
            this.f13195.removeAllViews();
            this.f13195.destroy();
        }
        if (this.f13194 != null && !this.f13194.isUnsubscribed()) {
            this.f13194.unsubscribe();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                setResult(0);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.f13195 != null) {
            this.f13195.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f13195 != null) {
            this.f13195.onResume();
        }
    }
}
