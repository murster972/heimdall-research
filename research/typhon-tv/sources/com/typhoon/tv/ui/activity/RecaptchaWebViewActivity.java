package com.typhoon.tv.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.ui.activity.base.BaseWebViewActivity;
import com.typhoon.tv.utils.NetworkUtils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.HashMap;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.HttpUrl;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RecaptchaWebViewActivity extends BaseWebViewActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public Subscription f13201;

    /* renamed from: 龘  reason: contains not printable characters */
    private WebView f13202;

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16982() {
        if (this.f13201 != null && !this.f13201.isUnsubscribed()) {
            this.f13201.unsubscribe();
        }
        this.f13201 = null;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_web_view);
        getSupportActionBar().m427(true);
        Bundle extras = getIntent().getExtras();
        if (extras.getString("url") == null || extras.getString("url").isEmpty() || !NetworkUtils.m17799()) {
            setResult(0);
            finish();
            return;
        }
        setTitle(I18N.m15706(R.string.verify));
        final String string = extras.getString("url");
        final String string2 = extras.getString("userAgent", "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0");
        if (string == null) {
            setResult(0);
            finish();
            return;
        }
        try {
            WebViewDatabase.getInstance(this).clearFormData();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        this.f13202 = (WebView) findViewById(R.id.webView);
        this.f13202.getSettings().setJavaScriptEnabled(true);
        this.f13202.getSettings().setAllowFileAccess(false);
        this.f13202.getSettings().setSaveFormData(false);
        this.f13202.getSettings().setSavePassword(false);
        this.f13202.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        this.f13202.getSettings().setCacheMode(2);
        this.f13202.getSettings().setAppCacheEnabled(false);
        if (string2 != null) {
            this.f13202.getSettings().setUserAgentString(string2);
        }
        try {
            this.f13202.clearCache(true);
            this.f13202.clearFormData();
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        CookieManager.getInstance().setAcceptCookie(true);
        this.f13202.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                String title = webView.getTitle();
                if (title == null || title.isEmpty() || title.toLowerCase().contains("attention required") || title.equalsIgnoreCase("Watch Free MOvies Tv Shows Online 1080p HD Stream Free without registration at Mehlizmovieshd.com")) {
                    RecaptchaWebViewActivity.this.findViewById(R.id.webView).setVisibility(0);
                    RecaptchaWebViewActivity.this.findViewById(R.id.tvPleaseWait).setVisibility(8);
                    RecaptchaWebViewActivity.this.findViewById(R.id.pbPleaseWait).setVisibility(8);
                } else if (!RecaptchaWebViewActivity.this.isFinishing()) {
                    RecaptchaWebViewActivity.this.setResult(-1);
                    RecaptchaWebViewActivity.this.finish();
                }
            }

            public boolean shouldOverrideUrlLoading(WebView webView, final String str) {
                if (str.contains("/cdn-cgi/l/chk_captcha")) {
                    RecaptchaWebViewActivity.this.findViewById(R.id.webView).setVisibility(8);
                    RecaptchaWebViewActivity.this.findViewById(R.id.tvPleaseWait).setVisibility(0);
                    RecaptchaWebViewActivity.this.findViewById(R.id.pbPleaseWait).setVisibility(0);
                    RecaptchaWebViewActivity.this.m16982();
                    Subscription unused = RecaptchaWebViewActivity.this.f13201 = Observable.m7359(new Observable.OnSubscribe<String>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        private String m16985(String str) {
                            try {
                                HttpUrl r2 = HttpUrl.m6937(str);
                                return r2.m6961() + "://" + r2.m6951();
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                                return "";
                            }
                        }

                        /* renamed from: 龘  reason: contains not printable characters */
                        public void call(Subscriber<? super String> subscriber) {
                            HashMap hashMap = new HashMap();
                            hashMap.put(AbstractSpiCall.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                            hashMap.put("Accept-Language", "en-US,en;q=0.5");
                            String replace = m16985(string).replace("https://", "").replace("http://", "");
                            if (!replace.replace(StringUtils.SPACE, "").isEmpty()) {
                                hashMap.put("Host", replace);
                            }
                            HttpHelper.m6343().m6359(str, string2 == null ? "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0" : string2, !string.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? string + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR : string, (Map<String, String>[]) new Map[]{hashMap});
                            subscriber.onCompleted();
                        }
                    }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<String>() {
                        public void onCompleted() {
                            if (!RecaptchaWebViewActivity.this.isFinishing()) {
                                RecaptchaWebViewActivity.this.setResult(-1);
                                RecaptchaWebViewActivity.this.finish();
                            }
                        }

                        public void onError(Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                            if (!RecaptchaWebViewActivity.this.isFinishing()) {
                                RecaptchaWebViewActivity.this.setResult(0);
                                RecaptchaWebViewActivity.this.finish();
                            }
                        }

                        /* renamed from: 龘  reason: contains not printable characters */
                        public void onNext(String str) {
                        }
                    });
                } else {
                    webView.loadUrl(str);
                }
                return true;
            }
        });
        this.f13202.loadUrl(string);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        m16982();
        if (this.f13202 != null) {
            if (this.f13202.getParent() != null && (this.f13202.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.f13202.getParent()).removeView(this.f13202);
            }
            this.f13202.removeAllViews();
            this.f13202.destroy();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                setResult(0);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f13202.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f13202.onResume();
    }
}
