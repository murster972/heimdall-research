package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.thunderrise.animations.PulseAnimation;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.TapTargetViewHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.helper.trakt.TraktHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvEpisodeInfo;
import com.typhoon.tv.model.media.tv.TvLatestPlayed;
import com.typhoon.tv.model.media.tv.TvSeasonInfo;
import com.typhoon.tv.model.media.tv.TvWatchedEpisode;
import com.typhoon.tv.presenter.IEpisodePresenter;
import com.typhoon.tv.presenter.impl.EpisodePresenterImpl;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.TvSeasonInfoArrayAdapter;
import com.typhoon.tv.ui.adapter.ViewPagerStateAdapter;
import com.typhoon.tv.ui.fragment.EpisodeDetailsFragment;
import com.typhoon.tv.ui.widget.SlidingTabLayout;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.IEpisodeView;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EpisodeListActivity extends BaseAdActivity implements IEpisodeView {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int f13083 = Color.parseColor("#FF669900");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f13084 = Color.parseColor("#80669900");

    /* renamed from: ʻ  reason: contains not printable characters */
    private ViewPagerStateAdapter f13085;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public MenuItem f13086;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public MediaInfo f13087;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f13088 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f13089 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public TvSeasonInfo f13090;

    /* renamed from: ٴ  reason: contains not printable characters */
    private ArrayList<TvEpisodeInfo> f13091;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean f13092 = false;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public ViewPager f13093;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public SlidingTabLayout f13094;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public IEpisodePresenter f13095;

    private interface OnSetEpisodeAsWatched {
        /* renamed from: 靐  reason: contains not printable characters */
        void m16895(boolean z);

        /* renamed from: 龘  reason: contains not printable characters */
        void m16896(boolean z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m16862() {
        if (this.f13091 == null || this.f13091.isEmpty()) {
            return false;
        }
        int i = -1;
        TvLatestPlayed r9 = TVApplication.m6287().m6307(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()));
        if (r9 != null) {
            i = r9.getEpisode();
        }
        int color = ContextCompat.getColor(TVApplication.m6288(), DeviceUtils.m6389(new boolean[0]) ? R.color.light_blue : R.color.light_blue_transparent_highlight);
        SparseIntArray highlightMap = this.f13094.getHighlightMap();
        if (highlightMap == null) {
            highlightMap = new SparseIntArray();
        }
        int i2 = -1;
        for (int i3 = 0; i3 < this.f13091.size(); i3++) {
            TvEpisodeInfo tvEpisodeInfo = this.f13091.get(i3);
            if (i != -1 && tvEpisodeInfo.getEpisode() == i) {
                i2 = i3;
            }
        }
        if (i2 <= -1) {
            return false;
        }
        List<TvWatchedEpisode> r2 = TVApplication.m6287().m6303(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()));
        ArrayList arrayList = new ArrayList();
        if (r2 != null) {
            for (TvWatchedEpisode episode : r2) {
                arrayList.add(Integer.valueOf(episode.getEpisode()));
            }
        }
        for (int i4 = 0; i4 < highlightMap.size(); i4++) {
            int keyAt = highlightMap.keyAt(i4);
            if (highlightMap.get(keyAt, -1) == color) {
                if (arrayList.contains(Integer.valueOf(this.f13091.get(keyAt).getEpisode()))) {
                    highlightMap.put(keyAt, DeviceUtils.m6389(new boolean[0]) ? f13083 : f13084);
                } else {
                    highlightMap.delete(keyAt);
                }
            }
        }
        if (i2 > -1) {
            highlightMap.put(i2, color);
        }
        this.f13094.setHighlightMap(highlightMap);
        this.f13094.m17642();
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m16866() {
        if (this.f13093 == null || this.f13091 == null || this.f13091.size() <= 0) {
            m16886(I18N.m15706(R.string.error));
            return;
        }
        int episode = this.f13091.get(this.f13093.getCurrentItem()).getEpisode();
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("mediaInfo", this.f13087);
        intent.putExtra("season", this.f13090.getSeasonNum());
        intent.putExtra("episode", episode);
        if (this.f13093.getCurrentItem() < this.f13085.getCount()) {
            ArrayList arrayList = new ArrayList();
            for (int currentItem = this.f13093.getCurrentItem() + 1; currentItem < this.f13085.getCount(); currentItem++) {
                arrayList.add(Integer.valueOf(this.f13091.get(currentItem).getEpisode()));
            }
            intent.putIntegerArrayListExtra("nextEpisodeList", arrayList);
        }
        startActivityForResult(intent, 1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m16868() {
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey("mediaInfo") || !extras.containsKey("selectedSeasonInfo") || !extras.containsKey("seasonInfoList") || extras.getParcelable("mediaInfo") == null || extras.getParcelable("selectedSeasonInfo") == null || extras.getParcelableArrayList("seasonInfoList") == null) {
            Toast.makeText(this, I18N.m15706(R.string.error), 1).show();
            finish();
            return;
        }
        this.f13087 = (MediaInfo) extras.getParcelable("mediaInfo");
        this.f13090 = (TvSeasonInfo) extras.getParcelable("selectedSeasonInfo");
        ArrayList parcelableArrayList = extras.getParcelableArrayList("seasonInfoList");
        for (int i = 0; i < parcelableArrayList.size(); i++) {
            if (((TvSeasonInfo) parcelableArrayList.get(i)).equals(this.f13090)) {
                this.f13088 = i;
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEpisodeList);
        setSupportActionBar(toolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.m427(true);
            supportActionBar.m433(false);
            supportActionBar.m440((CharSequence) "");
        }
        Spinner spinner = (Spinner) findViewById(R.id.toolbar_spinner_episode_list);
        final TvSeasonInfoArrayAdapter tvSeasonInfoArrayAdapter = new TvSeasonInfoArrayAdapter(this, 17367048, (TvSeasonInfo[]) parcelableArrayList.toArray(new TvSeasonInfo[parcelableArrayList.size()]));
        tvSeasonInfoArrayAdapter.setDropDownViewResource(17367049);
        spinner.setAdapter(tvSeasonInfoArrayAdapter);
        spinner.setSelection(this.f13088);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                if (EpisodeListActivity.this.f13089) {
                    boolean unused = EpisodeListActivity.this.f13089 = false;
                    return;
                }
                TvSeasonInfo r0 = tvSeasonInfoArrayAdapter.getItem(i);
                if (r0 != null) {
                    TvSeasonInfo unused2 = EpisodeListActivity.this.f13090 = r0;
                    int unused3 = EpisodeListActivity.this.f13088 = i;
                    boolean unused4 = EpisodeListActivity.this.f13092 = false;
                    EpisodeListActivity.this.m16876(true);
                    EpisodeListActivity.this.f13095.m16131();
                    EpisodeListActivity.this.f13095.m16132(EpisodeListActivity.this.f13087, EpisodeListActivity.this.f13090.getSeasonNum());
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        String posterUrl = this.f13087.getPosterUrl();
        String bannerUrl = this.f13087.getBannerUrl();
        String str = (posterUrl.isEmpty() || bannerUrl.isEmpty()) ? (!DeviceUtils.m6389(new boolean[0]) || bannerUrl.isEmpty()) ? posterUrl : bannerUrl : DeviceUtils.m6389(new boolean[0]) ? bannerUrl : posterUrl;
        if (!str.isEmpty()) {
            ImageView imageView = (ImageView) findViewById(R.id.ivEpisodeListBg);
            imageView.setVisibility(0);
            if (Build.VERSION.SDK_INT >= 16) {
                imageView.setImageAlpha(30);
            } else {
                imageView.setAlpha(30);
            }
            Glide.m3938((FragmentActivity) this).m3974(str).m25191(DiskCacheStrategy.SOURCE).m25198().m25181().m25209(imageView);
        }
        this.f13093 = (ViewPager) findViewById(R.id.viewPagerEpisodeList);
        this.f13093.setSaveEnabled(false);
        this.f13094 = (SlidingTabLayout) findViewById(R.id.tabsEpisodeList);
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        this.f13094.setSelectedIndicatorColors(typedValue.data);
        this.f13094.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                EpisodeListActivity.this.invalidateOptionsMenu();
            }
        });
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m16869(int i, boolean z) {
        if (this.f13091 != null && !this.f13091.isEmpty()) {
            boolean z2 = false;
            SparseIntArray highlightMap = this.f13094.getHighlightMap();
            if (highlightMap == null) {
                highlightMap = new SparseIntArray();
            }
            for (int i2 = 0; i2 < this.f13091.size(); i2++) {
                if (this.f13091.get(i2).getEpisode() == i) {
                    if (z && highlightMap.get(i2, -1) == -1) {
                        highlightMap.put(i2, DeviceUtils.m6389(new boolean[0]) ? f13083 : f13084);
                        z2 = true;
                    } else if (!z && highlightMap.get(i2, -1) != -1) {
                        highlightMap.delete(i2);
                        z2 = true;
                    }
                }
            }
            if (z2) {
                this.f13094.setHighlightMap(highlightMap);
            }
            if (!m16862()) {
                this.f13094.m17642();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m16872() {
        m17423(findViewById(R.id.adViewEpisodeList));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m16874() {
        this.f13095 = new EpisodePresenterImpl(this);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m16876(boolean z) {
        invalidateOptionsMenu();
        if (z) {
            findViewById(R.id.pbEpisodeList).setVisibility(0);
            this.f13093.setVisibility(8);
            this.f13094.setVisibility(8);
            if (!DeviceUtils.m6389(new boolean[0])) {
                ((FloatingActionButton) findViewById(R.id.fabEpisodeList)).setVisibility(8);
                return;
            }
            return;
        }
        findViewById(R.id.pbEpisodeList).setVisibility(8);
        this.f13093.setVisibility(0);
        this.f13094.setVisibility(0);
        if (!DeviceUtils.m6389(new boolean[0])) {
            ((FloatingActionButton) findViewById(R.id.fabEpisodeList)).setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16879(int i, boolean z) {
        if (z) {
            TVApplication.m6287().m6300(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()), Integer.valueOf(i));
        } else {
            TVApplication.m6287().m6310(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()), Integer.valueOf(i));
        }
        m16869(i, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16880(int i, boolean z, OnSetEpisodeAsWatched onSetEpisodeAsWatched) {
        if (!TraktCredentialsHelper.m16110().isValid() || !TVApplication.m6285().getBoolean("pref_auto_add_watched_episode_trakt", true)) {
            onSetEpisodeAsWatched.m16896(false);
            m16879(i, z);
        } else if (!NetworkUtils.m17799()) {
            onSetEpisodeAsWatched.m16895(true);
        } else {
            try {
                if (this.f13086 != null) {
                    this.f13086.setEnabled(false);
                }
                final Snackbar r2 = m16884("Sending to Trakt...", -2, false);
                final int i2 = i;
                final boolean z2 = z;
                final OnSetEpisodeAsWatched onSetEpisodeAsWatched2 = onSetEpisodeAsWatched;
                m17298(this.f13087, this.f13090.getSeasonNum(), i, z, false, new Callback<SyncResponse>() {
                    public void onFailure(Call<SyncResponse> call, Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                        if (EpisodeListActivity.this.f13086 != null) {
                            EpisodeListActivity.this.f13086.setEnabled(true);
                        }
                        if (r2 != null && r2.isShownOrQueued()) {
                            r2.dismiss();
                        }
                        TraktHelper.m16113();
                        onSetEpisodeAsWatched2.m16895(true);
                    }

                    public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
                        if (EpisodeListActivity.this.f13086 != null) {
                            EpisodeListActivity.this.f13086.setEnabled(true);
                        }
                        if (r2 != null && r2.isShownOrQueued()) {
                            r2.dismiss();
                        }
                        if (response.m24390()) {
                            EpisodeListActivity.this.m16879(i2, z2);
                            onSetEpisodeAsWatched2.m16896(true);
                            return;
                        }
                        onSetEpisodeAsWatched2.m16895(true);
                    }
                });
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                m16884("Failed to send to Trakt...", 0, true);
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (!m17282() || ((keyEvent.getAction() != 1 || (keyEvent.getKeyCode() != 126 && keyEvent.getKeyCode() != 85)) && (!keyEvent.isLongPress() || keyEvent.getKeyCode() != 23))) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (!this.f13092) {
            return true;
        }
        m16866();
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (m17422(true) != false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r7, int r8, android.content.Intent r9) {
        /*
            r6 = this;
            r5 = 1
            r4 = 0
            r3 = -1
            super.onActivityResult(r7, r8, r9)
            if (r8 != r3) goto L_0x007f
            if (r7 != r5) goto L_0x007f
            if (r9 == 0) goto L_0x007f
            android.os.Bundle r1 = r9.getExtras()
            if (r1 == 0) goto L_0x007f
            boolean r1 = com.typhoon.tv.TyphoonApp.f5865
            if (r1 != 0) goto L_0x0043
            android.os.Bundle r1 = r9.getExtras()
            java.lang.String r2 = "isInterstitialShown"
            boolean r1 = r1.getBoolean(r2, r4)
            if (r1 != 0) goto L_0x0043
            boolean r1 = com.typhoon.tv.TyphoonApp.m6333()
            if (r1 != 0) goto L_0x0040
            android.os.Bundle r1 = r9.getExtras()
            java.lang.String r2 = "hasLink"
            boolean r1 = r1.getBoolean(r2, r4)
            if (r1 == 0) goto L_0x0043
            boolean[] r1 = new boolean[r5]
            r1[r4] = r5
            boolean r1 = r6.m17422((boolean[]) r1)
            if (r1 == 0) goto L_0x0043
        L_0x0040:
            r6.m17421()
        L_0x0043:
            android.os.Bundle r1 = r9.getExtras()
            java.lang.String r2 = "isWatchedAnyLink"
            boolean r1 = r1.getBoolean(r2, r4)
            if (r1 == 0) goto L_0x007f
            android.os.Bundle r1 = r9.getExtras()
            java.lang.String r2 = "episode"
            int r1 = r1.getInt(r2, r3)
            if (r1 == r3) goto L_0x007f
            android.os.Bundle r1 = r9.getExtras()
            java.lang.String r2 = "episode"
            int r0 = r1.getInt(r2, r3)
            if (r0 <= r3) goto L_0x007f
            android.os.Bundle r1 = r9.getExtras()
            java.lang.String r2 = "needMarkAsWatched"
            boolean r1 = r1.getBoolean(r2, r4)
            if (r1 == 0) goto L_0x0080
            com.typhoon.tv.ui.activity.EpisodeListActivity$6 r1 = new com.typhoon.tv.ui.activity.EpisodeListActivity$6
            r1.<init>()
            r6.m16880((int) r0, (boolean) r5, (com.typhoon.tv.ui.activity.EpisodeListActivity.OnSetEpisodeAsWatched) r1)
        L_0x007f:
            return
        L_0x0080:
            r6.m16862()
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.ui.activity.EpisodeListActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (IllegalStateException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_episode_list);
        m17284();
        this.f13089 = true;
        m16868();
        m16874();
        if (!TyphoonApp.f5865) {
            m16872();
        }
        if (NetworkUtils.m17799()) {
            this.f13095.m16132(this.f13087, this.f13090.getSeasonNum());
        } else {
            m16885();
            m16886(I18N.m15706(R.string.no_internet));
        }
        m17419();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_episode_list, menu);
        if (!(this.f13093 == null || this.f13091 == null || this.f13091.size() <= 0)) {
            int episode = this.f13091.get(this.f13093.getCurrentItem()).getEpisode();
            this.f13086 = menu.findItem(R.id.action_set_watched);
            if (TVApplication.m6287().m6321(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()), Integer.valueOf(episode))) {
                this.f13086.setIcon(R.drawable.ic_check_box_white_24dp);
                this.f13086.setTitle(I18N.m15706(R.string.action_remove_watched));
            }
        }
        MenuItem findItem = menu.findItem(R.id.action_play);
        if (this.f13092) {
            findItem.setVisible(true);
            if (!TapTargetViewHelper.m15965("ttv_episode_list") && !DeviceUtils.m6389(new boolean[0])) {
                final ArrayList arrayList = new ArrayList();
                try {
                    arrayList.add(TapTarget.forView(findViewById(R.id.toolbar_spinner_episode_list), I18N.m15706(R.string.ttv_switch_season), I18N.m15706(R.string.ttv_switch_season_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(false).id(1));
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEpisodeList);
                if (toolbar != null) {
                    try {
                        arrayList.add(TapTarget.forToolbarMenuItem(toolbar, (int) R.id.action_set_watched, (CharSequence) I18N.m15706(R.string.ttv_set_episode_as_watched), (CharSequence) I18N.m15706(R.string.ttv_set_episode_as_watched_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(false).id(2));
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                    if (DeviceUtils.m6389(new boolean[0])) {
                        try {
                            arrayList.add(TapTarget.forToolbarMenuItem(toolbar, (int) R.id.action_play, (CharSequence) I18N.m15706(R.string.ttv_watch_now), (CharSequence) I18N.m15706(R.string.ttv_watch_now_desc)).dimColor(17170444).outerCircleColor(R.color.blue).targetCircleColor(17170444).titleTextColor(R.color.text_color).descriptionTextColor(R.color.secondary_text_color).transparentTarget(true).drawShadow(true).tintTarget(true).cancelable(true).id(3));
                        } catch (Exception e3) {
                            Logger.m6281((Throwable) e3, new boolean[0]);
                        }
                    }
                }
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        try {
                            new TapTargetSequence((Activity) EpisodeListActivity.this).targets((List<TapTarget>) arrayList).start();
                            TapTargetViewHelper.m15964("ttv_episode_list");
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                }, 1000);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f13095.m16130();
        this.f13095 = null;
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        int i = 0;
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            case R.id.action_play:
                m16866();
                return true;
            case R.id.action_reverse:
                if (this.f13093 == null || this.f13091 == null || this.f13091.size() <= 0) {
                    return true;
                }
                int i2 = TVApplication.m6285().getInt("pref_episode_reverse_order", 0);
                this.f13093.setCurrentItem(i2 == 1 ? 0 : this.f13091.size() - 1, true);
                invalidateOptionsMenu();
                SharedPreferences.Editor edit = TVApplication.m6285().edit();
                if (i2 == 0) {
                    i = 1;
                }
                edit.putInt("pref_episode_reverse_order", i).apply();
                return true;
            case R.id.action_set_watched:
                if (this.f13091 == null || this.f13091.size() <= 0 || this.f13093 == null || this.f13093.getAdapter() == null) {
                    return true;
                }
                int episode = this.f13091.get(this.f13093.getCurrentItem()).getEpisode();
                if (TVApplication.m6287().m6321(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()), Integer.valueOf(episode))) {
                    m16880(episode, false, (OnSetEpisodeAsWatched) new OnSetEpisodeAsWatched() {
                        /* renamed from: 靐  reason: contains not printable characters */
                        public void m16888(boolean z) {
                            if (!z) {
                                return;
                            }
                            if (!NetworkUtils.m17799()) {
                                EpisodeListActivity.this.m16884(I18N.m15706(R.string.no_internet), 0, true);
                            } else {
                                EpisodeListActivity.this.m16884("Failed to send to Trakt...", 0, true);
                            }
                        }

                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m16889(boolean z) {
                            menuItem.setIcon(R.drawable.ic_check_box_outline_blank_white_24dp);
                            menuItem.setTitle(I18N.m15706(R.string.action_set_watched));
                            if (z) {
                                EpisodeListActivity.this.m16884("Sent to Trakt successfully!", 0, true);
                            }
                        }
                    });
                    return true;
                }
                m16880(episode, true, (OnSetEpisodeAsWatched) new OnSetEpisodeAsWatched() {
                    /* renamed from: 靐  reason: contains not printable characters */
                    public void m16890(boolean z) {
                        if (!z) {
                            return;
                        }
                        if (!NetworkUtils.m17799()) {
                            EpisodeListActivity.this.m16884(I18N.m15706(R.string.no_internet), 0, true);
                        } else {
                            EpisodeListActivity.this.m16884("Failed to send to Trakt...", 0, true);
                        }
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m16891(boolean z) {
                        menuItem.setIcon(R.drawable.ic_check_box_white_24dp);
                        menuItem.setTitle(I18N.m15706(R.string.action_remove_watched));
                        int currentItem = EpisodeListActivity.this.f13093.getCurrentItem();
                        if (EpisodeListActivity.this.f13093.getAdapter().getCount() > currentItem + 1) {
                            EpisodeListActivity.this.f13093.setCurrentItem(currentItem + 1);
                        }
                        if (z) {
                            EpisodeListActivity.this.m16884("Sent to Trakt successfully!", 0, true);
                        }
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Snackbar m16884(String str, int i, boolean z) {
        final Snackbar make = Snackbar.make(findViewById(R.id.episode_list_rootLayout), (CharSequence) str, i);
        if (z) {
            make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    make.dismiss();
                }
            });
        }
        make.setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        make.show();
        return make;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16885() {
        findViewById(R.id.pbEpisodeList).setVisibility(8);
        findViewById(R.id.viewEmptyEpisodeList).setVisibility(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16886(String str) {
        m16884(str, -1, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16887(ArrayList<TvEpisodeInfo> arrayList) {
        if (arrayList == null) {
            m16885();
            return;
        }
        this.f13091 = arrayList;
        this.f13092 = true;
        int seasonNum = this.f13090.getSeasonNum();
        SparseIntArray sparseIntArray = new SparseIntArray();
        ArrayList arrayList2 = new ArrayList();
        List<TvWatchedEpisode> r18 = TVApplication.m6287().m6303(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(seasonNum));
        if (r18 != null) {
            for (TvWatchedEpisode next : r18) {
                if (next.getSeason() == seasonNum) {
                    arrayList2.add(Integer.valueOf(next.getEpisode()));
                }
            }
        }
        int i = -1;
        int i2 = -1;
        int i3 = -1;
        if (this.f13093 != null && this.f13091.size() > 0) {
            i = TVApplication.m6285().getInt("pref_episode_reverse_order", 0);
            i2 = TVApplication.m6287().m6293(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()));
        }
        int i4 = -1;
        int i5 = -1;
        TvLatestPlayed r12 = TVApplication.m6287().m6307(Integer.valueOf(this.f13087.getTmdbId()), Integer.valueOf(this.f13090.getSeasonNum()));
        if (r12 != null) {
            i4 = r12.getEpisode();
        }
        this.f13085 = new ViewPagerStateAdapter(getSupportFragmentManager());
        for (int i6 = 0; i6 < arrayList.size(); i6++) {
            TvEpisodeInfo tvEpisodeInfo = arrayList.get(i6);
            int episode = tvEpisodeInfo.getEpisode();
            this.f13085.m17534(EpisodeDetailsFragment.m17555(tvEpisodeInfo), seasonNum + "x" + Utils.m6413(episode));
            if (episode == i2) {
                i3 = i6 + 1;
            }
            if (episode == i4) {
                i5 = i6;
            }
            if (arrayList2.contains(Integer.valueOf(episode))) {
                sparseIntArray.put(i6, DeviceUtils.m6389(new boolean[0]) ? f13083 : f13084);
            }
        }
        if (i5 > -1) {
            sparseIntArray.put(i5, ContextCompat.getColor(TVApplication.m6288(), DeviceUtils.m6389(new boolean[0]) ? R.color.light_blue : R.color.light_blue_transparent_highlight));
        }
        this.f13093.setAdapter(this.f13085);
        this.f13094.setCustomTabViewWithHighlight(R.layout.tabstrip_item_transparent, R.id.tvTabStrip, sparseIntArray);
        this.f13094.setViewPager(this.f13093);
        if (DeviceUtils.m6389(new boolean[0]) && TVApplication.m6285().getBoolean("pref_simple_scrolling_on_tv", false)) {
            this.f13094.setOnTabFocusChangeListener(new SlidingTabLayout.OnTabFocusChangeListener() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m16894(int i, boolean z) {
                    if (z) {
                        EpisodeListActivity.this.f13094.performClick();
                        EpisodeListActivity.this.f13093.setCurrentItem(i);
                    }
                }
            });
            Snackbar make = Snackbar.make(findViewById(R.id.episode_list_rootLayout), (CharSequence) I18N.m15706(R.string.simple_scrolling_is_enabled), 0);
            final Snackbar snackbar = make;
            make.setAction((CharSequence) I18N.m15706(R.string.action_settings), (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    EpisodeListActivity.this.startActivity(new Intent(EpisodeListActivity.this, SettingsActivity.class));
                    snackbar.dismiss();
                }
            }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
            make.show();
        }
        m16876(false);
        if (i > -1) {
            this.f13093.setCurrentItem(i == 0 ? 0 : this.f13091.size() - 1, true);
        }
        if (i3 > -1 && i3 < this.f13085.getCount()) {
            this.f13093.setCurrentItem(i3, true);
        }
        if (!DeviceUtils.m6389(new boolean[0])) {
            FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fabEpisodeList);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    EpisodeListActivity.this.m16866();
                }
            });
            floatingActionButton.setBackgroundColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
            floatingActionButton.setVisibility(0);
            try {
                PulseAnimation.m15698().m15703((View) floatingActionButton).m15702(600).m15701(-1).m15699(2).m15700();
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        } else {
            ((LovelyInfoDialog) ((LovelyInfoDialog) ((LovelyInfoDialog) ((LovelyInfoDialog) new LovelyInfoDialog(this).m18218(R.color.blue)).m18220((int) R.drawable.ic_media_play_dark)).m18227(25).m18230(true).m18214((CharSequence) I18N.m15706(R.string.try_it))).m18221((CharSequence) I18N.m15706(R.string.click_play_button_on_your_rc))).m18228();
        }
    }
}
