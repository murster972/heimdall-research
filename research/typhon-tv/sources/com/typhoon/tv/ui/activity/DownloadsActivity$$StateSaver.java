package com.typhoon.tv.ui.activity;

import android.os.Bundle;
import com.evernote.android.state.Bundler;
import com.evernote.android.state.InjectionHelper;
import com.typhoon.tv.ui.activity.DownloadsActivity;
import com.typhoon.tv.ui.activity.base.BaseAdActivity$$StateSaver;
import java.util.HashMap;

public class DownloadsActivity$$StateSaver<T extends DownloadsActivity> extends BaseAdActivity$$StateSaver<T> {
    private static final HashMap<String, Bundler<?>> BUNDLERS = new HashMap<>();
    private static final InjectionHelper HELPER = new InjectionHelper("com.typhoon.tv.ui.activity.DownloadsActivity$$StateSaver", BUNDLERS);

    public void restore(T t, Bundle bundle) {
        super.restore(t, bundle);
        t.mIsInterstitialShown = HELPER.getBoolean(bundle, "mIsInterstitialShown");
        t.mAdShownTimes = HELPER.getInt(bundle, "mAdShownTimes");
    }

    public void save(T t, Bundle bundle) {
        super.save(t, bundle);
        HELPER.putBoolean(bundle, "mIsInterstitialShown", t.mIsInterstitialShown);
        HELPER.putInt(bundle, "mAdShownTimes", t.mAdShownTimes);
    }
}
