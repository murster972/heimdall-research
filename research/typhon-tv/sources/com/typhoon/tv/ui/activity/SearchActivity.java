package com.typhoon.tv.ui.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.ViewPagerStateAdapter;
import com.typhoon.tv.ui.fragment.SearchFragment;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.ToolbarUtils;
import java.util.List;

public class SearchActivity extends BaseAdActivity {

    /* renamed from: 靐  reason: contains not printable characters */
    private ViewPager f13209;

    /* renamed from: 麤  reason: contains not printable characters */
    private SearchView f13210;

    /* renamed from: 齉  reason: contains not printable characters */
    private MenuItem f13211;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f13212;

    /* renamed from: 齉  reason: contains not printable characters */
    private void m16987(String str) {
        List<Fragment> r3;
        if (str != null && !str.isEmpty()) {
            if (this.f13210 != null) {
                this.f13210.setQuery(str, false);
                this.f13210.clearFocus();
            }
            if (this.f13211 != null) {
                this.f13211.collapseActionView();
            }
            this.f13212 = str.replaceAll("(\\s+\\((\\d{4})\\))$", "");
            setTitle(I18N.m15707(R.string.search_result_of, this.f13212));
            if (!NetworkUtils.m17799()) {
                m16991(I18N.m15706(R.string.no_internet));
                return;
            }
            boolean z = false;
            try {
                PagerAdapter adapter = this.f13209.getAdapter();
                if ((adapter instanceof ViewPagerStateAdapter) && (r3 = ((ViewPagerStateAdapter) adapter).m17533()) != null && !r3.isEmpty()) {
                    for (Fragment next : r3) {
                        if (next instanceof SearchFragment) {
                            ((SearchFragment) next).m17600(this.f13212);
                            z = true;
                        }
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            if (!z) {
                m16991(I18N.m15706(R.string.error));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16988() {
        m17423(findViewById(R.id.adViewSearch));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16989(int i, boolean z) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSearch);
        setSupportActionBar(toolbar);
        ToolbarUtils.m17837(TVApplication.m6288(), toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.m427(true);
            supportActionBar.m433(true);
        }
        this.f13209 = (ViewPager) findViewById(R.id.viewpagerSearch);
        this.f13209.setSaveEnabled(false);
        this.f13209.setOffscreenPageLimit(2);
        ViewPagerStateAdapter viewPagerStateAdapter = new ViewPagerStateAdapter(getSupportFragmentManager());
        viewPagerStateAdapter.m17534(SearchFragment.m17594(0, z), I18N.m15706(R.string.tv_shows));
        viewPagerStateAdapter.m17534(SearchFragment.m17594(1, z), I18N.m15706(R.string.movies));
        this.f13209.setAdapter(viewPagerStateAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsSearch);
        tabLayout.setupWithViewPager(this.f13209);
        tabLayout.setVisibility(0);
        this.f13209.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(this.f13209));
        try {
            if (tabLayout.getTabCount() >= 2) {
                tabLayout.getTabAt(i == 0 ? 0 : 1).select();
            } else if (tabLayout.getTabCount() > 0) {
                tabLayout.getTabAt(0).select();
            } else {
                tabLayout.setVisibility(8);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16990(Intent intent) {
        if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.SEARCH")) {
            m16987(intent.getStringExtra("query"));
        }
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (IllegalStateException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_search);
        m17284();
        boolean z = false;
        Intent intent = getIntent();
        if (!(intent == null || intent.getAction() == null || !intent.getAction().equals("android.intent.action.SEARCH")) || (this.f13212 != null && !this.f13212.isEmpty())) {
            z = true;
        }
        int i = 0;
        if (intent != null) {
            i = intent.getIntExtra("mediaType", 0);
        }
        m16989(i, z);
        m16988();
        if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.SEARCH")) {
            m16990(intent);
        } else if (this.f13212 != null && !this.f13212.isEmpty()) {
            m16987(this.f13212);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        m17295(menu);
        MenuItem findItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) findItem.getActionView();
        searchView.setSearchableInfo(((SearchManager) getSystemService("search")).getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setIconifiedByDefault(true);
        searchView.setMaxWidth(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
        searchView.setQueryHint(I18N.m15706(R.string.search_something));
        m17296(findItem, searchView, true);
        findItem.expandActionView();
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        this.f13210 = searchView;
        this.f13211 = findItem;
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        m16990(intent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16991(String str) {
        if (!isFinishing()) {
            final Snackbar make = Snackbar.make(findViewById(R.id.search_rootLayout), (CharSequence) str, -1);
            make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    make.dismiss();
                }
            }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456)).show();
        }
    }
}
