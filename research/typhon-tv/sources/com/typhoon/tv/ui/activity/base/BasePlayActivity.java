package com.typhoon.tv.ui.activity.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.player.ExoPlayerHelper;
import com.typhoon.tv.model.LastPlaybackInfo;
import com.typhoon.tv.model.media.MediaInfo;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;

@SuppressLint({"Registered"})
public class BasePlayActivity extends BaseAdActivity {
    /* access modifiers changed from: protected */

    /* renamed from: 龘  reason: contains not printable characters */
    public BasePlayerHelper f13721;

    public interface OnReceiveLastPlaybackPositionListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17433(long j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ـ  reason: contains not printable characters */
    public void m17429() {
        BasePlayerHelper.m16036((Activity) this, (BasePlayerHelper.OnChoosePlayerListener) new BasePlayerHelper.OnChoosePlayerListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17432(String str) {
                if (str != null && !str.isEmpty()) {
                    for (BasePlayerHelper basePlayerHelper : BasePlayerHelper.m16034()) {
                        if (basePlayerHelper.m16041().equalsIgnoreCase(str)) {
                            BasePlayActivity.this.f13721 = basePlayerHelper;
                            return;
                        }
                    }
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17430(BasePlayerHelper basePlayerHelper, boolean z, int i, int i2, int i3, Intent intent, boolean z2) {
        if (TVApplication.m6285().getInt("pref_default_continue_playback_option", 1) != 0 || (basePlayerHelper instanceof ExoPlayerHelper)) {
            long r8 = basePlayerHelper.m16048(intent);
            long r10 = basePlayerHelper.m16043(intent);
            if (r8 > 0 && r10 > 0 && r10 > r8) {
                long j = (long) ((((float) r8) / ((float) r10)) * 100.0f);
                if (j > 5 && j < 95) {
                    TVApplication.m6287().m6317(z, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), r8, r10);
                } else if (j >= 95) {
                    TVApplication.m6287().m6316(z, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
                }
            } else if (!z2) {
                TVApplication.m6287().m6316(z, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
            }
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DefaultLocale"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17431(MediaInfo mediaInfo, int i, int i2, String str, OnReceiveLastPlaybackPositionListener onReceiveLastPlaybackPositionListener) {
        int i3 = TVApplication.m6285().getInt("pref_default_continue_playback_option", 1);
        if (i3 == 0) {
            onReceiveLastPlaybackPositionListener.m17433(-1);
        } else if (i3 == 2) {
            onReceiveLastPlaybackPositionListener.m17433(0);
        } else {
            try {
                LastPlaybackInfo r8 = TVApplication.m6287().m6295(mediaInfo.getType() == 1, Integer.valueOf(mediaInfo.getTmdbId()), Integer.valueOf(i), Integer.valueOf(i2));
                if (r8 == null || r8.getPosition() <= -1 || r8.getDuration() <= -1 || r8.getDuration() <= r8.getPosition()) {
                    onReceiveLastPlaybackPositionListener.m17433(-1);
                    return;
                }
                final long position = r8.getPosition();
                long duration = r8.getDuration();
                long j = (long) ((((float) position) / ((float) duration)) * 100.0f);
                if (i3 == 3) {
                    onReceiveLastPlaybackPositionListener.m17433(position);
                } else if (i3 == 1) {
                    final OnReceiveLastPlaybackPositionListener onReceiveLastPlaybackPositionListener2 = onReceiveLastPlaybackPositionListener;
                    final OnReceiveLastPlaybackPositionListener onReceiveLastPlaybackPositionListener3 = onReceiveLastPlaybackPositionListener;
                    new AlertDialog.Builder(this).m536((CharSequence) str).m538(true).m523((CharSequence) I18N.m15706(R.string.do_you_wish_to_resume) + StringUtils.SPACE + String.format("(%02d:%02d/%02d:%02d - %s)", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(position)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(position) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(position))), Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(duration)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))), j + "%"})).m537((CharSequence) I18N.m15706(R.string.option_resume), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onReceiveLastPlaybackPositionListener2.m17433(position);
                        }
                    }).m524((CharSequence) I18N.m15706(R.string.option_start_over), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onReceiveLastPlaybackPositionListener3.m17433(0);
                        }
                    }).m528();
                } else {
                    onReceiveLastPlaybackPositionListener.m17433(-1);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                onReceiveLastPlaybackPositionListener.m17433(-1);
            }
        }
    }
}
