package com.typhoon.tv.ui.activity.exoplayer;

import android.os.Bundle;
import com.evernote.android.state.Bundler;
import com.evernote.android.state.InjectionHelper;
import com.evernote.android.state.Injector;
import com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity;
import java.util.HashMap;

public class ExoPlayerActivity$$StateSaver<T extends ExoPlayerActivity> extends Injector.Object<T> {
    private static final HashMap<String, Bundler<?>> BUNDLERS = new HashMap<>();
    private static final InjectionHelper HELPER = new InjectionHelper("com.typhoon.tv.ui.activity.exoplayer.ExoPlayerActivity$$StateSaver", BUNDLERS);

    public void restore(T t, Bundle bundle) {
        t.mIsLoaded = HELPER.getBoolean(bundle, "mIsLoaded");
        t.mForceHttpUrlConnection = HELPER.getBoolean(bundle, "mForceHttpUrlConnection");
        t.mForceSoftwareAudioDecoder = HELPER.getBoolean(bundle, "mForceSoftwareAudioDecoder");
        t.mForceExtractorSource = HELPER.getBoolean(bundle, "mForceExtractorSource");
        t.mForceLowestResHLS = HELPER.getBoolean(bundle, "mForceLowestResHLS");
        t.mIsExitCalled = HELPER.getBoolean(bundle, "mIsExitCalled");
        t.mForceSoftwareVideoDecoder = HELPER.getBoolean(bundle, "mForceSoftwareVideoDecoder");
        t.mForceNoPlayerCache = HELPER.getBoolean(bundle, "mForceNoPlayerCache");
        t.mTrackErrCount = HELPER.getInt(bundle, "mTrackErrCount");
        t.mResumeWindow = HELPER.getInt(bundle, "mResumeWindow");
        t.mExtractorSourceHttpErrCount = HELPER.getInt(bundle, "mExtractorSourceHttpErrCount");
        t.mDecoderInitializationErrCount = HELPER.getInt(bundle, "mDecoderInitializationErrCount");
        t.mExtractorSourceErrCount = HELPER.getInt(bundle, "mExtractorSourceErrCount");
        t.mAudioTrackInitializationErrCount = HELPER.getInt(bundle, "mAudioTrackInitializationErrCount");
        t.mCurrentExtensionRendererMode = HELPER.getInt(bundle, "mCurrentExtensionRendererMode");
        t.mRuntimeErrCount = HELPER.getInt(bundle, "mRuntimeErrCount");
        t.mSourceErrCount = HELPER.getInt(bundle, "mSourceErrCount");
        t.mAdaptiveSourceErrCount = HELPER.getInt(bundle, "mAdaptiveSourceErrCount");
        t.mCurrentSubIndex = HELPER.getInt(bundle, "mCurrentSubIndex");
        t.mDuration = HELPER.getLong(bundle, "mDuration");
        t.mResumePosition = HELPER.getLong(bundle, "mResumePosition");
    }

    public void save(T t, Bundle bundle) {
        HELPER.putBoolean(bundle, "mIsLoaded", t.mIsLoaded);
        HELPER.putBoolean(bundle, "mForceHttpUrlConnection", t.mForceHttpUrlConnection);
        HELPER.putBoolean(bundle, "mForceSoftwareAudioDecoder", t.mForceSoftwareAudioDecoder);
        HELPER.putBoolean(bundle, "mForceExtractorSource", t.mForceExtractorSource);
        HELPER.putBoolean(bundle, "mForceLowestResHLS", t.mForceLowestResHLS);
        HELPER.putBoolean(bundle, "mIsExitCalled", t.mIsExitCalled);
        HELPER.putBoolean(bundle, "mForceSoftwareVideoDecoder", t.mForceSoftwareVideoDecoder);
        HELPER.putBoolean(bundle, "mForceNoPlayerCache", t.mForceNoPlayerCache);
        HELPER.putInt(bundle, "mTrackErrCount", t.mTrackErrCount);
        HELPER.putInt(bundle, "mResumeWindow", t.mResumeWindow);
        HELPER.putInt(bundle, "mExtractorSourceHttpErrCount", t.mExtractorSourceHttpErrCount);
        HELPER.putInt(bundle, "mDecoderInitializationErrCount", t.mDecoderInitializationErrCount);
        HELPER.putInt(bundle, "mExtractorSourceErrCount", t.mExtractorSourceErrCount);
        HELPER.putInt(bundle, "mAudioTrackInitializationErrCount", t.mAudioTrackInitializationErrCount);
        HELPER.putInt(bundle, "mCurrentExtensionRendererMode", t.mCurrentExtensionRendererMode);
        HELPER.putInt(bundle, "mRuntimeErrCount", t.mRuntimeErrCount);
        HELPER.putInt(bundle, "mSourceErrCount", t.mSourceErrCount);
        HELPER.putInt(bundle, "mAdaptiveSourceErrCount", t.mAdaptiveSourceErrCount);
        HELPER.putInt(bundle, "mCurrentSubIndex", t.mCurrentSubIndex);
        HELPER.putLong(bundle, "mDuration", t.mDuration);
        HELPER.putLong(bundle, "mResumePosition", t.mResumePosition);
    }
}
