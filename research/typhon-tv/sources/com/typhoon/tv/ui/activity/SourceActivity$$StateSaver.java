package com.typhoon.tv.ui.activity;

import android.os.Bundle;
import com.evernote.android.state.Bundler;
import com.evernote.android.state.InjectionHelper;
import com.typhoon.tv.ui.activity.SourceActivity;
import com.typhoon.tv.ui.activity.base.BaseAdActivity$$StateSaver;
import java.util.HashMap;

public class SourceActivity$$StateSaver<T extends SourceActivity> extends BaseAdActivity$$StateSaver<T> {
    private static final HashMap<String, Bundler<?>> BUNDLERS = new HashMap<>();
    private static final InjectionHelper HELPER = new InjectionHelper("com.typhoon.tv.ui.activity.SourceActivity$$StateSaver", BUNDLERS);

    public void restore(T t, Bundle bundle) {
        super.restore(t, bundle);
        t.mForceSetWatchedOnBackPressed = HELPER.getBoolean(bundle, "mForceSetWatchedOnBackPressed");
        t.mIsInterstitialShown = HELPER.getBoolean(bundle, "mIsInterstitialShown");
        t.mNeedMarkAsWatched = HELPER.getBoolean(bundle, "mNeedMarkAsWatched");
        t.dontRestoreSourcesState = HELPER.getBoolean(bundle, "dontRestoreSourcesState");
        t.mIsSubtitlesAdShown = HELPER.getBoolean(bundle, "mIsSubtitlesAdShown");
        t.mIsWatchedAnyLink = HELPER.getBoolean(bundle, "mIsWatchedAnyLink");
        t.isPlaying = HELPER.getBoolean(bundle, "isPlaying");
        t.mIsSubtitlesChildAppAdShown = HELPER.getBoolean(bundle, "mIsSubtitlesChildAppAdShown");
        t.mIsChildAppAdShown = HELPER.getBoolean(bundle, "mIsChildAppAdShown");
        t.mIsFromAnotherApp = HELPER.getBoolean(bundle, "mIsFromAnotherApp");
        t.mCurrentEpisodeListIndex = HELPER.getInt(bundle, "mCurrentEpisodeListIndex");
        t.mAdShownTimes = HELPER.getInt(bundle, "mAdShownTimes");
        t.mEpisode = HELPER.getInt(bundle, "mEpisode");
        t.mMediaSources = HELPER.getParcelableArrayList(bundle, "mMediaSources");
        t.mPreResolvedMediaSources = HELPER.getParcelableArrayList(bundle, "mPreResolvedMediaSources");
        t.mWatchedMediaSources = HELPER.getParcelableArrayList(bundle, "mWatchedMediaSources");
    }

    public void save(T t, Bundle bundle) {
        super.save(t, bundle);
        HELPER.putBoolean(bundle, "mForceSetWatchedOnBackPressed", t.mForceSetWatchedOnBackPressed);
        HELPER.putBoolean(bundle, "mIsInterstitialShown", t.mIsInterstitialShown);
        HELPER.putBoolean(bundle, "mNeedMarkAsWatched", t.mNeedMarkAsWatched);
        HELPER.putBoolean(bundle, "dontRestoreSourcesState", t.dontRestoreSourcesState);
        HELPER.putBoolean(bundle, "mIsSubtitlesAdShown", t.mIsSubtitlesAdShown);
        HELPER.putBoolean(bundle, "mIsWatchedAnyLink", t.mIsWatchedAnyLink);
        HELPER.putBoolean(bundle, "isPlaying", t.isPlaying);
        HELPER.putBoolean(bundle, "mIsSubtitlesChildAppAdShown", t.mIsSubtitlesChildAppAdShown);
        HELPER.putBoolean(bundle, "mIsChildAppAdShown", t.mIsChildAppAdShown);
        HELPER.putBoolean(bundle, "mIsFromAnotherApp", t.mIsFromAnotherApp);
        HELPER.putInt(bundle, "mCurrentEpisodeListIndex", t.mCurrentEpisodeListIndex);
        HELPER.putInt(bundle, "mAdShownTimes", t.mAdShownTimes);
        HELPER.putInt(bundle, "mEpisode", t.mEpisode);
        HELPER.putParcelableArrayList(bundle, "mMediaSources", t.mMediaSources);
        HELPER.putParcelableArrayList(bundle, "mPreResolvedMediaSources", t.mPreResolvedMediaSources);
        HELPER.putParcelableArrayList(bundle, "mWatchedMediaSources", t.mWatchedMediaSources);
    }
}
