package com.typhoon.tv.ui.activity.base;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.utils.LocaleUtils;

public abstract class BaseWebViewActivity extends AppCompatActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (TVApplication.m6285().getBoolean("pref_force_tv_mode", false)) {
            try {
                Resources resources = getResources();
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                Configuration configuration = resources.getConfiguration();
                configuration.uiMode = 4;
                resources.updateConfiguration(configuration, displayMetrics);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        super.onCreate(bundle);
        String trim = TVApplication.m6285().getString("pref_app_lang", "").trim();
        if (!trim.isEmpty()) {
            LocaleUtils.m17793(TVApplication.m6288(), LocaleUtils.m17792(trim));
        } else {
            LocaleUtils.m17793(TVApplication.m6288(), Resources.getSystem().getConfiguration().locale);
        }
    }
}
