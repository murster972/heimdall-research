package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.Pinkamena;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.MoPubRecyclerAdapter;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.RequestParameters;
import com.mopub.nativeads.ViewBinder;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvNewEpisodeInfo;
import com.typhoon.tv.presenter.ITvCalendarPresenter;
import com.typhoon.tv.presenter.impl.TvCalendarPresenter;
import com.typhoon.tv.ui.GridLayoutManagerWrapper;
import com.typhoon.tv.ui.GridSpacingItemDecoration;
import com.typhoon.tv.ui.LinearLayoutManagerWrapper;
import com.typhoon.tv.ui.activity.base.BaseAdActivity;
import com.typhoon.tv.ui.adapter.TvCalendarItemAdapter;
import com.typhoon.tv.ui.viewholder.TvCalendarItemViewHolder;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.ITvCalendarView;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class TvCalendarActivity extends BaseAdActivity implements ITvCalendarView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public TvCalendarItemAdapter f13540;

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<TvNewEpisodeInfo> f13541;

    /* renamed from: ʽ  reason: contains not printable characters */
    private List<TvNewEpisodeInfo> f13542;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final TvCalendarItemViewHolder.OnCardClickListener f13543 = new TvCalendarItemViewHolder.OnCardClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17255(View view, int i) {
            if (!NetworkUtils.m17799()) {
                TvCalendarActivity.this.m17252(I18N.m15706(R.string.no_internet));
            } else if (i < 0) {
                TvCalendarActivity.this.m17252(I18N.m15706(R.string.error));
            } else {
                int originalPosition = TvCalendarActivity.this.f13549.getOriginalPosition(i);
                if (originalPosition < 0) {
                    TvCalendarActivity.this.m17252(I18N.m15706(R.string.error));
                    return;
                }
                TvCalendarActivity.this.m17246(TvCalendarActivity.this.f13540.m17522(originalPosition));
            }
        }
    };

    /* renamed from: ʿ  reason: contains not printable characters */
    private final TvCalendarItemViewHolder.OnCardLongClickListener f13544 = new TvCalendarItemViewHolder.OnCardLongClickListener() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m17263(View view, int i) {
            if (!NetworkUtils.m17799()) {
                TvCalendarActivity.this.m17252(I18N.m15706(R.string.no_internet));
            } else if (i < 0) {
                TvCalendarActivity.this.m17252(I18N.m15706(R.string.error));
            } else {
                int originalPosition = TvCalendarActivity.this.f13549.getOriginalPosition(i);
                if (originalPosition < 0) {
                    TvCalendarActivity.this.m17252(I18N.m15706(R.string.error));
                    return;
                }
                TvCalendarActivity.this.m17245(TvCalendarActivity.this.f13540.m17522(originalPosition).getMediaInfo());
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public Subscription f13545;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public ITvCalendarPresenter f13546;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f13547;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f13548;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public MoPubRecyclerAdapter f13549;

    /* renamed from: 靐  reason: contains not printable characters */
    private final RequestParameters f13550 = new RequestParameters.Builder().desiredAssets(this.f13553).build();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public SuperRecyclerView f13551;

    /* renamed from: 齉  reason: contains not printable characters */
    private ProgressBar f13552;

    /* renamed from: 龘  reason: contains not printable characters */
    private final EnumSet<RequestParameters.NativeAdAsset> f13553 = EnumSet.of(RequestParameters.NativeAdAsset.TITLE, RequestParameters.NativeAdAsset.TEXT, RequestParameters.NativeAdAsset.ICON_IMAGE, RequestParameters.NativeAdAsset.MAIN_IMAGE, RequestParameters.NativeAdAsset.CALL_TO_ACTION_TEXT);
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public boolean f13554 = true;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m17221() {
        final DateTime now = (this.f13547 == null || this.f13547.isEmpty()) ? DateTime.now(DateTimeZone.forTimeZone(TimeZone.getDefault())) : DateTimeHelper.m15937(this.f13547);
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, (DatePickerDialog.OnDateSetListener) null, now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth());
        datePickerDialog.getDatePicker().init(now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
            public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
                try {
                    String unused = TvCalendarActivity.this.f13548 = new LocalDate(i, i2 + 1, i3).toString(DateTimeFormat.m21228("yyyy-MM-dd"));
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    String unused2 = TvCalendarActivity.this.f13548 = "";
                }
            }
        });
        datePickerDialog.setButton(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (TvCalendarActivity.this.f13548 == null) {
                    String unused = TvCalendarActivity.this.f13548 = DateTimeHelper.m15936(now);
                    if (TvCalendarActivity.this.f13548 == null && TvCalendarActivity.this.f13547 != null) {
                        String unused2 = TvCalendarActivity.this.f13548 = TvCalendarActivity.this.f13547;
                    }
                }
                if (TvCalendarActivity.this.f13548 == null || TvCalendarActivity.this.f13548.isEmpty()) {
                    TvCalendarActivity.this.m17253(I18N.m15706(R.string.choose_a_correct_date), 0);
                    return;
                }
                String unused3 = TvCalendarActivity.this.f13547 = TvCalendarActivity.this.f13548;
                TvCalendarActivity.this.m17225();
                if (!NetworkUtils.m17799()) {
                    TvCalendarActivity.this.m17252(I18N.m15706(R.string.no_internet));
                    boolean unused4 = TvCalendarActivity.this.f13554 = false;
                    TvCalendarActivity.this.m17238(false);
                    TvCalendarActivity.this.f13540.m17524();
                    return;
                }
                TvCalendarActivity.this.m17238(true);
                TvCalendarActivity.this.m17226(true);
            }
        });
        datePickerDialog.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                datePickerDialog.cancel();
            }
        });
        datePickerDialog.show();
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m17225() {
        String r1;
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            String str = this.f13547;
            try {
                if (this.f13547 != null && !this.f13547.isEmpty() && (r1 = DateTimeFormat.m21228("EE").m21245((ReadableInstant) DateTimeHelper.m15937(this.f13547))) != null && !r1.isEmpty()) {
                    str = str + " (" + r1 + ")";
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            supportActionBar.m426((CharSequence) str);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m17226(boolean z) {
        this.f13554 = true;
        if (z) {
            this.f13540.m17524();
        }
        this.f13546.m16159(this.f13547, 1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17229() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.m427(true);
            supportActionBar.m433(true);
            supportActionBar.m440((CharSequence) I18N.m15706(R.string.tv_calendar));
        }
        m17225();
        this.f13552 = (ProgressBar) findViewById(R.id.pbTvCalendar);
        this.f13551 = (SuperRecyclerView) findViewById(R.id.rvTvCalendarList);
        if (DeviceUtils.m6389(new boolean[0]) || getResources().getConfiguration().orientation == 2) {
            this.f13551.setLayoutManager(new GridLayoutManagerWrapper(this, 2));
            this.f13551.m26368((RecyclerView.ItemDecoration) new GridSpacingItemDecoration(2, 0, true));
        } else {
            this.f13551.setLayoutManager(new LinearLayoutManagerWrapper(this, 1, false));
        }
        this.f13551.setRefreshingColorResources(17170456, 17170450, 17170452, 17170454);
        this.f13551.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                TvCalendarActivity.this.f13546.m16158();
                if (!NetworkUtils.m17799()) {
                    TvCalendarActivity.this.m17252(I18N.m15706(R.string.no_internet));
                    boolean unused = TvCalendarActivity.this.f13554 = false;
                    TvCalendarActivity.this.m17242(false);
                    return;
                }
                TvCalendarActivity.this.m17226(false);
            }
        });
        this.f13540 = new TvCalendarItemAdapter(new ArrayList());
        this.f13540.m17525(this.f13543);
        this.f13540.m17526(this.f13544);
        this.f13549 = new MoPubRecyclerAdapter((Activity) this, (RecyclerView.Adapter) this.f13540, MoPubNativeAdPositioning.clientPositioning().addFixedPosition(1).enableRepeatingPositions(16));
        this.f13549.registerAdRenderer(new MoPubStaticNativeAdRenderer(new ViewBinder.Builder(R.layout.item_native_ad_recyclerview).titleId(R.id.tvNativeAdTitle).textId(R.id.tvNativeAdText).mainImageId(R.id.ivNativeAdImage).iconImageId(R.id.ivNativeAdIcon).callToActionId(R.id.btnNativeAdCta).build()));
        this.f13551.setAdapter(this.f13549);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17230(MediaInfo mediaInfo) {
        Intent intent = new Intent(this, MediaDetailsActivity.class);
        intent.putExtra("mediaInfo", mediaInfo);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17231(TvNewEpisodeInfo tvNewEpisodeInfo) {
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("mediaInfo", tvNewEpisodeInfo.getMediaInfo());
        intent.putExtra("season", tvNewEpisodeInfo.getSeason());
        intent.putExtra("episode", tvNewEpisodeInfo.getEpisode());
        intent.putExtra("forceSetWatchedOnBackPressed", true);
        startActivity(intent);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17236() {
        MoPubRecyclerAdapter moPubRecyclerAdapter = this.f13549;
        String str = TyphoonApp.f5838;
        RequestParameters requestParameters = this.f13550;
        Pinkamena.DianePie();
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m17238(boolean z) {
        if (z) {
            this.f13551.setVisibility(8);
            this.f13552.setVisibility(0);
            return;
        }
        this.f13551.setVisibility(0);
        this.f13552.setVisibility(8);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17240() {
        this.f13546 = new TvCalendarPresenter(this);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m17242(final boolean z) {
        this.f13551.getSwipeToRefresh().post(new Runnable() {
            public void run() {
                TvCalendarActivity.this.f13551.getSwipeToRefresh().setRefreshing(z);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17245(final MediaInfo mediaInfo) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(I18N.m15706(R.string.please_wait));
        progressDialog.setMessage(I18N.m15706(R.string.loading));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);
        progressDialog.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (TvCalendarActivity.this.f13545 != null && !TvCalendarActivity.this.f13545.isUnsubscribed()) {
                    TvCalendarActivity.this.f13545.unsubscribe();
                }
                if (!TvCalendarActivity.this.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        });
        if (mediaInfo.getTmdbId() > 0 || (mediaInfo.getTvdbId() <= 0 && (mediaInfo.getImdbId() == null || mediaInfo.getImdbId().isEmpty()))) {
            m17230(mediaInfo);
        } else {
            this.f13545 = Observable.m7359(new Observable.OnSubscribe<Integer>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super Integer> subscriber) {
                    subscriber.onNext(Integer.valueOf(TmdbApi.m15742().m15753(mediaInfo.getTvdbId(), mediaInfo.getImdbId())));
                    subscriber.onCompleted();
                }
            }).m7383((Action0) new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17257() {
                    TvCalendarActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (!progressDialog.isShowing()) {
                                progressDialog.show();
                            }
                        }
                    });
                }
            }).m7376(new Func1<Throwable, Integer>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Integer call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return -1;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7415(new Observer<Integer>() {
                public void onCompleted() {
                    TvCalendarActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (!TvCalendarActivity.this.isFinishing() && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    });
                }

                public void onError(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    if (TvCalendarActivity.this.f13545 != null && !TvCalendarActivity.this.f13545.isUnsubscribed()) {
                        TvCalendarActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                TvCalendarActivity.this.m17230(mediaInfo);
                            }
                        });
                    }
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void onNext(Integer num) {
                    if (num.intValue() > 0) {
                        mediaInfo.setTmdbId(num.intValue());
                        if (TvCalendarActivity.this.f13545 != null && !TvCalendarActivity.this.f13545.isUnsubscribed()) {
                            TvCalendarActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    TvCalendarActivity.this.m17230(mediaInfo);
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17246(final TvNewEpisodeInfo tvNewEpisodeInfo) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(I18N.m15706(R.string.please_wait));
        progressDialog.setMessage(I18N.m15706(R.string.loading));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);
        progressDialog.setButton(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (TvCalendarActivity.this.f13545 != null && !TvCalendarActivity.this.f13545.isUnsubscribed()) {
                    TvCalendarActivity.this.f13545.unsubscribe();
                }
                if (!TvCalendarActivity.this.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        });
        final MediaInfo mediaInfo = tvNewEpisodeInfo.getMediaInfo();
        if (mediaInfo.getTmdbId() > 0 || (mediaInfo.getTvdbId() <= 0 && (mediaInfo.getImdbId() == null || mediaInfo.getImdbId().isEmpty()))) {
            m17231(tvNewEpisodeInfo);
        } else {
            this.f13545 = Observable.m7359(new Observable.OnSubscribe<Integer>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super Integer> subscriber) {
                    subscriber.onNext(Integer.valueOf(TmdbApi.m15742().m15753(mediaInfo.getTvdbId(), mediaInfo.getImdbId())));
                    subscriber.onCompleted();
                }
            }).m7383((Action0) new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m17261() {
                    TvCalendarActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (!progressDialog.isShowing()) {
                                progressDialog.show();
                            }
                        }
                    });
                }
            }).m7376(new Func1<Throwable, Integer>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Integer call(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    return -1;
                }
            }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7415(new Observer<Integer>() {
                public void onCompleted() {
                    TvCalendarActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (!TvCalendarActivity.this.isFinishing() && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    });
                }

                public void onError(Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                    if (TvCalendarActivity.this.f13545 != null && !TvCalendarActivity.this.f13545.isUnsubscribed()) {
                        TvCalendarActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                TvCalendarActivity.this.m17231(tvNewEpisodeInfo);
                            }
                        });
                    }
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void onNext(Integer num) {
                    if (num.intValue() > 0) {
                        mediaInfo.setTmdbId(num.intValue());
                        tvNewEpisodeInfo.setMediaInfo(mediaInfo);
                        if (TvCalendarActivity.this.f13545 != null && !TvCalendarActivity.this.f13545.isUnsubscribed()) {
                            TvCalendarActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    TvCalendarActivity.this.m17231(tvNewEpisodeInfo);
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17249(List<TvNewEpisodeInfo> list, boolean z) {
        TextView textView;
        this.f13540.m17524();
        this.f13540.m17528(list);
        m17238(false);
        if (!list.isEmpty()) {
            this.f13551.getRecyclerView().smoothScrollToPosition(0);
        } else {
            View emptyView = this.f13551.getEmptyView();
            if (!(emptyView == null || (textView = (TextView) emptyView.findViewById(R.id.tvEmpty)) == null)) {
                textView.setText(I18N.m15706(z ? R.string.no_favorite_shows_airing_that_day : R.string.no_data));
                textView.setTextSize(2, 14.0f);
            }
        }
        this.f13554 = false;
        if (!TyphoonApp.f5865) {
            m17236();
        }
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (IllegalStateException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_tv_calendar);
        this.f13542 = new ArrayList();
        if (bundle != null && !bundle.isEmpty()) {
            this.f13547 = bundle.getString("dateString", "");
        }
        if (this.f13547 == null || this.f13547.isEmpty()) {
            DateTime now = DateTime.now(DateTimeZone.forTimeZone(TimeZone.getDefault()));
            this.f13547 = now.getYear() + "-" + Utils.m6413(now.getMonthOfYear()) + "-" + Utils.m6413(now.getDayOfMonth());
        }
        m17229();
        m17240();
        if (!NetworkUtils.m17799()) {
            m17252(I18N.m15706(R.string.no_internet));
            this.f13554 = false;
            m17238(false);
            return;
        }
        m17238(true);
        m17226(true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tv_calendar, menu);
        m17295(menu);
        boolean z = TVApplication.m6285().getBoolean("pref_tv_calendar_show_fav_only", false);
        MenuItem findItem = menu.findItem(R.id.action_favorites_only);
        findItem.setIcon(z ? R.drawable.ic_star_white_36dp : R.drawable.ic_star_border_white_36dp);
        findItem.setTitle(I18N.m15706(z ? R.string.show_all : R.string.show_favorites_only));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13546 != null) {
            this.f13546.m16157();
        }
        this.f13546 = null;
        if (this.f13549 != null) {
            this.f13549.destroy();
        }
        if (this.f13545 != null && !this.f13545.isUnsubscribed()) {
            this.f13545.unsubscribe();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean z;
        switch (menuItem.getItemId()) {
            case R.id.action_favorites_only:
                if (!(this.f13554 || this.f13541 == null || this.f13542 == null)) {
                    if (!TVApplication.m6285().getBoolean("pref_tv_calendar_show_fav_only", false)) {
                        z = true;
                        m17249(this.f13542, true);
                        menuItem.setIcon(R.drawable.ic_star_white_36dp);
                        menuItem.setTitle(I18N.m15706(R.string.show_all));
                    } else {
                        z = false;
                        m17249(this.f13541, false);
                        menuItem.setIcon(R.drawable.ic_star_border_white_36dp);
                        menuItem.setTitle(I18N.m15706(R.string.show_favorites_only));
                    }
                    TVApplication.m6285().edit().putBoolean("pref_tv_calendar_show_fav_only", z).apply();
                    break;
                }
            case R.id.action_select_date:
                m17221();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("dateString", this.f13547);
        super.onSaveInstanceState(bundle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17251() {
        this.f13542 = new ArrayList();
        this.f13540.m17524();
        m17238(false);
        this.f13554 = false;
        m17252(I18N.m15706(R.string.no_data));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17252(String str) {
        m17253(str, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17253(String str, int i) {
        final Snackbar make = Snackbar.make(findViewById(R.id.tv_calendar_rootLayout), (CharSequence) str, i);
        make.setAction((CharSequence) I18N.m15706(R.string.close), (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View view) {
                make.dismiss();
            }
        }).setActionTextColor(ContextCompat.getColor(TVApplication.m6288(), 17170456));
        make.show();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17254(List<TvNewEpisodeInfo> list) {
        this.f13541 = list;
        ArrayList<MediaInfo> r3 = TVApplication.m6287().m6312((Integer) 0);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator<MediaInfo> it2 = r3.iterator();
        while (it2.hasNext()) {
            MediaInfo next = it2.next();
            if (next.getTvdbId() > 0) {
                arrayList.add(Integer.valueOf(next.getTvdbId()));
            }
            if (next.getImdbId() != null && !next.getImdbId().isEmpty()) {
                arrayList2.add(next.getImdbId());
            }
        }
        ArrayList arrayList3 = new ArrayList();
        for (int i = 0; i < this.f13541.size(); i++) {
            TvNewEpisodeInfo tvNewEpisodeInfo = this.f13541.get(i);
            int tvdbId = tvNewEpisodeInfo.getMediaInfo().getTvdbId();
            String imdbId = tvNewEpisodeInfo.getMediaInfo().getImdbId();
            if ((imdbId != null && !imdbId.isEmpty() && arrayList2.contains(imdbId)) || (tvdbId > 0 && arrayList.contains(Integer.valueOf(tvdbId)))) {
                arrayList3.add(tvNewEpisodeInfo);
            }
        }
        if (this.f13542 != null) {
            this.f13542.clear();
        }
        this.f13542 = arrayList3;
        if (TVApplication.m6285().getBoolean("pref_tv_calendar_show_fav_only", false)) {
            m17249(this.f13542, true);
        } else {
            m17249(this.f13541, false);
        }
    }
}
