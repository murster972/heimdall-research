package com.typhoon.tv.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.api.TraktUserApi;
import com.typhoon.tv.backup.FavBackupRestoreHelper;
import com.typhoon.tv.backup.PrefsBackupRestoreHelper;
import com.typhoon.tv.backup.WatchedEpsBackupRestoreHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridGetTokenFailedEvent;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridGetTokenSuccessEvent;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridWaitingToVerifyEvent;
import com.typhoon.tv.event.trakt.TraktGetTokenFailedEvent;
import com.typhoon.tv.event.trakt.TraktGetTokenSuccessEvent;
import com.typhoon.tv.event.trakt.TraktUserCancelledAuthEvent;
import com.typhoon.tv.event.trakt.TraktWaitingToVerifyEvent;
import com.typhoon.tv.helper.ContinuePlaybackHelper;
import com.typhoon.tv.helper.PlayActionHelper;
import com.typhoon.tv.helper.category.MovieCategoryHelper;
import com.typhoon.tv.helper.category.TvShowCategoryHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.trakt.TraktCredentialsHelper;
import com.typhoon.tv.jobs.CheckNewEpisodeJob;
import com.typhoon.tv.jobs.CheckNewMovieReleaseJob;
import com.typhoon.tv.model.debrid.realdebrid.RealDebridCredentialsInfo;
import com.typhoon.tv.model.trakt.TraktCredentialsInfo;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.ui.activity.base.BasePreferenceActivity;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.NetworkUtils;
import com.typhoon.tv.utils.Utils;
import it.gmariotti.changelibs.library.view.ChangeLogRecyclerView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import org.apache.commons.lang3.CharUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class SettingsActivity extends BasePreferenceActivity implements Preference.OnPreferenceClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f13215 = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/Typhoon/Subtitles");

    /* renamed from: ʻ  reason: contains not printable characters */
    private Preference f13216;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private Subscription f13217;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public Preference f13218;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public Preference f13219;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private Subscription f13220;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public Preference f13221;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public Preference f13222;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public Preference f13223;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public Preference f13224;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public Preference f13225;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public Preference f13226;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public Preference f13227;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public Preference f13228;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public Preference f13229;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public Preference f13230;
    /* access modifiers changed from: private */

    /* renamed from: י  reason: contains not printable characters */
    public Preference f13231;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public Preference f13232;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public Preference f13233;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Preference f13234;
    /* access modifiers changed from: private */

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public String[] f13235;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public Preference f13236;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private CompositeSubscription f13237;
    /* access modifiers changed from: private */

    /* renamed from: ᵎ  reason: contains not printable characters */
    public String[] f13238;
    /* access modifiers changed from: private */

    /* renamed from: ᵔ  reason: contains not printable characters */
    public String[] f13239;
    /* access modifiers changed from: private */

    /* renamed from: ᵢ  reason: contains not printable characters */
    public String[] f13240;
    /* access modifiers changed from: private */

    /* renamed from: ⁱ  reason: contains not printable characters */
    public String[] f13241;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public Preference f13242;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Preference f13243;

    /* renamed from: 齉  reason: contains not printable characters */
    private Preference f13244;
    /* access modifiers changed from: private */

    /* renamed from: ﹳ  reason: contains not printable characters */
    public String[] f13245;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Preference f13246;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Preference f13247;
    /* access modifiers changed from: private */

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public String[] f13248;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m16992() {
        int i = TVApplication.m6285().getInt("pref_check_new_movie_release_frequency", 24);
        FrameLayout frameLayout = new FrameLayout(this);
        final NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setMinValue(6);
        numberPicker.setMaxValue(48);
        numberPicker.setValue(i);
        numberPicker.setDescendantFocusability(393216);
        frameLayout.addView(numberPicker, new FrameLayout.LayoutParams(-2, -2, 17));
        new AlertDialog.Builder(this).m523((CharSequence) I18N.m15706(R.string.pref_check_new_movie_release_frequency)).m522((View) frameLayout).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int value = numberPicker.getValue();
                SettingsActivity.this.f13226.setSummary(String.valueOf(value));
                TVApplication.m6285().edit().putInt("pref_check_new_movie_release_frequency", value).apply();
                Set<JobRequest> allJobRequestsForTag = JobManager.instance().getAllJobRequestsForTag("CheckNewMovieReleaseJob");
                if (allJobRequestsForTag != null && !allJobRequestsForTag.isEmpty()) {
                    for (JobRequest jobId : allJobRequestsForTag) {
                        JobManager.instance().cancel(jobId.getJobId());
                    }
                    CheckNewMovieReleaseJob.m16117();
                }
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m16995() {
        this.f13237.m25086(Observable.m7356(Boolean.valueOf(FavBackupRestoreHelper.m15823())).m7375(Observable.m7356(false)).m7382(Schedulers.computation()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Boolean bool) {
                SettingsActivity.this.m17041(bool.booleanValue());
            }
        }));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m16996() {
        this.f13237.m25086(Observable.m7356(Boolean.valueOf(FavBackupRestoreHelper.m15822())).m7375(Observable.m7356(false)).m7382(Schedulers.computation()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Boolean bool) {
                SettingsActivity.this.m17033(bool.booleanValue());
            }
        }));
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m16999() {
        String string = TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF");
        int parseColor = Color.parseColor("#00FFFFFF");
        try {
            parseColor = Color.parseColor(string);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        ColorPickerDialogBuilder.m26226((Context) this).m26237(I18N.m15706(R.string.pref_cc_subs_bg_color) + " (" + I18N.m15707(R.string.default_value, "#00FFFFFF") + ")").m26234(ColorPickerView.WHEEL_TYPE.FLOWER).m26230(1).m26238(true).m26231(true).m26233(parseColor).m26236((CharSequence) I18N.m15706(R.string.ok), (ColorPickerClickListener) new ColorPickerClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17053(DialogInterface dialogInterface, int i, Integer[] numArr) {
                String str = "#" + Integer.toHexString(i).toUpperCase();
                TVApplication.m6285().edit().putString("pref_cc_subs_bg_color", str).apply();
                SettingsActivity.this.f13231.setSummary(str);
            }
        }).m26235((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m26232().show();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m17000() {
        String string = TVApplication.m6285().getString("pref_alluc_api_key", "");
        AlertDialog r0 = new AlertDialog.Builder(this).m536((CharSequence) "Alluc API Key").m525();
        final EditText editText = new EditText(this);
        editText.setText(string, TextView.BufferType.EDITABLE);
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, final boolean z) {
                editText.post(new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) SettingsActivity.this.getSystemService("input_method");
                        if (z) {
                            inputMethodManager.showSoftInput(editText, 1);
                        } else {
                            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    }
                });
            }
        });
        r0.m518((View) editText);
        r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String obj = editText.getText().toString();
                TVApplication.m6285().edit().putString("pref_alluc_api_key", obj).apply();
                SettingsActivity.this.f13232.setSummary(obj);
            }
        });
        r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        r0.show();
    }

    /* access modifiers changed from: private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m17003() {
        if (this.f13217 != null && !this.f13217.isUnsubscribed()) {
            this.f13217.unsubscribe();
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m17004() {
        String string = TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF");
        int parseColor = Color.parseColor("#FFFFFFFF");
        try {
            parseColor = Color.parseColor(string);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        ColorPickerDialogBuilder.m26226((Context) this).m26237(I18N.m15706(R.string.pref_cc_subs_font_color) + " (" + I18N.m15707(R.string.default_value, "#FFFFFFFF") + ")").m26234(ColorPickerView.WHEEL_TYPE.FLOWER).m26230(1).m26238(true).m26231(true).m26233(parseColor).m26236((CharSequence) I18N.m15706(R.string.ok), (ColorPickerClickListener) new ColorPickerClickListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m17052(DialogInterface dialogInterface, int i, Integer[] numArr) {
                String str = "#" + Integer.toHexString(i).toUpperCase();
                TVApplication.m6285().edit().putString("pref_cc_subs_font_color", str).apply();
                SettingsActivity.this.f13229.setSummary(str);
            }
        }).m26235((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m26232().show();
    }

    /* access modifiers changed from: private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m17007() {
        if (this.f13220 != null && !this.f13220.isUnsubscribed()) {
            this.f13220.unsubscribe();
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m17009() {
        if (TraktCredentialsHelper.m16110().isValid()) {
            TraktUserApi.m15774().m15781(true, false);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m17010() {
        if (!NetworkUtils.m17799()) {
            Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
            return;
        }
        Toast.makeText(this, I18N.m15706(R.string.please_wait), 1).show();
        this.f13217 = TraktUserApi.m15774().m15779().m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<Boolean>() {
            public void onCompleted() {
                TraktCredentialsInfo r0 = TraktCredentialsHelper.m16110();
                boolean isValid = r0.isValid();
                String user = r0.getUser();
                SettingsActivity.this.f13228.setSummary(isValid ? (user == null || user.isEmpty()) ? "Status: Logged in" : "Status: Logged in as " + r0.getUser() : "");
                SettingsActivity.this.f13228.setEnabled(!isValid);
                if (isValid) {
                    Toast.makeText(SettingsActivity.this, "Trakt.tv authorized!", 1).show();
                }
                SettingsActivity.this.m17003();
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(Boolean bool) {
                RxBus.m15709().m15711(bool.booleanValue() ? new TraktGetTokenSuccessEvent() : new TraktGetTokenFailedEvent());
            }
        });
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private void m17013() {
        if (!NetworkUtils.m17799()) {
            Toast.makeText(this, I18N.m15706(R.string.no_internet), 1).show();
            return;
        }
        Toast.makeText(this, I18N.m15706(R.string.please_wait), 1).show();
        this.f13220 = RealDebridUserApi.m15845().m15848().m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<Boolean>() {
            public void onCompleted() {
                boolean isValid = RealDebridCredentialsHelper.m15838().isValid();
                SettingsActivity.this.f13223.setSummary(isValid ? "Status: Logged in" : "");
                SettingsActivity.this.f13223.setEnabled(!isValid);
                if (isValid) {
                    Toast.makeText(SettingsActivity.this, "Real-Debrid authorized!", 1).show();
                }
                SettingsActivity.this.m17007();
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(Boolean bool) {
                RxBus.m15709().m15711(bool.booleanValue() ? new RealDebridGetTokenSuccessEvent() : new RealDebridGetTokenFailedEvent());
            }
        });
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m17016() {
        this.f13237.m25086(Observable.m7356(Boolean.valueOf(WatchedEpsBackupRestoreHelper.m15830())).m7375(Observable.m7356(false)).m7382(Schedulers.computation()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Boolean bool) {
                SettingsActivity.this.m17041(bool.booleanValue());
            }
        }));
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m17020() {
        this.f13237.m25086(Observable.m7356(Boolean.valueOf(WatchedEpsBackupRestoreHelper.m15829())).m7375(Observable.m7356(false)).m7382(Schedulers.computation()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Boolean bool) {
                SettingsActivity.this.m17033(bool.booleanValue());
            }
        }));
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m17021() {
        String f = Float.toString(TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f));
        AlertDialog r0 = new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_cc_subs_font_scale)).m525();
        r0.m519((CharSequence) I18N.m15707(R.string.default_value, Float.toString(1.05f)));
        final EditText editText = new EditText(this);
        editText.setText(f, TextView.BufferType.EDITABLE);
        editText.setInputType(12290);
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, final boolean z) {
                editText.post(new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) SettingsActivity.this.getSystemService("input_method");
                        if (z) {
                            inputMethodManager.showSoftInput(editText, 1);
                        } else {
                            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    }
                });
            }
        });
        r0.m518((View) editText);
        r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                float f = -1.0f;
                try {
                    f = Float.parseFloat(editText.getText().toString());
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                if (f < 0.0f) {
                    Toast.makeText(SettingsActivity.this, I18N.m15706(R.string.value_must_be_positive), 1).show();
                    return;
                }
                TVApplication.m6285().edit().putFloat("pref_cc_subs_font_scale", f).apply();
                SettingsActivity.this.f13225.setSummary(Float.toString(f));
            }
        });
        r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        r0.show();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m17029() {
        int i = TVApplication.m6285().getInt("pref_check_new_episode_frequency", 2);
        FrameLayout frameLayout = new FrameLayout(this);
        final NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(24);
        numberPicker.setValue(i);
        numberPicker.setDescendantFocusability(393216);
        frameLayout.addView(numberPicker, new FrameLayout.LayoutParams(-2, -2, 17));
        new AlertDialog.Builder(this).m523((CharSequence) I18N.m15706(R.string.pref_check_new_episode_frequency)).m522((View) frameLayout).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int value = numberPicker.getValue();
                SettingsActivity.this.f13247.setSummary(String.valueOf(value));
                TVApplication.m6285().edit().putInt("pref_check_new_episode_frequency", value).apply();
                Set<JobRequest> allJobRequestsForTag = JobManager.instance().getAllJobRequestsForTag("CheckNewEpisodeJob");
                if (allJobRequestsForTag != null && !allJobRequestsForTag.isEmpty()) {
                    for (JobRequest jobId : allJobRequestsForTag) {
                        JobManager.instance().cancel(jobId.getJobId());
                    }
                    CheckNewEpisodeJob.m16115();
                }
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17030() {
        String string = TVApplication.m6285().getString("pref_synopsis_language", "en-US");
        Set<Map.Entry<String, String>> entrySet = I18N.m15704().entrySet();
        final ArrayList arrayList = new ArrayList();
        final ArrayList arrayList2 = new ArrayList();
        int i = -1;
        int i2 = 0;
        for (Map.Entry next : entrySet) {
            if (((String) next.getValue()).equals(string)) {
                i = i2;
            }
            arrayList.add(next.getKey());
            arrayList2.add(next.getValue());
            i2++;
        }
        new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_synopsis_choose_lang)).m539((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), i, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                TVApplication.m6285().edit().putString("pref_synopsis_language", (String) arrayList2.get(i)).apply();
                SettingsActivity.this.f13230.setSummary((CharSequence) arrayList.get(i));
                dialogInterface.dismiss();
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m17033(boolean z) {
        if (z) {
            Toast.makeText(this, I18N.m15706(R.string.restore_success), 0).show();
            Toast.makeText(this, I18N.m15706(R.string.restart_to_see_the_difference), 1).show();
            return;
        }
        Toast.makeText(this, I18N.m15706(R.string.failed_to_restore), 1).show();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m17036() {
        final ArrayList arrayList = new ArrayList();
        for (BaseResolver r5 : TyphoonApp.m6326()) {
            arrayList.add(r5.m16778());
        }
        String string = TVApplication.m6285().getString("pref_enabled_resolvers", (String) null);
        final ArrayList arrayList2 = new ArrayList();
        if (string == null) {
            arrayList2.addAll(arrayList);
        } else if (string.contains(",")) {
            for (String str : string.split(",")) {
                if (!arrayList2.contains(str) && arrayList.contains(str)) {
                    arrayList2.add(str);
                }
            }
        } else if (arrayList.contains(string)) {
            arrayList2.add(string);
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            if (arrayList2.contains((String) it2.next())) {
                arrayList3.add(true);
            } else {
                arrayList3.add(false);
            }
        }
        Boolean[] boolArr = (Boolean[]) arrayList3.toArray(new Boolean[arrayList3.size()]);
        boolean[] zArr = new boolean[boolArr.length];
        for (int i = 0; i < boolArr.length; i++) {
            zArr[i] = boolArr[i].booleanValue();
        }
        new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_enabled_resolvers)).m541((CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]), zArr, (DialogInterface.OnMultiChoiceClickListener) new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                String str = (String) arrayList.get(i);
                if (!arrayList2.contains(str)) {
                    arrayList2.add(str);
                } else {
                    arrayList2.remove(str);
                }
            }
        }).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                StringBuilder sb = new StringBuilder();
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    String str = (String) it2.next();
                    if (sb.indexOf(str + ",") == -1) {
                        sb.append(str).append(",");
                    }
                }
                String sb2 = sb.toString();
                TVApplication.m6285().edit().putString("pref_enabled_resolvers", sb2.length() + -1 > 0 ? sb2.substring(0, sb2.length() - 1) : "").apply();
                dialogInterface.dismiss();
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17039() {
        String string = TVApplication.m6285().getString("pref_sub_language_international", Locale.getDefault().getDisplayLanguage(Locale.US));
        final ArrayList arrayList = new ArrayList();
        if (!string.contains(",")) {
            arrayList.add(string);
        } else {
            for (String str : string.split(",")) {
                if (!arrayList.contains(str) && I18N.m15708().containsKey(str)) {
                    arrayList.add(str);
                }
            }
        }
        final ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList(I18N.m15705());
        Collections.sort(arrayList4);
        Iterator it2 = arrayList4.iterator();
        while (it2.hasNext()) {
            String str2 = (String) it2.next();
            try {
                if (!arrayList2.contains(str2)) {
                    if (arrayList.contains(str2)) {
                        arrayList3.add(true);
                    } else {
                        arrayList3.add(false);
                    }
                    arrayList2.add(str2);
                }
            } catch (MissingResourceException e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        Boolean[] boolArr = (Boolean[]) arrayList3.toArray(new Boolean[arrayList3.size()]);
        boolean[] zArr = new boolean[boolArr.length];
        for (int i = 0; i < boolArr.length; i++) {
            zArr[i] = boolArr[i].booleanValue();
        }
        new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_subtitle)).m541((CharSequence[]) arrayList2.toArray(new CharSequence[arrayList2.size()]), zArr, (DialogInterface.OnMultiChoiceClickListener) new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                if (!arrayList.contains((String) arrayList2.get(i))) {
                    arrayList.add(arrayList2.get(i));
                } else {
                    arrayList.remove(arrayList2.get(i));
                }
            }
        }).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                StringBuilder sb = new StringBuilder();
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String str = (String) it2.next();
                    if (sb.indexOf(str + ",") == -1) {
                        sb.append(str).append(",");
                    }
                }
                String sb2 = sb.toString();
                TVApplication.m6285().edit().putString("pref_sub_language_international", sb2.length() + -1 > 0 ? sb2.substring(0, sb2.length() - 1) : "").apply();
                dialogInterface.dismiss();
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m17041(boolean z) {
        if (z) {
            Toast.makeText(this, I18N.m15706(R.string.backup_success), 1).show();
        } else {
            Toast.makeText(this, I18N.m15706(R.string.failed_to_backup), 1).show();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17042(boolean z, String str) {
        BaseProvider[] r1 = z ? TyphoonApp.m6328() : TyphoonApp.m6331();
        final ArrayList arrayList = new ArrayList();
        for (BaseProvider r7 : r1) {
            arrayList.add(r7.m16284());
        }
        String string = TVApplication.m6285().getString(str, (String) null);
        final ArrayList arrayList2 = new ArrayList();
        if (string == null) {
            arrayList2.addAll(arrayList);
        } else if (string.contains(",")) {
            for (String str2 : string.split(",")) {
                if (!arrayList2.contains(str2) && arrayList.contains(str2)) {
                    arrayList2.add(str2);
                }
            }
        } else if (arrayList.contains(string)) {
            arrayList2.add(string);
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            if (arrayList2.contains((String) it2.next())) {
                arrayList3.add(true);
            } else {
                arrayList3.add(false);
            }
        }
        Boolean[] boolArr = (Boolean[]) arrayList3.toArray(new Boolean[arrayList3.size()]);
        boolean[] zArr = new boolean[boolArr.length];
        for (int i = 0; i < boolArr.length; i++) {
            zArr[i] = boolArr[i].booleanValue();
        }
        final String str3 = str;
        new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(z ? R.string.pref_enabled_movies_providers : R.string.pref_enabled_tv_shows_providers)).m541((CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]), zArr, (DialogInterface.OnMultiChoiceClickListener) new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                String str = (String) arrayList.get(i);
                if (!arrayList2.contains(str)) {
                    arrayList2.add(str);
                } else {
                    arrayList2.remove(str);
                }
            }
        }).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                StringBuilder sb = new StringBuilder();
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    String str = (String) it2.next();
                    if (sb.indexOf(str + ",") == -1) {
                        sb.append(str).append(",");
                    }
                }
                String sb2 = sb.toString();
                TVApplication.m6285().edit().putString(str3, sb2.length() + -1 > 0 ? sb2.substring(0, sb2.length() - 1) : "").apply();
                dialogInterface.dismiss();
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m17045() {
        int i = TVApplication.m6285().getInt("pref_alluc_results_limit", 20);
        AlertDialog r0 = new AlertDialog.Builder(this).m536((CharSequence) "Alluc results limit").m525();
        final EditText editText = new EditText(this);
        editText.setInputType(2);
        editText.setText(Integer.toString(i), TextView.BufferType.EDITABLE);
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, final boolean z) {
                editText.post(new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) SettingsActivity.this.getSystemService("input_method");
                        if (z) {
                            inputMethodManager.showSoftInput(editText, 1);
                        } else {
                            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    }
                });
            }
        });
        r0.m518((View) editText);
        r0.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                double d = -1.0d;
                try {
                    d = Double.parseDouble(editText.getText().toString());
                } catch (Exception e) {
                }
                if (d == -1.0d) {
                    Toast.makeText(SettingsActivity.this, "Not a number...", 1).show();
                    return;
                }
                int i2 = (int) d;
                if (i2 < 0) {
                    Toast.makeText(SettingsActivity.this, "Results limit should be a positive integer...", 1).show();
                    return;
                }
                TVApplication.m6285().edit().putInt("pref_alluc_results_limit", i2).apply();
                SettingsActivity.this.f13236.setSummary(Integer.toString(i2));
            }
        });
        r0.m517(-2, I18N.m15706(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        r0.show();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private void m17046() {
        if (TraktCredentialsHelper.m16110().isValid()) {
            TraktUserApi.m15774().m15785(true, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        if ((i == 0 || i == 1) && i2 == -1) {
            String path = intent.getData().getPath();
            if (i == 0) {
                TVApplication.m6285().edit().putString("pref_sub_down_path", path).apply();
                this.f13216.setSummary(path);
                return;
            }
            String charSequence = this.f13218.getSummary().toString();
            if (!path.contains(Environment.getExternalStorageDirectory().getAbsolutePath())) {
                str = charSequence;
                Snackbar.make((View) this.f13728, (CharSequence) I18N.m15706(R.string.download_path_error), -1).show();
            } else {
                str = path;
            }
            TVApplication.m6285().edit().putString("pref_media_down_path", str).apply();
            this.f13218.setSummary(str);
        } else if (i == 2 && i2 == -1) {
            Toast.makeText(this, "Unknown error while checking payment records. Try again later.", 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        this.f13237 = new CompositeSubscription();
        this.f13238 = new String[]{I18N.m15706(R.string.download_manager_internal), I18N.m15706(R.string.download_manager_external)};
        this.f13239 = new String[]{I18N.m15706(R.string.size_small), I18N.m15706(R.string.size_normal), I18N.m15706(R.string.size_large)};
        String string = TVApplication.m6285().getString("pref_sub_down_path", f13215);
        this.f13216 = findPreference("pref_sub_down_path");
        this.f13216.setSummary(string);
        this.f13216.setDefaultValue(string);
        this.f13216.setOnPreferenceClickListener(this);
        String string2 = TVApplication.m6285().getString("pref_media_down_path", Utils.m6397());
        int i = TVApplication.m6285().getInt("pref_choose_media_down_manager", 0);
        this.f13218 = findPreference("pref_media_down_path");
        this.f13218.setSummary(string2);
        this.f13218.setDefaultValue(string2);
        this.f13218.setEnabled(i == 0);
        this.f13218.setOnPreferenceClickListener(this);
        this.f13243 = findPreference("pref_app_lang");
        this.f13243.setSummary(TyphoonApp.m6330().get(TVApplication.m6285().getString("pref_app_lang", "")));
        this.f13243.setOnPreferenceClickListener(this);
        String string3 = TVApplication.m6285().getString("pref_choose_default_player", BasePlayerHelper.m16035().m16041());
        if (BasePlayerHelper.m16037()) {
            if (!string3.equalsIgnoreCase("Yes")) {
                TVApplication.m6285().edit().putString("pref_choose_default_player", "Yes").apply();
            }
            string3 = "Yes";
        }
        this.f13242 = findPreference("pref_choose_default_player");
        this.f13242.setOnPreferenceClickListener(this);
        this.f13242.setSummary(string3);
        this.f13244 = findPreference("pref_remove_ads");
        this.f13244.setOnPreferenceClickListener(this);
        if (TyphoonApp.f5865) {
            this.f13244.setEnabled(false);
            this.f13244.setSummary("Activated");
        }
        this.f13219 = findPreference("pref_choose_media_down_manager");
        this.f13219.setSummary(this.f13238[i]);
        this.f13219.setOnPreferenceClickListener(this);
        HashMap<String, String> r31 = Utils.m6416(I18N.m15704());
        String string4 = TVApplication.m6285().getString("pref_synopsis_language", "en-US");
        this.f13230 = findPreference("pref_synopsis_choose_lang");
        this.f13230.setSummary(r31.get(string4));
        this.f13230.setOnPreferenceClickListener(this);
        int i2 = TVApplication.m6285().getInt("pref_poster_size", 1);
        this.f13233 = findPreference("pref_poster_size");
        this.f13233.setSummary(this.f13239[i2]);
        this.f13233.setEnabled(TVApplication.m6285().getBoolean("pref_modern_ui", true));
        this.f13233.setOnPreferenceClickListener(this);
        findPreference("pref_modern_ui").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                if (((Boolean) obj).booleanValue()) {
                    SettingsActivity.this.f13233.setEnabled(true);
                } else {
                    SettingsActivity.this.f13233.setEnabled(false);
                }
                Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.restart_to_see_the_difference), 1).show();
                return true;
            }
        });
        findPreference("pref_force_tv_mode").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object obj) {
                Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.restart_to_see_the_difference), 1).show();
                return true;
            }
        });
        this.f13240 = new String[]{I18N.m15706(R.string.tv_shows), I18N.m15706(R.string.movies)};
        int i3 = TVApplication.m6285().getInt("pref_choose_default_nav", 0);
        this.f13234 = findPreference("pref_choose_default_nav");
        this.f13234.setSummary(this.f13240[i3]);
        this.f13234.setOnPreferenceClickListener(this);
        ArrayList<String> r34 = TvShowCategoryHelper.m16002();
        this.f13241 = (String[]) r34.toArray(new String[r34.size()]);
        String str = this.f13241[TVApplication.m6285().getInt("pref_choose_default_category_tv_shows", 1)];
        this.f13224 = findPreference("pref_choose_default_category_tv_shows");
        this.f13224.setSummary(str);
        this.f13224.setOnPreferenceClickListener(this);
        ArrayList<String> r19 = MovieCategoryHelper.m15998();
        this.f13245 = (String[]) r19.toArray(new String[r19.size()]);
        String str2 = this.f13245[TVApplication.m6285().getInt("pref_choose_default_category_movies", 1)];
        this.f13221 = findPreference("pref_choose_default_category_movies");
        this.f13221.setSummary(str2);
        this.f13221.setOnPreferenceClickListener(this);
        List<String> r3 = PlayActionHelper.m15963(false);
        this.f13248 = (String[]) r3.toArray(new String[r3.size()]);
        int i4 = TVApplication.m6285().getInt("pref_choose_default_play_action", 0);
        this.f13222 = findPreference("pref_choose_default_play_action");
        this.f13222.setSummary(this.f13248[i4]);
        this.f13222.setOnPreferenceClickListener(this);
        this.f13235 = ContinuePlaybackHelper.m15931();
        int i5 = TVApplication.m6285().getInt("pref_default_continue_playback_option", 1);
        this.f13246 = findPreference("pref_default_continue_playback_option");
        this.f13246.setSummary(this.f13235[i5]);
        this.f13246.setOnPreferenceClickListener(this);
        this.f13247 = findPreference("pref_check_new_episode_frequency");
        this.f13247.setSummary(String.valueOf(TVApplication.m6285().getInt("pref_check_new_episode_frequency", 2)));
        this.f13247.setOnPreferenceClickListener(this);
        this.f13226 = findPreference("pref_check_new_movie_release_frequency");
        this.f13226.setSummary(String.valueOf(TVApplication.m6285().getInt("pref_check_new_movie_release_frequency", 24)));
        this.f13226.setOnPreferenceClickListener(this);
        this.f13227 = findPreference("pref_sources_list_refresh_interval");
        this.f13227.setSummary(String.valueOf(TVApplication.m6285().getInt("pref_sources_list_refresh_interval", Utils.m6394())));
        this.f13227.setOnPreferenceClickListener(this);
        this.f13225 = findPreference("pref_cc_subs_font_scale");
        this.f13225.setSummary(Float.toString(TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f)));
        this.f13225.setOnPreferenceClickListener(this);
        this.f13229 = findPreference("pref_cc_subs_font_color");
        this.f13229.setSummary(TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        this.f13229.setOnPreferenceClickListener(this);
        this.f13231 = findPreference("pref_cc_subs_bg_color");
        this.f13231.setSummary(TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        this.f13231.setOnPreferenceClickListener(this);
        this.f13232 = findPreference("pref_alluc_api_key");
        this.f13232.setSummary(TVApplication.m6285().getString("pref_alluc_api_key", ""));
        this.f13232.setOnPreferenceClickListener(this);
        this.f13236 = findPreference("pref_alluc_results_limit");
        this.f13236.setSummary(Integer.toString(TVApplication.m6285().getInt("pref_alluc_results_limit", 243)));
        this.f13236.setOnPreferenceClickListener(this);
        CheckBoxPreference checkBoxPreference = (CheckBoxPreference) findPreference("pref_auto_resolve_hd_links_only");
        checkBoxPreference.setDefaultValue(Boolean.valueOf(!DeviceUtils.m6387()));
        checkBoxPreference.setChecked(TVApplication.m6285().getBoolean("pref_auto_resolve_hd_links_only", !DeviceUtils.m6387()));
        Preference findPreference = findPreference("pref_backup_settings");
        findPreference.setSummary(I18N.m15707(R.string.backup_path, PrefsBackupRestoreHelper.f12564));
        findPreference.setOnPreferenceClickListener(this);
        Preference findPreference2 = findPreference("pref_restore_settings");
        findPreference2.setSummary(I18N.m15707(R.string.restore_path, PrefsBackupRestoreHelper.f12564));
        findPreference2.setOnPreferenceClickListener(this);
        Preference findPreference3 = findPreference("pref_backup_favorites");
        findPreference3.setSummary(I18N.m15707(R.string.backup_path, FavBackupRestoreHelper.f12562));
        findPreference3.setOnPreferenceClickListener(this);
        Preference findPreference4 = findPreference("pref_restore_favorites");
        findPreference4.setSummary(I18N.m15707(R.string.restore_path, FavBackupRestoreHelper.f12562));
        findPreference4.setOnPreferenceClickListener(this);
        Preference findPreference5 = findPreference("pref_backup_watched_episodes");
        findPreference5.setSummary(I18N.m15707(R.string.backup_path, WatchedEpsBackupRestoreHelper.f12568));
        findPreference5.setOnPreferenceClickListener(this);
        Preference findPreference6 = findPreference("pref_restore_watched_episodes");
        findPreference6.setSummary(I18N.m15707(R.string.restore_path, WatchedEpsBackupRestoreHelper.f12568));
        findPreference6.setOnPreferenceClickListener(this);
        findPreference("pref_enabled_tv_shows_providers").setOnPreferenceClickListener(this);
        findPreference("pref_enabled_movies_providers").setOnPreferenceClickListener(this);
        findPreference("pref_enabled_resolvers").setOnPreferenceClickListener(this);
        findPreference("pref_clear_cache").setOnPreferenceClickListener(this);
        findPreference("pref_about_app").setOnPreferenceClickListener(this);
        findPreference("pref_privacy_policy").setOnPreferenceClickListener(this);
        findPreference("pref_translators").setOnPreferenceClickListener(this);
        findPreference("pref_opensubtitles_credit").setOnPreferenceClickListener(this);
        findPreference("pref_changelog").setOnPreferenceClickListener(this);
        findPreference("pref_sub_language_international").setOnPreferenceClickListener(this);
        TraktCredentialsInfo r5 = TraktCredentialsHelper.m16110();
        String user = r5.getUser();
        this.f13228 = findPreference("pref_auth_trakt");
        this.f13228.setSummary(r5.isValid() ? (user == null || user.isEmpty()) ? "Status: Logged in" : "Status: Logged in as " + r5.getUser() : "");
        this.f13228.setEnabled(!r5.isValid());
        this.f13228.setOnPreferenceClickListener(this);
        RealDebridCredentialsInfo r30 = RealDebridCredentialsHelper.m15838();
        this.f13223 = findPreference("pref_auth_real_debrid");
        this.f13223.setSummary(r30.isValid() ? "Status: Logged in" : "");
        this.f13223.setEnabled(!r30.isValid());
        this.f13223.setOnPreferenceClickListener(this);
        findPreference("pref_clear_trakt_credentials").setOnPreferenceClickListener(this);
        findPreference("pref_clear_real_debrid_credentials").setOnPreferenceClickListener(this);
        findPreference("pref_sync_watched_episodes").setOnPreferenceClickListener(this);
        this.f13237.m25086(RxBus.m15709().m15710().m7397(new Action1<Object>() {
            public void call(Object obj) {
                if (obj instanceof TraktWaitingToVerifyEvent) {
                    TraktWaitingToVerifyEvent traktWaitingToVerifyEvent = (TraktWaitingToVerifyEvent) obj;
                    String verificationUrl = traktWaitingToVerifyEvent.getVerificationUrl();
                    String userCode = traktWaitingToVerifyEvent.getUserCode();
                    Intent intent = new Intent(SettingsActivity.this, TraktAuthWebViewActivity.class);
                    intent.putExtra("verificationUrl", verificationUrl);
                    intent.putExtra("userCode", userCode);
                    SettingsActivity.this.startActivity(intent);
                } else if (!(obj instanceof TraktGetTokenSuccessEvent) && !(obj instanceof TraktGetTokenFailedEvent) && (obj instanceof TraktUserCancelledAuthEvent)) {
                    SettingsActivity.this.m17003();
                }
                if (obj instanceof RealDebridWaitingToVerifyEvent) {
                    RealDebridWaitingToVerifyEvent realDebridWaitingToVerifyEvent = (RealDebridWaitingToVerifyEvent) obj;
                    String verificationUrl2 = realDebridWaitingToVerifyEvent.getVerificationUrl();
                    String userCode2 = realDebridWaitingToVerifyEvent.getUserCode();
                    Intent intent2 = new Intent(SettingsActivity.this, RealDebridAuthWebViewActivity.class);
                    intent2.putExtra("verificationUrl", verificationUrl2);
                    intent2.putExtra("userCode", userCode2);
                    SettingsActivity.this.startActivity(intent2);
                } else if (!(obj instanceof TraktGetTokenSuccessEvent) && !(obj instanceof TraktGetTokenFailedEvent) && (obj instanceof TraktUserCancelledAuthEvent)) {
                    SettingsActivity.this.m17007();
                }
            }
        }));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f13237 != null && !this.f13237.isUnsubscribed()) {
            this.f13237.unsubscribe();
        }
        m17003();
        m17007();
        super.onDestroy();
    }

    public boolean onPreferenceClick(Preference preference) {
        if (isFinishing()) {
            return true;
        }
        String key = preference.getKey();
        char c = 65535;
        switch (key.hashCode()) {
            case -2119292554:
                if (key.equals("pref_restore_watched_episodes")) {
                    c = 30;
                    break;
                }
                break;
            case -1852191215:
                if (key.equals("pref_remove_ads")) {
                    c = ')';
                    break;
                }
                break;
            case -1712521849:
                if (key.equals("pref_synopsis_choose_lang")) {
                    c = 21;
                    break;
                }
                break;
            case -1306375758:
                if (key.equals("pref_auth_real_debrid")) {
                    c = '%';
                    break;
                }
                break;
            case -1303173807:
                if (key.equals("pref_sync_watched_episodes")) {
                    c = '(';
                    break;
                }
                break;
            case -1269314981:
                if (key.equals("pref_default_continue_playback_option")) {
                    c = 4;
                    break;
                }
                break;
            case -1251098170:
                if (key.equals("pref_alluc_results_limit")) {
                    c = '#';
                    break;
                }
                break;
            case -1157444781:
                if (key.equals("pref_about_app")) {
                    c = 12;
                    break;
                }
                break;
            case -1118397033:
                if (key.equals("pref_choose_media_down_manager")) {
                    c = 5;
                    break;
                }
                break;
            case -1109372112:
                if (key.equals("pref_choose_default_category_tv_shows")) {
                    c = 8;
                    break;
                }
                break;
            case -1104416233:
                if (key.equals("pref_choose_default_play_action")) {
                    c = 10;
                    break;
                }
                break;
            case -967846043:
                if (key.equals("pref_privacy_policy")) {
                    c = 14;
                    break;
                }
                break;
            case -925134208:
                if (key.equals("pref_enabled_movies_providers")) {
                    c = 18;
                    break;
                }
                break;
            case -782842983:
                if (key.equals("pref_choose_default_nav")) {
                    c = 7;
                    break;
                }
                break;
            case -612801391:
                if (key.equals("pref_auth_trakt")) {
                    c = '$';
                    break;
                }
                break;
            case -559610544:
                if (key.equals("pref_restore_settings")) {
                    c = 26;
                    break;
                }
                break;
            case -311063966:
                if (key.equals("pref_sub_language_international")) {
                    c = 20;
                    break;
                }
                break;
            case -173271557:
                if (key.equals("pref_clear_trakt_credentials")) {
                    c = '&';
                    break;
                }
                break;
            case -92002190:
                if (key.equals("pref_cc_subs_bg_color")) {
                    c = '!';
                    break;
                }
                break;
            case -25519140:
                if (key.equals("pref_clear_real_debrid_credentials")) {
                    c = '\'';
                    break;
                }
                break;
            case 41834131:
                if (key.equals("pref_enabled_resolvers")) {
                    c = 19;
                    break;
                }
                break;
            case 64021675:
                if (key.equals("pref_choose_default_player")) {
                    c = 3;
                    break;
                }
                break;
            case 71418055:
                if (key.equals("pref_sources_list_refresh_interval")) {
                    c = 22;
                    break;
                }
                break;
            case 160310244:
                if (key.equals("pref_backup_settings")) {
                    c = 25;
                    break;
                }
                break;
            case 379766679:
                if (key.equals("pref_poster_size")) {
                    c = 6;
                    break;
                }
                break;
            case 619267048:
                if (key.equals("pref_app_lang")) {
                    c = 2;
                    break;
                }
                break;
            case 639627591:
                if (key.equals("pref_sub_down_path")) {
                    c = 0;
                    break;
                }
                break;
            case 817776330:
                if (key.equals("pref_restore_favorites")) {
                    c = 28;
                    break;
                }
                break;
            case 1165094269:
                if (key.equals("pref_translators")) {
                    c = CharUtils.CR;
                    break;
                }
                break;
            case 1183480931:
                if (key.equals("pref_check_new_movie_release_frequency")) {
                    c = 24;
                    break;
                }
                break;
            case 1354240334:
                if (key.equals("pref_alluc_api_key")) {
                    c = '\"';
                    break;
                }
                break;
            case 1379734339:
                if (key.equals("pref_opensubtitles_credit")) {
                    c = 15;
                    break;
                }
                break;
            case 1528052412:
                if (key.equals("pref_cc_subs_font_color")) {
                    c = ' ';
                    break;
                }
                break;
            case 1542460579:
                if (key.equals("pref_cc_subs_font_scale")) {
                    c = 31;
                    break;
                }
                break;
            case 1637170808:
                if (key.equals("pref_changelog")) {
                    c = 16;
                    break;
                }
                break;
            case 1660484278:
                if (key.equals("pref_backup_favorites")) {
                    c = 27;
                    break;
                }
                break;
            case 1685306603:
                if (key.equals("pref_media_down_path")) {
                    c = 1;
                    break;
                }
                break;
            case 1708660310:
                if (key.equals("pref_enabled_tv_shows_providers")) {
                    c = 17;
                    break;
                }
                break;
            case 1747582708:
                if (key.equals("pref_clear_cache")) {
                    c = 11;
                    break;
                }
                break;
            case 1809151782:
                if (key.equals("pref_check_new_episode_frequency")) {
                    c = 23;
                    break;
                }
                break;
            case 1881764874:
                if (key.equals("pref_backup_watched_episodes")) {
                    c = 29;
                    break;
                }
                break;
            case 2143487770:
                if (key.equals("pref_choose_default_category_movies")) {
                    c = 9;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", false);
                intent.putExtra("nononsense.intent.ALLOW_CREATE_DIR", true);
                intent.putExtra("nononsense.intent.MODE", 1);
                intent.putExtra("nononsense.intent.START_PATH", preference.getSummary());
                startActivityForResult(intent, preference.getKey().equals("pref_sub_down_path") ? 0 : 1);
                return true;
            case 2:
                final LinkedHashMap<String, String> r7 = TyphoonApp.m6330();
                ArrayList arrayList = new ArrayList(r7.values());
                final String[] strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_app_lang)).m539((CharSequence[]) strArr, arrayList.indexOf(r7.get(TVApplication.m6285().getString("pref_app_lang", ""))), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putString("pref_app_lang", (String) Utils.m6412(r7, strArr[i])).apply();
                        SettingsActivity.this.f13243.setSummary(strArr[i]);
                        dialogInterface.dismiss();
                        Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.restart_to_see_the_difference), 1).show();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m528();
                return true;
            case 3:
                BasePlayerHelper.m16036((Activity) this, (BasePlayerHelper.OnChoosePlayerListener) new BasePlayerHelper.OnChoosePlayerListener() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m17054(String str) {
                        if (str != null && !str.isEmpty()) {
                            SettingsActivity.this.f13242.setSummary(str);
                        }
                    }
                });
                return true;
            case 4:
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_default_continue_playback_option)).m539((CharSequence[]) this.f13235, TVApplication.m6285().getInt("pref_default_continue_playback_option", 1), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_default_continue_playback_option", i).apply();
                        SettingsActivity.this.f13246.setSummary(SettingsActivity.this.f13235[i]);
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m528();
                return true;
            case 5:
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_choose_media_down_manager)).m539((CharSequence[]) this.f13238, TVApplication.m6285().getInt("pref_choose_media_down_manager", 0), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_choose_media_down_manager", i).apply();
                        SettingsActivity.this.f13219.setSummary(SettingsActivity.this.f13238[i]);
                        SettingsActivity.this.f13218.setEnabled(i == 0);
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m528();
                return true;
            case 6:
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_poster_size)).m539((CharSequence[]) this.f13239, TVApplication.m6285().getInt("pref_poster_size", 1), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_poster_size", i).apply();
                        SettingsActivity.this.f13233.setSummary(SettingsActivity.this.f13239[i]);
                        dialogInterface.dismiss();
                        Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.restart_to_see_the_difference), 1).show();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m528();
                return true;
            case 7:
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_choose_default_nav)).m539((CharSequence[]) this.f13240, TVApplication.m6285().getInt("pref_choose_default_nav", 0), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_choose_default_nav", i).apply();
                        SettingsActivity.this.f13234.setSummary(SettingsActivity.this.f13240[i]);
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m528();
                return true;
            case 8:
                final AlertDialog r4 = new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_choose_default_category_tv_shows)).m539((CharSequence[]) this.f13241, TVApplication.m6285().getInt("pref_choose_default_category_tv_shows", 1), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_choose_default_category_tv_shows", i).apply();
                        SettingsActivity.this.f13224.setSummary(SettingsActivity.this.f13241[i]);
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m525();
                r4.setOnShowListener(new DialogInterface.OnShowListener() {
                    public void onShow(DialogInterface dialogInterface) {
                        ListView r0 = r4.m516();
                        if (r0 != null) {
                            r0.smoothScrollToPosition(0);
                        }
                    }
                });
                r4.show();
                return true;
            case 9:
                final AlertDialog r3 = new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_choose_default_category_movies)).m539((CharSequence[]) this.f13245, TVApplication.m6285().getInt("pref_choose_default_category_movies", 1), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_choose_default_category_movies", i).apply();
                        SettingsActivity.this.f13221.setSummary(SettingsActivity.this.f13245[i]);
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m525();
                r3.setOnShowListener(new DialogInterface.OnShowListener() {
                    public void onShow(DialogInterface dialogInterface) {
                        ListView r0 = r3.m516();
                        if (r0 != null) {
                            r0.smoothScrollToPosition(0);
                        }
                    }
                });
                r3.show();
                return true;
            case 10:
                new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.pref_choose_default_play_action)).m539((CharSequence[]) this.f13248, TVApplication.m6285().getInt("pref_choose_default_play_action", 0), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TVApplication.m6285().edit().putInt("pref_choose_default_play_action", i).apply();
                        SettingsActivity.this.f13222.setSummary(SettingsActivity.this.f13248[i]);
                        dialogInterface.dismiss();
                    }
                }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).m528();
                return true;
            case 11:
                try {
                    Utils.m6421(TVApplication.m6288().getCacheDir());
                    Toast.makeText(this, I18N.m15706(R.string.cache_cleared), 1).show();
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    Toast.makeText(this, I18N.m15706(R.string.error), 1).show();
                }
                return true;
            case 12:
                try {
                    PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    String str = "";
                    if (packageInfo != null) {
                        str = packageInfo.versionName;
                    }
                    if (isFinishing()) {
                        return true;
                    }
                    new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.about_app_title)).m523((CharSequence) I18N.m15707(R.string.about_app_message, str)).m520((int) R.mipmap.ic_launcher).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).m528();
                    return true;
                } catch (PackageManager.NameNotFoundException e2) {
                    Snackbar.make((View) this.f13728, (CharSequence) I18N.m15706(R.string.error), 0).show();
                    return true;
                }
            case 13:
                Utils.m6419((Activity) this);
                return true;
            case 14:
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setData(Uri.parse("https://discord.gg/PhY9cs2"));
                    intent2.setFlags(268435456);
                    startActivity(intent2);
                } catch (Exception e3) {
                    Logger.m6281((Throwable) e3, new boolean[0]);
                    Toast.makeText(this, I18N.m15706(R.string.error), 0).show();
                }
                return true;
            case 15:
                try {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    intent3.setData(Uri.parse("http://real-debrid.com/?id=6089437"));
                    intent3.setFlags(268435456);
                    startActivity(intent3);
                } catch (Exception e4) {
                    Logger.m6281((Throwable) e4, new boolean[0]);
                    Toast.makeText(this, I18N.m15706(R.string.error), 0).show();
                }
                return true;
            case 16:
                final AlertDialog r8 = new AlertDialog.Builder(this).m536((CharSequence) I18N.m15706(R.string.changelog)).m525();
                r8.m518((View) (ChangeLogRecyclerView) getLayoutInflater().inflate(R.layout.dialog_changelog, (ViewGroup) null));
                r8.m517(-1, I18N.m15706(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                r8.setOnShowListener(new DialogInterface.OnShowListener() {
                    public void onShow(DialogInterface dialogInterface) {
                        Button r0 = r8.m515(-1);
                        r0.setFocusable(true);
                        r0.setFocusableInTouchMode(true);
                        r0.requestFocus();
                    }
                });
                r8.show();
                return true;
            case 17:
                m17042(false, key);
                return true;
            case 18:
                m17042(true, key);
                return true;
            case 19:
                m17036();
                return true;
            case 20:
                m17039();
                return true;
            case 21:
                m17030();
                return true;
            case 22:
                m17035();
                return true;
            case 23:
                m17029();
                return true;
            case 24:
                m16992();
                return true;
            case 25:
                m17041(PrefsBackupRestoreHelper.m15825());
                return true;
            case 26:
                m17033(PrefsBackupRestoreHelper.m15824());
                return true;
            case 27:
                m16995();
                return true;
            case 28:
                m16996();
                return true;
            case 29:
                m17016();
                return true;
            case 30:
                m17020();
                return true;
            case 31:
                m17021();
                return true;
            case ' ':
                m17004();
                return true;
            case '!':
                m16999();
                return true;
            case '\"':
                m17000();
                return true;
            case '#':
                m17045();
                return true;
            case '$':
                m17010();
                return false;
            case '%':
                m17013();
                return false;
            case '&':
                TraktCredentialsHelper.m16109();
                CookieManager.getInstance().removeAllCookie();
                HttpHelper.m6343().m6357("https://api.trakt.tv", "__beaconTrackerID=; __gacid=;");
                if (this.f13228 != null) {
                    this.f13228.setSummary("");
                    this.f13228.setEnabled(true);
                }
                return true;
            case '\'':
                RealDebridCredentialsHelper.m15837();
                CookieManager.getInstance().removeAllCookie();
                HttpHelper.m6343().m6357("https://api.real-debrid.com", "__beaconTrackerID=; __gacid=;");
                if (this.f13223 != null) {
                    this.f13223.setSummary("");
                    this.f13223.setEnabled(true);
                }
                return true;
            case '(':
                m17046();
                m17009();
                return true;
            case ')':
                Toast.makeText(this, "Unknown error while checking payment records. Try again later.", 1).show();
                return true;
            default:
                return false;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m17035() {
        int currentInterval = TVApplication.m6285().getInt("pref_sources_list_refresh_interval", 5);
        FrameLayout frameLayout = new FrameLayout(this);
        final NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setMinValue(2);
        numberPicker.setMaxValue(15);
        numberPicker.setValue(currentInterval);
        numberPicker.setDescendantFocusability(393216);
        frameLayout.addView(numberPicker, new FrameLayout.LayoutParams(-2, -2, 17));
        new AlertDialog.Builder(this).m523((CharSequence) I18N.m15706(R.string.pref_sources_list_refresh_interval_msg)).m522((View) frameLayout).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int value = numberPicker.getValue();
                SettingsActivity.this.f13227.setSummary(String.valueOf(value));
                TVApplication.m6285().edit().putInt("pref_sources_list_refresh_interval", value).apply();
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }
}
