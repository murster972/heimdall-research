package com.typhoon.tv.ui.activity.base;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.adincube.sdk.AdinCube;
import com.google.firebase.messaging.FirebaseMessaging;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.utils.LocaleUtils;

public abstract class BasePreferenceActivity extends PreferenceActivity {

    /* renamed from: 靐  reason: contains not printable characters */
    protected LinearLayout f13728;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (TVApplication.m6285().getBoolean("pref_force_tv_mode", false)) {
            try {
                Resources resources = getResources();
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                Configuration configuration = resources.getConfiguration();
                configuration.uiMode = 4;
                resources.updateConfiguration(configuration, displayMetrics);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        super.onCreate(bundle);
        AdinCube.m2301("734595550db947ffab46");
        if (!TyphoonApp.f5863) {
            AdinCube.Interstitial.m2307((Activity) this);
            TyphoonApp.f5863 = true;
        }
        String trim = TVApplication.m6285().getString("pref_app_lang", "").trim();
        if (!trim.isEmpty()) {
            LocaleUtils.m17793(TVApplication.m6288(), LocaleUtils.m17792(trim));
        } else {
            LocaleUtils.m17793(TVApplication.m6288(), Resources.getSystem().getConfiguration().locale);
        }
        FirebaseMessaging.m6109().m6110("allDevices");
        try {
            PackageInfo packageInfo = TVApplication.m6288().getPackageManager().getPackageInfo(TVApplication.m6288().getPackageName(), 0);
            if (packageInfo != null && packageInfo.versionName != null && packageInfo.versionName.length() > 0) {
                FirebaseMessaging.m6109().m6110(packageInfo.versionName);
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View onCreateView = super.onCreateView(str, context, attributeSet);
        if (onCreateView != null) {
            return onCreateView;
        }
        if (Build.VERSION.SDK_INT < 21) {
            char c = 65535;
            switch (str.hashCode()) {
                case -1455429095:
                    if (str.equals("CheckedTextView")) {
                        c = 4;
                        break;
                    }
                    break;
                case -339785223:
                    if (str.equals("Spinner")) {
                        c = 1;
                        break;
                    }
                    break;
                case 776382189:
                    if (str.equals("RadioButton")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1601505219:
                    if (str.equals("CheckBox")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1666676343:
                    if (str.equals("EditText")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return new AppCompatEditText(this, attributeSet);
                case 1:
                    return new AppCompatSpinner((Context) this, attributeSet);
                case 2:
                    return new AppCompatCheckBox(this, attributeSet);
                case 3:
                    return new AppCompatRadioButton(this, attributeSet);
                case 4:
                    return new AppCompatCheckedTextView(this, attributeSet);
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        LinearLayout linearLayout = (LinearLayout) findViewById(16908298).getParent().getParent().getParent();
        if (this.f13728 == null) {
            this.f13728 = linearLayout;
        }
        Toolbar toolbar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.toolbar_settings, linearLayout, false);
        linearLayout.addView(toolbar, 0);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                BasePreferenceActivity.this.finish();
            }
        });
    }
}
