package com.typhoon.tv.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import com.typhoon.tv.model.media.tv.TvSeasonInfo;

public class TvSeasonInfoArrayAdapter extends ArrayAdapter<TvSeasonInfo> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f13785;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f13786;

    /* renamed from: 齉  reason: contains not printable characters */
    private final TvSeasonInfo[] f13787;

    /* renamed from: 龘  reason: contains not printable characters */
    private final LayoutInflater f13788;

    public TvSeasonInfoArrayAdapter(Context context, int i, TvSeasonInfo[] tvSeasonInfoArr) {
        super(context, i, tvSeasonInfoArr);
        this.f13785 = i;
        this.f13788 = LayoutInflater.from(context);
        this.f13787 = tvSeasonInfoArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private View m17531(boolean z, int i, View view, ViewGroup viewGroup) {
        View view2;
        CheckedTextView checkedTextView;
        if (view == null) {
            view2 = this.f13788.inflate(z ? this.f13786 : this.f13785, viewGroup, false);
        } else {
            view2 = view;
        }
        if (z) {
            try {
                checkedTextView = (CheckedTextView) view2;
            } catch (ClassCastException e) {
                throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", e);
            }
        } else {
            checkedTextView = (TextView) view2;
        }
        checkedTextView.setText(this.f13787[i].getSeasonName());
        return z ? (CheckedTextView) checkedTextView : checkedTextView;
    }

    public int getCount() {
        return this.f13787.length;
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        return m17531(true, i, view, viewGroup);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return m17531(false, i, view, viewGroup);
    }

    public void setDropDownViewResource(int i) {
        super.setDropDownViewResource(i);
        this.f13786 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TvSeasonInfo getItem(int i) {
        return this.f13787[i];
    }
}
