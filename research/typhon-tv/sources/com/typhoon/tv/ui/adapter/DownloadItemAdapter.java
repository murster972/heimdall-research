package com.typhoon.tv.ui.adapter;

import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.download.DownloadItem;
import com.typhoon.tv.ui.viewholder.DownloadItemViewHolder;
import java.util.ArrayList;
import java.util.List;

public class DownloadItemAdapter extends RecyclerView.Adapter<DownloadItemViewHolder> {

    /* renamed from: 靐  reason: contains not printable characters */
    private DownloadItemViewHolder.OnCardClickListener f13774;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<DownloadItem> f13775;

    public DownloadItemAdapter(List<DownloadItem> list) {
        this.f13775 = list;
    }

    public int getItemCount() {
        return this.f13775.size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17501() {
        this.f13775.clear();
        notifyDataSetChanged();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17502(int i) {
        this.f13775.remove(i);
        notifyItemRemoved(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DownloadItem m17503(int i) {
        return this.f13775.get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DownloadItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new DownloadItemViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_download, viewGroup, false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<DownloadItem> m17505() {
        ArrayList<DownloadItem> arrayList = new ArrayList<>();
        for (int i = 0; i < getItemCount(); i++) {
            arrayList.add(m17503(i));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17506(DownloadItemViewHolder.OnCardClickListener onCardClickListener) {
        this.f13774 = onCardClickListener;
    }

    @SuppressLint({"SetTextI18n"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(DownloadItemViewHolder downloadItemViewHolder, int i) {
        DownloadItem downloadItem = this.f13775.get(i);
        String posterUrl = downloadItem.getPosterUrl();
        downloadItemViewHolder.f13883.setText(downloadItem.getTitle());
        downloadItemViewHolder.f13885.setText(downloadItem.getLocalUri());
        String errorMessage = downloadItem.getErrorMessage();
        if (errorMessage == null) {
            downloadItemViewHolder.f13884.setText(downloadItem.getTotalSize() == -1 ? I18N.m15706(R.string.unknown) : Formatter.formatFileSize(TVApplication.m6288(), downloadItem.getTotalSize()));
        } else {
            downloadItemViewHolder.f13884.setText("Error: " + errorMessage);
        }
        if (downloadItem.isDownloading()) {
            downloadItemViewHolder.f13882.setImageResource(R.drawable.ic_file_download_white_36dp);
            downloadItemViewHolder.f13879.setVisibility(0);
        } else {
            downloadItemViewHolder.f13882.setImageResource(R.drawable.ic_done_white_36dp);
            downloadItemViewHolder.f13879.setVisibility(8);
        }
        downloadItemViewHolder.f13883.setSelected(true);
        downloadItemViewHolder.f13883.requestFocus();
        downloadItemViewHolder.f13885.setSelected(true);
        downloadItemViewHolder.f13885.requestFocus();
        downloadItemViewHolder.f13884.setSelected(true);
        downloadItemViewHolder.f13884.requestFocus();
        if (posterUrl == null || posterUrl.isEmpty()) {
            Glide.m3941((View) downloadItemViewHolder.f13886);
            downloadItemViewHolder.f13886.setImageDrawable((Drawable) null);
        } else {
            downloadItemViewHolder.f13886.setScaleType(ImageView.ScaleType.FIT_CENTER);
            Glide.m3934(TVApplication.m6288()).m3974(posterUrl).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(-16777216)).m25196().m25209(downloadItemViewHolder.f13886);
        }
        downloadItemViewHolder.m17618(this.f13774);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17508(List<DownloadItem> list) {
        int size = this.f13775.size();
        this.f13775.addAll(size, list);
        notifyItemRangeInserted(size, list.size());
    }
}
