package com.typhoon.tv.ui.adapter;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvNewEpisodeInfo;
import com.typhoon.tv.ui.viewholder.TvCalendarItemViewHolder;
import com.typhoon.tv.utils.TypefaceUtils;
import com.typhoon.tv.utils.Utils;
import java.util.List;

public class TvCalendarItemAdapter extends RecyclerView.Adapter<TvCalendarItemViewHolder> {

    /* renamed from: 靐  reason: contains not printable characters */
    private TvCalendarItemViewHolder.OnCardClickListener f13781;

    /* renamed from: 齉  reason: contains not printable characters */
    private TvCalendarItemViewHolder.OnCardLongClickListener f13782;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<TvNewEpisodeInfo> f13783;

    public TvCalendarItemAdapter(List<TvNewEpisodeInfo> list) {
        this.f13783 = list;
    }

    public int getItemCount() {
        return this.f13783.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TvNewEpisodeInfo m17522(int i) {
        return this.f13783.get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TvCalendarItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new TvCalendarItemViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_tv_calendar, viewGroup, false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17524() {
        this.f13783.clear();
        notifyDataSetChanged();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17525(TvCalendarItemViewHolder.OnCardClickListener onCardClickListener) {
        this.f13781 = onCardClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17526(TvCalendarItemViewHolder.OnCardLongClickListener onCardLongClickListener) {
        this.f13782 = onCardLongClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(TvCalendarItemViewHolder tvCalendarItemViewHolder, int i) {
        TvNewEpisodeInfo tvNewEpisodeInfo = this.f13783.get(i);
        MediaInfo mediaInfo = tvNewEpisodeInfo.getMediaInfo();
        tvCalendarItemViewHolder.f13901.setText(mediaInfo.getNameAndYear());
        tvCalendarItemViewHolder.f13901.setSelected(true);
        tvCalendarItemViewHolder.f13901.requestFocus();
        int season = tvNewEpisodeInfo.getSeason();
        int episode = tvNewEpisodeInfo.getEpisode();
        String title = tvNewEpisodeInfo.getTitle();
        String str = season + "x" + Utils.m6413(episode);
        if (title != null && !title.isEmpty()) {
            str = str + " - " + title;
        }
        tvCalendarItemViewHolder.f13899.setText(str);
        tvCalendarItemViewHolder.f13899.setSelected(true);
        tvCalendarItemViewHolder.f13899.requestFocus();
        String overview = tvNewEpisodeInfo.getOverview();
        if (TVApplication.m6285().getBoolean("pref_hide_episode_synopsis", false)) {
            tvCalendarItemViewHolder.f13900.setText(I18N.m15706(R.string.synopsis_is_hidden));
            tvCalendarItemViewHolder.f13900.setTypeface(TypefaceUtils.m17838());
        } else if (overview == null || overview.isEmpty()) {
            tvCalendarItemViewHolder.f13900.setText(I18N.m15706(R.string.no_synopsis));
            tvCalendarItemViewHolder.f13900.setTypeface(TypefaceUtils.m17838());
        } else {
            tvCalendarItemViewHolder.f13900.setText(overview);
            tvCalendarItemViewHolder.f13900.setTypeface(TypefaceUtils.m17839());
        }
        String posterUrl = mediaInfo.getPosterUrl();
        if (posterUrl == null || posterUrl.isEmpty()) {
            tvCalendarItemViewHolder.f13902.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            Glide.m3934(TVApplication.m6288()).m3973(Integer.valueOf(R.drawable.ic_live_tv_white_36dp)).m25186(96, 96).m25185().m25209(tvCalendarItemViewHolder.f13902);
        } else {
            tvCalendarItemViewHolder.f13902.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.m3934(TVApplication.m6288()).m3974(posterUrl).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(-16777216)).m25196().m25209(tvCalendarItemViewHolder.f13902);
        }
        tvCalendarItemViewHolder.m17626(this.f13781);
        tvCalendarItemViewHolder.m17627(this.f13782);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17528(List<TvNewEpisodeInfo> list) {
        int size = this.f13783.size();
        this.f13783.addAll(size, list);
        notifyItemRangeInserted(size, list.size());
    }
}
