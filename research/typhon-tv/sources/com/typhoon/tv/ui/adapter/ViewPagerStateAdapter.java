package com.typhoon.tv.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.ArrayList;
import java.util.List;

public class ViewPagerStateAdapter extends FragmentStatePagerAdapter {

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<Fragment> f13789 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<String> f13790 = new ArrayList();

    public ViewPagerStateAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public int getCount() {
        return this.f13790.size();
    }

    public Fragment getItem(int i) {
        return this.f13789.get(i);
    }

    public CharSequence getPageTitle(int i) {
        return this.f13790.get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Fragment> m17533() {
        return this.f13789;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17534(Fragment fragment, String str) {
        this.f13789.add(fragment);
        this.f13790.add(str);
    }
}
