package com.typhoon.tv.ui.adapter;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvLatestPlayed;
import com.typhoon.tv.model.media.tv.TvSeasonInfo;
import com.typhoon.tv.ui.DrawGradientTransformation;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.viewholder.MediaPosterCardViewHolder;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.TypefaceUtils;
import java.util.List;

public class TvSeasonAdapter extends BaseTvCardAdapter<TvSeasonInfo> {

    /* renamed from: 麤  reason: contains not printable characters */
    private final MediaInfo f13784;

    public TvSeasonAdapter(MediaInfo mediaInfo, List<TvSeasonInfo> list) {
        super(list);
        this.f13784 = mediaInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaCardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return TVApplication.m6285().getBoolean("pref_modern_ui", true) ? new MediaPosterCardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_media_poster, viewGroup, false)) : new MediaCardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_media, viewGroup, false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(MediaCardViewHolder mediaCardViewHolder, int i) {
        TvSeasonInfo tvSeasonInfo = (TvSeasonInfo) this.f13773.get(i);
        boolean z = mediaCardViewHolder instanceof MediaPosterCardViewHolder;
        if (mediaCardViewHolder.f13892 != null) {
            mediaCardViewHolder.f13892.setImageDrawable((Drawable) null);
        }
        mediaCardViewHolder.f13889.setText(tvSeasonInfo.getSeasonName());
        mediaCardViewHolder.f13889.setVisibility(0);
        boolean z2 = false;
        TvLatestPlayed r3 = TVApplication.m6287().m6296(Integer.valueOf(this.f13784.getTmdbId()));
        if (r3 != null && tvSeasonInfo.getSeasonNum() == r3.getSeason()) {
            z2 = true;
            int color = ContextCompat.getColor(TVApplication.m6288(), R.color.light_blue_transparent_highlight);
            mediaCardViewHolder.f13889.setBackgroundColor(color);
            if (z) {
                MediaPosterCardViewHolder mediaPosterCardViewHolder = (MediaPosterCardViewHolder) mediaCardViewHolder;
                mediaPosterCardViewHolder.f13893.setBackgroundColor(color);
                mediaPosterCardViewHolder.f13893.setAlpha(1.0f);
            } else if (mediaCardViewHolder.f13891 != null) {
                mediaCardViewHolder.f13891.setBackgroundColor(color);
                mediaCardViewHolder.f13891.setAlpha(1.0f);
            }
        }
        if (z) {
            mediaCardViewHolder.f13889.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            mediaCardViewHolder.f13889.setMaxLines(1);
            mediaCardViewHolder.f13889.setSingleLine(true);
            mediaCardViewHolder.f13889.setSelected(true);
            mediaCardViewHolder.f13889.requestFocus();
        } else {
            mediaCardViewHolder.f13889.setEllipsize(TextUtils.TruncateAt.END);
            mediaCardViewHolder.f13889.setSelected(false);
            if (TVApplication.m6285().getInt("pref_poster_size", 1) == 2 || DeviceUtils.m6389(new boolean[0])) {
                mediaCardViewHolder.f13889.setMaxLines(2);
            } else {
                mediaCardViewHolder.f13889.setMaxLines(1);
            }
            mediaCardViewHolder.f13889.setSingleLine(false);
        }
        if (!z2) {
            mediaCardViewHolder.f13889.setBackgroundColor(0);
        }
        if (z) {
            MediaPosterCardViewHolder mediaPosterCardViewHolder2 = (MediaPosterCardViewHolder) mediaCardViewHolder;
            if (tvSeasonInfo.getAirDate() == null || tvSeasonInfo.getAirDate().isEmpty()) {
                mediaPosterCardViewHolder2.f13893.setText("");
            } else {
                mediaPosterCardViewHolder2.f13893.setText(tvSeasonInfo.getAirDate());
            }
            mediaPosterCardViewHolder2.f13889.setTypeface(TypefaceUtils.m17839());
            mediaPosterCardViewHolder2.f13893.setVisibility(0);
            mediaPosterCardViewHolder2.f13893.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            mediaPosterCardViewHolder2.f13893.setMaxLines(1);
            mediaPosterCardViewHolder2.f13893.setSingleLine(true);
            mediaPosterCardViewHolder2.f13893.setSelected(true);
            mediaPosterCardViewHolder2.f13893.requestFocus();
            if (!z2) {
                mediaPosterCardViewHolder2.f13893.setAlpha(0.7f);
                mediaPosterCardViewHolder2.f13893.setBackgroundColor(0);
            }
        } else if (mediaCardViewHolder.f13891 != null) {
            if (tvSeasonInfo.getAirDate() == null || tvSeasonInfo.getAirDate().isEmpty()) {
                mediaCardViewHolder.f13891.setText("");
            } else {
                mediaCardViewHolder.f13891.setText(tvSeasonInfo.getAirDate());
            }
            mediaCardViewHolder.f13891.setVisibility(0);
            mediaCardViewHolder.f13891.setEllipsize(TextUtils.TruncateAt.END);
            mediaCardViewHolder.f13891.setMaxLines(10);
            mediaCardViewHolder.f13891.setSingleLine(false);
            mediaCardViewHolder.f13891.setSelected(false);
            if (!z2) {
                mediaCardViewHolder.f13891.setAlpha(0.7f);
                mediaCardViewHolder.f13891.setBackgroundColor(0);
            }
        }
        mediaCardViewHolder.m17620(this.f13771);
        String bannerUrl = tvSeasonInfo.getBannerUrl();
        if (mediaCardViewHolder.f13892 == null) {
            return;
        }
        if (bannerUrl == null || bannerUrl.isEmpty()) {
            mediaCardViewHolder.f13892.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            Glide.m3934(TVApplication.m6288()).m3973(Integer.valueOf(R.drawable.ic_live_tv_white_36dp)).m25208(DrawGradientTransformation.m16845(mediaCardViewHolder.f13892.getContext())).m25186(96, 96).m25185().m25209(mediaCardViewHolder.f13892);
            return;
        }
        mediaCardViewHolder.f13892.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.m3934(TVApplication.m6288()).m3974(bannerUrl).m25191(DiskCacheStrategy.SOURCE).m25208(DrawGradientTransformation.m16845(mediaCardViewHolder.f13892.getContext())).m25187((Drawable) new ColorDrawable(-16777216)).m25196().m25209(mediaCardViewHolder.f13892);
    }
}
