package com.typhoon.tv.ui.adapter;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.ui.DrawGradientTransformation;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import com.typhoon.tv.ui.viewholder.MediaPosterCardViewHolder;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.TypefaceUtils;
import java.util.List;

public class MediaListAdapter extends BaseTvCardAdapter<MediaInfo> {
    public MediaListAdapter(List<MediaInfo> list) {
        super(list);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaCardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return TVApplication.m6285().getBoolean("pref_modern_ui", true) ? new MediaPosterCardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_media_poster, viewGroup, false)) : new MediaCardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_media, viewGroup, false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(MediaCardViewHolder mediaCardViewHolder, int i) {
        MediaInfo mediaInfo = (MediaInfo) this.f13773.get(i);
        boolean z = mediaCardViewHolder instanceof MediaPosterCardViewHolder;
        String posterUrl = z ? mediaInfo.getPosterUrl() : mediaInfo.getBannerUrl();
        boolean z2 = TVApplication.m6285().getBoolean("pref_hide_title_and_year_under_posters", false) && posterUrl != null && !posterUrl.isEmpty();
        if (mediaCardViewHolder.f13892 != null) {
            mediaCardViewHolder.f13892.setImageDrawable((Drawable) null);
        }
        if (z2) {
            mediaCardViewHolder.f13889.setText("");
            mediaCardViewHolder.f13889.setVisibility(8);
        } else {
            mediaCardViewHolder.f13889.setText(mediaInfo.getName());
            mediaCardViewHolder.f13889.setVisibility(0);
        }
        if (TVApplication.m6285().getInt("pref_poster_size", 1) == 2 || DeviceUtils.m6389(new boolean[0])) {
            mediaCardViewHolder.f13889.setMaxLines(2);
        } else {
            mediaCardViewHolder.f13889.setMaxLines(1);
        }
        if (z) {
            MediaPosterCardViewHolder mediaPosterCardViewHolder = (MediaPosterCardViewHolder) mediaCardViewHolder;
            mediaPosterCardViewHolder.f13893.setBackgroundColor(0);
            mediaPosterCardViewHolder.f13889.setTypeface(TypefaceUtils.m17839());
            if (z2) {
                mediaPosterCardViewHolder.f13893.setText("");
                mediaPosterCardViewHolder.f13893.setAlpha(0.0f);
                mediaPosterCardViewHolder.f13893.setVisibility(8);
            } else {
                mediaPosterCardViewHolder.f13893.setText(mediaInfo.getYear() > 0 ? String.valueOf(mediaInfo.getYear()) : "");
                mediaPosterCardViewHolder.f13893.setAlpha(0.7f);
                mediaPosterCardViewHolder.f13893.setVisibility(0);
            }
        } else if (mediaCardViewHolder.f13891 != null) {
            mediaCardViewHolder.f13891.setText(mediaInfo.getYear() > 0 ? String.valueOf(mediaInfo.getYear()) : "");
            mediaCardViewHolder.f13891.setBackgroundColor(0);
            mediaCardViewHolder.f13891.setAlpha(0.7f);
        }
        mediaCardViewHolder.f13889.setBackgroundColor(0);
        if (mediaCardViewHolder.f13890 != null && mediaInfo.getType() == 1 && TVApplication.m6287().m6322(Integer.valueOf(mediaInfo.getTmdbId()), mediaInfo.getImdbId())) {
            mediaCardViewHolder.f13890.setVisibility(0);
            mediaCardViewHolder.f13890.setBackgroundResource(R.color.blue);
        } else if (mediaCardViewHolder.f13890 != null && TVApplication.m6287().m6319(mediaInfo)) {
            mediaCardViewHolder.f13890.setVisibility(0);
            mediaCardViewHolder.f13890.setBackgroundResource(R.color.bookmark_line_yellow);
        } else if (mediaCardViewHolder.f13890 != null) {
            mediaCardViewHolder.f13890.setVisibility(8);
            mediaCardViewHolder.f13890.setBackgroundResource(17170445);
        }
        mediaCardViewHolder.m17620(this.f13771);
        mediaCardViewHolder.m17621(this.f13772);
        if (mediaCardViewHolder.f13892 == null) {
            return;
        }
        if (posterUrl == null || posterUrl.isEmpty()) {
            mediaCardViewHolder.f13892.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            Glide.m3934(TVApplication.m6288()).m3973(Integer.valueOf(R.drawable.ic_live_tv_white_36dp)).m25186(96, 96).m25185().m25209(mediaCardViewHolder.f13892);
            return;
        }
        mediaCardViewHolder.f13892.setScaleType(ImageView.ScaleType.CENTER_CROP);
        DrawableRequestBuilder<String> r0 = Glide.m3934(TVApplication.m6288()).m3974(posterUrl).m25191(DiskCacheStrategy.SOURCE).m25187((Drawable) new ColorDrawable(-16777216)).m25196();
        if (z2) {
            r0.m25198();
        } else {
            r0.m25208(DrawGradientTransformation.m16845(mediaCardViewHolder.f13892.getContext()));
        }
        r0.m25209(mediaCardViewHolder.f13892);
    }
}
