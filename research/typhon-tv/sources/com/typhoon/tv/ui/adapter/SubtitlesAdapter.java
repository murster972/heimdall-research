package com.typhoon.tv.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.typhoon.tv.R;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.ui.viewholder.SubtitlesCardViewHolder;
import java.util.ArrayList;
import java.util.List;

public class SubtitlesAdapter extends RecyclerView.Adapter<SubtitlesCardViewHolder> {

    /* renamed from: 靐  reason: contains not printable characters */
    private SubtitlesCardViewHolder.OnCardClickListener f13779;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<SubtitlesInfo> f13780;

    public SubtitlesAdapter(List<SubtitlesInfo> list) {
        this.f13780 = list;
    }

    public int getItemCount() {
        return this.f13780.size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17515() {
        this.f13780.clear();
        notifyDataSetChanged();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SubtitlesInfo m17516(int i) {
        return this.f13780.get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SubtitlesCardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new SubtitlesCardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_subtitle, viewGroup, false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<SubtitlesInfo> m17518() {
        ArrayList<SubtitlesInfo> arrayList = new ArrayList<>();
        for (int i = 0; i < getItemCount(); i++) {
            arrayList.add(m17516(i));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17519(SubtitlesCardViewHolder.OnCardClickListener onCardClickListener) {
        this.f13779 = onCardClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(SubtitlesCardViewHolder subtitlesCardViewHolder, int i) {
        SubtitlesInfo subtitlesInfo = this.f13780.get(i);
        subtitlesCardViewHolder.f13896.setText(subtitlesInfo.getName());
        subtitlesCardViewHolder.f13894.setText(subtitlesInfo.getLanguage());
        subtitlesCardViewHolder.m17624(this.f13779);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17521(List<SubtitlesInfo> list) {
        int size = this.f13780.size();
        this.f13780.addAll(size, list);
        notifyItemRangeInserted(size, list.size());
    }
}
