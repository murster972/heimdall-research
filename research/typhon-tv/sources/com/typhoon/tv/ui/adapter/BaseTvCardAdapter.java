package com.typhoon.tv.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.typhoon.tv.ui.viewholder.MediaCardViewHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class BaseTvCardAdapter<T> extends RecyclerView.Adapter<MediaCardViewHolder> {

    /* renamed from: 靐  reason: contains not printable characters */
    protected MediaCardViewHolder.OnCardClickListener f13771;

    /* renamed from: 齉  reason: contains not printable characters */
    protected MediaCardViewHolder.OnCardLongClickListener f13772;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final List<T> f13773;

    BaseTvCardAdapter(List<T> list) {
        this.f13773 = list;
    }

    public int getItemCount() {
        return this.f13773.size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17492() {
        this.f13773.clear();
        notifyDataSetChanged();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m17493() {
        ArrayList r0 = m17496();
        Collections.reverse(r0);
        this.f13773.clear();
        m17500(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract MediaCardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public T m17495(int i) {
        return (i != -1 || this.f13773.size() <= 0) ? this.f13773.get(i) : this.f13773.get(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<T> m17496() {
        ArrayList<T> arrayList = new ArrayList<>();
        for (int i = 0; i < getItemCount(); i++) {
            arrayList.add(m17495(i));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17497(MediaCardViewHolder.OnCardClickListener onCardClickListener) {
        this.f13771 = onCardClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17498(MediaCardViewHolder.OnCardLongClickListener onCardLongClickListener) {
        this.f13772 = onCardLongClickListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void onBindViewHolder(MediaCardViewHolder mediaCardViewHolder, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17500(List<T> list) {
        int size = this.f13773.size();
        this.f13773.addAll(size, list);
        notifyItemRangeInserted(size, list.size());
    }
}
