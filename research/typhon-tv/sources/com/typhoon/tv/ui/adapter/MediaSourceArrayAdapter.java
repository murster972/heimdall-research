package com.typhoon.tv.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.typhoon.tv.model.media.MediaSource;
import java.util.ArrayList;
import java.util.List;

public class MediaSourceArrayAdapter extends ArrayAdapter<MediaSource> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f13776;

    /* renamed from: 齉  reason: contains not printable characters */
    private ArrayList<MediaSource> f13777;

    /* renamed from: 龘  reason: contains not printable characters */
    private final LayoutInflater f13778;

    public MediaSourceArrayAdapter(Context context, int i, List<MediaSource> list) {
        super(context, i, 0, list);
        this.f13776 = i;
        this.f13778 = LayoutInflater.from(context);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        boolean z = false;
        View inflate = view == null ? this.f13778.inflate(this.f13776, viewGroup, false) : view;
        try {
            TextView textView = (TextView) inflate;
            MediaSource mediaSource = (MediaSource) getItem(i);
            if (mediaSource != null) {
                if (this.f13777 != null && this.f13777.contains(mediaSource)) {
                    z = true;
                }
                textView.setText(mediaSource.toString());
                if (mediaSource.isDebrid()) {
                    if (z) {
                        textView.setTextColor(Color.parseColor("#ffffb200"));
                    } else {
                        textView.setTextColor(Color.parseColor("#ffffb200"));
                    }
                } else if (z) {
                    textView.setTextColor(Color.parseColor("#9E9E9E"));
                } else {
                    textView.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }
            return inflate;
        } catch (ClassCastException e) {
            throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17511() {
        if (this.f13777 != null) {
            this.f13777.clear();
        } else {
            this.f13777 = new ArrayList<>();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<MediaSource> m17512() {
        return this.f13777;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17513(MediaSource mediaSource) {
        if (mediaSource != null) {
            MediaSource originalMediaSource = mediaSource.getOriginalMediaSource();
            if (this.f13777 != null) {
                if (mediaSource.isResolved() && originalMediaSource != null && !this.f13777.contains(originalMediaSource)) {
                    this.f13777.add(originalMediaSource);
                }
                if (!this.f13777.contains(mediaSource)) {
                    this.f13777.add(mediaSource);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17514(ArrayList<MediaSource> arrayList) {
        this.f13777 = arrayList;
    }
}
