package com.typhoon.tv.ui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f13058;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f13059;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f13060;

    public GridSpacingItemDecoration(int i, int i2, boolean z) {
        this.f13060 = i;
        this.f13058 = i2;
        this.f13059 = z;
    }

    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
        int i = childAdapterPosition % this.f13060;
        if (this.f13059) {
            rect.left = this.f13058 - ((this.f13058 * i) / this.f13060);
            rect.right = ((i + 1) * this.f13058) / this.f13060;
            if (childAdapterPosition < this.f13060) {
                rect.top = this.f13058;
            }
            rect.bottom = this.f13058;
            return;
        }
        rect.left = (this.f13058 * i) / this.f13060;
        rect.right = this.f13058 - (((i + 1) * this.f13058) / this.f13060);
        if (childAdapterPosition >= this.f13060) {
            rect.top = this.f13058;
        }
    }
}
