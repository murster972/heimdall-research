package com.typhoon.tv;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;
import net.pubnative.library.request.PubnativeRequest;

public class I18N {

    /* renamed from: 靐  reason: contains not printable characters */
    private static WeakReference<HashMap<String, String>> f12485;

    /* renamed from: 龘  reason: contains not printable characters */
    private static WeakReference<HashMap<String, String>> f12486;

    /* renamed from: 靐  reason: contains not printable characters */
    public static HashMap<String, String> m15704() {
        HashMap hashMap = null;
        try {
            hashMap = (HashMap) f12485.get();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (hashMap == null) {
            hashMap = new LinkedHashMap();
            hashMap.put("Afrikaans", PubnativeRequest.Parameters.ASSET_FIELDS);
            hashMap.put("Albanian", "sq");
            hashMap.put("Arabic", "ar");
            hashMap.put("Armenian", "hy");
            hashMap.put("Basque", "eu");
            hashMap.put("Bengali", "bn");
            hashMap.put("Bosnian", "bs");
            hashMap.put("Breton", TtmlNode.TAG_BR);
            hashMap.put("Bulgarian", "bg");
            hashMap.put("Burmese", "my");
            hashMap.put("Catalan", "ca");
            hashMap.put("Chinese (Simplified)", "zh-CN");
            hashMap.put("Chinese (Traditional)", "zh-TW");
            hashMap.put("Croatian", "hr");
            hashMap.put("Czech", "cs");
            hashMap.put("Danish", "da");
            hashMap.put("Dutch", "nl");
            hashMap.put("English", "en-US");
            hashMap.put("Esperanto", "eo");
            hashMap.put("Estonian", "et");
            hashMap.put("Finnish", "fi");
            hashMap.put("French", "fr");
            hashMap.put("Galician", "gl");
            hashMap.put("Georgian", "ka");
            hashMap.put("German", "de");
            hashMap.put("Greek", "el");
            hashMap.put("Hebrew", "he");
            hashMap.put("Hindi", "hi");
            hashMap.put("Hungarian", "hu");
            hashMap.put("Icelandic", "is");
            hashMap.put("Indonesian", "id");
            hashMap.put("Italian", "it");
            hashMap.put("Japanese", "jp");
            hashMap.put("Kazakh", "kk");
            hashMap.put("Khmer", "km");
            hashMap.put("Korean", "ko");
            hashMap.put("Latvian", "lv");
            hashMap.put("Lithuanian", "lt");
            hashMap.put("Luxembourgish", "lb");
            hashMap.put("Macedonian", "mk");
            hashMap.put("Malay", "ms");
            hashMap.put("Malayalam", "ml");
            hashMap.put("Mongolian", "mn");
            hashMap.put("Norwegian Bokmål", "nb");
            hashMap.put("Norwegian Nynorsk", "nn");
            hashMap.put("Occitan", "oc");
            hashMap.put("Persian", "fa");
            hashMap.put("Polish", "pl");
            hashMap.put("Portuguese", "pt-PT");
            hashMap.put("Portuguese (Brazil)", "pt-BR");
            hashMap.put("Romanian", "ro");
            hashMap.put("Russian", "ru");
            hashMap.put("Serbian", "sr");
            hashMap.put("Sinhalese", "si");
            hashMap.put("Slovak", "sk");
            hashMap.put("Slovenian", "sl");
            hashMap.put("Spanish", "es");
            hashMap.put("Swahili", "sw");
            hashMap.put("Swedish", "sv");
            hashMap.put("Tagalog", "tl");
            hashMap.put("Tamil", "ta");
            hashMap.put("Telugu", "te");
            hashMap.put("Thai", "th");
            hashMap.put("Turkish", "tr");
            hashMap.put("Ukrainian", "uk");
            hashMap.put("Urdu", "ur");
            hashMap.put("Vietnamese", "vi");
        }
        f12485 = new WeakReference<>(hashMap);
        return hashMap;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Set<String> m15705() {
        return m15708().keySet();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15706(int i) {
        return TVApplication.m6288().getResources().getString(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15707(int i, Object... objArr) {
        return TVApplication.m6288().getResources().getString(i, objArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static HashMap<String, String> m15708() {
        HashMap hashMap = null;
        try {
            hashMap = (HashMap) f12486.get();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (hashMap == null) {
            hashMap = new LinkedHashMap();
            hashMap.put("Afrikaans", "afr");
            hashMap.put("Albanian", "alb");
            hashMap.put("Arabic", "ara");
            hashMap.put("Armenian", "arm");
            hashMap.put("Basque", "baq");
            hashMap.put("Bengali", "ben");
            hashMap.put("Bosnian", "bos");
            hashMap.put("Breton", "bre");
            hashMap.put("Bulgarian", "bul");
            hashMap.put("Burmese", "bur");
            hashMap.put("Catalan", "cat");
            hashMap.put("Chinese", "chi");
            hashMap.put("Croatian", "hrv");
            hashMap.put("Czech", "cze");
            hashMap.put("Danish", "dan");
            hashMap.put("Dutch", "dut");
            hashMap.put("English", "eng");
            hashMap.put("Esperanto", "epo");
            hashMap.put("Estonian", "est");
            hashMap.put("Finnish", "fin");
            hashMap.put("French", "fre");
            hashMap.put("Galician", "glg");
            hashMap.put("Georgian", "geo");
            hashMap.put("German", "ger");
            hashMap.put("Greek", "ell");
            hashMap.put("Hebrew", "heb");
            hashMap.put("Hindi", "hin");
            hashMap.put("Hungarian", "hun");
            hashMap.put("Icelandic", "ice");
            hashMap.put("Indonesian", "ind");
            hashMap.put("Italian", "ita");
            hashMap.put("Japanese", "jpn");
            hashMap.put("Kazakh", "kaz");
            hashMap.put("Khmer", "khm");
            hashMap.put("Korean", "kor");
            hashMap.put("Latvian", "lav");
            hashMap.put("Lithuanian", "lit");
            hashMap.put("Luxembourgish", "ltz");
            hashMap.put("Macedonian", "mac");
            hashMap.put("Malay", "may");
            hashMap.put("Malayalam", "mal");
            hashMap.put("Manipuri", "mni");
            hashMap.put("Mongolian", "mon");
            hashMap.put("Montenegrin", "mne");
            hashMap.put("Norwegian", "nor");
            hashMap.put("Occitan", "oci");
            hashMap.put("Persian", "per");
            hashMap.put("Polish", "pol");
            hashMap.put("Portuguese", "por");
            hashMap.put("Portuguese (Brazil)", "pob");
            hashMap.put("Romanian", "rum");
            hashMap.put("Russian", "rus");
            hashMap.put("Serbian", "scc");
            hashMap.put("Sinhalese", "sin");
            hashMap.put("Slovak", "slo");
            hashMap.put("Slovenian", "slv");
            hashMap.put("Spanish", "spa");
            hashMap.put("Swahili", "swa");
            hashMap.put("Swedish", "swe");
            hashMap.put("Syriac", "syr");
            hashMap.put("Tagalog", "tgl");
            hashMap.put("Tamil", "tam");
            hashMap.put("Telugu", "tel");
            hashMap.put("Thai", "tha");
            hashMap.put("Turkish", "tur");
            hashMap.put("Ukrainian", "ukr");
            hashMap.put("Urdu", "urd");
            hashMap.put("Vietnamese", "vie");
        }
        f12486 = new WeakReference<>(hashMap);
        return hashMap;
    }
}
