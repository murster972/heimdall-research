package com.typhoon.tv.provider.universal;

import android.util.Base64;
import com.typhoon.tv.Logger;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.ReCaptchaRequiredEvent;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.VidCDNHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;
import org.apache.oltu.oauth2.common.OAuth;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import rx.Subscriber;

public class FliXanityThemeBase extends BaseProvider {
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16352() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16353(Subscriber<? super MediaSource> subscriber, String str, String str2, MediaInfo mediaInfo, String str3, String str4, String str5, String str6, boolean... zArr) {
        String str7;
        Elements r23;
        boolean z = mediaInfo.getType() == 1;
        String r42 = HttpHelper.m6343().m6359(str2, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0]);
        if (r42.toLowerCase().contains("web server is returning an unknown error<")) {
            HttpHelper.m6343().m6357(str, HttpHelper.m6343().m6349(str2).replaceAll("cf_use_ob\\s*=\\s*\\d+;?\\s*", "cf_use_ob=0; "));
            r42 = HttpHelper.m6343().m6359(str2, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0]);
        }
        if (r42.contains("Please complete the security check to access")) {
            RxBus.m15709().m15711(new ReCaptchaRequiredEvent(m16352(), str));
            return;
        }
        String r37 = Regex.m17800(r42, "<title>\\s*([^<]+)", 1);
        if (r42.isEmpty() || r37.isEmpty() || r37.toLowerCase().contains("%title%") || r42.contains("The page you're looking for no longer exists")) {
            if (z) {
                String str8 = str2 + "-" + mediaInfo.getYear();
                r42 = HttpHelper.m6343().m6359(str8, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0]);
                String r372 = Regex.m17800(r42, "<title>\\s*([^<]+)", 1);
                if (r42.isEmpty() || r372.isEmpty() || r372.toLowerCase().contains("%title%") || r42.contains("The page you're looking for no longer exists")) {
                    String replace = str2.replace("/film/", "/movies/");
                    r42 = HttpHelper.m6343().m6359(replace, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0]);
                    String r373 = Regex.m17800(r42, "<title>\\s*([^<]+)", 1);
                    if (r42.isEmpty() || r373.isEmpty() || r373.toLowerCase().contains("%title%") || r42.contains("The page you're looking for no longer exists")) {
                        String str9 = str2.replace("/film/", "/movies/") + "-" + mediaInfo.getYear();
                        r42 = HttpHelper.m6343().m6359(str9, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0]);
                        String r374 = Regex.m17800(r42, "<title>\\s*([^<]+)", 1);
                        if (!r42.isEmpty() && !r374.isEmpty() && !r374.toLowerCase().contains("%title%") && !r42.contains("The page you're looking for no longer exists")) {
                            str2 = str9;
                        } else {
                            return;
                        }
                    } else {
                        str2 = replace;
                    }
                } else {
                    str2 = str8;
                }
            } else {
                str2 = str2.replace("/season/", "-" + mediaInfo.getYear() + "/season/");
                r42 = HttpHelper.m6343().m6359(str2, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0]);
                String r375 = Regex.m17800(r42, "<title>\\s*([^<]+)", 1);
                if (r42.isEmpty() || r375.isEmpty() || r375.toLowerCase().contains("%title%") || r42.contains("The page you're looking for no longer exists")) {
                    return;
                }
            }
        }
        String imdbId = mediaInfo.getImdbId();
        if (!z || imdbId == null || imdbId.isEmpty() || r42.toLowerCase().contains(imdbId.toLowerCase())) {
            if (z) {
                String r33 = Regex.m17802(r42, "<span[^>]*class=['\"]dat['\"][^>]*>\\s*(\\d{4})\\s*<", 1, true);
                if (!r33.isEmpty() && Utils.m6426(r33) && Integer.parseInt(r33) != mediaInfo.getYear()) {
                    if (!(zArr != null && zArr.length > 0 && zArr[0])) {
                        m16353(subscriber, str, (str2.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? str2.substring(0, str2.length() - 1) : str2) + "-" + mediaInfo.getYear(), mediaInfo, str3, str4, str5, str6, true);
                        return;
                    }
                    return;
                }
            }
            String r46 = Regex.m17800(HttpHelper.m6343().m6349(str), "__utmx=(.+)", 1);
            if (r46.isEmpty()) {
                r46 = "false";
            } else if (r46.contains(";")) {
                r46 = r46.split(";")[0];
            }
            String str10 = "Bearer " + r46;
            String r44 = DateTimeHelper.m15934();
            try {
                r44 = Base64.encodeToString(r44.getBytes("UTF-8"), 10).trim();
            } catch (UnsupportedEncodingException e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            String r24 = Utils.m6414(r44, new boolean[0]);
            String r45 = Regex.m17800(r42, "var\\s+tok\\s*=\\s*['\"]([^'\"]+)", 1);
            String r27 = Regex.m17800(r42, "elid\\s*=\\s*['\"]([^'\"]+)", 1);
            if (r27.isEmpty() && (r23 = Jsoup.m21674(r42).m21815("main.watch[data-id]")) != null && !r23.isEmpty()) {
                Iterator it2 = r23.iterator();
                while (it2.hasNext()) {
                    r27 = ((Element) it2.next()).m21947("data-id");
                    if (!r27.isEmpty()) {
                        break;
                    }
                }
            }
            if (r27.isEmpty() && !z) {
                Iterator it3 = Jsoup.m21674(HttpHelper.m6343().m6359(str2.replaceAll("/season/(\\d+)/episode/.*", "/season/$1"), "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2", str, (Map<String, String>[]) new Map[0])).m21815("#episodes").iterator();
                while (it3.hasNext()) {
                    Element element = (Element) it3.next();
                    Elements r21 = element.m21815("a[data-e][data-id]");
                    r21.addAll(element.m21815("span[data-e][data-id]"));
                    Iterator it4 = r21.iterator();
                    while (it4.hasNext()) {
                        Element element2 = (Element) it4.next();
                        if (element2.m21947("data-e").trim().equals(str5)) {
                            r27 = element2.m21947("data-id").trim();
                            if (!r27.isEmpty()) {
                                break;
                            }
                        }
                    }
                }
            }
            if (!r27.isEmpty()) {
                HashMap<String, String> r26 = TyphoonApp.m6325();
                r26.put(OAuth.HeaderType.AUTHORIZATION, str10);
                r26.put("Referer", str2);
                r26.put(OAuth.HeaderType.CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8");
                r26.put(AbstractSpiCall.HEADER_ACCEPT, "application/json, text/javascript, */*; q=0.01");
                r26.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2");
                String str11 = z ? "getMovieEmb" : "getEpisodeEmb";
                String r38 = HttpHelper.m6343().m6350(str + "/ajax/" + str6, "action=" + str11 + "&idEl=" + r27 + "&token=" + r45 + "&elid=" + r24 + "&nopop=", r26);
                if (r38.toLowerCase().contains("web server is returning an unknown error<")) {
                    HttpHelper.m6343().m6357(str, HttpHelper.m6343().m6349(str2).replaceAll("cf_use_ob\\s*=\\s*\\d+;?\\s*", "cf_use_ob=0; "));
                    r38 = HttpHelper.m6343().m6350(str + "/ajax/" + str6, "action=" + str11 + "&idEl=" + r27 + "&token=" + r45 + "&elid=" + r24 + "&nopop=", r26);
                }
                if (r38.trim().isEmpty() || r38.toLowerCase().contains("invalid")) {
                    r38 = HttpHelper.m6343().m6350(str + "/ajax/" + str6, "action=" + str11 + "&idEl=" + r27 + "&token=" + r45 + "&elid=" + r24, r26);
                }
                if (!r38.trim().isEmpty() && !r38.toLowerCase().contains("invalid")) {
                    ArrayList arrayList = new ArrayList();
                    String replace2 = r38.replace("\\\"", "\"").replace("\\'", "'").replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                    arrayList.addAll(Regex.m17804(replace2, "'(http.+?)'", 1, 34).get(0));
                    arrayList.addAll(Regex.m17804(replace2, "\"(http.+?)\"", 1, 34).get(0));
                    arrayList.addAll(Regex.m17804(replace2, "src\\s*=\\s*['\"]([^'\"]+)['\"]", 1, 34).get(0));
                    Iterator it5 = Utils.m6415(arrayList).iterator();
                    while (it5.hasNext()) {
                        String str12 = (String) it5.next();
                        if (str12.startsWith("//")) {
                            str12 = "https:" + str12;
                        } else if (str12.startsWith(":")) {
                            str12 = com.mopub.common.TyphoonApp.HTTP + str12;
                        } else if (str12.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                            str12 = str + str12;
                        }
                        if (str12.startsWith(com.mopub.common.TyphoonApp.HTTP)) {
                            String replace3 = str12.replace(StringUtils.SPACE, "%20");
                            m16289(subscriber, replace3, "HD", m16352());
                            boolean r29 = GoogleVideoHelper.m15955(replace3);
                            if (r29) {
                                str7 = GoogleVideoHelper.m15949(replace3);
                            } else {
                                String r32 = Regex.m17800(replace3, "[/\\-\\.](\\d{3,4})p?\\.\\w{3,4}(?:$|\\?)", 1);
                                str7 = (r32.isEmpty() || (!VidCDNHelper.m15996(replace3) && !replace3.contains(".fbcdn.") && !replace3.contains(".wewon.") && !replace3.contains(".svid.") && !replace3.contains("cdn.vidnode.")) || (!r32.equals("240") && !r32.equals("360") && !r32.equals("480") && !r32.equals("720") && !r32.equals("1080"))) ? "HD" : r32 + TtmlNode.TAG_P;
                            }
                            MediaSource mediaSource = new MediaSource(m16352(), r29 ? "GoogleVideo" : "CDN-Quick", false);
                            mediaSource.setStreamLink(replace3);
                            mediaSource.setQuality(str7);
                            subscriber.onNext(mediaSource);
                        }
                    }
                    return;
                }
                return;
            }
            return;
        }
        if (!(zArr != null && zArr.length > 0 && zArr[0])) {
            m16353(subscriber, str, (str2.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? str2.substring(0, str2.length() - 1) : str2) + "-" + mediaInfo.getYear(), mediaInfo, str3, str4, str5, str6, true);
        }
    }
}
