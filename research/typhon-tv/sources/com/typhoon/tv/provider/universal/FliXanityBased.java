package com.typhoon.tv.provider.universal;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import rx.Observable;
import rx.Subscriber;

public class FliXanityBased extends FliXanityThemeBase {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f12845 = (TyphoonApp.f5907 + "/Typhoon-Server/flixanityProviderUrls.json");

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public HashMap<String, String> m16345() {
        JsonObject asJsonObject = new JsonParser().parse(HttpHelper.m6343().m6351(f12845, (Map<String, String>[]) new Map[0])).getAsJsonObject();
        HashMap<String, String> hashMap = new HashMap<>();
        Iterator<JsonElement> it2 = asJsonObject.get("bases").getAsJsonArray().iterator();
        while (it2.hasNext()) {
            JsonObject asJsonObject2 = it2.next().getAsJsonObject();
            String asString = asJsonObject2.get("url").getAsString();
            String asString2 = asJsonObject2.get("embedsPath").getAsString();
            String asString3 = asJsonObject2.get("bdcCookie").getAsString();
            if (asString != null && !asString.isEmpty() && asString2 != null && !asString2.isEmpty()) {
                if (asString3 != null && !asString3.isEmpty()) {
                    HttpHelper.m6343().m6357(asString, asString3);
                }
                hashMap.put(asString, asString2);
            }
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16347() {
        return "FliXanityBased";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16348(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String r6 = TitleHelper.m15972(mediaInfo.getName().replace("'", ""));
                String name = mediaInfo.getName();
                char c = 65535;
                switch (name.hashCode()) {
                    case -1438572772:
                        if (name.equals("Star Wars: The Last Jedi")) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1296695595:
                        if (name.equals("Mission: Impossible - Ghost Protocol")) {
                            c = 7;
                            break;
                        }
                        break;
                    case -616025719:
                        if (name.equals("Fast & Furious")) {
                            c = 4;
                            break;
                        }
                        break;
                    case -519161968:
                        if (name.equals("Justice League")) {
                            c = 6;
                            break;
                        }
                        break;
                    case -95314585:
                        if (name.equals("Star Wars: The Force Awakens")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 268366428:
                        if (name.equals("Self/less")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 607165378:
                        if (name.equals("Now You See Me 2")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 704771935:
                        if (name.equals("Fast & Furious 6")) {
                            c = 5;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        r6 = "star-wars-episode-vii-the-force-awakens";
                        break;
                    case 1:
                        r6 = "star-wars-episode-viii";
                        break;
                    case 2:
                        r6 = "now-you-see-me-the-second-act";
                        break;
                    case 3:
                        r6 = "self-less";
                        break;
                    case 4:
                        r6 = "fast-furious";
                        break;
                    case 5:
                        r6 = "fast-furious-6";
                        break;
                    case 6:
                        r6 = "the-justice-league-part-one";
                        break;
                    case 7:
                        subscriber.onCompleted();
                        return;
                }
                HashMap hashMap = new HashMap();
                hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2");
                for (Map.Entry entry : FliXanityBased.this.m16345().entrySet()) {
                    String str = (String) entry.getKey();
                    String str2 = (String) entry.getValue();
                    try {
                        HttpHelper.m6343().m6361(str + "/?ckattempt=1", str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, (Map<String, String>[]) new Map[]{hashMap});
                        String r15 = HttpHelper.m6343().m6363(str, false, (Map<String, String>[]) new Map[]{hashMap});
                        Subscriber<? super MediaSource> subscriber2 = subscriber;
                        FliXanityBased.this.m16353(subscriber2, str, str + "/film/" + r6, mediaInfo, r6, "-1", "-1", str2, new boolean[0]);
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16349(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String r6 = TitleHelper.m15972(mediaInfo.getName().replace("DC's ", "").replace("'", ""));
                String name = mediaInfo.getName();
                char c = 65535;
                switch (name.hashCode()) {
                    case -1989436052:
                        if (name.equals("Marvel's The Defenders")) {
                            c = 2;
                            break;
                        }
                        break;
                    case -1753753522:
                        if (name.equals("Marvel's Jessica Jones")) {
                            c = 5;
                            break;
                        }
                        break;
                    case -517469293:
                        if (name.equals("Marvel's Daredevil")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 91465286:
                        if (name.equals("Marvel's The Punisher")) {
                            c = 4;
                            break;
                        }
                        break;
                    case 424581095:
                        if (name.equals("Marvel's Iron Fist")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 1544707312:
                        if (name.equals("Will & Grace")) {
                            c = 6;
                            break;
                        }
                        break;
                    case 1728305255:
                        if (name.equals("Marvel's Agents of S.H.I.E.L.D.")) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        r6 = "agents-of-shield";
                        break;
                    case 1:
                        r6 = "iron-fist";
                        break;
                    case 2:
                        r6 = "the-defenders";
                        break;
                    case 3:
                        r6 = "marvels-daredevil";
                        break;
                    case 4:
                        r6 = "the-punisher";
                        break;
                    case 5:
                        r6 = "jessica-jones";
                        break;
                    case 6:
                        r6 = "will-and-grace";
                        break;
                }
                HashMap hashMap = new HashMap();
                hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/6.1.6 Safari/537.78.2");
                for (Map.Entry entry : FliXanityBased.this.m16345().entrySet()) {
                    String str = (String) entry.getKey();
                    String str2 = (String) entry.getValue();
                    try {
                        HttpHelper.m6343().m6361(str + "/?ckattempt=1", str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, (Map<String, String>[]) new Map[]{hashMap});
                        String r24 = HttpHelper.m6343().m6363(str, false, (Map<String, String>[]) new Map[]{hashMap});
                        Subscriber<? super MediaSource> subscriber2 = subscriber;
                        FliXanityBased.this.m16353(subscriber2, str, str + "/tv-show/" + r6 + "/season/" + str + "/episode/" + str2, mediaInfo, r6, str, str2, str2, new boolean[0]);
                        Subscriber<? super MediaSource> subscriber3 = subscriber;
                        String str3 = str;
                        FliXanityBased.this.m16353(subscriber3, str3, str + "/series/" + r6 + "/season/" + str + "/episode/" + str2, mediaInfo, r6, str, str2, str2, new boolean[0]);
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
