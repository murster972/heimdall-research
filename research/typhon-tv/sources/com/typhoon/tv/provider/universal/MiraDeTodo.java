package com.typhoon.tv.provider.universal;

import com.google.android.exoplayer2.util.MimeTypes;
import com.typhoon.tv.Logger;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.api.Tmdb;
import com.typhoon.tv.event.ReCaptchaRequiredEvent;
import com.typhoon.tv.helper.GkPluginsHelper;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import rx.Observable;
import rx.Subscriber;

public class MiraDeTodo extends BaseProvider {
    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16373(Subscriber<? super MediaSource> subscriber, MediaInfo mediaInfo, String str, String str2) {
        String name;
        String r19;
        boolean z = mediaInfo.getType() == 1;
        if (z) {
            String imdbId = mediaInfo.getImdbId();
            if ((imdbId == null || imdbId.isEmpty()) && mediaInfo.getTmdbId() > -1) {
                imdbId = Tmdb.龘().ʻ(mediaInfo.getTmdbId());
            }
            if (imdbId == null || imdbId.isEmpty()) {
                name = mediaInfo.getName();
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("Accept-Language", "es-ES");
                Document r6 = Jsoup.m21674(HttpHelper.m6343().m6351("https://m.imdb.com/title/" + imdbId + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, (Map<String, String>[]) new Map[]{hashMap}));
                String r35 = r6.m21815(PubnativeAsset.TITLE).size() > 0 ? r6.m21837(PubnativeAsset.TITLE).m21822() : mediaInfo.getName();
                if (!r35.isEmpty()) {
                    name = r35.replaceAll("(?:\\(|\\(?(?:TV\\s+Series)?\\s)\\d{4}.+", "").replaceAll("\\((?:.+?|)\\d{4}.+", "").replaceAll("(?:\\(|\\s)\\d{4}.+", "").trim();
                } else {
                    return;
                }
            }
        } else {
            name = mediaInfo.getName();
        }
        if (name.equalsIgnoreCase("Wonder Woman") && z) {
            name = "Mujer Maravilla";
        }
        String str3 = "";
        if (z) {
            String r33 = HttpHelper.m6343().m6359("https://miradetodo.net/?s=" + Utils.m6414(name, new boolean[0]), "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", "https://miradetodo.net", (Map<String, String>[]) new Map[0]);
            if (r33.contains("Please complete the security check to access")) {
                RxBus.m15709().m15711(new ReCaptchaRequiredEvent(m16375(), "https://miradetodo.net"));
                subscriber.onCompleted();
                return;
            }
            Iterator it2 = Jsoup.m21674(r33).m21815("div.item").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Element element = (Element) it2.next();
                r19 = element.m21815("a[href]").size() > 0 ? element.m21837("a[href]").m21947("href") : "";
                String r36 = element.m21815("span.tt").size() > 0 ? element.m21837("span.tt").m21822() : "";
                String trim = element.m21815("span.year").size() > 0 ? element.m21837("span.year").m21868().trim() : Regex.m17800(r36, ".*?\\s+\\((\\d{4})\\)", 2);
                if (!r19.isEmpty() && !r36.isEmpty() && !r36.matches("\\d+\\s*x\\s*\\d+") && TitleHelper.m15971(name).equals(TitleHelper.m15971(r36))) {
                    if (trim.trim().isEmpty() || !Utils.m6426(trim.trim()) || mediaInfo.getYear() <= 0 || Integer.parseInt(trim.trim()) == mediaInfo.getYear()) {
                        str3 = r19;
                    }
                }
            }
            str3 = r19;
        } else {
            String r38 = TitleHelper.m15970(TitleHelper.m15972(name));
            String r37 = HttpHelper.m6343().m6359("https://miradetodo.net/series/" + r38, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", "https://miradetodo.net", (Map<String, String>[]) new Map[0]);
            if (r37.contains("Please complete the security check to access")) {
                RxBus.m15709().m15711(new ReCaptchaRequiredEvent(m16375(), "https://miradetodo.net"));
                subscriber.onCompleted();
                return;
            }
            String r26 = Regex.m17802(r37, "tag\"[^>]*>(\\d{4})", 1, true);
            if (r26.isEmpty()) {
                r26 = Regex.m17802(r37, "<h1\\s*[^>]*?itemprop=['\"]name['\"][^>]*?>.*?\\(\\s*(\\d{4})\\s*-?.*?\\)\\s*<", 1, true);
            }
            if (!r26.isEmpty() && Integer.parseInt(r26.trim()) == mediaInfo.getYear()) {
                str3 = "https://miradetodo.net/episodio/" + r38 + "-" + str + "x" + str2;
            } else {
                return;
            }
        }
        String r30 = Regex.m17800(str3, "(?://.+?|)(/.+)", 1);
        if (!r30.isEmpty()) {
            String str4 = "https://miradetodo.net" + r30;
            Document r62 = Jsoup.m21674(HttpHelper.m6343().m6359(str4, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", "https://miradetodo.net", (Map<String, String>[]) new Map[0]));
            Elements r14 = r62.m21815("div.movieplay");
            r14.addAll(r62.m21815("div.embed2").select(TtmlNode.TAG_DIV));
            ArrayList arrayList = new ArrayList();
            try {
                Iterator it3 = r62.m21837("ul.idTabs").m21815("a[href]").iterator();
                while (it3.hasNext()) {
                    Element element2 = (Element) it3.next();
                    if (element2.m21822().toLowerCase().contains("dob") || element2.m21822().toLowerCase().contains("ads") || element2.m21822().toLowerCase().contains(MimeTypes.BASE_TYPE_AUDIO) || element2.m21822().toLowerCase().contains("latino")) {
                        arrayList.add(element2.m21947("href").replace("#", ""));
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
            Iterator it4 = r14.iterator();
            while (it4.hasNext()) {
                Element element3 = (Element) it4.next();
                Element r12 = element3.m21819();
                if (r12.m21831() == null || r12.m21831().isEmpty() || !arrayList.contains(r12.m21831())) {
                    Element r16 = element3.m21837("iframe");
                    if (r16 != null) {
                        if (r16.m21951("data-lazy-src")) {
                            String r25 = r16.m21947("data-lazy-src");
                            if (r25.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                r25 = "https://miradetodo.net" + r25;
                            }
                            if (!r25.toLowerCase().contains("miradetodo")) {
                                m16291(subscriber, r25, "HD", new boolean[0]);
                            }
                        }
                        if (r16.m21951("src")) {
                            String r252 = r16.m21947("src");
                            if (r252.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                r252 = "https://miradetodo.net" + r252;
                            }
                            if (!r252.toLowerCase().contains("miradetodo")) {
                                m16291(subscriber, r252, "HD", new boolean[0]);
                            }
                        }
                    }
                    Iterator it5 = Regex.m17805(element3.m21822(), "(?:\"|')(http.+?miradetodo\\..+?)(?:\"|')", 1, true).get(0).iterator();
                    while (it5.hasNext()) {
                        String str5 = (String) it5.next();
                        try {
                            m16378(subscriber, str4, str5);
                        } catch (Exception e2) {
                        }
                        Document r27 = Jsoup.m21674(HttpHelper.m6343().m6359(str5, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", str4, (Map<String, String>[]) new Map[0]));
                        if (r27.m21815("iframe[src]").isEmpty() && r27.m21822().contains("nav")) {
                            Iterator it6 = r27.m21815("li").iterator();
                            while (it6.hasNext()) {
                                Iterator it7 = ((Element) it6.next()).m21815("a[href]").iterator();
                                while (it7.hasNext()) {
                                    String r192 = ((Element) it7.next()).m21947("href");
                                    if (r192.contains("miradetodo")) {
                                        try {
                                            m16378(subscriber, str4, r192);
                                        } catch (Exception e3) {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16374(Subscriber<? super MediaSource> subscriber, String str) {
        String replace = str.replace(StringUtils.LF, "").replace(StringUtils.CR, "");
        if (GoogleVideoHelper.m15953(replace)) {
            HashMap<String, String> r4 = GoogleVideoHelper.m15948(replace);
            if (r4 != null && !r4.isEmpty()) {
                for (Map.Entry next : r4.entrySet()) {
                    MediaSource mediaSource = new MediaSource(m16375(), "GoogleVideo", false);
                    mediaSource.setStreamLink((String) next.getKey());
                    mediaSource.setQuality(((String) next.getValue()).isEmpty() ? "HD" : (String) next.getValue());
                    HashMap hashMap = new HashMap();
                    hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                    hashMap.put("Cookie", GoogleVideoHelper.m15954(replace, (String) next.getKey()));
                    mediaSource.setPlayHeader(hashMap);
                    subscriber.onNext(mediaSource);
                }
                return;
            }
            return;
        }
        m16291(subscriber, replace, "HD", new boolean[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16375() {
        return "MiraDeTodo";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16376(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                MiraDeTodo.this.m16373(subscriber, mediaInfo, "-1", "-1");
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16377(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                MiraDeTodo.this.m16373(subscriber, mediaInfo, str, str2);
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16378(Subscriber<? super MediaSource> subscriber, String str, String str2) {
        if (!subscriber.isUnsubscribed()) {
            String replace = str2.replace("&amp;", "&");
            if (!replace.endsWith(".gif") && !replace.endsWith(".png")) {
                if (replace.startsWith("//")) {
                    replace = "http:" + replace;
                } else if (replace.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                    replace = "https://miradetodo.net" + replace;
                }
                ArrayList arrayList = new ArrayList();
                HashMap hashMap = new HashMap();
                hashMap.put("Connection", "close");
                hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                if (replace.contains("player.miradetodo.net/api/mp4.php?")) {
                    m16378(subscriber, str, replace.replace("player.miradetodo.net/api/mp4.php?", "miradetodo.net/stream/mp4play.php?"));
                } else if (replace.contains("player.miradetodo.net/api/openload.php?")) {
                    m16378(subscriber, str, replace.replace("player.miradetodo.net/api/openload.php?", "player.miradetodo.net/api/ol.php?"));
                }
                String r26 = HttpHelper.m6343().m6359(replace, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", str, (Map<String, String>[]) new Map[0]);
                Document r25 = Jsoup.m21674(r26);
                ArrayList<String> r28 = m16285(r26);
                try {
                    m16374(subscriber, r25.m21837("iframe[src]").m21947("src").replace(StringUtils.LF, "").replace(StringUtils.CR, ""));
                } catch (Exception e) {
                }
                Iterator it2 = r25.m21815("a[href]").iterator();
                while (it2.hasNext()) {
                    try {
                        String r30 = ((Element) it2.next()).m21947("href");
                        if (!r30.trim().toLowerCase().contains("javascript:")) {
                            if (r30.startsWith("//")) {
                                r30 = "http:" + r30;
                            } else if (r30.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                r30 = "https://miradetodo.net" + r30;
                            }
                            if (!r30.endsWith(".gif") && !r30.endsWith(".png") && !r30.endsWith(".srt")) {
                                String r29 = HttpHelper.m6343().m6361(r30, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", (Map<String, String>[]) new Map[]{hashMap});
                                r28.addAll(m16285(r29));
                                String r8 = Regex.m17802(r29, "AmazonPlayer.*?file\\s*:\\s*\"([^\"]+)", 1, true);
                                if (!r8.isEmpty()) {
                                    r28.add(r8);
                                }
                            }
                        }
                    } catch (Exception e2) {
                    }
                }
                try {
                    Map<String, String> r32 = Utils.m6418(new URL(replace));
                    if (r32.containsKey("id")) {
                        String str3 = r32.get("id");
                        if (!arrayList.contains(str3)) {
                            arrayList.add(str3);
                            HashMap<String, String> r19 = TyphoonApp.m6325();
                            r19.put("Referer", replace);
                            r19.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                            String replace2 = Utils.m6414(str3, new boolean[0]).replace("%3D", "=");
                            String r17 = HttpHelper.m6343().m6350("https://miradetodo.net/stream/plugins/gkpluginsphp.php", "link=" + replace2, r19);
                            r28.addAll(GkPluginsHelper.m15938(r17).keySet());
                            if (r17.contains("amazon") && r17.contains("clouddrive")) {
                                String str4 = "https://player.miradetodo.net/api/get.php?id=" + replace2;
                                String r6 = HttpHelper.m6343().m6363(str4, false, (Map<String, String>[]) new Map[]{r19});
                                if (!r6.equalsIgnoreCase(str4)) {
                                    r28.add(r6);
                                }
                            }
                        }
                    }
                } catch (Exception e3) {
                    Logger.m6281((Throwable) e3, true);
                }
                if (replace.trim().toLowerCase().contains("gd.php")) {
                    try {
                        m16374(subscriber, Jsoup.m21674(HttpHelper.m6343().m6359(replace.replace("gd.php", "gdplay.php"), "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", str, (Map<String, String>[]) new Map[0])).m21837("iframe[src]").m21947("src").replace(StringUtils.LF, "").replace(StringUtils.CR, ""));
                    } catch (Exception e4) {
                        Logger.m6281((Throwable) e4, new boolean[0]);
                    }
                }
                ArrayList<T> r282 = Utils.m6415(r28);
                HashMap hashMap2 = new HashMap();
                hashMap2.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                Iterator<T> it3 = r282.iterator();
                while (it3.hasNext()) {
                    String str5 = (String) it3.next();
                    try {
                        if (subscriber.isUnsubscribed()) {
                            subscriber.onCompleted();
                            return;
                        }
                        String r27 = HttpHelper.m6343().m6363(str5, true, (Map<String, String>[]) new Map[]{hashMap2});
                        if (!r27.endsWith("srt") && !r27.endsWith("png")) {
                            boolean r23 = GoogleVideoHelper.m15955(r27);
                            boolean contains = r27.trim().toLowerCase().contains("amazon");
                            MediaSource mediaSource = new MediaSource(m16375(), r23 ? "GoogleVideo" : contains ? "AWS-FastServer" : "CDN-FastServer", !r23 && !contains && !r27.endsWith(".mp4") && !r27.contains("yandex"));
                            if (contains) {
                                r27 = r27.replace("?download=TRUE&", "?").replace("?download=TRUE", "").replace("?download=true&", "?").replace("?download=true", "").replace("&download=TRUE", "").replace("&download=true", "");
                            }
                            if (r23) {
                                for (MediaSource onNext : GoogleVideoHelper.m15952(r27, m16375())) {
                                    subscriber.onNext(onNext);
                                }
                            }
                            mediaSource.setStreamLink(r27);
                            mediaSource.setQuality(r23 ? GoogleVideoHelper.m15949(r27) : "HD");
                            subscriber.onNext(mediaSource);
                        }
                    } catch (Exception e5) {
                        Logger.m6281((Throwable) e5, new boolean[0]);
                    }
                }
            }
        }
    }
}
