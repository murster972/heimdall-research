package com.typhoon.tv.provider.universal;

import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

public class PFTV extends BaseProvider {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public String f12890 = "https://projecfreetv.co";

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16411(Subscriber<? super MediaSource> subscriber, String str) {
        Iterator it2 = Jsoup.m21674(HttpHelper.m6343().m6358(str, this.f12890)).m21815("tr").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (!subscriber.isUnsubscribed()) {
                String r6 = element.m21822();
                String replace = Regex.m17800(Regex.m17802(r6, "callvalue\\((.-?)\\)", 1, true), "(http.-?)(?:\\'|\\\")", 1).replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).replace("&amp;", "&");
                if (replace.startsWith("//")) {
                    replace = "http:" + replace;
                }
                if (!replace.isEmpty()) {
                    String lowerCase = Regex.m17802(r6, "quality(\\w+)\\.png", 1, true).trim().toLowerCase();
                    boolean[] zArr = new boolean[1];
                    zArr[0] = lowerCase.contains("cam") || lowerCase.contains("ts");
                    m16291(subscriber, replace, "HD", zArr);
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16412() {
        return "PFTV";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16413(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String str = PFTV.this.f12890 + "/movies/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName())) + "-" + mediaInfo.getYear() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                if (!HttpHelper.m6343().m6358(str, PFTV.this.f12890).contains("callvalue")) {
                    String unused = PFTV.this.f12890 = "https://projectfreetv.unblocked.gdn";
                    str = PFTV.this.f12890 + "/movies/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName())) + "-" + mediaInfo.getYear() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                    if (!HttpHelper.m6343().m6358(str, PFTV.this.f12890).contains("callvalue")) {
                        String unused2 = PFTV.this.f12890 = "https://projectfreetv1.bypassed.org";
                        str = PFTV.this.f12890 + "/movies/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName())) + "-" + mediaInfo.getYear() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                        if (!HttpHelper.m6343().m6358(str, PFTV.this.f12890).contains("callvalue")) {
                            String unused3 = PFTV.this.f12890 = "https://projectfreetv1.bypassed.bz";
                            str = PFTV.this.f12890 + "/movies/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName())) + "-" + mediaInfo.getYear() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                            if (!HttpHelper.m6343().m6358(str, PFTV.this.f12890).contains("callvalue")) {
                                String unused4 = PFTV.this.f12890 = "https://projectfreetv1.bypassed.eu";
                                str = PFTV.this.f12890 + "/movies/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName())) + "-" + mediaInfo.getYear() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                                String r0 = HttpHelper.m6343().m6358(str, PFTV.this.f12890);
                            }
                        }
                    }
                }
                PFTV.this.m16411(subscriber, str);
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16414(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String str = PFTV.this.f12890 + "/episode/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + str2 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                String r2 = HttpHelper.m6343().m6358(str, PFTV.this.f12890);
                if (!r2.contains("callvalue")) {
                    String unused = PFTV.this.f12890 = "https://projectfreetv.unblocked.gdn";
                    str = PFTV.this.f12890 + "/episode/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + str2 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                    r2 = HttpHelper.m6343().m6358(str, PFTV.this.f12890);
                    if (!r2.contains("callvalue")) {
                        String unused2 = PFTV.this.f12890 = "https://projectfreetv1.bypassed.org";
                        str = PFTV.this.f12890 + "/episode/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + str2 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                        r2 = HttpHelper.m6343().m6358(str, PFTV.this.f12890);
                        if (!r2.contains("callvalue")) {
                            String unused3 = PFTV.this.f12890 = "https://projectfreetv1.bypassed.bz";
                            str = PFTV.this.f12890 + "/episode/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + str2 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                            r2 = HttpHelper.m6343().m6358(str, PFTV.this.f12890);
                            if (!r2.contains("callvalue")) {
                                String unused4 = PFTV.this.f12890 = "https://projectfreetv1.bypassed.eu";
                                str = PFTV.this.f12890 + "/episode/" + TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + str2 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                                r2 = HttpHelper.m6343().m6358(str, PFTV.this.f12890);
                            }
                        }
                    }
                }
                String r1 = Regex.m17801(r2, "Released\\s*:\\s*</b>\\s*(\\d{4})", 1, 34);
                if (r1.isEmpty() || (Utils.m6426(r1) && Integer.parseInt(r1) == mediaInfo.getYear())) {
                    PFTV.this.m16411(subscriber, str);
                }
                subscriber.onCompleted();
            }
        });
    }
}
