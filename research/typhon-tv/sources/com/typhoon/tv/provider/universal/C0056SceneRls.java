package com.typhoon.tv.provider.universal;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

/* renamed from: com.typhoon.tv.provider.universal.SceneRls  reason: case insensitive filesystem */
public class C0056SceneRls extends BaseProvider {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f12923 = "http://scene-rls.net".replace("http://", "").replace("https://", "").replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "");

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16459(final MediaInfo mediaInfo, final String season, final String episode) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!C0056SceneRls.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                C0056SceneRls.this.m16455(subscriber, mediaInfo, season, episode);
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16458(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!C0056SceneRls.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                C0056SceneRls.this.m16455(subscriber, mediaInfo, "-1", "-1");
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16455(Subscriber<? super MediaSource> subscriber, MediaInfo mediaInfo, String season, String episode) {
        String queryExtra;
        TyphoonTV.ParsedLinkModel parsedLinkModel;
        String str;
        boolean isMovie = mediaInfo.getType() == 1;
        TyphoonTV dirHelper = new TyphoonTV();
        String searchTitle = mediaInfo.getName();
        if (isMovie) {
            queryExtra = String.valueOf(mediaInfo.getYear());
        } else {
            queryExtra = "S" + Utils.m6413(Integer.parseInt(season)) + "E" + Utils.m6413(Integer.parseInt(episode));
        }
        String searchUrl = "https://ia601502.us.archive.org/10/items/filesapps/" + String.format("scenerls.json", new Object[]{Utils.m6414(searchTitle.replace(": ", StringUtils.SPACE) + StringUtils.SPACE + queryExtra, new boolean[0]), Utils.m6414(String.valueOf(new Random().nextDouble()), new boolean[0])});
        HashMap<String, String> searchHeaders = TyphoonApp.m6325();
        searchHeaders.put("Cookie", "&serach_mode=light");
        HttpHelper.m6343().m6357(searchUrl, "&serach_mode=light");
        String searchJsonRes = Regex.m17802(HttpHelper.m6343().m6361(searchUrl, "http://scene-rls.net", (Map<String, String>[]) new Map[]{searchHeaders}), "(\\{.*?\\})$", 1, true);
        if (!searchJsonRes.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            try {
                Iterator<JsonElement> it2 = new JsonParser().parse(searchJsonRes).getAsJsonObject().get("results").getAsJsonArray().iterator();
                while (it2.hasNext()) {
                    JsonElement jEle = it2.next();
                    try {
                        String postTitle = jEle.getAsJsonObject().get("post_title").getAsString();
                        String postName = jEle.getAsJsonObject().get("post_name").getAsString();
                        if (!arrayList.contains(postName)) {
                            arrayList.add(postName);
                            if (isMovie) {
                                if (TitleHelper.m15971(postTitle).startsWith(TitleHelper.m15971(searchTitle)) && postTitle.contains(String.valueOf(mediaInfo.getYear()))) {
                                    String matchTitleLowerCase = postTitle.toLowerCase();
                                    if (!matchTitleLowerCase.contains(" 3d")) {
                                        if (!matchTitleLowerCase.contains("3d ")) {
                                            if (!matchTitleLowerCase.contains(" cam")) {
                                                if (!matchTitleLowerCase.contains("cam ")) {
                                                    if (!matchTitleLowerCase.contains("hdts ")) {
                                                        if (!matchTitleLowerCase.contains(" hdts")) {
                                                            if (!matchTitleLowerCase.contains(" ts ")) {
                                                                if (!matchTitleLowerCase.contains(" telesync")) {
                                                                    if (!matchTitleLowerCase.contains("telesync ")) {
                                                                        if (!matchTitleLowerCase.contains("hdtc ")) {
                                                                            if (!matchTitleLowerCase.contains(" hdtc")) {
                                                                                if (!matchTitleLowerCase.contains(" tc ")) {
                                                                                    if (!matchTitleLowerCase.contains(" telecine")) {
                                                                                        if (matchTitleLowerCase.contains("telecine ")) {
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (!TitleHelper.m15971(postTitle.replaceAll("(?i)(.*)([2-9]0\\d{2}|1[5-9]\\d{2})\\s+(S\\d+\\s*E\\d+)(.*)", "$1$3$4")).startsWith(TitleHelper.m15971(searchTitle + queryExtra.toLowerCase()))) {
                                }
                            }
                            Document postDoc = Jsoup.m21674(HttpHelper.m6343().m6358("http://scene-rls.net/" + postName, "http://scene-rls.net"));
                            ArrayList<String> arrayList3 = new ArrayList<>();
                            try {
                                Element elePost = postDoc.m21837("div.post");
                                if (elePost != null) {
                                    arrayList3.add(elePost.m21822());
                                }
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                            }
                            Iterator it3 = postDoc.m21815("div.comm_content").iterator();
                            while (it3.hasNext()) {
                                try {
                                    arrayList3.add(((Element) it3.next()).m21822());
                                } catch (Exception e2) {
                                    Logger.m6281((Throwable) e2, new boolean[0]);
                                }
                            }
                            for (String postContent : arrayList3) {
                                Iterator it4 = Regex.m17805(postContent.replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).replace("\\\"", "\""), "<a[^<]*href=['\"](.*?)['\"].*?</a>", 1, true).get(0).iterator();
                                while (it4.hasNext()) {
                                    String possibleResolverUrl = (String) it4.next();
                                    try {
                                        String urlHost = possibleResolverUrl.replace("http://", "").replace("https://", "");
                                        if (!urlHost.contains(f12923 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                            if (!urlHost.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + f12923) && !possibleResolverUrl.startsWith("#") && !possibleResolverUrl.contains("thepiratebay.org") && !possibleResolverUrl.contains("protected.to") && !possibleResolverUrl.contains("histats.com") && !possibleResolverUrl.contains("opensubtitles.org") && !possibleResolverUrl.contains("facebook.com") && !possibleResolverUrl.contains("twitter.com") && !possibleResolverUrl.contains("amazon.com") && !possibleResolverUrl.contains("youtube.com") && !possibleResolverUrl.contains("youtu.be") && !possibleResolverUrl.contains("imgaa.com") && !possibleResolverUrl.contains("imdb.com") && !possibleResolverUrl.contains("tvguide.com") && !possibleResolverUrl.contains(".7z") && !possibleResolverUrl.contains(".rar") && !possibleResolverUrl.contains(".zip") && !possibleResolverUrl.contains(".iso") && !possibleResolverUrl.contains(".avi") && !possibleResolverUrl.contains(".flv") && !possibleResolverUrl.contains(".pdf") && !possibleResolverUrl.contains(".mp3") && !possibleResolverUrl.contains("imdb.") && !arrayList2.contains(possibleResolverUrl)) {
                                                arrayList2.add(possibleResolverUrl);
                                                String quality = "HQ";
                                                String providerName = m16457();
                                                if (isMovie) {
                                                    parsedLinkModel = dirHelper.m15979(possibleResolverUrl);
                                                } else {
                                                    parsedLinkModel = dirHelper.m15980(possibleResolverUrl);
                                                }
                                                if (parsedLinkModel != null) {
                                                    quality = parsedLinkModel.m15985();
                                                    providerName = m16282(parsedLinkModel.m15983());
                                                }
                                                if (quality.equalsIgnoreCase("HQ")) {
                                                    if (isMovie) {
                                                        str = postName;
                                                    } else {
                                                        str = possibleResolverUrl;
                                                    }
                                                    String checkQualityStr = str.toLowerCase();
                                                    if (checkQualityStr.contains("1080p")) {
                                                        quality = "1080p";
                                                    } else if (checkQualityStr.contains("720p")) {
                                                        quality = "HD";
                                                    }
                                                }
                                                if (possibleResolverUrl.toLowerCase().contains("vidzi.") || possibleResolverUrl.toLowerCase().contains("estream.") || possibleResolverUrl.toLowerCase().contains(".m3u8")) {
                                                    quality = "HQ";
                                                }
                                                m16289(subscriber, possibleResolverUrl, quality, providerName);
                                            }
                                        }
                                    } catch (Exception e3) {
                                        Logger.m6281((Throwable) e3, new boolean[0]);
                                    }
                                }
                            }
                        }
                    } catch (Exception e4) {
                        Logger.m6281((Throwable) e4, new boolean[0]);
                    }
                }
            } catch (Exception e5) {
                Logger.m6281((Throwable) e5, new boolean[0]);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16457() {
        return "SceneRLS";
    }
}
