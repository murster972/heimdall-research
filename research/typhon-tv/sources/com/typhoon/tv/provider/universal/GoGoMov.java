package com.typhoon.tv.provider.universal;

import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import rx.Observable;
import rx.Subscriber;

public class GoGoMov extends BaseProvider {

    /* renamed from: 龘  reason: contains not printable characters */
    private String f12852 = "http://mvrls.com";

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16360(final MediaInfo mediaInfo, final String season, final String episode) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!GoGoMov.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                GoGoMov.this.m16356(subscriber, mediaInfo, season, episode);
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16359(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!GoGoMov.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                GoGoMov.this.m16356(subscriber, mediaInfo, "-1", "-1");
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16356(Subscriber<? super MediaSource> subscriber, MediaInfo mediaInfo, String season, String episode) {
        String queryExtra;
        TyphoonTV.ParsedLinkModel parsedLinkModel;
        boolean isMovie = mediaInfo.getType() == 1;
        TyphoonTV dirHelper = new TyphoonTV();
        String title = mediaInfo.getName();
        if (isMovie) {
            queryExtra = String.valueOf(mediaInfo.getYear());
        } else {
            queryExtra = "S" + Utils.m6413(Integer.parseInt(season)) + "E" + Utils.m6413(Integer.parseInt(episode));
        }
        String query = (title.replace("'", "") + StringUtils.SPACE + queryExtra).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", StringUtils.SPACE).replace("  ", StringUtils.SPACE);
        String searchXml = HttpHelper.m6343().m6351(this.f12852 + "/search/" + Utils.m6414(query, new boolean[0]) + "/feed/rss2", (Map<String, String>[]) new Map[0]);
        if (!searchXml.contains("item") && !searchXml.contains("enclosure")) {
            this.f12852 = "http://mvrls.com";
            searchXml = HttpHelper.m6343().m6351(this.f12852 + "/search/" + Utils.m6414(query, new boolean[0]) + "/feed/rss2", (Map<String, String>[]) new Map[0]);
        }
        ArrayList arrayList = new ArrayList();
        Iterator it2 = Jsoup.m21675(searchXml, "", Parser.m22154()).m21815("item").iterator();
        while (it2.hasNext()) {
            Element elePost = (Element) it2.next();
            Element eleTitle = elePost.m21837(PubnativeAsset.TITLE);
            if (eleTitle != null) {
                String matchTitle = eleTitle.m21868();
                Iterator it3 = elePost.m21815("enclosure[url]").iterator();
                while (it3.hasNext()) {
                    String newMatchTitle = matchTitle;
                    try {
                        String resolverUrl = ((Element) it3.next()).m21947("url");
                        if (!resolverUrl.contains("uploadgig") && !resolverUrl.contains(".7z") && !resolverUrl.contains(".rar") && !resolverUrl.contains(".zip") && !resolverUrl.contains(".iso") && !resolverUrl.contains(".7zip") && !resolverUrl.contains(".png") && !resolverUrl.contains("imdb.") && !arrayList.contains(resolverUrl)) {
                            arrayList.add(resolverUrl);
                            if (!isMovie && resolverUrl.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                String[] splittedUrlArr = resolverUrl.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                if (splittedUrlArr.length > 0) {
                                    newMatchTitle = newMatchTitle.replaceAll("(720p|1080p)", "") + StringUtils.SPACE + splittedUrlArr[splittedUrlArr.length - 1];
                                }
                            }
                            if (TitleHelper.m15971(title).equals(TitleHelper.m15971(newMatchTitle.replaceAll("(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)", "")))) {
                                List<String> extraMatches = Regex.m17803(newMatchTitle, "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]", 1).get(0);
                                if (!isMovie) {
                                    extraMatches.addAll(Regex.m17803(newMatchTitle, "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]", 1).get(0));
                                    extraMatches.addAll(Regex.m17803(newMatchTitle, "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]", 1).get(0));
                                }
                                if (extraMatches.size() > 0) {
                                    boolean isExtraMatch = false;
                                    Iterator<String> it4 = extraMatches.iterator();
                                    while (true) {
                                        if (it4.hasNext()) {
                                            if (it4.next().toUpperCase().equals(queryExtra)) {
                                                isExtraMatch = true;
                                                break;
                                            }
                                        } else {
                                            break;
                                        }
                                    }
                                    if (isExtraMatch) {
                                        String[] fmtSplitted = newMatchTitle.toUpperCase().split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                                        String quality = "HQ";
                                        boolean hasDisallowedFormat = false;
                                        int length = fmtSplitted.length;
                                        int i = 0;
                                        while (true) {
                                            if (i >= length) {
                                                break;
                                            }
                                            String fmt = fmtSplitted[i].toLowerCase();
                                            if (fmt.endsWith("subs") || fmt.endsWith("sub") || fmt.endsWith("pxui") || fmt.endsWith("xxui") || fmt.contains("dvdscr") || fmt.contains("r5") || fmt.contains("r6") || fmt.contains("camrip") || fmt.contains("tsrip") || fmt.contains("hdcam") || fmt.contains("hdts") || fmt.contains("dvdcam") || fmt.contains("dvdts") || fmt.contains("cam") || fmt.contains("telesync") || fmt.contains("ts") || fmt.contains("sample")) {
                                                hasDisallowedFormat = true;
                                            } else {
                                                if (fmt.contains("1080p") || fmt.equals("1080")) {
                                                    quality = "1080p";
                                                } else if (fmt.contains("720p") || fmt.equals("720") || fmt.contains("brrip") || fmt.contains("bdrip") || fmt.contains("hdrip") || fmt.contains("web-dl")) {
                                                    quality = "HD";
                                                }
                                                i++;
                                            }
                                        }
                                        hasDisallowedFormat = true;
                                        if (!hasDisallowedFormat) {
                                            String providerName = m16358();
                                            if (isMovie) {
                                                parsedLinkModel = dirHelper.m15979(resolverUrl);
                                            } else {
                                                parsedLinkModel = dirHelper.m15980(resolverUrl);
                                            }
                                            if (parsedLinkModel != null) {
                                                if (!parsedLinkModel.m15985().equalsIgnoreCase("HQ")) {
                                                    quality = parsedLinkModel.m15985();
                                                }
                                                providerName = m16282(parsedLinkModel.m15983());
                                            }
                                            m16289(subscriber, resolverUrl, quality, providerName);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16358() {
        return "MovieRelease";
    }
}
