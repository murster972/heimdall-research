package com.typhoon.tv.provider.universal;

import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

public class TVDL extends BaseProvider {
    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16473(Subscriber<? super MediaSource> subscriber, MediaInfo mediaInfo, String str, String str2) {
        Element r11;
        Element r15;
        Element r14;
        boolean z = mediaInfo.getType() == 1;
        TyphoonTV typhoonTV = new TyphoonTV();
        String str3 = z ? "" : "S" + Utils.m6413(Integer.parseInt(str)) + "E" + Utils.m6413(Integer.parseInt(str2));
        String name = mediaInfo.getName();
        String valueOf = z ? String.valueOf(mediaInfo.getYear()) : str3;
        String str4 = "https://tvdownload.net/?s=" + Utils.m6414((name.replace("'", "") + StringUtils.SPACE + valueOf).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", StringUtils.SPACE).replace("  ", StringUtils.SPACE), new boolean[0]);
        String r41 = HttpHelper.m6343().m6358(str4, "https://tvdownload.net/");
        ArrayList<String> arrayList = new ArrayList<>();
        Element r13 = Jsoup.m21674(r41).m21837("div.article#content");
        if (r13 != null) {
            Iterator it2 = r13.m21815("h2").iterator();
            while (it2.hasNext()) {
                try {
                    Element r10 = ((Element) it2.next()).m21837("a[href][title]");
                    if (r10 != null) {
                        String r27 = r10.m21947("href");
                        String replaceAll = r10.m21947(PubnativeAsset.TITLE).replaceAll("\\<[uibp]\\>", "").replaceAll("\\</[uibp]\\>", "");
                        String lowerCase = replaceAll.toLowerCase();
                        if (!z || (!lowerCase.contains(" png") && !lowerCase.contains("png ") && !lowerCase.contains(" cam") && !lowerCase.contains("cam ") && !lowerCase.contains("hdts ") && !lowerCase.contains(" hdts") && !lowerCase.contains(" ts ") && !lowerCase.contains(" telesync") && !lowerCase.contains("telesync ") && !lowerCase.contains("hdtc ") && !lowerCase.contains(" hdtc") && !lowerCase.contains(" tc ") && !lowerCase.contains(" telecine") && !lowerCase.contains("telecine "))) {
                            if (r27.startsWith("//")) {
                                r27 = "http:" + r27;
                            } else if (r27.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                r27 = "https://tvdownload.net" + r27;
                            }
                            if (replaceAll.toLowerCase().startsWith("goto")) {
                                replaceAll = replaceAll.substring(4, replaceAll.length()).trim();
                            }
                            if (z) {
                                if (TitleHelper.m15971(replaceAll).startsWith(TitleHelper.m15971(TitleHelper.m15970(mediaInfo.getName()))) && replaceAll.contains(String.valueOf(mediaInfo.getYear()))) {
                                    arrayList.add(r27);
                                }
                            } else if (TitleHelper.m15971(replaceAll).startsWith(TitleHelper.m15971(TitleHelper.m15970(mediaInfo.getName()))) && replaceAll.contains(str3)) {
                                arrayList.add(r27);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            if (!arrayList.isEmpty()) {
                HashMap hashMap = new HashMap();
                for (String r33 : arrayList) {
                    try {
                        Element r12 = Jsoup.m21674(HttpHelper.m6343().m6358(r33, str4)).m21837("div.single_post");
                        if (!(r12 == null || (r11 = r12.m21837("div#content")) == null || (r15 = r12.m21837("header")) == null || (r14 = r15.m21837("h1")) == null)) {
                            String r25 = r14.m21868();
                            Iterator it3 = r11.m21815("a[href]").iterator();
                            while (it3.hasNext()) {
                                try {
                                    String r39 = ((Element) it3.next()).m21947("href");
                                    if (r39.startsWith("//")) {
                                        r39 = "http:" + r39;
                                    } else if (r39.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                        r39 = "https://tvdownload.net" + r39;
                                    }
                                    if (!hashMap.containsKey(r39) && !r39.toLowerCase().contains("/rlsscn.in") && !r39.toLowerCase().contains("rlsscn.in/")) {
                                        hashMap.put(r39, r25);
                                    }
                                } catch (Exception e2) {
                                    Logger.m6281((Throwable) e2, new boolean[0]);
                                }
                            }
                        }
                    } catch (Exception e3) {
                        Logger.m6281((Throwable) e3, new boolean[0]);
                    }
                }
                if (!hashMap.isEmpty()) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        try {
                            String str5 = (String) entry.getKey();
                            String str6 = (String) entry.getValue();
                            if (!str5.contains(".7z") && !str5.contains(".rar") && !str5.contains(".zip") && !str5.contains(".iso") && !str5.contains(".avi") && !str5.contains(".exe") && !str5.contains("imdb.")) {
                                if (!z && str5.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                    String[] split = str5.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                    if (split.length > 0) {
                                        str6 = str6.replaceAll("(720p|1080p)", "") + StringUtils.SPACE + split[split.length - 1];
                                    }
                                }
                                if (TitleHelper.m15971(name).equals(TitleHelper.m15971(str6.replaceAll("(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)", "").replaceAll("Season\\s+\\d+\\s+Episode\\s+\\d+", "")))) {
                                    List list = Regex.m17803(str6, "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]", 1).get(0);
                                    if (!z) {
                                        list.addAll(Regex.m17803(str6, "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]", 1).get(0));
                                        list.addAll(Regex.m17803(str6, "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]", 1).get(0));
                                    }
                                    if (list.size() > 0) {
                                        boolean z2 = false;
                                        Iterator it4 = list.iterator();
                                        while (true) {
                                            if (it4.hasNext()) {
                                                if (((String) it4.next()).toUpperCase().equals(valueOf)) {
                                                    z2 = true;
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                        if (z2) {
                                            String[] split2 = str6.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                                            String str7 = "HQ";
                                            boolean z3 = false;
                                            int length = split2.length;
                                            int i = 0;
                                            while (true) {
                                                if (i >= length) {
                                                    break;
                                                }
                                                String lowerCase2 = split2[i].toLowerCase();
                                                if (lowerCase2.endsWith("subs") || lowerCase2.endsWith("sub") || lowerCase2.endsWith("dubbed") || lowerCase2.endsWith("dub") || lowerCase2.contains("dvdscr") || lowerCase2.contains("r5") || lowerCase2.contains("r6") || lowerCase2.contains("camrip") || lowerCase2.contains("tsrip") || lowerCase2.contains("hdcam") || lowerCase2.contains("hdts") || lowerCase2.contains("dvdcam") || lowerCase2.contains("dvdts") || lowerCase2.contains("cam") || lowerCase2.contains("telesync") || lowerCase2.contains("ts") || lowerCase2.contains("xpp")) {
                                                    z3 = true;
                                                } else {
                                                    if (lowerCase2.contains("1080p") || lowerCase2.equals("1080")) {
                                                        str7 = "1080p";
                                                    } else if (lowerCase2.contains("720p") || lowerCase2.equals("720") || lowerCase2.contains("brrip") || lowerCase2.contains("bdrip") || lowerCase2.contains("hdrip") || lowerCase2.contains("web-dl")) {
                                                        str7 = "HD";
                                                    }
                                                    i++;
                                                }
                                            }
                                            z3 = true;
                                            if (!z3) {
                                                String r34 = m16475();
                                                TyphoonTV.ParsedLinkModel r29 = z ? typhoonTV.m15979(str5) : typhoonTV.m15980(str5);
                                                if (r29 != null) {
                                                    if (!r29.m15985().equalsIgnoreCase("HQ")) {
                                                        str7 = r29.m15985();
                                                    }
                                                    r34 = m16282(r29.m15983());
                                                }
                                                m16289(subscriber, str5, str7, r34);
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e4) {
                            Logger.m6281((Throwable) e4, new boolean[0]);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16475() {
        return "X265Movies";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16476(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!TVDL.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                TVDL.this.m16473(subscriber, mediaInfo, "-1", "-1");
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16477(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!TVDL.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                TVDL.this.m16473(subscriber, mediaInfo, str, str2);
                subscriber.onCompleted();
            }
        });
    }
}
