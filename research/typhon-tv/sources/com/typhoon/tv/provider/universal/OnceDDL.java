package com.typhoon.tv.provider.universal;

import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import rx.Observable;
import rx.Subscriber;

public class OnceDDL extends BaseProvider {
    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16392(Subscriber<? super MediaSource> subscriber, MediaInfo mediaInfo, String str, String str2) {
        boolean z = mediaInfo.getType() == 1;
        TyphoonTV typhoonTV = new TyphoonTV();
        String name = mediaInfo.getName();
        String valueOf = z ? String.valueOf(mediaInfo.getYear()) : "S" + Utils.m6413(Integer.parseInt(str)) + "E" + Utils.m6413(Integer.parseInt(str2));
        String r28 = HttpHelper.m6343().m6351("https://onceddl.net/find/keyword/" + Utils.m6414((name + StringUtils.SPACE + valueOf).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", StringUtils.SPACE).replace("  ", StringUtils.SPACE), new boolean[0]) + "/rss2/", (Map<String, String>[]) new Map[0]);
        ArrayList arrayList = new ArrayList();
        Iterator it2 = Jsoup.m21675(r28, "", Parser.m22154()).m21815("item").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            Element r10 = element.m21837(PubnativeAsset.TITLE);
            if (r10 != null) {
                String r19 = r10.m21868();
                Iterator it3 = element.m21815("enclosure[url]").iterator();
                while (it3.hasNext()) {
                    String str3 = r19;
                    try {
                        String r26 = ((Element) it3.next()).m21947("url");
                        if (!r26.contains("openload") && !r26.contains(".7z") && !r26.contains(".rar") && !r26.contains(".zip") && !r26.contains(".iso") && !r26.contains(".avi") && !r26.contains(".flv") && !r26.contains("imdb.") && !arrayList.contains(r26)) {
                            arrayList.add(r26);
                            if (!z && r26.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                String[] split = r26.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                if (split.length > 0) {
                                    str3 = str3.replaceAll("(720p|1080p)", "") + StringUtils.SPACE + split[split.length - 1];
                                }
                            }
                            if (TitleHelper.m15971(name).equals(TitleHelper.m15971(str3.replaceAll("(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)", "")))) {
                                List list = Regex.m17803(str3, "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]", 1).get(0);
                                if (!z) {
                                    list.addAll(Regex.m17803(str3, "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]", 1).get(0));
                                    list.addAll(Regex.m17803(str3, "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]", 1).get(0));
                                }
                                if (list.size() > 0) {
                                    boolean z2 = false;
                                    Iterator it4 = list.iterator();
                                    while (true) {
                                        if (it4.hasNext()) {
                                            if (((String) it4.next()).toUpperCase().equals(valueOf)) {
                                                z2 = true;
                                                break;
                                            }
                                        } else {
                                            break;
                                        }
                                    }
                                    if (z2) {
                                        String[] split2 = str3.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                                        String str4 = "HQ";
                                        boolean z3 = false;
                                        int length = split2.length;
                                        int i = 0;
                                        while (true) {
                                            if (i >= length) {
                                                break;
                                            }
                                            String lowerCase = split2[i].toLowerCase();
                                            if (lowerCase.endsWith("subs") || lowerCase.endsWith("sub") || lowerCase.endsWith("dubbed") || lowerCase.endsWith("dub") || lowerCase.contains("dvdscr") || lowerCase.contains("r5") || lowerCase.contains("r6") || lowerCase.contains("camrip") || lowerCase.contains("tsrip") || lowerCase.contains("hdcam") || lowerCase.contains("hdts") || lowerCase.contains("dvdcam") || lowerCase.contains("dvdts") || lowerCase.contains("cam") || lowerCase.contains("telesync") || lowerCase.contains("ts") || lowerCase.contains("3d")) {
                                                z3 = true;
                                            } else {
                                                if (lowerCase.contains("1080p") || lowerCase.equals("1080")) {
                                                    str4 = "1080p";
                                                } else if (lowerCase.contains("720p") || lowerCase.equals("720") || lowerCase.contains("brrip") || lowerCase.contains("bdrip") || lowerCase.contains("hdrip") || lowerCase.contains("web-dl")) {
                                                    str4 = "HD";
                                                }
                                                i++;
                                            }
                                        }
                                        z3 = true;
                                        if (!z3) {
                                            String r22 = m16394();
                                            TyphoonTV.ParsedLinkModel r21 = z ? typhoonTV.m15979(r26) : typhoonTV.m15980(r26);
                                            if (r21 != null) {
                                                if (!r21.m15985().equalsIgnoreCase("HQ")) {
                                                    str4 = r21.m15985();
                                                }
                                                r22 = m16282(r21.m15983());
                                            }
                                            m16289(subscriber, r26, str4, r22);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16394() {
        return "OnceDDL";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16395(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!OnceDDL.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                OnceDDL.this.m16392(subscriber, mediaInfo, "-1", "-1");
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16396(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                if (!OnceDDL.this.m16283()) {
                    subscriber.onCompleted();
                    return;
                }
                OnceDDL.this.m16392(subscriber, mediaInfo, str, str2);
                subscriber.onCompleted();
            }
        });
    }
}
