package com.typhoon.tv.provider;

import com.typhoon.tv.Logger;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import rx.Observable;
import rx.Subscriber;

public abstract class BaseProvider {

    /* renamed from: 龘  reason: contains not printable characters */
    private String[] f12808;

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m16284();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16288(MediaInfo mediaInfo, String season, String episode) {
        return Observable.m7349();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16287(MediaInfo mediaInfo, int season, int episode) {
        return m16288(mediaInfo, String.valueOf(season), String.valueOf(episode));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16286(MediaInfo mediaInfo) {
        return Observable.m7349();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final ArrayList<String> m16285(String html) {
        ArrayList<String> matches = Regex.m17805(html, "['\"]?sources?['\"]?\\s*:\\s*\\[(.*?)\\]", 1, true).get(0);
        matches.addAll(Regex.m17805(html, "['\"]?sources?[\"']?\\s*:\\s*\\[(.*?)\\}\\s*,?\\s*\\]", 1, true).get(0));
        matches.addAll(Regex.m17805(html, "['\"]?sources?['\"]?\\s*:\\s*\\{(.*?)\\}", 1, true).get(0));
        matches.addAll(Regex.m17806(html, "['\"]?sources?[\"']?\\s*:\\s*[\\{\\[](\\s*)[\\}\\]]", true));
        List<String> emptyStringList = new ArrayList<>();
        emptyStringList.add("");
        matches.removeAll(emptyStringList);
        ArrayList<String> matches2 = Utils.m6415(matches);
        ArrayList<String> sources = new ArrayList<>();
        ArrayList<String> sourceMatches = new ArrayList<>();
        if (matches2.isEmpty()) {
            sourceMatches.addAll(Regex.m17805(html, "\\{\\s*['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]", 1, true).get(0));
        } else {
            Iterator<String> it2 = matches2.iterator();
            while (it2.hasNext()) {
                sourceMatches.addAll(Regex.m17805(it2.next(), "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)", 1, true).get(0));
            }
        }
        Iterator<String> it3 = sourceMatches.iterator();
        while (it3.hasNext()) {
            sources.add(it3.next().replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).replace("\\\\", ""));
        }
        return Utils.m6415(sources);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m16291(Subscriber<? super MediaSource> subscriber, String streamUrl, String quality, boolean... isCamArr) {
        String host;
        boolean isCam = false;
        if (isCamArr != null && isCamArr.length > 0 && isCamArr[0]) {
            isCam = true;
        }
        try {
            host = new URL(streamUrl).getHost();
        } catch (Exception e) {
            host = streamUrl;
        }
        m16290(subscriber, streamUrl, quality, m16284(), host, isCam);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m16289(Subscriber<? super MediaSource> subscriber, String streamUrl, String quality, String providerName) {
        String host;
        try {
            host = new URL(streamUrl).getHost();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            host = streamUrl;
        }
        m16290(subscriber, streamUrl, quality, providerName, host, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m16290(Subscriber<? super MediaSource> subscriber, String streamUrl, String quality, String providerName, String host, boolean isCam) {
        try {
            if (!subscriber.isUnsubscribed() && !host.equals("www.limetorrents.info") && !host.equals("archive.org") && !host.equals("themoviedb.org") && !host.equals("moviesleak.net") && !host.equals("imdb.com")) {
                if (this.f12808 == null) {
                    this.f12808 = BaseResolver.m16769();
                    if (RealDebridCredentialsHelper.m15838().isValid()) {
                        this.f12808 = Utils.m6428(this.f12808, BaseResolver.m16770());
                    }
                }
                boolean isResolvable = false;
                String[] strArr = this.f12808;
                int length = strArr.length;
                int i = 1;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    String resolverName = strArr[i];
                    if (TitleHelper.m15971(host).contains(TitleHelper.m15971(resolverName)) || TitleHelper.m15971(resolverName).contains(TitleHelper.m15971(host))) {
                        isResolvable = true;
                    } else {
                        i++;
                    }
                }
                isResolvable = true;
                if (isResolvable) {
                    if (quality == null || quality.isEmpty()) {
                        quality = "";
                    }
                    if (isCam) {
                        providerName = m16284() + " (CAM)";
                    }
                    MediaSource mediaSource = new MediaSource(providerName, "", true);
                    mediaSource.setQuality(quality);
                    mediaSource.setStreamLink(streamUrl);
                    subscriber.onNext(mediaSource);
                }
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m16282(String extra) {
        String providerName = m16284();
        String extra2 = extra.trim().toLowerCase();
        if (extra2.contains("web")) {
            providerName = providerName + " (WEB-DL)";
        } else if (extra2.contains("hdtv")) {
            providerName = providerName + " (HDTV)";
        }
        if (extra2.contains("264")) {
            providerName = providerName + " (x264)";
        } else if (extra2.contains("265") || extra2.contains("hevc")) {
            providerName = providerName + " (x265)";
        }
        if (extra2.contains("5.1") || extra2.contains("6ch")) {
            providerName = providerName + " (5.1CH)";
        } else if (extra2.contains("7.1")) {
            providerName = providerName + " (7.1CH)";
        } else if (extra2.contains("imax")) {
            providerName = providerName + " (IMAX)";
        }
        if (extra2.contains("truehd")) {
            return providerName + " (TrueHD)";
        }
        if (extra2.contains("atmos")) {
            return providerName + " (Atmos)";
        }
        return providerName;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m16283() {
        return RealDebridCredentialsHelper.m15838().isValid();
    }
}
