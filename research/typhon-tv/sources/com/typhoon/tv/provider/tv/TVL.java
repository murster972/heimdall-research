package com.typhoon.tv.provider.tv;

import com.mopub.common.TyphoonApp;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import rx.Observable;
import rx.Subscriber;

public class TVL extends BaseProvider {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f12821 = {""};

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16317(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                TVL.this.m16314(subscriber, mediaInfo, str, str2);
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16316(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                TVL.this.m16314(subscriber, mediaInfo, "-1", "-1");
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16314(Subscriber<? super MediaSource> subscriber, MediaInfo mediaInfo, String str, String str2) {
        String str3;
        TyphoonTV.ParsedLinkModel r3;
        TyphoonTV.ParsedLinkModel r4;
        TyphoonTV.ParsedLinkModel r32;
        boolean z = mediaInfo.getType() == 1;
        String r5 = TitleHelper.m15971(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", "").replace("'", ""));
        TyphoonTV typhoonTV = new TyphoonTV();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= f12821.length) {
                return;
            }
            if (subscriber.isUnsubscribed()) {
                subscriber.onCompleted();
                return;
            }
            String str4 = f12821[i2];
            if (i2 <= 1) {
                Iterator it2 = typhoonTV.龘(str4, new boolean[0]).iterator();
                while (it2.hasNext()) {
                    TyphoonTV.DirectoryFileModel directoryFileModel = (TyphoonTV.DirectoryFileModel) it2.next();
                    if (subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                        return;
                    } else if (!directoryFileModel.麤()) {
                        if (z) {
                            r32 = typhoonTV.齉(directoryFileModel.龘());
                        } else {
                            r32 = typhoonTV.m15979(directoryFileModel.龘());
                        }
                        if (r32 != null && !r32.m15984() && typhoonTV.m15982(r32, mediaInfo, str, str2)) {
                            MediaSource mediaSource = new MediaSource(m16282(r32.m15983()), "CacheTorrents", false);
                            mediaSource.setStreamLink(directoryFileModel.靐());
                            mediaSource.setQuality(r32.m15985());
                            subscriber.onNext(mediaSource);
                        }
                    }
                }
                continue;
            } else if (i2 == 2 || i2 == 3 || i2 == 4) {
                String r0 = HttpHelper.m6343().m6351(str4, (Map<String, String>[]) new Map[0]);
                if (z) {
                    str3 = str4 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                } else {
                    Iterator it3 = typhoonTV.m15980(r0).iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            str3 = "";
                            break;
                        }
                        TyphoonTV.DirectoryFileModel directoryFileModel2 = (TyphoonTV.DirectoryFileModel) it3.next();
                        if (!subscriber.isUnsubscribed()) {
                            String r33 = directoryFileModel2.龘();
                            String r9 = directoryFileModel2.齉();
                            if (directoryFileModel2.麤() && TitleHelper.m15971(r9).equals(r5)) {
                                str3 = r33;
                                break;
                            }
                        } else {
                            subscriber.onCompleted();
                            return;
                        }
                    }
                    if (str3.isEmpty()) {
                        str3 = str4 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                    } else {
                        try {
                            str3 = new URL(new URL(str4 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR), str3).toString();
                        } catch (MalformedURLException e) {
                            if (str3.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                str3 = str4 + str3;
                            } else if (!str3.startsWith(TyphoonApp.HTTP) && !str3.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                str3 = str4 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str3;
                            }
                        }
                    }
                }
                Iterator it4 = typhoonTV.龘(str3, new boolean[]{z}).iterator();
                while (it4.hasNext()) {
                    TyphoonTV.DirectoryFileModel directoryFileModel3 = (TyphoonTV.DirectoryFileModel) it4.next();
                    if (subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                        return;
                    } else if (!directoryFileModel3.麤()) {
                        if (z) {
                            r3 = typhoonTV.齉(directoryFileModel3.龘());
                        } else {
                            r3 = typhoonTV.m15979(directoryFileModel3.龘());
                        }
                        if (r3 != null && !r3.m15984() && typhoonTV.m15981(r3, mediaInfo, Integer.parseInt(str), Integer.parseInt(str2))) {
                            MediaSource mediaSource2 = new MediaSource(m16282(r3.m15983()), "CacheTorrents", false);
                            mediaSource2.setStreamLink(directoryFileModel3.靐());
                            mediaSource2.setQuality(r3.m15985());
                            subscriber.onNext(mediaSource2);
                        }
                    }
                }
                continue;
            } else if (z && i2 >= 5) {
                Iterator it5 = typhoonTV.龘(str4, new boolean[]{true}).iterator();
                while (it5.hasNext()) {
                    TyphoonTV.DirectoryFileModel directoryFileModel4 = (TyphoonTV.DirectoryFileModel) it5.next();
                    if (subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                        return;
                    } else if (!directoryFileModel4.麤() && (r4 = typhoonTV.齉(directoryFileModel4.龘().replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, ""))) != null && !r4.m15984() && typhoonTV.m15981(r4, mediaInfo, -1, -1)) {
                        MediaSource mediaSource3 = new MediaSource(m16282(r4.m15983()), "CacheTorrents", false);
                        mediaSource3.setStreamLink(directoryFileModel4.靐());
                        mediaSource3.setQuality(r4.m15985());
                        subscriber.onNext(mediaSource3);
                    }
                }
                continue;
            }
            i = i2 + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16315() {
        return "Priv";
    }
}
