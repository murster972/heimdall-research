package com.typhoon.tv.provider.tv;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

public class WatchEpisodes extends BaseProvider {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public String f12828 = "http://www.watchepisodes4.com";

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16323() {
        return "WatchEpisodes";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16324(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                JsonElement jsonElement;
                String str;
                String r25;
                String r20 = HttpHelper.m6343().m6351(WatchEpisodes.this.f12828 + "/search/ajax_search?q=" + Utils.m6414(mediaInfo.getName(), new boolean[0]), (Map<String, String>[]) new Map[]{TyphoonApp.m6325()});
                if (!r20.toLowerCase().contains("series")) {
                    String unused = WatchEpisodes.this.f12828 = "https://watchepisodes.unblocked.gdn";
                    r20 = HttpHelper.m6343().m6351(WatchEpisodes.this.f12828 + "/search/ajax_search?q=" + Utils.m6414(mediaInfo.getName(), new boolean[0]), (Map<String, String>[]) new Map[]{TyphoonApp.m6325()});
                    if (!r20.toLowerCase().contains("series")) {
                        String unused2 = WatchEpisodes.this.f12828 = "https://watchepisodes1.bypassed.eu";
                        r20 = HttpHelper.m6343().m6351(WatchEpisodes.this.f12828 + "/search/ajax_search?q=" + Utils.m6414(mediaInfo.getName(), new boolean[0]), (Map<String, String>[]) new Map[]{TyphoonApp.m6325()});
                        if (!r20.toLowerCase().contains("series")) {
                            String unused3 = WatchEpisodes.this.f12828 = "https://watchepisodes1.bypassed.bz";
                            r20 = HttpHelper.m6343().m6351(WatchEpisodes.this.f12828 + "/search/ajax_search?q=" + Utils.m6414(mediaInfo.getName(), new boolean[0]), (Map<String, String>[]) new Map[]{TyphoonApp.m6325()});
                        }
                    }
                }
                String str2 = "";
                String str3 = "";
                JsonElement parse = new JsonParser().parse(r20);
                if (parse != null && parse.isJsonObject() && (jsonElement = parse.getAsJsonObject().get("series")) != null && jsonElement.isJsonArray()) {
                    Iterator<JsonElement> it2 = jsonElement.getAsJsonArray().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        JsonElement next = it2.next();
                        if (next.isJsonObject()) {
                            JsonObject asJsonObject = next.getAsJsonObject();
                            JsonElement jsonElement2 = asJsonObject.get("label");
                            JsonElement jsonElement3 = asJsonObject.get("seo");
                            if (!(jsonElement2 == null || jsonElement2.isJsonNull() || jsonElement2.getAsString() == null || jsonElement3 == null || jsonElement3.isJsonNull() || jsonElement3.getAsString() == null || !TitleHelper.m15971(jsonElement2.getAsString()).equals(TitleHelper.m15971(mediaInfo.getName())))) {
                                str = WatchEpisodes.this.f12828 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + jsonElement3.getAsString();
                                r25 = HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[0]);
                                String r19 = Regex.m17801(r25, "<span>\\s*Year\\s*:\\s*</span>.*?<a[^>]*>\\s*(\\d{4})\\s*</a>", 1, 34);
                                if (r19.isEmpty() || !Utils.m6426(r19) || mediaInfo.getYear() <= 0 || Integer.parseInt(r19.trim()) == mediaInfo.getYear()) {
                                    str2 = str;
                                    str3 = r25;
                                }
                            }
                        }
                    }
                    str2 = str;
                    str3 = r25;
                }
                if (str2.isEmpty() || str3.isEmpty()) {
                    str3 = HttpHelper.m6343().m6351(WatchEpisodes.this.f12828 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + TitleHelper.m15972(mediaInfo.getName()), (Map<String, String>[]) new Map[0]);
                }
                String r10 = Regex.m17800(str3, "href=\"([^\"]*-[sS]" + Utils.m6413(Integer.parseInt(str)) + "[eE]" + Utils.m6413(Integer.parseInt(str2)) + "(?!\\d)[^\"]*)", 1);
                if (r10.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (r10.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                    r10 = WatchEpisodes.this.f12828 + r10;
                }
                Iterator it3 = Jsoup.m21674(HttpHelper.m6343().m6351(r10, (Map<String, String>[]) new Map[0])).m21815("div[class*=\"ldr-item\"]").iterator();
                while (it3.hasNext()) {
                    Element element = (Element) it3.next();
                    if (subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                        return;
                    }
                    Element r7 = element.m21837("a[data-actuallink*=\"http\"]");
                    if (r7 != null) {
                        WatchEpisodes.this.m16291(subscriber, r7.m21947("data-actuallink"), "HD", new boolean[0]);
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
