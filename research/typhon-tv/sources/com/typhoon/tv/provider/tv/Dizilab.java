package com.typhoon.tv.provider.tv;

import com.typhoon.tv.RxBus;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.ReCaptchaRequiredEvent;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

public class Dizilab extends BaseProvider {
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16310() {
        return "Dizilab";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16311(final MediaInfo mediaInfo, final int i, final int i2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String str = "https://m.dizilab.pw/" + TitleHelper.m15972(mediaInfo.getName().replace("Marvel's ", "")) + "/sezon-" + i + "/bolum-" + i2;
                String r20 = HttpHelper.m6343().m6359(str, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", "https://m.dizilab.pw", (Map<String, String>[]) new Map[0]);
                if (r20.contains("bot kontrol")) {
                    String r6 = Regex.m17801(r20, "<a[^>]*href=['\"](.*?)['\"][^>]*>Valla billa bot de.*?</a>", 1, 34);
                    if (!r6.isEmpty()) {
                        if (r6.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                            r6 = "https://m.dizilab.pw" + r6;
                        } else if (r6.startsWith("//")) {
                            r6 = "http:" + r6;
                        }
                        r20 = HttpHelper.m6343().m6359(r6, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", str, (Map<String, String>[]) new Map[0]);
                        str = r6;
                    }
                }
                if (r20.contains("Please complete the security check to access")) {
                    RxBus.m15709().m15711(new ReCaptchaRequiredEvent(Dizilab.this.m16310(), "https://m.dizilab.pw"));
                    subscriber.onCompleted();
                    return;
                }
                String str2 = "https://m.dizilab.pw/" + TitleHelper.m15972(mediaInfo.getName().replace("Marvel's ", ""));
                String r15 = Regex.m17801(HttpHelper.m6343().m6359(str2, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", "https://m.dizilab.pw", (Map<String, String>[]) new Map[0]), "YAPIM\\s*YILI\\s*<span>\\s*(\\d{4})\\s*<", 1, 34);
                if (r15.isEmpty() || !Utils.m6426(r15) || Integer.parseInt(r15) == mediaInfo.getYear() || Integer.parseInt(r15) == mediaInfo.getYear() + 1 || Integer.parseInt(r15) == mediaInfo.getYear() - 1) {
                    Element r7 = Jsoup.m21674(r20).m21837("ul.language.alternative");
                    if (r7 == null) {
                        subscriber.onCompleted();
                        return;
                    }
                    HashMap<String, String> r11 = TyphoonApp.m6325();
                    r11.put("Referer", str);
                    r11.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                    Iterator it2 = r7.m21815("li").iterator();
                    while (it2.hasNext()) {
                        Element element = (Element) it2.next();
                        if (subscriber.isUnsubscribed()) {
                            subscriber.onCompleted();
                            return;
                        }
                        String r13 = element.m21822();
                        boolean z = false;
                        if (!r13.contains("icon-orj")) {
                            z = true;
                        }
                        String replace = HttpHelper.m6343().m6350("https://m.dizilab.pw/request/php/", "vid=" + Utils.m6414(Regex.m17800(r13, "onclick\\s*=\\s*\"loadVideo\\('([^']+)", 1), new boolean[0]) + "&tip=1&type=loadVideo", r11).replace("\\\"", "\"").replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                        ArrayList<String> r26 = Dizilab.this.m16285(replace);
                        String r14 = Regex.m17802(replace, "src=\"([^\"]+)", 1, true);
                        if (r14.contains("dizilab") && r14.trim().toLowerCase().contains("player")) {
                            if (r14.startsWith("//")) {
                                r14 = "http:" + r14;
                            } else if (r14.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                r14 = "https://m.dizilab.pw" + r14;
                            }
                            String r22 = HttpHelper.m6343().m6359(r14, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36", str2, (Map<String, String>[]) new Map[0]);
                            r26.addAll(Dizilab.this.m16285(r22));
                            String r17 = Regex.m17802(r22, "src=\"([^\"]+)", 1, true);
                            if (!r17.isEmpty()) {
                                r14 = r17;
                            }
                        }
                        r26.add(r14);
                        for (String str3 : r26) {
                            if (!str3.isEmpty()) {
                                boolean r12 = GoogleVideoHelper.m15955(str3);
                                if (r12 && GoogleVideoHelper.m15953(str3) && (!z || GoogleVideoHelper.m15940(str3))) {
                                    HashMap<String, String> r30 = GoogleVideoHelper.m15948(str3);
                                    if (r30 != null && !r30.isEmpty()) {
                                        for (Map.Entry next : r30.entrySet()) {
                                            MediaSource mediaSource = new MediaSource(Dizilab.this.m16310(), "GoogleVideo", false);
                                            mediaSource.setStreamLink((String) next.getKey());
                                            mediaSource.setQuality(((String) next.getValue()).isEmpty() ? "HD" : (String) next.getValue());
                                            HashMap hashMap = new HashMap();
                                            hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                                            hashMap.put("Cookie", GoogleVideoHelper.m15954(str3, (String) next.getKey()));
                                            mediaSource.setPlayHeader(hashMap);
                                            subscriber.onNext(mediaSource);
                                        }
                                    }
                                } else if (!z) {
                                    MediaSource mediaSource2 = new MediaSource(Dizilab.this.m16310(), r12 ? "GoogleVideo" : "CDN", !r12);
                                    mediaSource2.setStreamLink(str3);
                                    mediaSource2.setQuality(r12 ? GoogleVideoHelper.m15949(str3) : "HD");
                                    subscriber.onNext(mediaSource2);
                                }
                            }
                        }
                    }
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onCompleted();
            }
        });
    }
}
