package com.typhoon.tv.provider.tv;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import rx.Observable;
import rx.Subscriber;

public class Dizibox extends BaseProvider {
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16305() {
        return "Dizibox";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16306(final MediaInfo mediaInfo, final String str, final String str2) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                JsonElement jsonElement;
                JsonElement jsonElement2;
                String asString;
                String r21;
                String r39 = TitleHelper.m15972(TitleHelper.m15970(mediaInfo.getName().replace("Marvel's ", "").replace("DC's ", "")).replace("'", "").replace("P.D.", "pd"));
                if (mediaInfo.getTmdbId() == 1408) {
                    r39 = "house-m-d";
                }
                String str = "http://www.dizibox.pw/" + r39 + "-" + str + "-sezon-" + str2 + "-bolum-izle/";
                String replaceAll = HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[0]).replaceAll("[^\\x00-\\x7F]+", "");
                Document r5 = Jsoup.m21674(replaceAll);
                boolean z = false;
                Iterator it2 = r5.m21815("option").iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Element element = (Element) it2.next();
                    try {
                        r21 = element.m21951("value") ? element.m21947("value") : element.m21951("href") ? element.m21947("href") : "";
                        String r20 = element.m21822();
                        if (r20.equals("Altyazsz") || r20.equals("Altyazısız")) {
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                if (r21.startsWith("//")) {
                    r21 = "http:" + r21;
                }
                if (r21.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                    r21 = "https://www.dizibox.pw" + r21;
                }
                z = true;
                if (!r21.isEmpty()) {
                    str = r21;
                    replaceAll = HttpHelper.m6343().m6351(r21, (Map<String, String>[]) new Map[0]).replaceAll("[^\\x00-\\x7F]+", "");
                    r5 = Jsoup.m21674(replaceAll);
                }
                if (!z || replaceAll.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                ArrayList arrayList = new ArrayList();
                Iterator it3 = r5.m21815("span[class*=\"embed-responsive-item\"]").iterator();
                while (it3.hasNext()) {
                    Iterator it4 = ((Element) it3.next()).m21815("iframe[src]").iterator();
                    while (true) {
                        if (it4.hasNext()) {
                            Element element2 = (Element) it4.next();
                            try {
                                if (subscriber.isUnsubscribed()) {
                                    subscriber.onCompleted();
                                    return;
                                }
                                String r18 = element2.m21947("src");
                                if (r18.startsWith("//")) {
                                    r18 = "http:" + r18;
                                } else if (r18.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                    r18 = "https://www.dizibox.pw" + r18;
                                }
                                String r42 = Regex.m17802(r18, "v=(.*)", 1, true);
                                if (r42.isEmpty() || !r18.contains("?")) {
                                    Dizibox.this.m16291(subscriber, r18, "HD", new boolean[0]);
                                } else {
                                    HashMap<String, String> r29 = TyphoonApp.m6325();
                                    r29.put("Referer", r18);
                                    String replace = HttpHelper.m6343().m6351(r18.split("\\?")[0] + "?v=" + r42, (Map<String, String>[]) new Map[]{r29}).replace("\\\"", "\"").replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                    if (!replace.isEmpty()) {
                                        try {
                                            Element r8 = Jsoup.m21674(replace).m21837("iframe[src]");
                                            if (r8 != null) {
                                                String r34 = r8.m21947("src");
                                                if (r34.startsWith("//")) {
                                                    r34 = "http:" + r34;
                                                } else if (r34.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                                    r34 = "https://www.dizibox.pw" + r34;
                                                } else if (!r34.startsWith(com.mopub.common.TyphoonApp.HTTP)) {
                                                    r34 = "https://www.dizibox.pw/" + r34;
                                                }
                                                Dizibox.this.m16291(subscriber, r34, "HD", new boolean[0]);
                                            }
                                        } catch (Exception e2) {
                                            Logger.m6281((Throwable) e2, new boolean[0]);
                                        }
                                        ArrayList<ArrayList<String>> r12 = Regex.m17803(replace, "\"file\"\\s*:\\s*\"([^\"]+)\"\\s*,\\s*\"label\"\\s*:\\s*\"([^\"]+)\"", 2);
                                        ArrayList arrayList2 = r12.get(0);
                                        ArrayList arrayList3 = r12.get(1);
                                        for (int i = 0; i < arrayList2.size(); i++) {
                                            String str2 = (String) arrayList2.get(i);
                                            if (!str2.endsWith("/.mp4") && !str2.isEmpty() && !arrayList.contains(str2)) {
                                                String str3 = "HQ";
                                                if (i < arrayList3.size()) {
                                                    str3 = (String) arrayList3.get(i);
                                                }
                                                if (GoogleVideoHelper.m15955(str2)) {
                                                    str3 = GoogleVideoHelper.m15949(str2);
                                                }
                                                if (str3.toLowerCase().contains("high")) {
                                                    str3 = "HD";
                                                } else if (str3.toLowerCase().contains("standard") || str3.toLowerCase().contains("low")) {
                                                    str3 = "HQ";
                                                }
                                                MediaSource mediaSource = new MediaSource(Dizibox.this.m16305(), GoogleVideoHelper.m15955(str2) ? "GoogleVideo" : "CDN", false);
                                                mediaSource.setStreamLink(str2);
                                                mediaSource.setQuality(str3);
                                                subscriber.onNext(mediaSource);
                                                arrayList.add(str2);
                                            }
                                        }
                                    }
                                }
                                Elements r28 = Jsoup.m21674(HttpHelper.m6343().m6358(r18, str)).m21815("param[value][name=\"flashvars\"]");
                                if (!r28.isEmpty()) {
                                    Iterator it5 = r28.iterator();
                                    while (it5.hasNext()) {
                                        String r24 = Regex.m17800(((Element) it5.next()).m21947("value"), "metadataUrl=([^\"]+)", 1);
                                        if (!r24.isEmpty() && (r24.contains("ok.ru") || r24.contains("odnoklassniki"))) {
                                            try {
                                                r24 = URLDecoder.decode(r24, "UTF-8");
                                            } catch (UnsupportedEncodingException e3) {
                                            }
                                            String r26 = HttpHelper.m6343().m6351(r24, (Map<String, String>[]) new Map[0]);
                                            if (!r26.isEmpty()) {
                                                JsonElement jsonElement3 = null;
                                                try {
                                                    jsonElement3 = new JsonParser().parse(r26);
                                                } catch (Exception e4) {
                                                    Logger.m6281((Throwable) e4, new boolean[0]);
                                                }
                                                if (jsonElement3 == null) {
                                                    Dizibox.this.m16291(subscriber, r24, "HD", new boolean[0]);
                                                } else if (!jsonElement3.isJsonNull() && jsonElement3.isJsonObject() && (jsonElement = jsonElement3.getAsJsonObject().get("movie")) != null && !jsonElement.isJsonNull() && jsonElement.isJsonObject() && (jsonElement2 = jsonElement.getAsJsonObject().get("url")) != null && !jsonElement2.isJsonNull() && (asString = jsonElement2.getAsString()) != null && !asString.isEmpty()) {
                                                    MediaSource mediaSource2 = new MediaSource(Dizibox.this.m16305(), "ok.ru", true);
                                                    mediaSource2.setStreamLink(asString);
                                                    mediaSource2.setQuality("HD");
                                                    subscriber.onNext(mediaSource2);
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e5) {
                                Logger.m6281((Throwable) e5, new boolean[0]);
                            }
                        }
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
