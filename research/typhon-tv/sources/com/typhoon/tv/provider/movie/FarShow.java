package com.typhoon.tv.provider.movie;

import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org2.jsoup.Jsoup;
import org2.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

public class FarShow extends BaseProvider {
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16294(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String str;
                TyphoonTV.ParsedLinkModel r2;
                TyphoonTV typhoonTV = new TyphoonTV();
                Iterator it2 = Jsoup.m23495(HttpHelper.m6343().m6351("http://bia2hd.ga/?s=" + Utils.m6414(mediaInfo.getName(), new boolean[0]), (Map<String, String>[]) new Map[0])).m23631("h2").iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        str = "";
                        break;
                    }
                    try {
                        Element r22 = ((Element) it2.next()).m23652("a[href]");
                        if (r22 != null) {
                            str = r22.m23760("href");
                            String replaceAll = r22.m23667().replaceAll("[^\\x00-\\x7F]", "");
                            String r3 = Regex.m17800(replaceAll, "(.*?)\\s+(\\d{4})$", 1);
                            String r4 = Regex.m17800(replaceAll, "(.*?)\\s+(\\d{4})$", 2);
                            if (r4.isEmpty()) {
                                r4 = Regex.m17800(replaceAll, "-(\\d{4})/?$", 1);
                            }
                            if (!r3.isEmpty()) {
                                replaceAll = r3;
                            }
                            if (TitleHelper.m15971(mediaInfo.getName()).equals(TitleHelper.m15971(replaceAll))) {
                                if (r4.trim().isEmpty() || !Utils.m6426(r4.trim()) || mediaInfo.getYear() <= 0 || Integer.parseInt(r4.trim()) == mediaInfo.getYear()) {
                                    break;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                if (str.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                    str = "http://bia2hd.ga/" + str;
                }
                Iterator it3 = Jsoup.m23495(HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[0])).m23631("a[href]").iterator();
                while (it3.hasNext()) {
                    Element element = (Element) it3.next();
                    if (subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                        return;
                    }
                    String r0 = element.m23760("href");
                    if (r0.contains("s19.bitdl.ir/") && (r2 = typhoonTV.齉(r0)) != null && !r2.m15984() && typhoonTV.m15981(r2, mediaInfo, -1, -1)) {
                        MediaSource mediaSource = new MediaSource(FarShow.this.m16282(r2.m15983()), "CDN-FastServer", false);
                        mediaSource.setStreamLink(r0);
                        mediaSource.setQuality(r2.m15985());
                        subscriber.onNext(mediaSource);
                    }
                }
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16293() {
        return "Bia2HD";
    }
}
