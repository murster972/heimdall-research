package com.typhoon.tv.provider.movie;

import com.mopub.common.TyphoonApp;
import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import rx.Observable;
import rx.Subscriber;

public class Movie25V2 extends BaseProvider {
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<MediaSource> m16300(final MediaInfo mediaInfo) {
        return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaSource> subscriber) {
                String str;
                boolean z;
                Element r6;
                Element r0;
                String str2 = "http://5movies.to/search.php?q=" + Utils.m6414(mediaInfo.getName() + StringUtils.SPACE + mediaInfo.getYear(), new boolean[0]);
                Iterator it2 = Jsoup.m21674(HttpHelper.m6343().m6351(str2, (Map<String, String>[]) new Map[0])).m21815("div.ml-img").iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        str = "";
                        break;
                    }
                    Element element = (Element) it2.next();
                    r6 = element.m21837("a[href]");
                    if (!(r6 == null || (r0 = element.m21837("img[alt]")) == null)) {
                        String r02 = r0.m21947("alt");
                        String r7 = Regex.m17800(r02, "(.*?)\\s+\\((\\d{4})\\)", 1);
                        String r03 = Regex.m17800(r02, "(.*?)\\s+\\((\\d{4})\\)", 2);
                        if (!r7.isEmpty() && !r03.isEmpty() && TitleHelper.m15971(r7).equals(TitleHelper.m15971(mediaInfo.getName()))) {
                            if (r03.trim().isEmpty() || !Utils.m6426(r03.trim()) || mediaInfo.getYear() <= 0 || Integer.parseInt(r03.trim()) == mediaInfo.getYear()) {
                                str = r6.m21947("href");
                            }
                        }
                    }
                }
                str = r6.m21947("href");
                if (str.isEmpty()) {
                    str = Movie25V2.this.m16296(mediaInfo);
                }
                if (str.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (str.startsWith("//")) {
                    str = "http:" + str;
                } else if (str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                    str = "http://5movies.to" + str;
                }
                String r04 = HttpHelper.m6343().m6358(str, str2);
                Document r4 = Jsoup.m21674(r04);
                String lowerCase = Regex.m17801(r04, "Links\\s*-\\s*Quality\\s*(.*?)\\s*<", 1, 34).trim().toLowerCase();
                if (lowerCase.equals("cam") || lowerCase.contains("ts")) {
                    z = true;
                } else {
                    z = false;
                }
                Iterator it3 = r4.m21815("li.link-button").iterator();
                while (it3.hasNext()) {
                    Element element2 = (Element) it3.next();
                    if (subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                        return;
                    }
                    Element r05 = element2.m21837("a[href]");
                    if (r05 != null) {
                        String r06 = r05.m21947("href");
                        if (r06.startsWith("//")) {
                            r06 = "http:" + r06;
                        } else if (r06.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                            r06 = "http://5movies.to" + r06;
                        }
                        if (r06.trim().toLowerCase().startsWith("?lk=")) {
                            String r07 = Regex.m17800(r06, "\\?lk=(.*?)$", 1);
                            String trim = HttpHelper.m6343().m6350("http://5movies.to/getlink.php?Action=get&lk=" + r07, "Action=get&lk=" + r07, new Map[0]).trim();
                            if (trim.contains("href=")) {
                                trim = Regex.m17800(trim, "href=['\"]([^'\"]+)", 1);
                            }
                            if (trim.startsWith("//")) {
                                trim = "http:" + trim;
                            } else if (trim.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                trim = "http://5movies.to" + trim;
                            }
                            Movie25V2.this.m16291(subscriber, trim, "HQ", z);
                        }
                    }
                }
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16296(MediaInfo mediaInfo) {
        MediaInfo cloneDeeply = mediaInfo.cloneDeeply();
        if (cloneDeeply.getTmdbId() == 381288) {
            cloneDeeply.setYear(2017);
        }
        String replace = "http://5movies.to".replace("https://", "http://");
        String replace2 = cloneDeeply.getName().replace("Marvel's ", "").replace("DC's ", "");
        String r1 = HttpHelper.m6343().m6358("https://www.google.ch/search?q=" + Utils.m6414(replace2, new boolean[0]).replace("%20", "+") + "+" + cloneDeeply.getYear() + "+site:" + replace, "https://www.google.ch");
        String r0 = HttpHelper.m6343().m6360("https://duckduckgo.com/html/", "kl=us-en&q=" + Utils.m6414(replace2 + StringUtils.SPACE + cloneDeeply.getYear() + " site:" + replace, new boolean[0]), true, (Map<String, String>[]) new Map[0]);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(r1);
        arrayList.add(r0);
        for (String str : arrayList) {
            ArrayList<ArrayList<String>> r12 = Regex.m17805(str, "<a[^>]+href=['\"]([^'\"]+)['\"][^>]*>(.+?)</a>", 2, !str.contains("DuckDuckGo (HTML)"));
            ArrayList arrayList2 = r12.get(0);
            ArrayList arrayList3 = r12.get(1);
            int i = 0;
            while (true) {
                if (i < arrayList2.size()) {
                    try {
                        String str2 = (String) arrayList2.get(i);
                        String replaceAll = ((String) arrayList3.get(i)).replaceAll("\\<[uibp]\\>", "").replaceAll("\\</[uibp]\\>", "");
                        if (str2.startsWith(TyphoonApp.HTTP) && str2.contains("5movies") && !str2.contains("//translate.") && str2.replace("https://", "http://").contains(replace)) {
                            String r8 = Regex.m17802(replaceAll, "(?:^Watch |)(.+?)\\s+\\((\\d{4})\\)", 1, true);
                            String trim = Regex.m17802(replaceAll, "(?:^Watch |)(.+?)\\s+\\((\\d{4})\\)", 2, true).trim();
                            if (!r8.isEmpty() && !trim.isEmpty()) {
                                boolean z = Utils.m6426(trim) && (trim.equals(String.valueOf(cloneDeeply.getYear())) || trim.equals(String.valueOf(cloneDeeply.getYear() + 1)) || trim.equals(String.valueOf(cloneDeeply.getYear() + -1)));
                                if (TitleHelper.m15971(cloneDeeply.getName()).equals(TitleHelper.m15971(r8)) && z) {
                                    try {
                                        return URLDecoder.decode(str2, "UTF-8");
                                    } catch (Exception e) {
                                        String decode = URLDecoder.decode(str2);
                                        Logger.m6281((Throwable) e, new boolean[0]);
                                        return decode;
                                    }
                                }
                            }
                        }
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                    i++;
                }
            }
        }
        return "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16299() {
        return "Movie25V2";
    }
}
