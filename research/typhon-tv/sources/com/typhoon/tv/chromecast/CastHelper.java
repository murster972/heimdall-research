package com.typhoon.tv.chromecast;

import android.graphics.Color;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.TextTrackStyle;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.utils.NetworkUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class CastHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    private static MediaInfo.Builder m15831(MediaMetadata mediaMetadata, MediaSource mediaSource) {
        int parseColor;
        int parseColor2;
        float f = TVApplication.m6285().getFloat("pref_cc_subs_font_scale", 1.05f);
        try {
            parseColor = Color.parseColor(TVApplication.m6285().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            parseColor = Color.parseColor("#FFFFFFFF");
            TVApplication.m6285().edit().putString("pref_cc_subs_font_color", "#FFFFFFFF").apply();
        }
        try {
            parseColor2 = Color.parseColor(TVApplication.m6285().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            parseColor2 = Color.parseColor("#00FFFFFF");
            TVApplication.m6285().edit().putString("pref_cc_subs_bg_color", "#00FFFFFF").apply();
        }
        TextTrackStyle textTrackStyle = new TextTrackStyle();
        textTrackStyle.m7960(parseColor2);
        textTrackStyle.m7967(parseColor);
        textTrackStyle.m7964(1);
        textTrackStyle.m7962(Color.parseColor("#FF000000"));
        textTrackStyle.m7954(1);
        textTrackStyle.m7966(f);
        textTrackStyle.m7968("Droid Sans");
        textTrackStyle.m7951(0);
        textTrackStyle.m7947(Color.parseColor("#00AA00FF"));
        textTrackStyle.m7949(10);
        textTrackStyle.m7958(0);
        return new MediaInfo.Builder(mediaSource.getStreamLink()).m7855(1).m7858(mediaSource.isHLS() ? MimeTypes.APPLICATION_M3U8 : MimeTypes.VIDEO_MP4).m7856(mediaMetadata).m7857(textTrackStyle);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaInfo m15832(MediaMetadata mediaMetadata, MediaSource mediaSource) {
        return m15831(mediaMetadata, mediaSource).m7860();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaInfo m15833(MediaMetadata mediaMetadata, MediaSource mediaSource, List<String> list, List<String> list2) {
        String r4 = NetworkUtils.m17798();
        if (r4.isEmpty()) {
            r4 = "127.0.0.1";
        }
        ArrayList arrayList = new ArrayList();
        int i = 1;
        for (int i2 = 0; i2 < list.size(); i2++) {
            try {
                arrayList.add(new MediaTrack.Builder((long) i, 1).m7939("en-US").m7942("http://" + r4 + ":" + 59104 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + list2.get(i2)).m7941(1).m7940(new File(list.get(i2)).getName()).m7938(MimeTypes.APPLICATION_TTML).m7943());
                i++;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, true);
            }
        }
        return m15831(mediaMetadata, mediaSource).m7859((List<MediaTrack>) arrayList).m7860();
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0086, code lost:
        if (r3.equals("4k") != false) goto L_0x004f;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.cast.MediaMetadata m15834(com.typhoon.tv.model.media.MediaInfo r11, int r12, int r13, com.typhoon.tv.model.media.MediaSource r14) {
        /*
            r8 = 2
            r6 = 0
            r9 = -1
            r5 = 1
            int r7 = r11.getType()
            if (r7 != r5) goto L_0x007b
            r1 = r5
        L_0x000b:
            com.google.android.gms.cast.MediaMetadata r2 = new com.google.android.gms.cast.MediaMetadata
            if (r1 == 0) goto L_0x007d
            r7 = r5
        L_0x0010:
            r2.<init>(r7)
            java.lang.String r7 = "com.google.android.gms.cast.metadata.TITLE"
            java.lang.String r10 = r11.getNameAndYear()
            r2.m7883((java.lang.String) r7, (java.lang.String) r10)
            com.google.android.gms.common.images.WebImage r7 = new com.google.android.gms.common.images.WebImage
            java.lang.String r10 = r11.getPosterUrl()
            android.net.Uri r10 = android.net.Uri.parse(r10)
            r7.<init>((android.net.Uri) r10)
            r2.m7881((com.google.android.gms.common.images.WebImage) r7)
            com.google.android.gms.common.images.WebImage r7 = new com.google.android.gms.common.images.WebImage
            java.lang.String r10 = r11.getBannerUrl()
            android.net.Uri r10 = android.net.Uri.parse(r10)
            r7.<init>((android.net.Uri) r10)
            r2.m7881((com.google.android.gms.common.images.WebImage) r7)
            r0 = -1
            r4 = -1
            java.lang.String r7 = r14.getQuality()
            java.lang.String r3 = r7.toLowerCase()
            int r7 = r3.hashCode()
            switch(r7) {
                case -948832125: goto L_0x0089;
                case 1657: goto L_0x0094;
                case 1719: goto L_0x007f;
                case 46737913: goto L_0x009f;
                default: goto L_0x004e;
            }
        L_0x004e:
            r6 = r9
        L_0x004f:
            switch(r6) {
                case 0: goto L_0x00aa;
                case 1: goto L_0x00af;
                case 2: goto L_0x00af;
                case 3: goto L_0x00b4;
                default: goto L_0x0052;
            }
        L_0x0052:
            if (r0 <= r9) goto L_0x005a
            java.lang.String r5 = "com.google.android.gms.cast.metadata.HEIGHT"
            r2.m7882((java.lang.String) r5, (int) r0)
        L_0x005a:
            if (r4 <= r9) goto L_0x0062
            java.lang.String r5 = "com.google.android.gms.cast.metadata.WIDTH"
            r2.m7882((java.lang.String) r5, (int) r4)
        L_0x0062:
            if (r1 != 0) goto L_0x007a
            java.lang.String r5 = "com.google.android.gms.cast.metadata.SERIES_TITLE"
            java.lang.String r6 = r11.getNameAndYear()
            r2.m7883((java.lang.String) r5, (java.lang.String) r6)
            java.lang.String r5 = "com.google.android.gms.cast.metadata.SEASON_NUMBER"
            r2.m7882((java.lang.String) r5, (int) r12)
            java.lang.String r5 = "com.google.android.gms.cast.metadata.EPISODE_NUMBER"
            r2.m7882((java.lang.String) r5, (int) r13)
        L_0x007a:
            return r2
        L_0x007b:
            r1 = r6
            goto L_0x000b
        L_0x007d:
            r7 = r8
            goto L_0x0010
        L_0x007f:
            java.lang.String r5 = "4k"
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x004e
            goto L_0x004f
        L_0x0089:
            java.lang.String r6 = "quadhd"
            boolean r6 = r3.equals(r6)
            if (r6 == 0) goto L_0x004e
            r6 = r5
            goto L_0x004f
        L_0x0094:
            java.lang.String r5 = "2k"
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x004e
            r6 = r8
            goto L_0x004f
        L_0x009f:
            java.lang.String r5 = "1080p"
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x004e
            r6 = 3
            goto L_0x004f
        L_0x00aa:
            r0 = 2160(0x870, float:3.027E-42)
            r4 = 3840(0xf00, float:5.381E-42)
            goto L_0x0052
        L_0x00af:
            r0 = 1440(0x5a0, float:2.018E-42)
            r4 = 2560(0xa00, float:3.587E-42)
            goto L_0x0052
        L_0x00b4:
            r0 = 1080(0x438, float:1.513E-42)
            r4 = 1920(0x780, float:2.69E-42)
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.chromecast.CastHelper.m15834(com.typhoon.tv.model.media.MediaInfo, int, int, com.typhoon.tv.model.media.MediaSource):com.google.android.gms.cast.MediaMetadata");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r2 = r3.m8075().m8022();
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m15835(android.content.Context r5, final long[] r6, long[] r7) {
        /*
            if (r6 == 0) goto L_0x0030
            if (r7 == 0) goto L_0x0030
            int r4 = r6.length
            if (r4 <= 0) goto L_0x0030
            int r4 = r7.length
            if (r4 <= 0) goto L_0x0030
            com.google.android.gms.cast.framework.CastContext r4 = com.google.android.gms.cast.framework.CastContext.m7977((android.content.Context) r5)     // Catch:{ Exception -> 0x0031 }
            com.google.android.gms.cast.framework.SessionManager r3 = r4.m7981()     // Catch:{ Exception -> 0x0031 }
            if (r3 == 0) goto L_0x0030
            com.google.android.gms.cast.framework.CastSession r4 = r3.m8075()     // Catch:{ Exception -> 0x0031 }
            if (r4 == 0) goto L_0x0030
            com.google.android.gms.cast.framework.CastSession r4 = r3.m8075()     // Catch:{ Exception -> 0x0031 }
            com.google.android.gms.cast.framework.media.RemoteMediaClient r2 = r4.m8022()     // Catch:{ Exception -> 0x0031 }
            if (r2 == 0) goto L_0x0030
            com.google.android.gms.common.api.PendingResult r0 = r2.m4162((long[]) r7)     // Catch:{ Exception -> 0x0031 }
            com.typhoon.tv.chromecast.CastHelper$1 r4 = new com.typhoon.tv.chromecast.CastHelper$1     // Catch:{ Exception -> 0x0031 }
            r4.<init>(r2, r6)     // Catch:{ Exception -> 0x0031 }
            r0.m8535(r4)     // Catch:{ Exception -> 0x0031 }
        L_0x0030:
            return
        L_0x0031:
            r1 = move-exception
            r4 = 0
            boolean[] r4 = new boolean[r4]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r4)
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.chromecast.CastHelper.m15835(android.content.Context, long[], long[]):void");
    }
}
