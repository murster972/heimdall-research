package com.typhoon.tv.chromecast;

import android.content.Context;
import android.content.res.Resources;
import android.util.Base64;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.OptionsProvider;
import com.google.android.gms.cast.framework.SessionProvider;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.ImagePicker;
import com.google.android.gms.cast.framework.media.MediaIntentReceiver;
import com.google.android.gms.cast.framework.media.NotificationOptions;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.ui.activity.ExpandedControlsActivity;
import com.typhoon.tv.utils.LocaleUtils;
import java.util.ArrayList;
import java.util.List;

public class CastOptionsProvider implements OptionsProvider {
    private String decodeAppId() {
        try {
            return new String(Base64.decode("NTA1QkFFQUM=", 0), "UTF-8");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                return new String(Base64.decode("NTA1QkFFQUM=", 0));
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                return "NTA1QkFFQUM=";
            }
        }
    }

    public List<SessionProvider> getAdditionalSessionProviders(Context context) {
        return null;
    }

    public CastOptions getCastOptions(Context context) {
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.google.android.gms.cast.framework.action.REWIND");
        arrayList.add("com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK");
        arrayList.add("com.google.android.gms.cast.framework.action.FORWARD");
        arrayList.add("com.google.android.gms.cast.framework.action.STOP_CASTING");
        CastMediaOptions r5 = new CastMediaOptions.Builder().m8141(new NotificationOptions.Builder().m8187(arrayList, new int[]{1, 3}).m8185(30000).m8186(ExpandedControlsActivity.class.getName()).m8188()).m8139(ExpandedControlsActivity.class.getName()).m8142(MediaIntentReceiver.class.getName()).m8140(new ImagePicker()).m8143();
        String trim = TVApplication.m6285().getString("pref_app_lang", "").trim();
        return new CastOptions.Builder().m7998(decodeAppId()).m7997(r5).m7996(new LaunchOptions.Builder().m7834(!trim.isEmpty() ? LocaleUtils.m17792(trim) : Resources.getSystem().getConfiguration().locale).m7835()).m7995(0.1d).m7994(true).m7999(true).m8000();
    }
}
