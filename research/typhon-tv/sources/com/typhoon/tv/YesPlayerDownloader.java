package com.typhoon.tv;

import android.os.Environment;
import rx.Observable;

public class YesPlayerDownloader {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String f12500 = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/Typhoon/YesPlayerAPK");

    /* renamed from: 齉  reason: contains not printable characters */
    public static final String f12501 = (f12500 + "/yesplayer.apk");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f12502 = (TyphoonApp.f5907 + "/yesplayer/yesplayer.apk");

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<Integer> m15721() {
        return Observable.m7359(new Observable.OnSubscribe<Integer>() {
            /* JADX WARNING: Removed duplicated region for block: B:112:0x0237 A[Catch:{ Exception -> 0x00dd }] */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00b3 A[SYNTHETIC, Splitter:B:20:0x00b3] */
            /* JADX WARNING: Removed duplicated region for block: B:71:0x0154 A[SYNTHETIC, Splitter:B:71:0x0154] */
            /* JADX WARNING: Removed duplicated region for block: B:74:0x0159 A[SYNTHETIC, Splitter:B:74:0x0159] */
            /* JADX WARNING: Removed duplicated region for block: B:77:0x015e A[SYNTHETIC, Splitter:B:77:0x015e] */
            /* JADX WARNING: Removed duplicated region for block: B:80:0x0163 A[SYNTHETIC, Splitter:B:80:0x0163] */
            /* JADX WARNING: Removed duplicated region for block: B:83:0x0168 A[SYNTHETIC, Splitter:B:83:0x0168] */
            /* renamed from: 龘  reason: contains not printable characters */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void call(rx.Subscriber<? super java.lang.Integer> r25) {
                /*
                    r24 = this;
                    java.io.File r13 = new java.io.File     // Catch:{ Exception -> 0x00dd }
                    java.lang.String r20 = com.typhoon.tv.YesPlayerDownloader.f12500     // Catch:{ Exception -> 0x00dd }
                    r0 = r20
                    r13.<init>(r0)     // Catch:{ Exception -> 0x00dd }
                    boolean r20 = r13.exists()     // Catch:{ Exception -> 0x00dd }
                    if (r20 != 0) goto L_0x00cc
                    r13.mkdirs()     // Catch:{ Exception -> 0x00dd }
                L_0x0012:
                    java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x00dd }
                    java.lang.String r20 = com.typhoon.tv.YesPlayerDownloader.f12501     // Catch:{ Exception -> 0x00dd }
                    r0 = r20
                    r12.<init>(r0)     // Catch:{ Exception -> 0x00dd }
                    boolean r20 = r12.exists()     // Catch:{ Exception -> 0x00dd }
                    if (r20 == 0) goto L_0x0024
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r12)     // Catch:{ Exception -> 0x00dd }
                L_0x0024:
                    com.typhoon.tv.helper.http.HttpHelper r20 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Exception -> 0x00dd }
                    java.lang.String r21 = com.typhoon.tv.YesPlayerDownloader.f12502     // Catch:{ Exception -> 0x00dd }
                    r22 = 0
                    r23 = 0
                    r0 = r23
                    java.util.Map[] r0 = new java.util.Map[r0]     // Catch:{ Exception -> 0x00dd }
                    r23 = r0
                    java.lang.String r9 = r20.m6363((java.lang.String) r21, (boolean) r22, (java.util.Map<java.lang.String, java.lang.String>[]) r23)     // Catch:{ Exception -> 0x00dd }
                    okhttp3.Request$Builder r20 = new okhttp3.Request$Builder     // Catch:{ Exception -> 0x00dd }
                    r20.<init>()     // Catch:{ Exception -> 0x00dd }
                    okhttp3.Request$Builder r20 = r20.m19990()     // Catch:{ Exception -> 0x00dd }
                    r0 = r20
                    okhttp3.Request$Builder r20 = r0.m19992((java.lang.String) r9)     // Catch:{ Exception -> 0x00dd }
                    java.lang.String r21 = "Accept-Language"
                    java.lang.String r22 = "en-US,en;q=0.9"
                    okhttp3.Request$Builder r20 = r20.m19993((java.lang.String) r21, (java.lang.String) r22)     // Catch:{ Exception -> 0x00dd }
                    java.lang.String r21 = "User-Agent"
                    java.lang.String r22 = com.typhoon.tv.TyphoonApp.f5835     // Catch:{ Exception -> 0x00dd }
                    okhttp3.Request$Builder r20 = r20.m19993((java.lang.String) r21, (java.lang.String) r22)     // Catch:{ Exception -> 0x00dd }
                    okhttp3.CacheControl r21 = okhttp3.CacheControl.f6219     // Catch:{ Exception -> 0x00dd }
                    okhttp3.Request$Builder r20 = r20.m19995((okhttp3.CacheControl) r21)     // Catch:{ Exception -> 0x00dd }
                    java.lang.String r21 = "downloadYesPlayer"
                    okhttp3.Request$Builder r20 = r20.m19991((java.lang.Object) r21)     // Catch:{ Exception -> 0x00dd }
                    okhttp3.Request r15 = r20.m19989()     // Catch:{ Exception -> 0x00dd }
                    com.typhoon.tv.helper.http.HttpHelper r20 = com.typhoon.tv.helper.http.HttpHelper.m6343()     // Catch:{ Exception -> 0x00dd }
                    r21 = 0
                    r0 = r21
                    int[] r0 = new int[r0]     // Catch:{ Exception -> 0x00dd }
                    r21 = r0
                    r0 = r20
                    r1 = r21
                    okhttp3.Response r16 = r0.m6368((okhttp3.Request) r15, (int[]) r1)     // Catch:{ Exception -> 0x00dd }
                    okhttp3.ResponseBody r17 = r16.m7056()     // Catch:{ Exception -> 0x00dd }
                    r8 = 0
                    r10 = 0
                    r7 = 0
                    java.io.InputStream r8 = r17.m7094()     // Catch:{ Exception -> 0x023f }
                    java.io.FileOutputStream r11 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x023f }
                    r11.<init>(r12)     // Catch:{ Exception -> 0x023f }
                    long r2 = r17.m7093()     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    r20 = 0
                    int r20 = (r2 > r20 ? 1 : (r2 == r20 ? 0 : -1))
                    if (r20 <= 0) goto L_0x0147
                    r20 = 1024(0x400, float:1.435E-42)
                    r0 = r20
                    byte[] r5 = new byte[r0]     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    r18 = 0
                L_0x00a1:
                    int r4 = r8.read(r5)     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    r20 = -1
                    r0 = r20
                    if (r4 == r0) goto L_0x00b1
                    boolean r20 = r25.isUnsubscribed()     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    if (r20 == 0) goto L_0x00f7
                L_0x00b1:
                    if (r17 == 0) goto L_0x00b6
                    r17.close()     // Catch:{ Exception -> 0x016c }
                L_0x00b6:
                    if (r11 == 0) goto L_0x00bb
                    r11.flush()     // Catch:{ Exception -> 0x017c }
                L_0x00bb:
                    if (r11 == 0) goto L_0x00c0
                    r11.close()     // Catch:{ Exception -> 0x018c }
                L_0x00c0:
                    if (r8 == 0) goto L_0x00c5
                    r8.close()     // Catch:{ Exception -> 0x019c }
                L_0x00c5:
                    if (r7 == 0) goto L_0x01ac
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r12)     // Catch:{ Exception -> 0x00dd }
                    r10 = r11
                L_0x00cb:
                    return
                L_0x00cc:
                    boolean r20 = r13.isDirectory()     // Catch:{ Exception -> 0x00dd }
                    if (r20 != 0) goto L_0x0012
                    boolean r20 = r13.delete()     // Catch:{ Exception -> 0x00dd }
                    if (r20 == 0) goto L_0x0012
                    r13.mkdirs()     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0012
                L_0x00dd:
                    r6 = move-exception
                    r20 = 1
                    r0 = r20
                    boolean[] r0 = new boolean[r0]
                    r20 = r0
                    r21 = 0
                    r22 = 1
                    r20[r21] = r22
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)
                    r0 = r25
                    r0.onError(r6)
                    goto L_0x00cb
                L_0x00f7:
                    long r0 = (long) r4
                    r20 = r0
                    long r18 = r18 + r20
                    r20 = 0
                    r0 = r20
                    r11.write(r5, r0, r4)     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    r20 = 100
                    long r20 = r20 * r18
                    long r20 = r20 / r2
                    r0 = r20
                    int r14 = (int) r0     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    java.lang.Integer r20 = java.lang.Integer.valueOf(r14)     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    r0 = r25
                    r1 = r20
                    r0.onNext(r1)     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    goto L_0x00a1
                L_0x0118:
                    r6 = move-exception
                    r10 = r11
                L_0x011a:
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ all -> 0x023c }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ all -> 0x023c }
                    r0 = r25
                    r0.onError(r6)     // Catch:{ all -> 0x023c }
                    r7 = 1
                    if (r17 == 0) goto L_0x0132
                    r17.close()     // Catch:{ Exception -> 0x01b2 }
                L_0x0132:
                    if (r10 == 0) goto L_0x0137
                    r10.flush()     // Catch:{ Exception -> 0x01c2 }
                L_0x0137:
                    if (r10 == 0) goto L_0x013c
                    r10.close()     // Catch:{ Exception -> 0x01d2 }
                L_0x013c:
                    if (r8 == 0) goto L_0x0141
                    r8.close()     // Catch:{ Exception -> 0x01e2 }
                L_0x0141:
                    if (r7 == 0) goto L_0x01f2
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r12)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x00cb
                L_0x0147:
                    java.lang.RuntimeException r20 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    java.lang.String r21 = "Content-Length is or less than 0"
                    r20.<init>(r21)     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                    throw r20     // Catch:{ Exception -> 0x0118, all -> 0x0150 }
                L_0x0150:
                    r20 = move-exception
                    r10 = r11
                L_0x0152:
                    if (r17 == 0) goto L_0x0157
                    r17.close()     // Catch:{ Exception -> 0x01f7 }
                L_0x0157:
                    if (r10 == 0) goto L_0x015c
                    r10.flush()     // Catch:{ Exception -> 0x0207 }
                L_0x015c:
                    if (r10 == 0) goto L_0x0161
                    r10.close()     // Catch:{ Exception -> 0x0217 }
                L_0x0161:
                    if (r8 == 0) goto L_0x0166
                    r8.close()     // Catch:{ Exception -> 0x0227 }
                L_0x0166:
                    if (r7 == 0) goto L_0x0237
                    com.typhoon.tv.utils.Utils.m6421((java.io.File) r12)     // Catch:{ Exception -> 0x00dd }
                L_0x016b:
                    throw r20     // Catch:{ Exception -> 0x00dd }
                L_0x016c:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x00b6
                L_0x017c:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x00bb
                L_0x018c:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x00c0
                L_0x019c:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x00c5
                L_0x01ac:
                    r25.onCompleted()     // Catch:{ Exception -> 0x00dd }
                    r10 = r11
                    goto L_0x00cb
                L_0x01b2:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0132
                L_0x01c2:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0137
                L_0x01d2:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x013c
                L_0x01e2:
                    r6 = move-exception
                    r20 = 0
                    r0 = r20
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r20 = r0
                    r0 = r20
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0141
                L_0x01f2:
                    r25.onCompleted()     // Catch:{ Exception -> 0x00dd }
                    goto L_0x00cb
                L_0x01f7:
                    r6 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0157
                L_0x0207:
                    r6 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x015c
                L_0x0217:
                    r6 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0161
                L_0x0227:
                    r6 = move-exception
                    r21 = 0
                    r0 = r21
                    boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x00dd }
                    r21 = r0
                    r0 = r21
                    com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)     // Catch:{ Exception -> 0x00dd }
                    goto L_0x0166
                L_0x0237:
                    r25.onCompleted()     // Catch:{ Exception -> 0x00dd }
                    goto L_0x016b
                L_0x023c:
                    r20 = move-exception
                    goto L_0x0152
                L_0x023f:
                    r6 = move-exception
                    goto L_0x011a
                */
                throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.YesPlayerDownloader.AnonymousClass1.call(rx.Subscriber):void");
            }
        });
    }
}
