package com.typhoon.tv;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.typhoon.tv";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 112;
    public static final String VERSION_NAME = "3.0.21";
}
