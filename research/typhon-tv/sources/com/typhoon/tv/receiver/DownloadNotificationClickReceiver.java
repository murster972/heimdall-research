package com.typhoon.tv.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.typhoon.tv.TVApplication;

public class DownloadNotificationClickReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (action.equals("android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED") || action.equals("android.intent.action.VIEW_DOWNLOADS")) {
                String packageName = TVApplication.m6288().getPackageName();
                Intent intent2 = new Intent();
                intent2.setClassName(packageName, packageName + ".ui.activity.DownloadsActivity");
                intent2.setFlags(268435456);
                context.startActivity(intent2);
            }
        }
    }
}
