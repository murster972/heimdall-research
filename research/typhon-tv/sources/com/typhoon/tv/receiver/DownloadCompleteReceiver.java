package com.typhoon.tv.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.event.DownloadCompleteEvent;

public class DownloadCompleteReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction().equals("android.intent.action.DOWNLOAD_COMPLETE")) {
            RxBus.m15709().m15711(new DownloadCompleteEvent());
        }
    }
}
