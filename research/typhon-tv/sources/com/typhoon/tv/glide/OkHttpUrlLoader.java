package com.typhoon.tv.glide;

import android.content.Context;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import java.io.InputStream;

public class OkHttpUrlLoader implements ModelLoader<GlideUrl, InputStream> {

    public static class Factory implements ModelLoaderFactory<GlideUrl, InputStream> {
        /* renamed from: 龘  reason: contains not printable characters */
        public ModelLoader<GlideUrl, InputStream> m15928(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new OkHttpUrlLoader();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m15929() {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DataFetcher<InputStream> m15927(GlideUrl glideUrl, int i, int i2) {
        return new OkHttpStreamFetcher(glideUrl);
    }
}
