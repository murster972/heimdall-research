package com.typhoon.tv.glide;

import android.content.Context;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;
import com.typhoon.tv.glide.OkHttpUrlLoader;
import java.io.InputStream;

public class OkHttpGlideModule implements GlideModule {
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15919(Context context, Glide glide) {
        glide.m3956(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15920(Context context, GlideBuilder glideBuilder) {
    }
}
