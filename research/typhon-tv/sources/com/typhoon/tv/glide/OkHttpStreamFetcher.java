package com.typhoon.tv.glide;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.util.ContentLengthInputStream;
import com.typhoon.tv.helper.http.HttpHelper;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkHttpStreamFetcher implements DataFetcher<InputStream> {

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile Call f12616;

    /* renamed from: 靐  reason: contains not printable characters */
    private final GlideUrl f12617;

    /* renamed from: 麤  reason: contains not printable characters */
    private ResponseBody f12618;

    /* renamed from: 齉  reason: contains not printable characters */
    private InputStream f12619;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Call.Factory f12620 = HttpHelper.m6343().m6348();

    public OkHttpStreamFetcher(GlideUrl glideUrl) {
        this.f12617 = glideUrl;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public InputStream m15924(Priority priority) throws Exception {
        Request.Builder r5 = new Request.Builder().m19992(this.f12617.m25635());
        for (Map.Entry next : this.f12617.m25637().entrySet()) {
            r5.m19988((String) next.getKey(), (String) next.getValue());
        }
        this.f12616 = this.f12620.m19888(r5.m19989());
        Response r6 = this.f12616.m19883();
        this.f12618 = r6.m7056();
        if (!r6.m7065()) {
            throw new IOException("Request failed with code: " + r6.m7066());
        }
        this.f12619 = ContentLengthInputStream.m26171(this.f12618.m7094(), this.f12618.m7093());
        return this.f12619;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m15922() {
        return this.f12617.m25636();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m15923() {
        Call call = this.f12616;
        if (call != null) {
            call.m19885();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15925() {
        try {
            if (this.f12619 != null) {
                this.f12619.close();
            }
        } catch (IOException e) {
        }
        if (this.f12618 != null) {
            this.f12618.close();
        }
    }
}
