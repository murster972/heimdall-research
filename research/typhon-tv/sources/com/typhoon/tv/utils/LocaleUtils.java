package com.typhoon.tv.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import java.util.Locale;

public class LocaleUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Locale m17792(String str) {
        if (!str.contains("-")) {
            return new Locale(str);
        }
        String[] split = str.split("-");
        return split.length > 1 ? new Locale(split[0], split[1].toUpperCase()) : new Locale(split[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m17793(Context context, Locale locale) {
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
    }
}
