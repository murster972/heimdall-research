package com.typhoon.tv.utils;

import com.adincube.sdk.NativeAd;
import com.typhoon.tv.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NativeAdsUtils {
    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m17794(String str) {
        if (str.equals("pubnative")) {
            return 9;
        }
        if (str.equals("avocarrot")) {
            return 8;
        }
        if (str.equals("appnext")) {
            return 7;
        }
        if (str.equals("mobvista")) {
            return 6;
        }
        if (str.equals("applovin")) {
            return 5;
        }
        return str.equals("startapp") ? 4 : 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<NativeAd> m17796(List<NativeAd> list) {
        if (list == null || list.size() <= 1) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            NativeAd nativeAd = list.get(i);
            if (nativeAd != null) {
                arrayList.add(nativeAd);
            }
        }
        if (arrayList.size() <= 1) {
            return arrayList;
        }
        Collections.sort(arrayList, new Comparator<NativeAd>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public int compare(NativeAd nativeAd, NativeAd nativeAd2) {
                String lowerCase = nativeAd.m2357() != null ? nativeAd.m2357().trim().toLowerCase() : "";
                String lowerCase2 = nativeAd2.m2357() != null ? nativeAd2.m2357().trim().toLowerCase() : "";
                if (lowerCase.equals(lowerCase2)) {
                    return 0;
                }
                String str = "";
                String str2 = "";
                String str3 = "";
                String str4 = "";
                if (lowerCase.equals("rtb")) {
                    try {
                        str = nativeAd.m2362().toLowerCase();
                    } catch (Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                    }
                    try {
                        str2 = nativeAd.m2361().toLowerCase();
                    } catch (Throwable th2) {
                        Logger.m6281(th2, new boolean[0]);
                    }
                }
                if (lowerCase2.equals("rtb")) {
                    try {
                        str3 = nativeAd2.m2362().toLowerCase();
                    } catch (Throwable th3) {
                        Logger.m6281(th3, new boolean[0]);
                    }
                    try {
                        str4 = nativeAd2.m2361().toLowerCase();
                    } catch (Throwable th4) {
                        Logger.m6281(th4, new boolean[0]);
                    }
                }
                boolean z = (str.contains("configuring") && str.contains("app")) || (str2.contains("configuring") && str2.contains("app"));
                boolean z2 = (str3.contains("configuring") && str3.contains("app")) || (str4.contains("configuring") && str4.contains("app"));
                if (!z && lowerCase.equals("rtb")) {
                    return -1;
                }
                if (z2 || !lowerCase2.equals("rtb")) {
                    return Utils.m6411(NativeAdsUtils.m17794(lowerCase2), NativeAdsUtils.m17794(lowerCase));
                }
                return 1;
            }
        });
        return arrayList;
    }
}
