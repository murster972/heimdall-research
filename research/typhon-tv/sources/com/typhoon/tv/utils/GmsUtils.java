package com.typhoon.tv.utils;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.GoogleApiAvailability;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;

public class GmsUtils {
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0018 A[Catch:{ Throwable -> 0x0036 }] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m6390() {
        /*
            r4 = 0
            android.content.Context r5 = com.typhoon.tv.TVApplication.m6288()     // Catch:{ Throwable -> 0x0036 }
            android.content.pm.PackageManager r2 = r5.getPackageManager()     // Catch:{ Throwable -> 0x0036 }
            r5 = 0
            java.util.List r3 = r2.getInstalledPackages(r5)     // Catch:{ Throwable -> 0x0036 }
            java.util.Iterator r5 = r3.iterator()     // Catch:{ Throwable -> 0x0036 }
        L_0x0012:
            boolean r6 = r5.hasNext()     // Catch:{ Throwable -> 0x0036 }
            if (r6 == 0) goto L_0x0035
            java.lang.Object r1 = r5.next()     // Catch:{ Throwable -> 0x0036 }
            android.content.pm.PackageInfo r1 = (android.content.pm.PackageInfo) r1     // Catch:{ Throwable -> 0x0036 }
            java.lang.String r6 = r1.packageName     // Catch:{ Throwable -> 0x0036 }
            java.lang.String r7 = "com.google.market"
            boolean r6 = r6.equalsIgnoreCase(r7)     // Catch:{ Throwable -> 0x0036 }
            if (r6 != 0) goto L_0x0034
            java.lang.String r6 = r1.packageName     // Catch:{ Throwable -> 0x0036 }
            java.lang.String r7 = "com.android.vending"
            boolean r6 = r6.equalsIgnoreCase(r7)     // Catch:{ Throwable -> 0x0036 }
            if (r6 == 0) goto L_0x0012
        L_0x0034:
            r4 = 1
        L_0x0035:
            return r4
        L_0x0036:
            r0 = move-exception
            boolean[] r5 = new boolean[r4]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r5)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.utils.GmsUtils.m6390():boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6391(Activity activity) {
        if (DeviceUtils.m6388()) {
            return false;
        }
        try {
            GoogleApiAvailability r1 = GoogleApiAvailability.m8486();
            if (r1 == null) {
                return false;
            }
            int r2 = r1.m8494((Context) activity);
            if (!r1.m8500(r2)) {
                return false;
            }
            r1.m8495(activity, r2, 2404).show();
            return true;
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6392(Context context) {
        if (DeviceUtils.m6388() || !TyphoonApp.f5853) {
            return false;
        }
        try {
            GoogleApiAvailability r1 = GoogleApiAvailability.m8486();
            return r1 != null && r1.m8494(context) == 0;
        } catch (Throwable th) {
            return false;
        }
    }
}
