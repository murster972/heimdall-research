package com.typhoon.tv.utils;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.Iterator;

public class NetworkUtils {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m17798() {
        int ipAddress;
        try {
            WifiInfo connectionInfo = ((WifiManager) TVApplication.m6288().getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo != null && (ipAddress = connectionInfo.getIpAddress()) > 0) {
                if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                    ipAddress = Integer.reverseBytes(ipAddress);
                }
                return InetAddress.getByAddress(BigInteger.valueOf((long) ipAddress).toByteArray()).getHostAddress();
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        try {
            for (T inetAddresses : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                Iterator<T> it2 = Collections.list(inetAddresses.getInetAddresses()).iterator();
                while (true) {
                    if (it2.hasNext()) {
                        InetAddress inetAddress = (InetAddress) it2.next();
                        if (!inetAddress.isLoopbackAddress()) {
                            String hostAddress = inetAddress.getHostAddress();
                            if (hostAddress.indexOf(58) < 0) {
                                return hostAddress;
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        return "";
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.net.ConnectivityManager} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m17799() {
        /*
            r8 = 0
            r7 = 1
            r1 = 0
            android.content.Context r6 = com.typhoon.tv.TVApplication.m6288()     // Catch:{ Exception -> 0x0016 }
            java.lang.String r9 = "connectivity"
            java.lang.Object r6 = r6.getSystemService(r9)     // Catch:{ Exception -> 0x0016 }
            r0 = r6
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x0016 }
            r1 = r0
        L_0x0012:
            if (r1 != 0) goto L_0x001f
            r6 = r7
        L_0x0015:
            return r6
        L_0x0016:
            r2 = move-exception
            boolean[] r6 = new boolean[r7]
            r6[r8] = r7
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
            goto L_0x0012
        L_0x001f:
            r6 = 1
            android.net.NetworkInfo r5 = r1.getNetworkInfo(r6)     // Catch:{ Exception -> 0x002e }
            if (r5 == 0) goto L_0x0036
            boolean r6 = r5.isConnected()     // Catch:{ Exception -> 0x002e }
            if (r6 == 0) goto L_0x0036
            r6 = r7
            goto L_0x0015
        L_0x002e:
            r2 = move-exception
            boolean[] r6 = new boolean[r7]
            r6[r8] = r7
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
        L_0x0036:
            r6 = 0
            android.net.NetworkInfo r3 = r1.getNetworkInfo(r6)     // Catch:{ Exception -> 0x0045 }
            if (r3 == 0) goto L_0x004d
            boolean r6 = r3.isConnected()     // Catch:{ Exception -> 0x0045 }
            if (r6 == 0) goto L_0x004d
            r6 = r7
            goto L_0x0015
        L_0x0045:
            r2 = move-exception
            boolean[] r6 = new boolean[r7]
            r6[r8] = r7
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
        L_0x004d:
            android.net.NetworkInfo r4 = r1.getActiveNetworkInfo()     // Catch:{ Exception -> 0x005b }
            if (r4 == 0) goto L_0x0063
            boolean r6 = r4.isConnected()     // Catch:{ Exception -> 0x005b }
            if (r6 == 0) goto L_0x0063
            r6 = r7
            goto L_0x0015
        L_0x005b:
            r2 = move-exception
            boolean[] r6 = new boolean[r7]
            r6[r8] = r7
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
        L_0x0063:
            r6 = r8
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.utils.NetworkUtils.m17799():boolean");
    }
}
