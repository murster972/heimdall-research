package com.typhoon.tv.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import com.facebook.device.yearclass.YearClass;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;
import org.apache.commons.lang3.StringUtils;

public class Utils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f5938 = Pattern.compile("[-+]?\\d+");

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m6393() {
        return 112;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static int m6394() {
        int r0 = YearClass.m7609(TVApplication.m6288());
        if (r0 <= 2013) {
            return 13;
        }
        return r0 <= 2014 ? 10 : 7;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0033 A[SYNTHETIC, Splitter:B:17:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0038 A[SYNTHETIC, Splitter:B:20:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0056 A[SYNTHETIC, Splitter:B:28:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0082 A[SYNTHETIC, Splitter:B:44:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0087 A[SYNTHETIC, Splitter:B:47:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m6395(java.io.File r12) {
        /*
            r11 = 0
            r9 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r9]
            org.mozilla.universalchardet.UniversalDetector r4 = new org.mozilla.universalchardet.UniversalDetector
            r9 = 0
            r4.<init>(r9)
            r6 = 0
            r0 = 0
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00a0 }
            r7.<init>(r12)     // Catch:{ Exception -> 0x00a0 }
            net.pempek.unicode.UnicodeBOMInputStream r1 = new net.pempek.unicode.UnicodeBOMInputStream     // Catch:{ Exception -> 0x00a2, all -> 0x0099 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x00a2, all -> 0x0099 }
        L_0x0017:
            int r8 = r1.read(r2)     // Catch:{ Exception -> 0x0028, all -> 0x009c }
            if (r8 <= 0) goto L_0x0054
            boolean r9 = r4.isDone()     // Catch:{ Exception -> 0x0028, all -> 0x009c }
            if (r9 != 0) goto L_0x0054
            r9 = 0
            r4.handleData(r2, r9, r8)     // Catch:{ Exception -> 0x0028, all -> 0x009c }
            goto L_0x0017
        L_0x0028:
            r5 = move-exception
            r0 = r1
            r6 = r7
        L_0x002b:
            r9 = 0
            boolean[] r9 = new boolean[r9]     // Catch:{ all -> 0x007f }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r9)     // Catch:{ all -> 0x007f }
            if (r0 == 0) goto L_0x0036
            r0.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0036:
            if (r6 == 0) goto L_0x003b
            r6.close()     // Catch:{ Exception -> 0x0078 }
        L_0x003b:
            r4.dataEnd()
            java.lang.String r3 = r4.getDetectedCharset()
            r4.reset()
            if (r3 == 0) goto L_0x0053
            java.lang.String r9 = "MACCYRILLIC"
            boolean r9 = r3.equalsIgnoreCase(r9)
            if (r9 == 0) goto L_0x0053
            java.lang.String r3 = "ISO-8859-5"
        L_0x0053:
            return r3
        L_0x0054:
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ Exception -> 0x0061 }
        L_0x0059:
            if (r7 == 0) goto L_0x00a5
            r7.close()     // Catch:{ Exception -> 0x0068 }
            r0 = r1
            r6 = r7
            goto L_0x003b
        L_0x0061:
            r5 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r9)
            goto L_0x0059
        L_0x0068:
            r5 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r9)
            r0 = r1
            r6 = r7
            goto L_0x003b
        L_0x0071:
            r5 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r9)
            goto L_0x0036
        L_0x0078:
            r5 = move-exception
            boolean[] r9 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r9)
            goto L_0x003b
        L_0x007f:
            r9 = move-exception
        L_0x0080:
            if (r0 == 0) goto L_0x0085
            r0.close()     // Catch:{ Exception -> 0x008b }
        L_0x0085:
            if (r6 == 0) goto L_0x008a
            r6.close()     // Catch:{ Exception -> 0x0092 }
        L_0x008a:
            throw r9
        L_0x008b:
            r5 = move-exception
            boolean[] r10 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r10)
            goto L_0x0085
        L_0x0092:
            r5 = move-exception
            boolean[] r10 = new boolean[r11]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r10)
            goto L_0x008a
        L_0x0099:
            r9 = move-exception
            r6 = r7
            goto L_0x0080
        L_0x009c:
            r9 = move-exception
            r0 = r1
            r6 = r7
            goto L_0x0080
        L_0x00a0:
            r5 = move-exception
            goto L_0x002b
        L_0x00a2:
            r5 = move-exception
            r6 = r7
            goto L_0x002b
        L_0x00a5:
            r0 = r1
            r6 = r7
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.utils.Utils.m6395(java.io.File):java.lang.String");
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 靐  reason: contains not printable characters */
    public static long m6396(File file) {
        long j = 10485760;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            boolean z = Build.VERSION.SDK_INT >= 18;
            j = ((z ? statFs.getBlockCountLong() : (long) statFs.getBlockCount()) * (z ? statFs.getBlockSizeLong() : (long) statFs.getBlockSize())) / 50;
        } catch (IllegalArgumentException e) {
        }
        return Math.max(Math.min(j, 52428800), 10485760);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m6397() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m6398(Context context) {
        return Build.VERSION.SDK_INT >= 24 ? PreferenceManager.getDefaultSharedPreferencesName(context) : context.getPackageName() + "_preferences";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m6399(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            sb.append((String) next.getKey()).append("=").append(m6414((String) next.getValue(), new boolean[0])).append("&");
        }
        String sb2 = sb.toString();
        return !sb2.isEmpty() ? sb2.substring(0, sb2.length() - 1) : sb2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Map<String, String> m6400(String str) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (!str.contains("&")) {
            int indexOf = str.indexOf("=");
            if (indexOf != -1) {
                String str2 = null;
                try {
                    str2 = URLDecoder.decode(str.substring(0, indexOf), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String str3 = "";
                if (str.length() >= indexOf + 1) {
                    try {
                        str3 = URLDecoder.decode(str.substring(indexOf + 1), "UTF-8");
                    } catch (UnsupportedEncodingException e2) {
                        e2.printStackTrace();
                    }
                }
                linkedHashMap.put(str2, str3);
            }
        } else {
            for (String str4 : str.split("&")) {
                int indexOf2 = str4.indexOf("=");
                if (indexOf2 != -1) {
                    try {
                        String decode = URLDecoder.decode(str4.substring(0, indexOf2), "UTF-8");
                        String str5 = "";
                        if (str4.length() >= indexOf2 + 1) {
                            str5 = URLDecoder.decode(str4.substring(indexOf2 + 1), "UTF-8");
                        }
                        linkedHashMap.put(decode, str5);
                    } catch (UnsupportedEncodingException e3) {
                        String substring = str4.substring(0, indexOf2);
                        String str6 = "";
                        if (str4.length() >= indexOf2 + 1) {
                            str6 = str4.substring(indexOf2 + 1);
                        }
                        linkedHashMap.put(substring, str6);
                    }
                }
            }
        }
        return linkedHashMap;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m6401(Activity activity) {
        boolean z = false;
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                activity.finishAndRemoveTask();
            } else {
                ActivityCompat.finishAffinity(activity);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            z = true;
        }
        try {
            Process.killProcess(Process.myPid());
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            z = true;
        }
        if (z) {
            try {
                ActivityCompat.finishAffinity(activity);
            } catch (Exception e3) {
                Logger.m6281((Throwable) e3, new boolean[0]);
                activity.finish();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m6402(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + str));
            intent.setFlags(268435456);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            try {
                Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + str));
                intent2.setFlags(268435456);
                context.startActivity(intent2);
            } catch (ActivityNotFoundException e2) {
                Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + str));
                intent3.setFlags(268435456);
                context.startActivity(IntentUtils.m17788(context, intent3, I18N.m15706(R.string.choose_browser)));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0042  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m6403(java.util.List<java.lang.String> r9) {
        /*
            r1 = 1
            r8 = 0
            if (r9 == 0) goto L_0x000a
            boolean r5 = r9.isEmpty()
            if (r5 == 0) goto L_0x000b
        L_0x000a:
            return r1
        L_0x000b:
            java.lang.String r5 = com.typhoon.tv.TyphoonApp.f5887
            if (r5 == 0) goto L_0x0025
            java.lang.String r5 = com.typhoon.tv.TyphoonApp.f5887
            java.lang.String r5 = r5.trim()
            java.lang.String r6 = " "
            java.lang.String r7 = ""
            java.lang.String r5 = r5.replace(r6, r7)
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x0037
        L_0x0025:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x006c }
            java.lang.String r5 = "RU1UNlhoSUdiOGNNQU1tem9Gb0wrY0tQOEtnPQ=="
            r6 = 0
            byte[] r5 = android.util.Base64.decode(r5, r6)     // Catch:{ Exception -> 0x006c }
            java.lang.String r6 = "UTF-8"
            r0.<init>(r5, r6)     // Catch:{ Exception -> 0x006c }
        L_0x0035:
            com.typhoon.tv.TyphoonApp.f5887 = r0
        L_0x0037:
            r1 = 0
            java.util.Iterator r5 = r9.iterator()
        L_0x003c:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x000a
            java.lang.Object r4 = r5.next()
            java.lang.String r4 = (java.lang.String) r4
            if (r4 == 0) goto L_0x006a
            java.lang.String r6 = r4.trim()
            java.lang.String r7 = " "
            java.lang.String r8 = ""
            java.lang.String r6 = r6.replace(r7, r8)
            boolean r6 = r6.isEmpty()
            if (r6 != 0) goto L_0x006a
            java.lang.String r6 = r4.trim()
            java.lang.String r7 = com.typhoon.tv.TyphoonApp.f5887
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x003c
        L_0x006a:
            r1 = 1
            goto L_0x000a
        L_0x006c:
            r2 = move-exception
            boolean[] r5 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r5)
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0080 }
            java.lang.String r5 = "RU1UNlhoSUdiOGNNQU1tem9Gb0wrY0tQOEtnPQ=="
            r6 = 0
            byte[] r5 = android.util.Base64.decode(r5, r6)     // Catch:{ Exception -> 0x0080 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x0080 }
            goto L_0x0035
        L_0x0080:
            r3 = move-exception
            boolean[] r5 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r3, (boolean[]) r5)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.utils.Utils.m6403(java.util.List):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[SYNTHETIC, Splitter:B:24:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003d A[SYNTHETIC, Splitter:B:27:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0052 A[SYNTHETIC, Splitter:B:35:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0057 A[SYNTHETIC, Splitter:B:38:0x0057] */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m6404(java.io.File r9) {
        /*
            r8 = 0
            r3 = 0
            r4 = 0
            r0 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002f }
            r5.<init>(r9)     // Catch:{ Exception -> 0x002f }
            net.pempek.unicode.UnicodeBOMInputStream r1 = new net.pempek.unicode.UnicodeBOMInputStream     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            java.lang.String r3 = com.mucommander.commons.io.EncodingDetector.m14545(r1)     // Catch:{ Exception -> 0x0073, all -> 0x006c }
            if (r1 == 0) goto L_0x0017
            r1.close()     // Catch:{ Exception -> 0x001f }
        L_0x0017:
            if (r5 == 0) goto L_0x0077
            r5.close()     // Catch:{ Exception -> 0x0026 }
            r0 = r1
            r4 = r5
        L_0x001e:
            return r3
        L_0x001f:
            r2 = move-exception
            boolean[] r6 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
            goto L_0x0017
        L_0x0026:
            r2 = move-exception
            boolean[] r6 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
            r0 = r1
            r4 = r5
            goto L_0x001e
        L_0x002f:
            r2 = move-exception
        L_0x0030:
            r6 = 0
            boolean[] r6 = new boolean[r6]     // Catch:{ all -> 0x004f }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x003b
            r0.close()     // Catch:{ Exception -> 0x0048 }
        L_0x003b:
            if (r4 == 0) goto L_0x001e
            r4.close()     // Catch:{ Exception -> 0x0041 }
            goto L_0x001e
        L_0x0041:
            r2 = move-exception
            boolean[] r6 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
            goto L_0x001e
        L_0x0048:
            r2 = move-exception
            boolean[] r6 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r6)
            goto L_0x003b
        L_0x004f:
            r6 = move-exception
        L_0x0050:
            if (r0 == 0) goto L_0x0055
            r0.close()     // Catch:{ Exception -> 0x005b }
        L_0x0055:
            if (r4 == 0) goto L_0x005a
            r4.close()     // Catch:{ Exception -> 0x0062 }
        L_0x005a:
            throw r6
        L_0x005b:
            r2 = move-exception
            boolean[] r7 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r7)
            goto L_0x0055
        L_0x0062:
            r2 = move-exception
            boolean[] r7 = new boolean[r8]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r2, (boolean[]) r7)
            goto L_0x005a
        L_0x0069:
            r6 = move-exception
            r4 = r5
            goto L_0x0050
        L_0x006c:
            r6 = move-exception
            r0 = r1
            r4 = r5
            goto L_0x0050
        L_0x0070:
            r2 = move-exception
            r4 = r5
            goto L_0x0030
        L_0x0073:
            r2 = move-exception
            r0 = r1
            r4 = r5
            goto L_0x0030
        L_0x0077:
            r0 = r1
            r4 = r5
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.utils.Utils.m6404(java.io.File):java.lang.String");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m6405(String str) {
        String r0 = Regex.m17800(str, "(\\.\\w{3,4})(?:$|\\?)", 1);
        if (r0.isEmpty() && str.lastIndexOf(".") != -1) {
            r0 = str.substring(str.lastIndexOf("."), str.length());
        }
        return r0.trim().toLowerCase();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static void m6406() {
        try {
            File file = new File(TVApplication.m6288().getFilesDir().getParent() + "/shared_prefs/secure_prefs.xml");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m6407(File file) {
        String r0 = m6404(file);
        if (r0 != null && !r0.isEmpty() && Charset.isSupported(r0)) {
            return r0;
        }
        String r02 = m6395(file);
        return (r02 == null || r02.isEmpty() || !Charset.isSupported(r02)) ? "ISO-8859-1" : r02;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m6408(String str) {
        return Regex.m17800(str, "(\\.\\w{3,4})(?:$|\\?)", 1).trim().toLowerCase();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static List<String> m6409(Context context) {
        String str;
        String str2;
        String str3;
        if (TyphoonApp.f5885 == null || TyphoonApp.f5885.isEmpty()) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), Integer.parseInt("59") + Integer.parseInt("5"));
                try {
                    str = new String(Base64.decode("c2lnbmF0dXJlcw==", 0), "UTF-8");
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    try {
                        str = new String(Base64.decode("c2lnbmF0dXJlcw==", 0));
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                        return TyphoonApp.f5885;
                    }
                }
                try {
                    str2 = new String(Base64.decode("YW5kcm9pZC5jb250ZW50LnBtLlNpZ25hdHVyZQ==", 0), "UTF-8");
                } catch (Exception e3) {
                    Logger.m6281((Throwable) e3, new boolean[0]);
                    try {
                        str2 = new String(Base64.decode("YW5kcm9pZC5jb250ZW50LnBtLlNpZ25hdHVyZQ==", 0));
                    } catch (Exception e4) {
                        Logger.m6281((Throwable) e4, new boolean[0]);
                        return TyphoonApp.f5885;
                    }
                }
                try {
                    str3 = new String(Base64.decode("dG9CeXRlQXJyYXk=", 0), "UTF-8");
                } catch (Exception e5) {
                    Logger.m6281((Throwable) e5, new boolean[0]);
                    try {
                        str3 = new String(Base64.decode("dG9CeXRlQXJyYXk=", 0));
                    } catch (Exception e6) {
                        Logger.m6281((Throwable) e6, new boolean[0]);
                        return TyphoonApp.f5885;
                    }
                }
                try {
                    Field declaredField = PackageInfo.class.getDeclaredField(str);
                    declaredField.setAccessible(true);
                    Object[] objArr = (Object[]) declaredField.get(packageInfo);
                    int length = objArr.length;
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= length) {
                            break;
                        }
                        Object obj = objArr[i2];
                        try {
                            Method declaredMethod = Class.forName(str2).getDeclaredMethod(str3, new Class[0]);
                            declaredMethod.setAccessible(true);
                            MessageDigest instance = MessageDigest.getInstance("SHA");
                            instance.update((byte[]) declaredMethod.invoke(obj, new Object[0]));
                            String encodeToString = Base64.encodeToString(instance.digest(), 0);
                            if (!encodeToString.trim().replace(StringUtils.LF, "").replace(StringUtils.CR, "").isEmpty()) {
                                if (TyphoonApp.f5885 == null) {
                                    TyphoonApp.f5885 = new ArrayList();
                                }
                                TyphoonApp.f5885.add(encodeToString.trim().replace(StringUtils.LF, "").replace(StringUtils.CR, ""));
                            }
                            i = i2 + 1;
                        } catch (Exception e7) {
                            Logger.m6281((Throwable) e7, new boolean[0]);
                            return TyphoonApp.f5885;
                        }
                    }
                } catch (Exception e8) {
                    Logger.m6281((Throwable) e8, new boolean[0]);
                    return TyphoonApp.f5885;
                }
            } catch (Exception e9) {
                Logger.m6281((Throwable) e9, new boolean[0]);
                return TyphoonApp.f5885;
            }
        }
        return TyphoonApp.f5885;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m6410() {
        return TyphoonApp.f5850 || TyphoonApp.f5851;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m6411(int i, int i2) {
        if (i < i2) {
            return -1;
        }
        return i == i2 ? 0 : 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T, E> T m6412(Map<T, E> map, E e) {
        for (Map.Entry next : map.entrySet()) {
            if (m6425((Object) e, next.getValue())) {
                return next.getKey();
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m6413(int i) {
        String valueOf = String.valueOf(i);
        return valueOf.length() == 1 ? "0" + valueOf : valueOf;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m6414(String str, boolean... zArr) {
        if (!(zArr != null && zArr.length > 0 && zArr[0]) && (str.trim().toLowerCase().startsWith("http://") || str.trim().toLowerCase().startsWith("https://"))) {
            try {
                URL url = new URL(str);
                return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef()).toURL().toString();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            return str.replace(":", "%3A").replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "%2F").replace("#", "%23").replace("?", "%3F").replace("&", "%24").replace("@", "%40").replace("%", "%25").replace("+", "%2B").replace(StringUtils.SPACE, "+").replace(";", "%3B").replace("=", "%3D").replace("$", "%26").replace(",", "%2C").replace("<", "%3C").replace(">", "%3E").replace("~", "%25").replace("^", "%5E").replace("`", "%60").replace("\\", "%5C").replace("[", "%5B").replace("]", "%5D").replace("{", "%7B").replace("|", "%7C").replace("\"", "%22");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> ArrayList<T> m6415(List<T> list) {
        return new ArrayList<>(new LinkedHashSet(list));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <K, V> HashMap<V, K> m6416(Map<K, V> map) {
        HashMap<V, K> hashMap = new HashMap<>();
        for (Map.Entry next : map.entrySet()) {
            hashMap.put(next.getValue(), next.getKey());
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> List<T> m6417(Iterator<T> it2) {
        ArrayList arrayList = new ArrayList();
        while (it2.hasNext()) {
            arrayList.add(it2.next());
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Map<String, String> m6418(URL url) {
        return url.getQuery() == null ? new HashMap() : m6400(url.getQuery());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m6419(Activity activity) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : TyphoonApp.m6329().entrySet()) {
            sb.append(next.getKey()).append(" : \n");
            sb.append(next.getValue()).append("\n\n");
        }
        sb.append("Thank you for all the contribution!");
        new AlertDialog.Builder(activity).m536((CharSequence) I18N.m15706(R.string.translators)).m523((CharSequence) sb.toString()).m520((int) R.mipmap.ic_launcher).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).m528();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m6420(Context context) {
        if (context instanceof Activity) {
            try {
                Intent intent = new Intent();
                intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                intent.setData(Uri.fromParts("package", context.getPackageName(), (String) null));
                context.startActivity(intent);
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m6421(java.io.File r7) {
        /*
            r4 = 0
            if (r7 == 0) goto L_0x0009
            boolean r3 = r7.exists()     // Catch:{ Throwable -> 0x002a }
            if (r3 != 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            boolean r3 = r7.isDirectory()     // Catch:{ Throwable -> 0x002a }
            if (r3 == 0) goto L_0x0036
            java.io.File[] r2 = r7.listFiles()     // Catch:{ Throwable -> 0x002a }
            if (r2 == 0) goto L_0x0036
            int r5 = r2.length     // Catch:{ Throwable -> 0x002a }
            r3 = r4
        L_0x0018:
            if (r3 >= r5) goto L_0x0036
            r0 = r2[r3]     // Catch:{ Throwable -> 0x002a }
            m6421((java.io.File) r0)     // Catch:{ Exception -> 0x0022 }
        L_0x001f:
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0022:
            r1 = move-exception
            r6 = 0
            boolean[] r6 = new boolean[r6]     // Catch:{ Throwable -> 0x002a }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r6)     // Catch:{ Throwable -> 0x002a }
            goto L_0x001f
        L_0x002a:
            r1 = move-exception
            r3 = 1
            boolean[] r3 = new boolean[r3]
            boolean r5 = r1 instanceof java.lang.StackOverflowError
            r3[r4] = r5
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r3)
            goto L_0x0009
        L_0x0036:
            r7.delete()     // Catch:{ Throwable -> 0x002a }
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.utils.Utils.m6421(java.io.File):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m6422(boolean z) {
        if (!Build.MANUFACTURER.equalsIgnoreCase("xiaomi") || !Build.MODEL.trim().toLowerCase().contains("mibox3")) {
            try {
                AudioManager r0 = TVApplication.m6286();
                if (z) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        boolean z2 = TyphoonApp.f5850;
                        if (!r0.isStreamMute(3)) {
                            z2 = true;
                        }
                        r0.adjustStreamVolume(3, -100, 0);
                        TyphoonApp.f5850 = z2;
                        return;
                    }
                    r0.setStreamMute(3, true);
                    TyphoonApp.f5851 = true;
                } else if (Build.VERSION.SDK_INT < 23) {
                    r0.setStreamMute(3, false);
                    TyphoonApp.f5851 = false;
                } else if (TyphoonApp.f5850) {
                    try {
                        r0.adjustStreamVolume(3, 100, 0);
                        TyphoonApp.f5850 = false;
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            } catch (Throwable th) {
                Logger.m6281(th, true);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6423() {
        try {
            return (Build.VERSION.SDK_INT < 17 || Build.VERSION.SDK_INT >= 21) ? Settings.Secure.getInt(TVApplication.m6288().getContentResolver(), "install_non_market_apps", 1) == 1 : Settings.Global.getInt(TVApplication.m6288().getContentResolver(), "install_non_market_apps", 1) == 1;
        } catch (Throwable th) {
            Logger.m6281(th, true);
            return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6424(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6425(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6426(String str) {
        return f5938.matcher(str).matches();
    }

    @SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6427(String str, File file, HashMap<String, String>... hashMapArr) {
        ResponseBody r3;
        HashMap<String, String> hashMap = null;
        if (!(hashMapArr == null || hashMapArr.length <= 0 || hashMapArr[0] == null)) {
            hashMap = hashMapArr[0];
        }
        Sink sink = null;
        BufferedSink bufferedSink = null;
        ResponseBody responseBody = null;
        if (hashMap != null) {
            try {
                r3 = HttpHelper.m6343().m6369(str, (Map<String, String>[]) new Map[]{hashMap});
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                if (bufferedSink != null) {
                    try {
                        bufferedSink.flush();
                    } catch (Exception e2) {
                        Logger.m6281((Throwable) e2, new boolean[0]);
                    }
                    try {
                        bufferedSink.close();
                    } catch (Exception e3) {
                        Logger.m6281((Throwable) e3, new boolean[0]);
                    }
                }
                if (sink != null) {
                    try {
                        sink.flush();
                    } catch (Exception e4) {
                        Logger.m6281((Throwable) e4, new boolean[0]);
                    }
                    try {
                        sink.close();
                    } catch (Exception e5) {
                        Logger.m6281((Throwable) e5, new boolean[0]);
                    }
                }
                if (responseBody == null) {
                    return false;
                }
                responseBody.close();
                return false;
            } catch (Throwable th) {
                if (bufferedSink != null) {
                    try {
                        bufferedSink.flush();
                    } catch (Exception e6) {
                        Logger.m6281((Throwable) e6, new boolean[0]);
                    }
                    try {
                        bufferedSink.close();
                    } catch (Exception e7) {
                        Logger.m6281((Throwable) e7, new boolean[0]);
                    }
                }
                if (sink != null) {
                    try {
                        sink.flush();
                    } catch (Exception e8) {
                        Logger.m6281((Throwable) e8, new boolean[0]);
                    }
                    try {
                        sink.close();
                    } catch (Exception e9) {
                        Logger.m6281((Throwable) e9, new boolean[0]);
                    }
                }
                if (responseBody != null) {
                    responseBody.close();
                }
                throw th;
            }
        } else {
            r3 = HttpHelper.m6343().m6369(str, (Map<String, String>[]) new Map[0]);
        }
        if (r3 != null) {
            Sink r4 = Okio.m20502(file);
            BufferedSink r0 = Okio.m20506(r4);
            r0.m20450(r3.m7095());
            if (r0 != null) {
                try {
                    r0.flush();
                } catch (Exception e10) {
                    Logger.m6281((Throwable) e10, new boolean[0]);
                }
                try {
                    r0.close();
                } catch (Exception e11) {
                    Logger.m6281((Throwable) e11, new boolean[0]);
                }
            }
            if (r4 != null) {
                try {
                    r4.flush();
                } catch (Exception e12) {
                    Logger.m6281((Throwable) e12, new boolean[0]);
                }
                try {
                    r4.close();
                } catch (Exception e13) {
                    Logger.m6281((Throwable) e13, new boolean[0]);
                }
            }
            if (r3 != null) {
                r3.close();
            }
            return true;
        }
        if (bufferedSink != null) {
            try {
                bufferedSink.flush();
            } catch (Exception e14) {
                Logger.m6281((Throwable) e14, new boolean[0]);
            }
            try {
                bufferedSink.close();
            } catch (Exception e15) {
                Logger.m6281((Throwable) e15, new boolean[0]);
            }
        }
        if (sink != null) {
            try {
                sink.flush();
            } catch (Exception e16) {
                Logger.m6281((Throwable) e16, new boolean[0]);
            }
            try {
                sink.close();
            } catch (Exception e17) {
                Logger.m6281((Throwable) e17, new boolean[0]);
            }
        }
        if (r3 == null) {
            return false;
        }
        r3.close();
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String[] m6428(String[] strArr, String[] strArr2) {
        try {
            int length = strArr.length;
            int length2 = strArr2.length;
            String[] strArr3 = new String[(length + length2)];
            System.arraycopy(strArr, 0, strArr3, 0, length);
            System.arraycopy(strArr2, 0, strArr3, length, length2);
            return strArr3;
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
            return strArr;
        }
    }
}
