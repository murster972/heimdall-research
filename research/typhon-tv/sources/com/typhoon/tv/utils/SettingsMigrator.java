package com.typhoon.tv.utils;

import android.content.SharedPreferences;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.helper.player.BasePlayerHelper;
import com.typhoon.tv.helper.player.ExoPlayerHelper;

public class SettingsMigrator {
    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m17807() {
        SharedPreferences r1 = TVApplication.m6285();
        SharedPreferences.Editor edit = r1.edit();
        if (r1.getBoolean("pref_show_menu_source", true)) {
            edit.putInt("pref_choose_default_play_action", 0);
        } else {
            edit.putInt("pref_choose_default_play_action", r1.getInt("pref_choose_default_play_action", 0) + 1);
        }
        edit.apply();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static void m17808() {
        boolean z = true;
        SharedPreferences.Editor edit = TVApplication.m6285().edit();
        edit.putBoolean("pref_resolve_all_links_immediately", true);
        if (DeviceUtils.m6387()) {
            z = false;
        }
        edit.putBoolean("pref_auto_resolve_hd_links_only", z);
        edit.apply();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static void m17809() {
        SharedPreferences r3 = TVApplication.m6285();
        SharedPreferences.Editor edit = r3.edit();
        int i = r3.getInt("pref_choose_default_play_action", 0);
        int i2 = i;
        if (i == 5) {
            i2 = 3;
        } else if (i == 3) {
            i2 = 4;
        } else if (i == 4) {
            i2 = 5;
        }
        edit.putInt("pref_choose_default_play_action", i2);
        edit.apply();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static void m17810() {
        if (ExoPlayerHelper.m16055()) {
            SharedPreferences r2 = TVApplication.m6285();
            SharedPreferences.Editor edit = r2.edit();
            String string = r2.getString("pref_choose_default_player", (String) null);
            if (string != null && string.equalsIgnoreCase("Exo")) {
                edit.putString("pref_choose_default_player", "Exo");
                edit.apply();
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private static void m17811() {
        if (ExoPlayerHelper.m16055()) {
            SharedPreferences r2 = TVApplication.m6285();
            SharedPreferences.Editor edit = r2.edit();
            String string = r2.getString("pref_choose_default_player", (String) null);
            if (string != null && !string.equalsIgnoreCase("Exo")) {
                edit.putString("pref_choose_default_player", "Exo");
                edit.apply();
            }
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private static void m17812() {
        try {
            SharedPreferences.Editor edit = TVApplication.m6285().edit();
            edit.putString("google_acc", "");
            edit.putBoolean("pref_show_disclaimer", true);
            edit.commit();
            Utils.m6406();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private static void m17813() {
        TVApplication.m6285().edit().putBoolean("choose_default_video_player_dialog_shown", false).apply();
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private static void m17814() {
        try {
            if (ExoPlayerHelper.m16055()) {
                SharedPreferences r3 = TVApplication.m6285();
                SharedPreferences.Editor edit = r3.edit();
                String string = r3.getString("pref_choose_default_player", (String) null);
                if (string == null || !string.equalsIgnoreCase("Yes")) {
                    edit.putString("pref_choose_default_player", "Exo");
                    edit.apply();
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static void m17815() {
        try {
            if (ExoPlayerHelper.m16055()) {
                SharedPreferences r3 = TVApplication.m6285();
                SharedPreferences.Editor edit = r3.edit();
                String string = r3.getString("pref_choose_default_player", (String) null);
                if (string == null || !string.equalsIgnoreCase("Exo")) {
                    edit.putString("pref_choose_default_player", "Exo");
                    edit.apply();
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static void m17816() {
        try {
            SharedPreferences sharedPreferences = TVApplication.m6288().getSharedPreferences("CookiePersistence", 0);
            if (sharedPreferences != null) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                if (sharedPreferences.getString("http://tv21.org/|__cfduid", (String) null) != null) {
                    edit.remove("http://tv21.org/|__cfduid");
                }
                if (sharedPreferences.getString("https://tv21.org/|__cfduid", (String) null) != null) {
                    edit.remove("https://tv21.org/|__cfduid");
                }
                if (sharedPreferences.getString("http://www.tv21.org/|__cfduid", (String) null) != null) {
                    edit.remove("http://www.tv21.org/|__cfduid");
                }
                if (sharedPreferences.getString("https://www.tv21.org/|__cfduid", (String) null) != null) {
                    edit.remove("https://www.tv21.org/|__cfduid");
                }
                if (sharedPreferences.getString("http://tv21.org/|cf_clearance", (String) null) != null) {
                    edit.remove("http://tv21.org/|cf_clearance");
                }
                if (sharedPreferences.getString("https://tv21.org/|cf_clearance", (String) null) != null) {
                    edit.remove("https://tv21.org/|cf_clearance");
                }
                if (sharedPreferences.getString("http://www.tv21.org/|cf_clearance", (String) null) != null) {
                    edit.remove("http://www.tv21.org/|cf_clearance");
                }
                if (sharedPreferences.getString("https://www.tv21.org/|cf_clearance", (String) null) != null) {
                    edit.remove("https://www.tv21.org/|cf_clearance");
                }
                edit.apply();
            }
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static void m17817() {
        try {
            if (ExoPlayerHelper.m16055()) {
                SharedPreferences r3 = TVApplication.m6285();
                SharedPreferences.Editor edit = r3.edit();
                String string = r3.getString("pref_choose_default_player", (String) null);
                if (string == null || !string.equalsIgnoreCase("Exo")) {
                    edit.putString("pref_choose_default_player", "Exo");
                    edit.apply();
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private static void m17818() {
        SharedPreferences r2 = TVApplication.m6285();
        SharedPreferences.Editor edit = r2.edit();
        String string = r2.getString("pref_choose_default_player", (String) null);
        if (string != null && string.equalsIgnoreCase("MX")) {
            edit.putString("pref_choose_default_player", BasePlayerHelper.m16035().m16041());
            edit.apply();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private static void m17819() {
        TVApplication.m6285().edit().putBoolean("choose_default_video_player_dialog_shown", false).apply();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static void m17820() {
        TVApplication.m6285().edit().putBoolean("choose_default_video_player_dialog_shown", false).apply();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static void m17821() {
        SharedPreferences r2 = TVApplication.m6285();
        SharedPreferences.Editor edit = r2.edit();
        if (r2.getInt("pref_choose_default_play_action", -2) == -1) {
            edit.putInt("pref_choose_default_play_action", 1);
        }
        edit.apply();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m17822() {
        SharedPreferences r1 = TVApplication.m6285();
        SharedPreferences.Editor edit = r1.edit();
        if (r1.getInt("pref_choose_default_category_movies", -1) == 2) {
            edit.putInt("pref_choose_default_category_movies", 1);
        } else if (r1.getInt("pref_choose_default_category_movies", -1) == 1) {
            edit.putInt("pref_choose_default_category_movies", 2);
        }
        edit.apply();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static void m17823() {
        SharedPreferences r2 = TVApplication.m6285();
        SharedPreferences.Editor edit = r2.edit();
        int i = r2.getInt("pref_choose_default_play_action", 0);
        if (i == 4) {
            edit.putInt("pref_choose_default_play_action", 0);
        } else if (i >= 5) {
            edit.putInt("pref_choose_default_play_action", i - 1);
        }
        edit.apply();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m17824() {
        SharedPreferences r2 = TVApplication.m6285();
        SharedPreferences.Editor edit = r2.edit();
        int i = r2.getInt("pref_choose_default_play_action", 0);
        if (i >= 4) {
            edit.putInt("pref_choose_default_play_action", i + 1);
        }
        edit.apply();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m17825() {
        SharedPreferences r1 = TVApplication.m6285();
        SharedPreferences.Editor edit = r1.edit();
        if (r1.getInt("pref_choose_default_play_action", 0) == 1) {
            edit.putInt("pref_choose_default_play_action", 2);
        } else if (r1.getInt("pref_choose_default_play_action", 0) == 2) {
            edit.putInt("pref_choose_default_play_action", 1);
        }
        edit.apply();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m17826(int i) {
        if (i < 68) {
            m17825();
        }
        if (i < 71) {
            m17822();
        }
        if (i < 73) {
            m17824();
        }
        if (i < 76) {
            m17823();
        }
        if (i < 77) {
            m17821();
        }
        if (i < 86) {
            m17807();
        }
        if (i < 88) {
            m17808();
        }
        if (i < 91) {
            m17809();
        }
        if (i < 95) {
            m17818();
        }
        if (i < 99) {
            m17819();
        }
        if (i < 100) {
            m17820();
        }
        if (i < 101) {
            m17813();
        }
        if (i < 105) {
            m17810();
        }
        if (i < 106) {
            m17811();
        }
        if (i < 108) {
            m17827();
        }
        if (i < 109) {
            m17828();
        }
        if (i < 110) {
            m17815();
        }
        if (i < 111) {
            m17816();
            m17817();
        }
        if (i < 112) {
            m17812();
            m17814();
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static void m17827() {
        if (ExoPlayerHelper.m16055()) {
            SharedPreferences r2 = TVApplication.m6285();
            SharedPreferences.Editor edit = r2.edit();
            String string = r2.getString("pref_choose_default_player", (String) null);
            if (string != null && !string.equalsIgnoreCase("Yes")) {
                edit.putString("pref_choose_default_player", "Exo");
                edit.apply();
            }
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static void m17828() {
        try {
            if (ExoPlayerHelper.m16055()) {
                SharedPreferences r3 = TVApplication.m6285();
                SharedPreferences.Editor edit = r3.edit();
                String string = r3.getString("pref_choose_default_player", (String) null);
                if (string == null || !string.equalsIgnoreCase("Yes")) {
                    edit.putString("pref_choose_default_player", "Exo");
                    edit.apply();
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }
}
