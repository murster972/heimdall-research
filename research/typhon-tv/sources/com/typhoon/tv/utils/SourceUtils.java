package com.typhoon.tv.utils;

import java.util.HashMap;

public class SourceUtils {
    /* renamed from: 靐  reason: contains not printable characters */
    public static HashMap<String, String> m17835(HashMap<String, String> hashMap) {
        if (hashMap == null) {
            return null;
        }
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.putAll(hashMap);
        if (!hashMap2.containsKey("X-TTV-Custom")) {
            return hashMap2;
        }
        String str = hashMap2.get("X-TTV-Custom");
        if (str != null && !str.isEmpty() && str.contains("rangeFromZero")) {
            hashMap2.put("Range", "bytes=0-");
        }
        hashMap2.remove("X-TTV-Custom");
        return hashMap2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static HashMap<String, String> m17836(HashMap<String, String> hashMap) {
        if (hashMap == null) {
            return null;
        }
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.putAll(hashMap);
        if (!hashMap2.containsKey("X-TTV-Custom")) {
            return hashMap2;
        }
        hashMap2.remove("X-TTV-Custom");
        return hashMap2;
    }
}
