package com.typhoon.tv.utils;

import com.typhoon.tv.Logger;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    /* renamed from: 龘  reason: contains not printable characters */
    public static ArrayList<ArrayList<String>> m17803(String str, String re, int maxGroupCount) {
        Matcher m = str == null ? null : Pattern.compile(re).matcher(str);
        int groupCount = m == null ? maxGroupCount : Math.max(m.groupCount(), maxGroupCount);
        ArrayList<ArrayList<String>> groupResultArrayList = new ArrayList<>();
        if (groupCount == 0) {
            groupResultArrayList.add(0, new ArrayList());
        } else {
            for (int i = 0; i < groupCount; i++) {
                groupResultArrayList.add(i, new ArrayList());
            }
        }
        if (groupResultArrayList.isEmpty()) {
            groupResultArrayList.add(new ArrayList());
        }
        if (m != null && !str.isEmpty()) {
            while (m.find()) {
                try {
                    if (maxGroupCount == 0) {
                        groupResultArrayList.get(0).add(m.group(0));
                    } else {
                        for (int i2 = 0; i2 < maxGroupCount; i2++) {
                            groupResultArrayList.get(i2).add(m.group(i2 + 1));
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            if (groupResultArrayList.isEmpty()) {
                groupResultArrayList.add(new ArrayList());
            }
        }
        return groupResultArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ArrayList<ArrayList<String>> m17804(String str, String re, int maxGroupCount, int mode) {
        Matcher m = str == null ? null : Pattern.compile(re, mode).matcher(str);
        int groupCount = m == null ? maxGroupCount : Math.max(m.groupCount(), maxGroupCount);
        ArrayList<ArrayList<String>> groupResultArrayList = new ArrayList<>();
        if (groupCount == 0) {
            groupResultArrayList.add(0, new ArrayList());
        } else {
            for (int i = 0; i < groupCount; i++) {
                groupResultArrayList.add(i, new ArrayList());
            }
        }
        if (groupResultArrayList.isEmpty()) {
            groupResultArrayList.add(new ArrayList());
        }
        if (m != null && !str.isEmpty()) {
            while (m.find()) {
                try {
                    if (maxGroupCount == 0) {
                        groupResultArrayList.get(0).add(m.group(0));
                    } else {
                        for (int i2 = 0; i2 < maxGroupCount; i2++) {
                            groupResultArrayList.get(i2).add(m.group(i2 + 1));
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            if (groupResultArrayList.isEmpty()) {
                groupResultArrayList.add(new ArrayList());
            }
        }
        return groupResultArrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ArrayList<String> m17806(String str, String re, boolean dotAll) {
        Matcher m;
        ArrayList<String> result = new ArrayList<>();
        if (str != null && !str.isEmpty()) {
            if (dotAll) {
                m = Pattern.compile(re, 32).matcher(str);
            } else {
                m = Pattern.compile(re).matcher(str);
            }
            while (m.find()) {
                result.add(m.group());
            }
        }
        return result;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ArrayList<ArrayList<String>> m17805(String str, String re, int maxGroupCount, boolean dotAll) {
        Matcher m;
        if (str == null) {
            m = null;
        } else if (dotAll) {
            m = Pattern.compile(re, 32).matcher(str);
        } else {
            m = Pattern.compile(re).matcher(str);
        }
        int groupCount = m == null ? maxGroupCount : Math.max(m.groupCount(), maxGroupCount);
        ArrayList<ArrayList<String>> groupResultArrayList = new ArrayList<>();
        if (groupCount == 0) {
            groupResultArrayList.add(0, new ArrayList());
        } else {
            for (int i = 0; i < groupCount; i++) {
                groupResultArrayList.add(i, new ArrayList());
            }
        }
        if (groupResultArrayList.isEmpty()) {
            groupResultArrayList.add(new ArrayList());
        }
        if (m != null && !str.isEmpty()) {
            while (m.find()) {
                try {
                    if (maxGroupCount == 0) {
                        groupResultArrayList.get(0).add(m.group(0));
                    } else {
                        for (int i2 = 0; i2 < maxGroupCount; i2++) {
                            groupResultArrayList.get(i2).add(m.group(i2 + 1));
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            if (groupResultArrayList.isEmpty()) {
                groupResultArrayList.add(new ArrayList());
            }
        }
        return groupResultArrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m17800(String str, String re, int group) {
        if (str == null || str.isEmpty()) {
            return "";
        }
        Matcher m = Pattern.compile(re).matcher(str);
        if (m.find()) {
            return m.group(group);
        }
        return "";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m17802(String str, String re, int group, boolean dotAll) {
        Matcher m;
        if (str == null || str.isEmpty()) {
            return "";
        }
        if (dotAll) {
            m = Pattern.compile(re, 32).matcher(str);
        } else {
            m = Pattern.compile(re).matcher(str);
        }
        if (m.find()) {
            return m.group(group);
        }
        return "";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m17801(String str, String re, int group, int mode) {
        if (str == null || str.isEmpty()) {
            return "";
        }
        Matcher m = Pattern.compile(re, mode).matcher(str);
        if (m.find()) {
            return m.group(group);
        }
        return "";
    }
}
