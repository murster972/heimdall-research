package com.typhoon.tv.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;

public class YouTubeUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m17840(Context context, String str) {
        if (DeviceUtils.m6388() && Utils.m6424(context, "org.chromium.youtube_apk")) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setClassName("org.chromium.youtube_apk", "org.chromium.youtube_apk.YouTubeActivity");
                intent.setFlags(268435456);
                intent.putExtra("com.amazon.extra.DIAL_PARAM", String.format("#/watch/video/control?v=%s&resume", new Object[]{str}));
                context.startActivity(intent);
                return;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("amzn://apps/android?p=org.chromium.youtube_apk"));
                    intent2.setFlags(268435456);
                    context.startActivity(intent2);
                    return;
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            }
        }
        Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse("https://www.youtube.com/watch?v=" + str));
        intent3.setFlags(268435456);
        try {
            context.startActivity(intent3);
        } catch (Exception e3) {
            Logger.m6281((Throwable) e3, new boolean[0]);
            try {
                Intent intent4 = new Intent("android.intent.action.VIEW", Uri.parse("vnd.youtube:" + str));
                intent4.setFlags(268435456);
                context.startActivity(intent4);
            } catch (Exception e4) {
                Logger.m6281((Throwable) e4, new boolean[0]);
                Toast.makeText(context, I18N.m15706(R.string.cant_open_youtube), 0).show();
            }
        }
    }
}
