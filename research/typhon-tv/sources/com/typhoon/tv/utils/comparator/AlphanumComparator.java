package com.typhoon.tv.utils.comparator;

import java.util.Comparator;

public class AlphanumComparator implements Comparator<String> {
    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17842(char ch) {
        return ch >= '0' && ch <= '9';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m17841(String s, int slength, int marker) {
        StringBuilder chunk = new StringBuilder();
        char c = s.charAt(marker);
        chunk.append(c);
        int marker2 = marker + 1;
        if (m17842(c)) {
            while (marker2 < slength) {
                char c2 = s.charAt(marker2);
                if (!m17842(c2)) {
                    break;
                }
                chunk.append(c2);
                marker2++;
            }
        } else {
            while (marker2 < slength) {
                char c3 = s.charAt(marker2);
                if (m17842(c3)) {
                    break;
                }
                chunk.append(c3);
                marker2++;
            }
        }
        return chunk.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int compare(String s1, String s2) {
        int result;
        if (s1 == null || s2 == null) {
            return 0;
        }
        int thisMarker = 0;
        int thatMarker = 0;
        int s1Length = s1.length();
        int s2Length = s2.length();
        while (thisMarker < s1Length && thatMarker < s2Length) {
            String thisChunk = m17841(s1, s1Length, thisMarker);
            thisMarker += thisChunk.length();
            String thatChunk = m17841(s2, s2Length, thatMarker);
            thatMarker += thatChunk.length();
            if (!m17842(thisChunk.charAt(0)) || !m17842(thatChunk.charAt(0))) {
                result = thisChunk.compareTo(thatChunk);
                continue;
            } else {
                int thisChunkLength = thisChunk.length();
                result = thisChunkLength - thatChunk.length();
                if (result == 0) {
                    for (int i = 0; i < thisChunkLength; i++) {
                        result = thisChunk.charAt(i) - thatChunk.charAt(i);
                        if (result != 0) {
                            return result;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            }
            if (result != 0) {
                return result;
            }
        }
        return s1Length - s2Length;
    }
}
