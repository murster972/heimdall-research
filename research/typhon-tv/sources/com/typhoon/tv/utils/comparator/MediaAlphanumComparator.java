package com.typhoon.tv.utils.comparator;

import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import java.util.Comparator;

public class MediaAlphanumComparator implements Comparator {
    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17845(char ch) {
        return ch >= '0' && ch <= '9';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m17844(String s, int slength, int marker) {
        StringBuilder chunk = new StringBuilder();
        char c = s.charAt(marker);
        chunk.append(c);
        int marker2 = marker + 1;
        if (m17845(c)) {
            while (marker2 < slength) {
                char c2 = s.charAt(marker2);
                if (!m17845(c2)) {
                    break;
                }
                chunk.append(c2);
                marker2++;
            }
        } else {
            while (marker2 < slength) {
                char c3 = s.charAt(marker2);
                if (m17845(c3)) {
                    break;
                }
                chunk.append(c3);
                marker2++;
            }
        }
        return chunk.toString();
    }

    public int compare(Object o1, Object o2) {
        String s1;
        String s2;
        int result;
        int result2;
        if ((o1 instanceof MediaSource) && (o2 instanceof MediaSource)) {
            s1 = ((MediaSource) o1).getStringToBeCompared();
            s2 = ((MediaSource) o2).getStringToBeCompared();
        } else if (!(o1 instanceof MediaInfo) || !(o2 instanceof MediaInfo)) {
            return 0;
        } else {
            s1 = ((MediaInfo) o1).getNameAndYear();
            s2 = ((MediaInfo) o2).getNameAndYear();
        }
        int thisMarker = 0;
        int thatMarker = 0;
        int s1Length = s1.length();
        int s2Length = s2.length();
        while (thisMarker < s1Length && thatMarker < s2Length) {
            String thisChunk = m17844(s1, s1Length, thisMarker);
            thisMarker += thisChunk.length();
            String thatChunk = m17844(s2, s2Length, thatMarker);
            thatMarker += thatChunk.length();
            if (!m17845(thisChunk.charAt(0)) || !m17845(thatChunk.charAt(0))) {
                result = thisChunk.compareTo(thatChunk);
                continue;
            } else {
                int thisChunkLength = thisChunk.length();
                if (o1 instanceof MediaInfo) {
                    result2 = thisChunk.length() - thatChunk.length();
                } else {
                    result2 = thatChunk.length() - thisChunk.length();
                }
                if (result == 0) {
                    for (int i = 0; i < thisChunkLength; i++) {
                        if (o1 instanceof MediaInfo) {
                            result = thisChunk.charAt(i) - thatChunk.charAt(i);
                        } else {
                            result = thatChunk.charAt(i) - thisChunk.charAt(i);
                        }
                        if (result != 0) {
                            return result;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            }
            if (result != 0) {
                return result;
            }
        }
        return s1Length - s2Length;
    }
}
