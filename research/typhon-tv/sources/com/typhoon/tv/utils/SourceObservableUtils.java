package com.typhoon.tv.utils;

import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaSource;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class SourceObservableUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Func1<MediaSource, Observable<MediaSource>> m17831(final boolean newThread) {
        return new Func1<MediaSource, Observable<MediaSource>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaSource> call(final MediaSource mediaSource) {
                if (!mediaSource.isResolved()) {
                    return Observable.m7356(mediaSource);
                }
                final String streamLink = mediaSource.getStreamLink();
                boolean r0 = GoogleVideoHelper.m15943(streamLink);
                Observable<MediaSource> r1 = Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void call(Subscriber<? super MediaSource> subscriber) {
                        String r10;
                        Response r12;
                        boolean r15 = SourceObservableUtils.m17832(streamLink);
                        HashMap hashMap = new HashMap();
                        if (mediaSource.getPlayHeader() != null) {
                            hashMap.putAll(mediaSource.getPlayHeader());
                        }
                        HashMap<String, String> r2 = SourceUtils.m17836(hashMap);
                        if (!r15) {
                            r2.put("Range", "bytes=0-1");
                        }
                        Response r9 = HttpHelper.m6343().m6367(streamLink, r15, (Map<String, String>) r2);
                        if (r9 == null) {
                            subscriber.onCompleted();
                            return;
                        }
                        if (r9.m7066() >= 400) {
                            try {
                                if (r9.m7056() != null) {
                                    r9.m7056().close();
                                }
                            } catch (Throwable th) {
                                Logger.m6281(th, new boolean[0]);
                            }
                            boolean z = true;
                            if (GoogleVideoHelper.m15955(streamLink) && (r10 = GoogleVideoHelper.m15942(streamLink)) != null && !r10.isEmpty() && !r10.equals(streamLink) && (r12 = HttpHelper.m6343().m6367(r10, r15, (Map<String, String>) r2)) != null) {
                                if (r12.m7066() < 400) {
                                    z = false;
                                    mediaSource.setStreamLink(r10);
                                    r9 = r12;
                                } else {
                                    try {
                                        if (r12.m7056() != null) {
                                            r12.m7056().close();
                                        }
                                    } catch (Throwable th2) {
                                        Logger.m6281(th2, new boolean[0]);
                                    }
                                }
                            }
                            if (z) {
                                if (r15) {
                                    r2.put("Range", "bytes=0-1");
                                    r9 = HttpHelper.m6343().m6367(streamLink, false, (Map<String, String>) r2);
                                    if (r9 == null) {
                                        subscriber.onCompleted();
                                        return;
                                    } else if (r9.m7066() >= 400) {
                                        try {
                                            if (r9.m7056() != null) {
                                                r9.m7056().close();
                                            }
                                        } catch (Throwable th3) {
                                            Logger.m6281(th3, new boolean[0]);
                                        }
                                        subscriber.onCompleted();
                                        return;
                                    }
                                } else {
                                    subscriber.onCompleted();
                                    return;
                                }
                            }
                        }
                        if (mediaSource.isHLS()) {
                            try {
                                if (r9.m7056() != null) {
                                    r9.m7056().close();
                                }
                            } catch (Throwable th4) {
                                Logger.m6281(th4, new boolean[0]);
                            }
                            subscriber.onNext(mediaSource);
                            subscriber.onCompleted();
                            return;
                        }
                        boolean z2 = true;
                        if (!SourceObservableUtils.m17830(streamLink)) {
                            long r4 = HttpHelper.m6342(r9, r2.containsKey("Range") || r2.containsKey("range"), new boolean[0]);
                            if (r4 > 20971520) {
                                mediaSource.setFileSize(r4);
                            } else {
                                z2 = false;
                            }
                        }
                        if (z2) {
                            subscriber.onNext(mediaSource);
                        }
                        try {
                            if (r9.m7056() != null) {
                                r9.m7056().close();
                            }
                        } catch (Throwable th5) {
                            Logger.m6281(th5, new boolean[0]);
                        }
                        subscriber.onCompleted();
                    }
                }).m7375(Observable.m7349()).m7367();
                return (!newThread || r0) ? r1 : r1.m7382(Schedulers.io());
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m17832(String streamLink) {
        String normalizedStreamLink = streamLink.trim().toLowerCase();
        if (GoogleVideoHelper.m15945(streamLink) || normalizedStreamLink.contains("amazonaws") || normalizedStreamLink.contains("amazon.") || normalizedStreamLink.contains("yandex") || normalizedStreamLink.contains("mediafire.") || normalizedStreamLink.contains("/docs/securesc/") || normalizedStreamLink.contains("googleapis") || normalizedStreamLink.contains("hulu.so") || normalizedStreamLink.contains("cloudfront") || normalizedStreamLink.contains("cdn.") || normalizedStreamLink.contains("dfcdn.") || normalizedStreamLink.contains("ntcdn.") || normalizedStreamLink.contains("micetop.") || normalizedStreamLink.contains("cdn.vidnode.") || normalizedStreamLink.contains("/proxy") || normalizedStreamLink.contains("proxy/") || normalizedStreamLink.contains("5.189.155.231") || normalizedStreamLink.contains("streamcherry.xyz") || normalizedStreamLink.contains("up2stream") || normalizedStreamLink.contains("real-debrid") || normalizedStreamLink.contains("juicyapi") || normalizedStreamLink.contains("fruity") || normalizedStreamLink.contains("streamhub") || normalizedStreamLink.contains("streamy.") || normalizedStreamLink.contains("moviestime") || normalizedStreamLink.contains("m4ufree") || normalizedStreamLink.contains("gomovieshd.to") || normalizedStreamLink.contains("cmovieshd.com") || normalizedStreamLink.contains("/videoplayback?data=") || normalizedStreamLink.contains("html5player") || normalizedStreamLink.contains("anyplayer.") || normalizedStreamLink.contains("gdplayer") || ((normalizedStreamLink.contains("mehliz") && !normalizedStreamLink.contains("search") && !normalizedStreamLink.contains("/?s=")) || normalizedStreamLink.contains("gphoto.stream") || normalizedStreamLink.contains("azmovies") || normalizedStreamLink.contains("dropboxusercontent."))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m17830(String streamLink) {
        return false;
    }
}
