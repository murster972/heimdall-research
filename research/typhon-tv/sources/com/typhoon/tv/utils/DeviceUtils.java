package com.typhoon.tv.utils;

import android.app.UiModeManager;
import android.os.Build;
import android.support.v4.os.EnvironmentCompat;
import com.facebook.device.yearclass.YearClass;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.utils.comparator.AlphanumComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeviceUtils {
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m6385() {
        return Build.MANUFACTURER.toLowerCase().contains("nvidia") && Build.MODEL.toLowerCase().contains("shield") && !Build.MODEL.toLowerCase().contains("tablet");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static List<String> m6386() {
        ArrayList arrayList = new ArrayList();
        try {
            if (Build.VERSION.SDK_INT < 21) {
                if (Build.CPU_ABI != null && !Build.CPU_ABI.trim().isEmpty() && !Build.CPU_ABI.equalsIgnoreCase(EnvironmentCompat.MEDIA_UNKNOWN)) {
                    arrayList.add(Build.CPU_ABI.trim().toLowerCase());
                }
                if (Build.CPU_ABI2 != null && !Build.CPU_ABI2.trim().isEmpty() && !Build.CPU_ABI2.equalsIgnoreCase(EnvironmentCompat.MEDIA_UNKNOWN)) {
                    arrayList.add(Build.CPU_ABI2.trim().toLowerCase());
                }
            } else if (Build.SUPPORTED_ABIS != null && Build.SUPPORTED_ABIS.length > 0) {
                for (String str : Build.SUPPORTED_ABIS) {
                    if (!str.isEmpty() && !str.equalsIgnoreCase(EnvironmentCompat.MEDIA_UNKNOWN)) {
                        arrayList.add(str.trim().toLowerCase());
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                Collections.sort(arrayList, new AlphanumComparator());
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m6387() {
        if (YearClass.m7609(TVApplication.m6288()) >= 2014) {
            if (m6385()) {
                return true;
            }
            if (!m6389(true)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6388() {
        boolean z = false;
        try {
            z = TVApplication.m6288().getPackageManager().hasSystemFeature("amazon.hardware.fire_tv");
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return z || (Build.MANUFACTURER.trim().toLowerCase().contains("amazon") && Build.MODEL.trim().toLowerCase().contains("aft"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6389(boolean... zArr) {
        boolean z = zArr != null && zArr.length > 0 && zArr[0];
        boolean z2 = Build.MANUFACTURER.equals("OUYA") && Build.MODEL.toLowerCase().startsWith("ouya_");
        boolean z3 = Build.MANUFACTURER.equalsIgnoreCase("xiaomi") && Build.MODEL.toLowerCase().contains("mibox");
        boolean z4 = Build.MANUFACTURER.equalsIgnoreCase("asus") && Build.MODEL.equalsIgnoreCase("nexus player");
        boolean z5 = Build.MANUFACTURER.equalsIgnoreCase("sony") && Build.MODEL.toLowerCase().contains("bravia");
        boolean equalsIgnoreCase = Build.MANUFACTURER.equalsIgnoreCase("amlogic");
        boolean equalsIgnoreCase2 = Build.MANUFACTURER.equalsIgnoreCase("MBX");
        boolean equalsIgnoreCase3 = Build.MANUFACTURER.equalsIgnoreCase("Netxeon");
        boolean equalsIgnoreCase4 = Build.MANUFACTURER.equalsIgnoreCase("DroidSticks");
        boolean equalsIgnoreCase5 = Build.MANUFACTURER.equalsIgnoreCase("MINIX");
        boolean z6 = Build.MANUFACTURER.equalsIgnoreCase("rockchip") || Build.MANUFACTURER.equalsIgnoreCase("Unblocktech") || Build.MANUFACTURER.equalsIgnoreCase("Skystream");
        boolean z7 = false;
        try {
            z7 = TVApplication.m6288().getResources().getBoolean(R.bool.is_tv_device);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return (!z && TVApplication.m6285().getBoolean("pref_force_tv_mode", false)) || m6388() || m6385() || z2 || z3 || z4 || z5 || equalsIgnoreCase || equalsIgnoreCase2 || equalsIgnoreCase3 || equalsIgnoreCase4 || equalsIgnoreCase5 || z6 || z7 || (TVApplication.m6288().getSystemService("uimode") != null && (TVApplication.m6288().getSystemService("uimode") instanceof UiModeManager) && ((UiModeManager) TVApplication.m6288().getSystemService("uimode")).getCurrentModeType() == 4) || (Build.VERSION.SDK_INT >= 21 && TVApplication.m6288().getPackageManager().hasSystemFeature("android.software.leanback"));
    }
}
