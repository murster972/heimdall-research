package com.typhoon.tv.utils;

import android.graphics.Typeface;
import com.typhoon.tv.TVApplication;

public class TypefaceUtils {
    /* renamed from: 靐  reason: contains not printable characters */
    public static Typeface m17838() {
        return TVApplication.m6289("fonts/Roboto-MediumItalic.ttf");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Typeface m17839() {
        return TVApplication.m6289("fonts/Roboto-Medium.ttf");
    }
}
