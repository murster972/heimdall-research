package com.typhoon.tv.utils;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.PointerIconCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.download.DownloadIDRecord;
import com.typhoon.tv.model.download.DownloadItem;
import com.typhoon.tv.model.media.MediaInfo;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.commons.lang3.StringUtils;

public class Downloader {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m17787(Context context, String url, String filePath, MediaInfo mediaInfo, String displayName, HashMap<String, String> headers) {
        String cookie;
        try {
            if (!m17786(context)) {
                return false;
            }
            String url2 = url.replace(StringUtils.SPACE, "%20");
            DownloadManager.Request req = new DownloadManager.Request(Uri.parse(url2));
            if (headers == null || headers.isEmpty()) {
                req.addRequestHeader(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                req.addRequestHeader("Cache-Control", "no-cache");
                req.addRequestHeader(AbstractSpiCall.HEADER_ACCEPT, "*/*");
                req.addRequestHeader("Accept-Language", "en-US;q=0.6,en;q=0.4");
                String cookie2 = HttpHelper.m6343().m6349(url2);
                if (cookie2 != null && !cookie2.isEmpty()) {
                    req.addRequestHeader("Cookie", cookie2);
                }
            } else {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    if (!(entry.getKey() == null || entry.getKey().isEmpty() || entry.getValue() == null)) {
                        req.addRequestHeader(entry.getKey(), entry.getValue());
                    }
                }
                if (!headers.containsKey(AbstractSpiCall.HEADER_USER_AGENT) && !headers.containsKey("user-agent")) {
                    req.addRequestHeader(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                }
                if (!headers.containsKey("Cache-Control") && !headers.containsKey("cache-control")) {
                    req.addRequestHeader("Cache-Control", "no-cache");
                }
                if (!headers.containsKey(AbstractSpiCall.HEADER_ACCEPT) && !headers.containsKey("accept")) {
                    req.addRequestHeader(AbstractSpiCall.HEADER_ACCEPT, "*/*");
                }
                if (!headers.containsKey("Accept-Language") && !headers.containsKey("Accept-Language")) {
                    req.addRequestHeader("Accept-Language", "en-US;q=0.6,en;q=0.4");
                }
                if (!headers.containsKey("Cookie") && !headers.containsKey("cookie") && (cookie = HttpHelper.m6343().m6349(url2)) != null && !cookie.isEmpty()) {
                    req.addRequestHeader("Cookie", cookie);
                }
            }
            String fileName = filePath;
            if (filePath.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                fileName = filePath.substring(filePath.lastIndexOf(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) + 1, filePath.length());
            }
            req.setTitle(fileName);
            req.setDescription("Downloading " + displayName + "From Typhoon");
            File destFolder = new File(TVApplication.m6285().getString("pref_media_down_path", Utils.m6397()));
            if (destFolder.exists() && !destFolder.isDirectory()) {
                Utils.m6421(destFolder);
                destFolder.mkdirs();
            } else if (!destFolder.exists()) {
                destFolder.mkdirs();
            }
            File saveFile = new File(destFolder.getAbsolutePath(), filePath);
            if (saveFile.exists()) {
                Utils.m6421(saveFile);
            }
            req.setDestinationUri(Uri.fromFile(saveFile));
            req.allowScanningByMediaScanner();
            req.setNotificationVisibility(1);
            try {
                long downloadId = ((DownloadManager) context.getSystemService("download")).enqueue(req);
                if (downloadId != -1) {
                    DownloadIDRecord idRecord = new DownloadIDRecord();
                    idRecord.setDownloadId(downloadId);
                    idRecord.setDownloadPath(saveFile.getAbsolutePath());
                    idRecord.setPosterUrl(mediaInfo.getPosterUrl() == null ? "" : mediaInfo.getPosterUrl());
                    TVApplication.m6287().m6314(idRecord);
                }
                return true;
            } catch (SecurityException ex) {
                Logger.m6281((Throwable) ex, true);
                Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.download_path_error), 0).show();
                TVApplication.m6285().edit().putString("pref_media_down_path", Utils.m6397()).apply();
                return false;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.error), 0).show();
                return false;
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            Toast.makeText(TVApplication.m6288(), I18N.m15706(R.string.error), 0).show();
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m17785(Context context, long downloadId) {
        if (m17786(TVApplication.m6288())) {
            try {
                ((DownloadManager) context.getSystemService("download")).remove(new long[]{downloadId});
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<DownloadItem> m17783() {
        return m17784(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static List<DownloadItem> m17779() {
        return m17784(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<DownloadItem> m17784(boolean inProgress) {
        List<DownloadItem> downloadItems = new ArrayList<>();
        if (m17786(TVApplication.m6288())) {
            DownloadManager downloadManager = (DownloadManager) TVApplication.m6288().getSystemService("download");
            for (DownloadIDRecord idRecord : TVApplication.m6287().m6302()) {
                try {
                    long downloadId = idRecord.getDownloadId();
                    String downloadPath = idRecord.getDownloadPath();
                    String posterUrl = idRecord.getPosterUrl();
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(new long[]{downloadId});
                    Cursor cursor = downloadManager.query(query);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                boolean isDownloadFailed = false;
                                int status = cursor.getInt(cursor.getColumnIndex(NotificationCompat.CATEGORY_STATUS));
                                if (status == 8 || status == 16) {
                                    if (inProgress) {
                                        if (cursor != null) {
                                            try {
                                                cursor.close();
                                            } catch (Exception e) {
                                                Logger.m6281((Throwable) e, new boolean[0]);
                                            }
                                        }
                                    } else if (status == 16) {
                                        isDownloadFailed = true;
                                    }
                                } else if ((status == 4 || status == 1 || status == 2) && !inProgress) {
                                    if (cursor != null) {
                                        try {
                                            cursor.close();
                                        } catch (Exception e2) {
                                            Logger.m6281((Throwable) e2, new boolean[0]);
                                        }
                                    }
                                }
                                String dTitle = cursor.getString(cursor.getColumnIndex(PubnativeAsset.TITLE));
                                long dTotalSizeInByte = cursor.getLong(cursor.getColumnIndex("total_size"));
                                DownloadItem item = new DownloadItem();
                                item.setDownloading(inProgress);
                                item.setDownloadId(downloadId);
                                item.setLocalUri(downloadPath);
                                item.setPosterUrl(posterUrl);
                                item.setTitle(dTitle);
                                item.setTotalSize(dTotalSizeInByte);
                                if (isDownloadFailed) {
                                    int downloadErrorCode = -999;
                                    try {
                                        downloadErrorCode = cursor.getInt(cursor.getColumnIndex("reason"));
                                    } catch (Exception e3) {
                                        Logger.m6281((Throwable) e3, new boolean[0]);
                                    }
                                    item.setErrorMessage(m17782(downloadErrorCode));
                                }
                                downloadItems.add(item);
                            }
                        } catch (Exception e4) {
                            Logger.m6281((Throwable) e4, new boolean[0]);
                            if (cursor != null) {
                                try {
                                    cursor.close();
                                } catch (Exception e5) {
                                    Logger.m6281((Throwable) e5, new boolean[0]);
                                }
                            }
                        } catch (Throwable th) {
                            if (cursor != null) {
                                try {
                                    cursor.close();
                                } catch (Exception e6) {
                                    Logger.m6281((Throwable) e6, new boolean[0]);
                                }
                            }
                            throw th;
                        }
                    }
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e7) {
                            Logger.m6281((Throwable) e7, new boolean[0]);
                        }
                    }
                } catch (Exception e8) {
                    Logger.m6281((Throwable) e8, new boolean[0]);
                }
            }
        }
        return downloadItems;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m17782(int code) {
        switch (code) {
            case -999:
            case 1000:
                return I18N.m15706(R.string.unknown);
            case 1001:
                return "A storage issue arose which didn't fit under any other error code.";
            case PointerIconCompat.TYPE_HAND:
                return "An HTTP code was received that download manager can't handle.";
            case PointerIconCompat.TYPE_WAIT:
                return "An error receiving or processing data occurred at the HTTP level.";
            case 1005:
                return "Too many redirects.";
            case PointerIconCompat.TYPE_CELL:
                return "There was insufficient storage space. Typically, this is because the SD card is full.";
            case PointerIconCompat.TYPE_CROSSHAIR:
                return "No external storage device was found. Typically, this is because the SD card is not mounted.";
            case PointerIconCompat.TYPE_TEXT:
                return "Some possibly transient error occurred but we can't resume the download.";
            case PointerIconCompat.TYPE_VERTICAL_TEXT:
                return "The requested destination file already exists (the download manager will not overwrite an existing file).";
            default:
                return "HTTP Error: Code=" + code;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m17786(Context context) {
        try {
            int state = context.getPackageManager().getApplicationEnabledSetting("com.android.providers.downloads");
            if (state == 2 || state == 3 || (Build.VERSION.SDK_INT >= 18 && state == 4)) {
                return false;
            }
            return true;
        } catch (Throwable e) {
            Logger.m6281(e, new boolean[0]);
            return true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m17780(Context context) {
        try {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setFlags(268435456);
            intent.setData(Uri.parse("package:com.android.providers.downloads"));
            context.startActivity(intent);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            try {
                Intent intent2 = new Intent("android.settings.MANAGE_APPLICATIONS_SETTINGS");
                intent2.setFlags(268435456);
                context.startActivity(intent2);
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                Toast.makeText(context, I18N.m15706(R.string.error), 1).show();
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static AlertDialog m17781(final Context context) {
        return new AlertDialog.Builder(context).m536((CharSequence) I18N.m15706(R.string.android_download_manager_is_disabled)).m523((CharSequence) I18N.m15706(R.string.android_download_manager_is_disabled_msg)).m538(false).m520((int) R.drawable.ic_warning_white_48dp).m537((CharSequence) I18N.m15706(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Downloader.m17780(context);
            }
        }).m524((CharSequence) I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if ((!(context instanceof Activity) || !((Activity) context).isFinishing()) && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        }).m525();
    }
}
