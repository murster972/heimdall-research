package com.typhoon.tv.utils;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import com.typhoon.tv.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IntentUtils {
    @SuppressLint({"WrongConstant"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static Intent m17788(Context context, Intent originalIntent, String title) {
        Intent chooserIntent = Intent.createChooser(originalIntent, title);
        try {
            List<Intent> targetedShareIntents = new ArrayList<>();
            Intent dummy = new Intent(originalIntent.getAction());
            dummy.setDataAndType(originalIntent.getData(), originalIntent.getType());
            List<ResolveInfo> resInfo = m17791(context, dummy);
            if (resInfo.isEmpty()) {
                return chooserIntent;
            }
            for (ResolveInfo resolveInfo : resInfo) {
                try {
                    if (resolveInfo.activityInfo != null) {
                        ActivityInfo ai = resolveInfo.activityInfo;
                        Intent intent = new Intent(originalIntent);
                        intent.addFlags(50331648);
                        intent.setComponent(new ComponentName(ai.applicationInfo.packageName, ai.name));
                        targetedShareIntents.add(intent);
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            chooserIntent.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
            return chooserIntent;
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<ResolveInfo> m17791(Context context, Intent oriIntent) {
        PackageManager packageManager = context.getPackageManager();
        Intent rawSchemeUriIntent = m17789(oriIntent);
        List<ResolveInfo> resInfoList = packageManager.queryIntentActivities(oriIntent, 0);
        if (resInfoList.isEmpty() && rawSchemeUriIntent != null) {
            resInfoList = packageManager.queryIntentActivities(rawSchemeUriIntent, 0);
        } else if (resInfoList.size() == 1) {
            ResolveInfo resolveInfo = resInfoList.get(0);
            if (resolveInfo != null && resolveInfo.activityInfo != null && !resolveInfo.activityInfo.packageName.equals(context.getPackageName())) {
                List<ResolveInfo> list = resInfoList;
                return resInfoList;
            } else if (rawSchemeUriIntent != null) {
                resInfoList = packageManager.queryIntentActivities(rawSchemeUriIntent, 0);
            } else {
                resInfoList = new ArrayList<>();
            }
        }
        if (!resInfoList.isEmpty()) {
            Collections.sort(resInfoList, new ResolveInfo.DisplayNameComparator(packageManager));
        }
        List<ResolveInfo> list2 = resInfoList;
        return resInfoList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Intent m17789(Intent intent) {
        if (intent.getData() == null) {
            return null;
        }
        return new Intent(intent).setData(m17790(intent.getData()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Uri m17790(Uri uri) {
        return new Uri.Builder().scheme(uri.getScheme()).build();
    }
}
