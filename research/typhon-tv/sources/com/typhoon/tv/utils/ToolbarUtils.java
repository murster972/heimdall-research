package com.typhoon.tv.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;

public class ToolbarUtils {
    @SuppressLint({"PrivateResource"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m17837(Context context, Toolbar toolbar) {
        try {
            toolbar.getLayoutParams().height = context.getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
    }
}
