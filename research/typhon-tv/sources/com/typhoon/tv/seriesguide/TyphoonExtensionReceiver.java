package com.typhoon.tv.seriesguide;

import com.battlelancer.seriesguide.api.SeriesGuideExtension;
import com.battlelancer.seriesguide.api.SeriesGuideExtensionReceiver;

public class TyphoonExtensionReceiver extends SeriesGuideExtensionReceiver {
    /* access modifiers changed from: protected */
    public Class<? extends SeriesGuideExtension> getExtensionClass() {
        return TyphoonExtensionService.class;
    }

    /* access modifiers changed from: protected */
    public int getJobId() {
        return 9109;
    }
}
