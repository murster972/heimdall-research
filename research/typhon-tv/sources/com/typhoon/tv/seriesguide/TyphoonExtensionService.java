package com.typhoon.tv.seriesguide;

import android.content.Intent;
import com.battlelancer.seriesguide.api.Action;
import com.battlelancer.seriesguide.api.Episode;
import com.battlelancer.seriesguide.api.Movie;
import com.battlelancer.seriesguide.api.SeriesGuideExtension;
import com.google.gson.Gson;
import com.typhoon.tv.Logger;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.ui.activity.SourceActivity;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TyphoonExtensionService extends SeriesGuideExtension {
    public TyphoonExtensionService() {
        super("TyphoonExtension");
    }

    /* access modifiers changed from: protected */
    public void onRequest(int i, Episode episode) {
        Logger.m6279("TyphoonExtensionService", "onRequest: episode " + episode.m25115().toString());
        String r13 = episode.m25114();
        int i2 = 0;
        if (r13 != null && !r13.isEmpty() && r13.length() >= 4) {
            String substring = r13.trim().substring(0, 4);
            if (!substring.contains("-") && Utils.m6426(substring)) {
                i2 = Integer.parseInt(substring);
            }
        }
        String r4 = episode.m25119();
        String r10 = Regex.m17800(r4, "(.*?)\\s+\\(\\d{4}\\)", 1);
        if (!r10.isEmpty()) {
            r4 = r10;
        }
        MediaInfo mediaInfo = new MediaInfo(0, 1, -1, r4, i2);
        try {
            if (episode.m25116() != null && !episode.m25116().isEmpty()) {
                String lowerCase = episode.m25116().toLowerCase();
                if (!lowerCase.startsWith(TtmlNode.TAG_TT)) {
                    lowerCase = TtmlNode.TAG_TT + lowerCase;
                }
                mediaInfo.setImdbId(lowerCase);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        try {
            if (episode.m25118() != null) {
                mediaInfo.setTvdbId(episode.m25118().intValue());
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        String json = new Gson().toJson((Object) mediaInfo, (Type) MediaInfo.class);
        int intValue = episode.m25117().intValue();
        int intValue2 = episode.m25120().intValue();
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("mediaInfo", json);
        intent.putExtra("season", intValue);
        intent.putExtra("episode", intValue2);
        intent.putExtra("isFromAnotherApp", true);
        publishAction(new Action.Builder("Watch on Typhoon", i).m25101(intent).m25102());
    }

    /* access modifiers changed from: protected */
    public void onRequest(int i, Movie movie) {
        Logger.m6279("TyphoonExtensionService", "onRequest: movie " + movie.m25137().toString());
        Date r10 = movie.m25139();
        int i2 = 0;
        if (r10 != null) {
            try {
                Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles"));
                if (instance != null) {
                    instance.setTime(r10);
                    i2 = instance.get(1);
                } else {
                    i2 = r10.getYear();
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        MediaInfo mediaInfo = new MediaInfo(1, 1, movie.m25138().intValue(), movie.m25141(), i2);
        try {
            if (movie.m25140() != null && !movie.m25140().isEmpty()) {
                String lowerCase = movie.m25140().toLowerCase();
                if (!lowerCase.startsWith(TtmlNode.TAG_TT)) {
                    lowerCase = TtmlNode.TAG_TT + lowerCase;
                }
                mediaInfo.setImdbId(lowerCase);
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        String json = new Gson().toJson((Object) mediaInfo, (Type) MediaInfo.class);
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("mediaInfo", json);
        publishAction(new Action.Builder("Watch on Typhoon", i).m25101(intent).m25102());
    }
}
