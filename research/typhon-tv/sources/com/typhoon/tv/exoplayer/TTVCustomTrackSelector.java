package com.typhoon.tv.exoplayer;

import android.text.TextUtils;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.FixedTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import java.util.HashSet;

public class TTVCustomTrackSelector extends DefaultTrackSelector {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int[] f12579 = new int[0];

    private static final class AudioConfigurationTuple {

        /* renamed from: 靐  reason: contains not printable characters */
        public final int f12580;

        /* renamed from: 齉  reason: contains not printable characters */
        public final String f12581;

        /* renamed from: 龘  reason: contains not printable characters */
        public final int f12582;

        public AudioConfigurationTuple(int i, int i2, String str) {
            this.f12582 = i;
            this.f12580 = i2;
            this.f12581 = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AudioConfigurationTuple audioConfigurationTuple = (AudioConfigurationTuple) obj;
            return this.f12582 == audioConfigurationTuple.f12582 && this.f12580 == audioConfigurationTuple.f12580 && TextUtils.equals(this.f12581, audioConfigurationTuple.f12581);
        }

        public int hashCode() {
            return (((this.f12582 * 31) + this.f12580) * 31) + (this.f12581 != null ? this.f12581.hashCode() : 0);
        }
    }

    public TTVCustomTrackSelector() {
    }

    public TTVCustomTrackSelector(BandwidthMeter bandwidthMeter) {
        super(bandwidthMeter);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m15862(int i, String str, Format format) {
        boolean z = (format.selectionFlags & 1) != 0;
        int i2 = formatHasLanguage(format, str) ? z ? 6 : 5 : (format.language == null || !TextUtils.equals("und", format.language)) ? z ? 2 : 1 : z ? 4 : 3;
        return isSupported(i, false) ? i2 + 1000 : i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m15863(TrackGroup trackGroup, int[] iArr, AudioConfigurationTuple audioConfigurationTuple) {
        int i = 0;
        for (int i2 = 0; i2 < trackGroup.length; i2++) {
            if (m15864(trackGroup.getFormat(i2), iArr[i2], audioConfigurationTuple)) {
                i++;
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m15864(Format format, int i, AudioConfigurationTuple audioConfigurationTuple) {
        if (isSupported(i, false) && format.channelCount == audioConfigurationTuple.f12582 && format.sampleRate == audioConfigurationTuple.f12580) {
            return audioConfigurationTuple.f12581 == null || TextUtils.equals(audioConfigurationTuple.f12581, format.sampleMimeType);
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int[] m15865(TrackGroup trackGroup, int[] iArr, boolean z) {
        int r2;
        int i = 0;
        AudioConfigurationTuple audioConfigurationTuple = null;
        HashSet hashSet = new HashSet();
        for (int i2 = 0; i2 < trackGroup.length; i2++) {
            Format format = trackGroup.getFormat(i2);
            AudioConfigurationTuple audioConfigurationTuple2 = new AudioConfigurationTuple(format.channelCount, format.sampleRate, z ? null : format.sampleMimeType);
            if (hashSet.add(audioConfigurationTuple2) && (r2 = m15863(trackGroup, iArr, audioConfigurationTuple2)) > i) {
                audioConfigurationTuple = audioConfigurationTuple2;
                i = r2;
            }
        }
        if (i <= 1) {
            return f12579;
        }
        int[] iArr2 = new int[i];
        int i3 = 0;
        for (int i4 = 0; i4 < trackGroup.length; i4++) {
            if (m15864(trackGroup.getFormat(i4), iArr[i4], audioConfigurationTuple)) {
                iArr2[i3] = i4;
                i3++;
            }
        }
        return iArr2;
    }

    /* access modifiers changed from: protected */
    public TrackSelection selectAudioTrack(TrackGroupArray trackGroupArray, int[][] iArr, DefaultTrackSelector.Parameters parameters, TrackSelection.Factory factory) throws ExoPlaybackException {
        int r11;
        int i = -1;
        int i2 = -1;
        int i3 = 0;
        for (int i4 = 0; i4 < trackGroupArray.length; i4++) {
            TrackGroup trackGroup = trackGroupArray.get(i4);
            int[] iArr2 = iArr[i4];
            for (int i5 = 0; i5 < trackGroup.length; i5++) {
                if (isSupported(iArr2[i5], parameters.exceedRendererCapabilitiesIfNecessary) && (r11 = m15862(iArr2[i5], parameters.preferredAudioLanguage, trackGroup.getFormat(i5))) > i3) {
                    i = i4;
                    i2 = i5;
                    i3 = r11;
                }
            }
        }
        if (i == -1) {
            return null;
        }
        TrackGroup trackGroup2 = trackGroupArray.get(i);
        if (factory != null) {
            int[] r1 = m15865(trackGroup2, iArr[i], parameters.allowMixedMimeAdaptiveness);
            if (r1.length > 0) {
                return factory.createTrackSelection(trackGroup2, r1);
            }
        }
        return new FixedTrackSelection(trackGroup2, i2);
    }

    /* access modifiers changed from: protected */
    public TrackSelection selectTextTrack(TrackGroupArray trackGroupArray, int[][] iArr, DefaultTrackSelector.Parameters parameters) throws ExoPlaybackException {
        int i;
        TrackGroup trackGroup = null;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < trackGroupArray.length; i4++) {
            TrackGroup trackGroup2 = trackGroupArray.get(i4);
            int[] iArr2 = iArr[i4];
            for (int i5 = 0; i5 < trackGroup2.length; i5++) {
                if (isSupported(iArr2[i5], parameters.exceedRendererCapabilitiesIfNecessary)) {
                    Format format = trackGroup2.getFormat(i5);
                    boolean z = (format.selectionFlags & 1) != 0;
                    boolean z2 = (format.selectionFlags & 2) != 0;
                    if (TrackSelectionHelper.m15904(format)) {
                        if (formatHasLanguage(format, parameters.preferredTextLanguage)) {
                            i = z ? 6 : !z2 ? 5 : 4;
                        } else if (z) {
                            i = 3;
                        } else if (z2) {
                            i = formatHasLanguage(format, parameters.preferredAudioLanguage) ? 2 : 1;
                        }
                        if (isSupported(iArr2[i5], false)) {
                            i += 1000;
                        }
                        if (i > i3) {
                            trackGroup = trackGroup2;
                            i2 = i5;
                            i3 = i;
                        }
                    }
                }
            }
        }
        if (trackGroup == null) {
            return null;
        }
        return new FixedTrackSelection(trackGroup, i2);
    }
}
