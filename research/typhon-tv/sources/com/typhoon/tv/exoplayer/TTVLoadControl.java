package com.typhoon.tv.exoplayer;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import com.google.android.exoplayer2.util.Util;

public final class TTVLoadControl implements LoadControl {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final PriorityTaskManager f12590;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f12591;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f12592;

    /* renamed from: 连任  reason: contains not printable characters */
    private final long f12593;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f12594;

    /* renamed from: 麤  reason: contains not printable characters */
    private final long f12595;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f12596;

    /* renamed from: 龘  reason: contains not printable characters */
    private final DefaultAllocator f12597;

    public TTVLoadControl() {
        this(new DefaultAllocator(true, 65536));
    }

    public TTVLoadControl(DefaultAllocator defaultAllocator) {
        this(defaultAllocator, 15000, 30000, 2500, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
    }

    public TTVLoadControl(DefaultAllocator defaultAllocator, int i, int i2, long j, long j2) {
        this(defaultAllocator, i, i2, j, j2, (PriorityTaskManager) null);
    }

    public TTVLoadControl(DefaultAllocator defaultAllocator, int i, int i2, long j, long j2, PriorityTaskManager priorityTaskManager) {
        this.f12597 = defaultAllocator;
        this.f12594 = ((long) i) * 1000;
        this.f12596 = ((long) i2) * 1000;
        this.f12595 = j * 1000;
        this.f12593 = j2 * 1000;
        this.f12590 = priorityTaskManager;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m15886(long j) {
        if (j > this.f12596) {
            return 0;
        }
        return j < this.f12594 ? 2 : 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15887(boolean z) {
        this.f12591 = 0;
        if (this.f12590 != null && this.f12592) {
            this.f12590.remove(0);
        }
        this.f12592 = false;
        if (z) {
            this.f12597.reset();
        }
    }

    public Allocator getAllocator() {
        return this.f12597;
    }

    public void onPrepared() {
        m15887(false);
    }

    public void onReleased() {
        m15887(true);
    }

    public void onStopped() {
        m15887(true);
    }

    public void onTracksSelected(Renderer[] rendererArr, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        this.f12591 = 0;
        for (int i = 0; i < rendererArr.length; i++) {
            if (trackSelectionArray.get(i) != null) {
                this.f12591 += Util.getDefaultBufferSize(rendererArr[i].getTrackType());
            }
        }
        this.f12597.setTargetBufferSize(this.f12591);
    }

    public boolean shouldContinueLoading(long j) {
        boolean z = true;
        int r0 = m15886(j);
        boolean z2 = this.f12597.getTotalBytesAllocated() >= this.f12591;
        boolean z3 = this.f12592;
        if (r0 != 2 && (r0 != 1 || z2)) {
            z = false;
        }
        this.f12592 = z;
        if (!(this.f12590 == null || this.f12592 == z3)) {
            if (this.f12592) {
                this.f12590.add(0);
            } else {
                this.f12590.remove(0);
            }
        }
        return this.f12592;
    }

    public boolean shouldStartPlayback(long j, boolean z) {
        long j2 = z ? this.f12593 : this.f12595;
        return j2 <= 0 || j >= j2;
    }
}
