package com.typhoon.tv.exoplayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.FixedTrackSelection;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.util.MimeTypes;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.utils.Utils;
import java.util.Arrays;

public final class TrackSelectionHelper implements DialogInterface.OnClickListener, View.OnClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final TrackSelection.Factory f12600 = new RandomTrackSelection.Factory();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final TrackSelection.Factory f12601 = new FixedTrackSelection.Factory();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f12602;

    /* renamed from: ʼ  reason: contains not printable characters */
    private TrackGroupArray f12603;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean[] f12604;

    /* renamed from: ʾ  reason: contains not printable characters */
    private CheckedTextView f12605;

    /* renamed from: ʿ  reason: contains not printable characters */
    private CheckedTextView[][] f12606;

    /* renamed from: ˈ  reason: contains not printable characters */
    private CheckedTextView f12607;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f12608;

    /* renamed from: ٴ  reason: contains not printable characters */
    private MappingTrackSelector.SelectionOverride f12609;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private CheckedTextView f12610;

    /* renamed from: 连任  reason: contains not printable characters */
    private MappingTrackSelector.MappedTrackInfo f12611;

    /* renamed from: 麤  reason: contains not printable characters */
    private final TrackSelection.Factory f12612;

    /* renamed from: 齉  reason: contains not printable characters */
    private final MappingTrackSelector f12613;

    public TrackSelectionHelper(MappingTrackSelector mappingTrackSelector, TrackSelection.Factory factory) {
        this.f12613 = mappingTrackSelector;
        this.f12612 = factory;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int[] m15898(MappingTrackSelector.SelectionOverride selectionOverride, int i) {
        int[] iArr = new int[(selectionOverride.length - 1)];
        int i2 = 0;
        for (int i3 = 0; i3 < iArr.length + 1; i3++) {
            int i4 = selectionOverride.tracks[i3];
            if (i4 != i) {
                iArr[i2] = i4;
                i2++;
            }
        }
        return iArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m15899(Player player, MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int i) {
        if (mappedTrackInfo != null) {
            for (int i2 = 0; i2 < mappedTrackInfo.length; i2++) {
                if (mappedTrackInfo.getTrackGroups(i2).length != 0 && player.getRendererType(i2) == i) {
                    return i2;
                }
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m15900(MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int i) {
        int i2 = 0;
        if (mappedTrackInfo != null && i > -1) {
            try {
                TrackGroupArray trackGroups = mappedTrackInfo.getTrackGroups(i);
                for (int i3 = 0; i3 < trackGroups.length; i3++) {
                    TrackGroup trackGroup = trackGroups.get(i3);
                    for (int i4 = 0; i4 < trackGroup.length; i4++) {
                        if (mappedTrackInfo.getTrackFormatSupport(i, i3, i4) == 4) {
                            i2++;
                        }
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return i2;
    }

    @SuppressLint({"InflateParams"})
    /* renamed from: 龘  reason: contains not printable characters */
    private View m15901(Context context, boolean z) {
        LayoutInflater from = LayoutInflater.from(context);
        View inflate = from.inflate(R.layout.track_selection_dialog, (ViewGroup) null);
        ViewGroup viewGroup = (ViewGroup) inflate.findViewById(R.id.root);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{16843534});
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        this.f12610 = (CheckedTextView) from.inflate(17367055, viewGroup, false);
        this.f12610.setBackgroundResource(resourceId);
        this.f12610.setText(I18N.m15706(R.string.exo_player_disabled));
        this.f12610.setFocusable(true);
        this.f12610.setOnClickListener(this);
        viewGroup.addView(this.f12610);
        this.f12607 = (CheckedTextView) from.inflate(17367055, viewGroup, false);
        this.f12607.setBackgroundResource(resourceId);
        this.f12607.setText(I18N.m15706(R.string.exo_player_default));
        this.f12607.setFocusable(true);
        this.f12607.setOnClickListener(this);
        viewGroup.addView(from.inflate(R.layout.list_divider, viewGroup, false));
        viewGroup.addView(this.f12607);
        boolean z2 = false;
        boolean z3 = false;
        this.f12606 = new CheckedTextView[this.f12603.length][];
        for (int i = 0; i < this.f12603.length; i++) {
            TrackGroup trackGroup = this.f12603.get(i);
            boolean z4 = this.f12604[i];
            z3 |= z4;
            this.f12606[i] = new CheckedTextView[trackGroup.length];
            for (int i2 = 0; i2 < trackGroup.length; i2++) {
                if (i2 == 0) {
                    viewGroup.addView(from.inflate(R.layout.list_divider, viewGroup, false));
                }
                CheckedTextView checkedTextView = (CheckedTextView) from.inflate(z4 ? 17367056 : 17367055, viewGroup, false);
                checkedTextView.setBackgroundResource(resourceId);
                checkedTextView.setText(TrackNameHelper.m15896(trackGroup.getFormat(i2)));
                if (this.f12611.getTrackFormatSupport(this.f12602, i, i2) == 4) {
                    checkedTextView.setFocusable(true);
                    checkedTextView.setTag(Pair.create(Integer.valueOf(i), Integer.valueOf(i2)));
                    checkedTextView.setOnClickListener(this);
                    Format format = trackGroup.getFormat(i2);
                    boolean z5 = this.f12609 == null && (((format.sampleMimeType.equals(MimeTypes.APPLICATION_SUBRIP) || format.sampleMimeType.equals(MimeTypes.TEXT_SSA) || format.sampleMimeType.equals(MimeTypes.APPLICATION_TTML) || format.sampleMimeType.equals(MimeTypes.TEXT_VTT)) && format.selectionFlags == 1 && m15904(format)) || z);
                    if (!z2 && z5 && !this.f12608) {
                        m15907((View) checkedTextView, false);
                        z2 = true;
                    }
                } else {
                    checkedTextView.setFocusable(false);
                    checkedTextView.setEnabled(false);
                }
                this.f12606[i][i2] = checkedTextView;
                viewGroup.addView(checkedTextView);
            }
        }
        if (z3) {
            this.f12605 = (CheckedTextView) from.inflate(17367056, viewGroup, false);
            this.f12605.setBackgroundResource(resourceId);
            this.f12605.setText("Enable random adaptation");
            this.f12605.setOnClickListener(this);
            viewGroup.addView(from.inflate(R.layout.list_divider, viewGroup, false));
            viewGroup.addView(this.f12605);
        }
        m15902();
        return inflate;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15902() {
        boolean z = true;
        this.f12610.setChecked(this.f12608);
        this.f12607.setChecked(!this.f12608 && this.f12609 == null);
        int i = 0;
        while (i < this.f12606.length) {
            for (int i2 = 0; i2 < this.f12606[i].length; i2++) {
                this.f12606[i][i2].setChecked(this.f12609 != null && this.f12609.groupIndex == i && this.f12609.containsTrack(i2));
            }
            i++;
        }
        if (this.f12605 != null) {
            boolean z2 = !this.f12608 && this.f12609 != null && this.f12609.length > 1;
            this.f12605.setEnabled(z2);
            this.f12605.setFocusable(z2);
            if (z2) {
                CheckedTextView checkedTextView = this.f12605;
                if (this.f12608 || !(this.f12609.factory instanceof RandomTrackSelection.Factory)) {
                    z = false;
                }
                checkedTextView.setChecked(z);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15903(int i, int[] iArr, boolean z) {
        this.f12609 = new MappingTrackSelector.SelectionOverride(iArr.length == 1 ? f12601 : z ? f12600 : this.f12612, i, iArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15904(Format format) {
        String str = format.id;
        return str != null && !str.isEmpty() && !Utils.m6426(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int[] m15905(MappingTrackSelector.SelectionOverride selectionOverride, int i) {
        int[] iArr = selectionOverride.tracks;
        int[] copyOf = Arrays.copyOf(iArr, iArr.length + 1);
        copyOf[copyOf.length - 1] = i;
        return copyOf;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f12613.setRendererDisabled(this.f12602, this.f12608);
        if (this.f12609 != null) {
            this.f12613.setSelectionOverride(this.f12602, this.f12603, this.f12609);
        } else {
            this.f12613.clearSelectionOverrides(this.f12602);
        }
    }

    public void onClick(View view) {
        m15907(view, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15906(Activity activity, CharSequence charSequence, MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int i, boolean z) {
        this.f12611 = mappedTrackInfo;
        this.f12602 = i;
        this.f12603 = mappedTrackInfo.getTrackGroups(i);
        this.f12604 = new boolean[this.f12603.length];
        for (int i2 = 0; i2 < this.f12603.length; i2++) {
            this.f12604[i2] = (this.f12612 == null || mappedTrackInfo.getAdaptiveSupport(i, i2, false) == 0 || this.f12603.get(i2).length <= 1) ? false : true;
        }
        this.f12608 = this.f12613.getRendererDisabled(i);
        this.f12609 = this.f12613.getSelectionOverride(i, this.f12603);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(charSequence).setView(m15901(builder.getContext(), z)).setPositiveButton(I18N.m15706(R.string.ok), this).setNegativeButton(I18N.m15706(R.string.cancel), (DialogInterface.OnClickListener) null).create().show();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15907(View view, boolean z) {
        if (view == this.f12610) {
            this.f12608 = true;
            this.f12609 = null;
        } else if (view == this.f12607) {
            this.f12608 = false;
            this.f12609 = null;
        } else if (view == this.f12605) {
            m15903(this.f12609.groupIndex, this.f12609.tracks, !this.f12605.isChecked());
        } else {
            this.f12608 = false;
            Pair pair = (Pair) view.getTag();
            if (pair != null) {
                int intValue = ((Integer) pair.first).intValue();
                int intValue2 = ((Integer) pair.second).intValue();
                if (!this.f12604[intValue] || this.f12609 == null || this.f12609.groupIndex != intValue) {
                    this.f12609 = new MappingTrackSelector.SelectionOverride(f12601, intValue, intValue2);
                } else {
                    boolean isChecked = ((CheckedTextView) view).isChecked();
                    int i = this.f12609.length;
                    if (!isChecked) {
                        m15903(intValue, m15905(this.f12609, intValue2), this.f12605.isChecked());
                    } else if (i == 1) {
                        this.f12609 = null;
                        this.f12608 = true;
                    } else {
                        m15903(intValue, m15898(this.f12609, intValue2), this.f12605.isChecked());
                    }
                }
            }
        }
        if (z) {
            m15902();
        }
    }
}
