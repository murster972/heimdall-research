package com.typhoon.tv.exoplayer;

import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataRenderer;
import com.google.android.exoplayer2.metadata.emsg.EventMessage;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.metadata.id3.CommentFrame;
import com.google.android.exoplayer2.metadata.id3.GeobFrame;
import com.google.android.exoplayer2.metadata.id3.Id3Frame;
import com.google.android.exoplayer2.metadata.id3.PrivFrame;
import com.google.android.exoplayer2.metadata.id3.TextInformationFrame;
import com.google.android.exoplayer2.metadata.id3.UrlLinkFrame;
import com.google.android.exoplayer2.source.AdaptiveMediaSourceEventListener;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

public final class ExoEventLogger implements Player.EventListener, AudioRendererEventListener, DefaultDrmSessionManager.EventListener, MetadataRenderer.Output, AdaptiveMediaSourceEventListener, ExtractorMediaSource.EventListener, VideoRendererEventListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final NumberFormat f12574 = NumberFormat.getInstance(Locale.US);

    /* renamed from: 连任  reason: contains not printable characters */
    private final long f12575 = SystemClock.elapsedRealtime();

    /* renamed from: 靐  reason: contains not printable characters */
    private final MappingTrackSelector f12576;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Timeline.Period f12577 = new Timeline.Period();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Timeline.Window f12578 = new Timeline.Window();

    static {
        f12574.setMinimumFractionDigits(2);
        f12574.setMaximumFractionDigits(2);
        f12574.setGroupingUsed(false);
    }

    public ExoEventLogger(MappingTrackSelector mappingTrackSelector) {
        this.f12576 = mappingTrackSelector;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m15853(int i) {
        switch (i) {
            case 0:
                return "NO";
            case 1:
                return "NO_UNSUPPORTED_TYPE";
            case 3:
                return "NO_EXCEEDS_CAPABILITIES";
            case 4:
                return "YES";
            default:
                return "?";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m15854() {
        return m15857(SystemClock.elapsedRealtime() - this.f12575);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15855(int i) {
        switch (i) {
            case 1:
                return "I";
            case 2:
                return "B";
            case 3:
                return "R";
            case 4:
                return "E";
            default:
                return "?";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15856(int i, int i2) {
        if (i < 2) {
            return "N/A";
        }
        switch (i2) {
            case 0:
                return "NO";
            case 8:
                return "YES_NOT_SEAMLESS";
            case 16:
                return "YES";
            default:
                return "?";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15857(long j) {
        return j == C.TIME_UNSET ? "?" : f12574.format((double) (((float) j) / 1000.0f));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15858(TrackSelection trackSelection, TrackGroup trackGroup, int i) {
        return m15859((trackSelection == null || trackSelection.getTrackGroup() != trackGroup || trackSelection.indexOf(i) == -1) ? false : true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15859(boolean z) {
        return z ? "[X]" : "[ ]";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15860(Metadata metadata, String str) {
        for (int i = 0; i < metadata.length(); i++) {
            Metadata.Entry entry = metadata.get(i);
            if (entry instanceof TextInformationFrame) {
                TextInformationFrame textInformationFrame = (TextInformationFrame) entry;
                Log.d("ExoEventLogger", str + String.format("%s: value=%s", new Object[]{textInformationFrame.id, textInformationFrame.value}));
            } else if (entry instanceof UrlLinkFrame) {
                UrlLinkFrame urlLinkFrame = (UrlLinkFrame) entry;
            } else if (entry instanceof PrivFrame) {
                PrivFrame privFrame = (PrivFrame) entry;
            } else if (entry instanceof GeobFrame) {
                GeobFrame geobFrame = (GeobFrame) entry;
                Log.d("ExoEventLogger", str + String.format("%s: mimeType=%s, filename=%s, description=%s", new Object[]{geobFrame.id, geobFrame.mimeType, geobFrame.filename, geobFrame.description}));
            } else if (entry instanceof ApicFrame) {
                ApicFrame apicFrame = (ApicFrame) entry;
                Log.d("ExoEventLogger", str + String.format("%s: mimeType=%s, description=%s", new Object[]{apicFrame.id, apicFrame.mimeType, apicFrame.description}));
            } else if (entry instanceof CommentFrame) {
                CommentFrame commentFrame = (CommentFrame) entry;
                Log.d("ExoEventLogger", str + String.format("%s: language=%s, description=%s", new Object[]{commentFrame.id, commentFrame.language, commentFrame.description}));
            } else if (entry instanceof Id3Frame) {
                Id3Frame id3Frame = (Id3Frame) entry;
            } else if (entry instanceof EventMessage) {
                EventMessage eventMessage = (EventMessage) entry;
                Log.d("ExoEventLogger", str + String.format("EMSG: scheme=%s, id=%d, value=%s", new Object[]{eventMessage.schemeIdUri, Long.valueOf(eventMessage.id), eventMessage.value}));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15861(String str, Exception exc) {
        Log.e("ExoEventLogger", "internalError [" + m15854() + ", " + str + "]", exc);
    }

    public void onAudioDecoderInitialized(String str, long j, long j2) {
    }

    public void onAudioDisabled(DecoderCounters decoderCounters) {
    }

    public void onAudioEnabled(DecoderCounters decoderCounters) {
    }

    public void onAudioInputFormatChanged(Format format) {
    }

    public void onAudioSessionId(int i) {
    }

    public void onAudioTrackUnderrun(int i, long j, long j2) {
        m15861("audioTrackUnderrun [" + i + ", " + j + ", " + j2 + "]", (Exception) null);
    }

    public void onDownstreamFormatChanged(int i, Format format, int i2, Object obj, long j) {
    }

    public void onDrmKeysLoaded() {
    }

    public void onDrmKeysRemoved() {
    }

    public void onDrmKeysRestored() {
    }

    public void onDrmSessionManagerError(Exception exc) {
        m15861("drmSessionManagerError", exc);
    }

    public void onDroppedFrames(int i, long j) {
    }

    public void onLoadCanceled(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5) {
    }

    public void onLoadCompleted(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5) {
    }

    public void onLoadError(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5, IOException iOException, boolean z) {
        m15861("loadError", (Exception) iOException);
    }

    public void onLoadError(IOException iOException) {
        m15861("loadError", (Exception) iOException);
    }

    public void onLoadStarted(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3) {
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onMetadata(Metadata metadata) {
        m15860(metadata, "  ");
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        Log.d("ExoEventLogger", "playbackParameters " + String.format("[speed=%.2f, pitch=%.2f]", new Object[]{Float.valueOf(playbackParameters.speed), Float.valueOf(playbackParameters.pitch)}));
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        Log.e("ExoEventLogger", "playerFailed [" + m15854() + "]", exoPlaybackException);
    }

    public void onPlayerStateChanged(boolean z, int i) {
        Log.d("ExoEventLogger", "state [" + m15854() + ", " + z + ", " + m15855(i) + "]");
    }

    public void onPositionDiscontinuity() {
    }

    public void onRenderedFirstFrame(Surface surface) {
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onTimelineChanged(Timeline timeline, Object obj) {
        int periodCount = timeline.getPeriodCount();
        int windowCount = timeline.getWindowCount();
        for (int i = 0; i < Math.min(periodCount, 3); i++) {
            timeline.getPeriod(i, this.f12577);
        }
        if (periodCount > 3) {
        }
        for (int i2 = 0; i2 < Math.min(windowCount, 3); i2++) {
            timeline.getWindow(i2, this.f12578);
            Log.d("ExoEventLogger", "  window [" + m15857(this.f12578.getDurationMs()) + ", " + this.f12578.isSeekable + ", " + this.f12578.isDynamic + "]");
        }
        if (windowCount > 3) {
        }
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        MappingTrackSelector.MappedTrackInfo currentMappedTrackInfo = this.f12576.getCurrentMappedTrackInfo();
        if (currentMappedTrackInfo != null) {
            for (int i = 0; i < currentMappedTrackInfo.length; i++) {
                TrackGroupArray trackGroups = currentMappedTrackInfo.getTrackGroups(i);
                TrackSelection trackSelection = trackSelectionArray.get(i);
                if (trackGroups.length > 0) {
                    for (int i2 = 0; i2 < trackGroups.length; i2++) {
                        TrackGroup trackGroup = trackGroups.get(i2);
                        String r1 = m15856(trackGroup.length, currentMappedTrackInfo.getAdaptiveSupport(i, i2, false));
                        for (int i3 = 0; i3 < trackGroup.length; i3++) {
                            String r9 = m15858(trackSelection, trackGroup, i3);
                            Log.d("ExoEventLogger", "      " + r9 + " Track:" + i3 + ", " + Format.toLogString(trackGroup.getFormat(i3)) + ", supported=" + m15853(currentMappedTrackInfo.getTrackFormatSupport(i, i2, i3)));
                        }
                    }
                    if (trackSelection != null) {
                        int i4 = 0;
                        while (true) {
                            if (i4 >= trackSelection.length()) {
                                break;
                            }
                            Metadata metadata = trackSelection.getFormat(i4).metadata;
                            if (metadata != null) {
                                m15860(metadata, "      ");
                                break;
                            }
                            i4++;
                        }
                    }
                }
            }
            TrackGroupArray unassociatedTrackGroups = currentMappedTrackInfo.getUnassociatedTrackGroups();
            if (unassociatedTrackGroups.length > 0) {
                for (int i5 = 0; i5 < unassociatedTrackGroups.length; i5++) {
                    TrackGroup trackGroup2 = unassociatedTrackGroups.get(i5);
                    for (int i6 = 0; i6 < trackGroup2.length; i6++) {
                        String r92 = m15859(false);
                        Log.d("ExoEventLogger", "      " + r92 + " Track:" + i6 + ", " + Format.toLogString(trackGroup2.getFormat(i6)) + ", supported=" + m15853(0));
                    }
                }
            }
        }
    }

    public void onUpstreamDiscarded(int i, long j, long j2) {
    }

    public void onVideoDecoderInitialized(String str, long j, long j2) {
    }

    public void onVideoDisabled(DecoderCounters decoderCounters) {
    }

    public void onVideoEnabled(DecoderCounters decoderCounters) {
    }

    public void onVideoInputFormatChanged(Format format) {
    }

    public void onVideoSizeChanged(int i, int i2, int i3, float f) {
    }
}
