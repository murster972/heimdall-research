package com.typhoon.tv.exoplayer;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.video.MediaCodecVideoRenderer;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.typhoon.tv.Logger;
import java.util.ArrayList;

public class TTVRenderersFactory extends DefaultRenderersFactory {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f12598 = false;

    public TTVRenderersFactory(Context context, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, int i) {
        super(context, drmSessionManager, i);
    }

    /* access modifiers changed from: protected */
    public void buildVideoRenderers(Context context, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, long j, Handler handler, VideoRendererEventListener videoRendererEventListener, int i, ArrayList<Renderer> arrayList) {
        arrayList.add(new MediaCodecVideoRenderer(context, this.f12598 ? new MediaCodecSelector() {
            public MediaCodecInfo getDecoderInfo(String str, boolean z) throws MediaCodecUtil.DecoderQueryException {
                MediaCodecInfo mediaCodecInfo = null;
                try {
                    mediaCodecInfo = TTVCustomVideoCodecUtil.m15866(str, z, TTVRenderersFactory.this.f12598);
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                if (mediaCodecInfo != null) {
                    return mediaCodecInfo;
                }
                try {
                    return MediaCodecUtil.getDecoderInfo(str, z);
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                    throw e2;
                }
            }

            public MediaCodecInfo getPassthroughDecoderInfo() throws MediaCodecUtil.DecoderQueryException {
                return MediaCodecUtil.getPassthroughDecoderInfo();
            }
        } : MediaCodecSelector.DEFAULT, j, drmSessionManager, false, handler, videoRendererEventListener, 50));
        if (i != 0) {
            int size = arrayList.size();
            int i2 = i == 2 ? size - 1 : size;
            try {
                int i3 = i2 + 1;
                try {
                    arrayList.add(i2, (Renderer) Class.forName("com.google.android.exoplayer2.ext.vp9.LibvpxVideoRenderer").getConstructor(new Class[]{Boolean.TYPE, Long.TYPE, Handler.class, VideoRendererEventListener.class, Integer.TYPE}).newInstance(new Object[]{true, Long.valueOf(j), handler, videoRendererEventListener, 50}));
                    Log.i("DefaultRenderersFactory", "Loaded LibvpxVideoRenderer.");
                } catch (ClassNotFoundException e) {
                } catch (Exception e2) {
                    e = e2;
                    throw new RuntimeException(e);
                }
            } catch (ClassNotFoundException e3) {
                int i4 = i2;
            } catch (Exception e4) {
                e = e4;
                int i5 = i2;
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15889(boolean z) {
        this.f12598 = z;
    }
}
