package com.typhoon.tv.exoplayer;

import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.Locale;

public class TrackNameHelper {
    /* renamed from: ʻ  reason: contains not printable characters */
    private static String m15890(Format format) {
        return format.id == null ? "" : format.id;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static String m15891(Format format) {
        return format.sampleMimeType == null ? "" : format.sampleMimeType;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static String m15892(Format format) {
        if (format.bitrate == -1) {
            return "";
        }
        return String.format(Locale.US, "%.2fMbit", new Object[]{Float.valueOf(((float) format.bitrate) / 1000000.0f)});
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m15893(Format format) {
        return (format.width == -1 || format.height == -1) ? "" : format.width + "x" + format.height;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static String m15894(Format format) {
        return (TextUtils.isEmpty(format.language) || "und".equals(format.language)) ? "" : format.language;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m15895(Format format) {
        return (format.channelCount == -1 || format.sampleRate == -1) ? "" : format.channelCount + "ch, " + format.sampleRate + "Hz";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15896(Format format) {
        String str;
        if (MimeTypes.isVideo(format.sampleMimeType)) {
            str = m15897(m15897(m15893(format), m15892(format)), m15891(format));
        } else if (MimeTypes.isAudio(format.sampleMimeType)) {
            str = m15897(m15897(m15897(m15894(format), m15895(format)), m15892(format)), m15891(format));
        } else {
            String r0 = m15894(format);
            str = (r0.isEmpty() ? "" : r0 + ": ") + m15890(format);
        }
        return str.length() == 0 ? EnvironmentCompat.MEDIA_UNKNOWN : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15897(String str, String str2) {
        return str.length() == 0 ? str2 : str2.length() == 0 ? str : str + ", " + str2;
    }
}
