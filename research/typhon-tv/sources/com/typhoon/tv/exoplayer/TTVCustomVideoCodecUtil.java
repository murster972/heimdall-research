package com.typhoon.tv.exoplayer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.typhoon.tv.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressLint({"InlinedApi"})
@TargetApi(16)
public final class TTVCustomVideoCodecUtil {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final SparseIntArray f12583 = new SparseIntArray();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final SparseIntArray f12584 = new SparseIntArray();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final HashMap<CodecKey, List<MediaCodecInfo>> f12585 = new HashMap<>();

    private static final class CodecKey {

        /* renamed from: 靐  reason: contains not printable characters */
        public final boolean f12586;

        /* renamed from: 龘  reason: contains not printable characters */
        public final String f12587;

        public CodecKey(String str, boolean z) {
            this.f12587 = str;
            this.f12586 = z;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || obj.getClass() != CodecKey.class) {
                return false;
            }
            CodecKey codecKey = (CodecKey) obj;
            return TextUtils.equals(this.f12587, codecKey.f12587) && this.f12586 == codecKey.f12586;
        }

        public int hashCode() {
            return (((this.f12587 == null ? 0 : this.f12587.hashCode()) + 31) * 31) + (this.f12586 ? 1231 : 1237);
        }
    }

    public static class DecoderQueryException extends Exception {
        private DecoderQueryException(Throwable th) {
            super("Failed to query underlying media codecs", th);
        }
    }

    private interface MediaCodecListCompat {
        /* renamed from: 靐  reason: contains not printable characters */
        boolean m15873();

        /* renamed from: 龘  reason: contains not printable characters */
        int m15874();

        /* renamed from: 龘  reason: contains not printable characters */
        android.media.MediaCodecInfo m15875(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m15876(String str, MediaCodecInfo.CodecCapabilities codecCapabilities);
    }

    private static final class MediaCodecListCompatV16 implements MediaCodecListCompat {
        private MediaCodecListCompatV16() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m15877() {
            return false;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m15878() {
            return MediaCodecList.getCodecCount();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public android.media.MediaCodecInfo m15879(int i) {
            return MediaCodecList.getCodecInfoAt(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m15880(String str, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return MimeTypes.VIDEO_H264.equals(str);
        }
    }

    @TargetApi(21)
    private static final class MediaCodecListCompatV21 implements MediaCodecListCompat {

        /* renamed from: 靐  reason: contains not printable characters */
        private android.media.MediaCodecInfo[] f12588;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f12589;

        public MediaCodecListCompatV21(boolean z) {
            this.f12589 = z ? 1 : 0;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m15881() {
            if (this.f12588 == null) {
                this.f12588 = new MediaCodecList(this.f12589).getCodecInfos();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m15882() {
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m15883() {
            m15881();
            return this.f12588.length;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public android.media.MediaCodecInfo m15884(int i) {
            m15881();
            return this.f12588[i];
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m15885(String str, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return codecCapabilities.isFeatureSupported("secure-playback");
        }
    }

    static {
        f12583.put(66, 1);
        f12583.put(77, 2);
        f12583.put(88, 4);
        f12583.put(100, 8);
        f12584.put(10, 1);
        f12584.put(11, 4);
        f12584.put(12, 8);
        f12584.put(13, 16);
        f12584.put(20, 32);
        f12584.put(21, 64);
        f12584.put(22, 128);
        f12584.put(30, 256);
        f12584.put(31, 512);
        f12584.put(32, 1024);
        f12584.put(40, 2048);
        f12584.put(41, 4096);
        f12584.put(42, 8192);
        f12584.put(50, 16384);
        f12584.put(51, 32768);
        f12584.put(52, 65536);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static com.google.android.exoplayer2.mediacodec.MediaCodecInfo m15866(String str, boolean z, boolean z2) throws DecoderQueryException {
        List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> r1 = m15868(str, z);
        if (r1.isEmpty()) {
            return null;
        }
        if (!z2) {
            return r1.get(0);
        }
        HashMap hashMap = new HashMap();
        com.google.android.exoplayer2.mediacodec.MediaCodecInfo mediaCodecInfo = null;
        for (com.google.android.exoplayer2.mediacodec.MediaCodecInfo next : r1) {
            if (next != null) {
                try {
                    String str2 = next.name;
                    String lowerCase = str2.trim().toLowerCase();
                    Log.i("TTVCustomVideoCodecUtil", "codecName = " + str2);
                    if (lowerCase.contains("omx.google.") && (lowerCase.contains("decoder") || lowerCase.contains(".dec"))) {
                        if (lowerCase.contains("mpeg4") || lowerCase.contains("h263") || lowerCase.contains("h264") || lowerCase.contains("vpx")) {
                            hashMap.put(next, 200);
                        } else if (lowerCase.contains("mpeg2") || lowerCase.contains("avc") || lowerCase.contains("hevc") || lowerCase.contains("vp8") || lowerCase.contains("vp9")) {
                            hashMap.put(next, 100);
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        }
        ArrayList<Map.Entry> arrayList = new ArrayList<>(hashMap.entrySet());
        try {
            if (!arrayList.isEmpty()) {
                Collections.sort(arrayList, new Comparator<Map.Entry<com.google.android.exoplayer2.mediacodec.MediaCodecInfo, Integer>>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public int compare(Map.Entry<com.google.android.exoplayer2.mediacodec.MediaCodecInfo, Integer> entry, Map.Entry<com.google.android.exoplayer2.mediacodec.MediaCodecInfo, Integer> entry2) {
                        return entry2.getValue().compareTo(entry.getValue());
                    }
                });
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
        for (Map.Entry entry : arrayList) {
            try {
                com.google.android.exoplayer2.mediacodec.MediaCodecInfo mediaCodecInfo2 = (com.google.android.exoplayer2.mediacodec.MediaCodecInfo) entry.getKey();
                String str3 = mediaCodecInfo2.name;
                Log.i("TTVCustomVideoCodecUtil", "swDecoderName = " + str3 + "; ranking = " + entry.getValue());
                if (mediaCodecInfo == null) {
                    Log.i("TTVCustomVideoCodecUtil", "targetSWCodec = " + str3);
                    mediaCodecInfo = mediaCodecInfo2;
                }
            } catch (Exception e3) {
                Logger.m6281((Throwable) e3, new boolean[0]);
            }
        }
        return mediaCodecInfo == null ? !arrayList.isEmpty() ? (com.google.android.exoplayer2.mediacodec.MediaCodecInfo) ((Map.Entry) arrayList.get(0)).getKey() : r1.get(0) : mediaCodecInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> m15867(CodecKey codecKey, MediaCodecListCompat mediaCodecListCompat) throws DecoderQueryException {
        ArrayList arrayList;
        String name;
        String str;
        try {
            arrayList = new ArrayList();
            String str2 = codecKey.f12587;
            int r10 = mediaCodecListCompat.m15874();
            boolean r12 = mediaCodecListCompat.m15873();
            int i = 0;
            loop0:
            while (true) {
                if (i >= r10) {
                    break;
                }
                android.media.MediaCodecInfo r3 = mediaCodecListCompat.m15875(i);
                name = r3.getName();
                if (m15870(r3, name, r12)) {
                    String[] supportedTypes = r3.getSupportedTypes();
                    int length = supportedTypes.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        str = supportedTypes[i2];
                        if (str.equalsIgnoreCase(str2)) {
                            MediaCodecInfo.CodecCapabilities capabilitiesForType = r3.getCapabilitiesForType(str);
                            boolean r11 = mediaCodecListCompat.m15876(str2, capabilitiesForType);
                            boolean r7 = m15871(name);
                            if ((!r12 || codecKey.f12586 != r11) && (r12 || codecKey.f12586)) {
                                if (!r12 && r11) {
                                    arrayList.add(com.google.android.exoplayer2.mediacodec.MediaCodecInfo.newInstance(name + ".secure", str2, capabilitiesForType, r7, true));
                                    break loop0;
                                }
                            } else {
                                arrayList.add(com.google.android.exoplayer2.mediacodec.MediaCodecInfo.newInstance(name, str2, capabilitiesForType, r7, false));
                            }
                        }
                    }
                    continue;
                }
                i++;
            }
            return arrayList;
        } catch (Exception e) {
            if (Util.SDK_INT > 23 || arrayList.isEmpty()) {
                Log.e("TTVCustomVideoCodecUtil", "Failed to query codec " + name + " (" + str + ")");
                throw e;
            }
            Log.e("TTVCustomVideoCodecUtil", "Skipping codec " + name + " (failed to query capabilities)");
        } catch (Exception e2) {
            throw new DecoderQueryException(e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> m15868(String str, boolean z) throws DecoderQueryException {
        List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> list;
        synchronized (TTVCustomVideoCodecUtil.class) {
            CodecKey codecKey = new CodecKey(str, z);
            List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> list2 = f12585.get(codecKey);
            if (list2 != null) {
                List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> list3 = list2;
                list = list2;
            } else {
                List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> r0 = m15867(codecKey, Util.SDK_INT >= 21 ? new MediaCodecListCompatV21(z) : new MediaCodecListCompatV16());
                if (z && r0.isEmpty() && 21 <= Util.SDK_INT && Util.SDK_INT <= 23) {
                    r0 = m15867(codecKey, (MediaCodecListCompat) new MediaCodecListCompatV16());
                    if (!r0.isEmpty()) {
                        Log.w("TTVCustomVideoCodecUtil", "MediaCodecList API didn't list secure decoder for: " + str + ". Assuming: " + r0.get(0).name);
                    }
                }
                m15869(r0);
                List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> unmodifiableList = Collections.unmodifiableList(r0);
                f12585.put(codecKey, unmodifiableList);
                List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> list4 = unmodifiableList;
                list = unmodifiableList;
            }
        }
        return list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15869(List<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> list) {
        if (Util.SDK_INT < 26 && list.size() > 1 && "OMX.MTK.AUDIO.DECODER.RAW".equals(list.get(0).name)) {
            for (int i = 1; i < list.size(); i++) {
                com.google.android.exoplayer2.mediacodec.MediaCodecInfo mediaCodecInfo = list.get(i);
                if ("OMX.google.raw.decoder".equals(mediaCodecInfo.name)) {
                    list.remove(i);
                    list.add(0, mediaCodecInfo);
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m15870(android.media.MediaCodecInfo mediaCodecInfo, String str, boolean z) {
        if (mediaCodecInfo.isEncoder()) {
            return false;
        }
        if (!z && str.endsWith(".secure")) {
            return false;
        }
        if (Util.SDK_INT < 21 && ("CIPAACDecoder".equals(str) || "CIPMP3Decoder".equals(str) || "CIPVorbisDecoder".equals(str) || "CIPAMRNBDecoder".equals(str) || "AACDecoder".equals(str) || "MP3Decoder".equals(str))) {
            return false;
        }
        if (Util.SDK_INT < 18 && "OMX.SEC.MP3.Decoder".equals(str)) {
            return false;
        }
        if (Util.SDK_INT < 18 && "OMX.MTK.AUDIO.DECODER.AAC".equals(str)) {
            if ("a70".equals(Util.DEVICE)) {
                return false;
            }
            if ("Xiaomi".equals(Util.MANUFACTURER) && Util.DEVICE.startsWith("HM")) {
                return false;
            }
        }
        if (Util.SDK_INT == 16 && "OMX.qcom.audio.decoder.mp3".equals(str) && ("dlxu".equals(Util.DEVICE) || "protou".equals(Util.DEVICE) || "ville".equals(Util.DEVICE) || "villeplus".equals(Util.DEVICE) || "villec2".equals(Util.DEVICE) || Util.DEVICE.startsWith("gee") || "C6602".equals(Util.DEVICE) || "C6603".equals(Util.DEVICE) || "C6606".equals(Util.DEVICE) || "C6616".equals(Util.DEVICE) || "L36h".equals(Util.DEVICE) || "SO-02E".equals(Util.DEVICE))) {
            return false;
        }
        if (Util.SDK_INT == 16 && "OMX.qcom.audio.decoder.aac".equals(str) && ("C1504".equals(Util.DEVICE) || "C1505".equals(Util.DEVICE) || "C1604".equals(Util.DEVICE) || "C1605".equals(Util.DEVICE))) {
            return false;
        }
        if (Util.SDK_INT < 24 && (("OMX.SEC.aac.dec".equals(str) || "OMX.Exynos.AAC.Decoder".equals(str)) && Util.MANUFACTURER.equals("samsung") && (Util.DEVICE.startsWith("zeroflte") || Util.DEVICE.startsWith("zerolte") || Util.DEVICE.startsWith("zenlte") || Util.DEVICE.equals("SC-05G") || Util.DEVICE.equals("marinelteatt") || Util.DEVICE.equals("404SC") || Util.DEVICE.equals("SC-04G") || Util.DEVICE.equals("SCV31")))) {
            return false;
        }
        if (Util.SDK_INT > 19 || !"OMX.SEC.vp8.dec".equals(str) || !"samsung".equals(Util.MANUFACTURER) || (!Util.DEVICE.startsWith("d2") && !Util.DEVICE.startsWith("serrano") && !Util.DEVICE.startsWith("jflte") && !Util.DEVICE.startsWith("santos") && !Util.DEVICE.startsWith("t0"))) {
            return Util.SDK_INT > 19 || !Util.DEVICE.startsWith("jflte") || !"OMX.qcom.video.decoder.vp8".equals(str);
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m15871(String str) {
        return Util.SDK_INT <= 22 && (Util.MODEL.equals("ODROID-XU3") || Util.MODEL.equals("Nexus 10")) && ("OMX.Exynos.AVC.Decoder".equals(str) || "OMX.Exynos.AVC.Decoder.secure".equals(str));
    }
}
