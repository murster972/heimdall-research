package com.typhoon.tv.jobs;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import com.mopub.common.TyphoonApp;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.CheckEpisodeResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.ui.activity.SourceActivity;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.Iterator;
import java.util.Random;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class NewEpisodeChecker {

    /* renamed from: 龘  reason: contains not printable characters */
    public String f12675 = "http://dwatchseries.to";

    /* renamed from: 龘  reason: contains not printable characters */
    private CheckEpisodeResult m16119(MediaInfo mediaInfo) {
        Element r6;
        String name = mediaInfo.getName();
        if (name.equalsIgnoreCase("The Daily Show with Trevor Noah")) {
            name = "The Daily Show";
        }
        String r31 = HttpHelper.m6343().m6358(this.f12675 + "/search/" + Utils.m6414(TitleHelper.m15970(name.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.f12675);
        if (!r31.toLowerCase().contains("search result")) {
            this.f12675 = "http://watchtvseries.unblckd.ist";
            r31 = HttpHelper.m6343().m6358(this.f12675 + "/search/" + Utils.m6414(TitleHelper.m15970(name.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.f12675);
            if (!r31.toLowerCase().contains("search result")) {
                this.f12675 = "http://watchseries.bypassed.org";
                r31 = HttpHelper.m6343().m6358(this.f12675 + "/search/" + Utils.m6414(TitleHelper.m15970(name.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.f12675);
                if (!r31.toLowerCase().contains("search result")) {
                    this.f12675 = "http://watchseries.bypassed.bz";
                    r31 = HttpHelper.m6343().m6358(this.f12675 + "/search/" + Utils.m6414(TitleHelper.m15970(name.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.f12675);
                }
            }
        }
        Iterator it2 = Jsoup.m21674(r31).m21815("a[href][title][target=\"_blank\"]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            try {
                Element r4 = element.m21819();
                Element r14 = element.m21837("strong");
                if (r14 != null) {
                    String r27 = element.m21947("href");
                    String r26 = r14.m21868();
                    String r25 = Regex.m17800(r26, "(.*?)\\s*\\((\\d{4})\\)", 1);
                    String r28 = Regex.m17800(r26, "(.*?)\\s*\\((\\d{4})\\)", 2);
                    if (r25.isEmpty()) {
                        r25 = r26;
                    }
                    if (TitleHelper.m15971(name.replace("Marvel's ", "").replace("DC's ", "")).equals(TitleHelper.m15971(r25.replace("Marvel's ", "").replace("DC's ", ""))) && (r28.trim().isEmpty() || !Utils.m6426(r28.trim()) || mediaInfo.getYear() <= 0 || Integer.parseInt(r28.trim()) == mediaInfo.getYear())) {
                        Element last = r4.m21815("strong").last();
                        if (last == null || !last.m21868().trim().replaceAll(" ", "").replaceAll("\\s", "").toLowerCase().startsWith("latestep")) {
                            return null;
                        }
                        int i = -1;
                        int i2 = -1;
                        Element r11 = last.m21849();
                        if (r11 != null && r11.m21869().equals("a")) {
                            String r17 = r11.m21868();
                            String r21 = Regex.m17801(r17, "Season\\s+(\\d+)\\s+Episode\\s+(\\d+)", 1, 2);
                            i = (r21.isEmpty() || !Utils.m6426(r21)) ? -1 : Integer.parseInt(r21);
                            String r19 = Regex.m17801(r17, "Season\\s+(\\d+)\\s+Episode\\s+(\\d+)", 2, 2);
                            i2 = (r19.isEmpty() || !Utils.m6426(r19)) ? -1 : Integer.parseInt(r19);
                        }
                        String str = r27;
                        if (str.startsWith("//")) {
                            str = "http:" + str;
                        } else if (str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                            str = this.f12675 + str;
                        } else if (!str.startsWith(TyphoonApp.HTTP)) {
                            str = this.f12675 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str;
                        }
                        Iterator it3 = Jsoup.m21674(HttpHelper.m6343().m6358(str, this.f12675)).m21815("div[itemprop=\"season\"]").iterator();
                        while (it3.hasNext()) {
                            Element element2 = (Element) it3.next();
                            try {
                                Element r13 = element2.m21837("span[itemprop=\"name\"]");
                                if (r13 != null && Integer.parseInt(Regex.m17801(r13.m21868(), "Season\\s+(\\d+)", 1, 2)) == i) {
                                    Iterator it4 = element2.m21815("li[itemprop=\"episode\"]").iterator();
                                    while (it4.hasNext()) {
                                        Element element3 = (Element) it4.next();
                                        Element r9 = element3.m21837("meta[content][itemprop=\"episodenumber\"]");
                                        if (r9 != null && Integer.parseInt(r9.m21947("content")) == i2 && (r6 = element3.m21837("b")) != null && Integer.parseInt(Regex.m17801(r6.m21868(), "\\(?(\\d+)\\s+links?\\)?", 1, 2)) >= 5) {
                                            CheckEpisodeResult checkEpisodeResult = new CheckEpisodeResult(mediaInfo);
                                            checkEpisodeResult.setLastSeason(i);
                                            checkEpisodeResult.setLastEpisode(i2);
                                            return checkEpisodeResult;
                                        }
                                    }
                                    continue;
                                }
                            } catch (Exception e) {
                                Logger.m6281((Throwable) e, new boolean[0]);
                            }
                        }
                        continue;
                    }
                } else {
                    continue;
                }
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16120(Context context, MediaInfo mediaInfo, CheckEpisodeResult checkEpisodeResult) {
        Intent intent = new Intent(context, SourceActivity.class);
        intent.putExtra("mediaInfo", mediaInfo);
        intent.putExtra("season", checkEpisodeResult.getLastSeason());
        intent.putExtra("episode", checkEpisodeResult.getLastEpisode());
        PendingIntent activity = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 1073741824);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        NotificationManagerCompat from = NotificationManagerCompat.from(context);
        NotificationCompat.Builder contentIntent = new NotificationCompat.Builder(context).setContentTitle(mediaInfo.getNameAndYear()).setContentText(I18N.m15707(R.string.new_episode, "S" + Utils.m6413(checkEpisodeResult.getLastSeason()) + "E" + Utils.m6413(checkEpisodeResult.getLastEpisode()))).setSmallIcon(R.drawable.ic_live_tv_white_36dp).setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher)).setAutoCancel(true).setSound(defaultUri).setLights(ContextCompat.getColor(context, R.color.light_blue), 500, 5000).setPriority(1).setContentIntent(activity);
        if (Build.VERSION.SDK_INT >= 19) {
            contentIntent.setShowWhen(true);
        }
        from.notify(new Random().nextInt(9999), contentIntent.build());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16121(Context context) {
        try {
            for (MediaInfo next : TVApplication.m6287().m6312((Integer) 0)) {
                try {
                    CheckEpisodeResult r0 = m16119(next);
                    if (r0 != null) {
                        CheckEpisodeResult r3 = TVApplication.m6287().m6306(Integer.valueOf(next.getTmdbId()));
                        if (r3 == null) {
                            r0.save();
                        } else {
                            r0.update();
                            int lastSeason = r3.getLastSeason();
                            int lastEpisode = r3.getLastEpisode();
                            int lastSeason2 = r0.getLastSeason();
                            if (lastSeason2 > lastSeason || (lastSeason2 == lastSeason && r0.getLastEpisode() > lastEpisode)) {
                                m16120(context, next, r0);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        } catch (Exception e2) {
            Logger.m6281((Throwable) e2, new boolean[0]);
        }
    }
}
