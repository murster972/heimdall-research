package com.typhoon.tv.jobs;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.typhoon.tv.TVApplication;
import java.util.concurrent.TimeUnit;

public class CheckNewMovieReleaseJob extends Job {
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m16117() {
        JobManager.instance().cancelAllForTag("CheckNewMovieReleaseJob");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m16118() {
        return new JobRequest.Builder("CheckNewMovieReleaseJob").setPeriodic(TimeUnit.HOURS.toMillis((long) TVApplication.m6285().getInt("pref_check_new_movie_release_frequency", 24)), TimeUnit.MINUTES.toMillis(5)).setRequiresCharging(false).setRequiredNetworkType(JobRequest.NetworkType.CONNECTED).setRequirementsEnforced(true).setUpdateCurrent(true).build().schedule();
    }

    /* access modifiers changed from: protected */
    public Job.Result onRunJob(Job.Params params) {
        new NewMovieReleaseChecker().m16124(getContext());
        return Job.Result.SUCCESS;
    }
}
