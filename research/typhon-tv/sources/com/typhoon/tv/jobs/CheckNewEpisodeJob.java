package com.typhoon.tv.jobs;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.typhoon.tv.TVApplication;
import java.util.concurrent.TimeUnit;

public class CheckNewEpisodeJob extends Job {
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m16115() {
        JobManager.instance().cancelAllForTag("CheckNewEpisodeJob");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m16116() {
        return new JobRequest.Builder("CheckNewEpisodeJob").setPeriodic(TimeUnit.HOURS.toMillis((long) TVApplication.m6285().getInt("pref_check_new_episode_frequency", 2)), TimeUnit.MINUTES.toMillis(5)).setRequiresCharging(false).setRequiredNetworkType(JobRequest.NetworkType.CONNECTED).setRequirementsEnforced(true).setUpdateCurrent(true).build().schedule();
    }

    /* access modifiers changed from: protected */
    public Job.Result onRunJob(Job.Params params) {
        new NewEpisodeChecker().m16121(getContext());
        return Job.Result.SUCCESS;
    }
}
