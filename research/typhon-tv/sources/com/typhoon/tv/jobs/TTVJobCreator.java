package com.typhoon.tv.jobs;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class TTVJobCreator implements JobCreator {
    public Job create(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case 247720654:
                if (str.equals("CheckNewMovieReleaseJob")) {
                    c = 1;
                    break;
                }
                break;
            case 540660090:
                if (str.equals("CheckNewEpisodeJob")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return new CheckNewEpisodeJob();
            case 1:
                return new CheckNewMovieReleaseJob();
            default:
                return null;
        }
    }
}
