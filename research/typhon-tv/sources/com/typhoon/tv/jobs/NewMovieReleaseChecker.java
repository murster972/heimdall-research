package com.typhoon.tv.jobs;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.model.CheckNewMovieReleaseResult;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.ui.activity.HomeActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class NewMovieReleaseChecker {
    /* renamed from: 龘  reason: contains not printable characters */
    private String[] m16123() {
        return new String[]{"2:22", "11:55"};
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* Debug info: failed to restart local var, previous not found, register: 5 */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m16124(Context context) {
        CheckNewMovieReleaseResult lastCheckResult = TVApplication.m6287().m6294();
        int tmdbId = lastCheckResult != null ? lastCheckResult.getTmdbId() : -1;
        MediaApiResult mediaApiResult = null;
        try {
            mediaApiResult = TmdbApi.m15742().m15748(3, -1, 1);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (mediaApiResult != null && mediaApiResult.getMediaInfoList() != null && !mediaApiResult.getMediaInfoList().isEmpty()) {
            boolean z = false;
            List<MediaInfo> mediaInfoList = new ArrayList<>();
            List<String> movieNameBlacklist = Arrays.asList(m16123());
            Iterator<MediaInfo> it2 = mediaApiResult.getMediaInfoList().iterator();
            while (it2.hasNext()) {
                MediaInfo next = it2.next();
                if (next.getTmdbId() > 0) {
                    if (lastCheckResult != null && tmdbId != -1) {
                        if (!z) {
                            lastCheckResult.setTmdbId(next.getTmdbId());
                            lastCheckResult.update();
                            z = true;
                        }
                        if (tmdbId == next.getTmdbId()) {
                            break;
                        } else if (!mediaInfoList.contains(next) && !movieNameBlacklist.contains(next.getName())) {
                            mediaInfoList.add(next);
                        }
                    } else {
                        CheckNewMovieReleaseResult checkNewMovieReleaseResult = new CheckNewMovieReleaseResult();
                        checkNewMovieReleaseResult.setTmdbId(next.getTmdbId());
                        checkNewMovieReleaseResult.save();
                        return;
                    }
                }
            }
            if (!mediaInfoList.isEmpty()) {
                m16122(context, mediaInfoList);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16122(Context context, List<MediaInfo> mediaInfoList) {
        String title = I18N.m15707(R.string.ttv_watch_new_movies_now, Integer.valueOf(mediaInfoList.size()));
        StringBuilder messageStringBuilder = new StringBuilder();
        for (MediaInfo mediaInfo : mediaInfoList) {
            messageStringBuilder.append(mediaInfo.getNameAndYear()).append(", ");
        }
        String message = messageStringBuilder.toString();
        if (message.endsWith(", ")) {
            message = message.substring(0, message.length() - 2);
        }
        Intent resultIntent = new Intent(context, HomeActivity.class);
        resultIntent.putExtra("displayNewMovieReleasesPage", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), resultIntent, 1073741824);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(2);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setContentTitle(title).setContentText(message).setSmallIcon(R.drawable.ic_movie_white_36dp).setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher)).setAutoCancel(true).setSound(defaultSoundUri).setLights(ContextCompat.getColor(context, R.color.light_blue), 500, 5000).setPriority(1).setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= 19) {
            notificationBuilder.setShowWhen(true);
        }
        notificationManager.notify(new Random().nextInt(9999), notificationBuilder.build());
    }
}
