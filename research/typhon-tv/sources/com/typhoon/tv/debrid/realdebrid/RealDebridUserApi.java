package com.typhoon.tv.debrid.realdebrid;

import android.annotation.SuppressLint;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.typhoon.tv.Logger;
import com.typhoon.tv.RxBus;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.event.debrid.realdebrid.RealDebridWaitingToVerifyEvent;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.model.debrid.realdebrid.RealDebridCheckAuthResult;
import com.typhoon.tv.model.debrid.realdebrid.RealDebridCredentialsInfo;
import com.typhoon.tv.model.debrid.realdebrid.RealDebridGetDeviceCodeResult;
import com.typhoon.tv.model.debrid.realdebrid.RealDebridGetTokenResult;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.oltu.oauth2.common.OAuth;
import rx.Observable;
import rx.Subscriber;

public class RealDebridUserApi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile RealDebridUserApi f12572;

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public static HashMap<String, String> m15843() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5841);
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RealDebridUserApi m15845() {
        RealDebridUserApi realDebridUserApi = f12572;
        if (realDebridUserApi == null) {
            synchronized (RealDebridUserApi.class) {
                try {
                    realDebridUserApi = f12572;
                    if (realDebridUserApi == null) {
                        RealDebridUserApi realDebridUserApi2 = new RealDebridUserApi();
                        try {
                            f12572 = realDebridUserApi2;
                            realDebridUserApi = realDebridUserApi2;
                        } catch (Throwable th) {
                            th = th;
                            RealDebridUserApi realDebridUserApi3 = realDebridUserApi2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return realDebridUserApi;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public RealDebridGetTokenResult m15847(String str, String str2, String str3) {
        try {
            String r2 = HttpHelper.m6343().m6360("https://api.real-debrid.com/oauth/v2/token", "client_id=" + Utils.m6414(str, new boolean[0]) + "&client_secret=" + Utils.m6414(str2, new boolean[0]) + "&code=" + Utils.m6414(str3, new boolean[0]) + "&grant_type=http://oauth.net/grant_type/device/1.0", false, (Map<String, String>[]) new Map[]{m15843()});
            if (r2.contains("access_token")) {
                return (RealDebridGetTokenResult) new Gson().fromJson(r2, RealDebridGetTokenResult.class);
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Observable<Boolean> m15848() {
        return Observable.m7359(new Observable.OnSubscribe<Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super Boolean> subscriber) {
                if (RealDebridCredentialsHelper.m15838().isValid()) {
                    subscriber.onNext(1);
                    subscriber.onCompleted();
                    return;
                }
                HashMap r9 = RealDebridUserApi.m15843();
                try {
                    String r14 = HttpHelper.m6343().m6351("https://api.real-debrid.com" + String.format("/oauth/v2/device/code?client_id=%s&new_credentials=yes", new Object[]{TyphoonApp.f5840}), (Map<String, String>[]) new Map[]{r9});
                    if (r14.isEmpty()) {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                        return;
                    }
                    RealDebridGetDeviceCodeResult realDebridGetDeviceCodeResult = (RealDebridGetDeviceCodeResult) new Gson().fromJson(r14, RealDebridGetDeviceCodeResult.class);
                    String verification_url = realDebridGetDeviceCodeResult.getVerification_url();
                    String user_code = realDebridGetDeviceCodeResult.getUser_code();
                    String device_code = realDebridGetDeviceCodeResult.getDevice_code();
                    int expires_in = realDebridGetDeviceCodeResult.getExpires_in();
                    int interval = realDebridGetDeviceCodeResult.getInterval();
                    RxBus.m15709().m15711(new RealDebridWaitingToVerifyEvent(verification_url, user_code));
                    RealDebridGetTokenResult realDebridGetTokenResult = null;
                    String str = "";
                    String str2 = "";
                    for (int i = 0; i < expires_in && !subscriber.isUnsubscribed(); i++) {
                        try {
                            Thread.sleep(1000);
                            if (((float) i) % ((float) interval) != 0.0f) {
                                continue;
                            } else {
                                RealDebridCheckAuthResult realDebridCheckAuthResult = (RealDebridCheckAuthResult) new Gson().fromJson(HttpHelper.m6343().m6351("https://api.real-debrid.com" + String.format("/oauth/v2/device/credentials?client_id=%s&code=%s", new Object[]{TyphoonApp.f5840, device_code}), (Map<String, String>[]) new Map[]{r9}), RealDebridCheckAuthResult.class);
                                try {
                                    str = realDebridCheckAuthResult.getClient_id();
                                    str2 = realDebridCheckAuthResult.getClient_secret();
                                    if (!(str == null || str2 == null || str.isEmpty() || str2.isEmpty() || (realDebridGetTokenResult = RealDebridUserApi.this.m15847(str, str2, device_code)) == null)) {
                                        break;
                                    }
                                } catch (Exception e) {
                                    Logger.m6281((Throwable) e, new boolean[0]);
                                }
                            }
                        } catch (Exception e2) {
                            Logger.m6281((Throwable) e2, new boolean[0]);
                        }
                    }
                    if (realDebridGetTokenResult == null || realDebridGetTokenResult.getAccess_token() == null || realDebridGetTokenResult.getAccess_token().isEmpty()) {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    } else if (str.isEmpty() || str2.isEmpty()) {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    } else {
                        String access_token = realDebridGetTokenResult.getAccess_token();
                        RealDebridCredentialsInfo realDebridCredentialsInfo = new RealDebridCredentialsInfo();
                        realDebridCredentialsInfo.setAccessToken(access_token);
                        realDebridCredentialsInfo.setRefreshToken(realDebridGetTokenResult.getRefresh_token());
                        realDebridCredentialsInfo.setClientId(str);
                        realDebridCredentialsInfo.setClientSecret(str2);
                        RealDebridCredentialsHelper.m15839(realDebridCredentialsInfo);
                        subscriber.onNext(1);
                        subscriber.onCompleted();
                    }
                } catch (Exception e3) {
                    Logger.m6281((Throwable) e3, new boolean[0]);
                    subscriber.onNext(null);
                }
            }
        });
    }

    @SuppressLint({"ApplySharedPref"})
    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized boolean m15849() {
        boolean z;
        if (RealDebridRefreshingTokenStatus.m15842()) {
            z = false;
        } else {
            long j = TVApplication.m6285().getLong("pref_rd_last_refresh_ts", -1);
            long parseLong = Long.parseLong(DateTimeHelper.m15934());
            if (j == -1 || parseLong - j >= 300000) {
                RealDebridRefreshingTokenStatus.m15841(true);
                TVApplication.m6285().edit().putLong("pref_rd_last_refresh_ts", Long.parseLong(DateTimeHelper.m15934())).commit();
                RealDebridCredentialsInfo r5 = RealDebridCredentialsHelper.m15838();
                if (!r5.isValid()) {
                    RealDebridCredentialsHelper.m15837();
                    RealDebridRefreshingTokenStatus.m15841(false);
                    z = false;
                } else {
                    String clientId = r5.getClientId();
                    String clientSecret = r5.getClientSecret();
                    RealDebridGetTokenResult r6 = m15847(clientId, clientSecret, r5.getRefreshToken());
                    if (r6 == null || r6.getAccess_token() == null || r6.getAccess_token().isEmpty()) {
                        RealDebridCredentialsHelper.m15837();
                        RealDebridRefreshingTokenStatus.m15841(false);
                        z = false;
                    } else if (clientId == null || clientSecret == null || clientId.isEmpty() || clientSecret.isEmpty()) {
                        RealDebridCredentialsHelper.m15837();
                        RealDebridRefreshingTokenStatus.m15841(false);
                        z = false;
                    } else {
                        String access_token = r6.getAccess_token();
                        RealDebridCredentialsInfo realDebridCredentialsInfo = new RealDebridCredentialsInfo();
                        realDebridCredentialsInfo.setAccessToken(access_token);
                        realDebridCredentialsInfo.setRefreshToken(r6.getRefresh_token());
                        realDebridCredentialsInfo.setClientId(clientId);
                        realDebridCredentialsInfo.setClientSecret(clientSecret);
                        RealDebridCredentialsHelper.m15839(realDebridCredentialsInfo);
                        RealDebridRefreshingTokenStatus.m15841(false);
                        z = true;
                    }
                }
            } else {
                z = true;
            }
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<ResolveResult> m15850(String str, String str2) {
        return m15851(str, str2, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<ResolveResult> m15851(String str, String str2, boolean z) {
        String replace;
        String replace2;
        ArrayList arrayList = new ArrayList();
        try {
            RealDebridCredentialsInfo r19 = RealDebridCredentialsHelper.m15838();
            if (!r19.isValid()) {
                return arrayList;
            }
            HashMap<String, String> r10 = m15843();
            r10.put(OAuth.HeaderType.AUTHORIZATION, "Bearer " + r19.getAccessToken());
            String r20 = HttpHelper.m6343().m6360("https://api.real-debrid.com/rest/1.0/unrestrict/link", "link=" + str, false, (Map<String, String>[]) new Map[]{r10});
            if (!r20.contains("bad_token")) {
                try {
                    JsonObject asJsonObject = new JsonParser().parse(r20).getAsJsonObject();
                    String asString = asJsonObject.get("download").getAsString();
                    String replace3 = asString.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                    if (!replace3.endsWith(".rar") && !replace3.endsWith(".7z") && !replace3.endsWith(".zip") && !replace3.endsWith(".iso") && !replace3.endsWith(".tar") && !replace3.endsWith(".flv") && !replace3.endsWith(".sub") && !replace3.endsWith(".pdf") && !replace3.endsWith(".mp3")) {
                        try {
                            replace2 = asJsonObject.get("quality").getAsString().trim().toUpperCase().replace("SD", "HQ").replace("P", TtmlNode.TAG_P);
                        } catch (Exception e) {
                        }
                        ResolveResult resolveResult = new ResolveResult(str2, asString, replace2);
                        resolveResult.setDebrid(true);
                        arrayList.add(resolveResult);
                    }
                    try {
                        Iterator<JsonElement> it2 = asJsonObject.get("alternative").getAsJsonArray().iterator();
                        while (it2.hasNext()) {
                            try {
                                JsonObject asJsonObject2 = it2.next().getAsJsonObject();
                                String asString2 = asJsonObject2.get("download").getAsString();
                                String replace4 = asString2.trim().toLowerCase().replace(StringUtils.SPACE, "").replace(StringUtils.CR, "").replace(StringUtils.LF, "");
                                if (!replace4.endsWith(".rar") && !replace4.endsWith(".7z") && !replace4.endsWith(".zip") && !replace4.endsWith(".iso") && !replace4.endsWith(".tar") && !replace4.endsWith(".flv") && !replace4.endsWith(".sub") && !replace4.endsWith(".pdf") && !replace4.endsWith(".mp3")) {
                                    try {
                                        replace = asJsonObject2.get("quality").getAsString().trim().toUpperCase().replace("SD", "HQ").replace("P", TtmlNode.TAG_P);
                                    } catch (Exception e2) {
                                    }
                                    ResolveResult resolveResult2 = new ResolveResult(str2, asString2, replace);
                                    resolveResult2.setDebrid(true);
                                    arrayList.add(resolveResult2);
                                }
                            } catch (Exception e3) {
                                Logger.m6281((Throwable) e3, new boolean[0]);
                            }
                        }
                        return arrayList;
                    } catch (Exception e4) {
                        return arrayList;
                    }
                } catch (Exception e5) {
                    return arrayList;
                }
            } else if (z) {
                return arrayList;
            } else {
                m15849();
                return m15851(str, str2, true);
            }
        } catch (Exception e6) {
            Logger.m6281((Throwable) e6, new boolean[0]);
            return arrayList;
        }
    }
}
