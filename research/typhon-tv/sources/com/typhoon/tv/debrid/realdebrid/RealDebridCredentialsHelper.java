package com.typhoon.tv.debrid.realdebrid;

import android.content.SharedPreferences;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.debrid.realdebrid.RealDebridCredentialsInfo;

public class RealDebridCredentialsHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized void m15837() {
        synchronized (RealDebridCredentialsHelper.class) {
            try {
                SharedPreferences.Editor edit = TVApplication.m6285().edit();
                edit.putString("real_debrid_access_token", (String) null);
                edit.putString("real_debrid_refresh_token", (String) null);
                edit.putString("real_debrid_client_id", (String) null);
                edit.putString("real_debrid_client_secret", (String) null);
                edit.apply();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RealDebridCredentialsInfo m15838() {
        RealDebridCredentialsInfo realDebridCredentialsInfo = new RealDebridCredentialsInfo();
        try {
            SharedPreferences r1 = TVApplication.m6285();
            realDebridCredentialsInfo.setAccessToken(r1.getString("real_debrid_access_token", (String) null));
            realDebridCredentialsInfo.setRefreshToken(r1.getString("real_debrid_refresh_token", (String) null));
            realDebridCredentialsInfo.setClientId(r1.getString("real_debrid_client_id", (String) null));
            realDebridCredentialsInfo.setClientSecret(r1.getString("real_debrid_client_secret", (String) null));
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        return realDebridCredentialsInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m15839(RealDebridCredentialsInfo realDebridCredentialsInfo) {
        synchronized (RealDebridCredentialsHelper.class) {
            m15840(realDebridCredentialsInfo.getAccessToken(), realDebridCredentialsInfo.getRefreshToken(), realDebridCredentialsInfo.getClientId(), realDebridCredentialsInfo.getClientSecret());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m15840(String str, String str2, String str3, String str4) {
        synchronized (RealDebridCredentialsHelper.class) {
            try {
                SharedPreferences.Editor edit = TVApplication.m6285().edit();
                edit.putString("real_debrid_access_token", str);
                edit.putString("real_debrid_refresh_token", str2);
                edit.putString("real_debrid_client_id", str3);
                edit.putString("real_debrid_client_secret", str4);
                edit.apply();
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return;
    }
}
