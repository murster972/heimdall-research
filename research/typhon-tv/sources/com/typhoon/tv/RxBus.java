package com.typhoon.tv;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

public class RxBus {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile RxBus f12488;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Subject<Object, Object> f12489 = new SerializedSubject(PublishSubject.m25061());

    /* renamed from: 龘  reason: contains not printable characters */
    public static RxBus m15709() {
        RxBus rxBus = f12488;
        if (rxBus == null) {
            synchronized (RxBus.class) {
                rxBus = f12488;
                if (rxBus == null) {
                    RxBus rxBus2 = new RxBus();
                    f12488 = rxBus2;
                    rxBus = rxBus2;
                }
            }
        }
        return rxBus;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Observable<Object> m15710() {
        return this.f12489;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15711(Object obj) {
        this.f12489.onNext(obj);
    }
}
