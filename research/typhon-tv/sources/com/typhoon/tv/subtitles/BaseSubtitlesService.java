package com.typhoon.tv.subtitles;

import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.ui.activity.SettingsActivity;
import com.typhoon.tv.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;

public abstract class BaseSubtitlesService {

    /* renamed from: 龘  reason: contains not printable characters */
    private Hashtable<String, String> f5936;

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m6372() {
        return TVApplication.m6285().getString("pref_sub_down_path", SettingsActivity.f13215);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m6373() {
        if (this.f5936 == null) {
            this.f5936 = new Hashtable<>();
            this.f5936.put(PubnativeRequest.LEGACY_ZONE_ID, "一");
            this.f5936.put("2", "二");
            this.f5936.put("3", "三");
            this.f5936.put("4", "四");
            this.f5936.put("5", "五");
            this.f5936.put("6", "六");
            this.f5936.put("7", "七");
            this.f5936.put("8", "八");
            this.f5936.put("9", "九");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0176 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0179 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x017c A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x017f A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0182 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0185 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005e A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0084 A[SYNTHETIC, Splitter:B:45:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0089 A[SYNTHETIC, Splitter:B:48:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x008e A[SYNTHETIC, Splitter:B:51:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ad A[SYNTHETIC, Splitter:B:61:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00b2 A[SYNTHETIC, Splitter:B:64:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00b7 A[SYNTHETIC, Splitter:B:67:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0103 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0106 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0121  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int m6374(java.lang.String r15) {
        /*
            java.io.File r7 = new java.io.File
            r7.<init>(r15)
            r13 = 1
            byte[] r4 = new byte[r13]
            r9 = 0
            r0 = 0
            r2 = 0
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ Exception -> 0x007b }
            r10.<init>(r7)     // Catch:{ Exception -> 0x007b }
            net.pempek.unicode.UnicodeBOMInputStream r1 = new net.pempek.unicode.UnicodeBOMInputStream     // Catch:{ Exception -> 0x0197, all -> 0x0188 }
            r1.<init>(r10)     // Catch:{ Exception -> 0x0197, all -> 0x0188 }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x019b, all -> 0x018c }
            r3.<init>(r1)     // Catch:{ Exception -> 0x019b, all -> 0x018c }
            r13 = 0
            r14 = 1
            r3.read(r4, r13, r14)     // Catch:{ Exception -> 0x01a0, all -> 0x0191 }
            if (r3 == 0) goto L_0x0024
            r3.close()     // Catch:{ Exception -> 0x0060 }
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ Exception -> 0x0068 }
        L_0x0029:
            if (r10 == 0) goto L_0x01a6
            r10.close()     // Catch:{ Exception -> 0x0070 }
            r2 = r3
            r0 = r1
            r9 = r10
        L_0x0031:
            r11 = 0
            java.lang.String r12 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x00d3 }
            java.lang.String r13 = "UTF-8"
            r12.<init>(r4, r13)     // Catch:{ UnsupportedEncodingException -> 0x00d3 }
            r11 = r12
        L_0x003b:
            if (r11 == 0) goto L_0x004f
            int r13 = r11.length()
            r14 = 1
            if (r13 != r14) goto L_0x004f
            r13 = -1
            int r14 = r11.hashCode()
            switch(r14) {
                case 80: goto L_0x00f7;
                case 81: goto L_0x004c;
                case 82: goto L_0x00eb;
                default: goto L_0x004c;
            }
        L_0x004c:
            switch(r13) {
                case 0: goto L_0x0103;
                case 1: goto L_0x0106;
                default: goto L_0x004f;
            }
        L_0x004f:
            java.lang.String r8 = com.typhoon.tv.utils.Utils.m6405((java.lang.String) r15)
            r13 = -1
            int r14 = r8.hashCode()
            switch(r14) {
                case 1467283: goto L_0x012d;
                case 1483061: goto L_0x0115;
                case 1484551: goto L_0x0121;
                case 1484563: goto L_0x0139;
                case 1487496: goto L_0x0169;
                case 1489193: goto L_0x015d;
                case 1490995: goto L_0x0109;
                case 45562920: goto L_0x0151;
                case 46052685: goto L_0x0145;
                default: goto L_0x005b;
            }
        L_0x005b:
            switch(r13) {
                case 0: goto L_0x0176;
                case 1: goto L_0x0179;
                case 2: goto L_0x017c;
                case 3: goto L_0x017f;
                case 4: goto L_0x017f;
                case 5: goto L_0x0182;
                case 6: goto L_0x0182;
                case 7: goto L_0x0182;
                case 8: goto L_0x0185;
                default: goto L_0x005e;
            }
        L_0x005e:
            r13 = 0
        L_0x005f:
            return r13
        L_0x0060:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            goto L_0x0024
        L_0x0068:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            goto L_0x0029
        L_0x0070:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x0031
        L_0x007b:
            r5 = move-exception
        L_0x007c:
            r13 = 0
            boolean[] r13 = new boolean[r13]     // Catch:{ all -> 0x00aa }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)     // Catch:{ all -> 0x00aa }
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ Exception -> 0x009a }
        L_0x0087:
            if (r0 == 0) goto L_0x008c
            r0.close()     // Catch:{ Exception -> 0x00a2 }
        L_0x008c:
            if (r9 == 0) goto L_0x0031
            r9.close()     // Catch:{ Exception -> 0x0092 }
            goto L_0x0031
        L_0x0092:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            goto L_0x0031
        L_0x009a:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            goto L_0x0087
        L_0x00a2:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            goto L_0x008c
        L_0x00aa:
            r13 = move-exception
        L_0x00ab:
            if (r2 == 0) goto L_0x00b0
            r2.close()     // Catch:{ Exception -> 0x00bb }
        L_0x00b0:
            if (r0 == 0) goto L_0x00b5
            r0.close()     // Catch:{ Exception -> 0x00c3 }
        L_0x00b5:
            if (r9 == 0) goto L_0x00ba
            r9.close()     // Catch:{ Exception -> 0x00cb }
        L_0x00ba:
            throw r13
        L_0x00bb:
            r5 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r14)
            goto L_0x00b0
        L_0x00c3:
            r5 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r14)
            goto L_0x00b5
        L_0x00cb:
            r5 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r14)
            goto L_0x00ba
        L_0x00d3:
            r5 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r13)
            java.lang.String r12 = new java.lang.String     // Catch:{ Exception -> 0x00e2 }
            r12.<init>(r4)     // Catch:{ Exception -> 0x00e2 }
            r11 = r12
            goto L_0x003b
        L_0x00e2:
            r6 = move-exception
            r13 = 0
            boolean[] r13 = new boolean[r13]
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r13)
            goto L_0x003b
        L_0x00eb:
            java.lang.String r14 = "R"
            boolean r14 = r11.equals(r14)
            if (r14 == 0) goto L_0x004c
            r13 = 0
            goto L_0x004c
        L_0x00f7:
            java.lang.String r14 = "P"
            boolean r14 = r11.equals(r14)
            if (r14 == 0) goto L_0x004c
            r13 = 1
            goto L_0x004c
        L_0x0103:
            r13 = 2
            goto L_0x005f
        L_0x0106:
            r13 = 1
            goto L_0x005f
        L_0x0109:
            java.lang.String r14 = ".zip"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 0
            goto L_0x005b
        L_0x0115:
            java.lang.String r14 = ".rar"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 1
            goto L_0x005b
        L_0x0121:
            java.lang.String r14 = ".srt"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 2
            goto L_0x005b
        L_0x012d:
            java.lang.String r14 = ".ass"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 3
            goto L_0x005b
        L_0x0139:
            java.lang.String r14 = ".ssa"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 4
            goto L_0x005b
        L_0x0145:
            java.lang.String r14 = ".ttml"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 5
            goto L_0x005b
        L_0x0151:
            java.lang.String r14 = ".dfxp"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 6
            goto L_0x005b
        L_0x015d:
            java.lang.String r14 = ".xml"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 7
            goto L_0x005b
        L_0x0169:
            java.lang.String r14 = ".vtt"
            boolean r14 = r8.equals(r14)
            if (r14 == 0) goto L_0x005b
            r13 = 8
            goto L_0x005b
        L_0x0176:
            r13 = 1
            goto L_0x005f
        L_0x0179:
            r13 = 2
            goto L_0x005f
        L_0x017c:
            r13 = 3
            goto L_0x005f
        L_0x017f:
            r13 = 4
            goto L_0x005f
        L_0x0182:
            r13 = 6
            goto L_0x005f
        L_0x0185:
            r13 = 5
            goto L_0x005f
        L_0x0188:
            r13 = move-exception
            r9 = r10
            goto L_0x00ab
        L_0x018c:
            r13 = move-exception
            r0 = r1
            r9 = r10
            goto L_0x00ab
        L_0x0191:
            r13 = move-exception
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x00ab
        L_0x0197:
            r5 = move-exception
            r9 = r10
            goto L_0x007c
        L_0x019b:
            r5 = move-exception
            r0 = r1
            r9 = r10
            goto L_0x007c
        L_0x01a0:
            r5 = move-exception
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x007c
        L_0x01a6:
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.subtitles.BaseSubtitlesService.m6374(java.lang.String):int");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m6375(int i) {
        m6373();
        String valueOf = String.valueOf(i);
        int length = valueOf.length();
        if (length > 2) {
            return "";
        }
        if (i == 0) {
            return "零";
        }
        if (length == 1 && i > 0 && i < 10) {
            return this.f5936.get(valueOf);
        }
        if (length != 2) {
            return "";
        }
        if (i == 10) {
            return "十";
        }
        String substring = valueOf.substring(0, 1);
        String substring2 = valueOf.substring(1, 2);
        return substring.equals(PubnativeRequest.LEGACY_ZONE_ID) ? "十" + this.f5936.get(substring2) : substring2.equals("0") ? this.f5936.get(substring) + "十" : this.f5936.get(substring) + "十" + this.f5936.get(substring2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public ArrayList<String> m6376(MediaInfo mediaInfo, int i, int i2) {
        String replace = TitleHelper.m15970(mediaInfo.getName()).replace("Marvel's ", "").replace("DC's ", "");
        String r1 = m6375(i);
        String r0 = m6375(i2);
        ArrayList<String> arrayList = new ArrayList<>();
        if (!r1.isEmpty() && !r0.isEmpty()) {
            arrayList.add(replace + " 第" + r1 + "季");
            arrayList.add(replace + " 第" + r1 + "季 第" + r0 + "集");
            arrayList.add(replace + " 第" + r1 + "季 第" + i2 + "集");
        }
        arrayList.add(replace + " 第" + i + "季");
        arrayList.add(replace + " 第" + Utils.m6413(i) + "季");
        arrayList.add(replace + " 第" + i + "季 第" + i2 + "集");
        arrayList.add(replace + " S" + Utils.m6413(i) + "E" + Utils.m6413(i2));
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract ArrayList<String> m6377(String str);

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6378(String str) {
        String lowerCase = str.toLowerCase();
        if (TVApplication.m6285().getBoolean("pref_sub_language_en", true) && lowerCase.contains(I18N.m15706(R.string.language_en).toLowerCase())) {
            return true;
        }
        if (!TVApplication.m6285().getBoolean("pref_sub_language_zh_tw", true) || !lowerCase.contains(I18N.m15706(R.string.language_zh_tw).toLowerCase())) {
            return TVApplication.m6285().getBoolean("pref_sub_language_zh_cn", true) && lowerCase.contains(I18N.m15706(R.string.language_zh_cn).toLowerCase());
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m6379();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6380(String str, String str2) {
        File file = new File(m6372());
        if (file.exists() && !file.isDirectory()) {
            file = new File(SettingsActivity.f13215);
            file.mkdirs();
        } else if (!file.exists()) {
            file.mkdirs();
        } else {
            File[] listFiles = file.listFiles();
            if (listFiles != null && listFiles.length >= 5) {
                for (File r2 : listFiles) {
                    Utils.m6421(r2);
                }
            }
        }
        File file2 = new File(file.getAbsolutePath() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str);
        try {
            file2.createNewFile();
        } catch (IOException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        if (!Utils.m6427(str2, file2, new HashMap[0])) {
            return null;
        }
        return file2.getAbsolutePath();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<String> m6381(MediaInfo mediaInfo) {
        String replace = TitleHelper.m15970(mediaInfo.getName()).replace("Marvel's ", "").replace("DC's ", "");
        ArrayList<String> arrayList = new ArrayList<>();
        if (mediaInfo.getYear() > 0) {
            arrayList.add(replace + StringUtils.SPACE + mediaInfo.getYear());
            arrayList.add(replace + " (" + mediaInfo.getYear() + ")");
            arrayList.add(replace + "(" + mediaInfo.getYear() + ")");
        } else {
            arrayList.add(replace);
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ArrayList<SubtitlesInfo> m6382(MediaInfo mediaInfo, int i, int i2);

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    @java.lang.SafeVarargs
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.ArrayList<java.lang.String> m6383(java.lang.String r32, java.util.ArrayList<java.lang.String>... r33) {
        /*
            r31 = this;
            java.io.File r23 = new java.io.File
            r0 = r23
            r1 = r32
            r0.<init>(r1)
            java.lang.String r21 = r23.getParent()
            java.io.File r7 = new java.io.File
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            r23.<init>()
            r0 = r23
            r1 = r21
            java.lang.StringBuilder r23 = r0.append(r1)
            java.lang.String r24 = "/"
            java.lang.StringBuilder r23 = r23.append(r24)
            java.util.Random r24 = new java.util.Random
            r24.<init>()
            r25 = 10000(0x2710, float:1.4013E-41)
            int r24 = r24.nextInt(r25)
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            r0 = r23
            r7.<init>(r0)
            boolean r23 = r7.exists()
            if (r23 != 0) goto L_0x0047
            boolean r23 = r7.isDirectory()
            if (r23 != 0) goto L_0x004a
        L_0x0047:
            com.typhoon.tv.utils.Utils.m6421((java.io.File) r7)
        L_0x004a:
            r7.mkdirs()
            java.lang.String r22 = r7.getAbsolutePath()
            java.util.ArrayList r20 = new java.util.ArrayList
            r20.<init>()
            if (r33 == 0) goto L_0x0070
            r0 = r33
            int r0 = r0.length
            r23 = r0
            if (r23 <= 0) goto L_0x0070
            r23 = 0
            r23 = r33[r23]
            if (r23 == 0) goto L_0x0070
            r23 = 0
            r23 = r33[r23]
            r0 = r20
            r1 = r23
            r0.addAll(r1)
        L_0x0070:
            int r10 = m6374((java.lang.String) r32)
            switch(r10) {
                case 0: goto L_0x0111;
                case 1: goto L_0x00da;
                case 2: goto L_0x0078;
                case 3: goto L_0x0108;
                case 4: goto L_0x0108;
                case 5: goto L_0x0108;
                case 6: goto L_0x0108;
                default: goto L_0x0077;
            }
        L_0x0077:
            return r20
        L_0x0078:
            r0 = r32
            r1 = r22
            com.typhoon.tv.extractor.RarExtractor.m15917(r0, r1)     // Catch:{ Exception -> 0x00b5 }
        L_0x007f:
            java.lang.String[] r11 = r7.list()
            if (r11 == 0) goto L_0x0077
            int r0 = r11.length
            r23 = r0
            if (r23 <= 0) goto L_0x0077
            int r0 = r11.length
            r26 = r0
            r23 = 0
            r25 = r23
        L_0x0091:
            r0 = r25
            r1 = r26
            if (r0 >= r1) goto L_0x0077
            r12 = r11[r25]
            java.io.File r9 = new java.io.File
            java.lang.String r23 = r7.getAbsolutePath()
            r0 = r23
            r9.<init>(r0, r12)
            boolean r23 = r9.isDirectory()
            if (r23 == 0) goto L_0x0235
            java.lang.String[] r13 = r9.list()
            if (r13 != 0) goto L_0x016c
        L_0x00b0:
            int r23 = r25 + 1
            r25 = r23
            goto L_0x0091
        L_0x00b5:
            r5 = move-exception
            r23 = 0
            r0 = r23
            boolean[] r0 = new boolean[r0]
            r23 = r0
            r0 = r23
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r0)
            r0 = r32
            r1 = r22
            com.typhoon.tv.extractor.ZipExtractor.m15918(r0, r1)     // Catch:{ Exception -> 0x00cb }
            goto L_0x007f
        L_0x00cb:
            r6 = move-exception
            r23 = 0
            r0 = r23
            boolean[] r0 = new boolean[r0]
            r23 = r0
            r0 = r23
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)
            goto L_0x0077
        L_0x00da:
            r0 = r32
            r1 = r22
            com.typhoon.tv.extractor.ZipExtractor.m15918(r0, r1)     // Catch:{ Exception -> 0x00e2 }
            goto L_0x007f
        L_0x00e2:
            r5 = move-exception
            r23 = 0
            r0 = r23
            boolean[] r0 = new boolean[r0]
            r23 = r0
            r0 = r23
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r5, (boolean[]) r0)
            r0 = r32
            r1 = r22
            com.typhoon.tv.extractor.RarExtractor.m15917(r0, r1)     // Catch:{ Exception -> 0x00f8 }
            goto L_0x007f
        L_0x00f8:
            r6 = move-exception
            r23 = 0
            r0 = r23
            boolean[] r0 = new boolean[r0]
            r23 = r0
            r0 = r23
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r6, (boolean[]) r0)
            goto L_0x0077
        L_0x0108:
            r0 = r20
            r1 = r32
            r0.add(r1)
            goto L_0x0077
        L_0x0111:
            java.io.File r8 = new java.io.File
            r0 = r32
            r8.<init>(r0)
            java.io.File r15 = new java.io.File
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            r23.<init>()
            java.lang.String r24 = r8.getAbsolutePath()
            java.lang.String r25 = r8.getAbsolutePath()
            java.lang.String r26 = "/"
            int r25 = r25.lastIndexOf(r26)
            java.lang.String r24 = r24.substring(r25)
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r24 = "/"
            java.lang.StringBuilder r23 = r23.append(r24)
            java.util.Random r24 = new java.util.Random
            r24.<init>()
            r25 = 10000(0x2710, float:1.4013E-41)
            int r24 = r24.nextInt(r25)
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r24 = ".srt"
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            r0 = r23
            r15.<init>(r0)
            r8.renameTo(r15)
            java.lang.String r23 = r15.getAbsolutePath()
            r0 = r20
            r1 = r23
            r0.add(r1)
            goto L_0x0077
        L_0x016c:
            int r0 = r13.length
            r27 = r0
            r23 = 0
            r24 = r23
        L_0x0173:
            r0 = r24
            r1 = r27
            if (r0 >= r1) goto L_0x00b0
            r8 = r13[r24]
            java.io.File r17 = new java.io.File
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            r23.<init>()
            java.lang.String r28 = r9.getAbsolutePath()
            r0 = r23
            r1 = r28
            java.lang.StringBuilder r23 = r0.append(r1)
            java.lang.String r28 = "/"
            r0 = r23
            r1 = r28
            java.lang.StringBuilder r23 = r0.append(r1)
            r0 = r23
            java.lang.StringBuilder r23 = r0.append(r8)
            java.lang.String r23 = r23.toString()
            r0 = r17
            r1 = r23
            r0.<init>(r1)
            java.lang.String r19 = r17.getAbsolutePath()
            boolean r23 = r17.isDirectory()
            if (r23 == 0) goto L_0x0207
            java.lang.String[] r18 = r17.list()
            if (r18 != 0) goto L_0x01bf
        L_0x01ba:
            int r23 = r24 + 1
            r24 = r23
            goto L_0x0173
        L_0x01bf:
            r0 = r18
            int r0 = r0.length
            r28 = r0
            r23 = 0
        L_0x01c6:
            r0 = r23
            r1 = r28
            if (r0 >= r1) goto L_0x01ba
            r4 = r18[r23]
            java.io.File r29 = new java.io.File
            r0 = r29
            r1 = r19
            r0.<init>(r1, r4)
            java.lang.String r3 = r29.getAbsolutePath()
            int r14 = m6374((java.lang.String) r3)
            r29 = 1
            r0 = r29
            if (r14 == r0) goto L_0x01eb
            r29 = 2
            r0 = r29
            if (r14 != r0) goto L_0x0201
        L_0x01eb:
            r29 = 1
            r0 = r29
            java.util.ArrayList[] r0 = new java.util.ArrayList[r0]
            r29 = r0
            r30 = 0
            r29[r30] = r20
            r0 = r31
            r1 = r29
            r0.m6383((java.lang.String) r3, (java.util.ArrayList<java.lang.String>[]) r1)
        L_0x01fe:
            int r23 = r23 + 1
            goto L_0x01c6
        L_0x0201:
            r0 = r20
            r0.add(r3)
            goto L_0x01fe
        L_0x0207:
            int r14 = m6374((java.lang.String) r19)
            r23 = 1
            r0 = r23
            if (r14 == r0) goto L_0x0217
            r23 = 2
            r0 = r23
            if (r14 != r0) goto L_0x022d
        L_0x0217:
            r23 = 1
            r0 = r23
            java.util.ArrayList[] r0 = new java.util.ArrayList[r0]
            r23 = r0
            r28 = 0
            r23[r28] = r20
            r0 = r31
            r1 = r19
            r2 = r23
            r0.m6383((java.lang.String) r1, (java.util.ArrayList<java.lang.String>[]) r2)
            goto L_0x01ba
        L_0x022d:
            r0 = r20
            r1 = r19
            r0.add(r1)
            goto L_0x01ba
        L_0x0235:
            java.lang.String r16 = r9.getAbsolutePath()
            int r14 = m6374((java.lang.String) r16)
            r23 = 1
            r0 = r23
            if (r14 == r0) goto L_0x0249
            r23 = 2
            r0 = r23
            if (r14 != r0) goto L_0x0260
        L_0x0249:
            r23 = 1
            r0 = r23
            java.util.ArrayList[] r0 = new java.util.ArrayList[r0]
            r23 = r0
            r24 = 0
            r23[r24] = r20
            r0 = r31
            r1 = r16
            r2 = r23
            r0.m6383((java.lang.String) r1, (java.util.ArrayList<java.lang.String>[]) r2)
            goto L_0x00b0
        L_0x0260:
            r0 = r20
            r1 = r16
            r0.add(r1)
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.subtitles.BaseSubtitlesService.m6383(java.lang.String, java.util.ArrayList[]):java.util.ArrayList");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6384(Integer num, Integer num2, String str) {
        m6373();
        String[] strArr = {"第" + num + "季", "第" + Utils.m6413(num.intValue()) + "季", "第" + m6375(num.intValue()) + "季", num + "季", Utils.m6413(num.intValue()) + "季", m6375(num.intValue()) + "季", "s" + Utils.m6413(num.intValue()), "s" + num.toString()};
        String[] strArr2 = {"第" + num2 + "集", "第" + Utils.m6413(num2.intValue()) + "集", "第" + m6375(num2.intValue()) + "集", num2 + "集", Utils.m6413(num2.intValue()) + "集", m6375(num2.intValue()) + "集", "e" + Utils.m6413(num2.intValue()), "e" + num2.toString()};
        boolean z = false;
        boolean z2 = false;
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            if (str.trim().toLowerCase().contains(strArr[i])) {
                z = true;
                break;
            }
            i++;
        }
        int length2 = strArr2.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length2) {
                break;
            }
            if (str.trim().toLowerCase().contains(strArr2[i2])) {
                z2 = true;
                break;
            }
            i2++;
        }
        return z && z2;
    }
}
