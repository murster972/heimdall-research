package com.typhoon.tv.subtitles;

public class SubtitlesConverter {
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d8 A[SYNTHETIC, Splitter:B:63:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00fa A[SYNTHETIC, Splitter:B:77:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00ff A[SYNTHETIC, Splitter:B:80:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0116 A[SYNTHETIC, Splitter:B:90:0x0116] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x011b A[SYNTHETIC, Splitter:B:93:0x011b] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m16818(java.lang.String r16, com.Ty.subtitle.converter.subtitleFile.TimedTextFileFormat r17, boolean r18) {
        /*
            java.lang.String r5 = com.typhoon.tv.utils.Utils.m6405((java.lang.String) r16)     // Catch:{ Exception -> 0x00a4 }
            r14 = -1
            int r15 = r5.hashCode()     // Catch:{ Exception -> 0x00a4 }
            switch(r15) {
                case 1467283: goto L_0x001c;
                case 1484069: goto L_0x0032;
                case 1484551: goto L_0x0011;
                case 1484563: goto L_0x0027;
                case 1489193: goto L_0x0053;
                case 45562920: goto L_0x0048;
                case 46052685: goto L_0x003d;
                default: goto L_0x000c;
            }     // Catch:{ Exception -> 0x00a4 }
        L_0x000c:
            switch(r14) {
                case 0: goto L_0x005e;
                case 1: goto L_0x0072;
                case 2: goto L_0x0072;
                case 3: goto L_0x0078;
                case 4: goto L_0x007e;
                case 5: goto L_0x007e;
                case 6: goto L_0x007e;
                default: goto L_0x000f;
            }     // Catch:{ Exception -> 0x00a4 }
        L_0x000f:
            r9 = 0
        L_0x0010:
            return r9
        L_0x0011:
            java.lang.String r15 = ".srt"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 0
            goto L_0x000c
        L_0x001c:
            java.lang.String r15 = ".ass"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 1
            goto L_0x000c
        L_0x0027:
            java.lang.String r15 = ".ssa"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 2
            goto L_0x000c
        L_0x0032:
            java.lang.String r15 = ".scc"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 3
            goto L_0x000c
        L_0x003d:
            java.lang.String r15 = ".ttml"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 4
            goto L_0x000c
        L_0x0048:
            java.lang.String r15 = ".dfxp"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 5
            goto L_0x000c
        L_0x0053:
            java.lang.String r15 = ".xml"
            boolean r15 = r5.equals(r15)     // Catch:{ Exception -> 0x00a4 }
            if (r15 == 0) goto L_0x000c
            r14 = 6
            goto L_0x000c
        L_0x005e:
            com.Ty.subtitle.converter.subtitleFile.FormatSRT r8 = new com.Ty.subtitle.converter.subtitleFile.FormatSRT     // Catch:{ Exception -> 0x00a4 }
            r8.<init>()     // Catch:{ Exception -> 0x00a4 }
        L_0x0063:
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x00a4 }
            r0 = r16
            r12.<init>(r0)     // Catch:{ Exception -> 0x00a4 }
            boolean r14 = r12.exists()     // Catch:{ Exception -> 0x00a4 }
            if (r14 != 0) goto L_0x0088
            r9 = 0
            goto L_0x0010
        L_0x0072:
            com.Ty.subtitle.converter.subtitleFile.FormatASS r8 = new com.Ty.subtitle.converter.subtitleFile.FormatASS     // Catch:{ Exception -> 0x00a4 }
            r8.<init>()     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0063
        L_0x0078:
            com.Ty.subtitle.converter.subtitleFile.FormatSCC r8 = new com.Ty.subtitle.converter.subtitleFile.FormatSCC     // Catch:{ Exception -> 0x00a4 }
            r8.<init>()     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0063
        L_0x007e:
            if (r18 == 0) goto L_0x0086
            com.Ty.subtitle.converter.subtitleFile.FormatTTML r8 = new com.Ty.subtitle.converter.subtitleFile.FormatTTML     // Catch:{ Exception -> 0x00a4 }
            r8.<init>()     // Catch:{ Exception -> 0x00a4 }
        L_0x0085:
            goto L_0x0063
        L_0x0086:
            r8 = 0
            goto L_0x0085
        L_0x0088:
            if (r8 != 0) goto L_0x00b1
            okio.Source r14 = okio.Okio.m20512((java.io.File) r12)     // Catch:{ Exception -> 0x00a4 }
            okio.BufferedSource r11 = okio.Okio.m20507((okio.Source) r14)     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r9 = r11.m20458()     // Catch:{ Exception -> 0x00a4 }
            r11.close()     // Catch:{ IOException -> 0x009b }
            goto L_0x0010
        L_0x009b:
            r4 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r14)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0010
        L_0x00a4:
            r4 = move-exception
            java.lang.String r14 = "Error converting subtitles..."
            r15 = 0
            boolean[] r15 = new boolean[r15]
            com.typhoon.tv.Logger.m6280(r4, r14, r15)
            r9 = 0
            goto L_0x0010
        L_0x00b1:
            java.lang.String r3 = com.typhoon.tv.utils.Utils.m6407((java.io.File) r12)     // Catch:{ Exception -> 0x00a4 }
            r13 = 0
            r6 = 0
            r1 = 0
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00f1 }
            r7.<init>(r12)     // Catch:{ Exception -> 0x00f1 }
            net.pempek.unicode.UnicodeBOMInputStream r2 = new net.pempek.unicode.UnicodeBOMInputStream     // Catch:{ Exception -> 0x015a, all -> 0x0153 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x015a, all -> 0x0153 }
            java.lang.String r14 = r12.getName()     // Catch:{ Exception -> 0x015d, all -> 0x0156 }
            com.Ty.subtitle.converter.subtitleFile.model.TimedTextObject r13 = r8.m7489(r14, r2, r3)     // Catch:{ Exception -> 0x015d, all -> 0x0156 }
            if (r2 == 0) goto L_0x00cf
            r2.close()     // Catch:{ Exception -> 0x00df }
        L_0x00cf:
            if (r7 == 0) goto L_0x0161
            r7.close()     // Catch:{ Exception -> 0x00e7 }
            r1 = r2
            r6 = r7
        L_0x00d6:
            if (r13 == 0) goto L_0x00dc
            java.util.TreeMap<java.lang.Integer, com.Ty.subtitle.converter.subtitleFile.model.Caption> r14 = r13.f6539     // Catch:{ Exception -> 0x00a4 }
            if (r14 != 0) goto L_0x012f
        L_0x00dc:
            r9 = 0
            goto L_0x0010
        L_0x00df:
            r4 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r14)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00cf
        L_0x00e7:
            r4 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r14)     // Catch:{ Exception -> 0x00a4 }
            r1 = r2
            r6 = r7
            goto L_0x00d6
        L_0x00f1:
            r4 = move-exception
        L_0x00f2:
            r14 = 0
            boolean[] r14 = new boolean[r14]     // Catch:{ all -> 0x0113 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r14)     // Catch:{ all -> 0x0113 }
            if (r1 == 0) goto L_0x00fd
            r1.close()     // Catch:{ Exception -> 0x010b }
        L_0x00fd:
            if (r6 == 0) goto L_0x00d6
            r6.close()     // Catch:{ Exception -> 0x0103 }
            goto L_0x00d6
        L_0x0103:
            r4 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r14)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00d6
        L_0x010b:
            r4 = move-exception
            r14 = 0
            boolean[] r14 = new boolean[r14]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r14)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00fd
        L_0x0113:
            r14 = move-exception
        L_0x0114:
            if (r1 == 0) goto L_0x0119
            r1.close()     // Catch:{ Exception -> 0x011f }
        L_0x0119:
            if (r6 == 0) goto L_0x011e
            r6.close()     // Catch:{ Exception -> 0x0127 }
        L_0x011e:
            throw r14     // Catch:{ Exception -> 0x00a4 }
        L_0x011f:
            r4 = move-exception
            r15 = 0
            boolean[] r15 = new boolean[r15]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r15)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0119
        L_0x0127:
            r4 = move-exception
            r15 = 0
            boolean[] r15 = new boolean[r15]     // Catch:{ Exception -> 0x00a4 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r15)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x011e
        L_0x012f:
            java.lang.String r14 = "Typhoon"
            r13.f6544 = r14     // Catch:{ Exception -> 0x00a4 }
            r0 = r17
            java.lang.Object r10 = r0.m7488(r13)     // Catch:{ Exception -> 0x00a4 }
            if (r10 != 0) goto L_0x013f
            r9 = 0
            goto L_0x0010
        L_0x013f:
            boolean r14 = r10 instanceof java.lang.String[]     // Catch:{ Exception -> 0x00a4 }
            if (r14 != 0) goto L_0x0146
            r9 = 0
            goto L_0x0010
        L_0x0146:
            java.lang.String r14 = "\n"
            java.lang.String[] r10 = (java.lang.String[]) r10     // Catch:{ Exception -> 0x00a4 }
            java.lang.String[] r10 = (java.lang.String[]) r10     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r9 = android.text.TextUtils.join(r14, r10)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0010
        L_0x0153:
            r14 = move-exception
            r6 = r7
            goto L_0x0114
        L_0x0156:
            r14 = move-exception
            r1 = r2
            r6 = r7
            goto L_0x0114
        L_0x015a:
            r4 = move-exception
            r6 = r7
            goto L_0x00f2
        L_0x015d:
            r4 = move-exception
            r1 = r2
            r6 = r7
            goto L_0x00f2
        L_0x0161:
            r1 = r2
            r6 = r7
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.subtitles.SubtitlesConverter.m16818(java.lang.String, com.Ty.subtitle.converter.subtitleFile.TimedTextFileFormat, boolean):java.lang.String");
    }
}
