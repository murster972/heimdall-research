package com.typhoon.tv.subtitles.international;

import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class Addic7ed extends BaseSubtitlesService {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f13051 = {"Specials", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth", "Twentieth", "Twenty-first", "Twenty-second", "Twenty-third", "Twenty-fourth", "Twenty-fifth", "Twenty-sixth", "Twenty-seventh", "Twenty-eighth", "Twenty-ninth"};

    /* renamed from: 靐  reason: contains not printable characters */
    private String m16825(MediaInfo mediaInfo) {
        String r9 = HttpHelper.m6343().m6351("https://www.addic7ed.com/srch.php?search=" + Utils.m6414(mediaInfo.getName(), new boolean[0]) + "&Submit=Search", (Map<String, String>[]) new Map[0]);
        if (r9.isEmpty()) {
            return null;
        }
        ArrayList<ArrayList<String>> r1 = Regex.m17804(r9, "<a href=\"(/original/[^\"]+)\">([^<]+)\\((\\d{4})\\)</a>\\s*</div>", 3, 34);
        ArrayList arrayList = r1.get(0);
        ArrayList arrayList2 = r1.get(1);
        ArrayList arrayList3 = r1.get(2);
        for (int i = 0; i < arrayList.size(); i++) {
            try {
                String str = (String) arrayList.get(i);
                String str2 = (String) arrayList3.get(i);
                if (TitleHelper.m15971(mediaInfo.getName()).equals(TitleHelper.m15971((String) arrayList2.get(i))) && (str2.trim().isEmpty() || !Utils.m6426(str2.trim()) || mediaInfo.getYear() <= 0 || Integer.parseInt(str2.trim()) == mediaInfo.getYear())) {
                    return str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? "https://www.addic7ed.com" + str : str;
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private ArrayList<String> m16826() {
        String string = TVApplication.m6285().getString("pref_sub_language_international", Locale.getDefault().getDisplayLanguage(Locale.US));
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(!string.contains(",") ? new String[]{string} : string.split(",")));
        boolean z = false;
        for (int i = 0; i < arrayList.size(); i++) {
            String str = arrayList.get(i);
            char c = 65535;
            switch (str.hashCode()) {
                case -1883983667:
                    if (str.equals("Chinese")) {
                        c = 1;
                        break;
                    }
                    break;
                case 295659104:
                    if (str.equals("Portuguese (Brazil)")) {
                        c = 0;
                        break;
                    }
                    break;
                case 986206080:
                    if (str.equals("Persian")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    arrayList.set(i, "Brazillian Portuguese".trim().toLowerCase());
                    break;
                case 1:
                    arrayList.set(i, "Big 5 code".trim().toLowerCase());
                    z = true;
                    break;
                case 2:
                    arrayList.set(i, "Farsi/Persian".trim().toLowerCase());
                    break;
                default:
                    arrayList.set(i, str.trim().toLowerCase());
                    break;
            }
        }
        if (z) {
            arrayList.add("Chinese BG code".trim().toLowerCase());
        }
        return arrayList;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private String m16827(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -1753753522:
                if (str.equals("Marvel's Jessica Jones")) {
                    c = 1;
                    break;
                }
                break;
            case -1430551969:
                if (str.equals("DC's Legends of Tomorrow")) {
                    c = 2;
                    break;
                }
                break;
            case 1728305255:
                if (str.equals("Marvel's Agents of S.H.I.E.L.D.")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return "Agents of Shield";
            case 1:
                return "Jessica Jones";
            case 2:
                return "Legends of Tomorrow";
            default:
                return str.replace("Marvel's ", "");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m16828(MediaInfo mediaInfo, int i) {
        String r14 = m16827(mediaInfo.getName());
        int length = f13051.length;
        int i2 = length;
        if (i >= length) {
            return null;
        }
        String str = f13051[i];
        TmdbApi r20 = TmdbApi.m15742();
        TmdbApi tmdbApi = r20;
        int r17 = r20.m15754(mediaInfo, String.valueOf(i));
        StringBuilder append = new StringBuilder().append(r14).append(" - ");
        StringBuilder sb = append;
        boolean[] zArr = new boolean[0];
        boolean[] zArr2 = zArr;
        boolean[] zArr3 = zArr;
        boolean[] zArr4 = zArr3;
        String str2 = "https://www.addic7ed.com/srch.php?search=" + Utils.m6414(append.append(str).append(" Season").toString(), zArr3) + "&Submit=Search";
        HttpHelper r202 = HttpHelper.m6343();
        Map[] mapArr = new Map[0];
        Map[] mapArr2 = mapArr;
        Map[] mapArr3 = mapArr;
        HttpHelper httpHelper = r202;
        String r12 = r202.m6351(str2, (Map<String, String>[]) mapArr3);
        if (r12.isEmpty()) {
            return null;
        }
        Object obj = "<a href=\"(/original/[^\"]+)\">([^<]+)\\((\\d{4})\\)</a>\\s*</div>";
        ArrayList<ArrayList<String>> r4 = Regex.m17804(r12, "<a href=\"(/original/[^\"]+)\">([^<]+)\\((\\d{4})\\)</a>\\s*</div>", 3, 34);
        ArrayList arrayList = r4.get(0);
        ArrayList arrayList2 = r4.get(1);
        ArrayList arrayList3 = r4.get(2);
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            try {
                String str3 = (String) arrayList.get(i3);
                String str4 = (String) arrayList2.get(i3);
                String str5 = (String) arrayList3.get(i3);
                if (str4.toLowerCase().contains("season")) {
                    Object obj2 = "";
                    if (!str4.trim().toLowerCase().contains(str.replace("-", "").trim().toLowerCase())) {
                        String valueOf = String.valueOf(i);
                        String str6 = valueOf;
                        if (!str4.contains(valueOf)) {
                            continue;
                        }
                    }
                    String r19 = Regex.m17801(str4, "(.+?)\\s+-\\s+.*(?:season|special(?:s)?)", 1, 2);
                    if (r19.isEmpty()) {
                        r19 = Regex.m17801(str4, "(.+?)\\s+season", 1, 2);
                    }
                    if (!r19.isEmpty()) {
                        str4 = r19;
                    }
                    if (TitleHelper.m15971(r14).equals(TitleHelper.m15971(str4)) && (str5.trim().isEmpty() || !Utils.m6426(str5.trim()) || r17 <= 0 || Integer.parseInt(str5.trim()) == r17)) {
                        String str7 = str3;
                        return str3.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? "https://www.addic7ed.com" + str3 : str3;
                    }
                } else {
                    continue;
                }
            } catch (Exception e) {
                boolean[] zArr5 = new boolean[0];
                boolean[] zArr6 = zArr5;
                Logger.m6281((Throwable) e, zArr5);
            }
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ArrayList<String> m16829(String str) {
        String r2 = Regex.m17800(HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[0]), "<a href=\"(.+?)\" rel=\"nofollow\" onclick=\"Download", 1);
        if (r2.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            r2 = "https://www.addic7ed.com" + r2;
        }
        String r1 = m6380("Addic7ed_" + new Random().nextInt(5000) + ".zip", r2);
        if (r1 == null || r1.isEmpty()) {
            return null;
        }
        return m6383(r1, (ArrayList<String>[]) new ArrayList[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16830() {
        return "Addic7ed";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<SubtitlesInfo> m16831(MediaInfo mediaInfo, int i, int i2) {
        ArrayList<SubtitlesInfo> arrayList = new ArrayList<>();
        boolean z = mediaInfo.getType() == 1;
        String r23 = z ? m16825(mediaInfo) : m16828(mediaInfo, i);
        if (r23 != null && !r23.isEmpty()) {
            HttpHelper r24 = HttpHelper.m6343();
            Map[] mapArr = new Map[0];
            HttpHelper httpHelper = r24;
            String str = r23;
            Map[] mapArr2 = mapArr;
            String r22 = r24.m6351(r23, (Map<String, String>[]) mapArr);
            ArrayList<String> r7 = m16826();
            ArrayList arrayList2 = new ArrayList();
            ArrayList<ArrayList<String>> r8 = Regex.m17804(r22, "<td class=\"a1\">\\s*<a href=\"(/original/[^\"]+)\">\\s*<span class=\"[^\"]+ (\\buttonDownload)\">\\s*([^\r\n\t]+)\\s*</span>\\s*<span>\\s*([^\r\n\t]+)\\s*</span>\\s*</a>\\s*</td>", 4, 34);
            ArrayList arrayList3 = r8.get(0);
            ArrayList arrayList4 = r8.get(1);
            ArrayList arrayList5 = r8.get(2);
            ArrayList arrayList6 = r8.get(3);
            TyphoonTV typhoonTV = new TyphoonTV();
            int i3 = 0;
            while (true) {
                int size = arrayList3.size();
                int i4 = size;
                if (i3 >= size) {
                    break;
                }
                ArrayList arrayList7 = arrayList3;
                try {
                    String str2 = (String) arrayList3.get(i3);
                    String str3 = (String) arrayList4.get(i3);
                    String str4 = (String) arrayList5.get(i3);
                    String str5 = (String) arrayList6.get(i3);
                    String lowerCase = str5.trim().toLowerCase();
                    String lowerCase2 = str4.trim().toLowerCase();
                    String str6 = lowerCase2;
                    if (r7.contains(lowerCase2) && !str3.trim().toLowerCase().equals("bad-icon")) {
                        if (!z) {
                            MediaInfo mediaInfo2 = mediaInfo;
                            int i5 = i;
                            int i6 = i2;
                            if (!typhoonTV.m15981(typhoonTV.m15980(lowerCase), mediaInfo, i, i2)) {
                            }
                        }
                        if ((!lowerCase.contains("trailer") || mediaInfo.getName().trim().toLowerCase().contains("trailer")) && !arrayList2.contains(str5 + "|" + str4.trim().toLowerCase())) {
                            if (str3.trim().toLowerCase().contains("positive")) {
                                String str7 = str5 + "|" + str4.trim().toLowerCase();
                                String str8 = str7;
                                arrayList2.add(str7);
                            }
                            String str9 = str2;
                            String str10 = str2;
                            Object obj = InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
                            if (str2.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                StringBuilder append = new StringBuilder().append("https://www.addic7ed.com");
                                StringBuilder sb = append;
                                str9 = append.append(str2).toString();
                            }
                            SubtitlesInfo subtitlesInfo = r0;
                            SubtitlesInfo subtitlesInfo2 = new SubtitlesInfo(4, str5, str4, str9);
                            ArrayList<SubtitlesInfo> arrayList8 = arrayList;
                            SubtitlesInfo subtitlesInfo3 = subtitlesInfo2;
                            arrayList.add(subtitlesInfo2);
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                i3++;
            }
        }
        return arrayList;
    }
}
