package com.typhoon.tv.subtitles.international;

import com.typhoon.tv.I18N;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class OpenSubtitles extends BaseSubtitlesService {

    /* renamed from: 靐  reason: contains not printable characters */
    private static String f13052;

    /* renamed from: 齉  reason: contains not printable characters */
    private static String f13053;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f13054 = (TyphoonApp.f5907 + "/Typhoon-Server/opensubtitles.txt");

    public static class OpenSubtitlesInfo extends SubtitlesInfo {

        /* renamed from: 龘  reason: contains not printable characters */
        private int f13055 = -1;

        public OpenSubtitlesInfo(int i, String str, String str2, String str3) {
            super(i, str, str2, str3);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m16836() {
            return this.f13055;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m16837(int i) {
            this.f13055 = i;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m16832() {
        String[] split;
        String str;
        String string = TVApplication.m6285().getString("pref_sub_language_international", Locale.getDefault().getDisplayLanguage(Locale.US));
        if (!string.contains(",")) {
            boolean z = true | true;
            split = new String[]{string};
        } else {
            split = string.split(",");
        }
        List asList = Arrays.asList(split);
        ArrayList arrayList = new ArrayList();
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : I18N.m15708().entrySet()) {
            String str2 = (String) next.getValue();
            if (asList.contains(next.getKey()) && !arrayList.contains(str2)) {
                arrayList.add(str2);
                sb.append(str2).append(",");
            }
        }
        String sb2 = sb.toString();
        if (sb2.isEmpty() || sb2.length() < 3) {
            str = "eng";
            String str3 = sb2;
            String str4 = sb2;
        } else {
            if (sb2.endsWith(",")) {
                sb2 = sb2.substring(0, sb2.length() - 1);
            }
            String str5 = sb2;
            String str6 = sb2;
            str = sb2;
        }
        return str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ArrayList<String> m16833(String str) {
        String r0 = m6380("OpenSub_" + new Random().nextInt(5000) + ".zip", str);
        return (r0 == null || r0.isEmpty()) ? null : m6383(r0, (ArrayList<String>[]) new ArrayList[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16834() {
        return "OpenSubtitles";
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0393  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0205  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0219  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0237  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0276  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02b7  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0385  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.typhoon.tv.model.SubtitlesInfo> m16835(com.typhoon.tv.model.media.MediaInfo r38, int r39, int r40) {
        /*
            r37 = this;
            java.lang.String r32 = f13052
            if (r32 == 0) goto L_0x000c
            java.lang.String r32 = f13052
            boolean r32 = r32.isEmpty()
            if (r32 == 0) goto L_0x0014
        L_0x000c:
            java.lang.String r31 = "useragent_v1"
            java.lang.String r31 = "useragent_v1"
            f13052 = r31
        L_0x0014:
            int r32 = r38.getType()
            r33 = 1
            r0 = r32
            r1 = r33
            r1 = r33
            if (r0 != r1) goto L_0x00e7
            r12 = 1
        L_0x0023:
            java.util.ArrayList r26 = new java.util.ArrayList
            r26.<init>()
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            de.timroes.axmlrpc.XMLRPCClient r23 = new de.timroes.axmlrpc.XMLRPCClient     // Catch:{ Exception -> 0x00ea }
            java.net.URL r32 = new java.net.URL     // Catch:{ Exception -> 0x00ea }
            java.lang.String r33 = "http://api.opensubtitles.org/xml-rpc"
            java.lang.String r33 = "http://api.opensubtitles.org/xml-rpc"
            r32.<init>(r33)     // Catch:{ Exception -> 0x00ea }
            r33 = 16384(0x4000, float:2.2959E-41)
            r0 = r23
            r0 = r23
            r1 = r32
            r2 = r33
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00ea }
            r32 = 45
            r0 = r23
            r0 = r23
            r1 = r32
            r1 = r32
            r0.m18291((int) r1)
            java.lang.String r32 = f13052
            r0 = r23
            r0 = r23
            r1 = r32
            r1 = r32
            r0.m18292((java.lang.String) r1)
            r11 = 0
            java.lang.String r32 = f13053     // Catch:{ Exception -> 0x00fd }
            if (r32 == 0) goto L_0x006e
            java.lang.String r32 = f13053     // Catch:{ Exception -> 0x00fd }
            boolean r32 = r32.isEmpty()     // Catch:{ Exception -> 0x00fd }
            if (r32 == 0) goto L_0x00da
        L_0x006e:
            java.lang.String r32 = "LogIn"
            r33 = 4
            r0 = r33
            r0 = r33
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x00fd }
            r33 = r0
            r34 = 0
            java.lang.String r35 = ""
            java.lang.String r35 = ""
            r33[r34] = r35     // Catch:{ Exception -> 0x00fd }
            r34 = 1
            java.lang.String r35 = ""
            r33[r34] = r35     // Catch:{ Exception -> 0x00fd }
            r34 = 2
            java.lang.String r35 = "eng"
            r33[r34] = r35     // Catch:{ Exception -> 0x00fd }
            r34 = 3
            java.lang.String r35 = f13052     // Catch:{ Exception -> 0x00fd }
            r33[r34] = r35     // Catch:{ Exception -> 0x00fd }
            r0 = r23
            r1 = r32
            r2 = r33
            java.lang.Object r15 = r0.m18290((java.lang.String) r1, (java.lang.Object[]) r2)     // Catch:{ Exception -> 0x00fd }
            boolean r0 = r15 instanceof java.util.HashMap     // Catch:{ Exception -> 0x00fd }
            r32 = r0
            r32 = r0
            if (r32 == 0) goto L_0x00da
            r0 = r15
            r0 = r15
            java.util.HashMap r0 = (java.util.HashMap) r0     // Catch:{ Exception -> 0x00fd }
            r21 = r0
            r21 = r0
            java.lang.String r32 = "token"
            java.lang.String r32 = "token"
            r0 = r21
            r0 = r21
            r1 = r32
            boolean r32 = r0.containsKey(r1)     // Catch:{ Exception -> 0x00fd }
            if (r32 == 0) goto L_0x00da
            java.lang.String r32 = "token"
            java.lang.String r32 = "token"
            r0 = r21
            r1 = r32
            java.lang.Object r32 = r0.get(r1)     // Catch:{ Exception -> 0x00fd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x00fd }
            f13053 = r32     // Catch:{ Exception -> 0x00fd }
            r11 = 1
        L_0x00da:
            java.lang.String r32 = f13053
            if (r32 == 0) goto L_0x00e6
            java.lang.String r32 = f13053
            boolean r32 = r32.isEmpty()
            if (r32 == 0) goto L_0x01d2
        L_0x00e6:
            return r26
        L_0x00e7:
            r12 = 0
            goto L_0x0023
        L_0x00ea:
            r7 = move-exception
            r32 = 0
            r0 = r32
            r0 = r32
            boolean[] r0 = new boolean[r0]
            r32 = r0
            r32 = r0
            r0 = r32
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x00e6
        L_0x00fd:
            r7 = move-exception
            r32 = 0
            r0 = r32
            boolean[] r0 = new boolean[r0]
            r32 = r0
            r32 = r0
            r0 = r32
            r0 = r32
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            java.lang.Throwable r32 = r7.getCause()
            r0 = r32
            boolean r0 = r0 instanceof java.io.FileNotFoundException
            r32 = r0
            if (r32 == 0) goto L_0x00da
            r32 = 4000(0xfa0, double:1.9763E-320)
            r32 = 4000(0xfa0, double:1.9763E-320)
            java.lang.Thread.sleep(r32)     // Catch:{ Throwable -> 0x01a6 }
        L_0x0122:
            java.lang.String r32 = f13053     // Catch:{ Exception -> 0x01be }
            if (r32 == 0) goto L_0x012e
            java.lang.String r32 = f13053     // Catch:{ Exception -> 0x01be }
            boolean r32 = r32.isEmpty()     // Catch:{ Exception -> 0x01be }
            if (r32 == 0) goto L_0x00da
        L_0x012e:
            java.lang.String r32 = "LogIn"
            java.lang.String r32 = "LogIn"
            r33 = 4
            r0 = r33
            r0 = r33
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x01be }
            r33 = r0
            r34 = 0
            java.lang.String r35 = ""
            java.lang.String r35 = ""
            r33[r34] = r35     // Catch:{ Exception -> 0x01be }
            r34 = 1
            java.lang.String r35 = ""
            java.lang.String r35 = ""
            r33[r34] = r35     // Catch:{ Exception -> 0x01be }
            r34 = 2
            java.lang.String r35 = "en"
            java.lang.String r35 = "en"
            r33[r34] = r35     // Catch:{ Exception -> 0x01be }
            r34 = 3
            java.lang.String r35 = f13052     // Catch:{ Exception -> 0x01be }
            r33[r34] = r35     // Catch:{ Exception -> 0x01be }
            r0 = r23
            r0 = r23
            r1 = r32
            r2 = r33
            r2 = r33
            java.lang.Object r15 = r0.m18290((java.lang.String) r1, (java.lang.Object[]) r2)     // Catch:{ Exception -> 0x01be }
            boolean r0 = r15 instanceof java.util.HashMap     // Catch:{ Exception -> 0x01be }
            r32 = r0
            if (r32 == 0) goto L_0x00da
            r0 = r15
            java.util.HashMap r0 = (java.util.HashMap) r0     // Catch:{ Exception -> 0x01be }
            r21 = r0
            java.lang.String r32 = "token"
            java.lang.String r32 = "token"
            r0 = r21
            r1 = r32
            r1 = r32
            boolean r32 = r0.containsKey(r1)     // Catch:{ Exception -> 0x01be }
            if (r32 == 0) goto L_0x00da
            java.lang.String r32 = "token"
            java.lang.String r32 = "token"
            r0 = r21
            r0 = r21
            r1 = r32
            java.lang.Object r32 = r0.get(r1)     // Catch:{ Exception -> 0x01be }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x01be }
            f13053 = r32     // Catch:{ Exception -> 0x01be }
            r11 = 1
            goto L_0x00da
        L_0x01a6:
            r30 = move-exception
            r32 = 0
            r0 = r32
            r0 = r32
            boolean[] r0 = new boolean[r0]
            r32 = r0
            r0 = r30
            r0 = r30
            r1 = r32
            r1 = r32
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
            goto L_0x0122
        L_0x01be:
            r8 = move-exception
            r32 = 0
            r0 = r32
            r0 = r32
            boolean[] r0 = new boolean[r0]
            r32 = r0
            r0 = r32
            r0 = r32
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r8, (boolean[]) r0)
            goto L_0x00da
        L_0x01d2:
            java.lang.String r14 = r37.m16832()
            java.util.HashMap r16 = new java.util.HashMap
            r16.<init>()
            java.lang.String r32 = "sublanguageid"
            r0 = r16
            r0 = r16
            r1 = r32
            r1 = r32
            r0.put(r1, r14)
            java.lang.String r9 = r38.getImdbId()
            if (r9 == 0) goto L_0x01f5
            boolean r32 = r9.isEmpty()
            if (r32 == 0) goto L_0x0211
        L_0x01f5:
            int r32 = r38.getType()
            r33 = 1
            r0 = r32
            r0 = r32
            r1 = r33
            r1 = r33
            if (r0 != r1) goto L_0x0385
            com.typhoon.tv.api.TmdbApi r32 = com.typhoon.tv.api.TmdbApi.m15742()
            int r33 = r38.getTmdbId()
            java.lang.String r9 = r32.m15744(r33)
        L_0x0211:
            if (r9 == 0) goto L_0x0393
            boolean r32 = r9.isEmpty()
            if (r32 != 0) goto L_0x0393
            java.lang.String r32 = "imdbid"
            java.lang.String r33 = r38.getImdbId()
            java.lang.String r34 = "[^0-9]"
            java.lang.String r35 = ""
            java.lang.String r33 = r33.replaceAll(r34, r35)
            r0 = r16
            r1 = r32
            r2 = r33
            r2 = r33
            r0.put(r1, r2)
        L_0x0235:
            if (r12 != 0) goto L_0x0274
            r32 = -1
            r0 = r39
            r1 = r32
            r1 = r32
            if (r0 <= r1) goto L_0x025a
            java.lang.String r32 = "season"
            java.lang.String r32 = "season"
            java.lang.Integer r33 = java.lang.Integer.valueOf(r39)
            r0 = r16
            r0 = r16
            r1 = r32
            r1 = r32
            r2 = r33
            r2 = r33
            r0.put(r1, r2)
        L_0x025a:
            r32 = -1
            r0 = r40
            r0 = r40
            r1 = r32
            if (r0 <= r1) goto L_0x0274
            java.lang.String r32 = "episode"
            java.lang.Integer r33 = java.lang.Integer.valueOf(r40)
            r0 = r16
            r1 = r32
            r2 = r33
            r0.put(r1, r2)
        L_0x0274:
            if (r11 != 0) goto L_0x027b
            r32 = 3500(0xdac, double:1.729E-320)
            java.lang.Thread.sleep(r32)     // Catch:{ Throwable -> 0x03fc }
        L_0x027b:
            java.lang.String r32 = "SearchSubtitles"
            java.lang.String r32 = "SearchSubtitles"
            r33 = 2
            r0 = r33
            r0 = r33
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ XMLRPCException -> 0x0410 }
            r33 = r0
            r33 = r0
            r34 = 0
            java.lang.String r35 = f13053     // Catch:{ XMLRPCException -> 0x0410 }
            r33[r34] = r35     // Catch:{ XMLRPCException -> 0x0410 }
            r34 = 1
            r35 = 1
            r0 = r35
            r0 = r35
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ XMLRPCException -> 0x0410 }
            r35 = r0
            r36 = 0
            r35[r36] = r16     // Catch:{ XMLRPCException -> 0x0410 }
            r33[r34] = r35     // Catch:{ XMLRPCException -> 0x0410 }
            r0 = r23
            r0 = r23
            r1 = r32
            r1 = r32
            r2 = r33
            r2 = r33
            java.lang.Object r25 = r0.m18290((java.lang.String) r1, (java.lang.Object[]) r2)     // Catch:{ XMLRPCException -> 0x0410 }
            if (r25 == 0) goto L_0x00e6
            r0 = r25
            r0 = r25
            boolean r0 = r0 instanceof java.util.HashMap
            r32 = r0
            r32 = r0
            if (r32 == 0) goto L_0x04fb
            r24 = r25
            r24 = r25
            java.util.HashMap r24 = (java.util.HashMap) r24
            java.lang.String r32 = "data"
            r0 = r24
            r0 = r24
            r1 = r32
            boolean r32 = r0.containsKey(r1)
            if (r32 == 0) goto L_0x04fb
            java.lang.String r32 = "data"
            java.lang.String r32 = "data"
            r0 = r24
            r0 = r24
            r1 = r32
            r1 = r32
            java.lang.Object r4 = r0.get(r1)
            boolean r0 = r4 instanceof java.lang.Object[]
            r32 = r0
            r32 = r0
            if (r32 == 0) goto L_0x04fb
            java.lang.Object[] r4 = (java.lang.Object[]) r4
            r29 = r4
            java.lang.Object[] r29 = (java.lang.Object[]) r29
            java.util.HashMap r32 = com.typhoon.tv.I18N.m15708()
            java.util.HashMap r22 = com.typhoon.tv.utils.Utils.m6416(r32)
            r0 = r29
            r0 = r29
            int r0 = r0.length
            r33 = r0
            r32 = 0
        L_0x0309:
            r0 = r32
            r1 = r33
            if (r0 >= r1) goto L_0x04fb
            r17 = r29[r32]
            r0 = r17
            r0 = r17
            boolean r0 = r0 instanceof java.util.HashMap
            r34 = r0
            r34 = r0
            if (r34 == 0) goto L_0x0382
            r18 = r17
            java.util.HashMap r18 = (java.util.HashMap) r18
            java.lang.String r34 = "ZipDownloadLink"
            r0 = r18
            r0 = r18
            r1 = r34
            r1 = r34
            boolean r34 = r0.containsKey(r1)
            if (r34 == 0) goto L_0x0382
            r34 = 2131821122(0x7f110242, float:1.9274978E38)
            java.lang.String r28 = com.typhoon.tv.I18N.m15706(r34)
            java.lang.String r34 = "SubFileName"
            r0 = r18
            r0 = r18
            r1 = r34
            boolean r34 = r0.containsKey(r1)
            if (r34 == 0) goto L_0x0359
            java.lang.String r34 = "SubFileName"
            r0 = r18
            r0 = r18
            r1 = r34
            java.lang.Object r34 = r0.get(r1)
            java.lang.String r28 = r34.toString()
        L_0x0359:
            java.lang.String r34 = r28.trim()
            java.lang.String r34 = r34.toLowerCase()
            java.lang.String r35 = "trailer"
            boolean r34 = r34.contains(r35)
            if (r34 == 0) goto L_0x0422
            java.lang.String r34 = r38.getName()
            java.lang.String r34 = r34.trim()
            java.lang.String r34 = r34.toLowerCase()
            java.lang.String r35 = "trailer"
            java.lang.String r35 = "trailer"
            boolean r34 = r34.contains(r35)
            if (r34 != 0) goto L_0x0422
        L_0x0382:
            int r32 = r32 + 1
            goto L_0x0309
        L_0x0385:
            com.typhoon.tv.api.TmdbApi r32 = com.typhoon.tv.api.TmdbApi.m15742()
            int r33 = r38.getTmdbId()
            java.lang.String r9 = r32.m15745(r33)
            goto L_0x0211
        L_0x0393:
            if (r12 == 0) goto L_0x03cc
            java.lang.String r32 = r38.getName()
            java.lang.String r32 = com.typhoon.tv.helper.TitleHelper.m15966(r32)
            java.lang.String r33 = "Marvel's "
            java.lang.String r34 = ""
            java.lang.String r34 = ""
            java.lang.String r32 = r32.replace(r33, r34)
            java.lang.String r33 = "DC's "
            java.lang.String r33 = "DC's "
            java.lang.String r34 = ""
            java.lang.String r34 = ""
            java.lang.String r20 = r32.replace(r33, r34)
        L_0x03ba:
            java.lang.String r32 = "query"
            r0 = r16
            r0 = r16
            r1 = r32
            r1 = r32
            r2 = r20
            r0.put(r1, r2)
            goto L_0x0235
        L_0x03cc:
            java.lang.String r32 = r38.getName()
            java.lang.String r33 = "Marvel's "
            java.lang.String r33 = "Marvel's "
            java.lang.String r34 = ""
            java.lang.String r34 = ""
            java.lang.String r32 = r32.replace(r33, r34)
            java.lang.String r33 = "DC's "
            java.lang.String r34 = ""
            java.lang.String r32 = r32.replace(r33, r34)
            java.lang.String r32 = com.typhoon.tv.helper.TitleHelper.m15968(r32)
            java.lang.String r33 = " "
            java.lang.String r34 = "+"
            java.lang.String r34 = "+"
            java.lang.String r20 = r32.replace(r33, r34)
            goto L_0x03ba
        L_0x03fc:
            r30 = move-exception
            r32 = 0
            r0 = r32
            boolean[] r0 = new boolean[r0]
            r32 = r0
            r0 = r30
            r1 = r32
            r1 = r32
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r0, (boolean[]) r1)
            goto L_0x027b
        L_0x0410:
            r7 = move-exception
            r32 = 0
            r0 = r32
            r0 = r32
            boolean[] r0 = new boolean[r0]
            r32 = r0
            r0 = r32
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r7, (boolean[]) r0)
            goto L_0x00e6
        L_0x0422:
            r5 = -1
            java.lang.String r34 = "SubDownloadsCnt"
            java.lang.String r34 = "SubDownloadsCnt"
            r0 = r18
            r0 = r18
            r1 = r34
            r1 = r34
            boolean r34 = r0.containsKey(r1)
            if (r34 == 0) goto L_0x0490
            java.lang.String r34 = "SubDownloadsCnt"
            r0 = r18
            r1 = r34
            java.lang.Object r34 = r0.get(r1)
            java.lang.String r6 = r34.toString()
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            r0 = r34
            r0 = r34
            r1 = r28
            r1 = r28
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r35 = " ["
            java.lang.String r35 = " ["
            java.lang.StringBuilder r34 = r34.append(r35)
            r35 = 2131820738(0x7f1100c2, float:1.92742E38)
            java.lang.String r35 = com.typhoon.tv.I18N.m15706(r35)
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r35 = " "
            java.lang.StringBuilder r34 = r34.append(r35)
            r0 = r34
            r0 = r34
            java.lang.StringBuilder r34 = r0.append(r6)
            java.lang.String r35 = "]"
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r28 = r34.toString()
            boolean r34 = com.typhoon.tv.utils.Utils.m6426((java.lang.String) r6)
            if (r34 == 0) goto L_0x0490
            int r5 = java.lang.Integer.parseInt(r6)
        L_0x0490:
            r34 = 2131821122(0x7f110242, float:1.9274978E38)
            java.lang.String r27 = com.typhoon.tv.I18N.m15706(r34)
            java.lang.String r34 = "SubLanguageID"
            java.lang.String r34 = "SubLanguageID"
            r0 = r18
            r0 = r18
            r1 = r34
            boolean r34 = r0.containsKey(r1)
            if (r34 == 0) goto L_0x04c8
            java.lang.String r34 = "SubLanguageID"
            r0 = r18
            r1 = r34
            java.lang.Object r34 = r0.get(r1)
            java.lang.String r13 = r34.toString()
            r0 = r22
            boolean r34 = r0.containsKey(r13)
            if (r34 == 0) goto L_0x04c8
            r0 = r22
            java.lang.Object r27 = r0.get(r13)
            java.lang.String r27 = (java.lang.String) r27
        L_0x04c8:
            com.typhoon.tv.subtitles.international.OpenSubtitles$OpenSubtitlesInfo r10 = new com.typhoon.tv.subtitles.international.OpenSubtitles$OpenSubtitlesInfo
            r34 = 3
            java.lang.String r35 = "ZipDownloadLink"
            java.lang.String r35 = "ZipDownloadLink"
            r0 = r18
            r0 = r18
            r1 = r35
            java.lang.Object r35 = r0.get(r1)
            java.lang.String r35 = r35.toString()
            r0 = r34
            r0 = r34
            r1 = r28
            r1 = r28
            r2 = r27
            r3 = r35
            r10.<init>(r0, r1, r2, r3)
            r10.m16837(r5)
            r0 = r19
            r0 = r19
            r0.add(r10)
            goto L_0x0382
        L_0x04fb:
            r0 = r26
            r1 = r19
            r0.addAll(r1)
            goto L_0x00e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.subtitles.international.OpenSubtitles.m16835(com.typhoon.tv.model.media.MediaInfo, int, int):java.util.ArrayList");
    }
}
