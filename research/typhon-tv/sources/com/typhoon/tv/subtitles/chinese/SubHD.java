package com.typhoon.tv.subtitles.chinese;

import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SubHD extends BaseSubtitlesService {
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x015a, code lost:
        r10 = r11.getAsString();
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<java.lang.String> m16822(java.lang.String r24) {
        /*
            r23 = this;
            com.typhoon.tv.helper.http.HttpHelper r18 = com.typhoon.tv.helper.http.HttpHelper.m6343()
            r19 = 0
            r0 = r19
            java.util.Map[] r0 = new java.util.Map[r0]
            r19 = r0
            r0 = r18
            r1 = r24
            r2 = r19
            java.lang.String r8 = r0.m6351((java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
            org.jsoup.nodes.Document r9 = org.jsoup.Jsoup.m21674(r8)
            java.lang.String r17 = ""
            java.lang.String r18 = "button.btn-danger[sid]"
            r0 = r18
            org.jsoup.select.Elements r7 = r9.m21815(r0)
            java.util.Iterator r18 = r7.iterator()
        L_0x002a:
            boolean r19 = r18.hasNext()
            if (r19 == 0) goto L_0x0055
            java.lang.Object r6 = r18.next()
            org.jsoup.nodes.Element r6 = (org.jsoup.nodes.Element) r6
            java.lang.String r19 = "btn"
            r0 = r19
            boolean r19 = r6.m21872(r0)
            if (r19 == 0) goto L_0x002a
            java.lang.String r19 = "btn-sm"
            r0 = r19
            boolean r19 = r6.m21872(r0)
            if (r19 == 0) goto L_0x002a
            java.lang.String r18 = "sid"
            r0 = r18
            java.lang.String r17 = r6.m21947(r0)
        L_0x0055:
            boolean r18 = r17.isEmpty()
            if (r18 == 0) goto L_0x005e
            r18 = 0
        L_0x005d:
            return r18
        L_0x005e:
            java.lang.String r5 = "http://subhd.com/ajax/down_ajax"
            java.util.HashMap r14 = com.typhoon.tv.TyphoonApp.m6325()
            java.lang.String r18 = "Content-Type"
            java.lang.String r19 = "application/x-www-form-urlencoded; charset=UTF-8"
            r0 = r18
            r1 = r19
            r14.put(r0, r1)
            java.lang.String r18 = "Accept"
            java.lang.String r19 = "application/json, text/javascript, */*; q=0.01"
            r0 = r18
            r1 = r19
            r14.put(r0, r1)
            java.lang.String r18 = "Host"
            java.lang.String r19 = "http://subhd.com"
            java.lang.String r20 = "http://"
            java.lang.String r21 = ""
            java.lang.String r19 = r19.replace(r20, r21)
            java.lang.String r20 = "/"
            java.lang.String r21 = ""
            java.lang.String r19 = r19.replace(r20, r21)
            r0 = r18
            r1 = r19
            r14.put(r0, r1)
            java.lang.String r18 = "Origin"
            java.lang.String r19 = "http://subhd.com/"
            r0 = r18
            r1 = r19
            r14.put(r0, r1)
            java.lang.String r18 = "Referer"
            r0 = r18
            r1 = r24
            r14.put(r0, r1)
            java.lang.String r18 = "User-Agent"
            java.lang.String r19 = com.typhoon.tv.TyphoonApp.f5835
            r0 = r18
            r1 = r19
            r14.put(r0, r1)
            com.typhoon.tv.helper.http.HttpHelper r18 = com.typhoon.tv.helper.http.HttpHelper.m6343()
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "sub_id="
            java.lang.StringBuilder r19 = r19.append(r20)
            r20 = 0
            r0 = r20
            boolean[] r0 = new boolean[r0]
            r20 = r0
            r0 = r17
            r1 = r20
            java.lang.String r20 = com.typhoon.tv.utils.Utils.m6414((java.lang.String) r0, (boolean[]) r1)
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r19 = r19.toString()
            r20 = 1
            r21 = 1
            r0 = r21
            java.util.Map[] r0 = new java.util.Map[r0]
            r21 = r0
            r22 = 0
            r21[r22] = r14
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            java.lang.String r4 = r0.m6360((java.lang.String) r5, (java.lang.String) r1, (boolean) r2, (java.util.Map<java.lang.String, java.lang.String>[]) r3)
            boolean r18 = r4.isEmpty()
            if (r18 != 0) goto L_0x0116
            java.lang.String r18 = "PHP Error"
            r0 = r18
            boolean r18 = r4.contains(r0)
            if (r18 == 0) goto L_0x011a
        L_0x0116:
            r18 = 0
            goto L_0x005d
        L_0x011a:
            com.google.gson.JsonParser r18 = new com.google.gson.JsonParser     // Catch:{ Exception -> 0x0131 }
            r18.<init>()     // Catch:{ Exception -> 0x0131 }
            r0 = r18
            com.google.gson.JsonElement r15 = r0.parse((java.lang.String) r4)     // Catch:{ Exception -> 0x0131 }
            if (r15 == 0) goto L_0x012d
            boolean r18 = r15.isJsonObject()
            if (r18 != 0) goto L_0x0143
        L_0x012d:
            r18 = 0
            goto L_0x005d
        L_0x0131:
            r12 = move-exception
            r18 = 0
            r0 = r18
            boolean[] r0 = new boolean[r0]
            r18 = r0
            r0 = r18
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r12, (boolean[]) r0)
            r18 = 0
            goto L_0x005d
        L_0x0143:
            com.google.gson.JsonObject r18 = r15.getAsJsonObject()
            java.lang.String r19 = "url"
            com.google.gson.JsonElement r11 = r18.get(r19)
            if (r11 == 0) goto L_0x0156
            boolean r18 = r11.isJsonNull()
            if (r18 == 0) goto L_0x015a
        L_0x0156:
            r18 = 0
            goto L_0x005d
        L_0x015a:
            java.lang.String r10 = r11.getAsString()
            java.lang.String r18 = "/"
            r0 = r18
            int r18 = r10.lastIndexOf(r0)
            int r19 = r10.length()
            r0 = r18
            r1 = r19
            java.lang.String r13 = r10.substring(r0, r1)
            r0 = r23
            java.lang.String r16 = r0.m6380((java.lang.String) r13, (java.lang.String) r10)
            if (r16 == 0) goto L_0x0181
            boolean r18 = r16.isEmpty()
            if (r18 == 0) goto L_0x0185
        L_0x0181:
            r18 = 0
            goto L_0x005d
        L_0x0185:
            r18 = 0
            r0 = r18
            java.util.ArrayList[] r0 = new java.util.ArrayList[r0]
            r18 = r0
            r0 = r23
            r1 = r16
            r2 = r18
            java.util.ArrayList r18 = r0.m6383((java.lang.String) r1, (java.util.ArrayList<java.lang.String>[]) r2)
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.subtitles.chinese.SubHD.m16822(java.lang.String):java.util.ArrayList");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16823() {
        return "SubHD";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<SubtitlesInfo> m16824(MediaInfo mediaInfo, int i, int i2) {
        Element r6;
        boolean z = mediaInfo.getType() == 1;
        ArrayList<SubtitlesInfo> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        Iterator<String> it2 = (z ? m6381(mediaInfo) : m6376(mediaInfo, i, i2)).iterator();
        while (it2.hasNext()) {
            Iterator it3 = Jsoup.m21674(HttpHelper.m6343().m6351("http://subhd.com/search/" + Utils.m6414(it2.next(), new boolean[0]).replace("+", "%20"), (Map<String, String>[]) new Map[0])).m21815("div.box").iterator();
            while (it3.hasNext()) {
                Element element = (Element) it3.next();
                Element r9 = element.m21837("div.d_title");
                if (!(r9 == null || (r6 = r9.m21837("a[href]")) == null)) {
                    String r24 = r6.m21822();
                    if (r24.trim().isEmpty()) {
                        r24 = I18N.m15706(R.string.unknown);
                    }
                    String r11 = r6.m21947("href");
                    if (!r11.trim().isEmpty() && !arrayList2.contains(r11)) {
                        arrayList2.add(r11);
                        if (!z) {
                            if (!m6384(Integer.valueOf(i), Integer.valueOf(i2), r24)) {
                                Element r10 = element.m21837("div.tvlist");
                                if (r10 != null) {
                                    String lowerCase = r10.m21868().trim().toLowerCase();
                                    if (!lowerCase.equals("s" + Utils.m6413(i) + "e" + Utils.m6413(i2)) && !lowerCase.equals("s" + i + "e" + i2)) {
                                    }
                                }
                            }
                        }
                        String str = r11;
                        if (r11.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                            str = "http://subhd.com" + r11;
                        }
                        StringBuilder sb = new StringBuilder();
                        String r15 = I18N.m15706(R.string.unknown);
                        Elements r14 = element.m21815("span.label");
                        if (r14 != null && r14.size() > 0) {
                            Iterator it4 = r14.iterator();
                            while (it4.hasNext()) {
                                Element element2 = (Element) it4.next();
                                if (!element2.m21872("label-default") || !element2.m21951(PubnativeAsset.TITLE)) {
                                    String r25 = element2.m21868();
                                    if (r25.contains("简")) {
                                        sb.append(I18N.m15706(R.string.language_zh_cn)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                    }
                                    if (r25.contains("繁")) {
                                        sb.append(I18N.m15706(R.string.language_zh_tw)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                    }
                                    if (r25.contains("英")) {
                                        sb.append(I18N.m15706(R.string.language_en)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                    }
                                }
                            }
                            if (sb.toString().trim().isEmpty()) {
                                r15 = I18N.m15706(R.string.unknown);
                            } else {
                                String sb2 = sb.toString();
                                r15 = sb2.substring(0, sb2.length() - 1);
                            }
                        }
                        if (m6378(r15)) {
                            arrayList.add(new SubtitlesInfo(2, r24, r15, str));
                        }
                    }
                }
            }
        }
        return arrayList;
    }
}
