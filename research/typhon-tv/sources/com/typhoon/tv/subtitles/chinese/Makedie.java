package com.typhoon.tv.subtitles.chinese;

import com.typhoon.tv.I18N;
import com.typhoon.tv.Logger;
import com.typhoon.tv.R;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import net.pubnative.library.request.PubnativeRequest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Makedie extends BaseSubtitlesService {

    /* renamed from: 龘  reason: contains not printable characters */
    private HashMap<String, String> f13050;

    public Makedie() {
        if (this.f13050 == null || this.f13050.isEmpty()) {
            this.f13050 = new HashMap<>();
            this.f13050.put(AbstractSpiCall.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            this.f13050.put("Accept-Language", "en-US,en;q=0.9,zh-HK;q=0.8,zh-TW;q=0.7,zh;q=0.6");
            this.f13050.put("Host", "http://assrt.net".replace("https://", "").replace("http://", "").replace(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, ""));
            this.f13050.put("Upgrade-Insecure-Requests", PubnativeRequest.LEGACY_ZONE_ID);
            this.f13050.put(AbstractSpiCall.HEADER_USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ArrayList<String> m16819(String str) {
        String decode;
        Elements r1 = Jsoup.m21674(HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[]{this.f13050})).m21815(".download");
        if (r1.isEmpty()) {
            return null;
        }
        Elements r0 = r1.first().m21815("a[href]");
        if (r0.isEmpty()) {
            return null;
        }
        String r4 = r0.first().m21947("href");
        try {
            decode = URLDecoder.decode(r4, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            decode = URLDecoder.decode(r4);
        }
        String r7 = m6380(decode.substring(decode.lastIndexOf(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR), decode.length()), "http://assrt.net" + decode);
        if (r7 == null || r7.isEmpty()) {
            return null;
        }
        return m6383(r7, (ArrayList<String>[]) new ArrayList[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16820() {
        return "Makedie";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<SubtitlesInfo> m16821(MediaInfo mediaInfo, int i, int i2) {
        String substring;
        boolean z = mediaInfo.getType() == 1;
        ArrayList<SubtitlesInfo> arrayList = new ArrayList<>();
        ArrayList<String> r19 = z ? m6381(mediaInfo) : m6376(mediaInfo, i, i2);
        ArrayList arrayList2 = new ArrayList();
        Iterator<String> it2 = r19.iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            if (!next.isEmpty()) {
                String format = String.format("http://assrt.net/sub/?searchword=%s&utm_source=xbmc&utm_medium=xbmc&utm_campaign=search", new Object[]{Utils.m6414(next.replace("'", ""), new boolean[0]).replace("+", "%20")});
                Elements r28 = Jsoup.m21674(HttpHelper.m6343().m6351(format, (Map<String, String>[]) new Map[]{this.f13050})).m21815(".subitem");
                if (r28 == null || r28.isEmpty()) {
                    String r20 = HttpHelper.m6343().m6363(format, false, (Map<String, String>[]) new Map[]{this.f13050});
                    if (!r20.equals(format)) {
                        String r22 = Regex.m17800(r20, "/xml/sub/\\d+/(\\d+).xml", 1);
                        if (!r22.isEmpty() && Utils.m6426(r22)) {
                            int parseInt = Integer.parseInt(r22);
                            if (!arrayList2.contains(Integer.valueOf(parseInt))) {
                                String r11 = HttpHelper.m6343().m6351(r20, (Map<String, String>[]) new Map[]{this.f13050});
                                if (r11 == null || r11.isEmpty()) {
                                    break;
                                }
                                String replace = Jsoup.m21674(r11).m21772().replace("字幕 -", "").replace("射手网(伪)", "");
                                arrayList2.add(Integer.valueOf(parseInt));
                                arrayList.add(new SubtitlesInfo(1, replace, I18N.m15706(R.string.unknown), String.format("http://assrt.net/xml/sub/%1$s/%2$s.xml", new Object[]{String.valueOf(parseInt).substring(0, 3), Integer.valueOf(parseInt)})));
                            } else {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                } else {
                    Iterator it3 = r28.iterator();
                    while (it3.hasNext()) {
                        Element element = (Element) it3.next();
                        Elements r14 = element.m21815(".introtitle[href]");
                        if (r14 != null && !r14.isEmpty()) {
                            Element first = r14.first();
                            String r222 = Regex.m17800(first.m21947("href"), "/xml/sub/\\d+/(\\d+).xml", 1);
                            if (!r222.isEmpty() && Utils.m6426(r222)) {
                                int parseInt2 = Integer.parseInt(r222);
                                if (!arrayList2.contains(Integer.valueOf(parseInt2))) {
                                    String r30 = I18N.m15706(R.string.unknown);
                                    if (first.m21947(PubnativeAsset.TITLE) != null && !first.m21947(PubnativeAsset.TITLE).isEmpty()) {
                                        r30 = first.m21947(PubnativeAsset.TITLE);
                                    }
                                    if (!z) {
                                        if (!m6384(Integer.valueOf(i), Integer.valueOf(i2), r30)) {
                                        }
                                    }
                                    StringBuilder sb = new StringBuilder();
                                    Elements r31 = element.m21815("#sublist_div");
                                    if (r31 != null && r31.size() >= 1) {
                                        Iterator it4 = r31.first().m21815(TtmlNode.TAG_SPAN).iterator();
                                        while (it4.hasNext()) {
                                            Element element2 = (Element) it4.next();
                                            if (element2.m21822().startsWith("语言")) {
                                                if (element2.m21822().contains("双语")) {
                                                    sb.append(I18N.m15706(R.string.language_zh_tw)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).append(I18N.m15706(R.string.language_zh_cn)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                                } else {
                                                    if (element2.m21822().contains("简")) {
                                                        sb.append(I18N.m15706(R.string.language_zh_cn)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                                    }
                                                    if (element2.m21822().contains("繁")) {
                                                        sb.append(I18N.m15706(R.string.language_zh_tw)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                                    }
                                                }
                                                if (element2.m21822().contains("英")) {
                                                    sb.append(I18N.m15706(R.string.language_en)).append(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                                                }
                                            }
                                        }
                                    }
                                    if (sb.toString().trim().isEmpty()) {
                                        substring = I18N.m15706(R.string.unknown);
                                    } else {
                                        String sb2 = sb.toString();
                                        substring = sb2.substring(0, sb2.length() - 1);
                                    }
                                    arrayList2.add(Integer.valueOf(parseInt2));
                                    String format2 = String.format("http://assrt.net/xml/sub/%1$s/%2$s.xml", new Object[]{String.valueOf(parseInt2).substring(0, 3), Integer.valueOf(parseInt2)});
                                    if (m6378(substring) || substring.equals(I18N.m15706(R.string.unknown))) {
                                        arrayList.add(new SubtitlesInfo(1, r30, substring, format2));
                                    }
                                }
                            }
                        }
                    }
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
            }
        }
        return arrayList;
    }
}
