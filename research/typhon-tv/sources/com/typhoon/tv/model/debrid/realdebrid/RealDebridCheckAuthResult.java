package com.typhoon.tv.model.debrid.realdebrid;

public class RealDebridCheckAuthResult {
    private String client_id;
    private String client_secret;

    public String getClient_id() {
        return this.client_id;
    }

    public String getClient_secret() {
        return this.client_secret;
    }

    public void setClient_id(String str) {
        this.client_id = str;
    }

    public void setClient_secret(String str) {
        this.client_secret = str;
    }
}
