package com.typhoon.tv.model.debrid.realdebrid;

public class RealDebridCredentialsInfo {
    private String accessToken;
    private String clientId;
    private String clientSecret;
    private String refreshToken;

    public String getAccessToken() {
        return this.accessToken;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public boolean isValid() {
        return this.accessToken != null && !this.accessToken.isEmpty() && this.refreshToken != null && !this.refreshToken.isEmpty() && this.clientId != null && !this.clientId.isEmpty() && this.clientSecret != null && !this.clientSecret.isEmpty();
    }

    public void setAccessToken(String str) {
        this.accessToken = str;
    }

    public void setClientId(String str) {
        this.clientId = str;
    }

    public void setClientSecret(String str) {
        this.clientSecret = str;
    }

    public void setRefreshToken(String str) {
        this.refreshToken = str;
    }

    public String toString() {
        return "RealDebridCredentialsInfo{accessToken='" + this.accessToken + '\'' + ", refreshToken='" + this.refreshToken + '\'' + ", clientId='" + this.clientId + '\'' + ", clientSecret='" + this.clientSecret + '\'' + '}';
    }
}
