package com.typhoon.tv.model;

import java.util.List;

public class AllucApiResult {
    private List<ResultBean> result;
    private String status;

    public static class ResultBean {
        private String extension;
        private String hostername;
        private List<HosterurlsBean> hosterurls;
        private String lang;
        private String sourcename;
        private String sourcetitle;
        private String sourceurl;
        private String title;

        public static class HosterurlsBean {
            private int part;
            private String url;

            public int getPart() {
                return this.part;
            }

            public String getUrl() {
                return this.url;
            }

            public void setPart(int i) {
                this.part = i;
            }

            public void setUrl(String str) {
                this.url = str;
            }
        }

        public String getExtension() {
            return this.extension;
        }

        public String getHostername() {
            return this.hostername;
        }

        public List<HosterurlsBean> getHosterurls() {
            return this.hosterurls;
        }

        public String getLang() {
            return this.lang;
        }

        public String getSourcename() {
            return this.sourcename;
        }

        public String getSourcetitle() {
            return this.sourcetitle;
        }

        public String getSourceurl() {
            return this.sourceurl;
        }

        public String getTitle() {
            return this.title;
        }

        public void setExtension(String str) {
            this.extension = str;
        }

        public void setHostername(String str) {
            this.hostername = str;
        }

        public void setHosterurls(List<HosterurlsBean> list) {
            this.hosterurls = list;
        }

        public void setLang(String str) {
            this.lang = str;
        }

        public void setSourcename(String str) {
            this.sourcename = str;
        }

        public void setSourcetitle(String str) {
            this.sourcetitle = str;
        }

        public void setSourceurl(String str) {
            this.sourceurl = str;
        }

        public void setTitle(String str) {
            this.title = str;
        }
    }

    public List<ResultBean> getResult() {
        return this.result;
    }

    public String getStatus() {
        return this.status;
    }

    public void setResult(List<ResultBean> list) {
        this.result = list;
    }

    public void setStatus(String str) {
        this.status = str;
    }
}
