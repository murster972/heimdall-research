package com.typhoon.tv.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubtitlesInfo implements Parcelable {
    public static final Parcelable.Creator<SubtitlesInfo> CREATOR = new Parcelable.Creator<SubtitlesInfo>() {
        public SubtitlesInfo createFromParcel(Parcel parcel) {
            return new SubtitlesInfo(parcel);
        }

        public SubtitlesInfo[] newArray(int i) {
            return new SubtitlesInfo[i];
        }
    };
    private String downloadPageUrl;
    private String language;
    private String name;
    private int service;

    public SubtitlesInfo(int i, String str, String str2, String str3) {
        this.service = i;
        this.name = str;
        this.language = str2;
        this.downloadPageUrl = str3;
    }

    private SubtitlesInfo(Parcel parcel) {
        this.service = parcel.readInt();
        this.name = parcel.readString();
        this.language = parcel.readString();
        this.downloadPageUrl = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getDownloadPageUrl() {
        return this.downloadPageUrl;
    }

    public String getLanguage() {
        return this.language;
    }

    public String getName() {
        return this.name;
    }

    public int getService() {
        return this.service;
    }

    public void setDownloadPageUrl(String str) {
        this.downloadPageUrl = str;
    }

    public void setLanguage(String str) {
        this.language = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setService(int i) {
        this.service = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.service);
        parcel.writeString(this.name);
        parcel.writeString(this.language);
        parcel.writeString(this.downloadPageUrl);
    }
}
