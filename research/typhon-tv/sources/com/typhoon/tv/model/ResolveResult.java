package com.typhoon.tv.model;

import java.util.HashMap;

public class ResolveResult {
    private boolean debrid;
    private HashMap<String, String> playHeader;
    private String resolvedLink;
    private String resolvedQuality;
    private String resolverName;

    public ResolveResult(String str, String str2, int i) {
        this(str, str2, String.valueOf(i));
    }

    public ResolveResult(String str, String str2, String str3) {
        this.resolverName = str;
        this.resolvedLink = str2;
        this.resolvedQuality = str3;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ResolveResult resolveResult = (ResolveResult) obj;
        if (this.debrid != resolveResult.debrid) {
            return false;
        }
        if (this.resolverName != null) {
            if (!this.resolverName.equals(resolveResult.resolverName)) {
                return false;
            }
        } else if (resolveResult.resolverName != null) {
            return false;
        }
        if (this.resolvedLink != null) {
            if (!this.resolvedLink.equals(resolveResult.resolvedLink)) {
                return false;
            }
        } else if (resolveResult.resolvedLink != null) {
            return false;
        }
        if (this.resolvedQuality != null) {
            if (!this.resolvedQuality.equals(resolveResult.resolvedQuality)) {
                return false;
            }
        } else if (resolveResult.resolvedQuality != null) {
            return false;
        }
        if (this.playHeader != null) {
            z = this.playHeader.equals(resolveResult.playHeader);
        } else if (resolveResult.playHeader != null) {
            z = false;
        }
        return z;
    }

    public HashMap<String, String> getPlayHeader() {
        return this.playHeader;
    }

    public String getResolvedLink() {
        return this.resolvedLink;
    }

    public String getResolvedQuality() {
        return this.resolvedQuality;
    }

    public String getResolverName() {
        return this.resolverName;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.resolverName != null ? this.resolverName.hashCode() : 0) * 31) + (this.resolvedLink != null ? this.resolvedLink.hashCode() : 0)) * 31) + (this.resolvedQuality != null ? this.resolvedQuality.hashCode() : 0)) * 31) + (this.playHeader != null ? this.playHeader.hashCode() : 0)) * 31;
        if (this.debrid) {
            i = 1;
        }
        return hashCode + i;
    }

    public boolean isDebrid() {
        return this.debrid;
    }

    public void setDebrid(boolean z) {
        this.debrid = z;
    }

    public void setPlayHeader(HashMap<String, String> hashMap) {
        this.playHeader = hashMap;
    }

    public ResolveResult setResolvedLink(String str) {
        this.resolvedLink = str;
        return this;
    }

    public void setResolvedQuality(String str) {
        this.resolvedQuality = str;
    }

    public ResolveResult setResolverName(String str) {
        this.resolverName = str;
        return this;
    }

    public String toString() {
        return "ResolveResult{resolverName='" + this.resolverName + '\'' + ", resolvedLink='" + this.resolvedLink + '\'' + ", resolvedQuality='" + this.resolvedQuality + '\'' + ", playHeader=" + this.playHeader + ", debrid=" + this.debrid + '}';
    }
}
