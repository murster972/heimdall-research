package com.typhoon.tv.model;

import com.orm.SugarRecord;

public class CheckNewMovieReleaseResult extends SugarRecord {
    private int tmdbId;

    public int getTmdbId() {
        return this.tmdbId;
    }

    public void setTmdbId(int i) {
        this.tmdbId = i;
    }
}
