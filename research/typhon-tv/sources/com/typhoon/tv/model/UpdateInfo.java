package com.typhoon.tv.model;

public class UpdateInfo {
    private final String description;
    private final String fileName;
    private boolean updateDisabled = false;
    private final String version;

    public UpdateInfo(String version2, String fileName2, String description2) {
        this.version = version2;
        this.fileName = fileName2;
        this.description = description2;
    }

    public String getDescription() {
        return this.description;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getVersion() {
        return this.version;
    }

    public boolean isUpdateDisabled() {
        return this.updateDisabled;
    }

    public void setUpdateDisabled(boolean updateDisabled2) {
        this.updateDisabled = updateDisabled2;
    }
}
