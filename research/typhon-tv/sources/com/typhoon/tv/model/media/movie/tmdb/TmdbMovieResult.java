package com.typhoon.tv.model.media.movie.tmdb;

import java.util.List;

public class TmdbMovieResult {
    private int page;
    private List<ResultsBean> results;
    private int total_pages;
    private int total_results;

    public static class ResultsBean {
        private boolean adult;
        private String backdrop_path;
        private int id;
        private String original_language;
        private String original_title;
        private String overview;
        private String poster_path;
        private String release_date;
        private String title;
        private boolean video;

        public String getBackdrop_path() {
            return this.backdrop_path;
        }

        public int getId() {
            return this.id;
        }

        public String getOriginal_language() {
            return this.original_language;
        }

        public String getOriginal_title() {
            return this.original_title;
        }

        public String getOverview() {
            return this.overview;
        }

        public String getPoster_path() {
            return this.poster_path;
        }

        public String getRelease_date() {
            return this.release_date;
        }

        public String getTitle() {
            return this.title;
        }

        public boolean isAdult() {
            return this.adult;
        }

        public boolean isVideo() {
            return this.video;
        }

        public void setAdult(boolean adult2) {
            this.adult = adult2;
        }

        public void setBackdrop_path(String backdrop_path2) {
            this.backdrop_path = backdrop_path2;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public void setOriginal_language(String original_language2) {
            this.original_language = original_language2;
        }

        public void setOriginal_title(String original_title2) {
            this.original_title = original_title2;
        }

        public void setOverview(String overview2) {
            this.overview = overview2;
        }

        public void setPoster_path(String poster_path2) {
            this.poster_path = poster_path2;
        }

        public void setRelease_date(String release_date2) {
            this.release_date = release_date2;
        }

        public void setTitle(String title2) {
            this.title = title2;
        }

        public void setVideo(boolean video2) {
            this.video = video2;
        }
    }

    public int getPage() {
        return this.page;
    }

    public List<ResultsBean> getResults() {
        return this.results;
    }

    public int getTotal_pages() {
        return this.total_pages;
    }

    public int getTotal_results() {
        return this.total_results;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public void setResults(List<ResultsBean> results2) {
        this.results = results2;
    }

    public void setTotal_pages(int total_pages2) {
        this.total_pages = total_pages2;
    }

    public void setTotal_results(int total_results2) {
        this.total_results = total_results2;
    }
}
