package com.typhoon.tv.model.media.tv.trakt;

public class TraktCalendarFullResult {
    private EpisodeBean episode;
    private String first_aired;
    private ShowBean show;

    public static class EpisodeBean {
        private String first_aired;
        private int number;
        private Object number_abs;
        private String overview;
        private int runtime;
        private int season;
        private String title;
        private String updated_at;

        public String getFirst_aired() {
            return this.first_aired;
        }

        public int getNumber() {
            return this.number;
        }

        public Object getNumber_abs() {
            return this.number_abs;
        }

        public String getOverview() {
            return this.overview;
        }

        public int getRuntime() {
            return this.runtime;
        }

        public int getSeason() {
            return this.season;
        }

        public String getTitle() {
            return this.title;
        }

        public String getUpdated_at() {
            return this.updated_at;
        }

        public void setFirst_aired(String str) {
            this.first_aired = str;
        }

        public void setNumber(int i) {
            this.number = i;
        }

        public void setNumber_abs(Object obj) {
            this.number_abs = obj;
        }

        public void setOverview(String str) {
            this.overview = str;
        }

        public void setRuntime(int i) {
            this.runtime = i;
        }

        public void setSeason(int i) {
            this.season = i;
        }

        public void setTitle(String str) {
            this.title = str;
        }

        public void setUpdated_at(String str) {
            this.updated_at = str;
        }
    }

    public static class ShowBean {
        private int aired_episodes;
        private String certification;
        private String country;
        private String first_aired;
        private IdsBean ids;
        private String language;
        private String network;
        private String overview;
        private int runtime;
        private String status;
        private String title;
        private Object trailer;
        private String updated_at;
        private int year;

        public static class IdsBean {
            private String imdb;
            private String slug;
            private int tmdb;
            private int trakt;
            private int tvdb;
            private int tvrage;

            public String getImdb() {
                return this.imdb;
            }

            public String getSlug() {
                return this.slug;
            }

            public int getTmdb() {
                return this.tmdb;
            }

            public int getTrakt() {
                return this.trakt;
            }

            public int getTvdb() {
                return this.tvdb;
            }

            public int getTvrage() {
                return this.tvrage;
            }

            public void setImdb(String str) {
                this.imdb = str;
            }

            public void setSlug(String str) {
                this.slug = str;
            }

            public void setTmdb(int i) {
                this.tmdb = i;
            }

            public void setTrakt(int i) {
                this.trakt = i;
            }

            public void setTvdb(int i) {
                this.tvdb = i;
            }

            public void setTvrage(int i) {
                this.tvrage = i;
            }
        }

        public int getAired_episodes() {
            return this.aired_episodes;
        }

        public String getCertification() {
            return this.certification;
        }

        public String getCountry() {
            return this.country;
        }

        public String getFirst_aired() {
            return this.first_aired;
        }

        public IdsBean getIds() {
            return this.ids;
        }

        public String getLanguage() {
            return this.language;
        }

        public String getNetwork() {
            return this.network;
        }

        public String getOverview() {
            return this.overview;
        }

        public int getRuntime() {
            return this.runtime;
        }

        public String getStatus() {
            return this.status;
        }

        public String getTitle() {
            return this.title;
        }

        public Object getTrailer() {
            return this.trailer;
        }

        public String getUpdated_at() {
            return this.updated_at;
        }

        public int getYear() {
            return this.year;
        }

        public void setAired_episodes(int i) {
            this.aired_episodes = i;
        }

        public void setCertification(String str) {
            this.certification = str;
        }

        public void setCountry(String str) {
            this.country = str;
        }

        public void setFirst_aired(String str) {
            this.first_aired = str;
        }

        public void setIds(IdsBean idsBean) {
            this.ids = idsBean;
        }

        public void setLanguage(String str) {
            this.language = str;
        }

        public void setNetwork(String str) {
            this.network = str;
        }

        public void setOverview(String str) {
            this.overview = str;
        }

        public void setRuntime(int i) {
            this.runtime = i;
        }

        public void setStatus(String str) {
            this.status = str;
        }

        public void setTitle(String str) {
            this.title = str;
        }

        public void setTrailer(Object obj) {
            this.trailer = obj;
        }

        public void setUpdated_at(String str) {
            this.updated_at = str;
        }

        public void setYear(int i) {
            this.year = i;
        }
    }

    public EpisodeBean getEpisode() {
        return this.episode;
    }

    public String getFirst_aired() {
        return this.first_aired;
    }

    public ShowBean getShow() {
        return this.show;
    }

    public void setEpisode(EpisodeBean episodeBean) {
        this.episode = episodeBean;
    }

    public void setFirst_aired(String str) {
        this.first_aired = str;
    }

    public void setShow(ShowBean showBean) {
        this.show = showBean;
    }
}
